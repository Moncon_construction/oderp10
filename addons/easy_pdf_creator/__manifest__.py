# noinspection PyStatementEffect
{
    'name': 'Easy PDF creator',
    'version': '1.0',
    'author': 'InceptionMara',
    'price': 25.0,
    'currency': 'USD',
    'website': 'www.tengersoft.mn',
    'category': 'Tools',
    'summary': "Easy PDF report create end print",
    'description': """
        Easy PDF template creator, No development, Quick print(no download) """,
    'images': [
        'static/description/icon.jpg',
    ],
    'depends': [
        'base',
        'web',
        'web_editor',
        'report',
    ],
    'data': [
        'xml/templates.xml',
        'view/pdf_template_generator_view.xml',
        'view/menu_view.xml',
    ],
    'installable': True,
    'application': True,
}
