# -*- coding: utf-8 -*-
##############################################################################
#
#    Author: Nasan-Ochir.Lkh
#    Copyright 2017-2020 Asterisk Technologies LLC
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'Geospatial support for Calendar event',
    'category': 'GeoBI',
    'author': "Nasan-Ochir.Lh developer of Asterisk Technologies LLC",
    'license': 'AGPL-3',
    'website': 'http://asterisk-tech.mn',
    'depends': [
        'base',
        'geoengine_partner',
        'calendar',
        'hr',
        'l10n_mn_sale'
    ],
    'data': [
        'views/geo_calendar_view.xml',
        'report/calendar_event_report_view.xml',
        'security/ir.model.access.csv'
    ],
    'installable': True,
    'application': True,
    'icon': '/geoengine_calender/static/description/icon.png',
    'active': False,
    'description': 'Geoengine calendar'
}
