# -*- coding: utf-8 -*-
##############################################################################
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2017 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved #
##############################################################################

from odoo.addons.base_geoengine import geo_model, fields
import logging
_logger = logging.getLogger(__name__)


import logging
_logger = logging.getLogger(__name__)
class CalendarEvent(geo_model.GeoModel):
    """Add geo_point to calendar.event"""
    _inherit = "calendar.event"

    geo_point = fields.GeoPoint('Addresses coordinate')

from odoo import api, fields, models, _, modules
from odoo.exceptions import Warning
from datetime import datetime, timedelta, tzinfo
from odoo.addons.calendar.models.calendar import calendar_id2real_id

class CalendarEvent(models.Model):
    """Add geo_point to calendar.event"""
    _inherit = "calendar.event"

    start = fields.Datetime('Start', required=False, help="Start date of an event, without time for full days events")
    stop = fields.Datetime('Stop', required=False, help="Stop date of an event, without time for full days events")
    start_date = fields.Date('Start Date', states={'done': [('readonly', True)]}, index=True)
    manager_id = fields.Many2one('res.users','Manager', default=lambda self: self.env.user, states={'open': [('readonly', True)]}, index=True)
    user_id = fields.Many2one('res.users', 'Responsible', states={'open': [('readonly', True)]}, index=True)
    user_of_employee_id = fields.Many2one('hr.employee', 'Responsible of employee', index=True)
    client_partner_id = fields.Many2one('res.partner', 'Customer', states={'open': [('readonly', True)]}, index=True)
    is_geo_event = fields.Boolean(string='Is GEO Event?', default=True, index=True)
    is_unexpected_meeting = fields.Boolean(string='Unexpected meeting?', default=False)
    
    start_datetime = fields.Datetime('Start DateTime', states={'done': [('readonly', True)]}, default=lambda self: fields.Datetime.now())
    stop_datetime = fields.Datetime('End Datetime', states={'done': [('readonly', True)]}, default=lambda self: fields.Datetime.now())
    done_datetime = fields.Datetime('Done Datetime', readonly=True)
    done_datetime_char = fields.Char('Done Datetime', size=128, readonly=True)
    
    section_id = fields.Many2one('crm.team', string='Section', index=True)
    
    allday = fields.Boolean('All Day', states={'done': [('readonly', True)]}, default=True)
    
    is_repeat = fields.Boolean('Is Repeat')
    repeat_type = fields.Selection([('daily', 'Day(s)'), 
                                    ('weekly', 'Week(s)'), 
                                    ('monthly', 'Month(s)'), 
                                    ('yearly', 'Year(s)')], 'Recurrency type', default='weekly', states={'done': [('readonly', True)]})
    invterval_count = fields.Integer('Interval count', default=1)
    repeat_type_all_count = fields.Integer('Repeat Count', default=1)
    
    mon = fields.Boolean('Monday')
    tue = fields.Boolean('Tuesday')
    wed = fields.Boolean('Wednesday')
    thue = fields.Boolean('Thursday')
    fri = fields.Boolean('Friday')
    sat = fields.Boolean('Saturday')
    sun = fields.Boolean('Sunday')

    @api.model
    def create(self, vals):
        self._set_date(vals, id=False)
        if not 'user_id' in vals:  # Else bug with quick_create when we are filter on an other user
            vals['user_id'] = self._uid

        if 'is_geo_event' in self._context:
            vals['is_geo_event'] = True
            #'start_datetime':datetime.now(), 'stop_datetime':datetime.now()

        if 'user_id' in vals:
            user = self.env['res.users'].browse(vals.get('user_id'))
            vals['partner_ids'] = [(6, 0, [user.partner_id.id])]
            employee = self.env['hr.employee'].search([('user_id','=',vals['user_id'])], limit=1)
            if employee:
                vals['user_of_employee_id'] = employee.id

        res = super(CalendarEvent, self).create(vals)

        final_date = res._get_recurrency_end_date()
        vals['final_date'] = final_date
        self.create_attendees()
        if 'is_repeat' in vals:
            if vals.get('is_repeat'):
                self.duplicate_event(vals.get('repeat_type_all_count'), res, vals)
        return res

    @api.multi
    def _set_date(self, values, id=False):
        if values.get('start_datetime') or values.get('start_date') or values.get('start') \
                or values.get('stop_datetime') or values.get('stop_date') or values.get('stop'):
            allday = values.get("allday", None)
            event = self
            if allday is None:
                if id:
                    allday = event.allday
                else:
                    allday = False
                    _logger.warning("Calendar - All day is not specified, arbitrarily set to False")
                    #raise osv.except_osv(_('Error!'), ("Need to know if it's an allday or not..."))

            key = "date" if allday else "datetime"
            notkey = "datetime" if allday else "date"

            for fld in ('start', 'stop'):
                if values.get('%s_%s' % (fld, key)) or values.get(fld):
                    values['%s_%s' % (fld, key)] = values.get('%s_%s' % (fld, key)) or values.get(fld)
                    values['%s_%s' % (fld, notkey)] = None
                    if fld not in values.keys():
                        values[fld] = values['%s_%s' % (fld, key)]

            diff = False
            if allday and (values.get('stop_date') or values.get('start_date')):
                stop_date = values.get('stop_date') or event.stop_date
                start_date = values.get('start_date') or event.start_date
                if stop_date and start_date:
                    diff = fields.Date.from_string(stop_date) - fields.Date.from_string(start_date)
            elif values.get('stop_datetime') or values.get('start_datetime'):
                stop_datetime = values.get('stop_datetime') or event.stop_datetime
                start_datetime = values.get('start_datetime') or event.start_datetime
                if stop_datetime and start_datetime:
                    diff = fields.Datetime.from_string(stop_datetime) - fields.Datetime.from_string(start_datetime)
            if diff:
                duration = float(diff.days) * 24 + (float(diff.seconds) / 3600)
                values['duration'] = round(duration, 2)

    @api.multi
    def duplicate_event(self, count, event, vals):
        if vals and vals.get('repeat_type') == 'weekly':
            if vals.get('mon'):
                self.duplicated_day(event, count, 0)
            if vals.get('tue'):
                self.duplicated_day(event, count, 1)
            if vals.get('wed'):
                self.duplicated_day(event, count, 2)
            if vals.get('thue'):
                self.duplicated_day(event, count, 3)
            if vals.get('fri'):
                self.duplicated_day(event, count, 4)
            if vals.get('sat'):
                self.duplicated_day(event, count, 5)
            if vals.get('sun'):
                self.duplicated_day(event, count, 6)

    @api.multi
    def duplicated_day(self, event, count, next_week_day):
        if event:
            create_event=event
            for i in range(count):
                if create_event.allday :
                    cdate = create_event.start_date
                    sdate = datetime.strptime(cdate, '%Y-%m-%d').date()
                    plus_day = (7 - sdate.weekday()) + next_week_day
                    next_date = sdate + timedelta(days=plus_day)
                    new_create_event = create_event.with_context(sdate=str(next_date)).copy()
                    create_event = new_create_event
                else :
                    cdate = create_event.start_datetime
                    sdate = datetime.strptime(cdate, '%Y-%m-%d %H:%M:%S')
                    plus_day = (7 - sdate.weekday()) + next_week_day
                    next_date = sdate + timedelta(days=plus_day)
                    new_create_event = create_event.with_context(sdate=str(next_date)).copy()
                    create_event = new_create_event

    @api.multi
    def copy(self, default=None):
        default = default or {}
        default.update({'is_repeat':False})
        if self.allday:
            if self._context and self._context.has_key('sdate'):
                default.update({'start_date':self._context.get('sdate'),
                                'stop_date':self._context.get('sdate')})
        else:
            if self._context and self._context.has_key('sdate'):
                default.update({'start_datetime':self._context.get('sdate'),
                                'stop_datetime':self._context.get('sdate')})

        res = super(CalendarEvent, self).copy(default)
        return res

    @api.multi
    def write(self, vals):
        if self.user_id:
            vals.update({'partner_ids': [(6, 0, [self.user_id.partner_id.id])]})
            employee = self.env['hr.employee'].search([('user_id','=',self.user_id.id)], limit=1)
            if employee:
                vals.update({'user_of_employee_id':employee.id})
        
        if vals.get('user_id',False):
            user = self.env['res.users'].browse(vals.get('user_id'))
            if user:
                vals.update({'partner_ids': [(6, 0, [user.partner_id.id])]})
                employee = self.env['hr.employee'].search([('user_id','=',vals.get('user_id'))], limit=1)
                if employee:
                    vals.update({'user_of_employee_id':employee.id})

        res = super(CalendarEvent, self).write(vals)

        return res

    @api.onchange('client_partner_id')
    def onchange_client_partner_id(self):
        if self.client_partner_id:
            name = u'%s уулзалт'%(self.client_partner_id.name)
            self.update({'name':name})
            self.update({'geo_point': self.client_partner_id.geo_point})

    @api.multi
    def action_confirmed(self):
        done_datetime_char = str(fields.Datetime.now())
        return self.write({'state':'open', 'done_datetime':fields.Datetime.now(), 'done_datetime_char':done_datetime_char})