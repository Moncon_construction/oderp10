# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import fields, tools, models, api
from odoo.tools.translate import _
from odoo.exceptions import Warning

class OpenCalendarEventAnalyzeReport(models.TransientModel):
    _name = "open.calendar.event.analyze.report"
    _description = "Calendar Event Analysis Filter"

    start_date = fields.Date('Start date')
    end_date = fields.Date('End date')
    section_id = fields.Many2one('crm.team', 'Section')


    @api.multi
    def open(self):
        this = self
        mod_obj = self.env['ir.model.data']
        result_ids = []
        search_id = None
        tree_id = None
        graph_id = None
        res_model = None
            
        res_model = 'calendar.event.analyze.report'
        search_res = mod_obj.get_object_reference('geoengine_calender', 'view_report_calendar_event_analyze_filter')
        search_id = search_res and search_res[1] or False
        graph_res = mod_obj.get_object_reference('geoengine_calender', 'view_report_calendar_event_analyze_graph')
        pivot_res = mod_obj.get_object_reference('geoengine_calender', 'view_report_calendar_event_analyze_pivot')
        graph_id = graph_res and graph_res[1] or False
        pivot_id = pivot_res and pivot_res[1] or False
        if this.section_id:
            events_ids = self.env['calendar.event'].search([('section_id', '=',this.section_id.id),('start_date','>=',this.start_date),('stop_date','<=',this.end_date),('is_geo_event','=', True),('active','=',True)])
        else:
            events_ids = self.env['calendar.event'].search([('start_date','>=',this.start_date),('stop_date','<=',this.end_date),('is_geo_event','=', True),('active','=',True)])
        if not events_ids:
            raise Warning(_(u'Үйлдэл цуцлагдлаа!\n Хайлтын үр дүн хоосон байна.'))

        meeting_ids = []
        for id in events_ids:
            meeting_ids.append(id.id)

        return {
            'name': _('Calendar Event'),
            'view_type': 'form',
            'view_mode': 'search,graph,pivot',
            'res_model': res_model,
            'view_id': False,
            'views': [(graph_id, 'graph'), (pivot_id, 'pivot')],
            'search_view_id': search_id,
            'domain': [('id', 'in', meeting_ids)],
            'type': 'ir.actions.act_window'
        }

class CalendarEventAnalyzeReport(models.Model):
    """ Calendar event Analysis """
    _name = "calendar.event.analyze.report"
    _auto = False
    _description = "Calendar Event Analysis"
    _rec_name = 'start_date'

    start_datetime = fields.Datetime('Start Datetime', readonly=True)
    stop_datetime = fields.Datetime('Stop Datetime', readonly=True)

    start_date = fields.Date('Start Date', readonly=True)
    stop_date = fields.Date('Stop Date', readonly=True)

    done_datetime_char = fields.Char('Done Datetime', readonly=True)
    user_of_employee_id = fields.Many2one('hr.employee', 'User of employee', readonly=True)
    client_partner_id = fields.Many2one('res.partner', 'Partner', readonly=True)
    manager_id = fields.Many2one('res.users', 'Manager', readonly=True)

    section_id = fields.Many2one('crm.team', 'Section', readonly=True)

    event_name = fields.Char('Meeting Subject', readonly=True)
    nbr_events = fields.Integer('Events', readonly=True)
    draft_events = fields.Integer('Draft Events', readonly=True)
    done_events = fields.Integer('Done Events', readonly=True)

    def init(self):
        """
            Calendar event Report
            @param cr: the current row, from the database cursor
        """
        tools.drop_view_if_exists(self._cr, 'calendar_event_analyze_report')
        self._cr.execute("""
            CREATE OR REPLACE VIEW calendar_event_analyze_report AS (
                SELECT
                    id,
                    c.start_date as start_date,
                    c.stop_date as stop_date,

                    c.user_of_employee_id as user_of_employee_id,
                    c.section_id as section_id,
                    c.client_partner_id as client_partner_id,
                    c.manager_id as manager_id,
                    c.name as event_name,
                    count(id) as nbr_events,
                    c.done_datetime_char as done_datetime_char,
                    
                    (select count(c2.id) from calendar_event c2 
                        where c2.is_geo_event = 'true' and 
                            c2.state='draft' and 
                            c2.user_of_employee_id=c.user_of_employee_id and  
                            c2.start_date >= c.start_date and c2.stop_date <= c.stop_date and
                            c2.client_partner_id = c.client_partner_id
                            ) as draft_events,
                            
                    (select count(c3.id) from calendar_event c3 
                        where c3.is_geo_event = 'true' and 
                            c3.state='open' and  c3.start_date >= c.start_date and c3.stop_date <= c.stop_date and
                            c3.client_partner_id = c.client_partner_id and 
                            c3.user_of_employee_id=c.user_of_employee_id) as done_events
                FROM
                    calendar_event c
                WHERE c.active = 'true' and c.is_geo_event = 'true'
                GROUP BY c.id
            )""")
        
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
