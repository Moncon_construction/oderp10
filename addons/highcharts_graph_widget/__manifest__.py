# -*- coding: utf-8 -*-

{
    'name': 'Highcharts Graph Widget',
    'summary': "To include highcharts's graph into our forms's views...",
    'description': """To include highcharts's graph into our forms's views...""",

    'author': "",
    'website': "",

    'price': 10.00,
    'currency': 'USD',

    # Categories can be used to filter modules in modules listing.
    # Check /odoo/addons/base/module/module_data.xml for the full list.
    'category': 'Extra Tools',
    'version': '1.0',

    # Any module necessary for this one to work correctly.
    'depends': [
    ],

    # Always loaded.
    'data': [
        # Views...
        'views/test_model_views.xml',

        # Templates...
        'static/src/xml/backend_templates.xml',
    ],

    # Only loaded in demonstration mode.
    'demo': [

    ],

    'test': [

    ],

    'qweb': [
        'static/src/xml/graph_widget_templates.xml',
    ],

    'installable': True,
    'application': False,
    'auto_install': False,
}
