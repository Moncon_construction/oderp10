# -*- coding: utf-8 -*-

import json

from odoo import http
from odoo.http import Controller, request


class GraphDispatcher(Controller):
    @http.route('/compute_graph/', type='json', auth='user')
    def dispatch_file(self, data):
        _data = json.loads(data)
        _obj_class = request.env[_data['model']]
        _obj = _obj_class.search([('id', '=', _data['record_id'])])[0]
        return getattr(_obj, _data['method'])()
