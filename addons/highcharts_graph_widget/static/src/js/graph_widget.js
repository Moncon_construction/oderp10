odoo.define('highcharts_graph_widget.graph_widget', function (require) {
    "use strict";

    var core = require('web.core');
    var form_common = require('web.form_common');

    var GraphWidget = form_common.FormWidget.extend({
        template: 'highcharts_graph_widget.GraphWidget',
        init: function() {
            this._super.apply(this, arguments);
        },

        start: function () {
            var self = this;
            self.view.save().then(function () {
                self.rpc('/compute_graph', {
                        data: JSON.stringify({
                        model: self.node.attrs['model'],
                        record_id: self.view.datarecord.id,
                        method: self.node.attrs['method']
                    })
                }).done(function (graph_data) {
                    $('#' + self.node.attrs['id']).highcharts(graph_data);
                });
            });
            return this._super();
        }
    });

    core.form_custom_registry.add('graph_widget', GraphWidget);
});
