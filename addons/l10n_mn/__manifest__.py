# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Mongolian Accounting',
    'version': '1.0',
    'category': 'Localization',
    'author': 'Asterisk Technologies LLC',
    'maintainer': 'nasanochir@asterisk-tech.mn',
    'website': 'http://asterisk-tech.mn',
    'description': """
This is the base module to manage the accounting chart for Mongolian.
    Account type data
    Account chart data
    Account tax
    """,
    'depends': ['base', 'account'],
    'data': [
        'data/account_financial_report_data.xml',
        'data/account_account_type_data.xml',
        'data/l10n_mn_chart_data.xml',
        'data/account_account_template_data.xml',
        'data/account_chart_template_data.xml',
        'data/account_tax_template_data.xml',
        'data/ir_translation.xml',
        'data/account_chart_template_data.yml',
        'views/account_view.xml'
    ],
}
