# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Mongolian User Access Report',
    'version': '1.0',
    'category': 'Base',
    'author': 'Asterisk Technologies LLC',
    'maintainer': 'ochinzorig@asterisk-tech.mn',
    'website': 'http://asterisk-tech.mn',
    'description': """
        L10n_mn - User Access Report
    """,
    'depends': ['base'],
    'data': [
        'wizard/access_report_view.xml'
    ],
    'license': 'GPL-3',
}
