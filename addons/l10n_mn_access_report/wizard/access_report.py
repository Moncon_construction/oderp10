# -*- encoding: utf-8 -*-
##############################################################################
import base64
import time
import xlsxwriter
from odoo import api, fields, models, _
from io import BytesIO
from xlsxwriter.utility import xl_rowcol_to_cell
from operator import itemgetter
from openerp.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles

class UsserAccessReport(models.TransientModel):
    _name = 'user.access.report'
    _description = _('User Access Report')
    
    company_id = fields.Many2one('res.company','Company', default=lambda self: self.env.user.company_id, required=True)
    users_ids = fields.Many2many('res.users','wizard_access_report_user_rel', 'wizard_id', 'user_id', string='Users')
    
    @api.onchange('company_id')
    def onchange_company_id(self):
        users = self.env['res.users'].search([('company_id','=', self.company_id.id)])
        return {'domain':{'users_ids':[('id','in',users.ids)]}}
    
    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('User Access Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_group_left = book.add_format(ReportExcelCellStyles.format_group_left)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_group_number = book.add_format(ReportExcelCellStyles.format_group_number)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_date = book.add_format(ReportExcelCellStyles.format_content_date)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)

        seq = 1
        data_dict = {}
        rowx = 0
        catogeries = []
        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('User_access_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        
        # compute column
        colx_number = 3
        sheet.set_column('A:A', 4)
        sheet.set_column('B:B', 15)
        sheet.set_column('C:C', 15)
        sheet.set_column('D:D', 15)
        sheet.set_column('E:E', 30)
        sheet.set_column('F:F', 15)
        sheet.set_column('G:G', 15)
        
        sheet.merge_range(rowx, 0, rowx, 1, '%s' % (self.company_id.name), format_filter)
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s' % (_('Company'), self.company_id.name), format_filter)
        rowx += 2
        sheet.merge_range(rowx, 1, rowx, 4, report_name, format_name)
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s' % (_('Print Date'), time.strftime('%Y-%m-%d')), format_filter)
        rowx += 1
        
        sheet.merge_range(rowx, 0, rowx + 1, 0, _('Seq'), format_title)
        sheet.merge_range(rowx, 1, rowx + 1, 1, _('Company'), format_title)
        sheet.merge_range(rowx, 2, rowx + 1, 2, _('User name'), format_title)           
        sheet.merge_range(rowx, 3, rowx + 1, 3, _('User Login'), format_title)
        sheet.merge_range(rowx, 4, rowx + 1, 4, _('Group'), format_title)
        rowx += 2
        users = self.users_ids if self.users_ids else self.env['res.users'].search([('company_id','=',self.company_id.id)])
        sequence = 1
        for user in users:
            self._cr.execute("select gid from res_groups_users_rel t where uid= " + str(user.id))
            groups = self._cr.fetchall()
            group_ids = []
            for group in groups:
                group_ids.append(group[0])
            group_obj = self.env['res.groups'].browse(group_ids)
            group_name = ''
            for group in group_obj:
                if group.category_id:
                    group_name += group.category_id.name + ' / ' + group.name + '\n'
                else:
                    group_name += group.name + '\n'
            sheet.write(rowx, 0, sequence, format_content_number)
            sheet.write(rowx, 1, user.company_id.name, format_content_text)
            sheet.write(rowx, 2, user.name, format_content_text)
            sheet.write(rowx, 3, user.login, format_content_text)
            sheet.write(rowx, 4, group_name, format_content_text)
            rowx += 1
            sequence += 1
    #             name_selection_groups
            # create date
        rowx += 1
        user = self.env['res.users'].browse(self._uid)
        sheet.write(rowx, 0, '%s: ........................................... /%s/' % (_('Owner'), user.name), format_filter)
        book.close()

        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report() 
    
