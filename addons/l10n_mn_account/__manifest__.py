# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Account",
    'version': '1.0',
    'depends': [
        'account',
        'account_cancel',
        'l10n_mn',
        'l10n_mn_base',
        'l10n_mn_contacts',
        'l10n_mn_report',
        'l10n_mn_product',
        'l10n_mn_analytic',
    ],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Account Modules',
    'description': """
         Мөнгөн гүйлгээний төрөл цэс.
         Журнал бүртгэх нэмэлт өөрчлөлт.
         Данс бүртгэх нэмэлт өөрчлөлт.
         Харилцахын гүйлгээ нь дээрээс анхан шатны баримт хэвлэгдэнэ.
         Кассын гүйлгээний баримт хэвлэгдэнэ.
         Татварууд дээрх татварын тайлангийн төрөл талбар.
    """,
    'website': 'http://asterisk-tech.mn',
    'data': [
        'data/account_chart_cashflow_template_data.xml',
        'data/account_cashflow_template_data.xml',
        'data/account_chart_cashflow_template_data.yml',
        'data/account_financial_report_data.xml',
        'data/account_data.xml',
        'data/invoice_report_paperformat.xml',
        'data/print_cash_order_paperformat.xml',
        'security/ir.model.access.csv',
        'security/ir_rule.xml',
        'security/account_security.xml',
        'wizard/account_financial_chart_view.xml',
        'wizard/account_report_general_account_view.xml',
        'wizard/account_validate_account_move_decline.xml',
        'wizard/send_invoice_by_email_wizard.xml',
        'wizard/mail_compose_message_wizard.xml',
        'views/res_config_view.xml',
        'views/cashflow_type_menu.xml',
        'views/account_view.xml',
        'views/account_bank_statement_import_view.xml',
        'views/account_bank_statement_view.xml',
        'views/account_invoice_view.xml',
        'views/payment_report_view.xml',
        'views/report_view.xml',
        'views/print_cash_order.xml',
        'views/account_financial_report_view.xml',
        'views/account_journal_dashboard_view.xml',
        'views/account_payment_view.xml',
        'views/invoice_report_view.xml',
        'views/res_company_view.xml',
        'views/res_partner_view.xml',
        'views/res_users_views.xml',
        'views/pos_box.xml',
        'views/account_transaction_view.xml',
        'views/report_journal.xml',
        'views/res_bank_view.xml',
        'views/account_menuitem.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
