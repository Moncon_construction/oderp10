# -*- coding: utf-8 -*-

import cashflow_type
import cashflow_chart_template
import res_company
import res_config
import account
import account_bank_statement_import
import account_bank_statement
import account_move
import account_invoice
import account_payment
import account_journal
import account_mobile
import pos_box
import account_transaction
import res_partner
import res_users
import res_bank
import res_sector