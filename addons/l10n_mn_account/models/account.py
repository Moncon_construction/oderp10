# -*- coding: utf-8 -*-
from odoo import fields, models, _, api # @UnresolvedImport
from odoo.exceptions import UserError, ValidationError
from odoo import exceptions
from odoo.tools.safe_eval import safe_eval as eval

class AccountAccount(models.Model):
    _name = "account.account"
    _inherit = ['account.account', 'mail.thread', 'ir.needaction_mixin']

    name = fields.Char(required=True, index=True, track_visibility='onchange')
    currency_id = fields.Many2one('res.currency', string='Account Currency', track_visibility='onchange',
                                  help="Forces all moves for this account to have this account currency.")
    code = fields.Char(size=64, required=True, index=True, track_visibility='onchange')
    cashflow_account_ids = fields.Many2many('account.cashflow.type', 'account_cashflow_type_rel', 'account_id', 'type_id', 'Allow Cashflow Types', domain=[('type', '=', 'normal')])
    journal_entries_selectable = fields.Boolean(string='Not selectable for journal entries', track_visibility='onchange', default=False,
                                                help="Check this box if you want to will not be selected to make transactions from the journal entries")
    req_analytic_account = fields.Boolean(string='Require analytic account', track_visibility='onchange', default=False,
                                          help="Check this box if you want to require analytic account to account transitions.")
    code_change = fields.Boolean(string='Code change', default=False, track_visibility='onchange')
    allowed_user_ids = fields.Many2many('res.users', 'account_account_users_rel', 'acc_id', 'user_id', string='Allowed Users')

    @api.multi
    def write(self, vals):
        config_setting = self._context.get('config_setting', False)
        if 'code' in vals.keys() and not config_setting:
            for obj in self:
                if vals['code'] != obj.code:
                    self._check_allow_code_change()
        return super(AccountAccount, self).write(vals)

    @api.multi
    def _check_allow_code_change(self):
        for account in self:
            if not account.code_change and self.env['account.move.line'].search([('account_id', '=', account.id)]):
                    raise UserError(_("You cannot change the code of account which contains journal items!"))
        return True

    @api.constrains('name')
    def _check_account_name(self):
        for account in self:
            if not account.code_change and self.env['account.move.line'].search([('account_id', '=', account.id)]):
                raise UserError(_("You cannot change the name of account which contains journal items!"))
            if account.name:
                other_accounts = self.env['account.account'].search([
                    ('name', '=', account.name),
                    ('id', '!=', account.id),
                    ('company_id', '=', account.company_id.id),
                    ('code', '=', account.code)])
            for other_account in other_accounts:
                if other_account:
                    exception = _('Account name duplicated: ') + other_account.name
                    raise exceptions.ValidationError(exception)

class AccountJournal(models.Model):
    _name = 'account.journal'
    _inherit = ['account.journal', 'mail.thread']
    
    _sql_constraints = [ 
         ('name_uniq', 'unique(code, company_id)', 'Code must be unique!'),
     ]

    user_id = fields.Many2one('res.users', string="Journal user", default=lambda self: self.env.user)
    journal_entries_selectable = fields.Boolean(string='Not selectable for journal entries', help="Check this box if you want to will not be selected to make transactions from the journal entries", default=True)
    auto_approve = fields.Boolean(string='Approve record automatically', help="Check this box if you want the entries in the journal will automatically be verified", default=True)
    res_id = fields.Many2one('res.users', string='Respondent')
    code = fields.Char(string='Short Code', size=10, required=True, help="The journal entries of this journal will be named using this prefix.")
    update_posted = fields.Boolean(string="Allow Cancelling Entries",
                                   help="Check this box if you want to allow the cancellation the entries related to this journal or of the invoice related to this journal",
                                   default=True)
    active = fields.Boolean('Active', default=True)

    @api.model
    def create(self, vals):
        res = super(AccountJournal, self).create(vals)
        if res.type == 'cash':
            res.default_debit_account_id = '';
            res.default_credit_account_id = '';
        return res


class AccountTax(models.Model):
    _inherit = 'account.tax'

    choose_taxes = fields.Selection([('1','VAT (1)'),
                                    ('2','City tax (2)')])
    partner_id = fields.Many2one('res.partner', 'Associated Partner')
    amount_type = fields.Selection(selection_add=[(('code'), _('Code'))])
    python_compute = fields.Text(string='Python Code', default="# price_unit\n# or False\n# product: product.product object or None\n# partner: res.partner object or None\n\nresult = price_unit * 0.10")

    def _compute_amount(self, base_amount, price_unit, quantity=1.0, product=None, partner=None):
        """ Returns the amount of a single tax. base_amount is the actual amount on which the tax is applied, which is
            price_unit * quantity eventually affected by previous taxes (if tax is include_base_amount XOR price_include)
        """
        res = super(AccountTax, self)._compute_amount(base_amount, price_unit, quantity=quantity, product=product, partner=partner)
        if self.amount_type == 'code':
            localdict = {'price_unit':base_amount, 'product':product, 'partner':partner}
            eval(self.python_compute, localdict, mode="exec", nocopy=True)
            return localdict['result']
        else:
            return res

class AccountAccountWizard(models.TransientModel):
    _name = "account.account.wizard"

    user_ids = fields.Many2many('res.users', string='Users')

    @api.multi
    def add_allowed_users(self):
        active_ids = self._context.get('active_ids', []) or []
        account_ids = self.env['account.account'].search([('id', 'in', active_ids)])
        if self.user_ids:
            for account_id in account_ids:
                account_id.write({'allowed_user_ids': [(4,  self.user_ids.ids)]})
        else:
            raise UserError(_("Select users!"))