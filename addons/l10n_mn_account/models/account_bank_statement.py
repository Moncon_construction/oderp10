# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools.misc import formatLang


class AccountBankStatementLine(models.Model):
    _name = "account.bank.statement.line"
    _inherit = ['account.bank.statement.line', 'mail.thread']

    cashflow_id = fields.Many2one('account.cashflow.type', 'Cashflow Type', ondelete='restrict')
    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic account')
    related_statement_line_id = fields.Many2one('account.bank.statement.line', 'Related Statement Line', readonly=True)
    currency_exchange_move_id = fields.Many2one('account.move', 'Currency exchange move', readonly=True)
    import_line_id = fields.Many2one('account.move.line', 'Imported move', readonly=True)
    related_invoice_id = fields.Many2one(string='Related Invoice Number', related='import_line_id.invoice_id')
    bank_account_id = fields.Many2one('res.partner.bank', string='Bank Account')
    payment_id = fields.Many2one('account.payment', string="Payment", copy=False)
    require_analytic = fields.Boolean(related="account_id.req_analytic_account", string="Require Analytic", readonly=True, default=False)
    account_transaction_id = fields.Many2one('account.transaction', 'Account Transaction')
    require_partner = fields.Boolean(compute='_compute_partner', string='Require partner', default=False)
    journal_type = fields.Char(compute='_compute_journal_type', help="Technical field used for usability purposes")

    @api.constrains('amount', 'cashflow_id')
    @api.onchange('amount', 'cashflow_id')
    def _check_amount(self):
        if self.currency_id and self.journal_id.currency_id != self.company_id.currency_id and self.currency_id != self.company_id.currency_id:
            ctx = dict(self._context, date=self.date)
            statement_currency = self.journal_id.currency_id or self.company_id.currency_id
            self.amount_currency = statement_currency.with_context(ctx).compute(self.amount, self.currency_id)
        elif self.currency_id and self.currency_id != self.company_id.currency_id:
            self.amount_currency = self.amount
        if self.cashflow_id:
            if self.amount < 0 and self.cashflow_id.value_of_amount_type == 'positive':
                raise ValidationError(_('"%s" cashflow only accepts positive amounts. The amount cannot be %s') % (self.cashflow_id.name, self.amount))
            if self.amount > 0 and self.cashflow_id.value_of_amount_type == 'negative':
                raise ValidationError(_('"%s" cashflow only accepts negative amounts. The amount cannot be %s') % (self.cashflow_id.name, self.amount))

    @api.one
    @api.constrains('date')
    def _check_date(self):
        for obj in self.filtered(lambda x: x.company_id.statement_date == 'same'):
            if obj.date != obj.statement_id.date:
                raise ValidationError(_("Statement line date must be same as statement date %s !!!") % obj.statement_id.date)
            
    @api.depends('account_id')
    def _compute_partner(self):
        for obj in self:
            if obj.account_id.user_type_id.type in ['receivable', 'payable']:
                obj.require_partner = True

    @api.onchange('account_transaction_id')
    def compute_cashflow_id(self):
        self.ensure_one()
        if self.account_transaction_id.cashflow_id:
            self.cashflow_id = self.account_transaction_id.cashflow_id

    # Банкны хуулгын эцсийн үлдэгдлийг шинэчлэгч
    #def _set_balance_end_real(self, statement_id, balance_end_real):
    #    self.env.cr.execute("UPDATE account_bank_statement SET balance_end_real = %s WHERE id = %s", (balance_end_real, statement_id.id))

    # Шинээр хуулгын мөр үүсэхэд эцсийн үлдэгдлийг тооцох
    '''
    @api.model
    def create(self, vals):
        if 'statement_id' in vals:
            st = self.env['account.bank.statement'].search([('id','=',vals['statement_id'])])
            if st:
                if 'amount' in vals:
                    self._set_balance_end_real(st, float(st.balance_start) + float(st.balance_end_income) - float(st.balance_end_outcome) + float(vals['amount']))
        return super(AccountBankStatementLine, self).create(vals)
    '''

    @api.multi
    def _compute_journal_type(self):
        for obj in self:
            obj.journal_type = obj.journal_id.type

    @api.multi
    def write(self, vals):
        res = super(AccountBankStatementLine, self).write(vals)

        if 'cashflow_id' in vals.keys():
            for obj in self:

                if obj.payment_id and (obj.payment_id.type_of_currency_id or obj.payment_id.type_of_exchange_id):
                    if obj.payment_id.company_id.period_lock_date and obj.payment_id.company_id.period_lock_date > obj.payment_id.payment_date:
                        raise UserError(_(u"Sorry. You can not change cashflow type. %s's period lock date is end for this payment.") %obj.payment_id.company_id.partner_id.name)
                    if obj.payment_id.type_of_currency_id:
                        obj.payment_id.write({'type_of_currency_id': vals['cashflow_id']})
                    if obj.payment_id.type_of_exchange_id:
                        obj.payment_id.write({'type_of_exchange_id': vals['cashflow_id']})

                if obj.journal_entry_ids and len(obj.journal_entry_ids) > 0:
                    for journal_entry in obj.journal_entry_ids:
                        if journal_entry.line_ids and len(journal_entry.line_ids) > 0:
                            for line in journal_entry.line_ids:
                                if line.cashflow_id:
                                    line.write({'cashflow_id': vals['cashflow_id'],
                                                'is_cashflow': True})
                                if line.payment_id and (line.payment_id.type_of_currency_id or line.payment_id.type_of_exchange_id):
                                    if line.payment_id.company_id.period_lock_date and line.payment_id.company_id.period_lock_date > obj.payment_id.payment_date:
                                        raise UserError(_(u"Sorry. You can not change cashflow type. %s's period lock date is end for this payment.") %line.payment_id.company_id.partner_id.name)
                                    if line.payment_id.type_of_currency_id:
                                        line.payment_id.write({'type_of_currency_id': vals['cashflow_id']})
                                    if line.payment_id.type_of_exchange_id:
                                        line.payment_id.write({'type_of_exchange_id': vals['cashflow_id']})

        if 'partner_id' in vals.keys():
            for obj in self:
                moves = self.env['account.move'].search([('statement_line_id', '=', obj.id)])
                if moves and len(moves) > 0:
                    for move in moves:
                        move.line_ids.write({'partner_id': vals['partner_id']})
                        for move_line in move.line_ids:
                            if move_line.invoice_id:
                                move_line.invoice_id.write({'partner_id': vals['partner_id']})
                            if move_line.payment_id:
                                move_line.payment_id.write({'partner_id': vals['partner_id']})
        return res

    @api.onchange('account_id')
    def onchange_account_id(self):
        domain = {}
        if self.account_id:
            cashflow_types = self.account_id.cashflow_account_ids.ids
            if cashflow_types:
                if len(cashflow_types) == 1:
                    self.cashflow_id = cashflow_types[0]
                domain['cashflow_id'] = [('id', 'in', cashflow_types)]
                return {'domain': domain}
            else:
                all_cashflow_types = self.env['account.cashflow.type'].search([('company_id', '=', self.company_id.id), ('type', '=', 'normal')])
                self.cashflow_id = False
                domain['cashflow_id'] = [('id', 'in', all_cashflow_types.ids)]
                return {'domain': domain}

    @api.onchange('account_transaction_id')
    def onchange_account_transaction(self):
        if self.account_transaction_id:
            self.name = self.account_transaction_id.name
            self.account_id = self.account_transaction_id.account_id

    @api.onchange('partner_id')
    def onchange_partner_id(self):
        if self.partner_id:
            partner_bank = self.env['res.partner.bank'].search([('partner_id', '=', self.partner_id.id)], limit=1)
            if partner_bank:
                self.bank_account_id = partner_bank.id

    @api.multi
    def button_cancel_reconciliation(self):
        #Касс харилцахын мөрийг цуцалж байна
        payment = False
        statement_ids = []
        for st_line in self:
            statement_ids.append(st_line.statement_id.id)
            moves_to_cancel = st_line.journal_entry_ids
            # Дотоод шилжүүлэг болон валют солилцоо бол хуулгын мөрийг цуцлах боломжтой байна
            if st_line.payment_id and st_line.payment_id.payment_type in ['transfer', 'currency']:
                if st_line.payment_id.payment_type == 'currency' and 'unlink_bank_statement_from_payment' not in self._context:
                    raise UserError(_('You can not cancel currency exchange\'s account bank statement line. Cancel the currency exchange!'))
                payment = True
                # Дотоод шилжүүлэг болон валют солилцоо бол хуулгын мөрийн холбоотой бичилтийг мөн цуцлана
                other_st_line = self.env['account.bank.statement.line'].search([('payment_id', '=', st_line.payment_id.id), ('id', '!=', st_line.id)], limit=1)
                if len(other_st_line) > 0:
                    statement_ids.append(other_st_line[0].statement_id.id)
                    moves_to_cancel += other_st_line[0].journal_entry_ids
                st_line.payment_id.write({'state': 'draft'})
            for move in moves_to_cancel:
                move.line_ids.filtered(lambda x: x.statement_id.id in statement_ids).write({'statement_id': False})
                if payment:
                    move.button_cancel()
                    move.unlink()
            st_line.statement_id.write({'state': 'open'})
            statement_ids = []
            payment = False
        return super(AccountBankStatementLine, self).button_cancel_reconciliation()

    @api.multi
    def confirm_button(self):
        '''Ордер хооронд дотоод шилжүүлэг хийх үед энэ функцыг ашиглана

        '''
        line = self.env['account.bank.statement.line'].browse(self.id)
        move_obj = self.env['account.move']
        inv_obj = self.env['account.invoice']
        company_currency = line.journal_id.company_id.currency_id
        statement_currency = line.journal_id.currency_id or company_currency
        st_line_currency = line.currency_id or statement_currency
        ctx = dict(self._context, date=line.date)
#         Журналын бичилтийн мөрийг бэлдэж байна
        if line.account_id and not line.journal_entry_ids.ids:
            line_ids = [(0, 0, {
                'name': line.name,
                'ref': line.name + ' - ' + line.ref if line.ref else line.name,
                'account_id': line.journal_id.default_debit_account_id.id,
                'debit': line.amount if line.amount > 0 else 0,
                'credit': 0 if line.amount > 0 else -line.amount,
                'journal_id': line.journal_id.id,
                'partner_id': line.partner_id.id if line.partner_id else '',
                'currency_id': line.currency_id.id,
                'amount_currency': line.amount_currency or 0.0,
                'date': line.date,
                'cashflow_id': line.cashflow_id.id,
                'is_cashflow': True,
                'statement_id': line.statement_id.id,
            }), (0, 0, {
                'name': line.name,
                'ref': line.name + ' - ' + line.ref if line.ref else line.name,
                'account_id': line.account_id.id,
                'debit': 0 if line.amount > 0 else -line.amount,
                'credit': line.amount if line.amount > 0 else 0,
                'journal_id': line.journal_id.id,
                'partner_id': line.partner_id.id if line.partner_id else '',
                'currency_id': line.currency_id.id,
                'amount_currency': line.amount_currency or 0.0,
                'analytic_account_id': line.analytic_account_id.id if line.analytic_account_id else False,
                'date': line.date,
                'statement_id': line.statement_id.id,
            })]
#             Вальютаас хамаарч debit, credit нь өөрчлөгдөнө
            st_line_currency = line.currency_id or statement_currency
            #st_line_currency_rate = line.currency_id and (
            #    self.amount_currency / self.amount) or False
            st_line_currency_rate = line.currency_id and (
                self.amount / self.amount_currency ) or False
            for mline in line_ids:
                if st_line_currency.id != company_currency.id:
                    mline[2]['amount_currency'] = mline[2]['debit'] - \
                        mline[2]['credit']
                    mline[2]['currency_id'] = st_line_currency.id
                    if line.currency_id and statement_currency.id == company_currency.id and st_line_currency_rate:
                        # Statement is in company currency but the transaction
                        # is in foreign currency
                        mline[2]['debit'] = company_currency.round(
                            mline[2]['debit'] / st_line_currency_rate)
                        mline[2]['credit'] = company_currency.round(
                            mline[2]['credit'] / st_line_currency_rate)
                    elif self.currency_id and st_line_currency_rate:
                        # Statement is in foreign currency and the transaction
                        # is in another one
                        mline[2]['debit'] = statement_currency.with_context(ctx).compute(
                            mline[2]['debit'] / st_line_currency_rate, company_currency)
                        mline[2]['credit'] = statement_currency.with_context(ctx).compute(
                            mline[2]['credit'] / st_line_currency_rate, company_currency)
                    else:
                        # Statement is in foreign currency and no extra
                        # currency is given for the transaction
                        mline[2]['debit'] = st_line_currency.with_context(
                            ctx).compute(mline[2]['debit'], company_currency)
                        mline[2]['credit'] = st_line_currency.with_context(
                            ctx).compute(mline[2]['credit'], company_currency)
                elif statement_currency.id != company_currency.id:
                    # Statement is in foreign currency but the transaction is
                    # in company currency
                    prorata_factor = (
                        mline[2]['debit'] - mline[2]['credit']) / self.amount_currency
                    mline[2]['amount_currency'] = prorata_factor * line.amount
                    mline[2]['currency_id'] = statement_currency.id
            #Журналын бичилтийг бэлдэж байна. Дээр олгосон утгуудаа журналын бичилтийн мөрөнд оноож байна
            move_vals = {
                'date': line.statement_id.date,
                'ref': line.statement_id.name + '/' + str(line.sequence),
                'journal_id': line.statement_id.journal_id.id,
                'line_ids': line_ids,
                'statement_line_id': line.id,
            }
            move_id = move_obj.create(move_vals)
            move_id.assert_balanced()
            line.statement_id.write({'move_line_ids': str(
                move_id.line_ids.ids).replace('[', '').replace(']', '')})
#             Хэрвээ журналын бичилт нь автоматаар батладаг байвал батлах
            if line.journal_id.auto_approve:
                move_id.post()
# #            Импорт хийсэн нэхэмжлэлийн төлбөрийг нэмэмжлэлтэй тулгах
            for mo_line in move_id.line_ids:
                if line.import_line_id:
                    invoice = inv_obj.browse(line.import_line_id.invoice_id.id)
                    if mo_line.statement_id.id == line.statement_id.id and mo_line.account_id.id == line.account_id.id:
                        invoice.assign_outstanding_credit(mo_line.id)
                if line and line.payment_id and line.payment_id.payment_type == 'currency' and \
                        line.payment_id.middle_account_id.id == mo_line.account_id.id:
                    self._cr.execute("UPDATE account_move_line SET amount_currency = 0, currency_rate = 1, currency_id = Null WHERE id = %s", (mo_line.id,))
#        Хэрвээ журналын бичилт байх юм бол мөнгөн гүйлгээний төрлийг дамжуулна.
        elif line.journal_entry_ids.ids:
            if self.amount:
                if self.amount > 0:
                    for journal_id in line.journal_entry_ids:
                        for line_id in journal_id.line_ids:
                            if line_id.debit > 0:
                                line_id.write(
                                    {'cashflow_id': line.cashflow_id.id})
                else:
                    for journal_id in line.journal_entry_ids:
                        for line_id in journal_id.line_ids:
                            if line_id.credit > 0:
                                line_id.write(
                                    {'cashflow_id': line.cashflow_id.id})
        elif not line.journal_entry_ids.ids:
            raise UserError(_('Account entry line must be processed in order to close the statement.'))

        line.statement_id.message_post(body=_("Statement %s's line 'No-%s' is confirmed, journal items were created.") % (line.statement_id.name, line.name))

    @api.multi
    def print_cash_order(self, docids, data=None):
        ''' Кассын орлого, зарлагын баримт хэвлэх '''
        statement = self.browse(self.ids)
        if statement.journal_id.type == 'bank':
            data = {
                'is_statement_line': True
            }
            return self.env['report'].get_action(self.ids, 'l10n_mn_account.payment_report_view', data=data)
        return self.env['report'].get_action(self.ids, 'l10n_mn_account.print_cash_order', data=data)

    def process_reconciliation(self, counterpart_aml_dicts=None, payment_aml_rec=None, new_aml_dicts=None):
        res = super(AccountBankStatementLine, self).process_reconciliation(counterpart_aml_dicts, payment_aml_rec, new_aml_dicts)
        inv_obj = self.env['account.invoice']
        rec_obj = self.env['account.move.line.reconcile']
        statement_line = self.related_statement_line_id
        for move in res:
            # Дотоод шилжүүлэг болон валют солилцоо бол хуулгын мөрийн аль нэг талыг нь батлахад нөгөө мөр нь мөн батлагддаг байна
            if self.related_statement_line_id and self.related_statement_line_id.payment_id and self.related_statement_line_id.payment_id.payment_type in ['transfer', 'currency']:
                # Хэрвээ банкны хуулгын мөр батлагдаж л байгаа бол заавал шинээр төлбөр үүсдэг тул ийнхүү хийсэн. Ирээдүйд заавал засах шаардлагатай
                self._cr.execute("DELETE FROM account_payment WHERE id = %s", (self.id,))
                self.payment_id = self.related_statement_line_id.payment_id
                if self.payment_id.amount == abs(self.amount) == abs(self.related_statement_line_id.amount):
                    self.confirm_button()
                    self.related_statement_line_id.confirm_button()
                    self.related_statement_line_id.payment_id.write({'state': 'posted'})
                else:
                    self.related_statement_line_id.payment_id.post()
            account_id = self.statement_id.journal_id.default_credit_account_id.id
            if self.amount >= 0:
                account_id = self.statement_id.journal_id.default_debit_account_id.id
            #if not self.cashflow_id:
            #    raise UserError(_('Cashflow type not selected!'))
            payment_id = False
            for move_line in move.line_ids:
                if statement_line and statement_line.payment_id and statement_line.payment_id.payment_type == 'currency' and \
                        statement_line.payment_id.middle_account_id.id == move_line.account_id.id:
                    self._cr.execute("UPDATE account_move_line SET amount_currency = 0, currency_rate = 1, currency_id = Null WHERE id = %s", (move_line.id,))
                if move_line.account_id.id == account_id:
                    if move_line.payment_id:
                        payment_id = move_line.payment_id.id
                        move_line.payment_id.write({'bank_account_id': self.bank_account_id.id if self.bank_account_id else False})
                    move_line.write({'cashflow_id': self.cashflow_id.id,
                                     'is_cashflow': True})
                    if self.statement_id.name and self.statement_id.name != '/':
                        move_line.move_id.write({'ref': self.statement_id.name + '/' + str(self.sequence)})
                else:
                    if self.analytic_account_id:
                        move_line.write({'analytic_account_id': self.analytic_account_id.id})
                        move_line.create_analytic_lines()
                move_line.write({'account_transaction_id': self.account_transaction_id.id})
                # Импорт хийсэн нэхэмжлэлийн төлбөрийг нэмэмжлэлтэй тулгах
                if self.import_line_id:
                    invoice = inv_obj.browse(self.import_line_id.invoice_id.id)
                    if move_line.statement_id.id == self.statement_id.id and move_line.account_id.id == self.account_id.id:
                        if invoice:
                            invoice.assign_outstanding_credit(move_line.id)
                            if self.import_line_id.full_reconcile_id and self.import_line_id.full_reconcile_id.exchange_move_id:
                                self.currency_exchange_move_id = self.import_line_id.full_reconcile_id.exchange_move_id
                        else:
                            currency = False
                            if self.statement_id and self.statement_id.journal_id and self.statement_id.journal_id.currency_id:
                                currency = self.statement_id.journal_id.currency_id.id
                            amount = self.import_line_id.debit - self.import_line_id.credit
                            amount_currency = self.import_line_id.amount_currency
                            active_ids = [self.import_line_id.id, move_line.id]
                            context = {'active_ids': active_ids, 'statement': True}
                            if not currency:
                                if amount == self.amount:
                                    rec_obj.with_context(context).trans_rec_reconcile_full()
                                elif amount != self.amount:
                                    rec_obj.with_context(context).trans_rec_reconcile_move_line()
                            if currency:
                                move_amount = move_line.debit - move_line.credit
                                if amount_currency == move_line.amount and amount == move_amount:
                                    rec_obj.with_context(context).trans_rec_reconcile_full()
                                else:
                                    rec_obj.with_context(context).trans_rec_reconcile_move_line()
            self.write({'payment_id': payment_id})
        return res

    @api.multi
    def unlink(self):
        payment_id = False
        for statement in self:
            statement.statement_id.message_post(body=_("Statement %s's line 'No-%s' is deleted.") % (statement.statement_id.name, statement.sequence))
            # Холбоотой Касс/Харилцахын мөр ноорог байгаа эсэхийг шалгаж устгах
            if statement.related_statement_line_id:
                if statement.related_statement_line_id.journal_entry_ids:
                    raise UserError(_('Can not delete the related statement line state is not draft.'))
                else:
                    self._cr.execute("DELETE FROM account_bank_statement_line WHERE id = %s", (statement.related_statement_line_id.id,))
                    #self._cr.execute("UPDATE account_bank_statement SET balance_end = %s, balance_end_real = %s WHERE id = %s", (
                    #    balance_end, balance_end, rel_statement.id))
            # Холбоотой үүссэн валютын ханшийн зөрүүний бичилт байгаа эсэхийг шалгаж Холбоотой бичилтүүдийг устгаж бичилтийг устгах
            if statement.currency_exchange_move_id:
                for move_line in statement.currency_exchange_move_id.line_ids:
                    self._cr.execute("DELETE FROM account_bank_statement_line_move_rel WHERE move_line_id = %s", (move_line.id,))
                statement.currency_exchange_move_id.button_cancel()
                statement.currency_exchange_move_id.unlink()
            if statement.payment_id and statement.payment_id.payment_type == 'transfer':
                payment_id = statement.payment_id
        res = super(AccountBankStatementLine, self).unlink()
        if payment_id:
            payment_id.cancel()
        return res

    @api.multi
    def button_confirm_bank(self):
        """
            @note: Касс/харилцахын хуулгыг мөр бүрээр батлах боломжтой болгов.
        """
        st_lines = self.filtered(lambda r: r.state == 'open')
        moves = self.env['account.move']

        # create move
        for st_line in st_lines:
            if st_line.account_id and not st_line.journal_entry_ids.ids:
                st_line.fast_counterpart_creation()
            elif not st_line.journal_entry_ids.ids:
                raise UserError(_('All the account entries lines must be processed in order to close the statement.'))
            moves = (moves | st_line.journal_entry_ids)

        if moves:
            moves.filtered(lambda m: m.state != 'posted').post()


class AccountBankStatement(models.Model):
    _inherit = 'account.bank.statement'

    @api.model
    def _default_journal(self):
        res = super(AccountBankStatement, self)._default_journal()
        if not res:
            journals = self.env['account.journal'].search([('type', 'in', ['bank']), ('company_id', '=', self.env.user.company_id.id)])
            if journals:
                return journals[0]
        return res

    journal_id = fields.Many2one('account.journal', string='Journal', required=True, states={'confirm': [('readonly', True)]}, default=_default_journal)
    name = fields.Char('Reference', default='/', copy=False)
    sequence_id = fields.Many2one('ir.sequence', 'Statement Sequence')
    balance_end_income = fields.Monetary('Balance Income', compute='_end_balance')
    balance_end_outcome = fields.Monetary('Balance Outcome', compute='_end_balance')
    is_last = fields.Boolean(compute='_compute_is_last', search='_search_is_last', default=False, string='Is Last')

    @api.multi
    def _compute_is_last(self):
        for obj in self:
            statement_id = self.env['account.bank.statement'].search([('journal_id', '=', obj.journal_id.id)], order='date desc', limit=1)
            if obj.id == statement_id.id:
                obj.is_last = True
            else:
                obj.is_last = False

    @api.multi
    def _search_is_last(self, operator, value):
        res = []
        journal = False
        statements = self.sudo().search([], order='journal_id, date DESC')
        for st in statements:
            if st.journal_id != journal:
                res.append(st.id)
                journal = st.journal_id
        if (operator == '=' and value is True) or (operator in ('<>', '!=') and value is False):
            search_operator = 'in'
        else:
            search_operator = 'not in'
        return [('id', search_operator, res)]

    @api.one
    @api.depends('line_ids', 'balance_start', 'line_ids.amount')
    def _end_balance(self):
        amount_income = amount_outcome = 0
        amount_income += sum(lines.amount if lines.amount >= 0 else 0 for lines in self.line_ids)
        amount_outcome += sum(lines.amount if lines.amount < 0 else 0 for lines in self.line_ids)
        self.total_entry_encoding = amount_income + amount_outcome
        self.balance_end_income = amount_income
        self.balance_end_outcome = abs(amount_outcome)
        self.balance_end = self.balance_start + self.total_entry_encoding
        self.balance_end_real = self.balance_start + amount_income - abs(amount_outcome)
        self.difference = self.balance_end_real - self.balance_end

    @api.multi
    def _get_opening_balance(self, journal_id):
        # ====================================================
        if self and self.ids:
            last_bnk_stmt = self.search([('id', '!=', self.ids[0]), ('journal_id', '=', journal_id)], limit=1)
        else:
            last_bnk_stmt = self.search([('journal_id', '=', journal_id)], limit=1)
        # ====================================================
        if last_bnk_stmt:
            return last_bnk_stmt.balance_end_real
        return 0

    @api.model
    def create(self, values):
        res = super(AccountBankStatement, self).create(values)
        if res.name == '/':
            context = {'ir_sequence_date': res.date}
            if res.journal_id.sequence_id:
                st_number = res.journal_id.sequence_id.with_context(**context).next_by_id()
            else:
                SequenceObj = self.env['ir.sequence']
                st_number = SequenceObj.with_context(**context).next_by_code('account.bank.statement')
            res.name = st_number
        res._set_opening_balance(res.journal_id.id)
        return res

    '''
    @api.multi
    def write(self, vals):
        for st in self:
            after_statements = self.search([('id','<>',st.id),('journal_id', '=', st.journal_id.id), ('date','>=',st.date), ('date', '!=', False)], order='date asc')
            # Тухайн журнал дээрх тухайн ордероос хойшхи хаагдсан ордерууд байвал эхний болон эцсийн үлдэгдлийг шинэчилж тааруулна.
            if after_statements:
                balance_start = st.balance_end_real  # Нээлтийн үлдэгдэл
                st.resolve_statement_balance(after_statements, balance_start)
        res = super(AccountBankStatement, self).write(vals)
        return res
    '''
    
    @api.multi
    def write(self, vals):
        res = super(AccountBankStatement, self).write(vals)
        if vals.get('date', False):
            self.check_statement_date()
        return res
        
    @api.multi
    def check_confirm_bank(self):
        self.check_statement_date()
        return super(AccountBankStatement, self).check_confirm_bank()
        
    @api.multi
    def check_statement_date(self):
        for obj in self.filtered(lambda x: x.company_id and x.company_id.statement_date == 'same'):
            if obj.line_ids.filtered(lambda x: x.date != obj.date):
                raise ValidationError(_("Statement line date must be same as statement date !!!"))
        
    @api.multi
    def resolve_statement_balance(self, after_statements, balance_start):
        # Касс харилцахын ордеруудын эхний болон эцсийн үлдэгдлийг
        # өгөгдсөн эхлэх үлдэгдлийн дагуу хөөж засварлана.
        for after in after_statements:
            balance_end = balance_start + after.balance_end_real - after.balance_start  # Хаалтын үлдэгдэл
            #self.env.cr.execute("UPDATE account_bank_statement SET balance_start = %s, balance_end_real = %s, balance_end = %s WHERE id = %s", (balance_start, balance_end, balance_end, after.id))
            after.write({'balance_start':balance_start,'balance_end_real':balance_end})
            balance_start = balance_end
        return True
    
    @api.multi
    def _balance_check(self):
        for stmt in self:
            if not stmt.currency_id.is_zero(stmt.difference):
                if stmt.journal_type == 'cash':
                    if stmt.difference < 0.0:
                        account = stmt.journal_id.loss_account_id
                        name = _('Loss')
                    else:
                        # statement.difference > 0.0
                        account = stmt.journal_id.profit_account_id
                        name = _('Profit')
                    if not account:
                        raise UserError(_('There is no account defined on the journal \"%s\" for \"%s\" involved in a cash difference.') % (stmt.journal_id.name, name))
                    #Автомат тооцоололд мөнгөн гүйлгээний төрөл нэмэв
                    if not stmt.journal_id.cashflow_id:
                        raise UserError(_('There is no default cashflow type defined on the journal \"%s\"') % (stmt.journal_id.name))

                    values = {
                        'statement_id': stmt.id,
                        'account_id': account.id,
                        'amount': stmt.difference,
                        'cashflow_id': stmt.journal_id.cashflow_id.id,
                        'name': _("Cash difference observed during the counting (%s)") % name,
                    }
                    self.env['account.bank.statement.line'].create(values)
                else:
                    balance_end_real = formatLang(self.env, stmt.balance_end_real, currency_obj=stmt.currency_id)
                    balance_end = formatLang(self.env, stmt.balance_end, currency_obj=stmt.currency_id)
                    if balance_end_real != balance_end:
                        raise UserError(_('The ending balance is incorrect !\nThe expected balance (%s) is different from the computed one. (%s)') % (balance_end_real, balance_end))
        return True

    @api.multi
    def button_confirm_bank(self):
        self._balance_check()
        res = super(AccountBankStatement, self).button_confirm_bank()
        for st in self:
            # Өмнөх кассын ордерууд хаагдаагүй байвал тэдгээрийг хаагдтал уг кассыг хаахгүй
            before_state_ids = self.search([('id', '!=', st.id),
                                            ('state', 'in', ['open', 'draft']),
                                            ('journal_id', '=', st.journal_id.id),
                                            ('date', '<', st.date)])
            if before_state_ids:
                raise UserError(_('Warning ! There is some open statement order exists in this journal before %s date! You cannot perform this action until close them.') % st.date)
            
            after_statements = self.search([('id','<>',st.id),('journal_id', '=', st.journal_id.id), ('date','>=',st.date), ('date', '!=', False)], order='date asc')
            # Тухайн журнал дээрх тухайн ордероос хойшхи хаагдсан ордерууд байвал эхний болон эцсийн үлдэгдлийг шинэчилж тааруулна.
            if after_statements:
                balance_start = st.balance_end_real  # Нээлтийн үлдэгдэл
                st.resolve_statement_balance(after_statements, balance_start)
        return res

    @api.model
    def get_journal_id_to_js(self, id):
        return self.env['account.bank.statement'].search([('id', '=', id)]).journal_id.type

    @api.model
    def get_object_reference(self, module, xml_id):
        """Returns (model, res_id) corresponding to a given module and xml_id (cached) or raise ValueError if not found"""
        return self.env['ir.model.data'].xmlid_lookup("%s.%s" % (module, xml_id))[1:3]

    @api.multi
    def button_cancel(self):
        for statement in self:
            if not statement.journal_id.update_posted:
                raise UserError(
                    _('You cannot modify a posted entry of this journal.\nFirst you should set the journal to allow cancelling entries.'))
            for line in statement.line_ids.filtered(lambda l: not (l.payment_id and l.payment_id.payment_type == 'currency')):
                for entry in line.journal_entry_ids:
                    entry.write({'state': 'draft',
                                 'statement_id': False})
                    for entry_line in entry.line_ids:
                        entry_line.write({'statement_id': False, })
                    entry.unlink()
            statement.write({'state': 'open'})

    @api.multi
    def button_draft(self):
        # Мөчлөг шалгах
        can_draft = True
        for obj in self:
            period = self.env['account.period'].search([('date_start', '<=', obj.date),
                                                        ('date_stop', '>=', obj.date),
                                                        ('company_id', '=', obj.company_id.id)], limit=1)
            can_draft = False if (period and period.state == 'done') else True
        if not can_draft:
            raise UserError(_('You cannot open bank statement in closed period!'))
        else:
            return super(AccountBankStatement, self).button_draft()
            
    @api.multi
    def calculate_after_bank_statements(self):
        for statement in self:
            after_statements = self.search([('id','<>',statement.id),('journal_id', '=', statement.journal_id.id), ('date','>=',statement.date), ('date', '!=', False)], order='date asc')
            # Тухайн журнал дээрх тухайн ордероос хойшхи хаагдсан ордерууд байвал эхний болон эцсийн үлдэгдлийг шинэчилж тааруулна.
            if after_statements:
                balance_start = statement.balance_end_real  # Нээлтийн үлдэгдэл 
                statement.resolve_statement_balance(after_statements, balance_start)
        return True