# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
import base64
import xlrd
from tempfile import NamedTemporaryFile
from datetime import datetime

class AccountBankStatementImportFile(models.TransientModel):
    _name = 'account.bank.statement.import.file'
    _description = "Import File"

    data_file = fields.Binary(string='Bank Statement File', required=True, help='Get you bank statements in electronic format from your bank and select them here.')

    @api.multi
    def import_file(self):
        company = self.env.user.company_id
        context = self._context
        statement=None
        if context.get('active_model') == 'account.bank.statement' and context.get('active_ids'):
            statement = self.env['account.bank.statement'].browse(context['active_ids'])[0]
        if not statement:
            raise UserError(_("No active statement!"))
            
        try:
            fileobj = NamedTemporaryFile('w+')
            fileobj.write(base64.decodestring(self.data_file))
            fileobj.seek(0)
            book = xlrd.open_workbook(fileobj.name)
        except ValueError:
            raise UserError(
                _('Error loading data file. \ Please try again!'))
        
        start_sequence = 1
        last_statment_line = self.env['account.bank.statement.line'].search([('statement_id', '=', statement.id)], order='sequence desc', limit=1)
        if last_statment_line:
            start_sequence = last_statment_line.sequence+1

        sheet = book.sheet_by_index(0)
        nrows = sheet.nrows
        rowi = 1
        while rowi < nrows:
            partner_id = False
            cashflow_id = False
            analytic_id = False
            amount = 0.0
            row = sheet.row(rowi)
            
            try:
                date = datetime.strptime(row[0].value, '%Y-%m-%d')
            except ValueError:
                raise ValidationError(_('Date error %s row! \n \
                format must \'YYYY-mm-dd\'' % rowi))
                
            name = row[1].value
            partner_name = row[2].value
            db_amount = row[3].value
            cr_amount = row[4].value
            cashflow_type_name = row[6].value
            analytic_name = row[5].value
            if db_amount and cr_amount:
                raise ValidationError(_('Data error %s row! \n \
                    Only one of Income and Expense columns \
                    must have a value' % rowi))
            
            if db_amount:
                amount = db_amount
            if cr_amount:
                amount = -cr_amount
            
            partner = self.env['res.partner'].search([('name', '=', partner_name)], limit=1)
            if partner:
                partner_id = partner.id

            if cashflow_type_name:
                cashflow = self.env['account.cashflow.type'].search([('name', '=', cashflow_type_name)], limit=1)
                if not cashflow:
                    raise UserError(_("No cashflow found matching '%s'.") % cashflow_type_name)
                cashflow_id = cashflow.id
                
            if analytic_name:
                analytic = self.env['account.analytic.account'].search([('name', '=', analytic_name), ('company_id', '=', company.id)], limit=1)
                analytic_id = analytic.id
                
            bank_account_id = False
            partner_bank = self.env['res.partner.bank'].search([('partner_id', '=', partner_id)], limit=1)
            if partner_bank:
                bank_account_id = partner_bank.id
            self.env['account.bank.statement.line'].create({
                    'name': name or '/',
                    'amount': amount,
                    'partner_id': partner_id,
                    'statement_id': statement.id,
                    'date': date,
                    'currency_id': statement.currency_id.id if statement.currency_id else False,
                    'cashflow_id': cashflow_id,
                    'analytic_account_id': analytic_id,
                    'sequence':start_sequence,
                    'bank_account_id':bank_account_id
                })
            rowi += 1
            start_sequence += 1
        return True

class account_statement_import_invoice(models.TransientModel):
    """ Generate Entries by Account Bank Statement from Invoices """
    _name = "account.bank.statement.import.invoice"
    _description = "Import Invoice"

    account_invoices = fields.Many2many(
        'account.invoice', 'account_invoice_relation', 'invoice_id', 'invoice_line', 'Invoices')

    def prepare_bank_statement_line(self,move_line,amount,bank_account_id,statement,line):
        vals = {
            'name': move_line.name or '?',
            'amount': amount,
            'partner_id': move_line.partner_id.id,
            'bank_account_id': bank_account_id,
            'statement_id': statement.id,
            'ref': move_line.ref,
            'date': statement.date,
            'amount_currency': amount if line.currency_id != line.company_id.currency_id else move_line.amount_currency,
            'currency_id': move_line.currency_id.id,
            'import_line_id': move_line.id,
            'account_id': line.account_id.id or False,
            'cashflow_id': line.account_id.cashflow_account_ids[0].id if line.account_id.cashflow_account_ids else False
        }
        return vals
    
    def populate_invoice(self):
        context = dict(self._context or {})
        statement_id = context.get('statement_id', False)
        if not statement_id:
            return {'type': 'ir.actions.act_window_close'}
        account_invoices = self.account_invoices
        if not account_invoices:
            return {'type': 'ir.actions.act_window_close'}

        statement_line_obj = self.env['account.bank.statement.line']
        move_line_obj = self.env['account.move.line']

        statement = self.env['account.bank.statement'].browse(statement_id)
        # for each selected move lines
        for line in self.account_invoices:
            bank_account_id = False
            ctx = context.copy()
            ctx['date'] = statement.date
            amount = 0.0
            count = 0
            reconcile = self.env['account.move.line.reconcile.writeoff'].search(
                [('writeoff_acc_id', '=', line.account_id.id)])
            move_line_id = move_line_obj.search(
                [('account_id', '=', line.account_id.id), ('invoice_id', '=', line.id),('reconciled','=',False)])
            if move_line_id:
                move_line = move_line_id[0]
                if line.type == 'out_refund' or line.type == 'in_invoice':
                    amount = -line.residual
                elif line.type == 'in_refund' or line.type == 'out_invoice':
                    amount = line.residual
                
                partner_bank = self.env['res.partner.bank'].search([('partner_id', '=', move_line.partner_id.id)], limit=1)
                if partner_bank:
                    bank_account_id = partner_bank.id
                vals = self.prepare_bank_statement_line(move_line,amount,bank_account_id,statement,line)
                statement_line_obj.create(vals)
        return {'type': 'ir.actions.act_window_close'}
