# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import Warning, UserError
from odoo.tools import float_is_zero

from datetime import datetime


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.depends('invoice_line_ids')
    def _compute_amount_discount(self):
        discount_total = 0
        for obj in self:
            for line in obj.invoice_line_ids:
                discount = line.quantity * line.price_unit * (1 - (line.discount or 0.0) / 100.0)
                total = line.quantity * line.price_unit
                discount_total += total - discount
    
            obj.amount_discount = discount_total

    is_prepayment = fields.Boolean('Is Prepayment')  # Урьдчилгаа нэхэмжлэлийн ялгах талбар
    account_transaction_id = fields.Many2one('account.transaction', 'Account Transaction')  # Гүйлгээний утга сонгох талбар
    amount_discount = fields.Monetary(string='Amount Discount', store=True, readonly=True, compute='_compute_amount_discount',track_visibility='always')
    category_hierarchy_id = fields.Many2one(related='partner_id.category_hierarchy_id')
    old_number = fields.Char('Old Number', copy=False)
    currency_rate = fields.Float(string='Currency Rate')
    date_invoice = fields.Date(string='Invoice Date', default=fields.Datetime.now,
                               readonly=True, states={'draft': [('readonly', False)]}, index=True, copy=False)

    @api.multi
    def recompute_price(self):
        for invoice in self:
            for line in invoice.invoice_line_ids:
                line._compute_price()
        return True

    def _check_invoice_reference(self):
        #Нэг худалдан авалтын захиалгаас олон нийлүүлэгчийн нэхэмжлэл үүсэх үед давхардсан код байна гэсэн алдаа зааж байсан.
        return True

    #Нэхэмжлэлийн гүйлгээний утгыг өөрчлөн сонгоход мөрийн гүйлгээний утгыг өөрчлөх
    @api.onchange('account_transaction_id')
    def onchange_account_transaction(self):
        value = []
        if self.account_transaction_id:
            for inv in self:
                inv.name = self.account_transaction_id.name
                for line in inv.invoice_line_ids:
                    value.append(
                        (1, line.id, {
                            'account_transaction_id': self.account_transaction_id.id,
                        }
                        )
                    )
            self.invoice_line_ids = value

    @api.onchange('date_invoice')
    def _onchange_date_invoice(self):
        if self.date_invoice and self.currency_id != self.company_id.currency_id:
            self.currency_rate = self.set_currency(self.date_invoice)

    @api.onchange('currency_id')
    def onchange_currency_id(self):
        if self.currency_id and self.currency_id != self.company_id.currency_id:
            date = fields.Date.context_today(self)
            if self.date_invoice:
                date = self.date_invoice
            self.currency_rate = self.set_currency(date)

    @api.multi
    def set_currency(self, date):
        # Тухайн огноо болон вальютанд харгалзах ханш гарч ирнэ, тухайн огнооны өмнөх хаалтын ханшаар тэгшитгэл хийгдэнэ.
        rate = 1
        rate_id = self.env['res.currency.rate'].search([('currency_id', '=', self.currency_id.id), ('name', '<=', date)], order='name desc')
        if rate_id:
            rate = rate_id[0].alter_rate
        return rate

    # Нэхэмжлэлээс ажил гүйлгээ үүсэхдээ валютын ханш талбарт байгаа ханшийг авахаар тохируулахын тулд
    # дахин тодорхойлов.
    @api.multi
    def compute_invoice_totals(self, company_currency, invoice_move_lines):
        total = 0
        total_currency = 0
        for line in invoice_move_lines:
            if self.currency_id != company_currency:
                currency = self.currency_id.with_context(date=self._get_currency_rate_date() or fields.Date.context_today(self))
                if not (line.get('currency_id') and line.get('amount_currency')):
                    line['currency_id'] = currency.id
                    line['amount_currency'] = currency.round(line['price'])
                    currency_rate = self.currency_rate
                    if currency_rate == 0:
                        currency_rate = self.set_currency(self.date_invoice if self.date_invoice else fields.Date.context_today(self))
                        self.write({'currency_rate': currency_rate})
                    line['price'] = line['price'] * currency_rate
            else:
                line['currency_id'] = False
                line['amount_currency'] = False
                line['price'] = self.currency_id.round(line['price'])
            if self.type in ('out_invoice', 'in_refund'):
                total += line['price']
                total_currency += line['amount_currency'] or line['price']
                line['price'] = - line['price']
            else:
                total -= line['price']
                total_currency -= line['amount_currency'] or line['price']
        return total, total_currency, invoice_move_lines

    @api.multi
    def cancel_invoice(self):
        # Төлөгдсөн төлөвт байгаа 0 дүнгээр үүссэн нэхэмжлэлийг цуцлах функц
        for invoice in self:
            if invoice.amount_total == 0:
                invoice.state = 'open'
                invoice.action_cancel()

    @api.multi
    def action_cancel(self):
        for move in self.move_id:
            for line in move.line_ids:
                line.write({'invoice_id': False})

        # Нэхэмжлэл устгахад move_name талбарын утгаас болж устгаж болохгүй байсан тул нэмэв.
        self.write({'move_name': False})

        res = super(AccountInvoice, self).action_cancel()
        return res

    @api.multi
    def invoice_print(self):
        self.ensure_one()
        self.sent = True
        return self.env['report'].get_action(self, 'l10n_mn_account.report_invoice')

    @api.onchange('origin')
    def onchange_origin(self):
        if self.origin:
            self.name = self.origin

    @api.model
    def tax_line_move_line_get(self):
        res = []
        # keep track of taxes already processed
        done_taxes = []
        # loop the invoice.tax.line in reversal sequence
        for tax_line in sorted(self.tax_line_ids, key=lambda x: -x.sequence):
            if tax_line.amount:
                tax = tax_line.tax_id
                if tax.amount_type == "group":
                    for child_tax in tax.children_tax_ids:
                        done_taxes.append(child_tax.id)
                res.append({
                    'invoice_tax_line_id': tax_line.id,
                    'tax_line_id': tax_line.tax_id.id,
                    'type': 'tax',
                    'name': tax_line.name,
                    'price_unit': tax_line.amount,
                    'quantity': 1,
                    'price': tax_line.amount,
                    'account_id': tax_line.account_id.id,
                    'account_analytic_id': tax_line.account_analytic_id.id,
                    'invoice_id': self.id,
                    # ############# Хэрэв татвар дээр холбогдох харилцагч байвал тухайн утгыг partner_id талбарт оноох
                    'tax_ids': [(6, 0, list(done_taxes))] if tax_line.tax_id.include_base_amount else [],
                    #################################################################################################
                    'partner_id': tax_line.partner_id.id if tax_line.partner_id else False,
                })
                done_taxes.append(tax.id)
        return res

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        super(AccountInvoice, self)._onchange_partner_id()

        for obj in self:
            obj.reference = obj.partner_id.ref


    @api.multi
    def action_move_create(self):
        """ Creates invoice related analytics and financial move lines """
        account_move = self.env['account.move']

        for inv in self:
            if not inv.journal_id.sequence_id:
                raise UserError(_('Please define sequence on the journal related to this invoice.'))
            if not inv.invoice_line_ids:
                raise UserError(_('Please create some invoice lines.'))
            if inv.move_id:
                continue

            ctx = dict(self._context, lang=inv.partner_id.lang)

            if not inv.date_invoice:
                inv.with_context(ctx).write({'date_invoice': fields.Date.context_today(self)})
            company_currency = inv.company_id.currency_id

            # create move lines (one per invoice line + eventual taxes and analytic lines)
            iml = inv.invoice_line_move_line_get()
            iml += inv.tax_line_move_line_get()
            diff_currency = inv.currency_id != company_currency
            description = ""
            # create one move line for the total and possibly adjust the other lines amount
            total, total_currency, iml = inv.with_context(ctx).compute_invoice_totals(company_currency, iml)

            name = inv.name or '/'
            if inv.payment_term_id:
                totlines = inv.with_context(ctx).payment_term_id.with_context(currency_id=company_currency.id).compute(total, inv.date_invoice)[0]
                res_amount_currency = total_currency
                ctx['date'] = inv._get_currency_rate_date()
                for i, t in enumerate(totlines):
                    if inv.currency_id != company_currency:
                        amount_currency = company_currency.with_context(ctx).compute(t[1], inv.currency_id)
                    else:
                        amount_currency = False

                    # last line: add the diff
                    res_amount_currency -= amount_currency or 0
                    if i + 1 == len(totlines):
                        amount_currency += res_amount_currency

                    iml.append({
                        'type': 'dest',
                        'name': name,
                        'price': t[1],
                        'account_id': inv.account_id.id,
                        'date_maturity': t[0],
                        'amount_currency': diff_currency and amount_currency,
                        'currency_id': diff_currency and inv.currency_id.id,
                        'invoice_id': inv.id
                    })
            else:
                iml.append({
                    'type': 'dest',
                    'name': name,
                    'price': total,
                    'account_id': inv.account_id.id,
                    'date_maturity': inv.date_due,
                    'amount_currency': diff_currency and total_currency,
                    'currency_id': diff_currency and inv.currency_id.id,
                    'invoice_id': inv.id
                })

            # part = self.env['res.partner']._find_accounting_partner(inv.partner_id)
            part = inv.partner_id
            # ############################ Хэрэв татвар дээр Холбогдох харилцагч талбар утгатай байвал тухайн утгыг журналын бичилтийн мөр лүү шилжүүлнэ
            line = [(0, 0, self.line_get_convert(l, l['partner_id'] if l.get('partner_id') else part.id)) for l in iml]
            ###########################################################################################################################################
            line = inv.group_lines(iml, line)

            journal = inv.journal_id.with_context(ctx)
            line = inv.finalize_invoice_move_lines(line)

            date = inv.date or inv.date_invoice
            purchase_ids = inv.invoice_line_ids.mapped('purchase_id')
            description = False
            for p in purchase_ids:
                if p.notes:
                    description = p.notes
                else:
                    description = p.name
            move_vals = {
                'ref': inv.origin, #inv.reference байсныг origin болгов
                'line_ids': line,
                'journal_id': journal.id,
                'date': date,
                'narration': inv.comment,
                'description': description,
            }
            ctx['company_id'] = inv.company_id.id
            ctx['invoice'] = inv
            ctx_nolang = ctx.copy()
            ctx_nolang.pop('lang', None)
            move = account_move.with_context(ctx_nolang).create(move_vals)
            # Pass invoice in context in method post: used if you want to get the same
            # account move reference when creating the same invoice after a cancelled one:
            move.post()
            # make the invoice point to that move
            vals = {
                'move_id': move.id,
                'date': date,
                'move_name': move.name,
                'old_number': move.name,
            }
            inv.with_context(ctx).write(vals)

        return True

    def action_invoice_open(self):
        #     хэрвээ тухайн захиалагчийн гүйлгээ хийх эрх түгжигдсэн бол анхааруулга буцаана
        for rec in self:
            if rec.partner_id.lock_bill:
                if rec.date_invoice:
                    lock_date = datetime.strptime(rec.partner_id.lock_date, "%Y-%m-%d")
                    date_inv = datetime.strptime(rec.date_invoice, "%Y-%m-%d")
                    if lock_date >= date_inv:
                        raise UserError(_('This partner is locked!'))
                else:
                    raise UserError(_('Must be enter date of invoice!'))
            # Нэхэмжлэхийн валют дансны валюттай ижил эсэхийг шалгах
            currency_id = rec.account_id.currency_id if rec.account_id.currency_id else self.env.user.company_id.currency_id
            if currency_id.id != rec.currency_id.id:
                raise UserError(_('Invoice currency does not match account currency!'))
        return super(AccountInvoice, self).action_invoice_open()
    
    @api.multi
    def action_invoices_cancel(self):
        # Called from server action: To Prevent singleton error, using loop.
        for obj in self:
            obj.action_invoice_cancel()
            
    @api.multi
    def action_invoices_draft(self):
        # Called from server action: To Prevent singleton error, using loop.
        for obj in self:
            obj.action_invoice_draft()
            
    #Захиалагчийн нэхэмжлэл дээр УОО буюу өглөг төрөлтэй дансаар гүйлгээ хийдэг болгосноор төлбөр төлөхөд өөрөө өөртэйгөө тулж байсныг болиулсан.
    @api.multi
    def register_payment(self, payment_line, writeoff_acc_id=False, writeoff_journal_id=False):
        """ Reconcile payable/receivable lines from the invoice with payment_line """
        line_to_reconcile = self.env['account.move.line']
        # Нэхэмжлэлийн үлдэгдэл тооцох үед борлуулалтын тохиргооны Хадгаламжийн Бүтээгдэхүүнд тохируулсан дансны бичилт тооцохгүй болгох
        product_id = self.env['ir.values'].get_default('sale.config.settings', 'deposit_product_id_setting', company_id=self.env.user.company_id.id)
        account_income_id = self.env['product.product'].browse(product_id).product_tmpl_id.property_account_income_id
        for inv in self:
            line_to_reconcile += inv.move_id.line_ids.filtered(lambda r: not r.reconciled and r.account_id.internal_type in ('payable', 'receivable') and r.account_id.id != account_income_id.id)
        return (line_to_reconcile + payment_line).reconcile(writeoff_acc_id, writeoff_journal_id)

class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'

    require_analytic = fields.Boolean(related="account_id.req_analytic_account", string="Require Analytic", readonly=True, default=False)
    account_transaction_id = fields.Many2one('account.transaction', 'Account Transaction')  # Гүйлгээний утга сонгох талбар
    date_invoice = fields.Date(related="invoice_id.date_invoice", store=True)
    state = fields.Selection(related="invoice_id.state", store=True)

    # Нэхэмжлэл дээр сонгосон гүйлгээний утга нь мөр нэмэхэд  default-р сонгогдох
    @api.model
    def default_get(self, fields):
        res = super(AccountInvoiceLine, self).default_get(fields)
        context = self._context
        if context.get('account_transaction_id'):
            # Нэхэмжлэлийн гүйлгээний утгыг мөрд default-р олгоно.
            res['account_transaction_id'] = context.get('account_transaction_id') or ''
        return res

    #Нэхэмжлэлийн мөрийн гүйлгээний утгыг өөрчлөн сонгоход мөрийн гүйлгээний утга талбарт утга оноох
    @api.onchange('account_transaction_id')
    def onchange_account_transaction(self):
        value = []
        if self.account_transaction_id:
            self.name = self.account_transaction_id.name

    @api.multi
    def name_get(self):
        return [(line.id, '%s%s' % (line.invoice_id and '%s/ ' % line.invoice_id.number or '', line.name))
                for line in self]
        
    @api.one
    @api.depends(
        'state', 'currency_id', 'invoice_line_ids.price_subtotal',
        'move_id.line_ids.amount_residual',
        'move_id.line_ids.currency_id')
    def _compute_residual(self):
        residual = 0.0
        residual_company_signed = 0.0
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        # OVERRIDE BEGINS
        # Нэхэмжлэлийн үлдэгдэл тооцох үед борлуулалтын тохиргооны Хадгаламжийн Бүтээгдэхүүнд тохируулсан дансны бичилт тооцохгүй болгох
        product_id = self.env['ir.values'].get_default('sale.config.settings', 'deposit_product_id_setting', company_id=self.env.user.company_id.id)
        account_income_id = self.env['product.product'].browse(product_id).product_tmpl_id.property_account_income_id
        for line in self.sudo().move_id.line_ids:
            if line.account_id.internal_type in ('receivable', 'payable') and line.account_id.id != account_income_id.id:
                residual_company_signed += line.amount_residual
                if line.currency_id == self.currency_id:
                    residual += line.amount_residual_currency if line.currency_id else line.amount_residual
                else:
                    from_currency = (line.currency_id and line.currency_id.with_context(date=line.date)) or line.company_id.currency_id.with_context(date=line.date)
                    residual += from_currency.compute(line.amount_residual, self.currency_id)
        self.residual_company_signed = abs(residual_company_signed) * sign
        self.residual_signed = abs(residual) * sign
        self.residual = abs(residual)
        digits_rounding_precision = self.currency_id.rounding
        # Ноорог нэхэмжлэл батлахад борлуулалтын тохиргооны Хадгаламжийн Бүтээгдэхүүнд тохируулсан дансны бичилтээс шалтгаалан төлсөн төлөвт орж байсныг нээх төлөвт оруулдаг болгов
        if float_is_zero(self.residual, precision_rounding=digits_rounding_precision):
            self.reconciled = True
        else:
            self.reconciled = False


class AccountInvoiceTax(models.Model):
    ''' Татварын журналын бичилтийг үндсэн харилцагчаас өөр харилцагчид бүртгэхийн тулд
        татварын мэдээллээс харилцагч талбарын утгыг авч ашиглана.
    '''
    _inherit = 'account.invoice.tax'

    partner_id = fields.Many2one('res.partner', 'Associated Partner')
    account_analytic_id = fields.Many2one('account.analytic.account', string='Analytic account',
                                          domain="[('type','=','normal')]")

    @api.model
    def create(self, vals):
        inv_tax = super(AccountInvoiceTax, self).create(vals)
        if inv_tax.tax_id and inv_tax.tax_id.partner_id:
            inv_tax.partner_id = inv_tax.tax_id.partner_id
        return inv_tax
