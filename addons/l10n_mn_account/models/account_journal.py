# -*- coding: utf-8 -*-
# Part of OdERP. See LICENSE file for full copyright and licensing details.

from odoo import models, api, fields


class account_journal(models.Model):
    _inherit = 'account.journal'

    # Нээлтийн балансаас төгсгөлийн баланс ялгаатай үед
    # касс батлахад автоматаар үүсэх ашиг/алдагдлын журналд зориулсан мөнгөн гүйлгээний төрөл
    cashflow_id = fields.Many2one('account.cashflow.type', string='Cashflow Type', help="Used to register a loss or profit when the ending balance of a cash register differs from what the system computes.", ondelete='restrict')
    payroll = fields.Boolean('Journal for payroll')

    @api.multi
    def open_exchange_money(self):
        return self.open_payments_action('currency')
