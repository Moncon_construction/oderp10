# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, addons

class ResUsers(models.Model):
    _inherit = 'res.users'

    @api.multi
    def get_account_user_role_mobile(self):
        """
            Групийн дагуу нэвтэрсэн хэрэглэгч Санхүү - д хандах эрхтэй эсэх, үүргийг тодорхойлно
        """
        user_groups_ids = self.env.user.groups_id.ids
        group_id_user = self.env.ref('account.group_account_manager').id
        if group_id_user in user_groups_ids:
            return "group_account_manager"
        group_id_manager = self.env.ref('account.group_account_user').id
        if group_id_manager in user_groups_ids:
            return "group_account_user"
        return ""