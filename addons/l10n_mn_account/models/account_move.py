# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from datetime import date, datetime
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from odoo.exceptions import UserError, ValidationError
from odoo.tools import float_is_zero

class AccountMove(models.Model):
    _name = "account.move"
    _inherit = ['account.move', 'mail.thread', 'ir.needaction_mixin']

    @api.multi
    def _get_default_journal(self):
#         Журналын төрөл нь context-с ирсэн төрөлтэй адилхан байх журналын бичилтэнд сонгогдохгүй журналыг буцаах
        if self.env.context.get('default_journal_type'):
             return self.env['account.journal'].search([('type', '=', self.env.context['default_journal_type']), ('journal_entries_selectable','=',False)], limit=1).id

    description = fields.Char(string="Label")
    journal_id = fields.Many2one('account.journal', string='Journal', required=True, states={'posted': [('readonly', True)]}, domain=[('journal_entries_selectable','=',False)], default=_get_default_journal)
    account_transaction_id = fields.Many2one('account.transaction', 'Account Transaction')# Гүйлгээний утга сонгох талбар
    state = fields.Selection([('draft', 'Unposted'), ('posted', 'Posted')], string='Status', required=True, readonly=True, copy=False, default='draft', track_visibility='onchange')

    @api.multi
    def unlink(self):
#         Журналын бичилт нь нэхэмжлэлээс, кассаас, харилцахаас, барааны шилжилт хөдөлгөнөөс, үндсэн хөрөнгөнөөс үүссэн бол устгагдахгүй
        for move in self:
            for line in move.line_ids:
                if line.statement_id:
                    raise UserError(_('The entries created by transactions of cash cannot delete.'))
                elif line.related_statement_line_ids:
                    raise UserError(_('The enteries created by currency exchange cannot delete'))
        return super(AccountMove, self).unlink()

    #Журналын бичилтийн гүйлгээний утгыг өөрчлөн сонгоход мөрийн гүйлгээний утгыг өөрчлөх
    @api.onchange('account_transaction_id')
    def onchange_account_transaction(self):
        value = []
        if self.account_transaction_id:
            for move in self:
                move.description = self.account_transaction_id.name
                for line in move.line_ids:
                    value.append(
                     (1, line.id, {
                            'account_transaction_id':self.account_transaction_id.id,
                          }
                      )
                    )
            self.line_ids = value

    @api.model
    def create(self, vals):
        # call super
        move = super(AccountMove, self).create(vals)
        lock_date = max(move.company_id.period_lock_date, move.company_id.fiscalyear_lock_date)
        if self.user_has_groups('account.group_account_manager'):
            lock_date = move.company_id.fiscalyear_lock_date

        if move.date <= lock_date and move.line_ids:
            for line in move.line_ids[0]:
                if line.stock_move_id and line.stock_move_id.date <= lock_date:
                    transfer_obj = self.env['stock.immediate.transfer'].search(
                        [('pick_id', '=', line.stock_move_id.picking_id.id)], order="id desc", limit=1)
                    if transfer_obj and transfer_obj.pick_id:
                        if transfer_obj.force_date >= lock_date:
                            move.write({'date': transfer_obj.force_date})
                if line.invoice_id and line.invoice_id.invoice_line_ids:
                    for invoice_line in line.invoice_id.invoice_line_ids[0]:
                        self._cr.execute("""SELECT order_line_id
                                                    FROM sale_order_line_invoice_rel
                                                    WHERE invoice_line_id IN (%s)
                                                            """ % invoice_line.id)
                        results = self._cr.fetchall()
                        if results:
                            sale_order = self.env['sale.order.line'].browse(results[0]).order_id
                            if sale_order:
                                is_prepayment = False
                                for invoice in sale_order.invoice_ids:
                                    if line.invoice_id.id != invoice.id and invoice.is_prepayment == True:
                                        is_prepayment = True
                                        break
                                if is_prepayment:
                                    move.write({'date': line.invoice_id.date_invoice})
        elif move.date >= lock_date and move.line_ids:
            for line in move.line_ids[0]:
                transfer_obj = self.env['stock.immediate.transfer'].search(
                    [('pick_id', '=', line.stock_move_id.picking_id.id)], order="id desc", limit=1)
                if transfer_obj and transfer_obj.pick_id:
                    if transfer_obj.force_date >= lock_date:
                        move.write({'date': transfer_obj.force_date})
                    else:
                        message = _("You cannot add/modify entries prior to and inclusive of the lock date %s") % (
                            lock_date)
                        raise UserError(message)
        # Журналын бичилтийн автомат батлах чагттай журналыг батлах
        if move.journal_id.auto_approve and not self.env.context.get('_reverse_move', False):
            move.post()
        return move

    @api.multi
    def write(self, vals):
        res = super(AccountMove, self).write(vals)

        for line_id in self:
            line_id.line_ids.compute_current_rate()
        return res

    @api.multi
    def post(self):
        self.assert_balanced_approve()
        invoice = self._context.get('invoice', False)
        self._post_validate()
        for move in self:
            move.line_ids.create_analytic_lines()
            if move.name == '/':
                new_name = False
                journal = move.journal_id

                if invoice and invoice.move_name and invoice.move_name != '/':
                    new_name = invoice.move_name
                else:
                    if journal.sequence_id:
                        # If invoice is actually refund and journal has a refund_sequence then use that one or use the regular one
                        sequence = journal.sequence_id
                        if invoice and invoice.type in ['out_refund', 'in_refund'] and journal.refund_sequence:
                            if not journal.refund_sequence_id:
                                raise UserError(_('Please define a sequence for the refunds'))
                            sequence = journal.refund_sequence_id
                        # BEGIN Тохиргооноос хамаарч дугаарлана.
                        if move.company_id.inv_sequence == 'current' and invoice and invoice.old_number:
                            new_name = invoice.old_number
                        else:
                            new_name = sequence.with_context(ir_sequence_date=move.date).next_by_id()
                        # END
                    else:
                        raise UserError(_('Please define a sequence on the journal.'))

                if new_name:
                    move.name = new_name

            if move.partner_id.lock_bill:
                for line_id in move.line_ids:
                    lock_date = datetime.strptime(move.partner_id.lock_date, "%Y-%m-%d")
                    date_inv = datetime.strptime(move.date, "%Y-%m-%d")
                    if lock_date >= date_inv:
                        raise UserError(_('This partner is locked!'))
        return self.write({'state': 'posted'})

    @api.multi
    def button_cancel(self):
        for move in self:
            for line in move.line_ids:
                if line.invoice_id:
                    raise UserError(_("Move cannot be canceled if linked to an invoice. (Invoice: %s - Move ID:%s)") % (line.invoice_id.number, move.name))
        self.write({'state': 'draft'})
        res = super(AccountMove, self).button_cancel()
        return res

    @api.multi
    def _post_validate(self):
        for move in self:
            if move.line_ids:
                if not all([x.company_id.id == move.company_id.id for x in move.line_ids]):
                    raise UserError(_("Cannot create moves for different companies."))
        return self._check_lock_date()

    @api.multi
    def assert_balanced(self):
        return True

    @api.onchange('date')
    def onchange_date(self):
        self.line_ids.onchange_amount_currency()

    @api.multi
    def assert_balanced_approve(self):
        if not self.ids:
            return True
        prec = self.env['decimal.precision'].precision_get('Account')

        self._cr.execute("""\
            SELECT      move_id
            FROM        account_move_line
            WHERE       move_id in %s
            GROUP BY    move_id
            HAVING      abs(sum(debit) - sum(credit)) > %s
            """, (tuple(self.ids), 10 ** (-prec or -5)))
        if len(self._cr.fetchall()) != 0:
            raise UserError(_("Cannot create unbalanced journal entry."))
        return True

    @api.multi
    def _reverse_move(self, date=None, journal_id=None):
        self.ensure_one()
        date = date or fields.Date.today()
        # OVERRIDE BEGINS: Журналын бичилт үүсэхдээ батлагдаж байсан тул батлах хэсэгт context илгээдэг болов
        reversed_move = self.with_context(_reverse_move=True).copy(default={
            'date': date,
            'journal_id': journal_id.id if journal_id else self.journal_id.id,
            'ref': _('reversal of: ') + self.name})
        for acm_line in reversed_move.line_ids.with_context(check_move_validity=False):
            acm_line.write({
                'debit': acm_line.credit,
                'credit': acm_line.debit,
                'amount_currency': -acm_line.amount_currency
            })
        self._reconcile_reversed_pair(self, reversed_move)
        return reversed_move

    # Дахин тодорхойлов
    @api.multi
    def _check_lock_date(self):
        for move in self:
            lock_date = max(move.company_id.period_lock_date, move.company_id.fiscalyear_lock_date)
            if self.user_has_groups('account.group_account_manager'):
                lock_date = move.company_id.fiscalyear_lock_date
            if move.date <= lock_date:
                inv = self.env['account.invoice'].search([('move_id', '=', move.id)])
                if not (inv and inv.is_prepayment and inv.date_invoice <= lock_date):
                    if self.user_has_groups('account.group_account_manager'):
                        message = _("You cannot add/modify entries prior to and inclusive of the lock date %s") % (
                            lock_date)
                    else:
                        message = _(
                            "You cannot add/modify entries prior to and inclusive of the lock date %s. Check the company settings or ask someone with the 'Adviser' role") % (
                                      lock_date)
                    raise UserError(message)
        return True


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    @api.model
    def _default_account(self):
#         Журналын бичилтийн мөр дээр context-с ирсэн дансыг буцаах ба байхгүй тохиолдолд тухайн журналын debit дансыг буцаана
        context = self._context.copy()
        journal_obj = self.env['account.journal']
        if context.get('account_id'):
            return context.get('account_id')
        else:
            if context.get('journal_id'):
                journal = journal_obj.search([['id','=',context.get('journal_id')]])
                if journal.default_debit_account_id.id:
                    return journal.default_debit_account_id.id
            return False

    @api.model
    def _get_currency(self):
#         context-с ирэх утгаас хамаарч мөр дээр журналын вальютыг эсвэл журналын debit дансны вальютыг онооно
        currency = False
        context = self._context or {}
        if context.get('default_journal_id', False):
            currency = self.env['account.journal'].browse(context['default_journal_id']).currency_id
        elif context.get('journal_id',False):
            account_id = self._default_account()
            account = self.env['account.account'].search([('id','=',account_id)])
            currency = account.currency_id
        return currency

    @api.model
    def _get_cashflow(self):
        is_cashflow = False
        for line in self:
            if line.cashflow_id and line.account_id.user_type_id.type == 'liquidity':
                is_cashflow = True
        return is_cashflow

    account_id = fields.Many2one('account.account', string='Account', required=True, index=True,
        ondelete="cascade", default=_default_account)
    currency_id = fields.Many2one('res.currency', string='Currency', default=_get_currency,
        help="The optional other currency if it is a multi-currency entry.")
    require_analytic = fields.Boolean(related="account_id.req_analytic_account", readonly=True, string='Require analytic account')
    invoice_id = fields.Many2one('account.invoice', string='Invoice', copy=False)
    currency_symbol = fields.Char(related='company_currency_id.symbol', string='Currency', readonly=True)
    amount_currency = fields.Monetary(default=0.0, help="The amount expressed in an optional other currency if it is a multi-currency entry.")
    cashflow_id = fields.Many2one('account.cashflow.type', string='Cashflow Type', ondelete='restrict')
    register_date = fields.Date(string="Register date")
    related_statement_line_ids = fields.Many2many('account.bank.statement.line', 'account_bank_statement_line_move_rel', 'move_line_id', 'statement_line_id', string='Related Statement Line', readonly=True)
    company_id = fields.Many2one('res.company', related='journal_id.company_id', string='Company', store=True, readonly=True,
        default=lambda self: self.env.user.company_id)
    currency_rate = fields.Float(string='Exchange rate', readonly=True)
    category_hierarchy_id = fields.Many2one(related='partner_id.category_hierarchy_id', store=True)
    account_transaction_id = fields.Many2one('account.transaction', 'Account Transaction')# Гүйлгээний утга сонгох талбар
    is_cashflow = fields.Boolean('Is Selected Cashflow', default=_get_cashflow)

    @api.onchange('cashflow_id')
    def onchange_cashflow(self):
        # мөнгөн гүйлгээний төрөл сонгосон бөгөөд дансны төрөл нь хөрвөх чадвартай үед утга онооно.
        for line in self:
            if line.cashflow_id and line.account_id.user_type_id.type == 'liquidity':
                line.is_cashflow = True

    @api.onchange('account_id')
    def onchange_account(self):
#         Данс өөрчлөгдвөл вальют өөрчлөгдөнө
        for line in self:
            if line.account_id:
                self.currency_id = line.account_id.currency_id

    #  Журналын бичилт дээр валютаарх дүнг тавихад монгол банкны ханшаар үржигдэн өглөг авлагын талбарт утга оноох
    @api.onchange('amount_currency', 'currency_id', 'date_maturity')
    @api.depends('amount_currency', 'currency_id', 'date_maturity')
    def onchange_amount_currency(self):
        for obj in self:
            if obj.date_maturity:
                move_dt = datetime.strptime(obj.date_maturity, '%Y-%m-%d')
                day = move_dt.replace(hour=0, minute=0, second=0)
                day = str(day)
                if obj.currency_id:
                    obj.debit = 0
                    obj.credit = 0
                    # Компаний валют сонгогдсон бол шууд дебит кредит дээр бичигдэнэ.
                    if obj.move_id.company_id.currency_id and obj.currency_id == obj.move_id.company_id.currency_id:
                        if obj.amount_currency > 0:
                            obj.debit = abs(obj.amount_currency)
                        else:
                            obj.credit = abs(obj.amount_currency)
                    else:
                        rate_id = obj.currency_id.rate_ids.filtered(lambda rate: rate.name <= day).sorted('name', reverse=True)
                        if rate_id:
                            rate_id = rate_id[0]
                            if obj.amount_currency > 0:
                                obj.debit = abs(obj.amount_currency * rate_id.alter_rate or 0)
                            else:
                                obj.credit = abs(obj.amount_currency * rate_id.alter_rate or 0)

    #Журналын бичилтийн мөрийн гүйлгээний утгыг өөрчлөн сонгоход мөрийн гүйлгээний утга талбарт утга оноох
    @api.onchange('account_transaction_id')
    def onchange_account_transaction(self):
        value = []
        if self.account_transaction_id:
            self.name = self.account_transaction_id.name

    @api.model
    def default_get(self, fields):
        res = super(AccountMoveLine, self).default_get(fields)
        context = self._context
        if context.get('line_ids'):
            total = 0.0
            # Нэг журналын бичилтийн хувьд мөрүүдийн дебит, кредит утгыг default-р олгоно.
            for line in self.env['account.move'].resolve_2many_commands('line_ids', context.get('line_ids')):
                total += line.get('debit', 0.0) - line.get('credit', 0.0)
            res['debit'] = total < 0 and -total or 0.0
            res['credit'] = total > 0 and total or 0.0
        if context.get('name'):
            # Нэг журналын бичилтийн хувьд гүйлгээний утга байвал мөрүүдийн гүйлгээний утга default-р олгоно.
            res['name'] = context.get('name') or ''
        if context.get('account_transaction_id'):
            # Нэхэмжлэлийн гүйлгээний утгыг мөрд default-р олгоно.
            res['account_transaction_id'] = context.get('account_transaction_id') or ''
        return res

    @api.multi
    def compute_current_rate(self):
        for line in self:
            if line.currency_id:
                query = """SELECT c.id, (SELECT r.alter_rate FROM res_currency_rate r
                                                WHERE r.currency_id = c.id AND r.name <= %s
                                                AND (r.company_id IS NULL OR r.company_id = %s)
                                                ORDER BY r.name DESC
                                                LIMIT 1) AS rate
                            FROM res_currency c
                            WHERE c.id = %s"""

                self._cr.execute(query, (line.date, line.company_id.id, line.currency_id.id))
                currency_rates = dict(self._cr.fetchall())
                if line.invoice_id and line.invoice_id.currency_rate != currency_rates.get(line.currency_id.id):
                    line.write({'currency_rate': line.invoice_id.currency_rate or 1.0})
                else:
                    line.write({'currency_rate': currency_rates.get(line.currency_id.id) or 1.0})
            else:
                line.currency_rate = 1.0

    @api.model
    def create(self, vals):
        # call super
        line = super(AccountMoveLine, self).create(vals)

        # compute rate
        line.compute_current_rate()

        # set register date
        line.write({'register_date': datetime.today().strftime('%Y-%m-%d')})

        #мөнгөн гүйлгээний төрөл сонгосон бөгөөд дансны төрөл нь хөрвөх чадвартай үед утга онооно.
        if line.cashflow_id and line.account_id.user_type_id.type == 'liquidity':
            line.is_cashflow = True

        # Гүйлгээний утга хоосон байхад нэхэмжлэх үүсгэж буй хүний нэрийн авах
        if line.name == '/':
            if line.invoice_id:
                line.name = line.invoice_id.partner_id.name + u' Нэхэмжлэл '
        return line

    @api.model
    def get_initial_balance(self, company_id, account_ids, date_start, target_move):
        ''' Тухайн дансны тайлант хугацааны эхний үлдэгдлийг олно.
        '''
        where = " "
        join = " "
        select = " "
        sub_select = " "
        group_by = " "
        order = " "

        if target_move == 'posted':
            where += "AND m.state = 'posted'"
        if self.env.context.get('journal_ids', False):
            journal_ids = self.env.context.get('journal_ids', False)
            sub_select += ", ml.journal_id AS journal_id "
            select += ", j.id AS jid, j.name AS jname "
            join += "LEFT JOIN account_journal j ON (j.id = mv.journal_id) "
            group_by += ", j.id, j.name "
            where += ' AND ml.journal_id in ('+','.join(map(str, journal_ids))+') '

        if self.env.context.get('partner_ids', False):
            partner_ids = self.env.context.get('partner_ids', False)
            sub_select += ", ml.partner_id AS partner_id "
            select += ", p.id AS pid, p.name AS pname "
            join += "LEFT JOIN res_partner p ON (p.id = mv.partner_id) "
            group_by += ", p.id, p.name "
            where += ' AND ml.partner_id in ('+','.join(map(str, partner_ids))+') '

        if self.env.context.get('group_account', False):
            select += ", at.id AS atid, at.code AS atcode , at.name AS atname "
            join += "LEFT JOIN account_account_type at ON (at.id = aa.user_type_id) "
            group_by += ", at.id, at.code, at.name "
            order += " at.code, at.name, "

        self.env.cr.execute("SELECT mv.account_id AS account_id, aa.code AS code, aa.name AS name, cur.id AS currency_id, cur.name AS currency, "
                                    "sum(mv.debit) AS start_debit, sum(mv.credit) AS start_credit, "
                                    "sum(mv.cur_debit) AS cur_start_debit, sum(mv.cur_credit) AS cur_start_credit, "
                                    "0 AS debit, 0 AS credit, 0 AS cur_debit, 0 AS cur_credit " + select + " "
                             "FROM (SELECT  ml.account_id AS account_id, ml.debit AS debit, "
                                            "ml.credit AS credit, "
                                            "CASE WHEN ml.amount_currency > 0 "
                                            "THEN ml.amount_currency ELSE 0 END AS cur_debit, "
                                            "CASE WHEN ml.amount_currency < 0 "
                                            "THEN abs(ml.amount_currency) ELSE 0 END AS cur_credit " + sub_select + " "
                                    "FROM account_move_line ml "
                                    "LEFT JOIN account_move m ON (ml.move_id = m.id) "
                                    "WHERE ml.date < %s AND ml.account_id in %s AND ml.company_id = %s " + where + ") AS mv "
                             "LEFT JOIN account_account aa ON (mv.account_id = aa.id) "  +
                             "LEFT JOIN res_currency cur ON (aa.currency_id = cur.id) " + join + ""
                             "GROUP BY mv.account_id, aa.code, aa.name, cur.id, cur.name " + group_by + ""
                             "ORDER BY " + order + "aa.code, aa.name ",
                             (date_start, tuple(account_ids), company_id))
        return self.env.cr.dictfetchall()

    @api.model
    def get_balance(self, company_id, account_ids, date_start, date_stop, target_move, without_profit_revenue=False):
        ''' Тухайн дансны тайлант хугацааны хоорондох дүнг олно.
        '''
        where = " "
        join = " "
        select = " "
        sub_select = " "
        group_by = " "

        if self.env.context.get('order_by', False):
            order = self.env.context.get('order_by', False)
        else:
            order = " aa.code, aa.name "
        # check period journal
        if without_profit_revenue:
            company = self.env['res.company'].browse(company_id)
            if not company.period_journal_id:
                raise ValidationError(_('Please configure period journal on account settings.'))
            else:
                where += 'AND ml.journal_id != %s ' % company.period_journal_id.id

        if target_move == 'posted':
            where += "AND m.state = 'posted'"

        if account_ids:
            where += ' AND ml.account_id in (' + ','.join(map(str, account_ids)) + ') '

        if self.env.context.get('journal_ids', False):
            journal_ids = self.env.context.get('journal_ids', False)
            sub_select += ", ml.journal_id AS journal_id "
            select += ", j.id AS jid, j.name AS jname "
            join += "LEFT JOIN account_journal j ON (j.id = mv.journal_id) "
            group_by += ", j.id, j.name "
            where += ' AND ml.journal_id in ('+','.join(map(str, journal_ids))+') '

        if self.env.context.get('partner_ids', False):
            partner_ids = self.env.context.get('partner_ids', False)
            sub_select += ", ml.partner_id AS partner_id "
            select += ", p.id AS pid, p.name AS pname "
            join += "LEFT JOIN res_partner p ON (p.id = mv.partner_id) "
            group_by += ", p.id, p.name "
            where += ' AND ml.partner_id in ('+','.join(map(str, partner_ids))+') '

        if self.env.context.get('group_account', False):
            select += ", at.id AS atid, at.code AS atcode , at.name AS atname "
            join += "LEFT JOIN account_account_type at ON (at.id = aa.user_type_id) "
            group_by += ", at.id, at.code, at.name "
            order += " ,at.code, at.name "

        self.env.cr.execute("SELECT mv.account_id AS account_id, aa.code AS code, aa.name AS name, "
                                    "aa.internal_type AS itype, cur.id AS currency_id, cur.name AS currency, "
                                    "sum(mv.debit) AS debit, sum(mv.credit) AS credit, "
                                    "sum(mv.cur_debit) AS cur_debit, sum(mv.cur_credit) AS cur_credit, "
                                    "0 AS start_debit, 0 AS start_credit, 0 AS cur_start_debit, 0 AS cur_start_credit " + select + " "
                            "FROM  (SELECT  ml.account_id AS account_id, ml.debit AS debit, "
                                            "ml.credit AS credit, "
                                            "CASE WHEN ml.amount_currency > 0 "
                                            "THEN ml.amount_currency ELSE 0 END AS cur_debit, "
                                            "CASE WHEN ml.amount_currency < 0 "
                                            "THEN abs(ml.amount_currency) ELSE 0 END AS cur_credit " + sub_select + " "
                                    "FROM account_move_line ml "
                                    "LEFT JOIN account_move m ON (ml.move_id = m.id) "
                                    "WHERE ml.date BETWEEN %s AND %s AND ml.company_id = %s " + where + " ) AS mv "
                             "LEFT JOIN account_account aa ON (mv.account_id = aa.id) " +
                             "LEFT JOIN res_currency cur ON (aa.currency_id = cur.id ) "+ join + ""
                             "GROUP BY mv.account_id, aa.code, aa.name, aa.internal_type, cur.id, cur.name " + group_by + ""
                             "ORDER BY " + order + " ",
                             (date_start, date_stop, company_id))
        return self.env.cr.dictfetchall()

    @api.model
    def get_all_balance(self, company_id, account_ids, date_start, date_stop, target_move):
        ''' Тухайн дансны эхний үлдэгдэл болон тайлант хугацааны хоорондох дүнг олно.
        '''
        where = " "
        join = " "
        select = " "
        sub_select = " "
        sub_join = " "
        group_by = " "
        if self.env.context.get('order_by', False):
            order_by = self.env.context.get('order_by')
        if not self.env.context.get('order_by', False):
            order_by = "aa.code, aa.name "
        if target_move == 'posted':
            where += "AND m.state = 'posted'"
        if self.env.context.get('journal_ids', False):
            journal_ids = self.env.context.get('journal_ids', False)
            sub_select += ", ml.journal_id AS journal_id "
            select += ", j.id AS jid, j.name AS jname "
            join += "LEFT JOIN account_journal j ON (j.id = mv.journal_id) "
            group_by += ", j.id, j.name "
            where += ' AND ml.journal_id in ('+','.join(map(str, journal_ids))+') '

        if self.env.context.get('partner_ids', False):
            partner_ids = self.env.context.get('partner_ids', False)
            sub_select += ", ml.partner_id AS partner_id "
            select += ", p.id AS pid, p.ref AS pcode, p.name AS pname "
            join += "LEFT JOIN res_partner p ON (p.id = mv.partner_id) "
            group_by += ", p.id, p.name "
            where += ' AND ml.partner_id in ('+','.join(map(str, partner_ids))+') '
            if not self.env.context.get('order_by', False):
                order_by += "p.name "
        # Харилцагчын товчоо тайланг борлуулалтын ажилтнаар бүлэглэсэн утга авах
        if self.env.context.get('salesman_ids',False):
            salesman_ids = self.env.context.get('salesman_ids', False)
            sub_select += ", ai.user_id AS salesman "
            select += ", COALESCE(mv.salesman, 0) AS salesman "
            join += " LEFT JOIN res_users ru on (ru.id = mv.salesman) "
            sub_join += " LEFT JOIN account_invoice ai on (ml.invoice_id = ai.id) "
            group_by += ", ru.id, mv.salesman "
            where += ' AND ai.user_id in ('+','.join(map(str, salesman_ids))+') '

        if self.env.context.get('group_account', False):
            select += ", at.id AS atid, at.code AS atcode , at.name AS atname "
            join += "LEFT JOIN account_account_type at ON (at.id = aa.user_type_id) "
            group_by += ", at.id, at.code, at.name "
            order_by = " at.code, at.name, aa.code, aa.name"

        if self.env.context.get('without_profit_revenue', False):
            company = self.env['res.company'].browse(company_id)
            if not company.period_journal_id:
                raise ValidationError(_('Please configure period journal on account settings.'))
            else:
                where += 'AND ml.journal_id != %s ' % company.period_journal_id.id

        if self.env.context.get('byline', False):
            select += " , mv.line_id "
            sub_select += ", ml.id AS line_id "
            group_by += ", mv.line_id "

        self.env.cr.execute("SELECT mv.account_id AS account_id, COALESCE(aa.code, '') AS acode, "
                                    "COALESCE(aa.name, '') AS aname, rc.id AS cid, COALESCE(rc.name, '') AS cname, "
                                    "sum(mv.start_balance) AS start_balance, sum(mv.cur_start_balance) AS cur_start_balance, "
                                    "sum(mv.debit) AS debit, sum(mv.credit) AS credit, COALESCE(aa.internal_type, '') AS atype, "
                                    "sum(mv.cur_debit) AS cur_debit, sum(mv.cur_credit) AS cur_credit " + select + " "
                            "FROM  (SELECT  ml.account_id AS account_id, "
                                            "CASE WHEN (ml.debit > 0 OR ml.credit > 0) AND ml.date < %s "
                                            "THEN COALESCE(ml.debit - ml.credit, 0) ELSE 0 END AS start_balance, "
                                            "CASE WHEN ml.amount_currency != 0  and ml.date < %s "
                                            "THEN COALESCE(amount_currency, 0) ELSE 0 END AS cur_start_balance, "
                                            "CASE WHEN ml.debit > 0 AND ml.date BETWEEN %s AND %s"
                                            "THEN COALESCE(ml.debit,0) ELSE 0 END AS debit, "
                                            "CASE WHEN ml.credit > 0 AND ml.date BETWEEN %s AND %s "
                                            "THEN COALESCE(ml.credit,0) ELSE 0 END AS credit, "
                                            "CASE WHEN ml.amount_currency > 0  AND ml.date BETWEEN %s AND %s "
                                            "THEN ml.amount_currency ELSE 0 END AS cur_debit, "
                                            "CASE WHEN ml.amount_currency < 0  AND ml.date BETWEEN %s AND %s "
                                            "THEN abs(ml.amount_currency) ELSE 0 END AS cur_credit " + sub_select + " "
                                    "FROM account_move_line ml "
                                    "LEFT JOIN account_move m ON (ml.move_id = m.id) " + sub_join + ""
                                    "WHERE ml.account_id in %s " + where + "AND ml.company_id = %s) AS mv "
                            "LEFT JOIN account_account aa ON (mv.account_id = aa.id) "
                            "LEFT JOIN res_currency rc ON (aa.currency_id = rc.id) " + join + ""                                                                   
                            "GROUP BY mv.account_id, aa.code, aa.name, rc.id, rc.name, aa.internal_type " + group_by + " "   
                            "HAVING sum(mv.start_balance)::decimal(16,2) != 0 or sum(mv.debit)::decimal(16,2) > 0 or sum(mv.credit)::decimal(16,2) > 0 "   
                                    "or sum(mv.cur_start_balance)::decimal(16,2) != 0 or sum(mv.cur_debit)::decimal(16,2) > 0 or sum(mv.cur_debit)::decimal(16,2) > 0 "                                                                                            
                            "ORDER BY " + order_by,
                             (date_start, date_start, date_start, date_stop, date_start, date_stop, date_start,
                              date_stop, date_start, date_stop, tuple(account_ids), company_id))
        return self.env.cr.dictfetchall()

    @api.model
    def get_partner_balance(self, company_id, account_ids, date_start, date_stop, target_move):
        ''' Тухайн дансны эхний үлдэгдэл болон тайлант хугацааны хоорондох дүнг олно.
        '''
        where = " "
        join = " "
        sub_join = " "
        select = " "
        sub_select = " "
        group_by = " "
        if self.env.context.get('order_by', False):
            order_by = self.env.context.get('order_by')
        if target_move == 'posted':
            where += "AND m.state = 'posted'"

        if self.env.context.get('partner_ids', False):
            partner_ids = self.env.context.get('partner_ids', False)
            sub_select += ", ml.partner_id AS partner_id "
            select += ", p.id AS pid, p.name AS pname "
            join += "LEFT JOIN res_partner p ON (p.id = mv.partner_id) "
            group_by += ", p.id, p.name "
            where += ' AND ml.partner_id in ('+','.join(map(str, partner_ids))+') '
            if not self.env.context.get('order_by', False):
                order_by += ", p.name "
        # Харилцагчын баланс тайланд борлуулалтын ажилтнаар бүлэглэх үед утга олгох
        if self.env.context.get('salesman_ids',False):
            salesman_ids = self.env.context.get('salesman_ids', False)
            sub_select += ", ai.user_id as salesman_id "
            select += ", mv.invoice as invoice, mv.salesman_id as salesman_id "
            join += " LEFT JOIN res_users ru on (ru.id = mv.salesman_id) "
            sub_join += " LEFT JOIN account_invoice ai on (ml.invoice_id = ai.id) "
            group_by += ", ru.id, mv.salesman_id, mv.invoice "
            where += ' AND ai.user_id in ('+','.join(map(str, salesman_ids))+') '

        self.env.cr.execute("SELECT mv.account_id AS account_id, COALESCE(aa.code, '') AS acode, "
                                    "COALESCE(aa.name, '') AS aname, COALESCE(rc.name, '') AS cname, "
                                    "mv.start_balance AS start_balance, mv.cur_start_balance AS cur_start_balance, "
                                    "mv.debit AS debit, mv.credit AS credit, COALESCE(aa.internal_type, '') AS atype, "
                                    "mv.cur_debit AS cur_debit, mv.cur_credit AS cur_credit, mv.name AS name, mv.ml_id AS ml_id, mv.ref AS ref, mv.ml_date as ml_date, mv.rec AS rec, mv.cid AS cid, mv.invoice AS invoice, "
                                    "mv.currency_rate as currency_rate"
                                                        + select + ""
                            "FROM     (SELECT  ml.account_id AS account_id, "
                                                "CASE WHEN (ml.debit > 0 OR ml.credit > 0) AND ml.date < %s "
                                                "THEN COALESCE(ml.debit - ml.credit, 0) ELSE 0 END AS start_balance, "
                                                "CASE WHEN ml.amount_currency != 0  and ml.date < %s "
                                                "THEN COALESCE(amount_currency, 0) ELSE 0 END AS cur_start_balance, "
                                                "CASE WHEN ml.debit > 0 AND m.date BETWEEN %s AND %s"
                                                "THEN COALESCE(ml.debit,0) ELSE 0 END AS debit, "
                                                "CASE WHEN ml.credit > 0 AND m.date BETWEEN %s AND %s "
                                                "THEN COALESCE(ml.credit,0) ELSE 0 END AS credit, "
                                                "CASE WHEN ml.amount_currency > 0  AND m.date BETWEEN %s AND %s "
                                                "THEN ml.amount_currency ELSE 0 END AS cur_debit, "
                                                "CASE WHEN ml.amount_currency < 0  AND m.date BETWEEN %s AND %s "
                                                "THEN abs(ml.amount_currency) ELSE 0 END AS cur_credit "
                                                ", ml.name AS name , ml.id as ml_id, ml.ref As ref, ml.date AS ml_date, ml.currency_rate AS currency_rate, ml.full_reconcile_id AS rec, ml.currency_id AS cid, ml.invoice_id AS invoice"
                                                        + sub_select + ""
                                        "FROM account_move_line ml "
                                        "LEFT JOIN account_move m ON (ml.move_id = m.id) " + sub_join + ""
                                        "WHERE ml.account_id in %s " + where + ""
                                              " AND ml.company_id = %s) AS mv "
                            "LEFT JOIN account_account aa ON (mv.account_id = aa.id) "
                            "LEFT JOIN res_currency rc ON (aa.currency_id = rc.id) " + join + ""
#                             "GROUP BY mv.account_id, aa.code, aa.name, rc.name, aa.internal_type " + group_by + ""
                            "ORDER BY " + order_by,
                             (date_start, date_start, date_start, date_stop, date_start, date_stop, date_start,
                              date_stop, date_start, date_stop, tuple(account_ids), company_id))
        return self.env.cr.dictfetchall()

    @api.model
    def get_all_partner_balance(self, company_id, account_ids, date_start, date_stop, target_move):
        ''' Тухайн дансны эхний үлдэгдэл болон тайлант хугацааны хоорондох дүнг олно.
        '''
        join = " "
        sub_join = " "
        select = " "
        sub_select = " "
        group_by = " "
        join_st = " "
        join_on = " "
        if self.env.context.get('order_by', False):
            order_by = self.env.context.get('order_by')
        where = 'AND ml.company_id = ' + str(company_id) + ''
        if account_ids:
            where += ' AND ml.account_id in (' + ','.join(map(str, account_ids)) + ') '
        if target_move == 'posted':
            where += "AND m.state = 'posted'"
        # Харилцагчын баланс тайланд борлуулалтын харилцагчаар бүлэглэх үед утга олгох
        if self.env.context.get('partner_ids', False):
            partner_ids = self.env.context.get('partner_ids', False)
            sub_select += ", ml.partner_id AS partner_id "
            select += ", p.id AS pid, p.name AS pname, COALESCE(p.credit_limit, 0) AS limit "
            join += "LEFT JOIN res_partner p ON (p.id = aml.partner_id) "
            join_st += "AND st.partner_id = aml.partner_id "
            join_on += "AND mv.partner_id = aml.partner_id "
            group_by += ", ml.partner_id "
            where += ' AND ml.partner_id in (' + ','.join(map(str, partner_ids)) + ') '
            if not self.env.context.get('order_by', False):
                order_by += ", p.name "
        # Харилцагчын баланс тайланд борлуулалтын ажилтнаар бүлэглэх үед утга олгох
        if self.env.context.get('salesman_ids', False):
            salesman_ids = self.env.context.get('salesman_ids', False)
            sub_select += ", ai.user_id as salesman_id "
            select += ", mv.invoice as invoice, COALESCE(mv.salesman_id, st.salesman_id) AS salesman "
            join += " LEFT JOIN res_users ru on (ru.id = aml.salesman_id) "
            sub_join += " LEFT JOIN account_invoice ai on (ml.invoice_id = ai.id) "
            join_st += "AND st.salesman_id = aml.salesman_id "
            join_on += "AND mv.salesman_id = aml.salesman_id "
            group_by += ", ai.user_id "
            where += ' AND ai.user_id in (' + ','.join(map(str, salesman_ids)) + ') '

        self.env.cr.execute("SELECT aml.account_id AS account_id, COALESCE(aa.code, '') AS acode, "
                            "COALESCE(aa.name, '') AS aname, COALESCE(rc.name, '') AS cname, "
                            "COALESCE(st.start_balance, 0) AS start_balance, COALESCE(st.cur_start_balance, 0) AS cur_start_balance, "
                            "COALESCE(mv.debit, 0) AS debit, COALESCE(mv.credit, 0) AS credit, COALESCE(aa.internal_type, '') AS atype, "
                            "COALESCE(mv.cur_debit, 0) AS cur_debit, COALESCE(mv.cur_credit, 0) AS cur_credit, COALESCE(mv.name, '') AS name,"
                            "COALESCE(mv.ml_id, 0) AS ml_id, COALESCE(mv.ref, '') AS ref, COALESCE(mv.mname, '') AS mname, mv.ml_date as ml_date, "
                            "mv.rec AS rec, fr.name AS frname, mv.cid AS cid, mv.invoice AS invoice, mv.currency_rate as currency_rate"
                            + select + ""
                            "FROM (SELECT ml.account_id AS account_id " + sub_select + ""
                                        "FROM account_move_line ml "
                                        "LEFT JOIN account_move m ON (ml.move_id = m.id) " + sub_join + ""                                                                  
                                        "WHERE ml.date <= %s " + where + " "
                                        "GROUP BY ml.account_id " + group_by + ") AS aml "
                            "LEFT JOIN  (SELECT ml.account_id, sum(COALESCE(ml.debit, 0) - COALESCE(ml.credit, 0))::decimal(16,2) AS start_balance, "
                                                "SUM(COALESCE(amount_currency, 0))::decimal(16,2) AS cur_start_balance " + sub_select + ""
                                        "FROM account_move_line ml "
                                        "LEFT JOIN account_move m ON (ml.move_id = m.id) " + sub_join + ""                                                                  
                                        "WHERE ml.date < %s " + where + " "
                                        "GROUP BY ml.account_id " + group_by + ") AS st ON (aml.account_id = st.account_id" + join_st + ") " 
                            "LEFT JOIN  (SELECT  ml.account_id AS account_id, COALESCE(ml.debit, 0)::decimal(16,2) AS debit, COALESCE(ml.credit,0)::decimal(16,2) AS credit, "
                                               "CASE WHEN ml.amount_currency > 0 "
                                               "THEN ml.amount_currency::decimal(16,2) ELSE 0 END AS cur_debit, "
                                               "CASE WHEN ml.amount_currency < 0 "
                                               "THEN abs(ml.amount_currency)::decimal(16,2) ELSE 0 END AS cur_credit, "
                                               "m.name AS mname, ml.name AS name , ml.id as ml_id, ml.ref As ref, ml.date AS ml_date, "
                                               "ml.currency_rate AS currency_rate, ml.full_reconcile_id AS rec, ml.currency_id AS cid, "
                                               "ml.invoice_id AS invoice" + sub_select + ""
                                       "FROM account_move_line ml "
                                       "LEFT JOIN account_move m ON (ml.move_id = m.id) " + sub_join + ""
                                       "WHERE ml.date BETWEEN %s AND %s " + where + ") AS mv ON (aml.account_id = mv.account_id" + join_on + ") "                                                               
                          "LEFT JOIN account_account aa ON (aml.account_id = aa.id) "
                          "LEFT JOIN res_currency rc ON (aa.currency_id = rc.id) " 
                          "LEFT JOIN account_full_reconcile fr ON (fr.id = mv.rec) "+ join + ""                                                                   
                          "ORDER BY " + order_by, (date_stop, date_start, date_start, date_stop))
        return self.env.cr.dictfetchall()

    def _prepare_analytic_line(self):
        res = super(AccountMoveLine, self)._prepare_analytic_line()
        res[0].update({'cashflow_id': self.cashflow_id.id})
        return res

