# -*- coding: utf-8 -*-
# Part of OdERP. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from odoo.exceptions import UserError
from datetime import datetime


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    payment_type = fields.Selection(selection_add=[('currency', 'Currency exchange')])
    in_amount = fields.Monetary(string='Receive Amount')
    in_currency_id = fields.Many2one('res.currency', string='Currency', default=lambda self: self.env.user.company_id.currency_id)
    money_out_id = fields.Many2one('account.bank.statement', string='Money out order')
    money_in_id = fields.Many2one('account.bank.statement', string='Money in order')
    type_of_exchange_id = fields.Many2one('account.cashflow.type', string='Cashflow type of exchange', default=lambda self: self.env.user.company_id.type_of_exchange_id, ondelete='restrict')
    journal_of_currency_exchange_id = fields.Many2one('account.journal', string='Journal of exchange equations', default=lambda self: self.env.user.company_id.currency_exchange_journal_id)
    type_of_currency_id = fields.Many2one('account.cashflow.type', string='Cashflow type of currency', default=lambda self: self.env.user.company_id.type_of_currency_id, ondelete='restrict')
    middle_account_id = fields.Many2one('account.account', string='Transfer account', default=lambda self: self.env.user.company_id.transfer_account_id)
    exchange_move_id = fields.Many2one('account.move', 'Exchange move')
    bank_account_id = fields.Many2one('res.partner.bank', string='Bank account')
    partner_id = fields.Many2one('res.partner', 'Partner')
    reconcile = fields.Boolean(related='middle_account_id.reconcile', string='Reconcile')
    open_statement_id = fields.Many2one('account.bank.statement', string='Open statement')
    cashflow_id = fields.Many2one('account.cashflow.type', string='Cashflow type')
    is_automatically_statement = fields.Selection([('no', 'No'), ('yes', 'Yes')], string='Automatically write on the account bank statement', default=lambda self: self.env.user.company_id.is_bank_statement_automatically)

    @api.onchange('destination_journal_id')
    def _onchange_destination_journal_id(self):
        # Тухайн сонгосон мөнгө орох журналын нээлттэй орох ордерыг буцаах
        if self.destination_journal_id:
            self.money_in_id = False
            self.in_currency_id = self.destination_journal_id.currency_id or self.company_id.currency_id
            return {'domain': {'money_in_id': [('journal_id', '=', self.destination_journal_id.id), ('state', '=', 'open')]}}
        return {}
    
    @api.onchange('middle_account_id')
    def _onchange_middle_account_id(self):
        # Тухайн сонгосон харьцах данс нь тулгах данс биш бол харилцагчын утгыг хоослоно.
        if not self.middle_account_id.reconcile:
            self.partner_id = ''

    @api.onchange('journal_id')
    def _onchange_journal(self):
        # Тухайн сонгосон мөнгө Гарах журналын нээлттэй мөнгө гарах ордерыг буцаах
        if self.journal_id:
            self.money_out_id = False
            super(AccountPayment, self)._onchange_journal(),
            return {'domain': {'money_out_id': [('journal_id', '=', self.journal_id.id), ('state', '=', 'open')]}}
        return {}

    @api.multi
    def button_currency_equalization(self):
        # Ханшийн тэгшитгэлийг харуулах
        account_move_lines = self.env['account.move.line'].search(
            [('statement_id', 'in', [self.money_in_id.id, self.money_out_id.id])])
        currency_equals = []
        for move_line in account_move_lines:
            currency_equal = self.env['account.currency.equalization.history'].search(
                [('currency_move_line_id', '=', move_line.id)]).ids
            if currency_equal:
                currency_equals += currency_equal

        return {
            'name': _('Journal Items'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.currency.equalization.history',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', currency_equals)],
        }

    @api.multi
    def button_statement_journal_entries(self):
        # Холбоотой ажил гүйлгээг төлбөр дээр харуулах
        if self.money_out_id and self.money_in_id:
            statement_id = [self.money_in_id.id, self.money_out_id.id]
            return {
                'name': _('Journal Items'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.move.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'domain': [('statement_id', 'in', statement_id)],
            }

    def check_currency_equalization(self, move_line_id):
        # Ханшийн тэгшитгэлийн бичилтийг устгаж, түүнээс хойш ханш тэгшитгэсэн байвал анхааруулга өгнө.
        currency_equal = self.env['account.currency.equalization.history'].search(
            [('currency_move_line_id', '=', move_line_id.id)], limit=1)
        if currency_equal:
            if currency_equal.date > str(datetime.now()):
                raise UserError(_('The exchange rate has been adjusted since that time'))
            else:
                currency_equal.sudo().unlink()

    def check_account_partial_reconcile(self, move_line_id):
        # Тулгалт байгаа эсэхийг шалгах
        debit_partial_reconcile = self.env['account.partial.reconcile'].search(
            [('debit_move_id', '=', move_line_id.id)])
        credit_partial_reconcile = self.env['account.partial.reconcile'].search(
            [('debit_move_id', '=', move_line_id.id)])
        if debit_partial_reconcile or credit_partial_reconcile:
            raise UserError(_('You cannot do this cancel on a reconciled entry'))

    def unlink_bank_statement(self, statement_id, payment_id=False):
        context = self._context.copy() or {}
        context['unlink_bank_statement_from_payment'] = True
        # Касс, харилцахын мөрүүд автоматаар устгагдаж, шинэ төлөвт орно.
        statement_id.button_draft()
        # Холбоотой ажил гүйлгээг давхар устгана
        for line in statement_id.line_ids:
            # Төлбөр цуцлах үед тухайн төлбөрийн бичигдсэн банкны хуулгаас холбоотой мөрийг цуцалдаг болгов
            if line.payment_id.id == payment_id:
                line.with_context(context).button_cancel_reconciliation()
                if line.journal_entry_ids:
                    for entry in line.journal_entry_ids:
                        self.check_account_partial_reconcile(entry)
                        self.check_currency_equalization(entry)
                        entry.button_cancel()
                if line.related_statement_line_id.journal_entry_ids:
                    line.with_context(context).button_cancel_reconciliation()
                    for entry in line.related_statement_line_id.journal_entry_ids:
                        self.check_currency_equalization(entry)
                        self.check_account_partial_reconcile(entry)
                        entry.button_cancel()
                line.unlink()

    @api.multi
    def cancel(self):
        statement_line_obj = self.env['account.bank.statement.line']
        for obj in self:
            if obj.payment_type in ('transfer', 'currency'):
                if obj.exchange_move_id:
                    obj.exchange_move_id.button_cancel()
                    query = """DELETE FROM account_move_line WHERE move_id = %s  """ % (obj.exchange_move_id.id)
                    self.env.cr.execute(query)
                    query = """DELETE FROM account_move WHERE id = %s  """ % (obj.exchange_move_id.id)
                    self.env.cr.execute(query)
                    # Цуцлах товч дарахад холбоотой касс, харилцахын мөрүүд автоматаар устгагдаж,хуулга нь "Шинэ" төлөвт орно.
                self.unlink_bank_statement(obj.money_out_id, payment_id=obj.id)
                self.unlink_bank_statement(obj.money_in_id, payment_id=obj.id)
        res = super(AccountPayment, self).cancel()
        self.write({'move_name': False})
        return res

    def get_bank_statement_from_values(self):
        return {
            'name': self.communication,
             'amount': -self.amount,
             'account_id': self.middle_account_id.id,
             'cashflow_id': self.type_of_exchange_id.id,
             'date': self.payment_date,
             'statement_id': self.money_out_id.id,
             'payment_id': self.id,
             'partner_id': self.partner_id.id
        }

    def get_bank_statement_to_values(self):
        return {
            'name': self.communication,
            'account_id': self.middle_account_id.id,
            'amount': self.amount if self.payment_type == 'transfer' else self.in_amount,
            'cashflow_id': self.type_of_exchange_id.id,
            'date': self.payment_date,
            'statement_id': self.money_in_id.id,
            'payment_id': self.id,
            'partner_id': self.partner_id.id
        }

    @api.multi
    def post(self):
        statement_line_obj = self.env['account.bank.statement.line']
        account_move_obj = self.env['account.move']
        account_move_line_obj = self.env['account.move.line']
        for obj in self:
            # Төлбөрийн төрөл Дотооод шилжүүлэг эсвэл Валют солих байх үед хийгдэх үйлдэлүүд
            if obj.payment_type in ('transfer', 'currency'):
                if obj.journal_id.id == obj.destination_journal_id.id:
                    raise UserError(_('You cannot create transaction between same journal!'))
                if obj.payment_type == 'transfer':
                    if obj.journal_id.currency_id.id != obj.destination_journal_id.currency_id.id:
                        raise UserError(_('You cannot create transaction between difference currency!'))
                    sequence_code = 'account.payment.transfer'

                if obj.payment_type == 'transfer':
                    prev_stmt_lines = statement_line_obj.search([('payment_id', '=', obj.id)])
                    for bank_smt_line in prev_stmt_lines:
                        bank_smt_line.button_cancel_reconciliation()
                        bank_smt_line.related_statement_line_id.button_cancel_reconciliation()
                        bank_smt_line.related_statement_line_id = False
                        bank_smt_line.unlink()
                from_vals = obj.get_bank_statement_from_values()
                # Мөнгө гарах ордерийн мөр үүсгэх
                line_from_id = self.env['account.bank.statement.line'].create(from_vals)
                # Мөнгө орох ордерийн мөр үүсгэх
                line_to_id = statement_line_obj.create(obj.get_bank_statement_to_values())
                # Ордеруудыг холбож батлах
                line_from_id.write({'related_statement_line_id': line_to_id.id})
                line_to_id.write({'related_statement_line_id': line_from_id.id})
                line_from_id.confirm_button()
                line_to_id.confirm_button()

                # Төлбөрийн төрөл Валют солих байх үед хийгдэх үйлдэлүүд
                if obj.payment_type == 'currency':
                    if obj.currency_id.id == obj.in_currency_id.id:
                        raise UserError(_('You cannot create transaction between same currency! Create Transfer'))
                    sequence_code = 'currency.exchange.sequence'
                    # Мөнгө гарах ба орох ордерийн мөрүүдийн ажил гүйлгээг олох
                    from_move = account_move_obj.search([('statement_line_id', '=', line_from_id.id)])
                    to_move = account_move_obj.search([('statement_line_id', '=', line_to_id.id)])
                    from_move_line = account_move_line_obj.search([('move_id', '=', from_move.id)])
                    to_move_line = account_move_line_obj.search([('move_id', '=', to_move.id)])
                    # үүссэн бичилтийн зөрүүг олох
                    amount_diff = from_move_line[0].debit - to_move_line[0].credit
                    # Зөрүү үүссэн байвал ханшийн зөрүүний бичилт бичих хэсэг
                    if not obj.company_id.currency_id.is_zero(amount_diff):
                        debit1, credit1, debit2, credit2 = 0, 0, 0, 0
                        if amount_diff > 0:
                            credit1 = debit2 = amount_diff
                            name = u'%s - валютын ханшийн зөрүүний хэрэгжсэн гарз' % (from_move_line[0].ref,)
                            exchange_account_id = obj.company_id.account_to_id.id
                        else:
                            # Төгрөгөөр гарсан мөнгөнөөс валютаарх орж буй мөнгө их байгаа нь ханшийн зөрүүний ашиг хүлээж байна.
                            debit1 = credit2 = -amount_diff
                            name = u'%s - валютын ханшийн зөрүүний хэрэгжсэн олз' % (to_move_line[0].ref,)
                            exchange_account_id = obj.company_id.account_from_id.id
                        # Ханшийн зөрүүний бичилт үүсгэж батлах
                        move_id = account_move_obj.create({
                            'journal_id': obj.journal_of_currency_exchange_id.id,
                            'date': obj.payment_date,
                            'narration': u'%s /ханш тэгшитгэл' % (obj.money_out_id.name,),
                            'line_ids': [(0, 0, {
                                'name': name,
                                'debit': debit1,
                                'credit': credit1,
                                'account_id': obj.middle_account_id.id,
                                'company_id': obj.company_id.id,
                                'amount_currency': 0,
                                'currency_id': False,
                                'date': obj.payment_date,
                                'journal_id': obj.journal_of_currency_exchange_id.id,
                                'related_statement_line_ids': [(6, 0, [line_from_id.id, line_to_id.id])],
                            }), (0, 0, {
                                'name': name,
                                'debit': debit2,
                                'credit': credit2,
                                'account_id': exchange_account_id,
                                'cashflow_id': obj.type_of_currency_id.id,
                                'company_id': obj.company_id.id,
                                'amount_currency': 0,
                                'currency_id': False,
                                'date': obj.payment_date,
                                'journal_id': obj.journal_of_currency_exchange_id.id,
                                'related_statement_line_ids': [(6, 0, [line_from_id.id, line_to_id.id])],
                            })]
                        })
                        move_id.post()
                        # Мөнгө орох гарах ордерийн үүссэн мөрүүдэд үүссэн ханшийн зөрүүний бичилтийг холбох
                        line_from_id.write({'currency_exchange_move_id': move_id.id})
                        line_to_id.write({'currency_exchange_move_id': move_id.id})
                        self.exchange_move_id = move_id.id
                obj.write({'name': self.env['ir.sequence'].with_context(ir_sequence_date=obj.payment_date).next_by_code(sequence_code),
                           'state': 'posted'})
                return True
            else:
                # Нэхэмжлэлээс төлбөр бүртгэхэд касс, харилцахын хуулга дээр автоматаар бүртгэх үйлдэлүүд
                if self.open_statement_id:
                    statement_line_obj.create({
                        'name': obj.communication,
                        'account_id': obj.invoice_ids[0].account_id.id,
                        'amount': obj.amount,  # if obj.payment_type == 'transfer' else obj.in_amount,
                        'cashflow_id': obj.cashflow_id.id,
                        'date': obj.payment_date,
                        'statement_id': obj.open_statement_id.id,
                        'payment_id': obj.id,
                        'partner_id': obj.partner_id.id
                    })
                    self.open_statement_id.check_confirm_bank()
                #Төлбөрийн төрөл Дотооод шилжүүлэг эсвэл Валют солихоос бусад тохиолдолд хийгдэх үйлдэлүүд
                return super(AccountPayment, self).post()

    @api.multi
    def do_print_picking(self):
        return self.env['report'].get_action(self, 'l10n_mn_account.payment_report_view')

    @api.multi
    def unlink(self):
        statement_line_obj = self.env['account.bank.statement.line']
        for this in self:
            if this.payment_type == 'currency':
                prev_stmt_lines = statement_line_obj.search([('payment_id', '=', this.id)])
                lines = [line.statement_id.name for line in prev_stmt_lines]
                if prev_stmt_lines:
                    raise UserError('There are related bank statement line to this payment (%s).' % ", ".join(lines))
        return super(AccountPayment, self).unlink()
