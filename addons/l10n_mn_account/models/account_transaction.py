# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.osv import expression


class AccountTransaction(models.Model):
    _name = "account.transaction"

    account_id = fields.Many2one('account.account', string='Account')
    name = fields.Char('Transaction', required=True)
    code = fields.Char('Code', required=False)
    active = fields.Boolean('Active', default=True)
    cashflow_id = fields.Many2one('account.cashflow.type', 'Cashflow Type', ondelete='restrict')

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('code', '=ilike', name + '%'), ('name', operator, name)]
            if operator in expression.NEGATIVE_TERM_OPERATORS:
                domain = ['&', '!'] + domain[1:]
        transactions = self.search(domain + args, limit=limit)
        return transactions.name_get()

    @api.multi
    @api.depends('name', 'code')
    def name_get(self):
        result = []
        for transaction in self:
            name = transaction.name
            if transaction.code:
                # str() функц нь coding: utf-8 гэж өгсөн ч алдаа заагаад байсан
                name = transaction.code + u' ' + transaction.name
            result.append((transaction.id, name))
        return result
