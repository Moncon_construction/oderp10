# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.osv import expression


class AccountCashflowType(models.Model):
    _name = "account.cashflow.type"
    _description = "Account Cashflow Type"

    @api.one
    @api.depends('children_ids')
    def _get_type(self):
        if self.children_ids:
            self.type = 'view'
            for child in self.children_ids:
                if child.children_ids:
                    child.type = 'view'
                else:
                    child.type = 'normal'
        else:
            self.type = 'normal'

    type = fields.Selection([('view', 'View'), ('normal', 'Normal')], 'Type', default='normal')
    code = fields.Char(string='Code')
    sequence = fields.Integer(string='Sequence', default=1)
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    name = fields.Char(string='Name', index=True, required=True, translate=True)
    parent_id = fields.Many2one('account.cashflow.type', 'Parent', domain=[('type', '=', 'view')], ondelete='restrict')
    children_ids = fields.One2many('account.cashflow.type', 'parent_id', 'Account Cashflow Value')
    report_cashflow_type = fields.Selection([('simple', 'Simple'),
                                             ('display', 'Display'),
                                             ('child_sum', 'Child Sum'),
                                             ('united', 'United'),
                                             ('initial', 'Initial Balance'),
                                             ('end', 'End Balance')], string='Report Type', default='simple', required=False)
    report_cashflow_ids = fields.Many2many('account.cashflow.type', 'account_cashflow_self_rel', 'account_cashflow_id', 'account_cashflow_type_id', 'Cashflows')
    minus_cashflow_ids = fields.Many2many('account.cashflow.type', 'account_cashflow_self_minus_rel', 'account_cashflow_id', 'account_cashflow_type_id', 'Minus Cashflows')
    value_of_amount_type = fields.Selection([('positive', 'Positive'), ('negative', 'Negative'), ('mixed', 'Mixed')], required=True)
    is_standart = fields.Boolean(string="Is standart")

    _sql_constraints = [ 
         ('account_cashflow_type_code_unique', 'unique(code,company_id)', 'Code must be unique per company!'),      
     ]
    
    @api.multi
    def name_get(self):
        result = []
        for cashflow in self:
            result.append((cashflow.id, "%s %s" % (cashflow.code or cashflow.type, cashflow.name or '')))
        return result
    
    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        args = args or []
        connector = '|'
        if operator in expression.NEGATIVE_TERM_OPERATORS:
            connector = '&'
        recs = self.search([connector, ('code', operator, name), ('name', operator, name)] + args, limit=limit)
        return recs.name_get()