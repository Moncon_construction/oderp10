# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError


class CashBox(models.TransientModel):
    _register = False
    
    # Сонгох данс нь Орлого, Зарлага, Авлага, Өглөг төрөлтэй байна
    def _domain_account(self):
        account_ids = self.env['account.account'].search([('internal_type', 'in', ['expense', 'income', 'receivable', 'payable'])])
        if account_ids:
            return [('id', 'in', account_ids.ids)]
        else:
            return False

    # Нээлтийн балансаас төгсгөлийн баланс ялгаатай үед 
    # касс батлахад автоматаар үүсэх ашиг/алдагдлын журналд зориулсан мөнгөн гүйлгээний төрөл
    #  Хэрэв Авлага, Өглөг данс сонгосон үед Харилцагчийг заавал сонгохыг шаардана
    cashflow_id = fields.Many2one('account.cashflow.type', 'Cashflow Type')
    partner_id = fields.Many2one('res.partner', 'Partner')
    account_id = fields.Many2one('account.account', 'Account', domain=_domain_account)
    account_type_name = fields.Selection(related='account_id.user_type_id.type', readonly=True)


class CashBoxIn(CashBox):
    _inherit = 'cash.box.in'

    @api.multi
    def _calculate_values_for_statement_line(self, record):
        if not record.journal_id.company_id.transfer_account_id:
            raise UserError(_("You should have defined an 'Internal Transfer Account' in your cash register's journal!"))
        return {
            'date': record.date,
            'statement_id': record.id,
            'journal_id': record.journal_id.id,
            'amount': self.amount or 0.0,
            'cashflow_id': self.cashflow_id.id,
            'partner_id': self.partner_id.id if self.partner_id.id else False,
            'account_id': self.account_id.id if self.account_id.id else record.journal_id.company_id.transfer_account_id.id,
            'ref': '%s' % (self.ref or ''),
            'name': self.name,
        }


class CashBoxOut(CashBox):
    _inherit = 'cash.box.out'

    @api.multi
    def _calculate_values_for_statement_line(self, record):
        if not record.journal_id.company_id.transfer_account_id:
            raise UserError(_("You should have defined an 'Internal Transfer Account' in your cash register's journal!"))
        amount = self.amount or 0.0
        return {
            'date': record.date,
            'statement_id': record.id,
            'journal_id': record.journal_id.id,
            'cashflow_id': self.cashflow_id.id,
            'amount':-amount if amount > 0.0 else amount,
            'partner_id': self.partner_id.id if self.partner_id.id else False,
            'account_id': self.account_id.id if self.account_id.id else record.journal_id.company_id.transfer_account_id.id,
            'name': self.name,
        }
