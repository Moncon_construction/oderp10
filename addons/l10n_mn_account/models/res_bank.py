from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class ResBank(models.Model):
    _inherit = 'res.bank'

    integrated_bank = fields.Selection([], 'Integrated bank')

    @api.constrains('integrated_bank')
    def _check_integrated_bank(self):
        for obj in self:
            if obj.integrated_bank:
                if obj.search([('integrated_bank', '=', obj.integrated_bank), ('id', '!=', obj.id)]):
                    raise ValidationError(_('Integrated bank field must only be chosen once: %s') % obj.integrated_bank)
