# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

STATEMENT_DATE_SELECTION = [('same', 'Bank statements are generated daily. The date of the bank statement line shall be the same as the date of the statement.'),
                            ('different', 'Bank statements are not generated every day. The date of the bank statement line may be different from the date of the statement.')]


class ResCompany(models.Model):
    _inherit = "res.company"

    cashflow_id = fields.Many2one('account.cashflow.type', string="Cashflow", help='That is cashflow')
    type_of_exchange_id = fields.Many2one('account.cashflow.type', string='Cashflow type of exchange', ondelete='restrict')  # Хөрвөлтийн мөнгөн гүйлгээний төрөл
    type_of_currency_id = fields.Many2one('account.cashflow.type', string='Cashflow type of currency', ondelete='restrict')  # Валютын мөнгөн гүйлгээний төрөл
    account_from_id = fields.Many2one('account.account', string='Currency Exchange Gain Account')  # Валютын ханшийн зөрүүний олзын данс
    account_to_id = fields.Many2one('account.account', string='Exchange rate loss account')  # Валютын ханшийн зөрүүний гарзын данс
    real_state_underpayment_tax_account = fields.Many2one('account.account', String='Underpayment')
    real_state_overpayment_tax_account = fields.Many2one('account.account', String='Overpayment')
    bank_account_lines = fields.One2many('res.company.banks', 'company_id', string='Bank Accounts')
    chart_cashflow_template_id = fields.Many2one('account.chart.cashflow.template', help='The chart template for the company (if any)')
    has_chart_of_cashflows = fields.Boolean(string='Company has a chart of cashflows', default=False)
    initial_config_account_id = fields.Many2one('account.account', string='Initial Balance Configuration Account')
    module_l10n_mn_account_move_group = fields.Boolean(string="Is allowed Journal move group", default=False)
    journal_of_currency_exchange_id = fields.Many2one('account.journal', string='Journal of exchange equations')
    is_bank_statement_automatically = fields.Selection([('no', 'No'), ('yes', 'Yes')], string="Automatically write on the account bank statement", default='no')
    configure_allowed_user_on_account = fields.Boolean(string="Configure Allowed User on Account", default=False)
    inv_sequence = fields.Selection([('current', 'When the invoice is canceled and saved, it will be saved as the current number'),
                                     ('new', 'When the invoice is canceled and saved, it will be saved as the new number')], default='new', string='Invoice Sequencer')
    statement_date = fields.Selection(STATEMENT_DATE_SELECTION, default='same', string='Bank Statement')

    def reflect_code_prefix_change(self, old_code, new_code, digits):
        accounts = self.env['account.account'].search([('code', 'like', old_code), ('internal_type', '=', 'liquidity'), ('company_id', '=', self.id)], order='code asc')
        for account in accounts:
            if account.code.startswith(old_code):
                account.with_context(config_setting=True).write({'code': self.get_new_account_code(account.code, old_code, new_code, digits)})

    def reflect_code_digits_change(self, digits):
        accounts = self.env['account.account'].search([('company_id', '=', self.id)], order='code asc')
        for account in accounts:
            account.with_context(config_setting=True).write({'code': account.code.rstrip('0').ljust(digits, '0')})

class ResCompanyBanks(models.Model):
    _name = 'res.company.banks'

    company_id = fields.Many2one('res.company', string='Company')
    bank_id = fields.Many2one('res.bank', string='Bank')
    bank_account_id = fields.Many2one('res.partner.bank', string='Bank Account')
    show_on_invoice = fields.Boolean(string='Show on invoice')
