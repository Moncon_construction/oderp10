# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
from odoo.exceptions import UserError
from odoo.addons.l10n_mn_account.models.res_company import STATEMENT_DATE_SELECTION


class AccountConfigSettings(models.TransientModel):
    _inherit = 'account.config.settings'
    _description = 'Account settings'

    type_of_exchange_id = fields.Many2one('account.cashflow.type', related='company_id.type_of_exchange_id', string='Cashflow type of exchange')  # Хөрвөлтийн мөнгөн гүйлгээний төрөл
    type_of_currency_id = fields.Many2one('account.cashflow.type', related='company_id.type_of_currency_id', string='Cashflow type of currency')  # Валютын мөнгөн гүйлгээний төрөл
    account_from_id = fields.Many2one('account.account', related='company_id.account_from_id', string='Currency Exchange Gain Account')  # Валютын ханшийн зөрүүний олзын данс
    account_to_id = fields.Many2one('account.account', related='company_id.account_to_id', string='Exchange rate loss account')  # Валютын ханшийн зөрүүний гарзын данс
    chart_cashflow_template_id = fields.Many2one('account.chart.cashflow.template', related='company_id.chart_cashflow_template_id', string='Chart Template')
    has_chart_of_cashflows = fields.Boolean(related='company_id.has_chart_of_cashflows', string='Company has a chart of cashflows')
    module_l10n_mn_account_move_group = fields.Boolean(related="company_id.module_l10n_mn_account_move_group", default=False,
                                                       help="installs l10n_mn_account_move_group",
                                                       string="Is allowed Journal move group")
    journal_of_currency_exchange_id = fields.Many2one('account.journal', related='company_id.journal_of_currency_exchange_id', string='Journal of exchange equations')
    is_bank_statement_automatically = fields.Selection([('no', 'No'), ('yes', 'Yes')], related="company_id.is_bank_statement_automatically", string="Automatically write on the account bank statement")
    inv_sequence = fields.Selection([('current', 'When the invoice is canceled and saved, it will be saved as the current number'),
                                    ('new', 'When the invoice is canceled and saved, it will be saved as the new number')], related='company_id.inv_sequence', string='Invoice Sequencer')
    statement_date = fields.Selection(STATEMENT_DATE_SELECTION, related="company_id.statement_date", default=lambda x: x.company_id.statement_date or 'same', string='Bank Statement')
    
    @api.onchange('company_id')
    def onchange_company_id_for_cashflow(self):
        # update related fields
        if self.company_id:
            company = self.company_id
            self.chart_cashflow_template_id = company.chart_cashflow_template_id
            self.has_chart_of_cashflows = len(company.chart_cashflow_template_id) > 0 or False

    # Дахин тодорхойлов
    @api.onchange('company_id')
    def onchange_company_id(self):
        # update related fields
        self.currency_id = False
        if self.company_id:
            company = self.company_id
            self.chart_template_id = company.chart_template_id
            self.has_chart_of_accounts = False
            self.expects_chart_of_accounts = company.expects_chart_of_accounts
            self.currency_id = company.currency_id
            self.transfer_account_id = company.transfer_account_id
            self.company_footer = company.rml_footer
            self.tax_calculation_rounding_method = company.tax_calculation_rounding_method
            self.bank_account_code_prefix = company.bank_account_code_prefix
            self.cash_account_code_prefix = company.cash_account_code_prefix
            self.code_digits = company.accounts_code_digits

            # update taxes
            ir_values = self.env['ir.values']
            taxes_id = ir_values.get_default('product.template', 'taxes_id', company_id=self.company_id.id)
            supplier_taxes_id = ir_values.get_default('product.template', 'supplier_taxes_id',
                                                      company_id=self.company_id.id)
            self.default_sale_tax_id = isinstance(taxes_id, list) and len(taxes_id) > 0 and taxes_id[0] or taxes_id
            self.default_purchase_tax_id = isinstance(supplier_taxes_id, list) and len(supplier_taxes_id) > 0 and \
                                           supplier_taxes_id[0] or supplier_taxes_id
        return {}

    @api.multi
    def set_chart_of_cashflow(self):
        """ install a chart of accounts for the given company (if required) """
        if self.chart_cashflow_template_id:
            if self.company_id.chart_cashflow_template_id and self.chart_cashflow_template_id != self.company_id.chart_cashflow_template_id:
                raise UserError(_('You can not change a company chart of account once it has been installed'))
            wizard = self.env['wizard.multi.charts.cashflows'].create({
                'company_id': self.company_id.id,
                'chart_cashflow_template_id': self.chart_cashflow_template_id.id,
            })
            self.has_chart_of_cashflows = True
            wizard.execute()