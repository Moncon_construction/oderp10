# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class ResPartner(models.Model):
    _inherit = "res.partner"

    lock_bill = fields.Boolean('Lock bill')
    lock_date = fields.Date('Lock date')