
# -*- coding: utf-8 -*-
from odoo import fields, models


class Sectort(models.Model):
    _inherit = "res.sector"

    analytic_account_id = fields.Many2one('account.analytic.account', string='Analytic Account')