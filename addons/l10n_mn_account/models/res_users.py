# -*- coding: utf-8 -*-
from odoo import fields, models, _, api


class ResUsers(models.Model):
    _inherit = "res.users"

    allowed_account_ids = fields.Many2many('account.account', 'account_account_users_rel', 'user_id', 'acc_id', string='Allowed Accounts')