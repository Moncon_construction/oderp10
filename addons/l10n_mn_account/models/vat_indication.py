# -*- coding: utf-8 -*-
from odoo import fields, models, _, api # @UnresolvedImport
from odoo.exceptions import UserError, ValidationError
from odoo.tools.safe_eval import safe_eval as eval

class VatIndication(models.Model):
    _name = "vat.indication"
    
    name = fields.Char('Name', required = True)
    indication_type = fields.Selection([('income', 'Income'),
                                        ('outcome', 'Outcome')], string = 'Indication type', required = True)