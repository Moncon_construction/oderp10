# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models, fields

# ---------------------------------------------------------
# Account Financial Report
# ---------------------------------------------------------


class AccountFinancialReport(models.Model):
    _inherit = "account.financial.report"

    def _compute_account_balance(self, accounts):
        '''
        Санхүүгийн тайлангийн бүтэц нь дансны төрөлтэй холбогдох бөгөөд дансны төрөл нь данстай
        холбогддог.
        '''
        mapping = {
            'balance': "COALESCE(SUM(debit),0) - COALESCE(SUM(credit), 0) as balance",
        }

        company_id = self.env['res.company'].browse(self.env.user.company_id.id)
        where_period = ""

        for type in self:
            if type.chart_type == 'profit':
                if company_id.period_journal_id:
                    period_journal_id_to_skip = company_id.period_journal_id.id
                    where_period = " AND account_move_line.journal_id <> " \
                                   + str(period_journal_id_to_skip)
        res = {}
        for account in accounts:
            res[account.id] = dict((fn, 0.0) for fn in mapping.keys())
        if accounts:
            tables, where_clause, where_params = self.env['account.move.line']._query_get()
            tables = tables.replace('"', '') if tables else "account_move_line"
            wheres = [""]
            if where_clause.strip():
                wheres.append(where_clause.strip())
            filters = " AND ".join(wheres)
            request = "SELECT account_id as id, " + ', '.join(mapping.values()) + \
                       " FROM " + tables + \
                       " WHERE account_id IN %s " \
                            + filters + \
                            where_period + \
                       " GROUP BY account_id"
            params = (tuple(accounts._ids),) + tuple(where_params)
            self.env.cr.execute(request, params)
            for row in self.env.cr.dictfetchall():
                res[row['id']] = row
        return res

    def _compute_report_balance(self, reports):
        '''
        Санхүүгийн тайлангийн бүтэц нь 4 төрөлтэй бөгөөд төрлөөсөө хамаарч тухайн тайлангийн
        шатлал нь өөр өөрөөр бодогдоно. Жишээлбэл accounts төрөлтэй байвал энэ нь шууд данстай холбогдоно.
        '''
        res = {}
        fields = ['balance']
        for report in reports:
            if report.id in res:
                continue
            res[report.id] = dict((fn, 0.0) for fn in fields)
            if report.type == 'accounts':
                # it's the sum of the linked accounts
                res[report.id]['account'] = self._compute_account_balance(report.account_ids)
                for value in res[report.id]['account'].values():
                    for field in fields:
                        res[report.id][field] += value.get(field)
            elif report.type == 'account_type':
                # it's the sum the leaf accounts with such an account type
                accounts = self.env['account.account'].search([('user_type_id', 'in', report.account_type_ids.ids)])
                res[report.id]['account'] = self._compute_account_balance(accounts)
                for value in res[report.id]['account'].values():
                    for field in fields:
                        res[report.id][field] += value.get(field)
            elif report.type == 'account_report' and report.account_report_id:
                # it's the amount of the linked report
                res2 = self._compute_report_balance(report.account_report_id)
                for key, value in res2.items():
                    for field in fields:
                        res[report.id][field] += value[field]
            elif report.type == 'sum':
                # it's the sum of the children of this account.report
                res2 = self._compute_report_balance(report.children_ids)
                for key, value in res2.items():
                    for field in fields:
                        res[report.id][field] += value[field]

        return res

    @api.multi
    def _get_balance(self):
        '''
        Тайлангийн шатлал дээрх холбоотай данснуудын гүйлгээний нийлбэрийг шүүлтүүрийн дагуу шүүж авна.
        Шүүж авсан утга нь тухайн шатлалын баланс талбарын утга болно.'''
        res = {}
        for this in self:
            child_reports = this._get_children_by_order()

            res = {}
            res = self._compute_report_balance(child_reports)
            balance = 0.0
            for report in child_reports:
                if report.id == this.id:
                    balance = res[report.id]['balance'] * report.sign

            this.balance = balance

    balance = fields.Float(compute='_get_balance', string='Balance')
    account_report_id = fields.Many2many('account.financial.report', 'account_financial_report_sub_rel', 'report_head_id', 'report_sub_id', 'Report Value')
    company_id = fields.Many2one('res.company', string='Company', required=True,default=lambda self: self.env.user.company_id)
