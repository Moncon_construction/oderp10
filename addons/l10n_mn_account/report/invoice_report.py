# -*- coding: utf-8 -*-

from odoo import tools
from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.addons.l10n_mn_report.models.report_helper import verbose_numeric, comma_me, convert_curr


class AccountInvoiceReportInherit(models.AbstractModel):
    _name = 'report.l10n_mn_account.report_invoice'

    @api.multi
    def get_from_invoice_address(self, invoices):
        name = street = street2 = email = '...........'
        phone = fax = '...........'
        site = number = '...........'
        taxpayer_number = taxpayer_number = ''
        res = {}
        partner_obj = self.env['res.partner']
        for inv in invoices if invoices else None:
            address = False
            if inv.type in ('in_invoice', 'out_refund'):
                address = inv.partner_id

                site = inv.partner_id.website or ''
                name = inv.partner_id.name or '...........'
                register_number = inv.partner_id.register or ''
                taxpayer_number = inv.partner_id.taxpayer_number or ''
            else:
                address = inv.company_id.partner_id
                site = address.website or '...........'
                name = address.name or '...........'
                register_number = address.register or '...........'
                taxpayer_number = address.taxpayer_number or '...........'
            if address:
                phone = address.phone or '...........'
                email = address.email or '...........'
                fax = address.fax or '...........'
                street = address.street or ''
                if address.street2:
                    street += address.street2 or ''
            res[inv.id] = {'name': name,
                           'phone': phone,
                           'fax': fax,
                           'email': email,
                           'register_number': register_number,
                           'taxpayer_number': taxpayer_number,
                           'website': site,
                           'address': street}
        return res

    @api.multi
    def get_to_invoice_address(self, invoices):
        street = name = '...........'
        phone = fax = '...........'
        contract = '...........'
        email = website = '...........'
        res = {}
        partner_obj = self.env['res.partner']
        for inv in invoices if invoices else None:
            address = False
            if inv.type in ('in_invoice', 'out_refund'):
                address = inv.company_id.partner_id
                name = inv.company_id.partner_id.name or '...........'
                register_number = inv.company_id.partner_id.register or ''
                taxpayer_number = inv.company_id.partner_id.taxpayer_number or ''
            else:
                address = inv.partner_id
                name = inv.partner_id.name or '...........'
                register_number = inv.partner_id.register or ''
                taxpayer_number = inv.partner_id.taxpayer_number or ''
            if address:
                phone = address.phone or '...........'
                fax = address.fax or '...........'
                street = address.street or '...........'
                if address.street2:
                    street += address.street2 or '...........'
            res[inv.id] = {'name': name,
                           'phone': phone,
                           'fax1': fax,
                           'address': street,
                           'register_number': register_number,
                           'taxpayer_number': taxpayer_number}
        return res

    @api.multi
    def is_footer_visible(self, account_number):
        company_id = self.env.user.company_id.id
        journals = self.env['account.journal'].search(
            [('type', '=', 'bank'), ('company_id', '=', company_id), ('name', '=', account_number)])
        if(len(journals) == 0):
            return True
        return journals['display_on_footer']

    @api.multi
    def render_html(self, docids, data=None):
        company_id = self.env.user.company_id.id
        showAccList = []
        account_ids = self.env['res.company.banks'].search(
            [('show_on_invoice', '=', True), ('company_id', '=', company_id)])
        if account_ids:
            for acc_id in account_ids:
                showAccList.append({'bank_name': acc_id.bank_id.name, 'acc_number': acc_id.bank_account_id.acc_number, 'acc_currency': "/" + acc_id.bank_account_id.currency_id.symbol + "/" if acc_id.bank_account_id.currency_id else ''})
        report_obj = self.env['report']
        report = report_obj._get_report_from_name(
            'l10n_mn_account.report_invoice')
        invoice_obj = self.env['account.invoice']
        invoices = invoice_obj.browse(docids)
        verbose_total_dict = {}
        get_from_invoice_address = self.get_from_invoice_address(invoices)
        get_to_invoice_address = self.get_to_invoice_address(invoices)
        is_footer_visible = self.is_footer_visible
        get_total = {}
        for inv in invoices:
            total_amount = 0
            total_tax = 0
            total_discount = 0
            curr = u''
            div_curr = u''
            get_total[inv.id] = {'amount': 0.0,
                                 'paid': 0.0,
                                 'tax': 0.0,
                                 'discount': 0.0,
                                 'currency': inv.currency_id.name}
            total_amount = sum(
                line.price_subtotal for line in inv.invoice_line_ids)
            total_tax = sum(line.amount for line in inv.tax_line_ids)
            total_discount = sum((line.discount / 100.0) * (line.price_unit * line.quantity)
                                 for line in inv.invoice_line_ids if line.discount > 0)
            total_amount_total = total_amount + total_discount
            if inv.currency_id:
                curr = inv.currency_id.integer
                div_curr = inv.currency_id.divisible
            list = verbose_numeric(abs(total_amount + total_tax))
            verbose_total_dict[inv.id] = convert_curr(list, curr, div_curr)
#             amounts[line.id] = comma_me(abs(line.amount))
            get_total[inv.id]['amount'] = comma_me(total_amount_total)
            get_total[inv.id]['tax'] = comma_me(total_tax)
            get_total[inv.id]['discount'] = comma_me(total_discount)
            get_total[inv.id]['paid'] = comma_me(total_amount + total_tax)
            
        docargs = {
            'doc_ids': docids,
            'showAccList': showAccList,
            'doc_model': report.model,
            'verbose_amount': verbose_total_dict,
            'from_address': get_from_invoice_address,
            'to_address': get_to_invoice_address,
            'sumtotal': get_total,
            'data_report_margin_top': 5,
            'data_report_header_spacing': 5,
            'docs': invoices,
            'is_footer_visible': is_footer_visible
        }
        return report_obj.render('l10n_mn_account.report_invoice', docargs)
