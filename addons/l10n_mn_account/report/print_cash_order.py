# -*- coding: utf-8 -*-

from odoo import api, models
from odoo.addons.l10n_mn_report.models.report_helper import verbose_numeric, comma_me, convert_curr  # @UnresolvedImport

class PrintCashOrder(models.AbstractModel):
    _name = "report.l10n_mn_account.print_cash_order"

    @api.model
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        statement_line_obj = self.env['account.bank.statement.line']
        # account_invoice_obj = self.env['account.invoice']
        report = report_obj._get_report_from_name('l10n_mn_account.print_cash_order')
        lines = statement_line_obj.browse(docids)
        verbose_total_dict = {}
        amounts = {}
        currency = {}
        # qr_data = False
        # bill_data = False
        # lottery = False
        for line in lines:
            word = verbose_numeric(abs(line.amount))
            curr = u''
            div_curr = u''
            symbol = u''
            if line.currency_id:
                curr = line.statement_id.currency_id.integer
                div_curr = line.statement_id.currency_id.divisible
                symbol = line.currency_id.symbol
            elif line.statement_id.currency_id:
                curr = line.statement_id.currency_id.integer
                div_curr = line.statement_id.currency_id.divisible
                symbol = line.statement_id.currency_id.symbol
            verbose_total_dict[line.id] = convert_curr(word, curr, div_curr)
            amounts[line.id] = comma_me(abs(line.amount))
            currency[line.id] = {'name': curr,
                                 'symbol': symbol}
        # inv_id = account_invoice_obj.search([('name','=',lines.ref)])
        #Сугалаанд ашиглагдана
        #if inv_id != [] and inv_id != None:
        #    invoice = account_invoice_obj.browse(inv_id[0])
        #    if invoice.vat_bill_id:
        #        bill_data = invoice.vat_bill_id
        #        dfl comment qr_data = invoice.vat_qr_data
        #        lottery = invoice.vat_lottery
        docargs = {
            'doc_ids': docids,
            'doc_model': report.model,
            'docs': lines,
            'verbose_amount': verbose_total_dict,
            'company_id': self.env.user.company_id,
            'amounts': amounts,
            'currency': currency,
            'user': self.env['res.users'].browse(self.env.uid),
            'first_sign': self.env.user.company_id.first_sign.name,
            'second_sign': self.env.user.company_id.second_sign_cart.name,
            # 'bill_data': bill_data,
            # 'qr_data': qr_data,
            # 'lottery': lottery,
            'data_report_margin_top': 20,
            'data_report_header_spacing': 5,
            #'format': self.env['account.config.settings']
        }
        return report_obj.render('l10n_mn_account.print_cash_order', docargs)
