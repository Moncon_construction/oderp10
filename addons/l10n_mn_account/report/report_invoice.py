# -*- coding: utf-8 -*-

from odoo import tools
from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.addons.l10n_mn_report.models.report_helper import verbose_numeric, comma_me, convert_curr


class AccountInvoiceReportInherit(models.AbstractModel):
    _name = 'report.l10n_mn_account.report_invoice'

    @api.multi
    def get_from_invoice_address(self, inv):
        name = street = street2 = email = '...........'
        phone = fax = '...........'
        site = number = '...........'
        res = {}
        partner_obj = self.env['res.partner']
        if inv:
            address = False
            if inv.type in ('in_invoice', 'out_refund'):
                address = inv.partner_id

                site = inv.partner_id.website or ''
                name = inv.partner_id.name or '...........'
                # register_number = inv.partner_id.register_number or ''
                register_number = inv.partner_id.register or ''
                taxpayer_number = inv.partner_id.taxpayer_number or ''

            else:
                address = inv.company_id.partner_id

                site = address.website or '...........'
                name = inv.company_id.partner_id.name or '...........'
                # register_number = address.register_number or '...........'
                register_number = address.register or '...........'
                taxpayer_number = address.taxpayer_number or '...........'

            if address:
                phone = address.phone or '...........'
                email = address.email or '...........'
                fax = address.fax or '...........'
                street = address.street or ''
                if address.street2:
                    street += address.street2 or ''
        res = {'name': name,
               'phone': phone,
               'fax': fax,
               'email': email,
               # 'register_number': register_number,
                'register_number': register_number,
               'taxpayer_number': taxpayer_number,

               'website': site,
               'address': street}
        return res

    @api.multi
    def get_to_invoice_address(self, inv):
        street = name = '...........'
        phone = fax = '...........'
        contract = '...........'
        email = website = '...........'
        res = {}
        partner_obj = self.env['res.partner']
        if inv:
            address = False
            if inv.type in ('in_invoice', 'out_refund'):
                address = inv.company_id.partner_id
                name = inv.company_id.partner_id.name or '...........'
                # register_number = inv.company_id.partner_id.register_number or ''
                register_number = inv.company_id.partner_id.register or ''
                taxpayer_number = inv.company_id.partner_id.taxpayer_number or ''

            else:
                address = inv.partner_id
                name = inv.partner_id.name or '...........'
                # register_number = inv.partner_id.register_number or ''
            if address:
                phone = address.phone or '...........'
                fax = address.fax or '...........'
                street = address.street or '...........'
                if address.street2:
                    street += address.street2 or '...........'
        res = {'name': name,
               'phone': phone,
               'fax': fax,
               'address': street,
               'register_number': register_number,
               'taxpayer_number': taxpayer_number }
        # 'register_number': register_number
        return res

    @api.multi
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name(
            'l10n_mn_account.report_invoice')
        invoice_obj = self.env['account.invoice']
        inv = invoice_obj.browse(docids)
        verbose_total_dict = {}
        get_from_invoice_address = self.get_from_invoice_address(inv)
        get_to_invoice_address = self.get_to_invoice_address(inv)
        total_amount = 0
        total_tax = 0
        total_discount = 0
        curr = u''
        div_curr = u''
        get_total = {'amount': 0.0,
                     'paid': 0.0,
                     'tax': 0.0,
                     'discount': 0.0,
                     'currency': inv.currency_id.name}
        total_amount = sum(
            line.price_subtotal for line in inv.invoice_line_ids)
        total_tax = sum(line.amount for line in inv.tax_line_ids)
        total_discount = sum((line.discount / 100.0) * (line.price_unit * line.quantity)
                             for line in inv.invoice_line_ids if line.discount > 0)
        total_amount_total = total_amount + total_discount
        if inv.currency_id:
            curr = inv.currency_id.integer
            div_curr = inv.currency_id.divisible
        list = verbose_numeric(abs(total_amount + total_tax))
        verbose_total_dict[inv.id] = convert_curr(list, curr, div_curr)
        get_total['amount'] = comma_me(total_amount_total)
        get_total['tax'] = comma_me(total_tax)
        get_total['discount'] = comma_me(total_discount)
        get_total['paid'] = comma_me(total_amount + total_tax)

        docargs = {
            'doc_ids': docids,
            'doc_model': report.model,
            'verbose_amount': verbose_total_dict,
            'from_address': get_from_invoice_address,
            'to_address': get_to_invoice_address,
            'sumtotal': get_total,
            'data_report_margin_top': 5,
            'data_report_header_spacing': 5,
            'docs': inv

        }

        return report_obj.render('l10n_mn_account.report_invoice', docargs)
