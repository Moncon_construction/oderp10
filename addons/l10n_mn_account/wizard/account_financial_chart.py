# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class AccountFinancialChart(models.TransientModel):
    _name = "account.financial.chart"
    _description = "Account financial chart"
    
    date_from = fields.Date(string='Start Date')
    date_to = fields.Date(string='End Date')
    target_move = fields.Selection([('posted', 'All Posted Entries')
                    , ('all', 'All Entries')], string='Filter by', required=True, default='posted')

    @api.multi
    def account_fin_chart_open_window(self):
        self.ensure_one()
        context = dict(self.env.context or {})
        
        def ref(module, xml_id):
            proxy = self.env['ir.model.data']
            return proxy.get_object_reference(module, xml_id)

        model, search_view_id = ref('account', 'view_account_financial_report_search')
        model, tree_view_id = ref('l10n_mn_account', 'view_account_financial_report_tree_hierarchy')

        if self.date_from:
            context.update(date_from=self.date_from)

        if self.date_to:
            context.update(date_to=self.date_to)

        if self.target_move:
            context.update(state=self.target_move)

        views = [
            (tree_view_id, 'tree'),
        ]

        return {
            'name': _('Financial report'),
            'context': context,
            'view_type': 'tree',
            "view_mode": 'tree',
            'res_model': 'account.financial.report',
            'type': 'ir.actions.act_window',
            'views': views,
            'view_id': False,
            'domain': [('parent_id','=',False)],
            'search_view_id': search_view_id,
        }