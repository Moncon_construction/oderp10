# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.tools.translate import _
import base64
from io import BytesIO
import time
import xlsxwriter
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class account_general_journal(models.Model):
    _inherit = 'account.common.report'
    _name = 'account.general.account'
    _description = 'Report General Account'

    date_from = fields.Date(required=True, default=lambda self: fields.datetime.now())
    date_to = fields.Date(required=True, default=lambda self: fields.datetime.now())
    show_analytic_account = fields.Boolean('Show analytic account')
    analytic_account_ids = fields.Many2many('account.analytic.account', string="Analytic accounts filter", help="Shows all analytic accounts when selected nothing.")
    partner_id = fields.Many2one('res.partner', 'Partner')

    account_types = fields.Many2many('account.account.type', 'account_general_acc_type_rel', 'account_type_id', 'user_type_id', 'Account Types')
    account_ids = fields.Many2many('account.account', 'account_general_account_account_rel', 'wizard_id', 'account_id', 'Account')

    journal_ids = fields.Many2many(required=False, default = False)

    @api.onchange('account_types')
    def add_account_from_type(self):
        if self.account_types:
            return {'domain': {'account_ids': [('user_type_id', 'child_of', self.account_types.ids)]}}
        else:
            return []

    @api.multi
    def combine_SQL_Cond(self, SQL_prev_cond, SQL_new_cond):
        SQL_res_cond = SQL_prev_cond + ' AND ' + SQL_new_cond
        return SQL_res_cond

    def is_module_installed(self, module_name):
        self._cr.execute("SELECT id FROM ir_module_module WHERE name = '%s' AND state IN ('installed', 'to upgrade')" % module_name)
        results = self._cr.dictfetchall()

        if results and len(results) > 0:
            return True
        else:
            return False
        
    @api.multi
    def get_header(self, sheet, rowx,column, format_title_small):
        # Тайлангийн хүснэгтийн толгой зурах
        sheet.merge_range(rowx, 0, rowx + 1, 0, _('Num'), format_title_small)
        sheet.merge_range(rowx, 1, rowx + 1, 1, _('Date'), format_title_small)
        sheet.merge_range(rowx, 2, rowx + 1, 2, _('Code'), format_title_small)
        if self.show_analytic_account:
            sheet.merge_range(rowx, 3, rowx + 1, 3, _('Analytic account'), format_title_small)
        sheet.merge_range(rowx, column + 3, rowx + 1, column + 3, _('Transaction Name'), format_title_small)
        sheet.merge_range(rowx, column + 4, rowx + 1, column + 5, _('Origin'), format_title_small)
        sheet.merge_range(rowx, column + 6, rowx + 1, column + 6, _('Partner'), format_title_small)
        sheet.merge_range(rowx, column + 7, rowx + 1, column + 7, _('Transaction Description'), format_title_small)
        sheet.merge_range(rowx, column + 8, rowx, column + 9, _('Debit'), format_title_small)
        sheet.write(rowx + 1, column + 8, _('Currency'), format_title_small)
        sheet.write(rowx + 1, column + 9, _('MNT'), format_title_small)
        sheet.merge_range(rowx, column + 10, rowx, column + 11, _('Credit'), format_title_small)
        sheet.write(rowx + 1, column + 10, _('Currency'), format_title_small)
        sheet.write(rowx + 1, column + 11, _('MNT'), format_title_small)
        sheet.merge_range(rowx, column + 12, rowx, column + 13, _('Balance'), format_title_small)
        sheet.write(rowx + 1, column + 12, _('Currency'), format_title_small)
        sheet.write(rowx + 1, column + 13, _('MNT'), format_title_small)
        return sheet

    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('General Account Detail')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)

        format_filter = book.add_format(ReportExcelCellStyles.format_filter)

        format_title_small = book.add_format(ReportExcelCellStyles.format_title_small)

        format_group_center = book.add_format(ReportExcelCellStyles.format_group)
        format_group_right = book.add_format(ReportExcelCellStyles.format_group_right)
        format_group_left = book.add_format(ReportExcelCellStyles.format_group_left)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)

        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)

        format_content_bold_left = book.add_format(ReportExcelCellStyles.format_content_bold_left)
        format_content_bold_right = book.add_format(ReportExcelCellStyles.format_content_bold_right)
        format_content_bold_float = book.add_format(ReportExcelCellStyles.format_content_bold_float)

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=_('general_account_detail'), form_title=file_name, date_to=self.date_to, date_from=self.date_from).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 0

        # compute column
        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 6)
        sheet.set_column('C:C', 11)
        sheet.set_column('D:D', 11 if self.show_analytic_account else 25)
        sheet.set_column('E:E', 25 if self.show_analytic_account else 10)
        sheet.set_column('F:F', 10)
        sheet.set_column('G:G', 10 if self.show_analytic_account else 25)
        sheet.set_column('H:H', 25)
        sheet.set_column('I:I', 25 if self.show_analytic_account else 10)
        sheet.set_column('J:J', 10)
        sheet.set_column('K:K', 10)
        sheet.set_column('L:L', 10)
        sheet.set_column('M:M', 11)
        sheet.set_column('N:N', 10 if self.show_analytic_account else 13)
        sheet.set_column('O:O', 13)

        # ** Print out Report Title
        sheet.merge_range(rowx, 1, rowx, 3, (_('Company Name: %s') % self.company_id.name), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 1, rowx, 3, report_name, format_name)
        rowx += 1
        sheet.merge_range(rowx, 1, rowx, 3, (_('Date: %s') % time.strftime('%Y-%m-%d')), format_filter)
        # **
        wiz = self
        # BEGIN: Form non account module table SQL addition
        tables_exist = False
        self.env.cr.execute("SELECT SUM(1) as tnum FROM pg_tables WHERE tablename in ('stock_move','procurement_group')")
        table_conds = self.env.cr.dictfetchall()
        for curr_table_conds in table_conds:
            if curr_table_conds['tnum'] == 2:
                tables_exist = True
        # Хэрэв stock_move болон procurement_group table
        # аль нэг нь үүсээгүй тохиолдолд sql-ийн бүтцийг өөрчлөх шаардлагатай.
        if tables_exist and self.is_module_installed("l10n_mn_stock_account"):
            p_pg_fields = " rp.name AS pname, pg.name AS pgname "
            p_pg_tables = " LEFT JOIN stock_move p ON p.id = l.stock_move_id LEFT JOIN procurement_group pg ON pg.id = p.group_id "
            p_pg_group = " , pg.name "
        else:
            print("stock_move or procurement_group tables do not exist.")
            p_pg_fields = " '' AS pname, '' AS pgname "
            p_pg_tables = ""
            p_pg_group = ""
        # Check if stock_move table exists
        # Check if procurement_group table extist

        # END: Form non account module table SQL addition

        rowx += 1
        sheet.merge_range(rowx, 1, rowx, 3, (_('Time Span: %s - %s') % (self.date_from, self.date_to)), format_filter)

        # BEGIN: Тайлангийн query унших хэсэг
        # ** Handle Additional SQL Conditions
        SQL_ADD_COND = ''
        cond_journal_tot = ""
        cond_account_tot = ""
        all_accounts_of_types = ""
        if wiz.target_move != 'all':
            SQL_ADD_COND = wiz.combine_SQL_Cond(SQL_ADD_COND, "m.state = 'posted'")
        if wiz.partner_id:
            SQL_ADD_COND = wiz.combine_SQL_Cond(SQL_ADD_COND, 'l.partner_id = %s' % wiz.partner_id.id)
            # Print out Partner name
            rowx += 1
            sheet.merge_range(rowx, 1, rowx, 3, (_('Partner: %s') % wiz.partner_id.name), format_filter)
        if wiz.journal_ids:
            SQL_ADD_COND = wiz.combine_SQL_Cond(SQL_ADD_COND, ('l.journal_id in (%s)' % ', '.join(map(str, wiz.journal_ids.ids))))
            for curr_journalcond in wiz.journal_ids:
                cond_journal_tot += curr_journalcond.name + ", "
        else:
            cond_journal_tot = _('All')
        if wiz.account_ids:
            SQL_ADD_COND = wiz.combine_SQL_Cond(SQL_ADD_COND, ('l.account_id in (%s)' % ', '.join(map(str, wiz.account_ids.ids))))
            all_accounts_of_types = wiz.account_ids.ids
            for curr_accountcond in wiz.account_ids:
                if not cond_account_tot:
                    cond_account_tot = curr_accountcond.name
                else:
                    cond_account_tot += ', ' + curr_accountcond.name
        elif wiz.account_types:
                rowx += 1
                all_account_types = self.env['account.account.type'].search([('id', 'child_of', wiz.account_types.ids)])
                SQL_ADD_COND = wiz.combine_SQL_Cond(SQL_ADD_COND, ('at.id in (%s)' % ', '.join(map(str, all_account_types.ids))))
                names = ''
                for account_type in wiz.account_types:
                    if not names:
                        names = account_type.name
                    else:
                        names += ', ' + account_type.name
                sheet.merge_range(rowx, 1, rowx, 3, (_('Account Types: %s') % names), format_filter)

                all_accounts_of_types = self.env['account.account'].search([('user_type_id', 'in', all_account_types.ids)])
                for account_id in all_accounts_of_types:
                    if account_id.name:
                        if not cond_account_tot:
                            cond_account_tot = account_id.name
                        else:
                            cond_account_tot += ', ' + account_id.name
                all_accounts_of_types = all_accounts_of_types.ids
        else:
            all_accounts_of_types = self.env['account.account'].search([]).ids
        # **
        # Print out All Journal
        rowx += 1
        sheet.merge_range(rowx, 1, rowx, 13, (_('Journal: %s') % cond_journal_tot), format_filter)
        # Print out All Account
        rowx += 1
        sheet.merge_range(rowx, 1, rowx, 13, (_('Account: %s') % cond_account_tot), format_filter)
        rowx += 2

        # ** Handle Parameters
        params = (self.date_from, self.date_to, self.company_id.id)
        # **
        if self.show_analytic_account:
            analytic_select = '''
                l.analytic_account_id AS analytic_account_id,
                aaa.name AS analytic_name,
                aaa.code AS analytic_code,
            '''
            analytic_join = '''
                LEFT JOIN account_analytic_account aaa ON l.analytic_account_id = aaa.id
            '''
            analytic_group_by = '''
                aaa.name,
                aaa.code,
            '''
            analytic_order_by = '''
                aaa.code,
            '''
            if self.analytic_account_ids:
                analytic_where = ''' AND
                    l.analytic_account_id in (''' + str(self.analytic_account_ids.ids)[1:-1] + ''')
                '''
            else:
                analytic_where = ''
        else:
            analytic_select = ''
            analytic_join = ''
            analytic_group_by = ''
            analytic_order_by = ''
            analytic_where = ''
        REPORT_SQL = ('''
            SELECT
                l.account_id AS account_id,
                a.name AS acc_name,
                a.code AS acc_code, ''' + analytic_select + '''
                at.type AS report_type,
                m.id AS move_id,
                m.date AS date,
                l.id AS id,
                l.debit AS debit_mnt,
                CASE WHEN (l.amount_currency > 0) THEN l.amount_currency ELSE 0 END AS debit_currency,
                l.credit AS credit_mnt,
                CASE WHEN (l.amount_currency < 0) THEN l.amount_currency ELSE 0 END AS credit_currency,
                (select string_agg(code,', ') from account_account where id in (select account_id from account_move_line l2 where l2.move_id=l.move_id and l2.account_id<>l.account_id)) as code,
                COALESCE(l.amount_currency,0) AS amount_currency,
                l.name AS name,
                m.name AS move_name,
                m.ref AS ref,
                i.origin AS origin,
                i.name AS iname, ''' + p_pg_fields + '''
            FROM
                account_move_line l
                LEFT JOIN account_move m ON l.move_id = m.id
                LEFT JOIN account_account a ON l.account_id = a.id ''' + analytic_join + '''
                LEFT JOIN account_account_type at ON a.user_type_id = at.id
                LEFT JOIN account_invoice i ON i.move_id = m.id
                LEFT JOIN res_partner rp ON l.partner_id = rp.id ''' + p_pg_tables + '''
            WHERE
                m.date BETWEEN %s AND
                %s AND
                l.company_id = %s ''' + SQL_ADD_COND + analytic_where + '''
            GROUP BY
                l.account_id,
                l.move_id,
                a.name,
                a.code,
                at.type,
                m.id,
                m.date,
                l.id,
                l.name,
                l.amount_currency,
                l.debit,
                l.credit,
                m.name,
                m.ref,
                i.origin,
                i.name, ''' + analytic_group_by + '''
                rp.name ''' + p_pg_group + '''
            ORDER BY
                a.code,
                l.account_id, ''' + analytic_order_by + '''
                m.date,
                m.id''')
        self.env.cr.execute(REPORT_SQL, params)
        lines = self.env.cr.dictfetchall()
        # END: Тайлангийн query унших хэсэг

        # ** Groups and Totals Variables
        # Groups
        ind = 1
        curr_acc = ''
        is_header = True
        # Totals
        total_dr_currency = 0
        total_dr_mnt = 0
        total_cr_currency = 0
        total_cr_mnt = 0
        total_balance_currency = 0
        total_balance_mnt = 0
        balance_currency = 0
        balance = 0
        total_dr_num = 0
        total_cr_num = 0
        total_num = 0
        # **
        curr_dr_currency = 0
        curr_dr_mnt = 0
        curr_cr_currency = 0
        curr_cr_mnt = 0
        curr_balance_currency = 0
        curr_balance_mnt = 0
        move_account_ids = []
        column = 0
        column += (1 if self.show_analytic_account else 0)
        for line in lines:
            if curr_acc != line['acc_code']:
                if is_header:
                    # ** Print out Report Header
                    sheet = self.get_header(sheet, rowx, column, format_title_small)
                    # **
                    rowx += 2
                    is_header = False
                else:
                    # ** Print subgroup footer
                    sheet.merge_range(rowx, 0, rowx, column + 6, (_('Dr - %s, Cr - %s, Total - %s') % (total_dr_num, total_cr_num, total_dr_num)), format_content_bold_left)
                    sheet.write(rowx, column + 7, _('Totals'), format_content_bold_right)
                    sheet.write(rowx, column + 8, total_dr_currency, format_content_bold_float)
                    sheet.write(rowx, column + 9, total_dr_mnt, format_content_bold_float)
                    sheet.write(rowx, column + 10, total_cr_currency, format_content_bold_float)
                    sheet.write(rowx, column + 11, total_cr_mnt, format_content_bold_float)
                    sheet.write(rowx, column + 12, '', format_content_bold_float)
                    sheet.write(rowx, column + 13, '', format_content_bold_float)
                    rowx += 1
                    sheet.merge_range(rowx, 0, rowx, column + 11, _('End Balance'), format_group_right)
                    sheet.write(rowx, column + 12, total_balance_currency, format_group_float)
                    sheet.write(rowx, column + 13, total_balance_mnt + balance, format_group_float)
                    rowx += 1
                    # **
                    # Clear Totals after print
                    total_dr_currency = 0
                    total_dr_mnt = 0
                    total_cr_currency = 0
                    total_cr_mnt = 0
                    total_balance_currency = 0
                    total_balance_mnt = 0
                    total_dr_num = 0
                    total_cr_num = 0
                    total_num = 0
                    # **
                # ** Print new subgroup
                # Calculate initial balance
                move_line_obj = self.env['account.move.line']
                initial_balance = move_line_obj.get_initial_balance(self.company_id.id, [line['account_id']], self.date_from, 'posted')
                if initial_balance:
                    if line['report_type'] in ('other', 'expense', 'receivable', 'liquidity'):
                        balance = (initial_balance[0]['start_debit'] - initial_balance[0]['start_credit']) or 0
                        balance_currency = (initial_balance[0]['cur_start_debit'] - initial_balance[0]['cur_start_credit']) or 0
                    else:
                        balance = (initial_balance[0]['start_credit'] - initial_balance[0]['start_debit']) or 0.0
                        balance_currency = (initial_balance[0]['cur_start_credit'] - initial_balance[0]['cur_start_debit']) or 0.0
                else:
                    balance = 0.0
                    balance_currency = 0.0
                move_account_ids.append(line['account_id'])
                sheet.merge_range(rowx, 0, rowx, column + 10, (_('Account: %s %s') % (line['acc_code'], line['acc_name'])), format_group_center)
                sheet.write(rowx, column + 11, _('Begin Balance'), format_group_right)
                sheet.write(rowx, column + 12, balance_currency, format_group_float)
                sheet.write(rowx, column + 13, balance, format_group_float)
                rowx += 1
                # Clear group variables
                curr_balance_currency = balance_currency
                curr_balance_mnt = 0
                ind = 1
                # **
            curr_acc = line['acc_code']

            # BEGIN: Calculate debit, credit, balance amounts
            curr_dr_currency = line['debit_currency'] or 0
            curr_dr_mnt = line['debit_mnt'] or 0
            curr_cr_currency = line['credit_currency'] * (-1) or 0
            curr_cr_mnt = line['credit_mnt'] or 0

            if line['report_type'] in ('other', 'expense', 'receivable', 'liquidity'):
                curr_balance_currency += curr_dr_currency - curr_cr_currency
                curr_balance_mnt += curr_dr_mnt - curr_cr_mnt
            else:
                curr_balance_currency += curr_cr_currency - curr_dr_currency
                curr_balance_mnt += curr_cr_mnt - curr_dr_mnt
            # END: Calculate debit, credit, balance amounts

            # BEGIN: Print out Data
            sheet.write(rowx, 0, ind, format_content_number)
            sheet.write(rowx, 1, line['date'], format_content_text)
            sheet.write(rowx, 2, line['code'], format_content_text)
            if self.show_analytic_account:
                sheet.write(rowx, column + 2, '[' + (line['analytic_code'] or '') + '] ' + (line['analytic_name'] or ''), format_content_text)
            sheet.write(rowx, column + 3, line['move_name'], format_content_text)
            sheet.write(rowx, column + 4, line['iname'], format_content_text)
            sheet.write(rowx, column + 5, line['origin'] or line['pgname'], format_content_text)
            sheet.write(rowx, column + 6, line['pname'], format_content_text)
            sheet.write(rowx, column + 7, line['name'], format_content_text)
            sheet.write(rowx, column + 8, curr_dr_currency, format_content_float)
            sheet.write(rowx, column + 9, curr_dr_mnt, format_content_float)
            sheet.write(rowx, column + 10, curr_cr_currency, format_content_float)
            sheet.write(rowx, column + 11, curr_cr_mnt, format_content_float)
            sheet.write(rowx, column + 12, curr_balance_currency, format_content_float)
            sheet.write(rowx, column + 13, curr_balance_mnt + balance, format_content_float)
            # END: Print out Data

            # BEGIN: Calculate debit, credit, balance totals
            total_dr_currency += curr_dr_currency
            total_dr_mnt += curr_dr_mnt
            total_cr_currency += curr_cr_currency
            total_cr_mnt += curr_cr_mnt
            total_balance_currency = curr_balance_currency
            total_balance_mnt = curr_balance_mnt
            if curr_dr_mnt > 0 or curr_dr_currency > 0:
                total_dr_num += 1
            if curr_cr_mnt > 0 or curr_cr_currency > 0:
                total_cr_num += 1
            total_num += 1
            # END: Calculate debit, credit, balance totals

            rowx += 1
            ind += 1
        # ** Handle subgroup footer
        if lines:
            sheet.merge_range(rowx, 0, rowx, column + 6, (_('Dr - %s, Cr - %s, Total - %s') % (total_dr_num, total_cr_num, total_dr_num)), format_content_bold_left)
            sheet.write(rowx, column + 7, _('Totals'), format_content_bold_right)
            sheet.write(rowx, column + 8, total_dr_currency, format_content_bold_float)
            sheet.write(rowx, column + 9, total_dr_mnt, format_content_bold_float)
            sheet.write(rowx, column + 10, total_cr_currency, format_content_bold_float)
            sheet.write(rowx, column + 11, total_cr_mnt, format_content_bold_float)
            sheet.write(rowx, column + 12, '', format_content_bold_float)
            sheet.write(rowx, column + 13, '', format_content_bold_float)
            rowx += 1
            sheet.merge_range(rowx, 0, rowx, column + 11, _('End Balance'), format_group_right)
            sheet.write(rowx, column + 12, total_balance_currency, format_group_float)
            sheet.write(rowx, column + 13, total_balance_mnt + balance, format_group_float)
        # **
        # Гүйлгээ хийгдээгүй данснуудын эхний үлдэгдлийг харуулах
        diff_accounts = set(all_accounts_of_types) - set(move_account_ids)        
        rowx += 1
        if diff_accounts:
            if not lines:
                sheet = self.get_header(sheet, rowx, column, format_title_small)
                rowx += 2
            move_line_obj = self.env['account.move.line']
            initial_balance = move_line_obj.get_initial_balance(self.company_id.id, diff_accounts, self.date_from, 'posted')
            for i_balance in initial_balance:
                account = self.env['account.account'].search([('id', '=', i_balance['account_id'])])
                report_type = account.user_type_id.type
                if report_type in ('other', 'expense', 'receivable', 'liquidity'):
                    balance = (i_balance['start_debit'] - i_balance['start_credit']) or 0
                    balance_currency = (i_balance['cur_start_debit'] - i_balance['cur_start_credit']) or 0
                else:
                    balance = (i_balance['start_credit'] - i_balance['start_debit']) or 0.0
                    balance_currency = (i_balance['cur_start_credit'] - i_balance['cur_start_debit']) or 0.0
                if balance != 0 or balance_currency != 0:
                    sheet.merge_range(rowx, 0, rowx, column + 10, (_('Account: %s %s') % (i_balance['code'], i_balance['name'])), format_group_center)
                    sheet.write(rowx, column + 11, _('Begin Balance'), format_group_right)
                    sheet.write(rowx, column + 12, balance_currency, format_group_float)
                    sheet.write(rowx, column + 13, balance, format_group_float)
                    rowx += 1
                    sheet.merge_range(rowx, 0, rowx, column + 11, _('End Balance'), format_group_right)
                    sheet.write(rowx, column + 12, balance_currency, format_group_float)
                    sheet.write(rowx, column + 13, balance, format_group_float)
                    rowx += 1
        rowx += 2
        sheet.merge_range(rowx, 1, rowx, 4, '%s: ........................................... (                          )' % _('Executive Director'), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 1, rowx, 4, '%s: ........................................... (                          )' % _('General Accountant'), format_filter)

        book.close()

        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()
