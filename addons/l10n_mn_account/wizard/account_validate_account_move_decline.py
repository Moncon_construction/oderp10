from odoo import models, api, _
from odoo.exceptions import UserError


class ValidateAccountMoveDecline(models.TransientModel):
    _name = "account.move.decline"
    _description = "Account Move"

    @api.multi
    def button_cancel(self,docids):
        moves = self.env['account.move'].browse(docids['active_ids'])
        for move in moves:
             move.button_cancel()
