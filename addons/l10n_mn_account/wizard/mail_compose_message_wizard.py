# -*- coding: utf-8 -*-

import base64
import re

from odoo import _, api, fields, models, SUPERUSER_ID, tools
from odoo.tools.safe_eval import safe_eval


class MailComposer(models.TransientModel):
    _inherit = 'mail.compose.message'

    email_to = fields.Char(string='Email To Emails', help="Message recipients' email address... split by comma ','")
    
    @api.multi
    def send_mail(self, auto_commit=False):
        res = super(MailComposer, self).send_mail()
        
        Mail = self.env['mail.mail']
        MailMessage = self.env['mail.message']
        
        # email_to талбарт бичсэн мэйл хаягууд руу мөн нэхэмжлэлийг явуулах
        for obj in self:
            if obj.email_to and obj.model and obj.model == 'account.invoice':
                body_html = obj.body
                message = MailMessage.create({
                    'author_id': self.env.user.partner_id.id,
                    'res_id': obj.res_id,
                    'model': obj.model,
                    'body': body_html,
                    'subject': obj.subject,
                    'subtype_id': obj.subtype_id.id or False,
                    'message_type': obj.composition_mode,
                    'attachment_ids': [(6, 0, [attachment_id.id for attachment_id in obj.attachment_ids])]
                })
                
                # Мэйлийн биеийг layout-д оруулах
                base_template = None
                if obj.res_id and self._context.get('custom_layout', False):
                    base_template = self.env.ref(self._context['custom_layout'], raise_if_not_found=False)
                if base_template:
                    base_template_ctx = self.env['res.partner']._notify_prepare_template_context(message)
                    if not self.env.user.signature:
                        base_template_ctx['signature'] = False
                    base_mail_values = self.env['res.partner']._notify_prepare_email_values(message)
                
                    values = base_template.generate_email(message.id, fields=['body_html', 'subject'])
                    body_html = tools.html_sanitize(values['body'])
                    
                mail = Mail.create({
                    'email_from': self.env.user.partner_id.sudo().email, 
                    'email_to': obj.email_to,
                    'mail_message_id': message.id,
                    'body_html': body_html,
                    'subject': obj.subject,
                    'subtype_id': obj.subtype_id.id or False,
                    'message_type': obj.composition_mode,
                    'attachment_ids': [(6, 0, [attachment_id.id for attachment_id in obj.attachment_ids])]
                }).send(raise_exception=False)
                
                if obj.model and obj.res_id:
                    self.env[obj.model].browse(obj.res_id).message_post(body=_("Mail has sent to belown emails: %s") % obj.email_to)
                
        return res 
    
