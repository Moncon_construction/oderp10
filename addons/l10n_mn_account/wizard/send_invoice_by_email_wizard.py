# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.addons.web.controllers.main import _serialize_exception
import base64


class CustomPopMessage(models.TransientModel):
    _name = "custom.pop.message"

    name = fields.Text('Message')
    
    
class SendInvoiceEmailWizard(models.TransientModel):
    _name = 'send.invoice.by.email.wizard'
    
    invoice_ids = fields.Many2many('account.invoice', 'send_invoice_wizard_to_invoice', 'wizard_id', 'invoice_id', ondelete='cascade', required=True, string='Invoice')
    template_id = fields.Many2one('mail.template', ondelete='cascade', required=True, default=lambda self: self.env.ref('account.email_template_edi_invoice'), string="Email Template")
    
    def send(self):
        # Гарах мэйл сервэр байгаа эсэхийг шалгах
        outgoing_email = self.env['ir.mail_server'].sudo().search([], order='sequence asc', limit=1)
        if not (outgoing_email and len(outgoing_email) > 0):
            raise UserError(_('There is no configuration for outgoing mail server. Please contact system administrator.'))
        
        # Мэйл амжилттай илгээгдсэн эсэхийг хэрэглэгчид визардаар үзүүлэхэд ашиглана
        results = {
            'success': {'have': False, 'msg': ' '},
            'failed': {'have': False, 'msg': ' '},
            'completely_failed': {'have': False, 'msg': ' '}
        }
        
        # Нэхэмжлэлийн харилцагч руу мэйл илгээхийг оролдож байна
        for invoice in self.invoice_ids.filtered(lambda x: x.partner_id and x.partner_id.email):
            # Харилцагч дээр бүртгэгдсэн НЭХЭМЖЛЭЛийн холбогч хаягуудыг мөн авах
            partner_mails = ''
            for partner in invoice.partner_id.child_ids:
                if partner.type == 'invoice' and partner.email:
                    partner_mails += (partner.email + ", ")
            
            email_values = {
                'attachment_ids': [], # Зөвхөн нэхэмжлэлийн .pdf файлыг attach хийхийн тулд 'attachment_ids' талбарыг хоослов.
                'email_cc': partner_mails,
                'reply_to': invoice.user_id.sudo().partner_id.sudo().email
            }
            
            # Мэйлийг илгээх
            try:
                self.template_id.send_mail(invoice.id, force_send=True, raise_exception=True, email_values=email_values)
                invoice.message_post(message_type='comment',
                                     body=_('Mail has sent to belown partners: %s') %(invoice.partner_id.email + ((", " + partner_mails) if partner_mails else "")))
                invoice.sent = True
                if not results['success']['have']:
                    results['success']['have'] = True
                results['success']['msg'] += "*\t\t" + ((invoice.number or '') + "\t\t-->\t\t" + invoice.partner_id.email + (("\t\t-->\t\t" + partner_mails) if partner_mails else '') + "\n")
            except Exception as e:
                se = _serialize_exception(e)
                if not results['failed']['have']:
                    results['failed']['have'] = True
                results['failed']['msg'] += "*\t\t" + ((invoice.number or '') + "\t\t-->\t\t" + str(se) + "\n")
         
        for failed_invoice in self.invoice_ids.filtered(lambda x: not x.partner_id or (x.partner_id and not x.partner_id.email)):
            if not results['completely_failed']['have']:
                results['completely_failed']['have'] = True
            results['completely_failed']['msg'] += "*\t\t" + ((failed_invoice.number or '') + "\t\t-->\t\t" + (failed_invoice.partner_id.name if failed_invoice.partner_id else '') + "\n")
               
        result = ''
        if results['success']['have']:
            result += _('Belown invoices SUCCESSFULLY sent:\n%s') % results['success']['msg']
        if results['failed']['have']:
            result += _('Belown invoices CAN\'T sent because of belown issue:\n%s') % results['failed']['msg']
        if results['completely_failed']['have']:
            result += _('Belown invoices CAN\'T sent because of invoice partner:\n%s') % results['completely_failed']['msg']

        return {
            'name': 'Message',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'custom.pop.message',
            'target': 'new',
            'context': {'default_name': result} 
        }

