# -*- coding: utf-8 -*-
{
    'name': "Mongolian Account Analytic Report",
    'version': '1.0',
    'depends': [
        'l10n_mn_account_base_report',
        'analytic',
        'l10n_mn_account_partner_report',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Analytic Account Modules',
    'description': """
         Санхүүгийн үндсэн тайлан
         - Гүйлгээ баланс тайланг шинжилгээний данстай хамт харна
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/account_transaction_balance_view.xml',
        'views/account_mongolian_balance_sheet_view.xml',
        'views/account_profit_report_view.xml',
        'views/account_cashflow_view.xml',
        'views/account_equity_change_report_view.xml',
        'views/account_partner_report_view.xml',
        'wizard/report_account_transaction_balance_view.xml',
        'wizard/report_account_partner_ledger_wizard.xml',
        'wizard/report_account_partner_balance_wizard.xml',
        'wizard/account_mongolian_balance_report_view.xml',
    ]
}
