# -*- encoding: utf-8 -*-
##############################################################################
import time
from datetime import datetime, timedelta

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.tools.safe_eval import safe_eval
from odoo.osv import expression


class AccountcashflowReport(models.Model):
    _inherit = 'account.cashflow.report'

    show_analytic_account = fields.Boolean(string='Show Analytic Account', default=False, track_visibility='onchange', states={'approved': [('readonly', True)]})
    show_analytic_line = fields.Boolean(string='Show Analytic Line', default=False, track_visibility='onchange', states={'approved': [('readonly', True)]})
    analytic_account_ids = fields.Many2many('account.analytic.account', 'acoount_cashflow_sheet_analytic_account', 'cashflow_report_id', 'analytic_account_id',
                                            string='Analytic Accounts', states={'approved': [('readonly', True)]})
    all_analytic_account_ids = fields.Many2many('account.analytic.account', 'acoount_cashflow_sheet_all_analytic_account', 'cashflow_report_id', 'analytic_account_id',
                                                string='All Analytic Accounts', states={'approved': [('readonly', True)]})

    @api.multi
    def write(self, vals):
        if 'show_analytic_account' in vals.keys():
            for obj in self:
                if vals['show_analytic_account'] != obj.show_analytic_account:
                    obj.line_ids.unlink()
        return super(AccountcashflowReport, self).write(vals)

    # Компани, эхлэх болон дуусах огноогоор нэр талбар бөглөгдөх
    @api.onchange('date_from', 'date_to', 'show_analytic_account')
    def onchange_account_balance(self):
        for report in self:
            name = ''
            if report.company_id:
                name += _('%s is ') % self.company_id.name
            if report.date_from:
                name += _('[%s - ') % self.date_from
            if report.date_to:
                name += _('%s] duration ') % self.date_to
            if report.show_analytic_account:
                name += _('Analytic Account ')
            else:
                report.show_analytic_line = False
            name += _('Cashflow account')
            report.name = name

    @api.multi
    def compute_all(self):
        self.line_ids.unlink()
        self.all_analytic_account_ids = None
        if self.show_analytic_account:
            self.compute_analytic()
        else:
            self.compute()

    @api.multi
    def _analytic_balance(self, sign, res, analytic_account_ids):
        # Журналын мөрөөс тухайн мөнгөн гүйгээний төрлөөр хайн нийт дүнг олох -- Хурдан болгохын тулд query болгов
        last_total = total = 0
        datetime_object = datetime.strptime(self.date_from, '%Y-%m-%d')
        last_date_from = '%s-01-01' % (datetime_object.year - 1)
        last_date_to = '%s-12-31' % (datetime_object.year - 1)
        cashflow_types = '(' + ', '.join(map(str, res)) + ')'
        self._cr.execute("SELECT b.id, SUM(b.bef_amount * %s) AS bef_amount , SUM(b.amount * %s) AS amount "
                         "FROM (SELECT a.id AS id, "
                                    "CASE WHEN a.type in ('positive', 'mixed') THEN -bef_amount ELSE bef_amount END AS bef_amount, "
                                    "CASE WHEN a.type in ('positive', 'mixed') THEN -amount ELSE amount END AS amount "
                                 "FROM (SELECT l.account_id AS id, t.value_of_amount_type AS type, "
                                             "CASE WHEN l.date BETWEEN '%s' AND '%s' THEN COALESCE(l.amount,0) ELSE 0.0 END AS bef_amount, "
                                             "CASE WHEN l.date BETWEEN '%s' AND '%s' THEN COALESCE(l.amount,0) ELSE 0.0 END AS amount "
                                        "FROM account_analytic_line l "
                                        "JOIN account_cashflow_type t ON (l.cashflow_id = t.id) "
                                        "WHERE l.account_id in %s AND l.cashflow_id in %s AND l.company_id = %s ) AS a) AS b "
                         "GROUP BY b.id "
                         "ORDER BY b.id " % (sign, sign, last_date_from, last_date_to, self.date_from, self.date_to, analytic_account_ids, cashflow_types, self.company_id.id))
        records = self._cr.fetchall()
        return records

    @api.multi
    def analytic_balance_cashflow(self, cashflow_type, analytic_account_ids):
        # Орох төрөл нь энгийн, нийлмэл болон хүүхдүүдийн нийлбэр тохиолдолд дүнг тооцоолох функц
        res = []
        res1 = []
        analytic_lines = []
        balances = []
        analytic_account = False
        analytic_last_total = analytic_total = 0
        last_total = total = 0
        # Орох төрөл нь нийлмэл
        if cashflow_type.report_cashflow_type == 'united':
            if cashflow_type.report_cashflow_ids:
                for cashflow in cashflow_type.report_cashflow_ids:
                    res += self._get_children_by_order(cashflow)
            if cashflow_type.minus_cashflow_ids:
                for cashflow in cashflow_type.minus_cashflow_ids:
                    res1 += self._get_children_by_order(cashflow)
        # Орох төрөл нь энгийн, хүүхдүүдийн нийлбэр
        else:
            res = self._get_children_by_order(cashflow_type)
        if res:
            balances = self._analytic_balance(1, res, analytic_account_ids)
        if res1:
            balances += self._analytic_balance(-1, res1, analytic_account_ids)
        if balances:
            balances.sort(key=lambda x: x[0])
            for balance in balances:
                last_total += balance[1]
                total += balance[2]
                if analytic_account and analytic_account != balance[0]:
                    analytic_lines.append((0, 0, { 'analytic_account_id': analytic_account,
                                                  'initial_balance': analytic_last_total,
                                                  'end_balance': analytic_total
                                                 }))
                    analytic_last_total = analytic_total = 0
                analytic_last_total += balance[1]
                analytic_total += balance[2]
                analytic_account = balance[0]
            analytic_lines.append((0, 0, {'analytic_account_id': analytic_account,
                                         'initial_balance': analytic_last_total,
                                         'end_balance': analytic_total
                                         }))
        return last_total, total, analytic_lines

    def get_analytic_account_ids(self):
        self.ensure_one()
        analytic_account_ids = self.analytic_account_ids
        if not analytic_account_ids:
            self._cr.execute("""SELECT  l.account_id 
                                         FROM account_analytic_line l  
                                         JOIN account_analytic_account aa ON (aa.id = l.account_id) 
                                         JOIN account_account a ON (a.id = l.general_account_id) 
                                         WHERE  l.company_id = %s AND l.date <= '%s' 
                                         GROUP BY l.account_id 
                                         ORDER BY l.account_id """ % (self.company_id.id, self.date_to))
            analytic_accounts = map(lambda x: x[0], self._cr.fetchall())
            if analytic_accounts:
                analytic_account_ids = self.env['account.analytic.account'].search([('id', 'in', analytic_accounts)])
        return analytic_account_ids
    
    @api.multi
    def compute_analytic(self):
        # Шинжилгээний дансны хувьд тооцоолол хийх функц
        line_obj = self.env['account.cashflow.report.line']
        # Шинжилгээний данснууд шалгана
        analytic_account_ids = self.get_analytic_account_ids()
        self.all_analytic_account_ids = [(4, analytic, False) for analytic in analytic_account_ids.ids] if analytic_account_ids else False
        if analytic_account_ids:
            analytic_account_ids = '(' + ', '.join(map(str, analytic_account_ids.ids)) + ')'
            # Шинжилгээний дансны дүнгийн dic
            # Тооцоолол хийх функц
            self.line_ids.unlink()
            cashflow_types = self.env['account.cashflow.type'].search([('company_id', '=', self.company_id.id), ('code', '!=', 'x')], order='sequence, code')
            for cashflow_type in cashflow_types:
                analytic_lines = []
                line_type = 'parent'
                if cashflow_type.report_cashflow_type == 'simple':
                    color = 'black'
                    line_type = 'view'
                elif cashflow_type.report_cashflow_type == 'child_sum':
                    color = 'blue'
                else:
                    color = 'bold'
                if cashflow_type.report_cashflow_type == 'display':
                    initial = end = ''
                    initial_balance = end_balance = 0
                elif cashflow_type.report_cashflow_type == 'initial':
                    account_balance = self.account()
                    initial = "{:,.2f}".format(account_balance[0] or 0)
                    initial_balance = account_balance[0] or 0
                    end = "{:,.2f}".format(account_balance[2] or 0)
                    end_balance = account_balance[2] or 0
                elif cashflow_type.report_cashflow_type == 'end':
                    account_balance = self.account()
                    initial = "{:,.2f}".format(account_balance[1] or 0)
                    initial_balance = account_balance[1] or 0
                    end = "{:,.2f}".format(account_balance[3] or 0)
                    end_balance = account_balance[3] or 0
                else:
                    balance = self.analytic_balance_cashflow(cashflow_type, analytic_account_ids)
                    initial = "{:,.2f}".format(balance[0] or 0)
                    initial_balance = balance[0] or 0
                    end = "{:,.2f}".format(balance[1] or 0)
                    end_balance = balance[1] or 0
                    analytic_lines = balance[2]
                line_obj.create({'line_number': cashflow_type.code,
                                 'cashflow_report_id': self.id,
                                 'cashflow_id': cashflow_type.id,
                                 'name': cashflow_type.name,
                                 'initial_balance': initial_balance,
                                 'end_balance': end_balance,
                                 'initial': initial,
                                 'end': end,
                                 'color': color,
                                 'line_type': line_type,
                                 'analytic_account_ids': analytic_lines})
        return True


class AccountCashflowReportLine(models.Model):
    _inherit = 'account.cashflow.report.line'

    analytic_account_id = fields.Many2one('account.analytic.account', string='Analytic Account')
    show_analytic_account = fields.Boolean(related='cashflow_report_id.show_analytic_account', string='Show Analytic Account')
    analytic_account_ids = fields.One2many('account.analytic.account.cashflow.line', 'analytic_cashflow_line', string='Analytic Account Cashflow Line')

    @api.multi
    def button_initial_analytic(self):
        # Эхний үлдэгдлийн шинжилгээний бичилтүүдийг харуулах
        if self.cashflow_report_id.show_analytic_account:
            for line in self:
                datetime_object = datetime.strptime(line.cashflow_report_id.date_from, '%Y-%m-%d')
                dateyear = '%s-01-01' % (datetime_object.year - 1)
                lastyear = '%s-12-31' % (datetime_object.year - 1)
                domain = [('move_id.cashflow_id', '=', line.cashflow_id.id),
                          ('date', '>=', dateyear),('date', '<=', lastyear),
                          ('company_id', '=', line.cashflow_report_id.company_id.id)]

                return {
                    'name': _('Analytic Entry Lines'),
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'res_model': 'account.analytic.line',
                    'view_id': False,
                    'type': 'ir.actions.act_window',
                    'context': {'group_by': ['cashflow_id', 'account_id']},
                    'domain': domain
                }
        else:
            # Хугацааны хоорогдох журналын мөрүүдийг харуулах
            for line in self:
                domain = [('cashflow_id', '=', line.cashflow_id.id), ('date', '>=', line.cashflow_report_id.date_from),
                          ('date', '<=', line.cashflow_report_id.date_to)]

                return {
                    'name': _('Journal Entry Lines'),
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'res_model': 'account.move.line',
                    'view_id': False,
                    'type': 'ir.actions.act_window',
                    'context': {'group_by': ['cashflow_id', 'account_id']},
                    'domain': domain
                }

    @api.multi
    def button_end_analytic(self):
        if self.cashflow_report_id.show_analytic_account:
            for line in self:
                domain = [('move_id.cashflow_id', '=', line.cashflow_id.id),
                          ('date', '>=', line.cashflow_report_id.date_from),('date', '<=', line.cashflow_report_id.date_to),
                          ('company_id', '=', line.cashflow_report_id.company_id.id)]

                return {
                    'name': _('Analytic Entry Lines'),
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'res_model': 'account.analytic.line',
                    'view_id': False,
                    'type': 'ir.actions.act_window',
                    'context': {'group_by': ['cashflow_id', 'account_id']},
                    'domain': domain
                }
        else:
            # Хугацааны хоорогдох журналын мөрүүдийг харуулах
            for line in self:
                domain = [('cashflow_id', '=', line.cashflow_id.id), ('date', '>=', line.cashflow_report_id.date_from),
                          ('date', '<=', line.cashflow_report_id.date_to)]

                return {
                    'name': _('Journal Entry Lines'),
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'res_model': 'account.move.line',
                    'view_id': False,
                    'type': 'ir.actions.act_window',
                    'context': {'group_by': ['cashflow_id', 'account_id']},
                    'domain': domain
                }

class AccountCashflowReportLine(models.Model):
    _name = 'account.analytic.account.cashflow.line'

    analytic_cashflow_line = fields.Many2one('account.cashflow.report.line')
    analytic_account_id = fields.Many2one('account.analytic.account', string='Analytic Account')
    initial_balance = fields.Float(string="Initial Amount", digits=(16, 2), default=0)
    end_balance = fields.Float(string="End Amount", digits=(16, 2), default=0)