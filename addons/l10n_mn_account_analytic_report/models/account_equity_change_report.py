# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT

from odoo import api, fields, models, _

class AccountEquityChangeReport(models.Model):
    """
        Өмчийн өөрчлөлтийн тайлан шинжилгээний дансаар
    """
    _inherit = "account.equity.change.report"
    
    
    show_analytic_account = fields.Boolean('Show Analytic Account', track_visibility='onchange', states={'approved': [('readonly', True)]})
    analytic_account_ids = fields.Many2many('account.analytic.account', string="Analytic Accounts", states={'approved': [('readonly', True)]})
    
    # Компани, эхлэх болон дуусах огноогоор нэр талбар бөглөгдөх
    @api.onchange('date_from','date_to', 'show_analytic_account')
    def onchange_account_date(self):
        for report in self:
            name = ''
            if report.company_id:
                name += _('%s is ') % report.company_id.name
            if report.date_from:
                name += _('[%s - ') % report.date_from
            if report.date_to:
                name += _('%s] duration') % report.date_to
            if report.show_analytic_account:
                name += _(' Analytic Account ')
            name += _(' equity change report')
            report.name = name
            
    @api.multi
    def compute_all(self):
        self.line_ids = None
        if self.show_analytic_account:
            self.compute_analytic()
        else:
            self.compute()
            
    @api.model
    def get_analytic_balance(self, company_id, date_from, date_to, accounts, analytic_accounts):
        ''' Тухайн дансны тайлант хугацааны эхний үлдэгдэл, болон балансыг олно.
        '''
        where = " "
        if self.env.context.get('journal_ids', False):
            journal_ids = self.env.context.get('journal_ids', False)
            where += ' AND aml.journal_id in ('+','.join(map(str, journal_ids))+') '
 
        self.env.cr.execute("SELECT aa.id, SUM(a.bef_amount) AS bef_amount, SUM(a.deb_amount) AS deb_amount, SUM(a.cr_amount) AS cr_amount, SUM(a.amount) AS amount "
                         "FROM "
                         "(SELECT aal.account_id, "
                         "CASE WHEN aal.date <= %s THEN aal.amount ELSE 0.0 END AS bef_amount, "
                         "CASE WHEN aal.date >= %s and aal.date <= %s and aal.amount < 0 THEN aal.amount ELSE 0.0 END AS deb_amount, "
                         "CASE WHEN aal.date >= %s and aal.date <= %s and aal.amount > 0 THEN aal.amount ELSE 0.0 END AS cr_amount, "
                         "CASE WHEN aal.date >= %s and aal.date <= %s THEN aal.amount ELSE 0.0 END AS amount "
                         "FROM account_analytic_line aal "
                         "LEFT JOIN account_move_line aml ON aml.id = aal.move_id "
                         "WHERE aal.general_account_id in %s AND aal.account_id in %s AND aal.company_id = %s " + where + ") AS a "
                         "LEFT JOIN account_analytic_account aa ON aa.id = a.account_id "
                         "GROUP BY aa.id ", (date_from, date_from, date_to, date_from, date_to, date_from, date_to, tuple(accounts),  tuple(analytic_accounts), company_id))
        return self.env.cr.dictfetchall()

    def get_analytic_account_ids(self):
        self.ensure_one()
        if self.analytic_account_ids:
            analytic_account_ids = self.analytic_account_ids
        else:
            analytic_account_ids = self.env['account.analytic.account'].search([])
        return analytic_account_ids

    @api.multi
    def compute_analytic(self):
        # Тооцоолол хийх функц
        self.line_ids = None
        account_ids = []
        journal_ids = []
        account_obj = self.env['account.account']
        journal_obj = self.env['account.journal']
        move_line_obj = self.env['account.move.line']
        line_obj = self.env['account.equity.change.report.line']
        row_value = [_(''),
                _('The effects of changes in accounting policies and correction of errors'),
                _('The corrected balance'),
                _('Net profit (loss) for the period'),
                _('Other comprehensive income'),
                _('Changes in equity'),
                _('Dividends declared'),
                _('Implemented the revaluation surplus'),
                _('')
                ]
        j = 2
        i = 1
        value_dict = {}
        company = self.company_id
        for report in self:
            statutory_fund_account = report.statutory_fund_account
            treasury_stock_account = report.treasury_stock_account
            additional_capital_account = report.additional_capital_account
            revaluation_addition_account = report.revaluation_addition_account
            foreign_currency_conversion_account = report.foreign_currency_conversion_account
            stockholders_equity_remainder_account = report.stockholders_equity_remainder_account
            previous_retained_earnings_account = report.previous_retained_earnings_account
            period_retained_earnings_account = report.period_retained_earnings_account
            dividend_expense_account = report.dividend_expense_account
            adjustment_journal = report.adjustment_journal
            equity_changes_journal = report.equity_changes_journal
    
            init_account_ids = []
            skip_earnings_journals = []
            if report.adjustment_journal:
                skip_earnings_journals.append(report.adjustment_journal.id)
            if report.equity_changes_journal:
                skip_earnings_journals.append(report.equity_changes_journal.id)
            if skip_earnings_journals:
                earning_journals = journal_obj.search([('id', 'not in', skip_earnings_journals)])
            
            date_start = datetime.strptime(report.date_from, "%Y-%m-%d")
            day = date_start.day
            month = date_start.month
            year = date_start.year
            date_from = (date(year, month, day) - relativedelta(years=1))
            date_to = (date(year, month, day) - relativedelta(days=1))
            i = 0
            j = 2
            num = 1
            total_treasury_stock_amount = total_equity_amount = total_additional_capital_amount = total_revaluation_addition_amount = total_foreign_currency_conversion_amount = 0
            total_stockholders_equity_remainder_amount = total_accumulated_profit_amount = 0
            
            analytic_account_ids = self.get_analytic_account_ids()
            analytic_account_ids = analytic_account_ids.ids if analytic_account_ids else []
            if not analytic_account_ids:
                return False
            
            while i < j:
                # ==================================================================
                # Эхний үлдэгдэл
                initial_equity_balance = end_equity_balance = 0
                initial_treasury_stock_balance = end_treasury_stock_balance = 0
                initial_additional_capital_balance = end_additional_capital_balance = 0
                initial_revaluation_addition_balance = end_revaluation_addition_balance = 0
                initial_foreign_currency_conversion_balance = end_foreign_currency_conversion_balance = 0
                initial_stockholders_equity_remainder_balance = end_stockholders_equity_remainder_balance = 0
                initial_accumulated_profit_balance = end_accumulated_profit_balance = 0
                initial_previous_retained_earnings_balance = initial_period_retained_earnings_balance = 0
                #НББ-н бодлогын өөрчлөлт мөрийн утга хадгалах хувьсагчууд
                coe_accumulated_profit_amount = coe_previous_retained_earning_amount = coe_period_retained_earnings_amount = 0
                #Тайлант үеийн цэвэр ашиг алдагдал мөрийн утга хадгалах хувьсагч
                profit_accumulated_profit_amount = 0
                #Бусад дэлгэрэнгүй орлого мөрийн утга хадгалах хувьсагчид
                other_stockholders_equity_remainder_amount = other_foreign_currency_conversion_amount = other_revaluation_addition_amount = 0
                #Өмчид гарсан өөрчлөлт хувьцаа мөрийн утга хадгалах хувьсагч
                eqch_equity_amount = eqch_treasury_stock_amount = eqch_additional_capital_amount = eqch_revaluation_addition_amount =  0
                eqch_foreign_currency_conversion_amount = eqch_stockholders_equity_remainder_amount = eqch_accumulated_profit_amount = 0
                equity_previous_retained_earning_amount = equity_period_retained_earnings_amount = 0
                #Зарласан ногдол ашиг мөрийн утга хадгалах хувьсагч
                dividend_expense_accumulated_profit_amount = 0
                #Дахин үнэлгээний нэмэгдлийн хэрэгжсэн дүн мөрийн утга хадгалах хувьсагч
                revaluation_additional_capital_amount = 0
                
                equity_amount = treasury_stock_amount = additional_capital_amount = revaluation_addition_amount = foreign_currency_conversion_amount = 0
                """Эхний үлдэгдлийн тооцоолол 
                        Эхлэл
                """
                init_journal_ids = []
                # Өмч баганы эхний үлдэгдэл
                if statutory_fund_account:
                    initial_bal = self.get_analytic_balance(company.id, date_from, date_to,[statutory_fund_account.id], analytic_account_ids)
                    if initial_bal:
                        initial_equity_balance = initial_bal[0]['bef_amount']
                    init_account_ids.append(statutory_fund_account.id)
                # Халаасны хувьцаа баганы эхний үлдэгдэл
                if treasury_stock_account:
                    initial_bal = self.get_analytic_balance(company.id, date_from, date_to,[treasury_stock_account.id], analytic_account_ids)
                    if initial_bal:
                        initial_treasury_stock_balance = initial_bal[0]['bef_amount']
                    init_account_ids.append(treasury_stock_account.id)
                # Нэмж төлөгдсөн капитал баганы эхний үлдэгдэл
                if additional_capital_account:
                    initial_bal = self.get_analytic_balance(company.id, date_from, date_to,[additional_capital_account.id], analytic_account_ids)
                    if initial_bal:
                        initial_additional_capital_balance = initial_bal[0]['bef_amount']
                    init_account_ids.append(additional_capital_account.id)
                # Хөрөнгийн дахин үнэлгээний баганы эхний үлдэгдэл
                if revaluation_addition_account:
                    initial_bal = self.get_analytic_balance(company.id, date_from, date_to,[revaluation_addition_account.id], analytic_account_ids)
                    if initial_bal:
                        initial_revaluation_addition_balance = initial_bal[0]['bef_amount']
                    init_account_ids.append(revaluation_addition_account.id)
                # Гадаад валютын хөрвүүлэлтийн нөөц баганы эхний үлдэгдэл
                if foreign_currency_conversion_account:
                    initial_bal = self.get_analytic_balance(company.id, date_from, date_to,[foreign_currency_conversion_account.id], analytic_account_ids)
                    if initial_bal:
                        initial_foreign_currency_conversion_balance = initial_bal[0]['bef_amount']
                    init_account_ids.append(foreign_currency_conversion_account.id)
                # Эздийн өмчийн бусад хэсэг баганы эхний үлдэгдэл
                if stockholders_equity_remainder_account:
                    initial_bal = self.get_analytic_balance(company.id, date_from, date_to,[stockholders_equity_remainder_account.id], analytic_account_ids)
                    if initial_bal:
                        initial_stockholders_equity_remainder_balance = initial_bal[0]['bef_amount']
                    init_account_ids.append(stockholders_equity_remainder_account.id)
                #Өмчийн өөрчлөлтийн тайлангийн эхний үлдэгдлийн хуримтлагдсан ашиг нь өмнөх үеийн хуримтлагдсан ашиг + тайлант үеийн хуримтлагдсан ашиг байдаг.
                #Өмнөх үеийн хуримтлагдсан ашиг дансны эхний үлдэгдэл
                if previous_retained_earnings_account:
                    initial_bal = self.get_analytic_balance(company.id, date_from, date_to,[previous_retained_earnings_account.id], analytic_account_ids)
                    if initial_bal:
                        initial_previous_retained_earnings_balance = initial_bal[0]['bef_amount']
                    init_account_ids.append(previous_retained_earnings_account.id)
                        
                #Тайлант үеийн хуримтлагдсан ашиг дансыг сонгосон эсэхийг шалгаж байна.
                if period_retained_earnings_account:
                    #Тайлант үеийн хуримтлагдсан ашиг дансан дээрх утгуудыг авч байна.
                    initial_bal = self.get_analytic_balance(company.id, date_from, date_to,[period_retained_earnings_account.id], analytic_account_ids)
                    if initial_bal:
                        #Өмнөх үеийн хуримтлагдсан ашиг дээр тайлант үеийн хуримтлагдсан ашгийг нэмж байна.
                        initial_period_retained_earnings_balance += initial_bal[0]['bef_amount']
                    init_account_ids.append(period_retained_earnings_account.id)

                # Хуримтлагдсан ашиг баганы эхний үлдэгдэл
                initial_accumulated_profit_balance = initial_previous_retained_earnings_balance + initial_period_retained_earnings_balance
                
                """Эхний үлдэгдлийн тооцоолол 
                        Төгсгөл
                """
                #==================================================================================================================
                """НББ-н бодлогын өөрчлөлт мөрийн утга тооцоолол
                        Эхлэл
                """
                coe_account_ids = []
                            
                #ННБ-н бодлогын хуримтлагдсан ашиг нь өмнөх үеийн хуримтлагдсан ашиг + тайлант үеийн хуримтлагдсан ашиг байдаг.
                #Өмнөх үеийн хуримтлагдсан ашиг дансны тухайн огнооны гүйлгээ
                if previous_retained_earnings_account and adjustment_journal.id:
                    previous_retained_earnings_line = self.with_context(journal_ids=[adjustment_journal.id]).get_analytic_balance(company.id, date_from, date_to,[previous_retained_earnings_account.id], analytic_account_ids)
                    if previous_retained_earnings_line:
                        for l in previous_retained_earnings_line:
                            coe_previous_retained_earning_amount += l['amount']
                            coe_account_ids.append(previous_retained_earnings_account.id)                        
                # Хуримтлагдсан ашиг багана
                coe_accumulated_profit_amount = coe_previous_retained_earning_amount
                
                """НББ-н бодлогын өөрчлөлт мөрийн утга тооцоолол
                        Төгсгөл
                """
                #==================================================================================================================
                
                """ Тайлант үеийн цэвэр ашиг(алдагдал) мөрийн тооцоолол
                        Эхлэл
                """
                profit_account_ids = []
                # Хуримтлагдсан ашиг багана
                if period_retained_earnings_account:
                    if skip_earnings_journals:
                        line = self.get_analytic_balance(company.id, date_from, date_to,[period_retained_earnings_account.id], analytic_account_ids)
                    if line:
                        for l in line:
                            profit_accumulated_profit_amount += l['amount']
                        profit_account_ids.append(period_retained_earnings_account.id)
                            
                """Төгсгөл"""
                #==================================================================================================================
                
                """ Бусад дэлгэрэнгүй орлого мөрийн тооцоолол
                        Эхлэл
                """
                other_account_ids = []
                            
                #Гадаад валютын хөрвүүлэлтийн нөөц багана
                if foreign_currency_conversion_account:
                    line = self.get_analytic_balance(company.id, date_from, date_to,[foreign_currency_conversion_account.id], analytic_account_ids)
                    if line:
                        for l in line:
                            other_foreign_currency_conversion_amount += l['amount']
                        other_account_ids.append(foreign_currency_conversion_account.id)
                            
                #Эздийн өмчийн бусад хэсэг багана
                if stockholders_equity_remainder_account:
                    line = self.get_analytic_balance(company.id, date_from, date_to,[stockholders_equity_remainder_account.id], analytic_account_ids)
                    if line:
                        for l in line:
                            other_stockholders_equity_remainder_amount += l['amount']
                        other_account_ids.append(stockholders_equity_remainder_account.id)
                        
                #Дахин үнэлгээний нэмэгдэл багана
                if revaluation_addition_account:
                    line = self.get_analytic_balance(company.id, date_from, date_to,[revaluation_addition_account.id], analytic_account_ids)
                    if line:
                        for l in line:
                            other_revaluation_addition_amount += l['cr_amount']
                        other_account_ids.append(revaluation_addition_account.id)
                """Төгсгөл"""
                #==================================================================================================================
                
                """ Өмчид гарсан өөрчлөлт мөрийн тооцоолол
                        Эхлэл
                """      
                
                eqch_account_ids = []         
                # Өмч багана
                if statutory_fund_account:
                    line = self.get_analytic_balance(company.id, date_from, date_to,[statutory_fund_account.id], analytic_account_ids)
                    if line:
                        for l in line:
                            eqch_equity_amount += l['amount']
                        eqch_account_ids.append(statutory_fund_account.id)
                            
                # Халаасны хувьцаа багана
                if treasury_stock_account:
                    line = self.get_analytic_balance(company.id, date_from, date_to,[treasury_stock_account.id], analytic_account_ids)
                    if line:
                        for l in line:
                            eqch_treasury_stock_amount += l['amount']
                        eqch_account_ids.append(treasury_stock_account.id)
                            
                # Нэмж төлөгдсөн капитал багана 
                if additional_capital_account:
                    line = self.get_analytic_balance(company.id, date_from, date_to,[additional_capital_account.id], analytic_account_ids)
                    if line:
                        for l in line:
                            eqch_additional_capital_amount += l['amount']
                        eqch_account_ids.append(additional_capital_account.id)
                            
                # Хуримтлагдсан ашиг баганы Өмчид гарсан өөрчлөлт мөр = өмнөх үеийн хуримтлагдсан ашиг + тайлант үеийн хуримтлагдсан ашиг байдаг.
                #Өмнөх үеийн хуримтлагдсан ашиг дансны тухайн огнооны гүйлгээ
                if previous_retained_earnings_account and skip_earnings_journals:
                    line = self.with_context(journal_ids=equity_changes_journal.ids).get_analytic_balance(company.id, date_from, date_to,[previous_retained_earnings_account.id], analytic_account_ids)
                    if line:
                        for l in line:
                            equity_previous_retained_earning_amount += l['amount']
                        eqch_account_ids.append(previous_retained_earnings_account.id)
                        
                # Хуримтлагдсан ашиг багана
                eqch_accumulated_profit_amount = equity_previous_retained_earning_amount
                
                """Төгсгөл"""
                #==================================================================================================================
                
                """ Зарласан ногдол ашиг мөрийн тооцоолол
                        Эхлэл
                """   
                dividend_expense_account_ids = []   
                #Хуримтлагдсан ашиг багана
                if dividend_expense_account and skip_earnings_journals:
                    line = self.with_context(journal_ids=earning_journals.ids).get_analytic_balance(company.id, date_from, date_to,[dividend_expense_account.id], analytic_account_ids)
                    if line:
                        for l in line:
                            dividend_expense_accumulated_profit_amount += l['cr_amount']
                        dividend_expense_accumulated_profit_amount = dividend_expense_accumulated_profit_amount * (-1)
                        dividend_expense_account_ids.append(period_retained_earnings_account.id)
                """Төгсгөл"""
                #==================================================================================================================
                
                """ Дахин үнэлгээний нэмэгдлийн хэрэгжсэн дүн мөрийн тооцоолол
                        Эхлэл
                """   
                revaluation_addition_account_ids = []  
                
                #Нэмж төлөгдсөн капитал багана
                if revaluation_addition_account:
                    line = self.get_analytic_balance(company.id, date_from, date_to,[revaluation_addition_account.id], analytic_account_ids)
                    if line:
                        for l in line:
                            revaluation_additional_capital_amount += l['deb_amount']
                        revaluation_addition_account_ids.append(revaluation_addition_account.id)
                """Төгсгөл"""
                
                #Мөрүүдийг тооцож үүсгэх
                
                date_from_obj = date_from
                date_to_obj = date_to
                if i == 1:
                    initial_equity_balance = total_equity_amount
                    initial_treasury_stock_balance = total_treasury_stock_amount
                    initial_additional_capital_balance = total_additional_capital_amount
                    initial_revaluation_addition_balance = total_revaluation_addition_amount
                    initial_foreign_currency_conversion_balance = total_foreign_currency_conversion_amount
                    initial_stockholders_equity_remainder_balance = total_stockholders_equity_remainder_amount
                    initial_accumulated_profit_balance = total_accumulated_profit_amount
                for row in row_value:
                    is_sum = True
                    line_date_from = line_date_to = ''
                    if num == 1 or num == 2 or num == 9 or num == 10 or num ==12:
                        is_sum = False
                    equity_amount = treasury_stock_amount = additional_capital_amount = revaluation_addition_amount = foreign_currency_conversion_amount = 0
                    stockholders_equity_remainder_amount = accumulated_profit_amount = total_amount = 0
                    account_ids = []
                    journal_ids = []
                    name = row
                    if num == 1:
                        name += _(' %s year') % date_from_obj.strftime('%Y')
                        name += _(' %s month') % date_from_obj.strftime('%m')
                        name += _(' %s day') % date_from_obj.strftime('%d')
                        equity_amount = initial_equity_balance
                        treasury_stock_amount = initial_treasury_stock_balance
                        additional_capital_amount = initial_additional_capital_balance
                        revaluation_addition_amount = initial_revaluation_addition_balance
                        foreign_currency_conversion_amount = initial_foreign_currency_conversion_balance
                        stockholders_equity_remainder_amount = initial_stockholders_equity_remainder_balance
                        accumulated_profit_amount = initial_accumulated_profit_balance
                        account_ids = init_account_ids
                        line_date_from = date_from
                    elif num == 2 or num == 11:
                        accumulated_profit_amount = coe_accumulated_profit_amount
                        line_date_from = date_from
                        line_date_to = date_to
                        account_ids = coe_account_ids
                    elif num == 3 or num == 12:
                        equity_amount = initial_equity_balance
                        treasury_stock_amount = initial_treasury_stock_balance
                        additional_capital_amount = initial_additional_capital_balance
                        revaluation_addition_amount = initial_revaluation_addition_balance
                        foreign_currency_conversion_amount = initial_foreign_currency_conversion_balance
                        stockholders_equity_remainder_amount = initial_stockholders_equity_remainder_balance
                        accumulated_profit_amount = coe_accumulated_profit_amount + initial_accumulated_profit_balance
                        account_ids = init_account_ids
                        line_date_from = date_from
                        line_date_to = date_to
                    elif num == 4 or num == 13:
                        accumulated_profit_amount = profit_accumulated_profit_amount
                        account_ids = profit_account_ids
                        line_date_from = date_from
                        line_date_to = date_to
                    elif num == 5 or num == 14:
                        foreign_currency_conversion_amount = other_foreign_currency_conversion_amount
                        stockholders_equity_remainder_amount = other_stockholders_equity_remainder_amount
                        revaluation_addition_amount = other_revaluation_addition_amount
                        account_ids = other_account_ids
                        line_date_from = date_from
                        line_date_to = date_to
                    elif num == 6 or num == 15:
                        equity_amount = eqch_equity_amount
                        treasury_stock_amount = eqch_treasury_stock_amount
                        additional_capital_amount = eqch_additional_capital_amount
                        accumulated_profit_amount = eqch_accumulated_profit_amount
                        account_ids = eqch_account_ids
                        if equity_changes_journal:
                            journal_ids.append(equity_changes_journal.ids)
                        line_date_from = date_from
                        line_date_to = date_to
                    elif num == 7 or num == 16:
                        accumulated_profit_amount = dividend_expense_accumulated_profit_amount
                        account_ids = dividend_expense_account_ids
                        if skip_earnings_journals:
                            journal_ids.append(earning_journals.ids)
                        line_date_from = date_from
                        line_date_to = date_to
                    elif num == 8 or num == 17:
                        revaluation_addition_amount = revaluation_additional_capital_amount
                        account_ids = revaluation_addition_account_ids
                        line_date_from = date_from
                        line_date_to = date_to
                    elif num == 9 or num ==18 :
                        name += _(' %s year') % date_to_obj.strftime('%Y')
                        name += _(' %s month') % date_to_obj.strftime('%m')
                        name += _(' %s day') % date_to_obj.strftime('%d')
                        equity_amount = total_equity_amount
                        treasury_stock_amount = total_treasury_stock_amount
                        additional_capital_amount = total_additional_capital_amount
                        revaluation_addition_amount = total_revaluation_addition_amount
                        foreign_currency_conversion_amount = total_foreign_currency_conversion_amount
                        stockholders_equity_remainder_amount = total_stockholders_equity_remainder_amount
                        accumulated_profit_amount = total_accumulated_profit_amount
                        account_ids = init_account_ids
                        line_date_from = date_from
                        line_date_to = date_to
                    if num != 10:
                        total_amount = equity_amount + treasury_stock_amount + additional_capital_amount + revaluation_addition_amount + foreign_currency_conversion_amount + stockholders_equity_remainder_amount + accumulated_profit_amount
                        if is_sum == True:
                            total_equity_amount += equity_amount
                            total_treasury_stock_amount += treasury_stock_amount
                            total_additional_capital_amount += additional_capital_amount
                            total_revaluation_addition_amount += revaluation_addition_amount
                            total_foreign_currency_conversion_amount += foreign_currency_conversion_amount
                            total_stockholders_equity_remainder_amount += stockholders_equity_remainder_amount
                            total_accumulated_profit_amount += accumulated_profit_amount
                        vals = {
                            'equity_change_report_id':report.id,
                            'line_number':num,
                            'name': name,
                            'equity_amount': equity_amount,
                            'treasury_stock_amount': treasury_stock_amount,
                            "additional_capital_amount":additional_capital_amount,
                            'revaluation_addition_amount': revaluation_addition_amount,
                            'foreign_currency_conversion_amount': foreign_currency_conversion_amount,
                            'stockholders_equity_remainder_amount': stockholders_equity_remainder_amount,
                            'accumulated_profit_amount': accumulated_profit_amount,
                            'date_from': line_date_from,
                            'date_to': line_date_to,
                            'total_amount': total_amount,
                            'journal_ids':[(6, 0, list(journal_ids))],
                            'account_ids':[(6, 0, list(account_ids))],
                        }
                        line = line_obj.create(vals)
                    num += 1
                i += 1
                date_from = datetime.strptime(report.date_from, '%Y-%m-%d')
                date_to = datetime.strptime(report.date_to, '%Y-%m-%d')
        return True
    
class AccountEquityChangeReportLine(models.Model):
    _inherit = 'account.equity.change.report.line'
    
    
    def get_analytic_ids(self, company_id, date_from, date_to, accounts, analytic_accounts):
        ''' Тухайн дансны тайлант хугацааны эхний үлдэгдэл, болон балансыг олно.
        '''
        where = " "
        if self.env.context.get('journal_ids', False):
            journal_ids = self.env.context.get('journal_ids', False)
            where += ' AND aml.journal_id in ('+','.join(map(str, journal_ids))+') '
        if not date_to:
            where += "AND aal.date <= '" + date_from + "'"
        else:
            where += "AND aal.date >= '"+ date_from + "' AND aal.date <= '" + str(date_to) + "'"
            
        if accounts and analytic_accounts:
            self.env.cr.execute("SELECT a.amove_id "
                             "FROM "
                             "(SELECT aal.id as amove_id "
                             "FROM account_analytic_line aal "
                             "LEFT JOIN account_move_line aml ON aml.id = aal.move_id "
                             "WHERE aal.general_account_id in %s AND aal.account_id in %s AND aal.company_id = %s " + where + ") AS a ", (tuple(accounts),  tuple(analytic_accounts), company_id))
        return self.env.cr.dictfetchall()

    @api.multi
    def button_journal_entries(self):
        # Журналын мөрүүдийг харуулах
        result = super(AccountEquityChangeReportLine, self).button_journal_entries()
        
        for line in self:
            domain = []
            ids = []
            if line.equity_change_report_id.show_analytic_account:
                if self.equity_change_report_id.analytic_account_ids:
                    analytic_account_ids = line.equity_change_report_id.analytic_account_ids.ids
                else:
                    analytic_account_ids = self.env['account.analytic.account'].search([]).ids
                analytic_line = self.with_context(journal_ids=line.journal_ids.ids).get_analytic_ids(line.equity_change_report_id.company_id.id, line.date_from, line.date_to,line.account_ids.ids, analytic_account_ids)
                if analytic_line:
                    for l in analytic_line:
                        ids.append(l['amove_id'])
                if ids:
                    domain = [('id', 'in', ids)]
                return {
                    'name': _('Analytic Journal Entry Lines'),
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'res_model': 'account.analytic.line',
                    'view_id': False,
                    'type': 'ir.actions.act_window',
                    'context': {'group_by': ['general_account_id'],'group_by': ['account_id']},
                    'domain': domain
                }
            else:
                return result