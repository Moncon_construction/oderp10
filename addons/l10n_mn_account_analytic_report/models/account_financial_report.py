# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models, fields

# ---------------------------------------------------------
# Account Financial Report
# ---------------------------------------------------------


class AccountFinancialReport(models.Model):
    _inherit = "account.financial.report"

    def _compute_sum(self, res):
        '''
        Санхүүгийн тайлангийн "дэлгэц" болон "тайлангийн утга" төрлийн шинжилгээний дансны нийлбэрийг гаргах
        '''
        sum = {}
        sum_list = []
        for key, value in res.items():
            for val in value:
                if val[0] not in sum:
                    sum[val[0]] = {'bef': 0, 'am': 0}
                sum[val[0]]['bef'] += val[1]
                sum[val[0]]['am'] += val[2]
        for key, value in sum.items():
            sum_list.append((key, value['bef'], value['am']))
        return sum_list

    def _compute_analytic_balance(self, accounts, analytic_accounts, company_ids, date_from, date_to, sign):
        '''
        Шинжилгээний бичилтээс тухайн санхүүгийн данстай холбоотой дүнг гаргах
        '''
        accs = '(' + ','.join(map(str, accounts.ids)) + ')'
        sn = 1      # Тайлангийн тэмдэг
        if sign == 1:
            sn = -1
        self._cr.execute("SELECT aa.id, SUM(a.bef_amount) AS bef_amount, SUM(a.amount) AS amount "
                         "FROM "
                         "(SELECT account_id, "
                         "CASE WHEN date <= '%s' THEN COALESCE(amount * %s,0) ELSE 0.0 END AS bef_amount, "
                         "CASE WHEN date <= '%s' THEN COALESCE(amount * %s,0) ELSE 0.0 END AS amount "
                         "FROM account_analytic_line "
                         "WHERE general_account_id in %s AND account_id in %s AND company_id in %s) AS a "
                         "LEFT JOIN account_analytic_account aa ON (aa.id = a.account_id) "
                         "GROUP BY aa.id "
                         "ORDER BY aa.id " % (date_from, sn, date_to, sn, accs, analytic_accounts, company_ids))
        records = self._cr.fetchall()
        return records

    def _compute_report_analytic(self, reports, analytic_accounts, company_ids, date_from, date_to):
        '''
        Санхүүгийн тайланг шинжилгээний дансаар гаргах
        Санхүүгийн тайлангийн 4 тарлаас хамааран гаргана
        '''

        res = {}
        for report in reports:
            if report.type == 'accounts':
                if len(report.account_ids) > 0:
                    res[report.id] = self._compute_analytic_balance(report.account_ids, analytic_accounts, company_ids, date_from, date_to, report.sign)
                else:
                    res[report.id] = {}
            elif report.type == 'account_type':
                accounts = self.env['account.account'].search([('user_type_id', 'in', report.account_type_ids.ids)])
                if len(accounts) > 0:
                    res[report.id] = self._compute_analytic_balance(accounts, analytic_accounts, company_ids, date_from, date_to, report.sign)
                else:
                    res[report.id] = {}
            elif report.type == 'account_report':
                if len(report.account_report_id) > 0:
                    res2 = self._compute_report_analytic(report.account_report_id, analytic_accounts, company_ids, date_from, date_to)
                    res[report.id] = self._compute_sum(res2)
                else:
                    res[report.id] = {}
            elif report.type == 'sum':
                if len(report.children_ids) > 0:
                    res2 = self._compute_report_analytic(report.children_ids, analytic_accounts, company_ids, date_from, date_to)
                    res[report.id] = self._compute_sum(res2)
                else:
                    res[report.id] = {}
        return res
