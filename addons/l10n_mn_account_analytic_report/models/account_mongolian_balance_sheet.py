# -*- encoding: utf-8 -*-
##############################################################################
import time
from datetime import datetime, timedelta

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.tools.safe_eval import safe_eval
from odoo.osv import expression


class AccountMongolianBalanceSheet(models.Model):
    _inherit = 'account.mongolian.balance.sheet'

    show_analytic_account = fields.Boolean(string='Show Analytic Account', default=False, track_visibility='onchange', states={'approved': [('readonly', True)]})
    show_analytic_line = fields.Boolean(string='Show Analytic Line', default=False, track_visibility='onchange', states={'approved': [('readonly', True)]})
    analytic_account_ids = fields.Many2many('account.analytic.account', 'acoount_mongolian_balance_sheet_analytic_account', 'balance_id', 'analytic_account_id',
                                            string='Analytic Accounts', states={'approved': [('readonly', True)]})
    all_analytic_account_ids = fields.Many2many('account.analytic.account', 'acoount_mongolian_balance_sheet_all_analytic_account', 'balance_id', 'analytic_account_id',
                                                string='All Analytic Accounts', states={'approved': [('readonly', True)]})

    @api.multi
    def write(self, vals):
        if 'show_analytic_account' in vals.keys():
            for obj in self:
                if vals['show_analytic_account'] != obj.show_analytic_account:
                    obj.line_ids.unlink()
        return super(AccountMongolianBalanceSheet, self).write(vals)

    # Компани, эхлэх болон дуусах огноогоор нэр талбар бөглөгдөх
    @api.onchange('date_from', 'date_to', 'show_analytic_account')
    def onchange_account_balance(self):
        for report in self:
            name = ''
            if report.company_id:
                name += _('%s is ') % self.company_id.name
            if report.date_from:
                name += _('[%s - ') % self.date_from
            if report.date_to:
                name += _('%s] duration ') % self.date_to
            if report.show_analytic_account:
                name += _('Analytic Account ')
            else:
                report.show_analytic_line = False
            name += _('mongolian balance')
            report.name = name

    @api.multi
    def compute_all(self):
        self.line_ids = None
        self.all_analytic_account_ids = None
        if self.show_analytic_account:
            self.compute_analytic()
        else:
            self.compute()

    def get_analytic_account_ids(self):
        self.ensure_one()
        analytic_account_ids = self.analytic_account_ids
        if not analytic_account_ids:
            self._cr.execute("""SELECT  l.account_id 
                                 FROM account_analytic_line l 
                                 JOIN account_account a ON (a.id = l.general_account_id) 
                                 JOIN account_analytic_account aa ON (aa.id = l.account_id) 
                                 WHERE  aa.type = 'normal' AND aa.active = 't' AND 
                                        l.company_id = %s AND l.date <= '%s' AND 
                                        a.internal_type not in ('expense', 'income') 
                                 GROUP BY l.account_id 
                                 ORDER BY l.account_id """ % (self.company_id.id, self.date_to))
            analytic_accounts = map(lambda x: x[0], self._cr.fetchall())
            if analytic_accounts:
                analytic_account_ids = self.env['account.analytic.account'].search([('id', 'in', analytic_accounts)])
        return analytic_account_ids
    
    @api.multi
    def compute_analytic(self):
        # Шинжилгээний дансны хувьд тооцоолол хийх функц
        line_obj = self.env['account.mongolian.balance.sheet.line']
        if not self.account_report_id:
            raise UserError(_('Please select a financial report.'))
        # Шинжилгээний данснууд шалгана
        analytic_account_ids = self.get_analytic_account_ids()
        self.all_analytic_account_ids = [(4, analytic, False) for analytic in analytic_account_ids.ids] if analytic_account_ids else False
        if analytic_account_ids:
            analytic_account_ids = '(' + ', '.join(map(str, analytic_account_ids.ids)) + ')'
        # Тухайн тайлангийн бүтцэд хамаарах хүүхдүүдийг хайж байна.
        # Өмнөх өдрөөр эхний үлдэгдлийг татна
        before_day = datetime.strptime(self.date_from, DEFAULT_SERVER_DATE_FORMAT) + timedelta(days=-1)
        # Одоо байгаа үндсэн функцийг өөрчлөхгүйгээр, company_id-г list болгох
        company_id = []
        company_id.append(self.company_id.id)
        company_ids = '(' + ','.join(map(str, company_id)) + ')'
        end_report_ids = self.account_report_id.with_context(date_to=self.date_to, state=self.target_move, company_id=company_id)._get_children_by_order()
        parent_numbers = {}
        parent_index = 1
        for account in end_report_ids:
            if analytic_account_ids:
                # Шинжилгээний дансны дүнгийн dic
                analytics = self.env['account.financial.report']._compute_report_analytic(account, analytic_account_ids, company_ids,
                                                                                          before_day.strftime(DEFAULT_SERVER_DATE_FORMAT), self.date_to)
                for key, value in sorted(analytics.iteritems()):
                    if account.id == self.account_report_id.id:
                        continue
                    # Тайлангийн мөрийн дугаарыг тооцож байна.
                    number = parent_index
                    if account.parent_id.id not in parent_numbers:
                        parent_numbers[account.id] = [str(number), 0]
                        parent_index += 1
                    else:
                        number = parent_numbers[account.parent_id.id][0] + '.' + str(parent_numbers[account.parent_id.id][1] + 1)
                        parent_numbers[account.parent_id.id][1] += 1
                        parent_numbers[account.id] = [str(number), 0]
                    # Тайлангийн шатлалын дагуу мөрийн дугаарлалтыг олж байна
                    number_str = '%s' % number
                    if account.style_overwrite in [1, 2, 3]:
                        name_str = u'%s' % account.name or ''
                        color = 'blue'
                    else:
                        name_str = u'   %s' % account.name or ''
                        color = 'black'
                    # Үндсэн тайлангийн мөрийг зурна
                    line = line_obj.create({'line_number': number_str,
                                            'name': name_str,
                                            'financial_report_id': account.id,
                                            'type': account.type,
                                            'balance_id': self.id,
                                            'color': 'bold' if account.type in ('account_report','sum') and not account.account_report_id else color
                                            })
                    initial_total = end_total = seq = 0
                    if account.id == key and not (account.type in ('account_report','sum') and not account.account_report_id):
                        for val in value:
                            # Шинжилгээний мөрийг зурна
                            analytic_account = self.env['account.analytic.account'].browse(val[0])
                            name_str = u'     [%s] %s' % (analytic_account.code or '', analytic_account.name or '')
                            initial_total += val[1] or 0
                            end_total += val[2] or 0
                            seq += 1
                            line_number = number_str + '.' + '%s' % seq
                            line_obj.create({'line_number': line_number,
                                             'name': name_str,
                                             'financial_report_id': account.id,
                                             'parent_id': line.id,
                                             'type': account.type,
                                             'analytic_account_id': val[0],
                                             'initial': '%s' % "{:,.2f}".format(val[1] or 0),
                                             'end': '%s' % "{:,.2f}".format(val[2] or 0),
                                             'initial_balance': val[1] or 0,
                                             'end_balance': val[2] or 0,
                                             'balance_id': self.id,
                                             'color': 'gray'
                                             })
                        initial = end = ''
                        initial_balance = end_balance = 0
                        if not (account.type in ('account_report','sum') and not account.account_report_id):
                            initial = "{:,.2f}".format(initial_total or 0)
                            end = "{:,.2f}".format(end_total or 0)
                            initial_balance = initial_total or 0
                            end_balance = end_total or 0
                        line.write({'initial': '%s' % initial,
                                    'end': '%s' % end,
                                    'initial_balance': initial_balance,
                                    'end_balance': end_balance})
        return True


class AccountMongolianBalanceSheetLine(models.Model):
    _inherit = 'account.mongolian.balance.sheet.line'

    analytic_account_id = fields.Many2one('account.analytic.account', string='Analytic Account')
    parent_id = fields.Many2one('account.mongolian.balance.sheet.line', string='Parent')
    child_ids = fields.One2many('account.mongolian.balance.sheet.line', 'parent_id', string='Child')
    show_analytic_account = fields.Boolean(related='balance_id.show_analytic_account', string='Show Analytic Account')
    show_analytic_line = fields.Boolean(related='balance_id.show_analytic_line', string='Show Analytic Line')
    color = fields.Selection([('black', 'Black'),
                              ('bold', 'Bold'),
                              ('gray', 'Gray'),
                              ('blue', 'Blue')], string='Color', default='black')

    @api.multi
    def button_initial_analytic_entries(self):
        # Эхний үлдэгдлийн шинжилгээний бичилтүүдийг харуулах
        for line in self:
            if line.type == 'accounts':
                accounts = line.financial_report_id.account_ids
            elif line.type == 'account_type':
                accounts = self.env['account.account'].search([('user_type_id', 'in', line.financial_report_id.account_type_ids.ids), ('company_id', '=', line.balance_id.company_id.id)])
            domain = [('general_account_id', 'in', accounts.ids), ('date', '<', line.balance_id.date_from), ('company_id', '=', line.balance_id.company_id.id)]
            if line.analytic_account_id:
                domain = expression.AND([domain, [('account_id', '=', line.analytic_account_id.id)]])
            return {
                'name': _('Analytic Entry Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.analytic.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'context': {'group_by': ['general_account_id']},
                'domain': domain
            }

    @api.multi
    def button_end_analytic_entries(self):
        # Эцсийн үлдэгдлийн шинжилгээний бичилтүүдийг харуулах
        for line in self:
            if line.type == 'accounts':
                accounts = line.financial_report_id.account_ids
            elif line.type == 'account_type':
                accounts = self.env['account.account'].search([('user_type_id', 'in', line.financial_report_id.account_type_ids.ids), ('company_id', '=', line.balance_id.company_id.id)])
            domain = [('general_account_id', 'in', accounts.ids), ('date', '<=', line.balance_id.date_to), ('company_id', '=', line.balance_id.company_id.id)]
            if line.analytic_account_id:
                domain = expression.AND([domain, [('account_id', '=', line.analytic_account_id.id)]])
            return {
                'name': _('Analytic Entry Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.analytic.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'context': {'group_by': ['general_account_id']},
                'domain': domain
            }