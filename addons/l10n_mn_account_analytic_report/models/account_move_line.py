# -*- coding: utf-8 -*-
from odoo import api, models, _
from odoo.exceptions import ValidationError


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    @api.model
    def get_initial_balance_with_analytic(self, company_id, account_ids, analytic_account_ids, date_start, target_move):
        ''' Тухайн дансны тайлант хугацааны эхний үлдэгдлийг олно.
        '''
        where = " "
        join = " "
        select = " "
        sub_select = " "
        group_by = " "
        if target_move == 'posted':
            where += "AND m.state = 'posted' "
        if self.env.context.get('journal_ids', False):
            journal_ids = self.env.context.get('journal_ids', False)
            sub_select += ", ml.journal_id AS journal_id "
            select += ", j.id AS jid, j.name AS jname "
            join += "LEFT JOIN account_journal j ON (j.id = mv.journal_id) "
            group_by += ", j.id, j.name "
            where += ' AND ml.journal_id in (' + ','.join(map(str, journal_ids)) + ') '
        if self.env.context.get('partner_ids', False):
            partner_ids = self.env.context.get('partner_ids', False)
            sub_select += ", ml.partner_id AS partner_id "
            select += ", p.id AS pid, p.name AS pname "
            join += "LEFT JOIN res_partner p ON (p.id = mv.partner_id) "
            group_by += ", p.id, p.name "
            where += ' AND ml.partner_id in (' + ','.join(map(str, partner_ids)) + ') '
        query = ("SELECT mv.account_id AS account_id, aa.code AS code, mv.analytic_account_id AS analytic_account_id, "
                 "aaa.code AS analytic_account_code, aaa.name AS analytic_account_name, aa.name AS name, "
                 "cur.id AS currency_id, cur.name AS currency, "
                 "sum(mv.debit) AS start_debit, sum(mv.credit) AS start_credit, "
                 "sum(mv.cur_debit) AS cur_start_debit, sum(mv.cur_credit) AS cur_start_credit, "
                 "0 AS debit, 0 AS credit, 0 AS cur_debit, 0 AS cur_credit " + select + ""
                 "FROM  (SELECT  ml.account_id AS account_id, ml.analytic_account_id AS analytic_account_id, ml.debit AS debit, "
                 "ml.credit AS credit, "
                 "CASE WHEN ml.amount_currency > 0 "
                 "THEN ml.amount_currency ELSE 0 END AS cur_debit, "
                 "CASE WHEN ml.amount_currency < 0 "
                 "THEN abs(ml.amount_currency) ELSE 0 END AS cur_credit " + sub_select + ""
                 "FROM account_move_line ml "
                 "LEFT JOIN account_move m ON (ml.move_id = m.id) "
                 "WHERE m.date < '%s' AND ml.account_id in %s AND ml.analytic_account_id in %s " + where + ""
                 " AND ml.company_id = %s) AS mv "
                 "LEFT JOIN account_account aa ON (mv.account_id = aa.id) "
                 "LEFT JOIN account_analytic_account aaa ON mv.analytic_account_id = aaa.id "
                 "LEFT JOIN res_currency cur ON cur.id = aa.currency_id " + join + " "
                 "WHERE aa.req_analytic_account = 't' "
                 "GROUP BY mv.account_id, aa.code, aa.name, mv.analytic_account_id, aaa.code, aaa.name, "
                 "cur.id, cur.name " + group_by + ""
                 "ORDER BY aa.code, aa.name ") % (date_start, '(' + str(account_ids)[1:-1] + ')', '(' + str(analytic_account_ids)[1:-1] + ')', company_id)
        self.env.cr.execute(query)
        return self.env.cr.dictfetchall()

    @api.model
    def get_balance_with_analytic(self, company_id, account_ids, analytic_account_ids, date_start, date_stop, target_move, without_profit_revenue=False):
        ''' Тухайн дансны тайлант хугацааны хоорондох дүнг олно.
        '''
        where = " "
        join = " "
        select = " "
        sub_select = " "
        group_by = " "

        # check period journal
        if without_profit_revenue:
            company = self.env['res.company'].browse(company_id)
            if not company.period_journal_id:
                raise ValidationError(_('Please configure period journal on account settings.'))
            else:
                where += 'AND ml.journal_id != %s ' % company.period_journal_id.id

        if target_move == 'posted':
            where += "AND m.state = 'posted'"
        if self.env.context.get('journal_ids', False):
            journal_ids = self.env.context.get('journal_ids', False)
            sub_select += ", ml.journal_id AS journal_id "
            select += ", j.id AS jid, j.name AS jname "
            join += "LEFT JOIN account_journal j ON (j.id = mv.journal_id) "
            group_by += ", j.id, j.name "
            where += ' AND ml.journal_id in (' + ','.join(map(str, journal_ids)) + ') '

        if self.env.context.get('partner_ids', False):
            partner_ids = self.env.context.get('partner_ids', False)
            sub_select += ", ml.partner_id AS partner_id "
            select += ", p.id AS pid, p.name AS pname "
            join += "LEFT JOIN res_partner p ON (p.id = mv.partner_id) "
            group_by += ", p.id, p.name "
            where += ' AND ml.partner_id in (' + ','.join(map(str, partner_ids)) + ') '
        query = ("SELECT mv.account_id AS account_id, aa.code AS code, aa.name AS name, cur.id AS currency_id, cur.name AS currency, "
                 "mv.analytic_account_id AS analytic_account_id, aaa.code AS analytic_account_code, aaa.name AS analytic_account_name, "
                 "sum(mv.debit) AS debit, sum(mv.credit) AS credit, "
                 "sum(mv.cur_debit) AS cur_debit, sum(mv.cur_credit) AS cur_credit, "
                 "0 AS start_debit, 0 AS start_credit, 0 AS cur_start_debit, 0 AS cur_start_credit " + select + ""
                 "FROM (SELECT  ml.account_id AS account_id, ml.analytic_account_id AS analytic_account_id, ml.debit AS debit, "
                 "ml.credit AS credit, "
                 "CASE WHEN ml.amount_currency > 0 "
                 "THEN ml.amount_currency ELSE 0 END AS cur_debit, "
                 "CASE WHEN ml.amount_currency < 0 "
                 "THEN abs(ml.amount_currency) ELSE 0 END AS cur_credit " + sub_select + ""
                 "FROM account_move_line ml "
                 "LEFT JOIN account_move m ON (ml.move_id = m.id) "
                 "WHERE m.date BETWEEN '%s' AND '%s' AND ml.account_id in %s AND ml.analytic_account_id in %s " + where + ""
                 " AND ml.company_id = %s) AS mv "
                 "LEFT JOIN account_account aa ON (mv.account_id = aa.id) "
                 "LEFT JOIN account_analytic_account aaa ON (mv.analytic_account_id = aaa.id) "
                 "LEFT JOIN res_currency cur ON cur.id = aa.currency_id " + join + " "
                 "WHERE aa.req_analytic_account = 't' "
                 "GROUP BY mv.account_id, aa.code, aa.name, mv.analytic_account_id, aaa.code, aaa.name, "
                 "cur.id, cur.name " + group_by + ""
                 "ORDER BY aa.code, aa.name") % (date_start, date_stop, '(' + str(account_ids)[1:-1] + ')', '(' + str(analytic_account_ids)[1:-1] + ')', company_id)
        self.env.cr.execute(query)
        return self.env.cr.dictfetchall()

    @api.model
    def get_all_balance_with_analytic(self, company_id, account_ids, date_start, date_stop, target_move):
        ''' Тухайн дансны эхний үлдэгдэл болон тайлант хугацааны хоорондох дүнг олно.
        '''
        where = " "
        join = " "
        select = " "
        sub_select = " "
        sub_join = " "
        group_by = " "
        if self.env.context.get('order_by', False):
            order_by = self.env.context.get('order_by')
        else:
            order_by = "aa.code, aa.name "
        if target_move == 'posted':
            where += "AND m.state = 'posted'"
        if self.env.context.get('journal_ids', False):
            journal_ids = self.env.context.get('journal_ids', False)
            sub_select += ", ml.journal_id AS journal_id "
            select += ", j.id AS jid, j.name AS jname "
            join += "LEFT JOIN account_journal j ON (j.id = mv.journal_id) "
            group_by += ", j.id, j.name "
            where += ' AND ml.journal_id in (' + ','.join(map(str, journal_ids)) + ') '

        if self.env.context.get('partner_ids', False):
            partner_ids = self.env.context.get('partner_ids', False)
            sub_select += ", ml.partner_id AS partner_id "
            select += ", p.id AS pid, p.name AS pname "
            join += "LEFT JOIN res_partner p ON (p.id = mv.partner_id) "
            group_by += ", p.id, p.name "
            where += ' AND ml.partner_id in (' + ','.join(map(str, partner_ids)) + ') '
            if not self.env.context.get('order_by', False):
                order_by += "p.name "

        if self.env.context.get('byline', False):
            select += " , mv.line_id "
            sub_select += ", ml.id AS line_id "
            group_by += ", mv.line_id "

        # Харилцагчын товчоо тайланг борлуулалтын ажилтнаар бүлэглэсэн утга авах
        if self.env.context.get('salesman_ids', False):
            salesman_ids = self.env.context.get('salesman_ids', False)
            sub_select += ", ml.invoice_id AS invoice, ai.user_id as salesman "
            select += ", mv.invoice as invoice, mv.salesman as salesman "
            join += " LEFT JOIN res_users ru on (ru.id = mv.salesman) "
            sub_join += " LEFT JOIN account_invoice ai on (ml.invoice_id = ai.id) "
            group_by += ", ru.id, mv.salesman, mv.invoice "
            where += ' AND ai.user_id in (' + ','.join(map(str, salesman_ids)) + ') '

        if self._context.get('analytic_ids', False):
            analytic_ids = self._context.get('analytic_ids', False)
            # sub_join += " LEFT JOIN account_analytic_account aaa on (ml.analytic_account_id = aaa.id) "
            where += ' AND ml.analytic_account_id in (' + ','.join(map(str, analytic_ids)) + ') '

        if self.env.context.get('group_account', False):
            select += ", at.id AS atid, at.code AS atcode , at.name AS atname "
            join += "LEFT JOIN account_account_type at ON (at.id = aa.user_type_id) "
            group_by += ", at.id, at.code, at.name "
            order_by = " at.code, at.name, aa.code, aa.name"

        if self.env.context.get('without_profit_revenue', False):
            company = self.env['res.company'].browse(company_id)
            if not company.period_journal_id:
                raise ValidationError(_('Please configure period journal on account settings.'))
            else:
                where += 'AND ml.journal_id != %s ' % company.period_journal_id.id

        query = '''SELECT mv.account_id AS account_id, COALESCE(aa.code, '') AS acode, COALESCE(aa.name, '') AS aname,
                            mv.analytic_account_id AS analytic_account_id, COALESCE(aaa.code, '') AS aaacode, COALESCE(aaa.name, '') AS aaaname,
                            rc.id AS cid, COALESCE(rc.name, '') AS cname, 
                            sum(mv.start_balance) AS start_balance, sum(mv.cur_start_balance) AS cur_start_balance,
                            sum(mv.debit) AS debit, sum(mv.credit) AS credit, COALESCE(aa.internal_type, '') AS atype,
                            sum(mv.cur_debit) AS cur_debit, sum(mv.cur_credit) AS cur_credit ''' + select + '''
                FROM ( SELECT ml.account_id AS account_id, ml.analytic_account_id AS analytic_account_id,
                            CASE WHEN (ml.debit > 0 OR ml.credit > 0) AND ml.date < \'''' + date_start + '''\' 
                                THEN COALESCE(ml.debit - ml.credit, 0) ELSE 0 END AS start_balance,
                            CASE WHEN ml.amount_currency != 0  and ml.date < \'''' + date_start + '''\' 
                                THEN COALESCE(amount_currency, 0) ELSE 0 END AS cur_start_balance,
                            CASE WHEN ml.debit > 0 AND ml.date BETWEEN \'''' + date_start + '''\' AND \'''' + date_stop + '''\' 
                                THEN COALESCE(ml.debit,0) ELSE 0 END AS debit,
                            CASE WHEN ml.credit > 0 AND ml.date BETWEEN \'''' + date_start + '''\' AND \'''' + date_stop + '''\' 
                                THEN COALESCE(ml.credit,0) ELSE 0 END AS credit,
                            CASE WHEN ml.amount_currency > 0  AND ml.date BETWEEN \'''' + date_start + '''\' AND \'''' + date_stop + '''\' 
                                THEN ml.amount_currency ELSE 0 END AS cur_debit,
                            CASE WHEN ml.amount_currency < 0  AND ml.date BETWEEN \'''' + date_start + '''\' AND \'''' + date_stop + '''\' 
                                THEN abs(ml.amount_currency) ELSE 0 END AS cur_credit ''' + sub_select + '''
                        FROM account_move_line ml
                        LEFT JOIN account_move m ON (ml.move_id = m.id) ''' + sub_join + '''
                        WHERE ml.account_id in (''' + str(account_ids)[1:-1] + ')' + where + ''' AND
                                m.state = 'posted' AND ml.company_id = ''' + str(company_id) + ''') AS mv
                LEFT JOIN account_account aa ON (mv.account_id = aa.id)
                LEFT JOIN res_currency rc ON (aa.currency_id = rc.id)
                LEFT JOIN account_analytic_account aaa ON (mv.analytic_account_id = aaa.id) ''' + join + '''
                GROUP BY mv.account_id, aa.code, aa.name, rc.id, rc.name, aa.internal_type ''' + group_by + ''',
                        mv.analytic_account_id, aaa.code, aaa.name
                ORDER BY ''' + order_by + ''', aaa.code'''
        self.env.cr.execute(query)
        return self.env.cr.dictfetchall()

    @api.model
    def get_all_analytic_balance(self, company_id, account_ids, analytic_account_ids, date_start, date_stop):
        '''  Зөв нь ----- Шинжилгээний бичилтээс дансны тайлант хугацааны эхний үлдэгдэл болон эцсийн үлдэгдлийг гаргана
        '''
        where = " "
        join = " "
        select = " "
        sub_select = " "
        sub_join = " "
        group_by = " "
        # Тайланг эрэмбэлэлтийг зааж өгсөн бол
        if self.env.context.get('order_by', False):
            order_by = self.env.context.get('order_by')
        else:
            order_by = "aa.code, aa.name "
        # Санхүүгийн данс
        if account_ids:
            where += ' AND al.general_account_id in (' + ','.join(map(str, account_ids)) + ') '
        # Шинжилгээний данс
        if analytic_account_ids:
            where += ' AND al.account_id in (' + ','.join(map(str, analytic_account_ids)) + ') '
        # Тайланг журналаар шүүн гаргах бол
        if self.env.context.get('journal_ids', False):
            journal_ids = self.env.context.get('journal_ids', False)
            sub_select += ", ml.journal_id AS journal_id "
            select += ", j.id AS jid, j.name AS jname "
            join += "LEFT JOIN account_journal j ON (j.id = mv.journal_id) "
            group_by += ", j.id, j.name "
            where += ' AND ml.journal_id in (' + ','.join(map(str, journal_ids)) + ') '
        # Тайланг харилцагчаар шүүн гаргах бол
        if self.env.context.get('partner_ids', False):
            partner_ids = self.env.context.get('partner_ids', False)
            sub_select += ", al.partner_id AS partner_id "
            select += ", p.id AS pid, p.name AS pname "
            join += "LEFT JOIN res_partner p ON (p.id = mv.partner_id) "
            group_by += ", p.id, p.name "
            where += ' AND al.partner_id in (' + ','.join(map(str, partner_ids)) + ') '
            if not self.env.context.get('order_by', False):
                order_by += "p.name "
        # Харилцагчын товчоо тайланг борлуулалтын ажилтнаар бүлэглэсэн утга авах
        if self.env.context.get('salesman_ids', False):
            salesman_ids = self.env.context.get('salesman_ids', False)
            sub_select += ", ai.user_id as salesman "
            select += ", mv.salesman as salesman "
            join += " LEFT JOIN res_users ru on (ru.id = mv.salesman) "
            sub_join += " LEFT JOIN account_invoice ai on (ml.invoice_id = ai.id) "
            group_by += ", ru.id, mv.salesman "
            where += ' AND ai.user_id in (' + ','.join(map(str, salesman_ids)) + ') '
        # Тайлангийн бүлэглэлтийг оруулах
        if self.env.context.get('grouping', False):
            grouping = self.env.context.get('grouping', False)
            if grouping == 'analytic_account':
                order_by = "aaa.code, aaa.name, aa.code, aa.name"
            elif grouping == 'account_type':
                select += ", at.id AS atid, at.code AS atcode , at.name AS atname "
                join += "LEFT JOIN account_account_type at ON (at.id = aa.user_type_id) "
                group_by += ", at.id, at.code, at.name "
                order_by = " at.code, at.name, aa.code, aa.name, aaa.code, aaa.name"
            else:
                order_by += ",aaa.code, aaa.name"
        # Тайлан
        if self.env.context.get('without_profit_revenue', False):
            company = self.env['res.company'].browse(company_id)
            if not company.period_journal_id:
                raise ValidationError(_('Please configure period journal on account settings.'))
            else:
                where += "AND ml.journal_id != " + str(company.period_journal_id.id) + " "
        # analytic_share ашиглаж байгаа үед тэмдэг шалгах
        if self.env.context.get('is_analytic_share', False):
            debit_sign = " > "
            credit_sign = " < "
        else:
            debit_sign = " < "
            credit_sign = " > "
        self.env.cr.execute("SELECT mv.aid AS account_id, COALESCE(aa.code, '') AS acode, COALESCE(aa.name, '') AS aname, "
                                    "mv.aaid AS aaid, COALESCE(aaa.code, '') AS aacode, COALESCE(aaa.name, '') AS aaname, "
                                    "rc.id AS cid, COALESCE(rc.name, '') AS cname, "
                                    "sum(mv.start_balance) AS start_balance, sum(mv.cur_start_balance) AS cur_start_balance, "
                                    "sum(mv.debit) AS debit, sum(mv.credit) AS credit, COALESCE(aa.internal_type, '') AS atype, "
                                    "sum(mv.cur_debit) AS cur_debit, sum(mv.cur_credit) AS cur_credit " + select + " "
                                "FROM ( SELECT al.general_account_id AS aid, al.account_id AS aaid, "
                                            "CASE WHEN al.amount != 0 AND al.date < ' " + date_start + " ' "
                                                "THEN COALESCE(al.amount, 0) ELSE 0 END AS start_balance, "
                                            "CASE WHEN al.amount_currency != 0  and al.date < ' " + date_start + " ' "
                                                "THEN COALESCE(al.amount_currency, 0) ELSE 0 END AS cur_start_balance, "
                                            "CASE WHEN al.amount < 0 AND al.date BETWEEN ' " + date_start + " ' AND ' " + date_stop + " ' "
                                                "THEN COALESCE(abs(al.amount), 0) ELSE 0 END AS debit, "
                                            "CASE WHEN al.amount > 0 AND al.date BETWEEN ' " + date_start + " ' AND ' " + date_stop + " ' "
                                                "THEN COALESCE(al.amount, 0) ELSE 0 END AS credit, " +  
                                            "CASE WHEN al.amount_currency " + debit_sign + " 0  AND al.date BETWEEN ' " + date_start + " ' AND ' " + date_stop + " ' "
                                                "THEN COALESCE(abs(al.amount_currency), 0) ELSE 0 END AS cur_debit, "
                                            "CASE WHEN al.amount_currency " + credit_sign + " 0  AND al.date BETWEEN ' " + date_start + " ' AND ' " + date_stop + " ' "
                                                "THEN COALESCE(al.amount_currency, 0) ELSE 0 END AS cur_credit " + sub_select + " "
                                        "FROM account_analytic_line al "
                                        "LEFT JOIN account_move_line ml ON (al.move_id = ml.id) "
                                        "LEFT JOIN account_move m ON (ml.move_id = m.id) " + sub_join + " "
                                        "WHERE m.state = 'posted' AND al.company_id = " + str(company_id) + where + ") AS mv "
                                "LEFT JOIN account_account aa ON (mv.aid = aa.id) "
                                "LEFT JOIN res_currency rc ON (aa.currency_id = rc.id) "
                                "LEFT JOIN account_analytic_account aaa ON (mv.aaid = aaa.id) " + join + " "
                                "GROUP BY mv.aid, aa.code, aa.name, rc.id, rc.name, aa.internal_type, mv.aaid, aaa.code, aaa.name " + group_by + " "
                                "HAVING sum(mv.start_balance)::decimal(16,2) != 0 or sum(mv.debit)::decimal(16,2) > 0 or sum(mv.credit)::decimal(16,2) > 0 "   
                                        "or sum(mv.cur_start_balance)::decimal(16,2) != 0 or sum(mv.cur_debit)::decimal(16,2) > 0 or sum(mv.cur_debit)::decimal(16,2) > 0 "                                                                                                                      
                                "ORDER BY " + order_by + " ")
        return self.env.cr.dictfetchall()
    
    @api.model
    def get_partner_all_analytic_balance(self, company_id, account_ids, analytic_account_ids, date_start, date_stop):
        ''' Шинжилгээний бичилтээс тухайн дансны эхний үлдэгдэл болон тайлант хугацааны хоорондох дүнг олно. 
        '''
        where = " "
        join = " "
        select = " "
        sub_select = " "
        sub_join = " "
        group_by = " "
        if self.env.context.get('order_by', False):
            order_by = self.env.context.get('order_by')
        else:
            order_by = "aa.code, aa.name "
        if account_ids:
            where += ' AND al.general_account_id in (' + ','.join(map(str, account_ids)) + ') '
        if analytic_account_ids:
            where += ' AND al.account_id in (' + ','.join(map(str, analytic_account_ids)) + ') '

        if self.env.context.get('journal_ids', False):
            journal_ids = self.env.context.get('journal_ids', False)
            sub_select += ", ml.journal_id AS journal_id "
            select += ", j.id AS jid, j.name AS jname "
            join += "LEFT JOIN account_journal j ON (j.id = mv.journal_id) "
            group_by += ", j.id, j.name "
            where += ' AND ml.journal_id in (' + ','.join(map(str, journal_ids)) + ') '

        if self.env.context.get('partner_ids', False):
            partner_ids = self.env.context.get('partner_ids', False)
            sub_select += ", al.partner_id AS partner_id "
            select += ", p.id AS pid, p.name AS pname "
            join += "LEFT JOIN res_partner p ON (p.id = mv.partner_id) "
            group_by += ", p.id, p.name "
            where += ' AND al.partner_id in (' + ','.join(map(str, partner_ids)) + ') '
            if not self.env.context.get('order_by', False):
                order_by += "p.name "

        if self.env.context.get('grouping', False):
            grouping = self.env.context.get('grouping', False)
            if grouping == 'analytic_account':
                order_by = "aaa.code, aaa.name, aa.code, aa.name"
            elif grouping == 'account_type':
                select += ", at.id AS atid, at.code AS atcode , at.name AS atname "
                join += "LEFT JOIN account_account_type at ON (at.id = aa.user_type_id) "
                group_by += ", at.id, at.code, at.name "
                order_by = " at.code, at.name, aa.code, aa.name, aaa.code, aaa.name"
            else:
                order_by += ",aaa.code, aaa.name"

        if self.env.context.get('without_profit_revenue', False):
            company = self.env['res.company'].browse(company_id)
            if not company.period_journal_id:
                raise ValidationError(_('Please configure period journal on account settings.'))
            else:
                where += "AND ml.journal_id != " + str(company.period_journal_id.id) + " "
        self.env.cr.execute("SELECT mv.ml_id AS ml_id, mv.invoice AS invoice, mv.rec as rec ,mv.currency_rate as currency_rate, mv.name as name, mv.ref as ref, mv.ml_date as ml_date, mv.aid AS account_id, COALESCE(aa.code, '') AS acode, COALESCE(aa.name, '') AS aname, "
                                    "mv.aaid AS aaid, COALESCE(aaa.code, '') AS aacode, COALESCE(aaa.name, '') AS aaname, "
                                    "rc.id AS cid, COALESCE(rc.name, '') AS cname, "
                                    "sum(mv.start_balance) AS start_balance, sum(mv.cur_start_balance) AS cur_start_balance, "
                                    "sum(mv.debit) AS debit, sum(mv.credit) AS credit, COALESCE(aa.internal_type, '') AS atype, "
                                    "sum(mv.cur_debit) AS cur_debit, sum(mv.cur_credit) AS cur_credit " + select + " "
                                "FROM ( SELECT ml.id as ml_id, ml.invoice_id AS invoice, ml.full_reconcile_id as rec, ml.currency_rate as currency_rate, ml.name as name, ml.ref as ref, ml.date as ml_date, al.general_account_id AS aid, al.account_id AS aaid, "
                                            "CASE WHEN al.amount != 0 AND al.date < ' " + date_start + " ' "
                                                "THEN COALESCE(al.amount, 0) ELSE 0 END AS start_balance, "
                                            "CASE WHEN al.amount_currency != 0  and al.date < ' " + date_start + " ' "
                                                "THEN COALESCE(al.amount_currency, 0) ELSE 0 END AS cur_start_balance, "
                                            "CASE WHEN al.amount < 0 AND al.date BETWEEN ' " + date_start + " ' AND ' " + date_stop + " ' "
                                                "THEN COALESCE(abs(al.amount), 0) ELSE 0 END AS debit, "
                                            "CASE WHEN al.amount > 0 AND al.date BETWEEN ' " + date_start + " ' AND ' " + date_stop + " ' "
                                                "THEN COALESCE(al.amount, 0) ELSE 0 END AS credit, "
                                            "CASE WHEN al.amount_currency < 0  AND al.date BETWEEN ' " + date_start + " ' AND ' " + date_stop + " ' "
                                                "THEN COALESCE(abs(al.amount_currency), 0) ELSE 0 END AS cur_debit, "
                                            "CASE WHEN al.amount_currency > 0  AND al.date BETWEEN ' " + date_start + " ' AND ' " + date_stop + " ' "
                                                "THEN COALESCE(al.amount_currency, 0) ELSE 0 END AS cur_credit " + sub_select + " "
                                        "FROM account_analytic_line al "
                                        "LEFT JOIN account_move_line ml ON (al.move_id = ml.id) "
                                        "LEFT JOIN account_move m ON (ml.move_id = m.id) " + sub_join + " "
                                        "WHERE m.state = 'posted' AND al.company_id = " + str(company_id) + where + ") AS mv "
                                "LEFT JOIN account_account aa ON (mv.aid = aa.id) "
                                "LEFT JOIN res_currency rc ON (aa.currency_id = rc.id) "
                                "LEFT JOIN account_analytic_account aaa ON (mv.aaid = aaa.id) " + join + " "
                                "GROUP BY mv.ml_id, mv.invoice, mv.rec, mv.currency_rate, mv.name, mv.ref, mv.ml_date, mv.aid, aa.code, aa.name, rc.id, rc.name, aa.internal_type, mv.aaid, aaa.code, aaa.name " + group_by + " "
                                "ORDER BY " + order_by + " ")
        return self.env.cr.dictfetchall()

    @api.model
    def get_partner_balance_with_analytic(self, company_id, account_ids, date_start, date_stop, target_move):
        ''' Тухайн дансны эхний үлдэгдэл болон тайлант хугацааны хоорондох дүнг олно.
        '''
        where = " "
        join = " "
        sub_join = " "
        select = " "
        sub_select = " "
        group_by = " "
        if self.env.context.get('order_by', False):
            order_by = self.env.context.get('order_by')
        if target_move == 'posted':
            where += "AND m.state = 'posted'"
        if self.env.context.get('partner_ids', False):
            partner_ids = self.env.context.get('partner_ids', False)
            sub_select += ", ml.partner_id AS partner_id "
            select += ", p.id AS pid, p.name AS pname "
            join += "LEFT JOIN res_partner p ON (p.id = mv.partner_id) "
            group_by += ", p.id, p.name "
            where += ' AND ml.partner_id in (' + ','.join(map(str, partner_ids)) + ') '
            if not self.env.context.get('order_by', False):
                order_by += ", p.name "
        # Харилцагчын баланс тайланд борлуулалтын ажилтнаар бүлэглэх үед утга олгох
        if self.env.context.get('salesman_ids', False):
            salesman_ids = self.env.context.get('salesman_ids', False)
            sub_select += ", ai.user_id as salesman_id "
            select += ", mv.invoice as invoice, mv.salesman_id as salesman_id "
            join += " LEFT JOIN res_users ru on (ru.id = mv.salesman_id) "
            sub_join += " LEFT JOIN account_invoice ai on (ml.invoice_id = ai.id) "
            group_by += ", ru.id, mv.salesman_id, mv.invoice "
            where += ' AND ai.user_id in (' + ','.join(map(str, salesman_ids)) + ') '
        if self._context.get('analytic_ids', False):
            analytic_ids = self._context.get('analytic_ids', False)
            where += ' AND ml.analytic_account_id in (' + ','.join(map(str, analytic_ids)) + ') '

        self.env.cr.execute('''
            SELECT
                mv.account_id AS account_id,
                COALESCE(aa.code, '') AS acode,
                COALESCE(aa.name, '') AS aname,
                mv.analytic_account_id AS analytic_account_id,
                COALESCE(aaa.code, '') AS aaacode,
                COALESCE(aaa.name, '') AS aaaname,
                COALESCE(rc.name, '') AS cname,
                mv.start_balance AS start_balance,
                mv.cur_start_balance AS cur_start_balance,
                mv.debit AS debit,
                mv.credit AS credit,
                COALESCE(aa.internal_type, '') AS atype,
                mv.cur_debit AS cur_debit,
                mv.cur_credit AS cur_credit,
                mv.name AS name,
                mv.ml_id AS ml_id,
                mv.ref AS ref,
                mv.ml_date as ml_date,
                mv.rec AS rec,
                mv.cid AS cid,
                mv.invoice AS invoice,
                mv.currency_rate as currency_rate ''' + select + '''
            FROM
                (
                    SELECT
                        ml.account_id AS account_id,
                        ml.analytic_account_id AS analytic_account_id,
                        CASE
                            WHEN (ml.debit > 0 OR ml.credit > 0) AND ml.date < \'''' + date_start + '''\' THEN COALESCE(ml.debit - ml.credit, 0)
                            ELSE 0
                        END AS start_balance,
                        CASE
                            WHEN ml.amount_currency != 0 AND ml.date < \'''' + date_start + '''\' THEN COALESCE(amount_currency, 0)
                            ELSE 0
                        END AS cur_start_balance,
                        CASE
                            WHEN ml.debit > 0 AND m.date BETWEEN \'''' + date_start + '''\' AND \'''' + date_stop + '''\' THEN COALESCE(ml.debit,0)
                            ELSE 0
                        END AS debit,
                        CASE
                            WHEN ml.credit > 0 AND m.date BETWEEN \'''' + date_start + '''\' AND \'''' + date_stop + '''\' THEN COALESCE(ml.credit,0)
                            ELSE 0
                        END AS credit,
                        CASE
                            WHEN ml.amount_currency > 0  AND m.date BETWEEN \'''' + date_start + '''\' AND \'''' + date_stop + '''\' THEN ml.amount_currency
                            ELSE 0
                        END AS cur_debit,
                        CASE
                            WHEN ml.amount_currency < 0  AND m.date BETWEEN \'''' + date_start + '''\' AND \'''' + date_stop + '''\' THEN abs(ml.amount_currency)
                            ELSE 0
                        END AS cur_credit,
                        ml.name AS name,
                        ml.id as ml_id,
                        ml.ref As ref,
                        ml.date AS ml_date,
                        ml.currency_rate AS currency_rate,
                        ml.full_reconcile_id AS rec,
                        ml.currency_id AS cid,
                        ml.invoice_id AS invoice ''' + sub_select + '''
                    FROM
                        account_move_line ml
                        LEFT JOIN account_move m ON (ml.move_id = m.id) ''' + sub_join + '''
                    WHERE
                        ml.account_id in (''' + str(account_ids)[1:-1] + ''')''' + where + ''' AND
                        ml.company_id = ''' + str(company_id) + '''
                ) AS mv
                LEFT JOIN account_account aa ON (mv.account_id = aa.id)
                LEFT JOIN res_currency rc ON (aa.currency_id = rc.id)
                LEFT JOIN account_analytic_account aaa ON (mv.analytic_account_id = aaa.id) ''' + join + '''
            ORDER BY ''' + order_by + ''', aaa.code''')
        return self.env.cr.dictfetchall()

    @api.model
    def get_all_analytic_partner_balance(self, company_id, account_ids, analytic_account_ids, date_start, date_stop):
        ''' Зөв нь ---- Тухайн харилцагч дансны эхний үлдэгдэл болон тайлант хугацааны хоорондох дүнг шинжилгээний бичилтээс олно.
        '''
        join = " "
        sub_join = " "
        select = " "
        sub_select = " "
        group_by = " "
        join_st = " "
        join_on = " "
        if self.env.context.get('order_by', False):
            order_by = self.env.context.get('order_by')
        where = 'AND al.company_id = ' + str(company_id) + ''
        if account_ids:
            where += ' AND al.general_account_id in (' + ','.join(map(str, account_ids)) + ') '
        if analytic_account_ids:
            where += ' AND al.account_id in (' + ','.join(map(str, analytic_account_ids)) + ') '
        if self.env.context.get('partner_ids', False):
            # Тайланг харилцагчаар шүүн гаргах бол
            partner_ids = self.env.context.get('partner_ids', False)
            sub_select += ", al.partner_id AS partner_id "
            select += ", p.id AS pid, p.name AS pname, COALESCE(p.credit_limit, 0) AS limit "
            join += "LEFT JOIN res_partner p ON (p.id = aml.partner_id) "
            join_st += "AND st.partner_id = aml.partner_id "
            join_on += "AND mv.partner_id = aml.partner_id "
            group_by += ", al.partner_id "
            where += ' AND al.partner_id in (' + ','.join(map(str, partner_ids)) + ') '
            if not self.env.context.get('order_by', False):
                order_by += ", p.name "
        # Харилцагчын баланс тайланд борлуулалтын ажилтнаар бүлэглэх үед утга олгох
        if self.env.context.get('salesman_ids', False):
            salesman_ids = self.env.context.get('salesman_ids', False)
            sub_select += ", ai.user_id as salesman_id "
            select += ", mv.invoice as invoice, COALESCE(mv.salesman_id, st.salesman_id) as salesman "
            join += " LEFT JOIN res_users ru on (ru.id = aml.salesman_id) "
            sub_join += " LEFT JOIN account_invoice ai on (ml.invoice_id = ai.id) "
            join_st += "AND st.salesman_id = aml.salesman_id "
            join_on += "AND mv.salesman_id = aml.salesman_id "
            group_by += ", ai.user_id "
            where += ' AND ai.user_id in (' + ','.join(map(str, salesman_ids)) + ') '
        # analytic_share ашиглаж байгаа үед тэмдэг шалгах
        if self.env.context.get('is_analytic_share', False):
            debit_sign = " > "
            credit_sign = " < "
        else:
            debit_sign = " < "
            credit_sign = " > "
        self.env.cr.execute("SELECT aml.account_id AS account_id, COALESCE(aa.code, '') AS acode, "
                                    "COALESCE(aa.name, '') AS aname, COALESCE(rc.name, '') AS cname, "
                                    "aml.aaid AS aaid, COALESCE(aaa.code, '') AS aacode, COALESCE(aaa.name, '') AS aaname, "
                                    "COALESCE(st.start_balance, 0) AS start_balance, COALESCE(st.cur_start_balance, 0) AS cur_start_balance, "
                                    "COALESCE(mv.debit, 0) AS debit, COALESCE(mv.credit, 0) AS credit, COALESCE(aa.internal_type, '') AS atype, "
                                    "COALESCE(mv.cur_debit, 0) AS cur_debit, COALESCE(mv.cur_credit, 0) AS cur_credit, COALESCE(mv.name, '') AS name, "
                                    "COALESCE(mv.ml_id, 0) AS ml_id, COALESCE(mv.ref, '') AS ref, COALESCE(mv.mname, '') AS mname, mv.ml_date as ml_date, "
                                    "mv.rec AS rec, fr.name AS frname, mv.cid AS cid, mv.invoice AS invoice, mv.currency_rate as currency_rate"
                                    + select + ""
                            "FROM (SELECT al.general_account_id AS account_id, al.account_id AS aaid " + sub_select + ""
                                      "FROM account_analytic_line al "
                                      "LEFT JOIN account_move_line ml ON (al.move_id = ml.id) "
                                      "LEFT JOIN account_move m ON (ml.move_id = m.id) " + sub_join + ""
                                      "WHERE al.date <= %s " + where + " "
                                      "GROUP BY al.general_account_id, al.account_id " + group_by + ") AS aml "
                            "LEFT JOIN  (SELECT al.general_account_id AS account_id, al.account_id AS aaid, sum(COALESCE(al.amount, 0))::decimal(16,2) AS start_balance, "
                                                "SUM(COALESCE(al.amount_currency, 0))::decimal(16,2) AS cur_start_balance " + sub_select + ""
                                       "FROM account_analytic_line al "
                                       "LEFT JOIN account_move_line ml ON (al.move_id = ml.id) "
                                       "LEFT JOIN account_move m ON (ml.move_id = m.id) " + sub_join + ""
                                       "WHERE al.date < %s " + where + " "
                                       "GROUP BY al.general_account_id, al.account_id " + group_by + ") AS st ON (aml.account_id = st.account_id AND aml.aaid = st.aaid" + join_st + ") "
                            "LEFT JOIN  (SELECT  al.general_account_id AS account_id, al.account_id AS aaid, "
                                               "CASE WHEN al.amount < 0 "
                                               "THEN al.amount::decimal(16,2) ELSE 0 END AS debit, "
                                               "CASE WHEN al.amount > 0 "
                                               "THEN abs(al.amount)::decimal(16,2) ELSE 0 END AS credit, "                                                                                        
                                               "CASE WHEN al.amount_currency " + debit_sign + " 0 "
                                               "THEN al.amount_currency::decimal(16,2) ELSE 0 END AS cur_debit, "
                                               "CASE WHEN al.amount_currency " + credit_sign + " 0 "
                                               "THEN abs(al.amount_currency)::decimal(16,2) ELSE 0 END AS cur_credit, "
                                               "m.name AS mname, al.name AS name , al.id as ml_id, ml.ref As ref, al.date AS ml_date, "
                                               "ml.currency_rate AS currency_rate, ml.full_reconcile_id AS rec, ml.currency_id AS cid, "
                                               "ml.invoice_id AS invoice" + sub_select + ""
                                         "FROM account_analytic_line al "
                                         "LEFT JOIN account_move_line ml ON (al.move_id = ml.id) "
                                         "LEFT JOIN account_move m ON (ml.move_id = m.id) " + sub_join + ""
                                         "WHERE al.date BETWEEN %s AND %s " + where + ") AS mv ON (aml.account_id = mv.account_id AND aml.aaid = mv.aaid" + join_on + ") "
                           "LEFT JOIN account_account aa ON (aml.account_id = aa.id) "
                           "LEFT JOIN account_analytic_account aaa ON (aml.aaid = aaa.id) "                                                                                                                    
                           "LEFT JOIN res_currency rc ON (aa.currency_id = rc.id) "
                           "LEFT JOIN account_full_reconcile fr ON (fr.id = mv.rec) " + join + ""
                           "ORDER BY " + order_by, (date_stop, date_start, date_start, date_stop))
        return self.env.cr.dictfetchall()