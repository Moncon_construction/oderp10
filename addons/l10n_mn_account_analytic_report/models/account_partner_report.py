# -*- encoding: utf-8 -*-
##############################################################################
from odoo import api, fields, models, _
from odoo.osv import expression


class AccountPartnerReport(models.Model):
    _inherit = 'account.partner.report'

    show_analytic_account = fields.Boolean('Show Analytic Account', track_visibility='onchange', states={'approved': [('readonly', True)]})
    analytic_account_ids = fields.Many2many('account.analytic.account', string="Analytic Accounts", states={'approved': [('readonly', True)]})
    analytic_line_ids = fields.One2many('account.partner.report.analytic.line', 'report_id', string='Analytic Lines', readonly=True, copy=False)
    analytic_currency_line_ids = fields.One2many('account.partner.report.analytic.currency.line', 'report_id', string='Analytic Currency Lines', readonly=True, copy=False)

    # Компани, эхлэх болон дуусах огноогоор нэр талбар бөглөгдөх
    @api.onchange('date_from', 'date_to', 'show_analytic_account')
    def onchange_account_transaction(self):
        for report in self:
            name = ''
            if report.company_id:
                name += _('%s is ') % self.company_id.name
            if report.date_from:
                name += _('[%s - ') % self.date_from
            if report.date_to:
                name += _('%s] duration ') % self.date_to
            if report.show_analytic_account:
                name += _('Analytic Account ')
            name += _('Partner Report')
            report.name = name

    @api.multi
    def get_analytic_ids(self):
        # Борлуулалтын ажилтан id-г олох
        analytic_ids = self.env['account.analytic.account'].search([]).ids
        if self.analytic_account_ids:
            analytic_ids = self.analytic_account_ids.ids
        return analytic_ids

    @api.multi
    def get_line(self, line, sequence, salesman_id, line_name, end, currency_end):
        # Тайлангийн мөр дээр
        res = super(AccountPartnerReport, self).get_line(line, sequence, salesman_id, line_name, end, currency_end)
        if self.show_analytic_account:
            res.update({'analytic_account_id': line['aaid']})
        return res

    @api.multi
    def compute_all(self):
        self.line_ids = None
        self.currency_line_ids = None
        self.analytic_line_ids = None
        self.analytic_currency_line_ids = None
        if self.show_analytic_account:
            self.compute_analytic()
        else:
            self.compute()

    @api.multi
    def compute_analytic(self):
        # Шинжилгээний данс харуулах үед тайлангын утгууд олох
        line_obj = self.env['account.partner.report.analytic.line']
        if self.show_currency_transaction:
            line_obj = self.env['account.partner.report.analytic.currency.line']
        # Данснуудын id-г олох
        account_ids, is_payable, is_receivable = self.get_account_ids()
        # Харилцагчийн id-г олох
        partner_ids = self.get_partner_ids()
        # Борлуулалтын ажилтан id-г олох
        salesman_ids = self.get_salesman_ids()
        # Шинжилгээний дансны id-г олох
        analytic_ids = self.get_analytic_ids()
        # Эрэмбэлэлт
        order_by = self.get_order_by('ledger')

        # Эхлэл болон тухайн хугацааны хоорондох утгыг шинжилгээний бичилтээс олох
        lines = self.env['account.move.line'].with_context(salesman_ids=salesman_ids, partner_ids=partner_ids, order_by=order_by).\
            get_all_analytic_balance(self.company_id.id, account_ids, analytic_ids, self.date_from, self.date_to)
        if lines:
            self.compute_line(line_obj, lines, is_payable, is_receivable)
        return True

class AccountPartnerReportAnalyticLine(models.Model):
    _name = 'account.partner.report.analytic.line'
    _description = 'Account Partner Report Analytic Line'

    sequence = fields.Char('Sequence')
    group_name = fields.Char('Group')
    name = fields.Char('Account/Partner')
    account_id = fields.Many2one('account.account', string='Account')
    partner_id = fields.Many2one('res.partner', string='Partner')
    salesperson_id = fields.Many2one('res.users', string='Salesperson')
    analytic_account_id = fields.Many2one('account.analytic.account', string="Analytic Account")
    report_id = fields.Many2one('account.partner.report', string='Account Partner Report', ondelete="cascade")
    currency_id = fields.Many2one('res.currency', string='Currency')
    initial = fields.Float(string='Initial Balance', digits=(16, 2), default=0)
    currency_initial = fields.Float(string='Initial Currency Balance', digits=(16, 2), default=0)
    debit = fields.Float(string='Transaction /Dt/', digits=(16, 2), default=0)
    credit = fields.Float(string='Transaction /Ct/', digits=(16, 2), default=0)
    currency_debit = fields.Float(string='Transaction Currency /Dt/', digits=(16, 2), default=0)
    currency_credit = fields.Float(string='Transaction Currency /Ct/', digits=(16, 2), default=0)
    end = fields.Float(string='End Balance', digits=(16, 2), default=0)
    currency_end = fields.Float(string='End Currency Balance', digits=(16, 2), default=0)
    color = fields.Selection([('black', 'Black'),
                              ('bold', 'Bold'),
                              ('blue', 'Blue')], string='Color', default='black')

    @api.multi
    def button_initial_analytic_lines(self):
        # Эхний үлдэгдлийн журналын мөрүүдийг харуулах
        for line in self:
            domain = [('date', '<', line.report_id.date_from), ('company_id', '=', line.report_id.company_id.id)]
            if line.account_id:
                domain = expression.AND([domain, [('general_account_id', '=', line.account_id.id)]])
            if line.partner_id:
                domain = expression.AND([domain, [('partner_id', '=', line.partner_id.id)]])
            if line.salesperson_id:
                domain = expression.AND([domain, [('move_id.invoice_id.user_id', '=', line.salesperson_id.id)]])
            if line.analytic_account_id:
                domain = expression.AND([domain, [('account_id', '=', line.analytic_account_id.id)]])
            return {
                'name': _('Account Analytic Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.analytic.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'context': {'group_by': ['general_account_id']},
                'domain': domain
            }

    @api.multi
    def button_analytic_lines(self):
        # Хугацааны хоорогдох журналын мөрүүдийг харуулах
        for line in self:
            domain = [('date', '>=', line.report_id.date_from), ('date', '<=', line.report_id.date_to), ('company_id', '=', line.report_id.company_id.id)]
            if line.account_id:
                domain = expression.AND([domain, [('general_account_id', '=', line.account_id.id)]])
            if line.partner_id:
                domain = expression.AND([domain, [('partner_id', '=', line.partner_id.id)]])
            if line.salesperson_id:
                domain = expression.AND([domain, [('move_id.invoice_id.user_id', '=', line.salesperson_id.id)]])
            if line.analytic_account_id:
                domain = expression.AND([domain, [('account_id', '=', line.analytic_account_id.id)]])
            return {
                'name': _('Account Analytic Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.analytic.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'context': {'group_by': ['general_account_id']},
                'domain': domain
            }

class AccountPartnerReportAnalyticCurrencyLine(models.Model):
    _name = 'account.partner.report.analytic.currency.line'
    _description = 'Account Partner Report Analytic Currency Line'

    sequence = fields.Char('Sequence')
    group_name = fields.Char('Group')
    name = fields.Char('Account/Partner')
    account_id = fields.Many2one('account.account', string='Account')
    partner_id = fields.Many2one('res.partner', string='Partner')
    salesperson_id = fields.Many2one('res.users', string='Salesperson')
    analytic_account_id = fields.Many2one('account.analytic.account', string="Analytic Account")
    report_id = fields.Many2one('account.partner.report', string='Account Partner Report', ondelete="cascade")
    currency_id = fields.Many2one('res.currency', string='Currency')
    initial = fields.Float(string='Initial Balance', digits=(16, 2), default=0)
    currency_initial = fields.Float(string='Initial Currency Balance', digits=(16, 2), default=0)
    debit = fields.Float(string='Transaction /Dt/', digits=(16, 2), default=0)
    credit = fields.Float(string='Transaction /Ct/', digits=(16, 2), default=0)
    currency_debit = fields.Float(string='Transaction Currency /Dt/', digits=(16, 2), default=0)
    currency_credit = fields.Float(string='Transaction Currency /Ct/', digits=(16, 2), default=0)
    end = fields.Float(string='End Balance', digits=(16, 2), default=0)
    currency_end = fields.Float(string='End Currency Balance', digits=(16, 2), default=0)
    color = fields.Selection([('black', 'Black'),
                              ('bold', 'Bold'),
                              ('blue', 'Blue')], string='Color', default='black')

    @api.multi
    def button_initial_analytic_lines(self):
        # Эхний үлдэгдлийн журналын мөрүүдийг харуулах
        for line in self:
            domain = [('date', '<', line.report_id.date_from), ('company_id', '=', line.report_id.company_id.id)]
            if line.account_id:
                domain = expression.AND([domain, [('general_account_id', '=', line.account_id.id)]])
            if line.partner_id:
                domain = expression.AND([domain, [('partner_id', '=', line.partner_id.id)]])
            if line.salesperson_id:
                domain = expression.AND([domain, [('move_id.invoice_id.user_id', '=', line.salesperson_id.id)]])
            if line.analytic_account_id:
                domain = expression.AND([domain, [('account_id', '=', line.analytic_account_id.id)]])
            return {
                'name': _('Account Analytic Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.analytic.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'context': {'group_by': ['general_account_id']},
                'domain': domain
            }

    @api.multi
    def button_analytic_lines(self):
        # Хугацааны хоорогдох журналын мөрүүдийг харуулах
        for line in self:
            domain = [('date', '>=', line.report_id.date_from), ('date', '<=', line.report_id.date_to), ('company_id', '=', line.report_id.company_id.id)]
            if line.account_id:
                domain = expression.AND([domain, [('general_account_id', '=', line.account_id.id)]])
            if line.partner_id:
                domain = expression.AND([domain, [('partner_id', '=', line.partner_id.id)]])
            if line.salesperson_id:
                domain = expression.AND([domain, [('move_id.invoice_id.user_id', '=', line.salesperson_id.id)]])
            if line.analytic_account_id:
                domain = expression.AND([domain, [('account_id', '=', line.analytic_account_id.id)]])
            return {
                'name': _('Account Analytic Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.analytic.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'context': {'group_by': ['general_account_id']},
                'domain': domain
            }