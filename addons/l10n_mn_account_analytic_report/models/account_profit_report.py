# -*- encoding: utf-8 -*-
##############################################################################
import time
from operator import itemgetter
from odoo import api, fields, models, _

from odoo.osv import expression

class AccountProfitReport(models.Model):
    _inherit = 'account.profit.report'

    show_analytic_account = fields.Boolean('Show Analytic Account', track_visibility='onchange', states={'approved': [('readonly', True)]})
    analytic_account_ids = fields.Many2many('account.analytic.account', string="Analytic Accounts", states={'approved': [('readonly', True)]})

    # Компани, эхлэх болон дуусах огноогоор нэр талбар бөглөгдөх
    @api.onchange('date_from', 'date_to', 'show_analytic_account')
    def onchange_account_transaction(self):
        for report in self:
            name = ''
            if report.company_id:
                name += _('%s is ') % self.company_id.name
            if report.date_from:
                name += _('[%s - ') % self.date_from
            if report.date_to:
                name += _('%s] duration ') % self.date_to
            if report.show_analytic_account:
                name += _('Analytic Account ')
            name += _('Profit Report')
            report.name = name
            
    @api.multi
    def compute_all(self):
        self.line_ids = None
        if self.show_analytic_account:
            self.compute_analytic()
        else:
            self.compute()

    def get_analytic_account_ids(self):
        self.ensure_one()
        if self.analytic_account_ids:
            analytic_account_ids = self.analytic_account_ids
        else:
            analytic_account_ids = self.env['account.analytic.account'].search([])
        return analytic_account_ids
    
    @api.multi
    def compute_analytic(self):
        # Шинжилгээний дансны тооцоолол хийх функц
        self.line_ids = None
        self.currency_line_ids = None
        self.analytic_line_ids = None
        self.currency_analytic_line_ids = None
        account_ids = re_account_ids = []
        analytic_account_ids = self.get_analytic_account_ids()
        analytic_account_ids = analytic_account_ids.ids if analytic_account_ids else []
        if not analytic_account_ids:
            return False

        account_obj = self.env['account.account']
        move_line_obj = self.env['account.move.line']
        line_obj = self.env['account.profit.report.line']
        report_ids = self.account_report_id._get_children_by_order()

        parent_numbers = {}
        parent_index = 1
        for account in report_ids:
            initial_balance = 0
            end_balance = 0
            if account.id == self.account_report_id.id:
                continue                   
             # Тайлангийн мөрийн дугаарыг тооцож байна.
            number = parent_index
            if account.parent_id.id not in parent_numbers:
                parent_numbers[account.id] = [str(number), 0]
                parent_index += 1
            else:
                number = parent_numbers[account.parent_id.id][0] + '.' + str(parent_numbers[account.parent_id.id][1] + 1)
                parent_numbers[account.parent_id.id][1] += 1
                parent_numbers[account.id] = [str(number), 0]
            # Тайлангийн шатлалын дагуу мөрийн дугаарлалтыг олж байна
            number_str = '%s' % number
            if account.style_overwrite in [1, 2, 3]:
                name_str = u'%s' % account.name or ''
                color = 'blue'
            else:
                name_str = u'   %s' % account.name or ''
                color = 'black'
            lines = []
            account_ids = account._get_all_accounts(lines)
            if account_ids:
                # Шинжилгээний бичилтээс дансны дүнг дуудах
                balances = move_line_obj.get_all_analytic_balance(self.company_id.id, account_ids, analytic_account_ids,
                                                                                               self.date_from, self.date_to)
                c = -1
                for line in balances:
                    initial_balance += line['start_balance'] 
                    end_balance += line['start_balance'] - line['debit'] + line['credit']

                # Тайлангийн бүтэц дээр утгыг + эсвэл - харагдуулахыг тооцож байна.
                initial_balance *= account.sign * c
                end_balance *= account.sign * c
            if type(account_ids) != list:
                for key, value in account_ids.items():
                    if value not in re_account_ids: 
                        re_account_ids += value
                account_ids = re_account_ids
            line_obj.create({'line_number': number_str,
                             'name': name_str,
                             'profit_report_id': self.id,
                             'account_financial_report_id': account.id,
                             'account_report_type': account.type,
                             'name':account.name,
                             'initial_balance': initial_balance,
                             'end_balance': end_balance,
                             'account_ids':[(6, 0, list(account_ids))],
                             'color': 'bold' if account.type in ('account_report','sum') and not account.account_report_id else color
                             })
                    
        return True
    
class AccountProfitReportLine(models.Model):
    _inherit = 'account.profit.report.line'
    
    @api.multi
    def button_initial_journal_entries(self):
        result = super(AccountProfitReportLine, self).button_initial_journal_entries()
        # Эхний үлдэгдлийн журналын мөрүүдийг харуулах
        for line in self:
            if line.profit_report_id.show_analytic_account:
                analytic_account_ids = line.profit_report_id.get_analytic_account_ids()
                analytic_account_ids = analytic_account_ids.ids if analytic_account_ids else []
                return {
                    'name': _('Account Analytic Lines'),
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'res_model': 'account.analytic.line',
                    'view_id': False,
                    'type': 'ir.actions.act_window',
                    'domain': [('account_id', 'in', analytic_account_ids), ('general_account_id', 'in', line.account_ids.ids), ('date', '<', line.profit_report_id.date_from)],
                }
            else:
                return result

    @api.multi
    def button_end_journal_entries(self):
        result = super(AccountProfitReportLine, self).button_end_journal_entries()
        # Хугацааны хоорогдох журналын мөрүүдийг харуулах
        # Эхний үлдэгдлийн журналын мөрүүдийг харуулах
        for line in self:
            if line.profit_report_id.show_analytic_account:
                analytic_account_ids = line.profit_report_id.get_analytic_account_ids()
                analytic_account_ids = analytic_account_ids.ids if analytic_account_ids else []
                return {
                    'name': _('Account Analytic Lines'),
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'res_model': 'account.analytic.line',
                    'view_id': False,
                    'type': 'ir.actions.act_window',
                    'domain': [('account_id', 'in', analytic_account_ids), ('general_account_id', 'in', line.account_ids.ids),
                               ('date', '>=', line.profit_report_id.date_from),('date', '<=', line.profit_report_id.date_to)],
                }
            else:
                return result
    