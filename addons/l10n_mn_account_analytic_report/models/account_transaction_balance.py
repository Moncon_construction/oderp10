# -*- encoding: utf-8 -*-
##############################################################################
import time
from operator import itemgetter
from odoo.exceptions import ValidationError, UserError

from odoo import api, fields, models, _


class AccountTransactionBalance(models.Model):
    _inherit = 'account.transaction.balance'

    show_analytic_account = fields.Boolean('Show Analytic Account', track_visibility='onchange', states={'approved': [('readonly', True)]})
    grouping = fields.Selection([('analytic_account', 'Analytic account'),
                                 ('account_type', 'Account type'),
                                 ('account', 'Account')], 'Grouping', track_visibility='onchange', states={'approved': [('readonly', True)]})
    analytic_account_ids = fields.Many2many('account.analytic.account', string="Analytic Accounts", states={'approved': [('readonly', True)]})
    analytic_line_ids = fields.One2many('account.transaction.balance.analytic.line', 'transaction_id', string='Analytic Lines', readonly=True, copy=False)
    currency_analytic_line_ids = fields.One2many('account.transaction.balance.currency.analytic.line', 'transaction_id', string='Currency Analytic Lines', readonly=True, copy=False)

    # Компани, эхлэх болон дуусах огноогоор нэр талбар бөглөгдөх
    @api.onchange('date_from', 'date_to', 'show_analytic_account')
    def onchange_account_transaction(self):
        for report in self:
            name = ''
            if report.company_id:
                name += _('%s is ') % self.company_id.name
            if report.date_from:
                name += _('[%s - ') % self.date_from
            if report.date_to:
                name += _('%s] duration ') % self.date_to
            if report.show_analytic_account:
                name += _('Analytic Account ')
            name += _('Transaction balance')
            report.name = name

    @api.multi
    def compute(self):
        # Тооцоолол хийх функц
        self.line_ids = None
        self.currency_line_ids = None
        self.analytic_line_ids = None
        self.currency_analytic_line_ids = None
        account_ids = []
        analytic_account_ids = []

        account_obj = self.env['account.account']
        move_line_obj = self.env['account.move.line']
        line_obj = self.env['account.transaction.balance.line']
        currency_line_obj = self.env['account.transaction.balance.currency.line']
        analytic_line_obj = self.env['account.transaction.balance.analytic.line']
        currency_analytic_line_obj = self.env['account.transaction.balance.currency.analytic.line']

        if self.account_ids:
            for account_id in self.account_ids.ids:
                account_ids.append(account_id)
        elif self.account_type_ids:
            account = account_obj.search([('user_type_id', 'in', self.account_type_ids.ids)])
            if account:
                for account_id in account.ids:
                    account_ids.append(account_id)
            if not account_ids:
                raise UserError(_('There are no accounts in selected account types'))
        else:
            account_ids = account_obj.search([]).ids
        if self.show_analytic_account:
            if self.analytic_account_ids:
                for analytic_account_id in self.analytic_account_ids:
                    analytic_account_ids.append(analytic_account_id.id)
            else:
                analytic_account_ids = self.env['account.analytic.account'].search([]).ids
            # Шинжилгээний бичилтээс дансны дүнг дуудах
            balances = move_line_obj.with_context(without_profit_revenue=self.without_profit_revenue,
                                                  grouping=self.grouping).get_all_analytic_balance(self.company_id.id, account_ids, analytic_account_ids,
                                                                                                   self.date_from, self.date_to)
        else:
            # Журналын мөрөөс дансны дүнг дуудах
            balances = move_line_obj.with_context(without_profit_revenue=self.without_profit_revenue,
                                                  group_account=self.group_account).get_all_balance(self.company_id.id,  account_ids,  self.date_from,
                                                                                                    self.date_to, self.target_move)
        #if not self.show_analytic_account:
        for line in balances:
            if self.show_analytic_account:
                end_balance = line['start_balance'] - line['debit'] + line['credit']
            else:
                end_balance = line['start_balance'] + line['debit'] - line['credit']
            if (not self.show_analytic_account and self.group_account) or (self.show_analytic_account and self.grouping and self.grouping == 'account_type'):
                type = line['atid']
            else:
                type = False
            if self.currency_transaction and not self.show_analytic_account:
                cur_end_balance = line['cur_start_balance'] + line['cur_debit'] - line['cur_credit']
                currency_line_obj.create({'account_id': line['account_id'],
                                          'account_type_id': type,
                                          'code': line['acode'],
                                          'name': line['aname'],
                                          'currency_id': line['cid'] or self.company_id.currency_id and self.company_id.currency_id.id,
                                          'initial_debit': (line['start_balance'] > 0 and line['start_balance']) or 0.0,
                                          'initial_credit': (line['start_balance'] < 0 and -line['start_balance']) or 0.0,
                                          'initial_currency_debit': (line['cur_start_balance'] > 0 and line['cur_start_balance']) or 0.0,
                                          'initial_currency_credit': (line['cur_start_balance'] < 0 and -line['cur_start_balance']) or 0.0,
                                          'debit': line['debit'] or 0.0,
                                          'credit': line['credit'] or 0.0,
                                          'currency_debit': line['cur_debit'] or 0.0,
                                          'currency_credit': line['cur_credit'] or 0.0,
                                          'end_debit': (end_balance > 0 and end_balance) or 0.0,
                                          'end_credit': (end_balance < 0 and -end_balance) or 0.0,
                                          'end_currency_debit': (cur_end_balance > 0 and cur_end_balance) or 0.0,
                                          'end_currency_credit': (cur_end_balance < 0 and -cur_end_balance) or 0.0,
                                          'transaction_id': self.id
                                          })
            elif not self.currency_transaction and not self.show_analytic_account:
                line_obj.create({'account_id': line['account_id'],
                                 'account_type_id': type,
                                 'code': line['acode'],
                                 'name': line['aname'],
                                 'initial_debit': (line['start_balance'] > 0 and line['start_balance']) or 0.0,
                                 'initial_credit': (line['start_balance'] < 0 and -line['start_balance']) or 0.0,
                                 'debit': line['debit'] or 0.0,
                                 'credit': line['credit'] or 0.0,
                                 'end_debit': (end_balance > 0 and end_balance) or 0.0,
                                 'end_credit': (end_balance < 0 and -end_balance) or 0.0,
                                 'transaction_id': self.id
                                 })
            elif not self.currency_transaction and self.show_analytic_account:
                analytic_line_obj.create({'account_id': line['account_id'],
                                          'account_type_id': type,
                                          'code': line['acode'],
                                          'name': line['aname'],
                                          'analytic_account_id': line['aaid'],
                                          'analytic_account_code': line['aacode'],
                                          'analytic_account_name': line['aaname'],
                                          'initial_debit': (line['start_balance'] < 0 and -line['start_balance']) or 0.0,
                                          'initial_credit': (line['start_balance'] > 0 and line['start_balance']) or 0.0,
                                          'debit': line['debit'] or 0.0,
                                          'credit': line['credit'] or 0.0,
                                          'end_debit': (end_balance < 0 and -end_balance) or 0.0,
                                          'end_credit': (end_balance > 0 and end_balance) or 0.0,
                                          'transaction_id': self.id
                                          })
            elif self.currency_transaction and self.show_analytic_account:
                cur_end_balance = line['cur_start_balance'] - line['cur_debit'] + line['cur_credit']
                currency_analytic_line_obj.create({'account_id': line['account_id'],
                                                   'account_type_id': type,
                                                   'code': line['acode'],
                                                   'name': line['aname'],
                                                   'currency_id': line['cid'] or self.company_id.currency_id and self.company_id.currency_id.id,
                                                   'analytic_account_id': line['aaid'],
                                                   'analytic_account_code': line['aacode'],
                                                   'analytic_account_name': line['aaname'],
                                                   'initial_debit': (line['start_balance'] < 0 and -line['start_balance']) or 0.0,
                                                   'initial_credit': (line['start_balance'] > 0 and line['start_balance']) or 0.0,
                                                   'initial_currency_debit': (line['cur_start_balance'] < 0 and -line['cur_start_balance']) or 0.0,
                                                   'initial_currency_credit': (line['cur_start_balance'] > 0 and line['cur_start_balance']) or 0.0,
                                                   'debit': line['debit'] or 0.0,
                                                   'credit': line['credit'] or 0.0,
                                                   'currency_debit': line['cur_debit'] or 0.0,
                                                   'currency_credit': line['cur_credit'] or 0.0,
                                                   'end_debit': (end_balance < 0 and -end_balance) or 0.0,
                                                   'end_credit': (end_balance > 0 and end_balance) or 0.0,
                                                   'end_currency_debit': (cur_end_balance < 0 and -cur_end_balance) or 0.0,
                                                   'end_currency_credit': (cur_end_balance > 0 and cur_end_balance) or 0.0,
                                                   'transaction_id': self.id
                                                  })
        return True

class AccountTransactionBalanceAnalyticLine(models.Model):
    _name = 'account.transaction.balance.analytic.line'
    _description = 'Account Transaction Balance Analytic Line'

    account_id = fields.Many2one('account.account', string="Account")
    account_type_id = fields.Many2one('account.account.type', string="Account Type")
    code = fields.Char('Account Code')
    name = fields.Char('Account Name')
    analytic_account_id = fields.Many2one('account.analytic.account', string="Analytic Account")
    analytic_account_code = fields.Char('Analytic Account Code')
    analytic_account_name = fields.Char('Analytic Account Name')
    transaction_id = fields.Many2one('account.transaction.balance', string='Transaction Balance')
    initial_debit = fields.Float(string='Initial /Dt/', digits=(16, 2), default=0)
    initial_credit = fields.Float(string='Initial /Ct/', digits=(16, 2), default=0)
    debit = fields.Float(string='Transaction /Dt/', digits=(16, 2), default=0)
    credit = fields.Float(string='Transaction /Ct/', digits=(16, 2), default=0)
    end_debit = fields.Float(string='End /Dt/', digits=(16, 2), default=0)
    end_credit = fields.Float(string='End /Ct/', digits=(16, 2), default=0)

    @api.multi
    def button_initial_analytic_lines(self):
        # Эхний үлдэгдлийн шинжилгээний бичилтийг харуулах
        for line in self:
            if line.transaction_id.without_profit_revenue and line.transaction_id.company_id and line.transaction_id.company_id.period_journal_id:
                move_lines = self.env['account.move.line'].search([('account_id', '=', line.account_id.id),
                                                                   ('journal_id', '=', line.transaction_id.company_id.period_journal_id.id),
                                                                   ('date', '<', line.transaction_id.date_from)])
                if move_lines:
                    return {
                        'name': _('Account Analytic Lines'),
                        'view_type': 'form',
                        'view_mode': 'tree,form',
                        'res_model': 'account.analytic.line',
                        'view_id': False,
                        'type': 'ir.actions.act_window',
                        'domain': [('account_id', '=', line.analytic_account_id.id), ('general_account_id', '=', line.account_id.id),
                                   ('date', '<', line.transaction_id.date_from), ('move_id', 'not in', move_lines.ids)],
                }
            return {
                'name': _('Account Analytic Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.analytic.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'domain': [('account_id', '=', line.analytic_account_id.id), ('general_account_id', '=', line.account_id.id), ('date', '<', line.transaction_id.date_from)],
            }

    @api.multi
    def button_analytic_lines(self):
        # Хугацааны хоорондох шинжилгээний бичилтийг харуулах
        for line in self:
            if line.transaction_id.without_profit_revenue and line.transaction_id.company_id and line.transaction_id.company_id.period_journal_id:
                move_lines = self.env['account.move.line'].search([('account_id', '=', line.account_id.id),
                                                                   ('journal_id', '=', line.transaction_id.company_id.period_journal_id.id),
                                                                   ('date', '>=', line.transaction_id.date_from), ('date', '<=', line.transaction_id.date_to)])
                if move_lines:
                    return {
                        'name': _('Account Analytic Lines'),
                        'view_type': 'form',
                        'view_mode': 'tree,form',
                        'res_model': 'account.analytic.line',
                        'view_id': False,
                        'type': 'ir.actions.act_window',
                        'domain': [('account_id', '=', line.analytic_account_id.id), ('general_account_id', '=', line.account_id.id),
                                   ('date', '>=', line.transaction_id.date_from), ('date', '<=', line.transaction_id.date_to), ('move_id', 'not in', move_lines.ids)],
                    }
            return {
                'name': _('Account Analytic Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.analytic.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'domain': [('account_id', '=', line.analytic_account_id.id), ('general_account_id', '=', line.account_id.id),
                           ('date', '>=', line.transaction_id.date_from), ('date', '<=', line.transaction_id.date_to)],
            }

class AccountTransactionBalanceCurrencyAnalyticLine(models.Model):
    _name = 'account.transaction.balance.currency.analytic.line'
    _description = 'Account Transaction Balance Currency Analytic Line'

    account_id = fields.Many2one('account.account', string='Account')
    account_type_id = fields.Many2one('account.account.type', string="Account Type")
    code = fields.Char('Account Code')
    name = fields.Char('Account Name')
    currency_id = fields.Many2one('res.currency', string='Currency')
    analytic_account_id = fields.Many2one('account.analytic.account', string="Analytic Account")
    analytic_account_code = fields.Char('Analytic Account Code')
    analytic_account_name = fields.Char('Analytic Account Name')
    transaction_id = fields.Many2one('account.transaction.balance', string='Transaction Balance')
    initial_debit = fields.Float(string='Initial Mnt /Dt/', digits=(16, 2), default=0)
    initial_credit = fields.Float(string='Initial Mnt /Ct/', digits=(16, 2), default=0)
    initial_currency_debit = fields.Float(string='Initial Currency /Dt/', digits=(16, 2), default=0)
    initial_currency_credit = fields.Float(string='Initial Currency /Ct/', digits=(16, 2), default=0)
    debit = fields.Float(string='Transaction Mnt /Dt/', digits=(16, 2), default=0)
    credit = fields.Float(string='Transaction Mnt /Ct/', digits=(16, 2), default=0)
    currency_debit = fields.Float(string='Transaction Currency /Dt/', digits=(16, 2), default=0)
    currency_credit = fields.Float(string='Transaction Currency /Ct/', digits=(16, 2), default=0)
    end_debit = fields.Float(string='End Mnt /Dt/', digits=(16, 2), default=0)
    end_credit = fields.Float(string='End Mnt /Ct/', digits=(16, 2), default=0)
    end_currency_debit = fields.Float(string='End Currency /Dt/', digits=(16, 2), default=0)
    end_currency_credit = fields.Float(string='End Currency /Ct/', digits=(16, 2), default=0)

    @api.multi
    def button_initial_analytic_lines(self):
        # Эхний үлдэгдлийн шинжилгээний бичилтийг харуулах
        for line in self:
            move_lines = self.env['account.move.line'].search([('account_id', '=', line.account_id.id),
                                                               ('journal_id', '=', line.transaction_id.company_id.period_journal_id.id),
                                                               ('date', '<', line.transaction_id.date_from)])
            if move_lines:
                return {
                    'name': _('Account Analytic Lines'),
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'res_model': 'account.analytic.line',
                    'view_id': False,
                    'type': 'ir.actions.act_window',
                    'domain': [('account_id', '=', line.analytic_account_id.id),
                               ('general_account_id', '=', line.account_id.id),
                               ('date', '<', line.transaction_id.date_from), ('move_id', 'not in', move_lines.ids)],
                }
            return {
                'name': _('Account Analytic Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.analytic.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'domain': [('account_id', '=', line.analytic_account_id.id),('general_account_id', '=', line.account_id.id),('date', '<', line.transaction_id.date_from)],
            }

    @api.multi
    def button_analytic_lines(self):
        # Хугацааны хоорондох шинжилгээний бичилтийг харуулах
        for line in self:
            if line.transaction_id.without_profit_revenue and line.transaction_id.company_id and line.transaction_id.company_id.period_journal_id:
                move_lines = self.env['account.move.line'].search([('account_id', '=', line.account_id.id),
                                                                   ('journal_id', '=', line.transaction_id.company_id.period_journal_id.id),
                                                                   ('date', '>=', line.transaction_id.date_from), ('date', '<=', line.transaction_id.date_to)])
                if move_lines:
                    print ('move_lines %s' %move_lines)
                    return {
                        'name': _('Account Analytic Lines'),
                        'view_type': 'form',
                        'view_mode': 'tree,form',
                        'res_model': 'account.analytic.line',
                        'view_id': False,
                        'type': 'ir.actions.act_window',
                        'domain': [('account_id', '=', line.analytic_account_id.id), ('general_account_id', '=', line.account_id.id),
                                   ('date', '>=', line.transaction_id.date_from), ('date', '<=', line.transaction_id.date_to), ('move_id', 'not in', move_lines.ids)],
                    }

            return {
                'name': _('Account Analytic Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.analytic.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'domain': [('account_id', '=', line.analytic_account_id.id),('general_account_id', '=', line.account_id.id),
                           ('date', '>=', line.transaction_id.date_from), ('date', '<=', line.transaction_id.date_to)],
            }