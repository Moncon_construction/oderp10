# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from operator import itemgetter
from odoo.exceptions import UserError
from odoo import api, models, _
from openerp.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles

class AccountEquityChangeReport(models.Model):
    _inherit = "account.equity.change.report"
    
    @api.multi
    def get_title(self, sheet, rowx, colx_number, report_name, format_name, format_filter):
        #Тайлангийн гарчиг зурах
        
        # create name
        sheet.merge_range(rowx, 0, rowx, colx_number, '%s: %s' % (_('Company Name'), self.company_id.name), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 0, rowx+1, colx_number, report_name.upper(), format_name)
        rowx += 2
        # create duration
        sheet.merge_range(rowx, 0, rowx, colx_number, '%s: %s - %s' % (_('Duration Date'), self.date_from, self.date_to), format_filter)
        rowx += 1
        # create date
        sheet.merge_range(rowx, 0, rowx, colx_number, '%s: %s' % (_('Created Date'), time.strftime('%Y-%m-%d')), format_filter)
        if self.show_analytic_account:
            rowx += 1
            analytics = ''
            analytic_account_ids = []
            # Шинжилгээний данс сонгоогүй тохиолдолд тухайн огнооны бүх шинжилгээний дансыг олж дүнг харуулах
            if self.analytic_account_ids:
                analytic_account_ids = self.analytic_account_ids
            else:
                analytic_account_ids = self.env['account.analytic.account'].search([])
                
            for analytic in analytic_account_ids:
                if analytics:
                    analytics += ', '
                analytics += analytic.name 
            sheet.write(rowx, 0, '%s: %s' % (_('Analytic accounts'), analytics), format_filter)
        rowx += 2
        return sheet, rowx