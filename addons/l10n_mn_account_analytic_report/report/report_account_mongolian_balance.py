# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell

from odoo import api, models, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class AccountMongolianBalanceSheet(models.Model):
    _inherit = 'account.mongolian.balance.sheet'

    @api.multi
    def get_sheet(self, sheet):
        sheet, colx_number = super(AccountMongolianBalanceSheet, self).get_sheet(sheet)
        if self.show_analytic_account:
            # compute column
            colx_number = 5
            sheet.set_column('E:NZ', 18)
        return sheet, colx_number

    @api.multi
    def get_header(self, sheet, rowx, format_title, format_title_small, str_start, str_stop):
        sheet = super(AccountMongolianBalanceSheet, self).get_header(sheet, rowx, format_title, format_title_small, str_start, str_stop)
        if self.show_analytic_account:
            count = 0
            # Шинжилгээний дансны тайлангийн хүснэгтийн толгой зурах
            for analytic_account in self.all_analytic_account_ids:
                col = count * 2 + 4
                sheet.merge_range(rowx, col, rowx, col + 1, u'[%s] %s' % (analytic_account.code or '', analytic_account.name or ''), format_title)
                sheet.write(rowx + 1, col, str_start, format_title_small)
                sheet.write(rowx + 1, col + 1, str_stop, format_title_small)
                count += 1
        return sheet

    @api.multi
    def get_value(self, sheet, rowx, format_text, format_float, line):
        # Тайлангийн мөр зурах
        if not line.parent_id:
            sheet, rowx = super(AccountMongolianBalanceSheet, self).get_value(sheet, rowx, format_text, format_float, line)
            if self.show_analytic_account:
                count = 0
                for analytic_account in self.all_analytic_account_ids:
                    col = count * 2 + 4
                    if line.type == 'account_report' and not line.financial_report_id.account_report_id:
                        sheet.write(rowx - 1, col, u'', format_float)
                        sheet.write(rowx - 1, col + 1, u'', format_float)
                    else:
                        analytic_line = self.env['account.mongolian.balance.sheet.line'].search([('analytic_account_id', '=', analytic_account.id), ('parent_id', '=', line.id)], limit=1)
                        if analytic_line:
                            sheet.write(rowx - 1, col, analytic_line.initial_balance, format_float)
                            sheet.write(rowx - 1, col + 1, analytic_line.end_balance, format_float)
                        else:
                            sheet.write(rowx - 1, col, 0, format_float)
                            sheet.write(rowx - 1, col + 1, 0, format_float)
                    count += 1
        return sheet, rowx