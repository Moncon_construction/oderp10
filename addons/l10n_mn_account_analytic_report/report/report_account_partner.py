# -*- encoding: utf-8 -*-
##############################################################################
from odoo import api, models
from odoo.tools.translate import _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class AccountPartnerReport(models.Model):
    _inherit = 'account.partner.report'

    @api.multi
    def get_column(self, sheet, type):
        colx = 6
        # compute column
        sheet.set_column('A:A', 6)
        sheet.set_column('B:B', 30)
        if self.show_analytic_account:
            colx += 1
            sheet.set_column('C:C', 20)
            if type == 'balance':
                colx += 4
                sheet.set_column('D:E', 8)
                sheet.set_column('F:F', 25)
                sheet.set_column('G:H', 10)
                sheet.set_column('I:R', 20)
            else:
                sheet.set_column('D:D', 10)
                sheet.set_column('E:M', 20)
        else:
            if type == 'balance':
                colx += 4
                sheet.set_column('C:D', 8)
                sheet.set_column('E:E', 25)
                sheet.set_column('F:G', 10)
                sheet.set_column('H:R', 20)
            else:
                sheet.set_column('C:C', 10)
                sheet.set_column('D:R', 20)
        if self.show_currency_transaction:
            colx += 4
        return sheet, colx

    @api.multi
    def get_text_header(self, sheet, rowx, format_title):
        # Тайлангийн хүснэгтийн толгой зурах
        row = 1
        if self.show_currency_transaction:
            row = 2
        sheet.merge_range(rowx, 0, rowx + row, 0, _('Seq'), format_title)
        if self.group_by == 'salesman':
            sheet.merge_range(rowx, 1, rowx + row, 1, _('Account/Salesman/Partner\n Reference'), format_title)
        else:
            sheet.merge_range(rowx, 1, rowx + row, 1, _('Partner/Account\n Reference'), format_title)
        col = 2
        if self.show_analytic_account:
            sheet.merge_range(rowx, col, rowx + row, col, _('Analytic Account'), format_title)
            col += 1
        sheet.merge_range(rowx, col, rowx + row, col, _('Currency'), format_title)
        return sheet, col + 1

    @api.multi
    def get_balance_header(self, sheet, rowx, format_title):
        # Дэлгэрэнгүй тайлангийн хүснэгтийн толгой зурах
        row = 1
        if self.show_currency_transaction:
            row = 2
        sheet.merge_range(rowx, 0, rowx + row, 0, _('Seq'), format_title)
        if self.group_by == 'salesman':
            sheet.merge_range(rowx, 1, rowx + row, 1, _('Account/Salesman/Partner\n Reference'), format_title)
        else:
            sheet.merge_range(rowx, 1, rowx + row, 1, _('Partner/Account\n Reference'), format_title)
        col = 2
        if self.show_analytic_account:
            sheet.merge_range(rowx, col, rowx + row, col, _('Analytic Account'), format_title)
            col += 1
        sheet.merge_range(rowx, col, rowx + row, col, _('Origin'), format_title)
        sheet.merge_range(rowx, col + 1, rowx + row, col + 1, _('Date'), format_title)
        sheet.merge_range(rowx, col + 2, rowx + row, col + 2, _('Transaction name'), format_title)
        sheet.merge_range(rowx, col + 3, rowx + row, col + 3, _('Currency'), format_title)
        sheet.merge_range(rowx, col + 4, rowx + row, col + 4, _('Currency Rate'), format_title)
        col += 5
        return sheet, col

    @api.multi
    def get_lines(self):
        lines = self.line_ids
        if self.show_currency_transaction and not self.show_analytic_account:
            lines = self.currency_line_ids
        elif not self.show_currency_transaction and self.show_analytic_account:
            lines = self.analytic_line_ids
        elif self.show_currency_transaction and self.show_analytic_account:
            lines = self.analytic_currency_line_ids
        return lines

    @api.multi
    def get_balance_line(self):
        # Данснуудын id-г олох
        account_ids, is_payable, is_receivable = self.get_account_ids()
        # Харилцагчийн id-г олох
        partner_ids = self.get_partner_ids()
        # Борлуулалтын ажилтан id-г олох
        salesman_ids = self.get_salesman_ids()
        # Эрэмбэлэлт
        order_by = self.get_order_by('balance')
        if self.show_analytic_account:
            # Шинжилгээний дансны id-г олох
            analytic_ids = self.get_analytic_ids()
            # Эхлэл болон тухайн хугацааны хоорондох утгыг олох
            lines = self.env['account.move.line'].with_context(salesman_ids=salesman_ids, partner_ids=partner_ids, order_by=order_by) \
                                    .get_all_analytic_partner_balance(self.company_id.id, account_ids, analytic_ids, self.date_from, self.date_to)
        else:
            # Эхлэл болон тухайн хугацааны хоорондох утгыг олох
            lines = self.env['account.move.line'].with_context(salesman_ids=salesman_ids, partner_ids=partner_ids, order_by=order_by) \
                                    .get_all_partner_balance(self.company_id.id, account_ids, self.date_from, self.date_to, self.target_move)
        return lines, is_payable, is_receivable

    @api.multi
    def get_text_value(self, sheet, rowx, format, line):
        # Тайлангийн мөрийн текст утгыг зурах
        col = 2
        sheet.write(rowx, 0, line.sequence, format['content_center'])
        if self.show_analytic_account:
            col += 1
        if line.color == 'black':
            sheet.write(rowx, 1, line.name or '', format['content_text'])
            if self.show_analytic_account:
                sheet.write(rowx, col - 1, line.analytic_account_id.name, format['content_text'])
            sheet.write(rowx, col, line.currency_id.name, format['content_center'])
        else:
            sheet.merge_range(rowx, 1, rowx, col, line.group_name, format['group'])
        return sheet, col

    @api.multi
    def get_balance_text_value(self, sheet, rowx, format, line, sequence, name, is_group):
        # Тайлангийн бүлэглэлт болон мөрийн текст утгыг зурах
        colx = 6
        if self.show_analytic_account:
            colx += 1
        if is_group:
            col = 0
            if sequence:
                sheet.write(rowx, 0, sequence, format['group'])
                col = 1
            sheet.merge_range(rowx, col, rowx, colx, name, format['group_left'] if sequence else format['group'])
        else:
            text = u''
            invoice = False
            if line['invoice']:
                invoice = self.env['account.invoice'].search([('id', '=', line['invoice'])])
                text = invoice.origin if invoice and invoice.origin else ''
            number = (invoice and invoice.number or '') or line['mname'] or line['ref']
            sheet.write(rowx, 0, sequence, format['content_center'])
            sheet.write(rowx, 1, number, format['content_text'])
            if self.show_analytic_account:
                sheet.write(rowx, 2, line['aaname'], format['content_text'])
            sheet.write(rowx, colx - 4, text, format['content_text'])
            sheet.write(rowx, colx - 3, line['ml_date'], format['content_center'])
            sheet.write(rowx, colx - 2, line['name'], format['content_text'])
            sheet.write(rowx, colx - 1, line['cname'] or self.company_id.currency_id and self.company_id.currency_id.name or '', format['content_center'])
            sheet.write(rowx, colx, line['currency_rate'], format['content_center'])
        return sheet, colx, rowx