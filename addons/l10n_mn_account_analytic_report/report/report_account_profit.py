# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from operator import itemgetter
from odoo.exceptions import UserError
from datetime import date, datetime

from odoo import api, models, _
from openerp.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles

class AccountProfitReport(models.Model):
    _inherit = 'account.profit.report'
    
    @api.multi
    def get_sheet(self, sheet):
        sheet, colx_number = super(AccountProfitReport, self).get_sheet(sheet)
        if self.show_analytic_account:
            # compute column
            colx_number = 4
            sheet.set_column('C:ZZ', 15)
        return sheet, colx_number
    
    @api.multi
    def get_title(self, sheet, rowx, col, report_name, format_name):
        #Тайлангийн гарчиг зурах
        sheet = super(AccountProfitReport, self).get_title(sheet, rowx, col, report_name, format_name)
        if self.show_analytic_account:
            count = 0
            for analytic_account in self.analytic_account_ids:
                count += 1
            col += count * 2 - 1
        sheet.merge_range(rowx, 0, rowx+1, col, report_name.upper(), format_name)
        return sheet
    
    @api.multi
    def get_header(self, sheet, rowx, format_title, initial_date, balance_date):
        sheet = super(AccountProfitReport, self).get_header(sheet, rowx, format_title, initial_date, balance_date)
        if self.show_analytic_account:
            count = 0
            # Шинжилгээний дансны тайлангийн хүснэгтийн толгой зурах
            for analytic_account in self.analytic_account_ids:
                col = count * 2 + 4
                sheet.merge_range(rowx, col, rowx, col + 1, u'[%s] %s' % (analytic_account.code or '', analytic_account.name or ''), format_title)
                sheet.write(rowx + 1, col, initial_date, format_title)
                sheet.write(rowx + 1, col + 1, balance_date, format_title)
                count += 1
        return sheet
    
    @api.multi
    def get_value(self, sheet, rowx, format_number,format_text, format_float, line):
        # Тайлангийн мөр зурах
        sheet, rowx = super(AccountProfitReport, self).get_value(sheet, rowx, format_number,format_text, format_float, line)
        if self.show_analytic_account:
            count = 0
            for analytic_account in self.analytic_account_ids:
                col = count * 2 + 4
                if line.account_report_type == 'account_report' and not line.account_financial_report_id.account_report_id:
                    sheet.write(rowx - 1, col, u'', format_float)
                    sheet.write(rowx - 1, col + 1, u'', format_float)
                else:
                    sheet.write(rowx - 1, col, line.initial_balance, format_float)
                    sheet.write(rowx - 1, col + 1, line.end_balance, format_float)
                count += 1
        return sheet, rowx

    @api.multi
    def get_footer(self, sheet, rowx, col, text, format_name):
        #Тайлангийн хөл зурах
        sheet = super(AccountProfitReport, self).get_footer(sheet, rowx, col, text, format_name)
        if self.show_analytic_account:
            count = 0
            for analytic_account in self.analytic_account_ids:
                count += 1
            col += count * 2
        sheet.merge_range(rowx, 0, rowx, col, '%s: ........................................... (                          )' % _(text), format_name)
        return sheet