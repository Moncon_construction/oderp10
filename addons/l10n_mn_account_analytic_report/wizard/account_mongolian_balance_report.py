# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
from datetime import datetime, timedelta
import xlsxwriter

from odoo.exceptions import UserError
from odoo import api, fields, models, _
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT

from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class AccountMongolianBalanceReport(models.TransientModel):
    _inherit = 'account.mongolian.balance.report'

    show_analytic_account = fields.Boolean('Show analytic account', default=False)
    analytic_account_ids = fields.Many2many('account.analytic.account', string="Analytic accounts filter", help="Shows all analytic accounts when selected nothing.")

    @api.multi
    def export_report(self):
        #create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        #create name
        report_name = _('Balance sheet')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_title_small_color = book.add_format(ReportExcelCellStyles.format_title_small_color)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_bold_text = book.add_format(ReportExcelCellStyles.format_content_bold_text)
        format_content_bold_float = book.add_format(ReportExcelCellStyles.format_content_bold_float)

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('balance_sheet'), form_title=file_name, date_to=self.date_to, date_from=self.date_from).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 1

        # compute column
        colx_number = 3
        sheet.set_column('A:A', 8)
        sheet.set_column('B:B', 45)
        sheet.set_column('C:D', 18)
        sheet.set_column('E:NZ', 15)

        # create name
        names = ''
        for company in self.companies:
            if names:
                names += ', '
            names += company.name
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s' % (_('Company'), names), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 0, rowx + 1, colx_number, report_name.upper(), format_name)
        rowx += 2

        # create duration
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s - %s' % (_('Duration'), self.date_from, self.date_to), format_filter)
        rowx += 1

        # create date
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s' % (_('Created On'), time.strftime('%Y-%m-%d')), format_filter)
        rowx += 2

        str_start = (datetime.strptime(self.date_from, '%Y-%m-%d') - timedelta(1))
        str_start = _('%s year %s month %s day') % (str_start.strftime('%Y'), str_start.strftime('%m'), str_start.strftime('%d'))
        str_stop = datetime.strptime(self.date_to, '%Y-%m-%d')
        str_stop = _('%s year %s month %s day') % (str_stop.strftime('%Y'), str_stop.strftime('%m'), str_stop.strftime('%d'))

        # Тайлангийн хүснэгтийн толгойг зурж байна.
        sheet.merge_range(rowx, 0, rowx + 1, 0, _('Line Number'), format_title)
        sheet.merge_range(rowx, 1, rowx + 1, 1, _('Indication'), format_title)
        sheet.merge_range(rowx, 2, rowx, 3, _('Balance'), format_title)
        sheet.write(rowx + 1, 2, str_start, format_title_small_color)
        sheet.write(rowx + 1, 3, str_stop, format_title_small_color)

        if not self.account_report_id:
            raise UserError(_('Please select a financial report.'))
        # Тухайн тайлангийн бүтцэд хамаарах хүүхдүүдийг хайж байна.
        # Өмнөх өдрөөр эхний үлдэгдлийг татна
        before_day = datetime.strptime(self.date_from, DEFAULT_SERVER_DATE_FORMAT) + timedelta(days=-1)
        # FIXME: Олон компаниар үед давтаж ажиллуулах. Operator буруу байгаа (company_id=self.companies.ids)
        init_report_ids = self.account_report_id.with_context(date_to=before_day.strftime(DEFAULT_SERVER_DATE_FORMAT), state=self.target_move, company_id=self.companies.ids)._get_children_by_order()
        end_report_ids = self.account_report_id.with_context(date_to=self.date_to, state=self.target_move, company_id=self.companies.ids)._get_children_by_order()

        # Шинжилгээний дансаар харуулах талбарыг чекэлснээр шинжилгээний дансааар тайлан гарна
        analytic_accounts = analytic_account_ids = []
        company_ids = '(' + ','.join(map(str, self.companies.ids)) + ')'
        # Шинжилгээний дансаар тайланг гаргах эсэх
        if self.show_analytic_account == True:
            # Шинжилгээний данснууд шалгана
            if self.analytic_account_ids:
                for account in self.analytic_account_ids:
                    analytic_accounts.append(account.id)
                analytic_account_ids = '(' + ', '.join(map(str, self.analytic_account_ids.ids)) + ')'
            else:
                self._cr.execute("""SELECT  l.account_id 
                                     FROM account_analytic_line l 
                                     JOIN account_account a ON (a.id = l.general_account_id) 
                                     JOIN account_analytic_account aa ON (aa.id = l.account_id) 
                                     WHERE  aa.type = 'normal' AND aa.active = 't' AND 
                                            l.company_id in %s AND l.date <= '%s' AND 
                                            a.internal_type not in ('expense', 'income') 
                                     GROUP BY l.account_id 
                                     ORDER BY l.account_id """% (company_ids, self.date_to))
                analytic_accounts = map(lambda x: x[0], self._cr.fetchall())
                analytic_account_ids = tuple(analytic_accounts)
            # Шинжилгээний дансны дүнгийн dic
            analytics = self.env['account.financial.report']._compute_report_analytic(end_report_ids, analytic_account_ids, company_ids, before_day.strftime(DEFAULT_SERVER_DATE_FORMAT), self.date_to)
            if analytic_accounts:
                for analytic_account in self.env['account.analytic.account'].browse(analytic_accounts):
                    # Шинжилгээний дансны  толгойг зурж байна.
                    col = analytic_accounts.index(analytic_account.id) * 2 + 4
                    sheet.merge_range(rowx, col, rowx, col + 1, u"%s" % analytic_account.name, format_title_small_color)
                    sheet.write(rowx + 1, col, str_start, format_title_small_color)
                    sheet.write(rowx + 1, col + 1, str_stop, format_title_small_color)

        rowx += 2
        parent_numbers = {}
        parent_index = 1

        for account in init_report_ids:
            for end_account in end_report_ids:
                if end_account.id == account.id:
                    if account.id == self.account_report_id.id:
                        continue
                    # Тайлангийн мөрийн дугаарыг тооцож байна.
                    number = parent_index
                    if account.parent_id.id not in parent_numbers:
                        parent_numbers[account.id] = [str(number), 0]
                        parent_index += 1
                    else:
                        number = parent_numbers[account.parent_id.id][0] + '.' + str(
                            parent_numbers[account.parent_id.id][1] + 1)
                        parent_numbers[account.parent_id.id][1] += 1
                        parent_numbers[account.id] = [str(number), 0]

                    # Үлдэгдлийг авч байна.
                    balance = account.balance

                    # Тайлангийн шатлалын дагуу мөрийн дугаарлалтыг олж байна
                    number_str = '%s' % number
                    name_str = account.name
                    if account.style_overwrite == 1:
                        number_str = '%s' % number_str
                        name_str = u'%s' % name_str
                    elif account.style_overwrite == 2:
                        name_str = u'%s' % name_str

                    # Тайлангийн бүтэц нь 4 төрөлтэй бөгөөд дэлгэц, тайлангийн утга төрөлтэй бол нийлбэрийг харуулахгүй
                    if account.type in ('account_report'):
                        if account.style_overwrite in [1, 2, 3]:
                            sheet.write(rowx, 0, number_str, format_content_bold_text)
                            sheet.write(rowx, 1, u"   %s" % name_str, format_content_bold_text)
                            if not account.account_report_id:
                                sheet.write(rowx, 2, u'', format_content_bold_float)
                                sheet.write(rowx, 3, u'', format_content_bold_float)
                            else:
                                sheet.write(rowx, 2, balance, format_content_bold_float)
                                sheet.write(rowx, 3, end_account.balance, format_content_bold_float)
                        else:
                            sheet.write(rowx, 0, number_str, format_content_text)
                            sheet.write(rowx, 1, u"   %s" % name_str, format_content_text)
                            if not account.account_report_id:
                                sheet.write(rowx, 2, u'', format_content_float)
                                sheet.write(rowx, 3, u'', format_content_float)
                            else:
                                sheet.write(rowx, 2, balance, format_content_float)
                                sheet.write(rowx, 3, end_account.balance, format_content_float)
                    else:
                        if account.style_overwrite in [1, 2, 3]:
                            sheet.write(rowx, 0, number_str, format_content_bold_text)
                            sheet.write(rowx, 1, u"     %s" % name_str, format_content_bold_text)
                            sheet.write(rowx, 2, balance, format_content_bold_float)
                            sheet.write(rowx, 3, end_account.balance, format_content_bold_float)
                        else:
                            sheet.write(rowx, 0, number_str, format_content_text)
                            sheet.write(rowx, 1, u"     %s" % name_str, format_content_text)
                            sheet.write(rowx, 2, balance, format_content_float)
                            sheet.write(rowx, 3, end_account.balance, format_content_float)

                    # Шинжилгээний дансаар гаргах эсэхийг шалгаж шинжилгээний дансны дүнг зурна
                    if analytic_accounts:
                        # Шинжилгээний дансны дүнгийн dic санхүүгийн тайлангийн id агуулсан
                        for key, value in sorted(analytics.iteritems()):
                            bef_sum = sum = 0
                            # Тухайн санхүүгийн тайлангийн id-тай шинжилгээний дансны дүнгийн санхүүгийн тайлангийн id тэнцүү эсэхийг шалгана
                            if account.id == key:
                                # Шинжилгээний дансны толгой dic
                                for analytic_account in analytic_accounts:
                                    check = False
                                    col = analytic_accounts.index(analytic_account) * 2 + 4
                                    if account.type == 'account_report' and not account.account_report_id:
                                        sheet.write(rowx, col, u'', format_content_bold_float)
                                        sheet.write(rowx, col + 1, u'', format_content_bold_float)
                                    else:
                                        # Шинжилгээний дансны дүнгийн dic
                                        for val in value:
                                            # Шинжилгээний дансны дүнг тайланд зурах хэсэг
                                            if val[0] == analytic_account:
                                                if account.type == 'account_report':
                                                    sheet.write(rowx, col, val[1], format_content_bold_float)
                                                    sheet.write(rowx, col + 1, val[2], format_content_bold_float)
                                                else:
                                                    sheet.write(rowx, col, val[1], format_content_float)
                                                    sheet.write(rowx, col+1, val[2], format_content_float)
                                                check = True
                                                bef_sum += val[1]
                                                sum += val[2]
                                        # Утга байхгүй хэсгүүдийг 0-ээр дүүргэнэ
                                        if check == False:
                                            if account.type == 'account_report':
                                                sheet.write(rowx, col, 0, format_content_bold_float)
                                                sheet.write(rowx, col + 1, 0, format_content_bold_float)
                                            else:
                                                sheet.write(rowx, col, 0, format_content_float)
                                                sheet.write(rowx, col + 1, 0, format_content_float)
                    rowx += 1
        rowx += 2
        sheet.merge_range(rowx, 1, rowx, 2, '%s: ........................................... (                          )' % _('Executive Director'), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 1, rowx, 2, '%s: ........................................... (                          )' % _('General Accountant'), format_filter)

        book.close()

        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()