# -*- coding: utf-8 -*-
from odoo import models, api, fields, _
from io import BytesIO
import xlsxwriter
import time
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
import base64


class ReportAccountPartnerLedger(models.TransientModel):
    _inherit = 'report.account.partner.ledger'

    show_analytic_account = fields.Boolean('Show analytic account')
    analytic_account_ids = fields.Many2many('account.analytic.account', string="Analytic accounts filter", help="Shows all analytic accounts when selected nothing.")

    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        print('analytic partner balance')

        # create name
        report_name = _('Partner Ledger Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'))

        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_group_left = book.add_format(ReportExcelCellStyles.format_group_left)
        format_group = book.add_format(ReportExcelCellStyles.format_group)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_center = book.add_format(ReportExcelCellStyles.format_content_center)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_float_color = book.add_format(ReportExcelCellStyles.format_content_float_color)
        format_content_bold_text = book.add_format(ReportExcelCellStyles.format_content_bold_text)
        format_content_bold_float = book.add_format(ReportExcelCellStyles.format_content_bold_float)

        move_line_obj = self.env['account.move.line']
        account_obj = self.env['account.account']
        partner_obj = self.env['res.partner']
        salesman_obj = self.env['res.users']
        AccountAnalyticAccount = self.env['account.analytic.account']
        # Ашиглах хувьсагчууд
        seq = sub_seq_first = sub_seq = check = 1
        total_start = total_currency_start = total_debit = total_currency_debit = 0
        total_credit = total_currency_credit = total_end = total_currency_end = 0
        sub_start = sub_currency_start = sub_debit = sub_currency_debit = 0
        sub_credit = sub_currency_credit = sub_end = sub_currency_end = 0
        type_start = type_currency_start = type_debit = type_currency_debit = 0
        type_credit = type_currency_credit = type_end = type_currency_end = 0
        first_rowx = 0
        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('partner_ledger_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 1

        # compute column
        sheet.set_column('A:A', 4)
        sheet.set_column('B:B', 40)
        sheet.set_column('C:C', 8 if not self.show_analytic_account else 20)
        sheet.set_column('D:D', 15 if not self.show_analytic_account else 8)
        sheet.set_column('E:E', 15)
        sheet.set_column('F:F', 15)
        sheet.set_column('G:G', 15)
        sheet.set_column('H:H', 15)

        # create name
        sheet.merge_range(rowx, 0, rowx, 1, '%s: %s' % (_('Company'), self.company_id.name), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 0, rowx, 6, report_name.upper(), format_name)
        rowx += 1

        # create duration
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s - %s' % (_('Duration'), self.date_from, self.date_to), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 0, rowx, 1, '%s: %s' % (_('Printed date'), time.strftime('%Y-%m-%d')), format_filter)
        rowx += 2

        # table header
        sheet.merge_range(rowx, 0, rowx + 1, 0, _('Seq'), format_title)
        if self.group_by == 'salesman':
            sheet.merge_range(rowx, 1, rowx + 1, 1, _('Account/Salesman/Partner\n Reference'), format_title)
        else:
            sheet.merge_range(rowx, 1, rowx + 1, 1, _('Partner/Account\n Reference'), format_title)
        column = 0
        if self.show_analytic_account:
            sheet.merge_range(rowx, 2, rowx + 1, 2, _('Analytic account'), format_title)
            column += 1
        sheet.merge_range(rowx, column + 2, rowx + 1, column + 2, _('Currency'), format_title)
        sheet.merge_range(rowx, column + 3, rowx + 1, column + 3, _('Initial Balance'), format_title)
        sheet.merge_range(rowx, column + 4, rowx, column + 5, _('Transaction Amount'), format_title)
        sheet.write(rowx + 1, column + 4, _('Debit'), format_title)
        sheet.write(rowx + 1, column + 5, _('Credit'), format_title)
        sheet.merge_range(rowx, column + 6, rowx + 1, column + 6, _('End Balance'), format_title)
        rowx += 2

        # Данснуудын id-г олох
        account_ids = []
        if self.account_ids and self.account_type == 'choose_accounts':
            account_ids = self.account_ids.ids
        elif self.account_type == 'receivable':
            account_ids = account_obj.search([('internal_type', '=', self.account_type), ('company_id', '=', self.company_id.id)]).ids
        elif self.account_type == 'payable':
            account_ids = account_obj.search([('internal_type', '=', self.account_type), ('company_id', '=', self.company_id.id)]).ids
        else:
            account_ids = account_obj.search([('internal_type', 'in', ('receivable', 'payable')), ('company_id', '=', self.company_id.id)]).ids

        # Бүлэглэлт
        if self.group_by == 'partner':
            order_by = 'p.name, aa.code, aa.name'
        elif self.group_by == 'account':
            order_by = 'aa.code, aa.name, p.name'
        else:
            order_by = 'aa.code, aa.name, ru.id, p.name'

        analytic_ids = self.analytic_account_ids.ids if self.show_analytic_account else []

        # Харилцагч нарыг дамжуулах
        if self.partner_id:
            partner_ids = partner_obj.with_context(active_test=False).search([('parent_id', 'child_of', [self.partner_id.id])]).ids
        else:
            partner_ids = partner_obj.with_context(active_test=False).search([]).ids

        if self.group_by == 'salesman' and self.salesperson_id:
            salesman_ids = self.salesperson_id.ids
        else:
            salesman_ids = salesman_obj.search([]).ids
        if self.group_by == 'salesman':
            # Эхлэл болон тухайн хугацааны хоорондох утгыг олох
            if column:
                lines = move_line_obj.with_context(salesman_ids=salesman_ids, partner_ids=partner_ids, order_by=order_by, analytic_ids=analytic_ids).get_all_balance_with_analytic(self.company_id.id, account_ids, self.date_from, self.date_to, self.target_move)
            else:
                lines = move_line_obj.with_context(salesman_ids=salesman_ids, partner_ids=partner_ids, order_by=order_by).get_all_balance(self.company_id.id, account_ids, self.date_from, self.date_to, self.target_move)
        else:
            # Эхлэл болон тухайн хугацааны хоорондох утгыг олох
            if column:
                lines = move_line_obj.with_context(partner_ids=partner_ids, order_by=order_by, analytic_ids=analytic_ids).get_all_balance_with_analytic(self.company_id.id, account_ids, self.date_from, self.date_to, self.target_move)
            else:
                lines = move_line_obj.with_context(partner_ids=partner_ids, order_by=order_by).get_all_balance(self.company_id.id, account_ids, self.date_from, self.date_to, self.target_move)
        is_first = False
        if lines:
            id = name = salesman_id = check_salesman = group_line_id = group_line_name = line_name = account_type = type_name = atype = False
            before_rowx = 0
            for line in lines:
                analytic = ('[' + (AccountAnalyticAccount.browse(line['analytic_account_id']).code or '') + '] ' + (AccountAnalyticAccount.browse(line['analytic_account_id']).name or '')) if 'analytic_account_id' in line and line['analytic_account_id'] else ''
                end = line['start_balance'] + line['debit'] - line['credit']
                currency_end = line['cur_start_balance'] + line['cur_debit'] - line['cur_credit']
                # Үлдэгдэл дүн 0-с ялгаатай эсэхийг шалгах
                if self.display_partner == 'all' or (self.display_partner == 'non_zero_balance' and (end != 0 or currency_end != 0)):
                    # Тайланг харилцагчаар бүлэглэн гаргаж байгаа бол group-лэх утгыг авна
                    if self.group_by == 'partner':
                        group_line_id = line['pid']  # Бүлэглэх id
                        group_line_name = u'[%s] %s' % (line['pcode'],line['pname'])  # Бүлэглэх нэр
                        line_name = u'[%s] %s' % (line['acode'], line['aname'])  # Мөрийн нэр
                        # Тайланг авлага, өглөгийн дансаар эсвэл данс сонгож гаргасан бол Авлага, Өглөгөөр бүлэглэн гаргах хэсэг
                        if account_type and account_type != line['atype'] and self.account_type in ('receivable_payable', 'choose_accounts'):
                            sheet.merge_range(rowx, 0, rowx, column + 2, type_name, format_content_bold_text)
                            sheet.write(rowx, column + 3, type_start, format_content_bold_float)
                            sheet.write(rowx, column + 4, type_debit, format_content_bold_float)
                            sheet.write(rowx, column + 5, type_credit, format_content_bold_float)
                            sheet.write(rowx, column + 6, type_end, format_content_bold_float)
                            rowx += 1
                            # Валютын дүнг дараагийн мөр болгон зурна
                            if (type_currency_start or type_currency_debit or type_currency_credit or type_currency_end) != 0:
                                sheet.merge_range(rowx, 0, rowx, column + 2, '', format_content_text)
                                sheet.write(rowx, column + 3, type_currency_start, format_content_float_color)
                                sheet.write(rowx, column + 4, type_currency_debit, format_content_float_color)
                                sheet.write(rowx, column + 5, type_currency_credit, format_content_float_color)
                                sheet.write(rowx, column + 6, type_currency_end, format_content_float_color)
                                rowx += 1
                            type_start = type_currency_start = type_debit = type_currency_debit = 0
                            type_credit = type_currency_credit = type_end = type_currency_end = 0
                    # Тайланг дансаар бүлэглэн гаргаж байгаа бол group-лэх утгыг авна
                    elif self.group_by == 'account':
                        group_line_id = line['account_id']  # Бүлэглэх id
                        group_line_name = u'[%s] %s' % (line['acode'], line['aname'])  # Бүлэглэх нэр
                        line_name = u'%s' % line['pname']  # Мөрийн нэр
                    # Борлуулагчаар бүлэглэн гаргаж байгаа бол group-лэх утгыг авна
                    else:
                        group_line_id = line['account_id']  # Бүлэглэх id
                        group_line_name = u'[%s] %s' % (line['acode'], line['aname'])  # Бүлэглэх нэр
                        line_name = u'%s' % line['pname']  # Мөрийн нэр
                        salesman = line['salesman']
                    if not id:
                        # Эхний бүлэглэлтийн мөрийг зурна
                        sub_seq_first = 1
                        sheet.write(rowx, 0, seq, format_group)
                        sheet.write(rowx, 1, group_line_name, format_group_left)
                        is_first = True
                        first_rowx = rowx
                        # sheet.merge_range(rowx, 2, rowx, column + 6, '', format_group)
                        sheet.merge_range(rowx, 1, rowx, column + 6, group_line_name, format_group_left)
                        rowx += 1

                    # Бүлэглэлтийн id болон мөрийн id-г таарч байгаа эсэхийг шалгана
                    elif id != group_line_id:
                        if check_salesman:
                            if before_rowx != 0:
                                sheet.write_formula('D%s' % (before_rowx - 1), '{=SUM(D%s:D%s)}' % (before_rowx, rowx), format_group_float)
                                sheet.write_formula('E%s' % (before_rowx - 1), '{=SUM(E%s:E%s)}' % (before_rowx, rowx), format_group_float)
                                sheet.write_formula('F%s' % (before_rowx - 1), '{=SUM(F%s:F%s)}' % (before_rowx, rowx), format_group_float)
                                sheet.write_formula('G%s' % (before_rowx - 1), '{=SUM(G%s:G%s)}' % (before_rowx, rowx), format_group_float)
                            check_salesman = False
                            before_rowx = rowx + 4

                        salesman_id = False
                        sub_seq_first = 1
                        sub_seq = 1
                        # Тайланг авлага, өглөгийн дансаар эсвэл данс сонгож гаргасан бол Авлага, Өглөгөөр бүлэглэн гаргах хэсэг
                        if account_type == line['atype'] and self.group_by == 'partner' and self.account_type in ('receivable_payable', 'choose_accounts'):
                            sheet.merge_range(rowx, 0, rowx, column + 2, type_name, format_content_bold_text)
                            sheet.write(rowx, column + 3, type_start, format_content_bold_float)
                            sheet.write(rowx, column + 4, type_debit, format_content_bold_float)
                            sheet.write(rowx, column + 5, type_credit, format_content_bold_float)
                            sheet.write(rowx, column + 6, type_end, format_content_bold_float)
                            rowx += 1
                            # Авлага, Өглөгөөр бүлэглэлт нь валюттай бол валютын дүнг дараагийн мөр болгон зурна
                            if (type_currency_start or type_currency_debit or type_currency_credit or type_currency_end) != 0:
                                sheet.merge_range(rowx, 0, rowx, column + 2, '', format_content_text)
                                sheet.write(rowx, column + 3, type_currency_start, format_content_float_color)
                                sheet.write(rowx, column + 4, type_currency_debit, format_content_float_color)
                                sheet.write(rowx, column + 5, type_currency_credit, format_content_float_color)
                                sheet.write(rowx, column + 6, type_currency_end, format_content_float_color)
                                rowx += 1
                            type_start = type_currency_start = type_debit = type_currency_debit = 0
                            type_credit = type_currency_credit = type_end = type_currency_end = 0
                        # Зөвхөн өглөгийн данснууд сонгосон бол тэмдэгийг нэмэхээр гаргана
                        if self.account_type == 'choose_accounts' and check == 1 and atype == 'payable':
                            sub_start = abs(sub_start)
                            sub_currency_start = abs(sub_currency_start)
                            sub_end = abs(sub_end)
                            sub_currency_end = abs(sub_currency_end)
                        # Үндсэн харилцагч/дансны бүлэглэлтйин дүнг зурна
                        sheet.merge_range(rowx, 0, rowx, column + 2, name, format_group)
                        sheet.write(rowx, column + 3, sub_start, format_group_float)
                        sheet.write(rowx, column + 4, sub_debit, format_group_float)
                        sheet.write(rowx, column + 5, sub_credit, format_group_float)
                        sheet.write(rowx, column + 6, sub_end, format_group_float)
                        rowx += 1
                        # Бүлэглэлтийн валютын дүнг дараагийн мөр болгон зурна
                        if (sub_currency_start or sub_currency_debit or sub_currency_credit or sub_currency_end) != 0:
                            sheet.merge_range(rowx, 0, rowx, column + 2, '', format_group)
                            sheet.write(rowx, column + 3, sub_currency_start, format_content_float_color)
                            sheet.write(rowx, column + 4, sub_currency_debit, format_content_float_color)
                            sheet.write(rowx, column + 5, sub_currency_credit, format_content_float_color)
                            sheet.write(rowx, column + 6, sub_currency_end, format_content_float_color)
                            rowx += 1

                        sub_start = sub_currency_start = sub_debit = sub_currency_debit = 0
                        sub_credit = sub_currency_credit = sub_end = sub_currency_end = 0
                        sub_seq = 1
                        seq += 1
                        sheet.write(rowx, 0, seq, format_group)
                        # sheet.write(rowx, 1, group_line_name, format_group_left)
                        sheet.merge_range(rowx, 1, rowx, column + 6, group_line_name, format_group_left)
                        # sheet.merge_range(rowx, 2, rowx, column + 6, '', format_group)
                        rowx += 1
                    if self.group_by == 'salesman':
                        if not salesman_id:
                            sub_seq = 1
                            salesman_name = self.env['res.users'].search([('id', '=', salesman)]).name
                            sheet.write(rowx, 0, u'%s.%s' % (seq, sub_seq_first), format_group)
                            sheet.merge_range(rowx, 1, rowx, column + 2, _('Salesman %s') % salesman_name, format_group)
                            last_rowx = rowx
                            # end
                            rowx += 1
                        elif salesman_id != salesman:
                            if not check_salesman:
                                before_rowx = rowx + 1
                            else:
                                if before_rowx != 0:
                                    sheet.write_formula('E%s' % (before_rowx - 1), '{=SUM(E%s:E%s)}' % (before_rowx, rowx), format_group_float)
                                    sheet.write_formula('F%s' % (before_rowx - 1), '{=SUM(F%s:F%s)}' % (before_rowx, rowx), format_group_float)
                                    sheet.write_formula('G%s' % (before_rowx - 1), '{=SUM(G%s:G%s)}' % (before_rowx, rowx), format_group_float)
                                    sheet.write_formula('H%s' % (before_rowx - 1), '{=SUM(H%s:H%s)}' % (before_rowx, rowx), format_group_float)
                                check_salesman = False
                                before_rowx = rowx + 2

                            sub_seq_first += 1
                            sub_seq = 1
                            # Тайланг авлага, өглөгийн дансаар эсвэл данс сонгож гаргасан бол Авлага, Өглөгөөр бүлэглэн гаргах хэсэг
                            salesman_name = self.env['res.users'].search([('id', '=', salesman)]).name
                            sheet.write(rowx, 0, u'%s.%s' % (seq, sub_seq_first), format_group)
                            sheet.merge_range(rowx, 1, rowx, column + 2, _('Salesman %s') % salesman_name, format_group)
                            last_rowx = rowx
                            if is_first:
                                sheet.write_formula('E%s' % (first_rowx + 2), '{=SUM(E%s:E%s)}' % (first_rowx + 3, rowx), format_group_float)
                                sheet.write_formula('F%s' % (first_rowx + 2), '{=SUM(F%s:F%s)}' % (first_rowx + 3, rowx), format_group_float)
                                sheet.write_formula('G%s' % (first_rowx + 2), '{=SUM(G%s:G%s)}' % (first_rowx + 3, rowx), format_group_float)
                                sheet.write_formula('H%s' % (first_rowx + 2), '{=SUM(H%s:H%s)}' % (first_rowx + 3, rowx), format_group_float)
                                is_first = False
                            # end --
                            rowx += 1
                    if self.account_type == 'choose_accounts' and atype and atype != line['atype']:
                        check = 0
                    # Мөрийн эхний болон эцсийн үлдэгдлийг олох кодчилол
                    balance = line['start_balance']
                    end = (line['start_balance'] + line['debit'] - line['credit'])
                    balance_currency = line['cur_start_balance']
                    currency_end = (line['cur_start_balance'] + line['cur_debit'] - line['cur_credit'])
                    # Мөрийг зурна
                    if self.group_by == 'salesman':
                        sheet.write(rowx, 0, '%s.%s.%s' % (seq, sub_seq_first, sub_seq), format_content_center)
                        check_salesman = True
                    else:
                        sheet.write(rowx, 0, '%s.%s' % (seq, sub_seq), format_content_center)
                    sheet.write(rowx, 1, line_name, format_content_text)
                    if column:
                        sheet.write(rowx, 2, analytic, format_content_text)
                    sheet.write(rowx, column + 2, (line['cname'] or self.company_id.currency_id.name or ''), format_content_center)
                    sheet.write(rowx, column + 3, balance, format_content_float)
                    sheet.write(rowx, column + 4, line['debit'], format_content_float)
                    sheet.write(rowx, column + 5, line['credit'], format_content_float)
                    sheet.write(rowx, column + 6, end, format_content_float)
                    sub_seq += 1
                    rowx += 1
                    # Мөр нь валюттай бол валютын дүнг дараагийн мөр болгон зурна
                    if line['cname']:
                        sheet.merge_range(rowx, 0, rowx, 1, '', format_content_center)
                        sheet.write(rowx, column + 2, line['cname'] or '', format_content_center)
                        sheet.write(rowx, column + 3, balance_currency, format_content_float_color)
                        sheet.write(rowx, column + 4, line['cur_debit'], format_content_float_color)
                        sheet.write(rowx, column + 5, line['cur_credit'], format_content_float_color)
                        sheet.write(rowx, column + 6, currency_end, format_content_float_color)
                        rowx += 1
                    # Авлага, Өглөгийн бүлэглэлтийн дүнг олно
                    type_start += balance
                    type_currency_start += balance_currency
                    type_debit += line['debit']
                    type_currency_debit += line['cur_debit']
                    type_credit += line['credit']
                    type_currency_credit += line['cur_credit']
                    type_end += end
                    type_currency_end += currency_end
                    # Үндсэн харилцагч/дансны бүлэглэлтийн дүнг олно
                    sub_start += line['start_balance']
                    sub_currency_start += line['cur_start_balance']
                    sub_debit += line['debit']
                    sub_currency_debit += line['cur_debit']
                    sub_credit += line['credit']
                    sub_currency_credit += line['cur_credit']
                    sub_end += (line['start_balance'] + line['debit'] - line['credit'])
                    sub_currency_end += (line['cur_start_balance'] + line['cur_debit'] - line['cur_credit'])
                    # Нийт дүнг олно
                    total_start += line['start_balance']
                    total_currency_start += line['cur_start_balance']
                    total_debit += line['debit']
                    total_currency_debit += line['cur_debit']
                    total_credit += line['credit']
                    total_currency_credit += line['cur_credit']
                    total_end += line['start_balance'] + line['debit'] - line['credit']
                    total_currency_end += line['cur_start_balance'] + line['cur_debit'] - line['cur_credit']
                    # Бүлэглэлтээс хамааран тухайн мөрийн id болон нэрийг хувьсагчдад оноож буй хэсэг
                    if self.group_by == 'partner':
                        id = line['pid']
                        name = u'%s %s' % (line['pname'], _('TOTAL'))
                        account_type = line['atype']
                        if account_type == 'receivable':
                            type_name = u'%s %s' % (_('TOTAL'), _('RECEIVABLE'))
                        elif account_type == 'payable':
                            type_name = u'%s %s' % (_('TOTAL'), _('PAYABLE'))
                    elif self.group_by == 'account':
                        id = line['account_id']
                        name = u'[%s] %s %s' % (line['acode'], line['aname'], _('TOTAL'))
                    else:
                        id = line['account_id']
                        name = u'[%s] %s %s' % (line['acode'], line['aname'], _('TOTAL'))
                        salesman_id = line['salesman']
                    # Данснууд сонгоход 1 төрлийн данс сонгосон эсэхийг шалгахын тулд өмнөх дансны төрлийг хадгална
                    if self.account_type == 'choose_accounts':
                        atype = line['atype']
            # Тайланг авлага, өглөгийн дансаар эсвэл данс сонгож гаргасан бол Авлага, Өглөгөөр бүлэглэн гаргах хэсэг
            if self.group_by in ('partner') and self.account_type in ('receivable_payable', 'choose_accounts'):
                sheet.merge_range(rowx, 0, rowx, column + 2, type_name, format_content_bold_text)
                sheet.write(rowx, column + 3, type_start, format_content_bold_float)
                sheet.write(rowx, column + 4, type_debit, format_content_bold_float)
                sheet.write(rowx, column + 5, type_credit, format_content_bold_float)
                sheet.write(rowx, column + 6, type_end, format_content_bold_float)
                rowx += 1
                # Авлага, Өглөгөөр бүлэглэлт нь валюттай бол валютын дүнг дараагийн мөр болгон зурна
                if (type_currency_start or type_currency_debit or type_currency_credit or type_currency_end) != 0:
                    sheet.merge_range(rowx, 0, rowx, column + 2, '', format_content_text)
                    sheet.write(rowx, column + 3, type_currency_start, format_content_float_color)
                    sheet.write(rowx, column + 4, type_currency_debit, format_content_float_color)
                    sheet.write(rowx, column + 5, type_currency_credit, format_content_float_color)
                    sheet.write(rowx, column + 6, type_currency_end, format_content_float_color)
                    rowx += 1
            # Зөвхөн өглөгийн данснууд сонгосон бол тэмдэгийг нэмэхээр гаргана
            if self.account_type == 'choose_accounts' and check == 1 and atype == 'payable':
                sub_start = abs(sub_start)
                sub_currency_start = abs(sub_currency_start)
                sub_end = abs(sub_end)
                sub_currency_end = abs(sub_currency_end)
            # Үндсэн харилцагч/дансны бүлэглэлтйин дүнг зурна
            sheet.merge_range(rowx, 0, rowx, column + 2, name, format_group)
            sheet.write(rowx, column + 3, sub_start, format_group_float)
            sheet.write(rowx, column + 4, sub_debit, format_group_float)
            sheet.write(rowx, column + 5, sub_credit, format_group_float)
            sheet.write(rowx, column + 6, sub_end, format_group_float)
            # end  - -- --
            rowx += 1
            # Бүлэглэлтийн валютын дүнг дараагийн мөр болгон зурна
            if (sub_currency_start or sub_currency_debit or sub_currency_credit or sub_currency_end) != 0:
                sheet.merge_range(rowx, 0, rowx, column + 2, '', format_content_text)
                sheet.write(rowx, column + 3, sub_currency_start, format_content_float_color)
                sheet.write(rowx, column + 4, sub_currency_debit, format_content_float_color)
                sheet.write(rowx, column + 5, sub_currency_credit, format_content_float_color)
                sheet.write(rowx, column + 6, sub_currency_end, format_content_float_color)
                rowx += 1
            # Зөвхөн өглөгийн данснууд сонгосон бол тэмдэгийг нэмэхээр гаргана
            if self.account_type == 'payable' or (self.account_type == 'choose_accounts' and check == 1 and atype == 'payable'):
                total_start = abs(total_start)
                total_currency_start = abs(total_currency_start)
                total_end = abs(total_end)
                total_currency_end = abs(total_currency_end)
            # Нийт дүнг зурна
            sheet.merge_range(rowx, 0, rowx, column + 2, _('TOTAL'), format_group)
            sheet.write(rowx, column + 3, total_start, format_group_float)
            sheet.write(rowx, column + 4, total_debit, format_group_float)
            sheet.write(rowx, column + 5, total_credit, format_group_float)
            sheet.write(rowx, column + 6, total_end, format_group_float)
            if self.group_by == 'salesman':
                if rowx != 0:
                    sheet.write_formula('E%s' % (last_rowx + 1), '{=SUM(E%s:E%s)}' % (last_rowx + 2, rowx - 1), format_group_float)
                    sheet.write_formula('F%s' % (last_rowx + 1), '{=SUM(F%s:F%s)}' % (last_rowx + 2, rowx - 1), format_group_float)
                    sheet.write_formula('G%s' % (last_rowx + 1), '{=SUM(G%s:G%s)}' % (last_rowx + 2, rowx - 1), format_group_float)
                    sheet.write_formula('H%s' % (last_rowx + 1), '{=SUM(H%s:H%s)}' % (last_rowx + 2, rowx - 1), format_group_float)
            rowx += 1
            # Валютын нийт дүнг зурна
            if (total_currency_start or total_currency_debit or total_currency_credit or total_currency_end) != 0:
                sheet.merge_range(rowx, 0, rowx, column + 2, '', format_content_text)
                sheet.write(rowx, column + 3, total_currency_start, format_content_float_color)
                sheet.write(rowx, column + 4, total_currency_debit, format_content_float_color)
                sheet.write(rowx, column + 5, total_currency_credit, format_content_float_color)
                sheet.write(rowx, column + 6, total_currency_end, format_content_float_color)
                rowx += 1

        rowx += 2
        # Footer зурна
        sheet.merge_range(rowx, 1, rowx, 3, '%s: ........................................... (                          )' % _('Executive Accountant'), format_filter)
        sheet.hide_gridlines(2)
        rowx += 1
        book.close()

        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()
