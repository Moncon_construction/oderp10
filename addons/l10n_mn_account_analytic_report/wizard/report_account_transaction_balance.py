# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from io import BytesIO
import base64
import time
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from operator import itemgetter
from odoo.exceptions import ValidationError, UserError
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class ReportAccountTransactionBalance(models.TransientModel):
    """
        Монголын Сангийн Яамнаас баталсан Гүйлгээ Баланс тайланг шинжилгээний дансаар гардаг болгов.
    """
    _inherit = 'report.account.transaction.balance'
    _description = "Transaction Balance"

    show_analytic_account = fields.Boolean('Show analytic account')
    grouping = fields.Selection([('analytic_account', 'Analytic account'),
                                 ('account_type', 'Account type'),
                                 ('account', 'Account')], 'Grouping')
    analytic_account_ids = fields.Many2many('account.analytic.account', string="Analytic accounts")

    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Transaction Balance')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_filter_with_border = ReportExcelCellStyles.format_filter.copy()
        format_filter_with_border['border'] = 1
        format_filter_with_border = book.add_format(format_filter_with_border)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_group_right = book.add_format(ReportExcelCellStyles.format_group_right)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)

        move_line_obj = self.env['account.move.line']
        account_obj = self.env['account.account']
        analytic_account_obj = self.env['account.analytic.account']
        seq = -1
        num = 1
        inner_seq = 0

        total_col4 = 0
        total_col5 = 0
        total_col6 = 0
        total_col7 = 0
        total_col8 = 0
        total_col9 = 0
        total_col10 = 0
        total_col11 = 0
        total_col12 = 0
        total_col13 = 0
        total_col14 = 0
        total_col15 = 0
        total_col16 = 0
        total_col17 = 0

        total_sum_col4 = 0
        total_sum_col5 = 0
        total_sum_col6 = 0
        total_sum_col7 = 0
        total_sum_col8 = 0
        total_sum_col9 = 0
        total_sum_col10 = 0
        total_sum_col11 = 0
        total_sum_col12 = 0
        total_sum_col13 = 0
        total_sum_col14 = 0
        total_sum_col15 = 0
        total_sum_col16 = 0
        total_sum_col17 = 0
        code_dict = {}
        analytic_code_dict = {}
        acc_code_dict = {}
        data_dict = {}

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('transaction_balance'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.hide_gridlines(2)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 1
        # compute column
        colx_number = 8
        sheet.set_column('A:A', 4)
        sheet.set_column('B:B', 15)
        sheet.set_column('C:C', 30)
        sheet.set_column('D:D', 10)
        sheet.set_column('E:E', 10)
        sheet.set_column('F:F', 10)
        sheet.set_column('G:G', 10)
        sheet.set_column('H:H', 10)
        sheet.set_column('I:I', 10)
        sheet.set_column('J:J', 10)
        sheet.set_column('K:K', 10)
        sheet.set_column('L:L', 10)
        if self.currency_transaction:
            colx_number = 14
            sheet.set_column('K:K', 10)
            sheet.set_column('L:L', 10)
            sheet.set_column('M:M', 10)
            sheet.set_column('N:N', 10)
            sheet.set_column('O:O', 10)
            sheet.set_column('P:P', 10)
            sheet.set_column('Q:Q', 10)
            sheet.set_column('R:R', 10)

        # create name
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s' % (_('Company'), self.company_id.name), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 0, rowx + 1, colx_number, report_name.upper(), format_name)
        rowx += 2

        # create duration
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s - %s' % (_('Duration'), self.date_from, self.date_to), format_filter)
        rowx += 1

        # create date
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s' % (_('Created On'), time.strftime('%Y-%m-%d')), format_filter)
        rowx += 2

        if not self.currency_transaction:
            if self.show_analytic_account and self.grouping != 'analytic_account':
                sheet.merge_range(rowx, 0, rowx + 1, 0, _('Seq'), format_title)
                sheet.merge_range(rowx, 1, rowx + 1, 1, _('Account code'), format_title)
                sheet.merge_range(rowx, 2, rowx + 1, 2, _('Account name'), format_title)
                sheet.merge_range(rowx, 3, rowx + 1, 3, _('Analytic account code'), format_title)
                sheet.merge_range(rowx, 4, rowx + 1, 4, _('Analytic account name'), format_title)
                sheet.merge_range(rowx, 5, rowx, 6, _('Initial Balance'), format_title)
                sheet.merge_range(rowx, 7, rowx, 8, _('Transaction'), format_title)
                sheet.merge_range(rowx, 9, rowx, 10, _('End Balance'), format_title)
                sheet.write(rowx + 1, 5, _('Debit'), format_title)
                sheet.write(rowx + 1, 6, _('Credit'), format_title)
                sheet.write(rowx + 1, 7, _('Debit'), format_title)
                sheet.write(rowx + 1, 8, _('Credit'), format_title)
                sheet.write(rowx + 1, 9, _('Debit'), format_title)
                sheet.write(rowx + 1, 10, _('Credit'), format_title)
                rowx += 2
            else:
                sheet.merge_range(rowx, 0, rowx + 1, 0, _('Seq'), format_title)
                sheet.merge_range(rowx, 1, rowx + 1, 1, _('Account code'), format_title)
                sheet.merge_range(rowx, 2, rowx + 1, 2, _('Account name'), format_title)
                sheet.merge_range(rowx, 3, rowx, 4, _('Initial Balance'), format_title)
                sheet.merge_range(rowx, 5, rowx, 6, _('Transaction'), format_title)
                sheet.merge_range(rowx, 7, rowx, 8, _('End Balance'), format_title)
                sheet.write(rowx + 1, 3, _('Debit'), format_title)
                sheet.write(rowx + 1, 4, _('Credit'), format_title)
                sheet.write(rowx + 1, 5, _('Debit'), format_title)
                sheet.write(rowx + 1, 6, _('Credit'), format_title)
                sheet.write(rowx + 1, 7, _('Debit'), format_title)
                sheet.write(rowx + 1, 8, _('Credit'), format_title)
                rowx += 2
        else:
            if self.show_analytic_account and self.grouping != 'analytic_account':
                sheet.merge_range(rowx, 0, rowx + 2, 0, _('Seq'), format_title)
                sheet.merge_range(rowx, 1, rowx + 2, 1, _('Account code'), format_title)
                sheet.merge_range(rowx, 2, rowx + 2, 2, _('Account name'), format_title)
                sheet.merge_range(rowx, 3, rowx + 2, 3, _('Analytic account code'), format_title)
                sheet.merge_range(rowx, 4, rowx + 2, 4, _('Analytic account name'), format_title)
                sheet.merge_range(rowx, 5, rowx + 2, 5, _('Account currency'), format_title)
                sheet.merge_range(rowx, 6, rowx, 9, _('Initial Balance'), format_title)
                sheet.merge_range(rowx + 1, 6, rowx + 1, 7, _('Debit'), format_title)
                sheet.merge_range(rowx + 1, 8, rowx + 1, 9, _('Credit'), format_title)
                sheet.merge_range(rowx, 10, rowx, 13, _('Transaction'), format_title)
                sheet.merge_range(rowx + 1, 10, rowx + 1, 11, _('Debit'), format_title)
                sheet.merge_range(rowx + 1, 12, rowx + 1, 13, _('Credit'), format_title)
                sheet.merge_range(rowx, 14, rowx, 17, _('End Balance'), format_title)
                sheet.merge_range(rowx + 1, 14, rowx + 1, 15, _('Debit'), format_title)
                sheet.merge_range(rowx + 1, 16, rowx + 1, 17, _('Credit'), format_title)
                sheet.write(rowx + 2, 6, _('By Currency'), format_title)
                sheet.write(rowx + 2, 7, _('By MNT'), format_title)
                sheet.write(rowx + 2, 8, _('By Currency'), format_title)
                sheet.write(rowx + 2, 9, _('By MNT'), format_title)
                sheet.write(rowx + 2, 10, _('By Currency'), format_title)
                sheet.write(rowx + 2, 11, _('By MNT'), format_title)
                sheet.write(rowx + 2, 12, _('By Currency'), format_title)
                sheet.write(rowx + 2, 13, _('By MNT'), format_title)
                sheet.write(rowx + 2, 14, _('By Currency'), format_title)
                sheet.write(rowx + 2, 15, _('By MNT'), format_title)
                sheet.write(rowx + 2, 16, _('By Currency'), format_title)
                sheet.write(rowx + 2, 17, _('By MNT'), format_title)
                rowx += 3
            else:
                sheet.merge_range(rowx, 0, rowx + 2, 0, _('Seq'), format_title)
                sheet.merge_range(rowx, 1, rowx + 2, 1, _('Account code'), format_title)
                sheet.merge_range(rowx, 2, rowx + 2, 2, _('Account name'), format_title)
                sheet.merge_range(rowx, 3, rowx + 2, 3, _('Account currency'), format_title)
                sheet.merge_range(rowx, 4, rowx, 7, _('Initial Balance'), format_title)
                sheet.merge_range(rowx + 1, 4, rowx + 1, 5, _('Debit'), format_title)
                sheet.merge_range(rowx + 1, 6, rowx + 1, 7, _('Credit'), format_title)
                sheet.merge_range(rowx, 8, rowx, 11, _('Transaction'), format_title)
                sheet.merge_range(rowx + 1, 8, rowx + 1, 9, _('Debit'), format_title)
                sheet.merge_range(rowx + 1, 10, rowx + 1, 11, _('Credit'), format_title)
                sheet.merge_range(rowx, 12, rowx, 15, _('End Balance'), format_title)
                sheet.merge_range(rowx + 1, 12, rowx + 1, 13, _('Debit'), format_title)
                sheet.merge_range(rowx + 1, 14, rowx + 1, 15, _('Credit'), format_title)
                sheet.write(rowx + 2, 4, _('By Currency'), format_title)
                sheet.write(rowx + 2, 5, _('By MNT'), format_title)
                sheet.write(rowx + 2, 6, _('By Currency'), format_title)
                sheet.write(rowx + 2, 7, _('By MNT'), format_title)
                sheet.write(rowx + 2, 8, _('By Currency'), format_title)
                sheet.write(rowx + 2, 9, _('By MNT'), format_title)
                sheet.write(rowx + 2, 10, _('By Currency'), format_title)
                sheet.write(rowx + 2, 11, _('By MNT'), format_title)
                sheet.write(rowx + 2, 12, _('By Currency'), format_title)
                sheet.write(rowx + 2, 13, _('By MNT'), format_title)
                sheet.write(rowx + 2, 14, _('By Currency'), format_title)
                sheet.write(rowx + 2, 15, _('By MNT'), format_title)
                rowx += 3
        account_ids = []
        if self.account_ids:
            for account_id in self.account_ids.ids:
                account_ids.append(account_id)
        elif self.account_type_filter:
            for account_type in self.account_type_filter:
                account = self.env['account.account'].search([('user_type_id', '=', account_type.id)])
                if account:
                    for account_id in account.ids:
                        account_ids.append(account_id)
            if not account_ids:
                raise UserError(_('There are no accounts in selected account types'))
        else:
            account_ids = account_obj.search([]).ids
        account_ids.sort()
        analytic_account_ids = []
        if self.analytic_account_ids:
            for analytic_account_id in self.analytic_account_ids:
                analytic_account_ids.append(analytic_account_id.id)
        else:
            analytic_account_ids = self.env['account.analytic.account'].search([]).ids
        initials = move_line_obj.get_initial_balance_with_analytic(self.company_id.id, account_ids, analytic_account_ids, self.date_from, self.target_move) if self.show_analytic_account else move_line_obj.get_initial_balance(self.company_id.id, account_ids, self.date_from, self.target_move)
        balances = move_line_obj.get_balance_with_analytic(self.company_id.id, account_ids, analytic_account_ids, self.date_from, self.date_to, self.target_move, without_profit_revenue=self.without_profit_revenue) if self.show_analytic_account else move_line_obj.get_balance(self.company_id.id, account_ids, self.date_from, self.date_to, self.target_move, without_profit_revenue=self.without_profit_revenue)
        fetched = initials + balances

        old_code = 0
        old_acc_code = 0
        old_analytic_code = 0
        for f in sorted(fetched, key=itemgetter('code')):
            acc = self.env[('account.account')].search([('id', '=', f['account_id'])])
            acc_type = self.env[('account.account.type')].search([('id', '=', acc.user_type_id.id)], limit=1)
            acc_journal = self.env['account.journal'].search([('default_debit_account_id', '=', acc.id)], limit=1)

            if acc_type.code is not old_code:
                old_code = acc_type.code
                if old_code not in code_dict:
                    code_dict[f['account_id']] = {'acc_code': acc_type.code, 'acc_type_name': acc_type.name}

            if acc.code is not old_acc_code:
                old_acc_code = acc.code
                if old_acc_code not in acc_code_dict:
                    acc_code_dict[f['account_id']] = {'acc_code': acc.code, 'acc_name': acc.name}

            if 'analytic_account_code' in f and f['analytic_account_code'] is not old_analytic_code:
                old_analytic_code = f['analytic_account_code']
                if old_analytic_code not in analytic_code_dict:
                    analytic_code_dict[f['analytic_account_id']] = {'analytic_account_code': f['analytic_account_code'], 'analytic_account_name': f['analytic_account_name']}

            if str(f['account_id']) + ':' + ('analytic_account_id' in f and str(f['analytic_account_id']) or '') not in data_dict:
                data_dict[str(f['account_id']) + ':' + ('analytic_account_id' in f and str(f['analytic_account_id']) or '')] = {
                    'acc_type': acc_type.id,
                    'acc_type_code': acc_type.code,
                    'acc_type_name': acc_type.name,
                    'analytic_account_code': 'analytic_account_code' in f and f['analytic_account_code'] or '',
                    'analytic_account_name': 'analytic_account_name' in f and f['analytic_account_name'] or '',
                    'currency_id': acc_journal.currency_id.id,
                    'code': f['code'],
                    'name': f['name'],
                    'start_debit': 0,
                    'start_credit': 0,
                    'cur_start_debit': 0,
                    'cur_start_credit': 0,
                    'debit': 0,
                    'credit': 0,
                    'cur_debit': 0,
                    'cur_credit': 0,
                    'end_debit': 0,
                    'end_credit': 0,
                    'cur_end_debit': 0,
                    'cur_end_credit': 0}
                data_dict[str(f['account_id']) + ':' + ('analytic_account_id' in f and str(f['analytic_account_id']) or '')]['currency'] = f['currency']
            data_dict[str(f['account_id']) + ':' + ('analytic_account_id' in f and str(f['analytic_account_id']) or '')]['start_debit'] += f['start_debit']
            data_dict[str(f['account_id']) + ':' + ('analytic_account_id' in f and str(f['analytic_account_id']) or '')]['start_credit'] += f['start_credit']
            data_dict[str(f['account_id']) + ':' + ('analytic_account_id' in f and str(f['analytic_account_id']) or '')]['cur_start_debit'] += f['cur_start_debit']
            data_dict[str(f['account_id']) + ':' + ('analytic_account_id' in f and str(f['analytic_account_id']) or '')]['cur_start_credit'] += f['cur_start_credit']
            data_dict[str(f['account_id']) + ':' + ('analytic_account_id' in f and str(f['analytic_account_id']) or '')]['debit'] += f['debit']
            data_dict[str(f['account_id']) + ':' + ('analytic_account_id' in f and str(f['analytic_account_id']) or '')]['credit'] += f['credit']
            data_dict[str(f['account_id']) + ':' + ('analytic_account_id' in f and str(f['analytic_account_id']) or '')]['cur_debit'] += f['cur_debit']
            data_dict[str(f['account_id']) + ':' + ('analytic_account_id' in f and str(f['analytic_account_id']) or '')]['cur_credit'] += f['cur_credit']
            data_dict[str(f['account_id']) + ':' + ('analytic_account_id' in f and str(f['analytic_account_id']) or '')]['end_debit'] += f['start_debit'] + f['debit']
            data_dict[str(f['account_id']) + ':' + ('analytic_account_id' in f and str(f['analytic_account_id']) or '')]['end_credit'] += f['start_credit'] + f['credit']
            data_dict[str(f['account_id']) + ':' + ('analytic_account_id' in f and str(f['analytic_account_id']) or '')]['cur_end_debit'] += f['cur_start_debit'] + f['cur_debit']
            data_dict[str(f['account_id']) + ':' + ('analytic_account_id' in f and str(f['analytic_account_id']) or '')]['cur_end_credit'] += f['cur_start_credit'] + f['cur_credit']

        if self.show_analytic_account:
            if self.grouping == 'analytic_account':
                old_analytic_code = 0
                for analytic_code in sorted(analytic_code_dict.values(), key=itemgetter('analytic_account_code')):
                    if analytic_code['analytic_account_code'] is not old_analytic_code:
                        old_analytic_code = analytic_code['analytic_account_code']
                        sheet.merge_range(rowx, 0, rowx, 8, '[' + analytic_code['analytic_account_code'] + '] ' + analytic_code['analytic_account_name'] if analytic_code['analytic_account_code'] else analytic_code['analytic_account_name'], format_filter_with_border)
                        rowx += 1
                        seq += 1
                        inner_seq += 1
                        for val in sorted(data_dict.values(), key=itemgetter('analytic_account_code', 'code')):
                            if val['analytic_account_name'] == analytic_code['analytic_account_name']:
                                sheet.write(rowx, 0, num, format_content_number)
                                sheet.write(rowx, 1, val['code'], format_content_text)
                                sheet.write(rowx, 2, val['name'], format_content_text)
                                start = val['start_debit'] - val['start_credit']
                                balance = start + val['debit'] - val['credit']
                                if not self.currency_transaction:
                                    sheet.write(rowx, 3, (start > 0 and start) or 0.0, format_content_float)
                                    sheet.write(rowx, 4, (start < 0 and -start) or 0.0, format_content_float)
                                    sheet.write(rowx, 5, val['debit'], format_content_float)
                                    sheet.write(rowx, 6, val['credit'], format_content_float)
                                    sheet.write(rowx, 7, (balance > 0 and balance) or 0.0, format_content_float)
                                    sheet.write(rowx, 8, (balance < 0 and -balance) or 0.0, format_content_float)

                                    total_col4 += (start > 0 and start) or 0.0
                                    total_col5 += (start < 0 and -start) or 0.0
                                    total_col6 += val['debit']
                                    total_col7 += val['credit']
                                    total_col8 += (balance > 0 and balance) or 0.0
                                    total_col9 += (balance < 0 and -balance) or 0.0

                                    total_sum_col4 += total_col4
                                    total_sum_col5 += total_col5
                                    total_sum_col6 += total_col6
                                    total_sum_col7 += total_col7
                                    total_sum_col8 += total_col8
                                    total_sum_col9 += total_col9
                                else:
                                    cur_start = val['cur_start_debit'] - val['cur_start_credit']
                                    cur_balance = cur_start + val['cur_debit'] - val['cur_credit']
                                    sheet.write(rowx, 3, (val['currency'] or '') if (val['currency_id'] != self.company_id.currency_id.id) else '', format_content_text)
                                    sheet.write(rowx, 4, (cur_start > 0 and cur_start) or '', format_content_float)
                                    sheet.write(rowx, 5, (start > 0 and start) or 0.0, format_content_float)
                                    sheet.write(rowx, 6, (cur_start < 0 and -cur_start) or '', format_content_float)
                                    sheet.write(rowx, 7, (start < 0 and -start) or 0.0, format_content_float)
                                    sheet.write(rowx, 8, val['cur_debit'] or '', format_content_float)
                                    sheet.write(rowx, 9, val['debit'], format_content_float)
                                    sheet.write(rowx, 10, val['cur_credit'] or '', format_content_float)
                                    sheet.write(rowx, 11, val['credit'], format_content_float)
                                    sheet.write(rowx, 12, (cur_balance > 0 and cur_balance) or '', format_content_float)
                                    sheet.write(rowx, 13, (balance > 0 and balance) or 0.0, format_content_float)
                                    sheet.write(rowx, 14, (cur_balance < 0 and -cur_balance) or '', format_content_float)
                                    sheet.write(rowx, 15, (balance < 0 and -balance) or 0.0, format_content_float)

                                    total_col4 += (cur_start > 0 and cur_start) or 0.0
                                    total_col5 += (start > 0 and start) or 0.0
                                    total_col6 += (cur_start < 0 and -cur_start) or 0.0
                                    total_col7 += (start < 0 and -start) or 0.0
                                    total_col8 += val['cur_debit']
                                    total_col9 += val['debit']
                                    total_col10 += val['cur_debit']
                                    total_col11 += val['credit']
                                    total_col12 += (cur_balance > 0 and cur_balance) or 0.0
                                    total_col13 += (balance > 0 and balance) or 0.0
                                    total_col14 += (cur_balance < 0 and -cur_balance) or 0.0
                                    total_col15 += (balance < 0 and -balance) or 0.0

                                    total_sum_col4 += total_col4
                                    total_sum_col5 += total_col5
                                    total_sum_col6 += total_col6
                                    total_sum_col7 += total_col7
                                    total_sum_col8 += total_col8
                                    total_sum_col9 += total_col9
                                    total_sum_col10 += total_col10
                                    total_sum_col11 += total_col11
                                    total_sum_col12 += total_col12
                                    total_sum_col13 += total_col13
                                    total_sum_col14 += total_col14
                                    total_sum_col15 += total_col15

                                num += 1
                                seq += 1
                                inner_seq += 1
                                rowx += 1

                                total_col4 = 0
                                total_col5 = 0
                                total_col6 = 0
                                total_col7 = 0
                                total_col8 = 0
                                total_col9 = 0
                                total_col10 = 0
                                total_col11 = 0
                                total_col12 = 0
                                total_col13 = 0
                                total_col14 = 0
                                total_col15 = 0
                                total_col16 = 0
                        coly = 2
                        if self.currency_transaction:
                            coly = 3
                        sheet.merge_range(rowx, 0, rowx, coly, _('Total'), format_group_right)
                        if not self.currency_transaction:
                            sheet.write_formula(rowx, coly + 1, '{=SUM(' + xl_rowcol_to_cell(rowx - inner_seq + 1, coly + 1) + ':' + xl_rowcol_to_cell(rowx - 1, coly + 1) + ')}', format_group_float)
                        else:
                            sheet.write(rowx, coly + 1, '', format_group_float)
                        sheet.write_formula(rowx, coly + 2, '{=SUM(' + xl_rowcol_to_cell(rowx - inner_seq + 1, coly + 2) + ':' + xl_rowcol_to_cell(rowx - 1, coly + 2) + ')}', format_group_float)
                        if not self.currency_transaction:
                            sheet.write_formula(rowx, coly + 3, '{=SUM(' + xl_rowcol_to_cell(rowx - inner_seq + 1, coly + 3) + ':' + xl_rowcol_to_cell(rowx - 1, coly + 3) + ')}', format_group_float)
                        else:
                            sheet.write(rowx, coly + 3, '', format_group_float)
                        sheet.write_formula(rowx, coly + 4, '{=SUM(' + xl_rowcol_to_cell(rowx - inner_seq + 1, coly + 4) + ':' + xl_rowcol_to_cell(rowx - 1, coly + 4) + ')}', format_group_float)
                        if not self.currency_transaction:
                            sheet.write_formula(rowx, coly + 5, '{=SUM(' + xl_rowcol_to_cell(rowx - inner_seq + 1, coly + 5) + ':' + xl_rowcol_to_cell(rowx - 1, coly + 5) + ')}', format_group_float)
                        else:
                            sheet.write(rowx, coly + 5, '', format_group_float)
                        sheet.write_formula(rowx, coly + 6, '{=SUM(' + xl_rowcol_to_cell(rowx - inner_seq + 1, coly + 6) + ':' + xl_rowcol_to_cell(rowx - 1, coly + 6) + ')}', format_group_float)
                        if self.currency_transaction:
                            sheet.write(rowx, coly + 7, '', format_group_float)
                            sheet.write_formula(rowx, coly + 8, '{=SUM(' + xl_rowcol_to_cell(rowx - inner_seq + 1, coly + 8) + ':' + xl_rowcol_to_cell(rowx - 1, coly + 8) + ')}', format_group_float)
                            sheet.write(rowx, coly + 9, '', format_group_float)
                            sheet.write_formula(rowx, coly + 10, '{=SUM(' + xl_rowcol_to_cell(rowx - inner_seq + 1, coly + 10) + ':' + xl_rowcol_to_cell(rowx - 1, coly + 10) + ')}', format_group_float)
                            sheet.write(rowx, coly + 11, '', format_group_float)
                            sheet.write_formula(rowx, coly + 12, '{=SUM(' + xl_rowcol_to_cell(rowx - inner_seq + 1, coly + 12) + ':' + xl_rowcol_to_cell(rowx - 1, coly + 12) + ')}', format_group_float)

                        rowx += 1
                        seq += 1
                        inner_seq = 0
            elif self.grouping == 'account_type':
                old_code = 0
                old_acc_code = 0
                for code_type in sorted(code_dict.values(), key=itemgetter('acc_code')):
                    if code_type['acc_code'] is not old_code:
                        old_code = code_type['acc_code']
                        sheet.merge_range(rowx, 0, rowx, 10 if not self.currency_transaction else 17, _(code_type['acc_type_name']), format_filter_with_border)
                        rowx += 1
                        inner_seq += 1
                        for val in sorted(data_dict.values(), key=itemgetter('acc_type_code', 'code', 'analytic_account_code')):
                            if val['acc_type_name'] is code_type['acc_type_name']:
                                seq += 1
                                # Шинэ дансны код орж ирвэл эсвэл анхны дансны код орж ирвэл
                                if old_acc_code != val['code']:
                                    if inner_seq == 1:
                                        rowx += 0
                                    else:
                                        coly = 5 if self.currency_transaction else 4
                                        rowx += 1
                                        sheet.merge_range(rowx - 1, 3, rowx - 1, coly, _('Total'), format_group_right)
                                        if not self.currency_transaction:
                                            sheet.write_formula(rowx - 1, coly + 1, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 1, coly + 1) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 1) + ')}', format_group_float)
                                            sheet.write_formula(rowx - 1, coly + 3, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 1, coly + 3) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 3) + ')}', format_group_float)
                                            sheet.write_formula(rowx - 1, coly + 5, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 1, coly + 5) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 5) + ')}', format_group_float)
                                        else:
                                            sheet.write(rowx - 1, coly + 1, '', format_group_float)
                                            sheet.write(rowx - 1, coly + 3, '', format_group_float)
                                            sheet.write(rowx - 1, coly + 5, '', format_group_float)
                                        sheet.write_formula(rowx - 1, coly + 2, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 1, coly + 2) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 2) + ')}', format_group_float)
                                        sheet.write_formula(rowx - 1, coly + 4, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 1, coly + 4) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 4) + ')}', format_group_float)
                                        sheet.write_formula(rowx - 1, coly + 6, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 1, coly + 6) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 6) + ')}', format_group_float)
                                        if self.currency_transaction:
                                            sheet.write(rowx - 1, coly + 7, '', format_group_float)
                                            sheet.write_formula(rowx - 1, coly + 8, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 1, coly + 8) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 8) + ')}', format_group_float)
                                            sheet.write(rowx - 1, coly + 9, '', format_group_float)
                                            sheet.write_formula(rowx - 1, coly + 10, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 1, coly + 10) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 10) + ')}', format_group_float)
                                            sheet.write(rowx - 1, coly + 11, '', format_group_float)
                                            sheet.write_formula(rowx - 1, coly + 12, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 1, coly + 12) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 12) + ')}', format_group_float)
                                    sheet.merge_range(rowx, 0, rowx + 1, 0, num, format_content_number)
                                    sheet.merge_range(rowx, 1, rowx + 1, 1, val['code'], format_content_text)
                                    sheet.merge_range(rowx, 2, rowx + 1, 2, val['name'], format_content_text)
                                    old_acc_code = val['code']
                                    seq = 0
                                    coly = 5 if self.currency_transaction else 4
                                else:
                                    num -= 1
                                    sheet.merge_range(rowx - seq, 0, rowx + 1, 0, num, format_content_number)
                                    sheet.merge_range(rowx - seq, 1, rowx + 1, 1, val['code'], format_content_text)
                                    sheet.merge_range(rowx - seq, 2, rowx + 1, 2, val['name'], format_content_text)
                                    coly = 5 if self.currency_transaction else 4
                                sheet.write(rowx, 3, val['analytic_account_code'], format_content_text)
                                sheet.write(rowx, 4, val['analytic_account_name'], format_content_text)
                                start = val['start_debit'] - val['start_credit']
                                balance = start + val['debit'] - val['credit']
                                if not self.currency_transaction:
                                    sheet.write(rowx, 5, (start > 0 and start) or 0.0, format_content_float)
                                    sheet.write(rowx, 6, (start < 0 and -start) or 0.0, format_content_float)
                                    sheet.write(rowx, 7, val['debit'], format_content_float)
                                    sheet.write(rowx, 8, val['credit'], format_content_float)
                                    sheet.write(rowx, 9, (balance > 0 and balance) or 0.0, format_content_float)
                                    sheet.write(rowx, 10, (balance < 0 and -balance) or 0.0, format_content_float)

                                    total_col5 += (start > 0 and start) or 0.0
                                    total_col6 += (start < 0 and -start) or 0.0
                                    total_col7 += val['debit']
                                    total_col8 += val['credit']
                                    total_col9 += (balance > 0 and balance) or 0.0
                                    total_col10 += (balance < 0 and -balance) or 0.0

                                    total_sum_col5 += total_col5
                                    total_sum_col6 += total_col6
                                    total_sum_col7 += total_col7
                                    total_sum_col8 += total_col8
                                    total_sum_col9 += total_col9
                                    total_sum_col10 += total_col10
                                else:
                                    cur_start = val['cur_start_debit'] - val['cur_start_credit']
                                    cur_balance = cur_start + val['cur_debit'] - val['cur_credit']
                                    sheet.write(rowx, 5, (val['currency'] or '') if (val['currency_id'] != self.company_id.currency_id.id) else '', format_content_text)
                                    sheet.write(rowx, 6, (cur_start > 0 and cur_start) or '', format_content_float)
                                    sheet.write(rowx, 7, (start > 0 and start) or 0.0, format_content_float)
                                    sheet.write(rowx, 8, (cur_start < 0 and -cur_start) or '', format_content_float)
                                    sheet.write(rowx, 9, (start < 0 and -start) or 0.0, format_content_float)
                                    sheet.write(rowx, 10, val['cur_debit'] or '', format_content_float)
                                    sheet.write(rowx, 11, val['debit'], format_content_float)
                                    sheet.write(rowx, 12, val['cur_credit'] or '', format_content_float)
                                    sheet.write(rowx, 13, val['credit'], format_content_float)
                                    sheet.write(rowx, 14, (cur_balance > 0 and cur_balance) or '', format_content_float)
                                    sheet.write(rowx, 15, (balance > 0 and balance) or 0.0, format_content_float)
                                    sheet.write(rowx, 16, (cur_balance < 0 and -cur_balance) or '', format_content_float)
                                    sheet.write(rowx, 17, (balance < 0 and -balance) or 0.0, format_content_float)

                                    total_col5 += (cur_start > 0 and cur_start) or 0.0
                                    total_col6 += (start > 0 and start) or 0.0
                                    total_col7 += (cur_start < 0 and -cur_start) or 0.0
                                    total_col8 += (start < 0 and -start) or 0.0
                                    total_col9 += val['cur_debit']
                                    total_col10 += val['debit']
                                    total_col11 += val['cur_debit']
                                    total_col12 += val['credit']
                                    total_col13 += (cur_balance > 0 and cur_balance) or 0.0
                                    total_col14 += (balance > 0 and balance) or 0.0
                                    total_col15 += (cur_balance < 0 and -cur_balance) or 0.0
                                    total_col16 += (balance < 0 and -balance) or 0.0

                                    total_sum_col5 += total_col5
                                    total_sum_col6 += total_col6
                                    total_sum_col7 += total_col7
                                    total_sum_col8 += total_col8
                                    total_sum_col9 += total_col9
                                    total_sum_col10 += total_col10
                                    total_sum_col11 += total_col11
                                    total_sum_col12 += total_col12
                                    total_sum_col13 += total_col13
                                    total_sum_col14 += total_col14
                                    total_sum_col15 += total_col15
                                    total_sum_col16 += total_col16
                                num += 1
                                inner_seq += 1
                                rowx += 1
                        rowx += 1
                        coly = 4
                        if self.currency_transaction:
                            coly = 5
                        sheet.merge_range(rowx - 1, 3, rowx - 1, coly, _('Total'), format_group_right)
                        if not self.currency_transaction:
                            sheet.write_formula(rowx - 1, coly + 1, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 2, coly + 1) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 1) + ')}' if not self.currency_transaction else '', format_group_float)
                            sheet.write_formula(rowx - 1, coly + 3, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 2, coly + 3) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 3) + ')}' if not self.currency_transaction else '', format_group_float)
                            sheet.write_formula(rowx - 1, coly + 5, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 2, coly + 5) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 5) + ')}' if not self.currency_transaction else '', format_group_float)
                        else:
                            sheet.write(rowx - 1, coly + 1, '', format_group_float)
                            sheet.write(rowx - 1, coly + 3, '', format_group_float)
                            sheet.write(rowx - 1, coly + 5, '', format_group_float)
                        sheet.write_formula(rowx - 1, coly + 2, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 2, coly + 2) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 2) + ')}', format_group_float)
                        sheet.write_formula(rowx - 1, coly + 4, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 2, coly + 4) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 4) + ')}', format_group_float)
                        sheet.write_formula(rowx - 1, coly + 6, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 2, coly + 6) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 6) + ')}', format_group_float)
                        sheet.merge_range(rowx, 0, rowx, coly, _('Total'), format_group_right)
                        sheet.write(rowx, coly + 1, total_col5 if not self.currency_transaction else '', format_group_float)
                        sheet.write(rowx, coly + 2, total_col6, format_group_float)
                        sheet.write(rowx, coly + 3, total_col7 if not self.currency_transaction else '', format_group_float)
                        sheet.write(rowx, coly + 4, total_col8, format_group_float)
                        sheet.write(rowx, coly + 5, total_col9 if not self.currency_transaction else '', format_group_float)
                        sheet.write(rowx, coly + 6, total_col10, format_group_float)
                        if self.currency_transaction:
                            sheet.write(rowx - 1, coly + 7, '', format_group_float)
                            sheet.write_formula(rowx - 1, coly + 8, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 2, coly + 8) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 8) + ')}', format_group_float)
                            sheet.write(rowx - 1, coly + 9, '', format_group_float)
                            sheet.write_formula(rowx - 1, coly + 10, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 2, coly + 10) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 10) + ')}', format_group_float)
                            sheet.write(rowx - 1, coly + 11, '', format_group_float)
                            sheet.write_formula(rowx - 1, coly + 12, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 2, coly + 12) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 12) + ')}', format_group_float)
                            sheet.write(rowx, coly + 7, '', format_group_float)
                            sheet.write(rowx, coly + 8, total_col12, format_group_float)
                            sheet.write(rowx, coly + 9, '', format_group_float)
                            sheet.write(rowx, coly + 10, total_col14, format_group_float)
                            sheet.write(rowx, coly + 11, '', format_group_float)
                            sheet.write(rowx, coly + 12, total_col16, format_group_float)
                        rowx += 1
                        seq += 1
                        inner_seq = 0
                        total_col4 = 0
                        total_col5 = 0
                        total_col6 = 0
                        total_col7 = 0
                        total_col8 = 0
                        total_col9 = 0
                        total_col10 = 0
                        total_col11 = 0
                        total_col12 = 0
                        total_col13 = 0
                        total_col14 = 0
                        total_col15 = 0
            if self.grouping == 'account':
                for val in sorted(data_dict.values(), key=itemgetter('code', 'analytic_account_code')):
                    seq += 1
                    inner_seq += 1
                    # Шинэ дансны код орж ирвэл эсвэл анхны дансны код орж ирвэл
                    if old_acc_code != val['code']:
                        if inner_seq == 1:
                            rowx += 0
                        else:
                            coly = 5 if self.currency_transaction else 4
                            rowx += 1
                            sheet.merge_range(rowx - 1, 3, rowx - 1, coly, _('Total'), format_group_right)
                            if not self.currency_transaction:
                                sheet.write_formula(rowx - 1, coly + 1, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 1, coly + 1) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 1) + ')}', format_group_float)
                                sheet.write_formula(rowx - 1, coly + 3, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 1, coly + 3) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 3) + ')}', format_group_float)
                                sheet.write_formula(rowx - 1, coly + 5, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 1, coly + 5) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 5) + ')}', format_group_float)
                            else:
                                sheet.write(rowx - 1, coly + 1, '', format_group_float)
                                sheet.write(rowx - 1, coly + 3, '', format_group_float)
                                sheet.write(rowx - 1, coly + 5, '', format_group_float)
                            sheet.write_formula(rowx - 1, coly + 2, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 1, coly + 2) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 2) + ')}', format_group_float)
                            sheet.write_formula(rowx - 1, coly + 4, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 1, coly + 4) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 4) + ')}', format_group_float)
                            sheet.write_formula(rowx - 1, coly + 6, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 1, coly + 6) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 6) + ')}', format_group_float)
                            if self.currency_transaction:
                                sheet.write(rowx - 1, coly + 7, '', format_group_float)
                                sheet.write_formula(rowx - 1, coly + 8, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 1, coly + 8) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 8) + ')}', format_group_float)
                                sheet.write(rowx - 1, coly + 9, '', format_group_float)
                                sheet.write_formula(rowx - 1, coly + 10, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 1, coly + 10) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 10) + ')}', format_group_float)
                                sheet.write(rowx - 1, coly + 11, '', format_group_float)
                                sheet.write_formula(rowx - 1, coly + 12, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 1, coly + 12) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 12) + ')}', format_group_float)
                        sheet.merge_range(rowx, 0, rowx + 1, 0, num, format_content_number)
                        sheet.merge_range(rowx, 1, rowx + 1, 1, val['code'], format_content_text)
                        sheet.merge_range(rowx, 2, rowx + 1, 2, val['name'], format_content_text)
                        old_acc_code = val['code']
                        seq = 0
                        coly = 5 if self.currency_transaction else 4
                    else:
                        num -= 1
                        sheet.merge_range(rowx - seq, 0, rowx + 1, 0, num, format_content_number)
                        sheet.merge_range(rowx - seq, 1, rowx + 1, 1, val['code'], format_content_text)
                        sheet.merge_range(rowx - seq, 2, rowx + 1, 2, val['name'], format_content_text)
                        coly = 5 if self.currency_transaction else 4
                    sheet.write(rowx, 3, val['analytic_account_code'], format_content_text)
                    sheet.write(rowx, 4, val['analytic_account_name'], format_content_text)
                    start = val['start_debit'] - val['start_credit']
                    balance = start + val['debit'] - val['credit']
                    if not self.currency_transaction:
                        sheet.write(rowx, 5, (start > 0 and start) or 0.0, format_content_float)
                        sheet.write(rowx, 6, (start < 0 and -start) or 0.0, format_content_float)
                        sheet.write(rowx, 7, val['debit'], format_content_float)
                        sheet.write(rowx, 8, val['credit'], format_content_float)
                        sheet.write(rowx, 9, (balance > 0 and balance) or 0.0, format_content_float)
                        sheet.write(rowx, 10, (balance < 0 and -balance) or 0.0, format_content_float)

                        total_col4 = (start > 0 and start) or 0.0
                        total_col5 = (start < 0 and -start) or 0.0
                        total_col6 = val['debit']
                        total_col7 = val['credit']
                        total_col8 = (balance > 0 and balance) or 0.0
                        total_col9 = (balance < 0 and -balance) or 0.0

                        total_sum_col4 += total_col4
                        total_sum_col5 += total_col5
                        total_sum_col6 += total_col6
                        total_sum_col7 += total_col7
                        total_sum_col8 += total_col8
                        total_sum_col9 += total_col9
                    else:
                        cur_start = val['cur_start_debit'] - val['cur_start_credit']
                        cur_balance = cur_start + val['cur_debit'] - val['cur_credit']
                        sheet.write(rowx, 5, (val['currency'] or '') if (val['currency_id'] != self.company_id.currency_id.id) else '', format_content_text)
                        sheet.write(rowx, 6, (cur_start > 0 and cur_start) or '', format_content_float)
                        sheet.write(rowx, 7, (start > 0 and start) or 0.0, format_content_float)
                        sheet.write(rowx, 8, (cur_start < 0 and -cur_start) or '', format_content_float)
                        sheet.write(rowx, 9, (start < 0 and -start) or 0.0, format_content_float)
                        sheet.write(rowx, 10, val['cur_debit'] or '', format_content_float)
                        sheet.write(rowx, 11, val['debit'], format_content_float)
                        sheet.write(rowx, 12, val['cur_credit'] or '', format_content_float)
                        sheet.write(rowx, 13, val['credit'], format_content_float)
                        sheet.write(rowx, 14, (cur_balance > 0 and cur_balance) or '', format_content_float)
                        sheet.write(rowx, 15, (balance > 0 and balance) or 0.0, format_content_float)
                        sheet.write(rowx, 16, (cur_balance < 0 and -cur_balance) or '', format_content_float)
                        sheet.write(rowx, 17, (balance < 0 and -balance) or 0.0, format_content_float)

                        total_col4 = (cur_start > 0 and cur_start) or 0.0
                        total_col5 = (start > 0 and start) or 0.0
                        total_col6 = (cur_start < 0 and -cur_start) or 0.0
                        total_col7 = (start < 0 and -start) or 0.0
                        total_col8 = val['cur_debit']
                        total_col9 = val['debit']
                        total_col10 = val['cur_debit']
                        total_col11 = val['credit']
                        total_col12 = (cur_balance > 0 and cur_balance) or 0.0
                        total_col13 = (balance > 0 and balance) or 0.0
                        total_col14 = (cur_balance < 0 and -cur_balance) or 0.0
                        total_col15 = (balance < 0 and -balance) or 0.0

                        total_sum_col4 += total_col4
                        total_sum_col5 += total_col5
                        total_sum_col6 += total_col6
                        total_sum_col7 += total_col7
                        total_sum_col8 += total_col8
                        total_sum_col9 += total_col9
                        total_sum_col10 += total_col10
                        total_sum_col11 += total_col11
                        total_sum_col12 += total_col12
                        total_sum_col13 += total_col13
                        total_sum_col14 += total_col14
                        total_sum_col15 += total_col15
                    num += 1
                    inner_seq += 1
                    rowx += 1
                rowx += 1
                coly = 4
                if self.currency_transaction:
                    coly = 5
                sheet.merge_range(rowx - 1, 3, rowx - 1, coly, _('Total'), format_group_right)
                if not self.currency_transaction:
                    sheet.write_formula(rowx - 1, coly + 1, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 2, coly + 1) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 1) + ')}' if not self.currency_transaction else '', format_group_float)
                    sheet.write_formula(rowx - 1, coly + 3, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 2, coly + 3) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 3) + ')}' if not self.currency_transaction else '', format_group_float)
                    sheet.write_formula(rowx - 1, coly + 5, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 2, coly + 5) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 5) + ')}' if not self.currency_transaction else '', format_group_float)
                else:
                    sheet.write(rowx - 1, coly + 1, '', format_group_float)
                    sheet.write(rowx - 1, coly + 3, '', format_group_float)
                    sheet.write(rowx - 1, coly + 5, '', format_group_float)
                sheet.write_formula(rowx - 1, coly + 2, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 2, coly + 2) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 2) + ')}', format_group_float)
                sheet.write_formula(rowx - 1, coly + 4, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 2, coly + 4) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 4) + ')}', format_group_float)
                sheet.write_formula(rowx - 1, coly + 6, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 2, coly + 6) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 6) + ')}', format_group_float)
                if self.currency_transaction:
                    sheet.write(rowx - 1, coly + 7, '', format_group_float)
                    sheet.write_formula(rowx - 1, coly + 8, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 2, coly + 8) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 8) + ')}', format_group_float)
                    sheet.write(rowx - 1, coly + 9, '', format_group_float)
                    sheet.write_formula(rowx - 1, coly + 10, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 2, coly + 10) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 10) + ')}', format_group_float)
                    sheet.write(rowx - 1, coly + 11, '', format_group_float)
                    sheet.write_formula(rowx - 1, coly + 12, '{=SUM(' + xl_rowcol_to_cell(rowx - seq - 2, coly + 12) + ':' + xl_rowcol_to_cell(rowx - 2, coly + 12) + ')}', format_group_float)
                seq += 1
                inner_seq = 0
        else:
            if self.is_grouping:
                old_code = 0
                for code_type in sorted(code_dict.values(), key=itemgetter('acc_code')):
                    if code_type['acc_code'] is not old_code:
                        old_code = code_type['acc_code']
                        sheet.merge_range(rowx, 0, rowx, 9, _(code_type['acc_type_name']), format_filter)
                        rowx += 1
                        seq += 1
                        inner_seq += 1
                        for val in sorted(data_dict.values(), key=itemgetter('acc_type_code', 'code')):
                            if val['acc_type_name'] is code_type['acc_type_name']:
                                sheet.write(rowx, 0, num, format_content_number)
                                sheet.write(rowx, 1, val['code'], format_content_text)
                                sheet.write(rowx, 2, val['name'], format_content_text)
                                start = val['start_debit'] - val['start_credit']
                                balance = start + val['debit'] - val['credit']
                                if not self.currency_transaction:
                                    sheet.write(rowx, 3, (start > 0 and start) or 0.0, format_content_float)
                                    sheet.write(rowx, 4, (start < 0 and -start) or 0.0, format_content_float)
                                    sheet.write(rowx, 5, val['debit'], format_content_float)
                                    sheet.write(rowx, 6, val['credit'], format_content_float)
                                    sheet.write(rowx, 7, (balance > 0 and balance) or 0.0, format_content_float)
                                    sheet.write(rowx, 8, (balance < 0 and -balance) or 0.0, format_content_float)

                                    total_col4 += (start > 0 and start) or 0.0
                                    total_col5 += (start < 0 and -start) or 0.0
                                    total_col6 += val['debit']
                                    total_col7 += val['credit']
                                    total_col8 += (balance > 0 and balance) or 0.0
                                    total_col9 += (balance < 0 and -balance) or 0.0

                                    total_sum_col4 += total_col4
                                    total_sum_col5 += total_col5
                                    total_sum_col6 += total_col6
                                    total_sum_col7 += total_col7
                                    total_sum_col8 += total_col8
                                    total_sum_col9 += total_col9
                                else:
                                    cur_start = val['cur_start_debit'] - val['cur_start_credit']
                                    cur_balance = cur_start + val['cur_debit'] - val['cur_credit']
                                    sheet.write(rowx, 3, (val['currency'] or '') if (val['currency_id'] != self.company_id.currency_id.id) else '', format_content_text)
                                    sheet.write(rowx, 4, (cur_start > 0 and cur_start) or '', format_content_float)
                                    sheet.write(rowx, 5, (start > 0 and start) or 0.0, format_content_float)
                                    sheet.write(rowx, 6, (cur_start < 0 and -cur_start) or '', format_content_float)
                                    sheet.write(rowx, 7, (start < 0 and -start) or 0.0, format_content_float)
                                    sheet.write(rowx, 8, val['cur_debit'] or '', format_content_float)
                                    sheet.write(rowx, 9, val['debit'], format_content_float)
                                    sheet.write(rowx, 10, val['cur_credit'] or '', format_content_float)
                                    sheet.write(rowx, 11, val['credit'], format_content_float)
                                    sheet.write(rowx, 12, (cur_balance > 0 and cur_balance) or '', format_content_float)
                                    sheet.write(rowx, 13, (balance > 0 and balance) or 0.0, format_content_float)
                                    sheet.write(rowx, 14, (cur_balance < 0 and -cur_balance) or '', format_content_float)
                                    sheet.write(rowx, 15, (balance < 0 and -balance) or 0.0, format_content_float)

                                    total_col4 += (cur_start > 0 and cur_start) or 0.0
                                    total_col5 += (start > 0 and start) or 0.0
                                    total_col6 += (cur_start < 0 and -cur_start) or 0.0
                                    total_col7 += (start < 0 and -start) or 0.0
                                    total_col8 += val['cur_debit']
                                    total_col9 += val['debit']
                                    total_col10 += val['cur_debit']
                                    total_col11 += val['credit']
                                    total_col12 += (cur_balance > 0 and cur_balance) or 0.0
                                    total_col13 += (balance > 0 and balance) or 0.0
                                    total_col14 += (cur_balance < 0 and -cur_balance) or 0.0
                                    total_col15 += (balance < 0 and -balance) or 0.0

                                    total_sum_col4 += total_col4
                                    total_sum_col5 += total_col5
                                    total_sum_col6 += total_col6
                                    total_sum_col7 += total_col7
                                    total_sum_col8 += total_col8
                                    total_sum_col9 += total_col9
                                    total_sum_col10 += total_col10
                                    total_sum_col11 += total_col11
                                    total_sum_col12 += total_col12
                                    total_sum_col13 += total_col13
                                    total_sum_col14 += total_col14
                                    total_sum_col15 += total_col15

                                num += 1
                                seq += 1
                                inner_seq += 1
                                rowx += 1

                                total_col4 = 0
                                total_col5 = 0
                                total_col6 = 0
                                total_col7 = 0
                                total_col8 = 0
                                total_col9 = 0
                                total_col10 = 0
                                total_col11 = 0
                                total_col12 = 0
                                total_col13 = 0
                                total_col14 = 0
                                total_col15 = 0
                        coly = 2
                        if self.currency_transaction:
                            coly = 3
                        sheet.merge_range(rowx, 0, rowx, coly, _('Total'), format_group_right)
                        if not self.currency_transaction:
                            sheet.write_formula(rowx, coly + 1, '{=SUM(' + xl_rowcol_to_cell(rowx - inner_seq + 1, coly + 1) + ':' + xl_rowcol_to_cell(rowx - 1, coly + 1) + ')}', format_group_float)
                        else:
                            sheet.write(rowx, coly + 1, '', format_group_float)
                        sheet.write_formula(rowx, coly + 2, '{=SUM(' + xl_rowcol_to_cell(rowx - inner_seq + 1, coly + 2) + ':' + xl_rowcol_to_cell(rowx - 1, coly + 2) + ')}', format_group_float)
                        if not self.currency_transaction:
                            sheet.write_formula(rowx, coly + 3, '{=SUM(' + xl_rowcol_to_cell(rowx - inner_seq + 1, coly + 3) + ':' + xl_rowcol_to_cell(rowx - 1, coly + 3) + ')}', format_group_float)
                        else:
                            sheet.write(rowx, coly + 3, '', format_group_float)
                        sheet.write_formula(rowx, coly + 4, '{=SUM(' + xl_rowcol_to_cell(rowx - inner_seq + 1, coly + 4) + ':' + xl_rowcol_to_cell(rowx - 1, coly + 4) + ')}', format_group_float)
                        if not self.currency_transaction:
                            sheet.write_formula(rowx, coly + 5, '{=SUM(' + xl_rowcol_to_cell(rowx - inner_seq + 1, coly + 5) + ':' + xl_rowcol_to_cell(rowx - 1, coly + 5) + ')}', format_group_float)
                        else:
                            sheet.write(rowx, coly + 5, '', format_group_float)
                        sheet.write_formula(rowx, coly + 6, '{=SUM(' + xl_rowcol_to_cell(rowx - inner_seq + 1, coly + 6) + ':' + xl_rowcol_to_cell(rowx - 1, coly + 6) + ')}', format_group_float)
                        if self.currency_transaction:
                            sheet.write(rowx, coly + 7, '', format_group_float)
                            sheet.write_formula(rowx, coly + 8, '{=SUM(' + xl_rowcol_to_cell(rowx - inner_seq + 1, coly + 8) + ':' + xl_rowcol_to_cell(rowx - 1, coly + 8) + ')}', format_group_float)
                            sheet.write(rowx, coly + 9, '', format_group_float)
                            sheet.write_formula(rowx, coly + 10, '{=SUM(' + xl_rowcol_to_cell(rowx - inner_seq + 1, coly + 10) + ':' + xl_rowcol_to_cell(rowx - 1, coly + 10) + ')}', format_group_float)
                            sheet.write(rowx, coly + 11, '', format_group_float)
                            sheet.write_formula(rowx, coly + 12, '{=SUM(' + xl_rowcol_to_cell(rowx - inner_seq + 1, coly + 12) + ':' + xl_rowcol_to_cell(rowx - 1, coly + 12) + ')}', format_group_float)

                        rowx += 1
                        seq += 1
                        inner_seq = 0
            else:
                for val in sorted(data_dict.values(), key=itemgetter('acc_type_code', 'code')):
                    sheet.write(rowx, 0, num, format_content_number)
                    sheet.write(rowx, 1, val['code'], format_content_text)
                    sheet.write(rowx, 2, val['name'], format_content_text)
                    start = val['start_debit'] - val['start_credit']
                    balance = start + val['debit'] - val['credit']
                    if not self.currency_transaction:
                        sheet.write(rowx, 3, (start > 0 and start) or 0.0, format_content_float)
                        sheet.write(rowx, 4, (start < 0 and -start) or 0.0, format_content_float)
                        sheet.write(rowx, 5, val['debit'], format_content_float)
                        sheet.write(rowx, 6, val['credit'], format_content_float)
                        sheet.write(rowx, 7, (balance > 0 and balance) or 0.0, format_content_float)
                        sheet.write(rowx, 8, (balance < 0 and -balance) or 0.0, format_content_float)

                        total_col4 += (start > 0 and start) or 0.0
                        total_col5 += (start < 0 and -start) or 0.0
                        total_col6 += val['debit']
                        total_col7 += val['credit']
                        total_col8 += (balance > 0 and balance) or 0.0
                        total_col9 += (balance < 0 and -balance) or 0.0

                        total_sum_col4 += total_col4
                        total_sum_col5 += total_col5
                        total_sum_col6 += total_col6
                        total_sum_col7 += total_col7
                        total_sum_col8 += total_col8
                        total_sum_col9 += total_col9
                    else:
                        cur_start = val['cur_start_debit'] - val['cur_start_credit']
                        cur_balance = cur_start + val['cur_debit'] - val['cur_credit']
                        sheet.write(rowx, 3, (val['currency'] or '') if (val['currency_id'] != self.company_id.currency_id.id) else '', format_content_text)
                        sheet.write(rowx, 4, (cur_start > 0 and cur_start) or '', format_content_float)
                        sheet.write(rowx, 5, (start > 0 and start) or 0.0, format_content_float)
                        sheet.write(rowx, 6, (cur_start < 0 and -cur_start) or '', format_content_float)
                        sheet.write(rowx, 7, (start < 0 and -start) or 0.0, format_content_float)
                        sheet.write(rowx, 8, val['cur_debit'] or '', format_content_float)
                        sheet.write(rowx, 9, val['debit'], format_content_float)
                        sheet.write(rowx, 10, val['cur_credit'] or '', format_content_float)
                        sheet.write(rowx, 11, val['credit'], format_content_float)
                        sheet.write(rowx, 12, (cur_balance > 0 and cur_balance) or '', format_content_float)
                        sheet.write(rowx, 13, (balance > 0 and balance) or 0.0, format_content_float)
                        sheet.write(rowx, 14, (cur_balance < 0 and -cur_balance) or '', format_content_float)
                        sheet.write(rowx, 15, (balance < 0 and -balance) or 0.0, format_content_float)

                        total_col4 += (cur_start > 0 and cur_start) or 0.0
                        total_col5 += (start > 0 and start) or 0.0
                        total_col6 += (cur_start < 0 and -cur_start) or 0.0
                        total_col7 += (start < 0 and -start) or 0.0
                        total_col8 += val['cur_debit']
                        total_col9 += val['debit']
                        total_col10 += val['cur_debit']
                        total_col11 += val['credit']
                        total_col12 += (cur_balance > 0 and cur_balance) or 0.0
                        total_col13 += (balance > 0 and balance) or 0.0
                        total_col14 += (cur_balance < 0 and -cur_balance) or 0.0
                        total_col15 += (balance < 0 and -balance) or 0.0

                        total_sum_col4 += total_col4
                        total_sum_col5 += total_col5
                        total_sum_col6 += total_col6
                        total_sum_col7 += total_col7
                        total_sum_col8 += total_col8
                        total_sum_col9 += total_col9
                        total_sum_col10 += total_col10
                        total_sum_col11 += total_col11
                        total_sum_col12 += total_col12
                        total_sum_col13 += total_col13
                        total_sum_col14 += total_col14
                        total_sum_col15 += total_col15

                    num += 1
                    seq += 1
                    inner_seq += 1
                    rowx += 1

                    total_col4 = 0
                    total_col5 = 0
                    total_col6 = 0
                    total_col7 = 0
                    total_col8 = 0
                    total_col9 = 0
                    total_col10 = 0
                    total_col11 = 0
                    total_col12 = 0
                    total_col13 = 0
                    total_col14 = 0
                    total_col15 = 0

                if self.is_grouping:
                    rowx += 1
                    seq += 1

        coly = 3 if self.currency_transaction else 2
        coly += 2 if self.show_analytic_account and self.grouping != 'analytic_account' else 0
        sheet.merge_range(rowx, 0, rowx, coly, _('Total amount'), format_group_right)
        sheet.write(rowx, coly + 1, total_sum_col4 if not self.currency_transaction else '', format_group_float)
        sheet.write(rowx, coly + 2, total_sum_col5, format_group_float)
        sheet.write(rowx, coly + 3, total_sum_col6 if not self.currency_transaction else '', format_group_float)
        sheet.write(rowx, coly + 4, total_sum_col7, format_group_float)
        sheet.write(rowx, coly + 5, total_sum_col8 if not self.currency_transaction else '', format_group_float)
        sheet.write(rowx, coly + 6, total_sum_col9, format_group_float)
        if self.currency_transaction:
            sheet.write(rowx, coly + 7, '', format_group_float)
            sheet.write(rowx, coly + 8, total_sum_col11, format_group_float)
            sheet.write(rowx, coly + 9, '', format_group_float)
            sheet.write(rowx, coly + 10, total_sum_col13, format_group_float)
            sheet.write(rowx, coly + 11, '', format_group_float)
            sheet.write(rowx, coly + 12, total_sum_col15, format_group_float)
        rowx += 3

        sheet.merge_range(rowx, 2, rowx, 4, '%s: ........................................... (                          )' % _('Executive Director'), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 2, rowx, 4, '%s: ........................................... (                          )' % _('General Accountant'), format_filter)

        book.close()

        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()
