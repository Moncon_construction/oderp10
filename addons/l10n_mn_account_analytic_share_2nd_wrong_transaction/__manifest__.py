# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Analytic Share 2 Wrong Transaction ",
    'version': '1.0',
    'depends': ['l10n_mn_analytic_2nd_account_share', 'l10n_mn_account_analytic_share_wrong_transaction'],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Analytic Modules',
    'description': """""",
    'website' : 'http://asterisk-tech.mn',
    'data': [
        'views/account_move_line_analytic_share_second_view.xml',
    ],
}