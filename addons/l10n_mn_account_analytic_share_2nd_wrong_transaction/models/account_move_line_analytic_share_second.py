#-*- coding: utf-8 -*-
from odoo import fields, models, api, _

class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    @api.multi
    def get_empty_journal_line_analytic_share_two(self):
        ids = []
        qry = """
        SELECT analytic_share.move_id 
        FROM (
         SELECT acml.id AS move_id, count(share.id) AS share_count
         FROM account_move_line acml
         LEFT JOIN account_analytic_share share ON acml.id = share.move_line_id2 
         LEFT JOIN account_account aa ON acml.account_id = aa.id 
         WHERE aa.req_analytic_account = 't'
         GROUP BY acml.id
        ) AS analytic_share  
        WHERE analytic_share.share_count = 0;
        """
        self.env.cr.execute(qry)
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]

        tree_id = self.env.ref("account.view_move_line_tree")
        form_id = self.env.ref("account.view_move_line_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Empty Tree Number Two'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move.line',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }

    @api.multi
    def get_one_hundred_percent_diff_two(self):
        ids = []
        qry = """
        SELECT analytic_share.move_id, share_count, share_rate
        FROM (
         SELECT count(share.id) AS share_count, acml.id AS move_id, sum(share.rate) AS share_rate 
         FROM account_move_line acml 
         LEFT JOIN account_analytic_share share ON acml.id = share.move_line_id2
         GROUP BY acml.id
        ) AS analytic_share 
        WHERE share_rate != 100
        """
        self._cr.execute(qry)
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]

        tree_id = self.env.ref("account.view_move_line_tree")
        form_id = self.env.ref("account.view_move_line_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Analytic Share 2 was 100% different'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move.line',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
    }
