# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Analytic Share Report",
    'version': '1.0',
    'depends': ['l10n_mn_account', 'l10n_mn_account_analytic_report'],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Analytic Modules',
    'description': """
        Харилцагчын авлага, өглөгийн товчоо тайланг шинжилгээний тархалттай үед гаргадаг болгосон.
    """,
    'website' : 'http://asterisk-tech.mn',
    'data': [
        'report/report_account_partner_ledger_wizard.xml',
        'report/report_account_partner_balance_wizard.xml',
        'report/account_report_general_account_view.xml'
    ],
}
