# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
import xlsxwriter

from odoo import api, fields, models, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles  # @UnresolvedImport


class ReportAccountPartnerBalance(models.TransientModel):
    """
        Авлага, өглөгийн дэлгэрэнгүй тайлан
    """

    _inherit = 'report.account.partner.balance'
    
    report_type = fields.Selection([('move_line', 'From Account Move Line'),
                                    ('analytic_line', 'From Account Analytic Line')], string='Report Type', required=True, default='move_line')
    group_by = fields.Selection([('partner', 'Partner'),
                                 ('account', 'Account')], string='Group By', required=True, default='partner')

    @api.multi
    def export_report(self):
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        # create name
        report_name = _('Partner Balance Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_title = book.add_format(ReportExcelCellStyles.format_title_border)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_filter_right = book.add_format(ReportExcelCellStyles.format_filter_right)
        format_group_color = book.add_format(ReportExcelCellStyles.format_group_color_border)
        format_group = book.add_format(ReportExcelCellStyles.format_group_border)
        format_group_left = book.add_format(ReportExcelCellStyles.format_group_left_border)
        format_group_float_color_border = book.add_format(ReportExcelCellStyles.format_group_float_color_border)
        format_group_float_border = book.add_format(ReportExcelCellStyles.format_group_float_border)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_noborder)
        format_content_center = book.add_format(ReportExcelCellStyles.format_content_center_noborder)
        format_content_center_color = book.add_format(ReportExcelCellStyles.format_content_center_color_noborder)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float_noborder)
        format_content_float_color = book.add_format(ReportExcelCellStyles.format_content_float_color_noborder)

        move_line_obj = self.env['account.move.line']
        account_obj = self.env['account.account']
        partner_obj = self.env['res.partner']
        salesman_obj = self.env['res.users']
        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(
            filename_prefix=('partner_balance_report'), form_title=file_name).create({})
        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        sheet.freeze_panes(8, 0)
        rowx = 1
        # compute column
        sheet.set_column('A:A', 4)
        sheet.set_column('B:B', 40)
        sheet.set_column('C:C', 8)
        sheet.set_column('D:D', 8)
        sheet.set_column('E:E', 30)
        sheet.set_column('F:F', 8)
        sheet.set_column('G:G', 8)
        sheet.set_column('H:H', 15)
        sheet.set_column('I:I', 15)
        sheet.set_column('J:J', 15)
        sheet.set_column('K:K', 15)
        sheet.set_column('L:L', 8)
        sheet.set_column('M:M', 8)
        # create name
        sheet.merge_range(rowx, 0, rowx, 1, '%s: %s' % (_('Company'), self.company_id.name), format_filter)
        if self.group_by == 'partner':
            sheet.merge_range(rowx, 9, rowx, 12, '%s: %s' % (_('Printed date'), time.strftime('%Y-%m-%d')), format_filter_right)
            rowx += 1
            sheet.merge_range(rowx, 0, rowx, 12, report_name.upper(), format_name)
        elif self.group_by == 'account':
            sheet.merge_range(rowx, 8, rowx, 11, '%s: %s' % (_('Printed date'), time.strftime('%Y-%m-%d')), format_filter_right)
            rowx += 1
            sheet.merge_range(rowx, 0, rowx, 11, report_name.upper(), format_name)
        else:
            rowx += 1
            sheet.merge_range(rowx, 0, rowx, 11, report_name.upper(), format_name)
        rowx += 1
        # create duration
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s - %s' % (_('Duration'), self.date_from, self.date_to), format_filter)
        rowx += 1
        if self.group_by == 'salesman':
            sheet.merge_range(rowx, 8, rowx, 11, '%s: %s' % (_('Printed date'), time.strftime('%Y-%m-%d')), format_filter_right)
        rowx += 2
        # table header
        sheet.merge_range(rowx, 0, rowx + 1, 0, _('Seq'), format_title)
        if self.group_by == 'salesman':
            sheet.merge_range(rowx, 1, rowx + 1, 1, _('Account/Salesman/Partner\n Reference'), format_title)
        else:
            sheet.merge_range(rowx, 1, rowx + 1, 1, _('Partner/Account\n Reference'), format_title)
        sheet.merge_range(rowx, 2, rowx + 1, 2, _('Origin'), format_title)
        sheet.merge_range(rowx, 3, rowx + 1, 3, _('Date'), format_title)
        sheet.merge_range(rowx, 4, rowx + 1, 4, _('Transaction name'), format_title)
        sheet.merge_range(rowx, 5, rowx + 1, 5, _('Currency'), format_title)
        sheet.merge_range(rowx, 6, rowx + 1, 6, _('Currency Rate'), format_title)
        sheet.merge_range(rowx, 7, rowx + 1, 7, _('Start Balance'), format_title)
        sheet.merge_range(rowx, 8, rowx, 9, _('Transaction Amount'), format_title)
        sheet.write(rowx + 1, 8, _('Debit'), format_title)
        sheet.write(rowx + 1, 9, _('Credit'), format_title)
        sheet.merge_range(rowx, 10, rowx + 1, 10, _('End Balance'), format_title)
        sheet.merge_range(rowx, 11, rowx + 1, 11, _('Reconcicle'), format_title)
        if self.group_by in ('partner', 'salesman'):
            sheet.merge_range(rowx, 12, rowx + 1, 12, _('Limit'), format_title)
        rowx += 2
        # Данснуудын id-г олох
        if self.account_ids:
            account_ids = self.account_ids.ids
        elif self.account_type == 'receivable':
            account_ids = account_obj.search(
                [('internal_type', '=', self.account_type), ('company_id', '=', self.company_id.id)]).ids
        elif self.account_type == 'payable':
            account_ids = account_obj.search(
                [('internal_type', '=', self.account_type), ('company_id', '=', self.company_id.id)]).ids
        else:
            account_ids = account_obj.search(
                [('internal_type', 'in', ('receivable', 'payable')), ('company_id', '=', self.company_id.id)]).ids
        # Бүлэглэлт
        order_by = 'mv.ml_date'
        # Харилцагч нарыг дамжуулах
        partner_ids = []
        if self.is_employee == True:
            partner_ids = partner_obj.with_context(active_test=False).search([('employee','=', True)]).ids
            if self.partner_id:
                partner_ids = partner_obj.with_context(active_test=False).search([('parent_id', 'child_of', [self.partner_id.id]), ('employee','=', True)]).ids
        else:
            if self.partner_id:
                partner_ids = partner_obj.with_context(active_test=False).search([('parent_id', 'child_of', [self.partner_id.id])]).ids
            else:
                partner_ids = partner_obj.with_context(active_test=False).search([]).ids

        if self.salesperson_id:
            salesman_ids = self.salesperson_id.ids
        else:
            salesman_ids = salesman_obj.search([]).ids

        if self.group_by in ('partner', 'account'):
            # Эхлэл болон тухайн хугацааны хоорондох утгыг олох
            lines = move_line_obj.with_context(partner_ids=partner_ids, order_by=order_by).get_partner_balance(
                self.company_id.id, account_ids, self.date_from, self.date_to, self.target_move)
        elif self.group_by == 'salesman':
            # Эхлэл болон тухайн хугацааны хоорондох утгыг олох
            lines = move_line_obj.with_context(partner_ids=partner_ids, salesman_ids=salesman_ids,
                                               order_by=order_by).get_partner_balance(self.company_id.id, account_ids, self.date_from, self.date_to, self.target_move)
        if self.report_type == 'analytic_line':
            analytic_ids = self.analytic_account_ids.ids
            lines = move_line_obj.with_context(salesman_ids=salesman_ids, partner_ids=partner_ids, order_by=order_by).get_partner_all_analytic_balance(self.company_id.id, account_ids, analytic_ids, self.date_from, self.date_to)
        ref = ''
        company_currency = self.company_id.currency_id
        if lines:
            partner_dict = account_dict = salesman_dict = {}
            # Бүлэглэх хэсэг
            if self.group_by == 'partner':
                # Харилцагчаар бүлэглэх тохиолдолд Dictionary гийн эхний түлхүүр Харилцагч байна
                first_obj = self.env['res.partner']
                second_obj = self.env['account.account']
                ml_count = 1
                for line in lines:
                    ref = line['ref']
                    if not ref:
                        line_id = move_line_obj.browse(line['ml_id'])
                        ref = line_id.move_id.name
                    if line['ml_date'] <= self.date_to:
                        if line['pid'] not in partner_dict:
                            partner_dict[line['pid']] = {
                                line['account_id']: {
                                    ml_count: {
                                        'name': line['name'],
                                        'ref': ref,
                                        'atype': line['atype'],
                                        'ml_date': line['ml_date'],
                                        'currency_rate': line['currency_rate'],
                                        'cid': line['cid'],
                                        'debit': line['debit'],
                                        'rec': line['rec'],
                                        'credit': line['credit'],
                                        'cur_debit': line['cur_debit'],
                                        'cur_credit': line['cur_credit'],
                                        'invoice': line['invoice'],
                                        'start_balance': line['start_balance'],
                                        'cur_start_balance': line['cur_start_balance']
                                    }
                                }
                            }
                            ml_count += 1
                        else:
                            if line['account_id'] not in partner_dict[line['pid']]:
                                partner_dict[line['pid']].update({
                                    line['account_id']: {
                                        line['ml_id']: {
                                            'name': line['name'],
                                            'ref': ref,
                                            'atype': line['atype'],
                                            'ml_date': line['ml_date'],
                                            'currency_rate': line['currency_rate'],
                                            'cid': line['cid'],
                                            'debit': line['debit'],
                                            'rec': line['rec'],
                                            'credit': line['credit'],
                                            'cur_debit': line['cur_debit'],
                                            'cur_credit': line['cur_credit'],
                                            'invoice': line['invoice'],
                                            'start_balance': line['start_balance'],
                                            'cur_start_balance': line['cur_start_balance']
                                        }
                                    }
                                })
                                ml_count += 1
                            else:
                                partner_dict[line['pid']][line['account_id']].update({line['ml_id']: {
                                    'name': line['name'],
                                    'ref': ref,
                                    'atype': line['atype'],
                                    'ml_date': line['ml_date'],
                                    'currency_rate': line['currency_rate'],
                                    'cid': line['cid'],
                                    'debit': line['debit'],
                                    'rec': line['rec'],
                                    'credit': line['credit'],
                                    'cur_debit': line['cur_debit'],
                                    'cur_credit': line['cur_credit'],
                                    'invoice': line['invoice'],
                                    'start_balance': line['start_balance'],
                                    'cur_start_balance': line['cur_start_balance']
                                }
                                })
                                ml_count += 1
            elif self.group_by == 'salesman':
                # Борлуулалтын ажилтнаар бүлэглэх тохиолдолд Dictionary гийн эхний түлхүүр Борлуулагч байна
                account_objs = self.env['account.account']
                salesman_objs = self.env['res.users']
                partner_objs = self.env['res.partner']

                ml_count = 1
                for line in lines:
                    if line['ml_date'] <= self.date_to:
                        if line['account_id'] not in salesman_dict:
                            salesman_dict[line['account_id']] = {
                                line['salesman_id']: {
                                    line['pid']: {
                                        line['ml_id']: {
                                            'name': line['name'],
                                            'ref': ref,
                                            'atype': line['atype'],
                                            'ml_date': line['ml_date'],
                                            'currency_rate': line['currency_rate'],
                                            'cid': line['cid'],
                                            'debit': line['debit'],
                                            'rec': line['rec'],
                                            'credit': line['credit'],
                                            'cur_debit': line['cur_debit'],
                                            'cur_credit': line['cur_credit'],
                                            'invoice': line['invoice'],
                                            'start_balance': line['start_balance'],
                                            'cur_start_balance': line['cur_start_balance']
                                        }
                                    }
                                }
                            }
                            ml_count += 1
                        else:
                            if line['salesman_id'] not in salesman_dict[line['account_id']]:
                                salesman_dict[line['account_id']].update({
                                    line['salesman_id']: {
                                        line['pid']: {
                                            line['ml_id']: {
                                                'name': line['name'],
                                                'ref': ref,
                                                'atype': line['atype'],
                                                'ml_date': line['ml_date'],
                                                'currency_rate': line['currency_rate'],
                                                'cid': line['cid'],
                                                'debit': line['debit'],
                                                'rec': line['rec'],
                                                'credit': line['credit'],
                                                'cur_debit': line['cur_debit'],
                                                'cur_credit': line['cur_credit'],
                                                'invoice': line['invoice'],
                                                'start_balance': line['start_balance'],
                                                'cur_start_balance': line['cur_start_balance']
                                            }
                                        }
                                    }
                                })
                                ml_count += 1
                            else:
                                if line['pid'] not in salesman_dict[line['account_id']][line['salesman_id']]:
                                    salesman_dict[line['account_id']][line['salesman_id']].update({
                                        line['pid']: {
                                            line['ml_id']: {
                                                'name': line['name'],
                                                'ref': ref,
                                                'atype': line['atype'],
                                                'ml_date': line['ml_date'],
                                                'currency_rate': line['currency_rate'],
                                                'cid': line['cid'],
                                                'debit': line['debit'],
                                                'rec': line['rec'],
                                                'credit': line['credit'],
                                                'cur_debit': line['cur_debit'],
                                                'cur_credit': line['cur_credit'],
                                                'invoice': line['invoice'],
                                                'start_balance': line['start_balance'],
                                                'cur_start_balance': line['cur_start_balance']
                                            }
                                        }
                                    })
                                    ml_count += 1
                                else:
                                    salesman_dict[line['account_id']][line['salesman_id']][line['pid']].update(
                                        {line['ml_id']: {
                                            'name': line['name'],
                                            'ref': ref,
                                            'atype': line['atype'],
                                            'ml_date': line['ml_date'],
                                            'currency_rate': line['currency_rate'],
                                            'cid': line['cid'],
                                            'debit': line['debit'],
                                            'rec': line['rec'],
                                            'credit': line['credit'],
                                            'cur_debit': line['cur_debit'],
                                            'cur_credit': line['cur_credit'],
                                            'invoice': line['invoice'],
                                            'start_balance': line['start_balance'],
                                            'cur_start_balance': line['cur_start_balance']
                                        }
                                        })
                                    ml_count += 1
            else:
                # Дансаар бүлэглэх тохиолдолд Dictionary гийн эхний түлхүүр Данс байна
                ml_count = 1
                for line in lines:
                    if line['ml_date'] <= self.date_to:
                        if line['account_id'] not in partner_dict:
                            partner_dict[line['account_id']] = {
                                line['pid']: {
                                    line['ml_id']: {
                                        'name': line['name'],
                                        'ref': ref,
                                        'atype': line['atype'],
                                        'ml_date': line['ml_date'],
                                        'currency_rate': line['currency_rate'],
                                        'cid': line['cid'],
                                        'debit': line['debit'],
                                        'rec': line['rec'],
                                        'credit': line['credit'],
                                        'cur_debit': line['cur_debit'],
                                        'cur_credit': line['cur_credit'],
                                        'invoice': line['invoice'],
                                        'start_balance': line['start_balance'],
                                        'cur_start_balance': line['cur_start_balance']
                                    }
                                }
                            }
                            ml_count += 1
                        else:
                            if line['pid'] not in partner_dict[line['account_id']]:
                                partner_dict[line['account_id']].update({
                                    line['pid']: {
                                        line['ml_id']: {
                                            'name': line['name'],
                                            'ref': ref,
                                            'atype': line['atype'],
                                            'ml_date': line['ml_date'],
                                            'currency_rate': line['currency_rate'],
                                            'cid': line['cid'],
                                            'debit': line['debit'],
                                            'rec': line['rec'],
                                            'credit': line['credit'],
                                            'cur_debit': line['cur_debit'],
                                            'cur_credit': line['cur_credit'],
                                            'invoice': line['invoice'],
                                            'start_balance': line['start_balance'],
                                            'cur_start_balance': line['cur_start_balance']
                                        }
                                    }
                                })
                                ml_count += 1
                            else:
                                partner_dict[line['account_id']][line['pid']].update({line['ml_id']: {
                                    'name': line['name'],
                                    'ref': ref,
                                    'atype': line['atype'],
                                    'ml_date': line['ml_date'],
                                    'currency_rate': line['currency_rate'],
                                    'cid': line['cid'],
                                    'debit': line['debit'],
                                    'rec': line['rec'],
                                    'credit': line['credit'],
                                    'cur_debit': line['cur_debit'],
                                    'cur_credit': line['cur_credit'],
                                    'invoice': line['invoice'],
                                    'start_balance': line['start_balance'],
                                    'cur_start_balance': line['cur_start_balance']
                                }
                                })
                                ml_count += 1
                first_obj = self.env['account.account']
                second_obj = self.env['res.partner']
            count = 1
            if self.group_by == 'salesman':
                last_total_start = last_total_debit = last_total_credit = last_total_end = ''
                # Account ID Dict
                for dict in salesman_dict:
                    account_object = account_objs.search([('id', '=', dict)])
                    account_sum_currency_start = account_sum_currency_debit = account_sum_currency_credit = account_sum_currency_end = ''
                    sheet.merge_range(rowx, 0, rowx, 12, u'[%s] %s' % (account_object.code, account_object.name), format_group_left)
                    rowx += 1
                    count = 1
                    second_count = 1
                    third_count = 1
                    # salesman ID dict
                    for lines in salesman_dict[dict]:
                        salesman_sum_currency_start = salesman_sum_currency_debit = salesman_sum_currency_credit = salesman_sum_currency_end = ''
                        sheet.write(rowx, 0, count, format_content_center)
                        second_count = 1
                        third_count = 1
                        salesman_object = salesman_objs.search([('id', '=', lines)])
                        sheet.merge_range(rowx, 1, rowx, 6, _('Salesman %s') % (salesman_object.name), format_group)
                        sheet.write(rowx, 11, '', format_group)
                        sheet.write(rowx, 12, '', format_group)
                        sheet.set_row(rowx, None, None, {'hidden': 0, 'level': 1})
                        salesman_rowx = rowx
                        rowx += 1
                        start_balance = cur_start_balance = 0
                        first = 0
                        # Partner Id
                        for partner_id in salesman_dict[dict][lines]:
                            total_start = total_currency_start = total_debit = total_currency_debit = 0
                            total_credit = total_currency_credit = total_end = total_currency_end = 0
                            third_count = 1
                            partner_object = partner_objs.with_context(active_test=False).search([('id', '=', partner_id)])
                            sheet.write(rowx, 0, u'%s.%s' % (count, second_count), format_content_center)
                            sheet.merge_range(rowx, 1, rowx, 6, partner_object.name, format_group_left)
                            partner_rowx = rowx
                            sheet.set_row(rowx, None, None, {'hidden': 0, 'level': 1})
                            rowx += 1
                            starting_balance = cur_starting_balance = 0
                            not_start_balance = False
                            for key, value in sorted(salesman_dict[dict][lines][partner_id].iteritems()):
                                if value['start_balance'] == 0 and (value['debit'] != 0 or value['credit'] != 0):
                                    not_start_balance = True
                            if not_start_balance:
                                for key, value in sorted(salesman_dict[dict][lines][partner_id].iteritems(), key=lambda x: x[1]['ml_date']):  # Dictionary эрэмбэлэх
                                    if value['ml_date'] < self.date_from:
                                        starting_balance += value['start_balance']
                                        cur_starting_balance += value['cur_start_balance']
                                    elif value['ml_date'] <= self.date_to:
                                        if first < 1:
                                            start_balance = starting_balance
                                            cur_start_balance = cur_starting_balance
                                            total_currency_start = cur_start_balance
                                            total_start = start_balance
                                            first += 1
                                        line_end = start_balance
                                        cur_line_end = cur_start_balance
                                        sheet.write(rowx, 0, u'%s.%s.%s' % (count, second_count, third_count), format_content_center)
                                        text = u''
                                        number = value.get('ref') if value.get('ref') else ''
                                        if value['invoice']:
                                            invoice = self.env['account.invoice'].search([('id', '=', value['invoice'])])
                                            if invoice:
                                                text = invoice.origin if invoice.origin else u''
                                                if not number:
                                                    number = invoice.number if invoice.number else u''
                                        sheet.write(rowx, 1, number, format_content_text)
                                        sheet.write(rowx, 2, text, format_content_text)
                                        sheet.write(rowx, 3, value['ml_date'], format_content_center)
                                        sheet.write(rowx, 4, value['name'], format_content_text)
                                        sheet.write(rowx, 5, company_currency.name, format_content_center)
                                        sheet.write(rowx, 6, u'1.0', format_content_float)
                                        sheet.write(rowx, 7, start_balance, format_content_float)
                                        sheet.set_row(rowx, None, None, {'hidden': 0, 'level': 2})

                                        if value['cid'] and value['cid'] != company_currency.id:
                                            sheet.write(rowx + 1, 7, cur_start_balance, format_content_center)

                                        total_debit += value['debit']
                                        total_credit += value['credit']
                                        total_currency_debit += value['cur_debit']
                                        total_currency_credit += value['cur_credit']

                                        start_balance += value['debit'] - value['credit']
                                        cur_start_balance += value['cur_debit'] - value['cur_credit']
                                        line_end += value['debit'] - value['credit']
                                        cur_line_end += value['cur_debit'] - value['cur_credit']

                                        total_end = line_end
                                        total_currency_end = cur_line_end

                                        sheet.write(rowx, 8, value['debit'], format_content_float)
                                        sheet.write(rowx, 9, value['credit'], format_content_float)
                                        sheet.write(rowx, 10, line_end, format_content_float)

                                        rec = self.env['account.full.reconcile'].search([('id', '=', value['rec'])])
                                        sheet.write(rowx, 11, rec.name if rec else '', format_content_center)
                                        sheet.write(rowx, 12, '', format_content_text)
                                        if value['cid'] and value['cid'] != company_currency.id:
                                            rowx += 1
                                            sheet.merge_range(rowx, 0, rowx, 4, '')
                                            sheet.write(rowx, 5, self.env['res.currency'].search([('id', '=', value['cid'])]).name, format_content_center_color)
                                            sheet.write(rowx, 6, value['currency_rate'], format_content_float_color)
                                            sheet.write(rowx, 8, value['cur_debit'], format_content_float_color)
                                            sheet.write(rowx, 9, value['cur_credit'], format_content_float_color)
                                            sheet.write(rowx, 10, cur_line_end, format_content_center)
                                            sheet.set_row(rowx, None, None, {'hidden': 0, 'level': 2})
                                        rowx += 1
                                    third_count += 1
                            else:
                                len_line = len(salesman_dict[dict][lines])
                                starting_balance = cur_starting_balance = 0
                                while len_line != 1 and len_line != 0:
                                    for key, value in sorted(salesman_dict[dict][lines][partner_id].iteritems()):
                                        if value['ml_date'] < self.date_from:
                                            starting_balance += value['start_balance']
                                            cur_starting_balance += value['cur_start_balance']
                                    len_line -= 1
                                for key, value in sorted(salesman_dict[dict][lines][partner_id].iteritems()):
                                    if value['ml_date'] <= self.date_to:
                                        total_debit = value['debit'] if value['debit'] else 0
                                        total_credit = value['credit'] if value['credit'] else 0
                                        total_start = value['start_balance'] if starting_balance == value['start_balance'] else starting_balance
                                        total_end = value['start_balance'] + total_debit - total_credit
                                        sheet.write(rowx, 0, u'%s.%s.%s' % (count, second_count, third_count), format_content_center)
                                        sheet.write(rowx, 1, '', format_content_text)
                                        sheet.write(rowx, 2, '', format_content_text)
                                        sheet.write(rowx, 3, value['ml_date'] if value['ml_date'] >= self.date_from else '', format_content_center)
                                        sheet.write(rowx, 4, value['name'], format_content_text)
                                        sheet.write(rowx, 5, company_currency.name, format_content_center)
                                        sheet.write(rowx, 6, u'1.0', format_content_float)
                                        sheet.write(rowx, 7, total_start, format_content_float)
                                        sheet.set_row(rowx, None, None, {'hidden': 0, 'level': 2})
                                        if value['cid'] and value['cid'] != company_currency.id:
                                            sheet.write(rowx + 1, 7, value['start_balance'], format_content_float)
                                        sheet.write(rowx, 8, value['debit'], format_content_float)
                                        sheet.write(rowx, 9, value['credit'], format_content_float)
                                        sheet.write(rowx, 10, total_start + total_debit - total_credit, format_content_float)
                                        sheet.write(rowx, 11, '', format_content_float)
                                        sheet.write(rowx, 12, '', format_content_float)
                                        rowx += 1
                                        break
                                    third_count += 1
                            sheet.write(partner_rowx, 7, total_start, format_group_float_border)
                            sheet.write(partner_rowx, 8, total_debit, format_group_float_border)
                            sheet.write(partner_rowx, 9, total_credit, format_group_float_border)
                            sheet.write(partner_rowx, 10, total_end, format_group_float_border)
                            sheet.write(partner_rowx, 11, '', format_group_float_border)
                            sheet.write(partner_rowx, 12, '', format_group_float_border)
                            if salesman_sum_currency_start == '':
                                salesman_sum_currency_start += 'H%s' % (partner_rowx + 1)
                                salesman_sum_currency_debit += 'I%s' % (partner_rowx + 1)
                                salesman_sum_currency_credit += 'J%s' % (partner_rowx + 1)
                                salesman_sum_currency_end += 'K%s' % (partner_rowx + 1)
                            else:
                                salesman_sum_currency_start += ',H%s' % (partner_rowx + 1)
                                salesman_sum_currency_debit += ',I%s' % (partner_rowx + 1)
                                salesman_sum_currency_credit += ',J%s' % (partner_rowx + 1)
                                salesman_sum_currency_end += ',K%s' % (partner_rowx + 1)
                            if total_currency_start > 0.0 or total_currency_debit > 0.0 or total_currency_credit > 0.0 or total_currency_end > 0.0:
                                sheet.merge_range(rowx, 0, rowx, 6, _('TOTAL (CURRENCY)'), format_group_color)
                                sheet.set_row(rowx, None, None, {'hidden': 0, 'level': 1})
                                sheet.write(rowx, 7, total_currency_start, format_group_float_color_border)
                                sheet.write(rowx, 8, total_currency_debit, format_group_float_color_border)
                                sheet.write(rowx, 9, total_currency_credit, format_group_float_color_border)
                                sheet.write(rowx, 10, total_currency_end, format_group_float_color_border)
                                sheet.write(rowx, 11, '', format_group_float_color_border)
                                sheet.write(rowx, 12, '', format_group_float_color_border)
                                rowx += 1
                            second_count += 1

                        if account_sum_currency_start == '':
                            account_sum_currency_start += 'H%s' % (salesman_rowx + 1)
                            account_sum_currency_debit += 'I%s' % (salesman_rowx + 1)
                            account_sum_currency_credit += 'J%s' % (salesman_rowx + 1)
                            account_sum_currency_end += 'K%s' % (salesman_rowx + 1)
                        else:

                            account_sum_currency_start += ',H%s' % (salesman_rowx + 1)
                            account_sum_currency_debit += ',I%s' % (salesman_rowx + 1)
                            account_sum_currency_credit += ',J%s' % (salesman_rowx + 1)
                            account_sum_currency_end += ',K%s' % (salesman_rowx + 1)

                        sheet.write_formula(salesman_rowx, 7, '{=SUM(' + salesman_sum_currency_start + ')}', format_group_float_border)
                        sheet.write_formula(salesman_rowx, 8, '{=SUM(' + salesman_sum_currency_debit + ')}', format_group_float_border)
                        sheet.write_formula(salesman_rowx, 9, '{=SUM(' + salesman_sum_currency_credit + ')}', format_group_float_border)
                        sheet.write_formula(salesman_rowx, 10, '{=SUM(' + salesman_sum_currency_end + ')}', format_group_float_border)
                        sheet.write(salesman_rowx, 11, '')
                        sheet.write(salesman_rowx, 12, '')
                        sheet.set_row(salesman_rowx, None, None, {'hidden': 0, 'level': 1})
                        count += 1
                    sheet.merge_range(rowx, 0, rowx, 6, _('[%s] %s TOTAL') % (account_object.code, account_object.name), format_group)
                    sheet.set_row(rowx, None, None, {'hidden': 0, 'level': 1})
                    sheet.write_formula(rowx, 7, '{=SUM(' + account_sum_currency_start + ')}', format_group_float_border)
                    sheet.write_formula(rowx, 8, '{=SUM(' + account_sum_currency_debit + ')}', format_group_float_border)
                    sheet.write_formula(rowx, 9, '{=SUM(' + account_sum_currency_credit + ')}', format_group_float_border)
                    sheet.write_formula(rowx, 10, '{=SUM(' + account_sum_currency_end + ')}', format_group_float_border)
                    sheet.write(rowx, 11, '', format_group_float_border)
                    sheet.write(rowx, 12, '', format_group_float_border)

                    if last_total_start == '':
                        last_total_start += 'H%s' % (rowx + 1)
                        last_total_debit += 'I%s' % (rowx + 1)
                        last_total_credit += 'J%s' % (rowx + 1)
                        last_total_end += 'K%s' % (rowx + 1)
                    else:
                        last_total_start += ',H%s' % (rowx + 1)
                        last_total_debit += ',I%s' % (rowx + 1)
                        last_total_credit += ',J%s' % (rowx + 1)
                        last_total_end += ',K%s' % (rowx + 1)
                    rowx += 1
                sheet.merge_range(rowx, 0, rowx, 6, u'TOTAL', format_group)
                sheet.set_row(rowx, None, None, {'hidden': 0, 'level': 1})
                sheet.write_formula(rowx, 7, '{=SUM(' + last_total_start + ')}', format_group_float_border)
                sheet.write_formula(rowx, 8, '{=SUM(' + last_total_debit + ')}', format_group_float_border)
                sheet.write_formula(rowx, 9, '{=SUM(' + last_total_credit + ')}', format_group_float_border)
                sheet.write_formula(rowx, 10, '{=SUM(' + last_total_end + ')}', format_group_float_border)
                sheet.write(rowx, 11, '', format_group_float_border)
                sheet.write(rowx, 12, '', format_group_float_border)

            else:
                last_start_total = last_debit_total = last_credit_total = last_end_total = 0
                for dict in partner_dict:
                    first_object = first_obj.with_context(active_test=False).search([('id', '=', dict)])
                    mobile = ' '
                    if self.group_by == 'partner':
                        if first_object.mobile and first_object.phone:
                            mobile += ' ' + first_object.mobile + ' ' + first_object.phone
                        elif first_object.mobile:
                            mobile += ' ' + first_object.mobile
                        elif first_object.phone:
                            mobile += ' ' + first_object.phone
                    sheet.merge_range(rowx, 0, rowx, 11, u'[%s] %s' % (first_object.ref if self.group_by == 'partner' else first_object.code, first_object.name + mobile), format_group_left)
                    if self.group_by == 'partner':
                        sheet.write(rowx, 12, first_object.credit_limit, format_group_left)
                    rowx += 1
                    for lines in partner_dict[dict]:
                        total_start = total_currency_start = total_debit = total_currency_debit = 0
                        total_credit = total_currency_credit = total_end = total_currency_end = 0
                        sheet.write(rowx, 0, count, format_content_center)
                        second_object = second_obj.with_context(active_test=False).search([('id', '=', lines)])
                        mobile = ' '
                        if self.group_by == 'partner':
                            sheet.merge_range(rowx, 1, rowx, 12, u'[%s] %s' % (
                                second_object.code if self.group_by == 'partner' else second_object.ref,
                                second_object.name + mobile), format_group_left)
                        else:
                            if second_object.mobile and second_object.phone:
                                mobile += ' ' + second_object.mobile + ' ' + second_object.phone
                            elif second_object.mobile:
                                mobile += ' ' + second_object.mobile
                            elif second_object.phone:
                                mobile += ' ' + second_object.phone
                            sheet.merge_range(rowx, 1, rowx, 11, u'[%s] %s' % (
                                second_object.code if self.group_by == 'partner' else second_object.ref,
                                second_object.name + mobile), format_group_left)
                        sheet.set_row(rowx, None, None, {'hidden': 0, 'level': 1})
                        rowx += 1
                        second_count = 1
                        first = 0
                        starting_balance = cur_starting_balance = 0
                        start_balance = cur_start_balance = 0
                        not_start_balance = False
                        for key, value in sorted(partner_dict[dict][lines].iteritems()):
                            if value['start_balance'] == 0 and (value['debit'] != 0 or value['credit'] != 0):
                                not_start_balance = True
                        if not_start_balance:
                            for key, value in sorted(partner_dict[dict][lines].iteritems(), key=lambda x: x[1]['ml_date']):  # Dictionary эрэмбэлэх
                                if self.report_type == 'analytic_line':
                                    value['start_balance'] = value['start_balance'] * -1
                                if value['ml_date'] < self.date_from:
                                    starting_balance += value['start_balance']
                                    cur_starting_balance += value['cur_start_balance']
                                elif value['ml_date'] <= self.date_to:
                                    if first < 1:
                                        start_balance = starting_balance
                                        cur_start_balance = cur_starting_balance
                                        total_currency_start = cur_start_balance
                                        total_start = start_balance
                                        first += 1
                                    line_end = start_balance
                                    cur_line_end = cur_start_balance
                                    sheet.write(rowx, 0, u'%s.%s' % (count, second_count), format_content_center)
                                    text = u''
                                    number = value.get('ref') if value.get('ref') else ''
                                    if value['invoice']:
                                        invoice = self.env['account.invoice'].search([('id', '=', value['invoice'])])
                                        if invoice:
                                            text = invoice.origin if invoice.origin else u''
                                            if not number:
                                                number = invoice.number if invoice.number else u''
                                    sheet.write(rowx, 1, number, format_content_text)
                                    sheet.write(rowx, 2, text, format_content_text)
                                    sheet.write(rowx, 3, value['ml_date'], format_content_center)
                                    sheet.write(rowx, 4, value['name'], format_content_text)
                                    sheet.write(rowx, 5, company_currency.name, format_content_center)
                                    sheet.write(rowx, 6, u'1.0', format_content_float)
                                    sheet.write(rowx, 7, start_balance, format_content_float)
                                    sheet.set_row(rowx, None, None, {'hidden': 0, 'level': 2})

                                    if value['cid'] and value['cid'] != company_currency.id:
                                        sheet.write(rowx + 1, 7, cur_start_balance, format_content_float_color)
                                    total_debit += value['debit']
                                    total_credit += value['credit']
                                    total_currency_debit += value['cur_debit']
                                    total_currency_credit += value['cur_credit']
                                    start_balance += value['debit'] - value['credit']
                                    cur_start_balance += value['cur_debit'] - value['cur_credit']
                                    line_end += value['debit'] - value['credit']
                                    cur_line_end += value['cur_debit'] - value['cur_credit']

                                    total_end = line_end
                                    total_currency_end = cur_line_end

                                    sheet.write(rowx, 8, abs(value['debit']), format_content_float)
                                    sheet.write(rowx, 9, abs(value['credit']), format_content_float)
                                    sheet.write(rowx, 10, line_end, format_content_float)

                                    rec = self.env['account.full.reconcile'].search([('id', '=', value['rec'])])
                                    sheet.write(rowx, 11, rec.name if rec else '', format_content_center)
                                    if value['cid'] and value['cid'] != company_currency.id:
                                        rowx += 1
                                        sheet.merge_range(rowx, 0, rowx, 4, '')
                                        sheet.write(rowx, 5, self.env['res.currency'].search([('id', '=', value['cid'])]).name, format_content_center_color)
                                        sheet.write(rowx, 6, value['currency_rate'], format_content_float_color)
                                        sheet.write(rowx, 8, value['cur_debit'], format_content_float_color)
                                        sheet.write(rowx, 9, value['cur_credit'], format_content_float_color)
                                        sheet.write(rowx, 10, cur_line_end, format_content_float_color)
                                        sheet.set_row(rowx, None, None, {'hidden': 0, 'level': 2})
                                    rowx += 1
                                    second_count += 1
                        else:
                            starting_balance = 0
                            for key, value in sorted(partner_dict[dict][lines].iteritems()):
                                if value['ml_date'] < self.date_from:
                                    starting_balance += value['start_balance']
                            for key, value in sorted(partner_dict[dict][lines].iteritems()):
                                if value['ml_date'] <= self.date_to:
                                    total_debit = value['debit'] if value['debit'] else 0
                                    total_credit = value['credit'] if value['credit'] else 0
                                    total_start = value['start_balance'] if starting_balance == value['start_balance'] else starting_balance
                                    total_end += value['start_balance'] + total_debit - total_credit
                        sheet.merge_range(rowx, 0, rowx, 6, _('TOTAL'), format_group)
                        sheet.set_row(rowx, None, None, {'hidden': 0, 'level': 1})
                        sheet.write(rowx, 7, total_start, format_group_float_border)
                        sheet.write(rowx, 8, total_debit, format_group_float_border)
                        sheet.write(rowx, 9, total_credit, format_group_float_border)
                        sheet.write(rowx, 10, total_end, format_group_float_border)
                        rowx += 1
                        if total_currency_start > 0.0 or total_currency_debit > 0.0 or total_currency_credit > 0.0 or total_currency_end > 0.0:
                            sheet.merge_range(rowx, 0, rowx, 6, _('TOTAL (CURRENCY)'), format_group_color)
                            sheet.set_row(rowx, None, None, {'hidden': 0, 'level': 1})
                            sheet.write(rowx, 7, total_currency_start, format_group_float_color_border)
                            sheet.write(rowx, 8, total_currency_debit, format_group_float_color_border)
                            sheet.write(rowx, 9, total_currency_credit, format_group_float_color_border)
                            sheet.write(rowx, 10, total_currency_end, format_group_float_color_border)
                            rowx += 1
                        count += 1
                        last_start_total += total_start
                        last_credit_total += total_credit
                        last_debit_total += total_debit
                        last_end_total += total_end
                sheet.merge_range(rowx, 0, rowx, 6, _('TOTAL'), format_group)
                sheet.set_row(rowx, None, None, {'hidden': 0, 'level': 1})
                sheet.write(rowx, 7, last_start_total, format_group_float_border)
                sheet.write(rowx, 8, last_debit_total, format_group_float_border)
                sheet.write(rowx, 9, last_credit_total, format_group_float_border)
                sheet.write(rowx, 10, last_end_total, format_group_float_border)
        rowx += 2

        # Footer зурна
        sheet.merge_range(rowx, 1, rowx, 3, '%s: ........................................... (                          )' % _('Executive Accountant'), format_filter)
        rowx += 1
        book.close()

        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()

    @api.depends('is_employee')
    @api.onchange('is_employee')
    def onchange_is_employee(self):
        if self.is_employee:
            return {'domain': {'partner_id': [('employee', '=', True)]}}
        else:
            return {'domain': {'partner_id': []}}