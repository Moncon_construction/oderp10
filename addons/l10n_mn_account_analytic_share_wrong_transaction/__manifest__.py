# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Analytic Share Wrong Transaction ",
    'version': '1.0',
    'depends': ['l10n_mn_account_wrong_transaction'],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Analytic Modules',
    'description': """""",
    'website' : 'http://asterisk-tech.mn',
    'data': [
        'views/account_move_analytic_share_diff_view.xml',
        'views/account_move_line_analytic_share_view.xml',
        'wizard/account_analytic_line_fix_amount_view.xml',
    ],
}
