#-*- coding: utf-8 -*-
from odoo import fields, models, api, _

class AccountAnalyticLine(models.Model):
    _inherit = "account.analytic.line"


    @api.multi
    def get_account_move_analytic_share_diff(self):
        # Шинжилгээний бичилтийн шинжилгээний данс журналын мөрөөс өөр байвал шинжилгээний бичилтийг харуулах функц
        ids = []
        self.env.cr.execute("""
        SELECT aal.id, ROUND((aal.amount)::numeric,2)  , aal.name, aml.debit, aml.credit, aas.rate, (ROUND( (aas.rate *  (-1) * aml.debit/100 )::numeric,2)  ) as aal_debit, (ROUND( (aas.rate * aml.credit/100 )::numeric,2)   ) as aal_credit
          FROM account_analytic_line aal
          LEFT JOIN account_move_line as aml on aal.move_id = aml.id
          LEFT JOIN account_analytic_share aas on aas.analytic_line_id = aal.id
          WHERE ROUND( (aas.rate * aml.credit/100 )::numeric,2) <>  ROUND((aal.amount)::numeric,2) and aml.credit>0
         UNION ALL
         
         SELECT aal.id, ROUND((aal.amount)::numeric,2)  , aal.name, aml.debit, aml.credit, aas.rate, (ROUND( (aas.rate *  (-1) * aml.debit/100 )::numeric,2) ) as aal_debit, (ROUND( (aas.rate * aml.credit/100 )::numeric,2)   ) as aal_credit
          FROM account_analytic_line aal
          LEFT JOIN account_move_line as aml on aal.move_id = aml.id
          LEFT JOIN account_analytic_share aas on aas.analytic_line_id = aal.id
          WHERE  ROUND( (aas.rate *  (-1) * aml.debit/100 )::numeric,2)  <>  ROUND((aal.amount)::numeric,2) and aml.debit>0;
        """)

        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]

        tree_id = self.env.ref("l10n_mn_account_analytic_share_wrong_transaction.view_account_move_analytic_share_diff")
        return {
            'type': 'ir.actions.act_window',
            'name': _('Account move line and analytic share difference'),
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'account.analytic.line',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree')],
            'target': 'current',
        }

