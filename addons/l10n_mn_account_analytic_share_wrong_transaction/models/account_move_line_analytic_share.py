# -*- coding: utf-8 -*-
from odoo import fields, models, api, _


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    @api.multi
    def get_empty_journal_line_analytic_share_one(self):
        ids = []
        self.env.cr.execute("""
        SELECT analytic_share.move_id 
        FROM (
         SELECT acml.id AS move_id, count(share.id) AS share_count
         FROM account_move_line acml
         LEFT JOIN account_analytic_share share ON acml.id = share.move_line_id 
         LEFT JOIN account_account aa ON acml.account_id = aa.id 
         WHERE aa.req_analytic_account = 't'
         GROUP BY acml.id
        ) AS analytic_share  
        WHERE analytic_share.share_count = 0;
        """)

        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]

        tree_id = self.env.ref("account.view_move_line_tree")
        form_id = self.env.ref("account.view_move_line_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Empty Tree Number 1'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move.line',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }

    @api.multi
    def get_one_hundred_percent_diff(self):
        ids = []
        qry = """
        SELECT analytic_share.move_id, share_count, share_rate
        FROM (
         SELECT count(share.id) AS share_count, acml.id AS move_id, sum(share.rate) AS share_rate 
         FROM account_move_line acml 
         LEFT JOIN account_analytic_share share ON acml.id = share.move_line_id
         GROUP BY acml.id
        ) AS analytic_share 
        WHERE share_rate != 100
        """
        self._cr.execute(qry)
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]

        tree_id = self.env.ref("account.view_move_line_tree")
        form_id = self.env.ref("account.view_move_line_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Analytic share 1 was 100% different'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move.line',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }

    @api.multi
    def sum_1_and_2_was_one_hudred_percent_diff(self):
        ids = []
        qry = """
        SELECT analytic_share.move_id, share_count1, share_count2, share_rate
        FROM (
         SELECT count(share1.id) AS share_count1, count(share2.id) AS share_count2, acml.id AS move_id, sum(share1.rate + share2.rate) AS share_rate
         FROM account_move_line acml
         LEFT JOIN account_analytic_share share1 ON acml.id = share1.move_line_id
         LEFT JOIN account_analytic_share share2 ON acml.id = share2.move_line_id2
         GROUP BY acml.id
        ) AS analytic_share
        WHERE share_rate != 100
        """
        self._cr.execute(qry)
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]

        tree_id = self.env.ref("account.view_move_line_tree")
        form_id = self.env.ref("account.view_move_line_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Analytic share 1 was 100% different'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move.line',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }

    @api.multi
    def account_move_not_analytic_line(self):
        qry = """
        select
            coalesce(move_line_id, move_line_id2)
        from
            account_analytic_share
        where
            move_line_id is not null
            or move_line_id2 is not null
        """
        self._cr.execute(qry)
        fetched = self._cr.fetchall()
        fetched = [f[0] for f in fetched]
        fetched = list(set(fetched))
        ids = []
        for f in fetched:
            analytic_line = self.env['account.analytic.line'].search(
                [('move_id', '=', f)])
            if not analytic_line:
                ids.append(f)
        tree_id = self.env.ref("account.view_move_line_tree")
        form_id = self.env.ref("account.view_move_line_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Account move line not analytic line'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move.line',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }

    @api.model
    def create(self, vals):
        res = super(AccountMoveLine, self).create(vals)
        res.create_analytic_lines()
        return res
