# -*- coding: utf-8 -*-
from odoo import fields, api, models, _

class AccountAnalyticLineFixAmount(models.TransientModel):
    _name = 'account.analytic.line.fix.amount'
    _description = 'Account analytic line fix'


    @api.multi
    def fix_analytic_line_amount(self):
        # Шинжилгээний бичилтийн дүн шинжилгээний тархалтын хувьчилсан дүнгээс өөр байвал засах функц
        context = self._context or {}
        lines = self.env['account.analytic.line'].browse(context.get('active_ids', []))
        move_ids = []
        for line in lines:
            if line.move_id not in move_ids:
                move_ids.append(line.move_id)
        for move in move_ids:
            move.create_analytic_lines()
