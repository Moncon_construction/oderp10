# -*- coding: utf-8 -*-
{
    'name': "Mongolian Account Asset",
    'version': '1.0',
    'depends': [
        'account_asset',
        'l10n_mn_account_period',
        'l10n_mn_hr'
    ],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Account Asset Modules',
    'description': """
         Хөрөнгийн дэлгэцийн сайжруулалт, хөрөнгө капиталжуулах, дахин үнэлэх, батлах, хаах, борлуулах болон хөрөнгийн байрлал
         Хөрөнгийн огнооноос эхэлж мөн хөрөнгө хааж байгаа огнооноос шалтгаалан өдрөөр элэгдлийг тооцон байгуулах болон хаах
         Хөрөнгө бөөнөөр нь шилжүүлэх, бөөнөөр нь элэгдүүлэх
    """,
    'website': 'http://asterisk-tech.mn',
    'data': [
        'security/ir.model.access.csv',
        'views/res_config_view.xml',
        'wizard/account_asset_freeze_views.xml',
        'wizard/account_asset_validate.xml',
        'wizard/account_asset_asset_sell_view.xml',
        'wizard/account_asset_close_wizard_view.xml',
        'views/account_menu_view.xml',
        'views/account_asset_move_view.xml',
        'views/account_asset_view.xml',
        'views/account_asset_location_view.xml',
        'views/account_asset_move_sequence.xml',
        'views/account_invoice_view.xml',
        'views/hr_employee_view.xml',
        'views/supply_asset_views.xml',
        'wizard/asset_modify_views.xml',
        'wizard/account_asset_depreciation_cancel_view.xml',
        'wizard/account_depreciation_confirmation_wizard_views.xml',
        'data/email_template.xml',
        'views/account_view.xml',
        'report/account_asset_templates.xml',
        'report/print_account_asset_moves.xml',
        'report/print_account_asset_close.xml',
        'report/account_asset_QR.xml',
        'security/ir_rule.xml',
        'report/print_account_asset_sale.xml',
        'report/account_asset_capital_report.xml',
    ]
}
