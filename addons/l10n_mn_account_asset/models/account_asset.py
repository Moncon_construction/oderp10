# -*- coding: utf-8 -*-
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
from calendar import monthrange
from odoo import api, fields, models, tools, _
from odoo.tools import float_compare, float_is_zero
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from odoo.exceptions import ValidationError, UserError
from odoo.osv import expression
from odoo.osv.orm import setup_modifiers
from odoo import exceptions
from lxml import etree
import logging
_logger = logging.getLogger(__name__)

class Currency(models.Model):
    _inherit = 'res.currency'

    @api.multi
    def compute_currency(self, from_currency_id, to_currency_id, from_amount, round=True, context=None):
        #         8-дээрх функцыг нэг аргумент нэмж авахаар байгуулав
        context = context or {}
        if not from_currency_id:
            from_currency_id = to_currency_id
        if not to_currency_id:
            to_currency_id = from_currency_id
        xc = self.browse([from_currency_id, to_currency_id])
        from_currency = (xc[0].id == from_currency_id and xc[0]) or xc[1]
        to_currency = (xc[0].id == to_currency_id and xc[0]) or xc[1]
        return self._compute(from_currency, to_currency, from_amount, round)


class AccountAssetAssetImage(models.Model):
    _name = 'account.asset.image'
    _order = 'sequence, id DESC'

    name = fields.Char('Name')
    description = fields.Text('Description')
    sequence = fields.Integer('Sequence')
    image_alt = fields.Text('Image Label')
    image = fields.Binary('Image')
    image_small = fields.Binary('Small Image')
    asset_id = fields.Many2one('account.asset.asset', 'Asset')
    from_main_image = fields.Boolean('From Main Image', default=False)


class AccountAssetHistory(models.Model):
    _name = 'account.asset.history'
    _description = 'Asset history'
    _order = 'date desc'

    name = fields.Char('History name')
    move_id = fields.Many2one('account.move', 'Account move')
    user_id = fields.Many2one('res.users', 'User', default=lambda self: self.env.uid)
    date = fields.Date('Date')
    asset_id = fields.Many2one('account.asset.asset', 'Asset')
    action = fields.Selection([('capital', 'Capitalization'),
                               ('revaluation', 'Revaluation'),
                               ('close', 'Close'),
                               ('sale', 'Sale')], 'Action', default='capital')
    old_method_number = fields.Integer('Old Number of Depreciations')
    old_method_period = fields.Integer('Old Period Length')
    old_method_end = fields.Date('Old Ending Date')
    old_value = fields.Float('Old Value')
    method_time = fields.Selection([('number', 'Number of Depreciations'), ('number_day', 'Day of Depreciations'), ('end', 'Ending Date')], 'Time Method', required=True,
                                   help="The method to use to compute the dates and number of depreciation lines.\n"
                                   "Number of Depreciations: Fix the number of depreciation lines and the time between 2 depreciations.\n"
                                   "Ending Date: Choose the time between 2 depreciations and the date the depreciations won't go beyond.")
    method_number = fields.Integer('Number of Depreciations', help="The number of depreciations needed to depreciate your asset")
    method_period = fields.Integer('Period Length', help="Time in month between two depreciations")
    method_end = fields.Date('Ending Date')
    amount = fields.Float('Amount Increase')
    capital_invoice_line_id = fields.Many2one('account.invoice.line', 'Capital Account Invoice Line')
    company_id = fields.Many2one(related="asset_id.company_id", string="Company", store=True)

    @api.multi
    def unlink(self):
        for his in self:
            closed = self._context.get('closed', False)
            history_ids = self.env['account.asset.history'].search([('asset_id', '=', his.asset_id.id)])
            asset = his.asset_id
            for history in history_ids:
                if history.id == his.id:
                    continue
                if history.date > his.date:
                    raise UserError(_("Cannot delete %s history, only delete the most recent history." % his.name))
                elif history.date == his.date:
                    if history.create_date > his.create_date:
                        raise UserError(_("Cannot delete %s history, only delete the most recent history." % his.name))
            if not closed and his.action in ('close', 'sale'):
                raise UserError(_("Cannot delete %s history of the sale, closed." % his.name))
            elif his.action in ('capital', 'revaluation'):
                if his.move_id:
                    for m_line in his.move_id.line_ids:
                        m_line.asset_id = False
                    if his.move_id.state == 'posted':
                        his.move_id.button_cancel()
                    his.move_id.unlink()
                value = asset.value
                asset.write({'method_number': his.old_method_number,
                             'method_period': his.old_method_period,
                             'method_end': his.old_method_end,
                             'value': value - his.amount
                             })
                asset.message_post(body=_("Deleted history. Value: %s -- %s") % (value, asset.value))
        return super(AccountAssetHistory, self).unlink()


class AccountAssetCategory(models.Model):
    _inherit = 'account.asset.category'
    _parent_name = "parent_id"
    _parent_store = True
    _parent_order = 'name'
    _order = 'parent_left'

    def get_method(self):
        res = [('linear', _('Linear')),
               ('linear_day', _('Day Linear')),
               ('degressive', _('Degressive'))]
        if self.env.user.company_id and self.env.user.company_id.use_operation_method == True:
            res.append(('operation', _('Operation')))
        return res

    account_depreciation_expense_id = fields.Many2one('account.account', string='Depreciation Entries: Expense Account', required=True, domain=[('internal_type', 'in', ['other', 'expense']), ('deprecated', '=', False)], oldname='account_income_recognition_id', help="Account used in the periodical entries, to record a part of the asset as expense.")
    parent_id = fields.Many2one('account.asset.category', 'Parent Category', index=True, ondelete='cascade', domain=[('categ_type', '=', 'view')])
    child_id = fields.One2many('account.asset.category', 'parent_id', 'Child Categories')
    parent_left = fields.Integer('Left Parent', index=1)
    parent_right = fields.Integer('Right Parent', index=1)
    categ_type = fields.Selection([('view', 'View'), ('normal', 'Normal')], string='Category Type', default='normal', help="A category of the view type is a virtual category that can be used as the parent of another category to create a hierarchical structure.")
    method = fields.Selection(get_method, string='Computation Method', default='linear', required=True,
                              help="Choose the method to use to compute the amount of depreciation lines.\n"
                                   "  * Linear: Calculated on basis of: Gross Value / Number of Depreciations\n"
                                   "  * Degressive: Calculated on basis of: Residual Value * Degressive Factor")
    method_time = fields.Selection(selection_add=[('number_day', _('Number day'))])

    @api.constrains('parent_id')
    def _check_parent_id(self):
        if not self._check_recursion():
            raise ValidationError(_('Error ! You cannot create recursive categories.'))

    @api.constrains('name')
    def _check_name(self):
        for category in self:
            if category.name:
                categories = self.env['account.asset.category'].search([
                    ('name', '=', category.name),
                    ('id', '!=', category.id),
                    ('company_id', '=', category.company_id.id)])
            for categ in categories:
                if categ:
                    exception = _('Category_id name duplicated: ') + categ.name
                    raise exceptions.ValidationError(exception)

    @api.one
    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        default = dict(default or {})
        default.setdefault('name', _("%s (copy)") % (self.name or ''))
        return super(AccountAssetCategory, self).copy(default)

    @api.multi
    def name_get(self):
        def get_names(cat):
            """ Return the list [cat.name, cat.parent_id.name, ...] """
            res = []
            while cat:
                res.append(cat.name)
                cat = cat.parent_id
            return res

        return [(cat.id, " / ".join(reversed(get_names(cat)))) for cat in self]

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        if not args:
            args = []
        if name:
            # Be sure name_search is symetric to name_get
            category_names = name.split(' / ')
            parents = list(category_names)
            child = parents.pop()
            domain = [('name', operator, child)]
            if parents:
                names_ids = self.name_search(' / '.join(parents), args=args, operator='ilike', limit=limit)
                category_ids = [name_id[0] for name_id in names_ids]
                if operator in expression.NEGATIVE_TERM_OPERATORS:
                    categories = self.search([('id', 'not in', category_ids)])
                    domain = expression.OR([[('parent_id', 'in', categories.ids)], domain])
                else:
                    domain = expression.AND([[('parent_id', 'in', category_ids)], domain])
                for i in range(1, len(category_names)):
                    domain = [[('name', operator, ' / '.join(category_names[-1 - i:]))], domain]
                    if operator in expression.NEGATIVE_TERM_OPERATORS:
                        domain = expression.AND(domain)
                    else:
                        domain = expression.OR(domain)
            categories = self.search(expression.AND([domain, args]), limit=limit)
        else:
            categories = self.search(args, limit=limit)
        return categories.name_get()
    
    @api.onchange('method')
    def onchange_method(self):
        # Хөрөнгө дээр Тооцоолох аргыг Шулуун шугамын арга (өдрөөр) арга сонгоход  Мөчлөг дэх сарын тоо талбарыг 1 болгоно
        if self.method:
            if self.method == 'linear_day':
                self.method_period = 1

class AccountAssetAsset(models.Model):
    _inherit = 'account.asset.asset'

    @api.one
    @api.depends('value', 'salvage_value', 'depreciation_line_ids.move_check', 'depreciation_line_ids.amount', 'history_ids.amount')
    def _amount_residual(self):
        total_amount = 0.0
        summary = 0.0
        for line in self.depreciation_line_ids:
            if line.move_check:
                total_amount += line.amount
        if self.history_ids:
            capital_ids = self.env['account.asset.history'].search([('action', '=', 'capital'), ('asset_id', '=', self.id)])
            for capital in capital_ids:
                summary += capital.amount
            self.capital_value = summary
        if self.method == 'linear_day':
            self.value_residual = self.value - total_amount
        else:
            self.value_residual = self.value - total_amount - self.salvage_value
        self.value_depreciated = total_amount

    @api.one
    @api.depends('method', 'depreciation_line_ids.move_check', 'depreciation_line_ids.unit_performance')
    def _amount_performance(self):
        total_performance = performance = 0.0
        if self.method == 'operation':
            for line in self.depreciation_line_ids:
                if line.move_check:
                    total_performance += line.unit_performance
                performance += line.unit_performance
        self.working_performance = total_performance
        
    # Элэгдэх өдрийн тоог Хугацааны аргын суурь, Элэгдэх тооноос хамааран тооцно  
    @api.depends('method_number','method_time','method_end')
    def _compute_number_day(self):
        for asset in self:
            start_date = datetime.strptime(asset.date, '%Y-%m-%d')
            if asset.method_time == 'number_day':
                tt_dayss = asset.method_number
                end_date = date(start_date.year, start_date.month, start_date.day)+ relativedelta(days=self.method_number)
            elif asset.method_time == 'number':
                end_date = start_date + relativedelta(months=asset.method_number)
                tt_dayss = (end_date - start_date).days
            else:
                if asset.method_end:
                    end_date = datetime.strptime(asset.method_end, '%Y-%m-%d')
                    tt_dayss = (end_date - start_date).days
            asset.method_number_day = tt_dayss
            asset.method_end = end_date           
                    
    # Элэгдэх өдрийн үлдсэн тоог тооцно
    @api.depends('method_number_day')
    def _compute_number_day_residual(self):
        for obj in self:
            days = 0.0
            if obj.method == 'linear_day':
                for line in obj.depreciation_line_ids:
                    if line.move_check:
                        days += line.period_day
                obj.method_number_day_residual = self.method_number_day - days

    @api.multi
    @api.depends('name', 'category_id', 'code', 'date', 'user_id', 'value')
    def _compute_qr_fields(self):
        for line in self:
            qr_data = '{\n'
            date_time_obj = datetime.strptime(line.purchase_date, '%Y-%m-%d').date()
            month = line.method_number * line.method_period
            stop_date = date_time_obj + relativedelta(months=month)
            qr_data += '"id": ' + format(line.id) + ',\n'
            qr_data += '"name": "'
            if line.code:
                qr_data += '[' + format(line.code.replace('"', '')) + '] '
            if line.name:
                qr_data += format(line.name.replace('"', ''))
            qr_data += '",\n'
            qr_data += '"duration": "' + format(line.purchase_date) + ' ~ ' + format(stop_date) + '"'
            if line.location_id:
                qr_data += ',\n"location": "' + format(line.location_id.name.replace('"', '')) + '"'
            if line.user_id:
                qr_data += ',\n"owner": "' + format(line.user_id.name.replace('"', '')) + '"\n}'
            else:
                qr_data += '\n}'
            line.qr_code = qr_data

    def get_method(self):
        res = [('linear', _('Linear')),
               ('linear_day', _('Day Linear')),
               ('degressive', _('Degressive')),
               ('sum', _('Sum of Year'))]
        if self.env.user.company_id and self.env.user.company_id.use_operation_method == True:
            res.append(('operation', _('Operation')))
        return res

    @api.constrains('code')
    def _check_code(self):
        for asset in self:
            if asset.code:
                assets = self.search([ ('code', '=', asset.code),('id', '!=', asset.id), ('company_id', '=', asset.company_id.id)])
                if len(assets) > 0:
                    raise exceptions.ValidationError(_('Account asset code duplicated: %s') % asset.code)

    location_id = fields.Many2one('account.asset.location', string='Asset Location')
    in_pawn = fields.Boolean(string="In pawn")
    bank_name_id = fields.Many2one('res.bank', string="Bank Name")
    # image: all image fields are base64 encoded and PIL-supported
    image = fields.Binary("Photo", attachment=True,
                          help="This field holds the image used as photo for the asset, limited to 1024x1024px.")
    image_medium = fields.Binary("Medium-sized photo", attachment=True,
                                 help="Medium-sized photo of the asset. It is automatically "
                                 "resized as a 128x128px image, with aspect ratio preserved. "
                                 "Use this field in form views or some kanban views.")
    image_small = fields.Binary("Small-sized photo", attachment=True,
                                help="Small-sized photo of the asset. It is automatically "
                                "resized as a 64x64px image, with aspect ratio preserved. "
                                "Use this field anywhere a small image is required.")
    user_id = fields.Many2one('hr.employee', 'Owner')
    value_depreciated = fields.Float(compute='_amount_residual', method=True, digits=0, string='Depreciated Value', store=True)
    note = fields.Text('Note')
    account_move_line_ids = fields.One2many('account.move.line', 'asset_id', 'Entries', readonly=True)
    repair_date = fields.Date('Repair date')
    initial_depreciation = fields.Float('Initial Depreciated Value',
                                        help='At the time launching erp system you can set already depreciated amount of the previous financial period.')
    purchase_date = fields.Date(string='Purchase Date', default=fields.Date.context_today)
    closed_date = fields.Date(string='Closed Date')
    images = fields.One2many('account.asset.image', 'asset_id', string='Images')
    invoice_line_id = fields.Many2one('account.invoice.line', string='Account Invoice')
    account_analytic_id = fields.Many2one('account.analytic.account', string='Analytic account', store=True)
    account_asset_id = fields.Many2one(related='category_id.account_asset_id', string='Asset Account')
    account_depreciation_id = fields.Many2one(related='category_id.account_depreciation_id', string='Depreciation Account')
    account_depreciation_expense_id = fields.Many2one(related='category_id.account_depreciation_expense_id', string='Depr. Expense Account')
    journal_id = fields.Many2one(related='category_id.journal_id', string='Journal')
    capital_value = fields.Float(compute='_amount_residual', method=True, digits=0, string='Capital Value')
    history_ids = fields.One2many('account.asset.history', 'asset_id', 'History')
    history_count = fields.Integer(compute='_history_count', string='# Asset Histories')
    invoice_count = fields.Integer(compute='_invoice_count', string='# Asset Invoices')
    move_history_count = fields.Integer(compute='_move_history_count', string='Asset move histories')
    state = fields.Selection([('draft', 'Draft'),
                              ('open', 'Using'),
                              ('close', 'Closed')], 'Status', required=True, default='draft', track_visibility='onchange', help="When an asset is created, the status is 'Draft'.\n"
                             "If the asset is confirmed, the status goes in 'Running' and the depreciation lines can be posted in the accounting.\n"
                             "You can manually close an asset when the depreciation is over. If the last line of depreciation is posted, the asset automatically goes in that status.")
    product_id = fields.Many2one('product.product', 'Product', help='It will uses when sale an asset.')
    certificate_number = fields.Char('Certificate Number')
    qr_code = fields.Char(compute='_compute_qr_fields', string="QR", store="True")
    method = fields.Selection(get_method, string='Computation Method', default='linear', required=True,
                              readonly=True, states={'draft': [('readonly', False)]},
                              help="Choose the method to use to compute the amount of depreciation lines.\n"
                                   "  * Linear: Calculated on basis of: Gross Value / Number of Depreciations\n"
                                   "  * Degressive: Calculated on basis of: Residual Value * Degressive Factor")
    depreciation_sum_ids = fields.One2many('account.asset.depreciation.line.sum', 'asset_id', 'Depreciation Sum')
    type_of_close = fields.Selection([('sell', 'To sell'),
                                      ('scrap', 'To scrap')], string="type_of_close", default="sell")
    working_performance = fields.Integer(compute='_amount_performance', string='Working Capacity Performance (unit)')
    working_capacity = fields.Integer(string='Working Capacity (unit)', readonly=True, states={'draft': [('readonly', False)]})
    uom_id = fields.Many2one('product.uom', string='Unit of Measure', readonly=True, states={'draft': [('readonly', False)]})
    is_supply_asset = fields.Boolean(default=False, string='Is Supply Asset or Not')
    method_time = fields.Selection(selection_add=[('number_day', _('Number day'))])
    method_number_day = fields.Integer(compute='_compute_number_day', string='Method Days') #Нийт элэгдэх тоог олно
    method_number_day_residual = fields.Integer(compute='_compute_number_day_residual', string='Method Days')

    @api.onchange('value')
    def onchange_depreciation(self):
        if self.value:
            self.depreciation_sum_ids.create({
                'asset_id': self.id,
            })

    @api.onchange('category_id')
    def onchange_category(self):
        # Хөрөнгийн ангилал дээр шинжилгээний данс тохируулсан байгаад хөрөнгийн байрлал дээр тохируулаагүй бол ангилал дээрх дансыг хөрөнгө дээр default-р олгох
        if not (self.location_id and self.location_id.account_analytic_id):
            if self.category_id.account_analytic_id:
                self.account_analytic_id = self.category_id.account_analytic_id.id
            else:
                self.account_analytic_id = False
        # Хөрөнгийн ангиллын Хугацааны Аргын Суурь болон Мөчлөг дахь сарын тоог авна.
        if self.category_id:
            self.update({
                'method_period': self.category_id.method_period,
                'method_time': self.category_id.method_time
            })

    @api.onchange('account_analytic_id')
    def onchange_analytic(self):
        # Шинжилгээний данс өөрчлөгдөхөд журналын бичилтийн данс дагаж өөрчлөгдөнө.
        if self.account_analytic_id:
            for line in self.depreciation_line_ids:
                if line.move_id:
                    move_line_obj = self.env['account.move.line'].search([('move_id', '=', line.move_id.id),
                                                                          ('debit', '!=', '0')], limit=1)
                    if move_line_obj.analytic_account_id.id != self.account_analytic_id.id:
                        move_line_obj.write({'analytic_account_id': self.account_analytic_id.id})
                        
    @api.onchange('method')
    def onchange_method(self):
        # Хөрөнгийн ангилал дээр Тооцоолох аргыг Шулуун шугамын арга (өдрөөр) арга сонгоход  Мөчлөг дэх сарын тоо талбарыг 1 болгоно
        if self.method:
            if self.method == 'linear_day':
                self.method_period = 1

    @api.multi
    def name_get(self):
        return [(line.id, '%s%s' % (line.id and '%s ' % line.code or '', line.name)) if line.code else (
            line.id, '%s' % (line.id and '%s ' % line.name)) for line in self]

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        args = args or []
        domain = ['|', ('code', operator, name), ('name', operator, name)]
        recs = self.search(domain + args, limit=limit)
        return recs.name_get()

    @api.one
    def action_copy_image_to_images(self):
        if not self.image:
            return
        image = None
        for r in self.images:
            if r.from_main_image:
                image = r
                break
        if image:
            image.image = self.image
        else:
            vals = {'image': self.image,
                    'name': self.name,
                    'product_tmpl_id': self.id,
                    'from_main_image': True, }
            self.env['product.image'].create(vals)

    def compute_sum_amount(self, sequence, undone):
        total = 0
        i = 0
        for counter in range(undone):
            i += 1
            total += i
        amount = 0
        total_val = self.value
        if self.salvage_value:
            total_val = total_val - self.salvage_value
        if sequence < undone:
            amount = total_val * (undone - sequence) / total
        elif sequence == undone:
            amount = total_val * undone / total
        return amount

    def _compute_board_amount(self, sequence, residual_amount, amount_to_depr, undone_dotation_number, posted_depreciation_line_ids, total_days, depreciation_date, lines=[]):
        # Элэгдлийн самбарын мөрийн үнийн дүнг тооцоолох нийт дүн, үлдэгдэл, элэгдэх тооноос хамааран тооцоолох
        amount = 0
        # Элэгдлийн сүүлийн мөр бол үлдэгдэл дүнг шууд олгох
        if sequence == lines:
            if self.method == 'sum':
                amount = self.compute_sum_amount(sequence - 1, undone_dotation_number)
            elif self.method == 'linear_day':
                amount = residual_amount - self.salvage_value
            else:
                amount = residual_amount
        else:
            if self.method == 'linear':
                if self.prorata:
                    amount = amount_to_depr / self.method_number
                    if sequence == 1:
                        if self.method_period % 12 != 0:
                            asset_date = datetime.strptime(self.date, '%Y-%m-%d')
                            month_days = monthrange(asset_date.year, asset_date.month)[1]
                            days = month_days - asset_date.day + 1
                            amount = (amount_to_depr / self.method_number) / month_days * days
                        else:
                            days = (self.company_id.compute_fiscalyear_dates(depreciation_date)['date_to'] - depreciation_date).days + 1
                            amount = (amount_to_depr / self.method_number) / total_days * days
                else:
                    if len(posted_depreciation_line_ids) > 0:
                        amount = amount_to_depr / (undone_dotation_number - len(posted_depreciation_line_ids))
                    else:
                        amount = amount_to_depr / undone_dotation_number
            elif self.method == 'degressive':
                amount = residual_amount * self.method_progress_factor
                if self.prorata:
                    if sequence == 1:
                        if self.method_period % 12 != 0:
                            asset_date = datetime.strptime(self.date, '%Y-%m-%d')
                            month_days = monthrange(asset_date.year, asset_date.month)[1]
                            days = month_days - asset_date.day + 1
                            amount = (residual_amount * self.method_progress_factor) / month_days * days
                        else:
                            days = (self.company_id.compute_fiscalyear_dates(depreciation_date)['date_to'] - depreciation_date).days + 1
                            amount = (residual_amount * self.method_progress_factor) / total_days * days
            elif self.method == 'sum':
                amount = self.compute_sum_amount(sequence - 1, undone_dotation_number)
            # Тооцоолох арга нь Шулуун шугамын арга (өдрөөр) байх үед элэгдлийн дүнг тооцож байна.
            elif self.method == 'linear_day':
                if sequence != 0:
                    if self.initial_depreciation > 0 and self.value_depreciated > 0:
                        total_dep_amount = self.value - self.initial_depreciation - self.salvage_value - (self.value_depreciated - self.initial_depreciation)
                    else:
                        total_dep_amount = self.value - self.initial_depreciation - self.salvage_value
                    end_depreciation_date = depreciation_date
                    start_depreciation_date = date(end_depreciation_date.year, end_depreciation_date.month, end_depreciation_date.day) + relativedelta(months=-self.method_period+1)
                    days = 0
                    while start_depreciation_date <= end_depreciation_date:
                        month_days = monthrange(start_depreciation_date.year, start_depreciation_date.month)[1]
                        start_depreciation_date = date(start_depreciation_date.year, start_depreciation_date.month, start_depreciation_date.day) + relativedelta(months=+1)
                        days += month_days
                    if self.method_number_day_residual > 0:
                        amount = (total_dep_amount / self.method_number_day_residual) * days
        return amount
    # Тооцоолох арга нь өдрөөр элэгдүүлэх байхад Хугацааны аргын сууриам хамаарч элэгдлийн мөрийн тоог тооцно
    def _compute_board_undone_dotation_nb(self, depreciation_date, total_days):
        undone_dotation_number = super(AccountAssetAsset, self)._compute_board_undone_dotation_nb(depreciation_date, total_days)
        if self.method == 'linear_day':
            if self.method_number == 0 and self.method_time != 'end':
                undone_dotation_number = self.method_number
            else:
                start_date = datetime.strptime(self.date, DF).date()
                if self.method_end:
                    end_date = datetime.strptime(self.method_end, DF).date()
                    undone_dotation_number = 0
                    while start_date <= end_date:
                        start_date = date(start_date.year, start_date.month, start_date.day) + relativedelta(months=+self.method_period)
                        undone_dotation_number += 1
        return undone_dotation_number

    @api.multi
    def last_day_of_month(self, date):
        # Тухайн огноонооны сарын сүүлчийн өдрийн огноог буцаах
        next_month = date.replace(day=28) + timedelta(days=4)  # this will never fail
        return next_month - timedelta(days=next_month.day)

    @api.multi
    def compute_depreciation_board(self):
        # Элэгдлийн самбарыг тооцоолохдоо хөрөнгийн огноогоор тооцоолон өдрөөр элэгдэл бодож самбар байгуулах
        self.ensure_one()
        move = False
        unit_amount = 0
        posted_depreciation_line_ids = self.depreciation_line_ids.filtered(lambda x: x.move_check or x.is_freeze).sorted(key=lambda l: l.depreciation_date)
        unposted_depreciation_line_ids = self.depreciation_line_ids.filtered(lambda x: not x.move_check and not x.is_freeze)
        freezed_depreciation_line_ids = self.depreciation_line_ids.filtered(lambda x: x.is_freeze)
        freeze = len(freezed_depreciation_line_ids)
        # Remove old unposted depreciation lines. We cannot use unlink() with One2many field
        if unposted_depreciation_line_ids:
            self._cr.execute("DELETE FROM account_asset_depreciation_line where id in (" + ",".join(map(str, unposted_depreciation_line_ids.ids)) + ") ")
        commands = []
        if self.value_residual != 0.0:
            amount_to_depr = residual_amount = self.value_residual
            if self.prorata:
                # if we already have some previous validated entries, starting date is last entry + method perio
                if posted_depreciation_line_ids and posted_depreciation_line_ids[-1].depreciation_date:
                    last_depreciation_date = datetime.strptime(posted_depreciation_line_ids[-1].depreciation_date, DF).date()
                    depreciation_date = self.last_day_of_month(last_depreciation_date)
                    if depreciation_date == last_depreciation_date:
                        depreciation_date = last_depreciation_date + relativedelta(months=+self.method_period)
                else:
                    depreciation_date = datetime.strptime(self._get_last_depreciation_date()[self.id], DF).date()
            else:
                # depreciation_date = 1st of January of purchase year if annual valuation, 1st of
                # purchase month in other cases
                if self.method_period >= 12:
                    asset_date = datetime.strptime(self.date[:4] + '-01-01', DF).date()
                else:
                    asset_date = datetime.strptime(self.date[:7] + '-01', DF).date()
                # if we already have some previous validated entries, starting date isn't 1st January but last entry + method period
                if posted_depreciation_line_ids and posted_depreciation_line_ids[-1].depreciation_date:
                    last_depreciation_date = datetime.strptime(posted_depreciation_line_ids[-1].depreciation_date, DF).date()
                    depreciation_date = self.last_day_of_month(last_depreciation_date)
                    if depreciation_date == last_depreciation_date:
                        depreciation_date = last_depreciation_date + relativedelta(months=+self.method_period)
                else:
                    depreciation_date = asset_date
            # Эхлэлийн элэгдлийн дугаарлалтаас эхлэхгүй, аль хэдийнэ compute_initial_depreciation функцээр үүсгэгдсэн нөгөөтэйгүүр батлагдсан бичилтүүдийг тооны дараагийн тооноос эхлэх
            start = len(posted_depreciation_line_ids) + 1
            initial = 0
            # Эхний элэгдлийг шалган үүсгэх
            if len(posted_depreciation_line_ids) == 0 and self.initial_depreciation > 0:
                start = 0
                init_vals = self.compute_initial_depreciation()
                commands.append((0, False, init_vals))
            elif len(posted_depreciation_line_ids) > 0 and self.initial_depreciation > 0:
                start -= 1
                initial = 1
            else:
                # Эхлэлийн элэгдэл нь тухайн өдрөөс элэгдүүлэхгүй тохиолдолд сарын сүүлийн өдрөөс байх
                if not self.prorata:
                    depreciation_date = self.last_day_of_month(depreciation_date)
            day = depreciation_date.day
            month = depreciation_date.month
            year = depreciation_date.year
            total_days = (year % 4) and 365 or 366
            undone_dotation_number = self._compute_board_undone_dotation_nb(depreciation_date, total_days)
            undone_dotation_number += freeze
            # Нийт мөрийн тоог хадгалж аваад үлдэгдэлийг шууд утга онооход ашиглана
            lines = undone_dotation_number
            for sequence in range(start, undone_dotation_number + 1):
                # Эхний элэгдлийг шалгах
                if sequence != 0:
                    period_days = days = 0
                    # Хөрөнгө хааж байгаа үед хаалтын огнооноос хамаарч элэгдлийн бичилтийн дүнг шинэчлэх
                    if 'date' in self._context and 'value' in self._context and not move:
                        dates = datetime.strptime(self._context.get('date'), '%Y-%m-%d')
                        d_lines = self.depreciation_line_ids.filtered(lambda l: not l.move_check and l.depreciation_date < str(dates.date()))
                        if d_lines:
                            raise UserError(_('Validate depreciation lines before %s closing date!' % dates.date()))
                        amount = self._context.get('value')
                        if type(depreciation_date) is not type(dates):
                            dates = dates.date()
                        if depreciation_date >= dates:
                            depreciation_date = dates
                            amount_to_depr = residual_amount - amount
                            move = True
                            amount = self._compute_board_amount(sequence, residual_amount, amount_to_depr, undone_dotation_number, posted_depreciation_line_ids.ids, total_days, depreciation_date, lines)
                            lastday_of_month = int(monthrange(depreciation_date.year, depreciation_date.month)[1])
                            amount = amount / lastday_of_month * depreciation_date.day
                    # Элэгдлийн бичилтийн огноо болон хөрөнгийн огнооны өдрөөс хамааруулж өдрөөр элэгдүүлийн дүнг тооцоолох
                    elif sequence == 1 and not self.prorata and depreciation_date > datetime.strptime(self.date, DF).date() and datetime.strptime(self.date, "%Y-%m-%d").day != 1:
                        days = (depreciation_date - datetime.strptime(self.date, "%Y-%m-%d").date()).days
                        division = undone_dotation_number + 1 - start
                        monthly_depreciate = residual_amount / division
                        daily_depreciate = monthly_depreciate / depreciation_date.day
                        # Үйл ажиллагааны аргын үед элэгдлийн дүнг гараар тооцоолох тул элэгдлийн самбар дээр 0 гэж оруулав
                        if self.method == 'operation':
                            if self.working_capacity > 0:
                                unit_amount = (self.value - self.salvage_value) / self.working_capacity
                            amount = 0
                        else:
                            amount = daily_depreciate * days
                        if self.method == 'linear_day':
                            total_dep_amount = self.value - self.initial_depreciation - self.salvage_value
                            if self.method_number_day_residual > 0:
                                amount = (total_dep_amount / self.method_number_day_residual) * days
                        amount = self.currency_id.round(amount)
                        amount_to_depr = residual_amount - amount
                        undone_dotation_number -= 1
                    else:
                        # Сарын сүүл өдөр бол дараа сараас элэгдүүлдэг болгов
                        if sequence == 1 and not self.prorata and depreciation_date == datetime.strptime(self.date, DF).date():
                            depreciation_date = date(year, month, day) + relativedelta(months=+self.method_period)
                            depreciation_date = self.last_day_of_month(depreciation_date)
                            day = depreciation_date.day
                            month = depreciation_date.month
                            year = depreciation_date.year
                        # Үйл ажиллагааны аргын үед элэгдлийн дүнг гараар тооцоолох тул элэгдлийн самбар дээр 0 гэж оруулав
                        if self.method == 'operation':
                            if self.working_capacity > 0 and self.working_capacity > self.working_performance:
                                unit_amount = (self.value - self.salvage_value - self.value_depreciated) / (self.working_capacity - self.working_performance)
                            amount = 0
                        else:
                            amount = self._compute_board_amount(sequence, residual_amount, amount_to_depr, undone_dotation_number + initial, posted_depreciation_line_ids.ids, total_days, depreciation_date, lines)
                            amount = self.currency_id.round(amount)
                    if self.method != 'operation' and float_is_zero(amount, precision_rounding=self.currency_id.rounding):
                        continue
                    residual_amount -= amount
                    if residual_amount > 0 and residual_amount < 1:
                        amount = amount - residual_amount
                    if residual_amount < 0 and residual_amount > -1:
                        amount = amount + residual_amount
                    # Тооцоолох арга нь өдрөөр байх үед элэгдлийн мөрд тухайн элэгдэх мөчлөгийн нийт элэгдлийн өдрийг хадгалдаг болгосон
                    end_depreciation_date = depreciation_date
                    start_depreciation_date = date(end_depreciation_date.year, end_depreciation_date.month, end_depreciation_date.day) + relativedelta(months=-self.method_period+1)
                    if sequence == lines:
                        if self.method == 'linear_day':
                            start_date = datetime.strptime(self.method_end[:7] + '-01', DF).date()
                            end_date = datetime.strptime(self.method_end, DF).date()
                            days = (end_date - start_date).days + 1
                            depreciation_date = end_date
                    while start_depreciation_date <= end_depreciation_date:
                        month_days = monthrange(start_depreciation_date.year, start_depreciation_date.month)[1]
                        start_depreciation_date = date(start_depreciation_date.year, start_depreciation_date.month, start_depreciation_date.day) + relativedelta(months=+1)
                        period_days += month_days
                    if sequence == 1 or sequence == lines:
                        period_days = days
                    if self.method == 'linear_day':
                        depreciated_value = self.value - (residual_amount + amount)
                    else:
                        depreciated_value = (self.value - self.salvage_value) - (residual_amount + amount)
                    vals = {
                        'amount': amount,
                        'period_day': period_days,
                        'unit_amount': unit_amount,
                        'asset_id': self.id,
                        'sequence': sequence,
                        'name': (self.code or '') + '/' + str(sequence),
                        'remaining_value': abs(residual_amount),
                        'depreciated_value': depreciated_value,
                        'depreciation_date': depreciation_date.strftime(DF),
                    }
                    commands.append((0, False, vals))
                    # Considering Depr. Period as months
                    depreciation_date = date(year, month, day) + relativedelta(months=+self.method_period)
                    depreciation_date = self.last_day_of_month(depreciation_date)
                else:
                    # Эхний элэгдлийн дүн
                    amount = self.initial_depreciation
                    amount_to_depr = residual_amount - amount
                    residual_amount -= amount
                    entry_date = datetime.strptime(self._context.get('entry_date', self.date), DF).date()
                    last_date = self.last_day_of_month(entry_date)
                    if last_date > entry_date:
                        depreciation_date = last_date
                    else:
                        depreciation_date = date(year, month, day) + relativedelta(months=+self.method_period)
                        depreciation_date = self.last_day_of_month(depreciation_date)
                day = depreciation_date.day
                month = depreciation_date.month
                year = depreciation_date.year

        self.write({'depreciation_line_ids': commands})
        if move:
            line = self.depreciation_line_ids.search([('depreciation_date', '>=', self._context.get('date'))], order='depreciation_date ASC', limit=1)
            line.create_move()
        return True

    ## Борлуулалтын визардаас борлуулалт хийх өдөр хүртэлх
    ## элэгдлийг тооцдог функцийг OOP зарчмын дагуу өөрийх нь модель дээр шилжүүлэв
    @api.one
    def get_depr_residual(self, date_str):
        date = datetime.strptime(date_str, '%Y-%m-%d')
        depr_line = self.search_and_raise_line(date_str)
        # Сонгосон огноонд тохирох элэгдлийн самбарын мөрийг олж элэгдлийн дүнгээс нь нэг өдрийн элэгдлийг тооцоолж сонгосон өдөр хүртэлх өдрөөр үржүүлэн үлдэгдэл дүнгээс хасах
        if depr_line:
            dates = self.last_day_of_month(datetime.strptime(depr_line.depreciation_date, '%Y-%m-%d'))
            depreciate_value = depr_line.amount / dates.day * date.day
            depr_line.depreciation_date = date_str
            return self.value_residual - depreciate_value
        return 0

    @api.multi
    def compute_initial_depreciation(self, context=None):
        # Хэрэв элэгдлийн эхний дүн байвал тухайн дүнгээр эдэгдэл үүснэ
        if context is None:
            context = self._context or {}
        currency_obj = self.env.get('res.currency')
        unit_amount = 0
        unit_performance = 0
        for asset in self:
            entry_date = context.get('entry_date', asset.date)
            # year = datetime.strptime(entry_date, '%Y-%m-%d').year
            # Хөрөнгийн ангилал дах хөрөнгийн дансны currency-г авах
            current_currency = asset.currency_id.id
            depreciation_amount = currency_obj.compute_currency(current_currency, asset.company_id.currency_id.id, asset.initial_depreciation)
            asset_amount = currency_obj.compute_currency(current_currency, asset.company_id.currency_id.id, asset.value)
            if asset.method == 'operation':
                if asset.working_capacity > 0:
                    unit_amount = (asset.value - asset.salvage_value) / asset.working_capacity
                    if unit_amount > 0:
                        unit_performance = depreciation_amount / unit_amount
            vals = {
                'amount': depreciation_amount,
                'asset_id': asset.id,
                'sequence': 0,
                'name': u'%s өмнөх элэгдэл ' % asset.name,
                'remaining_value': asset_amount - depreciation_amount,
                'depreciated_value': 0,
                'depreciation_date': entry_date,
                'initial_depreciation_check': True,
                'unit_amount': unit_amount,
                'unit_performance': unit_performance
            }
        return vals

    @api.multi
    def check_depreciation_board(self, context=None):
        for asset in self:
            unposted_lines = self.env['account.asset.depreciation.line'].search([('asset_id', '=', asset.id), ('move_check', '=', False)], order='sequence')
            seq = False
            depreciated_value = remaining_value = amount = 0
            if unposted_lines:
                for line in unposted_lines:
                    if seq == False:
                        depreciated_value = asset.value_depreciated
                        remaining_value = asset.value_residual - line.amount
                        seq = True
                    else:
                        depreciated_value += amount
                        remaining_value -= line.amount
                    amount = line.amount
                    line.depreciated_value = depreciated_value
                    line.remaining_value = remaining_value
            return True

    # Хөрөнгийн батлахад элэгдэлтэй хөрөнгө бол журналын бичилт үүсгэх утга буцаах функц
    @api.multi
    def _get_initial_depreciation_move_vals(self, asset, depreciation_amount, current_currency, entry_date, context, year):
        line_ids = [(0, 0, {
            'name': asset.category_id.name,
            'ref': asset.name,
            'account_id': asset.account_depreciation_id.id,
            'debit': 0.0,
            'credit': depreciation_amount,
            'journal_id': asset.journal_id.id,
            'partner_id': asset.partner_id.id,
            'currency_id': False if asset.company_id.currency_id.id != current_currency else current_currency,
            'amount_currency': asset.company_id.currency_id.id != current_currency and -asset.initial_depreciation or 0.0,
            'date': entry_date,
            'asset_id': asset.id
        }), (0, 0, {
            'name': asset.category_id.name,
            'ref': asset.name,
            'account_id': context['src_account_id'],
            'debit': depreciation_amount,
            'credit': 0.0,
            'journal_id': asset.journal_id.id,
            'partner_id': asset.partner_id.id,
            'currency_id': False if asset.company_id.currency_id.id != current_currency else current_currency,
            'amount_currency': asset.company_id.currency_id.id != current_currency and asset.initial_depreciation or 0.0,
            'date': entry_date,
            'asset_id': asset.id
        })]
        move_vals = {
            'name': asset.journal_id.code + '/' + str(year) + '/' + asset.name,
            'date': entry_date,
            'ref': asset.name,
            'journal_id': asset.journal_id.id,
            'line_ids': line_ids
        }
        return move_vals

    @api.multi
    def validate(self, context=None):
        # Хөрөнгийг батлах үед хөрөнгийн нийт үнэ болон эхний элэгдэлийн дүнгээр журналын бичилт үүснэ
        if context is None:
            context = self._context or {}
        move_obj = self.env.get('account.move')
        currency_obj = self.env.get('res.currency')
        depreciation_line_obj = self.env.get('account.asset.depreciation.line')
        for asset in self:
            asset_amount = 0
            for line in asset.account_move_line_ids:
                if line.account_id.id == asset.account_asset_id.id:
                    asset_amount += (line.debit - line.credit)
            if not asset_amount and asset.value:
                if not context.get('src_account_id', False):
                    wizard_id = self.env.get('ir.model.data').get_object_reference('l10n_mn_account_asset', 'action_view_validate_account_asset_asset')[1]
                    result = self.env.get('ir.actions.act_window').read(wizard_id)
                    result['context'] = dict(self.context.items(), active_id=asset.id, active_ids=[asset.id])
                    return result
                # Журналын бичилт хийх
                entry_date = context.get('entry_date', asset.date)
                year = datetime.strptime(entry_date, '%Y-%m-%d').year
                # Хөрөнгийн ангилал дах хөрөнгийн дансны currency-г авах
                current_currency = asset.currency_id.id
                asset_amount = currency_obj.compute_currency(current_currency, asset.company_id.currency_id.id, asset.value)
                move_id = move_obj.create(self._get_validate_move_vals(asset, asset_amount, current_currency, entry_date, context, year))
                if asset.journal_id.auto_approve:
                    move_id.post()
                # Хэрэв элэгдлийн эхний дүн байвал тухайн дүнгээр дахин ажил гүйлгээ үүсгэнэ
                if asset.initial_depreciation:
                    depreciation_amount = currency_obj.compute_currency(current_currency, asset.company_id.currency_id.id, asset.initial_depreciation)
                    move_id = move_obj.create(self._get_initial_depreciation_move_vals(asset, depreciation_amount, current_currency, entry_date, context, year))
                    if asset.journal_id.auto_approve:
                        move_id.post()
                    dep_id = depreciation_line_obj.search([('asset_id', '=', asset.id), ('initial_depreciation_check', '=', True)], limit=1)
                    dep_id.write({'move_check': True,
                                  'move_id': move_id.id})
                if not asset.date:
                    asset.write({'date': entry_date})
            return self.write({'state': 'open'})

    # Хөрөнгө батлахад хөрөнгө орлогодож байгаа журналын бичилт үүсгэх утга буцаах функц
    @api.multi
    def _get_validate_move_vals(self, asset, asset_amount, current_currency, entry_date, context, year):
        line_ids = [(0, 0, {
            'name': asset.category_id.name,
            'ref': asset.name,
            'account_id': asset.account_asset_id.id,
            'debit': asset_amount,
            'credit': 0.0,
            'journal_id': asset.journal_id.id,
            'partner_id': asset.partner_id.id,
            'currency_id': current_currency,
            'amount_currency': asset.company_id.currency_id.id != current_currency and asset.value or 0.0,
            'date': entry_date,
            'asset_id': asset.id
        }), (0, 0, {
            'name': asset.category_id.name,
            'ref': asset.name,
            'account_id': context['src_account_id'],
            'debit': 0.0,
            'credit': asset_amount,
            'journal_id': asset.journal_id.id,
            'partner_id': asset.partner_id.id,
            'currency_id': current_currency,
            'amount_currency': asset.company_id.currency_id.id != current_currency and -asset.value or 0.0,
            'date': entry_date,
            'asset_id': asset.id
        })]
        move_vals = {
            'name': asset.journal_id.code + '/' + str(year) + '/' + asset.name,
            'date': entry_date,
            'ref': asset.code,
            'journal_id': asset.journal_id.id,
            'line_ids': line_ids
        }
        return move_vals

    @api.multi
    @api.depends('depreciation_line_ids.move_id')
    def _entry_count(self):
        # Хөрөнгөөс үүссэн журналын бичилтүүдийг тоолох
        for asset in self:
            move_ids = []
            move_lines = self.env['account.move.line'].search([('asset_id', '=', self.id)])
            for move_line in move_lines:
                if move_line.move_id:
                    move_ids.append(move_line.move_id.id)
            res = self.env['account.move'].search_count([('id', 'in', move_ids)])
            asset.entry_count = res or 0

    @api.multi
    def open_entries(self):
        # Хөрөнгөөс үүссэн журналын бичилтүүд рүү орох
        move_ids = []
        move_lines = self.env['account.move.line'].search([('asset_id', '=', self.id)])
        for move_line in move_lines:
            if move_line.move_id:
                move_ids.append(move_line.move_id.id)
        return {
            'name': _('Journal Entries'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', move_ids)],
        }

    @api.multi
    def open_invoice(self):
        # Хөрөнгө борлуулахад үүссэн нэхэмжлэх  рүү орох
        inv_ids = []
        inv_lines = self.env['account.invoice.line'].search([('asset_id', '=', self.id)])
        for line in inv_lines:
            if line.invoice_id:
                inv_ids.append(line.invoice_id.id)
        context = dict(self.env.context or {})
        context.update(create=False)
        return {
            'name': _('Invoice'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.invoice',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', inv_ids)],
            'context': context,
        }

    @api.multi
    def _compute_entries(self, date_start, date_end=None, group_entries=False):
        # Журналын бичилт үүсгэх элэгдэлийн мөрүүдийг цуглуулж нэг нэгээр нь бичилт үүсгэх
        if date_end:
            depreciation_ids = self.env['account.asset.depreciation.line'].search([('asset_id', 'in', self.ids),
                                                                                   ('depreciation_date', '>=', date_start),
                                                                                   ('depreciation_date', '<=', date_end),
                                                                                   ('move_check', '=', False)])
        else:
            depreciation_ids = self.env['account.asset.depreciation.line'].search([('asset_id', 'in', self.ids),
                                                                                   ('depreciation_date', '<=', date_start),
                                                                                   ('move_check', '=', False)])
        if group_entries:
            res = depreciation_ids.create_grouped_move()
        res = depreciation_ids.create_move()
        return res

    @api.multi
    def search_and_raise_line(self, date):
        # Хаалтын огноонд тохирох сарын элэгдлийн мөр батлагдаагүй байх хэрэгтэй учир нь хөрөнгө хаах үед батлагдана
        line = []
        if date and self and self.depreciation_line_ids:
            date = datetime.strptime(date, '%Y-%m-%d')
            month = date.month
            line = self.depreciation_line_ids.search([('depreciation_date', '>=', date), ('asset_id', '=', self.id)],
                                                     order='depreciation_date ASC', limit=1)
            if line and line.move_check:
                raise ValidationError(
                    _("Line must not validated which line's date to belong closing date on depreciation board!"))
        return line

    @api.model
    def compute_generated_entries(self, date_start, date_end=None, asset_type=None, condition=False, ids=None):
        # Хөрөнгийн ангиалалаар бүлэглэх хөрөнгүүдийг нэг нэгээр нь үүсгэх, хэрэв сонгосон хөрөнгө байхгүй бол мөчлөгийн огноонд тохирох, ажиллаж буй төлөвтэй хөрөнгүүд автоматаар сонгогдоно.
        created_move_ids = []
        type_domain = []
        if asset_type:
            type_domain = [('type', '=', asset_type)]
        if ids:
            if condition:
                # Ангиллаар элэгдлүүлнэ
                asset_ids = self.env['account.asset.asset'].search([('category_id', 'in', [ids.ids])])
            else:
                # Хөрөнгөөр элэгдүүлнэ
                asset_ids = ids
        else:
            asset_ids = self.env['account.asset.asset'].search(type_domain + [('state', '=', 'open'), ('category_id.group_entries', '=', False)])
        created_move_ids += asset_ids._compute_entries(date_start, date_end, group_entries=False)
        for grouped_category in self.env['account.asset.category'].search(type_domain + [('group_entries', '=', True)]):
            assets = self.env['account.asset.asset'].search([('state', '=', 'open'), ('category_id', '=', grouped_category.id)])
            created_move_ids += assets._compute_entries(date_start, date_end, group_entries=True)
        return created_move_ids

    @api.multi
    def create_history(self, dates, type, name, end, amount, old_values, capital_invoice_line_id, move_id=False):
        history_id = self.env['account.asset.history'].create({
            'date': dates,
            'action': type,
            'name': name,
            'user_id': self.env.uid,
            'asset_id': self.id,
            'old_method_number': old_values['method_number'] if old_values else False,
            'old_method_period': old_values['method_period'] if old_values else False,
            'old_method_end': old_values['method_end'] if old_values else False,
            'old_value': old_values['value'] if old_values else 0,
            'capital_invoice_line_id': capital_invoice_line_id,
            'method_time': self.method_time,
            'method_number': self.method_number,
            'method_period': self.method_period,
            'method_end': end,
            'amount': amount,
            'move_id': move_id.id if move_id else False,
        })
        return history_id

    @api.multi
    def write_off(self, close_date, account_id, analytic_account_id, name, context=None):
        ''' Хөрөнгийн данснаас хасна. Хуримтлагдсан элэгдлийн бичилтийн хөрөнгийн кредит талд
            хааж бичилт хийнэ.
        '''
        period_id = self.env['account.period'].search([('date_start', '<=', close_date), ('date_stop', '>=', close_date)])
        move_obj = self.env['account.move']
        for asset in self:
            move_id = move_obj.create(self._get_move_vals(name, asset, account_id, close_date))
            if asset.journal_id.auto_approve:
                move_id.post()
            return move_id

    # Хөрөнгө актлахад үүсэх журналын бичилтийн утга буцаах функц
    @api.multi
    def _get_move_vals(self, name, asset, account_id, close_date):
        line_vals = []
        if asset.value_residual != 0:
            line_vals.append((0, 0, {
                'name': name,
                'ref': asset.name,
                'account_id': account_id,
                'debit': asset.value if asset.value_depreciated == 0 else (asset.value_residual > 0 and asset.value_residual) or 0,
                'credit': (asset.value_residual < 0 and -asset.value_residual) or 0,
                'journal_id': asset.journal_id.id,
                'currency_id': asset.currency_id.id,
                'date': close_date,
                'asset_id': asset.id
            }))
        line_vals.append((0, 0, {
            'name': name,
            'ref': asset.name,
            'account_id': asset.category_id.account_asset_id.id,
            'debit': (asset.value < 0 and -asset.value) or 0,
            'credit': (asset.value > 0 and asset.value) or 0,
            'journal_id': asset.journal_id.id,
            'currency_id': asset.currency_id.id,
            'date': close_date,
            'asset_id': asset.id
        }))
        if asset.value_depreciated != 0:
            line_vals.append([0, 0, {
                'name': name,
                'ref': asset.name,
                'account_id': asset.category_id.account_depreciation_id.id,
                'debit': (asset.value_depreciated > 0 and asset.value_depreciated) or 0,
                'credit': (asset.value_depreciated < 0 and -asset.value_depreciated) or 0,
                'journal_id': asset.journal_id.id,
                'currency_id': asset.currency_id.id,
                'date': close_date,
                'asset_id': asset.id
            }])
        move_vals = {
            'name': asset.journal_id.code + '/' + str(datetime.strptime(close_date, '%Y-%m-%d').year) + '/' + asset.name + '/' + name,
            'date': close_date,
            'ref': asset.name,
            'journal_id': asset.journal_id.id,
            'line_ids': line_vals
        }
        return move_vals

    @api.multi
    def set_to_close(self):
        ''' Хөрөнгийг хаах төлөвтэй болгоно'''
        return self.write({'state': 'close'})

    @api.multi
    def set_to_open(self):
        # Хөрөнгийг нээхэд хаасан огноонд харгалзах элэгдлийн самбарын бичилт устаж, мөр цуцлагдаж, элэгдлийн самбар дахин байгуулагдах
        m_date = ''
        m_id = ''
        inv_line_ids = []
        assets = []
        invoice_line_obj = self.env['account.invoice.line']
        move_obj = self.env['account.move']
        for asset in self:
            # Нэхэмжлэлээ олох
            invoice_id = invoice_line_obj.search([('asset_id', '=', asset.id)], limit=1).mapped('invoice_id')
            if invoice_id:
                payment_ids = self.env['account.payment'].search([('invoice_ids', '=', invoice_id.id)])
                if payment_ids:
                    raise UserError(_("Customer invoices are reconciled created from %s sell asset. First detach reconciled entries!" % asset.name))
                self._cr.execute("SELECT id from account_invoice_line where invoice_id = %s " % invoice_id.id)
                inv_line_ids = self._cr.fetchall()
            # Борлуулсан хөрөнгө тус бүрийг нээх
            if inv_line_ids:
                for inv_line in inv_line_ids:
                    line = invoice_line_obj.browse(inv_line[0])
                    asset = line.asset_id
                    # Хаалтын бичилтүүдийг олох
                    qry = """SELECT * FROM (SELECT move.id, count(m_line.id) FROM account_move_line m_line 
                                                        LEFT JOIN account_move move ON move.id = m_line.move_id 
                                                        WHERE m_line.asset_id = %s GROUP BY move.id) t where t.count >= 3;""" %(asset.id)
                    self._cr.execute(qry)
                    move = self._cr.fetchall()
                    if move:
                        move_id = self.env['account.move'].browse(move[0][0])
                        if not move_id.journal_id.update_posted:
                            raise UserError(_('You cannot modify a posted entry of this journal.\nFirst you should set the journal to allow cancelling entries.'))
                        self._cr.execute(""" UPDATE account_move_line set asset_id = NULL WHERE id in %s """, (tuple(move_id.line_ids.ids),))
                        self._cr.execute(""" DELETE FROM account_move WHERE id = %s """ % move_id.id)
                        dep_line = asset.depreciation_line_ids.search([('depreciation_date', '>=', move_id.date), ('asset_id', '=', asset.id)], order='depreciation_date ASC', limit=1)
                        dep_line.cancel_move()
                    self._cr.execute(""" UPDATE account_asset_asset set state = 'open', closed_date = Null WHERE id = %s """ % asset.id)
                    line.asset_id = False
            else:
                self._cr.execute("""SELECT * FROM (SELECT move.id, count(m_line.id) FROM account_move_line m_line 
                                                        LEFT JOIN account_move move ON move.id = m_line.move_id 
                                                        WHERE m_line.asset_id = %s GROUP BY move.id) t where t.count >= 3;""" % asset.id)
                move = self._cr.fetchall()
                if move:
                    move_id = self.env['account.move'].browse(move[0][0])
                    if not move_id.journal_id.update_posted:
                        raise UserError(_('You cannot modify a posted entry of this journal.\nFirst you should set the journal to allow cancelling entries.'))
                    self._cr.execute(""" UPDATE account_move_line set asset_id = NULL WHERE id in %s """, (tuple(move_id.line_ids.ids),))
                    self._cr.execute(""" DELETE FROM account_move WHERE id = %s """ % move_id.id)
                    dep_line = asset.depreciation_line_ids.search([('depreciation_date', '>=', move_id.date), ('asset_id', '=', asset.id)], order='depreciation_date ASC', limit=1)
                    dep_line.cancel_move()
                self._cr.execute(""" UPDATE account_asset_asset set state = 'open', closed_date = Null WHERE id = %s """ % asset.id)
            # Нэхэмжлэллийг цуцлах
            invoice_id.action_invoice_cancel()
            invoice_id.unlink()
            histories = self.env['account.asset.history'].search([('asset_id', '=', asset.id), ('action', 'in', ('sale', 'close'))])
            if histories:
                histories.with_context(closed=True).unlink()
            asset.compute_depreciation_board()
        return True

    @api.multi
    def set_to_draft(self):
        if self.history_ids or self.move_history_count > 0:
            raise UserError(_('You cannot draft because asset moved!'))
        line_ids = self.env['account.move.line'].search([('asset_id', '=', self.id)])
        if line_ids:
            for line in line_ids:
                line.asset_id = False
            move_ids = self.env['account.move'].search([('line_ids', 'in', line_ids.ids)])
            if move_ids:
                for move in self.env['account.move'].browse(move_ids.ids):
                    move.state = 'draft'
                    move.unlink()
        return super(AccountAssetAsset, self).set_to_draft()

    @api.multi
    def _move_history_count(self):
        for asset in self:
            res = self.env['account.asset.moves.line'].search_count([('asset_id', '=', asset.id)])
            asset.move_history_count = res or 0

    @api.multi
    @api.depends('history_ids.asset_id')
    def _history_count(self):
        for asset in self:
            res = self.env['account.asset.history'].search_count([('asset_id', '=', asset.id)])
            asset.history_count = res or 0

    @api.multi
    def _invoice_count(self):
        for asset in self:
            inv_ids = []
            inv_lines = self.env['account.invoice.line'].search([('asset_id', '=', asset.id)])
            for line in inv_lines:
                if line.invoice_id:
                    inv_ids.append(line.invoice_id.id)
            asset.invoice_count = self.env['account.invoice'].search_count([('id', 'in', inv_ids)]) or 0

    @api.model
    def create(self, vals):
     # Зургийг хэмжээнд нь оруулах
        tools.image_resize_images(vals)
        asset = super(AccountAssetAsset, self.with_context(mail_create_nolog=True)).create(vals)
        self.check_working_performance()
        return asset

    @api.multi
    def write(self, vals):
        # Зургийг хэмжээнд нь оруулах
        tools.image_resize_images(vals)
        if 'method' in vals and vals['method'] == 'linear_day':
            vals['method_period'] = 1
        asset = super(AccountAssetAsset, self).write(vals)
        self.check_working_performance()
        return asset

    @api.multi
    def unlink(self):
        for asset in self:
            for history in asset.history_ids:
                history.unlink()
        return super(AccountAssetAsset, self).unlink()

    @api.multi
    def check_working_performance(self):
        for asset in self:
            total_performance = 0
            if asset.method == 'operation':
                for line in self.depreciation_line_ids:
                    total_performance += line.unit_performance
                if total_performance > asset.working_capacity:
                    raise UserError(_('Working capacity exceeded!'))

    @api.multi
    def create_own_period_lines(self):
        if self.depreciation_line_ids:
            for obj in self.depreciation_line_ids:
                obj.unlink()

        for s_line in self.depreciation_sum_ids:
            self.env['account.asset.depreciation.line'].create({
                'sequence': s_line.sequence,
                'name': s_line.name,
                'asset_id': self.id,
                'depreciation_date': s_line.depreciation_date,
                'depreciated_value': s_line.depreciated_value,
                'amount': s_line.amount,
                'remaining_value': s_line.remaining_value,
            })

    @api.multi
    def cancel_period_lines(self):
        if self.depreciation_sum_ids:
            for obj in self.depreciation_sum_ids:
                obj.unlink()

    @api.multi
    def create_period_lines(self):
        if not self.depreciation_sum_ids:
            if self.depreciation_line_ids and self.method == 'sum':
                line_line = self.env['account.asset.depreciation.line.sum']
                sequencer = 1
                name_sequencer = 1
                depreciated_value = 0
                computing_daydiff = 0
                daydiff = 0
                purchase_date = datetime.strptime(self.purchase_date, "%Y-%m-%d")
                amount_collector = []
                is_first_line = True
                is_diff = False
                total_val = self.value
                if self.salvage_value:
                    total_val = total_val - self.salvage_value
                for line in self.depreciation_line_ids:
                    dater = datetime.strptime(line.depreciation_date, "%Y-%m-%d")
                    currday_of_month = int(purchase_date.day)
                    if currday_of_month != 31 or currday_of_month != 1:
                        lastday_of_month = int(monthrange(dater.year, dater.month)[1])
                        daydiff = lastday_of_month - currday_of_month
                        computing_daydiff = daydiff
                        dater = dater.replace(month=purchase_date.month, day=lastday_of_month)
                        is_diff = True
                    elif currday_of_month == 31 or currday_of_month == 1:
                        computing_daydiff = 1
                        daydiff = 1
                        is_diff = False
                    amount_collector.append(line.amount)
                    for month in range(12):
                        if month == 0:
                            if is_first_line:
                                amount = ((amount_collector[0] / 12) / 31) * (daydiff if daydiff != 0 else 1)
                                is_first_line = False
                            elif not is_first_line:
                                amount = ((amount_collector[-2] / 12) / 31) * (
                                    currday_of_month if currday_of_month != 0 else 1) + (
                                    (amount_collector[-1] / 12) / 31) * (
                                    daydiff if daydiff != 0 else 1)
                            remaining_value = total_val - amount - depreciated_value
                            line_line.create({
                                'sequence': sequencer,
                                'name': line.name + ' - ' + str(name_sequencer),
                                'asset_id': self.id,
                                'depreciation_date': dater + relativedelta(months=month),
                                'depreciated_value': depreciated_value,
                                'amount': amount,
                                'remaining_value': remaining_value,
                            })
                            depreciated_value += amount
                            sequencer += 1
                            name_sequencer += 1
                        elif month > 0:
                            amount = line.amount / 12
                            remaining_value = total_val - amount - depreciated_value
                            line_line.create({
                                'sequence': sequencer,
                                'name': line.name + ' - ' + str(name_sequencer),
                                'asset_id': self.id,
                                'depreciation_date': dater + relativedelta(months=month),
                                'depreciated_value': depreciated_value,
                                'amount': amount,
                                'remaining_value': remaining_value,
                            })
                            depreciated_value += amount
                            sequencer += 1
                            name_sequencer += 1
                    name_sequencer = 1
                is_first_line = True
        elif self.depreciation_sum_ids:
            raise UserError(_('Create after delete lines!'))


class AccountAssetDepreciationLine(models.Model):
    _inherit = 'account.asset.depreciation.line'

    initial_depreciation_check = fields.Boolean(string='Initial depreciation', default=False)
    unit_amount = fields.Float(string='Unit Amount')
    unit_performance = fields.Float(string='Unit Performance', digits=0)
    uom_id = fields.Many2one('product.uom', related='asset_id.uom_id', string='Unit Of Measure', store=True, readonly=True)
    calculated = fields.Boolean(string='Calculated', default=False)
    method = fields.Selection(related='asset_id.method', string='Method')
    period_day = fields.Integer(string='Days')
    is_freeze = fields.Boolean('Freeze', default=False)

    @api.model
    def fields_view_get(self, view_id=None, view_type='tree', toolbar=False, submenu=False):
        result = super(AccountAssetDepreciationLine, self).fields_view_get(view_id, view_type, toolbar=toolbar, submenu=submenu)
        method = self._context.get('default_method', False)
        if method and method == 'operation':
            doc = etree.XML(result['arch'])
            if doc.xpath("//field[@name='unit_amount']"):
                node = doc.xpath("//field[@name='unit_amount']")[0]
                node.set('invisible', '0')
                setup_modifiers(node, result['fields']['unit_amount'])
            if doc.xpath("//field[@name='unit_performance']"):
                node = doc.xpath("//field[@name='unit_performance']")[0]
                node.set('invisible', '0')
                setup_modifiers(node, result['fields']['unit_performance'])
            if doc.xpath("//field[@name='uom_id']"):
                node = doc.xpath("//field[@name='uom_id']")[0]
                node.set('invisible', '0')
                setup_modifiers(node, result['fields']['uom_id'])
            result['arch'] = etree.tostring(doc)
        return result

    @api.multi
    def calculate(self):
        # Үйл ажиллагааны аргаар элэгдэл тооцоход элэгдлийн дүнг тооцох функц
        for line in self:
            remaining_value = depreciated_value = 0
            amount = line.unit_performance * line.unit_amount
            line.amount = amount
            if line.sequence == 1:
                if line.initial_depreciation_check == True:
                    remaining_value = line.asset_id.value_residual - line.amount
                else:
                    remaining_value = line.asset_id.value_residual - amount
            else:
                lines = self.env['account.asset.depreciation.line'].search([('asset_id', '=', line.asset_id.id), ('move_check', '=', False), ('sequence', '=', line.sequence - 1)], limit=1)
                if lines:
                    for l in lines:
                        remaining_value = l.remaining_value - amount
                        depreciated_value = l.depreciated_value + l.amount
                        line.depreciated_value = depreciated_value
            line.remaining_value = remaining_value
            line.calculated = True
            if line.asset_id:
                line.asset_id.check_depreciation_board()
            return True

    @api.multi
    def cancel_move(self):
        # Тухайн элэгдлийн мөрийг цуцлан, журналын бичилтийг устгах
        for line in self:
            if line.parent_state != 'close' and line.move_check:
                if line.move_id:
                    if line.move_id.state != 'draft':
                        line.move_id.write({'state': 'draft'})
                    if line.move_id.line_ids:
                        for move_line in line.move_id.line_ids:
                            move_line.asset_id = False
                    line.move_id.unlink()
                line.write({'move_check': False})
        return True

    @api.multi
    def create_move(self, post_move=True):
        # Элэгдэлийг батлаж, журналын бичилт үүсгэх
        created_moves = self.env['account.move']
        prec = self.env['decimal.precision'].precision_get('Account')
        for line in self:
            #  Журналын бичилт үүсгэх
            # if line.depreciation_date < datetime.today().strftime('%Y-%m-%d'):
            # raise ValidationError(_("Cannot, depreciation date is finished."))
            depreciation_date = self.env.context.get('depreciation_date') or line.depreciation_date or fields.Date.context_today(self)
            # Үйл ажиллагааны элэгдэл бол эхлээд тооцоолно
            if line.method == 'operation':
                line.calculate()
            # Тухайн элэгдлээс өмнөх элэгдлүүдийг батлаагүй бол алдааны мэдээлэл өгнө. Энэ нь элэгдлийн самбар зөв ажиллахад зориулсан болно
            unposted_lines = self.env['account.asset.depreciation.line'].search([('asset_id', '=', line.asset_id.id), ('depreciation_date', '<=', depreciation_date),
                                                                                 ('move_check', '=', False), ('id', '!=', line.id)])
            if unposted_lines:
                raise UserError(_("Before depreciation line cannot approve!"))
            move_vals = self._get_depr_move_vals(line, depreciation_date, prec)

            move = self.env['account.move'].create(move_vals)
            line.write({'move_id': move.id, 'move_check': True})
            created_moves |= move
        if post_move and created_moves:
            if created_moves.filtered(lambda m: any(m.asset_depreciation_ids.mapped('asset_id.category_id.open_asset'))):
                created_moves.filtered(lambda m: any(m.asset_depreciation_ids.mapped('asset_id.category_id.open_asset'))).post()
            else:
                created_moves.filtered(lambda m: any(m.asset_depreciation_ids.mapped('asset_id.journal_id.auto_approve'))).post()
        return [x.id for x in created_moves]

    # Хөрөнгийн элэгдэл батлахад журналын бичилт үүсгэх утга буцаах функц
    @api.multi
    def _get_depr_move_vals(self, line, depreciation_date, prec):
        category_id = line.asset_id.category_id
        asset_name = line.asset_id.name + ' (%s/%s)' % (line.sequence, len(line.asset_id.depreciation_line_ids))
        company_currency = line.asset_id.company_id.currency_id
        current_currency = line.asset_id.currency_id
        amount = current_currency.with_context(date=depreciation_date).compute(line.amount, company_currency)
        move_line_1 = {
            'name': asset_name,
            'ref': line.name,
            'account_id': category_id.account_depreciation_id.id,
            'debit': 0.0 if float_compare(amount, 0.0, precision_digits=prec) > 0 else -amount,
            'credit': amount if float_compare(amount, 0.0, precision_digits=prec) > 0 else 0.0,
            'journal_id': category_id.journal_id.id,
            'partner_id': line.asset_id.partner_id.id,
            'analytic_account_id': category_id.account_analytic_id.id if category_id.type == 'sale' else False,
            'currency_id': company_currency != current_currency and current_currency.id or False,
            'amount_currency': company_currency != current_currency and -1.0 * line.amount or 0.0,
            'date': depreciation_date,
            'asset_id': line.asset_id.id,
        }
        move_line_2 = {
            'name': asset_name,
            'ref': line.name,
            'account_id': category_id.account_depreciation_expense_id.id,
            'credit': 0.0 if float_compare(amount, 0.0, precision_digits=prec) > 0 else -amount,
            'debit': amount if float_compare(amount, 0.0, precision_digits=prec) > 0 else 0.0,
            'journal_id': category_id.journal_id.id,
            'partner_id': line.asset_id.partner_id.id,
            'analytic_account_id': category_id.account_analytic_id.id if category_id.type == 'purchase' and category_id.account_analytic_id else line.asset_id.account_analytic_id.id,
            'currency_id': company_currency != current_currency and current_currency.id or False,
            'amount_currency': company_currency != current_currency and line.amount or 0.0,
            'date': depreciation_date,
            'asset_id': line.asset_id.id,
        }
        return {
            'name': asset_name,
            'ref': line.asset_id.code,
            'date': depreciation_date or False,
            'journal_id': category_id.journal_id.id,
            'line_ids': [(0, 0, move_line_1), (0, 0, move_line_2)],
        }

    @api.multi
    def create_grouped_move(self, post_move=True):
        # Хөрөнгийн ангилалаар нь бүлэглэх элэгдэлийн мөрийг батлаж, бичилт үүсгэх
        if not self.exists():
            return []
        created_moves = self.env['account.move']
        category_id = self[0].asset_id.category_id  # we can suppose that all lines have the same category
        depreciation_date = self.env.context.get('depreciation_date') or fields.Date.context_today(self)
        amount = 0.0
        for line in self:
            if line.method == 'operation':
                line.calculate()
            # Sum amount of all depreciation lines
            company_currency = line.asset_id.company_id.currency_id
            current_currency = line.asset_id.currency_id
            amount += current_currency.compute(line.amount, company_currency)
        name = category_id.name + _(' (grouped)')
        move_line_1 = {
            'name': name,
            'account_id': category_id.account_depreciation_id.id,
            'debit': 0.0,
            'credit': amount,
            'journal_id': category_id.journal_id.id,
            'analytic_account_id': category_id.account_analytic_id.id if category_id.type == 'sale' and category_id.account_analytic_id else line.asset_id.account_analytic_id.id,
            'date': depreciation_date,
            'asset_id': category_id.asset_id.id,
        }
        move_line_2 = {
            'name': name,
            'account_id': category_id.account_depreciation_expense_id.id,
            'credit': 0.0,
            'debit': amount,
            'journal_id': category_id.journal_id.id,
            'analytic_account_id': category_id.account_analytic_id.id if category_id.type == 'purchase' else False,
            'date': depreciation_date,
            'asset_id': category_id.asset_id.id,
        }
        move_vals = {
            'name': name,
            'ref': category_id.name,
            'date': depreciation_date or False,
            'journal_id': category_id.journal_id.id,
            'line_ids': [(0, 0, move_line_1), (0, 0, move_line_2)],
        }
        move = self.env['account.move'].create(move_vals)
        self.write({'move_id': move.id, 'move_check': True})
        created_moves |= move

        if post_move and created_moves:
            self.post_lines_and_close_asset()
            if move.journal_id.auto_approve:
                created_moves.post()
        return [x.id for x in created_moves]

    @api.multi
    def post_lines_and_close_asset(self):
        # we re-evaluate the assets to determine whether we can close them
        for line in self:
            line.log_message_when_posted()
            asset = line.asset_id
            if asset.currency_id.is_zero(asset.value_residual):
                asset.message_post(body=_("Asset closed."))
                # Хөрөнгө дээр элэгдээд дуусангуут автомат хаагдаад байгааг болиулсан
                asset.write({'state': 'open'})

    @api.multi
    def unlink(self):
        for line in self:
            if line.parent_state != 'draft':
                raise UserError(_("Cannot delete depreciation line!"))
        return super(AccountAssetDepreciationLine, self).unlink()


class AccountAssetDepreciationLineInheritSum(models.Model):
    _name = 'account.asset.depreciation.line.sum'

    asset_id = fields.Many2one('account.asset.asset', 'Asset')
    name = fields.Char(string='Depreciation Name', index=True)
    sequence = fields.Integer('Sequence')
    amount = fields.Float(string='Current Depreciation', digits=0)
    remaining_value = fields.Float(string='Next Period Depreciation', digits=0)
    depreciated_value = fields.Float(string='Cumulative Depreciation')
    depreciation_date = fields.Date('Depreciation Date', index=True)


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    asset_id = fields.Many2one('account.asset.asset', 'Asset', ondelete="restrict")


class AccountMove(models.Model):
    _inherit = 'account.move'

    @api.multi
    def button_cancel(self):
        # Журналын бичилт батлагдаагүй төлөвтэй болоход хэрэв шинжилгээний мөр үүссэн байвал тухайн мөрүүдийг устгах
        res = super(AccountMove, self).button_cancel()
        for move in self:
            for line in move.line_ids:
                if line.analytic_line_ids:
                    line.analytic_line_ids.unlink()
        return res
