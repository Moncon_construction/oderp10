# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from odoo.exceptions import ValidationError
from odoo import exceptions


class AccountAssetLocation(models.Model):
    _name = 'account.asset.location'
    _description = 'Asset location'

    name = fields.Char(string="Name", index=True)
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env['res.company']._company_default_get('account.asset.location'))
    description = fields.Text(string="Description")
    account_analytic_id = fields.Many2one('account.analytic.account', string='Analytic account')  # l10n_mn_analytic_balance дотор ашиглагдана. l10n_mn_account_asset-д ашиглагдах бол комментыг авна уу.

    @api.multi
    def unlink(self):
        for obj in self:
            asset_obj = self.env['account.asset.asset'].search([('location_id', '=', obj.id)], limit=1)
            if asset_obj:
                raise ValidationError(_('The location of the asset is selected on the %s asset.') % asset_obj.name)
            return super(AccountAssetLocation, self).unlink()
    

    @api.constrains('name')
    def _check_name(self):
        for locations in self:
            if locations.name:
                categories = self.env['account.asset.location'].search([
                    ('name', '=', locations.name),
                    ('id', '!=', locations.id),
                    ('company_id', '=', locations.company_id.id)])
            for categ in categories:
                if categ:
                    exception = _('location name duplicated: ') + categ.name
                    raise exceptions.ValidationError(exception)
                    
    @api.one
    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        default = dict(default or {})
        default.setdefault('name', _("%s (copy)") % (self.name or ''))
        return super(AccountAssetLocation, self).copy(default)