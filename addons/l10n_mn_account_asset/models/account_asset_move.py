# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF


class MailComposer(models.TransientModel):
    _inherit = 'mail.compose.message'

    @api.multi
    def send_mail_action(self):
        ''' Үндсэн хөрөнгө шилжүүлэх мэйл илгээх товчин дээр дарах үед ажиллана '''
        if self._context.get('active_model') == 'account.asset.moves':
            asset_move = self.env['account.asset.moves'].search([('id', '=', self._context.get('active_id'))])
            status = self._context.get('status')
            if status == 'sent':
                asset_move.write({'state': status})
                for line_move in asset_move.line_ids:
                    line_move.write({'state': status})
        return super(MailComposer, self).send_mail_action()


class AccountAssetMoves(models.Model):
    _name = 'account.asset.moves'
    _inherit = 'mail.thread'
    _description = "Asset move"

    name = fields.Char('Reference', default='/', copy=False)
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env['res.company']._company_default_get('account.asset.moves'))
    sequence_id = fields.Many2one('ir.sequence', 'Move Sequence')
    description = fields.Text('Description')
    move_date = fields.Date('Move Date', default=fields.Date.context_today)
    receipt_date = fields.Date('Receipt Date')
    state = fields.Selection([('draft', 'Draft'),
                              ('receipt_some', 'Receipted Some'),
                              ('receipt', 'Receipted'),
                              ('cancel', 'Canceled')], 'State', default='draft')
    line_ids = fields.One2many('account.asset.moves.line', 'move_id', 'Assets')

    _sql_constraints = [
        ('name_uniq', 'unique(name, company_id)', 'Reference must be unique per company!'),
    ]

    @api.model
    def default_get(self, default_fields):
        '''Хөрөнгүүдийг олноор нь сонгон шилжүүлэх үед мөр бүрт мэдээллийг оруулах '''
        res = super(AccountAssetMoves, self).default_get(default_fields)
        context = dict(self._context or {})
        move_vals = []
        if context.get('active_model') == 'account.asset.asset' and context.get('active_ids'):
            assets = self.env[context.get('active_model')].browse(context.get('active_ids'))
            moves_obj = self.env['account.asset.moves']
            for asset in assets:
                line = [0, False, {'asset_id': asset.id,
                                   'state': 'draft',
                                   'old_owner_id': asset.user_id.id,
                                   'old_location_id': asset.location_id.id,
                                   'old_category_id': asset.category_id.id,
                                   'old_account_analytic_id': asset.account_analytic_id.id,
                                   'new_owner_id': asset.user_id.id,
                                   'new_location_id': asset.location_id.id,
                                   'new_category_id': asset.category_id.id,
                                   'new_account_analytic_id': asset.account_analytic_id.id}]
                move_vals.append(line)
        res.update({'line_ids': move_vals})
        return res

    @api.model
    def create(self, values):
        ''' Хүлээн авсан огноо нь шилжүүлсэн огнооноос хойш байж болохгүй ба нэрийг автомат дугаарлана '''
        res = super(AccountAssetMoves, self).create(values)
        if res.name == '/':
            res.write({'name': self.env['ir.sequence'].get('account.asset.moves')})
        return res

    @api.multi
    def unlink(self):
        ''' Зөвхөн ноорог төлөвтэй шилжилтийг л устгадаг байна '''
        for move in self:
            if move.state != 'draft':
                raise UserError(_('You cannot delete an asset move which is not draft.'))
            for line in move.line_ids:
                line.unlink()
        return super(AccountAssetMoves, self).unlink()

    @api.multi
    def action_receipt(self):
        ''' Хүлээн авах товч дарахад дуудагдана '''
        context = dict(self._context or {})
        context['asset_move'] = True
        for obj in self:
            # Хэрвээ шилжилт хөдөлгөөн мөргүй бол алдааны мэдээлэл өгдөг байх
            if not obj.line_ids:
                raise UserError(_("You cannot receipt without line asset move."))
            for line in obj.line_ids:
                # Хэрвээ мөр нь цуцлагдсан төлөвтэй бол алдааны мэдээлэл өгдөг байх
                if line.state == 'cancel':
                    raise UserError(_("You cannot receipt cancel asset move line."))
            for line in obj.line_ids:
                # Хэрвээ мөр нь ноорог төлөвтэй бол шилжүүлэлт хийнэ
                receipt_lines = self.env['account.asset.moves.line'].search([('asset_id', '=', line.asset_id.id), ('id', '!=', line.id), ('state', '=', 'receipt')])
                for receipt_line in receipt_lines:
                    if receipt_line.move_id.move_date > line.move_id.move_date:
                        raise UserError(_("You cannot asset moves. Already %s asset moved after %s!" % (line.asset_id.name, obj.move_date)))
                if line.state == 'draft':
                    line.with_context(context).receipt_button()
            obj.write({'state': 'receipt', 'receipt_date': datetime.now()})
        return {'type': 'ir.actions.act_window_close'}

    @api.multi
    def action_cancel(self):
        ''' Хүлээн авсан үндсэн хөрөнгийг цуцлах үед тухайн хөрөнгөнд ямарваа ажил гүйлгээ үүссэн байвал дагаж устгах '''
        context = dict(self._context or {})
        context['asset_move'] = True
        for line in self.line_ids:
            if line.state == 'receipt':
                line.with_context(context).cancel_button()
            if line.state == 'draft':
                line.write({'state': 'cancel'})
        return self.write({'state': 'cancel', 'receipt_date': False})

    @api.multi
    def action_draft(self):
        ''' Цуцлагдсан төлөвтэй шилжих хөдөлгөөнийг Ноорог болгох '''
        for line in self.line_ids:
            line.write({'state': 'draft'})
        return self.write({'state': 'draft'})

    @api.multi
    def check_state(self):
        # Мөрөөс хамаарч шилжих хөдөлгөөний төлвийг солино
        draft_lines = self.env['account.asset.moves.line'].search([('move_id', '=', self.id), ('state', '=', 'draft')])
        state = 'draft'
        if len(draft_lines) > 0 and len(draft_lines) < len(self.line_ids):
            state = 'receipt_some'
        elif len(draft_lines) == 0:
            state = 'receipt'
        self.write({'state': state})


class AccountAssetMovesLine(models.Model):
    _name = 'account.asset.moves.line'
    _description = 'Asset move information'

    asset_id = fields.Many2one('account.asset.asset', 'Asset', ondelete='cascade')
    move_id = fields.Many2one('account.asset.moves', 'Asset move', ondelete='cascade')
    account_move_id = fields.Many2one('account.move', 'Account move')
    old_owner_id = fields.Many2one('hr.employee', 'Old owner')
    new_owner_id = fields.Many2one('hr.employee', 'New owner')
    old_location_id = fields.Many2one('account.asset.location', 'Old location')
    new_location_id = fields.Many2one('account.asset.location', 'New location')
    old_category_id = fields.Many2one('account.asset.category', 'Old category')
    new_category_id = fields.Many2one('account.asset.category', 'New category')
    old_account_analytic_id = fields.Many2one('account.analytic.account', 'Old analytic account')
    new_account_analytic_id = fields.Many2one('account.analytic.account', 'New analytic account')
    state = fields.Selection([('draft', 'Draft'),
                              ('receipt', 'Receipted'),
                              ('cancel', 'Canceled')], 'State', default='draft')

    @api.onchange('asset_id')
    def onchange_asset(self):
        if not self.asset_id:
            return
        self.old_owner_id = self.asset_id.user_id.id
        self.old_location_id = self.asset_id.location_id.id
        self.old_category_id = self.asset_id.category_id.id
        self.old_account_analytic_id = self.asset_id.account_analytic_id.id
        self.new_owner_id = self.asset_id.user_id.id
        self.new_location_id = self.asset_id.location_id.id
        self.new_category_id = self.asset_id.category_id.id
        self.new_account_analytic_id = self.asset_id.account_analytic_id.id

    @api.multi
    def write(self, values):
        ''' Хөрөнгийг өөрчилсөн, шинээр бичсэн тохиолдолд тухайн хөрөнгийн мэдээллээр мөрийг шинэчилж үүсгэх '''
        if 'asset_id' in values:
            asset = self.env['account.asset.asset'].browse(values['asset_id'])
            values.update({
                'old_owner_id': asset.user_id.id,
                'old_location_id': asset.location_id.id,
                'old_category_id': asset.category_id.id,
                'old_account_analytic_id': asset.account_analytic_id.id,
            })
        return super(AccountAssetMovesLine, self).write(values)

    @api.model
    def create(self, values):
        ''' Хөрөнгийг сонгосон тохиолдолд тухайн хөрөнгийн мэдээллээр мөрийг шинэчилж үүсгэх '''
        if 'asset_id' in values:
            asset = self.env['account.asset.asset'].browse(values['asset_id'])
            values.update({
                'old_owner_id': asset.user_id.id,
                'old_location_id': asset.location_id.id,
                'old_category_id': asset.category_id.id,
                'old_account_analytic_id': asset.account_analytic_id.id,
            })
        return super(AccountAssetMovesLine, self).create(values)

    @api.multi
    def unlink(self):
        ''' Зөвхөн ноорог төлөвтэй мөрийг л устгадаг байна '''
        for line in self:
            if line.state != 'draft':
                raise UserError(_('You cannot delete an asset move line which is not draft.'))
        return super(AccountAssetMovesLine, self).unlink()

    # Журналын мөрүүд үүсгэх
    @api.multi
    def _prepare_move_line(self, account, analytic_account, debit=0, credit=0):
        for line in self:
            return (0, 0, {'name': line.move_id.name,
                           'ref': line.id,
                           'account_id': account,
                           'debit': debit,
                           'credit': credit,
                           'journal_id': line.new_category_id.journal_id.id,
                           # 'currency_id': line.asset_id.company_id.currency_id.id != current_currency and current_currency or False,
                           # 'amount_currency': line.asset_id.company_id.currency_id.id != current_currency and -dep_amount or 0.0,
                           'date': line.move_id.move_date,
                           'analytic_account_id': analytic_account,
                           'asset_id': line.asset_id.id})

    @api.multi
    def receipt_button(self):
        # Хөрөнгийн шилжилт хөдөлгөөн хүлээн авах
        currency_obj = self.env['res.currency']
        asset_move = self.env.context.get('asset_move', False)
        for line in self:
            if line.old_category_id == line.new_category_id and line.old_owner_id == line.new_owner_id and \
                    line.old_location_id == line.new_location_id and line.old_account_analytic_id == line.new_account_analytic_id:
                raise UserError(_("There is no difference new and old assets."))
            current_currency = line.asset_id.currency_id.id or line.asset_id.company_id.currency_id.id
            asset_amount = currency_obj.compute_currency(current_currency, line.asset_id.company_id.currency_id.id, line.asset_id.value or 0)
            dep_amount = 0
            dep_date = False
            for dep_line in line.asset_id.depreciation_line_ids:
                if dep_line.move_check:
                    if not dep_date or (dep_date and dep_date < dep_line.depreciation_date):
                        dep_date = dep_line.depreciation_date
                    dep_amount += dep_line.amount
            if current_currency != line.asset_id.company_id.currency_id.id:
                depreciation_amount = currency_obj.compute_currency(current_currency, line.asset_id.company_id.currency_id.id, dep_amount or 0)
            else:
                depreciation_amount = dep_amount
            if line.old_category_id != line.new_category_id:
                if not dep_date:
                    dep_date = line.asset_id.date
                dep_date = datetime.strptime(dep_date, DF).date()
                move_date = datetime.strptime(line.move_id.move_date, DF).date()
                if dep_date > move_date:
                    raise UserError(_("You cannot receipt. %s cancel depreciation %s after." % (line.asset_id.name, move_date)))
                elif dep_date < move_date:
                    month = (move_date.month - dep_date.month) + (move_date.year - dep_date.year) * 12
                    if month > 1 and depreciation_amount != asset_amount:
                        raise UserError(_("You cannot receipt. %s validate depreciation %s before." % (line.asset_id.name, move_date)))
                # Хөрөнгийн ангилал өөрчлөгдсөн бол журналын бичилт үүсч элэгдлийн самбар дахин байгуулагдана.
                line_ids = []
                if line.new_category_id.account_asset_id.id != line.old_category_id.account_asset_id.id:
                    # Хөрөнгийн хуучин ангиллын хөрөнгийн дансны эсрэг бичилт
                    account = line.old_category_id.account_asset_id.id
                    analytic_account = line.old_account_analytic_id and line.old_account_analytic_id.id or False
                    line_ids.append(line._prepare_move_line(account, analytic_account, 0, asset_amount))
                    # Хөрөнгийн шинэ ангиллын хөрөнгийн дансыг нэмэгдүүлэх
                    account = line.new_category_id.account_asset_id.id
                    analytic_account = line.new_account_analytic_id and line.new_account_analytic_id.id or False
                    line_ids.append(line._prepare_move_line(account, analytic_account, asset_amount, 0))
                if line.new_category_id.account_depreciation_id.id != line.old_category_id.account_depreciation_id.id:
                    # Хөрөнгийн хуучин ангиллын элэгдлийн дансыг 0 болгох
                    account = line.old_category_id.account_depreciation_id.id
                    analytic_account = line.old_account_analytic_id and line.old_account_analytic_id.id or False
                    line_ids.append(line._prepare_move_line(account, analytic_account, depreciation_amount, 0))
                    # Хөрөнгийн шинэ ангиллын элэгдлийн дансанд элэгдлийн дүнг нэмэх
                    account = line.new_category_id.account_depreciation_id.id
                    analytic_account = line.new_account_analytic_id and line.new_account_analytic_id.id or False
                    line_ids.append(line._prepare_move_line(account, analytic_account, 0, depreciation_amount))

                if line_ids:
                    move_id = self.env['account.move'].create({'name': line.move_id.name,
                                                               'date': line.move_id.move_date,
                                                               'ref': line.asset_id.name,
                                                               'journal_id': line.new_category_id.journal_id.id,
                                                               'line_ids': line_ids
                                                               })
                    line.write({'account_move_id': move_id.id})

            line.asset_id.write({'user_id': line.new_owner_id.id,
                                 'location_id': line.new_location_id.id,
                                 'account_analytic_id': line.new_account_analytic_id.id if line.new_account_analytic_id else
                                                        line.new_account_analytic_id and line.new_account_analytic_id.id or False,
                                 'category_id': line.new_category_id.id if line.new_category_id else line.old_category_id.id})
            line.asset_id.compute_depreciation_board()
            line.write({'state': 'receipt'})
            if not asset_move:
                line.move_id.check_state()
                return {'type': 'ir.actions.client', 'tag': 'reload'}
        return True

    @api.multi
    def cancel_button(self):
        asset_move = self.env.context.get('asset_move', False)
        for line in self:
            # Үндсэн хөрөнгийн шилжилтийг устгах үед тухайн хөрөнгөнд шилжүүлэлтийн журналын бичилтээс хойш ямарваа журналын бичилт үүссэн байвал устгах боломжгүй
            # мөн хамгийн сүүлийн шилжүүлэлт биш бол устгах боломжгүй байна
            moves_lines = self.env['account.asset.moves.line'].search([('asset_id', '=', line.asset_id.id), ('state', '=', 'receipt')])
            for move_line in moves_lines:
                if move_line.id != line.id:
                    if move_line.move_id.move_date > line.move_id.move_date:
                        raise UserError(_("Cannot delete %s asset moves, only last moves of asset can delete!" % move_line.asset_id.name))
                    elif move_line.move_id.move_date == line.move_id.move_date:
                        if move_line.write_date > line.write_date:
                            raise UserError(_("Cannot delete %s asset moves, only last moves of asset can delete!" % move_line.asset_id.name))
            # Хөрөнгө шилжилтээс үүссэн ажил гүйлгээг устгах
            line_ids = self.env['account.move.line'].search([('asset_id', '=', line.asset_id.id), ('date', '>=', line.move_id.move_date)])
            move_ids = self.env['account.move'].search([('line_ids', 'in', line_ids.ids)])
            for move_id in move_ids:
                if move_id.id != line.account_move_id.id:
                    if move_id.date > line.account_move_id.date:
                        raise UserError(_("Journal entries are to be made on the %s asset after the transfer." % line.asset_id.name))
                    elif move_id.date == line.account_move_id.date:
                        if move_id.create_date > line.account_move_id.create_date:
                            raise UserError(_("Journal entries are to be made on the %s asset after the transfer." % line.asset_id.name))
            for m_line in line.account_move_id.line_ids:
                m_line.asset_id = False
            line.account_move_id.button_cancel()
            line.account_move_id.unlink()
            line.asset_id.write({'user_id': line.old_owner_id.id,
                                 'location_id': line.old_location_id.id,
                                 'account_analytic_id': line.old_account_analytic_id.id,
                                 'category_id': line.old_category_id.id})
            if asset_move:
                line.write({'state': 'cancel'})
            else:
                line.write({'state': 'draft'})
                line.move_id.check_state()
                return {'type': 'ir.actions.client', 'tag': 'reload'}
        return True