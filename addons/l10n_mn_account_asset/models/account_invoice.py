# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
from odoo.exceptions import UserError

class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'
    
    asset_id = fields.Many2one('account.asset.asset', 'Asset')

    def _get_taxes(self, price_subtotal):
        # хасагдуулах НӨАТ-н татварын дзнг тооцоолдог хэсэг
        taxes = False
        currency = self.invoice_id and self.invoice_id.currency_id or None_get_deduction_vat
        price = self.price_unit * (1 - (self.discount or 0.0) / 100.0)
        if price_subtotal:
            price = price * self.quantity
        if self.invoice_line_tax_ids:
            taxes = self.invoice_line_tax_ids.compute_all(price, currency, 1, product=self.product_id, partner=self.invoice_id.partner_id)
        return taxes, price

    def _get_price_subtotal(self, taxes, price):
        sign = self.invoice_id.type in ['in_refund', 'out_refund'] and -1 or 1
        price_subtotal_signed = taxes['total_excluded'] if taxes else price
        if self.invoice_id.currency_id and self.invoice_id.company_id and self.invoice_id.currency_id != self.invoice_id.company_id.currency_id:
            price_subtotal_signed = self.invoice_id.currency_id.with_context(date=self.invoice_id._get_currency_rate_date()).compute(price_subtotal_signed, self.invoice_id.company_id.currency_id)
        return price_subtotal_signed * sign

    @api.one
    def asset_create(self):
        if self.asset_category_id:
            taxes, price = self._get_taxes(False)
            price_subtotal_signed = self._get_price_subtotal(taxes, price)
            i = 0
            #Тоо хэмжээнээс хамаарч олон хөрөнгө үүсгэдэг болгов.
            while i < self.quantity:
                vals = {
                    'name': self.name,
                    'code': self.invoice_id.number or False,
                    'category_id': self.asset_category_id.id,
                    'invoice_line_id': self.id,
                    'value': price_subtotal_signed,
                    'partner_id': self.invoice_id.partner_id.id,
                    'company_id': self.invoice_id.company_id.id,
                    'currency_id': self.invoice_id.company_currency_id.id,
                    'date': self.invoice_id.date_invoice,
                    'invoice_id': self.invoice_id.id,
                }
                changed_vals = self.env['account.asset.asset'].onchange_category_id_values(vals['category_id'])
                vals.update(changed_vals['value'])
                asset = self.env['account.asset.asset'].create(vals)
                if self.asset_category_id.open_asset:
                    asset.validate()
                i += 1
        return True

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'
    
    @api.multi
    def action_invoice_cancel(self):
        for line in self.invoice_line_ids:
            if line.asset_id:
                raise UserError(_("Cannot cancel entry to sold asset. Do open asset process to asset."))
        return super(AccountInvoice, self).action_invoice_cancel()
    
    @api.multi
    def unlink(self):
        for invoice in self:
            for line in invoice.invoice_line_ids:
                if line.asset_id:
                    raise UserError(_("Cannot delete invoice line created from sell asset!"))
        return super(AccountInvoice, self).unlink()

    @api.multi
    def action_move_create(self):
        res = super(AccountInvoice, self).action_move_create()

        # l10n_mn_purchase_asset модуль суусан бол хөрөнгө үүсгэхгүй
        if not self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_purchase_asset'), ('state', 'in', ('installed', 'to upgrade'))]):
            context = dict(self.env.context)
            # Энэхүү context нь хөрөнгийх биш нэхэмжлэлийх тул устгагдах ёстой.
            # Үгүй бол нэхэмжлэлийн төрлөөр хөрөнгө үүсгэх гэж оролдох болно.
            context.pop('default_type', None)
            for inv in self:
                inv.invoice_line_ids.with_context(context).asset_create()

        return res
