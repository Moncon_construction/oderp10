# -*- coding: utf-8 -*-

from odoo import api, models, _

class AccountAssetAsset(models.Model):
    _inherit = 'account.asset.asset'

    @api.multi
    def get_account_asset_list_mobile(self, local_assets):
        """
       Мобайл төхөөрөмжөөс AccountAssetAsset-ийн жагсаалт татахад дуудагдах функц Агуулахын 
       програмаас
       :param local_assets: Мобайл төхөөрөмжид байгаа хөрөнгийн жагсаалтаас id,write_date
       утгууд дамжиж орж ирнэ
       :return: JSON бүтэцтэй харилцагчийн мэдээлэл буцаана
       """
        update_asset = []
        insert_asset = []
        local_ids = []
        result = []
        for asset in local_assets:
            local_ids.append(asset.get('id'))
            if not asset.get('write_date'):
                write_date = '2000-01-01 00:00:00.999999'
            else:
                write_date = asset.get('write_date') + '.999999'
            asset = self.env['account.asset.asset'].search(
                [('id', '=', asset.get('id')), ('write_date', '>', write_date)])
            # Хөрөнгийн мэдээлэл өөрчлөгдсөн байвал update хийнэ.
            if asset:
                update_asset.append({'id': asset.id,
                                   'name': asset.name,
                                   'code': asset.code,
                                   'date': asset.date,
                                   'purchase_date': asset.purchase_date,
                                   'certificate_number': asset.certificate_number,
                                   'user_id': asset.user_id.id,
                                   'partner_id2': asset.partner_id.id,
                                   'account_analytic_id': asset.account_analytic_id.id,
                                   'location_id': asset.location_id.id,
                                   'value': asset.value,
                                   'qr_code': asset.qr_code,
                                   'location_name': asset.location_id.name,
                                   'category_name': asset.category_id.name,
                                   'state': asset.state,
                                   'category_id': asset.category_id.id,
                                   'write_date':asset.write_date,
                                   'image':asset.image_medium
                                     })

        if len(local_assets) > 0:
            insert_assets = self.env['account.asset.asset'].search([('id', 'not in', tuple(local_ids))])

            for asset in insert_assets:
                insert_asset.append({'id': asset.id,
                                   'name': asset.name,
                                   'code': asset.code,
                                   'date': asset.date,
                                   'purchase_date': asset.purchase_date,
                                   'certificate_number': asset.certificate_number,
                                   'user_id': asset.user_id.id,
                                   'partner_id2': asset.partner_id.id,
                                   'account_analytic_id': asset.account_analytic_id.id,
                                   'location_id': asset.location_id.id,
                                   'value': asset.value,
                                   'qr_code': asset.qr_code,
                                   'location_name': asset.location_id.name,
                                   'category_name': asset.category_id.name,
                                   'state': asset.state,
                                   'category_id': asset.category_id.id,
                                   'write_date':asset.write_date,
                                   'image': asset.image_medium
                                     })
        else:
            insert_assets = self.env['account.asset.asset'].search([('id', 'not in', tuple(local_ids))])

            for asset in insert_assets:
                    insert_asset.append({'id': asset.id,
                                   'name': asset.name,
                                   'code': asset.code,
                                   'date': asset.date,
                                   'purchase_date': asset.purchase_date,
                                   'certificate_number': asset.certificate_number,
                                   'user_id': asset.user_id.id,
                                   'partner_id2': asset.partner_id.id,
                                   'account_analytic_id': asset.account_analytic_id.id,
                                   'location_id': asset.location_id.id,
                                   'value': asset.value,
                                   'qr_code': asset.qr_code,
                                   'location_name': asset.location_id.name,
                                   'category_name': asset.category_id.name,
                                   'state': asset.state,
                                   'category_id': asset.category_id.id,
                                   'write_date':asset.write_date,
                                   'image': asset.image_medium
                                         })


        result.append({'update': update_asset})
        result.append({'insert': insert_asset})
        return result
