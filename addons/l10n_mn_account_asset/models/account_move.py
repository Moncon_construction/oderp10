# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError

class AccountMove(models.Model):
    _inherit = "account.move"

    @api.multi
    def unlink(self):
#         Журналын бичилт нь үндсэн хөрөнгөнөөс үүссэн бол устгагдахгүй
        for move in self:
            for line in move.line_ids:
                if line.asset_id:
                    raise UserError(_('The entries created when registered asset or depreciated asset cannot delete.'))   
        return super(AccountMove, self).unlink()

class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    asset_id = fields.Many2one('account.asset.asset', string='Asset')
