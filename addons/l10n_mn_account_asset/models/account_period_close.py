# -*- coding: utf-8 -*-
from odoo import models, api, _
from odoo.exceptions import UserError


class AccountPeriodClose(models.TransientModel):
    _inherit = "account.period.close"

    @api.multi
    def data_save(self):
        for period in self.env['account.period'].browse(self.env.context.get('active_ids')):
            assets = self.env['account.asset.asset'].search([('company_id', '=', self.env.user.company_id.id)])
            asset_ids = assets.ids if assets else []
            dep_lines = self.env['account.asset.depreciation.line'].search([('asset_id', 'in', asset_ids), ('depreciation_date', '>=', period.date_start), ('depreciation_date', '<=', period.date_stop)])
            for dep_line in dep_lines:
                if not dep_line.move_id and dep_line.asset_id.state != 'close':
                    raise UserError(_('Warning: Non-depreciated assets are exists in this period. Please create asset depreciation firstly!'))
        return super(AccountPeriodClose, self).data_save()
