# -*- coding: utf-8 -*-
from odoo import fields, models, api, _

class hr_employee(models.Model):
    _inherit = "hr.employee"
    
    asset_count = fields.Integer(string='Asset', help='Total asset of employee', compute='_compute_total_asset')
    
    @api.one
    def _compute_total_asset(self):
        data = self.env['account.asset.asset'].read_group([('user_id', 'in', self.ids)], ['user_id'], ['user_id'])
        count_data = dict((item['user_id'][0], item['user_id_count']) for item in data)
        for order in self:
            order.asset_count = count_data.get(order.id, 0)