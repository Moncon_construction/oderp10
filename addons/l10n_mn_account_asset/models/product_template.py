# -*- coding: utf-8 -*-
from odoo import fields, models


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    asset_category_id = fields.Many2one(track_visibility='onchange')
    deferred_revenue_category_id = fields.Many2one(track_visibility='onchange')
