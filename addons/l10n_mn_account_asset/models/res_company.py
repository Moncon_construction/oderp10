# -*- coding: utf-8 -*-
from odoo import fields, models, _

class ResCompany(models.Model):
    _inherit = "res.company"
    
    income_account_asset = fields.Many2one('account.account', string="Income account of asset")
    expense_account_asset = fields.Many2one('account.account', string="Expense account of asset")
    addition_account_reval_asset = fields.Many2one('account.account', string="Addition account of asset revaluation")
    deduction_account_reval_asset = fields.Many2one('account.account', string="Deduction account of asset revaluation")
    module_l10n_mn_account_asset_code_sequencer = fields.Boolean(string="Asset Auto Code")
    use_operation_method = fields.Boolean(string="Use Operation Method", default=False)
