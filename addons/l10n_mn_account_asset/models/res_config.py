# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class AccountConfigSettings(models.TransientModel):
    _inherit = 'account.config.settings'
    
    income_account_asset = fields.Many2one('account.account', related='company_id.income_account_asset')
    expense_account_asset = fields.Many2one('account.account', related='company_id.expense_account_asset')
    addition_account_reval_asset = fields.Many2one('account.account', related='company_id.addition_account_reval_asset')
    deduction_account_reval_asset = fields.Many2one('account.account', related='company_id.deduction_account_reval_asset')
    module_l10n_mn_account_asset_code_sequencer = fields.Boolean(related="company_id.module_l10n_mn_account_asset_code_sequencer",
                                                                 help="installs l10n_mn_account_asset_code_sequencer",
                                                                 string="Asset auto code")  # Хөрөнгө автоматаар кодлох эсэхийг чагтлах
    use_operation_method = fields.Boolean(string="Use Operation Method", related='company_id.use_operation_method')
    
    @api.onchange('company_id')
    def onchange_company_id(self):
        # update related fields
        self.currency_id = False
        if self.company_id:
            company = self.company_id
            self.chart_template_id = company.chart_template_id
            self.has_chart_of_accounts = False
            self.expects_chart_of_accounts = company.expects_chart_of_accounts
            self.currency_id = company.currency_id
            self.transfer_account_id = company.transfer_account_id
            self.income_account_asset = company.income_account_asset
            self.expense_account_asset = company.expense_account_asset
            self.addition_account_reval_asset = company.addition_account_reval_asset
            self.deduction_account_reval_asset = company.deduction_account_reval_asset
            self.company_footer = company.rml_footer
            self.tax_calculation_rounding_method = company.tax_calculation_rounding_method
            self.bank_account_code_prefix = company.bank_account_code_prefix
            self.cash_account_code_prefix = company.cash_account_code_prefix
            self.code_digits = company.accounts_code_digits
            self.use_operation_method = company.use_operation_method

            # update taxes
            ir_values = self.env['ir.values']
            taxes_id = ir_values.get_default('product.template', 'taxes_id', company_id = self.company_id.id)
            supplier_taxes_id = ir_values.get_default('product.template', 'supplier_taxes_id', company_id = self.company_id.id)
            self.default_sale_tax_id = isinstance(taxes_id, list) and len(taxes_id) > 0 and taxes_id[0] or taxes_id
            self.default_purchase_tax_id = isinstance(supplier_taxes_id, list) and len(supplier_taxes_id) > 0 and supplier_taxes_id[0] or supplier_taxes_id
        return {}
