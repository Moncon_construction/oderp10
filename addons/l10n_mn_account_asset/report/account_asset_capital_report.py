# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from odoo.exceptions import UserError


class AccountAssetCapitalReport(models.AbstractModel):
    _name = 'report.l10n_mn_account_asset.account_asset_capital_report'

    @api.model
    def render_html(self, docids, data=None):
        histories = self.env['account.asset.history'].browse(docids)
        capitals = histories.filtered(lambda l: l.action == 'capital')
        if capitals:
            docargs = {
                'docs': capitals,
            }
            return self.env['report'].render('l10n_mn_account_asset.account_asset_capital_report', docargs)
        else:
            raise UserError(_('There is no capital in the histories!'))
