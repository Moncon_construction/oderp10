from odoo import api, models
from odoo.addons.l10n_mn_report.models.report_helper import verbose_numeric, convert_curr
from datetime import datetime


class PrintAccountAssetMoves(models.AbstractModel):
    _name = 'report.l10n_mn_account_asset.print_account_asset_moves'

    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        asset_moves_obj = self.env['account.asset.moves']
        asset_move = asset_moves_obj.browse(docids)
        report = report_obj._get_report_from_name('l10n_mn_account_asset.print_account_asset_moves')

        move_line_obj = self.env['account.asset.moves.line']
        qty = 1
        move_lines = move_line_obj.search([('move_id', '=', asset_move.id)])
        asset_move_lines = []
        owner_name = ''
        old_owner_name = ''
        location = ''
        for line in move_lines:
            if line.new_owner_id:
                owner_name = line.new_owner_id.name
                if line.new_owner_id.last_name:
                    owner_name = '%s.%s' % (line.new_owner_id.name, line.new_owner_id.last_name[:1])
            if line.new_owner_id:
                
                old_owner_name = line.old_owner_id.name
                if line.old_owner_id.last_name:
                    old_owner_name = '%s.%s' % (line.old_owner_id.name, line.old_owner_id.last_name[:1])
            if line.new_location_id:
                location = line.new_location_id.name
            asset_move_line = {
                'name': line.asset_id.name,
                'code': line.asset_id.code,
                'qty': qty,
                'location': location,
                'new_owner': owner_name,
                'old_owner_name': old_owner_name,
            }
            asset_move_lines.append(asset_move_line)

        docargs = {
            'doc_ids': docids,
            'doc_model': report.model,
            'docs': asset_move,
            'lines': asset_move_lines,
            'old_owner': '',
            'date': datetime.now().strftime("%Y-%m-%d"),
            'assigned_to': '',
            'checked_by': '',
            'page_no': asset_move.name
        }
        return report_obj.render('l10n_mn_account_asset.print_account_asset_moves', docargs)
