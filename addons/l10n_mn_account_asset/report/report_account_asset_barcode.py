# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2016 Asterisk Technologies LLC Co.,ltd
#    (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462
#
##############################################################################
from odoo import api, models
from odoo.tools.translate import _
from operator import itemgetter
import sys

class ReportAccountAssetBarcode(models.AbstractModel):
    _name = 'report.l10n_mn_account_asset.account_asset_templates'

    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        asset_obj = self.env['account.asset.asset']            
        asset = asset_obj.browse(docids)
        report = report_obj._get_report_from_name('l10n_mn_account_asset.account_asset_templates')        
        datas = {}        
        docargs = {
            'doc_ids': docids,            
            'docs': asset,
            'doc_model': report.model,
        }
        return report_obj.render('l10n_mn_account_asset.account_asset_templates', docargs)