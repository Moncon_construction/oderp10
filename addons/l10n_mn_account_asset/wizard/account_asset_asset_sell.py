# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from datetime import datetime


class AccountAssetAssetSell(models.TransientModel):
    _name = 'account.asset.asset.sell'
    _description = 'Sell asset'

    account_analytic_id = fields.Many2one('account.analytic.account', 'Analytic account')
    partner_id = fields.Many2one('res.partner', 'Partner', required=True)
    account_id = fields.Many2one('account.account', 'Account', required=True, domain=[('internal_type', '=', 'receivable')])
    require_analytic = fields.Boolean(related="account_id.req_analytic_account")
    journal_id = fields.Many2one('account.journal', 'Sell journal', required=True, domain=[('type', '=', 'sale')])
    date = fields.Date(string='Depreciation date', default=fields.Date.context_today, required=True)
    journal_date = fields.Date(string='Journal date', default=fields.Date.context_today, required=True)
    description = fields.Text('Transaction Description', required=True)
    line_ids = fields.One2many('account.asset.asset.sell.line', 'sell_id', 'Sell asset')

    @api.onchange('date')
    def onchange_date(self):
        for line in self.line_ids:
            [res] = line.asset_id.get_depr_residual(self.date)
            if res:
                line.value_residual = res
                line.sell_value = line.value_residual

    @api.onchange('partner_id')
    def onchange_partner(self):
        if self.partner_id:
            self.account_id = self.partner_id.property_account_receivable_id

    @api.model
    def default_get(self, default_fields):
        '''Хөрөнгүүдийг олноор нь сонгон боловсруулах үед мөр бүрт мэдээллийг оруулах '''
        res = super(AccountAssetAssetSell, self).default_get(default_fields)
        context = dict(self._context or {})
        move_vals = []
        if context.get('active_model') == 'account.asset.asset' and context.get('active_ids'):
            assets = self.env[context.get('active_model')].browse(context.get('active_ids'))
            for asset in assets:
                move_vals.append((0, False, {'asset_id': asset.id,
                                             'value': asset.value,
                                             'value_residual': asset.value_residual,
                                             'sell_value': asset.value_residual}))
        if context.get('active_model') == 'account.asset.close.wizard' and context.get('active_ids'):
            wizards = self.env[context.get('active_model')].browse(context.get('active_ids'))
            for wizard in wizards:
                if wizard.asset_id:
                    move_vals.append((0, False, {'asset_id': wizard.asset_id.id,
                                                 'value': wizard.asset_id.value,
                                                 'value_residual': wizard.asset_id.value_residual,
                                                 'sell_value': wizard.asset_id.value_residual}))
        res.update({'line_ids': move_vals})
        return res

    @api.multi
    def compute(self):
        self.ensure_one()
        """ Хөрөнгө борлуулах """
        line_vals = []
        product_ids = []
        for line in self.line_ids:
            value_residual = line.value_residual
            asset = line.asset_id
            """ Борлуулах огноонд тохирох элэгдлийн самбарын мөр батлагдсан эсэхийг шалгах үүднээс"""
            asset.search_and_raise_line(self.date)
            if asset.state != 'open':
                raise UserError(_("%s asset state is not open. Only can sell assets with open state!" % asset.name))
            if not asset.company_id.expense_account_asset:
                raise UserError(_("Configure expense account of sell asset in %s company!" % asset.company_id.name))
            if not asset.company_id.income_account_asset:
                raise UserError(_("Configure income account of sell asset in %s company!") % asset.company_id.name)
            """ Элэгдлийн самбар дээрээс элэгдүүлэхээр тооцоолж хөрөнгийн үлдэгдэл дүнгээс хассан дүнг тооцоолох """
            substract_value = asset.value_residual - line.value_residual
            asset.with_context(date=self.date, value=substract_value).compute_depreciation_board()
            if asset.product_id.id not in product_ids or not asset.product_id:
                if line.asset_id.product_id:
                    product_ids.append(line.asset_id.product_id.id)
                """ Борлуулах хөрөнгийн нэхэмжлэл үүсэх """
                line_vals.append(line._get_invoice_line_vals(asset, self))
            else:
                """ Хөрөнгийн бараа болон зарах үнэ ижил бол нэхэмжлэлийн мөрийн тоо ширхэг дээр нэмнэ"""
                for val in line_vals:
                    if val[2]['product_id'] == asset.product_id.id and val[2]['price_unit'] == line.sell_value:
                        val[2]['quantity'] += 1
        invoice_vals = {
            'partner_id': self.partner_id.id,
            'date_invoice': self.journal_date,
            'user_id': self.env.uid,
            'company_id': self.env.user.company_id.id,
            'currency_id': self.env.user.company_id.currency_id.id,
            'account_id': self.account_id.id,
            'journal_id': self.journal_id.id,
            'invoice_line_ids': line_vals
        }
        invoice_id = self.env['account.invoice'].create(invoice_vals)
        """ Нэхэмжлэхийг баталж холбоотой журналын бичилт үүсэх """
        invoice_id.action_invoice_open()
        """ Хөрөнгийг хааж холбоотой бичилтийг үүсгэх """
        move_obj = self.env['account.move']
        for line in self.line_ids:
            asset = line.asset_id
            move_id = move_obj.create(self._get_account_move_vals(line, value_residual))
            if self.journal_id.auto_approve:
                move_id.post()
            asset.create_history(self.journal_date, 'sale', self.description, asset.method_end, line.sell_value - line.value_residual, {}, False, move_id)
            asset.set_to_close()
            asset.closed_date = self.journal_date
        return {'type': 'ir.actions.act_window_close'}

    # Хөрөнгө борлуулахад үүсэх журналын бичилтийн утга буцаах функц
    @api.multi
    def _get_account_move_vals(self, line, value_residual):
        invoice_line_obj = self.env['account.invoice.line']
        asset = line.asset_id
        m_line_vals = [(0, 0, {
            'name': self.description,
            'ref': line.asset_id.name,
            'account_id': asset.account_asset_id.id,
            'debit': (line.value < 0 and -line.value) or 0,
            'credit': (line.value > 0 and line.value) or 0,
            'journal_id': self.journal_id.id,
            'currency_id': asset.currency_id.id,
            'date': self.journal_date,
            'asset_id': asset.id
        }), (0, 0, {
            'name': self.description,
            'ref': asset.name,
            'account_id': asset.category_id.account_depreciation_id.id,
            'debit': (line.value - line.value_residual > 0 and line.value - line.value_residual) or 0,
            'credit': (line.value - line.value_residual < 0 and -line.value - line.value_residual) or 0,
            'journal_id': self.journal_id.id,
            'currency_id': asset.currency_id.id,
            'date': self.journal_date,
            'asset_id': asset.id
        })]
        inv_line = invoice_line_obj.search([('asset_id', '=', asset.id)])
        if inv_line:
            inv_line = inv_line[0]
        m_line_vals.append((0, 0, {
            'name': self.description,
            'ref': asset.name,
            'account_id': self.journal_id.default_credit_account_id.id,
            'debit': (inv_line.price_subtotal > 0 and inv_line.price_subtotal) or 0,
            'credit': (inv_line.price_subtotal < 0 and -inv_line.price_subtotal) or 0,
            'journal_id': self.journal_id.id,
            'currency_id': asset.currency_id.id,
            'date': self.journal_date,
            'asset_id': asset.id
        }))
        if line.tax_id:
            debit = credit = 0.0
            account_id = asset.company_id.expense_account_asset
            if line.sell_value == line.asset_id.value_residual:
                debit = line.sell_value - inv_line.price_subtotal
            elif line.sell_value > line.asset_id.value_residual:
                credit = inv_line.price_subtotal - line.asset_id.value_residual
                account_id = asset.company_id.income_account_asset
                if credit < 0:
                    account_id = asset.company_id.expense_account_asset
                    debit = - credit
                    credit = 0
            else:
                debit = line.asset_id.value_residual - inv_line.price_subtotal
            m_line_vals.append((0, 0, {
                'name': self.description,
                'ref': asset.name,
                'account_id': account_id.id,
                'debit': debit,
                'credit': credit,
                'journal_id': self.journal_id.id,
                'currency_id': asset.currency_id.id,
                'date': self.journal_date,
                'asset_id': asset.id
            }))
        else:
            if value_residual != asset.value_residual:
                debit = credit = 0
                if asset.value_residual - line.sell_value > 0:
                    debit = asset.value_residual - line.sell_value
                else:
                    credit = line.sell_value - asset.value_residual
                m_line_vals.append((0, 0, {
                    'name': self.description,
                    'ref': asset.name,
                    'account_id': asset.company_id.income_account_asset.id if value_residual > asset.value_residual else asset.company_id.expense_account_asset.id,
                    'debit': debit,
                    'credit': credit,
                    'journal_id': self.journal_id.id,
                    'currency_id': asset.currency_id.id,
                    'date': self.journal_date,
                    'asset_id': asset.id
                }))
        move_vals = {
            'name': asset.journal_id.code + '/' + str(datetime.strptime(self.journal_date, '%Y-%m-%d').year) + '/' + self.description,
            'date': self.journal_date,
            'ref': self.description,
            'journal_id': asset.journal_id.id,
            'line_ids': m_line_vals
        }
        return move_vals


class AccountAssetAssetSellLine(models.TransientModel):
    _name = 'account.asset.asset.sell.line'
    _description = 'Sell asset'

    def _default_asset(self):
        if 'active_id' in self._context:
            return self.env['account.asset.asset'].browse(self._context.get('active_id'))
        return False

    def _default_sell_value(self):
        if self.asset_id:
            return self.asset_id.value_residual
        else:
            if 'active_id' in self._context:
                asset_id = self.env['account.asset.asset'].browse(self._context.get('active_id'))
                return asset_id.value_residual
            return False

    sell_id = fields.Many2one('account.asset.asset.sell', 'Sell asset')
    asset_id = fields.Many2one('account.asset.asset', 'Asset', default=_default_asset, required=True)
    tax_id = fields.Many2one('account.tax', 'Sell tax', domain=[('type_tax_use', '=', 'sale')])
    value = fields.Float(related='asset_id.value')
    value_residual = fields.Float(related='asset_id.value_residual')
    sell_value = fields.Float('Sell value', required=True, default=_default_sell_value)

    @api.onchange('tax_id')
    def onchange_tax(self):
        if self.tax_id and not self.tax_id.account_id:
            raise UserError(_("Configure tax account on %s tax!" % self.tax_id.name))

    @api.onchange('asset_id')
    def onchange_asset(self):
        if self.asset_id:
            self.sell_value = self.asset_id.value_residual
            self.sell_id.onchange_date()

    @api.multi
    def _get_invoice_line_vals(self, asset, sell):
        self.ensure_one()
        return (0, 0, {
            'product_id': asset.product_id.id if asset.product_id else [],
            'name': '%s %s' % (sell.description, asset.product_id.name if asset.product_id else ''),
            'account_id': sell.journal_id.default_credit_account_id.id,
            'account_analytic_id': sell.account_analytic_id.id if sell.account_analytic_id else False,
            'quantity': 1,
            'price_unit': self.sell_value,
            'invoice_line_tax_ids': [[6, 0, [self.tax_id.id]]] if self.tax_id else False,
            'asset_id': self.asset_id.id,
        })


class AccountAssetAssetSellOpen(models.TransientModel):
    _name = 'account.asset.asset.sell.open'
    _description = "Open asset all"

    line_ids = fields.One2many('account.asset.asset.sell.open.line', 'open_id', 'Open asset')

    @api.model
    def default_get(self, default_fields):
        '''Хөрөнгүүдийг олноор нь сонгон нээх үед мөр бүрт мэдээллийг оруулах '''
        res = super(AccountAssetAssetSellOpen, self).default_get(default_fields)
        context = dict(self._context or {})
        move_vals = []
        if context.get('active_model') == 'account.asset.asset' and context.get('active_ids'):
            assets = self.env[context.get('active_model')].browse(context.get('active_ids'))
            for asset in assets:
                move_vals.append((0, False, {'asset_id': asset.id,
                                             'value': asset.value,
                                             'value_residual': asset.value_residual,
                                             'state': asset.state}))
        res.update({'line_ids': move_vals})
        return res

    @api.multi
    def open(self):
        for sell in self:
            for line in sell.line_ids:
                line.asset_id.set_to_open()


class AccountAssetAssetSellOpenLine(models.TransientModel):
    _name = 'account.asset.asset.sell.open.line'
    _description = 'Open asset all'

    def _default_asset(self):
        if 'active_id' in self._context:
            return self.env['account.asset.asset'].browse(self._context.get('active_id'))
        return False

    open_id = fields.Many2one('account.asset.asset.sell.open', 'Open asset', ondelete='cascade')
    asset_id = fields.Many2one('account.asset.asset', 'Asset', default=_default_asset, required=True, ondelete='cascade')
    value = fields.Float(related='asset_id.value')
    value_residual = fields.Float(related='asset_id.value_residual')
    state = fields.Selection(related='asset_id.state', readonly=True)
