# -*- coding: utf-8 -*-
import time
from odoo import api, fields, models, _
from datetime import datetime, date
from odoo.exceptions import ValidationError


class AccountAssetClose(models.TransientModel):
    _name = "account.asset.close.wizard"
    _description = "Close Asset Wizard"

    def _domain_account(self):
        account_ids = self.env['account.account'].search([('internal_type', 'in', ['expense', 'receivable', 'payable', 'other'])])
        if account_ids:
            return [('id', 'in', account_ids.ids)]
        else:
            return False

    asset_id = fields.Many2one('account.asset.asset', 'Asset')
    value_residual = fields.Float('Residual Value')
    value = fields.Float('Asset Value')
    currency_id = fields.Many2one('res.currency', 'Currency')
    date = fields.Date('Closing Date')
    name = fields.Char('Transaction Description')
    income_account_id = fields.Many2one('account.account', 'Income Account')
    account_id = fields.Many2one('account.account', 'Closing Expense Account', domain=_domain_account)
    require_analytic = fields.Boolean(related="account_id.req_analytic_account", default=False)
    partner_id = fields.Many2one('res.partner', 'Partner')
    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic account')
    substract_value = fields.Float('Value', required=True)
    type_of_close = fields.Selection([('sell', 'To sell'),
                                      ('scrap', 'To scrap')], string="type_of_close", required=True, default="sell")
    company_id = fields.Many2one(related="asset_id.company_id", string="Company", readonly=True, store=True)

    @api.model
    def default_get(self, default_fields):
        context = dict(self._context or {})
        res = super(AccountAssetClose, self).default_get(default_fields)
        if context['active_model'] == 'account.asset.asset':
            asset = self.env['account.asset.asset'].browse(context['active_id'])
            if 'asset_id' in default_fields:
                res['asset_id'] = asset.id
            if 'value_residual' in default_fields:
                res['value_residual'] = asset.value_residual
            if 'value' in default_fields:
                res['value'] = asset.value
            if 'currency_id' in default_fields:
                res['currency_id'] = asset.currency_id.id
            if 'date' in default_fields:
                res['date'] = time.strftime('%Y-%m-%d')
            if 'name' in default_fields:
                res['name'] = _('%s asset write off') % asset.name_get()[0][1]
            if 'analytic_account_id' in default_fields:
                if asset.account_analytic_id:
                    res['analytic_account_id'] = asset.account_analytic_id.id
        return res

    @api.onchange('partner_id')
    def onchange_partner(self):
        acc_id = False
        if self.partner_id and self.partner_id.property_account_receivable_id:
            return {'value': {'income_account_id': self.partner_id.property_account_receivable_id.id}}
        else:
            return False

    @api.onchange('date')
    def onchange_date(self):
        line = ''
        if self.asset_id:
            line = self.asset_id.search_and_raise_line(self.date)
        # Сонгосон огноонд тохирох элэгдлийн самбарын мөрийг олж элэгдлийн дүнгээс нь нэг өдрийн элэгдлийг тооцоолж сонгосон өдөр хүртэлх өдрөөр үржүүлэн үлдэгдэл дүнгээс хасах
        if line:
            date = datetime.strptime(self.date, '%Y-%m-%d')
            dates = line.asset_id.last_day_of_month(datetime.strptime(line.depreciation_date, '%Y-%m-%d'))
            depreciate_value = line.amount / dates.day * date.day
            self.value_residual = line.asset_id.value_residual - depreciate_value
            self.substract_value = depreciate_value
            line.depreciation_date = self.date

    @api.multi
    def process(self):
        for form in self:
            form.asset_id.search_and_raise_line(form.date)
            form.asset_id.with_context(date=form.date, value=form.substract_value).compute_depreciation_board()
            move_id = form.asset_id.write_off(form.date, form.account_id.id, form.analytic_account_id.id, form.name, self._context)
            form.asset_id.create_history(form.date, 'close', form.name, form.asset_id.method_end, 0, {}, False, move_id[0] if move_id else False)
            form.asset_id.set_to_close()
            form.asset_id.closed_date = self.date
            form.asset_id.type_of_close = self.type_of_close
        return True
