# -*- coding: utf-8 -*-
import time
from odoo import api, fields, models, _
from datetime import datetime, date
from odoo.exceptions import ValidationError


class AccountAssetPickingDepreciationCancel(models.TransientModel):
    _name = "account.asset.depreciation.cancel"
    _description = "Account asset picking depreciation cancel"

    def _default_period(self):
        period_obj = self.env['account.period'].search([('date_start','<=',datetime.today()),
                                                        ('date_stop','>=',datetime.today()),
                                                        ('company_id','=',self.env.user.company_id.id)])
        return period_obj

    period = fields.Many2one('account.period', 'Account Period', default=_default_period, domain=[('state', 'not in', ['done'])])
    asset_ids = fields.Many2many('account.asset.asset', 'account_asset_depreciation_cancel_rel', 'wizard_id', 'line_id', 'Assets', domain=[('state', '=', 'open'),('depreciation_line_ids.move_check', '=', True)])


    @api.onchange('period')
    def onchange_period(self):
        lines = self.env['account.asset.depreciation.line'].search([('depreciation_date','=',self.period.date_stop),('move_check','=',True)])
        asset_ids = []
        for line in lines:
            asset_ids.append(line.asset_id.id)
        return {'domain':{'asset_ids':[('id','in',asset_ids),('state','=','open')]}}


    @api.multi
    def asset_depreciation_cancel(self):
        for asset_id in self.asset_ids:
            for line in asset_id.depreciation_line_ids:
                if str(self.period.date_stop) == str(line.depreciation_date):
                    line.cancel_move()
