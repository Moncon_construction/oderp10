# -*- coding: utf-8 -*-
import time
from odoo import api, fields, models, _
from datetime import datetime, date
from odoo.exceptions import ValidationError


class AssetFreeze(models.TransientModel):
    _name = 'account.asset.freeze'
    _description = 'Freeze asset depreciation'

    freeze_line_ids = fields.Many2many('account.asset.depreciation.line', 'account_asset_freeze_line_rel', 'wizard_id', 'line_id', string='Tobe freezed depreciation lines', required=True)
    asset_id = fields.Many2one('account.asset.asset', 'Asset', readonly=True)

    @api.model
    def default_get(self, fields):
        result = super(AssetFreeze, self).default_get(fields)
        if self._context.get('active_id'):
            asset_obj = self.env['account.asset.asset']
            asset = asset_obj.browse(self._context['active_id'])
        result.update({'asset_id': asset.id})
        return result

    def do_freeze(self):
        for line in self.freeze_line_ids:
            module = self.sudo().env['ir.module.module'].search(
                [('name', '=', 'l10n_mn_account_asset_tax_depr'), ('state', 'in', ('installed', 'to upgrade'))])
            if module:
                line.write({'is_freeze': True, 'amount': 0, 'amount_tax': 0, 'depreciated_value': 0, 'depreciated_value_tax': 0, 'remaining_value': 0, 'remaining_value_tax': 0})
            else:
                line.write({'is_freeze': True, 'amount': 0, 'depreciated_value': 0, 'remaining_value': 0})
        self.asset_id.compute_depreciation_board()
        return True