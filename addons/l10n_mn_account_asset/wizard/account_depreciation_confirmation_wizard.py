# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from datetime import datetime, timedelta

class AssetDepreciationConfirmationWizard(models.TransientModel):
    _inherit = "asset.depreciation.confirmation.wizard"
    
    def _default_period(self):
        period_obj = self.env['account.period'].search([('date_start','<=',datetime.today()),
                                                        ('date_stop','>=',datetime.today()),
                                                        ('company_id','=',self.env.user.company_id.id)])
        return period_obj
    
    period = fields.Many2one('account.period', 'Account Period', default=_default_period, help="Choose the period for which you want to automatically post the depreciation lines of running assets",domain=[('state','not in',['done'])])
    asset_ids = fields.Many2many('account.asset.asset', 'account_asset_depreciation_conf_rel', 'wizard_id', 'line_id', 'Assets', domain=[('state','=','open')])
    is_depriciate_category = fields.Boolean('Is depriciate by category', default=False)
    line_ids = fields.One2many('asset.depreciation.confirmation.wizard.line', 'wizard_id', 'Asset categories')
    
    @api.onchange('period')
    def _get_asset_domain(self):
        domain = {}
        
        account_asset_lines = self.env['account.asset.depreciation.line'].search([('move_check', '=', True),  
                                                                                  ('depreciation_date', '>=', self.period.date_start), ('depreciation_date', '<=', self.period.date_stop)])
       
        if account_asset_lines and len(account_asset_lines) > 0:
            found_ids = []
            for account_asset_line in account_asset_lines:
                found_ids.append(account_asset_line.asset_id.id)
            domain['asset_ids'] = [('id', 'not in', found_ids),('state','=','open')]
        else:
            domain['asset_ids'] = [('state','=','open')]
            
        return {'domain': domain}

    @api.multi
    def asset_compute(self):
        self.ensure_one()
        context = self._context
        if self.is_depriciate_category:
            ids = cat_ids = []
            for line in self.line_ids:
                cat_ids.append(line.category_id.id)
            if cat_ids:
                ids = self.env['account.asset.category'].browse(cat_ids)
        else:
            ids = self.asset_ids
        created_move_ids = self.env['account.asset.asset'].compute_generated_entries(self.period.date_start, self.period.date_stop, asset_type=context.get('asset_type'), condition=self.is_depriciate_category, ids=ids)
        return {
            'name': _('Created Asset Moves') if context.get('asset_type') == 'purchase' else _('Created Revenue Moves'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move',
            'view_id': False,
            'domain': "[('id','in',[" + ','.join(map(str, created_move_ids)) + "])]",
            'type': 'ir.actions.act_window',
        }
        
class AssetDepreciationConfirmationWizardLine(models.TransientModel):
    _name = "asset.depreciation.confirmation.wizard.line"
    
    wizard_id = fields.Many2one('asset.depreciation.confirmation.wizard', 'Confirmation wizard')
    category_id = fields.Many2one('account.asset.category', 'Asset')
    journal_id = fields.Many2one(related="category_id.journal_id")
    method = fields.Selection(related="category_id.method")