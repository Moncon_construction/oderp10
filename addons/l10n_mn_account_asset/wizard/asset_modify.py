# -*- coding: utf-8 -*-
import time
from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF


class AssetModify(models.TransientModel):
    _inherit = 'asset.modify'

    action = fields.Selection([('capital', 'Capitalization'),
                               ('revaluation', 'Revaluation')], 'Action', default='capital')
    capital_invoice_line_id = fields.Many2one('account.invoice.line', ondelete='restrict')
    capital_date = fields.Date('Capital Date', default=lambda self: fields.Date.today())
    capital_amount = fields.Float('Capital Amount', help='If you want to increase asset value then set capital amount on this field')
    revaluation_amount = fields.Float('Revaluation Amount')
    revaluation_date = fields.Date('Revaluation Date', default=lambda self: fields.Date.today())
    revaluation_type = fields.Selection([('gross_amount', 'Revaluation for Gross Amount'),
                                         ('depreciation_amount', 'Revaluation for Depreciated Amount')], 'Revaluation Method', default='gross_amount')
    capital_type = fields.Selection([('invoice_line', 'Capital with an invoice line'), ('account', 'Capital with an account')], string='Capital type')
    account_id = fields.Many2one('account.account', 'Account', domain=[('journal_entries_selectable', '=', False)])
    company_id = fields.Many2one('res.company', string="Company", readonly=0)

    @api.onchange('capital_invoice_line_id')
    def onchange_invoice(self):
        for asset in self:
            if asset.capital_invoice_line_id:
                asset.capital_date = asset.capital_invoice_line_id.invoice_id.date_invoice
                asset.capital_amount = asset.capital_invoice_line_id.price_subtotal

    @api.onchange('capital_type')
    def onchange_capital_type(self):
        res = {}
        if self.capital_type == 'invoice_line':
            self.env.cr.execute("SELECT capital_invoice_line_id FROM account_asset_history WHERE capital_invoice_line_id IS NOT NULL")
            invoice_lines = self.env.cr.fetchall()
            if len(invoice_lines) > 0:
                res = {'domain': {'capital_invoice_line_id': [('invoice_id.type', '=', 'in_invoice'), ('invoice_id.state', 'in', ('paid', 'open')),
                                                              ('id', 'not in', [x[0] for x in invoice_lines]), ('company_id', '=', self.env.user.company_id.id)]}}
            else:
                res = {'domain': {'capital_invoice_line_id': [('invoice_id.type', '=', 'in_invoice'), ('invoice_id.state', 'in', ('paid', 'open')),
                                                              ('company_id', '=', self.env.user.company_id.id)]}}
        return res

    # Хугацааны аргын сууриас хамаарч Элэгдэх тоог авч байсныг Элэгдэх тоо (өдрөөр) сонголтыг нэмэв
    @api.model
    def default_get(self, fields):
        res = super(AssetModify, self).default_get(fields)
        asset_id = self.env.context.get('active_id')
        asset = self.env['account.asset.asset'].browse(asset_id)
        if 'company_id' in fields:
            res.update({'company_id': asset.company_id.id})
        if 'method_number' in fields and (asset.method_time == 'number' or asset.method_time == 'number_day'):
            res.update({'method_number': asset.method_number})
        if 'method_end' in fields:
            res.update({'method_end': asset.method_end})
        return res

    @api.multi
    def modify(self):
        """ Modifies the duration of asset for calculating depreciation
        and maintains the history of old values, in the chatter.
        """
        for modify in self:
            asset_id = self.env.context.get('active_id', False)
            asset = self.env['account.asset.asset'].browse(asset_id)
            currency_obj = self.env['res.currency']
            move_obj = self.env['account.move']
            move_id = False
            old_values = {
                'value': asset.value,
                'method_number': asset.method_number,
                'method_period': asset.method_period,
                'method_end': asset.method_end,
                'value': asset.value
            }
            asset_vals = {
                'method_number': modify.method_number,
                'method_period': modify.method_period,
                'method_end': modify.method_end,
            }
            if asset.method_end:
                end = asset.method_end
            elif modify.method_end:
                end = modify.method_end
            else:
                if asset.depreciation_line_ids:
                    end = self.env['account.asset.depreciation.line'].browse(max(asset.depreciation_line_ids.ids)).depreciation_date
                else:
                    end = self.capital_date
            # Элэгдэл өөрчлөх огнооноос өмнөх элэгдлийн бичиилтүүд үүссэн эсэхийг шалгах
            modify_date = datetime.strptime(modify.capital_date if modify.action == 'capital' else modify.revaluation_date, DF).date()
            if asset.method_number > 0:
                current_currency = asset.currency_id.id or asset.company_id.currency_id.id
                asset_amount = currency_obj.compute_currency(current_currency, asset.company_id.currency_id.id, asset.value or 0)
                dep_amount = 0
                dep_date = False
                for dep_line in asset.depreciation_line_ids:
                    if dep_line.move_check:
                        if not dep_date or (dep_date and dep_date < dep_line.depreciation_date):
                            dep_date = dep_line.depreciation_date
                        dep_amount += dep_line.amount
                if current_currency != asset.company_id.currency_id.id:
                    depreciation_amount = currency_obj.compute_currency(current_currency, asset.company_id.currency_id.id, dep_amount or 0)
                else:
                    depreciation_amount = dep_amount
                if not dep_date:
                    dep_date = asset.date
                dep_date = datetime.strptime(dep_date, DF).date()
                if dep_date > modify_date:
                    raise UserError(_("You cannot modify. %s cancel depreciation %s after." % (asset.name, modify_date)))
                elif dep_date < modify_date:
                    month = (modify_date.month - dep_date.month) + (modify_date.year - dep_date.year) * 12
                    if month > 1 and depreciation_amount != asset_amount:
                        raise UserError(_("You cannot modify. %s validate depreciation %s before." % (asset.name, modify_date)))
            if modify.action == 'capital':
                amount = modify.capital_amount
                if modify.capital_invoice_line_id:
                    invoice_date = datetime.strptime(modify.capital_invoice_line_id.invoice_id.date_invoice, DF).date()
                    if amount > modify.capital_invoice_line_id.price_subtotal:
                        raise UserError(_('Capital amount cannot greater than invoice line subtotal!'))
                    if modify.capital_invoice_line_id and modify_date < invoice_date:
                        raise UserError(_('Capital date cannot less than invoice line date!'))
                if amount <= 0:
                    raise UserError(_('Capital amount cannot 0!'))
                asset_vals['value'] = modify.capital_amount + asset.value
                line_ids = modify._get_capital_move_line_vals(asset)
                move_vals = {
                    'name': asset.journal_id.code + '/' + str(datetime.strptime(modify.capital_date, '%Y-%m-%d').year) + '/' + asset.name + ' (' + modify.name + ')',
                    'date': modify.capital_date,
                    'ref': asset.name,
                    'journal_id': asset.journal_id.id,
                    'line_ids': line_ids,
                }
                move_id = move_obj.create(move_vals)
            else:
                if modify.revaluation_amount < asset.value_depreciated:
                    raise UserError(_('Revaluation amount cannot less than asset depreciated value!'))
                amount = modify.revaluation_amount - asset.value
                account_db = account_cr = False
                asset_vals['value'] = modify.revaluation_amount
                diff_depreciate = asset.value_depreciated / asset.value * modify.revaluation_amount - asset.value_depreciated
                if modify.revaluation_type == 'gross_amount' and modify.revaluation_amount != asset.value:
                    if modify.revaluation_amount > asset.value:
                        if not asset.company_id.addition_account_reval_asset:
                            raise UserError(_('Configure addition account of asset revaluation on %s company!' % asset.company_id.name))
                        account_db = asset.account_asset_id
                        account_cr = asset.company_id.addition_account_reval_asset
                    else:
                        if not asset.company_id.deduction_account_reval_asset:
                            raise UserError(_('Configure deduction account of asset revaluation on %s company!' % asset.company_id.name))
                        account_db = asset.company_id.deduction_account_reval_asset
                        account_cr = asset.account_asset_id
                elif modify.revaluation_type == 'depreciation_amount' and diff_depreciate != 0:
                    if diff_depreciate > 0:
                        account_db = asset.account_analytic_id if asset.account_analytic_id else asset.category_id.account_analytic_id
                        account_cr = asset.category_id.account_depreciation_id
                    else:
                        account_db = asset.category_id.account_depreciation_expense_id
                        account_cr = asset.account_analytic_id if asset.account_analytic_id else asset.category_id.account_analytic_id
                if account_db and account_cr:
                    line_ids = [(0, 0, {
                        'name': modify.name,
                        'ref': asset.name,
                        'account_id': account_db.id,
                        'debit': abs(modify.revaluation_amount - asset.value),
                        'credit': 0.0,
                        'journal_id': asset.journal_id.id,
                        'currency_id': asset.currency_id.id,
                        'date': modify.revaluation_date,
                        'asset_id': asset.id,
                    }), (0, 0, {
                        'name': modify.name,
                        'ref': asset.name,
                        'account_id': account_cr.id,
                        'debit': 0.0,
                        'credit': abs(modify.revaluation_amount - asset.value),
                        'journal_id': asset.journal_id.id,
                        'currency_id': asset.currency_id.id,
                        'date': modify.revaluation_date,
                        'asset_id': asset.id,
                    })]
                    move_vals = {
                        'name': asset.journal_id.code + '/' + str(datetime.strptime(modify.capital_date, '%Y-%m-%d').year) + '/' + asset.name + ' (' + modify.name + ')',
                        'date': modify.revaluation_date,
                        'ref': asset.name,
                        'journal_id': asset.journal_id.id,
                        'line_ids': line_ids,
                    }
                    move_id = move_obj.create(move_vals)
            asset.write(asset_vals)
            asset.compute_depreciation_board()
            tracked_fields = self.env['account.asset.asset'].fields_get(['value', 'method_number', 'method_period', 'method_end'])
            changes, tracking_value_ids = asset._message_track(tracked_fields, old_values)
            if changes:
                asset.message_post(subject=_('Depreciation board modified'), body=modify.name, tracking_value_ids=tracking_value_ids)
            asset.create_history(self.capital_date if self.action == 'capital' else self.revaluation_date, modify.action, modify.name, end,
                                 amount, old_values, modify.capital_invoice_line_id and modify.capital_invoice_line_id.id or False, move_id)
            return {'type': 'ir.actions.act_window_close'}

    @api.multi
    def _get_capital_move_line_vals(self, asset):
        self.ensure_one()
        return [(0, 0, {
            'name': self.name,
            'ref': asset.name,
            'account_id': asset.category_id.account_asset_id.id,
            'debit': self.capital_amount,
            'credit': 0.0,
            'journal_id': asset.journal_id.id,
            'currency_id': asset.currency_id.id,
            'date': self.capital_date,
            'asset_id': asset.id,
        }), (0, 0, {
            'name': self.name,
            'ref': asset.name,
            'account_id': self.capital_invoice_line_id.account_id.id if self.capital_type == 'invoice_line' else self.account_id.id,
            'debit': 0.0,
            'credit': self.capital_amount,
            'journal_id': asset.journal_id.id,
            'currency_id': asset.currency_id.id,
            'date': self.capital_date,
            'asset_id': asset.id,
        })]
