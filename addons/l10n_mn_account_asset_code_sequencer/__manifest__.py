# -*- coding: utf-8 -*-
{
    'name': "Mongolian Account Asset Code Sequencer",
    'version': '1.0',
    'depends': ['l10n_mn_account_asset'],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Account Asset Modules',
    'description': """
         Санхүүгийн цэсний Хөрөнгө хэсгийн Хөрөнгө автоматаар дугаарлах хэсгийг чагтласнаар энэхүү модуль суух бөгөөд
         Хөрөнгийн дугаарыг автоматаар дугаарлах болно.
    """,
    'website' : 'http://asterisk-tech.mn',
    'data': [
        'data/account_asset_sequence.xml',
        'views/account_asset_code_view.xml',
    ]
}

