# -*- coding: utf-8 -*-
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from odoo.modules.module import get_module_resource
from odoo.exceptions import ValidationError, UserError
from odoo import api, fields, models, _


class AccountAssetCode(models.Model):
    _inherit = 'account.asset.asset'

    @api.model
    def create(self, vals):
        # Хөрөнгийн дугаарыг автоматаар авч байна
        category = self.env['account.asset.category'].search([('id', '=', vals['category_id'])], limit=1)
        if category:
            if not category.asset_code_sequence_id:
                #            category нь сонгосон ангилалыг хадгалаж байгаа бөгөөд хэрэв тухайн ангилал кодгүй болон дараалалгүй бол
                #            write функц автоматаар код болон дараалал авна.
                category.write({})
            #                 Ангилалын кодыг дарааллын кодын хамт өгч байна
            vals['code'] = category.category_code + self.env["ir.sequence"].next_by_code(category.asset_code_sequence_id.code)
        return super(AccountAssetCode, self).create(vals)

    @api.multi
    def write(self, vals):
        if vals.get('category_id'):
            category = self.env['account.asset.category']
            if 'category_id' in vals:
                category = category.search([('id', '=', vals['category_id'])], limit=1)
            else:
                category = self.category_id
            #       category нь сонгосон ангилалыг хадгалаж байгаа бөгөөд хэрэв тухайн ангилал кодгүй болон дараалалгүй бол
            #       write функц автоматаар код болон дараалал авна.
            category.write({})
            #       Ангилал өөрчлөгдсөн тул кодыг шинэчилж байна
            if category.category_code:
                if 'code' in vals:
                    vals['code'] = category.category_code + self.env["ir.sequence"].next_by_code(category.asset_code_sequence_id.code)
        return super(AccountAssetCode, self).write(vals)


class AccountAssetCategoryCode(models.Model):
    _inherit = 'account.asset.category'

    category_code = fields.Char()
    asset_code_sequence_id = fields.Many2one('ir.sequence', readonly=True, store=True, string="Category Sequence")

    _sql_constraints = [
        ('category_code_unique', 'unique(category_code)', 'Category code must be unique!'),
    ]

    @api.model
    def create(self, vals):
        categ_code = self.env["ir.sequence"].next_by_code("mongolian.account.asset.sequence")
        #       Утгууд дунд category_code байгаа эсэхийг шалгаж байвал давхцаж буй эсэхийг шалгана.
        if 'category_code' in vals:
            if vals['category_code']:
                categ_code = vals['category_code']
            #       Тухайн ангилалын өөрийн дарааллыг үүсгэж байна.
        seq_obj = self.env['ir.sequence']
        seq_vals = {
            'padding': 4,
            'code': categ_code,
            'name': ('Asset(%s)' % vals['name']),
            'implementation': 'standard',
            'company_id': 1,
            'use_date_range': False,
            'number_increment': 1,
            'prefix': False,
            'date_range_ids': [],
            'number_next_actual': 0,
            'active': True,
            'suffix': False
        }
        seq_id = seq_obj.sudo().create(seq_vals)

        vals.update({'asset_code_sequence_id': seq_id.id, 'category_code': categ_code})
        return super(AccountAssetCategoryCode, self).create(vals)

    @api.multi
    def write(self, vals):
        #         Ангилалын name эсвэл category_code талбар өөрчлөгдөхөд тус ангилалын дарааллын
        #         талбарууд мөн өөрчлөгддөг болгож өгч байна.

        seq_id = self.asset_code_sequence_id
        seq_vals = {}
        if not self.category_code and not 'category_code' in vals:
            vals['category_code'] = self.env["ir.sequence"].next_by_code("mongolian.account.asset.sequence")
        else:
            if 'category_code' in vals and not vals['category_code']:
                vals['category_code'] = self.env["ir.sequence"].next_by_code("mongolian.account.asset.sequence")
        if not seq_id:
            seq_obj = self.env['ir.sequence']
            seq_vals = {
                'padding': 4,
                'code': ('%s' % vals['category_code'] if 'category_code' in vals else self.category_code),
                'implementation': 'standard',
                'company_id': 1,
                'use_date_range': False,
                'number_increment': 1,
                'prefix': False,
                'date_range_ids': [],
                'number_next_actual': 0,
                'active': True,
                'suffix': False
            }
            if 'name' in vals:
                seq_vals['name'] = 'Asset(%s)' % vals['name']
            else:
                seq_vals['name'] = 'Asset(%s)' % self.name
            seq_id = seq_obj.sudo().create(seq_vals)
            vals.update({'asset_code_sequence_id': seq_id.id})
        else:
            if 'name' in vals:
                seq_vals['name'] = ('Asset(%s)' % vals['name'])
            if 'category_code' in vals:
                seq_vals['code'] = ('%s' % vals['category_code'])
            seq_id.sudo().write(seq_vals)

        return super(AccountAssetCategoryCode, self).write(vals)

    @api.multi
    def unlink(self):
        for obj in self:
            obj.asset_code_sequence_id.sudo().unlink()
        return super(AccountAssetCategoryCode, self).unlink()
