# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _


class AccountAssetAsset(models.Model):
    _inherit = 'account.asset.asset'

    type_of_close = fields.Selection(selection_add=[('convert_to_product', 'Convert to product')])
