# -*- coding: utf-8 -*-
import time
from odoo import api, fields, models, _


class AccountAssetClose(models.TransientModel):
    _inherit = "account.asset.close.wizard"

    type_of_close = fields.Selection(selection_add=[('convert_to_product', 'Convert to product')])
