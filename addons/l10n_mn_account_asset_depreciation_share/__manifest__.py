# -*- coding: utf-8 -*-
{
    "name" : "Asset Expense Depreciation Share",
    "version" : "1.0",
    "depends" : ['l10n_mn_account_asset'],
    "author" : "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    "category" : "Account",
    "description": """
      Asset Expense Depreciation Share """,
    "data" : ['security/ir.model.access.csv',
              'wizard/account_asset_expense_depreciation_create_view.xml',
              'wizard/account_asset_expense_depreciation_move_view.xml',
              'wizard/account_asset_move_view.xml',
              'views/account_asset_view.xml',
    ],
    "active": False,
    "installable": True,
}