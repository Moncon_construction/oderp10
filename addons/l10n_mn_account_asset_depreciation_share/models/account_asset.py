# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _
from datetime import datetime
from odoo.exceptions import UserError

class AccountAssetAsset(models.Model):
    _inherit = 'account.asset.asset'

    account_expense_depreciation_share = fields.One2many('account.asset.expense.depreciation.share', 'asset_id', 'Depr. Expense Accounts')
    account_expense_depreciation_share_history = fields.One2many('account.asset.expense.depreciation.share.history', 'asset_id', 'History Depr. Expense Accounts')
    depr_exp_history_count = fields.Integer(compute='_depr_accounts_history_count', string='Asset depr. account histories')

    @api.multi
    def _depr_accounts_history_count(self):
        for asset in self:
            res = self.env['account.asset.expense.depreciation.share.history'].search_count([('asset_id', '=', asset.id)])
            asset.depr_exp_history_count = res or 0

    @api.model
    def create(self, vals):
        # Элэгдлийг зардлын данснуудад хуваарилах хэсгийн хувийг шалгах
        if 'account_expense_depreciation_share' in vals:
            rate = 0.0
            for line in vals['account_expense_depreciation_share']:
                # Шинэ утгын формат нь [0, False, {vals}] байдаг
                if len(line) == 3:
                    rate += line[2]['rate']
            if rate%100 != 0.0 and rate != 0:
                raise UserError(_("Total rate must be 100%!"))
        return super(AccountAssetAsset, self).create(vals)

    @api.multi
    def write(self, vals):
        # Элэгдлийг зардлын данснуудад хуваарилах хэсгийн хувийг шалгах
        for asset in self:
            if 'account_expense_depreciation_share' in vals:
                rate = 0.0
                depreciation_obj = self.env['account.asset.expense.depreciation.share']
                for line in vals['account_expense_depreciation_share']:
                    if len(line) == 3:
                        # Шинэ утгын формат нь [0, False, {vals}]
                        if line[0] == 0 and line[1] == False:
                            rate += line[2]['rate']
                        # Өөрчлөгдсөн хуучин утгын формат нь [1, id, {changedVals}]
                        elif line[0] == 1:
                            if 'rate' in line[2]:
                                rate += line[2]['rate']
                            else:
                                rate += depreciation_obj.browse(line[1]).rate
                        # Өөрчлөгдөөгүй хуучин утгын формат нь [4, id, False]
                        elif line[0] == 4:
                            rate += depreciation_obj.browse(line[1]).rate
                        # Устгагдсан утга нь [2, id, False] иймд тооцох шаардлагагүй
                if rate%100 != 0.0 and rate != 0:
                    raise UserError(_("Total rate must be 100%!"))
        return super(AccountAssetAsset, self).write(vals)