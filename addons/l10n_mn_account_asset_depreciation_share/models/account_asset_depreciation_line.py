# -*- coding: utf-8 -*-
import time
from odoo import api, fields, models, tools, _
from odoo.tools import float_compare, float_is_zero
from datetime import datetime

class AccountAssetDepreciationLine(models.Model):
    _inherit = 'account.asset.depreciation.line'
    
    @api.multi
    def create_move(self, post_move=True):
#         Элэгдэлийг батлаж, журналын бичилт үүсгэх
        created_moves = self.env['account.move']
        prec = self.env['decimal.precision'].precision_get('Account')
        for line in self:
            line_vals = []
#             Журналын бичилт үүсгэх
            category_id = line.asset_id.category_id
            depreciation_date = self.env.context.get('depreciation_date') or line.depreciation_date or fields.Date.context_today(self)
            company_currency = line.asset_id.company_id.currency_id
            current_currency = line.asset_id.currency_id
            amount = current_currency.with_context(date=depreciation_date).compute(line.amount, company_currency)
            asset_name = line.asset_id.name + ' (%s/%s)' % (line.sequence, len(line.asset_id.depreciation_line_ids))
            move_line_1 = {
                'name': asset_name,
                'ref': line.name,
                'account_id': category_id.account_depreciation_id.id,
                'debit': 0.0 if float_compare(amount, 0.0, precision_digits=prec) > 0 else -amount,
                'credit': amount if float_compare(amount, 0.0, precision_digits=prec) > 0 else 0.0,
                'journal_id': category_id.journal_id.id,
                'partner_id': line.asset_id.partner_id.id,
                'analytic_account_id': category_id.account_analytic_id.id if category_id.type == 'sale' else False,
                'currency_id': company_currency != current_currency and current_currency.id or False,
                'amount_currency': company_currency != current_currency and - 1.0 * line.amount or 0.0,
                'date': depreciation_date,
                'asset_id': line.asset_id.id,
            }
            line_vals.append((0,0, move_line_1))
            if line.asset_id.account_expense_depreciation_share:
                suma = sum([account.rate for account in line.asset_id.account_expense_depreciation_share])
                for expense in line.asset_id.account_expense_depreciation_share:
                    exp_amount = expense.rate/100*(amount/(suma/100))
                    move_line_2 = {
                        'name': asset_name,
                        'ref': line.name,
                        'account_id': expense.account_id.id,
                        'credit': 0.0 if float_compare(exp_amount, 0.0, precision_digits=prec) > 0 else -exp_amount,
                        'debit': exp_amount if float_compare(exp_amount, 0.0, precision_digits=prec) > 0 else 0.0,
                        'journal_id': category_id.journal_id.id,
                        'partner_id': line.asset_id.partner_id.id,
                        'analytic_account_id': expense.account_analytic_id.id if expense.account_analytic_id else False,
                        'currency_id': company_currency != current_currency and current_currency.id or False,
                        'amount_currency': company_currency != current_currency and line.amount or 0.0,
                        'date': depreciation_date,
                        'asset_id': line.asset_id.id,
                    }
                    line_vals.append((0,0, move_line_2))
            else:
                move_line_2 = {
                    'name': asset_name,
                    'ref': line.name,
                    'account_id': category_id.account_depreciation_expense_id.id,
                    'credit': 0.0 if float_compare(amount, 0.0, precision_digits=prec) > 0 else -amount,
                    'debit': amount if float_compare(amount, 0.0, precision_digits=prec) > 0 else 0.0,
                    'journal_id': category_id.journal_id.id,
                    'partner_id': line.asset_id.partner_id.id,
                    'analytic_account_id': category_id.account_analytic_id.id if category_id.type == 'purchase' and category_id.account_analytic_id else line.asset_id.account_analytic_id.id,
                    'currency_id': company_currency != current_currency and current_currency.id or False,
                    'amount_currency': company_currency != current_currency and line.amount or 0.0,
                    'date': depreciation_date,
                    'asset_id': line.asset_id.id,
                }
                line_vals.append((0,0, move_line_2))
            move_vals = {
                'name': asset_name,
                'ref': line.asset_id.code,
                'date': depreciation_date or False,
                'journal_id': category_id.journal_id.id,
                'line_ids': line_vals,
            }
            move = self.env['account.move'].create(move_vals)
            line.write({'move_id': move.id, 'move_check': True})
            created_moves |= move
        if post_move and created_moves:
            if created_moves.filtered(lambda m: any(m.asset_depreciation_ids.mapped('asset_id.category_id.open_asset'))):
                created_moves.filtered(lambda m: any(m.asset_depreciation_ids.mapped('asset_id.category_id.open_asset'))).post()
            else:
                created_moves.filtered(lambda m: any(m.asset_depreciation_ids.mapped('asset_id.journal_id.auto_approve'))).post() 
        return [x.id for x in created_moves]
    