# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _

class AccountAssetExpenseDepreciationShare(models.Model):
    ''' Хөрөнгийн зардлын данснуудыг сонгон, хэдэн хувиар тооцохыг бичих table '''
    
    _name = 'account.asset.expense.depreciation.share'
    
    asset_id = fields.Many2one('account.asset.asset','Asset', required=True, ondelete='cascade')  
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id) 
    account_id = fields.Many2one('account.account','Depr. Expense Account', required=True)
    require_analytic = fields.Boolean(related="account_id.req_analytic_account")
    account_analytic_id = fields.Many2one('account.analytic.account', 'Analytic Account')
    rate = fields.Float('Rate (%)', required=True, default=100)