# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _

class AccountAssetExpenseDepreciationShareHistory(models.Model):
    _name = 'account.asset.expense.depreciation.share.history'
    _description = 'Account Asset expense depreciation share History'
    
    """ Хөрөнгийн элэгдлийн зардлын данс өөрчилсөн түүх """
    
    asset_id = fields.Many2one('account.asset.asset', 'Asset')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id) 
    date = fields.Date('Date', required=True)
    account_id = fields.Many2one('account.account','Old Depr. Expense Account', required=True)
    require_analytic = fields.Boolean(related="account_id.req_analytic_account")
    account_analytic_id = fields.Many2one('account.analytic.account', 'Old Analytic Account')
    rate = fields.Float('Rate (%)', required=True, default=100)