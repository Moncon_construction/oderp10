# -*- coding: utf-8 -*-
import time
from lxml import etree
from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError

class AccountAssetExpenseDepreciationChoice(models.TransientModel):
    _name = 'account.asset.expense.depreciation.choice'
    _description = 'Choice Depr. Expense Account'
    
    account_expense_depreciation_choice_line =  fields.One2many('account.asset.expense.depreciation.choice.line', 'choice_id', 'Depr. Expense Accounts')

    @api.multi
    def select(self):
        for form in self:
            if not form.account_expense_depreciation_choice_line:
                raise UserError(_("Select expense accounts of depreciation!"))
            rate = 0.0
            for expense in form.account_expense_depreciation_choice_line:
                rate += expense.rate
            if rate%100 != 0 and rate != 0:
                raise UserError(_("Total rate must be 100%!"))
        asset_obj = self.env['account.asset.asset']
        dep_expense_obj = self.env['account.asset.expense.depreciation.share']
        form = self.browse(self.ids[0])
        active_ids = self._context.get('active_ids', []) or []
        for asset in asset_obj.browse(active_ids): 
            if asset.state == 'draft':
                if not asset.account_expense_depreciation_share:
                    for line in form.account_expense_depreciation_choice_line:
                        vals = {'asset_id': asset.id,
                                'account_id': line.account_id.id,
                                'account_analytic_id': line.account_analytic_id.id if line.account_analytic_id else False,
                                'rate': line.rate}
                        dep_expense_obj.create(vals)
                else:
                    raise UserError(_('%s asset selected depreciation expense accounts!' % asset.name))
            else:
                raise UserError(_("%s asset's state not draft! Only select draft state assets." % asset.name))
        return True
    
class AccountAssetExpenseDepreciationChoice_Line(models.TransientModel):
    _name = 'account.asset.expense.depreciation.choice.line'
    _description = 'Choice Depr. Expense Account Line'
    
    choice_id = fields.Many2one('account.asset.expense.depreciation.choice','Asset', required=True, ondelete='cascade')
    account_id = fields.Many2one('account.account','Depr. Expense Account', required=True)
    require_analytic = fields.Boolean(related="account_id.req_analytic_account")
    account_analytic_id = fields.Many2one('account.analytic.account','Analytic Account')
    rate = fields.Float('Rate (%)', required=True, default=100)