# -*- coding: utf-8 -*-
import time
from lxml import etree
from odoo import api, fields, models, tools, _
from datetime import datetime
from odoo.exceptions import UserError

class AccountAssetExpenseDepreciationMove(models.TransientModel):
    _name = 'account.asset.expense.depreciation.move'
    _description = 'Change Depr. Expense Account'

    account_expense_depreciation_move_line = fields.One2many('account.asset.expense.depreciation.move.line', 'move_id', 'New Depr. Expense Accounts')

    @api.multi
    def change(self):
        for form in self:
            if not form.account_expense_depreciation_move_line:
                raise UserError(_("Select expense accounts of depreciation!"))
            rate = 0.0
            for expense in form.account_expense_depreciation_move_line:
                rate += expense.rate
            if rate%100 != 0 and rate != 0:
                raise UserError(_("Total rate must be 100%!"))
        asset_obj = self.env['account.asset.asset']
        dep_expense_obj = self.env['account.asset.expense.depreciation.share']
        dep_expense_history_obj = self.env['account.asset.expense.depreciation.share.history']
        form = self.browse(self.ids[0])
        active_ids = self._context.get('active_ids', []) or []
        for asset in asset_obj.browse(active_ids):
            if asset.state == 'open':
                for share in asset.account_expense_depreciation_share:
                    vals = {'asset_id': asset.id,
                            'date': datetime.now(),
                            'account_id': share.account_id.id,
                            'rate': share.rate,
                            'account_analytic_id': share.account_analytic_id.id if share.account_analytic_id else False,}
                    dep_expense_history_obj.create(vals)
                self.env.cr.execute("DELETE FROM account_asset_expense_depreciation_share WHERE asset_id = %s",(asset.id,))
                for line in form.account_expense_depreciation_move_line:
                    vals = {'asset_id': asset.id,
                            'account_id': line.account_id.id,
                            'rate': line.rate,
                            'account_analytic_id': line.account_analytic_id.id if line.account_analytic_id else False}
                    dep_expense_obj.create(vals)
            else:
                raise UserError(_("%s asset's state not open! Only select open state assets." % asset.name))
            
        return True

class AccountAssetExpenseDepreciationMoveLine(models.TransientModel):
    _name = 'account.asset.expense.depreciation.move.line'
    _description = 'Change Depr. Expense Account Line'
    
    move_id = fields.Many2one('account.asset.expense.depreciation.move','Asset', required=True, ondelete='cascade') 
    account_id = fields.Many2one('account.account','Depr. Expense Account', required=True)
    require_analytic = fields.Boolean(related="account_id.req_analytic_account")
    account_analytic_id = fields.Many2one('account.analytic.account', 'Analytic Account')
    rate = fields.Float('Rate (%)', required=True, default=100)