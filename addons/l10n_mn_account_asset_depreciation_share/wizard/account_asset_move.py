# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
import time
from datetime import datetime
from odoo.exceptions import UserError
    
class AccountAssetMovesExpenseDepreciation(models.TransientModel):
    _name = 'account.asset.moves.expense.depreciation'
    _description = 'Change Depr. Expense Account Line'
    
    move_id = fields.Many2one('account.asset.moves','Asset')  
    account_id = fields.Many2one('account.account','Depr. Expense Account')
    require_analytic = fields.Boolean(related='account_id.req_analytic_account')
    account_analytic_id = fields.Many2one('account.analytic.account', 'Analytic Account')
    rate = fields.Float('Rate (%)', required=True, default=100)

class AccountAssetMoves(models.Model):
    _inherit = 'account.asset.moves'
    _description = "Asset move"

    account_asset_moves_expense_depreciation = fields.One2many('account.asset.moves.expense.depreciation', 'move_id', 'Depr. Expense Accounts', required=True)

    @api.model
    def create(self, vals):
        if 'account_asset_moves_expense_depreciation' in vals:
            rate = 0.0
            for line in vals['account_asset_moves_expense_depreciation']:
                # Шинэ утгын формат нь [0, False, {vals}] байдаг
                if len(line) == 3:
                    rate += line[2]['rate']
            if rate%100 != 0.0 and rate != 0:
                raise UserError(_("Total rate must be 100%!"))
        return super(AccountAssetMoves, self).create(vals)

    @api.multi
    def write(self, vals):
        if 'account_asset_moves_expense_depreciation' in vals:
            rate = 0.0
            depreciation_obj = self.env['account.asset.moves.expense.depreciation']
            for line in vals['account_asset_moves_expense_depreciation']:
                if len(line) == 3:
                    # Шинэ утгын формат нь [0, False, {vals}]
                    if line[0] == 0 and line[1] == False:
                        rate += line[2]['rate']
                    # Өөрчлөгдсөн хуучин утгын формат нь [1, id, {changedVals}]
                    elif line[0] == 1:
                        if 'rate' in line[2]:
                            rate += line[2]['rate']
                        else:
                            rate += depreciation_obj.browse(line[1]).rate
                    # Өөрчлөгдөөгүй хуучин утгын формат нь [4, id, False]
                    elif line[0] == 4:
                        rate += depreciation_obj.browse(line[1]).rate
                    # Устгагдсан утга нь [2, id, False] иймд тооцох шаардлагагүй
            if rate%100 != 0.0 and rate != 0:
                raise UserError(_("Total rate must be 100%!"))
        return super(AccountAssetMoves, self).write(vals)

    @api.multi
    def action_receipt(self):
        ''' Хөрөнгө элэгдлийн зардлын данснуудыг өөрчлөх'''
        params = False
        res = super(AccountAssetMoves, self).action_receipt()
        for a in self:
            for line in a.line_ids:
                if line.old_owner_id != line.new_owner_id:
                    params = True
        if params == False:
            dep_expense_obj = self.env['account.asset.expense.depreciation.share']
            dep_expense_history_obj = self.env['account.asset.expense.depreciation.share.history']
            if not self.account_asset_moves_expense_depreciation:
                raise UserError(_("Select expense accounts of depreciation!"))
            rate = 0.0
            for expense in self.account_asset_moves_expense_depreciation:
                rate += expense.rate
            if rate%100 != 0 and rate != 0:
                raise UserError(_("Total rate must be 100%!"))
            for line in self.line_ids:
                if line.state != 'cancel':
                    for share in line.asset_id.account_expense_depreciation_share:
                        vals = {'asset_id': line.asset_id.id,
                                'date': self.move_date,
                                'account_id': share.account_id.id,
                                'account_analytic_id': share.account_analytic_id.id if share.account_analytic_id else False,
                                'rate': share.rate
                                }
                        dep_expense_history_obj.create(vals)
                    self.env.cr.execute("DELETE FROM account_asset_expense_depreciation_share WHERE asset_id = %s",(line.asset_id.id,))
                    for share in self.account_asset_moves_expense_depreciation:
                        vals = {
                                'asset_id': line.asset_id.id,
                                'account_id': share.account_id.id,
                                'account_analytic_id': share.account_analytic_id.id if share.account_analytic_id else False,
                                'rate': share.rate
                                }
                        dep_expense_obj.create(vals)
        return res
