# -*- encoding: utf-8 -*-
import time
from lxml import etree
from odoo import api, fields, models, tools, _
from odoo.exceptions import Warning


class AccountAssetValidate(models.TransientModel):
    _inherit = 'account.asset.validate'
    _description = 'Asset Validate'

    def validate(self):
        context = dict(self._context)
        asset_obj = self.env['account.asset.asset']
        asset_id = context.get('active_id', False)
        if asset_id:
            asset = asset_obj.browse(asset_id)
            if not asset.account_expense_depreciation_share:
                raise Warning(_('Select Depr. Expense Accounts: %s' % asset.name))
            asset.validate(context=dict(context.items(),
                                        src_account_id=self.account_id.id,
                                        entry_date=self.purchase_date))
        return True

class AccountAssetValidateAll(models.TransientModel):
    _inherit = 'account.asset.validate.all'
    _description = 'Asset Validate'

    def validate_all(self):
        context = dict(self._context)
        active_ids = context.get('active_ids', []) or []
        if context is None:
            context = {}
        asset_obj = self.env['account.asset.asset']
        for asset in asset_obj.browse(active_ids):
            if not asset.account_expense_depreciation_share:
                raise Warning(_('Select Depr. Expense Accounts: %s' % asset.name))
            asset.validate(context=dict(context.items(),
                                        src_account_id=self.account_id.id,
                                        src_partner_id=asset.partner_id.id,
                                        entry_date=self.purchase_date))
        return True
