# -*- coding: utf-8 -*-
{
    "name" : "Mongolian Account Asset Inventory",
    "version" : "1.0",
    "author" : "Asterisk Technologies LLC",
    "description": """Account Asset Inventory""",
    "website" : "http://asterisk-tech.mn",
    "category" : "Mongolian Account",
    "depends" : ['l10n_mn_account','l10n_mn_account_asset'],
    "init": [],
    "data" : [
        'security/ir.model.access.csv',
        'views/sequence.xml',
        'views/account_asset_inventory_view.xml',
        'views/account_config_setting_view.xml',
        'report/inventory_report_view.xml'
    ],
    "demo_xml": [
    ],
    "installable": True,
    'auto_install': False,
    'application': True,
}
