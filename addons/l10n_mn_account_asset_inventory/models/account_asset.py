# -*- coding: utf-8 -*-
from odoo import fields, models, _, api
from odoo.exceptions import UserError, ValidationError

class ResCompany(models.Model):
    _inherit = 'res.company'

    is_asset_inventory_picture = fields.Boolean(string='Is Account Asset inventory report picture')

class AccountConfigSettings(models.TransientModel):
    _inherit = 'account.config.settings'
    _description = 'Account settings'

    is_asset_inventory_picture = fields.Boolean(related='company_id.is_asset_inventory_picture', string='Is Account Asset inventory report picture')
    
class AccountAssetAsset(models.Model):
    _inherit = 'account.asset.asset'

    @api.multi
    def unlink(self):
        for asset in self:
            inv_line = self.env['asset.inventory.line'].search([('asset_id', '=', asset.id)])
            if inv_line:
                raise UserError(_('Asset %s has asset inventory line delete it first') % asset.name)
        return super(AccountAssetAsset, self).unlink()
