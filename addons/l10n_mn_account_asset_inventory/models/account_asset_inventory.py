# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import fields, models, _, api
from odoo.exceptions import UserError, ValidationError
import time

import logging

_logger = logging.getLogger(__name__)


class AssetInventory(models.Model):
    _name = 'asset.inventory'
    _description = 'Asset inventory'

    name = fields.Char('Reference', required=True, copy=False, readonly=False)
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id)
    inventory_type = fields.Selection([('location', 'Location'), ('owner', 'Owner')], 'Type')
    state = fields.Selection(
        [('draft', 'Draft'), ('processing', 'Processing'), ('failure', 'Failure'), ('done', 'Done')], 'State',
        default='draft')
    asset_location = fields.Many2one('account.asset.location', 'Location')
    asset_owner = fields.Many2one('hr.employee', 'Owner')
    inventory_date = fields.Date('Inventory date', default=fields.Date.context_today)
    inventory_line = fields.One2many('asset.inventory.line', 'asset_inventory_id', 'Inventory line')
    failure_line = fields.One2many('asset.inventory.loss', 'asset_inventory_id', 'Failure line')
    description = fields.Text('Description')
    show_closed_asset = fields.Boolean('Show closed asset', default=False)

    def copy(self, default=None):
        if not default:
            default = {}
        default.update({
            'state': 'draft',
            'name': '',
        })
        return super(AssetInventory, self).copy(default)

    # Хөрөнгийн тооллого эхлүүлэх.
    def start_inventory(self):
        order_name = self.name;
        line_obj = self.env['asset.inventory.line']
        asset_obj = self.env['account.asset.asset']
        asset_ids = []
        search_args = []
        if self.inventory_type == 'owner':
            search_args = [('user_id', '=', self.asset_owner.id), ('state', '=', 'open')]
            if self.show_closed_asset:
                search_args = [('user_id', '=', self.asset_owner.id), ('state', '!=', 'draft')]
        elif self.inventory_type == 'location':
            search_args = [('location_id', '=', self.asset_location.id), ('state', '=', 'open')]
            if self.show_closed_asset:
                search_args = [('location_id', '=', self.asset_location.id), ('state', '!=', 'draft')]
        asset_ids = asset_obj.search(search_args)
        if asset_ids != []:

            # Хөрөнгө байхгүй тохиолдолд устгах
            assets = asset_obj.search([('id', 'in', asset_ids.ids)])
            self.env['asset.inventory.line'].search([('asset_inventory_id', '=', self.id)]).unlink()
            for asset in assets:
                closed_asset = False
                if asset.state == 'close':
                    closed_asset = True
                line_obj.create({'asset_inventory_id': self.ids[0],
                                 'asset_id': asset.id,
                                 'owner_id': asset.user_id.id,
                                 'location_id': asset.location_id.id,
                                 'closed_asset': closed_asset,
                                 'asset_code': asset.code})
        self.write({'state': 'processing', 'name': order_name})
        return True

    # Хөрөнгийн баталгаажуулах.
    def confirm_inventory(self):
        failure_obj = self.env['asset.inventory.loss']
        count_failure = 0
        for line in self.inventory_line:
            if line.is_have == False:
                # Хөрөнгө байхгүй тохиолдолд дутсан хөрөнгө-рүү орох.
                count_failure += 1
                failure_obj.create({'asset_inventory_id': self.ids[0],
                                    'asset_id': line.asset_id.id,
                                    'owner_id': line.owner_id.id,
                                    'location_id': line.location_id.id,
                                    'closed_asset': line.closed_asset,
                                    'is_have': False,
                                    'closed_asset': False,
                                    'asset_move_number': '',
                                    'state': 'not_solved',
                                    })
                self.env['asset.inventory.line'].search([('id', '=', line.id)]).unlink()
        if count_failure == 0:
            self.write({'state': 'done'})
        else:
            self.write({'state': 'failure'})
        return True

    # Хөрөнгийн дахин тооллох.
    def re_inventory(self):
        failure_obj = self.env['asset.inventory.loss']
        line_obj = self.env['asset.inventory.line']
        count_failure = 0
        count_line = 0
        for line in self.failure_line:
            count_failure += 1
        for line in self.failure_line:
            if line.is_have == True:
                # Хөрөнгө байгаа тохиолдолд хөрөнгө-рүү орох.
                count_line += 1
                line_obj.create({'asset_inventory_id': self.ids[0],
                                 'asset_id': line.asset_id.id,
                                 'owner_id': line.owner_id.id,
                                 'location_id': line.location_id.id,
                                 'closed_asset': line.closed_asset,
                                 'is_have': True,
                                 })

                self.env['asset.inventory.loss'].search([('id', '=', line.id)]).unlink()
            elif line.closed_asset == True:
                count_line += 1

            elif line.asset_state == 'moved':
                count_line += 1
        self.env.cr.commit()
        if count_failure == count_line:
            self.write({'state': 'done'})
        else:
            self.write({'state': 'failure'})
        return True

    # Хөрөнгийн цуцлах.
    def cancel_inventory(self):
        self.env['asset.inventory.line'].search([('asset_inventory_id', 'in', [self.id, False])]).unlink()
        self.env['asset.inventory.loss'].search([('asset_inventory_id', 'in', [self.id, False])]).unlink()
        self.write({'state': 'draft'})
        return True


class AssetInventoryLine(models.Model):
    _name = 'asset.inventory.line'
    _description = 'Asset inventory line'

    asset_inventory_id = fields.Many2one('asset.inventory', 'Asset inventory', ondelete='cascade')
    asset_code = fields.Char('Code')
    asset_id = fields.Many2one('account.asset.asset', 'Asset', required=True)
    owner_id = fields.Many2one('hr.employee', 'Owner')
    location_id = fields.Many2one('account.asset.location', 'Location')
    is_have = fields.Boolean('Is Have', default=False)
    closed_asset = fields.Boolean('Closed asset', readonly=True, default=False)
    inv_state = fields.Selection(
        [('draft', 'Draft'), ('processing', 'Processing'), ('failure', 'Failure'), ('done', 'Done')], 'Inv State', related='asset_inventory_id.state', default='draft')

    @api.onchange('asset_id')
    def onchange_asset_id(self):
        for line in self:
            asset_ids = self.env['account.asset.asset'].search([('id', '=', line.asset_id.id)])
            line.asset_code = asset_ids.code


class AssetInventoryFailure(models.Model):
    _name = 'asset.inventory.loss'
    _description = 'Asset inventory failure'

    asset_inventory_id = fields.Many2one('asset.inventory', 'Asset inventory', ondelete='cascade')
    asset_code = fields.Char('Code')
    asset_id = fields.Many2one('account.asset.asset', 'Asset', required=True, domain=[('state', '=', 'open')])
    owner_id = fields.Many2one('hr.employee', 'Owner')
    location_id = fields.Many2one('account.asset.location', 'Location')
    is_have = fields.Boolean('Is Have', default=False)
    closed_asset = fields.Boolean('Closed asset')
    asset_move_number = fields.Many2one('account.asset.moves', 'Asset Move Number', readonly=True)
    asset_state = fields.Selection([('not_solved', 'Not Solved'), ('closed', 'Closed'), ('moved', 'Moved')], default='not_solved', string="State", readonly=True)
    comment = fields.Char('Comment')
    inv_state = fields.Selection(
        [('draft', 'Draft'), ('processing', 'Processing'), ('failure', 'Failure'), ('done', 'Done')], 'Inv State', related='asset_inventory_id.state', default='draft')

    @api.onchange('asset_id')
    def onchange_asset_id(self):
        for line in self:
            asset_ids = self.env['account.asset.asset'].search([('id', '=', line.asset_id.id)])
            line.asset_code = asset_ids.code

    # Хөрөнгө хаах цонх дуудах.
    @api.multi
    def close_the_asset(self):
        for obj in self:
            asset = self.env['account.asset.asset'].browse(obj.asset_id.id)
            return {
                'type': 'ir.actions.act_window',
                'name': _('Close Asset'),
                'res_model': 'account.asset.close.wizard',
                'view_mode': 'form',
                'target': 'new',
                'context': {
                    'default_asset_id': asset.id,
                    'default_value_residual': asset.value_residual,
                    'default_value': asset.value,
                    'default_currency_id': asset.currency_id.id,
                    'default_date': time.strftime('%Y-%m-%d'),
                    'default_name': _('%s asset write off') % asset.name_get()[0][1],
                    'default_analytic_account_id': asset.account_analytic_id.id,
                    'access': 'asset.inventory.loss'
                }
            }

    # Хөрөнгө шилжүүлэх цонх дуудах.
    @api.multi
    def asset_move(self):
        for obj in self:
            self._context.get('active_ids')
            active_ids = [obj.asset_id.id]
            return {
                'type': 'ir.actions.act_window',
                'name': _('Asset Move'),
                'res_model': 'account.asset.moves',
                'view_mode': 'form',
                'target': 'new',
                'context': {
                    'active_ids': active_ids,
                    'active_model': 'account.asset.asset',
                    'access': 'asset.inventory.loss'
                }
            }


class AccountAssetMoves(models.Model):
    _inherit = 'account.asset.moves'

    @api.multi
    def action_sent(self):
        res = super(AccountAssetMoves, self).action_sent()
        context = dict(self._context or {})
        if context.get('access') == 'asset.inventory.loss' and context.get('active_id'):
            active_id = self.env.context['active_id']  # Хөрөнгө шилжүүлэх үед хөрөнгө дутагдалын төлөв өөрчлөгдөнө.
            inv_id = active_id
            inventory_id = self.env['asset.inventory.loss'].search([('id', '=', inv_id)])
            if inventory_id:
                for obj in self:
                    for inv in inventory_id:
                        inv.asset_state = 'moved'
                        inv.asset_move_number = obj.id
        return res
