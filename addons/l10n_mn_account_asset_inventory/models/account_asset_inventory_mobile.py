# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, models
import ast

class AssetInventory(models.Model):
    _inherit = 'asset.inventory'

    @api.multi
    def get_asset_inventory_mobile(self, date_limit, not_synced_ids):
        """
            Мобайлаас өөрт зөвшөөрөгдсөн хөрөнгийн тооллогууд татах функц
        """
        asset_inventories = []
        not_synced_ids = ast.literal_eval(not_synced_ids)
        if not_synced_ids:
            self.env.cr.execute("""SELECT s.id, s.name, s.company_id, s.asset_location, s.inventory_date, s.inventory_type,
                                             s.state ,s.asset_owner, s.show_closed_asset
                                            FROM asset_inventory s
                                            where state not in ('cancel')
                                            AND s.inventory_date > '%s'
                                            AND s.id NOT IN (%s)""" % (date_limit, ','.join(str(a) for a in tuple(not_synced_ids))))
            asset_inventory_object = self.env.cr.dictfetchall()
        else:
            self.env.cr.execute("""SELECT s.id, s.name, s.company_id, s.asset_location, s.inventory_date, s.inventory_type,
                                                         s.state ,s.asset_owner, s.show_closed_asset
                                                        FROM asset_inventory s
                                                        where state not in ('cancel')
                                                        AND s.inventory_date > '%s'""" % (date_limit))
            asset_inventory_object = self.env.cr.dictfetchall()

        if asset_inventory_object:
            for asset_inventory in asset_inventory_object:
                # хөрөнгийн мэдээлэл татна.
                self.env.cr.execute("""SELECT id, asset_id, location_id, is_have, closed_asset, owner_id, asset_code, asset_inventory_id
                                                            FROM asset_inventory_line
                                                            WHERE asset_inventory_id = %s""" % (asset_inventory['id']))
                asset_inventory_line = self.env.cr.dictfetchall()
                # дутсан хөрөнгийн мэдээлэл татна.
                self.env.cr.execute("""SELECT id, asset_id, location_id, is_have, closed_asset, owner_id, asset_code, asset_inventory_id 
                                                                        FROM asset_inventory_loss
                                                                        WHERE asset_inventory_id = %s""" % (
                    asset_inventory['id']))
                asset_inventory_loss = self.env.cr.dictfetchall()
                dic = {'id': asset_inventory['id'], 'name': asset_inventory['name'],
                       'company_id': asset_inventory['company_id'] or False,
                       'asset_location': asset_inventory['asset_location'] or False,
                       'asset_owner': asset_inventory['asset_owner'] or False,
                       'inventory_date': asset_inventory['inventory_date'],
                       'inventory_line': asset_inventory_line,
                       'failure_line': asset_inventory_loss,
                       'state': asset_inventory['state'],
                       'show_closed_asset': asset_inventory['show_closed_asset'],
                       'inventory_type': asset_inventory['inventory_type']
                       }
                asset_inventories.append(dic)
        return asset_inventories

    @api.multi
    def get_asset_inventory_line_mobile(self, asset_inventory_id):
        """
            Үндсэн хөрөнгийн тооллогын мөрүүдийг татах мобайл функц
        """
        inventory_line_data = []
        inventory_line_objs = self.env['asset.inventory.line'].search([('asset_inventory_id', '=', asset_inventory_id)])
        for line in inventory_line_objs:
            inventory_line_data.append({'id': line.id, 'asset_inventory_id': line.asset_inventory_id.id,
                                        'asset_code': line.asset_code, 'asset_id': line.asset_id.id,
                                        'owner_id': line.owner_id.id, 'location_id': line.location_id.id,
                                        'is_have': line.is_have, 'closed_asset': line.closed_asset,
                                        'inv_state': line.inv_state})
        return inventory_line_data
