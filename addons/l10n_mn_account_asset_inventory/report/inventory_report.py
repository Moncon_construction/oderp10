# -*- coding: utf-8 -*-
from odoo import models


class ReportAssetInventory(models.AbstractModel):
    _name = 'report.l10n_mn_account_asset_inventory.report_asset_inventory'

    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        inventory_obj = self.env['asset.inventory']
        asset_inventories = inventory_obj.browse(docids)
        report = report_obj._get_report_from_name('l10n_mn_account_asset_inventory.print_report_asset_inventory')

        docargs = {
            'doc_ids': docids,
            'doc_model': report.model,
            'docs': asset_inventories,
        }
        return report_obj.render('l10n_mn_account_asset_inventory.print_report_asset_inventory', docargs)
