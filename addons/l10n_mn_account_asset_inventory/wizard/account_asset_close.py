# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import fields, models, _ , api
from odoo.exceptions import UserError, ValidationError
import time

import logging

_logger = logging.getLogger(__name__)

class AccountAssetClose(models.TransientModel):
    _inherit = "account.asset.close.wizard"

    @api.multi
    def process(self):
        res = super(AccountAssetClose, self).process()
        context = dict(self._context or {})
        if context.get('access') == 'asset.inventory.loss' and context.get('active_id'):
            active_id = self.env.context['active_id']  # Хөрөнгө хаах үед хөрөнгө дутагдалын төлөв өөрчлөгдөнө.
            inv_id = active_id
            inventory_id = self.env['asset.inventory.loss'].search([('id', '=', inv_id)])
            if inventory_id:
                for inv in inventory_id:
                    inv.asset_state = 'closed'
                    inv.closed_asset = True
        return res