# -*- coding: utf-8 -*-
# Part of OdERP. See LICENSE file for full copyright and licensing details.
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/,
# http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
{
    'name': "Mongolian Asset Inventory Import",
    'version': '1.0',
    'depends': ['l10n_mn_account_asset_inventory'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Account',
    'description': """
                   Хөрөнгийн тооллого дээр Баркод импортолдог хэсэг нэмсэн 
                   """,
    'data': [
             'views/account_asset_inventory_import_view.xml',
             'views/account_asset_inventory_view.xml'
             ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}
