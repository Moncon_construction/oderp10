# -*- coding: utf-8 -*-
# Part of OdERP. See LICENSE file for full copyright and licensing details.
import logging
import base64
import xlrd
from odoo import fields, api, models, _
from tempfile import NamedTemporaryFile
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)

class AccountAssetInventoryImport(models.TransientModel):
    _name = 'account.asset.inventory.import'
    _description = 'Asset Inventory Import'
    '''
        Хөрөнгийн тооллогыг импортлох
    '''
    data = fields.Binary(string='Import File', required=True)
    inventory_id = fields.Many2one('asset.inventory', 'Asset Inventory', required=True)
    
    @api.model
    def default_get(self, fields):
        if len(self.env.context.get('active_ids', list())) > 1:
            raise UserError("You may only import one asset inventory!")
        res = super(AccountAssetInventoryImport, self).default_get(fields)

        inventory = self.env['asset.inventory'].browse(self.env.context.get('active_id'))
        if inventory:
            res.update({'inventory_id': inventory.id})
        return res
    
    
    def import_data(self):
        obj = self.browse(self.id)
        asset_obj = obj.env['account.asset.asset']
        line_obj = self.env['asset.inventory.line']
        try:
            fileobj = NamedTemporaryFile('w+')
            fileobj.write(base64.decodestring(obj.data))
            fileobj.seek(0)
            file_name = fileobj.name
            book = xlrd.open_workbook(file_name)
        except:
            raise UserError(_(u'Мэдээллийн файлыг уншихад алдаа гарлаа.\nЗөв файл эсэхийг шалгаад дахин оролдоно уу!'))

        sheet = book.sheet_by_index(0)
        nrows = sheet.nrows

        rowi = 1
        not_found_assets = []
        location_assets = []
        owner_assets = []
        close_assets = []
        draft_assets = []
        asset_ids = []
        while rowi < nrows:
            try:
                row = sheet.row(rowi)
                asset_code = row[0].value
                if not asset_code:
                    raise UserError(_(u'Энэ мөрийн Хөрөнгийн Дугаар багана хоосон байна: %s' % rowi))
                if asset_code:
                    asset = asset_obj.search([('code','=',asset_code)])
                    if not asset:
                        not_found_assets.append(asset_code)
                    else:
                        if asset.state == 'draft':
                            draft_assets.append(asset_code)
                        if obj.inventory_id.inventory_type == 'location' and obj.inventory_id.asset_location.id != asset.location_id.id:
                            location_assets.append(asset_code)
                        if obj.inventory_id.inventory_type == 'owner' and obj.inventory_id.asset_owner.id != asset.owner_id.id:
                            owner_assets.append(asset_code)
                        if obj.inventory_id.show_closed_asset == False and asset.state == 'close':
                            close_assets.append(asset_code)
                        asset_ids.append(asset.id)
                rowi += 1
            except IndexError:
                raise UserError(_('Excel sheet must be 1 columned : error on row: %s ' % rowi))
        if not_found_assets:
            raise UserError(_(u'Дараах кодтой хөрөнгө системд бүртгэгдээгүй байна. %s !' % (not_found_assets)))
        if draft_assets:
            raise UserError(_(u'Дараах хөрөнгүүд Ноорог төлөвтэй байна. %s !' % (draft_assets)))
        if obj.inventory_id.inventory_type == 'location' and location_assets:
            raise UserError(_(u'Дараах хөрөнгүүд %s байрлалд бүртгэгдээгүй байна. %s !' % (obj.inventory_id.asset_location.name, location_assets)))
        if obj.inventory_id.inventory_type == 'owner' and owner_assets:
            raise UserError(_(u'Дараах хөрөнгүүд %s эзэмшигчид бүртгэгдээгүй байна. %s !' % (obj.inventory_id.asset_owner.name, owner_assets)))
        if obj.inventory_id.show_closed_asset == False and close_assets:
            raise UserError(_(u'Дараах хөрөнгүүд Хаагдсан төлөвтэй байна. %s !' % (close_assets)))     
        asset_id = asset_obj.search([('id','in',tuple(asset_ids))])
        assets = asset_obj.search([('id', 'in', asset_id.ids)])
        for line in obj.inventory_id.inventory_line:
            is_have = False
            for asset in assets:
                if is_have == False and asset.id == line.asset_id.id:
                    line.write({'is_have': True})
                    is_have = True
        obj.inventory_id.confirm_inventory()