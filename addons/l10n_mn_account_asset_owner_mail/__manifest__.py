# -*- coding: utf-8 -*-
{
    'name': "Mongolian Account Asset Owner Mail",
    'version': '1.0',
    'depends': ['l10n_mn_account_asset'],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Account Asset Modules',
    'description': """
        Хөрөнгө эзэмшлийн мэйл ажилчдад илгээх модуль
    """,
    'website' : 'http://asterisk-tech.mn',
    'data': [
        'views/mail_wizard_asset_views.xml',
        'data/cron.xml'
    ]
}

