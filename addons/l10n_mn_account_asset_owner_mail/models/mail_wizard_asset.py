# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools
from datetime import datetime
import urllib


class MailWizardAsset(models.TransientModel):
    """ Wizard to send mail to the owner of the asset. """
    _name = 'mail.wizard.asset'
    _description = 'Send mail to the owner'

    @api.multi
    def create_wizard(self):
        wizard_id = self.create({})
        # YOUR POPULATION CODE HERE
        employee_obj = self.env['hr.employee']
        self._cr.execute("SELECT DISTINCT user_id FROM account_asset_asset WHERE state = 'open'")
        employees = self._cr.fetchall()
        res = []
        for employee_id in employees:
            employee = employee_obj.browse(employee_id[0])
            if employee.address_home_id and employee.address_home_id.email:
                res.append(employee.address_home_id.id)
        ctx = dict(self._context)
        ctx.update({'domain_partner_ids': res})
        return {
            'name': 'Send mail to the owner',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.wizard.asset',
            'res_id': wizard_id.id,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': ctx
        }

    partner_ids = fields.Many2many('res.partner', string='Recipients')

    @api.multi
    def send(self):
        asset_obj = self.env['account.asset.asset']
        employee_obj = self.env['hr.employee']
        email_from = self.env['mail.message']._get_default_from()
        if len(self) > 0:
            partner_ids = self.partner_ids
        else:
            # Эзэмшигчид авах хэсэг
            self._cr.execute("SELECT DISTINCT user_id FROM account_asset_asset WHERE state = 'open'")
            employees = self._cr.fetchall()
            partner_ids = []
            for employee_id in employees:
                employee = employee_obj.browse(employee_id[0])
                if employee.address_home_id and employee.address_home_id.email:
                    partner_ids.append(employee.address_home_id)
        for partner in partner_ids:
            # Мэйлийн бие зурах хэсэг
            self._cr.execute("SELECT id "
                             "FROM account_asset_asset "
                             "WHERE user_id = (SELECT id FROM hr_employee WHERE address_home_id = %s)" % partner.id)
            asset_ids = self._cr.fetchall()
            body_asset = ''
            number = 1
            for asset_id in asset_ids:
                asset = asset_obj.browse(asset_id[0])
                body_asset += u'<tr>' \
                              u'<td style="border: 1px solid black; border-collapse: collapse;">%s</td>' \
                              u'<td style="border: 1px solid black; border-collapse: collapse;">%s</td>' \
                              u'<td style="border: 1px solid black; border-collapse: collapse;">%s</td>' \
                              u'<td style="border: 1px solid black; border-collapse: collapse;">%s</td>' \
                              u'<td style="border: 1px solid black; border-collapse: collapse;"><img src="https://chart.googleapis.com/chart?cht=qr&chl=%s&chs=180x180&choe=UTF-8&chld=L|2" /></td>' \
                              u'</tr>' % (number, asset.name, asset.date, asset.location_id.name, urllib.quote_plus(str(asset.qr_code)))
            number += 1
            body_html = u'Сайн байна уу,<br/><br/>' \
                        u'%s танд %s-н байдлаар дараах үндсэн хөрөнгө/хангамжийн материал бүртгэлтэй байна.<br/><br/>' \
                        u'<table style="border: 1px solid black; border-collapse: collapse;">' \
                        u'<tr>' \
                        u'<th style="border: 1px solid black; border-collapse: collapse;">№</th>' \
                        u'<th style="border: 1px solid black; border-collapse: collapse;">Үндсэн хөрөнгийн нэр<br/>/Хангамжийн материалын нэр</th>' \
                        u'<th style="border: 1px solid black; border-collapse: collapse;">Ашиглалтанд<br/> орсон огноо</th>' \
                        u'<th style="border: 1px solid black; border-collapse: collapse;">Байршил</th>' \
                        u'<th style="border: 1px solid black; border-collapse: collapse;">Хөрөнгийн код</th>' \
                        u'</tr>' \
                        u'%s' \
                        u'</table>' \
                        u'<br/>' \
                        u'Дараах эд хөрөнгөтэй холбоотой асуудлаар үндсэн хөрөнгө/хангамж хариуцсан нягтлантай холбогдоно уу.' % (self.env['hr.employee'].search([('address_home_id', '=', partner.id)])[0].name, datetime.now().date(), body_asset)
            values = {
                'subject': u'Үндсэн хөрөнгийн мэдээлэл/Хангамжийн материалын мэдээлэл',
                'body_html': body_html,
                'email_from': email_from,
                'reply_to': email_from,
                'state': 'outgoing',
                'email_to': partner.email,
            }
            email_obj = self.env['mail.mail'].create(values)
            if email_obj:
                email_obj.send()
        return {'type': 'ir.actions.act_window_close'}
