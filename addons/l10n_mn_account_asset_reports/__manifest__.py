# -*- coding: utf-8 -*-
{
    'name': "Mongolian Account Asset Report",
    'version': '1.0',
    'depends': ['l10n_mn_account_asset'],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Account Asset Modules',
    'description': """
         Asset's module.
    """,
    'data': [
        'wizard/report_asset_detail_view.xml',
        'wizard/report_asset_move_view.xml',
        'wizard/report_asset_depreciation_view.xml',
        'wizard/property_owner_card_view.xml',
    ]
}

