# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _

class AccountAssetAsset(models.Model):
    _inherit = 'account.asset.asset'
    
    # Үндсэн хөрөнгийн элэгдлийн тайлангийн query дээр ашиглах тул store болгов
    value_residual = fields.Float(compute='_amount_residual', method=True, digits=0, string='Residual Value', store=True)