# -*- encoding: utf-8 -*-
##############################################################################
import base64
import time
import xlsxwriter
from odoo import api, fields, models, _
from io import BytesIO
from xlsxwriter.utility import xl_rowcol_to_cell
from operator import itemgetter
from openerp.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from odoo.exceptions import UserError


class PropertyOwnerCard(models.TransientModel):
    """
        Эд хөрөнгө эзэмшигчийн карт
    """
    _name = 'property.owner.card'
    _description = _('Property owner card')

    company_id = fields.Many2one('res.company', string='Company', readonly=True, default=lambda self: self.env.user.company_id)
    type = fields.Selection([('employee', 'Employee'),
                             ('category', 'Category'),
                             ('location', 'Location'),
                             ('department', 'Department')], default='employee', string="Type")
    employee_ids = fields.Many2many('hr.employee', string="Employee")
    category_ids = fields.Many2many('account.asset.category', string="Category")
    location_ids = fields.Many2many('account.asset.location', string="Location")
    department_ids = fields.Many2many('hr.department', string="Department")
    # Ангилал, байршил, хэлтэс

    @api.multi
    def export_report(self):
        HrEmployee = self.env['hr.employee']
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Property owner card')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_group_left = book.add_format(ReportExcelCellStyles.format_group_left)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_group_number = book.add_format(ReportExcelCellStyles.format_group_number)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_date = book.add_format(ReportExcelCellStyles.format_content_date)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)

        seq = 1
        data_dict = {}
        rowx = 1
        catogeries = []
        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('Property_owner_card'), form_title=file_name).create({})

        employee_ids = self.get_employee_ids()
        if not employee_ids:
            raise UserError(_('There are no employees with asset for your filter!'))
        sheet_sequence = 0
        for employee in employee_ids:
            employee = HrEmployee.browse(employee)
            # create sheet
            sheet_sequence += 1
            sheet = book.add_worksheet(str(sheet_sequence) + '. ' + employee.name)
            sheet.set_landscape()
            sheet.set_paper(9)  # A4
            sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
            sheet.fit_to_pages(1, 0)
            sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
            rowx = 1

            # compute column
            colx_number = 3
            sheet.set_column('A:A', 4)
            sheet.set_column('B:B', 15)
            sheet.set_column('C:C', 15)
            sheet.set_column('D:D', 15)
            sheet.set_column('E:E', 15)
            sheet.set_column('F:F', 15)
            sheet.set_column('G:G', 15)
            sheet.set_column('H:H', 15)
            sheet.merge_range(rowx, 1, rowx, 6, report_name, format_name)
            rowx += 1
            # create name
            sheet.merge_range(rowx, 0, rowx, 1, '%s' % (self.company_id.name), format_filter)
            sheet.merge_range(rowx, 0, rowx, 2, '%s: %s' % (_('Company'), self.company_id.name), format_filter)
            # create date
            sheet.write(rowx, 6, _('Created On: %s') % time.strftime('%Y-%m-%d'), format_filter)
            rowx += 1
            sheet.merge_range(rowx, 0, rowx, 1, '%s: %s' % (_('Department'), employee.department_id.name), format_filter)
            rowx += 1
            if employee.last_name:
                sheet.merge_range(rowx, 0, rowx, 1, '%s: %s. %s' % (_('Owner'), employee.last_name[:1], employee.name), format_filter)
            else:
                sheet.merge_range(rowx, 0, rowx, 1, '%s: %s' % (_('Owner'), employee.name), format_filter)
            rowx += 2
            # table header
            sheet.merge_range(rowx, 0, rowx + 1, 0, _('Seq'), format_title)
            sheet.merge_range(rowx, 1, rowx + 1, 1, _('Code'), format_title)
            sheet.merge_range(rowx, 2, rowx + 1, 2, _('Asset name'), format_title)
            sheet.merge_range(rowx, 3, rowx + 1, 3, _('Asset Location name'), format_title)
            sheet.merge_range(rowx, 4, rowx + 1, 4, _('Explanation'), format_title)
            sheet.merge_range(rowx, 5, rowx + 1, 5, _('Quantity'), format_title)
            sheet.merge_range(rowx, 6, rowx + 1, 6, _('Date'), format_title)
            sheet.merge_range(rowx, 7, rowx + 1, 7, _('Asset value'), format_title)
            rowx += 1
            assets = self.env['account.asset.asset'].search([('user_id', '=', employee.id), ('state', '=', 'open'), ('is_supply_asset', '=', False)])
            i = 0
            seq = 1
            rowx += 1
            categories = self.get_category(employee)
            categ_asset_count = 0
            total_asset_count = 0
            for category in categories:
                categ = self.env['account.asset.category'].browse(category)
                categ_asset_count = len(assets.filtered(lambda a: a.category_id.id == category))
                total_asset_count += categ_asset_count
                if categ_asset_count:
                    sheet.merge_range(rowx + i, 0, rowx + i, 4, categ.name, format_group_left)
                    sheet.write(rowx + i, 5, categ_asset_count, format_group_number)
                    sheet.write(rowx + i, 6, '', format_group_left)
                    sheet.write(rowx + i, 7, '', format_group_left)
                    i += 1
                for asset in assets:
                    if category == asset.category_id.id:
                        sheet.write(rowx + i, 0, seq, format_content_number)
                        sheet.write(rowx + i, 1, asset.code, format_content_number)
                        sheet.write(rowx + i, 2, asset.name, format_content_text)
                        sheet.write(rowx + i, 3, asset.location_id.name, format_content_text)
                        sheet.write(rowx + i, 4, asset.note, format_content_text)
                        sheet.write(rowx + i, 5, 1, format_content_number)
                        sheet.write(rowx + i, 6, asset.purchase_date, format_content_date)
                        sheet.write(rowx + i, 7, asset.value_residual, format_content_float)
                        seq += 1
                        i += 1
            if total_asset_count:
                rowx += i
                sheet.merge_range(rowx, 0, rowx, 4, _('Total'), format_group_left)
                sheet.write(rowx, 5, total_asset_count, format_group_number)
                sheet.write(rowx, 6, _(''), format_group_left)
                sheet.write_formula(rowx, 7, '{=SUM(' + xl_rowcol_to_cell(rowx - i + 1, 7) + ':' + xl_rowcol_to_cell(rowx - 1, 7) + ')}', format_group_float)
                rowx += 2
            else:
                rowx += i + 1
            i = 0
            for asset in assets:
                sheet.merge_range(rowx, 4, rowx, 6, '%s: ........................................... (%s)' % (_('Owner'), asset.user_id.name,), format_filter)
                sheet.merge_range(rowx, 1, rowx, 3, '%s: ........................................... (%s)' % (_('Accountant'), asset.partner_id.name if asset.partner_id else '',), format_filter)
            rowx += 5
        book.close()

        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report()

    def get_category(self, employee):
        if self.type != 'category':
            employee_assets = self.env['account.asset.asset'].search([('company_id', '=', self.company_id.id),
                                                                      ('user_id', '=', employee.id),
                                                                      ('state', '=', 'open'),
                                                                      ('is_supply_asset', '=', False)])
            return employee_assets.mapped('category_id').ids
        else:
            return [cat.id if self.env['account.asset.asset'].search([('category_id', '=', cat.id), ('is_supply_asset', '=', False)], limit=1) else False for cat in self.category_ids]

    def get_employee_ids(self):
        AccountAssetAsset = self.env['account.asset.asset']
        HrEmployee = self.env['hr.employee']
        search = [('is_supply_asset', '=', False)]
        if self.type == 'employee':
            return self.employee_ids.ids
        elif self.type in ('category', 'location'):
            category_ids = self.category_ids.ids
            location_ids = self.location_ids.ids
            search.append(('category_id', 'in', category_ids) if self.type == 'category' else ('location_id', 'in', location_ids))
            search.append(('state', '=', 'open'))
        elif self.type == 'department':
            employee_ids = HrEmployee.search([('department_id', 'in', self.department_ids.ids)]).ids
            search.append(('user_id', 'in', employee_ids))
            search.append(('state', '=', 'open'))
        assets = AccountAssetAsset.search(search)
        employee_ids = assets.mapped('user_id').ids
        return employee_ids
