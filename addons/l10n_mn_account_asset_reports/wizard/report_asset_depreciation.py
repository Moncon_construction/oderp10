# -*- encoding: utf-8 -*-
import time
import base64
import xlsxwriter

from odoo import api, fields, models, _
from datetime import datetime
from io import BytesIO
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from operator import itemgetter

class AccountAssetDepreciationReport(models.TransientModel):
    _name = 'account.asset.depreciation.report'
    _description = 'Asset Depreciation Report'

    def _get_companies(self):
        return self.env.user.company_ids

    start_period = fields.Many2one('account.period', 'Start Period', required=True)
    end_period = fields.Many2one('account.period', 'End Period', required=True)
    companies = fields.Many2many('res.company', 'account_asset_depreciation_report_company_rel', 'wizard_id', 'company_id','Company', default=_get_companies, required=True)
    categories = fields.Many2many('account.asset.category', 'account_asset_depreciation_report_category_rel', 'wizard_id', 'category_id','Category')
    employees = fields.Many2many('hr.employee', 'account_asset_depreciation_report_employee_rel', 'wizard_id', 'employee_id','Owner')
    assets = fields.Many2many('account.asset.asset', 'account_asset_depreciation_report_asset_rel', 'wizard_id', 'asset_id','Asset')
    locations = fields.Many2many('account.asset.location', 'account_asset_depreciation_report_location_rel', 'wizard_id', 'location_id','Location')
    
    @api.model
    def default_get(self, default_fields):
        context = dict(self._context or {})
        res = super(AccountAssetDepreciationReport, self).default_get(default_fields)
        # Тухайн өдрийн огноонд тохирох мөчлөгийг дуусах мөчлөгт оноож тухайн санхүүгийн жилийн эхний мөчлөгийг эхлэх мөчлөгт default-р оноох
        end = self.env['account.period'].search([('date_start','<=',datetime.now().date()),
                                                             ('date_stop','>=', datetime.now().date())])
        if end:
            start = self.env['account.period'].search([('date_start','<=',end[0].fiscalyear_id.date_start),
                                                        ('date_stop','>=',end[0].fiscalyear_id.date_start),
                                                        ('fiscalyear_id','=',end[0].fiscalyear_id.id)])
            if start and 'start_period' in default_fields:
                res['start_period'] = start[0].id
            if 'end_period' in default_fields:
                res['end_period'] = end[0].id
        return res
    
    @api.onchange('end_period')
    def onchange_period(self):
        return {'domain': {'companies': [('id','in', self.env.user.company_ids.ids)]}}

    def  get_xsl_column_name(self, index):
        alphabet = {'0': 'A',  '1':'B',  '2':'C',  '3':'D',  '4':'E',
                    '5': 'F',  '6':'G',  '7':'H',  '8':'I',  '9':'J',
                    '10':'K',  '11':'L', '12':'M', '13':'N', '14':'O',
                    '15':'P',  '16':'Q', '17':'R', '18':'S', '19':'T',
                    '20':'U',  '21':'V', '22':'W', '23':'X', '24':'Y', '25':'Z'}
        
        if index <= 25:
            return (alphabet[str(index)] + ":" + alphabet[str(index)])
        else:
            return (alphabet[str(index/26-1)] + alphabet[str(index%26)] + ":" + alphabet[str(index/26-1)] + alphabet[str(index%26)])
        
    @api.multi
    def export_report(self):
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        report_name = _('Asset Depreciation Report')
        file_name = "%s %s.xls" % (report_name, time.strftime('%Y-%m-%d %H:%M'))

        format_content_date = {
            'font_name': 'Arial',
            'font_size': 10,
            'align': 'left',
            'valign': 'vcenter',
            'num_format': 'yyyy-mm-dd'
        }
        format_footer_center = {
            'font_name': 'Arial',
            'font_size': 10,
            'align': 'center',
            'valign': 'vcenter'
        }
        format_table_subtitle_right = {
            'font_name': 'Arial',
            'font_size': 8.5,
            'bold': True,
            'align': 'right',
            'valign': 'vcenter',
            'border': 1,
            'bg_color': '#83CAFF'
        }
        format_table_subtitle_left = {
            'font_name': 'Arial',
            'font_size': 8.5,
            'bold': True,
            'align': 'left',
            'valign': 'vcenter',
            'border': 1,
            'bg_color': '#83CAFF'
        }
        format_table_small_float = {
            'font_name': 'Arial',
            'font_size': 8.5,
            'bold': True,
            'align': 'right',
            'valign': 'vcenter',
            'border': 1,
            'num_format': '#,##0.00',
            'bg_color': '#83CAFF'
        }
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_group = book.add_format(ReportExcelCellStyles.format_group)
        format_group_right = book.add_format(ReportExcelCellStyles.format_group_right)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_left = book.add_format(ReportExcelCellStyles.format_content_left)
        
        format_footer_center = book.add_format(format_footer_center)
        format_content_date = book.add_format(format_content_date)
        format_table_subtitle_right = book.add_format(format_table_subtitle_right)
        format_table_subtitle_left = book.add_format(format_table_subtitle_left)
        format_table_small_float = book.add_format(format_table_small_float)
        
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('Asset Depreciation Report'), form_title=file_name).create({})
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Arial"&9&P', {'margin': 0.1})
        sheet.set_column('A:A', 4)
        sheet.set_column('B:B', 10)
        sheet.set_column('C:C', 30)
        sheet.set_column('D:D', 10)
        sheet.set_column('E:E', 10)
        rowx = 0
        period_ids = self.env['account.period'].search([('date_start','>=',self.start_period.date_start),
                                                        ('date_stop','<=',self.end_period.date_stop)])
        sheet.merge_range(rowx, 0, rowx, 10+len(period_ids),'"%s" LLC' % (self.env.user.company_id.name), format_filter)
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, 10+len(period_ids), report_name, format_name)
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, 10+len(period_ids), _('Date: %s - %s') % (self.start_period.date_start, self.end_period.date_stop), format_content_date)
        rowx += 1
        sheet.merge_range(rowx, 0, rowx+1, 0, _('№'), format_group)
        sheet.merge_range(rowx, 1, rowx+1, 1, _('Asset code'), format_group) # Хөрөнгийн код
        sheet.merge_range(rowx, 2, rowx+1, 2, _('Asset name'), format_group) # Үндсэн хөрөнгийн нэр
        sheet.merge_range(rowx, 3, rowx+1, 3, _('Asset amount'), format_group) # Хөрөнгийн дүн
        sheet.merge_range(rowx, 4, rowx+1, 4, _('Previous depreciated amount'), format_group) # Өмнөх элэгдсэн дүн
        coly = 5
        
        if len(period_ids) > 0:
            sheet.merge_range(rowx, 5, rowx, 5+len(period_ids), _('Estimated depreciation information'), format_group) # Тооцсон элэгдлийн мэдээлэл
        else:
            sheet.write(rowx, 5, _('Estimated depreciation information'), format_group)
        for period in period_ids:
            sheet.write(rowx+1, coly, period.name, format_group)
            coly += 1
        sheet.write(rowx+1, coly, _('Total'), format_group)
        coly += 1
        sheet.set_column(self.get_xsl_column_name(coly), 10)
        sheet.set_column(self.get_xsl_column_name(coly+1), 10)
        sheet.set_column(self.get_xsl_column_name(coly+2), 10)
        sheet.set_column(self.get_xsl_column_name(coly+3), 15)
        sheet.set_column(self.get_xsl_column_name(coly+4), 15)
        sheet.merge_range(rowx, coly, rowx+1, coly, _('Ultimate depreciated amount'), format_group) # Эцсийн элэгдсэн дүн
        sheet.merge_range(rowx, coly+1, rowx+1, coly+1, _('Asset residual cost'), format_group) # Хөрөнгийн үлдэх өртөг
        sheet.merge_range(rowx, coly+2, rowx+1, coly+2, _('Residual amount'), format_group) # Үлдэгдэл дүн
        sheet.merge_range(rowx, coly+3, rowx+1, coly+3, _('Owner'), format_group) # Эзэмшигч
        sheet.merge_range(rowx, coly+4, rowx+1, coly+4, _('Asset location'), format_group) # Хөрөнгийн байрлал
        rowx += 2
        
        """ Хэрэв визардаас утгууд сонгогдсон бол тэр утгад хамаарах хөрөнгүүдийг л тооцох эсвэл бүх хөрөнгийн хувьд """
        where = ' AND (a.is_supply_asset = False or a.is_supply_asset IS NULL) '
        if self.assets:
            line_ids = []
            [line_ids.append(c.id) for c in self.assets]
            where += ' AND a.id in (' + ','.join(map(str, line_ids)) + ') '
        if self.employees:
            line_ids = []
            [line_ids.append(c.id) for c in self.employees]
            where += ' AND e.id in (' + ','.join(map(str, line_ids)) + ') '
        if self.categories:
            line_ids = []
            [line_ids.append(c.id) for c in self.categories]
            where += ' AND c.id in (' + ','.join(map(str, line_ids)) + ') '
        if self.locations:
            line_ids = []
            [line_ids.append(c.id) for c in self.locations]
            where += ' AND l.id in (' + ','.join(map(str, line_ids)) + ') '
        if self.companies:
            line_ids = []
            [line_ids.append(c.id) for c in self.companies]
            where += ' AND r.id in (' + ','.join(map(str, line_ids)) + ') '
        self._cr.execute("SELECT a.id AS aid, c.id AS cid, c.name AS category, a.code, a.name AS asset, a.value, line.amount as depreciation_value, "
                         "line.depreciation_date , a.salvage_value, a.user_id, a.location_id "
                         "FROM account_asset_depreciation_line line "
                         "LEFT JOIN account_asset_asset a ON a.id = line.asset_id "
                         "LEFT JOIN account_asset_category c ON c.id = a.category_id "
                         "LEFT JOIN hr_employee e ON e.id = a.user_id "
                         "LEFT JOIN account_asset_location l ON l.id = a.location_id "
                         "LEFT JOIN res_company r ON r.id = a.company_id "
                         "WHERE line.move_check = 't' AND line.depreciation_date BETWEEN %s AND %s "+where+" "
                         "GROUP BY line.id, a.id, c.id ORDER BY c.id, a.id;", (self.start_period.date_start, self.end_period.date_stop))
        depreciation_lines = self._cr.dictfetchall()
        categ_id = asset_id = False
        """ Нэг хөрөнгийн хувьд нийлбэр тооцоолох обьект """
        asset_compute_depr_total = asset_last_depreciated_value = asset_salvage_value = asset_residual_value = temp_salvage = 0
        """ Нэг ангилалын хувьд нийлбэр тооцоолох обьект """
        category_value = category_before_depreciated_value = category_compute_depr_total = category_last_depreciated_value = category_salvage_value = category_residual_value = 0
        """ Элэгдлийн тайлангын нийт тооцоолсон дүнгүүд """
        total_value = total_before_depreciated_value = total_asset_compute_depr_total = total_asset_last_depreciated_value = total_asset_salvage_value = total_asset_residual_value  =0

        init_depr = 0
        asset_value = 0
        asset_obj = self.env['account.asset.asset']
        count = 1
        total_summaries = []
        categ_summaries = []
        for line in depreciation_lines:
            """ Ямарваа хөрөнгө санагдсан байгаад дараагийн утгыг унших үед уншсан хөрөнгө нь санасан хөрөнгөөс өөр байвал нэг ангилалын хувьд
                тооцсон элэгдлийн нийт дүнд хөрөнгө нэг бүрийн тооцсон элэгдлийн дүнг нэмж 0 утга олгоно """
            categ_name = line['category']
            """ Шинэ ангилал зурах """
            if not categ_id:
                sheet.merge_range(rowx, 0, rowx, 10+len(period_ids), line['category'], format_table_subtitle_left)
                rowx += 1
            elif categ_id != line['cid']:
                if saved_asset:
                    """ Хуучин хөрөнгийн тооцоолсон талбаруудаа зурах """
                    asset_last_depreciated_value = init_depr + asset_compute_depr_total
                    asset_residual_value = asset_value - asset_salvage_value - asset_last_depreciated_value
                    sheet.write(rowx, 5+len(period_ids), asset_compute_depr_total, format_content_float)
                    sheet.write(rowx, 5+len(period_ids)+1, asset_last_depreciated_value, format_content_float)
                    sheet.write(rowx, 5+len(period_ids)+2, asset_salvage_value, format_content_float)
                    sheet.write(rowx, 5+len(period_ids)+3, asset_residual_value , format_content_float)
                    """ Нэг ангилалын хувь дах дүнгүүдийн шинэчлэх """
                    category_value += asset_value or 0
                    category_before_depreciated_value += init_depr or 0
                    category_compute_depr_total += asset_compute_depr_total or 0
                    category_last_depreciated_value += asset_last_depreciated_value or 0
                    category_salvage_value += asset_salvage_value or 0
                    category_residual_value += asset_residual_value or 0
                    asset_compute_depr_total = asset_last_depreciated_value = asset_residual_value = temp_salvage = 0
                    rowx += 1
                    count += 1
                    saved_asset = False
                """ Хуучин ангилалуудын утгыг зурах """
                sheet.merge_range(rowx, 0, rowx, 2, _('Category value'), format_table_subtitle_right)
                sheet.write(rowx, 3, category_value, format_table_small_float)
                sheet.write(rowx, 4, category_before_depreciated_value, format_table_small_float)
                coly = 5
                for summary in categ_summaries:
                    sheet.write(rowx, coly, summary, format_table_small_float)
                    if len(total_summaries) < len(categ_summaries):
                        total_summaries.append(summary)
                    else:
                        total_summaries[coly-5] += summary
                    coly += 1
                sheet.write(rowx, coly, category_compute_depr_total, format_table_small_float)
                coly += 1
                sheet.write(rowx, coly, category_last_depreciated_value, format_table_small_float)
                sheet.write(rowx, coly+1, category_salvage_value, format_table_small_float)
                sheet.write(rowx, coly+2, category_residual_value, format_table_small_float)
                sheet.merge_range(rowx, coly+3, rowx, coly+4, '', format_table_small_float)
                """ Элэгдлийн тайлангийн хувь дах дүнгүүдийн шинэчлэх """
                total_value += category_value or 0
                total_before_depreciated_value += category_before_depreciated_value  or 0
                total_asset_compute_depr_total += category_compute_depr_total or 0
                total_asset_last_depreciated_value += category_last_depreciated_value or 0
                total_asset_salvage_value += category_salvage_value or 0
                total_asset_residual_value += category_residual_value or 0
                category_value = category_before_depreciated_value = category_compute_depr_total = category_last_depreciated_value = category_salvage_value = category_residual_value = 0
                categ_summaries = []
                rowx += 1
                sheet.merge_range(rowx, 0, rowx, 10+len(period_ids), categ_name, format_table_subtitle_left)
                rowx += 1
            """ Эхлэлийн утгуудыг бэлдэх """
            if not asset_id or asset_id != line['aid']:
                if asset_id and saved_asset:
                    """ Хуучин хөрөнгийн тооцоолсон талбаруудаа зурах """
                    if not init_depr:init_depr = 0
                    asset_last_depreciated_value = init_depr + asset_compute_depr_total
                    asset_residual_value = asset_value - asset_salvage_value - asset_last_depreciated_value
                    sheet.write(rowx, 5+len(period_ids), asset_compute_depr_total, format_content_float)
                    sheet.write(rowx, 5+len(period_ids)+1, asset_last_depreciated_value, format_content_float)
                    sheet.write(rowx, 5+len(period_ids)+2, asset_salvage_value, format_content_float)
                    sheet.write(rowx, 5+len(period_ids)+3, asset_residual_value , format_content_float)
                    """ Нэг ангилалын хувь дах дүнгүүдийн шинэчлэх """
                    category_value += asset_value or 0
                    category_before_depreciated_value += init_depr or 0
                    category_compute_depr_total += asset_compute_depr_total or 0
                    category_last_depreciated_value += asset_last_depreciated_value or 0
                    category_salvage_value += asset_salvage_value or 0
                    category_residual_value += asset_residual_value or 0
                    asset_compute_depr_total = asset_last_depreciated_value = asset_residual_value = temp_salvage = 0
                    rowx += 1
                    count += 1
                    saved_asset = False
                asset_value = line['value'] or 0
                asset_salvage_value = line['salvage_value'] or 0
                self._cr.execute("SELECT SUM(amount) AS sum FROM account_asset_depreciation_line "
                                 "WHERE asset_id = %s AND move_check = 't' AND depreciation_date <= %s;",(line['aid'], self.start_period.date_start))
                summary_line = self._cr.dictfetchall()
                init_depr = summary_line[0]['sum'] or 0
                loc_name = u_name = ''
                if line['location_id']:
                    loc_name = self.env['account.asset.location'].browse(line['location_id']).name
                if line['user_id']:
                    u_name = self.env['hr.employee'].browse(line['user_id']).name
                """ Шинэ хөрөнгө зурах """
                sheet.write(rowx, 0, count, format_content_number)
                sheet.write(rowx, 1, line['code'], format_content_number)
                sheet.write(rowx, 2, line['asset'], format_content_left)
                sheet.write(rowx, 3, asset_value, format_content_float)
                sheet.write(rowx, 4, init_depr, format_content_float)
                sheet.write(rowx, 8+len(period_ids)+1, u_name, format_content_left)
                sheet.write(rowx, 9+len(period_ids)+1, loc_name, format_content_left)
                """ Мөчлөгүүдээрх ангилалын элэгдлийн дүнгүүдийг хадгалах сав бэлтгэх мөн хүснэгтийг зурах"""
                coly = 5
                for period in period_ids:
                    sheet.write(rowx, coly, '', format_content_float)
                    if len(categ_summaries) < len(period_ids):
                        categ_summaries.append(0.0)
                    coly += 1
            """ Зурагдсан байгаа хөрөнгийн элэгдлийн бичилтийг л зурах """
            coly = 5
            ind = 0
            for period in period_ids:
                """ Нэг хөрөнгийн элэгдлийн бичилтийг элэгдсэн хугацаанд нь тохирох мөчлөгийн харалдаа зурах"""
                if period.date_start <= line['depreciation_date'] and period.date_stop >= line['depreciation_date']:
                    """ Нэг хөрөнгийн хувьд - элэгдлийн нийт дүнг хадгалах обьектод элэгдсэн дүн бүрийг нэмнэ """
                    asset_compute_depr_total += line['depreciation_value'] or 0
                    temp_salvage += 1
                    sheet.write(rowx, coly, line['depreciation_value'], format_content_float)
                    """ ! """
                    categ_summaries[ind] += line['depreciation_value'] or 0
                    break
                coly += 1
                ind += 1
            """ Хөрөнгийн ангилал болон хөрөнгийг санаж авах """
            categ_id = line['cid']
            asset_id = line['aid']
            saved_asset = True
        rowx += 1
        if asset_id:
            """ Хуучин хөрөнгийн тооцоолсон талбаруудаа зурах """
            rowx -= 1
            asset_last_depreciated_value = init_depr + asset_compute_depr_total
            asset_residual_value = asset_value - asset_salvage_value - asset_last_depreciated_value
            sheet.write(rowx, 5+len(period_ids), asset_compute_depr_total, format_content_float)
            sheet.write(rowx, 5+len(period_ids)+1, asset_last_depreciated_value, format_content_float)
            sheet.write(rowx, 5+len(period_ids)+2, asset_salvage_value, format_content_float)
            sheet.write(rowx, 5+len(period_ids)+3, asset_residual_value , format_content_float)
            """ Нэг ангилалын хувь дах дүнгүүдийн шинэчлэх """
            category_value += asset_value or 0
            category_before_depreciated_value += init_depr or 0
            category_compute_depr_total += asset_compute_depr_total or 0
            category_last_depreciated_value += asset_last_depreciated_value or 0
            category_salvage_value += asset_salvage_value or 0
            category_residual_value += asset_residual_value or 0
            rowx += 1
            """ Хуучин ангилалуудын утгыг зурах """
            sheet.merge_range(rowx, 0, rowx, 2, _('Category value'), format_table_subtitle_right)
            sheet.write(rowx, 3, category_value, format_table_small_float)
            sheet.write(rowx, 4, category_before_depreciated_value, format_table_small_float)
            coly = 5
            for summary in categ_summaries:
                sheet.write(rowx, coly, summary, format_table_small_float)
                if len(total_summaries) < len(categ_summaries):
                    total_summaries.append(summary)
                else:
                    total_summaries[coly-5] += summary
                coly += 1
            sheet.write(rowx, coly, category_compute_depr_total, format_table_small_float)
            coly += 1
            sheet.write(rowx, coly, category_last_depreciated_value, format_table_small_float)
            sheet.write(rowx, coly+1, category_salvage_value, format_table_small_float)
            sheet.write(rowx, coly+2, category_residual_value, format_table_small_float)
            sheet.merge_range(rowx, coly+3, rowx, coly+4, '', format_table_small_float)
            """ Элэгдлийн тайлангийн хувь дах дүнгүүдийн шинэчлэх """
            total_value += category_value or 0
            total_before_depreciated_value += category_before_depreciated_value  or 0
            total_asset_compute_depr_total += category_compute_depr_total or 0
            total_asset_last_depreciated_value += category_last_depreciated_value or 0
            total_asset_salvage_value += category_salvage_value or 0
            total_asset_residual_value += category_residual_value or 0
            categ_summaries = []
            rowx += 1
            
            sheet.merge_range(rowx, 0, rowx, 2, _('Total'), format_group_right)
            sheet.write(rowx, 3, total_value, format_group_float)
            sheet.write(rowx, 4, total_before_depreciated_value , format_group_float)
            coly = 5
            for summ in total_summaries:
                sheet.write(rowx, coly, summ, format_group_float)
                coly += 1
            sheet.write(rowx, coly, total_asset_compute_depr_total, format_group_float)
            sheet.write(rowx, coly+1, total_asset_last_depreciated_value, format_group_float)
            sheet.write(rowx, coly+2, total_asset_salvage_value, format_group_float)
            sheet.write(rowx, coly+3, total_asset_residual_value, format_group_float)
            sheet.merge_range(rowx, coly+4, rowx, coly+5, '', format_group_float)
            rowx += 2
        sheet.merge_range(rowx, 0, rowx, coly+4, _('Recorderd accountant : .................................. /                                   /'), format_footer_center)
        rowx += 1
        sheet.merge_range(rowx, 0, rowx, coly+4, _('Reviewed accountant : .................................. /                               /'), format_footer_center)    
        book.close()
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        return report_excel_output_obj.export_report()
        