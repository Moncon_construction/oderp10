# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time, calendar
from datetime import date, datetime, timedelta
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from operator import itemgetter

from odoo import api, fields, models, _

from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class ReportAssetDetail(models.TransientModel):
    _name = "report.asset.detail"
    _description = "Asset Detail Report"

    def _get_last_day_of_month(self):
        range = calendar.monthrange(int(time.strftime('%Y')), int(time.strftime('%m')))
        return time.strftime('%Y-%m') + '-' + str(range[1])

    def _get_fiscalyear_id(self):
        res = False
        now = time.strftime('%Y-%m-%d')
        fiscalyear_obj = self.env['account.fiscalyear']
        fiscalyear_id = fiscalyear_obj.search([('date_start', '<', now), ('date_stop', '>', now)], limit=1)
        if fiscalyear_id:
            res = fiscalyear_id[0]
        return res

    def _get_type(self):
        type = 'summary'
        if self._context is None:
            self._context = {}
        if self._context.has_key('each_report'):
            if self._context['each_report'] == 'detail':
                type = 'detail'
        return type
    
    def _get_parent_department(self,categ_ids):
        parent_categ_ids = {}
        parent_categ = []
        sub_parent_categ = []
        if categ_ids:
            categs = self.env['account.asset.category'].search([('id', 'in', categ_ids)])
            for categ in categs:
                if categ.id not in parent_categ_ids:
                    categ_parents = []
                    parent_categ_ids[categ.id] = categ_parents
                    while categ:
                        categ_parents.append(categ)
                        if categ not in sub_parent_categ:
                            sub_parent_categ.append(categ)
                        categ = categ.parent_id                       
                    
                    categ_parents.reverse()
                if categ_parents[0] not in parent_categ:
                    parent_categ.append(categ_parents[0])
        return parent_categ,sub_parent_categ

    from_date = fields.Date('Start Date', required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    to_date = fields.Date('End Date', required=True, default=_get_last_day_of_month)
    asset_categ_ids = fields.Many2many('account.asset.category', 'wizard_asset_report_asset_categ_rel', 'wizard_id', 'categ_id', 'Asset Category')
    location_ids = fields.Many2many('account.asset.location', 'wizard_asset_report_asset_location_rel', 'wizard_id', 'location_id', 'Location')
    # 'account_asset_ids':fields.many2many('account.account', 'wizard_asset_report_account_rel', 'wizard_id', 'account_id', 'Asset account', domain=[('type','not in',['view','consolidation'])]),
    asset_asset_ids = fields.Many2many('account.asset.asset', 'wizard_asset_report_asset_rel', 'wizard_id', 'asset_id', 'Asset account', domain=[('state', 'not in', ['draft']), ('is_supply_asset', '=', False)])
    owner_id = fields.Many2one('hr.employee', 'Owner')
    fiscalyear_id = fields.Many2one('account.fiscalyear', 'Fiscal Year', required=True, default=_get_fiscalyear_id)
    order_by = fields.Selection([('name', 'Нэр'), ('date', 'Өдөр')], 'Order By', required=True, default=lambda *a: 'name')
    type = fields.Selection([('detail', 'Detail'), ('summary', 'Summary')], 'Type', required=True, default=_get_type)
    state = fields.Selection([('open', 'Open'), ('all', 'All'), ('closed', 'Closed')], 'State', required=True, default='all')
    purchased = fields.Boolean('Purchased', default=False)

    @api.multi
    def export(self):
        report_obj = self
        user = self.env['res.users'].search([('id', '=', self._uid)])
        company = user.company_id
        from_date = report_obj.from_date
        to_date = report_obj.to_date
        asset_categ_ids = report_obj.asset_categ_ids
        account_asset_ids = report_obj.asset_asset_ids
        location_ids = report_obj.location_ids
        # account_asset_ids = report_obj.account_asset_ids
        owner_id = report_obj.owner_id
        fiscalyear_id = report_obj.fiscalyear_id
        order_by = report_obj.order_by
        type = report_obj.type
        state = report_obj.state
        purchased = report_obj.purchased

        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Asset Detail Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        # Custom for standart
        format_name = {
            'font_name': 'Times New Roman',
            'font_size': 14,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter'
        }
        # create formats
        format_content_text_footer = {
        'font_name': 'Times New Roman',
        'font_size': 10,
        'align': 'vcenter',
        'valign': 'vcenter',
        }
        format_content_right = {
        'font_name': 'Times New Roman',
        'font_size': 9,
        'align': 'right',
        'valign': 'vcenter',
        'border': 1,
        'num_format': '#,##0.00'
        }
        format_content_right_bold = {
        'font_name': 'Times New Roman',
        'font_size': 10,
        'bold': True,
        'align': 'right',
        'valign': 'vcenter',
        'border': 1,
        'num_format': '#,##0.00'
        }
        format_content_left_bold = {
        'font_name': 'Times New Roman',
        'font_size': 10,
        'bold': True,
        'align': 'left',
        'valign': 'vcenter',
        'border': 1,
        'num_format': '#,##0.00'
        }
        format_group_center = {
        'font_name': 'Times New Roman',
        'font_size': 10,
        'align': 'center',
        'valign': 'vcenter',
        'border': 1,
        }
        format_group_center_bold = {
        'font_name': 'Times New Roman',
        'font_size': 10,
        'bold': True,
        'align': 'center',
        'valign': 'vcenter',
        'border': 1,
        'bg_color': '#CFE7F5',
        }
        format_group = {
        'font_name': 'Times New Roman',
        'font_size': 10,
        'bold': True,
        'align': 'center',
        'valign': 'vcenter',
        'border': 1,
        'bg_color': '#CFE7F5',
        'num_format': '#,##0.00'
        }
        format_group_bold = {
        'font_name': 'Times New Roman',
        'font_size': 10,
        'bold': True,
        'align': 'center',
        'valign': 'vcenter',
        'border': 1,
        'num_format': '#,##0.00'
        }
        format_group_center = book.add_format(format_group_center)
        format_name = book.add_format(format_name)
        format_content_text_footer = book.add_format(format_content_text_footer)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_group_right = book.add_format(ReportExcelCellStyles.format_group_right)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_group_left = book.add_format(ReportExcelCellStyles.format_group_left)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_center = book.add_format(ReportExcelCellStyles.format_content_center)
        format_group = book.add_format(format_group)
        format_group_bold = book.add_format(format_group_bold)
        format_content_left_bold = book.add_format(format_content_left_bold)
        format_group_center_bold = book.add_format(format_group_center_bold)  
        

        format_content_right = book.add_format(format_content_right)
        format_content_right_bold = book.add_format(format_content_right_bold)
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('asset_detail_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 0

        # compute column
        colx_number = 3
        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 15)
        sheet.set_column('C:C', 15)
        sheet.set_column('D:D', 6)
        sheet.set_column('E:E', 8)
        sheet.set_column('F:F', 15)
        sheet.set_column('G:G', 15)
        sheet.set_column('H:H', 15)
        sheet.set_column('I:I', 15)
        sheet.set_column('J:J', 15)
        sheet.set_column('K:K', 15)
        sheet.set_column('L:L', 15)
        sheet.set_column('M:M', 15)
        sheet.set_column('N:N', 15)
        sheet.set_column('O:O', 15)
        sheet.set_column('P:P', 15)
        sheet.set_column('Q:Q', 15)
        sheet.set_column('R:R', 15)
        sheet.set_column('S:S', 15)
        sheet.set_column('T:T', 10)
        sheet.set_column('U:U', 15)
        sheet.set_column('V:V', 10)

        # create name
        sheet.write(rowx, 0, '%s' % (company.name), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 1, rowx, 22, report_name.upper(), format_name)
        # report duration
        sheet.write(rowx + 1, 0, '%s: %s - %s' % (_('Duration'), self.from_date, self.to_date), format_filter)
        # create date
        sheet.write(rowx + 2, 0, '%s: %s' % (_('Created On'), time.strftime('%Y-%m-%d')), format_filter)
        rowx += 3

        sheet.merge_range(rowx, 0, rowx + 2, 0, _(u'№'), format_title)  # seq
        sheet.merge_range(rowx, 1, rowx + 2, 1, _(u'Хөрөнгийн дугаар'), format_title)  # Хөрөнгийн дугаар
        sheet.merge_range(rowx, 2, rowx + 2, 2, _(u'Үндсэн хөрөнгийн нэр'), format_title)  # Үндсэн хөрөнгийн нэр
        sheet.merge_range(rowx, 3, rowx + 2, 3, _(u'Элэгдэх тоо'), format_title)  # Элэгдэх тоо
        sheet.merge_range(rowx, 4, rowx + 2, 4, _(u'Худалдан авсан огноо'), format_title)  # Худалдан авсан огноо
        sheet.merge_range(rowx, 5, rowx, 12, _(u'Өртөг'), format_title)  # Өртөг
        sheet.merge_range(rowx + 1, 5, rowx + 2, 5, _(u'Эхний'), format_title)  # Эхний
        sheet.merge_range(rowx + 1, 6, rowx + 1, 7, _(u'Орлого'), format_title)  # Орлого
        sheet.write(rowx + 2, 6, _(u'Худалдан aвалт'), format_title)  # Худалдан aвалт
        sheet.write(rowx + 2, 7, _(u'Хөдөлгөөнөөр'), format_title)  # Хөдөлгөөнөөр
        sheet.merge_range(rowx + 1, 8, rowx + 2, 8, _(u'Капиталжуулалт'), format_title)  # Капиталжуулалт
        sheet.merge_range(rowx + 1, 9, rowx + 2, 9, _(u'Дахин үнэлгээ'), format_title)  # Дахин үнэлгээ
        sheet.merge_range(rowx + 1, 10, rowx + 1, 11, _(u'Зарлага'), format_title)  #
        sheet.write(rowx + 2, 10, _(u'Хаагдсан'), format_title)  # Хаагдсан
        sheet.write(rowx + 2, 11, _(u'Хөдөлгөөнөөр'), format_title)  # Хөдөлгөөнөөр
        sheet.merge_range(rowx + 1, 12, rowx + 2, 12, _(u'Эцсийн'), format_title)  # Эцсийн
        sheet.merge_range(rowx, 13, rowx, 18, _(u'Хуримтлагдсан элэгдэл'), format_title)  # Өртөг
        sheet.merge_range(rowx + 1, 13, rowx + 1, 14, _(u'Эхний'), format_title)  #
        sheet.write(rowx + 2, 13, _(u'Эхний'), format_title)  # Эхний
        sheet.write(rowx + 2, 14, _(u'Хөдөлгөөнөөр'), format_title)  # Хөдөлгөөнөөр
        sheet.merge_range(rowx + 1, 15, rowx + 2, 15, _(u'Нэмэгдсэн'), format_title)  # Нэмэгдсэн
        sheet.merge_range(rowx + 1, 16, rowx + 1, 17, _(u'Хасагдсан'), format_title)  # Хасагдсан
        sheet.write(rowx + 2, 16, _(u'Хаалтаар'), format_title)  # Хаалтаар
        sheet.write(rowx + 2, 17, _(u'Хөдөлгөөнөөр'), format_title)  # Хөдөлгөөнөөр
        sheet.merge_range(rowx + 1, 18, rowx + 2, 18, _(u'Эцсийн'), format_title)  # Эцсийн
        sheet.merge_range(rowx, 19, rowx + 2, 19, _(u'Үлдэх өртөг'), format_title)  # Үлдэх өртөг
        sheet.merge_range(rowx, 20, rowx + 2, 20, _(u'Эд хариуцагч'), format_title)  # Эд хариуцагч
        sheet.merge_range(rowx, 21, rowx + 2, 21, _(u'Байршил'), format_title)  # Байршил
        rowx += 3

        total_initial = total_purchase = total_move_income = 0
        total_capital = total_revaluation = total_closed = total_move_expense = 0
        total_c2_value = total_initial_dep = total_initmove_dep = total_move_dep = total_depreciation = 0
        total_deduct_dep = total_deduct_move_dep = total_end_dep = total_residual = 0
        sub_initial = sub_purchase = sub_move_income = 0
        sub_capital = sub_revaluation = sub_closed = sub_move_expense = 0
        sub_c2_value = sub_initial_dep = sub_initmove_dep = sub_move_dep = sub_depreciation = 0
        sub_deduct_dep = sub_deduct_move_dep = sub_end_dep = sub_residual = 0
        asset = []
        order_by = ' '
        where = ' AND (a.is_supply_asset = False or a.is_supply_asset IS NULL) '
        if self.order_by == 'name':
            order_by = ", a.name, sm.seq "
        else:
            order_by = ", a.purchase_date, sm.seq "
        if owner_id:
            where += ' AND h.id = ' + str(owner_id.id) + ' '
        if account_asset_ids:
            line_ids = []
            [line_ids.append(c.id) for c in account_asset_ids]
            where += ' AND a.id in (' + ','.join(map(str, line_ids)) + ') '
        if asset_categ_ids:
            line_ids = []
            [line_ids.append(c.id) for c in asset_categ_ids]
            where += ' AND c.id in (' + ','.join(map(str, line_ids)) + ') '
        if location_ids:
            line_ids = []
            [line_ids.append(c.id) for c in location_ids]
            where += ' AND l.id in (' + ','.join(map(str, line_ids)) + ') '
        if state == 'open':
            where += " AND state = 'open' "
        elif state == 'closed':
            where += " AND state = 'close' "
        if purchased:
            where += " AND a.purchase_date BETWEEN '%s' AND '%s' " % (self.from_date, self.to_date)

        query = """
            SELECT
                sm.asset_id, a.id AS id, a.code AS code, a.name AS name, 
                COALESCE(a.value,0) - his2.capital - his2.revaluation - his2.af_capital - his2.af_revaluation AS pvalue, 
                a.purchase_date AS pdate, COALESCE(a.initial_depreciation,0) AS idep,
                sum(sm.init_dep) AS init_dep, sum(sm.added_dep) AS added_dep, c.id AS cid, c.name AS cname, l.name AS lname, h.name_related AS hname, 
                c.method_number * c.method_period/12 AS dep_year, a.method_number as method_number, his2.capital AS capital, 
                his2.revaluation AS revaluation, sum(sm.depreciation) AS depreciation, sum(sm.close_am) AS close_am, 
                sum(sm.in_capital) AS in_capital, sum(sm.in_revaluation) AS in_revaluation, sum(sm.in_depreciation) AS in_depreciation, 
                a.state AS state, sm.seq AS seq, co.count AS count, inv_count.inv_count AS invoice, pur.pur_count AS purchase 
            FROM 
            (
                SELECT 
                    a.asset_id AS asset_id, 
                    CASE WHEN a.receipt_date >= '%s' AND a.receipt_date <= '%s' THEN a.capital ELSE 0.0 END AS capital, 
                    CASE WHEN a.receipt_date >= '%s' AND a.receipt_date <= '%s' THEN a.revaluation ELSE 0.0 END AS revaluation, 
                    CASE WHEN a.receipt_date >= '%s' THEN a.capital ELSE 0.0 END AS af_capital, 
                    CASE WHEN a.receipt_date >= '%s' THEN a.revaluation ELSE 0.0 END AS af_revaluation, 
                    CASE WHEN a.receipt_date <= '%s' THEN a.depreciation ELSE 0.0 END AS depreciation, 
                    CASE WHEN a.receipt_date <= m.date_start THEN a.capital ELSE 0.0 END AS in_capital, 
                    CASE WHEN a.receipt_date <= m.date_start THEN a.revaluation ELSE 0.0 END AS in_revaluation, 
                    CASE WHEN a.receipt_date <= m.date_start THEN a.depreciation ELSE 0.0 END AS in_depreciation, 
                    CASE WHEN a.receipt_date < '%s' THEN a.depreciation ELSE 0.0 END AS init_dep, 
                    CASE WHEN '%s' < a.receipt_date AND a.receipt_date <= '%s' THEN a.depreciation ELSE 0.0 END AS added_dep, a.receipt_date as receipt_date, 
                    a.close_am AS close_am, m.seq AS seq, m.categ_id AS categ_id, m.user_id AS user_id, m.loc_id AS loc_id 
                FROM 
                (
                    SELECT 
                        move.id AS aid, 1 AS seq, move_line.old_category_id AS categ_id, move.move_date AS date_start, move.receipt_date AS date_to, 
                        move_line.old_owner_id AS user_id, move_line.old_location_id AS loc_id 
                    FROM account_asset_moves_line move_line
                    LEFT JOIN account_asset_moves move ON move.id = move_line.move_id
                    WHERE 
                        move.move_date < '%s' AND move.receipt_date > '%s' AND move_line.old_category_id != move_line.new_category_id 
                        AND move_line.old_category_id IS NOT NULL AND move_line.new_category_id IS NOT NULL 
            
                    UNION 
            
                    SELECT 
                        move.id AS aid, row_number() over (partition by move.name order by move.receipt_date) AS seq, 
                        move_line.old_category_id AS categ_id, move.move_date AS date_start, move.receipt_date AS date_to, move_line.old_owner_id AS user_id, 
                        move_line.old_location_id AS loc_id 
                    FROM account_asset_moves_line move_line
                    LEFT JOIN account_asset_moves move ON move.id = move_line.move_id
                    WHERE 
                        move.receipt_date BETWEEN '%s' AND '%s' AND move_line.old_category_id != move_line.new_category_id 
                        AND move_line.old_category_id IS NOT NULL AND move_line.new_category_id IS NOT NULL 
            
                    UNION ALL 
            
                    SELECT 
                        a.id AS aid, Null AS seq, a.category_id AS categ_id, amh.receipt_date AS date_start,'%s' AS date_to, a.user_id AS user_id, a.location_id AS loc_id 
                    FROM account_asset_asset a 
                    LEFT JOIN 
                    (
                    SELECT 
                        move_line.asset_id AS aid, move.receipt_date AS receipt_date 
                    FROM account_asset_moves_line move_line
                    LEFT JOIN account_asset_moves move ON move.id = move_line.move_id
                    WHERE move.receipt_date BETWEEN '%s' AND '%s' 
                    ORDER BY move.receipt_date DESC, move.id DESC LIMIT 1
                    ) AS amh ON (a.id = amh.aid) 
                    
                ) AS m JOIN 
                (
                    SELECT 
                        'a' || cast(a.id as text) AS name, a.id AS asset_id, 0 AS capital, 0 AS revaluation, 0 AS depreciation, 
                        0 AS close_am , purchase_date AS receipt_date 
                    FROM account_asset_asset a 
                    WHERE a.purchase_date <= '%s' 
                    GROUP BY a.id, receipt_date 
            
                    UNION ALL 
            
                    SELECT 'c' || cast(a.id as text) AS name, a.id AS asset_id, 0 AS capital, 0 AS revaluation, 0 AS depreciation, 
                        SUM(COALESCE(l.credit,0)) AS close_am, l.date AS receipt_date 
                    FROM account_asset_asset a 
                    LEFT JOIN account_asset_category c ON (a.category_id = c.id) 
                    LEFT JOIN account_move_line l ON (l.asset_id = a.id AND c.account_asset_id = l.account_id) 
                    LEFT JOIN account_asset_history r ON(r.asset_id = a.id)
                    WHERE l.credit > 0 and a.id  in ( SELECT 
                         r.asset_id AS asset_id
                    FROM account_asset_history r
                    WHERE r.date <= '%s' or r.date <= '%s'
                    GROUP BY r.asset_id) 
                    GROUP BY a.id, l.date  

            
                    UNION ALL 
            
                    SELECT 'ca' || cast(l.id as text) AS name, r.asset_id AS asset_id, SUM(COALESCE(r.amount,0))  AS capital, 
                        0 AS revaluation, 0 AS depreciation, 0 AS close_am, l.date AS receipt_date /*capital*/ 
            
                    FROM account_asset_history r 
                    LEFT JOIN account_move l ON (r.move_id = l.id) 
                    where r.action = 'capital' 
                    GROUP BY l.id, r.asset_id, l.date 
            
                    UNION ALL /*revaluation*/ 
            
                    SELECT 're' || cast(l.id as text) AS name, r.asset_id AS asset_id, 0 AS capital, 
                        SUM(COALESCE(r.amount,0)) AS revaluation, 0 AS depreciation, 0 AS close_am, 
                        l.date AS receipt_date 
                    FROM account_asset_history r 
                    LEFT JOIN account_move l ON (r.move_id = l.id) 
                    where r.action = 'revaluation' 
                    GROUP BY l.id, r.asset_id, l.date 
            
                    UNION ALL 
            
                    SELECT 'de' || cast(l.id as text) AS name, l.asset_id AS asset_id, 0 AS capital, 
                        0 AS revaluation, SUM(COALESCE(amount,0)) AS depreciation, 0 AS close_am, l.depreciation_date AS receipt_date 
                    FROM account_asset_depreciation_line l 
                    WHERE l.move_check = 't' 
                    GROUP BY l.id, l.asset_id, l.depreciation_date 
                ) AS a  ON (a.asset_id = m.aid) 
                GROUP BY a.asset_id, m.seq, m.categ_id, m.user_id, m.loc_id, a.capital, a.revaluation, a.depreciation, a.close_am, a.name, m.date_start, a.receipt_date 
            ) AS sm  
            LEFT JOIN account_asset_asset a ON (a.id = sm.asset_id) 
            LEFT JOIN 
            (
                SELECT count(move_line_id) AS count, c.aid AS aid 
                FROM     
                (
                    SELECT move_line.asset_id AS aid, move_line.id as move_line_id
                    FROM account_asset_moves_line move_line
                    LEFT JOIN account_asset_moves move ON move.id = move_line.move_id
                    WHERE move.receipt_date BETWEEN '%s' AND '%s'  
                ) AS c GROUP BY c.aid 
            ) AS co ON (co.aid = a.id) 
            LEFT JOIN 
            (
                SELECT count(invoice) AS inv_count, invoice.asset_id AS aid 
                FROM     
                (
                    SELECT a.id AS asset_id, inv.id AS invoice 
                    FROM account_asset_asset a 
                    LEFT JOIN account_invoice inv ON (inv.id = a.invoice_id) 
                    WHERE inv.type = 'in_invoice' AND a.purchase_date BETWEEN '%s' AND '%s' 
                ) AS invoice GROUP BY invoice.asset_id
            ) AS inv_count ON (inv_count.aid = a.id) 
            LEFT JOIN 
            (
                SELECT count(pur_id) AS pur_count, purchase.asset_id AS aid 
                FROM     
                (
                    SELECT a.id AS asset_id, purch.id AS pur_id 
                    FROM account_asset_asset a 
                    LEFT JOIN purchase_asset_picking purch ON (purch.id = a.picking_id) 
                    WHERE a.purchase_date BETWEEN '%s' AND '%s' 
                ) AS purchase GROUP BY purchase.asset_id
            ) AS pur ON (pur.aid = a.id)  
            LEFT JOIN 
            (
                SELECT his1.asset_id AS aid, sum(his1.capital) as capital, sum(his1.revaluation) as revaluation, sum(his1.af_capital) as af_capital, sum(his1.af_revaluation) as af_revaluation  
                FROM     
                (
                    SELECT a.id AS asset_id, history.id AS history_id,  
                    CASE WHEN history.date >= '%s' AND history.date <= '%s' AND history.action='capital' THEN sum(history.amount) ELSE 0.0 END AS capital, 
                    CASE WHEN history.date >= '%s' AND history.date <= '%s' AND history.action='revaluation'  THEN sum(history.amount) ELSE 0.0 END AS revaluation, 
                    CASE WHEN history.date >= '%s' AND history.action='capital' THEN sum(history.amount)  ELSE 0.0 END AS af_capital, 
                    CASE WHEN history.date >= '%s' AND history.action='revaluation' THEN sum(history.amount) ELSE 0.0 END AS af_revaluation  
                    FROM account_asset_asset a 
                    LEFT JOIN account_asset_history history ON history.asset_id = a.id GROUP BY a.id , history.id 
                ) AS his1 GROUP BY his1.asset_id
            ) AS his2 ON (his2.aid = a.id) 
            LEFT JOIN account_asset_category c ON (sm.categ_id = c.id) 
            LEFT JOIN account_asset_location l ON (sm.loc_id = l.id) 
            LEFT JOIN hr_employee h ON (h.id = sm.user_id)
            LEFT JOIN account_asset_history hist ON sm.asset_id = hist.asset_id  
            WHERE a.purchase_date <= '%s' AND a.company_id = %s AND  ((hist.date >= '%s' OR hist.date >= '%s' OR (hist.action = 'sale' AND hist.action = 'close')) OR (hist.id IS NULL OR (hist.action != 'sale' AND hist.action != 'close'))) %s 
            GROUP BY a.id, a.name, a.value, a.purchase_date, a.initial_depreciation,  c.id, c.name, l.name, h.name_related, a.state, sm.seq, co.count , sm.asset_id, inv_count.inv_count, pur.pur_count, 
            his2.capital, his2.revaluation, his2.af_capital, his2.af_revaluation  
            ORDER BY c.name %s
        """ %(self.from_date, self.to_date, self.from_date, self.to_date, self.to_date, self.to_date, self.to_date, self.from_date, self.from_date, self.to_date,
              self.from_date, self.from_date, self.from_date, self.to_date,self.to_date,
              self.from_date, self.to_date, self.to_date, self.to_date, self.from_date, self.from_date,
              self.to_date,self.from_date, self.to_date,self.from_date, self.to_date,self.from_date, self.to_date,self.from_date, self.to_date, self.to_date, self.to_date, self.to_date, company.id, self.from_date, self.to_date, where, order_by)
        self._cr.execute(query)

        lines = self._cr.dictfetchall()

        total_initial = total_purchase = total_move_income = 0
        total_capital = total_revaluation = total_closed = total_move_expense = 0
        total_c2_value = total_initial_dep = total_initmove_dep = total_move_dep = total_depreciation = 0
        total_deduct_dep = total_deduct_move_dep = total_end_dep = total_residual = 0
        
        child_sub_initial = child_sub_purchase = child_sub_move_income = 0
        child_sub_capital = child_sub_revaluation = child_sub_closed = child_sub_move_expense = 0
        child_sub_c2_value = child_sub_initial_dep = child_sub_initmove_dep = child_sub_move_dep = child_sub_depreciation = 0
        child_sub_deduct_dep = child_sub_deduct_move_dep = child_sub_end_dep = child_sub_residual = 0
        
        sub_initial = sub_purchase = sub_move_income = 0
        sub_capital = sub_revaluation = sub_closed = sub_move_expense = 0
        sub_c2_value = sub_initial_dep = sub_initmove_dep = sub_move_dep = sub_depreciation = 0
        sub_deduct_dep = sub_deduct_move_dep = sub_end_dep = sub_residual = 0

        categ_ids = []
        if lines:
            counter = 1
            for line in lines:
                categ_ids.append(line['cid'])
            parent_category,parent_category1 = self._get_parent_department(categ_ids)
            for parent_categ in parent_category:
                sub_initial = sub_purchase = sub_move_income = 0
                sub_capital = sub_revaluation = sub_closed = sub_move_expense = 0
                sub_c2_value = sub_initial_dep = sub_move_dep = sub_depreciation = 0
                sub_deduct_dep = sub_end_dep = sub_residual = 0
                
                child_sub_initial = child_sub_purchase = child_sub_move_income = 0
                child_sub_capital = child_sub_revaluation = child_sub_closed = child_sub_move_expense = 0
                child_sub_c2_value = child_sub_initial_dep = child_sub_initmove_dep = child_sub_move_dep = child_sub_depreciation = 0
                child_sub_deduct_dep = child_sub_deduct_move_dep = child_sub_end_dep = child_sub_residual = 0
                            
                sub_total_initial = sub_total_purchase = sub_total_move_income = 0
                sub_total_capital = sub_total_revaluation = sub_total_closed = sub_total_move_expense = 0
                sub_total_c2_value = sub_total_initial_dep = sub_total_initmove_dep = sub_total_move_dep = sub_total_depreciation = 0
                sub_total_deduct_dep = sub_total_end_dep = sub_total_residual = sub_total_deduct_move_dep = 0
                
                categ_id = False
                is_parent = True
                is_child_parent = False
                child_categ ={}
                for categ in self.env['account.asset.category'].search([('id', 'child_of', parent_categ.id)]):
                    is_child_parent = False
                    child_parent = []
                    child_parents = self.env['account.asset.category'].search([('id', 'child_of', categ.id)])
                    for child in child_parents:
                        child_parent.append(child)
                    if categ not in parent_category and len(child_parent) > 1:
                        is_child_parent = True
                        child_categ = categ
                    if child_categ:
                        if child_categ.id != categ.parent_id.id and child_categ.id != categ.id:
                            child_categ ={}
                            if  categ not in parent_category and is_parent == False or is_child_parent == True:
                                sheet.merge_range(rowx, 0, rowx, 4, _(u'Ангилалын дүн'), format_group_left)
                                sheet.write(rowx, 5, child_sub_initial, format_group)
                                sheet.write(rowx, 6, child_sub_purchase, format_group)
                                sheet.write(rowx, 7, child_sub_move_income, format_group)
                                sheet.write(rowx, 8, child_sub_capital, format_group)
                                sheet.write(rowx, 9, child_sub_revaluation, format_group)
                                sheet.write(rowx, 10, child_sub_closed, format_group)
                                sheet.write(rowx, 11, child_sub_move_expense, format_group)
                                sheet.write(rowx, 12, child_sub_c2_value, format_group)
                                sheet.write(rowx, 13, child_sub_initial_dep, format_group)
                                sheet.write(rowx, 14, child_sub_initmove_dep, format_group)
                                sheet.write(rowx, 15, child_sub_depreciation, format_group)
                                sheet.write(rowx, 16, child_sub_deduct_dep, format_group)
                                sheet.write(rowx, 17, child_sub_deduct_move_dep, format_group)
                                sheet.write(rowx, 18, child_sub_end_dep, format_group)
                                sheet.write(rowx, 19, child_sub_residual, format_group)
                                sheet.merge_range(rowx, 20, rowx, 21, '', format_group)
                                rowx += 1
                            child_sub_initial = child_sub_purchase = child_sub_move_income = 0
                            child_sub_capital = child_sub_revaluation = child_sub_closed = child_sub_move_expense = 0
                            child_sub_c2_value = child_sub_initial_dep = child_sub_initmove_dep = child_sub_move_dep = child_sub_depreciation = 0
                            child_sub_deduct_dep = child_sub_deduct_move_dep = child_sub_end_dep = child_sub_residual = 0
                    is_parent = True   
                    if  categ in parent_category1:
                        if not categ.parent_id:
                            format = format_group_left
                        else:
                            format = format_content_left_bold
                        sheet.merge_range(rowx, 0, rowx, 21, categ.name, format)
                        rowx += 1

                        for line in lines:
                            asset_code = line['code']
                            asset_name = line['name']
                            purchase_date = line['pdate']
                            method_number = line['method_number']
                            categ_name = line['cname']
                            loc_name = line['lname']
                            hname = line['hname']
                            in_depreciation = line['in_depreciation']
                            history_count = 1
                            history_obj = self.env['account.asset.history'].search([('asset_id', '=', line['asset_id'])])
                            if history_obj:
                                history_count = len(history_obj)
                            depreciation = line['added_dep']/history_count
                            c2_value = 0
                            is_parent == True
                            if categ.id == line['cid']:
                                is_parent = False
                                initial = initial_dep = initial_end_dep = 0
                                purchase = capital = move_income = 0
                                revaluation = closed = expense_dep = move_expense = 0
                                initmove_dep = deduct_move_dep = deduct_dep = move_dep = 0
                                c2_value = end_dep = residual = 0
                                capital = line['capital'] - line['in_capital']
                                revaluation = line['revaluation']
                                if self.from_date > line['pdate']:
                                    initial = line['pvalue']
                                elif self.from_date <= line['pdate'] and self.to_date >= line['pdate']:
                                    purchase = line['pvalue']
                                if line['purchase'] or line['invoice']:
                                    purchase = line['pvalue']
                                initial_dep =  line['init_dep']/history_count
                                initial_end_dep = initial - initial_dep
                                if line['state'] =='close':
                                    history_obj = self.env['account.asset.history'].search([('asset_id', '=', line['asset_id']),('action', 'in', ('sale','close')),('date', '>=',self.from_date),('date', '<=',self.to_date)],order='date ASC')
                                    if history_obj:
                                        for history in history_obj:
                                            deduct_dep = initial_dep + depreciation
                                            if initial == 0:
                                                closed = purchase + revaluation + capital
                                            else:
                                                closed = initial + revaluation + capital
                                if line['count'] >= 0:
                                        move_income = line['pvalue'] + line['in_capital'] + line['in_revaluation'] - initial_dep
                                        initmove_dep = line['in_depreciation']/history_count
                                        move_expense = move_income
                                        deduct_move_dep = initmove_dep
                                c2_value = initial + purchase + move_income + capital + revaluation - closed - move_expense
                                end_dep = initial_dep + initmove_dep + depreciation - deduct_dep - deduct_move_dep
                                residual = c2_value - end_dep
                                
                                sheet.write(rowx, 0, counter, format_content_number)
                                sheet.write(rowx, 1, asset_code, format_content_text)
                                sheet.write(rowx, 2, asset_name, format_content_text)
                                sheet.write(rowx, 3, method_number, format_group_center)
                                sheet.write(rowx, 4, purchase_date, format_group_center)
                                sheet.write(rowx, 5, initial, format_content_right)
                                sheet.write(rowx, 6, purchase, format_content_right)
                                sheet.write(rowx, 7, move_income, format_content_right)
                                sheet.write(rowx, 8, capital, format_content_right)
                                sheet.write(rowx, 9, revaluation, format_content_right)
                                sheet.write(rowx, 10, closed, format_content_right)
                                sheet.write(rowx, 11, move_expense, format_content_right)
                                sheet.write(rowx, 12, c2_value, format_content_right)
                                sheet.write(rowx, 13, initial_dep, format_content_right)
                                sheet.write(rowx, 14, initmove_dep, format_content_right)
                                sheet.write(rowx, 15, depreciation, format_content_right)
                                sheet.write(rowx, 16, deduct_dep, format_content_right)
                                sheet.write(rowx, 17, deduct_move_dep, format_content_right)
                                sheet.write(rowx, 18, end_dep, format_content_right)
                                sheet.write(rowx, 19, residual, format_content_right)
                                sheet.write(rowx, 20, hname, format_content_text)
                                sheet.write(rowx, 21, loc_name, format_content_text)
                
                                sub_initial += initial
                                sub_purchase += purchase
                                sub_move_income += move_income
                                sub_capital += capital
                                sub_revaluation += revaluation
                                sub_closed += closed
                                sub_move_expense += move_expense
                                sub_c2_value += c2_value
                                sub_initial_dep += initial_dep
                                sub_initmove_dep += initmove_dep
                                sub_move_dep += move_dep
                                sub_depreciation += depreciation
                                sub_deduct_dep += deduct_dep
                                sub_deduct_move_dep += deduct_move_dep
                                sub_end_dep += end_dep
                                sub_residual += residual

                                if child_categ :
                                    child_sub_initial += initial
                                    child_sub_purchase += purchase
                                    child_sub_move_income += move_income
                                    child_sub_capital += capital
                                    child_sub_revaluation += revaluation
                                    child_sub_closed += closed
                                    child_sub_move_expense += move_expense
                                    child_sub_c2_value += c2_value
                                    child_sub_initial_dep += initial_dep
                                    child_sub_initmove_dep += initmove_dep
                                    child_sub_move_dep += move_dep
                                    child_sub_depreciation += depreciation
                                    child_sub_deduct_dep += deduct_dep
                                    child_sub_deduct_move_dep += deduct_move_dep
                                    child_sub_end_dep += end_dep
                                    child_sub_residual += residual
                
                                total_initial += initial
                                total_purchase += purchase
                                total_move_income += move_income
                                total_capital += capital
                                total_revaluation += revaluation
                                total_closed += closed
                                total_move_expense += move_expense
                                total_c2_value += c2_value
                                total_initial_dep += initial_dep
                                total_initmove_dep += initmove_dep
                                total_move_dep += move_dep
                                total_depreciation += depreciation
                                total_deduct_dep += deduct_dep
                                sub_deduct_move_dep += deduct_move_dep
                                total_end_dep += end_dep
                                total_residual += residual
                                counter += 1
                                rowx += 1
                                
                            categ_id = line['cid']
                        sub_total_initial += sub_initial
                        sub_total_purchase += sub_purchase
                        sub_total_move_income += sub_move_income
                        sub_total_capital += sub_capital
                        sub_total_revaluation += sub_revaluation
                        sub_total_closed += sub_closed
                        sub_total_move_expense += sub_move_expense
                        sub_total_c2_value += sub_c2_value
                        sub_total_initial_dep += sub_initial_dep
                        sub_total_initmove_dep += sub_initmove_dep
                        sub_total_move_dep += sub_move_dep
                        sub_total_depreciation += sub_depreciation
                        sub_total_deduct_dep += sub_deduct_dep
                        sub_total_deduct_move_dep += sub_deduct_move_dep
                        sub_total_end_dep += sub_end_dep
                        sub_total_residual += sub_residual
                        if  categ not in parent_category and is_parent == False :
                            sheet.merge_range(rowx, 0, rowx, 4, _(u'Ангилалын дүн'), format_content_right_bold)
                            sheet.write(rowx, 5, sub_initial, format_group_bold)
                            sheet.write(rowx, 6, sub_purchase, format_group_bold)
                            sheet.write(rowx, 7, sub_move_income, format_group_bold)
                            sheet.write(rowx, 8, sub_capital, format_group_bold)
                            sheet.write(rowx, 9, sub_revaluation, format_group_bold)
                            sheet.write(rowx, 10, sub_closed, format_group_bold)
                            sheet.write(rowx, 11, sub_move_expense, format_group_bold)
                            sheet.write(rowx, 12, sub_c2_value, format_group_bold)
                            sheet.write(rowx, 13, sub_initial_dep, format_group_bold)
                            sheet.write(rowx, 14, sub_initmove_dep, format_group_bold)
                            sheet.write(rowx, 15, sub_depreciation, format_group_bold)
                            sheet.write(rowx, 16, sub_deduct_dep, format_group_bold)
                            sheet.write(rowx, 17, sub_deduct_move_dep, format_group_bold)
                            sheet.write(rowx, 18, sub_end_dep, format_group_bold)
                            sheet.write(rowx, 19, sub_residual, format_group_bold)
                            sheet.merge_range(rowx, 20, rowx, 21, '', format_group_bold)
                            rowx += 1
                            sub_initial = sub_purchase = sub_move_income = 0
                            sub_capital = sub_revaluation = sub_closed = sub_move_expense = 0
                            sub_c2_value = sub_initial_dep = sub_move_dep = sub_depreciation = 0
                            sub_deduct_dep = sub_end_dep = sub_residual = 0
                
                sheet.merge_range(rowx, 0, rowx, 4, _(u'Ангилалын дүн'), format_group_left)
                sheet.write(rowx, 5, sub_total_initial, format_group)
                sheet.write(rowx, 6, sub_total_purchase, format_group)
                sheet.write(rowx, 7, sub_total_move_income, format_group)
                sheet.write(rowx, 8, sub_total_capital, format_group)
                sheet.write(rowx, 9, sub_total_revaluation, format_group)
                sheet.write(rowx, 10, sub_total_closed, format_group)
                sheet.write(rowx, 11, sub_total_move_expense, format_group)
                sheet.write(rowx, 12, sub_total_c2_value, format_group)
                sheet.write(rowx, 13, sub_total_initial_dep, format_group)
                sheet.write(rowx, 14, sub_total_initmove_dep, format_group)
                sheet.write(rowx, 15, sub_total_depreciation, format_group)
                sheet.write(rowx, 16, sub_total_deduct_dep, format_group)
                sheet.write(rowx, 17, sub_total_deduct_move_dep, format_group)
                sheet.write(rowx, 18, sub_total_end_dep, format_group)
                sheet.write(rowx, 19, sub_total_residual, format_group)
                sheet.merge_range(rowx, 20, rowx, 21, '', format_group)
                rowx += 1
                sub_total_initial = sub_total_purchase = sub_total_move_income = 0
                sub_total_capital = sub_total_revaluation = sub_total_closed = sub_total_move_expense = 0
                sub_total_c2_value = sub_total_initial_dep = sub_total_initmove_dep = sub_total_move_dep = sub_total_depreciation = 0
                sub_total_deduct_dep = sub_total_end_dep = sub_total_residual = sub_total_deduct_move_dep = 0

        sheet.merge_range(rowx, 0, rowx, 4, _(u'Нийт'), format_group_center_bold)
        sheet.write(rowx, 4, '', format_group)
        sheet.write(rowx, 5, total_initial, format_group)
        sheet.write(rowx, 6, total_purchase, format_group)
        sheet.write(rowx, 7, total_move_income, format_group)
        sheet.write(rowx, 8, total_capital, format_group)
        sheet.write(rowx, 9, total_revaluation, format_group)
        sheet.write(rowx, 10, total_closed, format_group)
        sheet.write(rowx, 11, total_move_expense, format_group)
        sheet.write(rowx, 12, total_c2_value, format_group)
        sheet.write(rowx, 13, total_initial_dep, format_group)
        sheet.write(rowx, 14, total_initmove_dep, format_group)
        sheet.write(rowx, 15, total_depreciation, format_group)
        sheet.write(rowx, 16, total_deduct_dep, format_group)
        sheet.write(rowx, 17, total_deduct_move_dep, format_group)
        sheet.write(rowx, 18, total_end_dep, format_group)
        sheet.write(rowx, 19, total_residual, format_group)
        sheet.merge_range(rowx, 20, rowx, 21, '', format_group)
        rowx += 2
        
        sheet.merge_range(rowx, 1, rowx, 4, _(u'Тайлан бэлтгэсэн:') + '........................' + '/' + self.env.user.name 
                          + '/', format_content_text_footer)
        rowx += 1
        sheet.merge_range(rowx, 1, rowx, 4, _(u'Тайлан хянасан: ') + '........................' + '/                          /', format_content_text_footer)
        
        sheet.set_zoom(80)
        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report()
