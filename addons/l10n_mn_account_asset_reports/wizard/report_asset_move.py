# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time, calendar
from datetime import date, datetime, timedelta
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from operator import itemgetter

from odoo import api, fields, models, _

from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class ReportAssetMove(models.TransientModel):
    _name = "report.asset.move"
    _description = "Asset move report"

    def _get_last_day_of_month(self):
        range = calendar.monthrange(int(time.strftime('%Y')), int(time.strftime('%m')))
        return time.strftime('%Y-%m') + '-' + str(range[1])
    
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id, required=True)
    from_date = fields.Date('Start Date', required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    to_date = fields.Date('End Date', required=True, default=_get_last_day_of_month)
    asset_categ_ids = fields.Many2many('account.asset.category', 'wizard_asset_move_report_asset_categ_rel', 'wizard_id', 'categ_id', 'Asset Category')
    location_ids = fields.Many2many('account.asset.location', 'wizard_asset_report_move_asset_location_rel', 'wizard_id', 'location_id', 'Location')
    # 'account_asset_ids':fields.many2many('account.account', 'wizard_asset_report_account_rel', 'wizard_id', 'account_id', 'Asset account', domain=[('type','not in',['view','consolidation'])]),
    asset_asset_ids = fields.Many2many('account.asset.asset', 'wizard_asset_report_move_asset_rel', 'wizard_id', 'asset_id', 'Asset account', domain=[('state', 'not in', ['draft']), ('is_supply_asset', '=', False)])
    owner_ids = fields.Many2many('hr.employee', 'wizard_asset_report_move_employee_rel', 'wizard_id','emp_id', 'Owners')
    group_by_categ = fields.Boolean('Group by Category')
    
    @api.multi
    def export(self):
        report_obj = self
        user = self.env['res.users'].search([('id', '=', self._uid)])
        company = user.company_id
        from_date = report_obj.from_date
        to_date = report_obj.to_date
        asset_categ_ids = report_obj.asset_categ_ids
        account_asset_ids = report_obj.asset_asset_ids
        location_ids = report_obj.location_ids
        owner_ids = report_obj.owner_ids
        group_by_categ = report_obj.group_by_categ
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Asset Move Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        # Custom for standart
        format_name = {
            'font_name': 'Times New Roman',
            'font_size': 14,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter'
        }
        # create formats
        format_content_text_footer = {
        'font_name': 'Times New Roman',
        'font_size': 10,
        'align': 'vcenter',
        'valign': 'vcenter',
        }
        format_name = book.add_format(format_name)
        format_content_text_footer = book.add_format(format_content_text_footer)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_group_left = book.add_format(ReportExcelCellStyles.format_group_left)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('asset_detail_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 0

        # compute column
        colx_number = 3
        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 15)
        sheet.set_column('C:C', 20)
        sheet.set_column('D:D', 15)
        sheet.set_column('E:E', 15)
        sheet.set_column('F:F', 25)
        sheet.set_column('G:G', 25)
        sheet.set_column('H:H', 25)
        sheet.set_column('I:I', 25)
        sheet.set_column('J:J', 25)
        sheet.set_column('K:K', 25)
        sheet.set_column('L:L', 25)
        sheet.set_column('M:M', 25)
        sheet.set_column('N:N', 25)
        sheet.set_column('O:O', 15)
        sheet.set_column('P:P', 15)
        sheet.set_column('Q:Q', 15)
        sheet.set_column('R:R', 15)
        sheet.set_column('S:S', 15)
        sheet.set_column('T:T', 10)
        sheet.set_column('U:U', 15)
        sheet.set_column('V:V', 10)

        # create name
        sheet.write(rowx, 0, '%s' % (company.name), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 1, rowx, 9, report_name.upper(), format_name)
        # report duration
        sheet.write(rowx + 1, 0, '%s: %s - %s' % (_('Duration'), self.from_date, self.to_date), format_filter)
        # create date
        sheet.write(rowx + 2, 0, '%s: %s' % (_('Created On'), time.strftime('%Y-%m-%d')), format_filter)
        rowx += 3

        sheet.merge_range(rowx, 0, rowx + 1, 0, _('Seq'), format_title)  # seq
        sheet.merge_range(rowx, 1, rowx + 1, 1, _('Asset Number'), format_title)  # Хөрөнгийн дугаар
        sheet.merge_range(rowx, 2, rowx + 1, 2, _('Name'), format_title)  # Үндсэн хөрөнгийн нэр
        sheet.merge_range(rowx, 3, rowx + 1, 3, _('Move Date'), format_title)  # Шилксэн огноо
        sheet.merge_range(rowx, 4, rowx + 1, 4, _('Receipt Date'), format_title)  # Худалдан авсан огноо
        sheet.merge_range(rowx, 5, rowx, 8, _('From'), format_title)  # Хаанаас
        sheet.merge_range(rowx, 9, rowx, 12, _('To'), format_title)  # Хаашаа
        sheet.write(rowx + 1, 5, _('Old Location'), format_title)  # Хуучин байрлал
        sheet.write(rowx + 1, 6, _('Old Owner'), format_title)  # Хуучин эзэмшигч
        sheet.write(rowx + 1, 7, _('Old Category'), format_title)  # Хуучин ангилал
        sheet.write(rowx + 1, 8, _('Old Analytic Account'), format_title)  # Хуучин шинжилгээний данс
        sheet.write(rowx + 1, 9, _('New Location'), format_title)  # Шинэ байрлал
        sheet.write(rowx + 1, 10, _('New Owner'), format_title)  # Шинэ эзэмшигч
        sheet.write(rowx + 1, 11, _('New Category'), format_title)  # Хуучин ангилал
        sheet.write(rowx + 1, 12, _('New Analytic Account'), format_title)  # Хуучин шинжилгээний данс
        sheet.merge_range(rowx, 13, rowx + 1, 13, _('Description'), format_title)  # Тодорхойлолт
        rowx += 2

        asset = []
        where = [('move_id.move_date', '>=', from_date), ('move_id.move_date', '<=', to_date), ('move_id.company_id', '<=', company.id), ('asset_id.is_supply_asset', '=', False)]
        if asset_categ_ids:
            where.append(('asset_id.category_id', 'in', asset_categ_ids.ids))
        if location_ids:
            where.append(('asset_id.location_id', 'in', location_ids.ids))
        if account_asset_ids:
            where.append(('asset_id', 'in', account_asset_ids.ids))
        if owner_ids:
            where.append(('asset_id.user_id', 'in', owner_ids.ids))
        move_lines = self.env['account.asset.moves.line'].search(where)
        sequence = 1
        categ_dict = {}
        for move in move_lines:
            if move.asset_id.category_id.id not in categ_dict:
                categ_dict[move.asset_id.category_id.id] = {'lines': [move], 'categ': move.asset_id.category_id.name}
            else:
                categ_dict[move.asset_id.category_id.id]['lines'].append(move)
        for cate in categ_dict:
            sheet.merge_range(rowx, 0, rowx, 13, categ_dict[cate]['categ'], format_group_left)  # Хаашаа
            rowx += 1
            for line in categ_dict[cate]['lines']:
                sheet.write(rowx, 0, sequence, format_content_number)
                sheet.write(rowx, 1, line.asset_id.code, format_content_text)
                sheet.write(rowx, 2, line.asset_id.name, format_content_text)
                sheet.write(rowx, 3, line.move_id.move_date, format_content_text)
                sheet.write(rowx, 4, line.move_id.receipt_date, format_content_text)
                sheet.write(rowx, 5, line.old_location_id.name, format_content_text)
                sheet.write(rowx, 6, '%s %s' % (line.old_owner_id.name.upper() if line.old_owner_id.name else '',
                                                line.old_owner_id.last_name or ''), format_content_text)
                sheet.write(rowx, 7, line.old_category_id.name, format_content_text)
                sheet.write(rowx, 8, line.old_account_analytic_id.name or ' ', format_content_text)
                sheet.write(rowx, 9, line.new_location_id.name, format_content_text)
                sheet.write(rowx, 10, '%s %s' % (line.new_owner_id.name.upper() if line.new_owner_id.name else '',
                                                 line.new_owner_id.last_name or ''), format_content_text)
                sheet.write(rowx, 11, line.new_category_id.name, format_content_text)
                sheet.write(rowx, 12, line.new_account_analytic_id.name or ' ', format_content_text)
                sheet.write(rowx, 13, line.move_id.description or ' ', format_content_text)
                rowx += 1
                sequence += 1

        rowx += 2
        sheet.merge_range(rowx, 1, rowx, 4, _('Owner:') + '........................' + '/' + self.env.user.name
                          + '/', format_content_text_footer)
        rowx += 1
        sheet.merge_range(rowx, 1, rowx, 4, _('Checker: ') + '........................' + '/                          /', format_content_text_footer)
        
        sheet.set_zoom(80)
        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report()