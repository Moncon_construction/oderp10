# -*- coding: utf-8 -*-
{
    'name': "Mongolian Account Asset Tax Depreciation",
    'version': '1.0',
    'depends': ['l10n_mn_account_asset'],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Account Asset Modules',
    'description': """
         Asset's module.
    """,
    'data': [
        'views/account_asset_view.xml',
        'views/res_company_view.xml',
    ]
}

