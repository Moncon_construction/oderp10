# -*- coding: utf-8 -*-
from odoo import fields, models, _, api  # @UnresolvedImport
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from odoo.exceptions import ValidationError, UserError
from odoo.tools import float_compare, float_is_zero
from calendar import monthrange


class AccountAssetAsset(models.Model):
    _inherit = 'account.asset.asset'

    @api.one
    @api.depends('value', 'tax_salvage_value', 'depreciation_line_ids.move_check', 'depreciation_line_ids.amount', 'depreciation_line_ids.amount_tax')
    def _amount_residual_tax(self):
        total_amount = total_amount_tax = 0.0
        for line in self.depreciation_line_ids:
            if line.move_check:
                total_amount += line.amount
                total_amount_tax += line.amount_tax
        self.value_residual_tax = self.value - total_amount_tax - self.tax_salvage_value
        self.value_depreciated_tax = total_amount_tax
        self.depreciation_difference = total_amount - total_amount_tax

    method_number_tax = fields.Integer(string='Number of Depreciations(Tax)', readonly=True, states={'draft': [('readonly', False)]}, default=5, help="The number of depreciations needed to depreciate your asset(Tax)")
    initial_depreciation_tax = fields.Float(string='Initial Depreciated Value (Tax)')
    value_residual_tax = fields.Float(compute='_amount_residual_tax', method=True, digits=0, string='Residual Value(Tax)')
    value_depreciated_tax = fields.Float(compute='_amount_residual_tax', method=True, digits=0, string='Depreciated Value(Tax)', store=True)
    depreciation_difference = fields.Float(compute='_amount_residual_tax', string='Depreciation Difference', help="Depreciation Difference = Depreciated Value - Depreciated Value(Tax)")
    tax_calculated = fields.Boolean(string='Tax Calculated', default=False)
    tax_salvage_value = fields.Float('Tax salvage value')

    def onchange_category_id_values(self, category_id):
        if category_id:
            vals = super(AccountAssetAsset, self).onchange_category_id_values(category_id)
            category = self.env['account.asset.category'].browse(category_id)
            vals['value'].update({'method_number_tax': category.method_number_tax})
            return vals

    def _compute_board_undone_dotation_nb_tax(self, depreciation_date, total_days):
        undone_dotation_number = self.method_number_tax
        if self.method_number_tax == 'end':
            end_date = datetime.strptime(self.method_number_tax, DF).date()
            undone_dotation_number = 0
            while depreciation_date <= end_date:
                depreciation_date = date(depreciation_date.year, depreciation_date.month, depreciation_date.day) + relativedelta(months=+self.method_period)
                undone_dotation_number += 1
        if self.prorata:
            undone_dotation_number += 1
        return undone_dotation_number

    @api.multi
    def compute_depreciation_board(self):
        # Элэгдлийн самбар үүсгэдэг функцийг дахин тодорхойлсон
        self.ensure_one()
        move = False
        unit_amount = 0
        posted_depreciation_line_ids = self.depreciation_line_ids.filtered(lambda x: x.move_check or x.is_freeze).sorted(key=lambda l: l.depreciation_date)
        unposted_depreciation_line_ids = self.depreciation_line_ids.filtered(lambda x: not x.move_check and not x.is_freeze)
        freezed_depreciation_line_ids = self.depreciation_line_ids.filtered(lambda x: x.is_freeze)
        freeze = len(freezed_depreciation_line_ids)
        if unposted_depreciation_line_ids:
            self._cr.execute("DELETE FROM account_asset_depreciation_line where id in (" + ",".join(map(str, unposted_depreciation_line_ids.ids)) + ") ")
        commands = []
        if self.value_residual != 0.0:
            amount_to_depr = residual_amount = self.value_residual
            residual_amount_tax = self.value_residual_tax
            if self.prorata:
                # if we already have some previous validated entries, starting date is last entry + method perio
                if posted_depreciation_line_ids and posted_depreciation_line_ids[-1].depreciation_date:
                    last_depreciation_date = datetime.strptime(posted_depreciation_line_ids[-1].depreciation_date, DF).date()
                    depreciation_date = self.last_day_of_month(last_depreciation_date)
                    if depreciation_date == last_depreciation_date:
                        depreciation_date = last_depreciation_date + relativedelta(months=+self.method_period)
                else:
                    depreciation_date = datetime.strptime(self._get_last_depreciation_date()[self.id], DF).date()
            else:
                # depreciation_date = 1st of January of purchase year if annual valuation, 1st of
                # purchase month in other cases
                if self.method_period >= 12:
                    asset_date = datetime.strptime(self.date[:4] + '-01-01', DF).date()
                else:
                    asset_date = datetime.strptime(self.date[:7] + '-01', DF).date()
                # if we already have some previous validated entries, starting date isn't 1st January but last entry + method period
                if posted_depreciation_line_ids and posted_depreciation_line_ids[-1].depreciation_date:
                    last_depreciation_date = datetime.strptime(posted_depreciation_line_ids[-1].depreciation_date, DF).date()
                    depreciation_date = self.last_day_of_month(last_depreciation_date)
                    if depreciation_date == last_depreciation_date:
                        depreciation_date = last_depreciation_date + relativedelta(months=+self.method_period)
                else:
                    depreciation_date = asset_date
            day = depreciation_date.day
            month = depreciation_date.month
            year = depreciation_date.year
            total_days = (year % 4) and 365 or 366
            # Элэгдүүлэх тоог олох
            number = self._compute_board_undone_dotation_nb(depreciation_date, total_days)
            number += freeze
            # Татварын хувьд элэгдүүлэх тоог олох
            tax_number = self._compute_board_undone_dotation_nb_tax(depreciation_date, total_days)
            # Татварын элэгдэх дүнг тооцоолох
            if tax_number > 0:
                amount_tax = (self.value - self.tax_salvage_value - self.initial_depreciation_tax or 0) / tax_number
                amount_tax = self.currency_id.round(amount_tax)
            else:
                amount_tax = 0
            # Эхлэлийн элэгдлийн дугаарлалтаас эхлэхгүй, аль хэдийнэ compute_initial_depreciation функцээр үүсгэгдсэн нөгөөтэйгүүр батлагдсан бичилтүүдийг тооны дараагийн тооноос эхлэх
            start = len(posted_depreciation_line_ids) + 1
            initial = 0
            # Эхний элэгдлийг шалган үүсгэх
            if len(posted_depreciation_line_ids) == 0 and self.initial_depreciation > 0:
                start = 0
                init_vals = self.compute_initial_depreciation()
                commands.append((0, False, init_vals))
            elif len(posted_depreciation_line_ids) > 0 and self.initial_depreciation > 0:
                start -= 1
                initial = 1
            else:
                # Эхлэлийн элэгдэл нь тухайн өдрөөс элэгдүүлэхгүй тохиолдолд сарын сүүлийн өдрөөс байх
                if not self.prorata:
                    depreciation_date = self.last_day_of_month(depreciation_date)
            start_tax = start
            # Хэрвээ тухайн хөрөнгө шинэ хөрөнгө бол элэгдлийн самбарын үүсгэхдээ огноо шалгах
            if len(posted_depreciation_line_ids) == 0 and self.initial_depreciation == 0:
                if depreciation_date.year < 2020:
                    tax_month = depreciation_date.month
                    if 1 <= tax_month <= 3:
                        start_tax = start + 4 - tax_month
                    elif 4 <= tax_month <= 6:
                        start_tax = start + 7 - tax_month
                    elif 7 <= tax_month <= 9:
                        start_tax = start + 10 - tax_month
                    else:
                        start_tax = start + 13 - tax_month
                else:
                    start_tax = start + 1
            tax_number += start_tax - start
            if number == tax_number or number > tax_number:
                undone_dotation_number = number
            elif number < tax_number:
                undone_dotation_number = tax_number
            number_lines = number
            # Батлагдсан мөрүүд дээр татварын элэгдүүлэлт тооцоолох
            # Бичилт үүсгэх эсэхийг дараа шийдэх
            if len(posted_depreciation_line_ids) > 0:
                res_amount_tax = (self.value - self.salvage_value)
                seq = 1
                if not self.tax_calculated:
                    for posted_line in self.env['account.asset.depreciation.line'].browse(posted_depreciation_line_ids.ids):
                        if posted_line.sequence == 0 and posted_line.initial_depreciation_check:
                            undone_dotation_number += 1
                            number += 1
                            tax_number += 1
                            posted_line.amount_tax = self.initial_depreciation_tax or 0
                            if posted_line.amount != self.initial_depreciation_tax:
                                posted_line.depreciation_difference = posted_line.amount - self.initial_depreciation_tax
                            res_amount_tax -= self.initial_depreciation_tax or 0
                        else:
                            if seq == tax_number:
                                amount_tax = res_amount_tax
                            elif seq > tax_number:
                                amount_tax = 0
                            posted_line.amount_tax = amount_tax
                            res_amount_tax -= amount_tax
                            posted_line.depreciated_value_tax = (self.value - self.salvage_value) - (res_amount_tax + amount_tax)
                            if posted_line.amount != amount_tax:
                                posted_line.depreciation_difference = posted_line.amount - amount_tax
                        posted_line.remaining_value_tax = res_amount_tax
                        residual_amount_tax = res_amount_tax
                        seq += 1
            # Элэгдлийн самбар үүсгэх хэсэг
            for sequence in range(start, undone_dotation_number + 1):
                lastday_of_month = int(monthrange(depreciation_date.year, depreciation_date.month)[1])
                if sequence != 0:
                    if number > sequence:
                        # Хөрөнгө хааж байгаа үед хаалтын огнооноос хамаарч элэгдлийн бичилтийн дүнг шинэчлэх
                        if 'date' in self._context and 'value' in self._context and not move:
                            dates = datetime.strptime(self._context.get('date'), '%Y-%m-%d')
                            d_lines = self.depreciation_line_ids.filtered(
                                lambda l: not l.move_check and l.depreciation_date < str(dates.date()))
                            if d_lines:
                                raise UserError(_('Validate depreciation lines before %s closing date!' % dates.date()))
                            amount = self._context.get('value')
                            if type(depreciation_date) is not type(dates):
                                dates = dates.date()
                            if depreciation_date >= dates:
                                depreciation_date = dates
                                amount_to_depr = residual_amount - amount
                                move = True
                                amount = self._compute_board_amount(sequence, residual_amount, amount_to_depr,number_lines,
                                                                    posted_depreciation_line_ids.ids, total_days, depreciation_date, number)
                                lastday_of_month = int(monthrange(depreciation_date.year, depreciation_date.month)[1])
                                amount = amount / lastday_of_month * depreciation_date.day
                        # Элэгдлийн бичилтийн огноо болон хөрөнгийн огнооны өдрөөс хамааруулж өдрөөр элэгдүүлийн дүнг тооцоолох
                        elif sequence == 1 and not self.prorata and depreciation_date > datetime.strptime(self.date, DF).date() and datetime.strptime(self.date, "%Y-%m-%d").day != 1:
                            days = (depreciation_date - datetime.strptime(self.date, "%Y-%m-%d").date()).days
                            division = undone_dotation_number + 1 - start
                            monthly_depreciate = residual_amount / division
                            daily_depreciate = monthly_depreciate / depreciation_date.day
                            # Үйл ажиллагааны аргын үед элэгдлийн дүнг гараар тооцоолох тул элэгдлийн самбар дээр 0 гэж оруулав
                            if self.method == 'operation':
                                if self.working_capacity > 0:
                                    unit_amount = (self.value - self.salvage_value) / self.working_capacity
                                amount = 0
                            else:
                                amount = daily_depreciate * days
                            amount_to_depr = residual_amount - amount
                            number_lines -= 1
                        else:
                            # Сарын сүүл өдөр бол дараа сараас элэгдүүлдэг болгов
                            if sequence == 1 and not self.prorata and depreciation_date == datetime.strptime(self.date, DF).date():
                                depreciation_date = date(year, month, day) + relativedelta(months=+self.method_period)
                                depreciation_date = self.last_day_of_month(depreciation_date)
                                day = depreciation_date.day
                                month = depreciation_date.month
                                year = depreciation_date.year
                            # Үйл ажиллагааны аргын үед элэгдлийн дүнг гараар тооцоолох тул элэгдлийн самбар дээр 0 гэж оруулав
                            if self.method == 'operation':
                                if self.working_capacity > 0 and self.working_capacity > self.working_performance:
                                    unit_amount = (self.value - self.salvage_value - self.value_depreciated) / (self.working_capacity - self.working_performance)
                                amount = 0
                            else:
                                amount = self._compute_board_amount(sequence, residual_amount, amount_to_depr,number_lines + initial,
                                                                    posted_depreciation_line_ids.ids, total_days, depreciation_date, number)
                        amount = self.currency_id.round(amount)
                        if self.method != 'operation' and float_is_zero(amount, precision_rounding=self.currency_id.rounding):
                            continue
                        residual_amount -= amount
                    elif number == sequence:
                        amount = residual_amount
                        residual_amount -= amount
                    else:
                        amount = 0
                    if tax_number > sequence:
                        if start_tax <= sequence:
                            amount_tax1 = amount_tax
                            residual_amount_tax -= amount_tax
                    elif tax_number == sequence:
                        amount_tax = residual_amount_tax
                        residual_amount_tax -= amount_tax
                    else:
                        amount_tax = 0
                    dep_value = (self.value - self.salvage_value) - (residual_amount + amount)
                    dep_value_tax = (self.value - self.salvage_value) - (residual_amount_tax + (amount_tax if start_tax <= sequence else 0))
                    vals = {
                        'amount': amount,
                        'unit_amount': unit_amount,
                        'amount_tax': amount_tax if start_tax <= sequence else 0,
                        'asset_id': self.id,
                        'sequence': sequence,
                        'name': (self.code or '') + '/' + str(sequence),
                        'tax_calculated': True,
                        'remaining_value': residual_amount,
                        'remaining_value_tax': residual_amount_tax,
                        'depreciated_value': dep_value,
                        'depreciated_value_tax': dep_value_tax,
                        'depreciation_date': depreciation_date.strftime(DF),
                        'depreciation_difference': amount - (amount_tax if start_tax <= sequence else 0)
                    }
                    commands.append((0, False, vals))
                    # Considering Depr. Period as months
                    depreciation_date = date(year, month, day) + relativedelta(months=+self.method_period)
                    depreciation_date = self.last_day_of_month(depreciation_date)
                else:
                    # Эхний элэгдлийн дүн
                    amount = self.initial_depreciation
                    amount_to_depr = residual_amount - amount
                    residual_amount -= amount
                    residual_amount_tax -= self.initial_depreciation_tax
                    entry_date = datetime.strptime(self._context.get('entry_date', self.date), DF).date()
                    last_date = self.last_day_of_month(entry_date)
                    if last_date > entry_date:
                        depreciation_date = last_date
                    else:
                        depreciation_date = date(year, month, day) + relativedelta(months=+self.method_period)
                        depreciation_date = self.last_day_of_month(depreciation_date)
                day = depreciation_date.day
                month = depreciation_date.month
                year = depreciation_date.year
        self.write({'depreciation_line_ids': commands, 'tax_calculated': True})
        if move:
            line = self.depreciation_line_ids.search([('depreciation_date', '>=', self._context.get('date'))],order='depreciation_date ASC', limit=1)
            line.create_move()
        return True

    @api.multi
    def compute_initial_depreciation(self, context=None):
        res = super(AccountAssetAsset, self).compute_initial_depreciation()
        if context is None:
            context = self._context or {}
        # Хэрэв элэгдлийн эхний дүн байвал тухайн дүнгээр эдэгдэл үүснэ
        currency_obj = self.env.get('res.currency')
        for asset in self:
            if self.initial_depreciation_tax or self.initial_depreciation:
                current_currency = asset.currency_id.id
                depreciation_amount = currency_obj.compute_currency(current_currency, asset.company_id.currency_id.id, asset.initial_depreciation)
                depreciation_amount_tax = currency_obj.compute_currency(current_currency, asset.company_id.currency_id.id, asset.initial_depreciation_tax)
                asset_amount = currency_obj.compute_currency(current_currency, asset.company_id.currency_id.id, asset.value)
                res.update({'tax_calculated': True,
                            'amount_tax': depreciation_amount_tax,
                            'remaining_value_tax': asset_amount - depreciation_amount_tax,
                            'depreciated_value_tax': 0,
                            'depreciation_difference': abs(depreciation_amount - depreciation_amount_tax)})
                return res

class AccountAssetDepreciationLine(models.Model):
    _inherit = 'account.asset.depreciation.line'

    depreciated_value_tax = fields.Float(string='Cumulative Depreciation(Tax)', required=True)
    amount_tax = fields.Float(string='Current Depreciation(Tax)', digits=0, required=True)
    remaining_value_tax = fields.Float(string='Next Period Depreciation(Tax)', digits=0, required=True)
    depreciation_difference = fields.Float(string='Depreciation difference')
    tax_move_id = fields.Many2one('account.move', string='Tax diff Depreciation Entry')

    @api.multi
    def calculate(self):
        # Үйл ажиллагааны аргаар элэгдэл тооцоход элэгдлийн дүнг тооцох функц
        res = super(AccountAssetDepreciationLine, self).calculate()
        for line in self:
            amount = line.unit_performance * line.unit_amount
            line.depreciation_difference = amount - line.amount_tax or 0
            return True

    @api.multi
    def cancel_move(self):
        # Тухайн элэгдлийн мөрийг цуцлан, журналын бичилтийг устгах
        res = super(AccountAssetDepreciationLine, self).cancel_move()
        for line in self:
            if line.tax_move_id:
                if line.tax_move_id.state != 'draft':
                    line.tax_move_id.write({'state': 'draft'})
                if line.tax_move_id.line_ids:
                    for move_line in line.tax_move_id.line_ids:
                        move_line.asset_id = False
                line.tax_move_id.unlink()
        return True

    @api.multi
    def create_move(self, post_move=True):
        company_id = self.env.user.company_id
        #TODO: Зөрүүгийн тайлангийн зарим хэсэг дутуу байгаа тул журналын бичилт хийх боломжгүй болсон. Хөгжүүлэлтийг үргэлжлүүлж хийнэ.
        if company_id.tax_difference_move_create == True:
            if not company_id.expense_tax_account:
                raise UserError(_('Tax expense account is not configured on company'))
            if not company_id.payable_tax_account:
                raise UserError(_('Tax payable account is not configured on company'))
            if not company_id.deferred_income_tax_liability:
                raise UserError(_('Deferred income tax liability account is not configured on company'))
            if not company_id.deferred_income_tax_asset:
                raise UserError(_('Deferred income tax asset account is not configured on company'))
            deferred_income_tax_liability = company_id.deferred_income_tax_liability
            deferred_income_tax_asset = company_id.deferred_income_tax_asset
            expense_tax_account = company_id.expense_tax_account
            payable_tax_account = company_id.payable_tax_account
            prec = self.env['decimal.precision'].precision_get('Account')
            for dep_line in self:
                if dep_line.depreciation_difference != 0:
                    depreciation_date = dep_line.depreciation_date
                    category_id = dep_line.asset_id.category_id
                    company_currency = dep_line.asset_id.company_id.currency_id
                    current_currency = dep_line.asset_id.currency_id
                    amount = current_currency.with_context(date=depreciation_date).compute(dep_line.amount, company_currency)
                    amount_tax = current_currency.with_context(date=depreciation_date).compute(dep_line.amount_tax, company_currency)
                    asset_name = dep_line.asset_id.name + ' (%s/%s)' % (dep_line.sequence, len(dep_line.asset_id.depreciation_line_ids))
                    #partner = dep_line.asset_id.partner_id2 and dep_line.asset_id.partner_id2.id or False
                    if amount_tax > amount:
                        # Хэрэв Татварын хугацаагаар тооцсон элэгдэл Санхүүгийн тайлагналын элэгдлийн зардлаас их бол:
                        # Хөрөнгө дээр сонгосон шинжилгээний дансанд хасах дүнтэйгээр бичнэ.
                        # Кт Татварын өглөг = Дт Татварын зардал + Дт Хойшлогдсон татварын хөрөнгө
                        move = self.env['account.move'].create({'journal_id': category_id.journal_id.id,
                                                                 'name': asset_name,
                                                                 'ref': self.name or '',
                                                                 'date': depreciation_date,
                                                                 'line_ids': [(0, 0, {
                                                                     'name': _('Depreciation difference') + asset_name,
                                                                     'ref': dep_line.name,
                                                                     'debit': amount_tax - amount,
                                                                     'credit': 0,
                                                                     'account_id': deferred_income_tax_asset.id,
                                                                     #'partner_id': partner,
                                                                     'currency_id': company_currency != current_currency and current_currency.id or False,
                                                                     'amount_currency': company_currency != current_currency and - 1.0 * (dep_line.amount_tax - dep_line.amount) or 0.0,
                                                                     'date': depreciation_date,
                                                                     'journal_id': category_id.journal_id.id,
                                                                     'analytic_account_id': dep_line.asset_id.account_analytic_id and dep_line.asset_id.account_analytic_id.id,
                                                                     'asset_id': dep_line.asset_id.id
                                                                 }), (0, 0, {
                                                                     'name': _('Depreciation difference') + asset_name,
                                                                     'ref': dep_line.name,
                                                                     'debit': 0,
                                                                     'credit': amount_tax - amount,
                                                                     'account_id': payable_tax_account.id,
                                                                     #'partner_id': partner,
                                                                     'currency_id': company_currency != current_currency and current_currency.id or False,
                                                                     'amount_currency': company_currency != current_currency and - 1.0 * (dep_line.amount_tax - dep_line.amount) or 0.0,
                                                                     'date': depreciation_date,
                                                                     'journal_id': category_id.journal_id.id,
                                                                     'asset_id': dep_line.asset_id.id
                                                                 })],
                                                                 })
                        dep_line.write({'tax_move_id': move.id})
                    elif amount_tax < amount:
                        # Хэрэв Татварын хугацаагаар тооцсон элэгдэл Санхүүгийн тайлагналын элэгдлийн зардлаас бага бол:
                        # Хөрөнгө дээр сонгосон шинжилгээний дансанд хасах дүнтэйгээр бичнэ.
                        # Дт Татварын өглөг = Дт Татварын зардал + Кт Хойшлогдсон татварын өглөг
                        move = self.env['account.move'].create({'journal_id': category_id.journal_id.id,
                                                                 'name': asset_name,
                                                                 'ref': self.name or '',
                                                                 'date': depreciation_date,
                                                                 'line_ids': [(0, 0, {
                                                                     'name': _('Depreciation difference') + asset_name,
                                                                     'ref': dep_line.name,
                                                                     'debit': 0,
                                                                     'credit': amount - amount_tax,
                                                                     'account_id': deferred_income_tax_liability.id,
                                                                     #'partner_id': partner,
                                                                     'currency_id': company_currency != current_currency and current_currency.id or False,
                                                                     'amount_currency': company_currency != current_currency and - 1.0 * (dep_line.amount - dep_line.amount_tax) or 0.0,
                                                                     'date': depreciation_date,
                                                                     'journal_id': category_id.journal_id.id,
                                                                     'analytic_account_id': dep_line.asset_id.account_analytic_id and dep_line.asset_id.account_analytic_id.id,
                                                                     'asset_id': dep_line.asset_id.id
                                                                 }), (0, 0, {
                                                                     'name': _('Depreciation difference') + asset_name,
                                                                     'ref': dep_line.name,
                                                                     'debit': amount - amount_tax,
                                                                     'credit': 0,
                                                                     'account_id': payable_tax_account.id,
                                                                     #'partner_id': partner,
                                                                     'currency_id': company_currency != current_currency and current_currency.id or False,
                                                                     'amount_currency': company_currency != current_currency and - 1.0 * (dep_line.amount - dep_line.amount_tax) or 0.0,
                                                                     'date': depreciation_date,
                                                                     'journal_id': category_id.journal_id.id,
                                                                     'asset_id': dep_line.asset_id.id
                                                                 })],
                                                                 })
                        dep_line.write({'tax_move_id': move.id})
        return super(AccountAssetDepreciationLine, self).create_move(post_move)

class AccountAssetCategory(models.Model):
    _inherit = 'account.asset.category'

    # method_time = fields.Selection(selection_add=[('tax_number', 'Number by Asset tax depreciation time')])
    method_number_tax = fields.Integer(string='Number of Depreciations(Tax)', default=5, help="The number of depreciations needed to depreciate your asset(Tax)")
