# -*- coding: utf-8 -*-
from odoo import fields, models, _, api # @UnresolvedImport


class ResCompany(models.Model):
    _inherit = "res.company"

    tax_difference_move_create = fields.Boolean(string='Tax Difference Account Move Create?', default=False)
    expense_tax_account = fields.Many2one('account.account', string='Tax expense account')
    payable_tax_account = fields.Many2one('account.account', string='Tax payable account')
    deferred_income_tax_liability = fields.Many2one('account.account', string='Deferred income tax liability')
    deferred_income_tax_asset = fields.Many2one('account.account', string='Deferred income tax asset')


