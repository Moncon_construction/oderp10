# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2017 Asterisk Technologies LLC (<http://www.erp.mn/,
# http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
{
    'name': 'XLSX Format Bank Statements Import',
    'version': '10.0.1.0.0',
    'license': 'AGPL-3',
    'author': 'Asterisk Technologies LLC',
    'website': 'http://www.asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'depends': [
        'account_bank_statement_import',
    ],
    'data': [
        'views/account_bank_statement_import_view.xml',
    ],
}
