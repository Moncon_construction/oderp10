# -*- coding: utf-8 -*-
# © 2013-2016 Therp BV <http://therp.nl>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
import logging
import base64
import xlrd
from tempfile import NamedTemporaryFile
from datetime import datetime
from odoo import api, models, _  # @UnresolvedImport
from odoo.exceptions import UserError, ValidationError  # @UnresolvedImport

_logger = logging.getLogger(__name__)


class AccountBankStatementImport(models.TransientModel):
    _inherit = 'account.bank.statement.import'
    """
        Монгол банкуудын хуулгыг өгөгдсөн Excel
        файлаас импортлон оруулах
    """
    @api.model
    def _parse_file(self, data_file):
        """Parse a xslx file."""
        _logger.debug("Try parsing with xslx.")
        try:
            fileobj = NamedTemporaryFile('w+')
            fileobj.write(base64.decodestring(self.data_file))
            fileobj.seek(0)
            book = xlrd.open_workbook(fileobj.name)
        except ValueError:
            raise UserError(_('Error loading data file. \ Please try again!'))
        sheet = book.sheet_by_index(0)
        nrows = sheet.nrows
        rowi = 1
        statement = []
        currency = None
        account_number = None
        amount = 0.0
        name = '/'
        date = partner = ''
        transactions = []
        while rowi < nrows:
            row = sheet.row(rowi)
            try:
                date = datetime.strptime(row[0].value, '%Y-%m-%d')
            except ValueError:
                raise ValidationError(_('Date error %s row! \n \
                format must \'YYYY-mm-dd\'' % rowi))
            name = row[1].value
            partner = row[2].value
            if row[3].value and row[4].value == '':
                amount = row[3].value
            elif row[4].value and row[3].value == '':
                amount = row[4].value * -1
            elif row[4].value == '' and row[3].value == '':
                raise ValidationError(_('Data error %s row! \n \
                Only one of Income and Expense columns \
                must have a value' % rowi))
            elif row[4].value and row[3].value:
                raise ValidationError(_('Data error %s row! \n \
                Only one of Income and Expense columns \
                must have a value' % rowi))
            transaction = {'name': name,
                           'date': date,
                           'partner_name': partner,
                           'amount': amount,
                           'account_id': False,
                           'cashflow_id': False,
                           }
            transactions.append(transaction)
            rowi += 1
        statements = {
                    'transactions': transactions
                      }
        statement.append(statements)
        return currency, account_number, statement
