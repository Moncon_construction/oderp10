# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Bank Statement Report",
    'version': '1.0',
    'depends': ['l10n_mn_account'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Account Modules',
    'description': """
         Касс, харилцахын тайлангууд
         
         - Бэлэн мөнгөний гүйлгээний тайлан болгох.
         - Банкны харилцах дансны тайлан.
    """,
    'data': [
        'views/account_report_menu.xml',
        'views/report_account_cash_view.xml',
        'views/account_bank_statement_report_view.xml'
    ]
}
