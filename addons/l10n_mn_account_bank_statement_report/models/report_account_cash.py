# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
import xlsxwriter
from odoo.exceptions import UserError
from xlsxwriter.utility import xl_rowcol_to_cell
from operator import itemgetter

from odoo import api, fields, models, _
from openerp.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class ReportAccountCash(models.TransientModel):
    """
       Бэлэн мөнгөний гүйлгээний тайлан.
    """

    _name = 'report.account.cash'
    _description = "Cashflow Report"

    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env['res.company']._company_default_get('account.bank.statement.report'))
    type = fields.Char(string='Type', required=True, default= lambda s: _('cash'), readonly=1)
    date_from = fields.Date("Start Date", required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    date_to = fields.Date("End Date", required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    journal_ids = fields.Many2many('account.journal', string="Journals")
    account_ids = fields.Many2many('account.account', string="Account")
    include_draft_statement = fields.Boolean("Include Draft Statement", default=False)
    target_move = fields.Selection([('posted', 'Posted'),
                                    ('draft', 'Draft')], 'Target move', default='posted')


    @api.multi
    def export_report(self):
        
        # create workbook
        
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        # create name
        report_name = _('Cashflow Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_filter_right = book.add_format(ReportExcelCellStyles.format_filter_right)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_group_right = book.add_format(ReportExcelCellStyles.format_group_right)
        format_group_left = book.add_format(ReportExcelCellStyles.format_group_left)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_float_color = book.add_format(ReportExcelCellStyles.format_content_float_color)
        format_content_bold_text = book.add_format(ReportExcelCellStyles.format_content_bold_text)
        format_content_bold_right = book.add_format(ReportExcelCellStyles.format_content_bold_right)
        format_content_bold_left = book.add_format(ReportExcelCellStyles.format_content_bold_left)
        format_content_bold_float = book.add_format(ReportExcelCellStyles.format_content_bold_float)

        move_line_obj = self.env['account.move.line']
        journal_obj = self.env['account.journal']
        seq = 1

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('cashflow_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 1
        # compute column
        sheet.set_column('A:A', 4)
        sheet.set_column('B:B', 8)
        sheet.set_column('C:C', 15)
        sheet.set_column('D:D', 30)
        sheet.set_column('E:E', 12)
        sheet.set_column('F:F', 12)
        sheet.set_column('G:G', 12)
        sheet.set_column('H:H', 15)

        #Header
        sheet.merge_range('A1:B2', _('Template Form MF-4'), format_filter)
        sheet.merge_range('G1:H2',_('Minister of Finance attachment to Order No. 347 of December 5, 2017'), format_content_text)
        rowx += 2

        # create name
        sheet.merge_range(rowx, 0, rowx, 4, '%s: %s' % (_('Company'), self.company_id.name), format_filter)
        rowx += 1
        sheet.merge_range('A5:H6', report_name.upper(), format_name)
        rowx += 3

        # create duration
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s - %s' % (_('Duration'), self.date_from, self.date_to), format_filter)
        sheet.merge_range(rowx, 6, rowx, 7, '%s: %s' % (_('Printed date'), time.strftime('%Y-%m-%d')),format_filter_right)
        rowx += 1

        sheet.merge_range('A9:A10', _('Seq'), format_title)
        sheet.merge_range('B9:B10', _('Date'), format_title)
        sheet.merge_range('C9:C10', _('Document Ref'), format_title)
        sheet.merge_range('D9:D10', _('Transaction Description'), format_title)
        sheet.merge_range('E9:E10', _('Income'), format_title)
        sheet.merge_range('F9:F10', _('Expense'), format_title)
        sheet.merge_range('G9:G10', _('Balance'), format_title)
        sheet.merge_range('H9:H10', _('Partner'), format_title)
        rowx += 2
        account_ids = self.account_ids.ids if self.account_ids else self.env['account.account'].search([('user_type_id.is_cash', '=', True)]).ids
        if self.target_move == 'draft':
            move_state = 'draft', 'posted'
        else:
            move_state = 'posted', ''
            
        account_qry = ""
        if account_ids and len(account_ids) > 0:
            account_qry = " AND ml.account_id in (%s) " %(','.join(map(str, account_ids)))
            
        qry = """
            SELECT j.id AS jid, j.code AS jcode, j.name AS jname, a.id AS aid, m.name AS mname, 
                a.code AS acode, a.name AS aname, m.date As date, 
                absl.name AS name, ml.currency_id AS cid, p.name AS partner, 
                CASE WHEN COALESCE(ml.debit,0) > 0 THEN ml.debit ELSE 0.00 END AS income,
                CASE WHEN COALESCE(ml.amount_currency,0) > 0 THEN ml.amount_currency ELSE 0.00 END AS cur_income, 
                CASE WHEN COALESCE(ml.amount_currency,0) < 0 THEN abs(ml.amount_currency) ELSE 0.00 END AS cur_expense, 
                CASE WHEN COALESCE(ml.credit,0) > 0 THEN abs(ml.credit) ELSE 0.00 END AS expense
            FROM account_move_line ml 
            LEFT JOIN account_move m ON (m.id = ml.move_id) 
            LEFT JOIN account_bank_statement_line absl on absl.id = m.statement_line_id
            LEFT JOIN account_journal j ON (j.id = ml.journal_id) 
            LEFT JOIN account_account a ON (ml.account_id = a.id) 
            LEFT JOIN res_partner p ON (ml.partner_id = p.id) 
            WHERE ml.company_id = %s AND m.state in %s 
                AND ml.date BETWEEN '%s' AND '%s' 
                %s
            GROUP BY ml.debit, j.id, j.code, j.name,a.id, a.code, a.name, ml.date, j.currency_id, m.name, absl.name, 
                ml.name, p.name, ml.debit, ml.credit, ml.amount_currency , m.date, ml.currency_id 
            ORDER BY  a.id, ml.date, j.name
            """ %(self.company_id.id, move_state, self.date_from, self.date_to, account_qry)
                            
        self.env.cr.execute(qry)

        balance_start = None
        lines = self.env.cr.dictfetchall()
        if lines:
            journal_id = False
            account_id = False
            currency_id = False
            inc = ex = 0
            start = cur_start = 0
            end = cur_end = 0
            journal_income = journal_expense = 0
            journal_cur_income = journal_cur_expense = 0
            total_income = total_expense = 0
            total_cur_income = total_cur_expense = 0
            total_start = total_end = 0
            total_cur_start = total_cur_end = 0
            seq = 1
            for line in lines:
                # Эхний касс/ харилцахын мэдээлэл болон эхний үлдэгдлийг гаргана
                if not account_id:
                    # Эхний үлдэгдлийг олно
                    initial = []
#                     Ноорог гүйлгээ эсэхийг шалгана
                    if not self.include_draft_statement:
                        initial = move_line_obj.get_initial_balance(self.company_id.id, [line['aid']], self.date_from, 'posted')
                    # Эхний үлдэгдэл байгаа эсэхийг шалгана
                        if initial:
                            start += initial[0]['start_debit'] - initial[0]['start_credit']
                            end = start

                            if line['cid']:
                                cur_start = initial[0]['cur_start_debit'] - initial[0]['cur_start_credit']
                                cur_end = cur_start
                    else:
                        if balance_start == None:
                            self.env.cr.execute("SELECT balance_start from account_bank_statement s "
                            "LEFT JOIN account_bank_statement_line l ON (l.statement_id = s.id) WHERE l.payment_id IS NULL AND s.date >='%s' ORDER BY s.date" % self.date_from)
                            draft_balance = self.env.cr.dictfetchall()
                            if draft_balance:
                                balance_start = draft_balance[0]['balance_start']
                                start = balance_start
                            balance_start = line['balance_start']
                            start = balance_start
                        end = start
                            # Валютын гүйлгээ байгаа эсэхийг шалгана
                        if initial:
                            if line['cid']:
                                cur_start = initial[0]['cur_start_debit'] - initial[0]['cur_start_credit']
                                cur_end = cur_start

                    total_cur_start += cur_start
                    total_start += start
                    # Кассын мэдээллийг зурна
                    # sheet.merge_range(rowx, 0, rowx, 4, '%s: %s %s' % (_('Name'), line['jcode'], line['jname']), format_group_left)
                    # sheet.write(rowx, 5, '%s' % _('Start Balance'), format_group_right)
                    # sheet.write(rowx, 6, start, format_group_float)
                    # sheet.write(rowx, 7, '', format_group_right)
                    # sheet.write(rowx, 8, '', format_group_right)
                    # sheet.write(rowx, 9, '', format_group_right)
                    # rowx += 1
                    # Дансны мэдээллийг зурна
                    sheet.write(rowx, 0, '', format_content_bold_left)
                    sheet.merge_range(rowx, 0, rowx, 7, '%s: [%s] %s' % (_('Account'), line['acode'], line['aname']), format_group_left)
                    # Валютын гүйлгээ байгаа эсэхийг шалгана
                    if line['cid']:
                        sheet.write(rowx, 6, cur_start, format_content_float_color)
                    else:
                        sheet.write(rowx, 6, '', format_content_bold_left)
                    sheet.write(rowx, 7, '', format_content_bold_left)
                    # sheet.write(rowx, 8, '', format_content_bold_left)
                    # sheet.write(rowx, 9, '', format_content_bold_left)
                    rowx += 1
                # Олон касс/харилцах сонгосон бол дараагийн журналыг гарган өмнөх журналын эцсийн үлдэгдлийг бичнэ
                if account_id and account_id != line['aid']:
                    total_income += journal_income
                    total_expense += journal_expense
                    total_cur_income += journal_cur_income
                    total_cur_expense += journal_cur_expense
                    total_end += end
                    total_cur_end += cur_end
                    # Өмнөх касс/харилцах нийт орлого, зарлагын дүнг гаргана
                    sheet.merge_range(rowx, 0, rowx, 3, '%s' % _('Account Balance'), format_content_bold_right)
                    sheet.write(rowx, 4, journal_income, format_content_bold_float)
                    sheet.write(rowx, 5, journal_expense, format_content_bold_float)
                    sheet.write(rowx, 6, '', format_content_bold_text)
                    sheet.write(rowx, 7, '', format_content_bold_text)
                    # sheet.write(rowx, 8, '', format_content_bold_text)
                    # sheet.write(rowx, 9, '', format_content_bold_text)
                    rowx += 1
                    # Валютын гүйлгээ байгаа эсэхийг шалгана. Байгаа бол валютын дүнг шинэ мөр болгон зурна
                    if currency_id:
                        sheet.write(rowx, 0, '', format_content_text)
                        sheet.write(rowx, 1, '', format_content_text)
                        sheet.write(rowx, 2, '', format_content_text)
                        sheet.write(rowx, 3, '', format_content_text)
                        sheet.write(rowx, 4, journal_cur_income, format_content_float_color)
                        sheet.write(rowx, 5, journal_cur_expense, format_content_float_color)
                        sheet.write(rowx, 6, '', format_content_text)
                        sheet.write(rowx, 7, '', format_content_text)
                        # sheet.write(rowx, 8, '', format_content_text)
                        # sheet.write(rowx, 9, '', format_content_text)
                        rowx += 1
                    # Өмнөх касс/харилцах эцсийн үлдэгдлийг гаргана
                    sheet.merge_range(rowx, 0, rowx, 5, '%s' % _('End Balance'), format_content_bold_right)
                    sheet.write(rowx, 6, end, format_content_bold_float)
                    sheet.write(rowx, 7, '', format_content_bold_text)
                    # sheet.write(rowx, 8, '', format_content_bold_text)
                    # sheet.write(rowx, 9, '', format_content_bold_text)
                    rowx += 1
                    # Валютын гүйлгээ байгаа эсэхийг шалгана. Байгаа бол валютын дүнг шинэ мөр болгон зурна
                    if currency_id:
                        sheet.write(rowx, 0, '', format_content_text)
                        sheet.write(rowx, 1, '', format_content_text)
                        sheet.write(rowx, 2, '', format_content_text)
                        sheet.write(rowx, 3, '', format_content_text)
                        sheet.write(rowx, 4, '', format_content_text)
                        sheet.write(rowx, 5, '', format_content_text)
                        sheet.write(rowx, 6, cur_end, format_content_float_color)
                        sheet.write(rowx, 7, '', format_content_text)
                        # sheet.write(rowx, 8, '', format_content_text)
                        # sheet.write(rowx, 9, '', format_content_text)
                        rowx += 1
                    # Шинэ касс/харилцахын эхний үлдэгдлийг олно
                    initial = move_line_obj.get_initial_balance(self.company_id.id, [line['aid']], self.date_from, 'posted')
                    # Эхний үлдэгдэл байгаа эсэхийг шалгана
                    if initial:
                        start = initial[0]['start_debit'] - initial[0]['start_credit']
                        end = start
                        # Валютын гүйлгээ байгаа эсэхийг шалгана
                        if line['cid']:
                            cur_start = initial[0]['cur_start_debit'] - initial[0]['cur_start_credit']
                            cur_end = cur_start
                    total_cur_start += cur_start
                    total_start += start
                    # Кассын мэдээллийг зурна
                    sheet.merge_range(rowx, 0, rowx, 4, '%s: %s %s' % (_('Name'), line['jcode'], line['jname']), format_group_left)
                    sheet.write(rowx, 5, '%s' % _('Start Balance'), format_group_right)
                    sheet.write(rowx, 6, start, format_group_float)
                    sheet.write(rowx, 7, '', format_group_right)
                    # sheet.write(rowx, 8, '', format_group_right)
                    # sheet.write(rowx, 9, '', format_group_right)
                    rowx += 1
                    # Дансны мэдээллийг зурна
                    sheet.write(rowx, 0, '', format_content_bold_left)
                    sheet.merge_range(rowx, 1, rowx, 5, '%s: [%s] %s' % (_('Account'), line['acode'], line['aname']), format_content_bold_left)
                    # Валютын гүйлгээ байгаа эсэхийг шалгана
                    if line['cid']:
                        sheet.write(rowx, 6, cur_start, format_content_float_color)
                    else:
                        sheet.write(rowx, 6, '', format_content_bold_left)
                    sheet.write(rowx, 7, '', format_content_bold_left)
                    # sheet.write(rowx, 8, '', format_content_bold_left)
                    # sheet.write(rowx, 9, '', format_content_bold_left)
                    rowx += 1
                    seq = 1
                    
                    if not initial:
                        total_cur_start = total_start = 0
                        end = cur_end = 0
                        start = cur_start = 0
                    journal_income = journal_expense = 0
                    journal_cur_income = journal_cur_expense = 0
                # Орлого, зарлагын тоог гаргаж ирнэ
                if line['income'] > 0:
                    inc += 1
                else:
                    ex += 1
                end += line['income'] - line['expense']
                # Кассын мөрийг зурна
                sheet.write(rowx, 0, seq, format_content_number)
                sheet.write(rowx, 1, line['date'], format_content_text)
                sheet.write(rowx, 2, '%s' % (line['jname']), format_content_text)
                sheet.write(rowx, 3, line['name'], format_content_text)
                sheet.write(rowx, 4, line['income'], format_content_float)
                sheet.write(rowx, 5, line['expense'], format_content_float)
                sheet.write(rowx, 6, end or 0.0, format_content_float)
                sheet.write(rowx, 7, line['partner'], format_content_text)
                rowx += 1
                # Валютын гүйлгээ байгаа эсэхийг шалгана. Байгаа бол валютын дүнг шинэ мөр болгон зурна
                if line['cid']:
                    cur_end += line['cur_income'] - line['cur_expense']
                    # Тухайн касс/харилцахын валютын нийлбэр дүнг олно
                    journal_cur_income += line['cur_income']
                    journal_cur_expense += line['cur_expense']
                    sheet.write(rowx, 0, '', format_content_text)
                    sheet.write(rowx, 1, '', format_content_text)
                    sheet.write(rowx, 2, '', format_content_text)
                    sheet.write(rowx, 3, '', format_content_text)
                    sheet.write(rowx, 4, line['cur_income'], format_content_float_color)
                    sheet.write(rowx, 5, line['cur_expense'], format_content_float_color)
                    sheet.write(rowx, 6, cur_end or 0.0, format_content_float_color)
                    sheet.write(rowx, 7, '', format_content_text)
                    rowx += 1
                seq += 1
                account_id = line['aid']
                # journal_id = line['jid']
                currency_id = line['cid']
                # Тухайн касс/харилцахын нийлбэр дүнг олно
                journal_income += line['income']
                journal_expense += line['expense']
            # Сүүлийн касс/харилцах нийт орлого, зарлагын дүнг гаргана
            total_income += journal_income
            total_expense += journal_expense
            total_cur_income += journal_cur_income
            total_cur_expense += journal_cur_expense
            total_end += end
            total_cur_end += cur_end
            sheet.merge_range(rowx, 0, rowx, 3, '%s' % _('Account Balance'), format_content_bold_right)
            sheet.write(rowx, 4, journal_income, format_content_bold_float)
            sheet.write(rowx, 5, journal_expense, format_content_bold_float)
            sheet.write(rowx, 6, '', format_content_bold_text)
            sheet.write(rowx, 7, '', format_content_bold_text)
            rowx += 1
            # Валютын гүйлгээ байгаа эсэхийг шалгана. Байгаа бол валютын дүнг шинэ мөр болгон зурна
            if currency_id:
                sheet.write(rowx, 0, '', format_content_text)
                sheet.write(rowx, 1, '', format_content_text)
                sheet.write(rowx, 2, '', format_content_text)
                sheet.write(rowx, 3, '', format_content_text)
                sheet.write(rowx, 4, journal_cur_income, format_content_float_color)
                sheet.write(rowx, 5, journal_cur_expense, format_content_float_color)
                sheet.write(rowx, 6, '', format_content_text)
                sheet.write(rowx, 7, '', format_content_text)
                rowx += 1
            # Сүүлийн касс/харилцах эцсийн үлдэгдлийг гаргана
            sheet.merge_range(rowx, 0, rowx, 5, '%s' % _('End Balance'), format_content_bold_right)
            sheet.write(rowx, 6, end, format_content_bold_float)
            sheet.write(rowx, 7, '', format_content_bold_text)
            # sheet.write(rowx, 8, '', format_content_bold_text)
            # sheet.write(rowx, 9, '', format_content_bold_text)
            rowx += 1
            # Валютын гүйлгээ байгаа эсэхийг шалгана. Байгаа бол валютын дүнг шинэ мөр болгон зурна
            if currency_id:
                sheet.write(rowx, 0, '', format_content_text)
                sheet.write(rowx, 1, '', format_content_text)
                sheet.write(rowx, 2, '', format_content_text)
                sheet.write(rowx, 3, '', format_content_text)
                sheet.write(rowx, 4, '', format_content_text)
                sheet.write(rowx, 5, '', format_content_text)
                sheet.write(rowx, 6, cur_end, format_content_float_color)
                sheet.write(rowx, 7, '', format_content_text)
                rowx += 1

            # Нийт касс/харилцах нийт орлого, зарлагын дүнг гаргана
            sheet.merge_range(rowx, 0, rowx, 3, '%s' % _('Total'), format_group_right)
            sheet.write(rowx, 4, total_income, format_group_float)
            sheet.write(rowx, 5, total_expense, format_group_float)
            sheet.write(rowx, 6, '', format_group_right)
            sheet.write(rowx, 7, '', format_group_right)
            rowx += 1
            # Валютын гүйлгээ байгаа эсэхийг шалгана. Байгаа бол валютын дүнг шинэ мөр болгон зурна
            if line['cid']:
                sheet.merge_range(rowx, 0, rowx, 3, '', format_content_text)
                sheet.write(rowx, 4, total_cur_income, format_content_float_color)
                sheet.write(rowx, 5, total_cur_expense, format_content_float_color)
                sheet.write(rowx, 6, '', format_content_text)
                sheet.write(rowx, 7, '', format_content_text)
                rowx += 1
            # Нийт касс/харилцах эхний болон эцсийн үлдэгдлийг гаргана
            sheet.merge_range(rowx, 0, rowx, 3, '%s' % _('Start Balance'), format_group_right)
            sheet.write(rowx, 4, total_start, format_group_float)
            sheet.write(rowx, 5, '%s' % _('End Balance'), format_group_right)
            sheet.write(rowx, 6, total_end, format_group_float)
            sheet.write(rowx, 7, '', format_group_right)
            rowx += 1
            # Валютын гүйлгээ байгаа эсэхийг шалгана. Байгаа бол валютын дүнг шинэ мөр болгон зурна
            if line['cid']:
                sheet.merge_range(rowx, 0, rowx, 3, '', format_content_text)
                sheet.write(rowx, 4, total_cur_start, format_content_float_color)
                sheet.write(rowx, 5, '', format_content_text)
                sheet.write(rowx, 6, total_cur_end, format_content_float_color)
                sheet.write(rowx, 7,'', format_content_text)
                rowx += 1
            rowx += 1
            sheet.merge_range(rowx, 2, rowx, 7, '%s: %s, %s: %s' % (_('Income bill'), inc, _('Expense bill'), ex), format_filter)
            rowx += 3
            sheet.merge_range(rowx, 3, rowx, 5, '%s: ........................................... /                          /' % _('Accountant'), format_filter)
            rowx += 2
            sheet.merge_range(rowx, 3, rowx, 5, '%s: ........................................... /                          /' % _('Cash Keeper'), format_filter)

        book.close()

        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()
