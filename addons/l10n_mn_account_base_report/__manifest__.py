# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Account Base Report",
    'version': '1.0',
    'depends': ['l10n_mn_account'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Account Modules',
    'description': """
         Санхүүгийн үндсэн тайлангууд
         
         - Гүйлгээ баланс тайлан
         - Баланс тайлан
         - Орлого үр дүнгийн тайлан
         - Мөнгөн гүйлгээний тайлан
         - Өмчийн өөрчлөлтийн тайлан
    """,
    'data': [
        'security/ir_rule.xml',
        'security/ir.model.access.csv',
        'views/account_base_report_menu.xml',
        'views/account_transaction_balance_view.xml',
        'views/account_mongolian_balance_sheet_view.xml',
        'views/account_profit_report_view.xml',
        'views/account_cashflow_report_view.xml',
        'wizard/account_mongolian_balance_report_view.xml',
        'wizard/report_account_cashflow_wizard.xml',
        'wizard/account_mongolian_profit_report_view.xml',
        'wizard/account_mongolia_equity_changes_report_view.xml',
        'wizard/report_account_cashflow_wizard.xml',
        'views/report_account_transaction_balance_wizard.xml',      
        'views/account_equity_change_report_view.xml'
    ]
}
