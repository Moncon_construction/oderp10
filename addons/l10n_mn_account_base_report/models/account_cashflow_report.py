# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import time
from odoo.exceptions import UserError
from datetime import date, datetime, timedelta
from operator import itemgetter

from odoo import api, fields, models, _


class AccountcashflowReport(models.Model):
    """
        Мөнгөн гүйлгээний тайлан
    """
    _name = "account.cashflow.report"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = "Account Cashflow Report"


    @api.model
    def _get_date_end(self):
        date_end = datetime.now().date()
        period = self.env['account.period'].search([('date_start', '<=', date_end), ('date_stop','>=',date_end), ('company_id','=',self.env.user.company_id.id)])
        if period:
            date_end = datetime.strptime(period.date_stop,'%Y-%m-%d')
        return date_end

    name = fields.Char('Name', required=True)
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    date_from = fields.Date("Start Date", required=True, default=lambda *a: time.strftime('%Y-01-01'))
    date_to = fields.Date("End Date", required=True,default=_get_date_end)
    line_ids = fields.One2many('account.cashflow.report.line', 'cashflow_report_id', string='Lines', readonly=True, copy=False)
    state = fields.Selection([('draft', 'Draft'),
                              ('approved', 'Approved')], string='State', default='draft', required=True, track_visibility='onchange')
    is_standart = fields.Boolean(string="Standart Cashflow Report")
    
    # Компани, эхлэх болон дуусах огноогоор нэр талбар бөглөгдөх
    @api.onchange('date_from','date_to')
    def onchange_account_date(self):
        for report in self:
            name = ''
            if report.company_id:
                name += _('%s is ') % self.company_id.name
            if report.date_from:
                name += _('[%s - ') % self.date_from
            if report.date_to:
                name += _('%s] duration cashflow') % self.date_to
            report.name = name

    @api.multi
    def unlink(self):
        for report in self:
            if report.state != 'draft':
                raise UserError(_('Delete only draft in state'))
        return super(AccountcashflowReport, self).unlink()

    @api.multi
    def validate(self):
        # Батлах
        self.write({'state': 'approved'})

    @api.multi
    def action_to_draft(self):
        # Ноороглох
        self.write({'state': 'draft'})

    def _get_children_by_order(self, cashflow_type):
        # Мөнгөн гүйлгээний төрлүүдийг олох функц
        res = []
        if cashflow_type.report_cashflow_type == 'simple':
            res.append(cashflow_type.id)
        if cashflow_type.type == 'view':
            children = self.env['account.cashflow.type'].search([('parent_id', '=', cashflow_type.id)])
            if children:
                for child in children:
                    res += self._get_children_by_order(child)
        return res

    @api.multi
    def _balance(self, res, date_from, date_to):
        # Журналын мөрөөс тухайн мөнгөн гүйгээний төрлөөр хайн нийт дүнг олох -- Хурдан болгохын тулд query болгов
        total = 0
        cashflow_types = '('+', '.join(map(str, res))+')'
        self._cr.execute("SELECT l.cashflow_id AS cashflow_id, t.value_of_amount_type AS type, sum(l.debit) AS debit, sum(l.credit) AS credit "
                         "FROM account_move_line l "
                         "JOIN account_move m ON (l.move_id = m.id) "
                         "JOIN account_cashflow_type t ON (l.cashflow_id = t.id) "
                         "WHERE m.state = 'posted' AND l.cashflow_id in %s AND l.date BETWEEN  '%s' AND '%s' AND l.company_id = %s" 
                         "GROUP BY l.cashflow_id, t.value_of_amount_type " % (cashflow_types, date_from, date_to, self.company_id.id))
        balances = self._cr.fetchall()
        for balance in balances:
            # Үнийн дүнгийн төрөл шалган дүнг олно
            if balance[1] in ('positive', 'mixed'):
                total += balance[2] - balance[3]
            else:
                total += balance[3] - balance[2]
        return total

    @api.multi
    def balance_cashflow(self, cashflow_type):
        # Орох төрөл нь энгийн, нийлмэл болон хүүхдүүдийн нийлбэр тохиолдолд дүнг тооцоолох функц
        res = []
        res1 = []
        datetime_object = datetime.strptime(self.date_from, '%Y-%m-%d')
        last_date_from = '%s-01-01' % (datetime_object.year - 1)
        last_date_to = '%s-12-31' % (datetime_object.year - 1)
        # Орох төрөл нь нийлмэл
        if cashflow_type.report_cashflow_type == 'united':
            if cashflow_type.report_cashflow_ids:
                for cashflow in cashflow_type.report_cashflow_ids:
                    res += self._get_children_by_order(cashflow)
            if cashflow_type.minus_cashflow_ids:
                for cashflow in cashflow_type.minus_cashflow_ids:
                    res1 += self._get_children_by_order(cashflow)
        # Орох төрөл нь энгийн, хүүхдүүдийн нийлбэр
        else:
            res = self._get_children_by_order(cashflow_type)
        last_total = total = 0
        if res:
            last_total = self._balance(res, last_date_from, last_date_to)
            total = self._balance(res, self.date_from, self.date_to)
        if res1:
            last_total -= self._balance(res1, last_date_from, last_date_to)
            total -= self._balance(res1, self.date_from, self.date_to)
        return last_total, total

    def account(self):
        move_line_obj = self.env['account.move.line']
        datetime_object = datetime.strptime(self.date_from, '%Y-%m-%d')
        last_date_from = '%s-01-01' % (datetime_object.year - 1)
        last_date_to = '%s-12-31' % (datetime_object.year - 1)
        last_start_balance = last_end_balance = start_balance = end_balance = 0
        # Мөнгөн хөрөнгийн данснуудыг олох
        account_ids = self.env['account.account'].search([('company_id', '=', self.company_id.id), ('internal_type', '=', 'liquidity')])
        if not account_ids:
            raise UserError(_('There is no cash and bank flow.'))
        else:
            # Өмнөх оны эхний үлдэгдэл болон эцсийн үлдэгдлийг олох
            initial_balances = move_line_obj.get_all_balance(self.company_id.id, account_ids.ids, last_date_from, last_date_to, 'posted')
            for init in initial_balances:
                last_start_balance += init['start_balance']
                last_end_balance += init['start_balance'] + init['debit'] - init['credit']
            # Тайлант хугацааны оны эхний үлдэгдэл болон эцсийн үлдэгдлийг олох
            balances = move_line_obj.get_all_balance(self.company_id.id, account_ids.ids, self.date_from, self.date_to, 'posted')
            for balance in balances:
                start_balance += balance['start_balance']
                end_balance += balance['start_balance'] + balance['debit'] - balance['credit']
        return last_start_balance, last_end_balance, start_balance, end_balance

    @api.multi
    def compute(self):
        # Тооцоолол хийх функц
        self.line_ids.unlink()
        line_obj = self.env['account.cashflow.report.line']
        cashflow_types = self.env['account.cashflow.type'].search([('company_id', '=', self.company_id.id), ('code', '!=', 'x')], order='sequence, code')
        for cashflow_type in cashflow_types:
            line_type = 'parent'
            if cashflow_type.report_cashflow_type == 'simple':
                color = 'black'
                line_type = 'view'
            elif cashflow_type.report_cashflow_type == 'child_sum':
                color = 'blue'
            else:
                color = 'bold'
            if cashflow_type.report_cashflow_type == 'display':
                initial = end = ''
                initial_balance = end_balance = 0
            elif cashflow_type.report_cashflow_type == 'initial':
                account_balance = self.account()
                initial = "{:,.2f}".format(account_balance[0] or 0)
                initial_balance = account_balance[0] or 0
                end = "{:,.2f}".format(account_balance[2] or 0)
                end_balance = account_balance[2] or 0
            elif cashflow_type.report_cashflow_type == 'end':
                account_balance = self.account()
                initial = "{:,.2f}".format(account_balance[1] or 0)
                initial_balance = account_balance[1] or 0
                end = "{:,.2f}".format(account_balance[3] or 0)
                end_balance = account_balance[3] or 0
            else:
                cashflow_balance = self.balance_cashflow(cashflow_type)
                initial = "{:,.2f}".format(cashflow_balance[0] or 0)
                initial_balance = cashflow_balance[0] or 0
                end = "{:,.2f}".format(cashflow_balance[1] or 0)
                end_balance = cashflow_balance[1] or 0

            # Мөрийг үүсгэх
            line_obj.create({'line_number': cashflow_type.code,
                             'cashflow_report_id': self.id,
                             'cashflow_id': cashflow_type.id,
                             'name':cashflow_type.name,
                             'initial_balance': initial_balance,
                             'end_balance': end_balance,
                             'initial': initial,
                             'end': end,
                             'color': color,
                             'line_type': line_type
                             })
        return True


class AccountCashflowReportLine(models.Model):
    _name = 'account.cashflow.report.line'
    _description = 'Account cashflow Report Line'

    name = fields.Char('Name')
    line_number = fields.Char(string='Code')
    cashflow_report_id = fields.Many2one('account.cashflow.report', string="Account cashflow Report", ondelete='cascade')
    cashflow_id = fields.Many2one('account.cashflow.type', string="Account Report Name")
    line_type = fields.Selection([('view', 'View'),
                                ('parent', 'Parent'),
                                ], string='Type', required=True, default='view')
    initial = fields.Char(string='Initial Balance')
    end = fields.Char(string='End Balance')
    initial_balance = fields.Float(string="Initial Amount",digits=(16, 2), default=0)
    end_balance = fields.Float(string="End Amount",digits=(16, 2), default=0)
    color = fields.Selection([('black', 'Black'),
                              ('bold', 'Bold'),
                              ('blue', 'Blue')], string='Color', default='black')

    @api.multi
    def button_initial_journal_entries(self):
        # Эхний үлдэгдлийн журналын мөрүүдийг харуулах
        for line in self:
            datetime_object = datetime.strptime(line.cashflow_report_id.date_from, '%Y-%m-%d')
            dateyear = '%s-01-01' % (datetime_object.year - 1)
            lastyear = '%s-12-31' % (datetime_object.year - 1)
            domain = [('cashflow_id', '=', line.cashflow_id.id),('date', '>=', dateyear), ('date', '<=', lastyear)]
            return {
                'name': _('Journal Entry Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.move.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'context': {'group_by': ['cashflow_id', 'account_id']},
                'domain': domain
            }

    @api.multi
    def button_end_journal_entries(self):
        # Хугацааны хоорогдох журналын мөрүүдийг харуулах
        for line in self:
            domain = [('cashflow_id', '=', line.cashflow_id.id),('date', '>=', line.cashflow_report_id.date_from),('date', '<=', line.cashflow_report_id.date_to)]
            return {
                'name': _('Journal Entry Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.move.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'context': {'group_by': ['cashflow_id', 'account_id']},
                'domain': domain
            }
