# -*- encoding: utf-8 -*-
##############################################################################
import time
from datetime import datetime, timedelta

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.tools.safe_eval import safe_eval
from odoo.osv import expression



class AccountMongolianBalanceSheet(models.Model):
    """
        САНХҮҮ БАЙДЛЫН ТАЙЛАН - БАЛАНС ТАЙЛАН.
    """
    _name = "account.mongolian.balance.sheet"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = "Account Mongolian Balance Report"

    @api.model
    def _get_account_report(self):
        reports = self.env['account.financial.report'].search([('chart_type', '=', 'balance'), ('type', '=', 'sum'), ('company_id', '=', self.env.user.company_id.id)])
        return reports and reports[0] or False

    name = fields.Char('Name', required=True, track_visibility='onchange', states={'approved': [('readonly', True)]})
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env['res.company']._company_default_get('account.mongolian.balance.sheet'))
    date_from = fields.Date('Start Date', required=True, default=lambda *a: time.strftime('%Y-01-01'), track_visibility='onchange', states={'approved': [('readonly', True)]})
    date_to = fields.Date('End Date', required=True, default=lambda *a: time.strftime('%Y-%m-%d'), track_visibility='onchange', states={'approved': [('readonly', True)]})
    account_report_id = fields.Many2one('account.financial.report', string='Account Reports', required=True, default=_get_account_report)
    target_move = fields.Selection([('posted', 'All Posted Entries'),
                                    ('all', 'All Entries')], string='Target Moves', required=True, default='posted')
    line_ids = fields.One2many('account.mongolian.balance.sheet.line', 'balance_id', string='Lines', limit=600, readonly=True, copy=False)
    description = fields.Text('Description')
    state = fields.Selection([('draft', 'Draft'),
                              ('approved', 'Approved')], string='State', default='draft', required=True, track_visibility='onchange')

    # Компани, эхлэх болон дуусах огноогоор нэр талбар бөглөгдөх
    @api.onchange('date_from', 'date_to')
    def onchange_account_transaction(self):
        for report in self:
            name = ''
            if report.company_id:
                name += _('%s is ') % self.company_id.name
            if report.date_from:
                name += _('[%s - ') % self.date_from
            if report.date_to:
                name += _('%s] duration Mongolian balance report') % self.date_to
            report.name = name

    @api.multi
    def unlink(self):
        for report in self:
            if report.state != 'draft':
                raise UserError(_('Delete only draft in state'))
        return super(AccountMongolianBalanceSheet, self).unlink()

    @api.multi
    def validate(self):
        # Батлах
        self.write({'state': 'approved'})

    @api.multi
    def action_to_draft(self):
        # Ноороглох
        self.write({'state': 'draft'})

    @api.multi
    def compute(self):
        # Тооцоолол хийх функц
        self.line_ids = None
        line_obj = self.env['account.mongolian.balance.sheet.line']

        if not self.account_report_id:
            raise UserError(_('Please select a financial report.'))

        # Тухайн тайлангийн бүтцэд хамаарах хүүхдүүдийг хайж байна.
        # Өмнөх өдрөөр эхний үлдэгдлийг татна
        before_day = datetime.strptime(self.date_from, DEFAULT_SERVER_DATE_FORMAT) + timedelta(days=-1)
        # Одоо байгаа үндсэн функцийг өөрчлөхгүйгээр, company_id-г list болгох
        company_id = []
        company_id.append(self.company_id.id)
        init_report_ids = self.account_report_id.with_context(date_to=before_day.strftime(DEFAULT_SERVER_DATE_FORMAT), state=self.target_move, company_id=company_id)._get_children_by_order()

        parent_numbers = {}
        parent_index = 1
        for account in init_report_ids:
            end_report_ids = account.with_context(date_to=self.date_to, state=self.target_move, company_id=company_id)._get_children_by_order()
            for end_account in end_report_ids:
                if end_account.id == account.id:
                    if account.id == self.account_report_id.id:
                        continue
                    # Тайлангийн мөрийн дугаарыг тооцож байна.
                    number = parent_index
                    if account.parent_id.id not in parent_numbers:
                        parent_numbers[account.id] = [str(number), 0]
                        parent_index += 1
                    else:
                        number = parent_numbers[account.parent_id.id][0] + '.' + str(parent_numbers[account.parent_id.id][1] + 1)
                        parent_numbers[account.parent_id.id][1] += 1
                        parent_numbers[account.id] = [str(number), 0]
                    # Тайлангийн шатлалын дагуу мөрийн дугаарлалтыг олж байна
                    number_str = '%s' % number
                    if account.style_overwrite in [1, 2, 3]:
                        name_str = u'%s' % account.name or ''
                        color = 'blue'
                    else:
                        name_str = u'   %s' % account.name or ''
                        color = 'black'
                    initial = end = ''
                    initial_balance = end_balance = 0
                    if not (account.type in ('account_report', 'sum') and not account.account_report_id):
                        initial = "{:,.2f}".format(account.balance or 0)
                        end = "{:,.2f}".format(end_account.balance or 0)
                        initial_balance = account.balance or 0
                        end_balance = end_account.balance or 0
                    # Үндсэн тайлангийн мөрийг зурна
                    line_obj.create({'line_number': number_str,
                                     'name': name_str,
                                     'financial_report_id': account.id,
                                     'type': account.type,
                                     'initial': '%s' % initial,
                                     'end': '%s' % end,
                                     'initial_balance': initial_balance,
                                     'end_balance': end_balance,
                                     'balance_id': self.id,
                                     'color': 'bold' if account.type in ('account_report','sum') and not account.account_report_id else color
                                     })
        return True

class AccountMongolianBalanceSheetLine(models.Model):
    _name = 'account.mongolian.balance.sheet.line'
    _description = 'Account Mongolian Balance Report Line'

    line_number = fields.Char(string='Line Number')
    name = fields.Char(string='Indication')
    financial_report_id = fields.Many2one('account.financial.report', string='Account Financial Report')
    type = fields.Selection(related='financial_report_id.type', string='Type')
    balance_id = fields.Many2one('account.mongolian.balance.sheet', string='Mongolian Balance Report', ondelete="cascade")
    initial_balance = fields.Float(string='Initial Balance', digits=(16, 2), default=0)
    end_balance = fields.Float(string='End Balance', digits=(16, 2), default=0)
    initial = fields.Char(string='Initial Balance')
    end = fields.Char(string='End Balance')
    color = fields.Selection([('black', 'Black'),
                              ('bold', 'Bold'),
                              ('blue', 'Blue')], string='Color', default='black')

    @api.multi
    def button_initial_journal_entries(self):
        # Эхний үлдэгдлийн журналын мөрүүдийг харуулах
        for line in self:
            if line.type == 'accounts':
                accounts = line.financial_report_id.account_ids
            elif line.type == 'account_type':
                accounts = self.env['account.account'].search([('user_type_id', 'in', line.financial_report_id.account_type_ids.ids),
                                                               ('company_id', '=', line.balance_id.company_id.id)])
            domain = [('account_id', 'in', accounts.ids), ('date', '<', line.balance_id.date_from), ('company_id', '=', line.balance_id.company_id.id)]
            if self.balance_id.target_move == 'posted':
                domain = expression.AND([domain, [('move_id.state', '=', 'posted')]])
            return {
                'name': _('Journal Entry Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.move.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'context': {'group_by': ['account_id']},
                'domain': domain
            }

    @api.multi
    def button_end_journal_entries(self):
        # Эцсийн үлдэгдлийн журналын мөрүүдийг харуулах
        for line in self:
            if line.type == 'accounts':
                accounts = line.financial_report_id.account_ids
            elif line.type == 'account_type':
                accounts = self.env['account.account'].search([('user_type_id', 'in', line.financial_report_id.account_type_ids.ids),
                                                               ('company_id', '=', line.balance_id.company_id.id)])
            domain = [('account_id', 'in', accounts.ids), ('date', '<=', line.balance_id.date_to), ('company_id', '=', line.balance_id.company_id.id)]
            if self.balance_id.target_move == 'posted':
                domain = expression.AND([domain, [('move_id.state', '=', 'posted')]])
            return {
                'name': _('Journal Entry Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.move.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'context': {'group_by': ['account_id']},
                'domain': domain
            }