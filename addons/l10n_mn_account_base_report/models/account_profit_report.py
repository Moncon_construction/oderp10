# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
from odoo.exceptions import UserError
from datetime import date, datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT

from odoo import api, fields, models, _

from odoo.osv import expression
from cookielib import domain_match

class AccountProfitReport(models.Model):
    """
        Орлогын дэлгэрэнгүй тайлан
    """
    _name = "account.profit.report"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = "Account Profit Report"

    @api.model
    def _get_account_report(self):
        reports = []
        reports = self.env['account.financial.report'].search([('chart_type', '=', 'profit')],order='sequence')
        return reports and reports[0] or False

    @api.model
    def _get_date_end(self):
        date_end = datetime.now().date()
        period = self.env['account.period'].search([('date_start', '<=', date_end),('date_stop','>=',date_end)])
        if period:
            date_end = datetime.strptime(period.date_stop,'%Y-%m-%d')
        return date_end

    name = fields.Char('Name', required=True)
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    date_from = fields.Date("Start Date", required=True, default=lambda *a: time.strftime('%Y-01-01'))
    date_to = fields.Date("End Date", required=True,default=_get_date_end)
    account_report_id = fields.Many2one('account.financial.report', string='Account Reports', required=True, default=_get_account_report, domain=[('chart_type', '=', 'profit')])
    line_ids = fields.One2many('account.profit.report.line', 'profit_report_id', string='Lines', readonly=True, copy=False)
    target_move = fields.Selection([('posted', 'All Posted Entries'),
                                    ('all', 'All Entries'),
                                    ], string='Target Moves', required=True, default='posted')
    state = fields.Selection([('draft', 'Draft'),
                              ('approved', 'Approved')], string='State', default='draft', required=True, track_visibility='onchange')
    
    # Компани, эхлэх болон дуусах огноогоор нэр талбар бөглөгдөх
    @api.onchange('date_from','date_to')
    def onchange_account_date(self):
        for report in self:
            name = ''
            if report.company_id:
                name += _('%s is ') % self.company_id.name
            if report.date_from:
                name += _('[%s - ') % self.date_from
            if report.date_to:
                name += _('%s] duration Income statement') % self.date_to
            report.name = name

    @api.multi
    def unlink(self):
        for report in self:
            if report.state != 'draft':
                raise UserError(_('Delete only draft in state'))
        return super(AccountProfitReport, self).unlink()

    @api.multi
    def validate(self):
        # Батлах
        self.write({'state': 'approved'})

    @api.multi
    def action_to_draft(self):
        # Ноороглох
        self.write({'state': 'draft'})

    @api.multi
    def compute(self):
        # Тооцоолол хийх функц
        self.line_ids = None
        account_ids = re_account_ids = []
        account_obj = self.env['account.account']
        move_line_obj = self.env['account.move.line']
        line_obj = self.env['account.profit.report.line']

        # Тухайн тайлангийн бүтцэд хамаарах хүүхдүүдийг хайж байна.
        # Өмнөх өдрөөр эхний үлдэгдлийг татна
        before_day = datetime.strptime(self.date_from, DEFAULT_SERVER_DATE_FORMAT) + timedelta(days=-1)
        init_report_ids = self.account_report_id.with_context(date_to=before_day.strftime(DEFAULT_SERVER_DATE_FORMAT), state=self.target_move, company_id=self.company_id.ids)._get_children_by_order()

        parent_numbers = {}
        parent_index = 1
        number_str = ""
        initial_balance = end_balance = 0
        for account in init_report_ids:
            lines = []
            account_ids = account._get_all_accounts(lines)
            end_report_ids = account.with_context(date_to=self.date_to, state=self.target_move, company_id=self.company_id.ids)._get_children_by_order()
            for end_account in end_report_ids:
                if end_account.id == account.id:
                    if account.id == self.account_report_id.id:
                        continue
                    # Тайлангийн мөрийн дугаарыг тооцож байна.
                    number = parent_index
                    if account.parent_id.id not in parent_numbers:
                        parent_numbers[account.id] = [str(number), 0]
                        parent_index += 1
                    else:
                        number = parent_numbers[account.parent_id.id][0] + '.' + str(parent_numbers[account.parent_id.id][1] + 1)
                        parent_numbers[account.parent_id.id][1] += 1
                        parent_numbers[account.id] = [str(number), 0]
                    # Тайлангийн шатлалын дагуу мөрийн дугаарлалтыг олж байна
                    number_str = '%s' % number
                    if account.style_overwrite in [1, 2, 3]:
                        name_str = u'%s' % account.name or ''
                        color = 'blue'
                    else:
                        name_str = u'   %s' % account.name or ''
                        color = 'black'
                    initial = end = ''
                    initial_balance = end_balance = 0
                    if not (account.type in ('account_report', 'sum') and not account.account_report_id):
                        initial = "{:,.2f}".format(account.balance or 0)
                        end = "{:,.2f}".format(end_account.balance or 0)
                        initial_balance = account.balance or 0
                        end_balance = end_account.balance or 0
                if type(account_ids) != list:
                    for key, value in account_ids.items():
                        if value not in re_account_ids: 
                            re_account_ids += value
                    account_ids = re_account_ids
                    line_obj.create({'line_number': number_str,
                                     'profit_report_id': self.id,
                                     'account_financial_report_id': account.id,
                                     'account_report_type': account.type,
                                     'name':account.name,
                                     'initial_balance': initial_balance,
                                     'end_balance': end_balance,
                                     'account_ids':[(6, 0, list(account_ids))],
                                     'color': 'bold' if account.type in ('account_report','sum') and not account.account_report_id else color
                                     })
        return True
    
class AccountProfitReportLine(models.Model):
    _name = 'account.profit.report.line'
    _description = 'Account Profit Report Line'

    name = fields.Char('Name')
    line_number = fields.Char(string='Line Number')
    profit_report_id = fields.Many2one('account.profit.report', string="Account Profit Report", ondelete='cascade')
    account_financial_report_id = fields.Many2one('account.financial.report', string="Account Report Name")
    account_ids = fields.Many2many('account.account', string='Accounts')
    account_report_type = fields.Selection([('sum', 'Sum'),
                                            ('accounts', 'Accounts'),
                                            ('account_type', 'Account Type'),
                                            ('account_report', 'Account Report'),], string='Type')
    initial_balance = fields.Float(string="Initial Amount",digits=(16, 2), default=0)
    end_balance = fields.Float(string="End Amount",digits=(16, 2), default=0)
    color = fields.Selection([('black', 'Black'),
                              ('bold', 'Bold'),
                              ('blue', 'Blue')], string='Color', default='black')

    @api.multi
    def button_initial_journal_entries(self):
        # Эхний үлдэгдлийн журналын мөрүүдийг харуулах
        for line in self:
            domain = [('account_id', '=', line.account_ids.ids),('date', '<', line.profit_report_id.date_from)]
            if line.profit_report_id.target_move == 'posted':
                domain = expression.AND([domain, [('move_id.state', '=', 'posted')]])
            return {
                'name': _('Journal Entry Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.move.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'context': {'group_by': ['account_id']},
                'domain': domain
            }

    @api.multi
    def button_end_journal_entries(self):
        # Хугацааны хоорогдох журналын мөрүүдийг харуулах
        for line in self:
            domain = [('account_id', '=', line.account_ids.ids),('date', '>=', line.profit_report_id.date_from),('date', '<=', line.profit_report_id.date_to)]
            if line.profit_report_id.target_move == 'posted':
                domain = expression.AND([domain, [('move_id.state', '=', 'posted')]])
            return {
                'name': _('Journal Entry Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.move.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'context': {'group_by': ['account_id']},
                'domain': domain
            }
