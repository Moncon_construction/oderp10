# -*- encoding: utf-8 -*-
##############################################################################
import time
from operator import itemgetter
from odoo.exceptions import ValidationError, UserError

from odoo import api, fields, models, _


class AccountTransactionBalance(models.Model):
    _name = 'account.transaction.balance'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = 'Account Transaction Balance Display'

    name = fields.Char('Name', required=True, track_visibility='onchange', states={'approved': [('readonly', True)]})
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env['res.company']._company_default_get('account.transaction.balance'))
    target_move = fields.Selection([('posted', 'All Posted Entries'),
                                    ('all', 'All Entries')], string='Target Moves', required=True, default='posted', track_visibility='onchange', states={'approved': [('readonly', True)]})
    date_from = fields.Date('Start Date', required=True, default=lambda *a: time.strftime('%Y-%m-01'), track_visibility='onchange', states={'approved': [('readonly', True)]})
    date_to = fields.Date('End Date', required=True, default=lambda *a: time.strftime('%Y-%m-%d'), track_visibility='onchange', states={'approved': [('readonly', True)]})
    account_ids = fields.Many2many('account.account', string='Accounts', states={'approved': [('readonly', True)]})
    account_type_ids = fields.Many2many('account.account.type', string='Account Types', states={'approved': [('readonly', True)]})
    line_ids = fields.One2many('account.transaction.balance.line', 'transaction_id', string='Lines', readonly=True, copy=False)
    currency_line_ids = fields.One2many('account.transaction.balance.currency.line', 'transaction_id', string='Currency Lines', readonly=True, copy=False)
    currency_transaction = fields.Boolean(string='Currency Transaction', default=False, track_visibility='onchange', states={'approved': [('readonly', True)]})
    group_account = fields.Boolean(string='Group by Account Type', default=True, track_visibility='onchange', states={'approved': [('readonly', True)]})
    without_profit_revenue = fields.Boolean(string='Without Reserve & Profit/Loss account', track_visibility='onchange', states={'approved': [('readonly', True)]})
    description = fields.Text('Description')
    state = fields.Selection([('draft', 'Draft'),
                              ('approved', 'Approved')], string='State', default='draft', required=True, track_visibility='onchange')

    # Компани, эхлэх болон дуусах огноогоор нэр талбар бөглөгдөх
    @api.onchange('date_from','date_to')
    def onchange_account_transaction(self):
        for report in self:
            name = ''
            if report.company_id:
                name += _('%s is ') % self.company_id.name
            if report.date_from:
                name += _('[%s - ') % self.date_from
            if report.date_to:
                name += _('%s] duration Transaction balance') % self.date_to
            report.name = name

    @api.onchange('account_type_ids')
    def onchange_account_type_ids(self):
        if self.account_type_ids:
            return {'domain': {'account_ids': [('user_type_id', 'child_of', self.account_type_ids.ids)]}}
        else:
            return {}

    @api.multi
    def unlink(self):
        for report in self:
            if report.state != 'draft':
                raise UserError(_('Delete only draft in state'))
        return super(AccountTransactionBalance, self).unlink()

    @api.multi
    def validate(self):
        # Батлах
        self.write({'state': 'approved'})

    @api.multi
    def action_to_draft(self):
        # Ноороглох
        self.write({'state': 'draft'})

    @api.multi
    def compute(self):
        # Тооцоолол хийх функц
        self.line_ids = None
        self.currency_line_ids = None
        account_ids = []

        account_obj = self.env['account.account']
        move_line_obj = self.env['account.move.line']
        line_obj = self.env['account.transaction.balance.line']
        currency_line_obj = self.env['account.transaction.balance.currency.line']
        currency_line_obj = self.env['account.transaction.balance.currency.line']

        if self.account_ids:
            for account_id in self.account_ids.ids:
                account_ids.append(account_id)
        elif self.account_type_ids:
            account = account_obj.search([('user_type_id', 'child_of', self.account_type_ids.ids)])
            if account:
                for account_id in account.ids:
                    account_ids.append(account_id)
            if not account_ids:
                raise UserError(_('There are no accounts in selected account types'))
        else:
            account_ids = account_obj.search([]).ids
        # Дансны дүнг дуудах
        balances = move_line_obj.with_context(without_profit_revenue = self.without_profit_revenue,
                                              group_account=self.group_account).get_all_balance(self.company_id.id, account_ids,
                                                                                                self.date_from, self.date_to, self.target_move)
        for line in balances:
            end_balance = line['start_balance'] + line['debit'] - line['credit']
            if self.group_account:
                type = line['atid']
            else:
                type = False
            if self.currency_transaction:
                cur_end_balance = line['cur_start_balance'] + line['cur_debit'] - line['cur_credit']
                currency_line_obj.create({'account_id': line['account_id'],
                                          'account_type_id': type,
                                          'code': line['acode'],
                                          'name': line['aname'],
                                          'currency_id': line['cid'] or self.company_id.currency_id and self.company_id.currency_id.id,
                                          'initial_debit': (line['start_balance'] > 0 and line['start_balance']) or 0.0,
                                          'initial_credit': (line['start_balance'] < 0 and -line['start_balance']) or 0.0,
                                          'initial_currency_debit': (line['cur_start_balance'] > 0 and line['cur_start_balance']) or 0.0,
                                          'initial_currency_credit': (line['cur_start_balance'] < 0 and -line['cur_start_balance']) or 0.0,
                                          'debit': line['debit'] or 0.0,
                                          'credit': line['credit'] or 0.0,
                                          'currency_debit': line['cur_debit'] or 0.0,
                                          'currency_credit': line['cur_credit'] or 0.0,
                                          'end_debit': (end_balance > 0 and end_balance) or 0.0,
                                          'end_credit': (end_balance < 0 and -end_balance) or 0.0,
                                          'end_currency_debit': (cur_end_balance > 0 and cur_end_balance) or 0.0,
                                          'end_currency_credit': (cur_end_balance < 0 and -cur_end_balance) or 0.0,
                                          'transaction_id': self.id
                                          })
            else:
                line_obj.create({'account_id': line['account_id'],
                                 'account_type_id': type,
                                 'code': line['acode'],
                                 'name': line['aname'],
                                 'initial_debit': (line['start_balance'] > 0 and line['start_balance']) or 0.0,
                                 'initial_credit': (line['start_balance'] < 0 and -line['start_balance']) or 0.0,
                                 'debit': line['debit'] or 0.0,
                                 'credit': line['credit'] or 0.0,
                                 'end_debit': (end_balance > 0 and end_balance) or 0.0,
                                 'end_credit': (end_balance < 0 and -end_balance) or 0.0,
                                 'transaction_id': self.id
                                 })
        return True


class AccountTransactionBalanceLine(models.Model):
    _name = 'account.transaction.balance.line'
    _description = 'Account Transaction Balance Line'

    account_id = fields.Many2one('account.account', string="Account")
    account_type_id = fields.Many2one('account.account.type', string="Account Type")
    code = fields.Char('Account Code')
    name = fields.Char('Account Name')
    transaction_id = fields.Many2one('account.transaction.balance', string='Transaction Balance', ondelete='cascade')
    initial_debit = fields.Float(string='Initial /Dt/', digits=(16, 2), default=0)
    initial_credit = fields.Float(string='Initial /Ct/', digits=(16, 2), default=0)
    debit = fields.Float(string='Transaction /Dt/', digits=(16, 2), default=0)
    credit = fields.Float(string='Transaction /Ct/', digits=(16, 2), default=0)
    end_debit = fields.Float(string='End /Dt/', digits=(16, 2), default=0)
    end_credit = fields.Float(string='End /Ct/', digits=(16, 2), default=0)

    @api.multi
    def button_initial_journal_entries(self):
        # Эхний үлдэгдлийн журналын мөрүүдийг харуулах
        for line in self:
            if line.transaction_id.without_profit_revenue and line.transaction_id.company_id and line.transaction_id.company_id.period_journal_id:
                return {
                    'name': _('Journal Entry Lines'),
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'res_model': 'account.move.line',
                    'view_id': False,
                    'type': 'ir.actions.act_window',
                    'domain': [('account_id', '=', line.account_id.id), ('date', '<', line.transaction_id.date_from),
                               ('journal_id', '!=', line.transaction_id.company_id.period_journal_id.id)],
                }
            return {
                'name': _('Journal Entry Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.move.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'domain': [('account_id', '=', line.account_id.id),('date', '<', line.transaction_id.date_from)],
            }

    @api.multi
    def button_journal_entries(self):
        # Хугацааны хоорогдох журналын мөрүүдийг харуулах
        for line in self:
            if line.transaction_id.without_profit_revenue and line.transaction_id.company_id and line.transaction_id.company_id.period_journal_id:
                return {
                    'name': _('Journal Entry Lines'),
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'res_model': 'account.move.line',
                    'view_id': False,
                    'type': 'ir.actions.act_window',
                    'domain': [('account_id', '=', line.account_id.id), ('date', '>=', line.transaction_id.date_from),
                               ('date', '<=', line.transaction_id.date_to), ('journal_id', '!=', line.transaction_id.company_id.period_journal_id.id)],
                }
            return {
                'name': _('Journal Entry Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.move.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'domain': [('account_id', '=', line.account_id.id),('date', '>=', line.transaction_id.date_from),('date', '<=', line.transaction_id.date_to)],
            }


class AccountTransactionBalanceCurrencyLine(models.Model):
    _name = 'account.transaction.balance.currency.line'
    _description = 'Account Transaction Balance Currency Line'

    account_id = fields.Many2one('account.account', string='Account')
    account_type_id = fields.Many2one('account.account.type', string="Account Type")
    code = fields.Char('Account Code')
    name = fields.Char('Account Name')
    currency_id = fields.Many2one('res.currency', string='Currency')
    transaction_id = fields.Many2one('account.transaction.balance', string='Transaction Balance')
    initial_debit = fields.Float(string='Initial Mnt /Dt/', digits=(16, 2), default=0)
    initial_credit = fields.Float(string='Initial Mnt /Ct/', digits=(16, 2), default=0)
    initial_currency_debit = fields.Float(string='Initial Currency /Dt/', digits=(16, 2), default=0)
    initial_currency_credit = fields.Float(string='Initial Currency /Ct/', digits=(16, 2), default=0)
    debit = fields.Float(string='Transaction Mnt /Dt/', digits=(16, 2), default=0)
    credit = fields.Float(string='Transaction Mnt /Ct/', digits=(16, 2), default=0)
    currency_debit = fields.Float(string='Transaction Currency /Dt/', digits=(16, 2), default=0)
    currency_credit = fields.Float(string='Transaction Currency /Ct/', digits=(16, 2), default=0)
    end_debit = fields.Float(string='End Mnt /Dt/', digits=(16, 2), default=0)
    end_credit = fields.Float(string='End Mnt /Ct/', digits=(16, 2), default=0)
    end_currency_debit = fields.Float(string='End Currency /Dt/', digits=(16, 2), default=0)
    end_currency_credit = fields.Float(string='End Currency /Ct/', digits=(16, 2), default=0)

    @api.multi
    def button_initial_journal_entries(self):
        # Эхний үлдэгдлийн журналын мөрүүдийг харуулах
        for line in self:
            if line.transaction_id.without_profit_revenue and line.transaction_id.company_id and line.transaction_id.company_id.period_journal_id:
                return {
                    'name': _('Journal Entry Lines'),
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'res_model': 'account.move.line',
                    'view_id': False,
                    'type': 'ir.actions.act_window',
                    'domain': [('account_id', '=', line.account_id.id), ('date', '<', line.transaction_id.date_from),
                               ('journal_id', '!=', line.transaction_id.company_id.period_journal_id.id)],
                }

            return {
                'name': _('Journal Entry Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.move.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'domain': [('account_id', '=', line.account_id.id),('date', '<', line.transaction_id.date_from)],
            }

    @api.multi
    def button_journal_entries(self):
        # Хугацааны хоорондох журналын мөрүүдийг харуулах
        for line in self:
            if line.transaction_id.without_profit_revenue and line.transaction_id.company_id and line.transaction_id.company_id.period_journal_id:
                return {
                    'name': _('Journal Entry Lines'),
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'res_model': 'account.move.line',
                    'view_id': False,
                    'type': 'ir.actions.act_window',
                    'domain': [('account_id', '=', line.account_id.id), ('date', '>=', line.transaction_id.date_from),
                               ('date', '<=', line.transaction_id.date_to), ('journal_id', '!=', line.transaction_id.company_id.period_journal_id.id)],
                }

            return {
                'name': _('Journal Entry Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.move.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'domain': [('account_id', '=', line.account_id.id),('date', '>=', line.transaction_id.date_from),('date', '<=', line.transaction_id.date_to)],
            }