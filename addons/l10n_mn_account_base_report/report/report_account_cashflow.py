# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
from datetime import datetime, timedelta
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell

from odoo import api, models, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class AccountCahsflowReport(models.Model):
    _inherit = 'account.cashflow.report'

    @api.multi
    def get_sheet(self, sheet):
        # compute column
        colx_number = 3
        sheet.set_column('A:A', 8)
        sheet.set_column('B:B', 45)
        sheet.set_column('C:D', 18)
        return sheet, colx_number

    @api.multi
    def get_header(self, sheet, rowx, format_title, format_title_small, str_start, str_stop):
        # Тайлангийн хүснэгтийн толгой зурах
        sheet.merge_range(rowx, 0, rowx + 1, 0, _('Code'), format_title)
        sheet.merge_range(rowx, 1, rowx + 1, 1, _('Indication'), format_title)
        sheet.merge_range(rowx, 2, rowx + 1, 2, str_start, format_title)
        sheet.merge_range(rowx, 3, rowx + 1, 3, str_stop, format_title)
        return sheet

    @api.multi
    def get_value(self, sheet, rowx, format_text, format_float, format_bold_text, format_bold_float, line):
        # Тайлангийн мөр зурах
        self._cr.execute("SELECT line_number,name,initial_balance,end_balance,cashflow_id FROM account_cashflow_report_line WHERE cashflow_report_id = '%s' ORDER BY line_number" % self.id)
        results = self._cr.dictfetchall()
        #CHANGE START Мөнгөн гүйлгээний тайлангийн төрлүүдийг задаргаатай харуулдаг болгов
        if not self.is_standart:
            for types in results:
                counts = types['line_number'].split('.')
                if len(counts) == 1:
                    sheet.write(rowx, 0, types['line_number'], format_bold_text)
                    sheet.write(rowx, 1, types['name'], format_bold_text)
                    sheet.write(rowx, 2, types['initial_balance'], format_bold_float)
                    sheet.write(rowx, 3, types['end_balance'], format_bold_float)
                elif len(counts) == 2:
                    sheet.write(rowx, 0, types['line_number'], format_bold_text)
                    sheet.write(rowx, 1, types['name'], format_bold_text)
                    sheet.write(rowx, 2, types['initial_balance'], format_bold_float)
                    sheet.write(rowx, 3, types['end_balance'], format_bold_float)
                elif len(counts) == 3:
                    sheet.write(rowx, 0, types['line_number'], format_bold_text)
                    sheet.write(rowx, 1, types['name'], format_bold_text)
                    sheet.write(rowx, 2, types['initial_balance'], format_bold_float)
                    sheet.write(rowx, 3, types['end_balance'], format_bold_float)
                elif len(counts) == 4:
                    sheet.write(rowx, 0, types['line_number'], format_text)
                    sheet.write(rowx, 1,  '        %s' % types['name'], format_text)
                    sheet.write(rowx, 2, types['initial_balance'], format_float)
                    sheet.write(rowx, 3, types['end_balance'], format_float)
                rowx += 1
                #CHANGE END
        else:
                if line.cashflow_id.is_standart:
                    if line.color == 'bold':
                        sheet.write(rowx, 0, line.line_number, format_bold_text)
                        sheet.write(rowx, 1, line.name, format_bold_text)
                        sheet.write(rowx, 2, line.initial_balance, format_bold_float)
                        sheet.write(rowx, 3, line.end_balance, format_bold_float)
                    else:
                        sheet.write(rowx, 0, line.line_number, format_text)
                        sheet.write(rowx, 1, line.name, format_text)
                        sheet.write(rowx, 2, line.initial_balance, format_float)
                        sheet.write(rowx, 3, line.end_balance, format_float)
                    rowx += 1
        return sheet, rowx

    # Энэ функцыг l10n_mn_account_analytic_report модуль дотор override хийсэн
    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Cashflow Sheet')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_title_small = book.add_format(ReportExcelCellStyles.format_title_small)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_bold_text = book.add_format(ReportExcelCellStyles.format_content_bold_left)
        format_content_bold_float = book.add_format(ReportExcelCellStyles.format_content_bold_float)

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('cashflow_sheet'), form_title=file_name, date_to=self.date_to, date_from=self.date_from).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_portrait()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 1

        sheet, colx_number = self.get_sheet(sheet)
        # create company
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s' % (_('Company'), self.company_id and self.company_id.name or ''), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 0, rowx + 1, colx_number, report_name.upper(), format_name)
        rowx += 2
        # create duration
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s - %s' % (_('Duration'), self.date_from, self.date_to), format_filter)
        rowx += 1
        # create date
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s' % (_('Created On'), time.strftime('%Y-%m-%d')), format_filter)
        rowx += 2
        # create header
        str_start = (datetime.strptime(self.date_from, '%Y-%m-%d') - timedelta(1))
        str_start = _('%s year %s month %s day') % (str_start.strftime('%Y'), str_start.strftime('%m'), str_start.strftime('%d'))
        str_stop = datetime.strptime(self.date_to, '%Y-%m-%d')
        str_stop = _('%s year %s month %s day') % (str_stop.strftime('%Y'), str_stop.strftime('%m'), str_stop.strftime('%d'))
        sheet = self.get_header(sheet, rowx, format_title, format_title_small, str_start, str_stop)
        rowx += 2
        if self.is_standart:
            if len(self.line_ids) > 0:
                # Баланс тайлангийн мөрийг зурах
                for line in self.line_ids:
                    format_text = format_content_text
                    format_float = format_content_float
                    format_bold_text = format_content_bold_text
                    format_bold_float = format_content_bold_float
                    sheet, rowx = self.get_value(sheet, rowx, format_text, format_float, format_bold_text,
                                                 format_bold_float, line)
        else:
            line = False
            format_text = format_content_text
            format_float = format_content_float
            format_bold_text = format_content_bold_text
            format_bold_float = format_content_bold_float
            sheet, rowx = self.get_value(sheet, rowx, format_text, format_float, format_bold_text,
                                         format_bold_float, line)
        rowx += 2
        sheet.merge_range(rowx, 1, rowx, 2, '%s: ........................................... (                          )' % _('Executive Director'), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 1, rowx, 2, '%s: ........................................... (                          )' % _('General Accountant'), format_filter)

        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report()