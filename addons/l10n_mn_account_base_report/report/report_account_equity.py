# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from operator import itemgetter
from odoo.exceptions import UserError
from odoo import api, models, _
from openerp.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles

class AccountEquityChangeReport(models.Model):
    _inherit = "account.equity.change.report"
    
    @api.multi
    def get_sheet(self, sheet):
        # compute column
        colx_number = 9
        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 43)
        sheet.set_column('C:J', 15)
        return sheet, colx_number

    @api.multi
    def get_title(self, sheet, rowx, colx_number, report_name, format_name, format_filter):
        #Тайлангийн гарчиг зурах
        
        # create name
        sheet.merge_range(rowx, 0, rowx, colx_number, '%s: %s' % (_('Company Name'), self.company_id.name), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 0, rowx+1, colx_number, report_name.upper(), format_name)
        rowx += 2
        # create duration
        sheet.merge_range(rowx, 0, rowx, colx_number, '%s: %s - %s' % (_('Duration Date'), self.date_from, self.date_to), format_filter)
        rowx += 1
        # create date
        sheet.merge_range(rowx, 0, rowx, colx_number, '%s: %s' % (_('Created Date'), time.strftime('%Y-%m-%d')), format_filter)
        rowx += 2
        return sheet, rowx

    @api.multi
    def get_header(self, sheet, rowx, format_title):
        # Тайлангийн хүснэгтийн толгой зурах
        sheet.set_row(rowx, 40)
        sheet.write(rowx, 0, _('№'), format_title)
        sheet.write(rowx, 1, _('Indication'), format_title)
        sheet.write(rowx, 2, _('Equity'), format_title)
        sheet.write(rowx, 3, _('Treasury Stock'), format_title)
        sheet.write(rowx, 4, _('Additional Paid-in Capital'), format_title)
        sheet.write(rowx, 5, _('Revaluation Addition'), format_title)
        sheet.write(rowx, 6, _('Foreign Currency Conversion Inventory'), format_title)
        sheet.write(rowx, 7, _('Stockholders Equity Remainder'), format_title)
        sheet.write(rowx, 8, _('Retained Earnings'), format_title)
        sheet.write(rowx, 9, _('Total Amount'), format_title)
        return sheet

    @api.multi
    def get_footer(self, sheet, rowx, col, text, format_name):
        #Тайлангийн хөл зурах
        sheet.merge_range(rowx, 0, rowx, col, '%s: ........................................... (                          )' % _(text), format_name)
        return sheet

    @api.multi
    def get_value(self, sheet, rowx, line_number, format_number,format_text, format_float, line):
        # Тайлангийн мөр зурах
        
        sheet.write(rowx, 0, line_number, format_number)
        sheet.write(rowx, 1, line.name, format_text)
        sheet.write(rowx, 2, line.equity_amount, format_float)
        sheet.write(rowx, 3, line.treasury_stock_amount, format_float)
        sheet.write(rowx, 4, line.additional_capital_amount, format_float)
        sheet.write(rowx, 5, line.revaluation_addition_amount, format_float)
        sheet.write(rowx, 6, line.foreign_currency_conversion_amount, format_float)
        sheet.write(rowx, 7, line.stockholders_equity_remainder_amount, format_float)
        sheet.write(rowx, 8, line.accumulated_profit_amount, format_float)
        sheet.write(rowx, 9, line.total_amount, format_float)
        rowx += 1
        return sheet, rowx
    
    @api.multi
    def export_report(self):
        #create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
 
        #create name
        report_name = _('Equity Changes Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
         
        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_title_left = book.add_format(ReportExcelCellStyles.format_title_left)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_center = book.add_format(ReportExcelCellStyles.format_content_center)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_bold_text = book.add_format(ReportExcelCellStyles.format_content_bold_text)
        format_content_bold_left = book.add_format(ReportExcelCellStyles.format_content_bold_left)
        format_content_bold_float = book.add_format(ReportExcelCellStyles.format_content_bold_float)
        format_filter_center = book.add_format(ReportExcelCellStyles.format_filter_center)
        format_title_small = book.add_format(ReportExcelCellStyles.format_title_small)
        format_title_float = book.add_format(ReportExcelCellStyles.format_title_float)
 
        seq = 1
        data_dict = {}
         
        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('equity_changes'), form_title=file_name, date_to=self.date_to, date_from=self.date_from).create({})
 
        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 0
 
        # compute column
        sheet, colx_number = self.get_sheet(sheet)
        
        sheet, rowx = self.get_title(sheet, rowx, colx_number,report_name, format_name, format_filter)
        
        # Тайлангийн хүснэгтийн толгойг зурж байна.
        sheet = self.get_header(sheet, rowx, format_title)
 
        rowx += 1
        line_number = 1
        for line in self.line_ids:
            if line_number == 1 or line_number == 3 or line_number == 11:
                format_number = format_content_bold_text
                format_text = format_content_bold_left
                format_float = format_content_bold_float
            elif line_number == 9 or line_number == 17:
                format_number = format_title_small
                format_text = format_title_left
                format_float = format_title_float
            else:
                format_number = format_content_center
                format_text = format_content_text
                format_float = format_content_float
            sheet, rowx = self.get_value(sheet, rowx, line_number, format_number,format_text, format_float, line)
            line_number += 1
             
        rowx += 2     
        sheet = self.get_footer(sheet, rowx, colx_number ,(_('Executive Director')), format_filter_center)   
        rowx += 2
        sheet = self.get_footer(sheet, rowx, colx_number ,(_('General Accountant')), format_filter_center)   
        sheet.hide_gridlines(2)
        book.close()
 
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
         
        # call export function
        return report_excel_output_obj.export_report()