# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from operator import itemgetter
from odoo.exceptions import UserError
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from openerp.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles

class AccountProfitReport(models.Model):
    _inherit = 'account.profit.report'
    
    @api.multi
    def get_sheet(self, sheet):
        # compute column
        colx_number = 3
        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 43)
        sheet.set_column('C:C', 23)
        sheet.set_column('D:D', 23)
        return sheet, colx_number

    @api.multi
    def get_title(self, sheet, rowx, col, report_name, format_name):
        #Тайлангийн гарчиг зурах
        sheet.merge_range(rowx, 0, rowx+1, col, report_name.upper(), format_name)
        return sheet

    @api.multi
    def get_header(self, sheet, rowx, format_title, initial_date, balance_date):
        # Тайлангийн хүснэгтийн толгой зурах
        sheet.merge_range(rowx, 0, rowx+1, 0, _('№'), format_title)
        sheet.merge_range(rowx, 1, rowx+1, 1, _('Indication'), format_title)
        sheet.merge_range(rowx, 2, rowx+1, 2, _(initial_date), format_title)
        sheet.merge_range(rowx, 3, rowx+1, 3, _(balance_date), format_title)
        return sheet

    @api.multi
    def get_footer(self, sheet, rowx, col, text, format_name):
        #Тайлангийн хөл зурах
        sheet.merge_range(rowx, 0, rowx, col, '%s: ........................................... (                          )' % _(text), format_name)
        return sheet

    @api.multi
    def get_value(self, sheet, rowx, format_number,format_text, format_float, line):
        # Тайлангийн мөр зурах
        
        sheet.write(rowx, 0, line.line_number, format_number)
        sheet.write(rowx, 1, line.name, format_text)
        if line.account_report_type == 'account_report' and not line.account_financial_report_id.account_report_id:
            sheet.write(rowx, 2, u'', format_float)
            sheet.write(rowx, 3, u'', format_float)
        else:
            sheet.write(rowx, 2, line.initial_balance, format_float)
            sheet.write(rowx, 3, line.end_balance, format_float)
        rowx += 1
        return sheet, rowx
    
    @api.multi
    def export_report(self):
        #create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
 
        #create name
        report_name = _('Income statement')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
         
        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_center = book.add_format(ReportExcelCellStyles.format_content_center)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_bold_text = book.add_format(ReportExcelCellStyles.format_content_bold_text)
        format_content_bold_left = book.add_format(ReportExcelCellStyles.format_content_bold_left)
        format_content_bold_float = book.add_format(ReportExcelCellStyles.format_content_bold_float)
        format_filter_center = book.add_format(ReportExcelCellStyles.format_filter_center)
 
        seq = 1
        data_dict = {}
         
        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('income_statement'), form_title=file_name, date_to=self.date_to, date_from=self.date_from).create({})
 
        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 0
 
        # compute column
        sheet, colx_number = self.get_sheet(sheet)
         
        # create name
        sheet.merge_range(rowx, 0, rowx, 3, '%s: %s' % (_('Company'), self.company_id.name), format_filter)
        rowx += 1
        sheet = self.get_title(sheet, rowx, colx_number,report_name, format_name)
        rowx += 2
         
        # create duration
        sheet.merge_range(rowx, 0, rowx, 3, '%s: %s - %s' % (_('Duration'), self.date_from, self.date_to), format_filter)
        rowx += 1
         
        # create date
        sheet.merge_range(rowx, 0, rowx, 3, '%s: %s' % (_('Created On'), time.strftime('%Y-%m-%d')), format_filter)
        rowx += 2
        
        date_start = datetime.strptime(self.date_from, "%Y-%m-%d")
        date_end = datetime.strptime(self.date_to, "%Y-%m-%d")
        year = date_start.year -1
        initial_date = str(year) + '-12'+'-31'
        year = date_end.year
        month = date_end.month
        day = date_end.day
        if month < 10:
            month = '-'+'0'+str(month)
        balance_date = str(year) + str(month) +str(-day)
 
        # Тайлангийн хүснэгтийн толгойг зурж байна.
        sheet = self.get_header(sheet, rowx, format_title, initial_date, balance_date)
 
        rowx += 2
        if not self.account_report_id:
            raise UserError(_('Please select a financial report.'))

        parent_numbers = {}
        parent_index = 1
                 
        for line in self.line_ids:
            account = line.account_financial_report_id
            if account.style_overwrite in [1, 2, 3]:
                format_number = format_content_bold_text
                format_text = format_content_bold_left
                format_float = format_content_bold_float
            else:
                format_number = format_content_center
                format_text = format_content_text
                format_float = format_content_float
            sheet, rowx = self.get_value(sheet, rowx, format_number,format_text, format_float, line)
             
        rowx += 2     
        sheet = self.get_footer(sheet, rowx, 3 ,('Executive Director'), format_filter_center)   
        rowx += 2
        sheet = self.get_footer(sheet, rowx, 3 ,('General Accountant'), format_filter_center)   
        sheet.hide_gridlines(2)
        book.close()
 
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
         
        # call export function
        return report_excel_output_obj.export_report()