# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell

from odoo import api, models, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class AccountTransactionBalance(models.Model):
    _inherit = 'account.transaction.balance'

    # Энэ функцыг l10n_mn_account_analytic_report модуль дотор override хийсэн
    @api.multi
    def export_report(self):

        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Transaction Balance')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_group_right = book.add_format(ReportExcelCellStyles.format_group_right)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_group_float_color = book.add_format(ReportExcelCellStyles.format_group_float_color)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_center = book.add_format(ReportExcelCellStyles.format_content_center)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_float_color = book.add_format(ReportExcelCellStyles.format_content_float_fcolor)

        seq = 1
        col = 3
        type_id = False
        total_start_debit = total_start_credit = total_debit = total_credit = total_end_debit = total_end_credit = 0
        total_start_cur_debit = total_start_cur_credit = total_cur_debit = total_cur_credit = total_end_cur_debit = total_end_cur_credit = 0

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('transaction_balance'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 1
        # compute column
        colx_number = 8
        sheet.set_column('A:A', 4)
        sheet.set_column('B:B', 15)
        sheet.set_column('C:C', 30)
        if self.currency_transaction:
            sheet.set_column('D:D', 8)
            sheet.set_column('E:P', 13)
        else:
            sheet.set_column('D:P', 13)


        # create name
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s' % (_('Company'), self.company_id.name), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 0, rowx + 1, colx_number, report_name.upper(), format_name)
        rowx += 2

        # create duration
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s - %s' % (_('Duration'), self.date_from, self.date_to), format_filter)
        rowx += 1

        # create date
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s' % (_('Created On'), time.strftime('%Y-%m-%d')), format_filter)
        rowx += 2

        if not self.currency_transaction:
            # Table title
            sheet.merge_range(rowx, 0, rowx + 1, 0, _('Seq'), format_title)
            sheet.merge_range(rowx, 1, rowx + 1, 1, _('Account code'), format_title)
            sheet.merge_range(rowx, 2, rowx + 1, 2, _('Account name'), format_title)
            sheet.merge_range(rowx, 3, rowx, 4, _('Initial Balance'), format_title)
            sheet.merge_range(rowx, 5, rowx, 6, _('Transaction'), format_title)
            sheet.merge_range(rowx, 7, rowx, 8, _('End Balance'), format_title)
            sheet.write(rowx + 1, 3, _('Debit'), format_title)
            sheet.write(rowx + 1, 4, _('Credit'), format_title)
            sheet.write(rowx + 1, 5, _('Debit'), format_title)
            sheet.write(rowx + 1, 6, _('Credit'), format_title)
            sheet.write(rowx + 1, 7, _('Debit'), format_title)
            sheet.write(rowx + 1, 8, _('Credit'), format_title)
            rowx += 2
        else:
            col += 1
            # Table title
            sheet.merge_range(rowx, 0, rowx + 2, 0, _('Seq'), format_title)
            sheet.merge_range(rowx, 1, rowx + 2, 1, _('Account code'), format_title)
            sheet.merge_range(rowx, 2, rowx + 2, 2, _('Account name'), format_title)
            sheet.merge_range(rowx, 3, rowx + 2, 3, _('Account currency'), format_title)
            sheet.merge_range(rowx, 4, rowx, 7, _('Initial Balance'), format_title)
            sheet.merge_range(rowx + 1, 4, rowx + 1, 5, _('Debit'), format_title)
            sheet.merge_range(rowx + 1, 6, rowx + 1, 7, _('Credit'), format_title)
            sheet.merge_range(rowx, 8, rowx, 11, _('Transaction'), format_title)
            sheet.merge_range(rowx + 1, 8, rowx + 1, 9, _('Debit'), format_title)
            sheet.merge_range(rowx + 1, 10, rowx + 1, 11, _('Credit'), format_title)
            sheet.merge_range(rowx, 12, rowx, 15, _('End Balance'), format_title)
            sheet.merge_range(rowx + 1, 12, rowx + 1, 13, _('Debit'), format_title)
            sheet.merge_range(rowx + 1, 14, rowx + 1, 15, _('Credit'), format_title)
            sheet.write(rowx + 2, 4, _('By Currency'), format_title)
            sheet.write(rowx + 2, 5, _('By MNT'), format_title)
            sheet.write(rowx + 2, 6, _('By Currency'), format_title)
            sheet.write(rowx + 2, 7, _('By MNT'), format_title)
            sheet.write(rowx + 2, 8, _('By Currency'), format_title)
            sheet.write(rowx + 2, 9, _('By MNT'), format_title)
            sheet.write(rowx + 2, 10, _('By Currency'), format_title)
            sheet.write(rowx + 2, 11, _('By MNT'), format_title)
            sheet.write(rowx + 2, 12, _('By Currency'), format_title)
            sheet.write(rowx + 2, 13, _('By MNT'), format_title)
            sheet.write(rowx + 2, 14, _('By Currency'), format_title)
            sheet.write(rowx + 2, 15, _('By MNT'), format_title)
            rowx += 3
        row = rowx
        if not self.currency_transaction and self.line_ids:
            # Валютаарх дүнгүй гүйлгээ балансын мөрийг зурах
            for line in self.line_ids:
                if self.group_account:
                    total_start_debit += line.initial_debit
                    total_start_credit += line.initial_credit
                    total_debit += line.debit
                    total_credit += line.credit
                    total_end_debit += line.end_debit
                    total_end_credit += line.end_credit
                    if not type_id:
                        sheet.merge_range(rowx, 0, rowx, 8, u'[%s] - %s' %(line.account_type_id.code or '', line.account_type_id.name or ''), format_filter)
                        rowx += 1
                        row = rowx
                    elif type_id != line.account_type_id.id:
                        # Table subtotal
                        sheet.merge_range(rowx, 0, rowx, col - 1, _('Subtotal'), format_group_right)
                        sheet.write_formula(rowx, col, '{=SUM(' + xl_rowcol_to_cell(row, col) + ':' + xl_rowcol_to_cell(rowx - 1, col) + ')}', format_group_float)
                        sheet.write_formula(rowx, col + 1, '{=SUM(' + xl_rowcol_to_cell(row, col + 1) + ':' + xl_rowcol_to_cell(rowx - 1, col + 1) + ')}', format_group_float)
                        sheet.write_formula(rowx, col + 2,'{=SUM(' + xl_rowcol_to_cell(row, col + 2) + ':' + xl_rowcol_to_cell(rowx - 1, col + 2) + ')}', format_group_float)
                        sheet.write_formula(rowx, col + 3, '{=SUM(' + xl_rowcol_to_cell(row, col + 3) + ':' + xl_rowcol_to_cell(rowx - 1, col + 3) + ')}', format_group_float)
                        sheet.write_formula(rowx, col + 4,'{=SUM(' + xl_rowcol_to_cell(row, col + 4) + ':' + xl_rowcol_to_cell(rowx - 1, col + 4) + ')}', format_group_float)
                        sheet.write_formula(rowx, col + 5,'{=SUM(' + xl_rowcol_to_cell(row, col + 5) + ':' + xl_rowcol_to_cell(rowx - 1, col + 5) + ')}', format_group_float)
                        sheet.merge_range(rowx + 1, 0, rowx + 1, 8, u'[%s] - %s' %(line.account_type_id.code or '', line.account_type_id.name or ''), format_filter)
                        rowx += 2
                        row = rowx
                        seq = 1
                    type_id = line.account_type_id.id
                # Table row
                sheet.write(rowx, 0, seq, format_content_number)
                sheet.write(rowx, 1, line.code, format_content_text)
                sheet.write(rowx, 2, line.name, format_content_text)
                sheet.write(rowx, 3, line.initial_debit, format_content_float)
                sheet.write(rowx, 4, line.initial_credit, format_content_float)
                sheet.write(rowx, 5, line.debit, format_content_float)
                sheet.write(rowx, 6, line.credit, format_content_float)
                sheet.write(rowx, 7, line.end_debit, format_content_float)
                sheet.write(rowx, 8, line.end_credit, format_content_float)
                rowx += 1
                seq += 1
            if self.group_account:
                # Table subtotal
                sheet.merge_range(rowx, 0, rowx, col - 1, _('Subtotal'), format_group_right)
                sheet.write_formula(rowx, col, '{=SUM(' + xl_rowcol_to_cell(row, col) + ':' + xl_rowcol_to_cell(rowx - 1, col) + ')}', format_group_float)
                sheet.write_formula(rowx, col + 1, '{=SUM(' + xl_rowcol_to_cell(row, col + 1) + ':' + xl_rowcol_to_cell(rowx - 1, col + 1) + ')}', format_group_float)
                sheet.write_formula(rowx, col + 2, '{=SUM(' + xl_rowcol_to_cell(row, col + 2) + ':' + xl_rowcol_to_cell(rowx - 1, col + 2) + ')}', format_group_float)
                sheet.write_formula(rowx, col + 3,'{=SUM(' + xl_rowcol_to_cell(row, col + 3) + ':' + xl_rowcol_to_cell(rowx - 1, col + 3) + ')}', format_group_float)
                sheet.write_formula(rowx, col + 4, '{=SUM(' + xl_rowcol_to_cell(row, col + 4) + ':' + xl_rowcol_to_cell(rowx - 1, col + 4) + ')}', format_group_float)
                sheet.write_formula(rowx, col + 5, '{=SUM(' + xl_rowcol_to_cell(row, col + 5) + ':' + xl_rowcol_to_cell(rowx - 1, col + 5) + ')}', format_group_float)
            rowx += 1
        elif self.currency_transaction and self.currency_line_ids:
            # Валютаарх дүнтэй гүйлгээ балансын мөрийг зурах
            for line in self.currency_line_ids:
                if self.group_account:
                    total_start_debit += line.initial_debit
                    total_start_credit += line.initial_credit
                    total_debit += line.debit
                    total_credit += line.credit
                    total_end_debit += line.end_debit
                    total_end_credit += line.end_credit
                    total_start_cur_debit += line.initial_currency_debit
                    total_start_cur_credit += line.initial_currency_credit
                    total_cur_debit += line.currency_debit
                    total_cur_credit += line.currency_credit
                    total_end_cur_debit += line.end_currency_debit
                    total_end_cur_credit += line.end_currency_credit
                    if not type_id:
                        sheet.merge_range(rowx, 0, rowx, 14, u'[%s] - %s' %(line.account_type_id.code or '', line.account_type_id.name or ''), format_filter)
                        rowx += 1
                        row = rowx
                    elif type_id != line.account_type_id.id:
                        # Table subtotal
                        sheet.merge_range(rowx, 0, rowx, col - 1, _('Subtotal'), format_group_right)
                        sheet.write_formula(rowx, col, '{=SUM(' + xl_rowcol_to_cell(row, col) + ':' + xl_rowcol_to_cell(rowx - 1, col) + ')}', format_group_float_color)
                        sheet.write_formula(rowx, col + 1, '{=SUM(' + xl_rowcol_to_cell(row, col + 1) + ':' + xl_rowcol_to_cell(rowx - 1, col + 1) + ')}', format_group_float)
                        sheet.write_formula(rowx, col + 2,'{=SUM(' + xl_rowcol_to_cell(row, col + 2) + ':' + xl_rowcol_to_cell(rowx - 1, col + 2) + ')}', format_group_float_color)
                        sheet.write_formula(rowx, col + 3, '{=SUM(' + xl_rowcol_to_cell(row, col + 3) + ':' + xl_rowcol_to_cell(rowx - 1, col + 3) + ')}', format_group_float)
                        sheet.write_formula(rowx, col + 4,'{=SUM(' + xl_rowcol_to_cell(row, col + 4) + ':' + xl_rowcol_to_cell(rowx - 1, col + 4) + ')}', format_group_float_color)
                        sheet.write_formula(rowx, col + 5,'{=SUM(' + xl_rowcol_to_cell(row, col + 5) + ':' + xl_rowcol_to_cell(rowx - 1, col + 5) + ')}', format_group_float)
                        sheet.write_formula(rowx, col + 6, '{=SUM(' + xl_rowcol_to_cell(row, col + 6) + ':' + xl_rowcol_to_cell(rowx - 1, col + 6) + ')}', format_group_float_color)
                        sheet.write_formula(rowx, col + 7, '{=SUM(' + xl_rowcol_to_cell(row, col + 7) + ':' + xl_rowcol_to_cell(rowx - 1, col + 7) + ')}', format_group_float)
                        sheet.write_formula(rowx, col + 8, '{=SUM(' + xl_rowcol_to_cell(row, col + 8) + ':' + xl_rowcol_to_cell(rowx - 1, col + 8) + ')}', format_group_float_color)
                        sheet.write_formula(rowx, col + 9, '{=SUM(' + xl_rowcol_to_cell(row, col + 9) + ':' + xl_rowcol_to_cell(rowx - 1, col + 9) + ')}', format_group_float)
                        sheet.write_formula(rowx, col + 10, '{=SUM(' + xl_rowcol_to_cell(row, col + 10) + ':' + xl_rowcol_to_cell(rowx - 1, col + 10) + ')}', format_group_float_color)
                        sheet.write_formula(rowx, col + 11, '{=SUM(' + xl_rowcol_to_cell(row, col + 11) + ':' + xl_rowcol_to_cell(rowx - 1, col + 11) + ')}', format_group_float)
                        sheet.merge_range(rowx + 1, 0, rowx + 1, 14, u'[%s] - %s' %(line.account_type_id.code or '', line.account_type_id.name or ''), format_filter)
                        rowx += 2
                        row = rowx
                        seq = 1
                    type_id = line.account_type_id.id
                # Table row
                sheet.write(rowx, 0, seq, format_content_number)
                sheet.write(rowx, 1, line.code, format_content_text)
                sheet.write(rowx, 2, line.name, format_content_text)
                sheet.write(rowx, 3, line.currency_id and line.currency_id.name, format_content_center)
                sheet.write(rowx, 4, line.initial_currency_debit, format_content_float_color)
                sheet.write(rowx, 5, line.initial_debit, format_content_float)
                sheet.write(rowx, 6, line.initial_currency_credit, format_content_float_color)
                sheet.write(rowx, 7, line.initial_credit, format_content_float)
                sheet.write(rowx, 8, line.currency_debit, format_content_float_color)
                sheet.write(rowx, 9, line.debit, format_content_float)
                sheet.write(rowx, 10, line.currency_credit, format_content_float_color)
                sheet.write(rowx, 11, line.credit, format_content_float)
                sheet.write(rowx, 12, line.end_currency_debit, format_content_float_color)
                sheet.write(rowx, 13, line.end_debit, format_content_float)
                sheet.write(rowx, 14, line.end_currency_credit, format_content_float_color)
                sheet.write(rowx, 15, line.end_credit, format_content_float)
                rowx += 1
                seq += 1
            if self.group_account:
                # Table subtotal
                sheet.merge_range(rowx, 0, rowx, col - 1, _('Subtotal'), format_group_right)
                sheet.write_formula(rowx, col, '{=SUM(' + xl_rowcol_to_cell(row, col) + ':' + xl_rowcol_to_cell(rowx - 1, col) + ')}', format_group_float_color)
                sheet.write_formula(rowx, col + 1, '{=SUM(' + xl_rowcol_to_cell(row, col + 1) + ':' + xl_rowcol_to_cell(rowx - 1, col + 1) + ')}', format_group_float)
                sheet.write_formula(rowx, col + 2, '{=SUM(' + xl_rowcol_to_cell(row, col + 2) + ':' + xl_rowcol_to_cell(rowx - 1, col + 2) + ')}', format_group_float_color)
                sheet.write_formula(rowx, col + 3, '{=SUM(' + xl_rowcol_to_cell(row, col + 3) + ':' + xl_rowcol_to_cell(rowx - 1, col + 3) + ')}', format_group_float)
                sheet.write_formula(rowx, col + 4, '{=SUM(' + xl_rowcol_to_cell(row, col + 4) + ':' + xl_rowcol_to_cell(rowx - 1, col + 4) + ')}', format_group_float_color)
                sheet.write_formula(rowx, col + 5, '{=SUM(' + xl_rowcol_to_cell(row, col + 5) + ':' + xl_rowcol_to_cell(rowx - 1, col + 5) + ')}', format_group_float)
                sheet.write_formula(rowx, col + 6, '{=SUM(' + xl_rowcol_to_cell(row, col + 6) + ':' + xl_rowcol_to_cell(rowx - 1, col + 6) + ')}', format_group_float_color)
                sheet.write_formula(rowx, col + 7, '{=SUM(' + xl_rowcol_to_cell(row, col + 7) + ':' + xl_rowcol_to_cell(rowx - 1, col + 7) + ')}', format_group_float)
                sheet.write_formula(rowx, col + 8, '{=SUM(' + xl_rowcol_to_cell(row, col + 8) + ':' + xl_rowcol_to_cell(rowx - 1, col + 8) + ')}', format_group_float_color)
                sheet.write_formula(rowx, col + 9, '{=SUM(' + xl_rowcol_to_cell(row, col + 9) + ':' + xl_rowcol_to_cell(rowx - 1, col + 9) + ')}', format_group_float)
                sheet.write_formula(rowx, col + 10, '{=SUM(' + xl_rowcol_to_cell(row, col + 10) + ':' + xl_rowcol_to_cell(rowx - 1, col + 10) + ')}', format_group_float_color)
                sheet.write_formula(rowx, col + 11, '{=SUM(' + xl_rowcol_to_cell(row, col + 11) + ':' + xl_rowcol_to_cell(rowx - 1, col + 11) + ')}', format_group_float)
                rowx += 1

        if self.group_account:
            # Table total
            if not self.currency_transaction:
                sheet.merge_range(rowx, 0, rowx, col - 1, _('TOTAL'), format_group_right)
                sheet.write(rowx, 3, total_start_debit, format_group_float)
                sheet.write(rowx, 4, total_start_credit, format_group_float)
                sheet.write(rowx, 5, total_debit, format_group_float)
                sheet.write(rowx, 6, total_credit, format_group_float)
                sheet.write(rowx, 7, total_end_debit, format_group_float)
                sheet.write(rowx, 8, total_end_credit, format_group_float)
            else:
                sheet.merge_range(rowx, 0, rowx, col - 1, _('TOTAL'), format_group_right)
                sheet.write(rowx, 4, total_start_cur_debit, format_group_float_color)
                sheet.write(rowx, 5, total_start_debit, format_group_float)
                sheet.write(rowx, 6, total_start_cur_credit, format_group_float_color)
                sheet.write(rowx, 7, total_start_credit, format_group_float)
                sheet.write(rowx, 8, total_cur_debit, format_group_float_color)
                sheet.write(rowx, 9, total_debit, format_group_float)
                sheet.write(rowx, 10, total_cur_credit, format_group_float_color)
                sheet.write(rowx, 11, total_credit, format_group_float)
                sheet.write(rowx, 12, total_end_cur_debit, format_group_float_color)
                sheet.write(rowx, 13, total_end_debit, format_group_float)
                sheet.write(rowx, 14, total_end_cur_credit, format_group_float_color)
                sheet.write(rowx, 15, total_end_credit, format_group_float)
        else:
            if not self.currency_transaction:
                # Table total
                sheet.merge_range(rowx, 0, rowx, col - 1, _('TOTAL'), format_group_right)
                sheet.write_formula(rowx, col, '{=SUM(' + xl_rowcol_to_cell(row, col) + ':' + xl_rowcol_to_cell(rowx - 1, col) + ')}', format_group_float)
                sheet.write_formula(rowx, col + 1, '{=SUM(' + xl_rowcol_to_cell(row, col + 1) + ':' + xl_rowcol_to_cell(rowx - 1, col + 1) + ')}', format_group_float)
                sheet.write_formula(rowx, col + 2, '{=SUM(' + xl_rowcol_to_cell(row, col + 2) + ':' + xl_rowcol_to_cell(rowx - 1, col + 2) + ')}', format_group_float)
                sheet.write_formula(rowx, col + 3, '{=SUM(' + xl_rowcol_to_cell(row, col + 3) + ':' + xl_rowcol_to_cell(rowx - 1, col + 3) + ')}', format_group_float)
                sheet.write_formula(rowx, col + 4, '{=SUM(' + xl_rowcol_to_cell(row, col + 4) + ':' + xl_rowcol_to_cell(rowx - 1, col + 4) + ')}', format_group_float)
                sheet.write_formula(rowx, col + 5, '{=SUM(' + xl_rowcol_to_cell(row, col + 5) + ':' + xl_rowcol_to_cell(rowx - 1, col + 5) + ')}', format_group_float)
            else:
                # Table total
                sheet.merge_range(rowx, 0, rowx, col - 1, _('TOTAL'), format_group_right)
                sheet.write_formula(rowx, col, '{=SUM(' + xl_rowcol_to_cell(row, col) + ':' + xl_rowcol_to_cell(rowx - 1, col) + ')}', format_group_float_color)
                sheet.write_formula(rowx, col + 1, '{=SUM(' + xl_rowcol_to_cell(row, col + 1) + ':' + xl_rowcol_to_cell(rowx - 1, col + 1) + ')}', format_group_float)
                sheet.write_formula(rowx, col + 2, '{=SUM(' + xl_rowcol_to_cell(row, col + 2) + ':' + xl_rowcol_to_cell(rowx - 1, col + 2) + ')}', format_group_float_color)
                sheet.write_formula(rowx, col + 3, '{=SUM(' + xl_rowcol_to_cell(row, col + 3) + ':' + xl_rowcol_to_cell(rowx - 1, col + 3) + ')}', format_group_float)
                sheet.write_formula(rowx, col + 4, '{=SUM(' + xl_rowcol_to_cell(row, col + 4) + ':' + xl_rowcol_to_cell(rowx - 1, col + 4) + ')}', format_group_float_color)
                sheet.write_formula(rowx, col + 5, '{=SUM(' + xl_rowcol_to_cell(row, col + 5) + ':' + xl_rowcol_to_cell(rowx - 1, col + 5) + ')}', format_group_float)
                sheet.write_formula(rowx, col + 6, '{=SUM(' + xl_rowcol_to_cell(row, col + 6) + ':' + xl_rowcol_to_cell(rowx - 1, col + 6) + ')}', format_group_float_color)
                sheet.write_formula(rowx, col + 7, '{=SUM(' + xl_rowcol_to_cell(row, col + 7) + ':' + xl_rowcol_to_cell(rowx - 1, col + 7) + ')}', format_group_float)
                sheet.write_formula(rowx, col + 8, '{=SUM(' + xl_rowcol_to_cell(row, col + 8) + ':' + xl_rowcol_to_cell(rowx - 1, col + 8) + ')}', format_group_float_color)
                sheet.write_formula(rowx, col + 9, '{=SUM(' + xl_rowcol_to_cell(row, col + 9) + ':' + xl_rowcol_to_cell(rowx - 1, col + 9) + ')}', format_group_float)
                sheet.write_formula(rowx, col + 10, '{=SUM(' + xl_rowcol_to_cell(row, col + 10) + ':' + xl_rowcol_to_cell(rowx - 1, col + 10) + ')}', format_group_float_color)
                sheet.write_formula(rowx, col + 11, '{=SUM(' + xl_rowcol_to_cell(row, col + 11) + ':' + xl_rowcol_to_cell(rowx - 1, col + 11) + ')}', format_group_float)

        rowx += 3

        sheet.merge_range(rowx, 2, rowx, 4, '%s: ........................................... (                          )' % _('Executive Director'), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 2, rowx, 4, '%s: ........................................... (                          )' % _('General Accountant'), format_filter)

        book.close()

        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()