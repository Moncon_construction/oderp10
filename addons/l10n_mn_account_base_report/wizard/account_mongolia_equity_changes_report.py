# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
from datetime import date, datetime, timedelta
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from odoo.exceptions import ValidationError, UserError

from odoo import api, fields, models, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles  # @UnresolvedImport


class AccountConfigSettings(models.TransientModel):
    _inherit = 'account.config.settings'
    _description = 'Account settings'

    # Дүрмийн сан
    statutory_fund_account = fields.Many2one('account.account', related='company_id.statutory_fund_account', string="Statutory Fund Account")
    # Халаасны хувьцаа
    treasury_stock_account = fields.Many2one('account.account', related='company_id.treasury_stock_account', string="Treasury Stock Account")
    # Нэмж төлөгдсөн капитал
    additional_capital_account = fields.Many2one('account.account', related='company_id.additional_capital_account', string="Additional Paid-in Capital Account")
    # Дахин үнэлгээний нэмэгдэл
    revaluation_addition_account = fields.Many2one('account.account', related='company_id.revaluation_addition_account', string="Revaluation Addition Account")
    # Гадаад валютын хөрвүүлэлтийн нөөц
    foreign_currency_conversion_account = fields.Many2one('account.account', related='company_id.foreign_currency_conversion_account', string="Foreign Currency Conversion Inventory Account")
    # Эздийн өмчийн бусад хэсэг
    stockholders_equity_remainder_account = fields.Many2one('account.account', related='company_id.stockholders_equity_remainder_account', string="Stockholders Equity Remainder Account")
    # Өмнөх үеийн хуримтлагдсан ашиг
    previous_retained_earnings_account = fields.Many2one('account.account', related='company_id.previous_retained_earnings_account', string="Previously Retained Earnings Account")
    # Тайлант үеийн хуримтлагдсан ашиг
    period_retained_earnings_account = fields.Many2one('account.account', related='company_id.period_retained_earnings_account', string="Period Retained Earnings Account")
    # Ногдол ашгийн зардал
    dividend_expense_account = fields.Many2one('account.account', related='company_id.dividend_expense_account', string="Dividend Expense Account")
    # Залруулгын журнал
    adjustment_journal = fields.Many2one('account.journal', related='company_id.adjustment_journal', string="Adjustment Journal", domain=[('type', '=', 'general')])
    # Өмчид гарсан өөрчлөлтийн журнал
    equity_changes_journal = fields.Many2one('account.journal', related='company_id.equity_changes_journal', string="Equity Changes Journal", domain=[('type', '=', 'general')])


class ResCompany(models.Model):
    _inherit = 'res.company'

    # Дүрмийн сан
    statutory_fund_account = fields.Many2one('account.account', "Statutory Fund Account")
    # Халаасны хувьцаа
    treasury_stock_account = fields.Many2one('account.account', "Treasury Stock Account")
    # Нэмж төлөгдсөн капитал
    additional_capital_account = fields.Many2one('account.account', "Additional Paid-in Capital Account")
    # Дахин үнэлгээний нэмэгдэл
    revaluation_addition_account = fields.Many2one('account.account', "Revaluation Addition Account")
    # Гадаад валютын хөрвүүлэлтийн нөөц
    foreign_currency_conversion_account = fields.Many2one('account.account', "Foreign Currency Conversion Inventory Account")
    # Эздийн өмчийн бусад хэсэг
    stockholders_equity_remainder_account = fields.Many2one('account.account', "Stockholders Equity Remainder Account")
    # Өмнөх үеийн хуримтлагдсан ашиг
    previous_retained_earnings_account = fields.Many2one('account.account', "Previously Retained Earnings Account")
    # Тайлант үеийн хуримтлагдсан ашиг
    period_retained_earnings_account = fields.Many2one('account.account', "Period Retained Earnings Account")
    # Ногдол ашгийн зардал
    dividend_expense_account = fields.Many2one('account.account', "Dividend Expense Account")
    # Залруулгын журнал
    adjustment_journal = fields.Many2one('account.journal', "Adjustment Journal", domain=[('type', '=', 'general')])
    # Өмчид гарсан өөрчлөлтийн журнал
    equity_changes_journal = fields.Many2one('account.journal', "Equity Changes Journal", domain=[('type', '=', 'general')])


class AccountMongolianEquityChangesReport(models.TransientModel):
    """
        Өмчийн өөрчлөлтийн тайлан.
    """
    _name = "account.mongolian.equity.changes.report"
    _description = "Account Mongolian Equity Changes Report"

    @api.model
    def _get_account_report(self):
        reports = []
        if self._context.get('active_id'):
            menu = self.env['ir.ui.menu'].browse(self._context.get('active_id')).name
            reports = self.env['account.financial.report'].search([('name', 'ilike', menu)])
        return reports and reports[0] or False

    company_ids = fields.Many2many('res.company', string='Company', default=lambda self: self.env.user.company_id)
    date_from = fields.Date(string='Start Date', required=True)
    date_to = fields.Date(string='End Date', required=True)
    target_move = fields.Selection([('posted', 'All Posted Entries'),
                                    ('all', 'All Entries'),
                                    ], string='Target Moves', required=True, default='posted')

    # Дүрмийн сан
    statutory_fund_account = fields.Many2one('account.account', string="Statutory Fund Account", default=lambda self: self.env.user.company_id.statutory_fund_account)
    # Халаасны хувьцаа
    treasury_stock_account = fields.Many2one('account.account', string="Treasury Stock Account", default=lambda self: self.env.user.company_id.treasury_stock_account)
    # Нэмж төлөгдсөн капитал
    additional_capital_account = fields.Many2one('account.account', string="Additional Paid-in Capital Account", default=lambda self: self.env.user.company_id.additional_capital_account)
    # Дахин үнэлгээний нэмэгдэл
    revaluation_addition_account = fields.Many2one('account.account', "Revaluation Addition Account", default=lambda self: self.env.user.company_id.revaluation_addition_account)
    # Гадаад валютын хөрвүүлэлтийн нөөц
    foreign_currency_conversion_account = fields.Many2one('account.account', "Foreign Currency Conversion Inventory Account", default=lambda self: self.env.user.company_id.foreign_currency_conversion_account)
    # Эздийн өмчийн бусад хэсэг
    stockholders_equity_remainder_account = fields.Many2one('account.account', "Stockholders Equity Remainder Account", default=lambda self: self.env.user.company_id.stockholders_equity_remainder_account)
    # Өмнөх үеийн хуримтлагдсан ашиг
    previous_retained_earnings_account = fields.Many2one('account.account', "Previously Retained Earnings Account", default=lambda self: self.env.user.company_id.previous_retained_earnings_account)
    # Тайлант үеийн хуримтлагдсан ашиг
    period_retained_earnings_account = fields.Many2one('account.account', "Period Retained Earnings Account", default=lambda self: self.env.user.company_id.period_retained_earnings_account)
    # Ногдол ашгийн зардал
    dividend_expense_account = fields.Many2one('account.account', "Dividend Expense Account", default=lambda self: self.env.user.company_id.dividend_expense_account)
    # Залруулгын журнал
    adjustment_journal = fields.Many2one('account.journal', "Adjustment Journal", domain=[('type', '=', 'general')], default=lambda self: self.env.user.company_id.adjustment_journal)
    # Өмчид гарсан өөрчлөлтийн журнал
    equity_changes_journal = fields.Many2one('account.journal', "Equity Changes Journal", domain=[('type', '=', 'general')], default=lambda self: self.env.user.company_id.equity_changes_journal)

    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Equity Changes Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_title_small = book.add_format(ReportExcelCellStyles.format_title_small)
        format_group_right = book.add_format(ReportExcelCellStyles.format_group_right)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_bold_text = book.add_format(ReportExcelCellStyles.format_content_bold_text)
        format_content_bold_float = book.add_format(ReportExcelCellStyles.format_content_bold_float)

        account_financial_obj = self.env['account.financial.report']
        journal_obj = self.env['account.journal']
        move_line_obj = self.env['account.move.line']

        fyobj = self.env['account.fiscalyear']
        fy_ids = fyobj.search([('date_start', '<=', self.date_from),
                               ('date_stop', '>=', self.date_to)], order='date_start')

        if not fy_ids:
            raise UserError(_('No fiscal year'))
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('equity_changes_sheet'), form_title=file_name, date_to=self.date_to, date_from=self.date_from).create({})
        #         for fy in fy_ids:
        date_from_obj = datetime.strptime(self.date_from, '%Y-%m-%d')
        date_to_obj = datetime.strptime(self.date_to, '%Y-%m-%d')
        # create report object

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 1

        # compute column
        colx_number = 3
        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 25)
        wid = 15
        sheet.set_column('C:C', wid)
        sheet.set_column('D:D', wid)
        sheet.set_column('E:E', wid)
        sheet.set_column('F:F', wid)
        sheet.set_column('G:G', wid)
        sheet.set_column('H:H', wid)
        sheet.set_column('I:I', wid)
        sheet.set_column('J:J', wid)

        company_names = ''
        start = True
        # create name
        for company in self.company_ids:
            if start:
                company_names += company.name
                start = False
            else:
                company_names += ', ' + company.name

        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s' % (_('Company'), company_names), format_filter)

        rowx += 1
        sheet.merge_range(rowx, 0, rowx + 1, colx_number, report_name.upper(), format_name)
        rowx += 2

        # create duration
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s - %s' % (_('Duration'), self.date_from, self.date_to), format_filter)
        rowx += 1

        # create date
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s' % (_('Created On'), time.strftime('%Y-%m-%d')), format_filter)
        rowx += 2

        str_start = (datetime.strptime(self.date_from, '%Y-%m-%d') - timedelta(1))
        str_start = _('%s year %s month %s day') % (str_start.strftime('%Y'), str_start.strftime('%m'), str_start.strftime('%d'))
        str_stop = datetime.strptime(self.date_to, '%Y-%m-%d')
        str_stop = _('%s year %s month %s day') % (str_stop.strftime('%Y'), str_stop.strftime('%m'), str_stop.strftime('%d'))

        # Тайлангийн хүснэгтийн толгойг зурж байна.
        sheet.set_row(rowx, 40)
        sheet.write(rowx, 0, _('Seq'), format_title)
        sheet.write(rowx, 1, _('Indication'), format_title)
        sheet.write(rowx, 2, _('Equity'), format_title)
        sheet.write(rowx, 3, _('Treasury Stock'), format_title)
        sheet.write(rowx, 4, _('Additional Paid-in Capital'), format_title)
        sheet.write(rowx, 5, _('Revaluation Addition'), format_title)
        sheet.write(rowx, 6, _('Foreign Currency Conversion Inventory'), format_title)
        sheet.write(rowx, 7, _('Stockholders Equity Remainder'), format_title)
        sheet.write(rowx, 8, _('Retained Earnings'), format_title)
        sheet.write(rowx, 9, _('Total Amount'), format_title)
        rowx += 1

        value_dict = {}

        for company in self.company_ids:
            statutory_fund_account = company.statutory_fund_account
            treasury_stock_account = company.treasury_stock_account
            additional_capital_account = company.additional_capital_account
            revaluation_addition_account = company.revaluation_addition_account
            foreign_currency_conversion_account = company.foreign_currency_conversion_account
            stockholders_equity_remainder_account = company.stockholders_equity_remainder_account
            previous_retained_earnings_account = company.previous_retained_earnings_account
            period_retained_earnings_account = company.period_retained_earnings_account
            dividend_expense_account = company.dividend_expense_account
            adjustment_journal = company.adjustment_journal
            equity_changes_journal = company.equity_changes_journal

            initial_account_ids = []
            skip_earnings_journals = []
            if self.adjustment_journal:
                skip_earnings_journals.append(self.adjustment_journal.id)
            if self.equity_changes_journal:
                skip_earnings_journals.append(self.equity_changes_journal.id)
            if skip_earnings_journals:
                earning_journals = journal_obj.search([('id', 'not in', skip_earnings_journals)])
            # ==================================================================
            # Эхний үлдэгдэл
            _amount_1 = _amount_2 = _amount_3 = _amount_4 = _amount_5 = _amount_6 = _amount_7 = 0
            if statutory_fund_account:
                initial_bal = move_line_obj.get_initial_balance(company.id, [statutory_fund_account.id], self.date_from, 'posted')
                if initial_bal:
                    _amount_1 = initial_bal[0]['start_credit'] - initial_bal[0]['start_debit']
            if treasury_stock_account:
                initial_bal = move_line_obj.get_initial_balance(company.id, [treasury_stock_account.id], self.date_from, self.target_move)
                if initial_bal:
                    _amount_2 = initial_bal[0]['start_credit'] - initial_bal[0]['start_debit']
            if additional_capital_account:
                initial_bal = move_line_obj.get_initial_balance(company.id, [additional_capital_account.id], self.date_from, self.target_move)
                if initial_bal:
                    _amount_3 = initial_bal[0]['start_credit'] - initial_bal[0]['start_debit']
            if revaluation_addition_account:
                initial_bal = move_line_obj.get_initial_balance(company.id, [revaluation_addition_account.id], self.date_from, self.target_move)
                if initial_bal:
                    _amount_4 = initial_bal[0]['start_credit'] - initial_bal[0]['start_debit']
            if foreign_currency_conversion_account:
                initial_bal = move_line_obj.get_initial_balance(company.id, [foreign_currency_conversion_account.id], self.date_from, self.target_move)
                if initial_bal:
                    _amount_5 = initial_bal[0]['start_credit'] - initial_bal[0]['start_debit']
            if stockholders_equity_remainder_account:
                initial_bal = move_line_obj.get_initial_balance(company.id, [stockholders_equity_remainder_account.id], self.date_from, self.target_move)
                if initial_bal:
                    _amount_6 = initial_bal[0]['start_credit'] - initial_bal[0]['start_debit']
            if previous_retained_earnings_account:
                initial_bal = move_line_obj.get_initial_balance(company.id, [previous_retained_earnings_account.id], self.date_from, self.target_move)
                if initial_bal:
                    _amount_7 = initial_bal[0]['start_credit'] - initial_bal[0]['start_debit']
                    
            #Өмчийн өөрчлөлтийн тайлангийн эхний үлдэгдлийн хуримтлагдсан ашиг нь өмнөх үеийн хуримтлагдсан ашиг + тайлант үеийн хуримтлагдсан ашиг байдаг.
            #Хэрэглэгч тайлант үеийн хуримтлагдсан ашиг дансыг сонгосон эсэхийг шалгаж байна.
            if period_retained_earnings_account:
                #Тайлант үеийн хуримтлагдсан ашиг дансан дээрх утгуудыг авч байна.
                initial_bal = move_line_obj.get_initial_balance(company.id, [period_retained_earnings_account.id], self.date_from, self.target_move)
                if initial_bal:
                    #Өмнөх үеийн хуримтлагдсан ашиг дээр тайлант үеийн хуримтлагдсан ашгийг нэмж байна.
                    _amount_7 += initial_bal[0]['start_credit'] - initial_bal[0]['start_debit']
            _sum_1 = _amount_1 + _amount_2 + _amount_3 + _amount_4 + _amount_5 + _amount_6 + _amount_7

            if '_amount_1' in value_dict:
                value_dict['_amount_1'] += _amount_1
                value_dict['_amount_2'] += _amount_2
                value_dict['_amount_3'] += _amount_3
                value_dict['_amount_4'] += _amount_4
                value_dict['_amount_5'] += _amount_5
                value_dict['_amount_6'] += _amount_6
                value_dict['_amount_7'] += _amount_7
                value_dict['_sum_1'] += _sum_1
            else:
                value_dict['_amount_1'] = _amount_1
                value_dict['_amount_2'] = _amount_2
                value_dict['_amount_3'] = _amount_3
                value_dict['_amount_4'] = _amount_4
                value_dict['_amount_5'] = _amount_5
                value_dict['_amount_6'] = _amount_6
                value_dict['_amount_7'] = _amount_7
                value_dict['_sum_1'] = _sum_1

            _amount_10 = _amount_10_1 = _amount_10_2 = 0
            if period_retained_earnings_account or previous_retained_earnings_account:
                #Залруулгын журналд хийгдсэн тайлант хугацаа болон хуримтлагдсан ашиг дансны дүнгүүд уг тооцоололд нөлөөлнө
                if adjustment_journal.id:
                    per_line = move_line_obj.with_context(journal_ids=[adjustment_journal.id]).get_balance(company.id, [period_retained_earnings_account.id], self.date_from, self.date_to, self.target_move)
                    if per_line:
                        for l in per_line:
                            _amount_10_1 += l['credit'] - l['debit']
                    prev_line = move_line_obj.with_context(journal_ids=[adjustment_journal.id]).get_balance(company.id, [previous_retained_earnings_account.id], self.date_from, self.date_to, self.target_move)
                    if prev_line:
                        for l in prev_line:
                            _amount_10_2 += l['credit'] - l['debit']
                    _amount_10 = _amount_10_1 + _amount_10_2
                    
            if '_amount_10' in value_dict:
                value_dict['_amount_10'] += _amount_10
                value_dict['_amount_7+_amount_10'] += _amount_7 + _amount_10
                value_dict['_sum_1+_amount_10'] += _sum_1 + _amount_10
            else:
                value_dict['_amount_10'] = _amount_10
                value_dict['_amount_7+_amount_10'] = _amount_7 + _amount_10
                value_dict['_sum_1+_amount_10'] = _sum_1 + _amount_10

            _amount_11 = 0
            ret_debit = 0
            ret_credit = 0
            if period_retained_earnings_account:
                if skip_earnings_journals:
                    line = move_line_obj.with_context(journal_ids=earning_journals.ids).get_balance(company.id, [period_retained_earnings_account.id], self.date_from, self.date_to, self.target_move)
                else:
                    line = move_line_obj.get_balance(company.id, [period_retained_earnings_account.id], self.date_from, self.date_to, self.target_move)
                if line:
                    for l in line:
                        ret_debit += l['debit']
                        ret_credit += l['credit']
                        if ret_debit < ret_credit:
                            _amount_11 = ret_credit - ret_debit
                        else:
                            _amount_11 = (ret_debit - ret_credit) * (-1)
            if '_amount_11' in value_dict:
                value_dict['_amount_11'] += _amount_11
            else:
                value_dict['_amount_11'] = _amount_11
            _amount_12 = 0
            if revaluation_addition_account:
                line = move_line_obj.get_balance(company.id, [revaluation_addition_account.id], self.date_from, self.date_to, self.target_move)
                if line:
                    for l in line:
                        _amount_12 += l['credit'] - l['debit']
            _amount_13 = 0
            if foreign_currency_conversion_account:
                line = move_line_obj.get_balance(company.id, [foreign_currency_conversion_account.id], self.date_from, self.date_to, self.target_move)
                if line:
                    for l in line:
                        _amount_13 += l['debit'] - l['credit']
            _amount_14 = 0
            if '_amount_12' in value_dict:
                value_dict['_amount_13'] += _amount_13
                value_dict['_amount_12'] += _amount_12
                value_dict['_amount_12+_amount_13'] += _amount_12 + _amount_13 #TODO
            else:
                value_dict['_amount_13'] = _amount_13
                value_dict['_amount_12'] = _amount_12
                value_dict['_amount_12+_amount_13'] = _amount_12 + _amount_13 #TODO

            _amount_15 = 0
            if statutory_fund_account:
                line = move_line_obj.get_balance(company.id, [statutory_fund_account.id], self.date_from, self.date_to, self.target_move)
                if line:
                    for l in line:
                        _amount_15 += l['credit'] - l['debit']
            _amount_16 = 0
            if treasury_stock_account:
                line = move_line_obj.get_balance(company.id, [treasury_stock_account.id], self.date_from, self.date_to, self.target_move)
                if line:
                    for l in line:
                        _amount_16 += l['debit'] - l['credit']
            _amount_17 = 0
            if additional_capital_account:
                line = move_line_obj.get_balance(company.id, [additional_capital_account.id], self.date_from, self.date_to, self.target_move)
                if line:
                    for l in line:
                        _amount_17 += l['debit'] - l['credit']
            _amount_18 = 0
            if stockholders_equity_remainder_account:
                line = move_line_obj.get_balance(company.id, [stockholders_equity_remainder_account.id], self.date_from, self.date_to, self.target_move)
                if line:
                    for l in line:
                        _amount_18 += l['credit'] - l['debit']

            if '_amount_15' in value_dict:
                value_dict['_amount_15'] += _amount_15
                value_dict['_amount_16'] += _amount_16
                value_dict['_amount_17'] += _amount_17
                value_dict['_amount_18'] += _amount_18
                value_dict['_amount_15+_amount_16+_amount_17+_amount_18'] += _amount_15 + _amount_16 + _amount_17 + _amount_18
            else:
                value_dict['_amount_15'] = _amount_15
                value_dict['_amount_16'] = _amount_16
                value_dict['_amount_17'] = _amount_17
                value_dict['_amount_18'] = _amount_18
                value_dict['_amount_15+_amount_16+_amount_17+_amount_18'] = _amount_15 + _amount_16 + _amount_17 + _amount_18

            _amount_19 = 0
            if dividend_expense_account:
                line = move_line_obj.get_balance(company.id, [dividend_expense_account.id], self.date_from, self.date_to, self.target_move)
                if line:
                    for l in line:
                        _amount_19 += l['credit'] - l['debit']

            _amount_20 = _amount_4 + _amount_12
            if '_amount_19' in value_dict:
                value_dict['_amount_19'] += _amount_19
                value_dict['_amount_20'] += _amount_4 + _amount_12
            else:
                value_dict['_amount_19'] = _amount_19
                value_dict['_amount_20'] = _amount_4 + _amount_12

            tot_1 = _amount_1 + _amount_15
            tot_2 = _amount_2 + _amount_16
            tot_3 = _amount_3 + _amount_17
            tot_5 = _amount_5 + _amount_13
            tot_6 = _amount_6 + _amount_18
            tot_7 = _amount_7 + _amount_10 + _amount_11 + _amount_19

            if 'tot_1' in value_dict:
                value_dict['tot_1'] += tot_1
                value_dict['tot_2'] += tot_2
                value_dict['tot_3'] += tot_3
                value_dict['tot_5'] += tot_5
                value_dict['tot_6'] += tot_6
                value_dict['tot_7'] += tot_7
                value_dict['tot_1+tot_2+tot_3+_amount_20+tot_5+tot_6+tot_7'] += tot_1 + tot_2 + tot_3 + _amount_20 + tot_5 + tot_6 + tot_7
            else:
                value_dict['tot_1'] = tot_1
                value_dict['tot_2'] = tot_2
                value_dict['tot_3'] = tot_3
                value_dict['tot_5'] = tot_5
                value_dict['tot_6'] = tot_6
                value_dict['tot_7'] = tot_7
                value_dict['tot_1+tot_2+tot_3+_amount_20+tot_5+tot_6+tot_7'] = tot_1 + tot_2 + tot_3 + _amount_20 + tot_5 + tot_6 + tot_7

        # ==================================================================
        # Эхний үлдэгдэл
        _amount_1 = value_dict['_amount_1']
        _amount_2 = value_dict['_amount_2']
        _amount_3 = value_dict['_amount_3']
        _amount_4 = value_dict['_amount_4']
        _amount_5 = value_dict['_amount_5']
        _amount_6 = value_dict['_amount_6']
        _amount_7 = value_dict['_amount_7']
        sheet.write(rowx, 0, '1', format_content_bold_text)
        sheet.write(rowx, 1, _('Balance of %s year %s month %s day') % (date_from_obj.strftime('%Y'),
                                                                        date_from_obj.strftime('%m'),
                                                                        date_from_obj.strftime('%d')), format_content_bold_text)

        sheet.write(rowx, 2, _amount_1, format_content_float)
        sheet.write(rowx, 3, _amount_2, format_content_float)
        sheet.write(rowx, 4, _amount_3, format_content_float)
        sheet.write(rowx, 5, _amount_4, format_content_float)
        sheet.write(rowx, 6, _amount_5, format_content_float)
        sheet.write(rowx, 7, _amount_6, format_content_float)
        sheet.write(rowx, 8, _amount_7, format_content_float)
        _sum_1 = value_dict['_sum_1']
        sheet.write(rowx, 9, _sum_1, format_content_float)
        # ==================================================================
        rowx += 1
        _amount_10 = value_dict['_amount_10']
        sheet.write(rowx, 0, '2', format_content_bold_text)
        sheet.write(rowx, 1, _('The effects of changes in accounting policies and correction of errors'), format_content_text)
        sheet.write(rowx, 2, '', format_content_float)
        sheet.write(rowx, 3, '', format_content_float)
        sheet.write(rowx, 4, '', format_content_float)
        sheet.write(rowx, 5, '', format_content_float)
        sheet.write(rowx, 6, '', format_content_float)
        sheet.write(rowx, 7, '', format_content_float)
        sheet.write(rowx, 8, _amount_10, format_content_float)
        sheet.write(rowx, 9, _amount_10, format_content_float)
        # ==================================================================
        rowx += 1
        sheet.write(rowx, 0, '3', format_content_bold_text)
        sheet.write(rowx, 1, _('The corrected balance'), format_content_text)
        sheet.write(rowx, 2, _amount_1, format_content_float)
        sheet.write(rowx, 3, _amount_2, format_content_float)
        sheet.write(rowx, 4, _amount_3, format_content_float)
        sheet.write(rowx, 5, _amount_4, format_content_float)
        sheet.write(rowx, 6, _amount_5, format_content_float)
        sheet.write(rowx, 7, _amount_6, format_content_float)
        sheet.write(rowx, 8, value_dict['_amount_7+_amount_10'], format_content_float)
        sheet.write(rowx, 9, value_dict['_sum_1+_amount_10'], format_content_float)
        # ==================================================================
        rowx += 1
        #Тайлант үеийн цэвэр ашиг (алдагдал)
        _amount_11 = value_dict['_amount_11']
        sheet.write(rowx, 0, '4', format_content_bold_text)
        sheet.write(rowx, 1, _('Net profit (loss) for the period'), format_content_text)
        sheet.write(rowx, 2, '', format_content_float)
        sheet.write(rowx, 3, '', format_content_float)
        sheet.write(rowx, 4, '', format_content_float)
        sheet.write(rowx, 5, '', format_content_float)
        sheet.write(rowx, 6, '', format_content_float)
        sheet.write(rowx, 7, '', format_content_float)
        sheet.write(rowx, 8, _amount_11, format_content_float)
        sheet.write(rowx, 9, _amount_11, format_content_float)
        # ==================================================================
        rowx += 1
        #Бусад дэлгэрэнгүй орлого
        _amount_12 = value_dict['_amount_12']
        _amount_13 = value_dict['_amount_13']

        sheet.write(rowx, 0, '5', format_content_bold_text)
        sheet.write(rowx, 1, _('Other comprehensive income'), format_content_text)
        sheet.write(rowx, 2, '', format_content_float)
        sheet.write(rowx, 3, '', format_content_float)
        sheet.write(rowx, 4, '', format_content_float)
        sheet.write(rowx, 5, '', format_content_float)
        sheet.write(rowx, 6, _amount_13, format_content_float)
        sheet.write(rowx, 7, _amount_14, format_content_float)
        sheet.write(rowx, 8, '', format_content_float)
        sheet.write(rowx, 9, value_dict['_amount_12+_amount_13'], format_content_float)
        # ==================================================================
        rowx += 1
        #Өмчид гарсан өөрчлөлт
        _amount_15 = value_dict['_amount_15']
        _amount_16 = value_dict['_amount_16']
        _amount_17 = value_dict['_amount_17']
        _amount_18 = value_dict['_amount_18']
        sheet.write(rowx, 0, '6', format_content_bold_text)
        sheet.write(rowx, 1, _('Changes in equity'), format_content_text)
        sheet.write(rowx, 2, _amount_15, format_content_float)
        sheet.write(rowx, 3, _amount_16, format_content_float)
        sheet.write(rowx, 4, _amount_17, format_content_float)
        sheet.write(rowx, 5, '', format_content_float)
        sheet.write(rowx, 6, '', format_content_float)
        sheet.write(rowx, 7, _amount_18, format_content_float)
        sheet.write(rowx, 8, '', format_content_float)
        sheet.write(rowx, 9, value_dict['_amount_15+_amount_16+_amount_17+_amount_18'], format_content_float)
        # ==================================================================
        rowx += 1
        _amount_19 = value_dict['_amount_19']
        sheet.write(rowx, 0, '7', format_content_bold_text)
        sheet.write(rowx, 1, _('Dividends declared'), format_content_text)
        sheet.write(rowx, 2, '', format_content_float)
        sheet.write(rowx, 3, '', format_content_float)
        sheet.write(rowx, 4, '', format_content_float)
        sheet.write(rowx, 5, '', format_content_float)
        sheet.write(rowx, 6, '', format_content_float)
        sheet.write(rowx, 7, '', format_content_float)
        sheet.write(rowx, 8, _amount_19, format_content_float)
        sheet.write(rowx, 9, _amount_19, format_content_float)
        # ==================================================================
        rowx += 1
        _amount_20 = value_dict['_amount_20']
        sheet.write(rowx, 0, '8', format_content_bold_text)
        sheet.write(rowx, 1, _('Implemented the revaluation surplus'), format_content_text)
        sheet.write(rowx, 2, '', format_content_float)
        sheet.write(rowx, 3, '', format_content_float)
        sheet.write(rowx, 4, '', format_content_float)
        sheet.write(rowx, 5, _amount_12, format_content_float)
        sheet.write(rowx, 6, '', format_content_float)
        sheet.write(rowx, 7, '', format_content_float)
        sheet.write(rowx, 8, '', format_content_float)
        sheet.write(rowx, 9, _amount_12, format_content_float)
        # ==================================================================
        rowx += 1

        sheet.write(rowx, 0, '9', format_content_bold_text)
        sheet.write(rowx, 1, _('Balance of %s year %s month %s day') % (date_to_obj.strftime('%Y'),
                                                                        date_to_obj.strftime('%m'),
                                                                        date_to_obj.strftime('%d')), format_content_bold_text)
        tot_1 = value_dict['tot_1']
        tot_2 = value_dict['tot_2']
        tot_3 = value_dict['tot_3']
        tot_5 = value_dict['tot_5']
        tot_6 = value_dict['tot_6']
        tot_7 = value_dict['tot_7']

        sheet.write(rowx, 2, tot_1, format_content_float)
        sheet.write(rowx, 3, tot_2, format_content_float)
        sheet.write(rowx, 4, tot_3, format_content_float)
        sheet.write(rowx, 5, _amount_20, format_content_float)
        sheet.write(rowx, 6, tot_5, format_content_float)
        sheet.write(rowx, 7, tot_6, format_content_float)
        sheet.write(rowx, 8, tot_7, format_content_float)
        sheet.write(rowx, 9, tot_1 + tot_2 + tot_3 + _amount_20 + tot_5 + tot_6 + tot_7, format_content_float)

        # Тухайн тайлангийн бүтцэд хамаарах хүүхдүүдийг хайж байна.

        rowx += 2
        sheet.merge_range(rowx, 1, rowx, 2, '%s: ........................................... (                          )' % _('Executive Director'), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 1, rowx, 2, '%s: ........................................... (                          )' % _('General Accountant'), format_filter)

        book.close()

        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()
