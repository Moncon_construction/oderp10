# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from operator import itemgetter
from odoo.exceptions import ValidationError, UserError

from odoo import api, fields, models, _
from openerp.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles

class AccountMongolianProfitReport(models.TransientModel):
    """
        Орлогын дэлгэрэнгүй тайлан
    """
    _name = "account.mongolian.profit.report"
    _description = "Account Mongolian Profit Report"

    @api.model
    def _get_account_report(self):
        reports = []
        if self._context.get('active_id'):
            menu = self.env['ir.ui.menu'].browse(self._context.get('active_id')).name
            reports = self.env['account.financial.report'].search([('name', 'ilike', menu)])
        return reports and reports[0] or False

    companies = fields.Many2many('res.company', 'mongolian_profit_report_res_company_rel', 'report_id', 'company_id', string='Company', default=lambda self: self.env.user.company_id)
    date_from = fields.Date(string='Start Date')
    date_to = fields.Date(string='End Date')
    account_report_id = fields.Many2one('account.financial.report', string='Account Reports', required=True, default=_get_account_report)
    target_move = fields.Selection([('posted', 'All Posted Entries'),
                                    ('all', 'All Entries'),
                                    ], string='Target Moves', required=True, default='posted')

    @api.onchange('date_to')
    def onchange_date_to(self):
        return {'domain': {'companies': [('id','in', self.env.user.company_ids.ids)]}}

    @api.multi
    def export_report(self):
        #create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        #create name
        report_name = _('Income statement')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        
        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_group_right = book.add_format(ReportExcelCellStyles.format_group_right)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_bold_text = book.add_format(ReportExcelCellStyles.format_content_bold_text)
        format_content_bold_float = book.add_format(ReportExcelCellStyles.format_content_bold_float)

        account_financial_obj = self.env['account.financial.report']
        seq = 1
        data_dict = {}
        
        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('income_statement'), form_title=file_name, date_to=self.date_to, date_from=self.date_from).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 1

        # compute column
        colx_number = 2
        sheet.set_column('A:A', 8)
        sheet.set_column('B:B', 50)
        sheet.set_column('C:C', 30)
        sheet.set_column('D:D', 30)
        
        # create name
        names = ''
        for company in self.companies:
            if names:
                names += ', '
            names += company.name
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s' % (_('Company'), names), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 0, rowx+1, colx_number, report_name.upper(), format_name)
        rowx += 2
        
        # create duration
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s - %s' % (_('Duration'), self.date_from, self.date_to), format_filter)
        rowx += 1
        
        # create date
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s' % (_('Created On'), time.strftime('%Y-%m-%d')), format_filter)
        rowx += 2

        # Тайлангийн хүснэгтийн толгойг зурж байна.
        sheet.merge_range(rowx, 0, rowx+1, 0, _('Line Number'), format_title)
        sheet.merge_range(rowx, 1, rowx+1, 1, _('Indication'), format_title)
        sheet.merge_range(rowx, 2, rowx+1, 2, _('Accounting period'), format_title)

        rowx += 2
        if not self.account_report_id:
            raise UserError(_('Please select a financial report.'))

        # Тухайн тайлангийн бүтцэд хамаарах хүүхдүүдийг хайж байна.
        # report_ids = self.account_report_id._get_children_by_order()
        report_ids = self.account_report_id.with_context(date_to=self.date_to, date_from=self.date_from, state=self.target_move, company_id=self.companies.ids)._get_children_by_order()
        
        parent_numbers = {}
        parent_index = 1
                
        first_top = False
        rows = []
               
        for account in report_ids:

            if account.id == self.account_report_id.id:
                continue

            # Тайлангийн мөрийн дугаарыг тооцож байна.
            number = parent_index
            if account.parent_id.id not in parent_numbers:
                parent_numbers[account.id] = [str(number), 0]
                parent_index += 1
            else :
                number = parent_numbers[account.parent_id.id][0] + '.' + str(parent_numbers[account.parent_id.id][1] + 1)
                parent_numbers[account.parent_id.id][1] += 1
                parent_numbers[account.id] = [str(number), 0]
            
            # Үлдэгдлийг авч байна. 
            balance = account.balance

            # Тайлангийн бүтэц дээр утгыг + эсвэл - харагдуулахыг тооцож байна.
            # balance *= account.sign
            
            # Тайлангийн шатлалын дагуу мөрийн дугаарлалтыг олж байна
            number_str = '%s' % number
            name_str = account.name
            if account.style_overwrite == 1 :
                number_str = '%s' % number_str
                name_str = u'%s' % name_str
            elif account.style_overwrite == 2 :
                name_str = u'%s' % name_str
            
            # Тайлангийн бүтэц нь 4 төрөлтэй бөгөөд дэлгэц, тайлангийн утга төрөлтэй бол нийлбэрийг харуулахгүй
            if account.type in ('account_report'):
                if account.style_overwrite in [1,2,3]:
                    sheet.write(rowx, 0, number_str, format_content_bold_text)
                    sheet.write(rowx, 1, u"   %s" % name_str, format_content_bold_text)
                    if not account.account_report_id:
                        sheet.write(rowx, 2, u'', format_content_bold_float)
                    else:
                        sheet.write(rowx, 2, balance, format_content_bold_float)
                else:
                    sheet.write(rowx, 0, number_str, format_content_text)
                    sheet.write(rowx, 1, u"   %s" % name_str, format_content_text)
                    if not account.account_report_id:
                        sheet.write(rowx, 2, u'', format_content_float)
                    else:
                        sheet.write(rowx, 2, balance, format_content_float)                        
            else:
                if account.style_overwrite in [1,2,3]:
                    sheet.write(rowx, 0, number_str, format_content_bold_text)
                    sheet.write(rowx, 1, u"     %s" % name_str, format_content_bold_text)
                    sheet.write(rowx, 2, balance, format_content_bold_float)
                else:
                    sheet.write(rowx, 0, number_str, format_content_text)
                    sheet.write(rowx, 1, u"     %s" % name_str, format_content_text)
                    sheet.write(rowx, 2, balance, format_content_float)

            rowx += 1
            
        rowx += 2        
        sheet.merge_range(rowx, 1, rowx, 2, '%s: ........................................... (                          )' % _('Executive Director'), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 1, rowx, 2, '%s: ........................................... (                          )' % _('General Accountant'), format_filter)
        
        book.close()

        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        
        # call export function
        return report_excel_output_obj.export_report()