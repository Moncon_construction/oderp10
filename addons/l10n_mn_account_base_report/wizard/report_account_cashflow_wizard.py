# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from operator import itemgetter
import xlrd
from odoo import api, fields, models, _
from openerp.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from datetime import date, datetime
from odoo.exceptions import UserError

class ReportAccountCashflow(models.TransientModel):
    """
        Монголын Сангийн Яамнаас баталсан Мөнгөн гүйлгээний тайлан.
    """

    _name = 'report.account.cashflow'
    _description = "Report Account Cashflow"

    def _default_chart_account(self):
        ret = []
        user = self.env.user
        account = self.env['account.cashflow.type'].search([('parent_id','=',False),('company_id','=', user.company_id.id)])
        if self.env['account.cashflow.type'].search([('id', '=', account.id)]):
            ret.append(account.id)
        return (ret and ret[0]) or False
    company_id = fields.Many2one('res.company', string='Company', required=True,
                                 default=lambda self: self.env['res.company']._company_default_get('report.account.transaction.balance'))
    date_from = fields.Date("Start Date", required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    date_to = fields.Date("End Date", required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    currency_transaction = fields.Boolean(string="Currency Transaction", default=False)
    compare_last_year = fields.Boolean(string="Compare Last Year")

    def convert_flow_to_cell(self, flow):
        cells = str()
        if not flow:
            return '0'
        for obj in flow:
            cells += xl_rowcol_to_cell(obj, 2)
            if obj != flow[-1]:
                cells += '+'
        return cells

    @api.multi
    def export_report(self):
        #create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        #create name
        report_name = _('Cashflow')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_group_right = book.add_format(ReportExcelCellStyles.format_group_right)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_group = book.add_format(ReportExcelCellStyles.format_group)
        move_line_obj = self.env['account.move.line']
        account_obj = self.env['account.account'].search([('company_id', '=', self.company_id.id)])
        seq = 1
        data_dict = {}
        cashflow_type = self.env['account.cashflow.type'].search([])
        # Мөнгөн хөрөнгийн дансууд
        cash_accounts = account_obj.search([('company_id','=',self.company_id.id)])
        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('cashflow_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 1
        # compute column
        colx_number = 3
        sheet.set_column('A:A', 4)
        sheet.set_column('B:B', 70)
        sheet.set_column('C:C', 20)
        sheet.set_column('D:D', 20)
        sheet.set_column('E:E', 10)
        sheet.write(rowx, 1, report_name.upper(), format_name)
        rowx += 1
        # create name
        sheet.write(rowx, 0, '%s' % (self.company_id.name), format_filter)
        # create date
        sheet.write(rowx, 2, '%s: %s' % (_('Created On'), time.strftime('%Y-%m-%d')), format_filter)
        rowx += 2
        #Өнгөрсөн онтой харьцуулах
        if self.compare_last_year == True:
            #Одоогийн ажил гүйлгээ биш бол
            if not self.currency_transaction:
                date_to_obj = datetime.strptime(self.date_to, '%Y-%m-%d')
                last_year_date_to_obj = date_to_obj.replace(year=date_to_obj.year - 1)
                date_from_obj = datetime.strptime(self.date_from, '%Y-%m-%d')
                last_year_date_from_obj = date_from_obj.replace(year=date_from_obj.year - 1)
                year = datetime.strptime(self.date_from, '%Y-%m-%d').strftime('%Y')
                month = datetime.strptime(self.date_from, '%Y-%m-%d').strftime('%m')
                day = datetime.strptime(self.date_from, '%Y-%m-%d').strftime('%d')
                year2 = int(datetime.strptime(self.date_to, '%Y-%m-%d').strftime('%Y')) - 1
                month2 = datetime.strptime(self.date_to, '%Y-%m-%d').strftime('%m')
                day2 = datetime.strptime(self.date_to, '%Y-%m-%d').strftime('%d')
                sheet.merge_range(rowx, 0, rowx+1, 0, _('Seq'), format_title)
                sheet.merge_range(rowx, 1, rowx+1, 1, _('Indication'), format_title)
                sheet.merge_range(rowx, 2, rowx+1, 2, _('%s year %s month %s day') % (year2,month2,day2), format_title)
                sheet.merge_range(rowx, 3, rowx+1, 3, _('%s year %s month %s day') % (year2,month2,day2), format_title)
                rowx += 2
            codes = self.code_to_sort()
            #Мөнгөн гүйлгээний үзүүлэлтүүдийг жагсаасан
            rowx = 6
            for cashflow_types in codes:
                counts = cashflow_types.code.split('.')
                if cashflow_types.parent_id:
                    if len(counts) == 1:
                        sheet.write(rowx, 0, cashflow_types.code, format_content_number)
                        sheet.write(rowx, 1, cashflow_types.name, format_group)
                        sheet.write(rowx, 2, '', format_content_float)
                    elif len(counts) == 2:
                        sheet.write(rowx, 0, cashflow_types.code, format_content_number)
                        sheet.write(rowx, 1, cashflow_types.name, format_group)
                        sheet.write(rowx, 2, '', format_content_float)
                    elif len(counts) == 3:
                        sheet.write(rowx, 0, '', format_content_number)
                        sheet.write(rowx, 1, '        %s' % cashflow_types.name, format_content_text)
                        sheet.write(rowx, 2, '', format_content_float)
                rowx += 1
            rowx = 6
            #Мөнгөн гүйлгээний үзүүлэлт тус бүр дээр байгаа мөнгөний нийлбэрийг харуулсан
            account_move_lines = self.env['account.move.line'].search([('date', '>=', self.date_from),('date', '<=', self.date_to)])
            for cashflow_types in codes:
                if cashflow_types.parent_id:
                    money = 0
                    money2 = 0
                    money_currency = 0
                    for account_move_line in account_move_lines:
                        if account_move_line.cashflow_id.id == cashflow_types.id:
                            if (account_move_line.is_cashflow is True):
                                # Зөвхөн валютын мөнгөн гүйлгээний зөрүүг олох үед ашиг алдагдлыг тооцно
                                if account_move_line.company_id.type_of_currency_id.id == account_move_line.cashflow_id.id:
                                    if account_move_line.debit:
                                        money_currency += account_move_line.debit
                                    else:
                                        money_currency -= account_move_line.credit
                                    sheet.write(rowx, 2, money_currency, format_group_float)
                                else:
                                    if account_move_line.debit:
                                        money += account_move_line.debit
                                        sheet.write(rowx, 2, money, format_content_float)
                                    else:
                                        money2 += account_move_line.credit
                                        sheet.write(rowx, 2, money2, format_content_float)
                    rowx += 1
            rowx = 6
            rowy = rowx
            rowz = 1
            inflows = 0
            outflows = 0
            flows = []
            initial = 0
            # Үзүүлэлт бүрийн холбогдох нийлбэрийг авах excel-ийн томъёо бичсэн
            for account_move_line in account_move_lines:
                initial = initial + account_move_line.debit - account_move_line.credit
            for cashflow_types in codes:
                counts = cashflow_types.code.split('.')
                if cashflow_types.parent_id:
                    if len(counts) == 1:
                        if counts[0] == '4':
                            debit_4 = sum(account_move_lines.filtered(lambda l: l.cashflow_id.id == cashflow_types.id and
                                                                      l.debit != 0 and l.user_type_id.type == 'liquidity').mapped('debit'))

                            credit_4 = sum(account_move_lines.filtered(lambda l: l.cashflow_id.id == cashflow_types.id and
                                                                       l.credit != 0 and l.user_type_id.type == 'liquidity').mapped('credit'))
                            flows.append(rowx)
                            sheet.write(rowx, 2, debit_4 - credit_4, format_group_float)
                        elif counts[0] == '5':
                            sheet.write_formula(rowx, 2,'{=(' + self.convert_flow_to_cell(flows) + ')}', format_group_float)
                            flows.append(rowx)
                        elif counts[0] == '6':
                            sheet.write(rowx, 2, initial, format_group_float)
                        elif counts[0] == '7':
                            sheet.write(rowx, 2, initial, format_group_float)
                    elif len(counts) == 2:
                        if counts[0] == '4':
                            flows.append(rowx)
                            debit_4 = sum(account_move_lines.filtered(lambda l: l.cashflow_id.id == cashflow_types.id and
                                                                      l.debit != 0 and l.user_type_id.type == 'liquidity').mapped('debit'))

                            credit_4 = sum(account_move_lines.filtered(lambda l: l.cashflow_id.id == cashflow_types.id and
                                                                       l.credit != 0 and l.user_type_id.type == 'liquidity').mapped('credit'))
                            sheet.write(rowx, 2, debit_4 - credit_4, format_group_float)
                        elif counts[0] == '5' and counts[1] == '1':
                            sheet.write_formula(rowx, 2, '{=(' + self.convert_flow_to_cell(flows) + ')}', format_group_float)
                        elif counts[1] == '3':
                            sheet.write_formula(rowx, 2, '{=(' + self.convert_flow_to_cell(flows) + ')}', format_group_float)
                            flows.append(rowx)
                        elif counts[1] == '1':
                            inflows = rowx
                        elif counts[1] == '2':
                            outflows = rowx
                        rowz = 1
                        rowy = rowx
                    elif len(counts) == 3:
                        sheet.write_formula(rowx - rowz, 2, '{=SUM(' + xl_rowcol_to_cell(rowy + 1, 2) + ':' + xl_rowcol_to_cell(rowx, 2) + ')}', format_group_float)
                        rowz += 1
                rowx += 1
            accounts_ids = []
            # Гүйлгээ баланс дуудах
            account_ids = account_obj.search([]).ids
            for a in account_ids:
                acc = self.env[('account.account')].search([('id', '=', a)])
                acc_type = self.env[('account.account.type')].search([('id', '=', acc.user_type_id.id)])
                if acc_type.type == 'liquidity':
                    accounts_ids.append(acc.id)
            if not accounts_ids:
                raise UserError(_('There is no cash and bank flow.'))
            else:
                c1 = 0
                c2 = 0
                initials = move_line_obj.get_initial_balance(self.company_id.id, accounts_ids, self.date_from, 'posted')
                balances = move_line_obj.get_balance(self.company_id.id, accounts_ids, self.date_from, self.date_to, 'posted')
                fetched = initials + balances
                for f in fetched:
                    if f['account_id'] not in data_dict:
                        data_dict[f['account_id']] = {'code': f['code'],
                                                      'name': f['name'],
                                                      'start_debit': 0,
                                                      'start_credit': 0,
                                                      'cur_start_debit': 0,
                                                      'cur_start_credit': 0,
                                                      'debit': 0,
                                                      'credit': 0,
                                                      'cur_debit': 0,
                                                      'cur_credit': 0,
                                                      'end_debit': 0,
                                                      'end_credit': 0,
                                                      'cur_end_debit': 0,
                                                      'cur_end_credit': 0,}
                    data_dict[f['account_id']]['start_debit'] += f['start_debit']
                    data_dict[f['account_id']]['start_credit'] += f['start_credit']
                    data_dict[f['account_id']]['cur_start_debit'] += f['cur_start_debit']
                    data_dict[f['account_id']]['cur_start_credit'] += f['cur_start_credit']
                    data_dict[f['account_id']]['debit'] += f['debit']
                    data_dict[f['account_id']]['credit'] += f['credit']
                    data_dict[f['account_id']]['cur_debit'] += f['cur_debit']
                    data_dict[f['account_id']]['cur_credit'] += f['cur_credit']
                    data_dict[f['account_id']]['end_debit'] += f['start_debit'] + f['debit']
                    data_dict[f['account_id']]['end_credit'] += f['start_credit'] + f['credit']
                    data_dict[f['account_id']]['cur_end_debit'] += f['cur_start_debit'] + f['cur_debit']
                    data_dict[f['account_id']]['cur_end_credit'] += f['cur_start_credit'] + f['cur_credit']
            balance = 0
            start = 0
            #Гүйлгээ балансын эхний үлдэгдэл эцсийн үлдэгдлийг авах
            for val in sorted(data_dict.values(), key=itemgetter('code')):
                start += val['start_debit'] - val['start_credit']
                balance += val['end_debit'] - val['credit']

            sheet.write(rowx - 2, 2, start, format_group_float)
            sheet.write(rowx - 1, 2, balance, format_group_float)

            sheet.write(rowx + 2, 1, '%s: ........................................... (                          )' % _('Executive Director'), format_filter)
            rowx += 2
            sheet.write(rowx + 2, 1, '%s: ........................................... (                          )' % _('General Accountant'), format_filter)
        else:
            #Одоогийн ажил гүйлгээ биш бол
            if not self.currency_transaction:
                year = datetime.strptime(self.date_from, '%Y-%m-%d').strftime('%Y')
                month = datetime.strptime(self.date_from, '%Y-%m-%d').strftime('%m')
                day = datetime.strptime(self.date_from, '%Y-%m-%d').strftime('%d')
                year2 = datetime.strptime(self.date_to, '%Y-%m-%d').strftime('%Y')
                month2 = datetime.strptime(self.date_to, '%Y-%m-%d').strftime('%m')
                day2 = datetime.strptime(self.date_to, '%Y-%m-%d').strftime('%d')
                sheet.merge_range(rowx, 0, rowx+1, 0, _('Seq'), format_title)
                sheet.merge_range(rowx, 1, rowx+1, 1, _('Indication'), format_title)
                sheet.merge_range(rowx, 2, rowx+1, 2, _('%s year %s month %s day') % (year2,month2,day2), format_title)
                rowx += 2
                codes = self.code_to_sort()
                #Мөнгөн гүйлгээний үзүүлэлтүүдийг жагсаасан
                rowx = 6
                for cashflow_types in codes:
                    counts = cashflow_types.code.split('.')
                    if cashflow_types.parent_id:
                        if len(counts) == 1:
                            sheet.write(rowx, 0, cashflow_types.code, format_content_number)
                            sheet.write(rowx, 1, cashflow_types.name, format_group)
                            sheet.write(rowx, 2, '', format_content_float)
                        elif len(counts) == 2:
                            sheet.write(rowx, 0, cashflow_types.code, format_content_number)
                            sheet.write(rowx, 1, cashflow_types.name, format_group)
                            sheet.write(rowx, 2, '', format_content_float)
                        elif len(counts) == 3:
                            sheet.write(rowx, 0, '', format_content_number)
                            sheet.write(rowx, 1, '        %s' % cashflow_types.name, format_content_text)
                            sheet.write(rowx, 2, '', format_content_float)
                    rowx += 1
                rowx = 6
                #Мөнгөн гүйлгээний үзүүлэлт тус бүр дээр байгаа мөнгөний нийлбэрийг харуулсан
                account_move_lines = self.env['account.move.line'].search([('date', '>=', self.date_from),('date', '<=', self.date_to)])
                for cashflow_types in codes:
                    if cashflow_types.parent_id:
                        money = 0
                        money2 = 0
                        money_currency = 0
                        for account_move_line in account_move_lines:
                            if account_move_line.cashflow_id.id == cashflow_types.id:
                                if (account_move_line.is_cashflow is True):
                                    #Зөвхөн валютын мөнгөн гүйлгээний зөрүүг олох үед ашиг алдагдлыг тооцно
                                    if account_move_line.company_id.type_of_currency_id.id == account_move_line.cashflow_id.id:
                                        if account_move_line.debit:
                                            money_currency += account_move_line.debit
                                        else:
                                            money_currency -= account_move_line.credit
                                        sheet.write(rowx, 2, money_currency, format_group_float)
                                    else:
                                        if account_move_line.debit:
                                            money += account_move_line.debit
                                            sheet.write(rowx, 2, money, format_content_float)
                                        else:
                                            money2 += account_move_line.credit
                                            sheet.write(rowx, 2, money2, format_content_float)
                        rowx += 1
                rowx = 6
                rowy = rowx
                rowz = 1
                inflows = 0
                outflows = 0
                flows = []
                initial = 0
                #Үзүүлэлт бүрийн холбогдох нийлбэрийг авах excel-ийн томъёо бичсэн
                for account_move_line in account_move_lines:
                    initial = initial + account_move_line.debit - account_move_line.credit
                for cashflow_types in codes:
                    counts = cashflow_types.code.split('.')
                    if cashflow_types.parent_id:
                        if len(counts) == 1:
                            if counts[0] == '4':
                                debit_4 = sum(account_move_lines.filtered(lambda l: l.cashflow_id.id == cashflow_types.id and
                                                                          l.debit != 0 and l.user_type_id.type == 'liquidity').mapped('debit'))

                                credit_4 = sum(account_move_lines.filtered(lambda l: l.cashflow_id.id == cashflow_types.id and
                                                                           l.credit != 0 and l.user_type_id.type == 'liquidity').mapped('credit'))
                                flows.append(rowx)
                                sheet.write(rowx, 2, debit_4 - credit_4, format_group_float)
                            elif counts[0] == '5':
                                sheet.write_formula(rowx, 2, '{=(' + self.convert_flow_to_cell(flows) + ')}', format_group_float)
                                flows.append(rowx)
                            elif counts[0] == '6':
                                sheet.write(rowx, 2, initial, format_group_float)
                            elif counts[0] == '7':
                                sheet.write(rowx, 2, initial, format_group_float)
                        elif len(counts) == 2:
                            if counts[0] == '4':
                                debit_4 = sum(account_move_lines.filtered(lambda l: l.cashflow_id.id == cashflow_types.id and
                                                                          l.debit != 0 and l.user_type_id.type == 'liquidity').mapped('debit'))

                                credit_4 = sum(account_move_lines.filtered(lambda l: l.cashflow_id.id == cashflow_types.id and
                                                                           l.credit != 0 and l.user_type_id.type == 'liquidity').mapped('credit'))
                                flows.append(rowx)
                                sheet.write(rowx, 2, debit_4 - credit_4, format_group_float)
                            elif counts[0] == '5' and counts[1] == '1':
                                sheet.write_formula(rowx, 2, '{=(' + self.convert_flow_to_cell(flows) + ')}', format_group_float)
                            elif counts[1] == '3':
                                sheet.write_formula(rowx, 2, '{=(' + xl_rowcol_to_cell(inflows, 2) + '-' + xl_rowcol_to_cell(outflows, 2) + ')}', format_group_float)
                                flows.append(rowx)
                            elif counts[1] == '1':
                                inflows = rowx
                            elif counts[1] == '2':
                                outflows = rowx
                            rowz = 1
                            rowy = rowx
                        elif len(counts) == 3:
                            sheet.write_formula(rowx - rowz, 2, '{=SUM(' + xl_rowcol_to_cell(rowy + 1, 2) + ':' + xl_rowcol_to_cell(rowx, 2) + ')}', format_group_float)
                            rowz += 1
                    rowx += 1
                accounts_ids = []
                #Гүйлгээ балансаас хөрвөх чадвартай гүйлгээ балансыг дуудаж байна
                account_ids = account_obj.search([]).ids
                for a in account_ids:
                    acc = self.env[('account.account')].search([('id', '=', a)])
                    acc_type = self.env[('account.account.type')].search([('id', '=', acc.user_type_id.id)])
                    if acc_type.type == 'liquidity':
                        accounts_ids.append(acc.id)
                if not accounts_ids:
                    raise UserError(_('There is no cash and bank flow.'))
                else:
                    c1 = 0
                    c2 = 0
                    initials = move_line_obj.get_initial_balance(self.company_id.id, accounts_ids, self.date_from, 'posted')
                    balances = move_line_obj.get_balance(self.company_id.id, accounts_ids, self.date_from, self.date_to, 'posted')
                    fetched = initials + balances
                    for f in fetched:
                        if f['account_id'] not in data_dict:
                            data_dict[f['account_id']] = {'code': f['code'],
                                                          'name': f['name'],
                                                          'start_debit': 0,
                                                          'start_credit': 0,
                                                          'cur_start_debit': 0,
                                                          'cur_start_credit': 0,
                                                          'debit': 0,
                                                          'credit': 0,
                                                          'cur_debit': 0,
                                                          'cur_credit': 0,
                                                          'end_debit': 0,
                                                          'end_credit': 0,
                                                          'cur_end_debit': 0,
                                                          'cur_end_credit': 0,}
                        data_dict[f['account_id']]['start_debit'] += f['start_debit']
                        data_dict[f['account_id']]['start_credit'] += f['start_credit']
                        data_dict[f['account_id']]['cur_start_debit'] += f['cur_start_debit']
                        data_dict[f['account_id']]['cur_start_credit'] += f['cur_start_credit']
                        data_dict[f['account_id']]['debit'] += f['debit']
                        data_dict[f['account_id']]['credit'] += f['credit']
                        data_dict[f['account_id']]['cur_debit'] += f['cur_debit']
                        data_dict[f['account_id']]['cur_credit'] += f['cur_credit']
                        data_dict[f['account_id']]['end_debit'] += f['start_debit'] + f['debit']
                        data_dict[f['account_id']]['end_credit'] += f['start_credit'] + f['credit']
                        data_dict[f['account_id']]['cur_end_debit'] += f['cur_start_debit'] + f['cur_debit']
                        data_dict[f['account_id']]['cur_end_credit'] += f['cur_start_credit'] + f['cur_credit']
                    balance = 0
                    start = 0
                    #Гүйлгээ балансын эхний үлдэгдэл эцсийн үлдэгдлийг авах
                    for val in sorted(data_dict.values(), key=itemgetter('code')):
                        start += val['start_debit'] - val['start_credit']
                        balance += val['start_debit'] - val['start_credit'] + val['debit'] - val['credit']
                    sheet.write(rowx - 2, 2, start, format_group_float)
                    sheet.write(rowx - 1, 2, balance, format_group_float)

                    sheet.write(rowx + 2, 1, '%s: ........................................... (                          )' % _('Executive Director'), format_filter)
                    rowx += 2
                    sheet.write(rowx + 2, 1, '%s: ........................................... (                          )' % _('General Accountant'), format_filter)
        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report()

    @api.multi
    def code_to_sort(self):
        # Мөнгөн гүйлгээний модын кодоор нь эрэмбэлэх
        cashflow_type = self.env['account.cashflow.type'].search([('company_id', '=', self.company_id.id)])
        obj_cashflow = []
        cashflow_types = []
        c = 0
        for cashflow in cashflow_type:
            if cashflow.code:
                if not cashflow.code == "x":
                    if not cashflow.code == "1":
                        if not cashflow.code > "7":
                            obj_cashflow.append(cashflow.code)
        obj_cashflow.sort()
        while len(obj_cashflow) != c:
            cashflow_type_search = self.env['account.cashflow.type'].search([('code', '=', obj_cashflow[c]), ('company_id', '=', self.company_id.id)])
            cashflow_types.append(cashflow_type_search)
            c += 1
        return cashflow_types
