# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Partner Bill Balance Act",
    'version': '1.0',
    'depends': ['l10n_mn_account', 'l10n_mn_report'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Account Partner Bill Balance Act',
    'description': """
         Харилцагчийн тооцоо нийлсэн акт хэвлэх
    """,
    'data': [
        'data/bill_balance_act_paperformat.xml',
        'report/print_report_bill_balance_act_view.xml',
        'wizard/report_balance_act_view.xml',
        'views/bill_balance_report_view.xml',
    ]
}
