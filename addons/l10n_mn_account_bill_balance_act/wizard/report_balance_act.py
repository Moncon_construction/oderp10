# -*- coding: utf-8 -*-
from odoo import api, models, fields

class ReportBillBalanceAct(models.TransientModel):
    _name = 'report.bill.balance.act'

    partner_id = fields.Many2one('res.partner', 'Partner', required=True)
    company_id = fields.Many2one('res.company', string='Company', required=True,
                                 default=lambda self: self.env.user.company_id)
    type = fields.Selection([('cash', 'Cash'), 
                             ('not_cash', 'Not cash'), 
                             ('barter', 'Barter'), 
                             ('organizations', 'Between organizations')], "Type", required=True, default='cash')
    date_start = fields.Date('Date start', required=True)
    date_stop = fields.Date('Date stop', required=True)
    account_type = fields.Selection([('receivable', 'Receivable Accounts'),
                                     ('payable', 'Payable Accounts'),
                                     ('receivable_payable', 'Receivable and Payable Accounts'),
                                     ('choose_accounts', 'Choose Accounts'),], string='Account Type', required=True, default='receivable_payable')
    account_ids = fields.Many2many('account.account', string="Accounts")
    write_partner_respondent_job_title = fields.Char('Write partner respondent job title')
    write_partner_respondent_surname = fields.Char('Write partner respondent surname')
    write_partner_respondent_firstname = fields.Char('Write partner respondent first name')
    write_respondent_job_title = fields.Char('Write job title')
    write_respondent_surname = fields.Char('Write surname')
    write_respondent_firstname = fields.Char('Write first name')

    @api.onchange('partner_id')
    def onchange_partner(self):
        self.write_partner_respondent_job_title = self.partner_id.function
        self.write_partner_respondent_firstname = self.partner_id.name

    @api.multi
    def report_action(self):
        account_obj = self.env['account.account']
        if self.account_ids:
            account_ids = self.account_ids.ids
        elif self.account_type == 'receivable':
            account_ids = account_obj.search([('internal_type','=',self.account_type),('company_id','=', self.company_id.id)]).ids
        elif self.account_type == 'payable':
            account_ids = account_obj.search([('internal_type','=',self.account_type),('company_id','=', self.company_id.id)]).ids
        else:
            account_ids = account_obj.search([('internal_type','in',('receivable','payable')),('company_id','=', self.company_id.id)]).ids
        data = {
            'ids': self._ids,
            'company_id': self.company_id.id,
            'partner_id': self.partner_id.id,
            'account_ids': account_ids,
            'type': self.type,
            'account_type': self.account_type,
            'date_start': self.date_start,
            'date_stop': self.date_stop,
            'write_partner_respondent_job_title': self.write_partner_respondent_job_title,
            'write_partner_respondent_surname': self.write_partner_respondent_surname,
            'write_partner_respondent_firstname': self.write_partner_respondent_firstname,
            'write_respondent_job_title': self.write_respondent_job_title,
            'write_respondent_surname': self.write_respondent_surname,
            'write_respondent_firstname': self.write_respondent_firstname,
        }
        result = self.env['report'].get_action([], 'l10n_mn_account_bill_balance_act.bill_balance_act', data)
        return result