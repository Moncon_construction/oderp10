# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Account Budget",
    'version': '1.0',
    'depends': ['account_budget', 'l10n_mn_workflow_config', 'l10n_mn_account_period'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'maintainer': 'chinzorig.o@asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
    Description text
    """,
    'data': [
             'security/security.xml',
             'security/ir.model.access.csv',
             'wizard/action_modify_budget.xml',
             'views/inherited_account_budget_view.xml',
             'views/crossovered_budget_report_menu.xml',
             'report/crossovered_budget_report_view.xml',
             'data/email_cron.xml',
    ],
    'demo': [
    ],
    'images': ['index.png'],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}