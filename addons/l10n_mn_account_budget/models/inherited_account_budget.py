# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.tools import ustr
from odoo.exceptions import UserError


class InheritedAccountBudgetPost(models.Model):
    _inherit = 'account.budget.post'
    _order = 'code'

    type = fields.Selection([
        ('view', 'View'),
        ('income', 'Income'),
        ('expense', 'Expense'),
        ('balance', 'Balance')], string='Type', required=True, default='expense')
    parent_id = fields.Many2one('account.budget.post', 'Parent Budgetary Position')
    child_id = fields.Many2one('account.budget.post', 'Child Budgetary Position')
    code = fields.Char(string='Code', required=True)
    sequence = fields.Integer(string='Sequence', required=True, default=1)

    # Эцэг төсвийн чиглэл эсвэл code талбар өөрчлөгдхөд хүүхдүүдийнх нь code өөрчлөгдөнө
    def recurse_childs(self, node, code):
        node_code = node.code.split('.')
        code = code + '.' + node_code[-1]
        super(InheritedAccountBudgetPost, node).write({'code': code})
        childs = self.env['account.budget.post'].search([('parent_id', '=', node.id)])
        for child in childs:
            self.recurse_childs(child, code)

    @api.onchange('parent_id')
    def onchange_parent_id(self):
        if self.parent_id:
            if self.parent_id.type != 'view':
                raise UserError(_('For parents, type must be view'))
            childs = self.env['account.budget.post'].search([('parent_id', '=', self.parent_id.id)])
            self.code = '%s.%s' % (self.parent_id.code, len(childs) + 1)

    @api.model
    def create(self, vals):
        if not vals['parent_id'] and '.' in vals['code'] and vals['type'] == 'view':
            raise UserError(_('(.) can\'t be used in code field'))
        return super(InheritedAccountBudgetPost, self).create(vals)

    @api.multi
    def write(self, vals):
        if 'code' in vals:
            if 'parent_id' in vals:
                if not vals['parent_id'] and '.' in vals['code']:
                    raise UserError(_('(.) can\'t be used in code field'))
            elif not self.parent_id and '.' in vals['code']:
                raise UserError(_('(.) can\'t be used in code field'))
        if 'parent_id' in vals or 'code' in vals:
            childs = self.env['account.budget.post'].search([('parent_id', '=', self.ids[0])])
            for child in childs:
                self.recurse_childs(child, vals['code'])
        return super(InheritedAccountBudgetPost, self).write(vals)

    @api.multi
    def name_get(self):
        return [(record.id, "[%s] " % record.code + record.name) for record in self]

    @api.multi
    def unlink(self):
        for this in self:
            is_parent = self.env['account.budget.post'].search([('parent_id', '=', this.id)])
            if is_parent:
                raise UserError(_('You can\'t delete parent Budgetary Position'))
        return super(InheritedAccountBudgetPost, self).unlink()


class CrossoveredBudgetLinesPeriod(models.Model):
    _name = 'crossovered.budget.lines.period'

    @api.multi
    def _prac_amt(self):
        result = 0.0
        for period_line in self:
            if period_line.id:
                if period_line.line_id.general_budget_id.account_ids:
                    acc_ids = [x.id for x in period_line.line_id.general_budget_id.account_ids]
                    if not acc_ids:
                        raise UserError(
                            _("Budgetary Position %s has no accounts!") % (period_line.line_id.general_budget_id.name))
                    date_from = period_line.date_from
                    date_to = period_line.date_to
                    if period_line.line_id.analytic_account_id.id:
                        self.env.cr.execute(
                            "SELECT SUM(amount) FROM account_analytic_line WHERE account_id = %s AND (date "
                            "between to_date(%s,'yyyy-mm-dd') AND to_date(%s,'yyyy-mm-dd')) AND "
                            "general_account_id=ANY(%s)",
                            (period_line.line_id.analytic_account_id.id, date_from, date_to, acc_ids,))
                        result = self.env.cr.fetchone()[0]
                    if result is None:
                        result = 0.00
                self.env.cr.execute("UPDATE crossovered_budget_lines_period SET practical_amount1 = %s WHERE id = %s",
                                    (abs(result), period_line.id,))
                period_line.practical_amount = abs(result)

    @api.multi
    def _perc(self):
        for period_line in self:
            if period_line.planned_amount != 0.00:
                period_line.percentage = float((period_line.practical_amount or 0.0) / period_line.planned_amount) * 100
            else:
                period_line.percentage = 0.00

    line_id = fields.Many2one('crossovered.budget.lines', 'Budget Lines', ondelete='cascade')
    date_from = fields.Date('Start Date', required=True)
    date_to = fields.Date('End Date', required=True)
    planned_amount = fields.Float('Planned Amount', required=True)
    practical_amount = fields.Float(compute=_prac_amt, string='Practical Amount')
    practical_amount1 = fields.Float('Inv Practical Amount')
    percentage = fields.Float(compute=_perc, string='Percentage')
    is_edit = fields.Boolean(string='Is Edit')


class InheritedCrossoveredBudgetLines(models.Model):
    _inherit = 'crossovered.budget.lines'

    @api.multi
    def _plan(self):
        for line in self:
            planned_amount = 0
            for period_line in line.period_line_ids:
                planned_amount += period_line.planned_amount
            line.planned_amount = planned_amount

    @api.multi
    def _prac(self):
        for line in self:
            practical_amount = 0
            for period_line in line.period_line_ids:
                practical_amount += period_line.practical_amount
            line.practical_amount = practical_amount

    @api.multi
    def _perc(self):
        for line in self:
            if line.planned_amount != 0.0:
                line.percentage = float((line.practical_amount or 0.0) / line.planned_amount) * 100
            else:
                line.percentage = 0.0

    @api.model
    def _default_edit(self):
        for line in self:
            if line.income_crossovered_budget_id.state == 'draft' or line.expense_crossovered_budget_id.state == 'draft':
                if line.income_crossovered_budget_id.period_selection == 'manual' or line.expense_crossovered_budget_id.period_selection == 'manual':
                    return False
                else:
                    return True
            else:
                return False

    crossovered_budget_id = fields.Many2one('crossovered.budget', 'Budget', ondelete='cascade', index=True,
                                            required=False)
    income_crossovered_budget_id = fields.Many2one('crossovered.budget', 'Budget', ondelete='cascade', index=True,
                                                   required=False)
    expense_crossovered_budget_id = fields.Many2one('crossovered.budget', 'Expense Budget', ondelete='cascade',
                                                    index=True)
    balance_crossovered_budget_id = fields.Many2one('crossovered.budget', 'Balance Budget', ondelete='cascade')
    type = fields.Selection([('income', 'Income'),
                             ('expense', 'Expense'),
                             ('balance', 'Balance')], 'Type', required=True)
    period_line_ids = fields.One2many('crossovered.budget.lines.period', 'line_id', 'Period Lines')
    planned_amount = fields.Float(compute=_plan, string='Planned Amount', digits=0)
    practical_amount = fields.Float(compute=_prac, string='Practical Amount')
    percentage = fields.Float(compute=_perc, string='Percentage', group_operator="avg")
    is_edit = fields.Boolean('Is Edit', default=_default_edit)
    is_email_sent = fields.Boolean(default=False)
    expense_difference_amount = fields.Float(compute='_expense_diff', string='Expense Difference Amount', digits=0)
    state = fields.Selection(string='State', readonly=True, store=True, related='crossovered_budget_id.state')

    @api.model
    def create(self, vals):
        if vals.get('income_crossovered_budget_id'):
            vals['crossovered_budget_id'] = vals['income_crossovered_budget_id']
        elif vals.get('expense_crossovered_budget_id'):
            vals['crossovered_budget_id'] = vals['expense_crossovered_budget_id']
        elif vals.get('balance_crossovered_budget_id'):
            vals['crossovered_budget_id'] = vals['balance_crossovered_budget_id']
        return super(InheritedCrossoveredBudgetLines, self).create(vals)

    @api.one
    def _expense_diff(self):
        self.expense_difference_amount = self.planned_amount - self.practical_amount

    @api.onchange('general_budget_id')
    def onchange_general_budget_id(self):
        if self.general_budget_id.id:
            budget_post = self.env['account.budget.post'].browse(self.general_budget_id.id)
            self.type = budget_post.type
            self.date_from = self.income_crossovered_budget_id.date_from or self.expense_crossovered_budget_id.date_from or self.balance_crossovered_budget_id.date_from
            self.date_to = self.income_crossovered_budget_id.date_to or self.expense_crossovered_budget_id.date_to or self.balance_crossovered_budget_id.date_to
            self.analytic_account_id = self.income_crossovered_budget_id.analytic_account_id.id or self.expense_crossovered_budget_id.analytic_account_id.id or self.balance_crossovered_budget_id.analytic_account_id.id

    @api.onchange('crossovered_budget_id')
    def onchange_crossovered_budget_id(self):
        if self.crossovered_budget_id.id:
            budget_line_cross = self.env['crossovered.budget'].browse(self.crossovered_budget_id.id)
            self.state = budget_line_cross.state
            
    @api.multi
    def create_line_periods(self):
        for line in self:
            period_obj = self.env['account.period']
            period_line_obj = self.env['crossovered.budget.lines.period']
            period_ids_list = []
            state = line.income_crossovered_budget_id.state or line.expense_crossovered_budget_id.state or line.balance_crossovered_budget_id.state
            period_selection = line.income_crossovered_budget_id.period_selection or line.expense_crossovered_budget_id.period_selection or line.balance_crossovered_budget_id.period_selection
            company_id = line.expense_crossovered_budget_id.company_id.id or line.income_crossovered_budget_id.company_id.id or line.balance_crossovered_budget_id.company_id.id
            if state == 'draft':
                if line.period_line_ids:
                    for period_line in line.period_line_ids:
                        if period_line.planned_amount > 0:
                            raise UserError(_("Already created Budget Period Lines!"))
                self.env.cr.execute("DELETE FROM crossovered_budget_lines_period WHERE line_id = %s", (line.id,))
                date_start = datetime.strptime(line.date_from, '%Y-%m-%d')
                period_end_date = datetime.strptime(line.date_from, '%Y-%m-%d')
                date_stop = datetime.strptime(line.date_to, '%Y-%m-%d')
                if period_selection == 'month':
                    first_periods = period_obj.search([('date_start', '<=', date_start), ('date_stop', '>=', date_start), ('company_id', '=', company_id)])
                    last_periods = period_obj.search([('date_start', '<=', date_stop), ('date_stop', '>=', date_stop), ('company_id', '=', company_id)])
                    period_ids = period_obj.search([('date_start', '>=', date_start), ('date_stop', '<=', date_stop), ('company_id', '=', company_id)])
                    if first_periods:
                        for period_id in first_periods:
                            if period_id not in period_ids_list:
                                period_ids_list.append(period_id)
                    if period_ids:
                        for period_id in period_ids:
                            if period_id not in period_ids_list:
                                period_ids_list.append(period_id)
                    if last_periods:
                        for period_id in last_periods:
                            if period_id not in period_ids_list:
                                period_ids_list.append(period_id)
                    if period_ids_list:
                        for period in period_ids_list:
                            res = {'line_id': line.id,
                                   'planned_amount': 0.0,
                                   'date_from': date_start if datetime.strptime(period.date_start,
                                                                                '%Y-%m-%d') < date_start else datetime.strptime(
                                       period.date_start, '%Y-%m-%d'),
                                   'date_to': date_stop if datetime.strptime(period.date_stop,
                                                                             '%Y-%m-%d') > date_stop else datetime.strptime(
                                       period.date_stop, '%Y-%m-%d'),
                                   }
                            period_line_obj = period_line_obj.create(res)
                    else:
                        raise UserError(_("There is no account period!"))
                if period_selection == 'week':
                    if date_start == date_stop:
                        res = {'line_id': line.id,
                               'planned_amount': 0.0,
                               'date_from': date_start,
                               'date_to': date_stop,
                               }
                        period_line_obj = period_line_obj.create(res)
                    else:
                        while (date_start <= date_stop):
                            if (date_stop - date_start).days > 7:
                                loop_day_count = 7
                            else:
                                loop_day_count = (date_stop - date_start).days
                            period_end_date = date_start + relativedelta(days=loop_day_count)
                            res = {'line_id': line.id,
                                   'planned_amount': 0.0,
                                   'date_from': date_start,
                                   'date_to': period_end_date,
                                   }
                            date_start += relativedelta(days=loop_day_count + 1)
                            period_line_obj = period_line_obj.create(res)


class WorkflowHistory(models.Model):
    _inherit = 'workflow.history'

    budget_id = fields.Many2one('crossovered.budget', 'Budget')


class InheritedCrossoveredBudget(models.Model):
    _inherit = 'crossovered.budget'

    @api.multi
    def _income_plan(self):
        for budget in self:
            planned_amount = 0
            for line in budget.crossovered_budget_line:
                planned_amount += line.planned_amount
            budget.income_planned_amount = planned_amount

    @api.multi
    def _income_prac(self):
        for budget in self:
            practical_amount = 0
            for line in budget.crossovered_budget_line:
                practical_amount += line.practical_amount
            budget.income_practical_amount = practical_amount

    @api.multi
    def _income_diff(self):
        for budget in self:
            budget.income_difference_amount = budget.income_planned_amount or 0.0 - budget.income_practical_amount or 0.0

    @api.multi
    def _income_perc(self):
        for budget in self:
            if budget.income_planned_amount != 0.00:
                budget.income_percentage = float(
                    (budget.income_practical_amount or 0.0) * 100 / budget.income_planned_amount)
            else:
                budget.income_percentage = 0.00

    @api.multi
    def _expense_plan(self):
        for budget in self:
            planned_amount = 0
            for line in budget.expense_budget_line:
                planned_amount += line.planned_amount
            budget.expense_planned_amount = planned_amount

    @api.multi
    def _expense_prac(self):
        for budget in self:
            practical_amount = 0
            for line in budget.expense_budget_line:
                practical_amount += line.practical_amount
            budget.expense_practical_amount = practical_amount

    @api.multi
    def _expense_diff(self):
        for budget in self:
            budget.expense_difference_amount = budget.expense_planned_amount - budget.expense_practical_amount

    @api.multi
    def _balance_plan(self):
        for budget in self:
            planned_amount = 0
            for line in budget.balance_budget_line:
                planned_amount += line.planned_amount
            budget.balance_planned_amount = planned_amount

    @api.multi
    def _balance_prac(self):
        for budget in self:
            practical_amount = 0
            for line in budget.balance_budget_line:
                practical_amount += line.practical_amount
            budget.balance_practical_amount = practical_amount

    @api.multi
    def _balance_diff(self):
        for budget in self:
            budget.balance_difference_amount = budget.balance_planned_amount - budget.balance_practical_amount

    @api.multi
    def _expense_perc(self):
        for budget in self:
            if budget.expense_planned_amount != 0.00:
                budget.expense_percentage = float(
                    (budget.expense_practical_amount or 0.0) * 100 / budget.expense_planned_amount)
            else:
                budget.expense_percentage = 0.00

    @api.multi
    def _show_approve_button(self):
        res = {}
        history_obj = self.env['workflow.history']
        for budget in self:
            history = history_obj.search([('budget_id', '=', budget.id), (
                'line_sequence', '=', budget.check_sequence)], limit=1, order='sent_date DESC')
            if history:
                budget.show_approve_button = (budget.state == 'confirm' and self.env.user.id in map(lambda x: x.id, history.user_ids))
            else:
                budget.show_approve_button = False
        return res

    @api.one
    def _compute_emails_sent(self):
        if self.expense_budget_line.filtered(lambda l: not l.is_email_sent and l.expense_difference_amount < 0):
            self.is_expense_email_sent = False
        else:
            self.is_expense_email_sent = True

        if self.balance_budget_line.filtered(lambda l: not l.is_email_sent and l.expense_difference_amount < 0):
            self.is_balance_email_sent = False
        else:
            self.is_balance_email_sent = True

    is_expense_email_sent = fields.Boolean(compute="_compute_emails_sent")
    is_balance_email_sent = fields.Boolean(compute="_compute_emails_sent")
    parent_id = fields.Many2one('crossovered.budget', 'Parent Budget')
    child_id = fields.Many2one('crossovered.budget', 'Child Budget')
    create_child = fields.Boolean('Create child', default=False)
    active = fields.Boolean('Active', default=True)
    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic Account', required=True)
    crossovered_budget_line = fields.One2many('crossovered.budget.lines', 'income_crossovered_budget_id',
                                              'Income Budget Lines',
                                              domain=[('type', '=', 'income')], readonly=True,
                                              states={'draft': [('readonly', False)]})
    expense_budget_line = fields.One2many('crossovered.budget.lines', 'expense_crossovered_budget_id',
                                          'Expense Budget Lines',
                                          domain=[('type', '=', 'expense')], readonly=True,
                                          states={'draft': [('readonly', False)]})
    balance_budget_line = fields.One2many('crossovered.budget.lines', 'balance_crossovered_budget_id',
                                          'Balanced Budget Lines',
                                          domain=[('type', '=', 'balance')], readonly=True,
                                          states={'draft': [('readonly', False)]})
    income_planned_amount = fields.Float(compute='_income_plan', string='Income Planned Amount', digits=0)
    income_practical_amount = fields.Float(compute='_income_prac', string='Income Practical Amount', digits=0)
    income_percentage = fields.Float(compute='_income_perc', string='Income Percentage', group_operator="avg", digits=(4, 2))
    income_difference_amount = fields.Float(compute='_income_diff', string='Income Difference Amount', digits=0)
    expense_planned_amount = fields.Float(compute='_expense_plan', string='Expense Planned Amount', digits=0)
    expense_practical_amount = fields.Float(compute='_expense_prac', string='Expense Practical Amount', digits=0)
    expense_difference_amount = fields.Float(compute='_expense_diff', string='Expense Difference Amount', digits=0)
    expense_percentage = fields.Float(compute='_expense_perc', string='Expense Percentage', group_operator="avg", digits=(4, 2))

    balance_planned_amount = fields.Float(compute='_balance_plan', string='Balance Planned Amount', digits=0)
    balance_practical_amount = fields.Float(compute='_balance_prac', string='Balance Practical Amount', digits=0)
    balance_difference_amount = fields.Float(compute='_balance_diff', string='Balance Difference Amount', digits=0)

    period_selection = fields.Selection([('manual', 'Manual'),
                                         ('week', 'Week'),
                                         ('month', 'Month')], 'Period Type', default='month', required=True)
    workflow_id = fields.Many2one('workflow.config', 'Workflow', required=True,
                                  domain=[('model_id.model', '=', 'crossovered.budget')])
    check_sequence = fields.Integer(string='Workflow Step', default=0)
    check_users = fields.Many2many('res.users', 'account_budget_check_user_rel', 'budget_id', 'user_id', 'Checkers',
                                   readonly=True, copy=False)
    show_approve_button = fields.Boolean(string='Show Approve Button?', compute='_show_approve_button')
    step_is_final = fields.Boolean(string='Final Step', default=False)
    workflow_history_ids = fields.One2many('workflow.history', 'budget_id', string='History', readonly=True)

    @api.multi
    def create_lines(self):
        post_obj = self.env['account.budget.post']
        for budget in self:
            if budget.state == 'draft':
                self.env.cr.execute(
                    "SELECT general_budget_id FROM crossovered_budget_lines WHERE income_crossovered_budget_id = %s",
                    (budget.id,))
                lines = self.env.cr.dictfetchall()
                income_lines = list(set([line['general_budget_id'] for line in lines]))

                self.env.cr.execute(
                    "SELECT general_budget_id FROM crossovered_budget_lines WHERE expense_crossovered_budget_id = %s",
                    (budget.id,))
                lines = self.env.cr.dictfetchall()
                expense_lines = list(set([line['general_budget_id'] for line in lines]))

                self.env.cr.execute(
                    "SELECT general_budget_id FROM crossovered_budget_lines WHERE balance_crossovered_budget_id = %s",
                    (budget.id,))
                lines = self.env.cr.dictfetchall()
                balance_lines = list(set([line['general_budget_id'] for line in lines]))

                post_ids = post_obj.search([])
                for post in post_ids:
                    if post.type in ('income') and post.id not in income_lines:
                        res = {'income_crossovered_budget_id': budget.id,
                               'analytic_account_id': budget.analytic_account_id.id,
                               'date_from': budget.date_from,
                               'date_to': budget.date_to,
                               'general_budget_id': post.id,
                               'type': post.type,
                               }
                        line = self.env['crossovered.budget.lines'].create(res)
                        line.create_line_periods()
                    elif post.type in ('expense') and post.id not in expense_lines:
                        res = {'expense_crossovered_budget_id': budget.id,
                               'analytic_account_id': budget.analytic_account_id.id,
                               'date_from': budget.date_from,
                               'date_to': budget.date_to,
                               'general_budget_id': post.id,
                               'type': post.type,
                               }
                        line = self.env['crossovered.budget.lines'].create(res)
                        line.create_line_periods()
                    elif post.type in ('balance') and post.id not in balance_lines:
                        res = {'balance_crossovered_budget_id': budget.id,
                               'analytic_account_id': budget.analytic_account_id.id,
                               'date_from': budget.date_from,
                               'date_to': budget.date_to,
                               'general_budget_id': post.id,
                               'type': post.type,
                               }
                        line = self.env['crossovered.budget.lines'].create(res)
                        line.create_line_periods()

    def is_email_sent_to_false(self, email_type):
        """
        :param
        email_type: expense, balance гэсэн 2 утга авах ба expense бол зардлын мөрүүдэд balance бол балансын мөрүүдэд нөлөөлнө
        """
        if email_type == 'expense':
            for obj in self:
                for exp_line in obj.expense_budget_line:
                    exp_line.is_email_sent = False
        elif email_type == "balance":
            for obj in self:
                for balance_line in obj.balance_budget_line:
                    balance_line.is_email_sent = False

    @api.multi
    def cron_send_email_owner_expense(self):
        email_from = self.env['mail.message']._get_default_from()
        if not self:
            budgets = self.search([('state', '=', 'validate')])
            # Имэйл илгээсний дараа төсөв хэтрэлт байхгүй болсон зардлын мөрүүдийн is_email_sent талбарыг False болгож байна
            budgets.filtered(lambda b: b.expense_difference_amount > 0 and b.is_expense_email_sent is True).is_email_sent_to_false('expense')
            for budget in budgets.filtered(lambda b: not b.is_expense_email_sent):
                body_budget = ''
                for line in budget.expense_budget_line.filtered(lambda l: l.expense_difference_amount < 0):
                    body_budget += _('''<tr>
                                      <td style="border: 1px solid black; border-collapse: collapse;">%s</td>
                                      <td style="border: 1px solid black; border-collapse: collapse;">%s</td>
                                      <td style="border: 1px solid black; border-collapse: collapse;">%s</td>
                                      <td style="border: 1px solid black; border-collapse: collapse;">%s</td>
                                      </tr>''' % (
                        line.general_budget_id.name, line.planned_amount, line.practical_amount,
                        line.expense_difference_amount))
                    line.is_email_sent = True
                body_html = _('''Hello there,<br/><br/>
                            This email is sent to notify you that %s budget's expense has been exceeded.<br/><br/>
                            Budget expense lines:
                            <table style="border: 1px solid black; border-collapse: collapse;">
                            <tr>
                            <th style="border: 1px solid black; border-collapse: collapse;">Budgetary Position</th>
                            <th style="border: 1px solid black; border-collapse: collapse;">Planned Amount</th>
                            <th style="border: 1px solid black; border-collapse: collapse;">Practical Amount</th>
                            <th style="border: 1px solid black; border-collapse: collapse;">Expense Difference Amount</th>
                            </tr>
                            %s
                            </table>
                            <br/>''') % (budget.name, body_budget)
                values = {
                    'subject': _('Budget Expenditure Statement email'),
                    'body_html': body_html,
                    'email_from': email_from,
                    'reply_to': email_from,
                    'state': 'outgoing',
                    'email_to': budget.creating_user_id.email,
                }
                email_obj = self.env['mail.mail'].create(values)
                if email_obj:
                    email_obj.send()
        else:
            if self.expense_budget_line.filtered(lambda l: l.expense_difference_amount < 0):
                body_budget = ''
                for line in self.expense_budget_line.filtered(lambda l: l.expense_difference_amount < 0):
                    body_budget += _('''<tr>
                                      <td style="border: 1px solid black; border-collapse: collapse;">%s</td>
                                      <td style="border: 1px solid black; border-collapse: collapse;">%s</td>
                                      <td style="border: 1px solid black; border-collapse: collapse;">%s</td>
                                      <td style="border: 1px solid black; border-collapse: collapse;">%s</td>
                                      </tr>''' % (line.general_budget_id.name, line.planned_amount, line.practical_amount,
                                                  line.expense_difference_amount))
                body_html = _('''Hello there,<br/><br/>
                            This email is sent to notify you that %s budget's expense has been exceeded.<br/><br/>
                            Budget expense lines:
                            <table style="border: 1px solid black; border-collapse: collapse;">
                            <tr>
                            <th style="border: 1px solid black; border-collapse: collapse;">Budgetary Position</th>
                            <th style="border: 1px solid black; border-collapse: collapse;">Planned Amount</th>
                            <th style="border: 1px solid black; border-collapse: collapse;">Practical Amount</th>
                            <th style="border: 1px solid black; border-collapse: collapse;">Expense Difference Amount</th>
                            </tr>
                            %s
                            </table>
                            <br/>''') % (self.name, body_budget)
                values = {
                    'subject': _('Budget Expenditure Statement email'),
                    'body_html': body_html,
                    'email_from': email_from,
                    'reply_to': email_from,
                    'state': 'outgoing',
                    'email_to': self.creating_user_id.email,
                }
                email_obj = self.env['mail.mail'].create(values)
                if email_obj:
                    email_obj.send()
                body_budget = ''
            if self.balance_budget_line.filtered(lambda l: l.expense_difference_amount < 0):
                for line in self.balance_budget_line.filtered(lambda l: l.expense_difference_amount < 0):
                    line.is_email_sent = True
                    body_budget += _('''<tr>
                                                  <td style="border: 1px solid black; border-collapse: collapse;">%s</td>
                                                  <td style="border: 1px solid black; border-collapse: collapse;">%s</td>
                                                  <td style="border: 1px solid black; border-collapse: collapse;">%s</td>
                                                  <td style="border: 1px solid black; border-collapse: collapse;">%s</td>
                                                  </tr>''' % (
                        line.general_budget_id.name, line.planned_amount, line.practical_amount,
                        line.expense_difference_amount))
                body_html = _('''Hello there,<br/><br/>
                                        This email is sent to notify you that %s budget's balance has been exceeded.<br/><br/>
                                        Budget expense lines:
                                        <table style="border: 1px solid black; border-collapse: collapse;">
                                        <tr>
                                        <th style="border: 1px solid black; border-collapse: collapse;">Budgetary Position</th>
                                        <th style="border: 1px solid black; border-collapse: collapse;">Planned Amount</th>
                                        <th style="border: 1px solid black; border-collapse: collapse;">Practical Amount</th>
                                        <th style="border: 1px solid black; border-collapse: collapse;">Expense Difference Amount</th>
                                        </tr>
                                        %s
                                        </table>
                                        <br/>''') % (self.name, body_budget)
                values = {
                    'subject': _('Budget balance Expenditure Statement email'),
                    'body_html': body_html,
                    'email_from': email_from,
                    'reply_to': email_from,
                    'state': 'outgoing',
                    'email_to': self.creating_user_id.email,
                }
                email_obj = self.env['mail.mail'].create(values)
                if email_obj:
                    email_obj.send()

    @api.multi
    def cron_send_email_owner_balance(self):
        email_from = self.env['mail.message']._get_default_from()
        budgets = self.search([('state', '=', 'validate')])
        # Имэйл илгээсний дараа төсөв хэтрэлт байхгүй болсон балансын мөрүүдийн is_email_sent талбарыг False болгож байна
        budgets.filtered(
            lambda b: b.expense_difference_amount > 0 and b.is_balance_email_sent is True).is_email_sent_to_false(
            'balance')
        for budget in budgets.filtered(lambda b: not b.is_balance_email_sent):
            body_budget = ''
            for line in budget.balance_budget_line.filtered(lambda l: l.expense_difference_amount < 0):
                line.is_email_sent = True
                body_budget += _('''<tr>
                                  <td style="border: 1px solid black; border-collapse: collapse;">%s</td>
                                  <td style="border: 1px solid black; border-collapse: collapse;">%s</td>
                                  <td style="border: 1px solid black; border-collapse: collapse;">%s</td>
                                  <td style="border: 1px solid black; border-collapse: collapse;">%s</td>
                                  </tr>''' % (line.general_budget_id.name, line.planned_amount, line.practical_amount,
                                              line.expense_difference_amount))
            body_html = _('''Hello there,<br/><br/>
                        This email is sent to notify you that %s budget's balance has been exceeded.<br/><br/>
                        Budget expense lines:
                        <table style="border: 1px solid black; border-collapse: collapse;">
                        <tr>
                        <th style="border: 1px solid black; border-collapse: collapse;">Budgetary Position</th>
                        <th style="border: 1px solid black; border-collapse: collapse;">Planned Amount</th>
                        <th style="border: 1px solid black; border-collapse: collapse;">Practical Amount</th>
                        <th style="border: 1px solid black; border-collapse: collapse;">Expense Difference Amount</th>
                        </tr>
                        %s
                        </table>
                        <br/>''') % (budget.name, body_budget)
            values = {
                'subject': _('Budget Expenditure Statement email'),
                'body_html': body_html,
                'email_from': email_from,
                'reply_to': email_from,
                'state': 'outgoing',
                'email_to': budget.creating_user_id.email,
            }
            email_obj = self.env['mail.mail'].create(values)
            if email_obj:
                email_obj.send()

    @api.multi
    def _create_budget_line_period(self, period_line_ids, line_id=False):
        for period_line in period_line_ids:
            res = {
                'line_id': line_id,
                'date_from': period_line.date_from,
                'date_to': period_line.date_to,
                'planned_amount': period_line.planned_amount,
            }
            self.env['crossovered.budget.lines.period'].create(res)

    @api.multi
    def _create_income_budget_lines(self, budget_id, line_ids):
        for line in line_ids:
            res = {'analytic_account_id': line.analytic_account_id.id,
                   'general_budget_id': line.general_budget_id.id,
                   'date_from': line.date_from,
                   'date_to': line.date_to,
                   'type': line.type,
                   'income_crossovered_budget_id': budget_id
                   }
            budget_line_id = self.env['crossovered.budget.lines'].create(res)
            self._create_budget_line_period(line.period_line_ids, budget_line_id.id)

    @api.multi
    def _create_expense_budget_lines(self, budget_id, line_ids):
        for line in line_ids:
            res = {'analytic_account_id': line.analytic_account_id.id,
                   'general_budget_id': line.general_budget_id.id,
                   'date_from': line.date_from,
                   'date_to': line.date_to,
                   'type': line.type,
                   'expense_crossovered_budget_id': budget_id
                   }
            budget_line_id = self.env['crossovered.budget.lines'].create(res)
            self._create_budget_line_period(line.period_line_ids, budget_line_id.id)

    @api.multi
    def create_modify(self):
        mod_obj = self.env['ir.model.data']

        res = mod_obj.get_object_reference('l10n_mn_account_budget', 'action_modify_crossovered_budget')
        return {
            'name': 'Crossovered Budget Modify',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'modify.crossovered.budget',
            'context': self._context,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
        }

    @api.multi
    def action_budget_confirm(self):
        for obj in self:
            if obj.workflow_id:
                workflow_obj = self.env['workflow.config']
                success, current_sequence = workflow_obj.send('workflow.history', 'budget_id', obj, self.env.user.id)
                is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(obj.workflow_id.id,
                                                                                                 obj, self.env.user.id,
                                                                                                 obj.check_sequence + 1,
                                                                                                 'next')
                if is_next:
                    obj.check_users = [(6, 0, next_user_ids)]
                if success:
                    obj.check_sequence = current_sequence
                    obj.ensure_one()
                    obj.state = 'confirm'
        return True

    @api.multi
    def action_budget_validate(self):
        self.ensure_one()
        workflow_obj = self.env['workflow.config']
        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve(
                'workflow.history', 'budget_id', self, self.env.user.id)
            is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(self.workflow_id.id, self,
                                                                                             self.env.user.id,
                                                                                             self.check_sequence + 1,
                                                                                             'next')
            if is_next:
                self.check_users = [(6, 0, next_user_ids)]
            if success:
                if sub_success:
                    self.state = 'validate'
                    self.step_is_final = True
                else:
                    self.check_sequence = current_sequence
                    self.show_approve_button = True
        return True

    @api.multi
    def action_budget_draft(self):
        self.write({'state': 'draft'})
        if self.workflow_history_ids:
            self.workflow_history_ids.unlink()
            self.check_sequence = 0

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        res = super(InheritedCrossoveredBudget, self).read_group(domain, fields, groupby, offset=0, limit=limit, orderby=orderby, lazy=lazy)
        for line in res:
            if '__domain' in line:
                lines = self.search(line['__domain'])
                income_planned_amount = 0
                income_practical_amount = 0
                income_difference_amount = 0
                expense_planned_amount = 0
                expense_practical_amount = 0
                expense_difference_amount = 0
                balance_planned_amount = 0
                balance_practical_amount = 0
                balance_difference_amount = 0
                if 'income_planned_amount' in fields or \
                        'income_practical_amount' in fields or \
                        'income_difference_amount' in fields or \
                        'expense_planned_amount' in fields or \
                        'expense_practical_amount' in fields or \
                        'expense_difference_amount' in fields or \
                        'balance_planned_amount' in fields or \
                        'balance_practical_amount' in fields or \
                        'balance_difference_amount' in fields:
                    for line2 in lines:
                        income_planned_amount += line2.income_planned_amount
                        income_practical_amount += line2.income_practical_amount
                        income_difference_amount += line2.income_difference_amount
                        expense_planned_amount += line2.expense_planned_amount
                        expense_practical_amount += line2.expense_practical_amount
                        expense_difference_amount += line2.expense_difference_amount
                        balance_planned_amount += line2.balance_planned_amount
                        balance_practical_amount += line2.balance_practical_amount
                        balance_difference_amount += line2.balance_difference_amount
                    if 'income_planned_amount' in fields:
                        line['income_planned_amount'] = income_planned_amount
                    if 'income_practical_amount' in fields:
                        line['income_practical_amount'] = income_practical_amount
                    if 'income_difference_amount' in fields:
                        line['income_difference_amount'] = income_difference_amount
                    if 'expense_planned_amount' in fields:
                        line['expense_planned_amount'] = expense_planned_amount
                    if 'expense_practical_amount' in fields:
                        line['expense_practical_amount'] = expense_practical_amount
                    if 'expense_difference_amount' in fields:
                        line['expense_difference_amount'] = expense_difference_amount
                    if 'balance_planned_amount' in fields:
                        line['balance_planned_amount'] = balance_planned_amount
                    if 'balance_practical_amount' in fields:
                        line['balance_practical_amount'] = balance_practical_amount
                    if 'balance_difference_amount' in fields:
                        line['balance_difference_amount'] = balance_difference_amount
        return res


class InheritedAccountAnalyticAccount(models.Model):
    _inherit = "account.analytic.account"

    budget = fields.Boolean('Create Budget', default=False)
