# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _

class ModifyCrossoveredBudget(models.TransientModel):
    _name = 'modify.crossovered.budget'
    
    name = fields.Char('Modified budget name', required=True)
    budget_id = fields.Many2one('crossovered.budget',index=True, string='Budget')
    
    @api.model
    def default_get(self, fields):
        result = super(ModifyCrossoveredBudget, self).default_get(fields)
        if self._context.get('active_id'):
            budget_obj = self.env['crossovered.budget']
            budget = budget_obj.browse(self._context['active_id'])
        result.update({
                       'budget_id': budget.id
                       })
        return result
    
    @api.multi
    def modify_button(self):
        budget_obj = self.env['crossovered.budget']
        for obj in self:
            budget_vals = {'name': obj.name,
                           'parent_id': obj.budget_id.id,
                           'analytic_account_id': obj.budget_id.analytic_account_id.id,
                           'active': True,
                           'date_from': obj.budget_id.date_from,
                           'date_to': obj.budget_id.date_to,
                           'workflow_id':obj.budget_id.workflow_id.id,
                           'state': 'draft'}
            budget_id = self.env['crossovered.budget'].create(budget_vals)
            budget_obj._create_income_budget_lines(budget_id.id, obj.budget_id.crossovered_budget_line)
            budget_obj._create_expense_budget_lines(budget_id.id, obj.budget_id.expense_budget_line)
            obj.budget_id.write({'create_child': True, 'active': False})

        mod_obj = self.env['ir.model.data']
        res = mod_obj.get_object_reference('l10n_mn_account_budget', 'crossovered_budget_inherit_form_l10n_mn_account_budget')

        res_id = res and res[1] or False,
        return {
            'name': _('Crossovered Budget'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': [res_id],
            'res_model': 'crossovered.budget',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            'res_id': budget_id.id or False,
        }
