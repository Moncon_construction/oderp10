# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
import xlsxwriter

from odoo import api, fields, models, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class CrossoveredBudgetReport(models.TransientModel):
    _name = 'crossovered.budget.report'
    _description = 'Crossovered Budget Report'

    budget_id = fields.Many2one('crossovered.budget', string='Budget', required=True)

    # nested dict-ийн бүх навчнаас роот хүртэлх замуудыг list хэлбэрт оруулан буцаана.
    def keypaths(self, nested):
        for key, value in nested.iteritems():
            if type(value) is dict:
                for subkey, subvalue in self.keypaths(value):
                    yield [key] + subkey, subvalue
            else:
                yield [key], value

    # Өгөгдсөн төслийн чиглэлээс бүх хүүхэд төслийн чиглэлүүдийг мод бүтцэд оруулна.
    def init_tree(self, node):
        heritage = {}
        childs = self.env['account.budget.post'].search([('parent_id', '=', node.id)])
        if not childs: return False
        for child in childs:
            heritage[child.id] = self.init_tree(child)
        return heritage

    # Өгөгдсөн төслийн чиглэлүүдийн кодыг харьцуулж аль нь эхэлж байрлахыг шийднэ.
    def compare_code(self, code1='', code2=''):
        code1 = code1.split('.')
        code2 = code2.split('.')
        if code1[0] == code2[0]:
            for c1, c2 in zip(code1[1:], code2[1:]):

                if c1 > c2:
                    return False
                elif c1 < c2:
                    return True
        else:
            for char1, char2 in zip(code1[0], code2[0]):
                if ord(char1) > ord(char2):
                    return False
                elif ord(char1) < ord(char2):
                    return True
            if len(code1) > len(code2):
                return False
            if len(code1) < len(code2):
                return True

    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Income Budget Report')
        report_name_expense = _('Expense Budget Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_content_left = book.add_format(ReportExcelCellStyles.format_content_left)
        format_content_center = book.add_format(ReportExcelCellStyles.format_content_center)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_bold_left = book.add_format(ReportExcelCellStyles.format_content_bold_left)
        format_content_bold_float = book.add_format(ReportExcelCellStyles.format_content_bold_float)
        format_content_bold_text = book.add_format(ReportExcelCellStyles.format_content_bold_text)

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('crossovered_budget_report'), form_title=file_name).create({})

        # create sheet
        if self.budget_id.crossovered_budget_line:
            sheet = book.add_worksheet(report_name)
            sheet.set_landscape()
            sheet.set_paper(9)  # A4
            sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
            sheet.fit_to_pages(1, 0)
            sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})

        if self.budget_id.expense_budget_line:
            sheet2 = book.add_worksheet(report_name_expense)
            sheet2.set_landscape()
            sheet2.set_paper(9)  # A4
            sheet2.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
            sheet2.fit_to_pages(1, 0)
            sheet2.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})

        chart_data_sheet = book.add_worksheet('sheet_data')

        # Эцэг талбар нь хоосон буюу роот төслийн чиглэлүүдийг авч мод бүтцэд оруулна.
        roots = self.env['account.budget.post'].search([('parent_id', '=', False)])
        heritage = {}
        for root in roots:
            heritage[root.id] = self.init_tree(root)

        style_text = style_float = None
        if self.budget_id.crossovered_budget_line:
            # compute column
            title_row = 4
            rowx = 6
            count = 1

            sheet.set_column('B:B', 20)
            sheet.merge_range(title_row - 3, 2, title_row - 2, 6, u'"%s" төсвийн орлогын төсөв' % (self.budget_id.name), format_title)
            sheet.merge_range(title_row, 0, title_row + 1, 0, u'Код', format_title)
            sheet.merge_range(title_row, 1, title_row + 1, 1, u'Төсвийн Чиглэл', format_title)

            budget_ids = []
            cols = 0

            # Төсвийн мөрүүдээс харгалзах төсвийн чиглэлүүдийг авч list болгоно.
            for bud_line in self.budget_id.crossovered_budget_line:
                budget_ids.append(bud_line.general_budget_id)

            # Төсвийн чиглэлүүдийг код талбараар нь эрэмбэлнэ.
            for passnum in range(len(budget_ids) - 1, 0, -1):
                for i in range(passnum):
                    if not self.compare_code(budget_ids[i].code, budget_ids[i + 1].code):
                        temp = budget_ids[i]
                        budget_ids[i] = budget_ids[i + 1]
                        budget_ids[i + 1] = temp

            # Эрэмбэлэгдсэн list-нд харгалзах төсвийн чиглэлийн index-д төсвийн мөрийг өгнө.
            bud_line_ids = budget_ids
            for bud_line in self.budget_id.crossovered_budget_line:
                for i in range(0, len(budget_ids)):
                    if budget_ids[i].id == bud_line.general_budget_id.id:
                        bud_line_ids[i] = bud_line

            # Мөр баганын нийлбэр болон эцэг төсвийн чиглэлийн утгыг олно
            parent_values = {}
            parent_values.update({'total_all_planned': 0, 'total_all_practical': 0})
            for bud_line in bud_line_ids:
                parent_ids = []
                for paths, values in self.keypaths(heritage):
                    if bud_line.general_budget_id.id == paths[-1]:
                        parent_ids = paths
                        break

                for parent_id in parent_ids:
                    i = 0
                    for line in bud_line.period_line_ids:
                        if parent_id not in parent_values:
                            parent_values[parent_id] = {}
                        if i not in parent_values[parent_id]:
                            parent_values[parent_id][i] = {'planned': 0, 'practical': 0}
                        parent_values[parent_id][i]['planned'] += line.planned_amount
                        parent_values[parent_id][i]['practical'] += line.practical_amount1
                        if 'total_planned' in parent_values[parent_id] and 'total_practical' in parent_values[parent_id]:
                            parent_values[parent_id]['total_planned'] += line.planned_amount
                            parent_values[parent_id]['total_practical'] += line.practical_amount1
                        else:
                            parent_values[parent_id]['total_planned'] = line.planned_amount
                            parent_values[parent_id]['total_practical'] = line.practical_amount1
                        parent = self.env['account.budget.post'].search([('id', '=', parent_id)])
                        if not parent.parent_id:
                            col = 'col_%s' % i
                            if col not in parent_values:
                                parent_values[col] = {'total_col_planned': 0, 'total_col_practical': 0}
                            parent_values[col]['total_col_planned'] += line.planned_amount
                            parent_values[col]['total_col_practical'] += line.practical_amount1
                            parent_values['total_all_planned'] += line.planned_amount
                            parent_values['total_all_practical'] += line.practical_amount1
                        i += 1
                    cols = i

            # Тайланг зурах хэсэг
            parent_ids_in_report = []
            for bud_line in bud_line_ids:
                parent_ids = []
                for paths, values in self.keypaths(heritage):
                    if bud_line.general_budget_id.id == paths[-1]:
                        parent_ids = paths
                        break

                for parent_id in parent_ids:
                    if parent_id not in parent_ids_in_report:
                        parent = self.env['account.budget.post'].search([('id', '=', parent_id)])
                        if bud_line.general_budget_id.id == parent_id:
                            style_code = format_content_left
                            style_text = format_content_center
                            style_float = format_content_float
                        else:
                            style_code = format_content_bold_left
                            style_text = format_content_bold_text
                            style_float = format_content_bold_float
                        sheet.write(rowx, 0, parent.code, style_code)
                        sheet.write(rowx, 1, parent.name, style_text)
                        col = 2
                        for line in bud_line.period_line_ids:
                            sheet.write(rowx, col, parent_values[parent_id][col/2-1]['planned'], style_float)
                            sheet.write(rowx, col+1, parent_values[parent_id][col/2-1]['practical'], style_float)
                            col += 2
                        sheet.write(rowx, col, parent_values[parent_id]['total_planned'], style_float)
                        sheet.write(rowx, col+1, parent_values[parent_id]['total_practical'], style_float)
                        parent_ids_in_report.append(parent_id)
                        rowx += 1

                col = 2
                for line in bud_line.period_line_ids:
                    sheet.set_column(rowx, col, 10)
                    sheet.merge_range(title_row, col, title_row, col + 1, u'%s - %s' % (line.date_from, line.date_to), format_title)
                    sheet.write(title_row + 1, col, u'Төлөвлөгөө', format_title)
                    sheet.write(title_row + 1, col + 1, u'Гүйцэтгэл', format_title)
                    col += 2
                sheet.merge_range(title_row, col, title_row, col + 1, u'Нийт', format_title)
                sheet.set_column(title_row + 1, col, 10)
                sheet.write(title_row + 1, col, u'Төлөвлөгөө', format_title)
                sheet.set_column(title_row + 1, col + 1, 10)
                sheet.write(title_row + 1, col + 1, u'Гүйцэтгэл', format_title)
                count += 1

            sheet.merge_range(rowx, 0, rowx, 1, u'Нийт', format_title)
            data_row = 0
            count = 1
            colx = 0
            for i in xrange(0, cols*2, 2):
                col = 'col_%s' % (i/2)
                chart_data_sheet.write(data_row, 0, parent_values[col]['total_col_planned'], format_content_float)
                chart_data_sheet.write(data_row, 1, parent_values[col]['total_col_practical'], format_content_float)
                sheet.write(rowx, i + 2, parent_values[col]['total_col_planned'], format_content_bold_float)
                sheet.write(rowx, i + 3, parent_values[col]['total_col_practical'], format_content_bold_float)
                colx = i + 2
                data_row += 1
                count += 1

            sheet.write(rowx, colx + 2, parent_values['total_all_planned'], format_content_bold_float)
            sheet.write(rowx, colx + 3, parent_values['total_all_practical'], format_content_bold_float)
            # Граффик зурах хэсэг
            chart = book.add_chart({'type': 'line'})
            values1 = '=sheet_data!A1:A%s' % count
            chart.add_series({
                'values': values1,
                'line': {'color': '#FF9900'},
                'marker': {
                    'type': 'square',
                    'size': 8,
                    'border': {'color': 'black'},
                    'fill': {'color': 'red'},
                },
            })
            values2 = '=sheet_data!B1:B%s' % count
            chart.add_series({
                'values': values2,
                'line': {'color': 'red'},
                'marker': {
                    'type': 'square',
                    'size': 8,
                    'border': {'color': 'black'},
                    'fill': {'color': 'green'},
                },
            })
            chart.set_chartarea({'fill': {'color': 'white', 'transparency': 100}})
            sheet.insert_chart('B%s' % (10 + len(self.budget_id.crossovered_budget_line)), chart)

        # ======================================================================================

        if self.budget_id.expense_budget_line:
            count = 1
            title_row = 4
            rowx = 6
            sheet2.set_column('B:B', 20)
            sheet2.merge_range(title_row - 3, 2, title_row - 2, 6, u'"%s" төсвийн зардалын төсөв' % (self.budget_id.name), format_title)
            sheet2.merge_range(title_row, 0, title_row + 1, 0, u'Код', format_title)
            sheet2.merge_range(title_row, 1, title_row + 1, 1, u'Төсвийн Чиглэл', format_title)

            budget_ids = []
            cols = 0

            # Төсвийн мөрүүдээс харгалзах төсвийн чиглэлүүдийг авч list болгоно.
            for bud_line in self.budget_id.expense_budget_line:
                budget_ids.append(bud_line.general_budget_id)

            # Төсвийн чиглэлүүдийг код талбараар нь эрэмбэлнэ.
            for passnum in range(len(budget_ids) - 1, 0, -1):
                for i in range(passnum):
                    if not self.compare_code(budget_ids[i].code, budget_ids[i + 1].code):
                        temp = budget_ids[i]
                        budget_ids[i] = budget_ids[i + 1]
                        budget_ids[i + 1] = temp

            # Эрэмбэлэгдсэн list-нд харгалзах төсвийн чиглэлийн index-д төсвийн мөрийг өгнө.
            bud_line_ids = budget_ids
            for bud_line in self.budget_id.expense_budget_line:
                for i in range(0, len(budget_ids)):
                    if budget_ids[i].id == bud_line.general_budget_id.id:
                        bud_line_ids[i] = bud_line

            # Мөр баганын нийлбэр болон эцэг төсвийн чиглэлийн утгыг олно
            parent_values = {}
            parent_values.update({'total_all_planned': 0, 'total_all_practical': 0})
            for bud_line in bud_line_ids:
                parent_ids = []
                for paths, values in self.keypaths(heritage):
                    if bud_line.general_budget_id.id == paths[-1]:
                        parent_ids = paths
                        break

                for parent_id in parent_ids:
                    i = 0
                    for line in bud_line.period_line_ids:
                        if parent_id not in parent_values:
                            parent_values[parent_id] = {}
                        if i not in parent_values[parent_id]:
                            parent_values[parent_id][i] = {'planned': 0, 'practical': 0}
                        parent_values[parent_id][i]['planned'] += line.planned_amount
                        parent_values[parent_id][i]['practical'] += line.practical_amount1
                        if 'total_planned' in parent_values[parent_id] and 'total_practical' in parent_values[parent_id]:
                            parent_values[parent_id]['total_planned'] += line.planned_amount
                            parent_values[parent_id]['total_practical'] += line.practical_amount1
                        else:
                            parent_values[parent_id]['total_planned'] = line.planned_amount
                            parent_values[parent_id]['total_practical'] = line.practical_amount1
                        parent = self.env['account.budget.post'].search([('id', '=', parent_id)])
                        if not parent.parent_id:
                            col = 'col_%s' % i
                            if col not in parent_values:
                                parent_values[col] = {'total_col_planned': 0, 'total_col_practical': 0}
                            parent_values[col]['total_col_planned'] += line.planned_amount
                            parent_values[col]['total_col_practical'] += line.practical_amount1
                            parent_values['total_all_planned'] += line.planned_amount
                            parent_values['total_all_practical'] += line.practical_amount1
                        i += 1
                    cols = i

            # Тайланг зурах хэсэг
            parent_ids_in_report = []
            for bud_line in bud_line_ids:
                parent_ids = []
                for paths, values in self.keypaths(heritage):
                    if bud_line.general_budget_id.id == paths[-1]:
                        parent_ids = paths
                        break

                for parent_id in parent_ids:
                    if parent_id not in parent_ids_in_report:
                        parent = self.env['account.budget.post'].search([('id', '=', parent_id)])
                        if bud_line.general_budget_id.id == parent_id:
                            style_code = format_content_left
                            style_text = format_content_center
                            style_float = format_content_float
                        else:
                            style_code = format_content_bold_left
                            style_text = format_content_bold_text
                            style_float = format_content_bold_float
                        sheet2.write(rowx, 0, parent.code, style_code)
                        sheet2.write(rowx, 1, parent.name, style_text)
                        col = 2
                        for line in bud_line.period_line_ids:
                            sheet2.write(rowx, col, parent_values[parent_id][col / 2 - 1]['planned'], style_float)
                            sheet2.write(rowx, col + 1, parent_values[parent_id][col / 2 - 1]['practical'], style_float)
                            col += 2
                        sheet2.write(rowx, col, parent_values[parent_id]['total_planned'], style_float)
                        sheet2.write(rowx, col + 1, parent_values[parent_id]['total_practical'], style_float)
                        parent_ids_in_report.append(parent_id)
                        rowx += 1

                col = 2
                for line in bud_line.period_line_ids:
                    sheet2.set_column(rowx, col, 10)
                    sheet2.merge_range(title_row, col, title_row, col + 1, u'%s - %s' % (line.date_from, line.date_to), format_title)
                    sheet2.write(title_row + 1, col, u'Төлөвлөгөө', format_title)
                    sheet2.write(title_row + 1, col + 1, u'Гүйцэтгэл', format_title)
                    col += 2
                sheet2.merge_range(title_row, col, title_row, col + 1, u'Нийт', format_title)
                sheet2.set_column(title_row + 1, col, 10)
                sheet2.write(title_row + 1, col, u'Төлөвлөгөө', format_title)
                sheet2.set_column(title_row + 1, col + 1, 10)
                sheet2.write(title_row + 1, col + 1, u'Гүйцэтгэл', format_title)
                count += 1

            sheet2.merge_range(rowx, 0, rowx, 1, u'Нийт', format_title)
            data_row = 0
            count = 1
            colx = 0
            for i in xrange(0, cols * 2, 2):
                col = 'col_%s' % (i / 2)
                chart_data_sheet.write(data_row, 2, parent_values[col]['total_col_planned'], format_content_float)
                chart_data_sheet.write(data_row, 3, parent_values[col]['total_col_practical'], format_content_float)
                sheet2.write(rowx, i + 2, parent_values[col]['total_col_planned'], format_content_bold_float)
                sheet2.write(rowx, i + 3, parent_values[col]['total_col_practical'], format_content_bold_float)
                colx = i + 2
                data_row += 1
                count += 1

            sheet2.write(rowx, colx + 2, parent_values['total_all_planned'], format_content_bold_float)
            sheet2.write(rowx, colx + 3, parent_values['total_all_practical'], format_content_bold_float)
            # Граффик зурах хэсэг
            chart = book.add_chart({'type': 'line'})
            values1 = '=sheet_data!C1:C%s' % count
            chart.add_series({
                'values': values1,
                'line': {'color': '#FF9900'},
                'marker': {
                    'type': 'square',
                    'size': 8,
                    'border': {'color': 'black'},
                    'fill': {'color': 'red'},
                },
            })
            values2 = '=sheet_data!D1:D%s' % count
            chart.add_series({
                'values': values2,
                'line': {'color': 'red'},
                'marker': {
                    'type': 'square',
                    'size': 8,
                    'border': {'color': 'black'},
                    'fill': {'color': 'green'},
                },
            })
            sheet2.insert_chart('B%s' % (10 + len(self.budget_id.expense_budget_line)), chart)

        book.close()

        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()
