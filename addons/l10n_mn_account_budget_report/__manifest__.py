# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Account Budget Report",
    'version': '1.0',
    'depends': ['l10n_mn_account_budget'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
    """,
    'data': [
                'report/integration_budget_report_view.xml',
    ]
}