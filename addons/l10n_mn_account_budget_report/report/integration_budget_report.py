# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
import xlsxwriter

from odoo import api, fields, models, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class IntegrationBudgetReport(models.TransientModel):
    _name = 'integration.budget.report'
    _description = 'Integration Budget Report'

    report_type = fields.Selection([('detail', 'Detail'), ('summary', 'Summary')], string="Report Type", required=True, default='summary')
    department_id = fields.Many2one('hr.department', string='Department')
    budget_id = fields.Many2one('crossovered.budget', string='Budget')
    department_ids = fields.Many2many('hr.department', string='Department')
    budget_ids = fields.Many2many('crossovered.budget', string='Budget')

    @api.onchange('department_ids')
    def _onchange_department_ids(self):
        if self.department_ids:
            department_ids = self.env['crossovered.budget'].search([('workflow_id.department_id', 'in', self.department_ids.ids)])
            return {'domain': {'budget_ids': [('id', 'in', department_ids.ids)]}}

    @api.onchange('department_id')
    def _onchange_department(self):
        if self.department_id:
            department_ids = self.env['crossovered.budget'].search([('workflow_id.department_id', '=', self.department_id.id)])
            return {'domain': {'budget_id': [('id', 'in', department_ids.ids)]}}

    # nested dict-ийн бүх навчнаас роот хүртэлх замуудыг list хэлбэрт оруулан буцаана.
    def keypaths(self, nested):
        for key, value in nested.iteritems():
            if type(value) is dict:
                for subkey, subvalue in self.keypaths(value):
                    yield [key] + subkey, subvalue
            else:
                yield [key], value

    # Өгөгдсөн төслийн чиглэлээс бүх хүүхэд төслийн чиглэлүүдийг мод бүтцэд оруулна.
    def init_tree(self, node):
        heritage = {}
        childs = self.env['account.budget.post'].search([('parent_id', '=', node.id)])
        if not childs: return False
        for child in childs:
            heritage[child.id] = self.init_tree(child)
        return heritage

    # Өгөгдсөн төслийн чиглэлүүдийн кодыг харьцуулж аль нь эхэлж байрлахыг шийднэ.
    def compare_code(self, code1='', code2=''):
        code1 = code1.split('.')
        code2 = code2.split('.')
        if code1[0] == code2[0]:
            for c1, c2 in zip(code1[1:], code2[1:]):

                if c1 > c2:
                    return False
                elif c1 < c2:
                    return True
        else:
            for char1, char2 in zip(code1[0], code2[0]):
                if ord(char1) > ord(char2):
                    return False
                elif ord(char1) < ord(char2):
                    return True
            if len(code1) > len(code2):
                return False
            if len(code1) < len(code2):
                return True

    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Income Budget Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_title = book.add_format(ReportExcelCellStyles.format_title_small_color)
        format_content_left = book.add_format(ReportExcelCellStyles.format_content_left)
        format_content_center = book.add_format(ReportExcelCellStyles.format_content_center)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_bold_left = book.add_format(ReportExcelCellStyles.format_content_bold_left)
        format_content_bold_float = book.add_format(ReportExcelCellStyles.format_content_bold_float)
        format_content_bold_text = book.add_format(ReportExcelCellStyles.format_content_bold_text)
        format_content_red_color_text = book.add_format(ReportExcelCellStyles.format_content_center_color_red)
        format_content_green_color_text = book.add_format(ReportExcelCellStyles.format_content_center_color_green)
        format_header_title_small = book.add_format(ReportExcelCellStyles.format_title_small)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_name = book.add_format(ReportExcelCellStyles.format_name)

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('crossovered_budget_report'), form_title=file_name).create({})

        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        style_text = style_float = None
        if self.report_type == 'summary':
            # compute column
            title_row = 4
            rowx = 10
            col = 0
            count = 1
            sheet.set_column('B:B', 20)
            sheet.merge_range(title_row - 4, 0, title_row - 4, 2,
                              '%s: %s' % (_('Company'), self.env.user.company_id.name), format_filter)

            sheet.merge_range(title_row - 3, 3, title_row - 3, 8, _('Integration budget report'),
                              format_name)
            sheet.merge_range(title_row, 0, title_row+3, 1, _('Budget'), format_header_title_small)
            sheet.merge_range(title_row + 5, 0, title_row+4, 0, _('Code'), format_header_title_small)
            sheet.merge_range(title_row + 5, 1, title_row + 4, 1, _('Budget Direction'), format_header_title_small)
            budget_ids = {}

            for department in self.department_ids:
                for budget in self.budget_ids:
                        if budget.workflow_id.department_id == department:
                            if department['name'] not in budget_ids:
                                budget_ids[department['name']] = {'budget_name': [budget.name], 'budget': [budget], 'department_count': 0}
                            else:
                                budget_ids[department['name']]['budget_name'].append(budget.name)
                                budget_ids[department['name']]['department_count'] += 6
                                budget_ids[department['name']]['budget'].append(budget)

            for department, value in budget_ids.items():
                sheet.merge_range(title_row, col+2, title_row+1, col + 7+value['department_count'], u'%s' % (department), format_header_title_small)
                col += 6 + value['department_count']

            count += 1
            col = 2
            budget_count = 0
            budget_count_expense = 0
            budget_count_balance = 0
            for department, value in budget_ids.items():
                for line in value['budget_name']:
                    sheet.merge_range(title_row + 2, col, title_row + 3, col + 5, u'%s' % (line), format_header_title_small)
                    sheet.merge_range(title_row + 4, col, title_row + 5, col + 1, _('Plan'), format_header_title_small)
                    sheet.merge_range(title_row + 4, col+2, title_row + 5, col + 3, _('Performance'), format_header_title_small)
                    sheet.merge_range(title_row + 4, col+4, title_row + 5, col + 5, _('Performance evaluation'), format_header_title_small)
                    col += 6
                    budget_count += 1
                    budget_count_expense +=1
                    budget_count_balance +=1
            sheet.merge_range(rowx, 0, rowx, col-1, _('Income budget'), format_title)
            expense_col = col-1
            rowx +=1
            col = 2
            i = 0
            budget_count_x = 0
            col_x = 2
            budget_count_total = {}
            for department, value in budget_ids.items():
                for budget in value['budget']:
                    for line in budget.crossovered_budget_line:
                        planned_amount = 0
                        practical_amount = 0
                        for period_line in line.period_line_ids:
                            planned_amount += period_line.planned_amount
                            practical_amount += period_line.practical_amount1
                            rate = practical_amount/planned_amount * 100 if planned_amount else 0
                        sheet.write(rowx, 0, line.general_budget_id.code, format_content_center)
                        sheet.write(rowx, 1, line.general_budget_id.name, format_content_center)
                        sheet.merge_range(rowx, col, rowx, col + 1, planned_amount, format_content_float)
                        sheet.merge_range(rowx, col+2, rowx, col + 3, practical_amount, format_content_float)
                        sheet.merge_range(rowx, col + 4, rowx, col + 5, rate, format_content_green_color_text if rate >= 100 else format_content_red_color_text)
                        sheet.merge_range(rowx, col + 6, rowx, col +6 * budget_count - 1, '', format_content_center)
                        if budget_count_x != 0:
                            sheet.merge_range(rowx, col_x, rowx, col_x + 6 * budget_count_x - 1, '', format_content_center)

                        rowx += 1
                        colss = 'col_%s' % i
                        if colss not in budget_count_total:
                            budget_count_total[colss] = {'total_col_planned': 0, 'total_col_practical': 0, 'total_rate': 0 }
                        budget_count_total[colss]['total_col_planned'] += planned_amount
                        budget_count_total[colss]['total_col_practical'] += practical_amount
                        budget_count_total[colss]['total_rate'] += budget_count_total[colss]['total_col_practical']/budget_count_total[colss]['total_col_planned'] * 100 if budget_count_total[colss]['total_col_planned'] else 0
                    budget_count -= 1
                    budget_count_x +=1
                    i += 1
                    col += 6
            cols = i

            sheet.merge_range(rowx, 0, rowx, 1, _('Total'), format_title)
            colx = 0
            for i in xrange(0, cols, 1):
                col = 'col_%s' % (i)
                sheet.merge_range(rowx, colx + 2, rowx, colx + 3, budget_count_total[col]['total_col_planned'] if col in budget_count_total else '', format_title)
                sheet.merge_range(rowx, colx + 4, rowx, colx + 5, budget_count_total[col]['total_col_practical'] if col in budget_count_total else '', format_title)
                sheet.merge_range(rowx, colx + 6, rowx, colx + 7, budget_count_total[col]['total_rate'] if col in budget_count_total else '', format_title)
                colx += 6

            rowx +=1
            col = 2
            col_x = 2
            sheet.merge_range(rowx, 0, rowx, expense_col, _('Expense budget'), format_title)
            rowx+=1
            budget_expense_count_total = {}
            budget_count_expsen_x = 0
            i=0
            for department, value in budget_ids.items():
                for budget in value['budget']:
                    for line in budget.expense_budget_line:
                        planned_amount = 0
                        practical_amount = 0

                        for period_line in line.period_line_ids:
                            planned_amount += period_line.planned_amount
                            practical_amount += period_line.practical_amount1
                            rate = practical_amount/planned_amount * 100 if planned_amount else 0
                        sheet.write(rowx, 0, line.general_budget_id.code, format_content_center)
                        sheet.write(rowx, 1, line.general_budget_id.name, format_content_center)
                        sheet.merge_range(rowx, col, rowx, col + 1, planned_amount, format_content_float)
                        sheet.merge_range(rowx, col+2, rowx, col + 3, practical_amount, format_content_float)
                        sheet.merge_range(rowx, col + 4, rowx, col + 5, rate, format_content_green_color_text if rate >= 100 else format_content_red_color_text)
                        sheet.merge_range(rowx, col + 6, rowx, col +6 * budget_count_expense - 1, '', format_content_center)
                        if budget_count_expsen_x != 0:
                            sheet.merge_range(rowx, col_x, rowx, col_x + 6 * budget_count_expsen_x - 1, '', format_content_center)

                        rowx += 1
                        cols_ex = 'col_%s' % i
                        if cols_ex not in budget_expense_count_total:
                            budget_expense_count_total[cols_ex] = {'total_col_planned': 0, 'total_col_practical': 0, 'total_rate': 0 }
                        budget_expense_count_total[cols_ex]['total_col_planned'] += planned_amount
                        budget_expense_count_total[cols_ex]['total_col_practical'] += practical_amount
                        budget_expense_count_total[cols_ex]['total_rate'] += budget_expense_count_total[cols_ex]['total_col_practical']/budget_expense_count_total[cols_ex]['total_col_planned'] * 100 if budget_expense_count_total[cols_ex]['total_col_planned'] else 0
                    budget_count_expense -= 1
                    budget_count_expsen_x +=1
                    i += 1
                    col += 6
            cols_expense = i

            sheet.merge_range(rowx, 0, rowx, 1, _('Total'), format_title)
            colx = 0
            for i in xrange(0, cols_expense, 1):
                col_ex = 'col_%s' % (i)

                sheet.merge_range(rowx, colx + 2, rowx, colx + 3, budget_expense_count_total[col_ex]['total_col_planned'] if col_ex in budget_expense_count_total else '', format_title)
                sheet.merge_range(rowx, colx + 4, rowx, colx + 5, budget_expense_count_total[col_ex]['total_col_practical'] if col_ex in budget_expense_count_total else '',  format_title)
                sheet.merge_range(rowx, colx + 6, rowx, colx + 7, budget_expense_count_total[col_ex]['total_rate'] if col_ex in budget_expense_count_total else '', format_title)
                colx += 6
            rowx += 1
            col = 2
            col_x = 2
            sheet.merge_range(rowx, 0, rowx, expense_col, _('Balance budget'), format_title)
            rowx += 1
            budget_balance_count_total = {}
            budget_count_balance_x = 0
            i = 0
            for department, value in budget_ids.items():
                for budget in value['budget']:
                    for line in budget.balance_budget_line:
                        planned_amount = 0
                        practical_amount = 0

                        for period_line in line.period_line_ids:
                            planned_amount += period_line.planned_amount
                            practical_amount += period_line.practical_amount1
                            rate = practical_amount / planned_amount * 100 if planned_amount else 0

                        sheet.write(rowx, 0, line.general_budget_id.code, format_content_center)
                        sheet.write(rowx, 1, line.general_budget_id.name, format_content_center)
                        sheet.merge_range(rowx, col, rowx, col + 1, planned_amount, format_content_float)
                        sheet.merge_range(rowx, col + 2, rowx, col + 3, practical_amount, format_content_float)
                        sheet.merge_range(rowx, col + 4, rowx, col + 5, rate, format_content_green_color_text if rate >= 100 else format_content_red_color_text)
                        sheet.merge_range(rowx, col + 6, rowx, col + 6 * budget_count_balance - 1, '', format_content_center)
                        if budget_count_balance_x != 0:
                            sheet.merge_range(rowx, col_x, rowx, col_x + 6 * budget_count_balance_x - 1, '', format_content_center)
                        rowx += 1
                        cols_ex = 'col_%s' % i
                        if cols_ex not in budget_balance_count_total:
                            budget_balance_count_total[cols_ex] = {'total_col_planned': 0, 'total_col_practical': 0,
                                                                   'total_rate': 0}
                        budget_balance_count_total[cols_ex]['total_col_planned'] += planned_amount
                        budget_balance_count_total[cols_ex]['total_col_practical'] += practical_amount
                        budget_balance_count_total[cols_ex]['total_rate'] += budget_balance_count_total[cols_ex]['total_col_practical'] / budget_balance_count_total[cols_ex]['total_col_planned'] * 100 if budget_balance_count_total[cols_ex]['total_col_planned'] else 0
                    budget_count_balance -= 1
                    budget_count_balance_x += 1
                    i += 1
                    col += 6
            cols_balance = i

            sheet.merge_range(rowx, 0, rowx, 1, _('Total'), format_title)
            colx = 0
            for i in xrange(0, cols_balance, 1):
                col_bl = 'col_%s' % (i)

                sheet.merge_range(rowx, colx + 2, rowx, colx + 3, budget_balance_count_total[col_bl][
                    'total_col_planned'] if col_bl in budget_balance_count_total else '', format_title)
                sheet.merge_range(rowx, colx + 4, rowx, colx + 5, budget_balance_count_total[col_bl][
                    'total_col_practical'] if col_bl in budget_balance_count_total else '', format_title)
                sheet.merge_range(rowx, colx + 6, rowx, colx + 7, budget_balance_count_total[col_bl][
                    'total_rate'] if col_bl in budget_balance_count_total else '', format_title)
                colx += 6

        if self.report_type == 'detail':
            title_row = 4
            rowx = 10
            # Эцэг талбар нь хоосон буюу роот төслийн чиглэлүүдийг авч мод бүтцэд оруулна.
            roots = self.env['account.budget.post'].search([('parent_id', '=', False)])
            heritage = {}
            for root in roots:
                heritage[root.id] = self.init_tree(root)
            sheet.set_column('B:B', 20)

            budget_ids = []
            # Төсвийн мөрүүдээс харгалзах төсвийн чиглэлүүдийг авч list болгоно.
            for bud_line in self.budget_id.crossovered_budget_line:
                budget_ids.append(bud_line.general_budget_id)

            # Төсвийн чиглэлүүдийг код талбараар нь эрэмбэлнэ.
            for passnum in range(len(budget_ids) - 1, 0, -1):
                for i in range(passnum):
                    if not self.compare_code(budget_ids[i].code, budget_ids[i + 1].code):
                        temp = budget_ids[i]
                        budget_ids[i] = budget_ids[i + 1]
                        budget_ids[i + 1] = temp

            # Эрэмбэлэгдсэн list-нд харгалзах төсвийн чиглэлийн index-д төсвийн мөрийг өгнө.
            bud_line_ids = budget_ids
            header_column_crossovered = 0
            for bud_line in self.budget_id.crossovered_budget_line:
                for i in range(0, len(budget_ids)):
                    if budget_ids[i].id == bud_line.general_budget_id.id:
                        bud_line_ids[i] = bud_line

            # Мөр баганын нийлбэр болон эцэг төсвийн чиглэлийн утгыг олно
            parent_values = {}
            parent_values.update({'total_all_planned': 0, 'total_all_practical': 0})
            income_cols = 0
            for bud_line in bud_line_ids:
                parent_ids = []
                for paths, values in self.keypaths(heritage):
                    if bud_line.general_budget_id.id == paths[-1]:
                        parent_ids = paths
                        break

                for parent_id in parent_ids:
                    i = 0
                    for line in bud_line.period_line_ids:
                        if parent_id not in parent_values:
                            parent_values[parent_id] = {}
                        if i not in parent_values[parent_id]:
                            parent_values[parent_id][i] = {'planned': 0, 'practical': 0}
                        parent_values[parent_id][i]['planned'] += line.planned_amount
                        parent_values[parent_id][i]['practical'] += line.practical_amount1
                        if 'total_planned' in parent_values[parent_id] and 'total_practical' in parent_values[
                            parent_id]:
                            parent_values[parent_id]['total_planned'] += line.planned_amount
                            parent_values[parent_id]['total_practical'] += line.practical_amount1
                        else:
                            parent_values[parent_id]['total_planned'] = line.planned_amount
                            parent_values[parent_id]['total_practical'] = line.practical_amount1
                        parent = self.env['account.budget.post'].search([('id', '=', parent_id)])
                        if not parent.parent_id:
                            col = 'col_%s' % i
                            if col not in parent_values:
                                parent_values[col] = {'total_col_planned': 0, 'total_col_practical': 0}
                            parent_values[col]['total_col_planned'] += line.planned_amount
                            parent_values[col]['total_col_practical'] += line.practical_amount1
                            parent_values['total_all_planned'] += line.planned_amount
                            parent_values['total_all_practical'] += line.practical_amount1
                        i += 1
                    income_cols = i

            expense_budget_ids = []
            expense_cols = 0
            # Төсвийн мөрүүдээс харгалзах төсвийн чиглэлүүдийг авч list болгоно.
            for expense_bud_line in self.budget_id.expense_budget_line:
                expense_budget_ids.append(expense_bud_line.general_budget_id)

            # Төсвийн чиглэлүүдийг код талбараар нь эрэмбэлнэ.
            for expense_passnum in range(len(expense_budget_ids) - 1, 0, -1):
                for i in range(expense_passnum):
                    if not self.compare_code(expense_budget_ids[i].code, expense_budget_ids[i + 1].code):
                        expense_temp = expense_budget_ids[i]
                        expense_budget_ids[i] = expense_budget_ids[i + 1]
                        expense_budget_ids[i + 1] = expense_temp

            # Эрэмбэлэгдсэн list-нд харгалзах төсвийн чиглэлийн index-д төсвийн мөрийг өгнө.
            expense_bud_line_ids = expense_budget_ids
            for expense_bud_line in self.budget_id.expense_budget_line:
                for i in range(0, len(expense_budget_ids)):
                    if expense_budget_ids[i].id == expense_bud_line.general_budget_id.id:
                        expense_bud_line_ids[i] = expense_bud_line

            # Мөр баганын нийлбэр болон эцэг төсвийн чиглэлийн утгыг олно
            expense_parent_values = {}
            expense_parent_values.update({'total_all_planned': 0, 'total_all_practical': 0})
            for expense_bud_line in expense_bud_line_ids:
                expense_parent_ids = []
                for paths, values in self.keypaths(heritage):
                    if expense_bud_line.general_budget_id.id == paths[-1]:
                        expense_parent_ids = paths
                        break

                for expense_parent_id in expense_parent_ids:
                    i = 0
                    for line in expense_bud_line.period_line_ids:
                        if expense_parent_id not in expense_parent_values:
                            expense_parent_values[expense_parent_id] = {}
                        if i not in expense_parent_values[expense_parent_id]:
                            expense_parent_values[expense_parent_id][i] = {'planned': 0, 'practical': 0}
                        expense_parent_values[expense_parent_id][i]['planned'] += line.planned_amount
                        expense_parent_values[expense_parent_id][i]['practical'] += line.practical_amount1
                        if 'total_planned' in expense_parent_values[expense_parent_id] and 'total_practical' in \
                                expense_parent_values[expense_parent_id]:
                            expense_parent_values[expense_parent_id]['total_planned'] += line.planned_amount
                            expense_parent_values[expense_parent_id]['total_practical'] += line.practical_amount1
                        else:
                            expense_parent_values[expense_parent_id]['total_planned'] = line.planned_amount
                            expense_parent_values[expense_parent_id]['total_practical'] = line.practical_amount1
                        expense_parent = self.env['account.budget.post'].search([('id', '=', expense_parent_id)])
                        if not expense_parent.parent_id:
                            col = 'col_%s' % i
                            if col not in expense_parent_values:
                                expense_parent_values[col] = {'total_col_planned': 0, 'total_col_practical': 0}
                            expense_parent_values[col]['total_col_planned'] += line.planned_amount
                            expense_parent_values[col]['total_col_practical'] += line.practical_amount1
                            expense_parent_values['total_all_planned'] += line.planned_amount
                            expense_parent_values['total_all_practical'] += line.practical_amount1
                        i += 1
                    expense_cols = i
            balance_budget_ids = []
            balance_cols = 0
            # Төсвийн мөрүүдээс харгалзах төсвийн чиглэлүүдийг авч list болгоно.
            for balance_bud_line in self.budget_id.balance_budget_line:
                balance_budget_ids.append(balance_bud_line.general_budget_id)

            # Төсвийн чиглэлүүдийг код талбараар нь эрэмбэлнэ.
            for balance_passnum in range(len(balance_budget_ids) - 1, 0, -1):
                for i in range(balance_passnum):
                    if not self.compare_code(balance_budget_ids[i].code, balance_budget_ids[i + 1].code):
                        balance_temp = balance_budget_ids[i]
                        balance_budget_ids[i] = balance_budget_ids[i + 1]
                        balance_budget_ids[i + 1] = balance_temp

            # Эрэмбэлэгдсэн list-нд харгалзах төсвийн чиглэлийн index-д төсвийн мөрийг өгнө.
            balance_bud_line_ids = balance_budget_ids
            for balance_bud_line in self.budget_id.balance_budget_line:
                for i in range(0, len(balance_budget_ids)):
                    if balance_budget_ids[i].id == balance_bud_line.general_budget_id.id:
                        balance_bud_line_ids[i] = balance_bud_line

            # Мөр баганын нийлбэр болон эцэг төсвийн чиглэлийн утгыг олно
            balance_parent_values = {}
            balance_parent_values.update({'total_all_planned': 0, 'total_all_practical': 0})
            for balance_bud_line in balance_bud_line_ids:
                balance_parent_ids = []
                for paths, values in self.keypaths(heritage):
                    if balance_bud_line.general_budget_id.id == paths[-1]:
                        balance_parent_ids = paths
                        break

                for balance_parent_id in balance_parent_ids:
                    i = 0
                    for line in balance_bud_line.period_line_ids:
                        if balance_parent_id not in balance_parent_values:
                            balance_parent_values[balance_parent_id] = {}
                        if i not in balance_parent_values[balance_parent_id]:
                            balance_parent_values[balance_parent_id][i] = {'planned': 0, 'practical': 0}
                        balance_parent_values[balance_parent_id][i]['planned'] += line.planned_amount
                        balance_parent_values[balance_parent_id][i]['practical'] += line.practical_amount1
                        if 'total_planned' in balance_parent_values[balance_parent_id] and 'total_practical' in \
                                balance_parent_values[balance_parent_id]:
                            balance_parent_values[balance_parent_id]['total_planned'] += line.planned_amount
                            balance_parent_values[balance_parent_id]['total_practical'] += line.practical_amount1
                        else:
                            balance_parent_values[balance_parent_id]['total_planned'] = line.planned_amount
                            balance_parent_values[balance_parent_id]['total_practical'] = line.practical_amount1
                        balance_parent = self.env['account.budget.post'].search([('id', '=', balance_parent_id)])
                        if not balance_parent.parent_id:
                            col = 'col_%s' % i
                            if col not in balance_parent_values:
                                balance_parent_values[col] = {'total_col_planned': 0, 'total_col_practical': 0}
                            balance_parent_values[col]['total_col_planned'] += line.planned_amount
                            balance_parent_values[col]['total_col_practical'] += line.practical_amount1
                            balance_parent_values['total_all_planned'] += line.planned_amount
                            balance_parent_values['total_all_practical'] += line.practical_amount1
                        i += 1
                    balance_cols = i

            # Тайлангийн толгой зурах хэсэг
            sheet.merge_range(title_row-4, 0, title_row-4, 2, '%s: %s' % (_('Company'), self.budget_id.company_id.name), format_filter)

            sheet.merge_range(title_row - 3, 3, title_row - 3, 8, _('Integration budget report'),
                              format_name)


            # Department Name
            sheet.merge_range(title_row-1, 0, title_row-1, 2, '%s: %s' % (_('Department Name'), self.department_id.name),
                              format_filter)

            # Budget name
            title_row +=1
            sheet.merge_range(title_row-1, 0, title_row-1, 2, '%s: %s' % (_('Budget Name'), self.budget_id.name), format_filter)
            title_row +=1

            # Тайланг орлогын тайлан зурах хэсэг

            sheet.merge_range(title_row, 0, title_row + 1, 1, _('Income Budget'), format_header_title_small)
            sheet.merge_range(title_row + 3, 0, title_row + 2, 0, _('Code'), format_header_title_small)
            sheet.merge_range(title_row + 3, 1, title_row + 2, 1, _('Budget Direction'), format_header_title_small)

            cols = 0
            parent_ids_in_report = []
            for bud_line in bud_line_ids:
                parent_ids = []
                for paths, values in self.keypaths(heritage):
                    if bud_line.general_budget_id.id == paths[-1]:
                        parent_ids = paths
                        break

                for parent_id in parent_ids:
                    if parent_id not in parent_ids_in_report:
                        parent = self.env['account.budget.post'].search([('id', '=', parent_id)])
                        if bud_line.general_budget_id.id == parent_id:
                            style_code = format_content_left
                            style_text = format_content_center
                            style_float = format_content_float
                        else:
                            style_code = format_content_bold_left
                            style_text = format_content_bold_text
                            style_float = format_content_bold_float
                        sheet.write(rowx, 0, parent.code, style_code)
                        sheet.write(rowx, 1, parent.name, style_text)
                        col = 2
                        for line in bud_line.period_line_ids:
                            sheet.write(rowx, col, parent_values[parent_id][col / 2 - 1]['planned'], style_float)
                            sheet.write(rowx, col + 1, parent_values[parent_id][col / 2 - 1]['practical'], style_float)
                            col += 2
                        sheet.write(rowx, col, parent_values[parent_id]['total_planned'], style_float)
                        sheet.write(rowx, col + 1, parent_values[parent_id]['total_practical'], style_float)
                        parent_ids_in_report.append(parent_id)
                        rowx += 1

                col = 2
                for line in bud_line.period_line_ids:
                    sheet.set_column(rowx, col, 10)
                    sheet.merge_range(title_row, col, title_row+1, col + 1, u'%s - %s' % (line.date_from, line.date_to),
                                      format_header_title_small)
                    sheet.merge_range(title_row+2, col, title_row + 3, col, _('Plan'),format_header_title_small)
                    sheet.merge_range(title_row + 2, col+1, title_row + 3, col+1, _('Performance'), format_header_title_small)
                    col += 2
                sheet.merge_range(title_row, col, title_row+1, col + 1, _('Total'), format_header_title_small)
                sheet.set_column(title_row + 1, col, 10)
                sheet.merge_range(title_row + 2, col, title_row + 3, col, _('Plan'), format_header_title_small)
                sheet.set_column(title_row + 1, col + 1, 10)
                sheet.merge_range(title_row + 2, col + 1, title_row + 3, col + 1, _('Performance'), format_header_title_small)


            sheet.merge_range(rowx, 0, rowx, 1, _('Total'), format_title)

            colx = 0
            for i in xrange(0, income_cols * 2, 2):
                col = 'col_%s' % (i / 2)
                sheet.write(rowx, i + 2, parent_values[col]['total_col_planned'], format_title)
                sheet.write(rowx, i + 3, parent_values[col]['total_col_practical'], format_title)
                colx = i + 2
            if income_cols:
                sheet.write(rowx, colx + 2, parent_values['total_all_planned'], format_title)
                sheet.write(rowx, colx + 3, parent_values['total_all_practical'], format_title)

            # Зарлагын төсөв тайлан зурах хэсэг
            rowx += 3
            sheet.merge_range(rowx, 0, rowx + 1, 1, _('Expense budget'), format_header_title_small)
            sheet.merge_range(rowx + 3, 0, rowx + 2, 0, _('Code'), format_header_title_small)
            sheet.merge_range(rowx + 3, 1, rowx + 2, 1, _('Budget Direction'), format_header_title_small)
            title_row = rowx
            rowx += 4

            # Тайланг зурах хэсэг
            expense_parent_ids_in_report = []
            for expense_bud_line in expense_bud_line_ids:
                expense_parent_ids = []
                for paths, values in self.keypaths(heritage):
                    if expense_bud_line.general_budget_id.id == paths[-1]:
                        expense_parent_ids = paths
                        break

                for expense_parent_id in expense_parent_ids:
                    if expense_parent_id not in expense_parent_ids_in_report:
                        expense_parent = self.env['account.budget.post'].search([('id', '=', expense_parent_id)])
                        if expense_bud_line.general_budget_id.id == expense_parent:
                            style_code = format_content_left
                            style_text = format_content_center
                            style_float = format_content_float
                        else:
                            style_code = format_content_bold_left
                            style_text = format_content_bold_text
                            style_float = format_content_bold_float
                        sheet.write(rowx, 0, expense_parent.code, style_code)
                        sheet.write(rowx, 1, expense_parent.name, style_text)
                        col = 2
                        for line in expense_bud_line.period_line_ids:
                            sheet.write(rowx, col, expense_parent_values[expense_parent_id][col / 2 - 1]['planned'], style_float)
                            sheet.write(rowx, col + 1, expense_parent_values[expense_parent_id][col / 2 - 1]['practical'], style_float)
                            col += 2
                        sheet.write(rowx, col, expense_parent_values[expense_parent_id]['total_planned'], style_float)
                        sheet.write(rowx, col + 1, expense_parent_values[expense_parent_id]['total_practical'], style_float)
                        expense_parent_ids_in_report.append(expense_parent_id)
                        rowx += 1
                col = 2
                for line in expense_bud_line.period_line_ids:
                    sheet.set_column(rowx, col, 10)
                    sheet.merge_range(title_row, col, title_row + 1, col + 1,
                                      u'%s - %s' % (line.date_from, line.date_to),
                                      format_header_title_small)
                    sheet.merge_range(title_row + 2, col, title_row + 3, col, _('Plan'), format_header_title_small)
                    sheet.merge_range(title_row + 2, col + 1, title_row + 3, col + 1, _('Performance'),
                                      format_header_title_small)
                    col += 2
                sheet.merge_range(title_row, col, title_row + 1, col + 1, _('Total'), format_header_title_small)
                sheet.set_column(rowx + 1, col, 10)
                sheet.merge_range(title_row + 2, col, title_row + 3, col, _('Plan'), format_header_title_small)
                sheet.set_column(rowx + 1, col + 1, 10)
                sheet.merge_range(title_row + 2, col + 1, title_row + 3, col + 1, _('Performance'),
                                  format_header_title_small)

            sheet.merge_range(rowx, 0, rowx, 1, _('Total'), format_title)

            colx = 0
            for i in xrange(0, expense_cols * 2, 2):
                col = 'col_%s' % (i / 2)
                sheet.write(rowx, i + 2, expense_parent_values[col]['total_col_planned'], format_title)
                sheet.write(rowx, i + 3, expense_parent_values[col]['total_col_practical'], format_title)
                colx = i + 2
            if expense_cols:
                sheet.write(rowx, colx + 2, expense_parent_values['total_all_planned'], format_title)
                sheet.write(rowx, colx + 3, expense_parent_values['total_all_practical'], format_title)

            # Баланс тайлан зурах хэсэг
            rowx += 3
            col = 2
            sheet.merge_range(rowx, 0, rowx + 1, 1, _('Balance budget'), format_header_title_small)
            sheet.merge_range(rowx + 3, 0, rowx + 2, 0, _('Code'), format_header_title_small)
            sheet.merge_range(rowx + 3, 1, rowx + 2, 1, _('Budget Direction'), format_header_title_small)
            title_row = rowx
            rowx +=4

            # Тайланг зурах хэсэг

            balance_parent_ids_in_report = []
            for balance_bud_line in balance_bud_line_ids:
                balance_parent_ids = []
                for paths, values in self.keypaths(heritage):
                    if balance_bud_line.general_budget_id.id == paths[-1]:
                        balance_parent_ids = paths
                        break

                for balance_parent_id in balance_parent_ids:
                    if balance_parent_id not in balance_parent_ids_in_report:
                        balance_parent = self.env['account.budget.post'].search([('id', '=', balance_parent_id)])
                        if balance_bud_line.general_budget_id.id == balance_parent:
                            style_code = format_content_left
                            style_text = format_content_center
                            style_float = format_content_float
                        else:
                            style_code = format_content_bold_left
                            style_text = format_content_bold_text
                            style_float = format_content_bold_float
                        sheet.write(rowx, 0, balance_parent.code, style_code)
                        sheet.write(rowx, 1, balance_parent.name, style_text)
                        col = 2
                        for line in balance_bud_line.period_line_ids:
                            sheet.write(rowx, col, balance_parent_values[balance_parent_id][col / 2 - 1]['planned'],
                                        style_float)
                            sheet.write(rowx, col + 1,
                                        balance_parent_values[balance_parent_id][col / 2 - 1]['practical'], style_float)
                            col += 2
                        sheet.write(rowx, col, balance_parent_values[balance_parent_id]['total_planned'], style_float)
                        sheet.write(rowx, col + 1, balance_parent_values[balance_parent_id]['total_practical'],
                                    style_float)
                        balance_parent_ids_in_report.append(balance_parent_id)
                        rowx += 1

                    col = 2
                    for line in balance_bud_line.period_line_ids:
                        sheet.set_column(rowx, col, 10)
                        sheet.merge_range(title_row, col, title_row + 1, col + 1, u'%s - %s' % (line.date_from, line.date_to),
                                          format_header_title_small)
                        sheet.merge_range(title_row + 2, col, title_row + 3, col, _('Plan'), format_header_title_small)
                        sheet.merge_range(title_row + 2, col + 1, title_row + 3, col + 1, _('Performance'), format_header_title_small)
                        col += 2
                    sheet.merge_range(title_row, col, title_row + 1, col + 1, _('Total'), format_header_title_small)
                    sheet.set_column(rowx + 1, col, 10)
                    sheet.merge_range(title_row + 2, col, title_row + 3, col, _('Plan'), format_header_title_small)
                    sheet.set_column(rowx + 1, col + 1, 10)
                    sheet.merge_range(title_row + 2, col + 1, title_row + 3, col + 1, _('Performance'), format_header_title_small)

            sheet.merge_range(rowx, 0, rowx, 1, _('Total'), format_title)

            colx = 0
            for i in xrange(0, balance_cols * 2, 2):
                col = 'col_%s' % (i / 2)
                sheet.write(rowx, i + 2, balance_parent_values[col]['total_col_planned'], format_title)
                sheet.write(rowx, i + 3, balance_parent_values[col]['total_col_practical'], format_title)
                colx = i + 2
            if balance_cols:
                sheet.write(rowx, colx + 2, balance_parent_values['total_all_planned'], format_title)
                sheet.write(rowx, colx + 3, balance_parent_values['total_all_practical'], format_title)

        book.close()

        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()
