# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Cashflow Budget",
    'version': '1.0',
    'depends': ['l10n_mn_account_budget'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
     Cashflow Budget
    """,
    'data': [
             'views/account_budget_view.xml',
             'views/account_bank_statement_view.xml',
             'report/cashflow_budget_report_view.xml',
             'report/crossovered_budget_report_view.xml',
    ],
    'demo': [
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}