# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError


class AccountBankStatementLine(models.Model):
    _inherit = 'account.bank.statement.line'

    cash_general_budget_id = fields.Many2one('account.budget.post', 'Budgetary Position', ondelete='restrict', domain=[('type', 'in', ('income_cashflow', 'expense_cashflow'))])

    @api.constrains('amount', 'cash_general_budget_id')
    @api.onchange('amount', 'cash_general_budget_id')
    def _check_budget_amount(self):
        if self.currency_id and self.currency_id != self.company_id.currency_id:
            self.amount_currency = self.amount
        if self.cash_general_budget_id:
            if self.amount > 0 and self.cash_general_budget_id.type == 'expense_cashflow':
                raise ValidationError(_('"%s" budgetary position only accepts negative amounts. The amount cannot be %s') % (self.cash_general_budget_id.name, self.amount))
            if self.amount < 0 and self.cash_general_budget_id.type == 'income_cashflow':
                raise ValidationError(_('"%s" budgetary position only accepts positive amounts. The amount cannot be %s') % (self.cash_general_budget_id.name, self.amount))

    def process_reconciliation(self, counterpart_aml_dicts=None, payment_aml_rec=None, new_aml_dicts=None):
        # Журналын мөр рүү мөнгөн гүйлгээний төсвийн чиглэлийг дамжуулж байна
        res = super(AccountBankStatementLine, self).process_reconciliation(counterpart_aml_dicts, payment_aml_rec, new_aml_dicts)
        for move in res:
            account_id = self.statement_id.journal_id.default_credit_account_id.id
            if self.amount >= 0:
                account_id = self.statement_id.journal_id.default_debit_account_id.id
            for move_line in move.line_ids:
                if move_line.account_id.id == account_id:
                    move_line.write({'cash_general_budget_id': self.cash_general_budget_id.id})
        return res