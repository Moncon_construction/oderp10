# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class InheritedAccountBudgetPost(models.Model):
    _inherit = 'account.budget.post'

    type = fields.Selection([('view', 'View'),
                             ('income', 'Income'),
                             ('expense', 'Expense'),
                             ('balance', 'Balance'),
                             ('view_cashflow', 'View - Cashflow'),
                             ('income_cashflow', 'Income Cashflow'),
                             ('expense_cashflow', 'Expense Cashflow')], string='Type', required=True, default='expense')

    @api.onchange('parent_id')
    def onchange_parent_id(self):
        if self.parent_id:
            if self.parent_id.type not in ('view', 'view_cashflow'):
                raise UserError(_('For parents, type must be view'))
            childs = self.env['account.budget.post'].search([('parent_id', '=', self.parent_id.id)])
            self.code = '%s.%s' % (self.parent_id.code, len(childs) + 1)

    @api.onchange('type')
    def onchange_parent_domain(self):
        res = {}
        if self.type:
            if self.type in ('income', 'expense', 'balance', 'view'):
                res = {'domain': {'parent_id': [('type', '=', 'view')]}}
            elif self.type in ('income_cashflow', 'expense_cashflow', 'view_cashflow'):
                res = {'domain': {'parent_id': [('type', '=', 'view_cashflow')]}}
        return res


class InheritedCrossoveredBudget(models.Model):
    _inherit = 'crossovered.budget'

    def _default_cashflow(self):
        general_budget = self.env['account.budget.post'].search([('parent_id', '=', False), ('type', '=', 'view_cashflow')])
        if len(general_budget) > 0:
            general_budget = general_budget[0]
        return general_budget.id

    is_cashflow_budget = fields.Boolean(string='Is Cashflow Budget', default=False)
    initial_balance = fields.Float(string='Budget Initial Balance', digits=(12, 2))
    crossovered_budget_line = fields.One2many('crossovered.budget.lines', 'income_crossovered_budget_id', domain=[('type', 'in', ('income', 'income_cashflow'))],
                                              string='Income Budget Lines', readonly=True, states={'draft': [('readonly', False)]})
    expense_budget_line = fields.One2many('crossovered.budget.lines', 'expense_crossovered_budget_id', domain=[('type', 'in', ('expense', 'expense_cashflow'))],
                                          string='Expense Budget Lines', readonly=True, states={'draft': [('readonly', False)]})

    @api.multi
    def create_lines(self):
        # Дахин тодорхойлсон нь
        post_obj = self.env['account.budget.post']
        for budget in self:
            balance_lines = []
            if budget.state == 'draft':
                # Хэрвээ Мөнгөн гүйлгээний төсвийг чекэлсэн бол
                if budget.is_cashflow_budget:
                    post_ids = post_obj.search([('type', 'in', ('income_cashflow', 'expense_cashflow'))], order='code')
                    self.env.cr.execute("DELETE FROM crossovered_budget_lines WHERE income_crossovered_budget_id = %s AND type = 'income' ", (budget.id,))
                    self.env.cr.execute("DELETE FROM crossovered_budget_lines WHERE expense_crossovered_budget_id = %s AND type = 'expense' ", (budget.id,))
                    self.env.cr.execute("DELETE FROM crossovered_budget_lines WHERE balance_crossovered_budget_id = %s AND type = 'balance' ", (budget.id,))
                # Хэрвээ Мөнгөн гүйлгээний төсвийг чеклээгүй бол
                else:
                    post_ids = post_obj.search([('type', 'in', ('income', 'expense', 'balance'))], order='code')
                    self.env.cr.execute("DELETE FROM crossovered_budget_lines WHERE income_crossovered_budget_id = %s AND type = 'income_cashflow' ", (budget.id,))
                    self.env.cr.execute("DELETE FROM crossovered_budget_lines WHERE expense_crossovered_budget_id = %s AND type = 'expense_cashflow' ", (budget.id,))

                    self.env.cr.execute("SELECT general_budget_id FROM crossovered_budget_lines WHERE balance_crossovered_budget_id = %s", (budget.id,))
                    lines = self.env.cr.dictfetchall()
                    balance_lines = list(set([line['general_budget_id'] for line in lines]))
                self.env.cr.execute("SELECT general_budget_id FROM crossovered_budget_lines WHERE income_crossovered_budget_id = %s", (budget.id,))
                lines = self.env.cr.dictfetchall()
                income_lines = list(set([line['general_budget_id'] for line in lines]))

                self.env.cr.execute("SELECT general_budget_id FROM crossovered_budget_lines WHERE expense_crossovered_budget_id = %s", (budget.id,))
                lines = self.env.cr.dictfetchall()
                expense_lines = list(set([line['general_budget_id'] for line in lines]))

                for post in post_ids:
                    if post.type in ('income', 'income_cashflow') and post.id not in income_lines:
                        res = {'income_crossovered_budget_id': budget.id,
                               'analytic_account_id': budget.analytic_account_id.id,
                               'date_from': budget.date_from,
                               'date_to': budget.date_to,
                               'general_budget_id': post.id,
                               'type': post.type,
                               }
                        line = self.env['crossovered.budget.lines'].create(res)
                        line.create_line_periods()
                    elif post.type in ('expense', 'expense_cashflow') and post.id not in expense_lines:
                        res = {'expense_crossovered_budget_id': budget.id,
                               'analytic_account_id': budget.analytic_account_id.id,
                               'date_from': budget.date_from,
                               'date_to': budget.date_to,
                               'general_budget_id': post.id,
                               'type': post.type,
                               }
                        line = self.env['crossovered.budget.lines'].create(res)
                        line.create_line_periods()
                    elif len(balance_lines) > 0 and post.type == 'balance' and post.id not in balance_lines:
                        res = {'balance_crossovered_budget_id': budget.id,
                               'analytic_account_id': budget.analytic_account_id.id,
                               'date_from': budget.date_from,
                               'date_to': budget.date_to,
                               'general_budget_id': post.id,
                               'type': post.type,
                               }
                        line = self.env['crossovered.budget.lines'].create(res)
                        line.create_line_periods()

class InheritedCrossoveredBudgetLines(models.Model):
    _inherit = 'crossovered.budget.lines'

    code = fields.Char(string='Budgetary Position Code', related='general_budget_id.code')
    sequence = fields.Integer(string='Budgetary Position Sequence', related='general_budget_id.sequence')
    type = fields.Selection([('income', 'Income'),
                             ('expense', 'Expense'),
                             ('balance', 'Balance'),
                             ('income_cashflow', 'Income Cashflow'),
                             ('expense_cashflow', 'Expense Cashflow')], 'Type', required=True)

    @api.onchange('general_budget_id')
    def onchange_general_budget_id(self):
        if self.general_budget_id.id:
            if self.income_crossovered_budget_id:
                if self.income_crossovered_budget_id.is_cashflow_budget and self.general_budget_id.type != 'income_cashflow':
                    raise UserError(_('Budgetary position type must be income cashflow'))
                elif not self.income_crossovered_budget_id.is_cashflow_budget and self.general_budget_id.type != 'income':
                    raise UserError(_('Budgetary position type must be income'))
            if self.expense_crossovered_budget_id:
                if self.expense_crossovered_budget_id.is_cashflow_budget and self.general_budget_id.type != 'expense_cashflow':
                    raise UserError(_('Budgetary position type must be expense cashflow'))
                elif not self.expense_crossovered_budget_id.is_cashflow_budget and self.general_budget_id.type != 'expense':
                    raise UserError(_('Budgetary position type must be expense'))
            self.type = self.general_budget_id.type
            self.date_from = self.income_crossovered_budget_id.date_from or self.expense_crossovered_budget_id.date_from or self.balance_crossovered_budget_id.date_from
            self.date_to = self.income_crossovered_budget_id.date_to or self.expense_crossovered_budget_id.date_to or self.balance_crossovered_budget_id.date_to
            self.analytic_account_id = self.income_crossovered_budget_id.analytic_account_id.id or self.expense_crossovered_budget_id.analytic_account_id.id or self.balance_crossovered_budget_id.analytic_account_id.id

class CrossoveredBudgetLinesPeriod(models.Model):
    _inherit = 'crossovered.budget.lines.period'

    @api.multi
    def _prac_amt(self):
        result = 0.0
        for period_line in self:
            if period_line.id:
                date_from = period_line.date_from
                date_to = period_line.date_to
                line = period_line.line_id
                is_cashflow_budget = line and (line.income_crossovered_budget_id and line.income_crossovered_budget_id.is_cashflow_budget or
                                              line.expense_crossovered_budget_id and line.expense_crossovered_budget_id.is_cashflow_budget or
                                              line.balance_crossovered_budget_id and line.balance_crossovered_budget_id.is_cashflow_budget) or False
                if is_cashflow_budget:
                    self.env.cr.execute("SELECT SUM(debit - credit) FROM account_move_line "
                                        "WHERE statement_id IS NOT NULL AND cash_general_budget_id = %s AND date BETWEEN %s AND %s ",
                                        (line.general_budget_id.id, date_from, date_to))
                    result = self.env.cr.fetchone()[0]
                elif not is_cashflow_budget and line.general_budget_id.account_ids:
                    acc_ids = [x.id for x in line.general_budget_id.account_ids]
                    if not acc_ids:
                        raise UserError(_("Budgetary Position %s has no accounts!") % (line.general_budget_id.name))
                    if line.analytic_account_id.id:
                        self.env.cr.execute("SELECT SUM(amount) FROM account_analytic_line WHERE account_id = %s AND (date "
                                            "between to_date(%s,'yyyy-mm-dd') AND to_date(%s,'yyyy-mm-dd')) AND "
                                            "general_account_id=ANY(%s)", (line.analytic_account_id.id, date_from, date_to, acc_ids))
                        result = self.env.cr.fetchone()[0]
                if result is None:
                    result = 0.00
                self.env.cr.execute("UPDATE crossovered_budget_lines_period SET practical_amount1 = %s WHERE id = %s", (abs(result), period_line.id,))
                period_line.practical_amount = abs(result)

    practical_amount = fields.Float(compute=_prac_amt, string='Practical Amount')