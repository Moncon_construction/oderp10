# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    cash_general_budget_id = fields.Many2one('account.budget.post', string='Cashflow Budgetary Position', ondelete='restrict', readonly=True)