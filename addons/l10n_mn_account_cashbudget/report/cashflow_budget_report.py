# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
import xlsxwriter

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class CashflowBudgetReport(models.TransientModel):
    _name = 'cashflow.budget.report'
    _description = 'Cashflow Budget Report'

    budget_id = fields.Many2one('crossovered.budget', string='Budget', required=True, domain=[('is_cashflow_budget', '=', True)], ondelete='cascade')
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env['res.company']._company_default_get('cashflow.budget.report'))
    start_period_id  = fields.Many2one('account.period', string='Start Period', required=True)
    end_period_id = fields.Many2one('account.period', string='End Period', required=True)
    show_plan_amount = fields.Boolean(string='Show Plan Amount', default=False)
    show_practical_amount = fields.Boolean(string='Show Practical Amount', default=False)

    @api.onchange('budget_id')
    def onchange_period_domain(self):
        res = {}
        if self.budget_id:
            periods = self.env['account.period'].search([('date_start', '>=', self.budget_id.date_from), ('date_stop', '<=', self.budget_id.date_to)], order='date_start')
            if len(periods) > 0:
                length = len(periods) - 1
                self.start_period_id = periods[0]
                self.end_period_id = periods[length]
                res = {'domain': {'start_period_id': [('id', 'in', periods.ids)], 'end_period_id': [('id', 'in', periods.ids)]}}
        return res

    @api.onchange('start_period_id', 'end_period_id')
    def onchange_periods(self):
        if self.start_period_id and self.end_period_id:
            if self.start_period_id.date_start > self.end_period_id.date_start:
                raise UserError(_('End period must be greater than start period!'))

    @api.multi
    def get_sheet(self, sheet):
        # compute column
        colx_number = 3
        sheet.set_column('A:A', 8)
        sheet.set_column('B:B', 36)
        sheet.set_column('C:BC', 12)
        return sheet, colx_number

    def get_child(self, general_budget_id):
        all = []
        child_ids = self.env['account.budget.post'].search([('parent_id', '=', general_budget_id.id)])
        if not child_ids:
            return []
        for child in child_ids:
            all.append(child.id)
            all += self.get_child(child)
        return all

    @api.multi
    def get_header(self, sheet, rowx, format_title, periods, date_from):
        # Тайлангийн хүснэгтийн толгой зурах
        sheet.merge_range(rowx, 0, rowx + 1, 0, _('Code'), format_title)
        sheet.merge_range(rowx, 1, rowx + 1, 1, _('Indication'), format_title)
        col = 2
        for period in periods:
            if period.date_start >= date_from:
                if self.show_plan_amount and not self.show_practical_amount:
                    sheet.write(rowx, col, period.name, format_title)
                elif not self.show_plan_amount and self.show_practical_amount:
                    sheet.merge_range(rowx, col, rowx, col + 1, period.name, format_title)
                else:
                    sheet.merge_range(rowx, col, rowx, col + 2, period.name, format_title)
                if (not self.show_plan_amount and not self.show_practical_amount) or self.show_plan_amount:
                    sheet.write(rowx + 1, col, _('Planned'), format_title)
                    col += 1
                if (not self.show_plan_amount and not self.show_practical_amount) or self.show_practical_amount:
                    sheet.write(rowx + 1, col, _('Practical'), format_title)
                    sheet.write(rowx + 1, col + 1, _('Practical Percent'), format_title)
                    col += 2
        if self.show_plan_amount and not self.show_practical_amount:
            sheet.write(rowx, col, _('Total'), format_title)
        elif not self.show_plan_amount and self.show_practical_amount:
            sheet.merge_range(rowx, col, rowx, col + 1, _('Total'), format_title)
        else:
            sheet.merge_range(rowx, col, rowx, col + 2, _('Total'), format_title)
        if (not self.show_plan_amount and not self.show_practical_amount) or self.show_plan_amount:
            sheet.write(rowx + 1, col, _('Planned'), format_title)
            col += 1
        if (not self.show_plan_amount and not self.show_practical_amount) or self.show_practical_amount:
            sheet.write(rowx + 1, col, _('Practical'), format_title)
            sheet.write(rowx + 1, col + 1, _('Practical Percent'), format_title)
            col += 2
        return sheet

    @api.multi
    def get_parent(self, sheet, rowx, format_text, format_float, general_budget_id, parent_id, periods, date_from):
        # Эцэг чиглэлийн дүнг тооцно
        if not general_budget_id.parent_id:
            return sheet, rowx
        if not parent_id and general_budget_id.parent_id:
            sheet, rowx = self.get_parent(sheet, rowx, format_text, format_float,  general_budget_id.parent_id, parent_id, periods, date_from)
        total_plan = total_prac = 0
        col = 2
        child_ids = self.get_child(general_budget_id)
        if len(child_ids) > 0:
            lines = self.env['crossovered.budget.lines'].search([('general_budget_id', 'in', child_ids), ('income_crossovered_budget_id', '=', self.budget_id.id)])
            lines += self.env['crossovered.budget.lines'].search([('general_budget_id', 'in', child_ids), ('expense_crossovered_budget_id', '=', self.budget_id.id)])
            if len(lines) > 0:
                # Эцэг чиглэлийн мөрийг зурах
                sheet.write(rowx, 0, general_budget_id.code, format_text)
                sheet.write(rowx, 1, general_budget_id.name, format_text)
                for period in periods:
                    if period.date_start >= date_from:
                        period_plan = period_prac = 0
                        for line in lines:
                            for period_line in line.period_line_ids:
                                if period_line.date_from == period.date_start and period_line.date_to == period.date_stop:
                                    period_plan += period_line.planned_amount
                                    period_prac += period_line.practical_amount
                        if (not self.show_plan_amount and not self.show_practical_amount) or self.show_plan_amount:
                            sheet.write(rowx, col, period_plan, format_float)
                            col += 1
                        if (not self.show_plan_amount and not self.show_practical_amount) or self.show_practical_amount:
                            sheet.write(rowx, col, period_prac, format_float)
                            sheet.write(rowx, col + 1, period_prac * 100 / period_plan if period_plan != 0 else 0, format_float)
                            col += 2
                        total_plan += period_plan
                        total_prac += period_prac
                if (not self.show_plan_amount and not self.show_practical_amount) or self.show_plan_amount:
                    sheet.write(rowx, col, total_plan, format_float)
                    col += 1
                if (not self.show_plan_amount and not self.show_practical_amount) or self.show_practical_amount:
                    sheet.write(rowx, col, total_prac, format_float)
                    sheet.write(rowx, col + 1, total_prac  * 100 / total_plan if total_plan != 0 else 0, format_float)
                rowx += 1
        return sheet, rowx

    @api.multi
    def get_value(self, sheet, rowx, format_text, format_float, line, periods, date_from, period_total):
        # Тайлангийн мөр зурах
        sheet.write(rowx, 0, line.general_budget_id.code, format_text)
        sheet.write(rowx, 1, line.general_budget_id.name, format_text)
        col = 2
        total_plan = total_prac = 0
        for period in periods:
            period_ok = False
            for period_line in line.period_line_ids:
                if period_line.date_from == period.date_start and period_line.date_to == period.date_stop:
                    if line.general_budget_id and line.general_budget_id.type == 'income_cashflow':
                        period_total[period.id]['planned'] += period_line.planned_amount
                        period_total[period.id]['practical'] += period_line.practical_amount
                    else:
                        period_total[period.id]['planned'] -= period_line.planned_amount
                        period_total[period.id]['practical'] -= period_line.practical_amount
                    if period.date_start >= date_from:
                        period_ok = True
                        if (not self.show_plan_amount and not self.show_practical_amount) or self.show_plan_amount:
                            sheet.write(rowx, col, period_line.planned_amount, format_float)
                            col += 1
                        if (not self.show_plan_amount and not self.show_practical_amount) or self.show_practical_amount:
                            sheet.write(rowx, col, period_line.practical_amount, format_float)
                            sheet.write(rowx, col + 1, period_line.percentage, format_float)
                            col += 2
                        total_plan += period_line.planned_amount
                        total_prac += period_line.practical_amount
            if period.date_start >= date_from and not period_ok:
                if (not self.show_plan_amount and not self.show_practical_amount) or self.show_plan_amount:
                    sheet.write(rowx, col, 0, format_float)
                    col += 1
                if (not self.show_plan_amount and not self.show_practical_amount) or self.show_practical_amount:
                    sheet.write(rowx, col, 0, format_float)
                    sheet.write(rowx, col + 1, 0, format_float)
                    col += 2
        if (not self.show_plan_amount and not self.show_practical_amount) or self.show_plan_amount:
            sheet.write(rowx, col, total_plan, format_float)
            col += 1
        if (not self.show_plan_amount and not self.show_practical_amount) or self.show_practical_amount:
            sheet.write(rowx, col, total_prac, format_float)
            sheet.write(rowx, col + 1, total_prac  * 100 / total_plan if total_plan != 0 else 0, format_float)
        rowx += 1
        return sheet, rowx

    # Энэ функцыг l10n_mn_account_analytic_report модуль дотор override хийсэн
    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Cashflow Budget Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_bold_text = book.add_format(ReportExcelCellStyles.format_content_bold_left)
        format_content_bold_float = book.add_format(ReportExcelCellStyles.format_content_bold_float)

        date_from = self.start_period_id.date_start
        date_to = self.end_period_id.date_stop
        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('cashfflow_budget'), form_title=file_name, bdate_to=date_to, date_from=date_from).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_portrait()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 1

        sheet, colx_number = self.get_sheet(sheet)
        # create report name
        sheet.merge_range(rowx, 0, rowx, 8, "'%s' %s-%s %s" % (self.company_id and self.company_id.name or '', date_from, date_to, _('Duration Cashflow Report')), format_name)
        rowx += 2
        periods = self.env['account.period'].search([('date_start', '>=', self.budget_id.date_from), ('date_stop', '<=', date_to)], order='date_start')
        sheet = self.get_header(sheet, rowx, format_title, periods, date_from)
        rowx += 2
        period_total = {}
        for period in periods:
            period_total[period.id] = {'planned': 0, 'practical': 0}
        row = rowx

        # Тайлангийн эхний үлдэгдэл зурах
        sheet.merge_range(row, 0, row, 1, _('CASH INITIAL BALANCE'), format_content_bold_text)
        # Тайлангийн эцсийн үлдэгдэл зурах
        sheet.merge_range(row + 1, 0, row + 1, 1,  _('CASH END BALANCE'), format_content_bold_text)
        rowx += 2

        if len(self.budget_id.crossovered_budget_line) > 0:
            # Мөнгөн гүйлгээний төсвийн орлогын мөрүүдийг зурах
            parent_id = False
            for line in sorted(self.budget_id.crossovered_budget_line, key=lambda x: x.sequence):
                if line.general_budget_id.parent_id and parent_id != line.general_budget_id.parent_id:
                    sheet, rowx = self.get_parent(sheet, rowx, format_content_bold_text, format_content_bold_float, line.general_budget_id.parent_id, parent_id, periods, date_from)
                sheet, rowx = self.get_value(sheet, rowx, format_content_text, format_content_float, line, periods, date_from, period_total)
                parent_id = line.general_budget_id.parent_id
        if len(self.budget_id.expense_budget_line) > 0:
            # Мөнгөн гүйлгээний төсвийн зарлагын мөрүүдийг зурах
            parent_id = False
            for line in sorted(self.budget_id.expense_budget_line, key=lambda x: x.sequence):
                if line.general_budget_id.parent_id and parent_id != line.general_budget_id.parent_id:
                    sheet, rowx = self.get_parent(sheet, rowx, format_content_bold_text, format_content_bold_float, line.general_budget_id.parent_id, parent_id, periods, date_from)
                sheet, rowx = self.get_value(sheet, rowx, format_content_text, format_content_float, line, periods, date_from, period_total)
                parent_id = line.general_budget_id.parent_id

        # Тайлангийн цэвэр мөнгөн урсгал зурах
        sheet.merge_range(rowx, 0, rowx, 1,  _('NET CASHFLOW'), format_content_bold_text)
        col = 2
        total_plan = total_prac = 0
        # Мөнгө, түүнтэй адилтгах хөрөнгийн эхний үлдэгдлийг тооцоолох хэсэг
        initial_plan = end_plan = self.budget_id.initial_balance
        initial_prac = 0
        all_initial_plan = all_initial_prac = 0
        is_initial = False
        account_ids = self.env['account.account'].search([('company_id', '=', self.company_id.id), ('internal_type', '=', 'liquidity')])
        if not account_ids:
            raise UserError(_('There is no cash and bank flow.'))
        else:
            initials = self.env['account.move.line'].get_all_balance(self.company_id.id, account_ids.ids, self.budget_id.date_from, date_to, 'posted')
            for init in initials:
                initial_prac += init['start_balance']
        end_prac = initial_prac
        # Мөчлөг бүрээр цэвэр мөнгөн урсгалыг зурах
        for period in periods:
            end_plan += period_total[period.id]['planned']
            end_prac += period_total[period.id]['practical']
            if period.date_start >= date_from:
                if not is_initial:
                    all_initial_plan = initial_plan
                    all_initial_prac = initial_prac
                    is_initial = True
                total_plan += period_total[period.id]['planned']
                total_prac += period_total[period.id]['practical']
                if (not self.show_plan_amount and not self.show_practical_amount) or self.show_plan_amount:
                    sheet.write(rowx, col, period_total[period.id]['planned'], format_content_bold_float)
                    # Тайлангийн эхний үлдэгдэл болон эцсийн үлдэгдэл зурах
                    sheet.write(row , col, initial_plan, format_content_bold_float)
                    sheet.write(row + 1 , col, end_plan, format_content_bold_float)
                    col += 1
                if (not self.show_plan_amount and not self.show_practical_amount) or self.show_practical_amount:
                    sheet.write(rowx, col, period_total[period.id]['practical'], format_content_bold_float)
                    sheet.write(rowx, col + 1, period_total[period.id]['practical'] * 100 / period_total[period.id]['planned'] if period_total[period.id]['planned'] else 0, format_content_bold_float)
                    # Тайлангийн эхний үлдэгдэл болон эцсийн үлдэгдэл зурах
                    sheet.write(row , col, initial_prac, format_content_bold_float)
                    sheet.write(row + 1, col, end_prac, format_content_bold_float)
                    sheet.write(row, col + 1, initial_prac * 100 / initial_plan if initial_plan else 0, format_content_bold_float)
                    sheet.write(row + 1, col + 1, end_prac * 100 / end_plan if end_plan else 0, format_content_bold_float)
                    col += 2
            initial_plan = end_plan
            initial_prac = end_prac
        if (not self.show_plan_amount and not self.show_practical_amount) or self.show_plan_amount:
            sheet.write(rowx, col, total_plan, format_content_bold_float)
            # Тайлангийн эхний үлдэгдэл болон эцсийн үлдэгдэл зурах
            sheet.write(row, col, all_initial_plan, format_content_bold_float)
            sheet.write(row + 1, col, end_plan, format_content_bold_float)
            col += 1
        if (not self.show_plan_amount and not self.show_practical_amount) or self.show_practical_amount:
            sheet.write(rowx, col, total_prac, format_content_bold_float)
            sheet.write(rowx, col + 1, total_prac  * 100 / total_plan if total_plan != 0 else 0, format_content_bold_float)
            # Тайлангийн эхний үлдэгдэл болон эцсийн үлдэгдэл зурах
            sheet.write(row, col, all_initial_prac, format_content_bold_float)
            sheet.write(row + 1, col, end_prac, format_content_bold_float)
            sheet.write(row, col + 1, all_initial_prac * 100 / all_initial_plan if all_initial_plan else 0, format_content_bold_float)
            sheet.write(row + 1, col + 1, end_prac * 100 / end_plan if end_plan else 0, format_content_bold_float)

        rowx += 2
        sheet.merge_range(rowx, 1, rowx, 2, '%s: ........................................... (                          )' % _('Executive Director'), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 1, rowx, 2, '%s: ........................................... (                          )' % _('General Accountant'), format_filter)

        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report()