# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Account CashBudget Report",
    'version': '1.0',
    'depends': ['l10n_mn_account_budget_report', 'l10n_mn_account_cashbudget'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """  Төсвиийн нэгтгэл тайлан дээр Мөнгөн гүйлгээний төсвийг сонгодоггүй болгов """,
    'data': [],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}
