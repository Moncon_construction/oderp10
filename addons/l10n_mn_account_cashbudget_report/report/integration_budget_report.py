# -*- encoding: utf-8 -*-
##############################################################################
from odoo import api, fields, models


class IntegrationBudgetReport(models.TransientModel):
    _inherit = 'integration.budget.report'

    @api.onchange('department_ids')
    def _onchange_department_ids(self):
        if self.department_ids:
            department_ids = self.env['crossovered.budget'].search([('workflow_id.department_id', 'in', self.department_ids.ids), ('is_cashflow_budget', '=', False)])
            return {'domain': {'budget_ids': [('id', 'in', department_ids.ids)]}}

    @api.onchange('department_id')
    def _onchange_department(self):
        if self.department_id:
            department_ids = self.env['crossovered.budget'].search([('workflow_id.department_id', '=', self.department_id.id), ('is_cashflow_budget', '=', False)])
            return {'domain': {'budget_id': [('id', 'in', department_ids.ids)]}}