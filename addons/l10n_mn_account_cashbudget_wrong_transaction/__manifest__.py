# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Cashflow Budget Wrong Transaction",
    'version': '1.0',
    'depends': ['l10n_mn_account_cashbudget', 'l10n_mn_account_wrong_transaction'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
    Description text
    """,
    'data': [
             'views/bank_statement_line_not_budgetary_position_view.xml',
    ],
    'demo': [],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}