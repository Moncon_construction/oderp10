# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class AccountBankStatementLine(models.Model):
    _inherit = 'account.bank.statement.line'

    @api.multi
    def get_bank_statement_line_not_budgetary_position(self):
        # Төсвийн чиглэл сонгоогүй касс, харилцахын мөрүүдийг гаргах
        tree_id = self.env.ref("l10n_mn_account_cashbudget.view_bank_statement_line_tree_inherit_cashbudget")
        #form_id = self.env.ref("l10n_mn_account_cashbudget.view_bank_statement_line_form_inherit_cashbudget")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Account bank statement line not budgetary position'),
            'view_mode': 'tree',
            'res_model': 'account.bank.statement.line',
            'domain': [('cash_general_budget_id', '=', False), ('date', '>=', self.env.user.company_id.cashbudget_installed_date)],
            'views': [(tree_id.id, 'tree')],
            'target': 'current',
        }