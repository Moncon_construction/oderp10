    # -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Cashflow Detail Report",
    'version': '1.0',
    'depends': ['l10n_mn', 'l10n_mn_account', 'account'],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Account Modules',
    'description': """
         Мөнгөн гүйлгээний төрлийн дэлгэрэнгүй.
    """,
    'website' : 'http://asterisk-tech.mn',
    'data': [
        'views/cashflow_detail_report.xml',
        'security/security.xml',
        'security/ir.model.access.csv',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
