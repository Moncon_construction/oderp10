# encoding: utf-8
import time

from odoo import fields, api, models, tools
from odoo.tools.translate import _
import odoo.tools
import logging
_logger = logging.getLogger('l10n_mn_account')

class AccountCashflowDetailReport(models.Model):
    _name = 'account.cashflow.detail.report'
    _auto=False
    _order = 'id'

    id = fields.Integer('Id')
    statement_id = fields.Many2one('account.bank.statement', string='Statement', index=True)
    date = fields.Date('Date')
    name = fields.Char(string='Label')
    amount = fields.Float('Amount')
    amount_currency = fields.Float(String='Amount Currency')
    journal_id = fields.Many2one('account.journal', string='Journal')
    cashflow_id = fields.Many2one('account.cashflow.type', string='Cashflow type')
    connected_account_id = fields.Many2one('account.account', string='Counterpart Account')
    partner_id = fields.Many2one('res.partner', string='Partner')
    bank_account_id = fields.Many2one('res.partner.bank', string='Bank Account')
    note = fields.Text(string='Notes')
    ref = fields.Char(string='Reference')
    state = fields.Selection([('open','Open'),('confirm','Confirmed')], string='Status', default='open', readonly=True, required=True)
    company= fields.Many2one('res.company')
    
    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self._cr, 'account_cashflow_detail_report')
        
        self._cr.execute("""
            CREATE or REPLACE VIEW account_cashflow_detail_report as (
                SELECT table4.id as id, table4.name as name,
                    account_bank_statement_line.statement_id as statement_id,
                    account_bank_statement_line.date as date,
                    CASE 
                        WHEN table4.credit > 0.0 THEN table4.credit
                        ELSE -table4.debit 
                    END as amount,
                    account_bank_statement_line.amount_currency as amount_currency,
                    account_bank_statement_line.journal_id as journal_id,
                    account_bank_statement_line.cashflow_id as cashflow_id,
                    table4.account_id as connected_account_id,
                    account_bank_statement_line.partner_id as partner_id,
                    account_bank_statement_line.bank_account_id as bank_account_id,
                    account_bank_statement_line.note as note,
                    account_bank_statement_line.ref as ref,
                    account_bank_statement_line.company_id as company,
                    table4.state
                    FROM account_bank_statement_line
                    INNER JOIN
                        (SELECT table2.id as id, account_bank_statement.state as state, table2.statement_id, table2.name, table2.account_id,
                            table2.cashflow_id, table2.line_id, table2.credit, table2.debit FROM account_bank_statement
                            INNER JOIN
                                (SELECT table1.id, table1.statement_id as statement_id, table1.name as name, table1.account_id as account_id,
                                    table1.cashflow_id as cashflow_id, table1.line_id as line_id, table1.credit as credit, table1.debit as debit FROM
                                    (SELECT table3.id, statement_id, account_move_line.name as name, credit, debit, account_id, cashflow_id, table3.line_id as line_id
                                        FROM
                                            (SELECT account_bank_statement_line.id as line_id,
                                                    account_move.id as id,
                                                    account_bank_statement_line.name
                                            FROM account_move INNER JOIN account_bank_statement_line
                                            ON account_move.statement_line_id = account_bank_statement_line.id) as table3
                                            INNER JOIN account_move_line on account_move_line.move_id = table3.id) as table1
                                WHERE table1.cashflow_id is NULL
                                GROUP BY table1.id, table1.statement_id, table1.name, table1.account_id, table1.cashflow_id, table1.line_id, table1.credit, table1.debit) as table2
                        ON account_bank_statement.id = table2.statement_id) table4
                    ON account_bank_statement_line.id = table4.line_id
            ) """)
