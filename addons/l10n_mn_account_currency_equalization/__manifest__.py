    # -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Account Currency Equalization",
    'version': '1.0',
    'depends': ['account_accountant', 'l10n_mn_account', 'l10n_mn_analytic'],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Account Modules',
    'description': """
         Гадаад валютаар хийгдсэн журналын бичилтүүдэд ханшийн тэгшитгэл хийнэ.
    """,
    'website' : 'http://asterisk-tech.mn',
    'data': [
        'security/ir.model.access.csv',
        'views/res_config_view.xml',
        'wizard/account_currency_equalization_view.xml',
        'views/account_view.xml',
        'views/currency_equalization_history.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
