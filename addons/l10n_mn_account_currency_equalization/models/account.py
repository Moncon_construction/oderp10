# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
from odoo.exceptions import ValidationError, UserError
import odoo.addons.decimal_precision as dp

class AccountAccount(models.Model):
    _inherit = "account.account"
    
    date_currency_equalization = fields.Date('Date Currency Equalization') # compute талбар байсныг болиулав. Тухайн дансан дээр ханш тэгшитгэх/тэгшитгэл устгах болгонд огноог шинэчилдэг болгов.
    
class AccountInvoice(models.Model):
    _inherit = "account.invoice"
    
    @api.multi
    def action_invoice_cancel(self):
        # Тухайн нэхэмжлэлийн огнооноос хойш нэхэмжлэлийн холбоотой данс дээр ханш тэгшитгэсэн бол нэхэмжлэлийг ЦУЦЛАХ боломжгүй болгов
        ids = self.ids
        if len(ids) > 0:
            ids = str(tuple(ids))[:-2] + ")" if str(tuple(ids))[-2:] == ",)" else str(tuple(ids))
            qry = """
                SELECT sub_table.min_eq_date AS min_eq_date, CONCAT(acc.code, ' ', acc.name) AS acc
                FROM
                (
                    SELECT MIN(eq.date) AS min_eq_date, acc.id AS acc_id
                    FROM account_invoice inv
                    LEFT JOIN account_account acc ON acc.id = inv.account_id 
                    LEFT JOIN account_currency_equalization_history eq ON eq.account_id = acc.id
                    WHERE inv.id IN %s AND acc.date_currency_equalization IS NOT NULL AND acc.date_currency_equalization >= inv.date_invoice AND eq.date IS NOT NULL
                    GROUP BY acc.id
                ) sub_table
                LEFT JOIN account_account acc ON acc.id = sub_table.acc_id 
            """ %ids
            self._cr.execute(qry)
            results = self._cr.dictfetchall()
            
            if results and len(results) > 0:
                raise UserError(_("'%s' account is already equalized.\n\nFirst you must delete currency equalizations on this account which equalized after %s.") %(results[0]['acc'], results[0]['min_eq_date']))
        
        return super(AccountInvoice, self).action_invoice_cancel()
    
class AccountBankStatementLine(models.Model):
    _inherit = "account.bank.statement.line"
    
    @api.multi
    def button_cancel_reconciliation(self):
        # Тухайн хуулгын мөрийн огнооноос хойш ханш тэгшитгэсэн бол уг хуулгын мөрийг ЦУЦЛАХ боломжгүй болгов
        ids = self.ids
        if len(ids) > 0:
            ids = str(tuple(ids))[:-2] + ")" if str(tuple(ids))[-2:] == ",)" else str(tuple(ids))
            qry = """
                SELECT sub_table.min_eq_date AS min_eq_date, CONCAT(acc.code, ' ', acc.name) AS acc
                FROM
                (
                    SELECT MIN(eq.date) AS min_eq_date, acc.id AS acc_id
                    FROM account_bank_statement_line absl
                    LEFT JOIN account_account acc ON acc.id = absl.account_id 
                    LEFT JOIN account_currency_equalization_history eq ON eq.account_id = acc.id
                    WHERE absl.id IN %s AND acc.date_currency_equalization IS NOT NULL AND acc.date_currency_equalization >= absl.date AND eq.date IS NOT NULL
                    GROUP BY acc.id
                ) sub_table
                LEFT JOIN account_account acc ON acc.id = sub_table.acc_id 
            """ %ids
            self._cr.execute(qry)
            results = self._cr.dictfetchall()
            
            if results and len(results) > 0:
                raise UserError(_("'%s' account is already equalized.\n\nFirst you must delete currency equalizations on this account which equalized after %s.") %(results[0]['acc'], results[0]['min_eq_date']))
        
        return super(AccountBankStatementLine, self).button_cancel_reconciliation()
        
class AccountBankStatement(models.Model):
    _inherit = "account.bank.statement"

    @api.multi
    def check_statement_is_equalized(self):
        # Тухайн хуулгын мөрд ханш тэгшитгэсэн эсэхийг шалгах
        ids = self.ids
        if len(ids) > 0:
            ids = str(tuple(ids))[:-2] + ")" if str(tuple(ids))[-2:] == ",)" else str(tuple(ids))
            qry = """
                SELECT sub_table.min_eq_date AS min_eq_date, CONCAT(acc.code, ' ', acc.name) AS acc
                FROM
                (
                    SELECT MIN(eq.date) AS min_eq_date, acc.id AS acc_id
                    FROM account_bank_statement_line absl
                    LEFT JOIN account_account acc ON acc.id = absl.account_id 
                    LEFT JOIN account_currency_equalization_history eq ON eq.account_id = acc.id
                    WHERE absl.statement_id IN %s AND acc.date_currency_equalization IS NOT NULL AND acc.date_currency_equalization >= absl.date AND eq.date IS NOT NULL
                    GROUP BY acc.id
                ) sub_table
                LEFT JOIN account_account acc ON acc.id = sub_table.acc_id 
            """ %ids
            self._cr.execute(qry)
            results = self._cr.dictfetchall()
            
            if results and len(results) > 0:
                raise UserError(_("'%s' account is already equalized.\n\nFirst you must delete currency equalizations on this account which equalized after %s.") %(results[0]['acc'], results[0]['min_eq_date']))
        
        return False 
            
    @api.multi
    def button_draft(self):
        # Тухайн хуулгын мөрийн огнооноос хойш ханш тэгшитгэсэн бол уг хуулгын мөртэй холбоотой хуулгыг ШИНЭ БОЛГОХ боломжгүй болгов
        self.check_statement_is_equalized()
        return super(AccountBankStatement, self).button_draft()
        
    @api.multi
    def button_cancel(self):
        # Тухайн хуулгын мөрийн огнооноос хойш ханш тэгшитгэсэн бол уг хуулгын мөртэй холбоотой хуулгыг ЦУЦЛАХ боломжгүй болгов
        self.check_statement_is_equalized()
        return super(AccountBankStatement, self).button_cancel()