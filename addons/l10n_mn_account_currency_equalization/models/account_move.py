# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import UserError


class AccountMove(models.Model):
    _inherit = "account.move"

    @api.multi
    def unlink(self):
        # Ханшийн тэгшитгэлээс үүссэн журналын бичилт бол дараахи шалгалтаар шууд устгах боломжгүй болгов.
        # Зөвхөн тэгшитгэлийн түүхийг устгахад давхар устгадаг болгов.
        for move in self:
            history = self.env['account.currency.equalization.history'].search([('account_move_id', '=', move.id)])
            currency_equalization = self._context.get('directly_delete_from_currency_history', False)
            if not currency_equalization and history:
                raise UserError(_("You cannot delete currency equalized journal entries."))
            # Тухайн ханш тэгшитгэлийн түүхийн өмнөх түүхийг олон дансан дээрх ханш тэгшитгэсэн огноо талбарын утгыг шинэчлэнэ
            if history:
                before_history = self.env['account.currency.equalization.history'].search([('date', '<', history.date), ('account_id', '=', history.account_id.id),
                                                                                           ('id', '!=', history.id)], order="date desc", limit=1)
                if before_history and len(before_history) > 0:
                    history.account_id.date_currency_equalization = before_history[0].date
                else:
                    history.account_id.date_currency_equalization = False
        return super(AccountMove, self).unlink()

    @api.multi
    def button_cancel(self):
        # Тухайн бичилтийн мөрийн огнооноос хойш ханш тэгшитгэсэн бол уг бичилтийг ЦУЦЛАХ боломжгүй болгов
        currency_equalization = self._context.get('directly_delete_from_currency_history', False)
        for move in self:
            if not currency_equalization and move.id:
                history = self.env['account.currency.equalization.history'].search([('account_move_id', '=', move.id)])
                if history:
                    raise UserError(_("You cannot cancel currency equalized journal entries."))
        res = super(AccountMove, self).button_cancel()
        return res