# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import UserError

class AccountCurrencyEqualizationHistory(models.Model):
    _name = "account.currency.equalization.history"
    _description = "Account Entry"
    

    name = fields.Char('Name', copy=False)
    account_id = fields.Many2one('account.account', string='Equalized Account', readonly=True)
    partner_id = fields.Many2one('res.partner', string='Equalized Partner', readonly=True)
    date = fields.Date('Equalized date', readonly=True)
    currency_id = fields.Many2one('res.currency', string='Currency', readonly=True)
    currency_rate = fields.Float(string='Currency rate', readonly=True)
    account_move_id = fields.Many2one('account.move', string='Equalization Account Move', readonly=True)
    currency_move_line_id = fields.Many2one('account.move.line', 'Currency Move Line', readonly=True)
    equalization_date = fields.Date(string='Equalization Date', readonly=True)
    debit = fields.Float('Debit', readonly=True)
    credit = fields.Float('Credit', readonly=True)
    amount_currency = fields.Float('Amount Currency', readonly=True)
    added_debit = fields.Float('Added Debit', readonly=True)
    added_credit = fields.Float('Added Credit', readonly=True)
    total_debit = fields.Float('Total Debit', readonly=True)
    total_credit = fields.Float('Total Credit', readonly=True)
    
    @api.multi
    def unlink(self):
        # Холбоотой журналын бичилтийг устгасны дараа өөрийгөө утсгана
        for obj in self:
            # Устгах түүхээс хойш дахин ханш тэгшитгэсэн эсэхийг шалгана
            histories = self.env['account.currency.equalization.history'].search([('date', '>', obj.date), ('account_id', '=', obj.account_id.id)])
            histories_which_cannot_delete = []
            for history in histories:
                if history.id not in self.ids:
                    histories_which_cannot_delete.append(history)
            if len(histories_which_cannot_delete) > 0:
                raise UserError(_("You cannot delete this history. Because '%s' account already equalized %s times after this equalization.\n\n If you want to delete this history, first you must delete all of %s histories which equalized after '%s'.") %(obj.account_id.name, len(histories), len(histories), obj.date))
            # журналын бичилтийг устгах хэсэг
            obj.account_move_id.with_context({"directly_delete_from_currency_history": True}).button_cancel()
            obj.account_move_id.with_context({"directly_delete_from_currency_history": True}).unlink()
        return super(AccountCurrencyEqualizationHistory, self).unlink()