# -*- coding: utf-8 -*-
from odoo import fields, models

class ResCompany(models.Model):
    _inherit = "res.company"

    exchange_analytic_account_id = fields.Many2one('account.analytic.account',
            string='Exchange Analytic Account') #Валютын шинжилгээний данс  domain=[('type','=','normal')]
    unperformed_exchange_gain_account_id = fields.Many2one('account.account', string='Unperformed Rate Exchange Gain Account', domain=[('internal_type','=','income')],
            help="This account will be used when compute currency rate exchange unperformed gain or loss.") #Валютын ханшийн хэрэгжээгүй олзын данс
    unperformed_exchange_loss_account_id = fields.Many2one('account.account', string='Unperformed Rate Exchange Loss Account', domain=[('internal_type','=','expense')],
            help="This account will be used when compute currency rate exchange unperformed gain or loss.") #Валютын ханшийн хэрэгжээгүй гарзын данс