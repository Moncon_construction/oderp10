# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class AccountConfigSettings(models.TransientModel):
    _inherit = 'account.config.settings'
    
    exchange_analytic_account_id = fields.Many2one(related="company_id.exchange_analytic_account_id") #Валютын шинжилгээний данс
    unperformed_exchange_gain_account_id = fields.Many2one(related="company_id.unperformed_exchange_gain_account_id") #Валютын ханшийн хэрэгжээгүй олзын данс
    unperformed_exchange_loss_account_id = fields.Many2one(related="company_id.unperformed_exchange_loss_account_id") #Валютын ханшийн хэрэгжээгүй гарзын данс