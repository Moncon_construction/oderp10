# -*- coding: utf-8 -*-
import time
import calendar
from lxml import etree
from datetime import datetime, time, timedelta

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError
from odoo.tools.float_utils import float_round
import odoo.addons.decimal_precision as dp


class AccountCurrencyEqualization(models.TransientModel):
    """
        Гадаад валютаар хийгдсэн журналын бичилтүүдэд ханшийн тэгшитгэл хийнэ.
    """
    _name = "account.currency.equalization"
    _description = "Equalize Currency Rate"

    # Санхүүгийн тохиргоонд тохируулсан Ханшийн тэгшитгэлийн журнал автоматаар дуудагдана
    def _default_journal(self):
        if self.env.user.company_id and self.env.user.company_id.journal_of_currency_exchange_id:
            return self.env.user.company_id.journal_of_currency_exchange_id

    # Санхүүгийн тохиргоонд тохируулсан Валютын мөнгөн гүйлгээний төрөл автоматаар дуудагдана
    def _defaut_cashflow(self):
        if self.env.user.company_id and self.env.user.company_id.type_of_currency_id:
            return self.env.user.company_id.type_of_currency_id

    company_id = fields.Many2one('res.company', 'Company', required=True, default=lambda self: self.env['res.company']._company_default_get('account.currency.equalization'))
    journal_id = fields.Many2one('account.journal', 'Journal', required=True, default=_default_journal)
    cashflow_id = fields.Many2one('account.cashflow.type', 'Cashflow Type', default=_defaut_cashflow, domain=[('type', '=', 'normal')])
    currency_id = fields.Many2one('res.currency', 'Currency', required=True)
    date = fields.Date('Date', required=True, default=lambda self: fields.datetime.now())
    rate_date = fields.Date('Rate Date', required=True)
    line_ids = fields.One2many('account.currency.equalization.line', 'wizard_id', 'Lines')
    type = fields.Selection([('liquidity', 'Liquidity Accounts'),
                             ('partner', 'Partner Balances')], 'Currency Equalization Type', default='liquidity', required=True)
    rate = fields.Float('Currency Rate', digits=(4, 2))
    description = fields.Char('Description')

    @api.onchange('company_id')
    def onchange_domain(self):
        # Компанийн үндсэн валютыг харагдуулахгүй болгох
        domain = {}
        domain['currency_id'] = [('id', '!=', self.company_id.currency_id.id)]
        return {'domain': domain}

    @api.onchange('type')
    def onchange_equalization(self):
        # Ханш тэгшитгэх төрөл өөрчлөгдөхөд гүйлгээний утга дагаж өөрчлөгдөнө
        description = _("Currency equalization of monetary assets")  # "Мөнгөн хөрөнгийн ханшийн тэгшитгэл"
        if self.type == 'partner':
            description = _("Currency equalization of partner accounting")  # "Харилцагчийн тооцооны ханшийн тэгшитгэл"
        self.description = description
        if self.currency_id:
            self.onchange_currency()

    @api.onchange('date', 'currency_id')
    def onchange_currency(self):
        # Валют өөрчлөгдөхөд ханш болон сонгогдох данснууд өөрлөгдөнө, харилцагчийн тооцоо төрөлтэй үед гадаад вальюттай данс сонгосон харилцагчуудын нийт тооцоог мөр болгож харуулна
        if self.currency_id:
            self.rate = self.set_currency()
            self.get_lines()

    @api.multi
    def set_currency(self):
        # Тухайн огноо болон вальютанд харгалзах ханш гарч ирнэ, тухайн огнооны өмнөх хаалтын ханшаар тэгшитгэл хийгдэнэ.
        if self.date and self.currency_id:
            rate_id = self.env['res.currency.rate'].search([('currency_id', '=', self.currency_id.id), ('name', '<=', self.date)], order='name desc')
            rate = 0
            if rate_id:
                rate = rate_id[0].alter_rate
                rate_date = datetime.strptime(rate_id[0].name, "%Y-%m-%d %H:%M:%S")
                self.rate_date = rate_date.date()
            return rate

    @api.multi
    def get_lines(self):
        ''' Ханшийн тэгшитгэл дээр сонгосон валюттай /Компанийн валютаас ялгаатай валют/
            1. мөнгөн хөрөнгийн дансны эцсийн үлдэгдлийг
            2. харилцагчийн авлага, өглөгийн дансны эцсийн үлдэгдлийг /харилцагч бүрээр/
            тэгшитгэх огнооны ханшаар тооцсон үлдэгдлээс зөрүүтэй бол мөрөнд харуулна.
            3. Үлдэгдэл дээр тэгшитгэл хийх тул мөнгөн хөрөнгө болон харилцагчийн тооцоо ялгаагүй болсон
        '''
        self.line_ids = False
        precision = self.env['decimal.precision'].precision_get('Account')
        equalization_rate = float_round(self.rate, precision_digits=precision)
        create_vals = []
        where = ''
        select = ''
        group_by = ''
        if self.type == 'liquidity':
            where += "AND a.internal_type = 'liquidity' "
        else:
            where += "AND a.internal_type in ('payable', 'receivable') "
            select += ', ml.partner_id AS partner_id'
            group_by += ', ml.partner_id'
        self.env.cr.execute("SELECT ml.account_id AS account_id, SUM(ml.debit) AS debit, SUM(ml.credit) AS credit, "
                            "SUM(ml.amount_currency) AS amount_currency " + select + " "
                            "FROM account_move_line ml "
                            "JOIN account_move m ON (ml.move_id = m.id) "
                            "JOIN account_account a ON (ml.account_id = a.id) "
                            "WHERE m.state = 'posted' AND ml.date <= %s AND a.currency_id = %s AND ml.company_id = %s "
                            "AND (a.date_currency_equalization IS NULL OR a.date_currency_equalization < %s) " + where + " "
                            "GROUP BY ml.account_id " + group_by + " "
                            "ORDER BY ml.account_id ", (self.date, self.currency_id.id, self.company_id.id, self.date))
        lines = self.env.cr.dictfetchall()
        if len(lines) > 0:
            for line in lines:
                debit = float_round(line['debit'], precision_digits=precision)
                credit = float_round(line['credit'], precision_digits=precision)
                amount_currency = float_round(line['amount_currency'], precision_digits=precision)
                if not (debit == 0 and credit == 0 and amount_currency == 0):
                    diff = debit - credit
                    if amount_currency != 0:
                        rate = float_round(diff / amount_currency, precision_digits=precision)
                    elif amount_currency == 0:
                        rate = 0
                    if rate != equalization_rate:
                        vals = [0, False, {'wizard_id': self.id,
                                           'account_id': line['account_id'],
                                           'partner_id': line['partner_id'] if self.type == 'partner' else False,
                                           'debit': diff >= 0 and diff or 0,
                                           'credit': diff < 0 and -diff or 0,
                                           'amount_currency': line['amount_currency']
                                           }]
                        create_vals.append(vals)
            self.line_ids = create_vals

    def get_analytic_account(self):
        return self.company_id.exchange_analytic_account_id.id if self.company_id.exchange_analytic_account_id else False

    @api.multi
    def _get_equalization_move_line_vals(self, description, name, diff_amount, gain_loss_account, partner, cashflow_id, analytic_account_id, line):
        self.ensure_one()
        return [(0, 0, {'name': _('%s - %s') % (description, name),
                        'debit': diff_amount < 0 and -diff_amount or 0.0,
                        'credit': diff_amount > 0 and diff_amount or 0.0,
                        'account_id': gain_loss_account,
                        'partner_id': partner,
                        'cashflow_id': cashflow_id,
                        'analytic_account_id': analytic_account_id,
                        'company_id': self.company_id.id,
                        'date': self.date,
                        'journal_id': self.journal_id.id,
                        'amount_currency': 0, }),
                (0, 0, {'name': description,
                        'debit': diff_amount > 0 and diff_amount or 0.0,
                        'credit': diff_amount < 0 and -diff_amount or 0.0,
                        'account_id': line.account_id.id,
                        'partner_id': partner,
                        'cashflow_id': cashflow_id,
                        'analytic_account_id': analytic_account_id,
                        'company_id': self.company_id.id,
                        'currency_id': self.currency_id.id,
                        'date': self.date,
                        'journal_id': self.journal_id.id,
                        'amount_currency': 0, })]

    @api.multi
    def action_equalize(self):
        ''' 1. Мөрүүд дээрх данс, харилцагчийн ханшийг тэгшитгэн, ханшийн зөрүүгээр журналын бичилт үүсгэнэ
            2. Ханш тэгшитгэлийн түүх үүсгэнэ. Түүхэнд ямар данс, харилцагчийн валютыг тэгшитгэснийг харуулна
            3. Үлдэгдэл дээр тэгшитгэл хийх тул мөнгөн хөрөнгө болон харилцагчийн тооцоо ялгаагүй болсон
        '''
        amove_obj = self.env['account.move']
        history_obj = self.env['account.currency.equalization.history']
        analytic_account_id = self.get_analytic_account()
        res = []
        cashflow_id = False
        account_id = False
        precision = self.env['decimal.precision'].precision_get('Account')
        if self.cashflow_id:
            cashflow_id = self.cashflow_id.id
        for line in self.line_ids:
            if account_id and account_id != line.account_id:
                account_id.write({'date_currency_equalization': self.date})
            if line.account_id.date_currency_equalization < self.date:
                partner = False
                description = u'%s, %s %s' % (self.date, line.account_id.code, line.account_id.name)
                if line.partner_id:
                    partner = line.partner_id.id
                    description += u', %s' % (line.partner_id.name)
                balance = line.debit - line.credit
                balance = float_round(balance, precision_digits=precision)
                # Валютын дүнгээрх үлдэгдэл
                # Тухайн өдрийн ханшаар валютын үлдэгдлийг үржүүлэхэд дансны үлдэгдэл гарна.
                #  Үүнийг бодит үлдэгдэлтэй харьцуулж ханшийн зөрүү дүнг олно.
                converted_amount = self.currency_id.with_context(date=self.date).compute(line.amount_currency, self.company_id.currency_id)
                #converted_amount = self.rate * line.amount_currency
                diff_amount = converted_amount - balance
                diff_amount = float_round(diff_amount, precision_digits=precision)
                if self.company_id.currency_id.is_zero(diff_amount):
                    # Ханшийн зөрүү үүсээгүй болно.
                    continue
                if diff_amount > 0:
                    # Ханшийн зөрүүний ашиг
                    if self.company_id.unperformed_exchange_gain_account_id:
                        gain_loss_account = self.company_id.unperformed_exchange_gain_account_id.id
                    else:
                        raise ValidationError(_('Warning! There is no currency rate exchange unperformed gain or loss account defined for this company : %s') % self.company_id.name)
                    name = _("Unperformed Rate Exchange Gain")  # "Валютын ханшийн зөрүүгийн  хэрэгжээгүй ашиг"
                else:
                    # Ханшийн зөрүүний алдагдал
                    if self.company_id.unperformed_exchange_loss_account_id:
                        gain_loss_account = self.company_id.unperformed_exchange_loss_account_id.id
                    else:
                        raise ValidationError(_('Warning! There is no currency rate exchange unperformed gain or loss account defined for this company : %s') % self.company_id.name)
                    name = _("Unperformed Rate Exchange Loss")  # "Валютын ханшийн зөрүүгийн  хэрэгжээгүй  зардал"
                # Ханшийн тэгшитгэлийн түүхийн талбарууд
                added_debit = added_credit = 0
                if diff_amount > 0:
                    added_debit = diff_amount or 0.0
                elif diff_amount < 0:
                    added_credit = -diff_amount or 0.0
                total = (line.debit + added_debit) - (line.credit + added_credit)
                # Ханшийн тэгшитгэлийн түүх үүсгэх
                history_id = history_obj.create({'account_id': line.account_id.id,
                                                 'partner_id': partner,
                                                 'date': self.date,
                                                 'currency_id': self.currency_id.id,
                                                 'currency_rate': self.rate,
                                                 'equalization_date': datetime.now(),
                                                 'debit': line.debit,
                                                 'credit': line.credit,
                                                 'amount_currency': line.amount_currency,
                                                 'added_debit': added_debit,
                                                 'added_credit': added_credit,
                                                 'total_debit': total >= 0 and total or 0,
                                                 'total_credit': total < 0 and -total or 0,
                                                 })
                # Ханшийн зөрүүг журналд бичих
                line_ids = self._get_equalization_move_line_vals(description, name, diff_amount, gain_loss_account, partner, cashflow_id, analytic_account_id, line)
                move_id = amove_obj.create({'journal_id': self.journal_id.id,
                                            'date': self.date,
                                            'narration': description,
                                            'currency_equalized': True,
                                            'line_ids': line_ids
                                            })
                move_id.post()
                # Ханшийн тэгшитгэлийн түүхэд үүсгэсэн журналын бичилтийг нэмж байна
                history_id.write({'name': move_id.name,
                                  'account_move_id': move_id.id
                                  })
                res.append(history_id.id)
                account_id = line.account_id
            else:
                raise UserError(_("'%s' account is already equalized.\n\nFirst you must delete currency equalizations on this account which equalized after %s.")
                                % (line.account_id.name, self.date))
        if account_id:
            account_id.write({'date_currency_equalization': self.date})

        return {'name': _('Equalization History'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.currency.equalization.history',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'domain': [('id', 'in', res if res else False)],
                }


class AccountCurrencyEqualizationLine(models.TransientModel):
    _name = "account.currency.equalization.line"

    wizard_id = fields.Many2one('account.currency.equalization', 'Wizard')
    account_id = fields.Many2one('account.account', required=True)
    partner_id = fields.Many2one('res.partner', 'Partner')
    debit = fields.Float('Debit', default=0)
    credit = fields.Float('Credit', default=0)
    amount_currency = fields.Float('Amount currency', default=0)
