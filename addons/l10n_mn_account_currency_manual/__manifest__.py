# -*- coding: utf-8 -*-
{
    'name': "Mongolian Account Currency Manual",
    'version': '1.0',
    'depends': [
        'l10n_mn_account'
    ],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Account Modules',
    'description': """
        Manual currency for account bank statement.
    """,
    'website': 'http://asterisk-tech.mn',
    'data': [
        'views/account_bank_statement_view.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
