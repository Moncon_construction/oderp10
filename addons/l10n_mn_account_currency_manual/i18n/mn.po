# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* l10n_mn_account_currency_manual
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 10.0+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-09-01 06:47+0000\n"
"PO-Revision-Date: 2020-09-01 06:47+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: l10n_mn_account_currency_manual
#: model:ir.model,name:l10n_mn_account_currency_manual.model_account_bank_statement
msgid "Bank Statement"
msgstr "Банкны хуулга"

#. module: l10n_mn_account_currency_manual
#: model:ir.model,name:l10n_mn_account_currency_manual.model_account_bank_statement_line
msgid "Bank Statement Line"
msgstr "Банкны хуулгын мөр"

#. module: l10n_mn_account_currency_manual
#: model:ir.model.fields,field_description:l10n_mn_account_currency_manual.field_account_bank_statement_hide_manual_currency_rate
#: model:ir.model.fields,field_description:l10n_mn_account_currency_manual.field_account_bank_statement_line_hide_manual_currency_rate
msgid "Hide manual currency rate"
msgstr "Албан бус ханш нуух"

#. module: l10n_mn_account_currency_manual
#: model:ir.model.fields,field_description:l10n_mn_account_currency_manual.field_account_bank_statement_line_manual_currency_rate
#: model:ir.model.fields,field_description:l10n_mn_account_currency_manual.field_account_bank_statement_manual_currency_rate
msgid "Manual currency rate"
msgstr "Албан бус ханш"
