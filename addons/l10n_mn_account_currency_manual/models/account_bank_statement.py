from odoo import api, fields, models


class AccountBankStatement(models.Model):
    _inherit = 'account.bank.statement'

    hide_manual_currency_rate = fields.Boolean('Hide manual currency rate', compute='_compute_hide_manual_currency_rate', store=True, default=lambda x: x.company_id.currency_id == x.currency_id)
    manual_currency_rate = fields.Float('Manual currency rate')

    @api.multi
    @api.depends('currency_id', 'company_id.currency_id')
    def _compute_hide_manual_currency_rate(self):
        for record in self:
            record.hide_manual_currency_rate = True if record.currency_id == record.company_id.currency_id else False
   
    @api.onchange('manual_currency_rate')
    def set_line_manual_currency_rate(self):
        for obj in self:
            obj.line_ids.write({'manual_currency_rate': obj.manual_currency_rate})