# -*- coding: utf-8 -*-
from odoo import api, fields, models


class AccountBankStatementLine(models.Model):
    _inherit = 'account.bank.statement.line'

    hide_manual_currency_rate = fields.Boolean('Hide manual currency rate', related='statement_id.hide_manual_currency_rate', readonly=True)
    manual_currency_rate = fields.Float('Manual currency rate', default=lambda x: x.statement_id.manual_currency_rate)

    def process_reconciliation(self, counterpart_aml_dicts=None, payment_aml_rec=None, new_aml_dicts=None):
        # @Override: Мөр дээр тохируулсан албан бус ханшаас ханшийн тооцоолол хийгддэг болгов.
        res = super(AccountBankStatementLine, self).process_reconciliation(counterpart_aml_dicts, payment_aml_rec, new_aml_dicts)
        if res and not self.hide_manual_currency_rate and self.manual_currency_rate:
            for move in res:
                move.button_cancel()
                diff = False
                if self.amount > 0:
                    line = move.line_ids.filtered(lambda l: l.account_id == self.account_id)
                    diff = line.credit + line.amount_residual_currency * self.manual_currency_rate
                    line.write({
                        'amount_residual': line.amount_residual_currency * self.manual_currency_rate,
                        'credit': -line.amount_residual_currency * self.manual_currency_rate,
                        'credit_cash_basis': line.amount_residual_currency * self.manual_currency_rate,
                        'balance_cash_basis': line.amount_residual_currency * self.manual_currency_rate,
                        'balance': line.amount_residual_currency * self.manual_currency_rate,
                        'currency_rate': self.manual_currency_rate,
                    })
                    if diff:
                        move.line_ids.create({
                            'statement_id': self.statement_id.id,
                            'journal_id': self.statement_id.journal_id.id,
                            'currency_id': self.statement_id.company_id.currency_id.id,
                            'date_maturity': self.date,
                            'company_id': self.statement_id.company_id.id,
                            'debit': -diff if diff < 0 else 0,
                            'credit': diff if diff > 0 else 0,
                            'account_id': self.statement_id.company_id.account_to_id.id if diff < 0 else self.statement_id.company_id.account_from_id.id,
                            'date': self.date,
                            'move_id': move.id,
                            'name': self.name,
                        })
                elif self.amount < 0:
                    line = move.line_ids.filtered(lambda l: l.account_id == self.account_id)
                    diff = line.debit - line.amount_currency * self.manual_currency_rate
                    line.write({
                        'debit': line.amount_currency * self.manual_currency_rate,
                        'debit_cash_basis': line.amount_currency * self.manual_currency_rate,
                        'balance_cash_basis': line.amount_currency * self.manual_currency_rate,
                        'balance': line.amount_currency * self.manual_currency_rate,
                        'company_currency': line.amount_currency * self.manual_currency_rate,
                        'currency_rate': self.manual_currency_rate,
                    })
                    if diff:
                        move.line_ids.create({
                            'statement_id': self.statement_id.id,
                            'journal_id': self.statement_id.journal_id.id,
                            'currency_id': self.statement_id.company_id.currency_id.id,
                            'date_maturity': self.date,
                            'company_id': self.statement_id.company_id.id,
                            'debit': diff if diff > 0 else 0,
                            'credit': -diff if diff < 0 else 0,
                            'account_id': self.statement_id.company_id.account_from_id.id if diff < 0 else self.statement_id.company_id.account_to_id.id,
                            'date': self.date,
                            'move_id': move.id,
                            'name': self.name,
                        })
                move.post()
        return res
