# -*- coding: utf-8 -*-
{
    'name': 'Mongolian Proportional Deduction Vat Calculation',
    'version': '1.0',
    'depends': ['l10n_mn_account_tax_report'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """ Хувь тэнцүүлэн хасагдуулах НӨАТ-н тооцоолол """,
    'data': [
        'security/ir.model.access.csv',
        'data/ir_sequence.xml',
        'views/account_tax_view.xml',
        'views/account_proportional_deduction_vat_view.xml',
        'views/account_asset_view.xml',
        'wizard/account_deduction_vat_create_view.xml',
    ],
    "auto_install": False,
    "installable": True,
}