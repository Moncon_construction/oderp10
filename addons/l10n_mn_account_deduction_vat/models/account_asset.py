# -*- coding: utf-8 -*-
from odoo import fields, models, api, _


class AccountAssetAsset(models.Model):
    _inherit = 'account.asset.asset'

    deduction_vat_count = fields.Integer(compute='_deduction_vat_count', string='# Asset Deduction Vat')

    @api.multi
    def set_to_open(self):
        for asset in self:
            deduction_vats = self.env['account.proportional.deduction.vat'].search([('asset_id', '=', asset.id)])
            for deduction_vat in deduction_vats:
                deduction_vat.action_asset_open()
        res = super(AccountAssetAsset, self).set_to_open()
        return res

    @api.multi
    def _deduction_vat_count(self):
        for asset in self:
            res = self.env['account.proportional.deduction.vat'].search_count([('asset_id', '=', asset.id)])
            asset.deduction_vat_count = res or 0

    @api.multi
    def action_view_deduction_vat(self):
        action = self.env.ref('l10n_mn_account_deduction_vat.action_account_proportional_deduction_vat')
        result = action.read()[0]
        result.pop('id', None)
        result['context'] = {}
        vat_ids = self.env['account.proportional.deduction.vat'].search([('asset_id', '=', self.id)]).ids
        if len(vat_ids) > 1 or len(vat_ids) == 0:
            result['domain'] = "[('id','in',[" + ','.join(map(str, vat_ids)) + "])]"
        elif len(vat_ids) == 1:
            res = self.env.ref('l10n_mn_account_deduction_vat.account_proportional_deduction_vat_form', False)
            result['views'] = [(res and res.id or False, 'form')]
            result['res_id'] = vat_ids and vat_ids[0] or False
        return result