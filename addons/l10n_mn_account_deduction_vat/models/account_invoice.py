# -*- coding: utf-8 -*-
from odoo import models, api, fields, _
from odoo.exceptions import UserError


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    def _prepare_tax_line_vals(self, line, tax):
        # account.invoice.tax дээр Хөрөнгийн хувь тэнцүүлэн хасагдуулах НӨАТ-н данс сонгодог болгосон
        res = super(AccountInvoice, self)._prepare_tax_line_vals(line, tax)
        tax = self.env['account.tax'].browse(tax['id'])
        if tax.type_tax_use == 'purchase' and tax.tax_report_type == '4' and tax.purchase_vat_account_id and line.asset_category_id:
            res.update({'account_id': tax.purchase_vat_account_id.id})
        return res

    @api.multi
    def action_move_create(self):
        res = super(AccountInvoice, self).action_move_create()
        context = dict(self.env.context)
        # Энэхүү context нь хөрөнгийх биш нэхэмжлэлийх тул устгагдах ёстой.
        # Үгүй бол нэхэмжлэлийн төрлөөр хөрөнгө үүсгэх гэж оролдох болно.
        context.pop('default_type', None)
        for inv in self:
            inv.invoice_line_ids.with_context(context).deduction_vat_create()
        return res

    @api.multi
    def action_cancel(self):
        for inv in self:
            if len(inv.invoice_line_ids) > 0:
                # for line in inv.invoice_line_ids:
                #     line.write({'create_deduction_vat': False})
                deduction_vats = self.env['account.proportional.deduction.vat'].search([('invoice_line_id', 'in', [line.id for line in inv.invoice_line_ids])])
                for deduction_vat in deduction_vats:
                    if deduction_vat.state != 'draft':
                        raise UserError(_('You cannot cancel an invoice. You should draft %s deduction vat calculation.' % deduction_vat.name))
                    deduction_vat.invoice_line_id.write({'create_deduction_vat': False})
                    deduction_vat.write({'invoice_line_id': False})
                    deduction_vat.unlink()
                deduction_vats = self.env['account.proportional.deduction.vat'].search([('invoice_line_ids', 'in', [line.id for line in inv.invoice_line_ids])])
                if len(deduction_vats) > 0:
                    raise UserError(_("You cannot cancel an invoice. You should draft deduction vat calculation!"))
        res = super(AccountInvoice, self).action_cancel()
        return res

class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'

    create_deduction_vat = fields.Boolean('Create Deduction Vat', default=False)

    def _get_deduction_vat(self, taxes):
        vat = 0
        tax_id = False
        sign = self.invoice_id.type in ['in_refund', 'out_refund'] and -1 or 1
        if taxes and taxes['taxes']:
            for tax in taxes['taxes']:
                deduction_tax = self.env['account.tax'].browse(tax['id'])
                if deduction_tax.type_tax_use == 'purchase' and deduction_tax.tax_report_type == '4' \
                        and deduction_tax.purchase_vat_account_id and self.asset_category_id.tax_report_type not in ('progress_building','exploration_cost'):
                    vat = tax['amount']
                    tax_id = tax['id']
        if vat != 0 and self.invoice_id.currency_id and self.invoice_id.company_id and self.invoice_id.currency_id != self.invoice_id.company_id.currency_id:
            vat = self.invoice_id.currency_id.with_context(date=self.invoice_id._get_currency_rate_date()).compute(vat, self.invoice_id.company_id.currency_id)
        return vat * sign, tax_id

    def _prepare_deduction_vat(self, amount_no_vat, vat, asset, tax_id):
        period = self.env['account.period'].search([('company_id', '=', self.company_id.id), ('date_start', '<=', self.invoice_id.date_invoice),
                                                    ('date_stop', '>=', self.invoice_id.date_invoice)], limit=1)
        return {'partner_id': self.invoice_id.partner_id.id,
                'date': self.invoice_id.date_invoice,
                'invoice_id': self.invoice_id.id,
                'invoice_date': self.invoice_id.date_invoice,
                'invoice_line_id': self.id,
                'asset_category_id': self.asset_category_id.id,
                'asset_id': asset,
                'tax_report_type': self.asset_category_id.tax_report_type,
                'tax_id': tax_id,
                'amount_no_vat': amount_no_vat,
                'vat': vat,
                'amount_vat': amount_no_vat + vat,
                'vat_indication_id': self.asset_category_id.vat_indication_id and self.asset_category_id.vat_indication_id.id or False,
                'deduction_vat_number': self.asset_category_id.deduction_vat_number if self.asset_category_id.deduction_vat_number > 0 else 1,
                'deduction_start_period_id': period[0].id if period else False,
                }

    def _get_asset(self):
        self._cr.execute("""SELECT id FROM account_asset_asset 
                            WHERE invoice_line_id = %s AND id NOT IN (SELECT asset_id FROM account_proportional_deduction_vat WHERE invoice_line_id = %s AND asset_id IS NOT NULL) 
                            ORDER BY id LIMIT 1""", (self.id, self.id))
        asset = self._cr.fetchone()
        return asset

    @api.one
    def deduction_vat_create(self):
        vat_ids = []
        if self.asset_category_id:
            taxes, price = self._get_taxes(False)
            price_subtotal_signed = self._get_price_subtotal(taxes, price)
            vat, tax_id = self._get_deduction_vat(taxes)
            i = 0
            # Тоо хэмжээнээс хамаарч олон хөрөнгө үүсгэдэг болгов.
            while i < self.quantity:
                asset = self._get_asset()
                if vat > 0 and tax_id:
                    deduction_vat_vals = self._prepare_deduction_vat(price_subtotal_signed, vat, asset, tax_id)
                    vat_id = self.env['account.proportional.deduction.vat'].create(deduction_vat_vals)
                    vat_ids.append(vat_id.id)
                i += 1
            self.write({'create_deduction_vat': True})
        return vat_ids