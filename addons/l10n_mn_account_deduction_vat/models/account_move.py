# -*- coding: utf-8 -*-
from odoo import api, models, _
from odoo.exceptions import UserError

class AccountMove(models.Model):
    _inherit = "account.move"

    @api.multi
    def button_cancel(self):
        if not self.env.context.get('deduction_vat', False):
            for move in self:
                lines = self.env['account.proportional.deduction.vat.line'].search([('account_move_id', '=', move.id)])
                if lines:
                    raise UserError(_("Move cannot be canceled if linked to an proportional deduction vat calculation. (Calculation: %s - Move ID:%s)") % (lines[0].deduction_vat_id.name, move.name))
        res = super(AccountMove, self).button_cancel()
        return res