# -*- coding: utf-8 -*-
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta

from odoo import fields, models, api, _
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF, float_round


class AccountProportionalDeductionVat(models.Model):
    _name = 'account.proportional.deduction.vat'
    _inherit = ['mail.thread']
    _description = 'Proportional Deduction Vat Calculation'

    name = fields.Char('Name', default=lambda self: self.env['ir.sequence'].get('account.proportional.deduction.vat') or '/', required=True)
    date = fields.Date('Date', required=True, default=fields.Date.context_today, readonly=True, states={'draft': [('readonly', False)]}, track_visibility='always')
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    partner_id = fields.Many2one('res.partner', 'Supplier', readonly=True, states={'draft': [('readonly', False)]}, track_visibility='always')
    invoice_id = fields.Many2one('account.invoice', 'Account Invoice')
    invoice_date = fields.Date('Invoice Date')
    invoice_line_id = fields.Many2one('account.invoice.line', 'Account Invoice Line')
    invoice_line_ids = fields.Many2many('account.invoice.line', string='Account Invoice Lines')
    asset_category_id = fields.Many2one('account.asset.category', 'Asset Category', readonly=True, states={'draft': [('readonly', False)]}, track_visibility='always')
    asset_id = fields.Many2one('account.asset.asset', 'Asset', readonly=True, states={'draft': [('readonly', False)]}, track_visibility='always')
    tax_report_type = fields.Selection([('building', 'Building'),
                                        ('equipment', 'Equipment'),
                                        ('vehicle', 'Vehicle'),
                                        ('progress_building', 'Building in Progress'),
                                        ('exploration_cost', 'Exploration Cost'),
                                        ('other', 'Other Asset')], string='Tax Report Type', readonly=True, states={'draft': [('readonly', False)]}, track_visibility='always')
    tax_id = fields.Many2one('account.tax', 'Tax', readonly=True, states={'draft': [('readonly', False)]}, track_visibility='always')
    amount_no_vat = fields.Float(string='No Vat Amount', readonly=True, states={'draft': [('readonly', False)]}, track_visibility='always')
    vat = fields.Float(string='Vat', readonly=True, states={'draft': [('readonly', False)]}, track_visibility='always')
    amount_vat = fields.Float(compute='_compute_amount_vat', string='Vat Amount')
    residual_amount_no_vat = fields.Float(compute='_compute_amount_vat', string='Residual No Vat Amount')
    residual_amount_vat = fields.Float(compute='_compute_amount_vat', string='Residual Vat Amount')
    vat_indication_id = fields.Many2one('vat.indication', 'Vat Indication', readonly=True, states={'draft': [('readonly', False)]}, track_visibility='always')
    deduction_vat_number = fields.Integer(string='Deduction Vat Number /month/', default=1, readonly=True, states={'draft': [('readonly', False)]}, track_visibility='always')
    deduction_start_period_id = fields.Many2one('account.period', 'Deduction Start Period', required=True, readonly=True, states={'draft': [('readonly', False)]}, track_visibility='always')
    deduction_vat = fields.Float(compute='_compute_deduction_vat', string='Deduction Vat',)
    no_deduction_vat = fields.Float(compute='_compute_deduction_vat', string='No Deduction Vat')
    line_ids = fields.One2many('account.proportional.deduction.vat.line', 'deduction_vat_id', 'Lines')
    state = fields.Selection([('draft', 'Draft'),
                              ('deduct', 'Deducting'),
                              ('done', 'Deduct Done'),
                              ('sale', 'Sale')], default='draft', string='State')
    description = fields.Text('Description')

    @api.one
    @api.depends('amount_no_vat', 'vat', 'line_ids.move_check')
    def _compute_amount_vat(self):
        amount_no_vat = amount_vat = 0
        self.amount_vat = self.amount_no_vat + self.vat
        for line in self.line_ids:
            if line.move_check:
                amount_no_vat += line.amount_no_vat
                amount_vat += line.amount_vat
        self.residual_amount_no_vat = self.amount_no_vat - amount_no_vat
        self.residual_amount_vat = self.amount_vat - amount_vat

    @api.one
    @api.depends('vat', 'line_ids.move_check', 'line_ids.deduction_vat')
    def _compute_deduction_vat(self):
        total_amount = 0.0
        for line in self.line_ids:
            if line.move_check:
                total_amount += line.deduction_vat
        self.deduction_vat = total_amount
        self.no_deduction_vat = self.vat - total_amount

    @api.onchange('company_id')
    def onchange_domain(self):
        if self.company_id:
            self.env.cr.execute("SELECT asset_id FROM account_proportional_deduction_vat WHERE asset_id IS NOT NULL")
            assets = self.env.cr.fetchall()
            return {'domain': {'asset_id':  [('state', '=', 'open'), ('id', 'not in', [x[0] for x in assets]), ('company_id', '=', self.env.user.company_id.id)]}}
        else:
            return {}

    @api.onchange('date')
    def onchange_date(self):
        if self.date:
            period = self.env['account.period'].search([('company_id', '=', self.company_id.id), ('date_start', '<=', self.date), ('date_stop', '>=', self.date)], limit=1)
            self.deduction_start_period_id = period[0].id if period else False,

    @api.onchange('asset_id')
    def onchange_asset_id(self):
        if self.asset_id:
            self.asset_category_id = self.asset_id.category_id and self.asset_id.category_id.id or False

    @api.onchange('tax_report_type')
    def onchange_tax_report_type(self):
        if self.tax_report_type == 'building':
            self.deduction_vat_number = 120
        elif self.tax_report_type == 'equipment':
            self.deduction_vat_number = 60
        else:
            self.deduction_vat_number = 1

    @api.model
    def create(self, vals):
        deduction_vat = super(AccountProportionalDeductionVat, self).create(vals)
        deduction_vat.sudo().compute_deduction_vat()
        return deduction_vat

    @api.multi
    def write(self, vals):
        res = super(AccountProportionalDeductionVat, self).write(vals)
        if 'line_ids' not in vals and 'state' not in vals and 'invoice_line_id' not in vals:
            for rec in self:
                rec.compute_deduction_vat()
        return res

    @api.multi
    def unlink(self):
        for obj in self:
            if obj.state != 'draft':
                raise UserError(_('You cannot delete an calculation which is not draft. You should draft it instead.'))
            elif obj.invoice_line_id:
                raise UserError(_("You cannot unlink the proceeds from the invoice. First %s invoice cancel" % obj.invoice_id.name))
            elif obj.invoice_line_ids:
                raise UserError(_("You cannot unlink the proceeds from the invoice lines."))
        return super(AccountProportionalDeductionVat, self).unlink()

    @api.multi
    def last_day_of_month(self, date):
        # Тухайн огноонооны сарын сүүлчийн өдрийн огноог буцаах
        next_month = date.replace(day=28) + timedelta(days=4)  # this will never fail
        return next_month - timedelta(days=next_month.day)

    @api.multi
    def compute_deduction_vat(self):
        # Мөрүүд үүсгэх
        self.ensure_one()
        commands = []
        precision = self.env['decimal.precision'].precision_get('Account')
        posted_line_ids = self.line_ids.filtered(lambda x: x.move_check).sorted(key=lambda l: l.date)
        unposted_line_ids = self.line_ids.filtered(lambda x: not x.move_check)
        if len(unposted_line_ids) > 0:
            # Журналын бичилт үүсээгүй мөрүүдийг устгах
            self._cr.execute("DELETE FROM account_proportional_deduction_vat_line where id in (" + ",".join(map(str, unposted_line_ids.ids)) + ") ")
        if self.no_deduction_vat != 0.0:
            number = self.deduction_vat_number - len(posted_line_ids)
            residual_no_deduction = self.no_deduction_vat
            residual_vat = self.residual_amount_vat
            residual_no_vat = self.residual_amount_no_vat
            no_deduction_vat = float_round(self.no_deduction_vat / number, precision_digits=precision)
            amount_vat = float_round(self.residual_amount_vat / number, precision_digits=precision)
            amount_no_vat = float_round(self.residual_amount_no_vat / number, precision_digits=precision)
            if len(posted_line_ids) > 0:
                last_date = datetime.strptime(posted_line_ids[-1].date, DF).date()
                line_date = last_date + relativedelta(months=+1)
                line_date = self.last_day_of_month(line_date)
            else:
                if not self.deduction_start_period_id:
                    raise UserError(_('Please fill deduction start period field!'))
                line_date = datetime.strptime(self.deduction_start_period_id.date_stop, DF).date()
            for x in range(len(posted_line_ids), self.deduction_vat_number):
                vals = {'date': line_date.strftime(DF),
                        'amount_vat': amount_vat if x + 1 != self.deduction_vat_number else residual_vat,
                        'amount_no_vat': amount_no_vat if x + 1 != self.deduction_vat_number else residual_no_vat,
                        'no_deduction_vat': no_deduction_vat if x + 1 != self.deduction_vat_number else residual_no_deduction,
                       }
                commands.append((0, False, vals))
                # Considering Depr. Period as months
                day = line_date.day
                month = line_date.month
                year = line_date.year
                line_date = date(year, month, day) + relativedelta(months=+1)
                line_date = self.last_day_of_month(line_date)
                residual_no_deduction -= no_deduction_vat
                residual_vat -= amount_vat
                residual_no_vat -= amount_no_vat
        self.write({'line_ids': commands})
        return True

    @api.multi
    def action_check_state(self):
        # Тооцооллын төлвийг шалгах функц
        posted_line_ids = self.line_ids.filtered(lambda x: x.move_check)
        unposted_line_ids = self.line_ids.filtered(lambda x: not x.move_check)
        if len(unposted_line_ids) == 0:
            self.action_done()
        elif len(posted_line_ids) == 0:
            self.action_draft()
        elif len(posted_line_ids) > 0:
            self.action_deduct()
            return self.write({'state': 'deduct'})

    @api.multi
    def action_draft(self):
        # Тооцооллыг ноорог төлөвтэй болгоно
        return self.write({'state': 'draft'})

    @api.multi
    def action_deduct(self):
        # Тооцооллыг хасагдуулж буй төлөвтэй болгоно
        return self.write({'state': 'deduct'})

    @api.multi
    def action_done(self):
        # Тооцооллыг дууссан төлөвтэй болгоно
        return self.write({'state': 'done'})

    @api.multi
    def action_asset_sell(self, date):
        # Хөрөнгиийг борлуулахад тооцооллын мөрүүдийг 1 болгоод тухайн огнооноор журналын бичилт үүсгэнэ
        self.ensure_one()
        posted_line = self.env['account.proportional.deduction.vat.line'].search([('deduction_vat_id', '=', self.id), ('move_check', '=', True)], order='date DESC', limit=1)
        if posted_line and posted_line[0].date > date:
            # Борлуулах огнооноос хойш тооцооллын мөрүүдийг баталсан байвал цуцална уу гэсэн анхааруулга өгнө
            raise UserError(_('You cannot sale %s asset. You should cancel the %s calculation line account move after the %s date.' % (self.asset_id.name, self.name, date)))
        unposted_line_ids = self.line_ids.filtered(lambda x: not x.move_check)
        if len(unposted_line_ids) > 0:
            # Журналын бичилт үүсээгүй мөрүүдийг устгах
            self._cr.execute("DELETE FROM account_proportional_deduction_vat_line where id in (" + ",".join(map(str, unposted_line_ids.ids)) + ") ")
        if self.no_deduction_vat != 0.0:
            vals = {'date': date,
                    'amount_vat': self.residual_amount_vat,
                    'amount_no_vat': self.residual_amount_no_vat,
                    'no_deduction_vat': self.no_deduction_vat,
                    'deduction_vat_id': self.id,
                    'sale_check': True
                    }
            line = self.env['account.proportional.deduction.vat.line'].create(vals)
            line.create_move()
        self.write({'state': 'sale'})
        return True

    def action_asset_open(self):
        # Хөрөнгиийг борлуулахад тооцооллын мөрүүдийг 1 болгоод тухайн огнооноор журналын бичилт үүсгэнэ
        self.ensure_one()
        posted_lines = self.line_ids.filtered(lambda x: x.move_check and x.sale_check)
        if len(posted_lines) > 0:
            posted_lines.cancel_move()
            self.compute_deduction_vat()
        return True


class AccountProportionalDeductionVatLine(models.Model):
    _name = 'account.proportional.deduction.vat.line'
    _description = 'Proportional Deduction Vat Calculation Line'

    date = fields.Date('Date')
    deduction_vat_id = fields.Many2one('account.proportional.deduction.vat', 'Proportional Deduction Vat Calculation', required=True, ondelete='cascade')
    amount_no_vat = fields.Float(string='No Vat Amount')
    amount_vat = fields.Float(string='Vat Amount')
    deduction_vat = fields.Float(string='Deduction Vat')
    no_deduction_vat = fields.Float(string='No Deduction Vat')
    account_move_id = fields.Many2one('account.move', 'Account Move')
    move_check = fields.Boolean('Move Check', default=False)
    sale_check = fields.Boolean('Sale Check', default=False)
    parent_state = fields.Selection(related='deduction_vat_id.state', string='State of Deduction Vat')

    @api.multi
    def unlink(self):
        for line in self:
            if line.parent_state != 'draft':
                raise UserError(_('You can only delete an calculation line if the calculation is in draft state.'))
        return super(AccountProportionalDeductionVatLine, self).unlink()

    @api.multi
    def _prepare_move_line(self, name, journal_id, partner_id, vat_indication, account_id, debit, credit):
        # Журналын мөрийн утга буцаах функц
        return {'name': name,
                'ref': name,
                'account_id': account_id,
                'debit': debit,
                'credit': credit,
                'journal_id': journal_id,
                'partner_id': partner_id,
                'vat_indication_id': vat_indication,
                'date': self.date or fields.Date.context_today(self),
                }

    @api.multi
    def _get_line_move_vals(self):
        # Мөрийг батлахад журналын бичилт үүсгэх утга буцаах функц
        if not self.deduction_vat_id.tax_id:
            raise UserError(_('Please fill tax field!'))
        tax = self.deduction_vat_id.tax_id
        if not tax.purchase_vat_journal_id:
            raise UserError(_('You must set purchase vat journal on the settings of tax %s!' % tax.name))
        if not tax.account_id:
            raise UserError(_('You must set account on the settings of tax %s' % tax.name))
        if not tax.purchase_vat_account_id:
            raise UserError(_('You must set purchase vat account on the settings of tax %s' % tax.name))
        if not tax.partner_id:
            raise UserError(_('You must set partner on the settings of tax %s' % tax.name))
        if not self.deduction_vat_id.vat_indication_id:
            raise UserError(_('You must set vat indication on the this %s calculation' % self.deduction_vat_id.name))
        journal_id = tax.purchase_vat_journal_id.id
        partner_id = tax.partner_id.id
        dt_account_id = tax.account_id.id
        kt_account_id = tax.purchase_vat_account_id.id
        vat_indication = self.deduction_vat_id.vat_indication_id and self.deduction_vat_id.vat_indication_id.id or False
        name = self.deduction_vat_id.asset_id.name + ' %s ' % (self.date or fields.Date.context_today(self)) + _('Asset vat deduction')
        move_line_1 = self._prepare_move_line(name, journal_id, partner_id, vat_indication, dt_account_id, self.no_deduction_vat, 0)
        move_line_2 = self._prepare_move_line(name, journal_id, partner_id, False, kt_account_id, 0, self.no_deduction_vat)
        return {
            'ref': name,
            'date': self.date or fields.Date.context_today(self),
            'journal_id': journal_id,
            'line_ids': [(0, 0, move_line_1), (0, 0, move_line_2)],
        }

    @api.multi
    def create_move(self, post_move=True):
        # Мөрийг батлаж, журналын бичилт үүсгэх
        created_moves = self.env['account.move']
        for line in self:
            deduction_vat = line.deduction_vat_id
            move_vals = line._get_line_move_vals()
            move = self.env['account.move'].create(move_vals)
            line.write({'account_move_id': move.id, 'move_check': True, 'deduction_vat': line.no_deduction_vat, 'no_deduction_vat': 0})
            if deduction_vat.asset_id.category_id.open_asset or move.journal_id.auto_approve:
                move.post()
            deduction_vat.action_check_state()
            created_moves |= move
        return [x.id for x in created_moves]

    @api.multi
    def cancel_move(self):
        # Тухайн мөрийг цуцлан, журналын бичилтийг устгах
        for line in self:
            if line.move_check and line.account_move_id:
                if line.account_move_id.state != 'draft':
                    line.account_move_id.with_context(deduction_vat=True).button_cancel()
                line.account_move_id.unlink()
                line.write({'move_check': False, 'sale_check': False, 'no_deduction_vat': line.deduction_vat, 'deduction_vat': 0})
                line.deduction_vat_id.action_check_state()
        return True