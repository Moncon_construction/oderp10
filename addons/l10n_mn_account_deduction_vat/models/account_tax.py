# -*- coding: utf-8 -*-
from odoo import models, fields


class AccountTax(models.Model):
    _inherit = 'account.tax'

    purchase_vat_account_id = fields.Many2one('account.account', 'Purchase Vat Account')
    purchase_vat_journal_id = fields.Many2one('account.journal', 'Purchase Vat Journal')