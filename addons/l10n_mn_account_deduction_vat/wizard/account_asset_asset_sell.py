# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from datetime import datetime


class AccountAssetAssetSell(models.TransientModel):
    _inherit = 'account.asset.asset.sell'

    @api.multi
    def compute(self):
        for line in self.line_ids:
            if line.asset_id:
                deduction_vats = self.env['account.proportional.deduction.vat'].search([('asset_id', '=', line.asset_id.id)])
                for deduction_vat in deduction_vats:
                    deduction_vat.action_asset_sell(self.date)
        res = super(AccountAssetAssetSell, self).compute()
        return res
