# -*- coding: utf-8 -*-
from odoo import fields, api, models, _
from odoo.exceptions import UserError


class AccountDeductionVatCreate(models.TransientModel):
    _name = 'account.deduction.vat.create'
    _description = 'Proportional Deduction Vat Calculation Create'

    trans_nbr = fields.Integer(string='# of Create', readonly=True)
    tax_report_type = fields.Selection([('progress_building', 'Building in Progress'),
                                        ('exploration_cost', 'Exploration Cost')], string='Tax Report Type', required=True)
    line_ids = fields.One2many('account.deduction.vat.create.line', 'wizard_id', 'Lines')

    @api.model
    def default_get(self, fields):
        context = dict(self._context or {})
        res = super(AccountDeductionVatCreate, self).default_get(fields)
        items = []
        lines = self.env['account.invoice.line'].browse(context.get('active_ids', []))
        for line in lines:
            taxes, price = line._get_taxes(False)
            vat, tax_id = line._get_deduction_vat(taxes)
            if line.invoice_id.type == 'in_invoice' and line.invoice_id.state in ('open', 'paid') and \
                    not line.create_deduction_vat and line.company_id.id == self.env.user.company_id.id and tax_id and vat > 0:
                item = [0, False,
                        {'invoice_line_id': line.id,
                         'asset_category_id': line.asset_category_id and line.asset_category_id.id or False,
                         'tax_report_type': line.asset_category_id and line.asset_category_id.tax_report_type or False,
                       }]
                items.append(item)
        trans_nbr = len(items)
        res.update({'trans_nbr': trans_nbr, 'line_ids': items})
        return res

    @api.onchange('tax_report_type')
    def onchange_tax_report_type(self):
        if self.tax_report_type and len(self.line_ids) > 0:
            for line in self.line_ids:
                if not (line.asset_category_id and line.asset_category_id.tax_report_type not in ('progress_building', 'exploration_cost')):
                    line.tax_report_type = self.tax_report_type

    def _prepare_deduction_vat(self, line_ids, amount_no_vat, vat, tax_id, tax_report_type):
        date = fields.Date.context_today(self)
        period = self.env['account.period'].search([('company_id', '=', self.env.user.company_id.id), ('date_start', '<=', date), ('date_stop', '>=', date)], limit=1)
        return {'date': date,
                'tax_report_type': tax_report_type,
                'tax_id': tax_id,
                'amount_no_vat': amount_no_vat,
                'vat': vat,
                'amount_vat': amount_no_vat + vat,
                'deduction_vat_number': 1,
                'deduction_start_period_id': period[0].id if period else False,
                'invoice_line_ids': [(6, 0, line_ids)]
                }

    @api.multi
    def create_deduction_vat(self):
        # Хувь хэнцүүлэн хасагдуулах НӨАТ-н тооцоолол үүсгэх
        building_line_ids = []
        building_no_vat = building_vat = 0
        exploration_no_vat = exploration_vat = 0
        exploration_line_ids = []
        vat_ids = []
        tax_id = False
        for line in self.line_ids:
            if line.asset_category_id:
                vat_ids.extend(line.invoice_line_id.deduction_vat_create()[0])
            else:
                taxes, price = line.invoice_line_id._get_taxes(True)
                price_subtotal = line.invoice_line_id._get_price_subtotal(taxes, price)
                vat, tax_id = line.invoice_line_id._get_deduction_vat(taxes)
                if line.tax_report_type == 'progress_building':
                    building_no_vat += price_subtotal
                    building_vat += vat
                    building_line_ids.append(line.invoice_line_id.id)
                elif line.tax_report_type == 'exploration_cost':
                    exploration_no_vat += price_subtotal
                    exploration_vat += vat
                    exploration_line_ids.append(line.invoice_line_id.id)
            line.invoice_line_id.write({'create_deduction_vat': True})
        if len(building_line_ids) > 0 and building_vat > 0 and tax_id:
            deduction_vat_vals = self._prepare_deduction_vat(building_line_ids, building_no_vat, building_vat, tax_id, 'progress_building')
            vat_id = self.env['account.proportional.deduction.vat'].create(deduction_vat_vals)
            vat_ids.append(vat_id.id)
        if len(exploration_line_ids) > 0 and exploration_vat > 0 and tax_id:
            deduction_vat_vals = self._prepare_deduction_vat(exploration_line_ids, exploration_no_vat, exploration_vat, tax_id, 'exploration_cost')
            vat_id = self.env['account.proportional.deduction.vat'].create(deduction_vat_vals)
            vat_ids.append(vat_id.id)

        tree_id = self.env.ref("l10n_mn_account_deduction_vat.account_proportional_deduction_vat_tree")
        form_id = self.env.ref("l10n_mn_account_deduction_vat.account_proportional_deduction_vat_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Proportional Deduction Vat Calculation'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.proportional.deduction.vat',
            'domain': [('id', 'in', vat_ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }


class AccountDeductionVatCreateLine(models.TransientModel):
    _name = 'account.deduction.vat.create.line'
    _description = 'Proportional Deduction Vat Calculation Create Line'

    invoice_line_id = fields.Many2one('account.invoice.line', 'Account Invoice Line', readonly=True)
    asset_category_id = fields.Many2one('account.asset.category', 'Asset Category', readonly=True)
    wizard_id = fields.Many2one('account.deduction.vat.create', 'Proportional Deduction Vat Create')
    tax_report_type = fields.Selection([('building', 'Building'),
                                        ('equipment', 'Equipment'),
                                        ('vehicle', 'Vehicle'),
                                        ('progress_building', 'Building in Progress'),
                                        ('exploration_cost', 'Exploration Cost'),
                                        ('other', 'Other Asset')], string='Tax Report Type', required=True)

    @api.onchange('tax_report_type')
    def onchange_tax_report_type(self):
        if not self.asset_category_id and self.tax_report_type not in ('progress_building', 'exploration_cost'):
            self.tax_report_type = 'progress_building'
        elif self.asset_category_id and self.tax_report_type in ('progress_building', 'exploration_cost'):
            self.tax_report_type = 'other'