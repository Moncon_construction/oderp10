# -*- coding: utf-8 -*-
{
    'name': 'Mongolian Proportional Deduction Vat Calculation Purchase Asset',
    'version': '1.0',
    'depends': ['l10n_mn_account_deduction_vat',
                'l10n_mn_purchase_asset'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """Хувь тэнцүүлэн хасагдуулах НӨАТ-н тооцоолол - Хөрөнгийн худалдан авалт""",
    'data': [],
    "auto_install": True,
    "installable": True,
}