# -*- coding: utf-8 -*-
from odoo import models

class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'

    def _get_asset(self):
        asset = False
        if self.purchase_line_id:
            self._cr.execute("""SELECT id FROM account_asset_asset 
                                WHERE po_line_id = %s AND id NOT IN (SELECT asset_id FROM account_proportional_deduction_vat WHERE invoice_line_id = %s AND asset_id IS NOT NULL) 
                                ORDER BY id LIMIT 1""", (self.purchase_line_id.id, self.id))
            asset = self._cr.fetchone()
        return asset

