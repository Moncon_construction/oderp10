# -*- coding: utf-8 -*-
from odoo import models, api, _
from odoo.exceptions import UserError


class ReceiveAssetsWizard(models.TransientModel):
    _inherit = 'receive.assets.wizard'

    @api.multi
    def do_transfer(self):
        self.ensure_one()
        if self.picking_id.state not in ['ready']:
            raise Warning(_('Please confirm asset picking'))
        asset_codes = []
        for item in self.line_ids:
            asset_id = self.env['account.asset.asset'].search([('code', '=', item.code)])
            if asset_id:
                raise UserError(_('This asset code is overlapping: %s') % item.code)
            if item.code not in asset_codes:
                asset_codes.append(item.code)
            else:
                raise UserError(_('This asset code is overlapping: %s') % item.code)
        for item in self.line_ids:
            asset_datas = self.get_asset_data(item)
            asset_id = self.env['account.asset.asset'].create(asset_datas)
            self._cr.execute("""UPDATE account_proportional_deduction_vat SET asset_id = %s
                                            WHERE id = (SELECT id FROM account_proportional_deduction_vat 
                                                        WHERE asset_id IS NULL AND invoice_line_id IN (SELECT id FROM account_invoice_line WHERE purchase_line_id = %s) ORDER BY id LIMIT 1) 
                                         """, (asset_id.id, item.po_line_id.id))
        done = True
        is_all_received = False
        for line in self.picking_id.line_ids:
            asset_ids = self.env['account.asset.asset'].search([('po_line_id', '=', line.po_line_id.id)])
            if line.qty != len(asset_ids):
                done = False
        if done:
            self.picking_id.write({'state': 'done'})
            self.picking_id.signal_workflow('asset_picking_done')
            is_all_received = True
        if self.picking_id.purchase_id:
            self.picking_id.purchase_id.is_product_received = is_all_received
        return True
