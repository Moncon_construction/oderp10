# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################

{
    'name': 'Mongolian Accounting Disclosure Reports',
    'version': '1.0',
    'depends': ['l10n_mn_account', 'l10n_mn_account_period', 'l10n_mn_account_base_report'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """Санхүүгийн тайлангийн тодруулга тайлан""",
    'data': [
        'views/account_disclosure_report_view.xml',
        'views/account_disclosure_view.xml',
        'security/ir.model.access.csv',
    ],
    "auto_install": False,
    "installable": True,
}
