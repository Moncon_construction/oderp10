{
    'name': 'Mongolian Accounting Financial Analysis',
    'version': '1.0',
    'depends': ['l10n_mn_account_tax_report'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """Санхүүгийн тайлангийн тохиргоо""",
    'data': [
        'data/financial_statement_analysis_data.xml',
        'views/financial_analysis_views.xml',
        'security/ir.model.access.csv'
    ],
}
