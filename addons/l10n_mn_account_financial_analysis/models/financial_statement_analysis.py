from odoo import models, fields, api, _
from odoo.tools.translate import _
import time
from odoo import SUPERUSER_ID
from operator import itemgetter
from odoo.tools.safe_eval import safe_eval as eval

ALL_REPORT_SELECTION = [
    ('hva', 'Horizontal and vertical analysis'),
    ('sa', 'Solvency analysis'),
    ('fsa', 'Financial stability analysis'),
    ('aua', 'Asset utilization analysis'),
    ('pa', 'profitability analysis'),
    ('cfa', 'Cash flow analysis')
]


class AccountTaxReportOption(models.Model):
    _name = 'account.financial.statement.analysis'
    _description = 'Financial statement analysis settings'
    _order = 'parent_id, sequence'

    # report = fields.Selection(ALL_REPORT_SELECTION, 'Report', required=True, default='tt02')
    report = fields.Selection(ALL_REPORT_SELECTION, 'Report')
    name = fields.Text('Name', required=True)
    code = fields.Char('Code', size=32)
    type = fields.Selection([('view', 'View'),
                             ('account', 'Select Accounts'),
                             ('account_type', 'Select Account Type'),
                             ('python', 'Python Compute')], 'Type', required=True, default='account')
    balance_type = fields.Selection([('balance', 'Balance')], 'Balance Type', required=True, default='balance')
    company_id = fields.Many2one('res.company', 'Company', required=True, default=lambda self: self.env['res.company']._company_default_get('account.financial.statement.analysis'))
    sequence = fields.Integer('Sequence')
    # tabuud
    account_ids = fields.Many2many('account.account', 'report_option_account', 'op_id', 'acc_id', 'Accounts')
    partner_ids = fields.Many2many('res.partner', 'rep_option_partner_rel', 'op_id', 'part_id', 'Partners')
    parent_id = fields.Many2one('account.financial.statement.analysis', 'Parent Option')
    child_ids = fields.One2many('account.financial.statement.analysis', 'parent_id', 'Child Options')
    account_type_ids = fields.Many2many('account.account.type', 'report_acc_type_rel', 'op_id', 'acc_type_id', 'Account Types')
    python_compute = fields.Text('Python Compute')