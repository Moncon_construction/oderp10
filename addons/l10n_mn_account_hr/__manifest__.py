# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Account on Human Resource",
    'version': '1.0',
    'depends': [
        'l10n_mn_account', 'l10n_mn_hr',
    ],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Account and Human resource',
    'description': """
         Санхүүгийн модуль болон Хүний нөөцийн модулийн хамаарлын модуль
    """,
    'website': 'http://asterisk-tech.mn',
    'data': [
        'views/account_view.xml',
    ],
    'installable': True,
    'auto_install': True,
    'application': True,
}
