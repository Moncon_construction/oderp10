# -*- coding: utf-8 -*-
from odoo import fields, models, _


class AccountAccount(models.Model):
    _inherit = "account.account"
    
    permitted_departments = fields.Many2many('hr.department', 'account_department_rel', 'aid', 'did', string='Permitted Departments')
