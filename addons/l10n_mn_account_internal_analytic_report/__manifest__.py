# -*- coding: utf-8 -*-
# Part of OdErp. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Analytic Account Internal Reports",
    'version': '1.0',
    'depends': ['l10n_mn_account_internal_report'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
      Account Reports
    """,
    'data': [
        'views/internal_profit_report_view.xml', 
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
