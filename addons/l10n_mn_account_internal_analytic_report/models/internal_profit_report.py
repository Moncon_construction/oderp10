# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from io import BytesIO
import base64
import time
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta

class InternalProfitReport(models.Model):
    _inherit='internal.profit.report'

    analytic_account_ids = fields.Many2many('account.analytic.account', 'analytic_account_analytic_internal_profit_report_rel', 'analytic_internal_profit_report_id', 'analytic_account_id', 'Analytic accounts',domain="[('type','=','normal')]")

    #Эхлэх дуусах огнооноос хамааран тайлангийн нэрийг оноох болон өмнөх сартай харьцуулах эсэх товчийг харуулах эсэх утгыг онооно
    @api.onchange('start_date','end_date','company_id')
    def onchange_period_id(self):
        result = super(InternalProfitReport, self).onchange_period_id()
        
        if self.is_analytic == True:
            if self.start_date == self.end_date:
                name = _('%s - %s month Internal Profit Details Analytic Report') % (self.company_id.name, self.start_date)
            else:
                name = _('%s - %s - %s months Internal Profit Details Analytic Report') % (self.company_id.name, self.start_date, self.end_date)
            self.update({'name': name,
                         })
        return result
    
    #Тухайн тайланд тохируулсан бүх санхүүгийн данснуудыг буцаах
    def _get_all_accounts(self, reports,lines):
        res = {}
        line_ids = lines
        for report in reports:
            if report.id in res:
                continue
            if report.type == 'accounts':
                if report.account_ids:
                    res[report.id] =  report.account_ids.ids
                    if  report.account_ids.ids not in line_ids: 
                        line_ids += report.account_ids.ids
            elif report.type == 'account_type':
                accounts = self.env['account.account'].search([('user_type_id', 'in', report.account_type_ids.ids)])
                if accounts:
                    res[report.id] = accounts.ids
                    if  accounts.ids not in line_ids: 
                        line_ids += accounts.ids
            elif report.type == 'account_report' and report.account_report_id:
                res2 = self._get_all_accounts(report.account_report_id,line_ids)
                if type(res2) != list:
                    for key, value in res2.items():
                        
                        if value not in line_ids: 
                            line_ids += value
                            res = line_ids
                else:
                    res = res2
        return res
    
    def _compute_account_balance(self, accounts):
        """ compute the balance for the provided accounts
        """
        result = super(InternalProfitReport, self)._compute_account_balance(accounts)
        if self.is_analytic == True:
            mapping = {
                'balance': "COALESCE(SUM(amount),0) as balance ",
            }
            res = {}
            context = dict(self._context or {})
            filters = ''
            for account in accounts:
                res[account.id] = dict((fn, 0.0) for fn in mapping.keys())
            if context.get('date_to'):
                date_to = context['date_to']
            if context.get('date_from'):
                date_from = context['date_from']
            if context.get('account_analytic_ids'):
                analytic_account_ids = context['account_analytic_ids']
                filters = ' AND account_id in (' + ','.join(map(str, analytic_account_ids)) + ') '
            if accounts:
                request =   "SELECT general_account_id as id, " + ', '.join(mapping.values()) + \
                    " FROM account_analytic_line WHERE " \
                    " date between %s AND %s AND " \
                    " general_account_id in %s " \
                        + filters + \
                    " GROUP BY general_account_id"
                params = date_from,date_to,tuple(accounts._ids),
                self.env.cr.execute(request, params)
                for row in self.env.cr.dictfetchall():
                    res[row['id']] = row
            return res
        else:
            return result
    
    #Мөрийн утгыг цуглуулна
    def get_account_lines(self,data,type):
        result = super(InternalProfitReport, self).get_account_lines(data,type)
        if self.is_analytic == True:
            lines = []
            sub_account_report = []
            account_report = self.env['account.financial.report'].search([('chart_type', '=', 'revenue_results'),('type', '=', 'sum')],order='sequence')
            child_reports = account_report._get_children_by_order()
            for report in child_reports:
                sub_account_report += report
                for rep in report.account_report_id:
                    if rep.chart_type != 'revenue_results':
                        sub_account_report += rep
            res = self.with_context(data.get('used_context'))._compute_report_balance(sub_account_report)
            comparison_res1 = self.with_context(data.get('comparison_context1'))._compute_report_balance(sub_account_report)
            comparison_res2 = self.with_context(data.get('comparison_context2'))._compute_report_balance(sub_account_report)
            before_month_comp = self.with_context(data.get('before_month_comparison_context'))._compute_report_balance(sub_account_report)
            for report in sub_account_report:
                vals = {
                    'name': report.name,
                    'balance': res[report.id]['balance'] * report.sign,
                    'type': report.type,
                    'report_id': report.id,
                    'level': bool(report.style_overwrite) and report.style_overwrite or report.level,
                    'account_type': report.type or False, #used to underline the financial report balances
                    'balance_cmp1':comparison_res1[report.id]['balance'] * report.sign,
                    'balance_cmp2':comparison_res2[report.id]['balance'] * report.sign,
                    'balance_be_month':before_month_comp[report.id]['balance'] * report.sign,
                }
                lines.append(vals)
            return lines
        else:
            return result

    #Тайлангийн мөрийг зурах
    @api.multi
    def compute(self):
        res = super(InternalProfitReport, self).compute()
        if self.is_analytic == True:
            data ={}
            c = -1
            profit_lines = self.env['internal.profit.report.line']
            for profit in self:      
                profit.line_ids.unlink() 
                analytic_account_ids = self.analytic_account_ids.ids 
                date_from  = self.start_date
                date_to  = self.end_date
                date_start = datetime.strptime(date_from, "%Y-%m-%d")
                date_end = datetime.strptime(date_to, "%Y-%m-%d")
                day = date_end.day
                month = date_end.month
                year = date_end.year
                before_month_end = (datetime(year, month, day) - relativedelta(months=1))
                one_year_ago_end = (datetime(year, month, day) - relativedelta(years=1))
                two_year_ago_end = (datetime(year, month, day) - relativedelta(years=2))
                before_month_end = self.last_day_of_month(before_month_end)
                one_year_ago_end = self.last_day_of_month(one_year_ago_end)
                two_year_ago_end = self.last_day_of_month(two_year_ago_end)
                before_month_start = date_start - relativedelta(months=1)
                one_year_ago_start = date_start - relativedelta(years=1)
                two_year_ago_start = date_start - relativedelta(years=2)
                this_month_performance = two_year_ago_performance = one_year_ago_performance = before_month_performance = 0
                pro_coef_before_month_per_percent = pro_coef_one_year_ago_per_percent = pro_coef_two_year_ago_per_percent = 0  
                 
                data['used_context'] = {
                        'date_from':date_from,
                        'date_to': date_to,
                        'account_analytic_ids':analytic_account_ids
                }
                year2_comparison_context =  {'date_from': two_year_ago_start, 'date_to': two_year_ago_end, 'account_analytic_ids':analytic_account_ids}
                year1_comparison_context =  {'date_from': one_year_ago_start, 'date_to': one_year_ago_end, 'account_analytic_ids':analytic_account_ids}
                before_month_comparison_context =  {'date_from': before_month_start, 'date_to': before_month_end, 'account_analytic_ids':analytic_account_ids}
                data['comparison_context1'] = year1_comparison_context
                data['comparison_context2'] = year2_comparison_context
                data['before_month_comparison_context'] = before_month_comparison_context
                profit_report_lines = self.get_account_lines(data,'revenue_results')
                for line in profit_report_lines:
                    if line['type'] != 'sum':
                        this_month_performance = line['balance']
                        two_year_ago_performance = line['balance_cmp2']
                        one_year_ago_performance = line['balance_cmp1']
                        before_month_performance = line['balance_be_month']
                        if before_month_performance != 0:
                            pro_coef_before_month_per_percent = this_month_performance/before_month_performance*100
                        if one_year_ago_performance != 0:
                            pro_coef_one_year_ago_per_percent = this_month_performance/one_year_ago_performance*100
                        if two_year_ago_performance != 0:
                            pro_coef_two_year_ago_per_percent = this_month_performance/two_year_ago_performance*100
                        res = {
                                'name':line['name'],
                                'profit_report_id': profit.id,
                                'report_id': line['report_id'],
                                'this_month_performance':this_month_performance * c,
                                'two_year_ago_performance':two_year_ago_performance * c,
                                'one_year_ago_performance':one_year_ago_performance * c,
                                'before_month_performance':before_month_performance * c,
                                'before_month_per_tug':this_month_performance - before_month_performance * c,
                                'before_month_per_percent':pro_coef_before_month_per_percent * c,
                                'one_year_ago_per_tug':this_month_performance - one_year_ago_performance * c,
                                'one_year_ago_per_percent':pro_coef_one_year_ago_per_percent * c,
                                'two_year_ago_per_tug':this_month_performance - two_year_ago_performance * c,
                                'two_year_ago_per_percent':pro_coef_two_year_ago_per_percent * c,
                        }
                        profit_lines += self.env['internal.profit.report.line'].create(res)
                        pro_coef_before_month_per_percent = pro_coef_one_year_ago_per_percent = pro_coef_two_year_ago_per_percent = 0  
        return res
            
class InternalProfitReportLine(models.Model):
    _inherit = "internal.profit.report.line"
 
    #Шинжилгээний бичилтийн мөрүүдийг буцаах
    def _get_analytic_accounts(self, reports, lines):
        res = {}
        line_ids = lines
        for report in reports:
            if report.id in res:
                continue
            if report.type == 'accounts':
                if report.account_ids:
                    res[report.id] =  self._compute_analytic_account_move_lines(report.account_ids)
                    if type(res) != list:
                        for key, value in res.items():
                            line_ids += value
                            res = line_ids
                    else:
                        res = res[report.id]
                else:
                    res = line_ids
            elif report.type == 'account_type':
                accounts = self.env['account.account'].search([('user_type_id', 'in', report.account_type_ids.ids)])
                if accounts:
                    if type(res) != list:
                        res[report.id] = self._compute_analytic_account_move_lines(accounts)
                        if type(res[report.id]) != list:
                            for key, value in res.items():
                                line_ids += value
                                res = line_ids
                        else:
                            line_ids += res[report.id]
                            res = line_ids
                    else:
                        res += self._compute_analytic_account_move_lines(accounts)
                else:
                    res = line_ids
            elif report.type == 'account_report' and report.account_report_id:
                res2 = self._get_analytic_accounts(report.account_report_id,line_ids)
                if type(res2) != list:
                    for key, value in res2.items():
                        line_ids += value
                        res = line_ids
                else:
                    res = res2
        return res
     
    #Шинжилгээний мөрүүдийг дансаар шүүх олох
    def _compute_analytic_account_move_lines(self, accounts):
        move_line_ids = []
        date_from = self.profit_report_id.start_date
        date_to  = self.profit_report_id.end_date
        analytic_account_ids = self.profit_report_id.analytic_account_ids.ids
        if analytic_account_ids:
            filters = ' AND account_id in (' + ','.join(map(str, analytic_account_ids)) + ') '
        if accounts:
            request =   "SELECT account_analytic_line.id as id " \
                " FROM account_analytic_line WHERE " \
                " date between %s AND %s AND " \
                " general_account_id in %s " \
                    + filters + \
                " GROUP BY account_analytic_line.id"
            params = date_from,date_to,tuple(accounts._ids),
            self.env.cr.execute(request, params)
            move_line_ids = [row['id'] for row in self.env.cr.dictfetchall()]
        return move_line_ids
    
    #Шинжилгээний мөрүүдийг харуулах
    @api.multi
    def open_account_move_lines(self):
        result = super(InternalProfitReportLine, self).open_account_move_lines()
        data = {}
        lines = []
        if self.profit_report_id.is_analytic == True:
            for line in self:
                date_from  = line.profit_report_id.start_date
                date_to  = line.profit_report_id.end_date
                data['used_context'] = {
                        'date_from':date_from,
                        'date_to': date_to,
                }
                move_line_ids = self.with_context(data.get('used_context'))._get_analytic_accounts(line.report_id,lines)
                if move_line_ids == {}:
                    move_line_ids = []
                res = self.env['ir.actions.act_window'].for_xml_id('analytic', 'account_analytic_line_action_entries')
                res['domain'] = [('id', 'in', move_line_ids)]
                res['context'] = {}
            return res
        return result
