# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from io import BytesIO
import base64
import time
from odoo import api, models, _
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from passlib.tests.utils import limit

class InternalProfitReport(models.Model):
    _inherit='internal.profit.report'
    
#    Тайланг хэвлэх
    @api.multi
    def export_report_analytic(self):
        
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Analytic Internal Profit Details Report')
        sheet_name = _('Integration')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        
        # create formats
        
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_filter_center = book.add_format(ReportExcelCellStyles.format_filter_center)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_group = book.add_format(ReportExcelCellStyles.format_group)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_float_color = book.add_format(ReportExcelCellStyles.format_group_float)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_bold_number = book.add_format(ReportExcelCellStyles.format_content_bold_number)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_bold_left = book.add_format(ReportExcelCellStyles.format_content_bold_left)
        format_content_text_color = book.add_format(ReportExcelCellStyles.format_content_text_color)
        format_content_bold_number_color = book.add_format(ReportExcelCellStyles.format_content_bold_number_color)
        format_content_bold_float = book.add_format(ReportExcelCellStyles.format_content_bold_float)
        format_content_percent_bold = book.add_format(ReportExcelCellStyles.format_content_percent_bold)
        format_content_center_text_color = book.add_format(ReportExcelCellStyles.format_content_center_text_color)
        format_content_percent = book.add_format(ReportExcelCellStyles.format_content_percent)
        format_content_percent_color = book.add_format(ReportExcelCellStyles.format_content_percent_color)
        seq = 1
        rowx = 0
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('internal_profit_details_analytic_report'), form_title=file_name).create({})

#         create sheet
        sheet = book.add_worksheet(sheet_name)
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.39, 0.39, 0.39, 0.39)  # 1cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        
        # compute column
        sheet.set_column('A:A', 3)
        sheet.set_column('B:B', 30)
        sheet.set_column('C:C', 15)
        sheet.set_column('D:D', 15)
        sheet.set_column('E:E', 15)
        sheet.set_column('F:F', 13)
        sheet.set_column('G:G', 13)
        sheet.set_column('H:H', 13)
        sheet.set_column('I:I', 13)
        sheet.set_column('J:J', 13)
        sheet.set_column('K:K', 13)
        sheet.set_column('L:L', 13)
        sheet.set_column('M:M', 13)
        sheet.set_column('N:N', 13)
        sheet.set_column('O:O', 13)
        sheet.set_column('P:P', 13)
        sheet.set_column('Q:Q', 13)
        
        # create employee name
        executives_names = ''
        accountants_names = ''
        company = self.company_id
        executives_employees = []
        accountants_employees = []
        date_from  = self.start_date
        date_to  = self.end_date
        date_start = datetime.strptime(date_from, "%Y-%m-%d")
        date_end = datetime.strptime(date_to, "%Y-%m-%d")
        day = date_end.day
        month = date_end.month
        year = date_end.year
        before_month_end = (datetime(year, month, day) - relativedelta(months=1))
        one_year_ago_end = (datetime(year, month, day) - relativedelta(years=1))
        two_year_ago_end = (datetime(year, month, day) - relativedelta(years=2))
        before_month_end = self.last_day_of_month(before_month_end)
        one_year_ago_end = self.last_day_of_month(one_year_ago_end)
        two_year_ago_end = self.last_day_of_month(two_year_ago_end)
        before_month_start = date_start - relativedelta(months=1)
        one_year_ago_start = date_start - relativedelta(years=1)
        two_year_ago_start = date_start - relativedelta(years=2)
        if company.executive_signature:
            executives_employees = self.env['hr.employee'].search([('user_id', '=', company.executive_signature.id)])  
        if company.genaral_accountant_signature:
            accountants_employees = self.env['hr.employee'].search([('user_id', '=', company.genaral_accountant_signature.id)])
        
        for employee in executives_employees:
            if employee.last_name:
                executives_names = employee.last_name[0]
            
            executives_names += '.%s' % (employee.name,)
            if employee.job_id.name:
                executives_names += ',  %s' % (employee.job_id.name,)
            
        for employee in accountants_employees:
            if employee.last_name:
                accountants_names = employee.last_name[0]
            
            accountants_names += '.%s' % (employee.name,)
            if employee.job_id.name:
                accountants_names += ',  %s' % (employee.job_id.name,)
                
        analytics = ''
        analytic_account_ids = []
        # Шинжилгээний данс сонгоогүй тохиолдолд тухайн огнооны бүх шинжилгээний дансыг олж дүнг харуулах
        if self.analytic_account_ids:
            analytic_account_ids = self.analytic_account_ids
        else:
            lines = []
            account_report = self.env['account.financial.report'].search([('chart_type', '=', 'revenue_results'),('type', '=', 'sum')],order='sequence')
            child_reports = account_report._get_children_by_order()
            accounts = self._get_all_accounts(child_reports,lines)
            if accounts:
                request =   "SELECT account_analytic_line.account_id as id " \
                    " FROM account_analytic_line WHERE " \
                    " date between %s AND %s AND " \
                    " general_account_id in %s " \
                    " GROUP BY account_analytic_line.account_id"
                params = date_from,date_to,tuple(accounts),
                self.env.cr.execute(request, params)
                analytic_account_ids = [row['id'] for row in self.env.cr.dictfetchall()]
            analytic_account_ids = self.env['account.analytic.account'].search([('id', 'in', analytic_account_ids)]) 
        for analytic in analytic_account_ids:
            if analytics:
                analytics += ', '
            analytics += analytic.name 
            
        #Compute header column
        name_col = 7
        if self.is_compare_two_year_ago and self.is_compare_one_year_ago and self.is_compare_before_month:
            name_col = 11
        if self.is_compare_two_year_ago and self.is_compare_one_year_ago and not self.is_compare_before_month:
            name_col = 8
        if self.is_compare_two_year_ago and  not self.is_compare_one_year_ago and self.is_compare_before_month:
            name_col = 8
        if not self.is_compare_two_year_ago and  self.is_compare_one_year_ago and self.is_compare_before_month:
            name_col = 8
        if self.is_compare_two_year_ago and not self.is_compare_one_year_ago and not self.is_compare_before_month:
            name_col = 5
        if not self.is_compare_two_year_ago and self.is_compare_one_year_ago and not self.is_compare_before_month:
            name_col = 5
        if not self.is_compare_two_year_ago and not self.is_compare_one_year_ago and self.is_compare_before_month:
            name_col = 5
        
        sheet.write(rowx, 0, '%s: %s' % (_('Company'), self.company_id.name), format_filter)
        rowx += 3
        sheet.merge_range(rowx, 0, rowx, name_col, report_name.upper(), format_name)
        rowx += 2
        
        sheet.write(rowx + 1, 0, '%s: %s - %s' % (_('Duration'), self.start_date, self.end_date), format_filter)
        sheet.write(rowx + 2, 0, '%s: %s' % (_('Created On'), time.strftime('%Y-%m-%d')), format_filter) 
        sheet.write(rowx + 3, 0, '%s:( %s )' % (_('Analytic accounts:Merged'), analytics), format_filter)
        rowx += 5
        # table header
            
        col = 0
        sheet.merge_range(rowx, col, rowx + 1, col, _('№'), format_group)
        col += 1
        sheet.merge_range(rowx, col, rowx + 1, col, _('Reports'), format_group)
        
        col += 1
        if self.is_compare_two_year_ago:
            sheet.merge_range(rowx, col, rowx + 1, col, _('2 years ago performance'), format_group)
            col += 1
        if self.is_compare_one_year_ago:
            sheet.merge_range(rowx, col, rowx + 1, col, _('1 years ago performance'), format_group)
            col += 1
        if self.is_compare_before_month:
            sheet.merge_range(rowx, col, rowx + 1, col, _('Before month performance'), format_group)
            col += 1        
        sheet.merge_range(rowx, col, rowx + 1, col, _('Performance'), format_group)
        col += 1        
        if self.is_compare_before_month:
            sheet.merge_range(rowx, col, rowx, col + 1, _('Before Month'), format_group)
            sheet.write(rowx + 1, col, _('In ₮'), format_group)
            sheet.write(rowx + 1, col + 1, _('In %'), format_group)
            col += 2
        if self.is_compare_one_year_ago:
            sheet.merge_range(rowx, col, rowx, col + 1, _('1 year ago performance'), format_group)
            sheet.write(rowx + 1, col, _('In ₮'), format_group)
            sheet.write(rowx + 1, col + 1, _('In %'), format_group)
            col += 2
        if self.is_compare_two_year_ago:
            sheet.merge_range(rowx, col, rowx, col + 1, _('2 year ago performance'), format_group)
            sheet.write(rowx + 1, col, _('In ₮'), format_group)
            sheet.write(rowx + 1, col + 1, _('In %'), format_group)
            col += 2             
        rowx += 2
        
        col = seq = sub_seq = 0
        for line in self.line_ids:
            col = 0
            if line.report_id.style_overwrite == 2: 
                format_float = format_content_float_color
                format_content = format_content_text_color
                format_percent = format_content_percent_color
                format_number = format_content_bold_number_color
            else:                
                format_float = format_content_float
                format_content = format_content_text
                format_percent = format_content_percent
                format_number = format_content_number
            if line.report_id.chart_type == 'revenue_results':
                seq += 1
                if line.report_id.style_overwrite != 2:
                    format_content = format_content_bold_left
                    format_float = format_content_bold_float
                    format_percent = format_content_percent_bold
                    format_number = format_content_bold_number
                sheet.write(rowx, col, seq, format_number)
                sub_seq = 0 
            else:
                sub_seq += 1
                sheet.write(rowx, col, str(seq) + '.' + str(sub_seq), format_number)
            col += 1 
            sheet.write(rowx, col, line.report_id.name, format_content) 
            col += 1 
            if self.is_compare_two_year_ago:
                sheet.write(rowx, col, line.two_year_ago_performance, format_float)
                col += 1
            if self.is_compare_one_year_ago:
                sheet.write(rowx, col, line.one_year_ago_performance, format_float)
                col += 1
            if self.is_compare_before_month:
                sheet.write(rowx, col, line.before_month_performance, format_float)
                col += 1   
            sheet.write(rowx, col, line.this_month_performance, format_float)
            col += 1  
            if self.is_compare_before_month:
                sheet.write(rowx, col, line.before_month_per_tug, format_float)                
                sheet.write(rowx, col + 1, line.before_month_per_percent/100, format_percent)
                col += 2
            if self.is_compare_one_year_ago:
                sheet.write(rowx, col, line.one_year_ago_per_tug, format_float)                
                sheet.write(rowx, col + 1, line.one_year_ago_per_percent/100, format_percent)
                col += 2
            if self.is_compare_two_year_ago:
                sheet.write(rowx, col, line.two_year_ago_per_tug, format_float)                
                sheet.write(rowx, col + 1, line.two_year_ago_per_percent/100, format_percent)
                col += 2  
    
            rowx += 1
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, name_col, _('Executive Director:..................................../%s/')%(executives_names),format_filter_center)
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, name_col, _('Accountant:..................................../%s/')%(accountants_names),format_filter_center)
        sheet.hide_gridlines(2) 
        
        for analytic in analytic_account_ids:
            if analytic.type == 'normal':
                # compute column
                name = analytic.name
                analytic_ids = analytic.ids
                sheet = book.add_worksheet(name)
                sheet.set_paper(9)  # A4
                sheet.set_margins(0.39, 0.39, 0.39, 0.39)  # 1cm, 1cm, 1cm, 1cm
                sheet.fit_to_pages(1, 0)
                
                # compute column
                sheet.set_column('A:A', 3)
                sheet.set_column('B:B', 30)
                sheet.set_column('C:C', 15)
                sheet.set_column('D:D', 15)
                sheet.set_column('E:E', 15)
                sheet.set_column('F:F', 13)
                sheet.set_column('G:G', 13)
                sheet.set_column('H:H', 13)
                sheet.set_column('I:I', 13)
                sheet.set_column('J:J', 13)
                sheet.set_column('K:K', 13)
                sheet.set_column('L:L', 13)
                sheet.set_column('M:M', 13)
                sheet.set_column('N:N', 13)
                sheet.set_column('O:O', 13)
                sheet.set_column('P:P', 13)
                sheet.set_column('Q:Q', 13)
                
                data ={}
                rowx = 0
                col = 0
                this_month_performance = two_year_ago_performance = one_year_ago_performance = before_month_performance = 0
                before_month_per_percent = one_year_ago_per_percent = two_year_ago_per_percent = 0  
                
                data['used_context'] = {
                        'date_from':date_from,
                        'date_to': date_to,
                        'account_analytic_ids':analytic_ids
                }
                year2_comparison_context =  {'date_from': two_year_ago_start, 'date_to': two_year_ago_end, 'account_analytic_ids':analytic_ids}
                year1_comparison_context =  {'date_from': one_year_ago_start, 'date_to': one_year_ago_end, 'account_analytic_ids':analytic_ids}
                before_month_comparison_context =  {'date_from': before_month_start, 'date_to': before_month_end, 'account_analytic_ids':analytic_ids}
                data['comparison_context1'] = year1_comparison_context
                data['comparison_context2'] = year2_comparison_context
                data['before_month_comparison_context'] = before_month_comparison_context
                
                #Compute header column
                name_col = 7
                if self.is_compare_two_year_ago and self.is_compare_one_year_ago and self.is_compare_before_month:
                    name_col = 11
                if self.is_compare_two_year_ago and self.is_compare_one_year_ago and not self.is_compare_before_month:
                    name_col = 8
                if self.is_compare_two_year_ago and  not self.is_compare_one_year_ago and self.is_compare_before_month:
                    name_col = 8
                if not self.is_compare_two_year_ago and  self.is_compare_one_year_ago and self.is_compare_before_month:
                    name_col = 8
                if self.is_compare_two_year_ago and not self.is_compare_one_year_ago and not self.is_compare_before_month:
                    name_col = 5
                if not self.is_compare_two_year_ago and self.is_compare_one_year_ago and not self.is_compare_before_month:
                    name_col = 5
                if not self.is_compare_two_year_ago and not self.is_compare_one_year_ago and self.is_compare_before_month:
                    name_col = 5
                
                sheet.write(rowx, 0, '%s: %s' % (_('Company'), self.company_id.name), format_filter)
                rowx += 3
                sheet.merge_range(rowx, 0, rowx, name_col, report_name.upper(), format_name)
                rowx += 2
                sheet.write(rowx + 1, 0, '%s: %s - %s' % (_('Duration'), self.start_date, self.end_date), format_filter)
                sheet.write(rowx + 2, 0, '%s: %s' % (_('Created On'), time.strftime('%Y-%m-%d')), format_filter) 
                sheet.write(rowx + 3, 0, '%s: %s' % (_('Analytic account'), name), format_filter)
                rowx += 5
                
                sheet.merge_range(rowx, col, rowx + 1, col, _('№'), format_group)
                col += 1
                sheet.merge_range(rowx, col, rowx + 1, col, _('Reports'), format_group)
                col += 1
                if self.is_compare_two_year_ago:
                    sheet.merge_range(rowx, col, rowx + 1, col, _('2 years ago performance'), format_group)
                    col += 1
                if self.is_compare_one_year_ago:
                    sheet.merge_range(rowx, col, rowx + 1, col, _('1 years ago performance'), format_group)
                    col += 1
                if self.is_compare_before_month:
                    sheet.merge_range(rowx, col, rowx + 1, col, _('Before month performance'), format_group)
                    col += 1        
                sheet.merge_range(rowx, col, rowx + 1, col, _('Performance'), format_group)
                col += 1        
                if self.is_compare_before_month:
                    sheet.merge_range(rowx, col, rowx, col + 1, _('Before Month'), format_group)
                    sheet.write(rowx + 1, col, _('In ₮'), format_group)
                    sheet.write(rowx + 1, col + 1, _('In %'), format_group)
                    col += 2
                if self.is_compare_one_year_ago:
                    sheet.merge_range(rowx, col, rowx, col + 1, _('1 year ago performance'), format_group)
                    sheet.write(rowx + 1, col, _('In ₮'), format_group)
                    sheet.write(rowx + 1, col + 1, _('In %'), format_group)
                    col += 2
                if self.is_compare_two_year_ago:
                    sheet.merge_range(rowx, col, rowx, col + 1, _('2 year ago performance'), format_group)
                    sheet.write(rowx + 1, col, _('In ₮'), format_group)
                    sheet.write(rowx + 1, col + 1, _('In %'), format_group)
                    col += 2             
                 
                rowx += 2
                col = seq = sub_seq = 0
                for line in self.line_ids:
                    this_month_performance = two_year_ago_performance = one_year_ago_performance = before_month_performance = 0
                    before_month_per_percent = one_year_ago_per_percent = two_year_ago_per_percent = 0  
                    col = 0
                    if line.report_id.style_overwrite == 2: 
                        format_float = format_content_float_color
                        format_content = format_content_text_color
                        format_percent = format_content_percent_color
                        format_number = format_content_bold_number_color
                    else:                
                        format_float = format_content_float
                        format_content = format_content_text
                        format_percent = format_content_percent
                        format_number = format_content_number
                    if line.report_id.chart_type == 'revenue_results':
                        seq += 1
                        if line.report_id.style_overwrite != 2:
                            format_content = format_content_bold_left
                            format_float = format_content_bold_float
                            format_percent = format_content_percent_bold
                            format_number = format_content_bold_number
                        sheet.write(rowx, col, seq, format_number)
                        sub_seq = 0 
                    else:
                        sub_seq += 1
                        sheet.write(rowx, col, str(seq) + '.' + str(sub_seq), format_number)
                    res = self.with_context(data.get('used_context'))._compute_report_balance(line.report_id)
                    comparison_res1 = self.with_context(data.get('comparison_context1'))._compute_report_balance(line.report_id)
                    comparison_res2 = self.with_context(data.get('comparison_context2'))._compute_report_balance(line.report_id)
                    before_month_comp = self.with_context(data.get('before_month_comparison_context'))._compute_report_balance(line.report_id)
                    
                    name = line.report_id.name
                    two_year_ago_performance = comparison_res2[line.report_id.id]['balance'] * line.report_id.sign
                    one_year_ago_performance = comparison_res1[line.report_id.id]['balance'] * line.report_id.sign
                    before_month_performance = before_month_comp[line.report_id.id]['balance'] * line.report_id.sign
                    this_month_performance = res[line.report_id.id]['balance'] * line.report_id.sign
                    before_month_per_tug = this_month_performance - before_month_performance
                    one_year_ago_per_tug = this_month_performance - one_year_ago_performance
                    two_year_ago_per_tug = this_month_performance - two_year_ago_performance
                    if before_month_performance != 0:
                        before_month_per_percent = this_month_performance/before_month_performance*100
                    if one_year_ago_performance != 0:
                        one_year_ago_per_percent = this_month_performance/one_year_ago_performance*100
                    if two_year_ago_performance != 0:
                        two_year_ago_per_percent = this_month_performance/two_year_ago_performance*100
                    
                    col += 1 
                    sheet.write(rowx, col, name, format_content) 
                    col += 1 
                    if self.is_compare_two_year_ago:
                        sheet.write(rowx, col, two_year_ago_performance, format_float)
                        col += 1
                    if self.is_compare_one_year_ago:
                        sheet.write(rowx, col, one_year_ago_performance, format_float)
                        col += 1
                    if self.is_compare_before_month:
                        sheet.write(rowx, col, before_month_performance, format_float)
                        col += 1   
                    sheet.write(rowx, col, this_month_performance, format_float)
                    col += 1  
                    if self.is_compare_before_month:
                        sheet.write(rowx, col, before_month_per_tug, format_float)                
                        sheet.write(rowx, col + 1, before_month_per_percent/100, format_percent)
                        col += 2
                    if self.is_compare_one_year_ago:
                        sheet.write(rowx, col, one_year_ago_per_tug, format_float)                
                        sheet.write(rowx, col + 1, one_year_ago_per_percent/100, format_percent)
                        col += 2
                    if self.is_compare_two_year_ago:
                        sheet.write(rowx, col, two_year_ago_per_tug, format_float)                
                        sheet.write(rowx, col + 1, two_year_ago_per_percent/100, format_percent)
                        col += 2  
                    rowx += 1
                rowx += 2
                sheet.merge_range(rowx, 0, rowx, name_col, _('Executive Director:..................................../%s/')%(executives_names),format_filter_center)
                rowx += 2
                sheet.merge_range(rowx, 0, rowx, name_col, _('Accountant:..................................../%s/')%(accountants_names),format_filter_center)
                sheet.hide_gridlines(2)     
                
        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        
        # call export function
        return report_excel_output_obj.export_report()    