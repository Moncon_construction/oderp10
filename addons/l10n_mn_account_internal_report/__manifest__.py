# -*- coding: utf-8 -*-
# Part of OdErp. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Account Internal Reports",
    'version': '1.0',
    'depends': ['l10n_mn_account'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
      Account Reports
    """,
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'data/internal_expense_report_data.xml',
        'data/internal_income_report_data.xml',
        'data/internal_profit_report_data.xml',
        'views/internal_reports_menu.xml',
        'views/internal_profit_report_view.xml', 
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
