# -*- coding: utf-8 -*-
from odoo import api, fields, models

class AccountFinancialReport(models.Model):
    _inherit = "account.financial.report"
    
    type = fields.Selection([
        ('sum', 'View'),
        ('accounts', 'Accounts'),
        ('account_type', 'Account Type'),
        ('account_report', 'Report Value'),
        ('account_budget_post', 'Account Budget Post'),
        ], 'Type', default='sum')
    
    chart_type = fields.Selection([('balance', 'Balance sheet')
                    , ('profit', 'Income statement')
                    , ('acc_chart', 'Account chart')
                    , ('other', 'Other')
                    , ('acc_expense', 'Account Expense')
                    , ('sales_income', 'Sales Income')
                    , ('revenue_results', 'Revenue Results')], string='Chart type', default='other')