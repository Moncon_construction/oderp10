# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from io import BytesIO
import logging
import base64
import time
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from operator import itemgetter
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles

_logger = logging.getLogger(__name__)
    
class InternalExpenseReportLine(models.Model):
    _name = "internal.expense.report.line"

    profit_report_id = fields.Many2one('internal.profit.report', string='Internal Report',copy=False, ondelete='cascade')
    report_id = fields.Many2one('account.financial.report', string='Report')
    name = fields.Char(string='Name')
    two_year_ago_performance = fields.Float(string='Two year ago performance')
    one_year_ago_performance = fields.Float(string='One year ago performance')
    before_month_performance = fields.Float(string='Before month performance')
    this_month_performance = fields.Float(string='This month performance')
    before_month_per_tug = fields.Float(string='Before month performance Tugrug')
    before_month_per_percent = fields.Float(string='Before month performance percent',group_operator="avg", digits=(4,2))
    one_year_ago_per_tug = fields.Float(string='One year ago performance Tugrug')
    one_year_ago_per_percent = fields.Float(string='One year ago performance percent',group_operator="avg", digits=(4,2))
    two_year_ago_per_tug = fields.Float(string='Two year ago performance Tugrug')
    two_year_ago_per_percent = fields.Float(string='Two year ago performance percent',group_operator="avg", digits=(4,2))

    #Журналын мөрүүдийг буцаах
    def _get_accounts(self, reports, lines):
        res = {}
        line_ids = lines
        for report in reports:
            if report.id in res:
                continue
            if report.type == 'accounts':
                if report.account_ids:
                    res[report.id] =  self._compute_account_move_lines(report.account_ids)
                    if type(res) != list:
                        for key, value in res.items():
                            line_ids += value
                            res = line_ids
                    else:
                        res = res[report.id]
                else:
                    res = line_ids
            elif report.type == 'account_type':
                accounts = self.env['account.account'].search([('user_type_id', 'in', report.account_type_ids.ids)])
                if accounts:
                    if type(res) != list:
                        res[report.id] = self._compute_account_move_lines(accounts)
                        if type(res[report.id]) != list:
                            for key, value in res.items():
                                line_ids += value
                                res = line_ids
                        else:
                            line_ids += res[report.id]
                            res = line_ids
                    else:
                        res += self._compute_account_move_lines(accounts)
                else:
                    res = line_ids
            elif report.type == 'account_report' and report.account_report_id:
                res2 = self._get_accounts(report.account_report_id,line_ids)
                if type(res2) != list:
                    for key, value in res2.items():
                        line_ids += value
                        res = line_ids
                else:
                    res = res2
        return res
    
    #Журналын мөрүүдийг дансаар шүүх олох
    def _compute_account_move_lines(self, accounts):
        move_line_ids = []
        if accounts:
            tables, where_clause, where_params = self.env['account.move.line']._query_get()
            tables = tables.replace('"', '') if tables else "account_move_line"
            wheres = [""]
            if where_clause.strip():
                wheres.append(where_clause.strip())
            filters = " AND ".join(wheres)
            request = "SELECT account_move_line.id "  \
                       " FROM " + tables + \
                       " WHERE account_id IN %s " + filters
            params = (tuple(accounts._ids),) + tuple(where_params)
            self.env.cr.execute(request, params)
            move_line_ids = [row['id'] for row in self.env.cr.dictfetchall()]
        return move_line_ids  
    
    #Журналын мөрүүдийг харуулах
    @api.multi
    def open_account_move_lines(self):
        data = {}
        lines = []
        for line in self:
            date_from  = line.profit_report_id.start_date
            date_to  = line.profit_report_id.end_date
            data['used_context'] = {
                    'date_from':date_from,
                    'date_to': date_to,
                    'state':'posted',
            }
            move_line_ids = self.with_context(data.get('used_context'))._get_accounts(line.report_id,lines)
            if move_line_ids == {}:
                move_line_ids = []
            res = self.env['ir.actions.act_window'].for_xml_id('account', 'action_account_moves_all_a')
            res['domain'] = [('id', 'in', move_line_ids)]
            res['context'] = {}
            return res

class InternalExpenseReport(models.Model):
    _inherit = "internal.profit.report"

    expense_line_ids = fields.One2many(
        'internal.expense.report.line', 'profit_report_id', string='Report Line')
    expense_line_ids1 = fields.One2many(
        'internal.expense.report.line', 'profit_report_id', string='Report Line')
    expense_line_ids2 = fields.One2many(
        'internal.expense.report.line', 'profit_report_id', string='Report Line')
    expense_line_ids3 = fields.One2many(
        'internal.expense.report.line', 'profit_report_id', string='Report Line')
    expense_line_ids4 = fields.One2many(
        'internal.expense.report.line', 'profit_report_id', string='Report Line')
    expense_line_ids5 = fields.One2many(
        'internal.expense.report.line', 'profit_report_id', string='Report Line')
    expense_line_ids6 = fields.One2many(
        'internal.expense.report.line', 'profit_report_id', string='Report Line')
    expense_line_ids7 = fields.One2many(
        'internal.expense.report.line', 'profit_report_id', string='Report Line')
