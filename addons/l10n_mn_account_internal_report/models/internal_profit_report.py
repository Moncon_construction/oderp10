# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from io import BytesIO
import base64
import time
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
STATE_SELECTION = [
    ('draft', 'Draft'),
    ('approved', 'Approved'),
]
class InternalProfitReport(models.Model):
    _name='internal.profit.report'
    _inherit = ['mail.thread']

    def _is_analytic_default(self):
        context = self._context or {}
        if 'is_analytic' in context:
            if context['is_analytic'] == True:
                return True
        return False
    
    name = fields.Char(string='Name')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    start_date = fields.Date(string='Start Date')
    end_date = fields.Date(string='End Date')
    is_compare_before_month = fields.Boolean(string='Is Compare Before Month')
    is_compare_one_year_ago = fields.Boolean(string='Is Compare One Year Ago')
    is_compare_two_year_ago = fields.Boolean(string='Is Compare Two Year Ago')
    show_check = fields.Boolean(string='Show Check')
    show_income = fields.Boolean(string='Show Income')
    show_expense = fields.Boolean(string='Show Expense')
    state = fields.Selection(STATE_SELECTION, string='Status', default='draft', copy=False, index=True, readonly=True, help="Status of the performance.", track_visibility='always')
    is_analytic = fields.Boolean('Is Analytic',default=_is_analytic_default)
            
    #Эхлэх дуусах огнооноос хамааран тайлангийн нэрийг оноох болон өмнөх сартай харьцуулах эсэх товчийг харуулах эсэх утгыг онооно
    @api.onchange('start_date','end_date','company_id')
    def onchange_period_id(self):
        show_check = False     
        name = ''
        is_compare_before_month = self.is_compare_before_month
  
        date_from  = self.start_date
        date_to  = self.end_date
        if date_from and date_to:
            date_start = datetime.strptime(date_from, "%Y-%m-%d")
            date_end = datetime.strptime(date_to, "%Y-%m-%d")
            start_month = date_start.month
            start_year = date_start.year
            end_month = date_end.month
            end_year = date_end.year
            if start_year == end_year:
                if start_month == end_month:
                    show_check = True
                else:                               
                    is_compare_before_month = False
            else:                               
                is_compare_before_month = False
            if self.start_date == self.end_date:
                name = _('%s - %s month Internal Profit Details Report') % (self.company_id.name, self.start_date)
            else:
                name = _('%s - %s - %s months Internal Profit Details Report') % (self.company_id.name, self.start_date, self.end_date)
            self.update({'name': name,
                         'show_check':show_check,
                         'is_compare_before_month':is_compare_before_month,
                         })  
                    
    #Батлах товчны үйлдэл
    @api.multi
    def validate(self):
        self.write({'state': 'approved'})
        
    #Ноороглох
    @api.multi
    def action_to_draft(self):
        self.write({'state': 'draft'})
    
class InternalProfitReportLine(models.Model):
    _name = "internal.profit.report.line"

    profit_report_id = fields.Many2one('internal.profit.report', string='Internal Report',copy=False, ondelete='cascade')
    report_id = fields.Many2one('account.financial.report', string='Report')
    name = fields.Char(string='Name')
    two_year_ago_performance = fields.Float(string='Two year ago performance')
    one_year_ago_performance = fields.Float(string='One year ago performance')
    before_month_performance = fields.Float(string='Before month performance')
    this_month_performance = fields.Float(string='This month performance')
    before_month_per_tug = fields.Float(string='Before month performance Tugrug')
    before_month_per_percent = fields.Float(string='Before month performance percent',group_operator="avg", digits=(4,2))
    one_year_ago_per_tug = fields.Float(string='One year ago performance Tugrug')
    one_year_ago_per_percent = fields.Float(string='One year ago performance percent',group_operator="avg", digits=(4,2))
    two_year_ago_per_tug = fields.Float(string='Two year ago performance Tugrug')
    two_year_ago_per_percent = fields.Float(string='Two year ago performance percent',group_operator="avg", digits=(4,2))
    
    #Журналын мөрүүдийг буцаах
    def _get_accounts(self, reports, lines):
        res = {}
        line_ids = lines
        for report in reports:
            if report.id in res:
                continue
            if report.type == 'accounts':
                if report.account_ids:
                    res[report.id] =  self._compute_account_move_lines(report.account_ids)
                    if type(res) != list:
                        for key, value in res.items():
                            line_ids += value
                            res = line_ids
                    else:
                        res = res[report.id]
                else:
                    res = line_ids
            elif report.type == 'account_type':
                accounts = self.env['account.account'].search([('user_type_id', 'in', report.account_type_ids.ids)])
                if accounts:
                    if type(res) != list:
                        res[report.id] = self._compute_account_move_lines(accounts)
                        if type(res[report.id]) != list:
                            for key, value in res.items():
                                line_ids += value
                                res = line_ids
                        else:
                            line_ids += res[report.id]
                            res = line_ids
                    else:
                        res += self._compute_account_move_lines(accounts)
                else:
                    res = line_ids
            elif report.type == 'account_report' and report.account_report_id:
                res2 = self._get_accounts(report.account_report_id,line_ids)
                if type(res2) != list:
                    for key, value in res2.items():
                        line_ids += value
                        res = line_ids
                else:
                    res = res2
        return res
    
    #Журналын мөрүүдийг дансаар шүүх олох
    def _compute_account_move_lines(self, accounts):
        move_line_ids = []
        if accounts:
            tables, where_clause, where_params = self.env['account.move.line']._query_get()
            tables = tables.replace('"', '') if tables else "account_move_line"
            wheres = [""]
            if where_clause.strip():
                wheres.append(where_clause.strip())
            filters = " AND ".join(wheres)
            request = "SELECT account_move_line.id "  \
                       " FROM " + tables + \
                       " WHERE account_id IN %s " + filters
            params = (tuple(accounts._ids),) + tuple(where_params)
            self.env.cr.execute(request, params)
            move_line_ids = [row['id'] for row in self.env.cr.dictfetchall()]
        return move_line_ids  
    
    #Журналын мөрүүдийг харуулах
    @api.multi
    def open_account_move_lines(self):
        data = {}
        lines = []
        for line in self:
            date_from  = line.profit_report_id.start_date
            date_to  = line.profit_report_id.end_date
            data['used_context'] = {
                    'date_from':date_from,
                    'date_to': date_to,
                    'state':'posted',
            }
            move_line_ids = self.with_context(data.get('used_context'))._get_accounts(line.report_id,lines)
            if move_line_ids == {}:
                move_line_ids = []
            res = self.env['ir.actions.act_window'].for_xml_id('account', 'action_account_moves_all_a')
            res['domain'] = [('id', 'in', move_line_ids)]
            res['context'] = {}
            return res
    
class InternalProfitReport(models.Model):
    _inherit = "internal.profit.report"

    line_ids = fields.One2many(
        'internal.profit.report.line', 'profit_report_id', string='Report Line')
    line_ids1 = fields.One2many(
        'internal.profit.report.line', 'profit_report_id', string='Report Line')
    line_ids2 = fields.One2many(
        'internal.profit.report.line', 'profit_report_id', string='Report Line')
    line_ids3 = fields.One2many(
        'internal.profit.report.line', 'profit_report_id', string='Report Line')
    line_ids4 = fields.One2many(
        'internal.profit.report.line', 'profit_report_id', string='Report Line')
    line_ids5 = fields.One2many(
        'internal.profit.report.line', 'profit_report_id', string='Report Line')
    line_ids6 = fields.One2many(
        'internal.profit.report.line', 'profit_report_id', string='Report Line')
    line_ids7 = fields.One2many(
        'internal.profit.report.line', 'profit_report_id', string='Report Line')
    
    @api.multi
    def last_day_of_month(self, date):
        #         Тухайн огноонооны сарын сүүлчийн өдрийн огноог буцаах
        next_month = date.replace(day=28) + timedelta(days=4)  # this will never fail
        return next_month - timedelta(days=next_month.day)
    
    
    def _compute_account_balance(self, accounts):
        res = {}
        context = dict(self._context or {})
        if not  context.get('is_analytic'):
            """ compute the balance, debit and credit for the provided accounts
            """
            mapping = {
                'balance': "COALESCE(SUM(debit),0) - COALESCE(SUM(credit), 0) as balance",
                'debit': "COALESCE(SUM(debit), 0) as debit",
                'credit': "COALESCE(SUM(credit), 0) as credit",
            }
    
            for account in accounts:
                res[account.id] = dict((fn, 0.0) for fn in mapping.keys())
            if accounts:
                tables, where_clause, where_params = self.env['account.move.line']._query_get()
                tables = tables.replace('"', '') if tables else "account_move_line"
                wheres = [""]
                if where_clause.strip():
                    wheres.append(where_clause.strip())
                filters = " AND ".join(wheres)
                request = "SELECT account_id as id, " + ', '.join(mapping.values()) + \
                           " FROM " + tables + \
                           " WHERE account_id IN %s " \
                                + filters + \
                           " GROUP BY account_id"
                params = (tuple(accounts._ids),) + tuple(where_params)
                self.env.cr.execute(request, params)
                for row in self.env.cr.dictfetchall():
                    res[row['id']] = row
        return res
    
    #Тайлангийн тохиргооноос хамааран дүнгүүдийг тооцоолно
    def _compute_report_balance(self, reports):
        '''returns a dictionary with key=the ID of a record and value=the credit, debit and balance amount
           computed for this record. If the record is of type :
               'accounts' : it's the sum of the linked accounts
               'account_type' : it's the sum of leaf accoutns with such an account_type
               'account_report' : it's the amount of the related report
               'sum' : it's the sum of the children of this record (aka a 'view' record)'''
        res = {}
        fields = [ 'balance']
        for report in reports:
            if report.id in res:
                continue
            res[report.id] = dict((fn, 0.0) for fn in fields)
            if report.type == 'accounts':
                # it's the sum of the linked accounts
                res[report.id]['account'] = self._compute_account_balance(report.account_ids)
                for value in res[report.id]['account'].values():
                    for field in fields:
                        res[report.id][field] += value.get(field)
            elif report.type == 'account_type':
                # it's the sum the leaf accounts with such an account type
                accounts = self.env['account.account'].search([('user_type_id', 'in', report.account_type_ids.ids)])
                res[report.id]['account'] = self._compute_account_balance(accounts)
                for value in res[report.id]['account'].values():
                    for field in fields:
                        res[report.id][field] += value.get(field)
            elif report.type == 'account_report' and report.account_report_id:
                # it's the amount of the linked report
                res2 = self._compute_report_balance(report.account_report_id)
                for key, value in res2.items():
                    for field in fields:
                        res[report.id][field] += value[field]
            elif report.type == 'sum':
                # it's the sum of the children of this account.report
                res2 = self._compute_report_balance(report.children_ids)
                for key, value in res2.items():
                    for field in fields:
                        res[report.id][field] += value[field]
        return res
    
    #Мөрийн утгыг цуглуулна
    def get_account_lines(self,data,type):
        context = dict(self._context or {})
        if not context.get('is_analytic'):
            lines = []
            account_report = self.env['account.financial.report'].search([('chart_type', '=', type),('type', '=', 'sum')],order='sequence')
            child_reports = account_report._get_children_by_order()
            res = self.with_context(data.get('used_context'))._compute_report_balance(child_reports)
            comparison_res1 = self.with_context(data.get('comparison_context1'))._compute_report_balance(child_reports)
            comparison_res2 = self.with_context(data.get('comparison_context2'))._compute_report_balance(child_reports)
            before_month_comp = self.with_context(data.get('before_month_comparison_context'))._compute_report_balance(child_reports)
            for report in child_reports:
                vals = {
                    'name': report.name,
                    'balance': res[report.id]['balance'] * report.sign,
                    'type': report.type,
                    'report_id': report.id,
                    'level': bool(report.style_overwrite) and report.style_overwrite or report.level,
                    'account_type': report.type or False, #used to underline the financial report balances
                    'balance_cmp1':comparison_res1[report.id]['balance'] * report.sign,
                    'balance_cmp2':comparison_res2[report.id]['balance'] * report.sign,
                    'balance_be_month':before_month_comp[report.id]['balance'] * report.sign,
                }
                lines.append(vals)
            return lines
    
    #Тайлангийн мөрийг зурах
    @api.multi
    def compute(self):
        context = dict(self._context or {})
        if not context.get('is_analytic'):
            data ={}
            income_lines = self.env['internal.income.report.line']
            expense_lines = self.env['internal.expense.report.line']
            profit_lines = self.env['internal.profit.report.line']
            
            for profit in self:      
                profit.line_ids.unlink()  
                profit.income_line_ids.unlink()  
                profit.expense_line_ids.unlink()  
                date_from  = self.start_date
                date_to  = self.end_date
                date_start = datetime.strptime(date_from, "%Y-%m-%d")
                date_end = datetime.strptime(date_to, "%Y-%m-%d")
                day = date_end.day
                month = date_end.month
                year = date_end.year
                before_month_end = (datetime(year, month, day) - relativedelta(months=1))
                one_year_ago_end = (datetime(year, month, day) - relativedelta(years=1))
                two_year_ago_end = (datetime(year, month, day) - relativedelta(years=2))
                before_month_end = self.last_day_of_month(before_month_end)
                one_year_ago_end = self.last_day_of_month(one_year_ago_end)
                two_year_ago_end = self.last_day_of_month(two_year_ago_end)
                before_month_start = date_start - relativedelta(months=1)
                one_year_ago_start = date_start - relativedelta(years=1)
                two_year_ago_start = date_start - relativedelta(years=2)
                this_month_performance = two_year_ago_performance = one_year_ago_performance = before_month_performance = 0
                pro_coef_before_month_per_percent = pro_coef_one_year_ago_per_percent = pro_coef_two_year_ago_per_percent = 0  
                
                data['used_context'] = {
                        'date_from':date_from,
                        'date_to': date_to,
                        'state':'posted',
                }
                year2_comparison_context =  {'strict_range': True, 'state': 'posted', 'date_from': two_year_ago_start, 'date_to': two_year_ago_end}
                year1_comparison_context =  {'strict_range': True, 'state': 'posted', 'date_from': one_year_ago_start, 'date_to': one_year_ago_end}
                before_month_comparison_context =  {'strict_range': True, 'state': 'posted', 'date_from': before_month_start, 'date_to': before_month_end}
                data['comparison_context1'] = year1_comparison_context
                data['comparison_context2'] = year2_comparison_context
                data['before_month_comparison_context'] = before_month_comparison_context
                income_report_lines = self.get_account_lines(data,'sales_income')
                expense_report_lines = self.get_account_lines(data,'acc_expense')
                profit_report_lines = self.get_account_lines(data,'revenue_results')
                
                for line in income_report_lines:
                    if line['type'] != 'sum':
                        this_month_performance = line['balance']
                        two_year_ago_performance = line['balance_cmp2']
                        one_year_ago_performance = line['balance_cmp1']
                        before_month_performance = line['balance_be_month']
                        if before_month_performance > 0:
                            pro_coef_before_month_per_percent = this_month_performance/before_month_performance*100
                        if one_year_ago_performance > 0:
                            pro_coef_one_year_ago_per_percent = this_month_performance/one_year_ago_performance*100
                        if two_year_ago_performance > 0:
                            pro_coef_two_year_ago_per_percent = this_month_performance/two_year_ago_performance*100
                        res = {
                                'name':line['name'],
                                'profit_report_id': profit.id,
                                'report_id': line['report_id'],
                                'this_month_performance':this_month_performance,
                                'two_year_ago_performance':two_year_ago_performance,
                                'one_year_ago_performance':one_year_ago_performance,
                                'before_month_performance':before_month_performance,
                                'before_month_per_tug':this_month_performance - before_month_performance,
                                'before_month_per_percent':pro_coef_before_month_per_percent,
                                'one_year_ago_per_tug':this_month_performance - one_year_ago_performance,
                                'one_year_ago_per_percent':pro_coef_one_year_ago_per_percent,
                                'two_year_ago_per_tug':this_month_performance - two_year_ago_performance,
                                'two_year_ago_per_percent':pro_coef_two_year_ago_per_percent,
                        }
                        income_lines += self.env['internal.income.report.line'].create(res)
                        pro_coef_before_month_per_percent = pro_coef_one_year_ago_per_percent = pro_coef_two_year_ago_per_percent = 0  
                
                for line in expense_report_lines:
                    if line['type'] != 'sum':
                        this_month_performance = line['balance']
                        two_year_ago_performance = line['balance_cmp2']
                        one_year_ago_performance = line['balance_cmp1']
                        before_month_performance = line['balance_be_month']
                        if before_month_performance > 0:
                            pro_coef_before_month_per_percent = this_month_performance/before_month_performance*100
                        if one_year_ago_performance > 0:
                            pro_coef_one_year_ago_per_percent = this_month_performance/one_year_ago_performance*100
                        if two_year_ago_performance > 0:
                            pro_coef_two_year_ago_per_percent = this_month_performance/two_year_ago_performance*100
                        res = {
                                'name':line['name'],
                                'profit_report_id': profit.id,
                                'report_id': line['report_id'],
                                'this_month_performance':this_month_performance,
                                'two_year_ago_performance':two_year_ago_performance,
                                'one_year_ago_performance':one_year_ago_performance,
                                'before_month_performance':before_month_performance,
                                'before_month_per_tug':this_month_performance - before_month_performance,
                                'before_month_per_percent':pro_coef_before_month_per_percent,
                                'one_year_ago_per_tug':this_month_performance - one_year_ago_performance,
                                'one_year_ago_per_percent':pro_coef_one_year_ago_per_percent,
                                'two_year_ago_per_tug':this_month_performance - two_year_ago_performance,
                                'two_year_ago_per_percent':pro_coef_two_year_ago_per_percent,
                        }
                        expense_lines += self.env['internal.expense.report.line'].create(res)
                        pro_coef_before_month_per_percent = pro_coef_one_year_ago_per_percent = pro_coef_two_year_ago_per_percent = 0  
                
                for line in profit_report_lines:
                    if line['type'] != 'sum':
                        this_month_performance = line['balance']
                        two_year_ago_performance = line['balance_cmp2']
                        one_year_ago_performance = line['balance_cmp1']
                        before_month_performance = line['balance_be_month']
                        if before_month_performance > 0:
                            pro_coef_before_month_per_percent = this_month_performance/before_month_performance*100
                        if one_year_ago_performance > 0:
                            pro_coef_one_year_ago_per_percent = this_month_performance/one_year_ago_performance*100
                        if two_year_ago_performance > 0:
                            pro_coef_two_year_ago_per_percent = this_month_performance/two_year_ago_performance*100
                        res = {
                                'name':line['name'],
                                'profit_report_id': profit.id,
                                'report_id': line['report_id'],
                                'this_month_performance':this_month_performance,
                                'two_year_ago_performance':two_year_ago_performance,
                                'one_year_ago_performance':one_year_ago_performance,
                                'before_month_performance':before_month_performance,
                                'before_month_per_tug':this_month_performance - before_month_performance,
                                'before_month_per_percent':pro_coef_before_month_per_percent,
                                'one_year_ago_per_tug':this_month_performance - one_year_ago_performance,
                                'one_year_ago_per_percent':pro_coef_one_year_ago_per_percent,
                                'two_year_ago_per_tug':this_month_performance - two_year_ago_performance,
                                'two_year_ago_per_percent':pro_coef_two_year_ago_per_percent,
                        }
                        profit_lines += self.env['internal.profit.report.line'].create(res)
                        pro_coef_before_month_per_percent = pro_coef_one_year_ago_per_percent = pro_coef_two_year_ago_per_percent = 0  
        return True
    
    @api.multi
    def unlink(self):
        for report in self:
            if report.state not in ('draft'):
                raise UserError(_('Delete only draft in state'))
        return super(InternalProfitReport, self).unlink()   