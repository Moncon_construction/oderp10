# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from io import BytesIO
import base64
import time
from odoo import api, models, _
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from passlib.tests.utils import limit

class InternalProfitReport(models.Model):
    _inherit='internal.profit.report'
    
#    Тайланг хэвлэх
    @api.multi
    def export_report(self):
        
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Internal Profit Details Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        
        # create formats
        
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_filter_center = book.add_format(ReportExcelCellStyles.format_filter_center)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_float_color = book.add_format(ReportExcelCellStyles.format_group_float)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_text_color = book.add_format(ReportExcelCellStyles.format_content_text_color)
        format_content_center_text_color = book.add_format(ReportExcelCellStyles.format_content_center_text_color)
        format_content_percent = book.add_format(ReportExcelCellStyles.format_content_percent)
        format_content_percent_color = book.add_format(ReportExcelCellStyles.format_content_percent_color)
        seq = 1
        rowx = 0
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('internal_profit_details_report'), form_title=file_name).create({})

#         create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.39, 0.39, 0.39, 0.39)  # 1cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        
        # compute column
        sheet.set_column('A:A', 3)
        sheet.set_column('B:B', 30)
        sheet.set_column('C:C', 15)
        sheet.set_column('D:D', 15)
        sheet.set_column('E:E', 15)
        sheet.set_column('F:F', 13)
        sheet.set_column('G:G', 13)
        sheet.set_column('H:H', 13)
        sheet.set_column('I:I', 13)
        sheet.set_column('J:J', 13)
        sheet.set_column('K:K', 13)
        sheet.set_column('L:L', 13)
        sheet.set_column('M:M', 13)
        sheet.set_column('N:N', 13)
        sheet.set_column('O:O', 13)
        sheet.set_column('P:P', 13)
        sheet.set_column('Q:Q', 13)
        
        # create name
        executives_names = ''
        accountants_names = ''
        company = self.company_id
        executives_employees = []
        accountants_employees = []
        
        if company.executive_signature:
            executives_employees = self.env['hr.employee'].search([('user_id', '=', company.executive_signature.id)])  
        if company.genaral_accountant_signature:
            accountants_employees = self.env['hr.employee'].search([('user_id', '=', company.genaral_accountant_signature.id)])
        
        for employee in executives_employees:
            if employee.last_name:
                executives_names = employee.last_name[0]
            
            executives_names += '.%s' % (employee.name,)
            if employee.job_id.name:
                executives_names += ',  %s' % (employee.job_id.name,)
            
        for employee in accountants_employees:
            if employee.last_name:
                accountants_names = employee.last_name[0]
            
            accountants_names += '.%s' % (employee.name,)
            if employee.job_id.name:
                accountants_names += ',  %s' % (employee.job_id.name,)
        #Compute header column
        name_col = 7
        if self.is_compare_two_year_ago and self.is_compare_one_year_ago and self.is_compare_before_month:
            name_col = 11
        if self.is_compare_two_year_ago and self.is_compare_one_year_ago and not self.is_compare_before_month:
            name_col = 8
        if self.is_compare_two_year_ago and  not self.is_compare_one_year_ago and self.is_compare_before_month:
            name_col = 8
        if not self.is_compare_two_year_ago and  self.is_compare_one_year_ago and self.is_compare_before_month:
            name_col = 8
        if self.is_compare_two_year_ago and not self.is_compare_one_year_ago and not self.is_compare_before_month:
            name_col = 5
        if not self.is_compare_two_year_ago and self.is_compare_one_year_ago and not self.is_compare_before_month:
            name_col = 5
        if not self.is_compare_two_year_ago and not self.is_compare_one_year_ago and self.is_compare_before_month:
            name_col = 5
        sheet.merge_range(rowx, 0, rowx, name_col, report_name.upper(), format_name)
        rowx += 1
        
        sheet.write(rowx, 0, '%s: %s' % (_('Company'), self.company_id.name), format_filter)
        sheet.write(rowx + 1, 0, '%s: %s - %s' % (_('Duration'), self.start_date, self.end_date), format_filter)
        sheet.write(rowx + 2, 0, '%s: %s' % (_('Created On'), time.strftime('%Y-%m-%d')), format_filter) 
        rowx += 4
        
        # table header
            
        col = 0
        sheet.merge_range(rowx, col, rowx + 1, col, _('№'), format_title)
        col += 1
        sheet.merge_range(rowx, col, rowx + 1, col, _('Reports'), format_title)
        
        col += 1
        if self.is_compare_two_year_ago:
            sheet.merge_range(rowx, col, rowx + 1, col, _('2 years ago performance'), format_title)
            col += 1
        if self.is_compare_one_year_ago:
            sheet.merge_range(rowx, col, rowx + 1, col, _('1 years ago performance'), format_title)
            col += 1
        if self.is_compare_before_month:
            sheet.merge_range(rowx, col, rowx + 1, col, _('Before month performance'), format_title)
            col += 1        
        sheet.merge_range(rowx, col, rowx + 1, col, _('Performance'), format_title)
        col += 1        
        if self.is_compare_before_month:
            sheet.merge_range(rowx, col, rowx, col + 1, _('Before Month'), format_title)
            sheet.write(rowx + 1, col, _('In ₮'), format_title)
            sheet.write(rowx + 1, col + 1, _('In %'), format_title)
            col += 2
        if self.is_compare_one_year_ago:
            sheet.merge_range(rowx, col, rowx, col + 1, _('1 year ago performance'), format_title)
            sheet.write(rowx + 1, col, _('In ₮'), format_title)
            sheet.write(rowx + 1, col + 1, _('In %'), format_title)
            col += 2
        if self.is_compare_two_year_ago:
            sheet.merge_range(rowx, col, rowx, col + 1, _('2 year ago performance'), format_title)
            sheet.write(rowx + 1, col, _('In ₮'), format_title)
            sheet.write(rowx + 1, col + 1, _('In %'), format_title)
            col += 2             
         
        rowx += 2
        
        col = seq = sub_seq = 0
        for line in self.line_ids:
            col = 0
            seq += 1
            if line.report_id.style_overwrite == 2: 
                format_float = format_content_float_color
                format_content = format_content_text_color
                format_percent = format_content_percent_color
            else:                
                format_float = format_content_float
                format_content = format_content_text
                format_percent = format_content_percent
            sheet.write(rowx, col, seq, format_content)
            col += 1 
            sheet.write(rowx, col, line.report_id.name, format_content) 
            col += 1 
            if self.is_compare_two_year_ago:
                sheet.write(rowx, col, line.two_year_ago_performance, format_float)
                col += 1
            if self.is_compare_one_year_ago:
                sheet.write(rowx, col, line.one_year_ago_performance, format_float)
                col += 1
            if self.is_compare_before_month:
                sheet.write(rowx, col, line.before_month_performance, format_float)
                col += 1   
            sheet.write(rowx, col, line.this_month_performance, format_float)
            col += 1  
            if self.is_compare_before_month:
                sheet.write(rowx, col, line.before_month_per_tug, format_float)                
                sheet.write(rowx, col + 1, line.before_month_per_percent/100, format_percent)
                col += 2
            if self.is_compare_one_year_ago:
                sheet.write(rowx, col, line.one_year_ago_per_tug, format_float)                
                sheet.write(rowx, col + 1, line.one_year_ago_per_percent/100, format_percent)
                col += 2
            if self.is_compare_two_year_ago:
                sheet.write(rowx, col, line.two_year_ago_per_tug, format_float)                
                sheet.write(rowx, col + 1, line.two_year_ago_per_percent/100, format_percent)
                col += 2  
    
            rowx += 1
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, name_col, _('Executive Director:..................................../%s/')%(executives_names),format_filter_center)
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, name_col, _('Accountant:..................................../%s/')%(accountants_names),format_filter_center)
        sheet.hide_gridlines(2) 
        if self.show_income:
            report_name = _('Internal Income Details Report')
            sheet = book.add_worksheet(report_name)
            sheet.set_paper(9)  # A4
            sheet.set_margins(0.39, 0.39, 0.39, 0.39)  # 1cm, 1cm, 1cm, 1cm
            sheet.fit_to_pages(1, 0)
            sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
            
            # compute column
            sheet.set_column('A:A', 3)
            sheet.set_column('B:B', 35)
            sheet.set_column('C:C', 15)
            sheet.set_column('D:D', 15)
            sheet.set_column('E:E', 15)
            sheet.set_column('F:F', 13)
            sheet.set_column('G:G', 13)
            sheet.set_column('H:H', 13)
            sheet.set_column('I:I', 13)
            sheet.set_column('J:J', 13)
            sheet.set_column('K:K', 13)
            sheet.set_column('L:L', 13)
            sheet.set_column('M:M', 13)
            sheet.set_column('N:N', 13)
            sheet.set_column('O:O', 13)
            sheet.set_column('P:P', 13)
            sheet.set_column('Q:Q', 13)
            
            rowx = 0

            # create name
            sheet.merge_range(rowx, 0, rowx, name_col, report_name.upper(), format_name)
            rowx += 1
            
            sheet.write(rowx, 0, '%s: %s' % (_('Company'), self.company_id.name), format_filter)
            sheet.write(rowx + 1, 0, '%s: %s - %s' % (_('Duration'), self.start_date, self.end_date), format_filter)
            sheet.write(rowx + 2, 0, '%s: %s' % (_('Created On'), time.strftime('%Y-%m-%d')), format_filter) 
            rowx += 4
            
            # table header
                
            col = seq = sub_seq = 0
            sheet.merge_range(rowx, col, rowx + 1, col, _('№'), format_title)
            col += 1
            sheet.merge_range(rowx, col, rowx + 1, col, _('Reports'), format_title)
            
            col += 1
            if self.is_compare_two_year_ago:
                sheet.merge_range(rowx, col, rowx + 1, col, _('2 years ago performance'), format_title)
                col += 1
            if self.is_compare_one_year_ago:
                sheet.merge_range(rowx, col, rowx + 1, col, _('1 years ago performance'), format_title)
                col += 1
            if self.is_compare_before_month:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Before month performance'), format_title)
                col += 1        
             
            sheet.merge_range(rowx, col, rowx + 1, col, _('Performance'), format_title)
            col += 1        
            if self.is_compare_before_month:
                sheet.merge_range(rowx, col, rowx, col + 1, _('Before Month'), format_title)
                sheet.write(rowx + 1, col, _('In ₮'), format_title)
                sheet.write(rowx + 1, col + 1, _('In %'), format_title)
                col += 2
            if self.is_compare_one_year_ago:
                sheet.merge_range(rowx, col, rowx, col + 1, _('1 year ago performance'), format_title)
                sheet.write(rowx + 1, col, _('In ₮'), format_title)
                sheet.write(rowx + 1, col + 1, _('In %'), format_title)
                col += 2
            if self.is_compare_two_year_ago:
                sheet.merge_range(rowx, col, rowx, col + 1, _('2 year ago performance'), format_title)
                sheet.write(rowx + 1, col, _('In ₮'), format_title)
                sheet.write(rowx + 1, col + 1, _('In %'), format_title)
                col += 2             
             
            rowx += 2
            for line in self.income_line_ids:
                col = 0 
                if line.report_id.style_overwrite == 2:
                    seq = line.report_id.sequence /100
                    format_float = format_content_float_color
                    format_content = format_content_text_color
                    format_percent = format_content_percent_color
                    sub_seq = 0 
                    sheet.write(rowx, col, seq, format_content)
                    col += 1 
                elif line.report_id.style_overwrite == 3:
                    format_float = format_content_float_color
                    format_content = format_content_center_text_color
                    format_percent = format_content_percent_color
                    sheet.write(rowx, col, '', format_content)
                    col += 1 
                else:                
                    format_float = format_content_float
                    format_content = format_content_text
                    format_percent = format_content_percent
                    sub_seq += 1 
                    sheet.write(rowx, col, str(seq) + '.' + str(sub_seq), format_content)
                    col += 1 
                sheet.write(rowx, col, line.report_id.name, format_content) 
                col += 1 
                if self.is_compare_two_year_ago:
                    sheet.write(rowx, col, line.two_year_ago_performance, format_float)
                    col += 1
                if self.is_compare_one_year_ago:
                    sheet.write(rowx, col, line.one_year_ago_performance, format_float)
                    col += 1
                if self.is_compare_before_month:
                    sheet.write(rowx, col, line.before_month_performance, format_float)
                    col += 1   
                sheet.write(rowx, col, line.this_month_performance, format_float)
                col += 1  
                if self.is_compare_before_month:
                    sheet.write(rowx, col, line.before_month_per_tug, format_float)                
                    sheet.write(rowx, col + 1,line.before_month_per_percent/100, format_percent)
                    col += 2
                if self.is_compare_one_year_ago:
                    sheet.write(rowx, col, line.one_year_ago_per_tug, format_float)                
                    sheet.write(rowx, col + 1, line.one_year_ago_per_percent/100, format_percent)
                    col += 2
                if self.is_compare_two_year_ago:
                    sheet.write(rowx, col, line.two_year_ago_per_tug, format_float)                
                    sheet.write(rowx, col + 1, line.two_year_ago_per_percent/100, format_percent)
                    col += 2  
        
                rowx += 1
            rowx += 2
            sheet.merge_range(rowx, 0, rowx, name_col, _('Executive Director:..................................../%s/')%(executives_names),format_filter_center)
            rowx += 2
            sheet.merge_range(rowx, 0, rowx, name_col, _('Accountant:..................................../%s/')%(accountants_names),format_filter_center)
        sheet.hide_gridlines(2)  
        if self.show_expense:
            report_name = _('Internal Expense Details Report')
            sheet = book.add_worksheet(report_name)
            sheet.set_paper(9)  # A4
            sheet.set_margins(0.39, 0.39, 0.39, 0.39)  # 1cm, 1cm, 1cm, 1cm
            sheet.fit_to_pages(1, 0)
            sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
            
            # compute column
            sheet.set_column('A:A', 3)
            sheet.set_column('B:B', 30)
            sheet.set_column('C:C', 15)
            sheet.set_column('D:D', 15)
            sheet.set_column('E:E', 15)
            sheet.set_column('F:F', 13)
            sheet.set_column('G:G', 13)
            sheet.set_column('H:H', 13)
            sheet.set_column('I:I', 13)
            sheet.set_column('J:J', 13)
            sheet.set_column('K:K', 13)
            sheet.set_column('L:L', 13)
            sheet.set_column('M:M', 13)
            sheet.set_column('N:N', 13)
            sheet.set_column('O:O', 13)
            sheet.set_column('P:P', 13)
            sheet.set_column('Q:Q', 13)
            
            rowx = 0
            
            sheet.merge_range(rowx, 0, rowx, name_col, report_name.upper(), format_name)
            rowx += 1
            sheet.write(rowx, 0, '%s: %s' % (_('Company'), self.company_id.name), format_filter)
            sheet.write(rowx + 1, 0, '%s: %s - %s' % (_('Duration'), self.start_date, self.end_date), format_filter)
            sheet.write(rowx + 2, 0, '%s: %s' % (_('Created On'), time.strftime('%Y-%m-%d')), format_filter) 
            rowx += 4
            
            # table header
                
            col = seq = sub_seq = 0
            sheet.merge_range(rowx, col, rowx + 1, col, _('№'), format_title)
            col += 1
            sheet.merge_range(rowx, col, rowx + 1, col, _('Reports'), format_title)
            
            col += 1
            if self.is_compare_two_year_ago:
                sheet.merge_range(rowx, col, rowx + 1, col, _('2 years ago performance'), format_title)
                col += 1
            if self.is_compare_one_year_ago:
                sheet.merge_range(rowx, col, rowx + 1, col, _('1 years ago performance'), format_title)
                col += 1
            if self.is_compare_before_month:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Before month performance'), format_title)
                col += 1        
             
            sheet.merge_range(rowx, col, rowx + 1, col, _('Performance'), format_title)
            col += 1        
            if self.is_compare_before_month:
                sheet.merge_range(rowx, col, rowx, col + 1, _('Before Month'), format_title)
                sheet.write(rowx + 1, col, _('In ₮'), format_title)
                sheet.write(rowx + 1, col + 1, _('In %'), format_title)
                col += 2
            if self.is_compare_one_year_ago:
                sheet.merge_range(rowx, col, rowx, col + 1, _('1 year ago performance'), format_title)
                sheet.write(rowx + 1, col, _('In ₮'), format_title)
                sheet.write(rowx + 1, col + 1, _('In %'), format_title)
                col += 2
            if self.is_compare_two_year_ago:
                sheet.merge_range(rowx, col, rowx, col + 1, _('2 year ago performance'), format_title)
                sheet.write(rowx + 1, col, _('In ₮'), format_title)
                sheet.write(rowx + 1, col + 1, _('In %'), format_title)
                col += 2             
             
            rowx += 2
            
            for line in self.expense_line_ids:
                col = 0
                if line.report_id.style_overwrite == 2:
                    seq = line.report_id.sequence /100
                    format_float = format_content_float_color
                    format_content = format_content_text_color
                    format_percent = format_content_percent_color
                    sub_seq = 0 
                    sheet.write(rowx, col, seq, format_content)
                    col += 1 
                elif line.report_id.style_overwrite == 3:
                    format_float = format_content_float_color
                    format_content = format_content_center_text_color
                    format_percent = format_content_percent_color
                    sheet.write(rowx, col, '', format_content)
                    col += 1 
                else:                
                    format_float = format_content_float
                    format_content = format_content_text
                    format_percent = format_content_percent
                    sub_seq += 1 
                    sheet.write(rowx, col, str(seq) + '.' + str(sub_seq), format_content)
                    col += 1 
                sheet.write(rowx, col, line.report_id.name, format_content) 
                col += 1 
                if self.is_compare_two_year_ago:
                    sheet.write(rowx, col, line.two_year_ago_performance, format_float)
                    col += 1
                if self.is_compare_one_year_ago:
                    sheet.write(rowx, col, line.one_year_ago_performance, format_float)
                    col += 1
                if self.is_compare_before_month:
                    sheet.write(rowx, col, line.before_month_performance, format_float)
                    col += 1   
                sheet.write(rowx, col, line.this_month_performance, format_float)
                col += 1  
                if self.is_compare_before_month:
                    sheet.write(rowx, col, line.before_month_per_tug, format_float)                
                    sheet.write(rowx, col + 1, line.before_month_per_percent/100, format_percent)
                    col += 2
                if self.is_compare_one_year_ago:
                    sheet.write(rowx, col, line.one_year_ago_per_tug, format_float)                
                    sheet.write(rowx, col + 1, line.one_year_ago_per_percent/100, format_percent)
                    col += 2
                if self.is_compare_two_year_ago:
                    sheet.write(rowx, col, line.two_year_ago_per_tug, format_float)                
                    sheet.write(rowx, col + 1, line.two_year_ago_per_percent/100, format_percent)
                    col += 2  
        
                rowx += 1
            rowx += 2
            sheet.merge_range(rowx, 0, rowx, name_col, _('Executive Director:..................................../%s/')%(executives_names),format_filter_center)
            rowx += 2
            sheet.merge_range(rowx, 0, rowx, name_col, _('Accountant:..................................../%s/')%(accountants_names),format_filter_center)
        
        sheet.hide_gridlines(2)                                    
        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        
        # call export function
        return report_excel_output_obj.export_report()    