# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from io import BytesIO
import base64
import time
from odoo import api, models, _
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from passlib.tests.utils import limit

class InternalProfitReport(models.Model):
    _inherit='internal.profit.report'
    
#    Стандарт загвараар Тайланг хэвлэх
    @api.multi
    def export_standard_report(self):
        
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Profit Details Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        
        # create formats
        
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_content_bold_number = book.add_format(ReportExcelCellStyles.format_content_bold_number)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_bold_float = book.add_format(ReportExcelCellStyles.format_content_bold_float)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_center = book.add_format(ReportExcelCellStyles.format_content_center)
        format_content_bold_text = book.add_format(ReportExcelCellStyles.format_content_bold_text)
        format_content_text_color = book.add_format(ReportExcelCellStyles.format_content_text_color)
        format_content_center_text_color = book.add_format(ReportExcelCellStyles.format_content_center_text_color)
        seq = 1
        rowx = 1
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('internal_standard_profit_details_report'), form_title=file_name).create({})

#         create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.39, 0.39, 0.39, 0.39)  # 1cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        
        # compute column
        sheet.set_column('A:A', 11)
        sheet.set_column('B:B', 55)
        sheet.set_column('C:C', 35)
        
        # create name
        executives_names = ''
        accountants_names = ''
        company = self.company_id
        executives_employees = []
        accountants_employees = []
        
        if company.executive_signature:
            executives_employees = self.env['hr.employee'].search([('user_id', '=', company.executive_signature.id)])  
        if company.genaral_accountant_signature:
            accountants_employees = self.env['hr.employee'].search([('user_id', '=', company.genaral_accountant_signature.id)])
        
        for employee in executives_employees:
            if employee.last_name:
                executives_names = employee.last_name[0]
            
            executives_names += '.%s' % (employee.name,)
            if employee.job_id.name:
                executives_names += ',  %s' % (employee.job_id.name,)
            
        for employee in accountants_employees:
            if employee.last_name:
                accountants_names = employee.last_name[0]
            
            accountants_names += '.%s' % (employee.name,)
            if employee.job_id.name:
                accountants_names += ',  %s' % (employee.job_id.name,)
        #Compute header column
        sheet.write(rowx, 0, '%s: %s' % (_('Company'), self.company_id.name), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 0, rowx+1, 2, report_name.upper(), format_name)
        rowx += 1
        
        sheet.write(rowx + 1, 0, '%s: %s - %s' % (_('Duration'), self.start_date, self.end_date), format_filter)
        sheet.write(rowx + 2, 0, '%s: %s' % (_('Created On'), time.strftime('%Y-%m-%d')), format_filter) 
        rowx += 4
        
        # table header
            
        col = 0
        sheet.merge_range(rowx, col, rowx + 1, col, _('Number'), format_title)
        col += 1
        sheet.merge_range(rowx, col, rowx + 1, col, _('Names'), format_title)
        col += 1
        sheet.merge_range(rowx, col, rowx + 1, col, _('Period'), format_title)
        col += 1
        rowx += 2
        
        col = 0
        seq = sub_seq = i = 1
        for line in self.line_ids:
            col = 0
            seq1 = seq
            seq = line.report_id.sequence/100
            if seq1 != seq:
                sub_seq = 0
            if i == 1:
                sheet.write(rowx, col, str(seq) + '.' + str(sub_seq), format_content_bold_text)
                col += 1 
                sheet.write(rowx, col, line.report_id.name, format_content_bold_text) 
                col += 1 
                sheet.write(rowx, col, line.this_month_performance, format_content_bold_float)
            else:
                if line.report_id.style_overwrite == 2: 
                    format_float = format_content_bold_float
                    format_content = format_content_bold_text
                    format_number = format_content_bold_text
                else:                
                    format_float = format_content_float
                    format_content = format_content_text
                    format_number = format_content_center
                
                sub_seq += 1
                sheet.write(rowx, col, str(seq) + '.' + str(sub_seq), format_number)
                col += 1 
                sheet.write(rowx, col, line.report_id.name, format_content) 
                col += 1 
                sheet.write(rowx, col, line.this_month_performance, format_float)
            rowx += 1
            i += 1
            
        sheet.write(rowx, 0, '2.18', format_content_bold_text) 
        sheet.write(rowx, 1, _('Profit (loss) tax after from operations'), format_content_bold_text)
        sheet.write(rowx, 2, 0, format_float) 
        rowx += 1 
        sheet.write(rowx, 0, '2.19', format_content_bold_text) 
        sheet.write(rowx, 1, _('Net profit (loss) for the reporting period'), format_content_bold_text)
        sheet.write(rowx, 2, 0, format_float) 
        rowx += 1 
        sheet.write(rowx, 0, '2.20', format_content_bold_text) 
        sheet.write(rowx, 1, _('Other comprehensive income'), format_content_bold_text)
        sheet.write(rowx, 2, '', format_float) 
        rowx += 1 
        sheet.write(rowx, 0, '2.20.1', format_content_center) 
        sheet.write(rowx, 1, _('The difference the revaluation plus assets'), format_content_text)
        sheet.write(rowx, 2, 0, format_content_float) 
        rowx += 1 
        sheet.write(rowx, 0, '2.20.2', format_content_center) 
        sheet.write(rowx, 1, _('Foreign currency conversion differences'), format_content_text)
        sheet.write(rowx, 2, 0, format_content_float) 
        rowx += 1 
        sheet.write(rowx, 0, '2.20.3', format_content_center) 
        sheet.write(rowx, 1, _('Other profit (loss)'), format_content_text)
        sheet.write(rowx, 2, 0, format_content_float) 
        rowx += 1 
        sheet.write(rowx, 0, '2.21', format_content_bold_text) 
        sheet.write(rowx, 1, _('Total Income Amount'), format_content_bold_text)
        sheet.write(rowx, 2, 0, format_float) 
        rowx += 1 
        sheet.write(rowx, 0, '2.22', format_content_bold_text) 
        sheet.write(rowx, 1, _('Basic profit (loss) per unit share'), format_content_bold_text)
        sheet.write(rowx, 2, 0, format_float) 
        
        rowx += 2
        sheet.merge_range(rowx, 1, rowx, 3, _('Executive Director:....................................(%s)')%(executives_names),format_filter)
        rowx += 1
        sheet.merge_range(rowx, 1, rowx, 3, _('Accountant:....................................(%s)')%(accountants_names),format_filter)
        
        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        
        # call export function
        return report_excel_output_obj.export_report()    