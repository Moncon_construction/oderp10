# -*- coding: utf-8 -*-
# Part of OdErp. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Account Internal Reports With Budget",
    'version': '1.0',
    'depends': ['l10n_mn_account_internal_report','l10n_mn_account_budget','l10n_mn_account_internal_analytic_report'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
      Account Reports
    """,
    'data': [
        'data/account_income_budget_post_data.xml',
        'data/internal_expense_budget_post_data.xml',
        'data/internal_expense_report_data.xml',
        'data/internal_income_report_data.xml',
        'data/internal_profit_report_data.xml',
        'views/account_view.xml',
        'views/internal_profit_report_view.xml', 
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
