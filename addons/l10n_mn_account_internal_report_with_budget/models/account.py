# -*- coding: utf-8 -*-
from odoo import api, fields, models

class AccountFinancialReport(models.Model):
    _inherit = "account.financial.report"
    
    account_budget_post_ids = fields.Many2many('account.budget.post', 'account_budget_post_financial_report', 'report_line_id', 'budget_post_id', 'Account Budget Posts')
    type = fields.Selection([
        ('sum', 'View'),
        ('accounts', 'Accounts'),
        ('account_type', 'Account Type'),
        ('account_report', 'Report Value'),
        ('account_budget_post', 'Account Budget Post'),
        ], 'Type', default='sum')