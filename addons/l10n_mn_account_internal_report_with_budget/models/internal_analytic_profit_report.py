# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from io import BytesIO
import base64
import time
from odoo import api, models, _
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles

class InternalProfitReport(models.Model):
    _inherit='internal.profit.report'
    
    #Эхлэх дуусах мөчлөгөөс хамааран тайлангийн нэрийг оноох болон өмнөх сартай харьцуулах эсэх товчийг харуулах эсэх утгыг онооно\
    @api.onchange('start_period_id','end_period_id','company_id')
    def onchange_period_id(self):
        result = super(InternalProfitReport, self).onchange_period_id()
        if self.is_analytic == True:
            show_check = False     
            name = ''
            is_compare_before_month = self.is_compare_before_month
             
            employee = self.env['hr.employee'].search([('user_id', '=', self._uid)])
            if self.start_period_id and self.end_period_id and self.company_id : 
                if self.start_period_id == self.end_period_id:
                    show_check = True
                    name = _('%s - %s month Internal Profit Details Analytic Report') % (self.company_id.name, self.start_period_id.name)
                else:                               
                    is_compare_before_month = False
                    name = _('%s - %s - %s months Internal Profit Details Analytic Report') % (self.company_id.name, self.start_period_id.name, self.end_period_id.name)
                self.update({'name': name,
                             'show_check':show_check,
                             'is_compare_before_month':is_compare_before_month,
                             }) 
        else:
            return result
            
    #Мөрийн утгыг цуглуулна
    def get_account_lines(self,data,type):
        result = super(InternalProfitReport, self).get_account_lines(data,type)
        context = dict(self._context or {})
        if self.is_analytic == True:
            lines = []
            sub_account_report = []
            account_report = self.env['account.financial.report'].search([('chart_type', '=', 'revenue_results'),('type', '=', 'sum')],order='sequence')
            child_reports = account_report._get_children_by_order()
            for report in child_reports:
                sub_account_report += report
                for rep in report.account_report_id:
                    if rep.chart_type != 'revenue_results':
                        sub_account_report += rep
            res = self.with_context(data.get('used_context'))._compute_report_balance(sub_account_report)
            comparison_res1 = self.with_context(data.get('comparison_context1'))._compute_report_balance(sub_account_report)
            comparison_res2 = self.with_context(data.get('comparison_context2'))._compute_report_balance(sub_account_report)
            before_month_comp = self.with_context(data.get('before_month_comparison_context'))._compute_report_balance(sub_account_report)
            for report in sub_account_report:
                vals = {
                    'name': report.name,
                    'this_month_plan':res[report.id]['plan'] * report.sign,
                    'this_month_performance': res[report.id]['performance'] * report.sign,
                    'type': report.type,
                    'report_id': report.id,
                    'level': bool(report.style_overwrite) and report.style_overwrite or report.level,
                    'account_type': report.type or False, #used to underline the financial report balances
                    'balance_cmp1':comparison_res1[report.id]['performance'] * report.sign,
                    'balance_cmp2':comparison_res2[report.id]['performance'] * report.sign,
                    'balance_be_month':before_month_comp[report.id]['performance'] * report.sign,
                }
                lines.append(vals)
            return lines
        else:
            return result   
        
        
     #Тайлангийн тохиргооны төсөвийн чиглэлүүдээр шинжилгээний дансыг буцаах
    def _get_budget_posts(self, reports, lines):
        res = {}
        line_ids = lines
        for report in reports:
            if report.id in res:
                continue
            if report.type == 'account_budget_post':
                if report.account_budget_post_ids:
                    if type(res) != list:
                        res[report.id] = self._get_analytic_ids(report.account_budget_post_ids)
                        if type(res[report.id]) != list:
                            for key, value in res.items():
                                line_ids += value
                                res = line_ids
                        else:
                            line_ids += res[report.id]
                            res = line_ids
                    else:
                        res += self._get_analytic_ids(report.account_budget_post_ids)
                else:
                    res = line_ids
            elif report.type == 'account_report' and report.account_report_id:
                res2 = self._get_budget_posts(report.account_report_id,line_ids)
                if type(res2) != list:
                    for key, value in res2.items():
                        line_ids += value
                        res = line_ids
                else:
                    res = res2
        return res
    
    #Төсөвийн мөрүүдэд орсон шинжилгээний дансыг олох
    def _get_analytic_ids(self, account_budget_posts):
        result = []
        analytic_account_ids = []
        date_from = self.start_period_id.date_start
        date_to  = self.end_period_id.date_stop
        company_id = self.company_id.id
        if account_budget_posts:
            self.env.cr.execute("SELECT budl.analytic_account_id as id " 
                "FROM crossovered_budget_lines_period perl  " 
                "LEFT JOIN crossovered_budget_lines budl ON (budl.id=perl.line_id) " 
                "WHERE budl.general_budget_id IN  %s  AND perl.date_from >=  %s  AND perl.date_to <=  %s AND budl.company_id = %s AND budl.analytic_account_id is not null GROUP BY budl.analytic_account_id",
                (tuple(account_budget_posts.ids),date_from,date_to,company_id))
            result = [row['id'] for row in self.env.cr.dictfetchall()]
        return result
    
    #Тайлангийн мөрийг зурах
    @api.multi
    def compute(self):
        res = super(InternalProfitReport, self).compute()
        if self.is_analytic == True:
            data ={}
            c = -1
            profit_lines = self.env['internal.profit.report.line']
            for profit in self:      
                profit.line_ids.unlink() 
                analytic_account_ids = self.analytic_account_ids.ids 
                if not analytic_account_ids:
                    analytic_account_ids = self.env['account.analytic.account'].search([('company_id', '=', self.company_id.id),('type', '=', 'normal')]).ids
                date_from  = self.start_period_id.date_start
                date_to  = self.end_period_id.date_stop
                date_start = datetime.strptime(date_from, "%Y-%m-%d")
                date_end = datetime.strptime(date_to, "%Y-%m-%d")
                day = date_end.day
                month = date_end.month
                year = date_end.year
                before_month_end = (datetime(year, month, day) - relativedelta(months=1))
                one_year_ago_end = (datetime(year, month, day) - relativedelta(years=1))
                two_year_ago_end = (datetime(year, month, day) - relativedelta(years=2))
                before_month_end = self.last_day_of_month(before_month_end)
                one_year_ago_end = self.last_day_of_month(one_year_ago_end)
                two_year_ago_end = self.last_day_of_month(two_year_ago_end)
                before_month_start = date_start - relativedelta(months=1)
                one_year_ago_start = date_start - relativedelta(years=1)
                two_year_ago_start = date_start - relativedelta(years=2)
                this_month_plan = this_month_performance = two_year_ago_performance = one_year_ago_performance = before_month_performance = 0
                pro_coef_before_month_per_percent = pro_coef_one_year_ago_per_percent = pro_coef_two_year_ago_per_percent = this_month_per_percent = 0
                 
                data['used_context'] = {
                        'date_from':date_from,
                        'date_to': date_to,
                        'account_analytic_ids':analytic_account_ids
                }
                year2_comparison_context =  {'date_from': two_year_ago_start, 'date_to': two_year_ago_end, 'account_analytic_ids':analytic_account_ids}
                year1_comparison_context =  {'date_from': one_year_ago_start, 'date_to': one_year_ago_end, 'account_analytic_ids':analytic_account_ids}
                before_month_comparison_context =  {'date_from': before_month_start, 'date_to': before_month_end, 'account_analytic_ids':analytic_account_ids}
                data['comparison_context1'] = year1_comparison_context
                data['comparison_context2'] = year2_comparison_context
                data['before_month_comparison_context'] = before_month_comparison_context
                profit_report_lines = self.get_account_lines(data,'revenue_results')
                for line in profit_report_lines:
                    if line['type'] != 'sum':
                        this_month_plan = line['this_month_plan']
                        this_month_performance = line['this_month_performance']
                        two_year_ago_performance = line['balance_cmp2']
                        one_year_ago_performance = line['balance_cmp1']
                        before_month_performance = line['balance_be_month']
                        if this_month_plan != 0:
                            this_month_per_percent = this_month_performance/this_month_plan*100
                        if before_month_performance != 0:
                            pro_coef_before_month_per_percent = this_month_performance/before_month_performance*100
                        if one_year_ago_performance != 0:
                            pro_coef_one_year_ago_per_percent = this_month_performance/one_year_ago_performance*100
                        if two_year_ago_performance != 0:
                            pro_coef_two_year_ago_per_percent = this_month_performance/two_year_ago_performance*100
                        res = {
                                'name':line['name'],
                                'profit_report_id': profit.id,
                                'report_id': line['report_id'],
                                'this_month_plan':this_month_plan * c,
                                'this_month_performance':this_month_performance * c,
                                'this_month_per_tug':(this_month_performance - this_month_plan) * c,
                                'this_month_per_percent':this_month_per_percent * c,
                                'two_year_ago_performance':two_year_ago_performance * c,
                                'one_year_ago_performance':one_year_ago_performance * c,
                                'before_month_performance':before_month_performance * c,
                                'before_month_per_tug':(this_month_performance - before_month_performance) * c,
                                'before_month_per_percent':pro_coef_before_month_per_percent * c,
                                'one_year_ago_per_tug':(this_month_performance - one_year_ago_performance) * c,
                                'one_year_ago_per_percent':pro_coef_one_year_ago_per_percent * c,
                                'two_year_ago_per_tug':(this_month_performance - two_year_ago_performance) * c,
                                'two_year_ago_per_percent':pro_coef_two_year_ago_per_percent * c,
                        }
                        profit_lines += self.env['internal.profit.report.line'].create(res)
                        pro_coef_before_month_per_percent = pro_coef_one_year_ago_per_percent = pro_coef_two_year_ago_per_percent = 0  
            return True
        else:
            return res
        
#    Тайланг хэвлэх
    @api.multi
    def export_report_analytic(self):
        
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Analytic Internal Profit Details Report')
        sheet_name = _('Integration')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        
        # create formats
        
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_filter_center = book.add_format(ReportExcelCellStyles.format_filter_center)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_group = book.add_format(ReportExcelCellStyles.format_group)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_float_color = book.add_format(ReportExcelCellStyles.format_group_float)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_bold_number = book.add_format(ReportExcelCellStyles.format_content_bold_number)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_bold_left = book.add_format(ReportExcelCellStyles.format_content_bold_left)
        format_content_text_color = book.add_format(ReportExcelCellStyles.format_content_text_color)
        format_content_bold_number_color = book.add_format(ReportExcelCellStyles.format_content_bold_number_color)
        format_content_bold_float = book.add_format(ReportExcelCellStyles.format_content_bold_float)
        format_content_percent_bold = book.add_format(ReportExcelCellStyles.format_content_percent_bold)
        format_content_center_text_color = book.add_format(ReportExcelCellStyles.format_content_center_text_color)
        format_content_percent = book.add_format(ReportExcelCellStyles.format_content_percent)
        format_content_percent_color = book.add_format(ReportExcelCellStyles.format_content_percent_color)
        seq = 1
        rowx = 0
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('internal_profit_details_analytic_report'), form_title=file_name).create({})

#         create sheet
        sheet = book.add_worksheet(sheet_name)
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.39, 0.39, 0.39, 0.39)  # 1cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        
        # compute column
        sheet.set_column('A:A', 3)
        sheet.set_column('B:B', 30)
        sheet.set_column('C:C', 15)
        sheet.set_column('D:D', 15)
        sheet.set_column('E:E', 15)
        sheet.set_column('F:F', 13)
        sheet.set_column('G:G', 13)
        sheet.set_column('H:H', 13)
        sheet.set_column('I:I', 13)
        sheet.set_column('J:J', 13)
        sheet.set_column('K:K', 13)
        sheet.set_column('L:L', 13)
        sheet.set_column('M:M', 13)
        sheet.set_column('N:N', 13)
        sheet.set_column('O:O', 13)
        sheet.set_column('P:P', 13)
        sheet.set_column('Q:Q', 13)
        
        # create employee name
        executives_names = ''
        accountants_names = ''
        company = self.company_id
        executives_employees = []
        accountants_employees = []
        date_from  = self.start_period_id.date_start
        date_to  = self.end_period_id.date_stop
        date_start = datetime.strptime(date_from, "%Y-%m-%d")
        date_end = datetime.strptime(date_to, "%Y-%m-%d")
        day = date_end.day
        month = date_end.month
        year = date_end.year
        before_month_end = (datetime(year, month, day) - relativedelta(months=1))
        one_year_ago_end = (datetime(year, month, day) - relativedelta(years=1))
        two_year_ago_end = (datetime(year, month, day) - relativedelta(years=2))
        before_month_end = self.last_day_of_month(before_month_end)
        one_year_ago_end = self.last_day_of_month(one_year_ago_end)
        two_year_ago_end = self.last_day_of_month(two_year_ago_end)
        before_month_start = date_start - relativedelta(months=1)
        one_year_ago_start = date_start - relativedelta(years=1)
        two_year_ago_start = date_start - relativedelta(years=2)
        if company.executive_signature:
            executives_employees = self.env['hr.employee'].search([('user_id', '=', company.executive_signature.id)])  
        if company.genaral_accountant_signature:
            accountants_employees = self.env['hr.employee'].search([('user_id', '=', company.genaral_accountant_signature.id)])
        
        for employee in executives_employees:
            if employee.last_name:
                executives_names = employee.last_name[0]
            
            executives_names += '.%s' % (employee.name,)
            if employee.job_id.name:
                executives_names += ',  %s' % (employee.job_id.name,)
            
        for employee in accountants_employees:
            if employee.last_name:
                accountants_names = employee.last_name[0]
            
            accountants_names += '.%s' % (employee.name,)
            if employee.job_id.name:
                accountants_names += ',  %s' % (employee.job_id.name,)
                
        analytics = ''
        analytic_account_ids = []
        # Шинжилгээний данс сонгоогүй тохиолдолд тухайн огнооны бүх шинжилгээний дансыг олж дүнг харуулах
        if self.analytic_account_ids:
            analytic_account_ids = self.analytic_account_ids
        else:
            lines = []
            account_report = self.env['account.financial.report'].search([('chart_type', '=', 'revenue_results'),('type', '=', 'sum')],order='sequence')
            child_reports = account_report._get_children_by_order()
            analytic_account_ids = self._get_budget_posts(child_reports,lines)
            analytic_account_ids = self.env['account.analytic.account'].search([('id', 'in', analytic_account_ids)])
            
        for analytic in analytic_account_ids:
            if analytics:
                analytics += ', '
            analytics += analytic.name 
            
            
        #Compute header column
        name_col = 10
        if self.is_compare_two_year_ago and self.is_compare_one_year_ago and self.is_compare_before_month:
            name_col = 14
        if self.is_compare_two_year_ago and self.is_compare_one_year_ago and not self.is_compare_before_month:
            name_col = 11
        if self.is_compare_two_year_ago and  not self.is_compare_one_year_ago and self.is_compare_before_month:
            name_col = 11
        if not self.is_compare_two_year_ago and  self.is_compare_one_year_ago and self.is_compare_before_month:
            name_col = 11
        if self.is_compare_two_year_ago and not self.is_compare_one_year_ago and not self.is_compare_before_month:
            name_col = 8
        if not self.is_compare_two_year_ago and self.is_compare_one_year_ago and not self.is_compare_before_month:
            name_col = 8
        if not self.is_compare_two_year_ago and not self.is_compare_one_year_ago and self.is_compare_before_month:
            name_col = 8
        
        sheet.write(rowx, 0, '%s: %s' % (_('Company'), self.company_id.name), format_filter)
        rowx += 3
        sheet.merge_range(rowx, 0, rowx, name_col, report_name.upper(), format_name)
        rowx += 2
        
        sheet.write(rowx + 1, 0, '%s: %s - %s' % (_('Duration'), self.start_period_id.date_start, self.end_period_id.date_stop), format_filter)
        sheet.write(rowx + 2, 0, '%s: %s' % (_('Created On'), time.strftime('%Y-%m-%d')), format_filter) 
        sheet.write(rowx + 3, 0, '%s:( %s )' % (_('Analytic accounts:Merged'), analytics), format_filter)
        rowx += 5
        # table header
            
        col = 0
        sheet.merge_range(rowx, col, rowx + 1, col, _('№'), format_title)
        col += 1
        sheet.merge_range(rowx, col, rowx + 1, col, _('Reports'), format_title)
        
        col += 1
        if self.is_compare_two_year_ago:
            sheet.merge_range(rowx, col, rowx + 1, col, _('2 years ago performance'), format_title)
            col += 1
        if self.is_compare_one_year_ago:
            sheet.merge_range(rowx, col, rowx + 1, col, _('1 years ago performance'), format_title)
            col += 1
        if self.is_compare_before_month:
            sheet.merge_range(rowx, col, rowx + 1, col, _('Before month performance'), format_title)
            col += 1        
        sheet.merge_range(rowx, col, rowx + 1, col, _('Plan'), format_title)
        col += 1 
        sheet.merge_range(rowx, col, rowx + 1, col, _('Performance'), format_title)
        col += 1     
        sheet.merge_range(rowx, col, rowx, col + 1, _('From the Plan'), format_title)
        sheet.write(rowx + 1, col, _('In ₮'), format_title)
        sheet.write(rowx + 1, col + 1, _('In %'), format_title)
        col += 2   
        if self.is_compare_before_month:
            sheet.merge_range(rowx, col, rowx, col + 1, _('Before Month'), format_title)
            sheet.write(rowx + 1, col, _('In ₮'), format_title)
            sheet.write(rowx + 1, col + 1, _('In %'), format_title)
            col += 2
        if self.is_compare_one_year_ago:
            sheet.merge_range(rowx, col, rowx, col + 1, _('1 year ago performance'), format_title)
            sheet.write(rowx + 1, col, _('In ₮'), format_title)
            sheet.write(rowx + 1, col + 1, _('In %'), format_title)
            col += 2
        if self.is_compare_two_year_ago:
            sheet.merge_range(rowx, col, rowx, col + 1, _('2 year ago performance'), format_title)
            sheet.write(rowx + 1, col, _('In ₮'), format_title)
            sheet.write(rowx + 1, col + 1, _('In %'), format_title)
            col += 2             
        rowx += 2
        
        col = seq = sub_seq = 0
        for line in self.line_ids:
            col = 0
            if line.report_id.style_overwrite == 2: 
                format_float = format_content_float_color
                format_content = format_content_text_color
                format_percent = format_content_percent_color
                format_number = format_content_bold_number_color
            else:                
                format_float = format_content_float
                format_content = format_content_text
                format_percent = format_content_percent
                format_number = format_content_number
            if line.report_id.chart_type == 'revenue_results':
                seq += 1
                if line.report_id.style_overwrite != 2:
                    format_content = format_content_bold_left
                    format_float = format_content_bold_float
                    format_percent = format_content_percent_bold
                    format_number = format_content_bold_number
                sheet.write(rowx, col, seq, format_number)
                sub_seq = 0 
            else:
                sub_seq += 1
                sheet.write(rowx, col, str(seq) + '.' + str(sub_seq), format_number)
            col += 1 
            sheet.write(rowx, col, line.report_id.name, format_content) 
            col += 1 
            if self.is_compare_two_year_ago:
                sheet.write(rowx, col, line.two_year_ago_performance, format_float)
                col += 1
            if self.is_compare_one_year_ago:
                sheet.write(rowx, col, line.one_year_ago_performance, format_float)
                col += 1
            if self.is_compare_before_month:
                sheet.write(rowx, col, line.before_month_performance, format_float)
                col += 1 
            sheet.write(rowx, col, line.this_month_plan, format_float)
            col += 1
            sheet.write(rowx, col, line.this_month_performance, format_float)
            col += 1  
            sheet.write(rowx, col, line.this_month_per_tug, format_float)                
            sheet.write(rowx, col + 1, line.this_month_per_percent/100, format_percent)
            col += 2
            if self.is_compare_before_month:
                sheet.write(rowx, col, line.before_month_per_tug, format_float)                
                sheet.write(rowx, col + 1, line.before_month_per_percent/100, format_percent)
                col += 2
            if self.is_compare_one_year_ago:
                sheet.write(rowx, col, line.one_year_ago_per_tug, format_float)                
                sheet.write(rowx, col + 1, line.one_year_ago_per_percent/100, format_percent)
                col += 2
            if self.is_compare_two_year_ago:
                sheet.write(rowx, col, line.two_year_ago_per_tug, format_float)                
                sheet.write(rowx, col + 1, line.two_year_ago_per_percent/100, format_percent)
                col += 2  
    
            rowx += 1
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, name_col, _('Executive Director:..................................../%s/')%(executives_names),format_filter_center)
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, name_col, _('Accountant:..................................../%s/')%(accountants_names),format_filter_center)
        sheet.hide_gridlines(2) 
        
        for analytic in analytic_account_ids:
            # compute column
            name = analytic.name
            c = -1
            analytic_ids = analytic.ids
            sheet = book.add_worksheet(name)
            sheet.set_paper(9)  # A4
            sheet.set_margins(0.39, 0.39, 0.39, 0.39)  # 1cm, 1cm, 1cm, 1cm
            sheet.fit_to_pages(1, 0)
            
            # compute column
            sheet.set_column('A:A', 3)
            sheet.set_column('B:B', 30)
            sheet.set_column('C:C', 15)
            sheet.set_column('D:D', 15)
            sheet.set_column('E:E', 15)
            sheet.set_column('F:F', 13)
            sheet.set_column('G:G', 13)
            sheet.set_column('H:H', 13)
            sheet.set_column('I:I', 13)
            sheet.set_column('J:J', 13)
            sheet.set_column('K:K', 13)
            sheet.set_column('L:L', 13)
            sheet.set_column('M:M', 13)
            sheet.set_column('N:N', 13)
            sheet.set_column('O:O', 13)
            sheet.set_column('P:P', 13)
            sheet.set_column('Q:Q', 13)
            
            data ={}
            rowx = 0
            col = 0
            data['used_context'] = {
                    'date_from':date_from,
                    'date_to': date_to,
                    'account_analytic_ids':analytic_ids
            }
            year2_comparison_context =  {'date_from': two_year_ago_start, 'date_to': two_year_ago_end, 'account_analytic_ids':analytic_ids}
            year1_comparison_context =  {'date_from': one_year_ago_start, 'date_to': one_year_ago_end, 'account_analytic_ids':analytic_ids}
            before_month_comparison_context =  {'date_from': before_month_start, 'date_to': before_month_end, 'account_analytic_ids':analytic_ids}
            data['comparison_context1'] = year1_comparison_context
            data['comparison_context2'] = year2_comparison_context
            data['before_month_comparison_context'] = before_month_comparison_context
            
            #Compute header column
            name_col = 7
            if self.is_compare_two_year_ago and self.is_compare_one_year_ago and self.is_compare_before_month:
                name_col = 11
            if self.is_compare_two_year_ago and self.is_compare_one_year_ago and not self.is_compare_before_month:
                name_col = 8
            if self.is_compare_two_year_ago and  not self.is_compare_one_year_ago and self.is_compare_before_month:
                name_col = 8
            if not self.is_compare_two_year_ago and  self.is_compare_one_year_ago and self.is_compare_before_month:
                name_col = 8
            if self.is_compare_two_year_ago and not self.is_compare_one_year_ago and not self.is_compare_before_month:
                name_col = 5
            if not self.is_compare_two_year_ago and self.is_compare_one_year_ago and not self.is_compare_before_month:
                name_col = 5
            if not self.is_compare_two_year_ago and not self.is_compare_one_year_ago and self.is_compare_before_month:
                name_col = 5
            
            sheet.write(rowx, 0, '%s: %s' % (_('Company'), self.company_id.name), format_filter)
            rowx += 3
            sheet.merge_range(rowx, 0, rowx, name_col, report_name.upper(), format_name)
            rowx += 2
            sheet.write(rowx + 1, 0, '%s: %s - %s' % (_('Duration'), self.start_period_id.date_start, self.end_period_id.date_stop), format_filter)
            sheet.write(rowx + 2, 0, '%s: %s' % (_('Created On'), time.strftime('%Y-%m-%d')), format_filter) 
            sheet.write(rowx + 3, 0, '%s: %s' % (_('Analytic account'), name), format_filter)
            rowx += 5
                
            col = seq = sub_seq = 0
            sheet.merge_range(rowx, col, rowx + 1, col, _('№'), format_title)
            col += 1
            sheet.merge_range(rowx, col, rowx + 1, col, _('Reports'), format_title)
            
            col += 1
            if self.is_compare_two_year_ago:
                sheet.merge_range(rowx, col, rowx + 1, col, _('2 years ago performance'), format_title)
                col += 1
            if self.is_compare_one_year_ago:
                sheet.merge_range(rowx, col, rowx + 1, col, _('1 years ago performance'), format_title)
                col += 1
            if self.is_compare_before_month:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Before month performance'), format_title)
                col += 1        
             
            sheet.merge_range(rowx, col, rowx + 1, col, _('Plan'), format_title)
            col += 1        
            sheet.merge_range(rowx, col, rowx + 1, col, _('Performance'), format_title)
            col += 1     
            sheet.merge_range(rowx, col, rowx, col + 1, _('From the Plan'), format_title)
            sheet.write(rowx + 1, col, _('In ₮'), format_title)
            sheet.write(rowx + 1, col + 1, _('In %'), format_title)
            col += 2   
            if self.is_compare_before_month:
                sheet.merge_range(rowx, col, rowx, col + 1, _('Before Month'), format_title)
                sheet.write(rowx + 1, col, _('In ₮'), format_title)
                sheet.write(rowx + 1, col + 1, _('In %'), format_title)
                col += 2
            if self.is_compare_one_year_ago:
                sheet.merge_range(rowx, col, rowx, col + 1, _('1 year ago performance'), format_title)
                sheet.write(rowx + 1, col, _('In ₮'), format_title)
                sheet.write(rowx + 1, col + 1, _('In %'), format_title)
                col += 2
            if self.is_compare_two_year_ago:
                sheet.merge_range(rowx, col, rowx, col + 1, _('2 year ago performance'), format_title)
                sheet.write(rowx + 1, col, _('In ₮'), format_title)
                sheet.write(rowx + 1, col + 1, _('In %'), format_title)
                col += 2              
                 
                rowx += 2
                col = seq = sub_seq = 0
                for line in self.line_ids:
                    this_month_plan =  this_month_performance = two_year_ago_performance = one_year_ago_performance = before_month_performance = 0
                    before_month_per_percent = one_year_ago_per_percent = two_year_ago_per_percent = this_month_per_percent = 0
                    this_month_per_tug = one_year_ago_per_tug = two_year_ago_per_tug = 0
                    col = 0
                    if line.report_id.style_overwrite == 2: 
                        format_float = format_content_float_color
                        format_content = format_content_text_color
                        format_percent = format_content_percent_color
                        format_number = format_content_bold_number_color
                    else:                
                        format_float = format_content_float
                        format_content = format_content_text
                        format_percent = format_content_percent
                        format_number = format_content_number
                    if line.report_id.chart_type == 'revenue_results':
                        seq += 1
                        if line.report_id.style_overwrite != 2:
                            format_content = format_content_bold_left
                            format_float = format_content_bold_float
                            format_percent = format_content_percent_bold
                            format_number = format_content_bold_number
                        sheet.write(rowx, col, seq, format_number)
                        sub_seq = 0 
                    else:
                        sub_seq += 1
                        sheet.write(rowx, col, str(seq) + '.' + str(sub_seq), format_number)
                        
                        

                    res = self.with_context(data.get('used_context'))._compute_report_balance(line.report_id)
                    comparison_res1 = self.with_context(data.get('comparison_context1'))._compute_report_balance(line.report_id)
                    comparison_res2 = self.with_context(data.get('comparison_context2'))._compute_report_balance(line.report_id)
                    before_month_comp = self.with_context(data.get('before_month_comparison_context'))._compute_report_balance(line.report_id)
                    name = line.report_id.name
                    two_year_ago_performance = comparison_res2[line.report_id.id]['performance'] * line.report_id.sign * c
                    one_year_ago_performance = comparison_res1[line.report_id.id]['performance'] * line.report_id.sign * c
                    before_month_performance = before_month_comp[line.report_id.id]['performance'] * line.report_id.sign * c
                    this_month_performance = res[line.report_id.id]['performance'] * line.report_id.sign * c
                    this_month_plan = res[line.report_id.id]['plan'] * line.report_id.sign * c
                    this_month_per_tug = this_month_performance - this_month_plan
                    before_month_per_tug = this_month_performance - before_month_performance
                    one_year_ago_per_tug = this_month_performance - one_year_ago_performance
                    two_year_ago_per_tug = this_month_performance - two_year_ago_performance
                    if this_month_plan != 0:
                        this_month_per_percent = this_month_performance/this_month_plan*100
                    if before_month_performance != 0:
                        before_month_per_percent = this_month_performance/before_month_performance*100
                    if one_year_ago_performance != 0:
                        one_year_ago_per_percent = this_month_performance/one_year_ago_performance*100
                    if two_year_ago_performance != 0:
                        two_year_ago_per_percent = this_month_performance/two_year_ago_performance*100
                    col += 1 
                    sheet.write(rowx, col, name, format_content) 
                    col += 1 
                    if self.is_compare_two_year_ago:
                        sheet.write(rowx, col, two_year_ago_performance, format_float)
                        col += 1
                    if self.is_compare_one_year_ago:
                        sheet.write(rowx, col, one_year_ago_performance, format_float)
                        col += 1
                    if self.is_compare_before_month:
                        sheet.write(rowx, col, before_month_performance, format_float)
                        col += 1 
                    sheet.write(rowx, col, this_month_plan, format_float)
                    col += 1
                    sheet.write(rowx, col, this_month_performance, format_float)
                    col += 1  
                    sheet.write(rowx, col, this_month_per_tug, format_float)                
                    sheet.write(rowx, col + 1, this_month_per_percent/100, format_percent)
                    col += 2
                    if self.is_compare_before_month:
                        sheet.write(rowx, col, before_month_per_tug, format_float)                
                        sheet.write(rowx, col + 1, before_month_per_percent/100, format_percent)
                        col += 2
                    if self.is_compare_one_year_ago:
                        sheet.write(rowx, col, one_year_ago_per_tug, format_float)                
                        sheet.write(rowx, col + 1, one_year_ago_per_percent/100, format_percent)
                        col += 2
                    if self.is_compare_two_year_ago:
                        sheet.write(rowx, col, two_year_ago_per_tug, format_float)                
                        sheet.write(rowx, col + 1, two_year_ago_per_percent/100, format_percent)
                        col += 2  
                    rowx += 1
                rowx += 2
                sheet.merge_range(rowx, 0, rowx, name_col, _('Executive Director:..................................../%s/')%(executives_names),format_filter_center)
                rowx += 2
                sheet.merge_range(rowx, 0, rowx, name_col, _('Accountant:..................................../%s/')%(accountants_names),format_filter_center)
                sheet.hide_gridlines(2)     
                
        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        
        # call export function
        return report_excel_output_obj.export_report()      