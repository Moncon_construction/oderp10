# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from io import BytesIO
import base64
import time
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from operator import itemgetter
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles

class InternalIncomeReportLine(models.Model):
    _inherit = "internal.income.report.line"
    
    this_month_plan = fields.Float(string='This month plan')
    this_month_per_tug = fields.Float(string='This month performance Tugrug')
    this_month_per_percent = fields.Float(string='This month performance percent' ,group_operator="avg", digits=(4,2))

     #Журналын мөрүүдийг буцаах
    def _get_accounts(self, reports, lines):
        res = {}
        line_ids = lines
        for report in reports:
            if report.id in res:
                continue
            if report.type == 'account_budget_post':# and line.report_id.account_budget_post_ids:
                if report.account_budget_post_ids:
                    if type(res) != list:
                        res[report.id] = self._get_analytic_lines(report.account_budget_post_ids)
                        if type(res[report.id]) != list:
                            for key, value in res.items():
                                line_ids += value
                                res = line_ids
                        else:
                            line_ids += res[report.id]
                            res = line_ids
                    else:
                        res += self._get_analytic_lines(report.account_budget_post_ids)
                else:
                    res = line_ids
            elif report.type == 'account_report' and report.account_report_id:
                res2 = self._get_accounts(report.account_report_id,line_ids)
                if type(res2) != list:
                    for key, value in res2.items():
                        line_ids += value
                        res = line_ids
                else:
                    res = res2
        return res
    
    #Журналын мөрүүдийг дансаар шүүх олох
    def _get_analytic_lines(self, account_budget_posts):
        result = []
        date_from = self.profit_report_id.start_period_id.date_start
        date_to  = self.profit_report_id.end_period_id.date_stop
        if account_budget_posts:
            for post in account_budget_posts:
                self.env.cr.execute("SELECT budl.analytic_account_id as id " 
                    "FROM crossovered_budget_lines_period perl  " 
                    "LEFT JOIN crossovered_budget_lines budl ON (budl.id=perl.line_id) " 
                    "WHERE budl.general_budget_id IN  %s  AND perl.date_from >=  %s  AND perl.date_to <=  %s AND budl.analytic_account_id is not null GROUP BY budl.analytic_account_id",
                    (tuple(post.ids),date_from,date_to))
                lines = self.env.cr.dictfetchall()
                for bline in lines:
                    analytic_account_id = bline['id']
                    if analytic_account_id and post.account_ids:
                        acc_ids = [x.id for x in post.account_ids]
                        if acc_ids:
                            if analytic_account_id:
                                self.env.cr.execute(
                                    "SELECT id  FROM account_analytic_line WHERE account_id = %s AND (date "
                                    "between to_date(%s,'yyyy-mm-dd') AND to_date(%s,'yyyy-mm-dd')) AND "
                                    "general_account_id=ANY(%s)",
                                    (analytic_account_id, date_from, date_to, acc_ids,))
                            result = [row['id'] for row in self.env.cr.dictfetchall()]
        return result  
    
    #Журналын мөрүүдийг харуулах
    @api.multi
    def open_account_move_lines(self):
        data = {}
        lines = []
        for line in self:
            date_from = line.profit_report_id.start_period_id.date_start
            date_to  = line.profit_report_id.end_period_id.date_stop
            move_line_ids = self._get_accounts(line.report_id,lines)
            if move_line_ids == {}:
                move_line_ids = []
            res = self.env['ir.actions.act_window'].for_xml_id('analytic', 'account_analytic_line_action')
            res['domain'] = [('id', 'in', move_line_ids)]
            res['context'] = {}
            return res