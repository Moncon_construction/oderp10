    # -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from io import BytesIO
import base64
import time
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
            
class InternalProfitReportLine(models.Model):
    _inherit = "internal.profit.report.line"
    
    this_month_plan = fields.Float(string='This month plan')
    this_month_per_tug = fields.Float(string='This month performance Tugrug')
    this_month_per_percent = fields.Float(string='This month performance percent' ,group_operator="avg", digits=(4,2))
        
     #Журналын мөрүүдийг буцаах
    def _get_accounts(self, reports, lines):
        res = {}
        line_ids = lines
        for report in reports:
            if report.id in res:
                continue
            if report.type == 'account_budget_post':# and line.report_id.account_budget_post_ids:
                if report.account_budget_post_ids:
                    if type(res) != list:
                        res[report.id] = self._get_analytic_lines(report.account_budget_post_ids)
                        if type(res[report.id]) != list:
                            for key, value in res.items():
                                line_ids += value
                                res = line_ids
                        else:
                            line_ids += res[report.id]
                            res = line_ids
                    else:
                        res += self._get_analytic_lines(report.account_budget_post_ids)
                else:
                    res = line_ids
            elif report.type == 'account_report' and report.account_report_id:
                res2 = self._get_accounts(report.account_report_id,line_ids)
                if type(res2) != list:
                    for key, value in res2.items():
                        line_ids += value
                        res = line_ids
                else:
                    res = res2
        return res
    
    #Журналын мөрүүдийг дансаар шүүх олох
    def _get_analytic_lines(self, account_budget_posts):
        result = []
        date_from = self.profit_report_id.start_period_id.date_start
        date_to  = self.profit_report_id.end_period_id.date_stop
        if account_budget_posts:
            for post in account_budget_posts:
                self.env.cr.execute("SELECT budl.analytic_account_id as id " 
                    "FROM crossovered_budget_lines_period perl  " 
                    "LEFT JOIN crossovered_budget_lines budl ON (budl.id=perl.line_id) " 
                    "WHERE budl.general_budget_id IN  %s  AND perl.date_from >=  %s  AND perl.date_to <=  %s AND budl.analytic_account_id is not null GROUP BY budl.analytic_account_id",
                    (tuple(post.ids),date_from,date_to))
                lines = self.env.cr.dictfetchall()
                for bline in lines:
                    analytic_account_id = bline['id']
                    if analytic_account_id and post.account_ids:
                        acc_ids = [x.id for x in post.account_ids]
                        if acc_ids:
                            if analytic_account_id:
                                self.env.cr.execute(
                                    "SELECT id  FROM account_analytic_line WHERE account_id = %s AND (date "
                                    "between to_date(%s,'yyyy-mm-dd') AND to_date(%s,'yyyy-mm-dd')) AND "
                                    "general_account_id=ANY(%s)",
                                    (analytic_account_id, date_from, date_to, acc_ids,))
                            result = [row['id'] for row in self.env.cr.dictfetchall()]
        return result  
    
    #Журналын мөрүүдийг харуулах
    @api.multi
    def open_account_move_lines(self):
        data = {}
        lines = []
        for line in self:
            date_from = line.profit_report_id.start_period_id.date_start
            date_to  = line.profit_report_id.end_period_id.date_stop
            move_line_ids = self._get_accounts(line.report_id,lines)
            if move_line_ids == {}:
                move_line_ids = []
            res = self.env['ir.actions.act_window'].for_xml_id('analytic', 'account_analytic_line_action')
            res['domain'] = [('id', 'in', move_line_ids)]
            res['context'] = {}
            return res
    
class InternalProfitReport(models.Model):
    _inherit='internal.profit.report'

    start_period_id = fields.Many2one('account.period', string='Start Period',required = True)
    end_period_id = fields.Many2one('account.period', string='End Period',required = True)            
    
    #Эхлэх дуусах мөчлөгөөс хамааран тайлангийн нэрийг оноох болон өмнөх сартай харьцуулах эсэх товчийг харуулах эсэх утгыг онооно\
    @api.onchange('start_period_id','end_period_id','company_id')
    def onchange_period_id(self):
        context = dict(self._context or {})
        if not context.get('is_analytic'):
            show_check = False     
            name = ''
            is_compare_before_month = self.is_compare_before_month
            
            employee = self.env['hr.employee'].search([('user_id', '=', self._uid)])
            if self.start_period_id and self.end_period_id and self.company_id : 
                if self.start_period_id == self.end_period_id:
                    show_check = True
                else:                               
                    is_compare_before_month = False
                if self.start_period_id == self.end_period_id:
                    name = _('%s - %s month Internal Profit Details Report') % (self.company_id.name, self.start_period_id.name)
                else:
                    name = _('%s - %s - %s months Internal Profit Details Report') % (self.company_id.name, self.start_period_id.name, self.end_period_id.name)
                self.update({'name': name,
                             'show_check':show_check,
                             'is_compare_before_month':is_compare_before_month,
                             }) 
    
    #Ирүүлсэн огноо дахь төлөвлөгөө гүйцэтгэлийн утгыг буцаах
    @api.model
    def compute_budget_plan_and_per(self, post_ids):
        context = dict(self._context or {})
        res = {}
        where = ""
        account_analytic_ids = []
        if context.get('date_to'):
            date_to = context['date_to']
        if context.get('date_from'):
            date_from = context['date_from']
        if context.get('account_analytic_ids'):
            account_analytic_ids = context['account_analytic_ids']
        posts_ids = post_ids
        plan = perf = 0
        if account_analytic_ids:
            where =' AND budl.analytic_account_id IN (' + ','.join(map(str, account_analytic_ids)) + ') '
        if posts_ids:
            self.env.cr.execute("SELECT budl.general_budget_id as post, SUM(perl.planned_amount) AS plan, SUM(perl.practical_amount1) as performance " 
                "FROM crossovered_budget_lines_period perl  " 
                "LEFT JOIN crossovered_budget_lines budl ON (budl.id=perl.line_id) " 
                "WHERE budl.general_budget_id IN  %s " +where+" AND perl.date_from >=  %s  AND perl.date_to <=  %s GROUP BY budl.general_budget_id",
                (tuple(posts_ids),date_from,date_to))
            for row in self.env.cr.dictfetchall():
                res[row['post']] = row
        return res
    
    #Тайлангийн тохиргооноос хамааран дүнгүүдийг тооцоолно
    def _compute_report_balance(self, reports):
        res = {}
        fields = ['plan', 'performance']
        for report in reports:
            if report.id in res:
                continue
            res[report.id] = dict((fn, 0.0) for fn in fields)
            if report.type == 'account_budget_post':
                # it's the sum of the linked budget post
                res[report.id]['account_budget_post'] = self.compute_budget_plan_and_per(report.account_budget_post_ids.ids)
                for value in res[report.id]['account_budget_post'].values():
                    for field in fields:
                        res[report.id][field] += value.get(field)
            elif report.type == 'account_report' and report.account_report_id:
                # it's the amount of the linked report
                res2 = self._compute_report_balance(report.account_report_id)
                for key, value in res2.items():
                    for field in fields:
                        res[report.id][field] += value[field]
            elif report.type == 'sum':
                # it's the sum of the children of this account.report
                res2 = self._compute_report_balance(report.children_ids)
                for key, value in res2.items():
                    for field in fields:
                        res[report.id][field] += value[field]
        return res
    
    #Мөрийн утгыг цуглуулна
    def get_account_lines(self,data,type):
        context = dict(self._context or {})
        if not context.get('is_analytic'):
            lines = []
            account_report = self.env['account.financial.report'].search([('chart_type', '=', type),('type', '=', 'sum')],order='sequence')
            child_reports = account_report._get_children_by_order()
            res = self.with_context(data.get('used_context'))._compute_report_balance(child_reports)
            comparison_res1 = self.with_context(data.get('comparison_context1'))._compute_report_balance(child_reports)
            comparison_res2 = self.with_context(data.get('comparison_context2'))._compute_report_balance(child_reports)
            before_month_comp = self.with_context(data.get('before_month_comparison_context'))._compute_report_balance(child_reports)
            for report in child_reports:
                vals = {
                    'name': report.name,
                    'this_month_plan':res[report.id]['plan'] * report.sign,
                    'this_month_performance': res[report.id]['performance'] * report.sign,
                    'type': report.type,
                    'report_id': report.id,
                    'level': bool(report.style_overwrite) and report.style_overwrite or report.level,
                    'account_type': report.type or False, #used to underline the financial report balances
                    'balance_cmp1':comparison_res1[report.id]['performance'] * report.sign,
                    'balance_cmp2':comparison_res2[report.id]['performance'] * report.sign,
                    'balance_be_month':before_month_comp[report.id]['performance'] * report.sign,
                }
                lines.append(vals)
            return lines
    
    #Тайлангийн мөрийг зурах
    @api.multi
    def compute(self):
        data ={}
        income_lines = self.env['internal.income.report.line']
        expense_lines = self.env['internal.expense.report.line']
        profit_lines = self.env['internal.profit.report.line']
        
        data = {}
        for profit in self:      
            profit.line_ids.unlink()  
            profit.income_line_ids.unlink()  
            profit.expense_line_ids.unlink()  
            date_from  = self.start_period_id.date_start
            date_to  = self.end_period_id.date_stop
            date_start = datetime.strptime(date_from, "%Y-%m-%d")
            date_end = datetime.strptime(date_to, "%Y-%m-%d")
            day = date_end.day
            month = date_end.month
            year = date_end.year
            before_month_end = (datetime(year, month, day) - relativedelta(months=1))
            one_year_ago_end = (datetime(year, month, day) - relativedelta(years=1))
            two_year_ago_end = (datetime(year, month, day) - relativedelta(years=2))
            before_month_end = self.last_day_of_month(before_month_end)
            one_year_ago_end = self.last_day_of_month(one_year_ago_end)
            two_year_ago_end = self.last_day_of_month(two_year_ago_end)
            before_month_start = date_start - relativedelta(months=1)
            one_year_ago_start = date_start - relativedelta(years=1)
            two_year_ago_start = date_start - relativedelta(years=2)
            
            
            this_month_plan = this_month_performance = two_year_ago_performance = one_year_ago_performance = before_month_performance = 0
            pro_coef_before_month_per_percent = pro_coef_one_year_ago_per_percent = pro_coef_two_year_ago_per_percent = this_month_per_percent = 0  
            
            data['used_context'] = {
                    'date_from':date_from,
                    'date_to': date_to,
            }
            year2_comparison_context =  {'date_from': two_year_ago_start, 'date_to': two_year_ago_end}
            year1_comparison_context =  {'date_from': one_year_ago_start, 'date_to': one_year_ago_end}
            before_month_comparison_context =  {'date_from': before_month_start, 'date_to': before_month_end}
            data['comparison_context1'] = year1_comparison_context
            data['comparison_context2'] = year2_comparison_context
            data['before_month_comparison_context'] = before_month_comparison_context
            income_report_lines = self.get_account_lines(data,'sales_income')
            expense_report_lines = self.get_account_lines(data,'acc_expense')
            profit_report_lines = self.get_account_lines(data,'revenue_results')
            
            for line in income_report_lines:
                if line['type'] != 'sum':
                    this_month_plan = line['this_month_plan']
                    this_month_performance = line['this_month_performance']
                    two_year_ago_performance = line['balance_cmp2']
                    one_year_ago_performance = line['balance_cmp1']
                    before_month_performance = line['balance_be_month']
                    if this_month_plan != 0:
                        this_month_per_percent = this_month_performance/this_month_plan*100
                    if before_month_performance != 0:
                        pro_coef_before_month_per_percent = this_month_performance/before_month_performance*100
                    if one_year_ago_performance != 0:
                        pro_coef_one_year_ago_per_percent = this_month_performance/one_year_ago_performance*100
                    if two_year_ago_performance != 0:
                        pro_coef_two_year_ago_per_percent = this_month_performance/two_year_ago_performance*100
                    res = {
                            'name':line['name'],
                            'profit_report_id': profit.id,
                            'report_id': line['report_id'],
                            'this_month_plan':this_month_plan,
                            'this_month_performance':this_month_performance,
                            'this_month_per_tug':this_month_performance - this_month_plan,
                            'this_month_per_percent':this_month_per_percent,
                            'two_year_ago_performance':two_year_ago_performance,
                            'one_year_ago_performance':one_year_ago_performance,
                            'before_month_performance':before_month_performance,
                            'before_month_per_tug':this_month_performance - before_month_performance,
                            'before_month_per_percent':pro_coef_before_month_per_percent,
                            'one_year_ago_per_tug':this_month_performance - one_year_ago_performance,
                            'one_year_ago_per_percent':pro_coef_one_year_ago_per_percent,
                            'two_year_ago_per_tug':this_month_performance - two_year_ago_performance,
                            'two_year_ago_per_percent':pro_coef_two_year_ago_per_percent,
                    }
                    
                    income_lines += self.env['internal.income.report.line'].create(res)
                    this_month_per_percent = pro_coef_before_month_per_percent = pro_coef_one_year_ago_per_percent = pro_coef_two_year_ago_per_percent = 0  
            
            for line in expense_report_lines:
                if line['type'] != 'sum':
                    this_month_plan = line['this_month_plan']
                    this_month_performance = line['this_month_performance']
                    two_year_ago_performance = line['balance_cmp2']
                    one_year_ago_performance = line['balance_cmp1']
                    before_month_performance = line['balance_be_month']
                    if this_month_plan != 0:
                        this_month_per_percent = this_month_performance/this_month_plan*100
                    if before_month_performance != 0:
                        pro_coef_before_month_per_percent = this_month_performance/before_month_performance*100
                    if one_year_ago_performance != 0:
                        pro_coef_one_year_ago_per_percent = this_month_performance/one_year_ago_performance*100
                    if two_year_ago_performance != 0:
                        pro_coef_two_year_ago_per_percent = this_month_performance/two_year_ago_performance*100
                    res = {
                            'name':line['name'],
                            'profit_report_id': profit.id,
                            'report_id': line['report_id'],
                            'this_month_plan':this_month_plan,
                            'this_month_performance':this_month_performance,
                            'this_month_per_tug':this_month_performance - this_month_plan ,
                            'this_month_per_percent':this_month_per_percent,
                            'two_year_ago_performance':two_year_ago_performance,
                            'one_year_ago_performance':one_year_ago_performance,
                            'before_month_performance':before_month_performance,
                            'before_month_per_tug':this_month_performance - before_month_performance,
                            'before_month_per_percent':pro_coef_before_month_per_percent,
                            'one_year_ago_per_tug':this_month_performance - one_year_ago_performance,
                            'one_year_ago_per_percent':pro_coef_one_year_ago_per_percent,
                            'two_year_ago_per_tug':this_month_performance - two_year_ago_performance,
                            'two_year_ago_per_percent':pro_coef_two_year_ago_per_percent,
                    }
                    expense_lines += self.env['internal.expense.report.line'].create(res)
                    this_month_per_percent = pro_coef_before_month_per_percent = pro_coef_one_year_ago_per_percent = pro_coef_two_year_ago_per_percent = 0  
            
            for line in profit_report_lines:
                if line['type'] != 'sum':
                    this_month_plan = line['this_month_plan']
                    this_month_performance = line['this_month_performance']
                    two_year_ago_performance = line['balance_cmp2']
                    one_year_ago_performance = line['balance_cmp1']
                    before_month_performance = line['balance_be_month']
                    if this_month_plan != 0:
                        this_month_per_percent = this_month_performance/this_month_plan*100
                    if before_month_performance != 0:
                        pro_coef_before_month_per_percent = this_month_performance/before_month_performance*100
                    if one_year_ago_performance != 0:
                        pro_coef_one_year_ago_per_percent = this_month_performance/one_year_ago_performance*100
                    if two_year_ago_performance != 0:
                        pro_coef_two_year_ago_per_percent = this_month_performance/two_year_ago_performance*100
                    res = {
                            'name':line['name'],
                            'profit_report_id': profit.id,
                            'report_id': line['report_id'],
                            'this_month_plan':this_month_plan,
                            'this_month_performance':this_month_performance,
                            'this_month_per_tug':this_month_performance - this_month_plan ,
                            'this_month_per_percent':this_month_per_percent,
                            'two_year_ago_performance':two_year_ago_performance,
                            'one_year_ago_performance':one_year_ago_performance,
                            'before_month_performance':before_month_performance,
                            'before_month_per_tug':this_month_performance - before_month_performance,
                            'before_month_per_percent':pro_coef_before_month_per_percent,
                            'one_year_ago_per_tug':this_month_performance - one_year_ago_performance,
                            'one_year_ago_per_percent':pro_coef_one_year_ago_per_percent,
                            'two_year_ago_per_tug':this_month_performance - two_year_ago_performance,
                            'two_year_ago_per_percent':pro_coef_two_year_ago_per_percent,
                    }
                    profit_lines += self.env['internal.profit.report.line'].create(res)
                    this_month_per_percent = pro_coef_before_month_per_percent = pro_coef_one_year_ago_per_percent = pro_coef_two_year_ago_per_percent = 0  
        return True