# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Account Move Group",
    'version': '1.0',
    'depends': ['l10n_mn_account'],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Account Modules',
    'description': """
         
    """,
    'website': 'http://asterisk-tech.mn',
    'data': [
        'security/ir.model.access.csv',
        'views/account_move_group_view.xml',
        'views/account_move_view.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
