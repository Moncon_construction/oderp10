# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class AccountMove(models.Model):
    _inherit = "account.move"

    move_line_group_ids = fields.One2many('account.move.line.group', 'move_id', string='Move Line Groups')


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    line_group_id = fields.Many2one('account.move.line.group')

    @api.model
    def create(self, vals):
        res = super(AccountMoveLine, self).create(vals)
        if res.debit > 0:
            line_group = self.env['account.move.line.group'].search([('move_id', '=', res.move_id.id), ('account_id', '=', res.account_id.id), ('partner_id', '=', res.partner_id.id), ('debit', '>', 0)])
        else:
            line_group = self.env['account.move.line.group'].search([('move_id', '=', res.move_id.id), ('account_id', '=', res.account_id.id), ('partner_id', '=', res.partner_id.id), ('credit', '>', 0)])
        if line_group:
            res.line_group_id = line_group
        else:
            res.line_group_id = self.env['account.move.line.group'].create({'move_id': res.move_id.id,
                                                                            'name': res.name,
                                                                            'account_id': res.account_id.id,
                                                                            # 'debit': res.debit,
                                                                            # 'credit': res.credit,
                                                                            'partner_id': res.partner_id.id,
                                                                            'move_line_ids': [(4, res.id)],
                                                                            # 'analytic_account_id': res.analytic_account_id,
                                                                            'currency_id': res.account_id.currency_id.id,
                                                                            'amount_currency': res.amount_currency,
                                                                            'company_id': res.company_id.id,
                                                                            'min_date': res.date,
                                                                            })
        res.line_group_id._compute_amounts()
        return res

    @api.multi
    def write(self, vals):
        res = super(AccountMoveLine, self).write(vals)
        for move_line in self:
            move_line.line_group_id._compute_amounts()
        return res

    @api.multi
    def unlink(self):
        for move_line in self:
            old_line_group_id = move_line.line_group_id
            move_line.line_group_id = None
            old_line_group_id._compute_amounts()
            if len(old_line_group_id.move_line_ids) == 0:
                old_line_group_id.unlink()
        return super(AccountMoveLine, self).unlink()
