# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class AccountMoveLineGroup(models.Model):
    _name = "account.move.line.group"

    name = fields.Char(string='Name')
    move_id = fields.Many2one('account.move', string='Journal Entry', ondelete="cascade")
    move_line_ids = fields.One2many('account.move.line', 'line_group_id', string='Journal Entry Lines')
    company_currency_id = fields.Many2one('res.currency', related='company_id.currency_id', string="Company Currency", readonly=True, store=True)
    partner_id = fields.Many2one('res.partner', related='move_id.partner_id', string='Partner', store=True)
    account_id = fields.Many2one('account.account', string='Account', required=True, ondelete="cascade")
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    currency_id = fields.Many2one('res.currency', string="Currency")
    debit = fields.Monetary(default=0.0, currency_field='company_currency_id')
    credit = fields.Monetary(default=0.0, currency_field='company_currency_id')
    min_date = fields.Date(string='Pay day')
    amount_currency = fields.Monetary(default=0.0, currency_field='currency_id')
    journal_id = fields.Many2one('account.journal', related='move_id.journal_id', string='Journal', store=True)

    def _compute_amounts(self):
        for this in self:
            this.debit = 0
            this.credit = 0
            this.amount_currency = 0
            for line in this.move_line_ids:
                this.debit += line.debit
                this.credit += line.credit
                this.amount_currency += line.amount_currency

