# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Account Partner Report",
    'version': '1.0',
    'depends': ['l10n_mn_account', 'l10n_mn_report'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Account Modules',
    'description': """
         Харилцагчийн тайлангууд
         
         - Харилцагчийн товчоо
         - Харилцагчийн дэлгэрэнгүй тайлан
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/res_config_view.xml',
        'views/account_partner_report_menu.xml',
        'views/report_account_partner_ledger_wizard.xml',
        'views/report_account_partner_balance_wizard.xml',
        'views/account_partner_report_view.xml',
    ]
}
