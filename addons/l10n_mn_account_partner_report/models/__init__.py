# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import res_company
import res_config
import report_account_partner_ledger_wizard
import report_account_partner_balance_wizard
import account_partner_report