# -*- encoding: utf-8 -*-
##############################################################################
import time

from odoo import api, fields, models, _
from odoo.osv import expression
from odoo.exceptions import UserError


class AccountPartnerReport(models.Model):
    _name = 'account.partner.report'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = 'Account Partner Report'

    name = fields.Char('Name', required=True, track_visibility='onchange', states={'approved': [('readonly', True)]})
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env['res.company']._company_default_get('account.transaction.balance'))
    target_move = fields.Selection([('posted', 'All Posted Entries'),
                                    ('all', 'All Entries')], string='Target Moves', required=True, default='posted', track_visibility='onchange', states={'approved': [('readonly', True)]})
    date_from = fields.Date('Start Date', required=True, default=lambda *a: time.strftime('%Y-01-01'), track_visibility='onchange', states={'approved': [('readonly', True)]})
    date_to = fields.Date('End Date', required=True, default=lambda *a: time.strftime('%Y-%m-%d'), track_visibility='onchange', states={'approved': [('readonly', True)]})
    group_by = fields.Selection([('partner', 'Partner'),
                                 ('account', 'Account'),
                                 ('salesman', 'Salesman')], string='Group By', required=True, default='partner', track_visibility='onchange', states={'approved': [('readonly', True)]})
    account_type = fields.Selection([('all', 'All Accounts'),
                                     ('receivable', 'Receivable Accounts'),
                                     ('payable', 'Payable Accounts'),
                                     ('receivable_payable', 'Receivable and Payable Accounts'),
                                     ('prepaid_income', 'Prepaid Income'),
                                     ('prepaid_expense', 'Prepaid Expense'),
                                     ('prepaid_income_expense', 'Prepaid Income and Expense Accounts')], string='Account Type', required=True, default='all',
                                    track_visibility='onchange', states={'approved': [('readonly', True)]})
    show_non_zero_balance = fields.Boolean(string='Show Partner with balance is not equal to 0', default=False, track_visibility='onchange', states={'approved': [('readonly', True)]})
    show_currency_transaction = fields.Boolean(string='Show Currency Transaction', default=False, track_visibility='onchange', states={'approved': [('readonly', True)]})
    show_employee = fields.Boolean(string='Show Employee', default=False, track_visibility='onchange', states={'approved': [('readonly', True)]})
    partner_ids = fields.Many2many('res.partner', string='Partners', states={'approved': [('readonly', True)]})
    account_ids = fields.Many2many('account.account', string='Accounts', states={'approved': [('readonly', True)]})
    salesperson_ids = fields.Many2many('res.users', string="Salespersons", states={'approved': [('readonly', True)]})
    line_ids = fields.One2many('account.partner.report.line', 'report_id', string='Lines', readonly=True, copy=False)
    currency_line_ids = fields.One2many('account.partner.report.currency.line', 'report_id', string='Currency Lines', readonly=True, copy=False)
    description = fields.Text('Description')
    state = fields.Selection([('draft', 'Draft'),
                              ('approved', 'Approved')], string='State', default='draft', required=True, track_visibility='onchange')

    # Компани, эхлэх болон дуусах огноогоор нэр талбар бөглөгдөх
    @api.onchange('date_from','date_to')
    def onchange_partner_report(self):
        for report in self:
            name = ''
            if report.company_id:
                name += _('%s is ') % self.company_id.name
            if report.date_from:
                name += _('[%s - ') % self.date_from
            if report.date_to:
                name += _('%s] duration Partner Report') % self.date_to
            report.name = name

    @api.onchange('account_type')
    def onchange_account_ids(self):
        if self.account_type == 'receivable':
            return {'domain': {'account_ids': [('internal_type','=','receivable'),('user_type_id.is_prepaid','=',False),('company_id','=',self.company_id.id)]}}
        elif self.account_type == 'payable':
            return {'domain': {'account_ids': [('internal_type','=','payable'),('user_type_id.is_prepaid','=',False),('company_id','=',self.company_id.id)]}}
        elif self.account_type == 'receivable_payable':
            return {'domain': {'account_ids': [('internal_type','in',('receivable','payable')),('user_type_id.is_prepaid', '=', False),('company_id','=',self.company_id.id)]}}
        elif self.account_type == 'prepaid_income':
            return {'domain': {'account_ids': [('internal_type','=','payable'),('user_type_id.is_prepaid', '=', True),('company_id','=',self.company_id.id)]}}
        elif self.account_type == 'prepaid_expense':
            return {'domain': {'account_ids': [('internal_type','=','receivable'),('user_type_id.is_prepaid', '=', True),('company_id','=',self.company_id.id)]}}
        elif self.account_type == 'prepaid_income_expense':
            return {'domain': {'account_ids': [('internal_type','in',('receivable','payable')),('user_type_id.is_prepaid', '=', False),('company_id','=',self.company_id.id)]}}
        else:
            return {'domain': {'account_ids': [('internal_type','in',('receivable','payable')),('company_id','=',self.company_id.id)]}}

    @api.onchange('show_employee')
    def onchange_partner_ids(self):
        if self.show_employee:
            return {'domain': {'partner_ids': [('employee', '=', True)]}}
        else:
            return {'domain': {'partner_ids': []}}

    @api.multi
    def unlink(self):
        for report in self:
            if report.state != 'draft':
                raise UserError(_('Delete only draft in state'))
        return super(AccountPartnerReport, self).unlink()

    @api.multi
    def validate(self):
        # Батлах
        self.write({'state': 'approved'})

    @api.multi
    def action_to_draft(self):
        # Ноороглох
        self.write({'state': 'draft'})

    @api.multi
    def _reset_dict(self):
        # Тайлангийн дэд болон нийт дүнгийн dictionary
        return {
            'start_balance': 0,
            'cur_start_balance': 0,
            'debit': 0,
            'credit': 0,
            'cur_debit': 0,
            'cur_credit': 0,
        }

    @api.multi
    def get_account_ids(self):
        # Данснуудын id-г олох
        account_ids = []
        account_type_ids = []
        account_obj = self.env['account.account']
        account_type_obj = self.env['account.account.type']
        is_payable = is_receivable = False
        if self.account_ids:
            account_ids = self.account_ids.ids
            for account in self.account_ids:
                if account.internal_type == 'receivable' and not is_receivable:
                    is_receivable = True
                if account.internal_type == 'payable' and not is_payable:
                    is_payable = True
        elif self.account_type == 'receivable':
            account_type_ids = account_type_obj.search([('type','=',self.account_type),('is_prepaid','=',False)])
        elif self.account_type == 'payable':
            account_type_ids = account_type_obj.search([('type','=',self.account_type),('is_prepaid','=',False)])
        elif self.account_type == 'receivable_payable':
            account_type_ids = account_type_obj.search([('type','in',('receivable','payable')),('is_prepaid', '=', False)])
            is_payable = is_receivable = True
        elif self.account_type == 'prepaid_income':
            account_type_ids = account_type_obj.search([('type','=','payable'),('is_prepaid', '=', True)])
        elif self.account_type == 'prepaid_expense':
            account_type_ids = account_type_obj.search([('type','=','receivable'),('is_prepaid', '=', True)])
        elif self.account_type == 'prepaid_income_expense':
            account_type_ids = account_type_obj.search([('type','in',('receivable','payable')),('is_prepaid', '=', True)])
            is_payable = is_receivable = True
        else:
            account_type_ids = account_type_obj.search([('type','in',('receivable', 'payable'))])
            is_payable = is_receivable = True
        if len(account_type_ids) > 0:
            account_ids = account_obj.search([('user_type_id', 'in', account_type_ids.ids), ('company_id', '=', self.company_id.id)]).ids
        elif len(account_type_ids) == 0 and len(account_ids) == 0:
            raise UserError(_('No accounts'))
        return account_ids, is_payable, is_receivable

    @api.multi
    def get_partner_ids(self):
        # Харилцагчийн id-г олох
        partner_obj = self.env['res.partner']
        if self.partner_ids:
            partner_ids = partner_obj.with_context(active_test=False).search([('parent_id', 'child_of', self.partner_ids.ids)])
        elif self.show_employee:
            partner_ids = partner_obj.with_context(active_test=False).search([('employee', '=', True)])
        else:
            partner_ids = partner_obj.with_context(active_test=False).search([])
        if len(partner_ids) == 0:
            raise UserError(_('No partners'))
        return partner_ids.ids

    @api.multi
    def get_salesman_ids(self):
        # Борлуулалтын ажилтан id-г олох
        salesman_ids = False
        if self.group_by == 'salesman':
            salesman_ids = self.env['res.users'].search([]).ids
        elif self.salesperson_ids:
            salesman_ids = self.salesperson_ids.ids
        return salesman_ids

    @api.multi
    def get_order_by(self, type):
        # Эрэмбэлэлт
        if self.group_by == 'partner':
            order_by = 'p.name, p.id, aa.internal_type DESC, aa.code, aa.name'
        elif self.group_by == 'account':
            order_by = 'aa.code, aa.name, p.name, p.id'
        else:
            order_by = 'aa.code, aa.name, ru.id, p.name, p.id'
        if type == 'balance':
            order_by += ', mv.ml_date'
        return order_by

    @api.multi
    def get_sub_line(self, sequence, group_line_id, group_line_name, parent_id, color):
        # Тайлангийн бүлэглэлтийн мөр
        return {'account_id': parent_id or (group_line_id if group_line_id and self.group_by in ('account', 'salesman') and color == 'blue' else False),
                'partner_id': group_line_id if group_line_id and self.group_by == 'partner' and color == 'blue' else False,
                'salesperson_id': group_line_id if group_line_id and self.group_by == 'salesman' and color == 'bold' else False,
                'sequence': sequence,
                'group_name': group_line_name,
                'report_id': self.id,
                'color': color
                }

    @api.multi
    def write_sub_line(self, sub, type):
        # Тайлангийн бүлэглэлтийн дэд дүн
        end = sub['start_balance'] + sub['debit'] - sub['credit']
        currency_end = sub['cur_start_balance'] + sub['cur_debit'] - sub['cur_credit']
        sign = 1
        if self.company_id.account_partner_report_settings == 'payable_balance_plus' and type == 'payable':
            sign = -1
        return {'initial': sign * sub['start_balance'] or 0.0,
                'currency_initial': sign * sub['cur_start_balance'] or 0.0,
                'debit': sub['debit'] or 0.0,
                'credit': sub['credit'] or 0.0,
                'currency_debit': sub['cur_debit'] or 0.0,
                'currency_credit': sub['cur_credit'] or 0.0,
                'end': sign * end or 0.0,
                'currency_end': sign * currency_end or 0.0
                }

    @api.multi
    def get_line(self, line, sequence, salesman_id, line_name, end, currency_end):
        # Тайлангийн мөр
        sign = 1
        if self.company_id.account_partner_report_settings == 'payable_balance_plus' and line['atype'] == 'payable':
            sign = -1
        return {'account_id': line['account_id'],
                'partner_id': line['pid'],
                'salesperson_id': salesman_id,
                'sequence': sequence,
                'name': line_name,
                'currency_id': line['cid'] or self.company_id.currency_id.id or False,
                'initial': sign * line['start_balance'] or 0.0,
                'currency_initial': sign * line['cur_start_balance'] or 0.0,
                'debit': line['debit'] or 0.0,
                'credit': line['credit'] or 0.0,
                'currency_debit': line['cur_debit'] or 0.0,
                'currency_credit': line['cur_credit'] or 0.0,
                'end': sign * end or 0.0,
                'currency_end': sign * currency_end or 0.0,
                'report_id': self.id,
                'color': 'black'
                }

    @api.multi
    def get_total(self, dict, line, type):
        # Нийт дүнгүүдийг олох
        if type == 'balance':
            dict['start_balance'] = line['start_balance'] or 0
            dict['cur_start_balance'] = line['cur_start_balance'] or 0
        else:
            dict['start_balance'] += line['start_balance'] or 0
            dict['cur_start_balance'] += line['cur_start_balance'] or 0
        dict['debit'] += line['debit'] or 0
        dict['credit'] += line['credit'] or 0
        dict['cur_debit'] += line['cur_debit'] or 0
        dict['cur_credit'] += line['cur_credit'] or 0

    @api.multi
    def get_total_line(self, dict, name, color, type):
        # Тайлангийн авлага, өглөг болон нийт дүнгийн мөр
        end = dict['start_balance'] + dict['debit'] - dict['credit']
        currency_end = dict['cur_start_balance'] + dict['cur_debit'] - dict['cur_credit']
        sign = 1
        if self.company_id.account_partner_report_settings == 'payable_balance_plus' and type == 'payable':
            sign = -1
        return {'group_name': name,
                'initial': sign * dict['start_balance'] or 0.0,
                'currency_initial': sign * dict['cur_start_balance'] or 0.0,
                'debit': dict['debit'] or 0.0,
                'credit': dict['credit'] or 0.0,
                'currency_debit': dict['cur_debit'] or 0.0,
                'currency_credit': dict['cur_credit'] or 0.0,
                'end': sign * end or 0.0,
                'currency_end': sign * currency_end or 0.0,
                'report_id': self.id,
                'color': color
                }

    @api.multi
    def compute_line(self, line_obj, lines, is_payable, is_receivable):
        # Тооцоолол хийн мөрүүд үүсгэх функц
        group = False
        salesman = False
        rec_total = self._reset_dict()
        payable_total = self._reset_dict()
        total = self._reset_dict()
        sub = self._reset_dict()
        sale_sub = self._reset_dict()
        seq = sub_seq = sale_seq = 1
        internal_type = False
        if is_payable and is_receivable:
            acc_sub = self._reset_dict()
        for line in lines:
            end = line['start_balance'] + line['debit'] - line['credit']
            currency_end = line['cur_start_balance'] + line['cur_debit'] - line['cur_credit']
            salesman_id = False
            if self.group_by == 'partner':
                group_line_id = line['pid']  # Бүлэглэх id
                group_line_name = u'%s' % line['pname']  # Бүлэглэх нэр
                line_name = u'[%s] %s' % (line['acode'], line['aname'])  # Мөрийн нэр
            elif self.group_by == 'account':
                group_line_id = line['account_id']  # Бүлэглэх id
                group_line_name = u'[%s] %s' % (line['acode'], line['aname'])  # Бүлэглэх нэр
                line_name = u'%s' % line['pname']  # Мөрийн нэр
            else:
                # Борлуулагчаар бүлэглэн гаргаж байгаа бол group-лэх утгыг авна
                group_line_id = line['account_id']  # Бүлэглэх id
                group_line_name = u'[%s] %s' % (line['acode'], line['aname'])  # Бүлэглэх нэр
                line_name = u'%s' % line['pname']  # Мөрийн нэр
                salesman_id = line['salesman']
            # Үлдэгдэл дүн 0-с ялгаатай эсэхийг шалгах
            if not self.show_non_zero_balance or (self.show_non_zero_balance and (end != 0 or currency_end != 0)):
                if not group or group != group_line_id:
                    # Бүлэглэлт
                    if group:
                        sub_val = self.write_sub_line(sub, internal_type)
                        group_id.write(sub_val)
                        sub = self._reset_dict()
                        sub_seq += 1
                        seq = 1
                        sale_seq = 0
                    sequence = _('%s') % sub_seq
                    sub_val = self.get_sub_line(sequence, group_line_id, group_line_name, False, 'blue')
                    group_id = line_obj.create(sub_val)
                if self.group_by == 'salesman' and (not salesman or salesman != salesman_id or group != group_line_id):
                    # Борлуулалтын ажилтны бүлэглэлт
                    salesman_name = self.env['res.users'].search([('id', '=', salesman_id)]).name
                    if salesman:
                        sale_sub_val = self.write_sub_line(sale_sub, internal_type)
                        sale_group_id.write(sale_sub_val)
                        sale_sub = self._reset_dict()
                        sale_seq += 1
                        seq = 1
                    sequence = _('%s.%s') % (sub_seq, sale_seq)
                    sale_sub_val = self.get_sub_line(sequence, salesman_id, salesman_name, group_line_id, 'bold')
                    sale_group_id = line_obj.create(sale_sub_val)
                if is_payable and is_receivable and (not internal_type or internal_type != line['atype'] or group != group_line_id):
                    # Авлага, Өглөгийн дансаарх бүлэглэлт
                    type_name = _('Total Receivable')
                    if line['atype'] == 'payable':
                        type_name = _('Total Payable')
                    if internal_type:
                        type_sub_val = self.write_sub_line(acc_sub, internal_type)
                        type_group_id.write(type_sub_val)
                        acc_sub = self._reset_dict()
                    type_sub_val = self.get_sub_line('', False, type_name, False, 'bold')
                    type_group_id = line_obj.create(type_sub_val)
                # Үндсэн мөр
                sequence = _('%s.%s') % (sub_seq, seq)
                if self.group_by == 'salesman':
                    sequence = _('%s.%s.%s') % (sub_seq, sale_seq, seq)
                # Тайлангийн мөрийг үүсгэх
                val = self.get_line(line, sequence, salesman_id, line_name, end, currency_end)
                line_obj.create(val)
                seq += 1
                if line['atype'] == 'receivable':
                    # Нийт авлагын дүнг тооцоолох
                    self.get_total(rec_total, line, 'ledger')
                elif line['atype'] == 'payable':
                    # Нийт өглөгийн дүнг тооцоолох
                    self.get_total(payable_total, line, 'ledger')
                # Нийт дүнг тооцоолох
                self.get_total(total, line, 'ledger')
                # Бүлэглэлтийн дэд дүнг тооцоолох
                self.get_total(sub, line, 'ledger')
                # Борлуулалтын ажилтны бүлэглэлтийн дэд дүнг тооцоолох
                self.get_total(sale_sub, line, 'ledger')
                if is_payable and is_receivable and self.group_by != 'salesman':
                    # Авлага, Өглөгөөр дэд дүнг тооцоолох
                    self.get_total(acc_sub, line, 'ledger')
                if self.group_by == 'partner':
                    group = line['pid']
                elif self.group_by == 'account':
                    group = line['account_id']
                else:
                    group = line['account_id']
                    salesman = line['salesman']
                internal_type = line['atype']
        # Бүлэглэлт
        sub_val = self.write_sub_line(sub, internal_type)
        group_id.write(sub_val)
        if self.group_by == 'salesman':
            # Борлуулалтын ажилтны бүлэглэлт
            sale_sub_val = self.write_sub_line(sale_sub, internal_type)
            sale_group_id.write(sale_sub_val)
        if is_payable and is_receivable:
            # Авлага, Өглөгийн дансаарх бүлэглэлт
            type_sub_val = self.write_sub_line(acc_sub, internal_type)
            type_group_id.write(type_sub_val)
        rec_total_val = self.get_total_line(rec_total, _('Total Receivable'), 'blue', 'receivable')
        line_obj.create(rec_total_val)
        payable_total_val = self.get_total_line(payable_total, _('Total Payable'), 'blue', 'payable')
        line_obj.create(payable_total_val)
        return True

    @api.multi
    def compute(self):
        # Тайлангын утгууд олох
        self.line_ids = None
        self.currency_line_ids = None
        line_obj = self.env['account.partner.report.line']
        if self.show_currency_transaction:
            line_obj = self.env['account.partner.report.currency.line']
        # Данснуудын id-г олох
        account_ids, is_payable, is_receivable = self.get_account_ids()
        # Харилцагчийн id-г олох
        partner_ids = self.get_partner_ids()
        # Борлуулалтын ажилтан id-г олох
        salesman_ids = self.get_salesman_ids()
        # Эрэмбэлэлт
        order_by = self.get_order_by('ledger')
        # Эхлэл болон тухайн хугацааны хоорондох утгыг олох
        lines = self.env['account.move.line'].with_context(salesman_ids=salesman_ids, partner_ids=partner_ids, order_by=order_by).get_all_balance(self.company_id.id, account_ids,
                                                                                                                                  self.date_from, self.date_to, self.target_move)
        if lines:
            self.compute_line(line_obj, lines, is_payable, is_receivable)
        return True


class AccountPartnerReportLine(models.Model):
    _name = 'account.partner.report.line'
    _description = 'Account Partner Report Line'

    sequence = fields.Char('Sequence')
    group_name = fields.Char('Group')
    name = fields.Char('Account/Partner')
    account_id = fields.Many2one('account.account', string='Account')
    partner_id = fields.Many2one('res.partner', string='Partner')
    salesperson_id = fields.Many2one('res.users', string='Salesperson')
    report_id = fields.Many2one('account.partner.report', string='Account Partner Report', ondelete="cascade")
    currency_id = fields.Many2one('res.currency', string='Currency')
    initial = fields.Float(string='Initial Balance', digits=(16, 2), default=0)
    currency_initial = fields.Float(string='Initial Currency Balance', digits=(16, 2), default=0)
    debit = fields.Float(string='Transaction /Dt/', digits=(16, 2), default=0)
    credit = fields.Float(string='Transaction /Ct/', digits=(16, 2), default=0)
    currency_debit = fields.Float(string='Transaction Currency /Dt/', digits=(16, 2), default=0)
    currency_credit = fields.Float(string='Transaction Currency /Ct/', digits=(16, 2), default=0)
    end = fields.Float(string='End Balance', digits=(16, 2), default=0)
    currency_end = fields.Float(string='End Currency Balance', digits=(16, 2), default=0)
    color = fields.Selection([('black', 'Black'),
                              ('bold', 'Bold'),
                              ('blue', 'Blue')], string='Color', default='black')

    @api.multi
    def button_initial_journal_entries(self):
        # Эхний үлдэгдлийн журналын мөрүүдийг харуулах
        for line in self:
            domain = [('date', '<', line.report_id.date_from), ('company_id', '=', line.report_id.company_id.id)]
            if line.account_id:
                domain = expression.AND([domain, [('account_id', '=', line.account_id.id)]])
            if line.partner_id:
                domain = expression.AND([domain, [('partner_id', '=', line.partner_id.id)]])
            if line.salesperson_id:
                domain = expression.AND([domain, [('invoice_id.user_id', '=', line.salesperson_id.id)]])
            if self.report_id.target_move == 'posted':
                domain = expression.AND([domain, [('move_id.state', '=', 'posted')]])
            return {
                'name': _('Journal Entry Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.move.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'context': {'group_by': ['account_id']},
                'domain': domain
                }

    @api.multi
    def button_journal_entries(self):
        # Хугацааны хоорогдох журналын мөрүүдийг харуулах
        for line in self:
            domain = [('date', '>=', line.report_id.date_from), ('date', '<=', line.report_id.date_to), ('company_id', '=', line.report_id.company_id.id)]
            if line.account_id:
                domain = expression.AND([domain, [('account_id', '=', line.account_id.id)]])
            if line.partner_id:
                domain = expression.AND([domain, [('partner_id', '=', line.partner_id.id)]])
            if line.salesperson_id:
                domain = expression.AND([domain, [('invoice_id.user_id', '=', line.salesperson_id.id)]])
            if self.report_id.target_move == 'posted':
                domain = expression.AND([domain, [('move_id.state', '=', 'posted')]])
            return {
                'name': _('Journal Entry Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.move.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'context': {'group_by': ['account_id']},
                'domain': domain
            }

class AccountPartnerReportCurrencyLine(models.Model):
    _name = 'account.partner.report.currency.line'
    _description = 'Account Partner Report Currency Line'

    sequence = fields.Char('Sequence')
    group_name = fields.Char('Group')
    name = fields.Char('Account/Partner')
    account_id = fields.Many2one('account.account', string='Account')
    partner_id = fields.Many2one('res.partner', string='Partner')
    salesperson_id = fields.Many2one('res.users', string='Salesperson')
    report_id = fields.Many2one('account.partner.report', string='Account Partner Report', ondelete="cascade")
    currency_id = fields.Many2one('res.currency', string='Currency')
    initial = fields.Float(string='Initial Balance', digits=(16, 2), default=0)
    currency_initial = fields.Float(string='Initial Currency Balance', digits=(16, 2), default=0)
    debit = fields.Float(string='Transaction /Dt/', digits=(16, 2), default=0)
    credit = fields.Float(string='Transaction /Ct/', digits=(16, 2), default=0)
    currency_debit = fields.Float(string='Transaction Currency /Dt/', digits=(16, 2), default=0)
    currency_credit = fields.Float(string='Transaction Currency /Ct/', digits=(16, 2), default=0)
    end = fields.Float(string='End Balance', digits=(16, 2), default=0)
    currency_end = fields.Float(string='End Currency Balance', digits=(16, 2), default=0)
    color = fields.Selection([('black', 'Black'),
                              ('bold', 'Bold'),
                              ('blue', 'Blue')], string='Color', default='black')

    @api.multi
    def button_initial_journal_entries(self):
        # Эхний үлдэгдлийн журналын мөрүүдийг харуулах
        for line in self:
            domain = [('date', '<', line.report_id.date_from), ('company_id', '=', line.report_id.company_id.id)]
            if line.account_id:
                domain = expression.AND([domain, [('account_id', '=', line.account_id.id)]])
            if line.partner_id:
                domain = expression.AND([domain, [('partner_id', '=', line.partner_id.id)]])
            if line.salesperson_id:
                domain = expression.AND([domain, [('invoice_id.user_id', '=', line.salesperson_id.id)]])
            if self.report_id.target_move == 'posted':
                domain = expression.AND([domain, [('move_id.state', '=', 'posted')]])
            return {
                'name': _('Journal Entry Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.move.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'context': {'group_by': ['account_id']},
                'domain': domain
                }

    @api.multi
    def button_journal_entries(self):
        # Хугацааны хоорогдох журналын мөрүүдийг харуулах
        for line in self:
            domain = [('date', '>=', line.report_id.date_from), ('date', '<=', line.report_id.date_to), ('company_id', '=', line.report_id.company_id.id)]
            if line.account_id:
                domain = expression.AND([domain, [('account_id', '=', line.account_id.id)]])
            if line.partner_id:
                domain = expression.AND([domain, [('partner_id', '=', line.partner_id.id)]])
            if line.salesperson_id:
                domain = expression.AND([domain, [('invoice_id.user_id', '=', line.salesperson_id.id)]])
            if self.report_id.target_move == 'posted':
                domain = expression.AND([domain, [('move_id.state', '=', 'posted')]])
            return {
                'name': _('Journal Entry Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.move.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'context': {'group_by': ['account_id']},
                'domain': domain
            }