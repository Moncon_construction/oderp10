# -*- coding: utf-8 -*-
from odoo import fields, models, _

class ResCompany(models.Model):
    _inherit = "res.company"

    account_partner_report_settings = fields.Selection([('payable_balance_minus', 'Payable Balance Minus'),
                                                        ('payable_balance_plus', 'Payable Balance Plus')],
                                                       default='payable_balance_minus', string='Account Partner Report Settings')
