# -*- coding: utf-8 -*-
from odoo import fields, models, _


class AccountConfigSettings(models.TransientModel):
    _inherit = 'account.config.settings'
    _description = 'Account settings'

    account_partner_report_settings = fields.Selection([('payable_balance_minus', 'Payable Balance Minus'),
                                                        ('payable_balance_plus', 'Payable Balance Plus')],
                                                       default='payable_balance_minus', related='company_id.account_partner_report_settings', string='Account Partner Report Settings')