# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell

from odoo import api, models
from odoo.tools.translate import _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class AccountPartnerReport(models.Model):
    _inherit = 'account.partner.report'

    @api.multi
    def get_format(self, book):
        # create format
        return {'name': book.add_format(ReportExcelCellStyles.format_name),
                'filter': book.add_format(ReportExcelCellStyles.format_filter),
                'title': book.add_format(ReportExcelCellStyles.format_title),
                'content_text': book.add_format(ReportExcelCellStyles.format_content_text),
                'content_center': book.add_format(ReportExcelCellStyles.format_content_center),
                'content_float': book.add_format(ReportExcelCellStyles.format_content_float),
                'content_float_color': book.add_format(ReportExcelCellStyles.format_content_float_fcolor),
                'group_left': book.add_format(ReportExcelCellStyles.format_group_left),
                'group': book.add_format(ReportExcelCellStyles.format_group),
                'group_float': book.add_format(ReportExcelCellStyles.format_group_float),
                'group_float_color': book.add_format(ReportExcelCellStyles.format_group_float_color)
                }

    @api.multi
    def get_sheet(self, book, format, report_name, type):
        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_portrait()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 1
        sheet, colx_number = self.get_column(sheet, type)
        # create company
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s' % (_('Company'), self.company_id and self.company_id.name or ''), format['filter'])
        rowx += 1
        sheet.merge_range(rowx, 0, rowx + 1, colx_number, report_name.upper(), format['name'])
        rowx += 2
        # create duration
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s - %s' % (_('Duration'), self.date_from, self.date_to), format['filter'])
        rowx += 1
        # create date
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s' % (_('Created On'), time.strftime('%Y-%m-%d')), format['filter'])
        rowx += 2
        return sheet, rowx

    @api.multi
    def get_column(self, sheet, type):
        # compute column
        sheet.set_column('A:A', 6)
        sheet.set_column('B:B', 30)
        if type == 'balance':
            sheet.set_column('C:D', 8)
            sheet.set_column('E:E', 25)
            sheet.set_column('F:G', 10)
            sheet.set_column('H:R', 20)
            colx = 10
        else:
            sheet.set_column('C:C', 10)
            sheet.set_column('D:R', 20)
            colx = 6
        if self.show_currency_transaction:
            colx += 4
        return sheet, colx

    @api.multi
    def get_text_header(self, sheet, rowx, format_title):
        # Товчоо тайлангийн хүснэгтийн толгой зурах
        row = 1
        if self.show_currency_transaction:
            row = 2
        sheet.merge_range(rowx, 0, rowx + row, 0, _('Seq'), format_title)
        if self.group_by == 'salesman':
            sheet.merge_range(rowx, 1, rowx + row, 1, _('Account/Salesman/Partner\n Reference'), format_title)
        else:
            sheet.merge_range(rowx, 1, rowx + row, 1, _('Partner/Account\n Reference'), format_title)
        sheet.merge_range(rowx, 2, rowx + row, 2, _('Currency'), format_title)
        col = 3
        return sheet, col

    @api.multi
    def get_balance_header(self, sheet, rowx, format_title):
        # Дэлгэрэнгүй тайлангийн хүснэгтийн толгой зурах
        row = 1
        if self.show_currency_transaction:
            row = 2
        sheet.merge_range(rowx, 0, rowx + row, 0, _('Seq'), format_title)
        if self.group_by == 'salesman':
            sheet.merge_range(rowx, 1, rowx + row, 1, _('Account/Salesman/Partner\n Reference'), format_title)
        else:
            sheet.merge_range(rowx, 1, rowx + row, 1, _('Partner/Account\n Reference'), format_title)
        sheet.merge_range(rowx, 2, rowx + row, 2, _('Origin'), format_title)
        sheet.merge_range(rowx, 3, rowx + row, 3, _('Date'), format_title)
        sheet.merge_range(rowx, 4, rowx + row, 4, _('Transaction name'), format_title)
        sheet.merge_range(rowx, 5, rowx + row, 5, _('Currency'), format_title)
        sheet.merge_range(rowx, 6, rowx + row, 6, _('Currency Rate'), format_title)
        col = 7
        return sheet, col

    @api.multi
    def get_header(self, sheet, rowx, col, format_title, type):
        rowc = rowx
        # Тайлангийн хүснэгтийн толгой зурах
        if self.show_currency_transaction:
            sheet.merge_range(rowx, col, rowx + 1, col + 1, _('Initial Balance'), format_title)
            sheet.write(rowx + 2, col, _('By Currency'), format_title)
            sheet.write(rowx + 2, col + 1, _('By MNT'), format_title)
            sheet.merge_range(rowx, col + 2, rowx, col + 5, _('Transaction Amount'), format_title)
            sheet.merge_range(rowx + 1, col + 2, rowx + 1, col + 3, _('Debit'), format_title)
            sheet.write(rowx + 2, col + 2, _('By Currency'), format_title)
            sheet.write(rowx + 2, col + 3, _('By MNT'), format_title)
            sheet.merge_range(rowx + 1, col + 4, rowx + 1, col + 5, _('Credit'), format_title)
            sheet.write(rowx + 2, col + 4, _('By Currency'), format_title)
            sheet.write(rowx + 2, col + 5, _('By MNT'), format_title)
            sheet.merge_range(rowx, col + 6, rowx + 1, col + 7, _('End Balance'), format_title)
            sheet.write(rowx + 2, col + 6, _('By Currency'), format_title)
            sheet.write(rowx + 2, col + 7, _('By MNT'), format_title)
            col += 8
            rowx += 3
        else:
            sheet.merge_range(rowx, col, rowx + 1, col, _('Initial Balance'), format_title)
            sheet.merge_range(rowx, col + 1, rowx, col + 2, _('Transaction Amount'), format_title)
            sheet.write(rowx + 1, col + 1, _('Debit'), format_title)
            sheet.write(rowx + 1, col + 2, _('Credit'), format_title)
            sheet.merge_range(rowx, col + 3, rowx + 1, col + 3, _('End Balance'), format_title)
            col += 4
            rowx += 2
        if type == 'balance':
            sheet.merge_range(rowc, col, rowx -1, col, _('Reconcile'), format_title)
            if self.group_by == 'partner':
                sheet.merge_range(rowc, col + 1, rowx - 1, col + 1, _('Limit'), format_title)
        return sheet, rowx

    @api.multi
    def get_lines(self):
        lines = self.line_ids
        if self.show_currency_transaction:
            lines = self.currency_line_ids
        return lines

    @api.multi
    def get_balance_line(self):
        # Данснуудын id-г олох
        account_ids, is_payable, is_receivable = self.get_account_ids()
        # Харилцагчийн id-г олох
        partner_ids = self.get_partner_ids()
        # Борлуулалтын ажилтан id-г олох
        salesman_ids = self.get_salesman_ids()
        # Эрэмбэлэлт
        order_by = self.get_order_by('balance')
        # Эхлэл болон тухайн хугацааны хоорондох утгыг олох
        lines = self.env['account.move.line'].with_context(salesman_ids=salesman_ids, partner_ids=partner_ids, order_by=order_by) \
                        .get_all_partner_balance(self.company_id.id, account_ids, self.date_from, self.date_to, self.target_move)
        return lines, is_payable, is_receivable

    @api.multi
    def get_text_value(self, sheet, rowx, format, line):
        # Товчоо тайлангийн мөрийн текст утгыг зурах
        if line.color in ('blue', 'bold'):
            sheet.write(rowx, 0, line.sequence, format['group'])
            sheet.merge_range(rowx, 1, rowx, 2, line.group_name, format['group_left'])
        else:
            sheet.write(rowx, 0, line.sequence, format['content_center'])
            sheet.write(rowx, 1, line.name or '', format['content_text'])
            sheet.write(rowx, 2, line.currency_id.name, format['content_center'])
        col = 2
        return sheet, col

    @api.multi
    def get_balance_text_value(self, sheet, rowx, format, line, sequence, name, is_group):
        # Тайлангийн бүлэглэлт болон мөрийн текст утгыг зурах
        if is_group:
            col = 0
            if sequence:
                sheet.write(rowx, 0, sequence, format['group'])
                col = 1
            sheet.merge_range(rowx, col, rowx, 6, name, format['group_left'] if sequence else format['group'])
        else:
            text = u''
            invoice = False
            if line['invoice']:
                invoice = self.env['account.invoice'].search([('id', '=', line['invoice'])])
                text = invoice.origin if invoice and invoice.origin else ''
            number = (invoice and invoice.number or '') or line['mname'] or line['ref']
            sheet.write(rowx, 0, sequence, format['content_center'])
            sheet.write(rowx, 1, number, format['content_text'])
            sheet.write(rowx, 2, text, format['content_text'])
            sheet.write(rowx, 3, line['ml_date'], format['content_center'])
            sheet.write(rowx, 4, line['name'], format['content_text'])
            sheet.write(rowx, 5, line['cname'] or self.company_id.currency_id and self.company_id.currency_id.name or '', format['content_center'])
            sheet.write(rowx, 6, line['currency_rate'], format['content_center'])
        col = 6
        return sheet, col, rowx

    @api.multi
    def get_value(self, sheet, rowx, col, format, line):
        # Тайлангийн мөрийн тоон утгыг зурах
        colx = 1
        format_float = format['content_float']
        format_float_color = format['content_float_color']
        if line.color in ('blue', 'bold'):
            format_float = format['group_float']
            format_float_color = format['group_float_color']
        if self.show_currency_transaction:
            colx = 2
            sheet.write(rowx, col + 1, line.currency_initial, format_float_color)
            sheet.write(rowx, col + 3, line.currency_debit, format_float_color)
            sheet.write(rowx, col + 5, line.currency_credit, format_float_color)
            sheet.write(rowx, col + 7, line.currency_end, format_float_color)
        sheet.write(rowx, col + colx, line.initial, format_float)
        sheet.write(rowx, col + colx * 2, line.debit, format_float)
        sheet.write(rowx, col + colx * 3, line.credit, format_float)
        sheet.write(rowx, col + colx * 4, line.end, format_float)
        return sheet

    @api.multi
    def get_balance_value(self, sheet, rowx, col, format, line, type, start, is_group):
        # Тайлангийн мөрийн тоон утгыг зурах
        colx = sign = 1
        if self.company_id.account_partner_report_settings == 'payable_balance_plus' and type == 'payable':
            sign = -1
        format_float = format['content_float']
        format_float_color = format['content_float_color']
        if is_group:
            format_float = format['group_float']
            format_float_color = format['group_float_color']
            start['start_balance'] = line['start_balance']
            start['cur_start_balance'] = line['cur_start_balance']
        end = start['start_balance'] + line['debit'] - line['credit']
        currency_end = start['cur_start_balance'] + line['cur_debit'] - line['cur_credit']
        if self.show_currency_transaction:
            colx = 2
            sheet.write(rowx, col + 1, sign * start['cur_start_balance'] or 0, format_float_color)
            sheet.write(rowx, col + 3, line['cur_debit'] or 0, format_float_color)
            sheet.write(rowx, col + 5, line['cur_credit'] or 0, format_float_color)
            sheet.write(rowx, col + 7, sign * currency_end, format_float_color)
        sheet.write(rowx, col + colx, sign * start['start_balance'] or 0, format_float)
        sheet.write(rowx, col + colx * 2, line['debit'] or 0, format_float)
        sheet.write(rowx, col + colx * 3, line['credit'] or 0, format_float)
        sheet.write(rowx, col + colx * 4, sign * end, format_float)
        sheet.write(rowx, col + colx * 4 + 1, line['frname'] if not is_group else '', format['content_center'] if not is_group else format['group'])
        if self.group_by == 'partner':
            if 'limit' in line.keys() and is_group:
                sheet.write(rowx, col + colx * 4 + 2, line['limit'] or 0, format_float)
            else:
                sheet.write(rowx, col + colx * 4 + 2, '', format['content_text'] if not is_group else format['group'])
        return sheet, end, currency_end

    @api.multi
    def get_start_balance(self, dict, line, is_group, end, currency_end):
        # Эхний үлдэгдлийг олох
        if is_group:
            dict['start_balance'] = line['start_balance'] or 0
            dict['cur_start_balance'] = line['cur_start_balance'] or 0
        else:
            dict['start_balance'] = end
            dict['cur_start_balance'] = currency_end

    @api.multi
    def get_all(self, dict, line, is_group):
        # Нийт дүнгүүдийг олох
        if is_group:
            dict['start_balance'] += line['start_balance'] or 0
            dict['cur_start_balance'] += line['cur_start_balance'] or 0
        dict['debit'] += line['debit'] or 0
        dict['credit'] += line['credit'] or 0
        dict['cur_debit'] += line['cur_debit'] or 0
        dict['cur_credit'] += line['cur_credit'] or 0

    @api.multi
    def get_footer(self, sheet, rowx, format):
        # create footer
        sheet.merge_range(rowx, 1, rowx, 2, '%s: ........................................... (                          )' % _('Executive Director'), format['filter'])
        rowx += 1
        sheet.merge_range(rowx, 1, rowx, 2, '%s: ........................................... (                          )' % _('General Accountant'), format['filter'])

    @api.multi
    def export_ledger_report(self):
        # Авлага, Өглөгийн товчоо тайлан
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        # create name
        report_name = _('Partner Ledger Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        # create formats
        format = self.get_format(book)
        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('partner_ledger_report'), form_title=file_name).create({})
        # create sheet
        sheet, rowx = self.get_sheet(book, format, report_name, 'ledger')
        # create header
        sheet, col = self.get_text_header(sheet, rowx, format['title'])
        sheet, rowx = self.get_header(sheet, rowx, col, format['title'], 'ledger')
        lines = self.get_lines()
        # Авлага, өглөгийн товчоо тайлангийн мөрийг зурах
        for line in lines:
            sheet, col = self.get_text_value(sheet, rowx, format, line)
            sheet = self.get_value(sheet, rowx, col, format, line)
            rowx += 1
        rowx += 2
        # create footer
        self.get_footer(sheet, rowx, format)
        book.close()
        # set file dat
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report()

    @api.multi
    def export_balance_report(self):
        # Авлага, Өглөгийн дэлгэрэнгүй тайлан
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        # create name
        report_name = _('Partner Balance Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        # create formats
        format = self.get_format(book)
        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('partner_balance_report'), form_title=file_name).create({})
        # create sheet
        sheet, rowx = self.get_sheet(book, format, report_name, 'balance')
        # create header
        sheet, col = self.get_balance_header(sheet, rowx, format['title'])
        sheet, rowx = self.get_header(sheet, rowx, col, format['title'], 'balance')
        lines, is_payable, is_receivable = self.get_balance_line()
        group = False
        sub_group = False
        salesman = False
        internal_type = False
        start = self._reset_dict()
        total = self._reset_dict()
        rec_total = self._reset_dict()
        payable_total = self._reset_dict()
        group_total = self._reset_dict()
        sub_total = self._reset_dict()
        sale_total = self._reset_dict()
        seq = group_seq = sub_seq = sale_seq = 1
        end = currency_end = 0
        # Авлага, өглөгийн дэлгэрэнгүй тайлангийн мөрийг зурах
        for line in lines:
            if line['ml_id'] != 0 or line['start_balance'] != 0 or line['cur_start_balance'] != 0:
                is_group = False
                salesman_id = False
                if self.group_by == 'partner':
                    group_line_id = line['pid']  # Бүлэглэх id
                    group_line_name = u'%s' % line['pname']  # Бүлэглэх нэр
                    sub_id = line['account_id']
                    sub_name = u'[%s] %s' % (line['acode'], line['aname'])  # Дэд бүлэглэлтийн нэр
                elif self.group_by == 'account':
                    group_line_id = line['account_id']  # Бүлэглэх id
                    group_line_name = u'[%s] %s' % (line['acode'], line['aname'])  # Бүлэглэх нэр
                    sub_id = line['pid']
                    sub_name = u'%s' % line['pname']  # Дэд бүлэглэлтийн нэр
                else:
                    # Борлуулагчаар бүлэглэн гаргаж байгаа бол group-лэх утгыг авна
                    group_line_id = line['account_id']  # Бүлэглэх id
                    group_line_name = u'[%s] %s' % (line['acode'], line['aname'])  # Бүлэглэх нэр
                    sub_id = line['pid']
                    sub_name = u'%s' % line['pname']  # Дэд бүлэглэлтийн нэр
                    salesman_id = line['salesman']
                if not group or group != group_line_id:
                    is_group = True
                    # Бүлэглэлт
                    if group:
                        sheet = self.get_balance_value(sheet, rowg, col, format, group_total, internal_type, start, True)[0]
                        group_total = self._reset_dict()
                        group_seq += 1
                        sub_seq = 0
                        sale_seq = 0
                        seq = 1
                    sheet, col, rowg = self.get_balance_text_value(sheet, rowx, format, line, _('%s') % group_seq, group_line_name, True)
                    rowx += 1
                if self.group_by == 'salesman' and (not salesman or salesman != salesman_id or group != group_line_id):
                    # Борлуулалтын ажилтны бүлэглэлт
                    is_group = True
                    salesman_name = self.env['res.users'].search([('id', '=', salesman_id)]).name
                    if salesman:
                        sheet = self.get_balance_value(sheet, rows, col, format, sale_total, internal_type, start, True)[0]
                        sale_total = self._reset_dict()
                        sale_seq += 1
                        sub_seq = 0
                        seq = 1
                    sheet, col, rows = self.get_balance_text_value(sheet, rowx, format, line, _('%s.%s') % (group_seq, sale_seq), salesman_name, True)
                    sheet.set_row(rowx, None, None, {'hidden': 0, 'level': 1})
                    rowx += 1
                if not sub_group or sub_group != sub_id or group != group_line_id or (self.group_by == 'salesman' and salesman != salesman_id):
                    # Дэд бүлэглэлт
                    is_group = True
                    if sub_group:
                        sheet = self.get_balance_value(sheet, rowsub, col, format, sub_total, internal_type, start, True)[0]
                        sub_total = self._reset_dict()
                        sub_seq += 1
                        seq = 1
                    # Үндсэн мөр
                    level = 1
                    sequence = _('%s.%s') % (group_seq, sub_seq)
                    if self.group_by == 'salesman':
                        sequence = _('%s.%s.%s') % (group_seq, sale_seq, sub_seq)
                        level = 2
                    sheet, col, rowsub = self.get_balance_text_value(sheet, rowx, format, line, sequence, sub_name, True)
                    sheet.set_row(rowx, None, None, {'hidden': 0, 'level': level})
                    rowx += 1
                # Үндсэн мөр
                sequence = _('%s.%s.%s') % (group_seq, sub_seq, seq)
                if self.group_by == 'salesman':
                    sequence = _('%s.%s.%s.%s') % (group_seq, sale_seq, sub_seq, seq)
                # Тайлангийн мөрийг үүсгэх
                if line['ml_id'] != 0:
                    sheet, col, rowx = self.get_balance_text_value(sheet, rowx, format, line, sequence, line['name'], False)
                    self.get_start_balance(start, line, is_group, end, currency_end)
                    sheet, end, currency_end = self.get_balance_value(sheet, rowx, col, format, line, line['atype'], start, False)
                    sheet.set_row(rowx, None, None, {'hidden': 0, 'level': level + 1})
                    rowx += 1
                    seq += 1
                if line['atype'] == 'receivable':
                    # Нийт авлагын дүнг тооцоолох
                    self.get_all(rec_total, line, is_group)
                elif line['atype'] == 'payable':
                    # Нийт өглөгийн дүнг тооцоолох
                    self.get_all(payable_total, line, is_group)
                # Нийт дүнг тооцоолох
                self.get_all(total, line, is_group)
                # Бүлэглэлтийн дэд дүнг тооцоолох
                self.get_total(group_total, line, 'balance')
                # Борлуулалтын ажилтны бүлэглэлтийн дэд дүнг тооцоолох
                self.get_total(sale_total, line, 'balance')
                # Дэд бүлэглэлтийн дэд дүнг тооцоолох
                self.get_total(sub_total, line, 'balance')
                internal_type = line['atype']
                if self.group_by == 'partner':
                    group = line['pid']
                    sub_group = line['account_id']
                elif self.group_by == 'account':
                    group = line['account_id']
                    sub_group = line['pid']
                else:
                    group = line['account_id']
                    sub_group = line['pid']
                    salesman = line['salesman']
        if group:
            # Бүлэглэлт
            sheet = self.get_balance_value(sheet, rowg, col, format, group_total, internal_type, start, True)[0]
        if self.group_by == 'salesman' and salesman:
            # Борлуулалтын ажилтны бүлэглэлт
            sheet = self.get_balance_value(sheet, rows, col, format, sale_total, internal_type, start, True)[0]
        if sub_group:
            # Дэд бүлэглэлт
            sheet = self.get_balance_value(sheet, rowsub, col, format, sub_total, internal_type, start, True)[0]
        # Авлага, Өглөгийн дансаарх бүлэглэлт
        sheet, col, rows = self.get_balance_text_value(sheet, rowx, format, False, '', _('TOTAL RECEIVABLE'), True)
        sheet = self.get_balance_value(sheet, rowx, col, format, rec_total, 'all', start, True)[0]
        rowx += 1
        sheet, col, rows = self.get_balance_text_value(sheet, rowx, format, False, '', _('TOTAL PAYABLE'), True)
        sheet = self.get_balance_value(sheet, rowx, col, format, payable_total, 'all', start, True)[0]
        rowx += 2
        # create footer
        self.get_footer(sheet, rowx, format)
        book.close()
        # set file dat
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report()