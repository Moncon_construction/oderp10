# -*- coding: utf-8 -*-
{
    'name': 'Mongolian Account Period',
    'version': '1.1',
    'depends': ['l10n_mn_account'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """

    """,
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'wizard/account_period_close_view.xml',
        'views/account_period_view.xml',
        'wizard/account_fiscalyear_close_view.xml',
        'views/account_fiscalyear_view.xml',
        'wizard/configuration_view.xml',
        'views/account_journal_view.xml',
        'views/account_config_settings_view.xml',
        'views/account_move_view.xml',
        'views/account_move_line_view.xml',
    ],
    'installable': True,
    'auto_install': False
}
