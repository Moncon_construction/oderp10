# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

import res_company
import account_move
import account_invoice
import account_period
import account_fiscalyear
import account_journal
import account_config_settings
