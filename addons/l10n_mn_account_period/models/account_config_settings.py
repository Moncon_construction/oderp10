# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class AccountConfigSettings(models.TransientModel):
    _inherit = 'account.config.settings'

    open_journal_id = fields.Many2one(related='company_id.open_journal_id', domain=[('type', '=', 'open')])
