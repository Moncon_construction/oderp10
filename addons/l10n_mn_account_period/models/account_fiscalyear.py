# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

from datetime import datetime
from odoo import fields, models, api, _
from odoo.exceptions import UserError, RedirectWarning, ValidationError
from dateutil.relativedelta import relativedelta


class AccountFiscalyear(models.Model):
    _name = "account.fiscalyear"
    _description = "Fiscal Year"

    name = fields.Char('Fiscal Year', required=True)
    code = fields.Char('Code', size=6, required=True)
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id)
    date_start = fields.Date('Start Date', required=True)
    date_stop = fields.Date('End Date', required=True)
    period_ids = fields.One2many('account.period', 'fiscalyear_id', 'Periods')
    state = fields.Selection([('draft', 'Open'), ('done', 'Closed')], 'Status', default="draft", copy=False)
    move_id = fields.Many2one('account.move', 'Close account move')

    _order = "date_start"
    _sql_constraints = [
        ('company_name_uniq', 'unique(name, company_id)', 'The name of the fiscalyear must be unique per company!'),
    ]

    @api.multi
    def _check_duration(self):
        for obj_fy in self:
            if obj_fy.date_stop < obj_fy.date_start:
                return False
        return True

    _constraints = [
        (_check_duration, 'Error!\nThe start date of a fiscal year must precede its end date.', ['date_start', 'date_stop'])
    ]

    # Мөчлөгийг улирлаар үүсгэнэ.
    @api.multi
    def create_period3(self):
        return self.create_period(3)

    # Мөчлөгийг сараар үүсгэнэ
    @api.multi
    def create_period(self, interval=1):
        period_obj = self.env['account.period']
        month = interval
        if type(interval) == dict:
            month = interval['interval']
        for fy in self:
            ds = datetime.strptime(fy.date_start, '%Y-%m-%d')
            while ds.strftime('%Y-%m-%d') < fy.date_stop:
                de = ds + relativedelta(months=month, days=-1)

                if de.strftime('%Y-%m-%d') > fy.date_stop:
                    de = datetime.strptime(fy.date_stop, '%Y-%m-%d')

                period_obj.create({
                    'name': ds.strftime('%m/%Y'),
                    'code': ds.strftime('%m/%Y'),
                    'date_start': ds.strftime('%Y-%m-%d'),
                    'date_stop': de.strftime('%Y-%m-%d'),
                    'fiscalyear_id': fy.id,
                    'company_id': fy.company_id.id,
                })
                ds = ds + relativedelta(months=month)
        return True,

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        if not args:
            args = args or []
        ids = self.browse()
        if name:
            ids = self.search([('name', operator, name)] + args, limit=limit)
        if not ids:
            ids = self.search([('code', operator, name)] + args, limit=limit)
        result = ids.name_get()
        return result

    @api.multi
    def close_fiscalyear(self):
        for fiscal_year in self:
            # Check periods which are still open. If exists raise ERROR.
            open_periods = self.env['account.period'].search([('fiscalyear_id', '=', fiscal_year.id), ('state', '=', 'draft')])
            if open_periods and len(open_periods) > 0:
                if len(open_periods) == 1:
                    raise UserError(_("You cannot close fiscal year %s. Because one period is still open.") % (fiscal_year.name))
                else:
                    raise UserError(_("You cannot close fiscal year %s. Because %s periods are still open.") % (fiscal_year.name, len(open_periods)))

            fiscal_year.state = 'done'
            last_period = self.env['account.period'].search([('fiscalyear_id', '=', fiscal_year.id)], order='date_stop desc', limit=1)
            if last_period and last_period.date_stop and fiscal_year.company_id:
                fiscal_year.company_id.write({
                    'period_lock_date': last_period.date_stop,
                    'fiscalyear_lock_date': last_period.date_stop
                })

    @api.multi
    def open_fiscalyear(self):
        self.ensure_one()
        if self.move_id:
            self.move_id.line_ids.mapped('full_reconcile_id').unlink()
            self.move_id.button_cancel()
            self.move_id.unlink()
        self.write({'state': 'draft'})

    @api.multi
    def open_account_fiscalyear_close(self):
        self.ensure_one()
        if not self.company_id.open_journal_id:
            raise UserError(_('You have not set open journal in “Account – Settings – Settings” menu.'))
        view = self.env.ref('l10n_mn_account_period.view_account_fiscalyear_close_form')
        return {
            'name': _('Close fiscalyear'),
            'context': self._context,
            'view_mode': 'form',
            'res_model': 'account.fiscalyear.close',
            'type': 'ir.actions.act_window',
            'target': 'new',
        }
