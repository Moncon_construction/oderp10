# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from odoo.exceptions import UserError

class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.multi
    def write(self, vals):
        # Нэхэмжлэлийн огноо хаагдсан мөчлөгт байгаа эсэхийг шалгах
        for invoice in self:
            period_ids = []
            if not ('state' in vals.keys() or 'message_follower_ids' in vals.keys()):
                period_ids = self.env['account.period'].search([('date_start', '<=', invoice.date_invoice), ('date_stop', '>=', invoice.date_invoice),
                                                                ('company_id', '=', invoice.company_id.id), ('state', '<=', 'done')], limit=1)
            # Нэхэмжлэлийн огноог өөрчлөн хаагдсан мөчлөгийэ огноо руу болгож байгаа эсэхийг шалгах
            if 'date_invoice' in vals.keys():
                period_ids = self.env['account.period'].search([('date_start', '<=', vals['date_invoice']), ('date_stop', '>=', vals['date_invoice']),
                                                                ('company_id', '=', invoice.company_id.id), ('state', '<=', 'done')], limit=1)
            if len(period_ids) > 0:
                raise UserError(_("You cannot write account invoice because this %s period closed.") % period_ids[0].name)
        return super(AccountInvoice, self).write(vals)