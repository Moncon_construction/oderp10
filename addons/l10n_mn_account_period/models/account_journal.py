# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class AccountJournal(models.Model):
    _inherit = 'account.journal'

    type = fields.Selection(selection_add=[('open', 'Open year')])

    @api.onchange('type')
    def _onchange_type(self):
        if self.type == 'open':
            self.journal_entries_selectable = True
