# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.
from datetime import date, datetime, timedelta

from odoo import fields, models, api, _
from odoo.exceptions import UserError


class AccountMove(models.Model):
    _inherit = "account.move"

    from_closing_period = fields.Boolean(default=False)  # Мөчлөг хаах үед үүссэн журналыг ялгахад ашиглах талбар
    from_closing_fiscalyear = fields.Boolean(default=False)  # Мөчлөг хаах үед үүссэн журналыг ялгахад ашиглах талбар

    @api.multi
    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        default['date'] = datetime.now()
        self.ensure_one()
        vals = self.copy_data(default)[0]
        new = self.create(vals)
        return new

    @api.model
    def create(self, vals):
        # Журналын бичилтийн огноо хаагдсан мөчлөгт байгаа эсэхийг шалгах
        for move in self:
            if 'date' in vals.keys():
                period_ids = self.env['account.period'].search([('date_start', '<=', vals['date']), ('date_stop', '>=', vals['date']),
                                                                ('company_id', '=', move.company_id.id), ('state', '<=', 'done')], limit=1)
            else:
                period_ids = self.env['account.period'].search([('date_start', '<=', move.date), ('date_stop', '>=', move.date),
                                                                ('company_id', '=', move.company_id.id), ('state', '<=', 'done')], limit=1)
            if len(period_ids) > 0:
                # Борлуулалтын нэхэмжлэхийн урьдчилгаа нэхэмжлэх нь мөчлөг түгжихээс өмнө хийгдсэн, үндсэн нэхэмжлэх нь
                # дараа нь батлагдах үед тулгалт шалгахад хаагдсан мөчлөгийг хэрэгсэхгүй болгов.
                is_prepayment = False
                is_from_sale = False
                for line in move.line_ids[0]:
                    if line.invoice_id and line.invoice_id.invoice_line_ids:
                        for invoice_line in line.invoice_id.invoice_line_ids[0]:
                            self._cr.execute("""SELECT order_line_id
                                                                FROM sale_order_line_invoice_rel
                                                                WHERE invoice_line_id IN (%s)
                                                                        """ % invoice_line.id)
                            results = self._cr.fetchall()
                            if results:
                                sale_order = self.env['sale.order.line'].browse(results[0]).order_id
                                if sale_order:
                                    is_from_sale = True
                                    for invoice in sale_order.invoice_ids:
                                        if invoice.is_prepayment:
                                            is_prepayment = True
                                            break
                if not is_prepayment and is_from_sale:
                    raise UserError(
                        _("You cannot write account move because this %s period closed.") % period_ids[0].name)
        return super(AccountMove, self).create(vals)

    @api.multi
    def write(self, vals):
        # Журналын бичилтийн огноо хаагдсан мөчлөгт байгаа эсэхийг шалгах
        for move in self:
            period_ids = []
            if not ('narration' in vals.keys() or 'description' in vals.keys() or 'message_follower_ids' in vals.keys()):
                period_ids = self.env['account.period'].search([('date_start', '<=', move.date), ('date_stop', '>=', move.date),
                                                                ('company_id', '=', move.company_id.id), ('state', '<=', 'done')], limit=1)
            # Журналын бичилтийн огноог өөрчлөн хаагдсан мөчлөгийэ огноо руу болгож байгаа эсэхийг шалгах
            if 'date' in vals.keys():
                period_ids = self.env['account.period'].search([('date_start', '<=', vals['date']), ('date_stop', '>=', vals['date']),
                                                                ('company_id', '=', move.company_id.id), ('state', '<=', 'done')], limit=1)
            if len(period_ids) > 0:
                if self.env.get('purchase.order', False) != False:
                    is_from_purchase = False
                    for line in move.line_ids:
                        if line.stock_move_id and line.stock_move_id.purchase_line_id:
                            is_from_purchase = True
                    # Борлуулалтын нэхэмжлэхийн урьдчилгаа нэхэмжлэх нь мөчлөг түгжихээс өмнө хийгдсэн, үндсэн нэхэмжлэх нь
                    # дараа нь батлагдах үед тулгалт шалгахад хаагдсан мөчлөгийг хэрэгсэхгүй болгов.
                    is_prepayment = False
                    is_from_sale = False
                    for line in move.line_ids[0]:
                        if line.invoice_id and line.invoice_id.invoice_line_ids:
                            for invoice_line in line.invoice_id.invoice_line_ids[0]:
                                self._cr.execute("""SELECT order_line_id
                                                                    FROM sale_order_line_invoice_rel
                                                                    WHERE invoice_line_id IN (%s)
                                                                            """ % invoice_line.id)
                                results = self._cr.fetchall()
                                if results:
                                    sale_order = self.env['sale.order.line'].browse(results[0]).order_id
                                    if sale_order:
                                        is_from_sale = True
                                        for invoice in sale_order.invoice_ids:
                                            if invoice.is_prepayment:
                                                is_prepayment = True
                                                break
                    if not is_prepayment and is_from_sale:
                        raise UserError(
                            _("You cannot write account move because this %s period closed.") % period_ids[0].name)
                    if not is_from_purchase and not is_from_sale:
                        raise UserError(
                            _("You cannot write account move because this %s period closed.") % period_ids[0].name)
                else:
                    # Борлуулалтын нэхэмжлэхийн урьдчилгаа нэхэмжлэх нь мөчлөг түгжихээс өмнө хийгдсэн, үндсэн нэхэмжлэх нь
                    # дараа нь батлагдах үед тулгалт шалгахад хаагдсан мөчлөгийг хэрэгсэхгүй болгов.
                    is_prepayment = False
                    is_from_sale = False
                    for line in move.line_ids[0]:
                        if line.invoice_id and line.invoice_id.invoice_line_ids:
                            for invoice_line in line.invoice_id.invoice_line_ids[0]:
                                self._cr.execute("""SELECT order_line_id
                                                                    FROM sale_order_line_invoice_rel
                                                                    WHERE invoice_line_id IN (%s)
                                                                            """ % invoice_line.id)
                                results = self._cr.fetchall()
                                if results:
                                    sale_order = self.env['sale.order.line'].browse(results[0]).order_id
                                    if sale_order:
                                        is_from_sale = True
                                        for invoice in sale_order.invoice_ids:
                                            if invoice.is_prepayment:
                                                is_prepayment = True
                                                break
                    if not is_prepayment and is_from_sale:
                        raise UserError(
                            _("You cannot write account move because this %s period closed.") % period_ids[0].name)
                    if not is_from_sale:
                        raise UserError(
                            _("You cannot write account move because this %s period closed.") % period_ids[0].name)
        return super(AccountMove, self).write(vals)

    @api.multi
    def unlink(self):
        # Журналын бичилтийн огноо хаагдсан мөчлөгт байгаа эсэхийг шалгах
        for move in self:
            period_ids = self.env['account.period'].search([('date_start', '<=', move.date), ('date_stop', '>=', move.date),
                                                            ('company_id', '=', move.company_id.id), ('state', '<=', 'done')], limit=1)
            if period_ids:
                raise UserError(_("You cannot delete account move because this %s period closed.") % period_ids[0].name)
        return super(AccountMove, self).unlink()

    @api.multi
    def button_cancel(self):
        # Журналын бичилтийн огноо хаагдсан мөчлөгт байгаа эсэхийг шалгах
        for move in self:
            period_ids = self.env['account.period'].search([('date_start', '<=', move.date), ('date_stop', '>=', move.date),
                                                            ('company_id', '=', move.company_id.id), ('state', '<=', 'done')], limit=1)
            if period_ids:
                raise UserError(_("You cannot cancel account move because this %s period closed.") % period_ids[0].name)
            fiscalyear = self.env['account.fiscalyear'].search([('move_id', '=', move.id)])
            if fiscalyear and not self._context.get('open_account_fiscalyear_close', False):
                raise UserError(_('Account move of a closed fiscalyear cannot be edited. You should open the fiscalyear.'))
        return super(AccountMove, self).button_cancel()
