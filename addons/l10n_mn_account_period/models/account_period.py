# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.
import datetime
from dateutil.relativedelta import relativedelta

from odoo import fields, models, api, _   # @UnresolvedImport
from odoo.exceptions import UserError     # @UnresolvedImport

class AccountPeriod(models.Model):
    _name = "account.period"
    _description = "Account period"

    name = fields.Char('Period Name', required=True)
    code = fields.Char('Code', required=True)
    date_start = fields.Date('Start of Period', required=True, states={'done': [('readonly', True)]})
    date_stop = fields.Date('End of Period', required=True, states={'done': [('readonly', True)]})
    fiscalyear_id = fields.Many2one('account.fiscalyear', 'Fiscal Year', required=True, states={'done': [('readonly', True)]})
    state = fields.Selection([('draft', 'Open'), ('done', 'Closed')], 'Status', readonly=True, copy=False, default="draft",
                              help='When monthly periods are created. The status is \'Draft\'. At the end of monthly period it is in \'Done\' status.')
    company_id = fields.Many2one('res.company', string='Company', store=True)

    _order = "date_start desc"
    _sql_constraints = [
        ('name_company_uniq', 'unique(name, company_id)', 'The name of the period must be unique per company!'),
    ]

    def _check_duration(self):
        obj_period = self.env['account.period']
        if obj_period.date_stop < obj_period.date_start:
            return False
        return True

    def _check_year_limit(self):
        for obj_period in self:
            if obj_period.fiscalyear_id.date_stop < obj_period.date_stop or \
               obj_period.fiscalyear_id.date_stop < obj_period.date_start or \
               obj_period.fiscalyear_id.date_start > obj_period.date_start or \
               obj_period.fiscalyear_id.date_start > obj_period.date_stop:
                return False
            pids = self.search([('date_stop', '>=', obj_period.date_start), ('date_start', '<=', obj_period.date_stop), ('id', '<>', obj_period.id)])
            for period in self and pids:
                if period.fiscalyear_id.company_id.id == obj_period.fiscalyear_id.company_id.id:
                    return False
        return True

    _constraints = [
        (_check_duration, 'Error!\nThe duration of the Period(s) is/are invalid.', ['date_stop']),
        (_check_year_limit, 'Error!\nThe period is invalid. Either some periods are overlapping or the period\'s dates are not matching the scope of the fiscal year.', ['date_stop'])
    ]

    @api.multi
    def action_draft(self):
        # Мөчлөгийг дахин нээхэд тухайн мөчлөгийн эхлэх огнооны өмнөх огноогоор түгжигдэнэ.
        state_id = 'done'
        date_start = datetime.datetime.strptime(self.date_start, '%Y-%m-%d')
        lock_date = date_start + relativedelta(days=-1)
        self.env['res.company'].search([('id', '=', self.env.user.company_id.id)], order="id desc", limit=1).write({
                'period_lock_date': lock_date if lock_date else False,
                'fiscalyear_lock_date': lock_date if lock_date else False
        })
        mode = 'draft'
        self.env.cr.execute('update account_journal_period set state=%s where period_id in %s', (mode, tuple(self.ids),))
        self.env.cr.execute('update account_period set state=%s where id in %s', (mode, tuple(self.ids),))

        # Мөчлөгийг нээсэн тохиолдолд тухайн журналын бичилтийг цуцалж, устгана.
        for obj in self:
            move_ids = self.env['account.move'].search([('company_id', '=', obj.company_id.id), ('from_closing_period', '=', True), ('date', '=', str(obj.date_stop))])
            for move in move_ids:
                move_line_ids = [move_line.id for move_line in move.line_ids]
                if len(move_line_ids) > 0:
                    move_line_ids_str = str(move_line_ids)[1:len(str(move_line_ids))-1]
                    self.env.cr.execute("DELETE FROM account_partial_reconcile WHERE debit_move_id IN (%s) OR credit_move_id IN (%s)" %(move_line_ids_str, move_line_ids_str))
                move.button_cancel()
                move.unlink()
        return True

    # Батлагдсан төлөвтэй мөчлөг устахгүй.
    @api.multi
    def unlink(self):
        for period in self:
            if period.state not in ('draft'):
                raise UserError(_('You cannot delete an period which is done. You should reopen it instead.'))
        return super(AccountPeriod, self).unlink()

class account_journal_period(models.Model):
    _name = "account.journal.period"
    _description = "Journal Period"

    @api.multi
    def _icon_get(self):
        result = {}.fromkeys('STOCK_NEW')
        for r in self.read(['state']):
            result[r['id']] = {
                'draft': 'STOCK_NEW',
                'printed': 'STOCK_PRINT_PREVIEW',
                'done': 'STOCK_DIALOG_AUTHENTICATION',
            }.get(r['state'], 'STOCK_NEW')
        return result

    name = fields.Char('Journal-Period Name', required=True)
    journal_id = fields.Many2one('account.journal', 'Journal', required=True, ondelete="cascade")
    period_id = fields.Many2one('account.period', 'Period', required=True, ondelete="cascade")
    icon = fields.Char(compute=_icon_get, string='Icon')
    active = fields.Boolean('Active', help="If the active field is set to False, it will allow you to hide the journal period without removing it.")
    state = fields.Selection([('draft', 'Draft'), ('printed', 'Printed'), ('done', 'Done')], 'Status', required=True, active=True, default="draft",
                              help='When journal period is created. The status is \'Draft\'. If a report is printed it comes to \'Printed\' status. When all transactions are done, it comes in \'Done\' status.')
    fiscalyear_id = fields.Many2one(related='period_id.fiscalyear_id', string='Fiscal Year')
    company_id = fields.Many2one(related='journal_id.company_id', string='Company', store=True, readonly=True)

    @api.model
    def _check(self):
        for obj in self:
            self.cr.execute('select * from account_move_line where journal_id=%s and period_id=%s limit 1', (obj.journal_id.id, obj.period_id.id))
            res = self.cr.fetchall()
            if res:
                raise UserError(_('You cannot modify/delete a journal with entries for this period.'))
        return True

    @api.model
    def unlink(self):
        self._check()
        return super(account_journal_period, self).unlink()

    _order = "period_id"
