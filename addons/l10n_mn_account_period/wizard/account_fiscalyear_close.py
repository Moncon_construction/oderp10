# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from odoo.exceptions import UserError


class AccountFiscalyearClose(models.TransientModel):
    _name = 'account.fiscalyear.close'
    _description = 'Fiscalyear close'

    fiscalyear_to_close = fields.Many2one('account.fiscalyear', 'Fiscalyear to close', required=True, domain=[('state', '=', 'draft')], default=lambda self: self._context.get('active_id'))
    new_fiscalyear = fields.Many2one('account.fiscalyear', 'New fiscalyear', required=True, domain=[('state', '=', 'draft')])
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    journal_id = fields.Many2one('account.journal', 'Open journal', required=True, related='company_id.open_journal_id')
    description = fields.Char('Transaction description', required=True, default=u'Санхүүгийн жилийн нээлтийн бичилт')

    @api.onchange('fiscalyear_to_close')
    def _onchange_fiscalyear_to_close(self):
        if self.fiscalyear_to_close:
            return {'domain': {'new_fiscalyear': ['&', ('state', '=', 'draft'), ('id', '!=', self.fiscalyear_to_close.id)]}}

    def close_fiscalyear(self):
        if self.fiscalyear_to_close.date_start <= self.new_fiscalyear.date_stop <= self.fiscalyear_to_close.date_stop or \
                self.fiscalyear_to_close.date_start <= self.new_fiscalyear.date_start <= self.fiscalyear_to_close.date_stop:
            raise UserError(_('Fiscalyear to close interval and new fiscalyear interval intersects.'))
        ctx = self._context
        fiscalyears = self.env[ctx['active_model']].browse(ctx['active_ids'])
        # account.fiscalyear model дахь санхүүгийн жил хаадаг функцыг ажиллуулж байна
        if fiscalyears:
            fiscalyears.close_fiscalyear()
        module = self.env['ir.module.module'].search([('name', '=', 'l10n_mn_analytic_balance')])
        l10n_mn_analytic_balance_installed = False
        if module and module.state in ('installed', 'to upgrade'):
            l10n_mn_analytic_balance_installed = True
        # l10n_mn_analytic_balance модуль суусан байвал шинжилгээний дансаар бүлэглэнэ
        select = ''
        group_by = ''
        if l10n_mn_analytic_balance_installed:
            group_by = ''',
            aml.analytic_account_id'''
            select = ''', aml.analytic_account_id
            '''
        query = '''
            select
                aml.account_id,
                aml.partner_id,
                aml.currency_id,
                sum(
                    case
                        when aat.type in ('other', 'receivable', 'liquidity') then aml.debit - aml.credit
                        else 0
                    end
                ) as debit,
                sum(
                    case
                        when aat.type in ('payable', 'liability') then aml.credit - aml.debit
                        else 0
                    end
                ) as credit,
                avg(aml.currency_rate) as currency_rate,
                sum(aml.amount_currency) as amount_currency''' + select + '''
            from
                account_move_line aml
                left join account_move am on am.id = aml.move_id
                left join account_account aa on aa.id = aml.account_id
                left join account_account_type aat on aat.id = aa.user_type_id
            where
                am.state = 'posted'
                and aat.type in (
                    'other',
                    'receivable',
                    'payable',
                    'liquidity',
                    'liability'
                )
            group by
                aml.account_id,
                aml.partner_id,
                aml.currency_id''' + group_by + '''
            having
                sum(
                    case
                        when aat.type in ('other', 'receivable', 'liquidity') then aml.debit - aml.credit
                        else 0
                    end
                ) != 0
                or sum(
                    case
                        when aat.type in ('payable', 'liability') then aml.credit - aml.debit
                        else 0
                    end
                ) != 0
            order by
                aml.account_id,
                aml.partner_id,
                aml.currency_id;
            '''

        self._cr.execute(query)
        amls = self._cr.dictfetchall()
        move_lines = []
        AccountAccount = self.env['account.account']
        for l in amls:
            account = AccountAccount.browse(l['account_id'])
            rate = 1
            if account.currency_id:
                rate_id = self.env['res.currency.rate'].search([('currency_id', '=', account.currency_id.id), ('name', '<=', self.new_fiscalyear.date_start)], order='name desc')
                if rate_id:
                    rate = rate_id[0].alter_rate
            debit = l['debit']
            credit = l['credit']
            if debit < 0:
                credit = -debit
                debit = 0
            if credit < 0:
                debit = -credit
                credit = 0
            vals = {
                'account_id': l['account_id'],
                'partner_id': l['partner_id'],
                'name': self.description,
                'currency_id': account.currency_id and account.currency_id.id,
                'currency_rate': rate,
                'amount_currency': l['amount_currency'] if account.currency_id else 0,
                'debit': debit,
                'credit': credit,
            }
            if l10n_mn_analytic_balance_installed:
                vals.update({'analytic_account_id': l['analytic_account_id'], })
            move_lines.append((0, 0, vals))
        if move_lines:
            move = self.env['account.move'].create({
                'line_ids': move_lines,
                'journal_id': self.journal_id.id,
                'date': self.new_fiscalyear.date_start,
                'description': self.description,
                'from_closing_fiscalyear': True,
            })
            fiscalyears.write({'move_id': move.id})
            self.env['account.full.reconcile'].create({'reconciled_line_ids': [(6, 0, move.line_ids.ids)]})
