# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

from openerp import fields, models, api, _  # @UnresolvedImport
from odoo.exceptions import UserError, ValidationError  # @UnresolvedImport

class AccountPeriodClose(models.TransientModel):

    _name = "account.period.close"
    _description = "period close"

    def _get_company(self):
        if self.env.context.get('active_model', 'ir.ui.menu') == 'account.period':
            period = self.env['account.period'].browse(self.env.context.get('active_id'))
            return period.company_id.id

    def _get_configuration(self):
        company = self.env.user.company_id
        return company.period_journal_id

    def _get_period_display(self):
        res = {}
        closing_periods = u''
        if self.env.context.get('active_model', 'ir.ui.menu') == 'account.period':
            for p in self.env['account.period'].browse(self.env.context.get('active_ids')):
                closing_periods += u'%s\n' % p.name
        return res.setdefault(p, closing_periods)

    period_display = fields.Text(default=_get_period_display, method=True, string="To be closed periods", store=False)
    journal_id = fields.Many2one('account.journal', string='Reserve & Profit/Loss Journal', default=_get_configuration)
    registered_date = fields.Date(string='Date', default=fields.Date.today)
    description = fields.Char(string='Name of New Entries', size=64)
    company_id = fields.Many2one('res.company', string='Company', default=_get_company)
    temporary = fields.Boolean(string='Temporary Closing', help='If check this box, closing of periods done without any closing entries.')

    @api.model
    def close_state(self, period_ids):
        """ Тайлант хугацааны төлөвийг хаагдсан төлөвт шилжүүлнэ.
        """
        mode = 'done'
        for period_id in period_ids:
            self.env.cr.execute("""UPDATE account_journal_period set state=%s WHERE period_id=%s""", (mode, period_id))
            self.env.cr.execute("""UPDATE account_period set state='done' WHERE id=""" + str(period_id))

        return True

    @api.model
    def action_close(self, period_ids):
        ''' Тайлант хугацааг хааж орлого зарлагын түр дансуудын
            үлдэгдлийг орлого зарлагын нэгдсэн дансанд хаана.
        '''
        obj_acc_move = self.env['account.move']
        if not self.journal_id.default_debit_account_id or not self.journal_id.default_credit_account_id:
            raise ValidationError(_('There is no Default Debit/Credit Account defined on your selected journal!'))
        if not self.journal_id.profit_account_id or not self.journal_id.loss_account_id:
            raise ValidationError(_('There is no Reserve & Profit/Loss Account defined on your selected journal!'))
        res = []
        for period in self.env['account.period'].browse(period_ids):
            # Орлого, Зарлагын түр дансны тайлант хугацааны хоорондох дүнг шинжилгээний дансаар олно
            self.env.cr.execute('''
                SELECT ml.account_id AS aid,
                    al.account_id AS aaid,
                    ml.currency_id AS cid,
                    SUM(COALESCE(ml.debit, 0)) AS debit,
                    SUM(COALESCE(ml.credit, 0)) AS credit,
                    SUM(COALESCE(ml.amount_currency, 0)) AS amount_currency,
                    SUM(COALESCE(al.amount, 0)) AS amount,
                    SUM(COALESCE(al.amount_currency, 0)) AS aamount_currency
                FROM account_move_line ml
                    LEFT JOIN account_move m ON (ml.move_id = m.id)
                    LEFT JOIN account_analytic_line al ON (al.move_id = ml.id)
                    LEFT JOIN account_account aa ON (ml.account_id = aa.id)
                WHERE m.state = 'posted' AND aa.internal_type in ('income', 'expense')
                    AND ml.date BETWEEN \'''' + period.date_start + '''\'
                    AND \'''' + period.date_stop + '''\'
                    AND ml.company_id = ''' + str(period.company_id.id) + '''
                    AND aa.id != ''' + str(self.journal_id.default_debit_account_id.id) + '''
                GROUP BY ml.account_id,
                    al.account_id,
                    ml.currency_id''')
            account_sum_dict = self.env.cr.dictfetchall()
            reverse_debit = reverse_credit = 0.0
            move_lines = []
            for line in account_sum_dict:
                # account_move руу бичих өгөгдөл
                debit, credit = line['debit'], line['credit']
                balance = credit - debit
                amount_currency = line['amount_currency']
                if (credit > debit and line['amount_currency'] < 0) or (credit < debit and line['amount_currency'] > 0):
                    amount_currency = -amount_currency
                # Орлого зарлагын түр дансуудыг хаана.
                move_lines.append((0, 0, {
                            'debit': (credit > debit and abs(balance)) or 0.0,
                            'credit': (debit > credit and abs(balance)) or 0.0,
                            'name': self.description,
                            'date': period.date_stop,
                            'journal_id': self.journal_id.id,
                            'amount_currency': amount_currency,
                            'currency_id': line['cid'] or False,
                            'account_id': line['aid'],
                            'company_id': self.company_id.id,
                            'analytic_account_id': line['aaid'] or False,
                }))
                reverse_debit += (credit > debit and abs(balance)) or 0.0
                reverse_credit += (debit > credit and abs(balance)) or 0.0
            if reverse_credit > reverse_debit:
                # create move lines
                move_lines.append((0, 0, {
                        'debit': abs(reverse_credit - reverse_debit),
                        'credit': 0,
                        'name': self.description,
                        'date': period.date_stop,
                        'create_date': period.date_stop,
                        'journal_id': self.journal_id.id,
                        'period_id': period.id,
                        'account_id': self.journal_id.default_debit_account_id.id,
                        'company_id': self.company_id.id,
                }))
                move_lines.append((0, 0, {
                        'debit': 0,
                        'credit': abs(reverse_credit - reverse_debit),
                        'name': self.description,
                        'date': period.date_stop,
                        'create_date': period.date_stop,
                        'journal_id': self.journal_id.id,
                        'period_id': period.id,
                        'account_id': self.journal_id.default_debit_account_id.id,
                        'company_id': self.company_id.id,
                }))
                move_lines.append((0, 0, {
                        'debit': abs(reverse_credit - reverse_debit),
                        'credit': 0,
                        'name': self.description,
                        'date': period.date_stop,
                        'create_date': period.date_stop,
                        'journal_id': self.journal_id.id,
                        'period_id': period.id,
                        'account_id': self.journal_id.loss_account_id.id,
                        'company_id': self.company_id.id,
                }))
            elif reverse_debit > reverse_credit:
                move_lines.append((0, 0, {
                        'debit': 0,
                        'credit': abs(reverse_debit - reverse_credit),
                        'name': self.description,
                        'date': period.date_stop,
                        'date_created': period.date_stop,
                        'journal_id': self.journal_id.id,
                        'period_id': period.id,
                        'account_id': self.journal_id.default_credit_account_id.id,
                        'company_id': self.company_id.id,
                }))
                move_lines.append((0, 0, {
                        'debit': abs(reverse_debit - reverse_credit),
                        'credit': 0,
                        'name': self.description,
                        'date': period.date_stop,
                        'date_created': period.date_stop,
                        'journal_id': self.journal_id.id,
                        'period_id': period.id,
                        'account_id': self.journal_id.default_credit_account_id.id,
                        'company_id': self.company_id.id,
                }))
                move_lines.append((0, 0, {
                        'debit': 0,
                        'credit': abs(reverse_debit - reverse_credit),
                        'name': self.description,
                        'date': period.date_stop,
                        'date_created': period.date_stop,
                        'journal_id': self.journal_id.id,
                        'period_id': period.id,
                        'account_id': self.journal_id.profit_account_id.id,
                        'company_id': self.company_id.id,
                }))
            if move_lines:
                move_id = obj_acc_move.create({
                        'journal_id': self.journal_id.id,
                        'date': period.date_stop,
                        'from_closing_period': True,
                        'state': 'posted',
                        'narration': u'%s мөчлөгийн хаалтын гүйлгээ.' % (period.name,),
                        'line_ids': move_lines
                })
                res.append(move_id)
        return res

    @api.multi
    def data_save(self):
        for period_close_id in self:
            if 'active_ids' in self.env.context and self.env.context.get('active_ids'):
                period_ids = self.env.context.get('active_ids')
            else:
                period_ids = period_close_id.id

        # Өмнөх сарын мөчлөг хаагдаагүй болон мөчлөгүүд нэг санхүүгийн жилд харьяалагдаагүй бол анхааруулна.
        active_ids = self.env['account.period'].search([('id', 'in', period_ids)], order="id desc")
        minimum_value = 0
        fis_id = False
        for active_id in active_ids:
            if not fis_id:
                fis_id = active_id.fiscalyear_id.id
            else:
                if fis_id != active_id.fiscalyear_id.id:
                    raise ValidationError(_('Which period you have selected are must be same fiscalyear.'))
                else:
                    fis_id = active_id.fiscalyear_id.id

            if active_id.id < minimum_value or minimum_value == 0:
                minimum_value = active_id.id

        minimum_record = self.env['account.period'].search([('state', "=", 'draft'), ('fiscalyear_id', '=', fis_id)], order="id asc", limit=1)
        if minimum_value > minimum_record.id:
            raise UserError(_("Please, close previous month's period!"))

        """ Хаах гэж буй тайлант хугацааны гүйлгээнүүдийг хааж
                орлого зарлагын нэгдсэн дансанд бичнэ.
        """
        obj_acc_move = self.env['account.move']
        if not self.temporary:
            if not self.journal_id:
                raise UserError(_('There is no Journal defined on this company !'))

            res = self.action_close(period_ids)
            if res:
                for res_id in res:
                    obj_acc_move.browse(res_id.id).post()

            # Хаагдсан мөчлөгийн дуусах огноогоор бичилтийг түгжинэ.
            self.close_state(period_ids)
            for line in self.env['account.period'].browse(self.env.context.get('active_ids')):
                line.company_id.write({
                    'period_lock_date': line.date_stop,
                    'fiscalyear_lock_date': line.date_stop,
                })

            # Бичигдсэн журналын бичилтүүдийг дэлгэцэнд харуулах
            return {
                'name': _('Closing Entries'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.move',
                'type': 'ir.actions.act_window',
                'context': "{'visible_id':%(j)s, 'journal_id': %(j)d, 'search_default_journal_id':%(j)d}" % ({'j': self.journal_id.id}),
                'domain': (len(res) > 0 and "[('id', 'in', " + str([str(i.id) for i in res]) + ")]") or "[('id','=',False)]"

            }
        else:
            # closing All selected Periods without closing entries
            for line in self.env['account.period'].browse(self.env.context.get('active_ids')):
                line.company_id.write({
                    'period_lock_date': line.date_stop,
                    'fiscalyear_lock_date': line.date_stop,
                })
            self.close_state(period_ids)
            return {'type': 'ir.actions.act_window_close'}



# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
