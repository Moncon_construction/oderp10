# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Account Reconciliation",
    'version': '1.0',
    'depends': [
        'l10n_mn_account',
        'l10n_mn_account_currency_equalization'
    ],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Account Modules',
    'description': """ Санхүүгийн тулгалт.
                        - Үндсэн тулгалтыг сайжруулсан
                        - Авлага өглөг болон олон харилцагч сонгон тулгадаг болгосон
                        - Валютын авлага өглөгийг тулгадаг болгосон
                    """,
    'website': 'http://asterisk-tech.mn',
    'data': ['security/ir.model.access.csv',
             'data/ir_sequence.xml',
             'views/account_move_receivable_payable_reconcile_view.xml',
             'views/account_move_view.xml',
             'views/account_reconcile_view.xml',
             'views/account_invoice_view.xml',

             ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
