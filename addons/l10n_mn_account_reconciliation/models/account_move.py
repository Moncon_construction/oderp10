# -*- coding: utf-8 -*-
import time

from odoo import api, fields, models, _
from odoo.tools import float_compare, float_round
from odoo.exceptions import RedirectWarning, UserError, ValidationError

import logging
_logger = logging.getLogger(__name__)


class AccountMove(models.Model):
    _inherit = "account.move"

    @api.multi
    def button_cancel(self):
        ''' Тулгалттай холбогдсон бол журналын бичилт цуцлахгүй алдааны мэдээлэл өгдөг болох.'''
        res = super(AccountMove, self).button_cancel()
        for move in self:
            for line in move.line_ids:
                if line.receivable_payable_reconcile_id or line.full_reconcile_id:
                    raise UserError(_('You cannot cancel reconciled journal entry!'))
        return res


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    receivable_payable_reconcile_id = fields.Many2one('account.move.receivable.payable.reconcile', 'Receivable, Payable Reconcile', readonly=True, ondelete='set null', select=2, copy=False)
    reconcile_move_id = fields.Many2one('account.move.line', 'Reconcile Move Line', readonly=True, copy=False)

    # Салгах бичилтийн утгууд цуглуулах функц
    @api.multi
    def _get_partial_move_line_vals(self, par_debit, par_credit, balance_currency):
        self.ensure_one()
        return {'name': _('Reconcile Partial: ') + self.name,
                'ref': _('Reconcile Partial: ') + self.name,
                'debit': par_debit,
                'credit': par_credit,
                'account_id': self.account_id and self.account_id.id,
                'invoice_id': self.invoice_id and self.invoice_id.id,
                'date': self.date,
                'journal_id': self.journal_id and self.journal_id.id,
                'partner_id': self.partner_id and self.partner_id.id,
                'currency_id': self.currency_id and self.currency_id.id,
                'amount_currency': balance_currency,
                'move_id': self.move_id.id}

    @api.multi
    def remove_move_reconcile(self):
        """ Undo a reconciliation """
        if not self:
            return True
        rec_move_ids = self.env['account.partial.reconcile']
        reconcile_ids = []
        receivable_payable_reconcile_ids = []
        reconcile_move_ids = []
        currency_move_ids = []
        receivable_payable_reconcile = False
        _logger.info(_('Start: Remove Reconcile'))
        for account_move_line in self:
            account_move_line.reconciled = False
            # Авлага, өглөгийн тулгалтыг шалгах
            if account_move_line.receivable_payable_reconcile_id and (not receivable_payable_reconcile_ids or account_move_line.receivable_payable_reconcile_id not in receivable_payable_reconcile_ids):
                receivable_payable_reconcile_ids.append(account_move_line.receivable_payable_reconcile_id)
            # Үндсэн тулгалтыг шалгах
            if account_move_line.full_reconcile_id and (not reconcile_ids or account_move_line.full_reconcile_id not in reconcile_ids):
                reconcile_ids.append(account_move_line.full_reconcile_id)
                # Ханшийн зөрүүний бичилт байгаа эсэхийг шалгах
                if account_move_line.full_reconcile_id.exchange_move_id and (not currency_move_ids or account_move_line.full_reconcile_id.exchange_move_id not in currency_move_ids):
                    currency_move_ids.append(account_move_line.full_reconcile_id.exchange_move_id)
                    account_move_line.full_reconcile_id.exchange_move_id = False
                    account_move_line.full_reconcile_id.exchange_move_id.full_reconcile_id = False
            # Авлага, өглөгийн тулгалтаас үүссэн журналын бичилтийг шалгах, банкны хуулгатай холбогдсон бол устгахгүй байна
            if not account_move_line.payment_id and not account_move_line.statement_id and account_move_line.reconcile_move_id and \
                    (not reconcile_move_ids or account_move_line.move_id not in reconcile_move_ids):
                reconcile_move_ids.append(account_move_line.move_id)
            for invoice in account_move_line.payment_id.invoice_ids:
                if invoice.id == self.env.context.get('invoice_id') and account_move_line in invoice.payment_move_line_ids:
                    account_move_line.payment_id.write({'invoice_ids': [(3, invoice.id, None)]})
            rec_move_ids += account_move_line.matched_debit_ids
            rec_move_ids += account_move_line.matched_credit_ids
        if self.env.context.get('invoice_id'):
            current_invoice = self.env['account.invoice'].browse(self.env.context['invoice_id'])
            aml_to_keep = current_invoice.move_id.line_ids | current_invoice.move_id.line_ids.mapped('full_reconcile_id.exchange_move_id.line_ids')
            rec_move_ids = rec_move_ids.filtered(lambda r: (r.debit_move_id + r.credit_move_id) & aml_to_keep)
        _logger.info(_('rec_move_ids: %s, reconcile_ids: %s, receivable_payable_reconcile_ids: %s') % (rec_move_ids, reconcile_ids, receivable_payable_reconcile_ids))
        # Авлага, өглөгийн тулгалтыг устгах
        if len(receivable_payable_reconcile_ids) > 0:
            receivable_payable_reconcile = True
            for reconcile_id in receivable_payable_reconcile_ids:
                for account_move_line in reconcile_id.line_ids:
                    account_move_line.reconciled = False
                    if not reconcile_ids or account_move_line.full_reconcile_id and account_move_line.full_reconcile_id not in reconcile_ids:
                        reconcile_ids.append(account_move_line.full_reconcile_id)
                    # Банкны хуулгатай холбогдсон бол устгахгүй байна
                    if not account_move_line.payment_id and not account_move_line.statement_id and account_move_line.reconcile_move_id and \
                            (not reconcile_move_ids or account_move_line.move_id not in reconcile_move_ids):
                        reconcile_move_ids.append(account_move_line.move_id)
                    # Хэсэгчилсэн тулгалт шалгана
                    rec_move_ids += account_move_line.matched_debit_ids
                    rec_move_ids += account_move_line.matched_credit_ids
                reconcile_id.unlink()
        _logger.info(_('rec_move_ids: %s, currency_move_ids: %s, reconcile_move_ids: %s') % (rec_move_ids, currency_move_ids, reconcile_move_ids))
        # Хэсэгчилсэн тулгалтыг устгах
        if len(rec_move_ids) > 0:
            for full_reconcile_id in rec_move_ids.mapped('full_reconcile_id'):
                if full_reconcile_id in reconcile_ids:
                    reconcile_ids.remove(full_reconcile_id)
            rec_move_ids.unlink()
        # Үндсэн тулгалтыг устгах
        if len(reconcile_ids) > 0:
            for reconcile_id in reconcile_ids:
                reconcile_id.unlink()
        _logger.info(_('reconcile_move_ids: %s') % reconcile_move_ids)
        # Авлага, өглөгийн тулгалтаас үүссэн журналын бичилтийг цуцлаад устгах
        if receivable_payable_reconcile and len(reconcile_move_ids) > 0:
            for move_id in reconcile_move_ids:
                move_id.button_cancel()
                move_id.unlink()
        # Ханшийн зөрүүний бичилтийг устгах
        if currency_move_ids:
            for currency_move in currency_move_ids:
                currency_move.button_cancel()
                for line in currency_move.line_ids:
                    if line.matched_debit_ids:
                        line.matched_debit_ids.unlink()
                    if line.matched_credit_ids:
                        line.matched_credit_ids.unlink()
                    line.reconciled = False
                currency_move.unlink()
                # self._cr.execute("DELETE FROM account_move WHERE id = %s", (currency_move.id,))
        _logger.info(_('Finished: Remove Reconcile'))
        return True

    # Авлага, Өглөгийн тулгалтын үед журналын мөр үүсгэх функц
    @api.multi
    def _create_move_line(self, date=False, journal_id=False, debit=0, credit=0):
        for line in self:
            return (0, 0, {'name': _('Receivable, Payable Reconcile: ') + line.name,
                           'ref': _('Receivable, Payable Reconcile: ') + line.name,
                           'reconcile_move_id': line.id,
                           'debit': debit,
                           'credit': credit,
                           'account_id': line.account_id and line.account_id.id,
                           'date': date,
                           'journal_id': journal_id,
                           'partner_id': line.partner_id and line.partner_id.id,
                           'currency_id': line.currency_id and line.currency_id.id,
                           'amount_currency': -line.amount_currency or 0
                           })

    # Олон харилцагч, олон дансны хувьд: Дт, Кт-н зөрүү нь 0-тэй тэнцүү байх үеийн тулгалт
    # Мөн Дт, Кт 2 өөр валют бөгөөд Дт, Кт-н зөрүү нь 0-тэй тэнцүү үеийн тулгалт
    @api.multi
    def reconcile_receivable_payable(self, date=False, reconcile_journal_id=False):
        if not self:
            return True
        # Perform all checks on lines
        company_ids = set()
        all_accounts = []
        move_lines = []
        res = []    # Үүссэн журналын бичилтийг харуулна
        total_credit = total_debit = 0.0
        _logger.info(_('Start: Receivable, Payable Full Reconcile'))
        for line in self:
            company_ids.add(line.company_id.id)
            all_accounts.append(line.account_id)
            if line.full_reconcile_id or line.receivable_payable_reconcile_id:
                raise UserError(_('You are trying to reconcile some entries that are already reconciled!'))
        if len(company_ids) > 1:
            raise UserError(_('To reconcile the entries company should be the same for all entries!'))
        if not (all_accounts[0].reconcile or all_accounts[0].internal_type == 'liquidity'):
            raise UserError(_('The account %s (%s) is not marked as reconciliable !') % (all_accounts[0].name, all_accounts[0].code))
        for line in self:
            total_credit += line.credit or 0
            total_debit += line.debit or 0
            debit = credit = 0
            if line.credit > 0:
                debit = line.credit
            elif line.debit > 0:
                credit = line.debit
            # Тухайн журналын мөрийн эсрэг журналын мөр үүсгэх
            move_lines.append(line._create_move_line(date, reconcile_journal_id, debit, credit))
        digits_rounding_precision = self[0].company_id.currency_id.rounding
        if float_compare(total_debit, total_credit, precision_rounding=digits_rounding_precision) == 0:
            # Эсрэг журналын бичилт үүсгэх
            move = self.env['account.move'].create({'ref': 'Receivable, Payable Reconcile',
                                                    'date': date,
                                                    'journal_id': reconcile_journal_id,
                                                    'line_ids': move_lines})
            move.post()
            res = self.ids + move.line_ids.ids
            # Авлага, өглөгийн тулгалт үүсгэнэ
            self.env['account.move.receivable.payable.reconcile'].with_context(check_move_validity=False).create({'line_ids': [(6, 0, (self.ids + move.line_ids.ids))]})
            # Нэхэмжлэл төлөгдсөн төлөвт оруулах функц
            line_ids = self.env['account.move.line'].search([('id', 'in', (self.ids + move.line_ids.ids))])
            line_ids.auto_reconcile_lines()
            for line in self:
                line_id = self.env['account.move.line'].search([('reconcile_move_id', '=', line.id)])
                if line_id:
                    reconcile_lines = line_id.ids
                    reconcile_lines.append(line.id)
                    # Үндсэн тулгалт үүсгэнэ
                    self.env['account.full.reconcile'].with_context(check_move_validity=False).create({'reconciled_line_ids': [(6, 0, reconcile_lines)]})
        _logger.info(_('Finished: Receivable, Payable Full Reconcile'))
        return res

    # Бичилт салган тулгалт. Тухайн журналын мөрийг хуваан тулгана
    @api.multi
    def reconcile_part_line(self, date=False, reconcile_journal_id=False):
        if not self:
            return True
        # Perform all checks on lines
        context = dict(self._context or {})
        company_ids = set()
        all_accounts = []
        partners = set()
        currency_ids = []
        res = []  # Үүссэн журналын бичилтийг харуулна
        total_credit = total_debit = currency_debit = currency_credit = 0.0
        move_lines = []                 # Тухайн журналын мөрийн эсрэг бичилтийг хадгалах
        line_ids = []                   # Сонгосон журналын мөрүүдийн Дт, Кт-н аль ихийг хадгалах
        ids = []                        # Эсрэг бичилт үүсгэх журналын мөрийн id-г хадгалах
        kt_ids = []                     # Кт талаараа өссөн журналын мөрийн id-г хадгалах
        dt_ids = []                     # Дт талаараа өссөн журналын мөрийн id-г хадгалах
        dt_currency_ids = []
        kt_currency_ids = []
        off_ids = []
        dt = True                       # Дт тал нь их бол
        last_date = False
        currency_writeoff = 0
        company_currency = self[0].company_id.currency_id.id
        writeoff_lines = []             # Тулгалт хасалттай үед ашиглана
        _logger.info(_('Start: Receivable, Payable Part/Writeoff Reconcile'))
        for line in self:
            company_ids.add(line.company_id.id)
            if line.account_id and line.account_id not in all_accounts:
                all_accounts.append(line.account_id)
            if not line.currency_id and company_currency not in currency_ids:
                currency_ids.append(company_currency)
            elif line.currency_id and line.currency_id.id not in currency_ids:
                currency_ids.append(line.currency_id.id)
                equalization_ids = self.env['account.currency.equalization.history'].search([('account_id', '=', line.account_id.id),
                                                                                             ('partner_id', '=', line.partner_id.id),
                                                                                             ('equalization_date', '>', date)])
                if len(equalization_ids) > 0:                        # Тухайн огнооноос хойш ханшийн тэгшитгэл хйисэн эсэхийг шалгана
                    raise UserError(_('You are trying to reconcile some entries that are already reconciled!'))
            if line.account_id.internal_type in ('receivable', 'payable'):
                partners.add(line.partner_id.id)
            if line.full_reconcile_id or line.receivable_payable_reconcile_id:
                raise UserError(_('You are trying to reconcile some entries that are already reconciled!'))
            if line.debit > 0:                                      # Дт тал их бол Кт list-д хийнэ
                dt_ids.append(line.id)
                if not line.currency_id and company_currency not in currency_ids:
                    dt_currency_ids.append(line.currency_id.id)
                elif line.currency_id and line.currency_id.id not in currency_ids:
                    dt_currency_ids.append(line.currency_id.id)
                total_debit += line.debit or 0
                currency_debit += line.amount_currency
            elif line.credit > 0:                                     # Кт тал их бол Кт list-д хийнэ
                kt_ids.append(line.id)
                if not line.currency_id and company_currency not in currency_ids:
                    kt_currency_ids.append(line.currency_id.id)
                elif line.currency_id and line.currency_id.id not in currency_ids:
                    kt_currency_ids.append(line.currency_id.id)
                total_credit += line.credit or 0
                currency_credit -= line.amount_currency
            if not last_date or last_date < line.date:
                last_date = line.date
        if len(company_ids) > 1:
            raise UserError(_('To reconcile the entries company should be the same for all entries!'))
        if len(partners) > 1 and not reconcile_journal_id:
            raise UserError(_('Entries are not of the same partner!'))
        if len(all_accounts) > 1 and not reconcile_journal_id:
            raise UserError(_('Entries are not of the same account!'))
        if not (all_accounts[0].reconcile or all_accounts[0].internal_type == 'liquidity'):
            raise UserError(_('The account %s (%s) is not marked as reconciliable !') % (all_accounts[0].name, all_accounts[0].code))
        if len(currency_ids) > 2:
            raise UserError(_('3 different currencies are not reconcile!'))
        if len(currency_ids) == 2 and company_currency not in currency_ids\
                and len(dt_currency_ids) == 2 or len(kt_currency_ids) == 2:             # 2 өөр валютын тулгалт
            raise UserError(_('Debit or Credit entries are not of the same currency!'))
        elif len(currency_ids) == 2 and company_currency in currency_ids\
                and currency_credit != 0 and currency_debit != 0:                       # Компанийн үндсэн валют болон өөр 1 валютын тулгалт
            raise UserError(_('1 Debit or Credit entries are not of the same currency!'))
        elif len(currency_ids) == 1 and currency_ids[0] != company_currency:            # 1 валютын тулгалт
            currency_writeoff = currency_debit - currency_credit
        # Дт, Кт-н зөрүү
        precision = self.env.user.company_id.currency_id.decimal_places
        writeoff = float_round(total_debit - total_credit, precision_digits=precision)
        if currency_writeoff != 0 and currency_writeoff > 0 and writeoff < 0:
            raise UserError(_('Writeoff > 0 and Currency writeoff < 0 is not reconcile!'))
        if currency_writeoff != 0 and currency_writeoff < 0 and writeoff > 0:
            raise UserError(_('Writeoff < 0 and Currency writeoff > 0 is not reconcile!'))
        total_currency = 0
        if writeoff > 0:                                    # Дт тал нь их бол
            line_ids = dt_ids
            ids += kt_ids
            total = total_credit
            total_currency = abs(currency_credit)
        elif writeoff < 0:                                  # Кт тал нь их бол
            line_ids = kt_ids
            ids += dt_ids
            total = total_debit
            total_currency = currency_debit
            dt = False
        elif writeoff == 0:
            if currency_writeoff == 0:      # Урьдчилгаа нэхэмжлэлтэй автомат тулгалт хийхийн тулд нэмэв.
                res = self.reconcile_receivable_payable(date=date, reconcile_journal_id=reconcile_journal_id)
                return res
            elif currency_writeoff > 0:      # Нийт дүн нь 0 боловч валютын дүн нь 0-с их үед тооцов.
                line_ids = dt_ids
                ids += kt_ids
                total = total_credit
                total_currency = abs(currency_credit)
            elif currency_writeoff < 0:      # Нийт дүн нь 0 боловч валютын дүн нь 0-с бага үед тооцов.
                line_ids = kt_ids
                ids += dt_ids
                total = total_debit
                total_currency = currency_debit
                dt = False
        # Тулгалт хасалттайгаар үед ашиглана
        writeoff_ids = line_ids
        journal_id = self.env.context.get('journal_id', False)
        writeoff_acc_id = self.env.context.get('writeoff_acc_id', False)
        date_p = self.env.context.get('date_p', False)
        analytic_account_id = self.env.context.get('analytic_id', False)
        # Олон харилцагч, Олон данс байгаа эсэхийг шалгах
        if reconcile_journal_id and date:
            # Тухайн журналын мөрийн эсрэг журналын мөр үүсгэх
            for line in self.env['account.move.line'].browse(ids):
                move_lines.append(line._create_move_line(date, reconcile_journal_id, line.credit, line.debit))
        # Огноогоор эрэмбэлэн Дт, Кт аль их талаар давталт хийх
        for line in self.env['account.move.line'].browse(line_ids):
            # Дт эвсэл Кт тал нь 0 бөгөөд олон дансны хувьд тулгалт хасалттай ашигласан үед хэрэглэнэ
            if total != 0:
                ids.append(line.id)
                # Тулгалт хасалттайгаар үед ашиглана
                writeoff_ids.remove(line.id)
                total -= (line.debit + line.credit)
                if line.amount_currency and line.debit > 0:
                    total_currency -= line.amount_currency
                elif line.amount_currency and line.credit > 0:
                    total_currency += line.amount_currency
            # Нийлбэр дүнг шалгах
            if total <= 0:
                amount_currency = 0
                balance_currency = 0
                if dt:  # Дт тал нь их бол
                    debit = float_round(total + line.debit, precision_digits=precision)
                    credit = 0
                    par_debit = float_round(-total, precision_digits=precision)
                    par_credit = 0
                else:  # Кт тал нь их бол
                    debit = 0
                    credit = float_round(total + line.credit, precision_digits=precision)
                    par_debit = 0
                    par_credit = float_round(-total, precision_digits=precision)
                if currency_writeoff != 0 and total_currency <= 0:
                    if dt:
                        amount_currency = total_currency + line.amount_currency
                        balance_currency = -total_currency
                    else:
                        amount_currency = line.amount_currency - total_currency
                        balance_currency = total_currency
                elif currency_writeoff == 0 and line.currency_id:
                    if dt:
                        if line.amount_currency == 0:
                            amount_currency = 0
                        else:
                            rate = line.debit / line.amount_currency
                            amount_currency = debit / rate
                    else:
                        if line.amount_currency == 0:
                            amount_currency = 0
                        else:
                            rate = line.credit / line.amount_currency
                            amount_currency = credit / rate
                    balance_currency = line.amount_currency - amount_currency
                elif currency_writeoff != 0 and total_currency > 0:
                    ids.remove(line.id)
                    total += (line.debit + line.credit)
                    writeoff_ids.append(line.id)                    # Тулгалт хасалттайгаар үед ашиглана
                    if line.debit > 0:
                        total_currency += line.amount_currency
                    elif line.credit > 0:
                        total_currency -= line.amount_currency
                if (total == 0 and total_currency != 0) or (total != 0 and total_currency == 0) or (total < 0 and total_currency < 0):
                    # Үндсэн журналын бичилтийг салгах
                    if journal_id and writeoff_acc_id:                                  # Тулгалт хасалттайгаар үед
                        writeoff_account = self.env['account.account'].browse(writeoff_acc_id)
                        amount_currency = 0
                        currency = False
                        # Дансны валютууд шалгах хэсэг
                        if line.amount_currency != 0 and line.currency_id and writeoff_account.currency_id:
                            if line.currency_id == writeoff_account.currency_id:
                                amount_currency = balance_currency
                                currency = line.currency_id.id
                            else:
                                # 3 дагч валют дээр алдааны мэдээлэл өгнө
                                raise UserError(_("Operation not allowed. You can only reconcile entries that share the same secondary currency or that don't have one. Change your writeoff account."))
                        # Тулгалт хасалттай үед журналын мөрийн эсрэг журналын мөр үүсгэх
                        writeoff_lines.append(line._create_move_line(date_p or date, journal_id, par_credit, par_debit))
                        # Тулгалт хасалттай дээр сонгосон дансны бичилт
                        vals = {'name': self._context.get('comment') or _('Write-Off'),
                                'ref': self._context.get('comment') or _('Write-Off'),
                                'debit': par_debit or 0,
                                'credit': par_credit or 0,
                                'account_id': writeoff_acc_id,
                                'date': date_p or date,
                                'journal_id': journal_id,
                                'analytic_account_id': analytic_account_id,
                                'partner_id': line.partner_id and line.partner_id.id,
                                'currency_id': currency,
                                'amount_currency': amount_currency or 0
                                }
                        if writeoff_account.req_analytic_account:
                            if 'analytic_share_ids' in self._context:
                                vals['analytic_share_ids'] = self._context['analytic_share_ids']
                            if 'analytic_2nd_share_ids' in self._context:
                                vals['analytic_share_ids2'] = self._context['analytic_2nd_share_ids']
                        writeoff_lines.append((0, 0, vals))
                        # Тухайн журналын мөрийн эсрэг журналын мөр үүсгэх
                        move_lines.append(line._create_move_line(date, reconcile_journal_id, credit, debit))
                    else:                                                                   # Бичилт салган тулгалт үед
                        if debit > 0 and amount_currency < 0:
                            raise UserError(_('Debit: %s and Currency amount: %s is not reconcile!')
                                            % (float_round(debit, precision_digits=precision), float_round(amount_currency, precision_digits=precision)))
                        if credit > 0 and amount_currency > 0:
                            raise UserError(_('Credit: %s and Currency amount: %s is not reconcile!')
                                            % (float_round(credit, precision_digits=precision), float_round(amount_currency, precision_digits=precision)))
                        self.env.cr.execute('''UPDATE account_move_line SET debit = %s, credit = %s, amount_currency = %s,
                                                      balance = %s WHERE id = %s''', (debit, credit, amount_currency, (debit - credit), line.id))
                        line.create_analytic_lines()
                        line_id = self.env['account.move.line'].create(line._get_partial_move_line_vals(par_debit, par_credit, balance_currency))
                # Олон харилцагч, Олон данс байгаа эсэхийг шалгах
                if reconcile_journal_id and date:
                    # Тулгалт хасалттайгаар үед ашиглана
                    if journal_id and writeoff_acc_id:
                        count = 0
                        writeoff_account = self.env['account.account'].browse(writeoff_acc_id)
                        ids += writeoff_ids
                        for writeoff_line in self.env['account.move.line'].browse(writeoff_ids):
                            count += 1
                            amount_currency = 0
                            currency = False
                            # Дансны валютууд шалгах хэсэг
                            if writeoff_line.amount_currency != 0 and writeoff_line.currency_id and writeoff_account.currency_id:
                                if writeoff_line.currency_id == writeoff_account.currency_id:
                                    amount_currency = writeoff_line.amount_currency
                                    currency = writeoff_line.currency_id.id
                                else:
                                    # 3 дагч валют дээр алдааны мэдээлэл өгнө
                                    raise UserError(_("Operation not allowed. You can only reconcile entries that share the same secondary currency or that don't have one. Change your writeoff account."))
                            # Тулгалт хасалттай үед журналын мөрийн эсрэг журналын мөр үүсгэх
                            writeoff_lines.append(writeoff_line._create_move_line(date_p or date, journal_id, writeoff_line.credit, writeoff_line.debit))
                            # Тулгалт хасалттай дээр сонгосон дансны бичилт
                            writeoff_lines.append((0, 0, {'name': self._context.get('comment') or _('Write-Off'),
                                                          'ref': self._context.get('comment') or _('Write-Off'),
                                                          'debit': writeoff_line.debit or 0,
                                                          'credit': writeoff_line.credit or 0,
                                                          'account_id': writeoff_acc_id,
                                                          'date': date_p or date,
                                                          'journal_id': journal_id,
                                                          'analytic_account_id': analytic_account_id,
                                                          'partner_id': writeoff_line.partner_id and writeoff_line.partner_id.id,
                                                          'currency_id': currency,
                                                          'amount_currency': amount_currency or 0
                                                          }))
                        # Тулгалт хасалттай эсрэг журналын бичилт үүсгэх
                        if writeoff_lines:
                            writeoff_move = self.env['account.move'].create({'ref': self._context.get('comment') or _('Write-Off'),
                                                                             'date': date,
                                                                             'journal_id': journal_id,
                                                                             'line_ids': writeoff_lines})
                            writeoff_move.post()
                            # Авлага өглөгийн тулгалт руу нэмэх
                            off_id = self.env['account.move.line'].search([('move_id', '=', writeoff_move.id), ('reconcile_move_id', '!=', False)])
                            if off_id:
                                off_ids += off_id.ids
                    else:
                        # Тухайн журналын мөрийн эсрэг журналын мөр үүсгэх
                        move_lines.append(line._create_move_line(date, reconcile_journal_id, line.credit, line.debit))
                    if move_lines:
                        # Эсрэг журналын бичилт үүсгэх
                        move = self.env['account.move'].create({'ref': _('Receivable, Payable Reconcile: '),
                                                                'date': date,
                                                                'journal_id': reconcile_journal_id,
                                                                'line_ids': move_lines})
                        move.post()
                        res = move.line_ids.ids
                    # Нийт үүссэн журналын мөрийн id- нэмнэ
                    res += ids + off_ids
                    # Авлага, өглөгийн тулгалт үүсгэнэ
                    self.env['account.move.receivable.payable.reconcile'].create({'line_ids': [(6, 0, res)]})
                    # Нэхэмжлэл төлөгдсөн төлөвт оруулах функц
                    line_ids = self.env['account.move.line'].search([('id', 'in', res)])
                    line_ids.auto_reconcile_lines()
                    for id in ids:
                        line_id = self.env['account.move.line'].search([('reconcile_move_id', '=', id)])
                        if line_id:
                            reconcile_lines = line_id.ids
                            reconcile_lines.append(id)
                            # Үндсэн тулгалт үүсгэнэ
                            self.env['account.full.reconcile'].with_context(check_move_validity=False).create({'reconciled_line_ids': [(6, 0, reconcile_lines)]})
                else:
                    # Үндсэн тулгалт үүсгэнэ
                    self.env['account.full.reconcile'].with_context(check_move_validity=False).create({'reconciled_line_ids': [(6, 0, ids)]})
                _logger.info(_('Finished: Receivable, Payable Part/Writeoff Reconcile'))
                # Нэхэмжлэл төлөгдсөн төлөвт оруулах функц
                line_ids = self.env['account.move.line'].search([('id', 'in', ids)])
                line_ids.auto_reconcile_lines()
                return ids

            # Олон харилцагч, Олон данс байгаа эсэхийг шалгах
            if reconcile_journal_id and date:
                # Тухайн журналын мөрийн эсрэг журналын мөр үүсгэх
                move_lines.append(line._create_move_line(date, reconcile_journal_id, line.credit, line.debit))
        # Нэхэмжлэл төлөгдсөн төлөвт оруулах функц
        line_ids = self.env['account.move.line'].search([('id', 'in', ids)])
        line_ids.auto_reconcile_lines()
        return ids
