# -*- coding: utf-8 -*-
from odoo import fields, models, api


class AccountMoveReceivablePayableReconcile(models.Model):
    _name = "account.move.receivable.payable.reconcile"
    _description = "Receivable, Payable Account Reconciliation"
    
    name = fields.Char('Name', required=True, copy=False, default=lambda self: self.env['ir.sequence'].next_by_code('account.move.receivable.payable.reconcile'))
    line_ids = fields.One2many('account.move.line', 'receivable_payable_reconcile_id', 'Entry Lines')
    create_date = fields.Date('Creation date', readonly=True)
