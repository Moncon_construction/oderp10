# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.tools.float_utils import float_round
from odoo.exceptions import UserError


class AccountMoveLineReconcile(models.TransientModel):
    _inherit = "account.move.line.reconcile"

    currency_credit = fields.Float('Credit Amount Currency', readonly=True, digits=0)
    currency_debit = fields.Float('Debit Amount Currency', readonly=True, digits=0)
    currency_writeoff = fields.Float('Write-Off Amount Currency', readonly=True)
    check_writeoff = fields.Float('Write-Off Check', readonly=True)
    currency = fields.Boolean(string='Many Currency', readonly=True, default=False)
    partner = fields.Boolean(string='Many Partner', readonly=True, default=False)
    account = fields.Boolean(string='Many Account', readonly=True, default=False)
    reconcile_journal_id = fields.Many2one('account.journal', 'Reconcile Journal')
    date = fields.Date('Date', default=fields.Date.context_today)

    @api.model
    def default_get(self, fields):
        res = super(AccountMoveLineReconcile, self).default_get(fields)
        data = self.trans_rec_get()
        if 'trans_nbr' in fields:
            res.update({'trans_nbr': data['trans_nbr']})
        if 'credit' in fields:
            res.update({'credit': data['credit']})
        if 'debit' in fields:
            res.update({'debit': data['debit']})
        if 'writeoff' in fields:
            res.update({'writeoff': data['writeoff']})
        if 'currency_credit' in fields:
            res.update({'currency_credit': data['currency_credit']})
        if 'currency_debit' in fields:
            res.update({'currency_debit': data['currency_debit']})
        if 'currency_writeoff' in fields:
            res.update({'currency_writeoff': data['currency_writeoff']})
        if 'check_writeoff' in fields:
            res.update({'check_writeoff': data['check_writeoff']})
        if 'partner' in fields:
            res.update({'partner': data['partner']})
        if 'account' in fields:
            res.update({'account': data['account']})
        if 'currency' in fields:
            res.update({'currency': data['currency']})
        if 'move_line_reconcile' in fields:
            res.update({'move_line_reconcile': data['move_line_reconcile']})
        return res

    @api.multi
    def trans_rec_get(self):
        context = self._context or {}
        credit = debit = currency_credit = currency_debit = 0
        account = partner = False
        account_id = False
        partner_id = False
        currency = False
        dt_currency_ids = []
        kt_currency_ids = []
        currency_ids = []
        lines = self.env['account.move.line'].browse(context.get('active_ids', []))
        company_currency = lines[0].company_id.currency_id.id
        for line in lines:
            if not line.full_reconcile_id:
                if account_id and account_id != line.account_id.id:
                    account = True
                if partner_id and partner_id != line.partner_id.id:
                    partner = True
                if line.debit > 0:
                    if not line.currency_id and company_currency not in currency_ids:
                        dt_currency_ids.append(line.currency_id.id)
                    elif line.currency_id and line.currency_id.id not in currency_ids:
                        dt_currency_ids.append(line.currency_id.id)
                    debit += line.debit or 0
                    currency_debit += line.amount_currency
                elif line.credit > 0:
                    if not line.currency_id and company_currency not in currency_ids:
                        kt_currency_ids.append(line.currency_id.id)
                    elif line.currency_id and line.currency_id.id not in currency_ids:
                        kt_currency_ids.append(line.currency_id.id)
                    credit += line.credit or 0
                    currency_credit -= line.amount_currency
                if not line.currency_id and company_currency not in currency_ids:
                    currency_ids.append(line.currency_id.id)
                elif line.currency_id and line.currency_id.id not in currency_ids:
                    currency_ids.append(line.currency_id.id)
                account_id = line.account_id.id
                partner_id = line.partner_id.id
        precision = self.env.user.company_id.currency_id.decimal_places
        writeoff = float_round(debit - credit, precision_digits=precision)
        credit = float_round(credit, precision_digits=precision)
        debit = float_round(debit, precision_digits=precision)
        currency_writeoff = float_round(currency_debit - currency_credit, precision_digits=precision)
        currency_credit = float_round(currency_credit, precision_digits=precision)
        currency_debit = float_round(currency_debit, precision_digits=precision)
        if len(currency_ids) == 2 and company_currency not in currency_ids\
                and len(dt_currency_ids) == 2 or len(kt_currency_ids) == 2 and currency_writeoff != 0:          # 2 өөр валютын тулгалт
            currency = True
        elif len(currency_ids) == 2 and company_currency in currency_ids\
                and currency_credit != 0 and currency_debit != 0 and currency_writeoff != 0:                    # Компанийн үндсэн валют болон өөр 1 валютын тулгалт
            currency = True
        elif len(currency_ids) == 1 and currency_ids[0] != company_currency and currency_writeoff != 0:         # 1 валютын тулгалт
            currency = True
        check_writeoff = 0
        if debit > 0 and credit > 0:
            check_writeoff = writeoff
        return {'trans_nbr': len(lines), 'credit': credit, 'debit': debit, 'writeoff': writeoff,
                'currency': currency, 'currency_credit': currency_credit, 'currency_debit': currency_debit,
                'currency_writeoff': currency_writeoff, 'check_writeoff': check_writeoff, 'partner': partner, 'account': account}

    @api.multi
    def trans_rec_reconcile_move_line(self):
        move_lines = self.env['account.move.line'].search([('id', 'in', self._context.get('active_ids', []))], order='date')
        res = move_lines.reconcile_part_line(self.date or False, self.reconcile_journal_id and self.reconcile_journal_id.id or False)
        if self._context.get('statement', False):
            return True
        else:
            return {
                'name': _('Reconciled Journal Entry Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.move.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'domain': [('id', 'in', res)],
            }

    @api.multi
    def trans_rec_reconcile_full(self):
        move_lines = self.env['account.move.line'].browse(self._context.get('active_ids', []))
        if self.partner or self.account or self.currency_writeoff != 0:
            res = move_lines.reconcile_receivable_payable(self.date, self.reconcile_journal_id.id)
            if self._context.get('statement', False):
                return True
            else:
                return {
                    'name': _('Reconciled Journal Entry Lines'),
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'res_model': 'account.move.line',
                    'view_id': False,
                    'type': 'ir.actions.act_window',
                    'domain': [('id', 'in', res)],
                }
        currency = False
        for aml in move_lines:
            if not currency and aml.currency_id.id:
                currency = aml.currency_id.id
            elif aml.currency_id:
                if aml.currency_id.id == currency:
                    continue
                raise UserError(_('Operation not allowed. You can only reconcile entries that share the same secondary currency or that don\'t have one. Edit your journal items or make another selection before proceeding any further.'))
        # Don't consider entrires that are already reconciled
        move_lines_filtered = move_lines.filtered(lambda aml: not aml.reconciled)
        # Because we are making a full reconcilition in batch, we need to consider use cases as defined in the test test_manual_reconcile_wizard_opw678153
        # So we force the reconciliation in company currency only at first
        move_lines_filtered.with_context(skip_full_reconcile_check='amount_currency_excluded', manual_full_reconcile_currency=currency).reconcile()

        # then in second pass the amounts in secondary currency, only if some lines are still not fully reconciled
        move_lines_filtered = move_lines.filtered(lambda aml: not aml.reconciled)
        if move_lines_filtered:
            move_lines_filtered.with_context(skip_full_reconcile_check='amount_currency_only', manual_full_reconcile_currency=currency).reconcile()
        move_lines.compute_full_after_batch_reconcile()
        if self._context.get('statement', False):
            return True
        else:
            return {
                'name': _('Reconciled Journal Entry Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.move.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'domain': [('id', 'in', self.ids)],
            }

    @api.multi
    def trans_rec_addendum_writeoff(self):
        context = {'reconcile_journal_id': self.reconcile_journal_id and self.reconcile_journal_id.id or False,
                   'date': self.date or False,
                   'currency_writeoff': self.currency_writeoff,
                   'line_ids': self._context.get('active_ids', [])}
        return self.env['account.move.line.reconcile.writeoff'].with_context(context).trans_rec_addendum()


class AccountMoveLineReconcileWriteoff(models.TransientModel):
    _inherit = 'account.move.line.reconcile.writeoff'

    @api.model
    def default_get(self, fields):
        res = super(AccountMoveLineReconcileWriteoff, self).default_get(fields)
        data = self.trans_rec_get()
        if 'journal_id' in fields:
            res.update({'journal_id': data['reconcile_journal_id']})
        return res

    @api.multi
    def trans_rec_get(self):
        reconcile_journal_id = self._context.get('reconcile_journal_id')
        return {'reconcile_journal_id': reconcile_journal_id}

    @api.multi
    def trans_rec_reconcile(self):
        context = dict(self._context or {})
        context['date_p'] = self.date_p
        context['comment'] = self.comment
        context['writeoff_acc_id'] = self.writeoff_acc_id.id
        context['journal_id'] = self.journal_id.id
        if 'analytic_share_ids' in self and self.analytic_share_ids:
            context['analytic_share_ids'] = [(0, 0, {'analytic_account_id': sh.analytic_account_id.id, 'rate': sh.rate}) for sh in self.analytic_share_ids]
        if 'analytic_2nd_share_ids' in self and self.analytic_2nd_share_ids:
            context['analytic_2nd_share_ids'] = [(0, 0, {'analytic_account_id': sh.analytic_account_id.id, 'rate': sh.rate}) for sh in self.analytic_2nd_share_ids]
        reconcile_journal_id = self._context.get('reconcile_journal_id')
        date = self._context.get('date')
        currency_writeoff = self._context.get('currency_writeoff')
        if self.analytic_id:
            context['analytic_id'] = self.analytic_id.id
        move_lines = self.env['account.move.line'].search([('id', 'in', self._context.get('line_ids', []))], order='date')
        if reconcile_journal_id or currency_writeoff != 0:
            res = move_lines.with_context(context).reconcile_part_line(date, reconcile_journal_id)
            return {
                'name': _('Reconciled Journal Entry Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.move.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'domain': [('id', 'in', res)],
            }
        currency = False
        for aml in move_lines:
            if not currency and aml.currency_id.id:
                currency = aml.currency_id.id
            elif aml.currency_id:
                if aml.currency_id.id == currency:
                    continue
                raise UserError(_(
                    'Operation not allowed. You can only reconcile entries that share the same secondary currency or that don\'t have one. Edit your journal items or make another selection before proceeding any further.'))
        # Don't consider entrires that are already reconciled
        move_lines_filtered = move_lines.filtered(lambda aml: not aml.reconciled)
        # Because we are making a full reconcilition in batch, we need to consider use cases as defined in the test test_manual_reconcile_wizard_opw678153
        # So we force the reconciliation in company currency only at first,
        context['skip_full_reconcile_check'] = 'amount_currency_excluded'
        context['manual_full_reconcile_currency'] = currency
        writeoff = move_lines_filtered.with_context(context).reconcile(self.writeoff_acc_id, self.journal_id)
        # then in second pass the amounts in secondary currency, only if some lines are still not fully reconciled
        move_lines_filtered = move_lines.filtered(lambda aml: not aml.reconciled)
        if move_lines_filtered:
            move_lines_filtered.with_context(skip_full_reconcile_check='amount_currency_only', manual_full_reconcile_currency=currency).reconcile()
        if not isinstance(writeoff, bool):
            move_lines += writeoff
        move_lines.compute_full_after_batch_reconcile()
        return {
            'name': _('Reconciled Journal Entry Lines'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move.line',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', move_lines.ids)],
        }
