# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################
{
    'name': 'Mongolian Accounting Tax Reports',
    'version': '1.0',
    'depends': [
        'l10n_mn_account',
        'l10n_mn_account_period',
        'l10n_mn_contacts',
        'l10n_mn_account_asset'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """ТТ-12 Суутгагчийн хувь хүнд олгосон орлогын албан татварын тайлан, TT-13 Суутгагчийн аж ахуй нэгжид олгосон орлогоос суутгасан албан татварын тайлан,
    TT-02 Аж ахуй нэгжийн орлогын албан татварын тайлан, TT23 Үл хөдлөх эд хөрөнгийн албан татварын тайлан, ТТ24(3) Агаарын бохирдлын тайлан""",
    'data': [
        'security/ir.model.access.csv',
        'security/ct30_tt02_access_user.xml',
        'data/account_report_option_data.xml',
        'data/vat_indication_data.xml',
        'data/tt02b_data.xml',
        'data/tt02_data.xml',
        'data/tt03b_data.xml',
        'data/tt13_data.xml',
        'data/tt12_data.xml',
        'data/tt03a_data.xml',
        'data/tt23_data.xml',
        'data/tt24_data.xml',
        'report/report_option_view.xml',
        'views/account_view.xml',
        'views/account_vehicle_view.xml',
        'views/account_asset_view.xml',
        'views/account_tax_view.xml',
        'views/vat_indication_view.xml',
        'views/account_move_view.xml',
        'views/account_invoice_view.xml',
        'views/tax_ct30_view.xml',
        'views/tax_tt02_report_view.xml',
        'wizard/tax_tt04_report_view.xml',
        'wizard/tax_tt12_report_view.xml',
        'wizard/tax_tt13_report_view.xml',
        'wizard/tax_tt03b_report_view.xml',
        'wizard/tax_tt23_report_view.xml',
        'wizard/tax_tt24_report_view.xml',
        'wizard/account_report_vat_journal_view.xml',
        'views/tt03a_tax_report_view.xml'
    ],
    "auto_install": False,
    "installable": True,
}
