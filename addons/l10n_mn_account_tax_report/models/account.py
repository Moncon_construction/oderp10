# -*- coding: utf-8 -*-
from odoo import fields, models


class AccountAccount(models.Model):
    _inherit = "account.account"

    is_vat = fields.Boolean(string='Is VAT Account ?')
    vat_indication_income_ids = fields.Many2many('vat.indication', 'account_vat_indication_income_rel', 'account_id', 'vat_indication_income_id', string='Vat Indication', domain=[('indication_type', '=', 'income')])
    vat_indication_outcome_ids = fields.Many2many('vat.indication', 'account_vat_indication_outcome_rel', 'account_id', 'vat_indication_outcome_id', string='Vat Indication', domain=[('indication_type', '=', 'outcome')])
    choose_vat_indication = fields.Boolean('Choose vat indication')