# -*- coding: utf-8 -*-
from odoo import fields, models, api, _


class AccountAssetLocation(models.Model):
    _inherit = 'account.asset.location'

    asset_tax_percent = fields.Float(String='Asset Tax Percent', required=True)


class AccountAssetCategory(models.Model):
    _inherit = 'account.asset.category'

    real_estate_intent = fields.Selection([('manufacturing and service', 'Manufacturing and Service'),
                                           ('private apartment', 'Private Apartment'),
                                           ('public building', 'Public Building')])
    real_estate_type = fields.Selection([('house, building', 'House, Building'),
                                         ('facility', 'Facility'),
                                         ('construction', 'Construction'),
                                         ('other', 'Other')])
    tax_report_type = fields.Selection([('building', 'Building'),
                                        ('equipment', 'Equipment'),
                                        ('vehicle', 'Vehicle'),
                                        ('other', 'Other Asset')], string='Tax Report Type')
    deduction_vat_number = fields.Integer(string='Deduction Vat Number /month/', default=1)
    vat_indication_id = fields.Many2one('vat.indication', 'Vat Indication')

    @api.onchange('tax_report_type')
    def onchange_tax_report_type(self):
        if self.tax_report_type == 'building':
            self.deduction_vat_number = 120
        elif self.tax_report_type == 'equipment':
            self.deduction_vat_number = 60
        else:
            self.deduction_vat_number = 1


class AccountAssetAsset(models.Model):
    _inherit = 'account.asset.asset'

    tax_report_type = fields.Selection(related='category_id.tax_report_type', string='Tax Report Type')
    asset_record_amount = fields.Float('Asset Record Amount', help='This field shows asset record amount')
    asset_insurance_amount = fields.Float('Asset Insurance Amount', help='This field shows asset insurance amount')
    engine_type = fields.Selection([('gasoline', 'Gasoline'), ('diesel', 'Diesel')], 'Type of engine', default='gasoline', required=True, help="This selection field choose type of engine")
    date_of_manufacture = fields.Date('Date of manufacture', default=fields.Date.context_today, required=True, help="This field gives you date of manufacture")
    automobiles_and_self_moving_vehicles = fields.Boolean('Automobiles and self moving vehicles', default=False, help="Automobiles and self moving vehicles")
    cylinder_capacitance = fields.Integer('Cylinder capacitance', default=0, required=True, help="This field gives cylinder capacitance information")
    co2_emissions = fields.Selection([('no tax', 'No Tax'),
                                      ('a', 'A type'),
                                      ('b', 'B type'),
                                      ('c', 'C type'),
                                      ('d', 'D type'),
                                      ('e', 'E type'),
                                      ('f', 'F type')], 'CO2 EMISSION g/km', default='no tax', required=True,
                                      help="No tax 0-120\nA - 121-180 \nB - 181-250 \nC - 251-350 \nD - 351-500 \nE - 501-750 \n F - 751+ \n ")
    vehicle_tax_id = fields.Many2one('account.vehicle.tax', string='Vehicle Annual Tax Adjustment')
    vehicle_name = fields.Text('Vehicle Name,Mark')
    vehicle_reg_plates = fields.Text('Vehicle Registration Plates')
    vehicle_reg_code = fields.Text('Vehicle Registration Code')
    airpol_cer_number = fields.Text('Vehicle, Air Pollution Payment Certificate Number')
    comment = fields.Text('Comment')
    passenger_capacity = fields.Char('Passenger Capacity', invisible=True)
    truck_capacity = fields.Char('Truck Capacity', invisible=True)
    passenger_invi = fields.Boolean(default=False)
    truck_invi = fields.Boolean(default=False)

    @api.onchange('vehicle_tax_id')
    def onchange_vehicle_tax_id(self):
        for obj in self:
            veh_line = self.env['account.vehicle.type'].search([('id', '=', obj.vehicle_tax_id.vehicle_type_id.id)])
            if veh_line['vehicle_type'] == 'passenger_car':
                obj.passenger_invi = True
            elif veh_line['vehicle_type'] == 'truck':
                obj.truck_invi = True
            else :
                obj.passenger_invi = False
                obj.truck_invi = False