# -*- coding: utf-8 -*-
from odoo import models, api, fields


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    vat_indication_id = fields.Many2one('vat.indication', 'Vat Indication')

    @api.onchange('vat_indication_id')
    def onchange_vat_indication_id(self):
        for line in self.invoice_line_ids:
            if (line.choose_vat_indication and not line.vat_indication_id) or line.vat_indication_id == self._origin.vat_indication_id:
                line.vat_indication_id = self.vat_indication_id.id

    def action_invoice_open(self):
        res = super(AccountInvoice, self).action_invoice_open()
        for line in self.invoice_line_ids:
            if line.vat_indication_id:
                for move_line in self.move_id.line_ids.filtered(lambda l: l.account_id.id == line.account_id.id):
                    move_line.write({'vat_indication_id': line.vat_indication_id.id})
        return res


class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'

    @api.onchange('account_id')
    def _onchange_account_id(self):
        domain = {}
        super(AccountInvoiceLine, self)._onchange_account_id()
        self.vat_indication_id = self.invoice_id.vat_indication_id
        account_id = self.account_id
        if account_id and account_id.choose_vat_indication:
            if account_id.internal_type == 'income':
                self._cr.execute("SELECT vat_indication_income_id FROM account_vat_indication_income_rel where account_id = " + str(account_id.id))
            else:
                self._cr.execute("SELECT vat_indication_outcome_id FROM account_vat_indication_outcome_rel where account_id = " + str(account_id.id))
            indications = self._cr.fetchall()
            if len(indications) == 1:
                self.vat_indication_id = indications[0][0]
            if indications:
                domain['vat_indication_id'] = [('id', 'in', indications)]
                return {'domain': domain}
            else:
                domain['vat_indication_id'] = []
                return {'domain': domain}
        else:
            self.vat_indication_id = False

    vat_indication_id = fields.Many2one('vat.indication', 'Vat Indication')
    is_taxpayer = fields.Boolean(string='Tax Payer', default=lambda self: self.env.user.company_id.partner_id.is_taxpayer)
    is_vat_include = fields.Boolean(default=False, string='Tax Report Type')
    choose_vat_indication = fields.Boolean(related='account_id.choose_vat_indication')

    @api.onchange('invoice_line_tax_ids')
    def onchange_invoice_line_tax_ids(self):
        for obj in self:
            if obj.is_taxpayer and obj.invoice_line_tax_ids:
                for tax_line in obj.invoice_line_tax_ids:
                    if tax_line.tax_report_type == '4':
                        obj.is_vat_include = True
                        break
                    else:
                        obj.is_vat_include = False
            else:
                obj.is_vat_include = False