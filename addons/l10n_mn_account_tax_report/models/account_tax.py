# -*- coding: utf-8 -*-
from odoo import models, fields


class AccountTax(models.Model):
    _inherit = 'account.tax'

    tax_report_type = fields.Selection([('1', 'KHKHOAT-TT-12 (1)'),
                                        ('2', 'KHKHOAT-TT-12 (2)'),
                                        ('3', 'KHKHOAT-TT-12 (3)'),
                                        ('4', 'TT-03a')], string='Tax report type')
