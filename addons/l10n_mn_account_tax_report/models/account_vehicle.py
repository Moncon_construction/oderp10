# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
from odoo.exceptions import UserError

class AccountVehicleType(models.Model):
    _name = 'account.vehicle.type'
    name = fields.Char('Name')
    code = fields.Char('Code' , required=True)
    vehicle_type = fields.Selection([('2_wheeled', '2 wheeled motorcycle /2000-3000,  1800-2700/'),
                                      ('3_wheeled', '3 wheeled motorcycle /3000-4500,  2800-4200/'),
                                      ('passenger_car','Passenger car'),
                                      ('micro_bus', 'Micro bus /15 seats/ /35000-52500, 28000-42000/'),
                                      ('bus', 'Bus /52000-78000 , 40000-60000/'),
                                      ('truck','Truck'),
                                      ('em_vehicle', 'Emergency vehicle /16000-24000, 15000-22500/'),
                                      ('other_tractor', 'Other tractor automotive tools /14000-21000, 11200-16800/'),
                                      ('compact_tractor', 'Compact tractor /7000-10500,  5600-8400/'),
                                      ('tracker_load','Tracker Load /Per tonne/ /5500-8250, 5500-8250/')],
                                     string='АТБӨЯХ-ийн төрөл', required=True)

    @api.onchange('vehicle_type')
    def onchange_vehicle_type(self):
        for obj in self:
            obj.name = dict(self._fields['vehicle_type'].selection).get(obj.vehicle_type)

class AccountVehicleTax(models.Model):
    _name = 'account.vehicle.tax'

    name = fields.Char('Name')
    vehicle_type_id = fields.Many2one('account.vehicle.type' , string="Vehicle Type", required=True)
    inv_vehicle_type = fields.Selection(related='vehicle_type_id.vehicle_type')

    truck_load = fields.Selection([('1_ton', 'Up to 1 tonne /25000-37500 , 20000-30000/'),
                                      ('1_2_ton', '1-2 tons /35000-52500 , 28000-42000/'),
                                      ('2_3_ton', '2-3 tons /45000-67500 , 36000-54000/'),
                                      ('3_5_ton', '3-5 tons /55000-82500 , 44000-66000/'),
                                      ('5_8_ton', '5-8 tons /80000-120000 , 64000-96000/'),
                                      ('8_10_ton', '8-10 tons /90000-135000 , 72000-108000/'),
                                      ('10_20_ton', '10-20 tons /100000-150000 , 80000-120000/'),
                                      ('20_30_ton', '20-30 tons /140000-210000 , 120000-180000/'),
                                      ('30_40_ton', '30-40 tons /180000-270000 , 160000-240000/'),
                                      ('40_50_ton', '40-50 tons /220000-330000 , 200000-300000/'),
                                      ('50_60_ton', '50-60 tons /280000-420000 , 240000-360000/'),
                                      ('60_70_ton', '60-70 tons /320000-480000 , 280000-420000/'),
                                      ('70_80_ton', '70-80 tons /360000-540000 , 320000-480000/'),
                                      ('80_90_ton', '80-90 tons /400000-600000 , 380000-570000/'),
                                      ('90_100_ton', '90-100 tons /440000-660000 , 420000-630000/'),
                                      ('100_ton', 'Over 100 tons /480000-720000 , 440000-660000/')],
                                     string='Truck load')

    car_capacity = fields.Selection([('car_cube_one', 'Up to 2000 cm cube/ 16-24, 14-21 /'),
                                        ('car_cube_two', '2001-3000 см cube / 18-27, 16-24 /'),
                                        ('car_cube_three', '3001 cm above cube / 18-27, 16-24 /')],
                                     string='Car seat cylinder capacity')

    location_id = fields.Many2one('res.district', string='District', required=True)

    tracker_capacity = fields.Integer(string='Tracker capacity')
    taxation = fields.Integer(string='Taxation', required=True)

    @api.model
    def create(self, vals):
        res = super(AccountVehicleTax, self).create(vals)
        type = self.env['account.vehicle.type'].search([('id','=',res.vehicle_type_id.id)])
        location = self.env['res.district'].search([('id','=',res.location_id.id)])

        if dict(self._fields['truck_load'].selection).get(res.truck_load):
            res.name = "{0} /{1}/".format(location.name ,type.name)
        elif dict(self._fields['car_capacity'].selection).get(res.car_capacity):
            res.name = "{0} /{1}/".format(location.name , type.name)
        else:
            res.name = "{0} /{1}/".format(location.name , type.name)
        return res