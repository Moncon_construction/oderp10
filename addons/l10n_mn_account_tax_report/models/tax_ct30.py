# -*- encoding: utf-8 -*-
##############################################################################
from odoo import api, fields, models, _
from odoo.modules import get_module_resource
from datetime import datetime
import json
import openpyxl
from io import BytesIO
from operator import itemgetter
import time
import base64
from odoo.exceptions import UserError
    
"""
   САНХҮҮГИЙН БОЛОН ОРЛОГЫН АЛБАН ТАТВАРЫН ТАЙЛАНГИЙН ҮЗҮҮЛЭЛТ ХООРОНДЫН ЗӨРҮҮГ ЗОХИЦУУЛАХ ТАЙЛАН
"""

INCOME_TYPE_SELECTION = [('line_1', u'Ногдол ашгийн орлого'),
                        ('line_2', u'Эрхийн шимтгэлийн орлого'),
                        ('line_3', u'Эрх борлуулсны орлого'),
                        ('line_4', u'Үл хөдлөх хөрөнгө борлуулсны орлого'),
                        ('line_5', u'Бусад')]
SPECIFICATION_1 = [('line_1', u'Үндсэн хөрөнгө'),
                    ('line_2', u'Гадаад валютын ханшийн зөрүү'),
                    ('line_3', u'Бараа материалын үнийн бууралтын алдагдал')]
SPECIFICATION_2 = [('line_1', u'Үндсэн хөрөнгө'),
                    ('line_2', u'Гадаад валютаар илэрхийлэгдсэн өглөг (ханшийн зөрүү)'),
                    ('line_3', u'Гадаад валютаар илэрхийлэгдсэн авлага (ханшийн зөрүү)'),
                    ('line_4', u'Бараа материал (Бараа материалын үнийн бууралтын алдагдал)')]


class TaxCT30ReportTable1(models.Model):
    _name = "tax.ct30.table1"
    _description = "Model of Table for Tax Difference Report CT-30"
    
    def _compute_tax_expense_amount(self):
        for obj in self:
            obj.tax_expense_amount = obj.income_amount / 100 * obj.percentage
        
    income_type = fields.Selection(INCOME_TYPE_SELECTION, u'Орлогын төрөл')
    from_where = fields.Char(u'Хаанаас ')
    income_amount = fields.Float(u'Орлогын дүн')
    percentage = fields.Float(u'Хувь хэмжээ')
    tax_expense_amount = fields.Float(u'Татварын зардлын дүн', compute=_compute_tax_expense_amount)
    recieved_income = fields.Float(u'Хүлээн авсан орлого')
    tax_id = fields.Many2one('tax.ct30', 'CT-30')
    

class TaxCT30ReportTable2(models.Model):
    _name = "tax.ct30.table2"
    _description = "Model of Table for Tax Difference Report CT-30"

    def _compute_tax_basic(self):
        for obj in self:
            obj.temporary_discrepancy = obj.account_amount - obj.tax_basic

    specification = fields.Selection(SPECIFICATION_1, u'Үзүүлэлт')
    account_amount = fields.Float(u'Дансны дүн')
    tax_basic = fields.Float(u'Татварын суурь')
    temporary_discrepancy = fields.Float(u'Түр зөрүү', compute=_compute_tax_basic)
    deferred_tax_liablities = fields.Float(u'Хойшлогдсон татварын өглөг')
    deferred_tax_assets = fields.Float(u'Хойшлогдсон татварын хөрөнгө')
    tax_id = fields.Many2one('tax.ct30', 'CT-30')

    
class TaxCT30ReportTable3(models.Model):
    _name = "tax.ct30.table3"
    _description = "Model of Table for Tax Difference Report CT-30"

    def _compute_last_balance(self):
        for obj in self:
            obj.last_balance = obj.first_balance + obj.add - obj.sub

    specification = fields.Selection(SPECIFICATION_2, u'Үзүүлэлт')
    first_balance = fields.Float(u'Эхний үлдэгдэл')
    add = fields.Float(u'Нэмэгдэх ')
    sub = fields.Float(u'Хасагдах')
    last_balance = fields.Float(u'Эцсийн үлдэгдэл', compute=_compute_last_balance)
    category = fields.Float(u'Ангилал /хөрөнгө, өр төлбөр/')
    tax_id = fields.Many2one('tax.ct30', 'CT-30')


class TaxCT30Report(models.Model):
    _name = "tax.ct30"
    _description = "Model for Tax Difference Report CT-30"
    
    name = fields.Char(u'Name')
    company_id = fields.Many2one('res.company', string=u'Компани', required=True, readonly=True, default=lambda self: self.env.user.company_id)
    fiscalyear_id = fields.Many2one('account.fiscalyear', u'Санхүүгийн жил', required=True)
    date_from = fields.Date(string=u'Эхлэл огноо', required=True)
    date_to = fields.Date(string=u'Дуусах огноо', required=True)
    state = fields.Selection([('draft', u'Ноорог'),
                              ('confirmed', u'Батлагдсан')], u'Төлөв', required=True, default='draft')

    tax_table1_ids = fields.One2many('tax.ct30.table1', 'tax_id', required=True)
    tax_table2_ids = fields.One2many('tax.ct30.table2', 'tax_id', required=True)
    tax_table3_ids = fields.One2many('tax.ct30.table3', 'tax_id', required=True)
    tax_tt02_id = fields.Many2one('tax.tt02', u'ААНОАТ-н бүртгэл')

    @api.onchange('tax_table1_ids')
    def onchange_check_table1(self):
        if self.tax_table1_ids:
            if len(self.tax_table1_ids) > 5:
                raise UserError(u'Давхардаагүй яг 5 мөр байх ёстой!')

    @api.onchange('tax_table2_ids')
    def onchange_check_table2(self):
        if self.tax_table2_ids:
            if len(self.tax_table2_ids) > 3:
                raise UserError(u'Давхардаагүй яг 3 мөр байх ёстой')

    @api.onchange('tax_table3_ids')
    def onchange_check_table3(self):
        if self.tax_table3_ids:
            if len(self.tax_table3_ids) > 4:
                raise UserError(u'Давхардаагүй яг 4 мөр байх ёстой')

    # A
    # 1
    a_1 = fields.Float(u'A.1. Санхүүгийн тайлангийн татварын өмнөх ашгийн дүн', readonly=True, related='tax_tt02_id.a_5')
    # 1.1
    a_1_1_1 = fields.Float(u'1.1 Хасагдахгүй зардал', readonly=True)
    a_1_1_2 = fields.Float(u'Засгийн газрын баталснаас бусад хэвийн хорогдол')
    a_1_1_3 = fields.Float(u'Баримтаар нотлогдохгүй байгаа зардал')
    a_1_1_4 = fields.Float(u'хандив, хувийн хэрэглээний зардал', readonly=True)
    # 1.2 Хязгаарлалтаас хэтэрсэн зардал
    a_1_2 = fields.Float(u'1.2 Томилолтын зардал')
    a_1_3 = fields.Float(u'1.3 Хүүгийн зардлаар хүлээн зөвшөөрөгдөхгүй зээлийн хүүгийн зардал')
    a_1_4 = fields.Float(u'1.4 Харилцан хамаарал бүхий ажил гүйлгээ')
    a_1_5 = fields.Float(u'1.5 Бараа материалын')
    a_1_6 = fields.Float(u'1.6 Бусад')
        
    def _compute_a_1_amount(self):
        for obj in self:
            obj.a_1_amount = obj.a_1_1_1 + obj.a_1_1_2 + obj.a_1_1_3 + obj.a_1_1_4 + obj.a_1_2 + obj.a_1_3 + obj.a_1_4 + obj.a_1_5 + obj.a_1_6
  
    a_1_amount = fields.Float(u'А1.Дүн', compute=_compute_a_1_amount)
    # 2
    a_2_1 = fields.Float(u'2.1 Чөлөөлөгдөх орлого ', readonly=True)   
    a_2_2 = fields.Float(u'2.2 Хөнгөлөлт эдлэх орлого ', readonly=True)   
    a_2_3 = fields.Float(u'2.3 Тусгайлсан хувь хэмжээтэй орлого', readonly=True)   
    a_2_4 = fields.Float(u'2.4 Татвар нь суутгагдсан  орлого ')   
    a_2_5 = fields.Float(u'2.5 Бусад')
    
    def _compute_a_2_amount(self):
        for obj in self:
            obj.a_2_amount = obj.a_2_1 + obj.a_2_2 + obj.a_2_3 + obj.a_2_4 + obj.a_2_5

    a_2_amount = fields.Float(u'А2.Дүн', compute=_compute_a_2_amount)

    # Б
    def _compute_b(self):
        for obj in self:
            obj.b = obj.a_1 + obj.a_1_amount - obj.a_2_amount

    def _compute_b_1(self):
        for obj in self:
            if 0 < obj.b and obj.b < 3000000:
                obj.b_1 = obj.b * 0.1
            elif 3000000 < obj.b:
                obj.b_1 = obj.b * 0.25 + 300000

    b = fields.Float(u'Б.Байнгын зөрүүгээр зохицуулагдсан дүн', compute=_compute_b)
    b_1 = fields.Float(u'1. Татварын зардал', compute=_compute_b_1)
    b_2 = fields.Float(u'2. Суутган тооцоололтоор төлөгдсөн татварын зардал')

    def _compute_b_3(self):
        for obj in self:
            obj.b_3 = obj.b_1 + obj.b_2

    b_3 = fields.Float(u'3. Тайлант үеийн татварын зардал', compute=_compute_b_3)
    # В ТҮР ЗӨРҮҮ
    # 3 
    v_3_1 = fields.Float(u'В.3.1 Элэгдлийн зардал – Санхүүгийн тайлангийн')
    v_3_2 = fields.Float(u'3.2 Валютын ханшийн зөрүүгийн бодит бус алдагдал', readonly=True)
    v_3_3 = fields.Float(u'3.3 Бусад')

    def _compute_v_3_amount(self):
        for obj in self:
            obj.v_3_amount = obj.v_3_1 + obj.v_3_2 + obj.v_3_3

    v_3_amount = fields.Float(u'В3.Нийт дүн (3.1+3.2+3.3)', compute=_compute_v_3_amount)
    # 4
    v_4_1 = fields.Float(u'4.1 Элэгдлийн зардал –Татварын тайлангийн')
    v_4_2 = fields.Float(u'4.2 Валютын ханшийн зөрүүгийн бодит бус ашиг', readonly=True)
    v_4_3 = fields.Float(u'4.3 Бусад')

    def _compute_v_4_amount(self):
        for obj in self:
            obj.v_4_amount = obj.v_4_1 + obj.v_4_2 + obj.v_4_3

    v_4_amount = fields.Float(u'В4.Нийт дүн (3.1+3.2+3.3)', compute=_compute_v_4_amount)

    # 5
    def _compute_v_5(self):
        for obj in self:
            obj.v_5 = obj.b + obj.v_3_amount - obj.v_4_amount

    v_5 = fields.Float(u'5.Нийтлэг хувь хэмжээгээр татвар ногдуулах орлого (Б + В3– В4)', compute=_compute_v_5)
    v_5_1 = fields.Float(u'5.1 Нэмэх нь: Сайн дурын даатгалын хэтрэлт')
    v_5_2 = fields.Float(u'5.2 Хасах нь: Шилжүүлэх алдагдлын дүн')

    # 6
    def _compute_v_6(self):
        for obj in self:
            obj.v_6 = obj.v_5_1 + obj.v_5_2 + obj.v_5

    v_6 = fields.Float(u'6.Татвар ногдуулах орлогын дүн (5+5.1+5.2)', compute=_compute_v_6)

    # 7 
    def _compute_v_7(self):
        for obj in self:
            if 0 <= obj.v_6 and obj.v_6 <= 3000000000:
                obj.v_7 = obj.v_6 * 0.1
            elif 3000000000 < obj.v_6:
                obj.v_7 = obj.v_6 * 0.25 + 3000000000

    v_7 = fields.Float(u'7.Ногдуулсан татвар (0-3000000,0 бол 10%, 3000000,0-аас дээш дүнгийн 25% + 300000,0)', compute=_compute_v_7)

    # 8
    def _compute_v_8_amount(self):
        for obj in self:
            obj.v_8_amount = obj.v_8_1 + obj.v_8_2

    v_8_amount = fields.Float(u'8.Хөнгөлөгдөх татварын дүн', compute=_compute_v_8_amount)
    v_8_1 = fields.Float(u'8.1 ААНОАТ-ын хуулийн ... заалтын дагуу')
    v_8_2 = fields.Float(u'8.2 ААНОАТ-ын хуулийн ... заалтын дагуу')

    # 9
    def _compute_v_9(self):
        for obj in self:
            obj.v_9 = obj.v_9_1 + obj.v_9_2

    v_9 = fields.Float(u'9.Тусгайлсан хувь хэмжээгээр татвар ногдуулах орлого, ногдох татвар', compute=_compute_v_9)
    v_9_1 = fields.Float(u'9.1 Tатварын хувь ...')
    v_9_2 = fields.Float(u'9.2 татварын хувь ...')
    # 10
    
    def _compute_v_10_a(self):
        for obj in self:
            obj.v_10_a = obj.v_6 + obj.v_9
        
    def _compute_v_10_b(self):
        for obj in self:
            obj.v_10_b = obj.v_7 - obj.v_8_amount + obj.v_9
        
    v_10_a = fields.Float(u'10.Нийт дүн (Татвар ногдуулах орлого 6+9)', compute=_compute_v_10_a)
    v_10_b = fields.Float(u'10.Нийт дүн (Төлбөл зохих татвар 7-8+9)', compute=_compute_v_10_b)
    v_11 = fields.Float(u'11.Хойшлогдсон татварын хөрөнгө Тайлангийн Дебит талд бичигдэнэ')
    v_12 = fields.Float(u'12.Хойшлогдсон татварын өглөг Тайлангийн Кредит талд бичигдэнэ.')
        
    def action_draft(self):
        self.write({'state':'draft'})

    def action_confirm(self):
        self.write({'state':'confirmed'})

    @api.multi
    def compute_with_account(self, obj):
        for this in self:
            report_option_id = self.env['account.tax.report.option'].search([('id', '=', obj)])
            total_amount = 0.0
            total_first_balance_amount = 0.0
            total_debit_amount = 0.0
            total_credit_amount = 0.0
            balance = 0.0
            if report_option_id.account_ids:
                if report_option_id.end_balance:
                    for account in report_option_id.account_ids:
                        initial_bal = self.env['account.move.line'].get_initial_balance(self.company_id.id, [account.id], this.date_from, 'posted')
                        if initial_bal:
                            balance += initial_bal[0]['start_credit'] - initial_bal[0]['start_debit']
                else:
                    account_ids = []
                    where = ' '
                    [account_ids.append(account.id) for account in report_option_id.account_ids]
                    if report_option_id.partner_ids:
                        partner_ids = []
                        [partner_ids.append(partner.id) for partner in report_option_id.partner_ids]
                        where = 'AND l.partner_id in %s' % tuple(partner_ids)
                    self._cr.execute("SELECT  sum(l.debit) AS debit, sum(l.credit) AS credit, at.type AS type "
                                        "FROM account_move_line l "
                                        "LEFT JOIN account_move m ON l.move_id = m.id " 
                                        "JOIN account_journal j ON l.journal_id = j.id " 
                                        "JOIN account_account aa ON l.account_id = aa.id " 
                                        "JOIN account_account_type at ON aa.user_type_id = at.id " 
                                        "WHERE m.date BETWEEN %s AND %s "
                                        "AND l.company_id = %s AND m.state = 'posted' "
                                        "AND l.account_id in %s "
                                        "GROUP BY at.type ", (this.date_from, this.date_to, self.company_id.id, tuple(account_ids)))
                    lines = self._cr.dictfetchall()
                    for line in lines:
                        if report_option_id.balance_type == 'balance':
                            if line['type'] in ('expense', 'asset'):
                                balance += line['debit'] - line['credit']
                            else:
                                balance += line['credit'] - line['debit']
                        elif report_option_id.balance_type == 'debit':
                            balance += line['debit']
                        elif report_option_id.balance_type == 'credit':
                            balance += line['credit']
            total_amount = balance
            if report_option_id.tax_percent > 0.0:
                return total_amount / 100 * report_option_id.tax_percent
            else:
                return total_amount

    def action_compute(self):
        report_option = self.env['account.tax.report.option'].search([('report', '=', 'ct30')])
        table1_ids = []
        if self.env['tax.ct30.table1'].search([('income_type', '=', 'line_1')]):
            line1 = self.env['tax.ct30.table1'].search([('income_type', '=', 'line_1')])
            line1.write({
                'income_type':'line_1',
                'income_amount':self.tax_tt02_id.v_22,
                'percentage':10,
                'tax_id':self.id,
                 })
        else:
            table1_line_1_id = self.env['tax.ct30.table1'].create(
                {
                'income_type':'line_1',
                'income_amount':self.tax_tt02_id.v_22,
                'percentage':10,
                'tax_id':self.id,
                 })
            table1_ids.append(table1_line_1_id.id)
            
        if self.env['tax.ct30.table1'].search([('income_type', '=', 'line_2')]):
            line2 = self.env['tax.ct30.table1'].search([('income_type', '=', 'line_2')]).write(
                {'income_type':'line_2',
                'income_amount':self.tax_tt02_id.v_23,
                'percentage':10,
                'tax_id':self.id, })
        else:
            table1_line_2_id = self.env['tax.ct30.table1'].create(
                {
                'income_type':'line_2',
                'income_amount':self.tax_tt02_id.v_23,
                'percentage':10,
                'tax_id':self.id,
                 })
            table1_ids.append(table1_line_2_id.id)
            
        if self.env['tax.ct30.table1'].search([('income_type', '=', 'line_3')]):
            line3 = self.env['tax.ct30.table1'].search([('income_type', '=', 'line_3')]).write(
                {'income_type':'line_3',
                'income_amount':self.tax_tt02_id.v_24,
                'percentage':30,
                'tax_id':self.id, })
        else:
            table1_line_3_id = self.env['tax.ct30.table1'].create(
                {
                'income_type':'line_3',
                'income_amount':self.tax_tt02_id.v_24,
                'percentage':30,
                'tax_id':self.id,
                 })
            table1_ids.append(table1_line_3_id.id)
            
        if self.env['tax.ct30.table1'].search([('income_type', '=', 'line_4')]):
            line4 = self.env['tax.ct30.table1'].search([('income_type', '=', 'line_4')]).write(
                {
                'income_type':'line_4',
                'income_amount':self.tax_tt02_id.v_25,
                'percentage':2,
                'tax_id':self.id, })
        else:
            table1_line_4_id = self.env['tax.ct30.table1'].create(
                {
                'income_type':'line_4',
                'income_amount':self.tax_tt02_id.v_25,
                'percentage':2,
                'tax_id':self.id,
                 })
            table1_ids.append(table1_line_4_id.id)
            
        if self.env['tax.ct30.table1'].search([('income_type', '=', 'line_5')]):
            line5 = self.env['tax.ct30.table1'].search([('income_type', '=', 'line_5')]).write(
                {
                'income_type':'line_5',
                'income_amount':self.tax_tt02_id.v_26,
                'tax_id':self.id, })
        else:
            table1_line_5_id = self.env['tax.ct30.table1'].create(
                {
                'income_type':'line_5',
                'income_amount':self.tax_tt02_id.v_26,
                'tax_id':self.id,
                 })
            table1_ids.append(table1_line_5_id.id)
            self.tax_table1_ids = table1_ids
            
        if not self.tax_table2_ids:
            table2_ids = []
            table2_line_1_id = self.env['tax.ct30.table2'].create(
                {
                'specification':'line_1',
                'tax_id':self.id,
                 })
            table2_ids.append(table2_line_1_id.id)
            table2_line_2_id = self.env['tax.ct30.table2'].create(
                {
                'specification':'line_2',
                'tax_id':self.id,
                 })
            table2_ids.append(table2_line_2_id.id)
            table2_line_3_id = self.env['tax.ct30.table2'].create(
                {
                'specification':'line_3',
                'tax_id':self.id,
                 })
            table2_ids.append(table2_line_3_id.id)     
            self.tax_table2_ids = table2_ids
            
        if not self.tax_table3_ids:
            table3_ids = []
            table3_line_1_id = self.env['tax.ct30.table3'].create(
                {
                'specification':'line_1',
                'tax_id':self.id,
                 })
            table3_ids.append(table3_line_1_id.id)
            table3_line_2_id = self.env['tax.ct30.table3'].create(
                {
                'specification':'line_2',
                'tax_id':self.id,
                 })
            table3_ids.append(table3_line_2_id.id)
            table3_line_3_id = self.env['tax.ct30.table3'].create(
                {
                'specification':'line_3',
                'tax_id':self.id,
                 })
            table3_ids.append(table3_line_3_id.id)     
            table3_line_4_id = self.env['tax.ct30.table3'].create(
                {
                'specification':'line_4',
                'tax_id':self.id,
                 })
            table3_ids.append(table3_line_4_id.id)     
            self.tax_table3_ids = table3_ids

        self.a_1_1_1 = self.compute_with_account(report_option.search([('name', 'ilike', '1.1 Хасагдахгүй зардал')]).id)
        self.a_1_1_2 = self.compute_with_account(report_option.search([('name', 'ilike', 'Засгийн газрын баталснаас бусад хэвийн хорогдол')]).id)
        self.a_1_1_4 = self.compute_with_account(report_option.search([('name', 'ilike', 'хандив, хувийн хэрэглээний зардал')]).id)
        self.a_2_1 = self.compute_with_account(report_option.search([('name', 'ilike', '2.1 Чөлөөлөх орлго')]).id)
        self.a_2_2 = self.compute_with_account(report_option.search([('name', 'ilike', '2.2 Хөнгөлөлт эдлэх орлого')]).id)
        self.a_2_3 = self.compute_with_account(report_option.search([('name', 'ilike', '2.3 Тусгайлсан хувь хэмжээтэй орлого')]).id)
        self.a_4_2 = self.compute_with_account(report_option.search([('name', 'ilike', '4.2 Валютын ханшийн зөрүүгийн бодит бус ашиг')]).id)
        self.v_3_2 = self.compute_with_account(report_option.search([('name', 'ilike', '3.2 Валютын ханшийн зөрүүгийн бодит бус алдагдал')]).id)

    @api.multi
    def action_export(self):
        company = self.company_id
        company_dict = {'ttd0': '', 'ttd1': '', 'ttd2': '', 'ttd3': '', 'ttd4': '', 'ttd5': '', 'ttd6': '',
                        'company_name': u'2. Нэр: %s' % (company.name), 'basic_operation_name':'', 'basic_operation_code': '',
                        'date_str':'%s - %s' % (datetime.strptime(self.date_from, '%Y-%m-%d').strftime('%Y.%m.%d'), datetime.strptime(self.date_to, '%Y-%m-%d').strftime('%Y.%m.%d'))}
        if company.vat:
            for i in range(0, len(company.vat)):
                company_dict.update({'ttd%s' % (i):company.vat[i]})

        table1 = []
        for line in self.tax_table1_ids:
            table1.append({line.income_type:{
                        'type':dict(INCOME_TYPE_SELECTION)[line.income_type] or '',
                        'from':line.from_where or 0,
                        'income_amount':line.income_amount or 0,
                        'recieved_income':line.recieved_income or 0
                        }
                })
        table1 = sorted(table1)
        table2 = []
        for line in self.tax_table2_ids:
            table2.append({line.specification:{
                    'specification':dict(SPECIFICATION_1)[line.specification] or '',
                    'account_amount':line.account_amount or 0,
                    'tax_basic':line.tax_basic or 0,
                    'deferred_tax_liablities':line.deferred_tax_liablities or 0,
                    'deferred_tax_assets':line.deferred_tax_assets or 0,
                    }
                })
        table2 = sorted(table2)
        table3 = []
        for line in self.tax_table3_ids:
            table3.append({line.specification:{
                    'specification':dict(SPECIFICATION_2)[line.specification] or '',
                    'first_balance':line.first_balance or 0,
                    'add':line.add or 0,
                    'sub':line.sub or 0,
                    'last_balance':line.last_balance or 0,
                    'category':line.category or 0,
                    }
                })
        table3 = sorted(table3)
        dicts = {
                 'company_name': u' %s' % company.name,
                 'date_str': '%s - %s' % (self.date_from, self.date_to),
                 '{a_1}': round(self.a_1, 2),
                 '{a_1_1_1}': self.a_1_1_1,
                 '{a_1_1_2}': self.a_1_1_2,
                 '{a_1_1_3}': self.a_1_1_3,
                 '{a_1_1_4}': self.a_1_1_4,
                 '{a_1_2}': self.a_1_2,
                 '{a_1_3}': self.a_1_3,
                 '{a_1_4}': self.a_1_4,
                 '{a_1_5}': self.a_1_5,
                 '{a_1_6}': self.a_1_6,
                 '{a_1_amount}':self.a_1_amount,
                 '{a_2_1}':self.a_2_1 ,
                 '{a_2_2}':self.a_2_2 ,
                 '{a_2_3}':self.a_2_3 ,
                 '{a_2_4}':self.a_2_4 ,
                 '{a_2_5}':self.a_2_5 ,
                 '{a_2_amount}':self.a_2_amount ,
                 '{b}':self.b ,
                 '{b_1}':self.b_1 ,
                 '{b_2}':self.b_2 ,
                 '{b_3}':self.b_3 ,
                 '{v_3_1}':self.v_3_1 ,
                 '{v_3_2}':self.v_3_2 ,
                 '{v_3_3}':self.v_3_3 ,
                 '{v_3_amount}':self.v_3_amount ,
                 '{v_4_1}':self.v_4_1 ,
                 '{v_4_2}':self.v_4_2 ,
                 '{v_4_3}':self.v_4_3 ,
                 '{v_4_amount}':self.v_4_amount ,
                 '{v_5}':self.v_5 ,
                 '{v_5_1}':self.v_5_1 ,
                 '{v_5_2}':self.v_5_2 ,
                 '{v_6}':self.v_6 ,
                 '{v_7}':self.v_7 ,
                 '{v_8_amount}':self.v_8_amount ,
                 '{v_8_1}':self.v_8_1 ,
                 '{v_8_2}':self.v_8_2 ,
                 '{v_8_1}':self.v_8_1 ,
                 '{v_8_2}':self.v_8_2 ,
                 '{v_8_1}':self.v_8_1 ,
                 '{v_8_2}':self.v_8_2 ,
                 '{v_9}':self.v_9 ,
                 '{v_9_1}':self.v_9_1 ,
                 '{v_9_2}':self.v_9_2 ,
                 '{v_10_a}':self.v_10_a ,
                 '{v_10_b}':self.v_10_b ,
                 '{v_11}':self.v_11 ,
                 '{v_12}':self.v_12 ,
                 
                '{tb1_1_2}':table1[0]['line_1']['from'] or 0,
                '{tb1_1_1}':table1[0]['line_1']['income_amount'] or 0,
                '{tb1_1_3}':table1[0]['line_1']['recieved_income'] or 0,
                
                '{tb1_2_2}':table1[1]['line_2']['from'] or 0,
                '{tb1_2_1}':table1[1]['line_2']['income_amount'] or 0,
                '{tb1_2_3}':table1[1]['line_2']['recieved_income'] or 0,
                
                '{tb1_3_2}':table1[2]['line_3']['from'] or 0,
                '{tb1_3_1}':table1[2]['line_3']['income_amount'] or 0,
                '{tb1_3_3}':table1[2]['line_3']['recieved_income'] or 0,
                
                '{tb1_4_2}':table1[3]['line_4']['from'] or 0,
                '{tb1_4_1}':table1[3]['line_4']['income_amount'] or 0,
                '{tb1_4_3}':table1[3]['line_4']['recieved_income'] or 0,
                
                '{tb1_5_2}':table1[4]['line_5']['from'] or 0,
                '{tb1_5_1}':table1[4]['line_5']['income_amount'] or 0,
                '{tb1_5_3}':table1[4]['line_5']['recieved_income'] or 0,

                '{tb2_1_specification}':            table2[0]['line_1']['specification'] or 0,
                '{tb2_1_account_amount}':           table2[0]['line_1']['account_amount'] or 0,
                '{tb2_1_tax_basic}':                table2[0]['line_1']['tax_basic'] or 0,
                '{tb2_1_deferred_tax_liablities}':  table2[0]['line_1']['deferred_tax_liablities'] or 0,
                '{tb2_1_deferred_tax_assets}':      table2[0]['line_1']['deferred_tax_assets'] or 0,
                
                '{tb2_2_specification}':            table2[1]['line_2']['specification'] or 0,
                '{tb2_2_account_amount}':           table2[1]['line_2']['account_amount'] or 0,
                '{tb2_2_tax_basic}':                table2[1]['line_2']['tax_basic'] or 0,
                '{tb2_2_deferred_tax_liablities}':  table2[1]['line_2']['deferred_tax_liablities'] or 0,
                '{tb2_2_deferred_tax_assets}':      table2[1]['line_2']['deferred_tax_assets'] or 0,
                
                '{tb2_3_specification}':            table2[2]['line_3']['specification'] or 0,
                '{tb2_3_account_amount}':           table2[2]['line_3']['account_amount'] or 0,
                '{tb2_3_tax_basic}':                table2[2]['line_3']['tax_basic'] or 0,
                '{tb2_3_deferred_tax_liablities}':  table2[2]['line_3']['deferred_tax_liablities'] or 0,
                '{tb2_3_deferred_tax_assets}':      table2[2]['line_3']['deferred_tax_assets'] or 0,
              
                '{tb3_1_first_balance}':table3[0]['line_1']['first_balance'] or 0,
                '{tb3_1_add}':          table3[0]['line_1']['add'] or 0,
                '{tb3_1_sub}':          table3[0]['line_1']['sub'] or 0,
                '{tb3_1_last_balance}': table3[0]['line_1']['last_balance'] or 0,
                '{tb3_1_category}':     table3[0]['line_1']['category'] or 0,
                
                '{tb3_2_first_balance}':table3[1]['line_2']['first_balance'] or 0,
                '{tb3_2_add}':          table3[1]['line_2']['add'] or 0,
                '{tb3_2_sub}':          table3[1]['line_2']['sub'] or 0,
                '{tb3_2_last_balance}': table3[1]['line_2']['last_balance'] or 0,
                '{tb3_2_category}':     table3[1]['line_2']['category'] or 0,
                
                '{tb3_3_first_balance}':table3[2]['line_3']['first_balance'] or 0,
                '{tb3_3_add}':          table3[2]['line_3']['add'] or 0,
                '{tb3_3_sub}':          table3[2]['line_3']['sub'] or 0,
                '{tb3_3_last_balance}': table3[2]['line_3']['last_balance'] or 0,
                '{tb3_3_category}':     table3[2]['line_3']['category'] or 0,
                
                '{tb3_4_first_balance}':table3[3]['line_4']['first_balance'] or 0,
                '{tb3_4_add}':          table3[3]['line_4']['add'] or 0,
                '{tb3_4_sub}':          table3[3]['line_4']['sub'] or 0,
                '{tb3_4_last_balance}': table3[3]['line_4']['last_balance'] or 0,
                '{tb3_4_category}':     table3[3]['line_4']['category'] or 0,
                'province_district_id':company.province_district_id.name if company.province_district_id else '',
                'sum_khoroo_id':company.sum_khoroo_id.name if company.sum_khoroo_id.name else '',
                'sum_khoroo_id_code':company.sum_khoroo_id.code if company.sum_khoroo_id.code else '',
                'street':company.street if company.street else '',
                'door_number':company.door_number if company.door_number else '',
                'date_str':'%s - %s' % (datetime.strptime(self.date_from, '%Y-%m-%d').strftime('%Y.%m.%d'), datetime.strptime(self.date_to, '%Y-%m-%d').strftime('%Y.%m.%d')),
                'phone':u'Утас: ' + company.phone if company.phone else '',
                'fax':u'Факс: ' + company.fax if company.fax else '',
                'email':u'E-mail хаяг: ' + company.email if company.email else '',
                'company_name':u'2. Нэр: ' + company.name
                 }

        template_file = get_module_resource('l10n_mn_account_tax_report', 'static/src/report', 'CT30-input.xlsx')
        generate_file = get_module_resource('l10n_mn_account_tax_report', 'static/src/report', 'CT30-output.xlsx')

        wb = openpyxl.load_workbook(template_file)
        sheet = wb.active
        rows = sheet.rows
        for row in rows:
            for cell in row:
                if dicts.has_key(cell.value):
                    cell.value = dicts[cell.value]
        wb.save(generate_file)

        file_name = "CT-30_%s.xlsx" % (time.strftime('%Y%m%d_%H%M'),)
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix='CT-30', form_title=file_name).create({})
        image = open(generate_file, 'rb')
        image_read = image.read()
        report_excel_output_obj.filedata = base64.encodestring(image_read)
        return report_excel_output_obj.export_report()

