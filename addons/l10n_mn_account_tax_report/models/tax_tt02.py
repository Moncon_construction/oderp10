# -*- encoding: utf-8 -*-
##############################################################################
from odoo import api, fields, models, _
from odoo.modules import get_module_resource
from datetime import datetime
import json
import openpyxl
from io import BytesIO
from operator import itemgetter
import time
import base64
from odoo.exceptions import UserError

"""
   АЖ АХУЙ НЭГЖИЙН ОРЛОГЫН АЛБАН ТАТВАРЫН ТАЙЛАНД ЗОРИУЛСАН БҮРТГЭЛ
"""


class TaxCT30Report(models.Model):
    _name = "tax.tt02"
    _description = "Model for Tax Report TT-02"

    name = fields.Char(u'Name')
    tax_ct30_id = fields.Many2one('tax.ct30', u'Зөрүүгийн тайлан СТ-30')
    company_id = fields.Many2one('res.company', string=u'Компани', required=True, readonly=True,
                                 default=lambda self: self.env.user.company_id)
    fiscalyear_id = fields.Many2one('account.fiscalyear', u'Санхүүгийн жил', required=True)
    date_from = fields.Date(string=u'Эхлэл огноо', required=True)
    date_to = fields.Date(string=u'Дуусах огноо', required=True)
    state = fields.Selection([('draft', u'Ноорог'),
                              ('confirmed', u'Батлагдсан')], u'Төлөв', required=True, default='draft')

    def _compute_a_1(self):
        for obj in self:
            obj.a_1 = obj.a_1_1 + obj.a_1_2 + obj.a_1_3 + obj.a_1_4 + obj.a_1_5

    def _compute_a_1_2(self):
        for obj in self:
            obj.a_1_2 = obj.b_16 + obj.b_17 + obj.b_18 + obj.b_19 + obj.b_20a + obj.b_20_b

    a_1 = fields.Float(string=u'A.1. Нийт борлуулалтын орлогын дүн (мөр 2+3+4+5+6)', compute=_compute_a_1,
                       readonly=True)
    a_1_1 = fields.Float(string=u'1.1. Татвараас чөлөөлөгдөх орлогын дүн (ТТ-02б маягтын А хэсгийн дүн)')
    a_1_2 = fields.Float(
        string=u'1.2. Тусгай хувь хэмжээгээр татвар ногдох орлогын дүн (Хүснэгт Б, мөр 32+34+39+41+43+47)',
        compute=_compute_a_1_2, readonly=True)

    def _compute_a_1_3(self):
        for obj in self:
            obj.a_1_3 = obj.v_22 + obj.v_23 + obj.v_24 + obj.v_25 + obj.v_26

    a_1_3 = fields.Float(
        string=u'1.3.Хуулийн дагуу бусдад татвар суутгуулсан орлогын дүн (Хүснэгт В, мөр 50+52+54+56+58)',
        compute=_compute_a_1_3)
    a_1_4 = fields.Float(string=u'1.4. Бусад орлогын дүн (Гадаад валютын ханшийн зөрүүгийн бодит бус орлого)')

    def _compute_a_1_5(self):
        for obj in self:
            obj.a_1_5 = obj.a_1_6 + obj.a_1_7 + obj.a_1_8 + obj.a_1_9 + obj.a_1_10 + obj.a_1_11 + obj.a_1_12 + obj.a_1_13 + obj.a_1_14 + obj.a_1_15 + obj.a_1_16

    a_1_5 = fields.Float(string=u'1.5. Нийтлэг хувь хэмжээгээр татвар ногдох орлогын дүн /мөр 7+8+...+17/',
                         compute=_compute_a_1_5)
    a_1_6 = fields.Float(string=u'1.6. Үндсэн үйлдвэрлэл, ажил үйлчилгээний борлуулалтын орлого ', readonly=True)
    a_1_7 = fields.Float(string=u'1.7. Туслах үйлдвэрлэл, ажил үйлчилгээний борлуулалтын орлого')
    a_1_8 = fields.Float(string=u'1.8. Хувьцаа, үнэт цаас борлуулсны орлого')
    a_1_9 = fields.Float(string=u'1.9. Үнэ төлбөргүйгээр бусдаас авсан бараа, ажил, үйлчилгээ')
    a_1_10 = fields.Float(string=u'1.10. Биет бус хөрөнгө борлуулсны орлого')
    a_1_11 = fields.Float(string=u'1.11. Техникийн, удирдлагын зөвлөх болон бусад үйлчилгээний орлого')
    a_1_12 = fields.Float(
        string=u'1.12. Гэрээгээр хүлээсэн үүргээ биелүүлээгүй этгээдээс авсан хүү, анз/торгууль, алданги/, хохирлын нөхөн төлбөрийн орлого',
        readonly=True)
    a_1_13 = fields.Float(string=u'1.13. Гадаад валютын ханшны зөрүүгийн бодит орлого', readonly=True)
    a_1_14 = fields.Float(string=u'1.14. Хөдлөх болон үл хөдлөх эд хөрөнгийн түрээсийн орлого', readonly=True)
    a_1_15 = fields.Float(string=u'1.15. Хөдлөх эд хөрөнгө борлуулсны орлого')
    a_1_16 = fields.Float(string=u'1.16. Албан татвар ногдох бусад орлого', readonly=True)
    a_2 = fields.Float(string=u'2. Борлуулсан бүтээгдэхүүний өртөг', readonly=True)
    a_3 = fields.Float(string=u'3. Удирдлагын болон борлуулалтын үйл ажиллагааны зардал ', readonly=True)
    a_4 = fields.Float(string=u'4. Үндсэн бус үйл ажиллагааны  алдагдал')

    # Санхүүгийн тайлангийн татварын өмнөх ашгийн дүн талбарын утгыг авдаг талбар
    def _compute_a_5(self):
        for obj in self:
            obj.a_5 = obj.a_1 - obj.a_2 - obj.a_3 - obj.a_4

    a_5 = fields.Float(string=u'5.Татварын өмнөх ашиг + , алдагдал - (мөр 1-18-19-20)', compute=_compute_a_5)

    # Хуульд заасан татвар ногдох орлогоос хасагдахгүй зардлын дүн буюу 
    # “Санхүүгийн болон орлогын албан татварын тайлангийн үзүүлэлт хоорондын 
    # зөрүүг зохицуулах тайлан”-гийн А1, В3 мөрийн нийлбэр дүн /татварын өмнөх ашгийг нэмэгдүүлэх дүн/
    def _compute_a_6(self):
        for obj in self:
            obj.a_6 = obj.tax_ct30_id.a_1_amount + obj.tax_ct30_id.v_3_amount

    a_6 = fields.Float(
        string=u'6. Хуульд заасан татвар ногдох орлогоос хасагдахгүй зардлын дүн буюу "Санхүүгийн болон орлогын албан татварын тайлангийн үзүүлэлт хоорондын зөрүүг зохицуулах тайлан"-гийн A1, B3 мөрийн нийлбэр дүн /татварын өмнөх ашгийг нэмэгдүүлэх дүн/',
        compute=_compute_a_6)

    # Мөр 23. 7. “Санхүүгийн болон орлогын албан татварын тайлангийн үзүүлэлт хоорондын зөрүүг 
    # зохицуулах тайлан”-гийн А2, В4 мөрийн дүн буюу татвар ногдуулах орлогыг бууруулах дүн
    def _compute_a_7(self):
        for obj in self:
            obj.a_7 = obj.tax_ct30_id.a_2_amount + obj.tax_ct30_id.v_4_amount

    a_7 = fields.Float(
        string=u'7. “Санхүүгийн болон орлогын албан татварын тайлангийн үзүүлэлт хоорондын зөрүүг зохицуулах тайлан"-ийн А2, В4 мөрийн дүн буюу татвар төлөхийн өмнөх ашгийг бууруулах дүн',
        compute=_compute_a_7)

    def _compute_a_8(self):
        for obj in self:
            obj.a_8 = obj.a_5 + obj.a_6 - obj.a_7

    a_8 = fields.Float(string=u'8. Татвар ногдуулах орлогын дүн (мөр 21+22-23)', compute=_compute_a_8)

    def _compute_a_9(self):
        for obj in self:
            obj.a_9 = obj.tax_ct30_id.v_5_1

    a_9 = fields.Float(
        string=u'9. Сайн дурын даатгалын хураамжийн хэтрэлт (“Санхүүгийн болон орлогын албан татварын тайлангийн үзүүлэлт хоорондын зөрүүг зохицуулах тайлан”-гийн 5.1 дэх мөрийн дүн)',
        compute=_compute_a_9)

    def _compute_a_10(self):
        for obj in self:
            obj.a_10 = obj.a_8 + obj.a_9

    a_10 = fields.Float(string=u'10. Зохицуулагдсан татвар ногдуулах орлогын дүн (мөр 24+25)', compute=_compute_a_10)
    a_11 = fields.Float(
        string=u'11.Өмнөх жилүүдийн татварын тайлангаар гарсан татварын албаар баталгаажуулсан алдагдлаас тайлант хугацаанд шилжүүлсэн дүн (Маягт ТТ-02в, А  хүснэгтийн 3 дахь хэсгийн дүн)')

    def _compute_a_12(self):
        for obj in self:
            obj.a_12 = obj.a_10 - obj.a_11

    a_12 = fields.Float(string=u'12.Нийтлэг хувь хэмжээгээр татвар ногдуулах орлогын дүн (мөр 26-27)',
                        compute=_compute_a_12)

    def _compute_a_13(self):
        for obj in self:
            if obj.a_12 < 0:
                obj.a_13 = obj.a_12 * 0
            elif 0 < obj.a_12 < 3000000000:
                obj.a_13 = obj.a_12 * 0.1
            elif obj.a_12 > 3000000000:
                obj.a_13 = 300000000 + (obj.a_12 - 3000000000) * 0.25

    a_13 = fields.Float(string=u'13. Ногдуулсан татвар (мөр 28 * хуулийн 17.1-д заасан хувиар)', compute=_compute_a_13)

    a_14 = fields.Float(string=u'14. Хөнгөлөгдөх татварын дүн  (ТТ-02б маягтын Б хэсгийн дүн)')

    def _compute_a_15(self):
        for obj in self:
            obj.a_15 = obj.a_13 - obj.a_14

    a_15 = fields.Float(string=u'15 Нийтлэг хувь хэмжээгээр төлбөл зохих албан татвар (мөр 29-30)',
                        compute=_compute_a_15)

    b = fields.Float(string=u'Б. Тусгай хувь хэмжээгээр ногдуулах татварын тооцоолол:')

    def _compute_b_16_1(self):
        for obj in self:
            obj.b_16_1 = obj.b_16 * 0.4

    b_16 = fields.Float(
        string=u'16. Эротик хэвлэл, ном зохиол, дүрс бичлэг худалдсан буюу төлбөртэй ашиглуулсан, эротик тоглолт явуулсан үйлчилгээний орлого')
    b_16_1 = fields.Float(
        string=u'Эротик хэвлэл, ном зохиол, дүрс бичлэг худалдсан буюу төлбөртэй ашиглуулсан, эротик тоглолт явуулсан үйлчилгээний орлогод ногдуулсан татвар (36х40%)',
        compute=_compute_b_16_1)

    b_17 = fields.Float(string=u'17.Төлбөрт таавар, бооцоот тоглоом, эд мөнгөний хонжворт сугалааны орлого')
    b_17_1 = fields.Float(string=u'Баримтаар нотлогдох зардал')
    b_17_2 = fields.Float(string=u'Хонжворт олгосон мөнгө болон барааны үнэ')

    def _compute_b_17_3(self):
        for obj in self:
            obj.b_17_3 = obj.b_17 - obj.b_17_1 - obj.b_17_2

    b_17_3 = fields.Float(string=u'Татвар ногдуулах орлого (34-35-36)', compute=_compute_b_17_3)

    def _compute_b_17_4(self):
        for obj in self:
            obj.b_17_4 = obj.b_17_3 * 0.4

    b_17_4 = fields.Float(string=u'Ногдуулсан татвар (37x40%)', compute=_compute_b_17_4)

    def _compute_b_18_1(self):
        for obj in self:
            obj.b_18_1 = obj.b_18 * 0.1

    b_18 = fields.Float(string=u'18.Хүүгийн орлого', readonly=True)
    b_18_1 = fields.Float(string=u'Хүүгийн орлогод ногдуулсан татвар (39 x 10%)', readonly=True,
                          compute=_compute_b_18_1)

    b_19 = fields.Float(string=u'19.Үл хөдлөх эд хөрөнгө борлууласны орлого')

    def _compute_b_19_1(self):
        for obj in self:
            obj.b_19_1 = obj.b_19 * 0.02

    b_19_1 = fields.Float(string=u'Үл хөдлөх эд хөрөнгө борлууласны орлогод ногдуулсан татвар (41 x 2%)',
                          compute=_compute_b_19_1)

    b_20a = fields.Float(
        string=u'20а. Давхар татварын гэрээтэй гадаад улсад олсон тухайн гэрээнд заасны дагуу Монгол Улсад татвар ногдуулах ногдол ашиг, хүүгийн орлого')
    b_20a_1 = fields.Float(
        string=u'ААНОАТ-ын хуульд заасан хувиар ногдуулах албан татварын дүн (мөр 43*хуульд зааснаар)')
    b_20a_2 = fields.Float(
        string=u'Давхар татварын гэрээний заасан хувиар гадаад улсад суутгуулсан албан татварын дүн  (мөр 43 x гэрээнд заасан хувиар)')

    def _compute_b_20a_3(self):
        for obj in self:
            obj.b_20a_3 = obj.b_20a_1 - obj.b_20a_2

    b_20a_3 = fields.Float(
        string=u'Давхар татварын гэрээтэй гадаад улсад олсон орлогоос Монгол улсад төлбөл зохих албан татварын  дүн (мөр 44-45)',
        compute=_compute_b_20a_3)

    b_20_b = fields.Float(
        string=u'20б. Давхар татварын гэрээгүй гадаад улсад олсон ААНОАТ-ын хуульд заасан тусгайлсан хувь хэмжээгээр албан татвар ногдуулах орлого ')
    b_20_b_1 = fields.Float(
        string=u'ААНОАТ-ын хуульд заасан хувиар ногдуулах албан татварын дүн (мөр 47  x  хуульд  заасан хувиар)')

    def _compute_b_21(self):
        for obj in self:
            obj.b_21 = obj.b_16_1 + obj.b_17_4 + obj.b_18_1 + obj.b_19_1 + obj.b_20a_3 + obj.b_20_b_1

    b_21 = fields.Float(string=u'21. ТУСГАЙ ХУВЬ ХЭМЖЭЭГЭЭР ТӨЛБӨЛ ЗОХИХ АЛБАН ТАТВАРЫН ДҮН ', compute=_compute_b_21)

    v = fields.Float(string=u'В. Хуулийн дагуу бусдад суутгуулсан татварын тооцоолол:')

    v_22 = fields.Float(string=u'22.Ногдол ашгийн орлого')

    def _compute_v_22_1(self):
        for obj in self:
            obj.v_22_1 = obj.v_22 * 0.1

    v_22_1 = fields.Float(string=u'Ногдол ашгийн орлогод суутгуулсан татварын хувь ( мөр 50*10 хувь)',
                          compute=_compute_v_22_1)

    v_23 = fields.Float(string=u'23. Эрхийн шимтгэлийн орлого')

    def _compute_v_23_1(self):
        for obj in self:
            obj.v_23_1 = obj.v_23 * 0.1

    v_23_1 = fields.Float(string=u'Эрхийн шимтгэлийн  орлогод суутгуулсан татварын хувь ( мөр 52*10 хувь)',
                          compute=_compute_v_23_1)

    v_24 = fields.Float(string=u'24. Эрх борлуулсаны орлого')

    def _compute_v_24_1(self):
        for obj in self:
            obj.v_24_1 = obj.v_24 * 0.3

    v_24_1 = fields.Float(string=u'Эрх борлуулсаны  орлогод суутгуулсан татварын хувь ( мөр 54*30 хувь)',
                          compute=_compute_v_24_1)

    v_25 = fields.Float(string=u'25. Үл хөдлөх эд хөрөнгө борлуулсны орлого')

    def _compute_v_25_1(self):
        for obj in self:
            obj.v_25_1 = obj.v_25 * 0.02

    v_25_1 = fields.Float(string=u'Үл хөдлөх эд хөрөнгө борлуулсны орлогод ногдуулсан татвар (мөр 56х2% )',
                          compute=_compute_v_25_1)

    v_26 = fields.Float(string=u'26. Төлбөрт тоглоом, бооцоот тоглоом, эд мөнгөний хонжворт сугалааны орлого')

    def _compute_v_26_1(self):
        for obj in self:
            obj.v_26_1 = obj.v_26 * 0.4

    v_26_1 = fields.Float(
        string=u'Төлбөрт тоглоом, бооцоот тоглоом, эд мөнгөний хонжворт сугалааны орлогод суутгуулсан татвар / мөр 58*40% /',
        compute=_compute_v_26_1)

    def _compute_v_27(self):
        for obj in self:
            obj.v_27 = obj.v_22_1 + obj.v_23_1 + obj.v_24_1 + obj.v_25_1 + obj.v_26_1

    v_27 = fields.Float(string=u'27. ХУУЛИЙН ДАГУУ БУСДАД СУУТГУУЛСАН АЛБАН ТАТВАРЫН ДҮН (мөр 51+53+55+57+59)',
                        compute=_compute_v_27)

    def _compute_v_28(self):
        for obj in self:
            obj.v_28 = obj.a_15 + obj.b_21

    v_28 = fields.Float(
        string=u'28. НИЙТ ТӨЛБӨЛ ЗОХИХ ТАТВАРЫН ДҮН (Хүснэгт А, мөр 31+ХүснэгтБ, мөр49+ МаягтТТ-13, мөр 24)',
        compute=_compute_v_28)

    g = fields.Float(string=u'Г.Татварын тооцоолол')

    g_1_1 = fields.Float(string=u'Тайлангийн эхний үлдэгдэл Илүү' )
    g_1_2 = fields.Float(string=u'Тайлангийн эхний үлдэгдэл Дутуу')

    g_2_1 = fields.Float(string=u'Тайлант хугацаанд ногдуулсан татвар /мөр 59/ Илүү')

    def _compute_g_2_2(self):
        for obj in self:
            if obj.v_28 >= 0:
                obj.g_2_2 = obj.v_28
            elif obj.v_28 < 0:
                obj.g_2_1 = obj.v_28


    g_2_2 = fields.Float(string=u'Тайлант хугацаанд ногдуулсан татвар /мөр 59/ Дутуу', compute=_compute_g_2_2)

    g_3 = fields.Float(string=u'Тайлант хугацаанд дансаар төлсөн/Гүйцэтгэл/')

    g_4 = fields.Float(string=u'Татварын алба болон татвар төлөгч хоорондын тооцоогоор төлсөн')

    g_5 = fields.Float(string=u'Тайлант хугацаанд төсвөөс буцаан авсан')

    g_6_1 = fields.Float(string=u'Тайлант хугацаанд хүлээн авсан үлдэгдэл Илүү')
    g_6_2 = fields.Float(string=u'Тайлант хугацаанд хүлээн авсан үлдэгдэл Дутуу')

    g_7_1 = fields.Float(string=u'Тайлант хугацаанд шилжүүлсэн үлдэгдэл Илүү')
    g_7_2 = fields.Float(string=u'Тайлант хугацаанд шилжүүлсэн үлдэгдэл Дутуу')

    g_8 = fields.Float(string=u'Тайлант хугацаанд хүчингүй болсон')

    g_9_1 = fields.Float(string=u'Суутган тооцоолсон Илүү')
    g_9_2 = fields.Float(string=u'Суутган тооцоолсон Дутуу')

    # Татварын тооцоолол хэсгийн мөр 1+3+5+6+8+11+12+13г тооцоолж тавигдана
    def _compute_g_10_1(self):
        for obj in self:
            if 0 > (obj.g_1_2 + obj.g_2_2 + obj.g_5 + obj.g_6_2 + obj.g_7_1 + obj.g_9_2) - (obj.g_1_1+ obj.g_2_1+ obj.g_3+ obj.g_4+obj.g_6_1+obj.g_7_2+obj.g_8+obj.g_9_1):
                obj.g_10_1 = -((obj.g_1_2 + obj.g_2_2 + obj.g_5 + obj.g_6_2 + obj.g_7_1 + obj.g_9_2) - (obj.g_1_1+ obj.g_2_1+ obj.g_3+ obj.g_4+obj.g_6_1+obj.g_7_2+obj.g_8+obj.g_9_1))

    g_10_1 = fields.Float(string=u'Тайлангийн эцсийн үлдэгдэл Илүү', compute=_compute_g_10_1)

    # Татварын тооцоолол хэсгийн мөр 2+4+7+9+10+14 г тооцоолж тавигдана
    def _compute_g_10_2(self):
        for obj in self:
            if 0 < (obj.g_1_2 + obj.g_2_2 + obj.g_5 + obj.g_6_2 + obj.g_7_1 + obj.g_9_2) - (obj.g_1_1+ obj.g_2_1+ obj.g_3+ obj.g_4+obj.g_6_1+obj.g_7_2+obj.g_8+obj.g_9_1):
                obj.g_10_2 = (obj.g_1_2 + obj.g_2_2 + obj.g_5 + obj.g_6_2 + obj.g_7_1 + obj.g_9_2) - (obj.g_1_1+ obj.g_2_1+ obj.g_3+ obj.g_4+obj.g_6_1+obj.g_7_2+obj.g_8+obj.g_9_1)
    g_10_2 = fields.Float(string=u'Тайлангийн эцсийн үлдэгдэл Дутуу', compute=_compute_g_10_2)

    def action_draft(self):
        self.write({'state': 'draft'})

    def action_confirm(self):
        self.write({'state': 'confirmed'})

    @api.multi
    def compute_with_account(self, obj):
        for this in self:
            report_option_id = self.env['account.tax.report.option'].search([('id', '=', obj)])
            total_amount = 0.0
            total_first_balance_amount = 0.0
            total_debit_amount = 0.0
            total_credit_amount = 0.0
            balance = 0.0
            if report_option_id.account_ids:
                if report_option_id.end_balance:
                    for account in report_option_id.account_ids:
                        initial_bal = self.env['account.move.line'].get_initial_balance(self.company_id.id,
                                                                                        [account.id], this.date_from,
                                                                                        'posted')
                        if initial_bal:
                            balance += initial_bal[0]['start_credit'] - initial_bal[0]['start_debit']
                else:
                    account_ids = []
                    where = ' '
                    [account_ids.append(account.id) for account in report_option_id.account_ids]
                    if report_option_id.partner_ids:
                        partner_ids = []
                        [partner_ids.append(partner.id) for partner in report_option_id.partner_ids]
                        where = 'AND l.partner_id in %s' % tuple(partner_ids)
                    self._cr.execute("SELECT  sum(l.debit) AS debit, sum(l.credit) AS credit, at.type AS type "
                                     "FROM account_move_line l "
                                     "LEFT JOIN account_move m ON l.move_id = m.id "
                                     "JOIN account_journal j ON l.journal_id = j.id "
                                     "JOIN account_account aa ON l.account_id = aa.id "
                                     "JOIN account_account_type at ON aa.user_type_id = at.id "
                                     "WHERE m.date BETWEEN %s AND %s "
                                     "AND l.company_id = %s AND m.state = 'posted' "
                                     "AND l.account_id in %s "
                                     "GROUP BY at.type ",
                                     (this.date_from, this.date_to, self.company_id.id, tuple(account_ids)))
                    lines = self._cr.dictfetchall()
                    for line in lines:
                        if report_option_id.balance_type == 'balance':
                            if line['type'] in ('expense', 'asset'):
                                balance += line['debit'] - line['credit']
                            else:
                                balance += line['credit'] - line['debit']
                        elif report_option_id.balance_type == 'debit':
                            balance += line['debit']
                        elif report_option_id.balance_type == 'credit':
                            balance += line['credit']

            total_amount = balance
            if report_option_id.tax_percent > 0.0:
                return total_amount / 100 * report_option_id.tax_percent
            else:
                return total_amount

            if report_option_id.code == 'TT02OPT_1_2':
                return this.b_16 + this.b_17 + this.b_18 + this.b_19 + this.b_20a + this.b_20_b
            elif report_option_id.code == 'TT02OPT_12':
                return this.a_10 - this.a_11
            elif report_option_id.code == 'TT02OPT_18_1':
                return this.b_18 / 100 * 10
            elif report_option_id.code == 'TT02OPT_28':
                return this.a_15 + this.b_21

    def action_compute(self):
        self.b_18_1 = self.b_20a_1 * 0.1
        report_option = self.env['account.tax.report.option'].search([('report', '=', 'tt02')])
        self.a_1_2 = self.compute_with_account(
            report_option.search([('name', 'ilike', 'Тусгай хувь хэмжээгээр татвар ногдох орлогын дүн')]).id)
        self.a_1_6 = self.compute_with_account(
            report_option.search([('name', 'ilike', 'Үндсэн үйлдвэрлэл, ажил үйлчилгээний борлуулалтын орлого')]).id)
        self.a_1_12 = self.compute_with_account(report_option.search([('name', 'ilike',
                                                                       'Гэрээгээр хүлээсэн үүргээ биелүүлээгүй этгээдээс авсан хүү, анз/торгууль, алданги/, хохирлын нөхөн төлбөрийн орлого')]).id)

        a_1_13_amount = self.compute_with_account(
            report_option.search([('name', 'ilike', 'Гадаад валютын ханшны зөрүүгийн бодит орлого')]).id)

        if a_1_13_amount:
            if a_1_13_amount < 0:
                self.a_1_13 = a_1_13_amount

        self.a_1_14 = self.compute_with_account(
            report_option.search([('name', 'ilike', 'Хөдлөх болон үл хөдлөх эд хөрөнгийн түрээсийн орлого')]).id)
        self.a_1_16 = self.compute_with_account(
            report_option.search([('name', 'ilike', 'Албан татвар ногдох бусад орлого')]).id)
        self.a_2 = self.compute_with_account(
            report_option.search([('name', 'ilike', 'Борлуулсан бүтээгдэхүүний өртөг')]).id)
        self.a_3 = self.compute_with_account(
            report_option.search([('name', 'ilike', 'Удирдлагын болон борлуулалтын үйл ажиллагааны зардал')]).id)
        self.a_4 = self.compute_with_account(
            report_option.search([('name', 'ilike', 'Үндсэн бус үйл ажиллагааны алдагдал')]).id)
        self.a_1_1 = self.compute_with_account(report_option.search(
            [('name', 'ilike', 'Татвараас чөлөөлөгдөх орлогын дүн (ТТ-02б маягтын А хэсгийн дүн')]).id)
        self.v_22 = self.compute_with_account(report_option.search([('name', 'ilike', 'Ногдол ашгийн орлого/')]).id)
        self.v_23 = self.compute_with_account(report_option.search([('name', 'ilike', 'Эрхийн шимтгэлийн орлого/')]).id)
        self.v_24 = self.compute_with_account(report_option.search([('name', 'ilike', 'Эрх борлуулсны орлого/')]).id)
        self.v_25 = self.compute_with_account(
            report_option.search([('name', 'ilike', '25. Үл хөдлөх эд хөрөнгийн борлуулсны орлого/')]).id)
        self.v_26 = self.compute_with_account(
            report_option.search([('name', 'ilike', '25. Үл хөдлөх эд хөрөнгийн борлуулсны орлого/')]).id)
        self.a_1_4 = self.compute_with_account(report_option.search([('name', 'ilike', 'Бусад орлогын дүн')]).id)
        self.a_1_13 = self.compute_with_account(
            report_option.search([('name', 'ilike', 'Гадаад валютын ханшны зөрүүгийн бодит орлого')]).id)
        self.b_18 = self.compute_with_account(report_option.search([('name', 'ilike', '18. Хүүгийн орлого')]).id)
        self.b_18_1 = self.compute_with_account(
            report_option.search([('name', 'ilike', 'Хүүгийн орлогод ногдуулсан татвар (39 x 10%)')]).id)
        self.g_2_1 = self.v_28 * -1 if self.v_28 < 0 else 0
        self.g_2_2 = self.v_28 if self.v_28 > 0 else 0

    @api.multi
    def action_export(self):
        company = self.company_id
        company_dict = {'ttd0': '', 'ttd1': '', 'ttd2': '', 'ttd3': '', 'ttd4': '', 'ttd5': '', 'ttd6': '',
                        'company_name': u'2. Нэр: %s' % (company.name), 'basic_operation_name': '',
                        'basic_operation_code': '',
                        'date_str': '%s - %s' % (datetime.strptime(self.date_from, '%Y-%m-%d').strftime('%Y.%m.%d'),
                                                 datetime.strptime(self.date_to, '%Y-%m-%d').strftime('%Y.%m.%d'))}
        if company.vat:
            for i in range(0, len(company.vat)):
                company_dict.update({'ttd%s' % (i): company.vat[i]})
        if company.operation_ids:
            for operation in company.operation_ids:
                if operation.type == 'basic':
                    company_dict.update({'basic_operation_name': operation.name})
                    company_dict.update({'basic_operation_code': operation.code})
                    break

        dicts = {
            'company_name': u' %s' % company.name,
            'date_str': '%s - %s' % (self.date_from, self.date_to),
            'a_1': self.a_1,
            'a_1_1': self.a_1_1,
            'a_1_2': self.a_1_2,
            'a_1_3': self.a_1_3,
            'a_1_4': self.a_1_4,
            'a_1_5': self.a_1_5,
            'a_1_6': self.a_1_6,
            'a_1_7': self.a_1_7,
            'a_1_8': self.a_1_8,
            'a_1_9': self.a_1_9,
            'a_1_10': self.a_1_10,
            'a_1_11': self.a_1_11,
            'a_1_12': self.a_1_12,
            'a_1_13': self.a_1_13,
            'a_1_14': self.a_1_14,
            'a_1_15': self.a_1_15,
            'a_1_16': self.a_1_16,
            'a_2': self.a_2,
            'a_3': self.a_3,
            'a_4': self.a_4,
            'a_5': self.a_5,
            'a_6': self.a_6,
            'a_7': self.a_7,
            'a_8': self.a_8,
            'a_9': self.a_9,
            'a_10': self.a_10,
            'a_11': self.a_11,
            'a_12': self.a_12,
            'a_13': self.a_13,
            'a_14': self.a_14,
            'a_15': self.a_15,
            'b': self.b,
            'b_16': self.b_16,
            'b_16_1': self.b_16_1,
            'b_17': self.b_17,
            'b_17_1': self.b_17_1,
            'b_17_2': self.b_17_2,
            'b_17_3': self.b_17_3,
            'b_17_4': self.b_17_4,
            'b_18': self.b_18,
            'b_18_1': self.b_18_1,
            'b_19': self.b_19,
            'b_19_1': self.b_19_1,
            'b_20a': self.b_20a,
            'b_20a_1': self.b_20a_1,
            'b_20a_2': self.b_20a_2,
            'b_20a_3': self.b_20a_3,
            'b_20_b': self.b_20_b,
            'b_20_b_1': self.b_20_b_1,
            'b_21': self.b_21,
            'v': self.v,
            'v_22': self.v_22,
            'v_22_1': self.v_22_1,
            'v_23': self.v_23,
            'v_23_1': self.v_23_1,
            'v_24': self.v_24,
            'v_24_1': self.v_24_1,
            'v_25': self.v_25,
            'v_25_1': self.v_25_1,
            'v_26': self.v_26,
            'v_26_1': self.v_26_1,
            'v_27': self.v_27,
            'v_28': self.v_28,
            'g': self.g,
            'g_1_1': self.g_1_1,
            'g_1_2': self.g_1_2,
            'g_2_1': self.g_2_1,
            'g_2_2': self.g_2_2,
            'g_3': self.g_3,
            'g_4': self.g_4,
            'g_5': self.g_5,
            'g_6_1': self.g_6_1,
            'g_6_2': self.g_6_2,
            'g_7_1': self.g_7_1,
            'g_7_2': self.g_7_2,
            'g_8': self.g_8,
            'g_9_1': self.g_9_1,
            'g_9_2': self.g_9_2,
            'g_10_1': self.g_10_1,
            'g_10_2': self.g_10_2,
            'ttd0': company_dict['ttd0'],
            'ttd1': company_dict['ttd1'],
            'ttd2': company_dict['ttd2'],
            'ttd3': company_dict['ttd3'],
            'ttd4': company_dict['ttd4'],
            'ttd5': company_dict['ttd5'],
            'ttd6': company_dict['ttd6'],
            'company_name': u'2. Нэр: %s' % (company.name),
            'basic_operation_name': company_dict['basic_operation_name'],
            'basic_operation_code': company_dict['basic_operation_code'],
            'date_str': '%s - %s' % (datetime.strptime(self.date_from, '%Y-%m-%d').strftime('%Y.%m.%d'),
                                     datetime.strptime(self.date_to, '%Y-%m-%d').strftime('%Y.%m.%d'))
        }

        template_file = get_module_resource('l10n_mn_account_tax_report', 'static/src/report', 'TT02-input.xlsx')
        generate_file = get_module_resource('l10n_mn_account_tax_report', 'static/src/report', 'TT02-output.xlsx')

        wb = openpyxl.load_workbook(template_file)
        sheet = wb.active
        rows = sheet.rows
        for row in rows:
            for cell in row:
                if dicts.has_key(cell.value):
                    cell.value = dicts[cell.value]
        wb.save(generate_file)

        file_name = "TT-02_%s.xlsx" % (time.strftime('%Y%m%d_%H%M'),)
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix='TT-02',
                                                                                     form_title=file_name).create({})
        image = open(generate_file, 'rb')
        image_read = image.read()
        report_excel_output_obj.filedata = base64.encodestring(image_read)
        return report_excel_output_obj.export_report()
