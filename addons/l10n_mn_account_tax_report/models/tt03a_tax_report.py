# -*- encoding: utf-8 -*-
##############################################################################
import time

from odoo import api, fields, models
from odoo.tools.translate import _
from odoo.exceptions import UserError

"""
   НӨАТ-н тайлан ТТ-03а
"""

class TT03aTaxReport(models.Model):
    _name = "tt03a.tax.report"
    _inherit = ['mail.thread']
    _description = "TT-03a Vat Report"

    name = fields.Char('Name')
    company_id = fields.Many2one('res.company', string='Company', required=True, readonly=True,
                                 default=lambda self: self.env.user.company_id, track_visibility='onchange', states={'confirmed': [('readonly', True)]})
    date_from = fields.Date(string='Start Date', required=True, default=lambda *a: time.strftime('%Y-%m-01'), track_visibility='onchange', states={'confirmed': [('readonly', True)]})
    date_to = fields.Date(string='End Date', required=True, default=lambda *a: time.strftime('%Y-%m-%d'), track_visibility='onchange', states={'confirmed': [('readonly', True)]})
    state = fields.Selection([('draft', 'Draft'),
                              ('confirmed', 'Confirmed')], 'State', required=True, default='draft')
    line_ids = fields.One2many('tt03a.tax.report.line', 'report_id', string='Lines', copy=False, states={'confirmed': [('readonly', True)]})

    # Компани, эхлэх болон дуусах огноогоор нэр талбар бөглөгдөх
    @api.onchange('date_from', 'date_to')
    def onchange_tax_report(self):
        for report in self:
            name = ''
            if report.company_id:
                name += _('%s is ') % self.company_id.name
            if report.date_from:
                name += _('[%s - ') % self.date_from
            if report.date_to:
                name += _('%s] duration TT-03A VAT Report') % self.date_to
            report.name = name

    def action_draft(self):
        # ТТ-03a тайлангийн мөрүүд бол Хавсралт мэдээний мөрүүдийг хоослох
        self.line_ids = None
        self.write({'state': 'draft'})

    def action_confirm(self):
        self.write({'state': 'confirmed'})

    @api.multi
    def create_lines(self):
        # ТТ-03a тайлангийн мөрүүдийг хоослох
        self.line_ids = None
        # ТТ-03a тайлангийн мөрүүдийг үүсгэх
        self.env['account.tax.report.option']._create_lines(self.id, 'tt03a.tax.report.line', 'tax_tt03a')
        return True

    @api.multi
    def compute(self):
        # ТТ-03a тайлангийн мөрүүд бол Хавсралт мэдээний мөрүүдэд утга оноох
        # Данс болон дансны төрөл бол дүнг олдог хэсэг нэмэх
        # Python тооцоолол үед бодож олох. report_sequence талбарын дагуу тооцоолол хийх
        options = self.env['account.tax.report.option'].get_lines('tt03a.tax.report.line', self.line_ids)
        self.env['account.tax.report.option'].compute_report(options, self.date_from, self.date_to, self.company_id)
        return True


class TT03aTaxReportLine(models.Model):
    _name = 'tt03a.tax.report.line'
    _description = 'TT-03a Vat Report Line'

    report_option_id = fields.Many2one('account.tax.report.option', string='Indications')
    report_id = fields.Many2one('tt03a.tax.report', string='TT-03a Vat Report', ondelete="cascade")
    sequence = fields.Char('Line Number')
    amount = fields.Float(string='Amount', digits=(16, 2), default=0)
    color = fields.Selection(related='report_option_id.color', readonly=True)
    type = fields.Selection(related='report_option_id.type', readonly=True)

    @api.multi
    def button_journal_entries(self):
        # Хугацааны хоорондох журналын мөрүүдийг харуулах
        for line in self:
            domain = [('id', '=', 0)]
            if line.report_option_id and line.type == 'vat_indication' and len(line.report_option_id.vat_indication_ids) > 0:
                domain = [('vat_indication_id', 'in', line.report_option_id.vat_indication_ids.ids), ('date', '>=', line.report_id.date_from),
                          ('date', '<=', line.report_id.date_to), ('company_id', '=', line.report_id.company_id.id), ('move_id.state', '=', 'posted')]
                context = 'vat_indication_id'
            elif line.report_option_id and line.type in ('account', 'account_type'):
                accounts = []
                context = 'account_id'
                if line.type == 'account' and len(line.report_option_id.account_ids) > 0:
                    accounts = line.report_option_id.account_ids
                elif line.type == 'account_type' and len(line.report_option_id.account_type_ids) > 0:
                    accounts = self.env['account.account'].search([('user_type_id', 'child_of', line.report_option_id.account_type_ids.ids)])
                if len(accounts) > 0:
                    domain = [('account_id', 'in', accounts.ids), ('date', '>=', line.report_id.date_from),
                              ('date', '<=', line.report_id.date_to), ('company_id', '=', line.report_id.company_id.id), ('move_id.state', '=', 'posted')]
            return {
                'name': _('Journal Entry Lines'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.move.line',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'context': {'group_by': [context]},
                'domain': domain
            }