# -*- coding: utf-8 -*-
from odoo import fields, models, _, api


class VatIndication(models.Model):
    _name = "vat.indication"

    name = fields.Char('Name', required=True)
    code = fields.Char('Code', required=True)
    indication_type = fields.Selection([('income', 'Income'),
                                        ('outcome', 'Outcome'),
                                        ('asset', 'Asset')], string='Indication type', required=True)

    @api.multi
    def name_get(self):
        return [(record.id, '[' + record.code + '] ' + record.name[:30] + '...') if record.code else
                (record.id,  record.name[:30] + '...') for record in self]

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        domain = ['|', ('name', operator, name), ('code', operator, name)] or []
        records = self.search(domain + args, limit=limit)
        return records.name_get()
