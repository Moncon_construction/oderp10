# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################

from odoo import models, fields, api, _
from odoo.tools.translate import _
import time
from odoo import SUPERUSER_ID
from operator import itemgetter
from odoo.tools.safe_eval import safe_eval as eval

ALL_REPORT_SELECTION = [
    ('tt02', 'Income Tax Report TT02'),
    ('tt02b', 'Income Tax Report Discount TT02b'),
    ('tt13', 'Deduction of Tax Report TT13'),
    ('ttd', 'Finance and Tax Report Difference'),
    ('tt03b', 'Vat TT-03b'),
    ('tax_tt03a', 'New Vat TT-03a'),
    ('tt12', 'Income tax report submitted to individual taxpayers TA-12'),
    ('tt23', 'Tax Report TT23'),
    ('tt24', 'Air Pollution Payment Report TT24')
]


class AccountTaxReportOption(models.Model):
    _name = 'account.tax.report.option'
    _description = 'Accounting Report Options'
    _order = 'report, sequence'

    report = fields.Selection(ALL_REPORT_SELECTION, 'Report', required=True, default='tt02')
    name = fields.Text('Name', required=True)
    code = fields.Char('Code', size=32)
    type = fields.Selection([('view', 'View'),
                             ('account', 'Select Accounts'),
                             ('account_type', 'Select Account Type'),
                             ('python', 'Python Compute'),
                             ('tax', 'Tax'),
                             ('manual_value', 'Manual Value'),
                             ('vat_indication', 'Vat Indication')], 'Type', required=True, default='account')
    balance_type = fields.Selection([('balance', 'Balance'),
                                     ('debit', 'Debit'),
                                     ('credit', 'Credit')], 'Balance Type', required=True, default='balance')
    python_compute = fields.Text('Python Compute')
    sequence = fields.Integer('Sequence')
    report_sequence = fields.Integer('Report Sequence')
    partner_ids = fields.Many2many('res.partner', 'report_option_partner_rel', 'option_id', 'partner_id', 'Partners')
    account_ids = fields.Many2many('account.account', 'report_option_account_rel', 'option_id', 'account_id', 'Accounts')
    account_type_ids = fields.Many2many('account.account.type', 'report_option_account_type_rel', 'option_id', 'account_type_id', 'Account Types')
    vat_indication_ids = fields.Many2many('vat.indication', 'report_option_vat_indicatoin_rel', 'option_id', 'vat_indication_id', 'Vat Indications')
    company_id = fields.Many2one('res.company', 'Company', required=True, default=lambda self: self.env['res.company']._company_default_get('account.tax.report.option'))
    active = fields.Boolean('Active', default=True)
    show = fields.Boolean('Show', default=True)
    end_balance = fields.Boolean('End Balance')
    taxpayer_number = fields.Integer('Taxpayer Number')
    tax_percent = fields.Float('Tax Percent')
    tax_id = fields.Many2one('account.tax', 'Tax')
    parent_id = fields.Many2one('account.tax.report.option', 'Parent Option')
    child_ids = fields.One2many('account.tax.report.option', 'parent_id', 'Child Options')
    comment = fields.Text('Comment')
    excel_key = fields.Char(string='Key')
    color = fields.Selection([('black', 'Black'),
                              ('bold', 'Bold'),
                              ('blue', 'Blue')], 'Color', required=True, default='black')

    @api.multi
    def create_report_data(self, options, rounding=False, extradict=None):
        ''' Баланc тайлангийн мэдээллийг боловсруулж
            тайлангийн форматад тохируулан python [{},{},{}...]
            загвараар хүснэгтийн мөр багануудын өгөгдлийг боловсруулна.

            @return: cols - Хүснэгтийн толгойн өгөгдлүүд
                     rows - Хүснэгтийн биеийн өгөгдлүүд
        '''
        vat_indication_obj = self.env['vat.indication']
        context = self.env.context
        company_id = context['company_id']
        date_from = context['date_from']
        date_to = context['date_to']
        def get_childs(option, number, level=0):
            childgrid = []
            for child in option.child_ids:
                row = {}
                row['name'] = child.name
                row['number'] = number
                row['level'] = level + 1
                row['code'] = child.code
                row['type'] = child.type
                row['sequence'] = child.sequence
                row['key'] = child.excel_key
                row['key2'] = 0
                row['tax_percent'] = child.tax_percent
                """ tt12-н үзүүлэлтийн мөрийн урт нь 22 байдаг бол tt13-нх 24 байдаг """
                adder = 24
                if child.report == 'tt12':
                    adder = 22
                if child.report in ('tt13', 'tt12'):
                    if row['key']:
                        split1 = child.excel_key.split('{')
                        split2 = split1[1].split('}')
                        int_split = int(split2[0]) + adder
                        string_split = '{' + str(int_split) + '}'
                        row['key2'] = string_split
                a = 0
                b = 0
                if child.type in ('account', 'tax'):
                    """ Татвартай үед, tt12 нь child-ууд дээрээ tax-тай бол tt13 нь parent-үүд дээрээ tax-тай харин child-ууд дээрээ данс сонгох"""
                    if child.parent_id.tax_id or (child.type == 'tax' and child.tax_id):
                        inv_line_ids = []
                        inv_ids = []
                        total_partner = []
                        if child.type == 'tax' and child.tax_id:
                            self._cr.execute("SELECT invoice_line_id as invoice_line_id "
                                             "FROM account_invoice_line_tax A "
                                             "LEFT JOIN account_invoice_line line ON line.id = invoice_line_id "
                                             "LEFT JOIN account_invoice invoice ON invoice.id = line.invoice_id "
                                             "LEFT JOIN account_move move ON move.id = invoice.move_id "
                                             "WHERE A.tax_id = %s AND invoice.state in ('open','paid') AND move.date BETWEEN %s AND %s ", (child.tax_id.id, date_from, date_to))
                        else:
                            self._cr.execute('SELECT invoice_line_id as invoice_line_id FROM account_invoice_line_tax A WHERE A.tax_id = %s' % child.parent_id.tax_id.id)
                        in_ids = self._cr.fetchall()
                        if child.account_ids:
                            in_ids2 = self.env['account.invoice.line'].search([('account_id', 'in', child.account_ids.ids), ('id', 'in', in_ids), ('company_id', '=', company_id)])
                        else:
                            in_ids2 = self.env['account.invoice.line'].search([('id', 'in', in_ids), ('company_id', '=', company_id)])
                        for in_id in in_ids2:
                            if in_id:
                                inv_line_ids.append(self.env['account.invoice.line'].browse(in_id.id))
                        for inv_line_id in inv_line_ids:
                            inv_ids.append(inv_line_id.invoice_id)
                        if inv_ids:
                            for inv_id in inv_ids:
                                if inv_id.partner_id.id not in total_partner:
                                    total_partner.append(inv_id.partner_id.id)
                                # тт12 дээр татваргүй дүнгийн нийлбэр бодогдоно
                                if child.type == 'tax':
                                    a = a + inv_id.amount_untaxed
                                else:
                                    a = a + inv_id.amount_total
                            b = len(total_partner)
                        else:
                            a = 0
                    else:
                        if child.account_ids:
                            if child.end_balance:
                                for account in child.account_ids:
                                    initial_bal = self.env['account.move.line'].get_initial_balance(company_id, [account.id], date_from, 'posted')
                                    if initial_bal:
                                        a += initial[0]['start_credit'] - initial[0]['start_debit']
                            else:
                                account_ids = []
                                [account_ids.append(account.id) for account in child.account_ids]
                                if child.partner_ids:
                                    partner_ids = []
                                    [partner_ids.append(partner.id) for partner in child.partner_ids]
                                self._cr.execute("SELECT  sum(l.debit) AS debit, sum(l.credit) AS credit, at.type AS type "
                                                 "FROM account_move_line l "
                                                 "LEFT JOIN account_move m ON l.move_id = m.id "
                                                 "JOIN account_journal j ON l.journal_id = j.id "
                                                 "JOIN account_account aa ON l.account_id = aa.id "
                                                 "JOIN account_account_type at ON aa.user_type_id = at.id "
                                                 "WHERE m.date BETWEEN %s AND %s "
                                                 "AND l.company_id = %s AND m.state = 'posted' "
                                                 "AND l.account_id in %s "
                                                 "GROUP BY at.type ", (date_from, date_to, company_id, tuple(account_ids)))
                                lines = self._cr.dictfetchall()
                                for line in lines:
                                    if child.balance_type == 'balance':
                                        if line['type'] in ('expense', 'asset'):
                                            a += line['debit'] - line['credit']
                                        else:
                                            a += line['credit'] - line['debit']
                                    elif child.balance_type == 'debit':
                                        a += line['debit']
                                    elif child.balance_type == 'credit':
                                        a += line['credit']
                if rounding:
                    a /= 1000.0
                if child.type == 'python':
                    row['python'] = child.python_compute
                localdict.setdefault(child.code, a)
                row['amount'] = a
                row['total_partner'] = b
                if child.report == 'tt03a':
                    # Хэрвээ тухайн татаж буй тайлан нь tt03a бол нэхэжмлэхийн мөрүүдээс НӨАТ-ийн үзүүлэлтийн нэрээр шүүж нийт дүнг бодож олно.
                    indication_id = vat_indication_obj.search([('name', '=', row['name'])])
                    if indication_id:
                        self._cr.execute("SELECT SUM(invl.price_subtotal) as amount, COUNT(inv.partner_id) as total_partner FROM account_invoice_line invl LEFT JOIN account_invoice inv ON invl.invoice_id = inv.id WHERE inv.state in ('paid', 'open') AND inv.date_invoice BETWEEN %s AND %s AND inv.company_id = %s AND invl.vat_indication_id = %s", (date_from, date_to, company_id, str(indication_id.id)))
                        indications = self._cr.dictfetchall()
                        if indications:
                            for indication in indications:
                                row['amount'] = indication['amount']
                                row['total_partner'] = indication['total_partner']
                if child.show:
                    number += 1
                childgrid2, number = get_childs(child, number, level=level + 1)
                childgrid.append(row)
                childgrid += childgrid2
            return childgrid, number

        def childs_sum(parent_code):
            option = self.search([('code', '=', parent_code)])
            a = 0
            if option:
                childs = self.search([('parent_id', '=', option[0].id)])
                if childs:
                    for c in self.read(childs, ['code']):
                        a += localdict.get(c['code'], 0)
            return a

        if context is None:
            context = {}
        if extradict is None:
            extradict = {}
        rows = []

        localdict = extradict

        if options:
            line_number = 0
            for option in options:
                row = {}
                row['name'] = option.name
                row['number'] = line_number
                row['code'] = option.code
                row['type'] = option.type
                row['sequence'] = option.sequence
                row['show'] = option.show
                row['key'] = option.excel_key
                row['tax_percent'] = option.tax_percent
                """ tt12-н үзүүлэлтийн мөрийн урт нь 22 байдаг бол tt13-нх 24 байдаг """
                adder = 24
                if option.report == 'tt12':
                    adder = 22
                if option.excel_key:
                    split1 = option.excel_key.split('{')
                    split2 = split1[1].split('}')
                    int_split = int(split2[0]) + adder
                    string_split = '{' + str(int_split) + '}'
                    row['key2'] = string_split
                else:
                    row['key2'] = 0
                a = 0
                b = 0
                if option.type in ('account', 'tax'):
                    """ Татвартай үед, tt12 нь child-ууд дээрээ tax-тай бол tt13 нь parent-үүд дээрээ tax-тай харин child-ууд дээрээ данс сонгох"""
                    if option.parent_id.tax_id or (option.type == 'tax' and option.tax_id):
                        inv_line_ids = []
                        inv_ids = []
                        total_partner = []
                        if child.type == 'tax' and child.tax_id:
                            self._cr.execute("SELECT invoice_line_id as invoice_line_id "
                                             "FROM account_invoice_line_tax A "
                                             "LEFT JOIN account_invoice_line line ON line.id = invoice_line_id "
                                             "LEFT JOIN account_invoice invoice ON invoice.id = line.invoice_id "
                                             "LEFT JOIN account_move move ON move.id = invoice.move_id "
                                             "WHERE A.tax_id = %s AND invoice.state in ('open','paid') AND move.date BETWEEN %s AND %s ", (option.tax_id.id, date_from, date_to))
                        else:
                            self._cr.execute('SELECT invoice_line_id as invoice_line_id FROM account_invoice_line_tax A WHERE A.tax_id = %s' % option.parent_id.tax_id.id)
                        in_ids = self._cr.fetchall()
                        if option.account_ids:
                            in_ids2 = self.env['account.invoice.line'].search([('account_id', 'in', option.account_ids.ids), ('id', 'in', in_ids), ('company_id', '=', company_id)])
                        else:
                            in_ids2 = self.env['account.invoice.line'].search([('id', 'in', in_ids), ('company_id', '=', company_id)])
                        for in_id in in_ids2:
                            if in_id:
                                inv_line_ids.append(self.env['account.invoice.line'].browse(in_id.id))
                        for inv_line_id in inv_line_ids:
                            inv_ids.append(inv_line_id.invoice_id)
                        if inv_ids:
                            for inv_id in inv_ids:
                                if inv_id.partner_id.id not in total_partner:
                                    total_partner.append(inv_id.partner_id.id)
                                # тт12 дээр татваргүй дүнгийн нийлбэр бодогдоно
                                if child.type == 'tax':
                                    a = a + inv_id.amount_untaxed
                                else:
                                    a = a + inv_id.amount_total
                            b = len(total_partner)
                        else:
                            a = 0
                    else:
                        if option.account_ids:
                            if option.end_balance:
                                for account in option.account_ids:
                                    initial_bal = self.env['account.move.line'].get_initial_balance(company_id, [account.id], date_from, 'posted')
                                    if initial_bal:
                                        a += initial[0]['start_credit'] - initial[0]['start_debit']
                            else:
                                account_ids = []
                                [account_ids.append(account.id) for account in option.account_ids]
                                if option.partner_ids:
                                    partner_ids = []
                                    [partner_ids.append(partner.id) for partner in option.partner_ids]
                                self._cr.execute("SELECT  sum(l.debit) AS debit, sum(l.credit) AS credit, at.type AS type "
                                                 "FROM account_move_line l "
                                                 "LEFT JOIN account_move m ON l.move_id = m.id "
                                                 "JOIN account_journal j ON l.journal_id = j.id "
                                                 "JOIN account_account aa ON l.account_id = aa.id "
                                                 "JOIN account_account_type at ON aa.user_type_id = at.id "
                                                 "WHERE m.date BETWEEN %s AND %s "
                                                 "AND l.company_id = %s AND m.state = 'posted' "
                                                 "AND l.account_id in %s "
                                                 "GROUP BY at.type ", (date_from, date_to, company_id, tuple(account_ids)))
                                lines = self._cr.dictfetchall()
                                for line in lines:
                                    if option.balance_type == 'balance':
                                        if line['type'] in ('expense', 'asset'):
                                            a += line['debit'] - line['credit']
                                        else:
                                            a += line['credit'] - line['debit']
                                    elif option.balance_type == 'debit':
                                        a += line['debit']
                                    elif option.balance_type == 'credit':
                                        a += line['credit']
                if rounding:
                    a /= 1000.0
                if option.type == 'python':
                    row['python'] = option.python_compute
                localdict.setdefault(option.code, a)
                row['amount'] = a
                row['level'] = 0
                row['total_partner'] = b
                if option.show == True:
                    line_number += 1
                childgrid, line_number = get_childs(option, line_number, level=0)
                rows.append(row)
                rows += childgrid

        localdict['CHILDS_SUM'] = childs_sum
        again_loop = []
        for r in sorted(rows, key=itemgetter('level', 'type'), reverse=True):
            if r.get('type', False) == 'python' and r.get('python', False):
                eval(r['python'], localdict, mode='exec', nocopy=True)
                a = localdict.get('amount', False)
                localdict[r['code']] = a
                r.update({'amount': a})
                if a == 0 or not a:  # Дарааллаас хамаараад уг үзүүлэлтийг тооцоолох болоогүй байж магадгүй.
                    # Тиймээс дахин нэг удаа тооцоолно.
                    again_loop.append(r)

        for r in sorted(again_loop, key=itemgetter('level', 'type', 'sequence'), reverse=True):
            eval(r['python'], localdict, mode='exec', nocopy=True)
            a = localdict.get('amount', False)
            localdict[r['code']] = a
            r.update({'amount': a})

        return sorted(rows, key=itemgetter('number'))

    @api.model
    def get_vat_indication_balance(self, company_id, vat_indication_ids, date_start, date_stop):
        ''' Тухайн НӨАТ-н үзүүлэлтийн тайлант хугацааны хоорондох дүнг олно.
        '''
        where = " "
        if vat_indication_ids:
            where += ' AND ml.vat_indication_id in (' + ','.join(map(str, vat_indication_ids)) + ') '
        self.env.cr.execute("SELECT ml.vat_indication_id AS indication_id, v.name AS indication_name, v.indication_type AS itype, "
                                    "sum(ml.debit) AS debit, sum(ml.credit) AS credit "
                            "FROM  account_move_line ml "
                            "LEFT JOIN account_move m ON (ml.move_id = m.id) "
                            "LEFT JOIN vat_indication v ON (ml.vat_indication_id = v.id) "                                                                       
                            "WHERE ml.date BETWEEN %s AND %s AND ml.company_id = %s AND m.state = 'posted' " + where + " "
                            "GROUP BY ml.vat_indication_id, v.name,v.indication_type "
                            "ORDER BY v.name, ml.vat_indication_id ", (date_start, date_stop, company_id))
        return self.env.cr.dictfetchall()

    @api.multi
    def _create_lines(self, report_id, obj, report):
        # Татварын тайлангийн мөрүүдийг үүсгэх
        options = self.search([('report', '=', report)], order='sequence')
        seq = 0
        for option in options:
            if option.type != 'view':
                seq += 1
            self.env[obj].create({'report_option_id': option.id,
                                  'report_id': report_id,
                                  'sequence': seq if option.type != 'view' else False,
                                  })
        return True

    @api.multi
    def get_lines(self, obj, lines):
        # Нийт бүх үзүүлэлтийг агуулсан dict үүсгэх
        options = []
        for line in lines:
            dict = {'line': obj,
                    'line_id': line.id,
                    'option_id': line.report_option_id.id,
                    'type': line.report_option_id.type,
                    'report_sequence': line.report_option_id.report_sequence,
                    'tax_percent': line.report_option_id.tax_percent,
                    'amount': line.amount or 0}
            options.append(dict)
        return options

    @api.multi
    def key_check(self, option, report_option, amount):
        # Хуулийн 18.4-т заасан зардлын дүн шалгав
        if option['line'] == 'tt02.tax.report.xm3b' and report_option.excel_key == '{3}':
            self.env[option['line']].browse(option['line_id']).write({'amount': 0, 'principle_amount': amount})
        # Гараар утга оруулж байгаа эсэхийг шалгав
        if option['type'] != 'manual_value':
            self.env[option['line']].browse(option['line_id']).write({'amount': amount})
        return True

    @api.multi
    def compute_report(self, options, date_from, date_to, company_id):
        # Татварын тайлангийн мөрүүдэд утга оноох
        # Данс болон дансны төрөл бол дүнг олдог хэсэг нэмэх
        # Python тооцоолол үед бодож олох. report_sequence талбарын дагуу тооцоолол хийх
        localdict = {}
        for option in sorted(options, key=itemgetter('report_sequence'), reverse=False):
            account_ids = []
            report_option = self.env['account.tax.report.option'].browse(option['option_id'])
            amount = 0
            tax_percent = option['tax_percent'] / 100 if option['tax_percent'] > 0 else 1
            if option['type'] == 'manual_value':
                amount = option['amount']
                self.key_check(option, report_option, amount)
            elif option['type'] in ('account', 'account_type'):
                if option['type'] == 'account' and len(report_option.account_ids) > 0:
                    account_ids = report_option.account_ids.ids
                elif option['type'] == 'account_type' and len(report_option.account_type_ids) > 0:
                    account_ids = self.env['account.account'].search([('user_type_id', 'child_of', report_option.account_type_ids.ids)]).ids
                if len(account_ids) > 0:
                    moves = self.env['account.move.line'].get_balance(company_id.id, account_ids, date_from, date_to, 'posted', True)
                    for move in moves:
                        if report_option.balance_type == 'balance':
                            if move['itype'] in ('payable', 'income'):
                                amount += move['credit'] - move['debit']
                            else:
                                amount += move['debit'] - move['credit']
                        elif report_option.balance_type == 'debit':
                            amount += move['debit']
                        elif report_option.balance_type == 'credit':
                            amount += move['credit']
                amount = amount * tax_percent
                option['amount'] = amount
                self.key_check(option, report_option, amount)
            elif option['type'] == 'python' and report_option.python_compute:
                eval(report_option.python_compute, localdict, mode='exec', nocopy=True)
                amount = localdict.get('amount', 0)
                amount = amount * tax_percent
                option['amount'] = amount
                self.key_check(option, report_option, amount)
            elif option['type'] == 'vat_indication':
                if report_option.vat_indication_ids:
                    lines = self.get_vat_indication_balance(company_id.id, report_option.vat_indication_ids.ids, date_from, date_to)
                    for line in lines:
                        if report_option.balance_type == 'balance':
                            if line['itype'] == 'income':
                                amount += line['credit'] - line['debit']
                            else:
                                amount += line['debit'] - line['credit']
                        elif report_option.balance_type == 'debit':
                            amount += line['debit']
                        elif report_option.balance_type == 'credit':
                            amount += line['credit']
                amount = amount * tax_percent
                option['amount'] = amount
                self.key_check(option, report_option, amount)
            localdict.setdefault(report_option.code, amount)
        return True