# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################

from odoo import models, fields, api, _

class ResCompanyOperation(models.Model):
    _name = 'res.company.operation'
    _description = 'Res company operation'

    name = fields.Char(string='Name', required=True)
    code = fields.Char(string='Code', size=6, required=True)
    type = fields.Selection([('basic', 'Basic'),
                              ('supporting', 'Supporting')], string='Type', required=True)
    company_id = fields.Many2one('res.company', string='Company')

class ResCompany(models.Model):
    _inherit = "res.company"

    foreign_investment = fields.Float(string='Foreign Investment')
    foreign_percent = fields.Float(string='Foreign Percent')
    operation_ids = fields.One2many('res.company.operation', 'company_id', string='Operations')