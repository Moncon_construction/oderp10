# -*- coding: utf-8 -*-
import base64
import time
from io import BytesIO
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from operator import itemgetter
from datetime import date, datetime, timedelta

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class AccountReportVatJournal(models.TransientModel):
    _name = 'account.report.vat.journal'
    _description = 'Account VAT Journal'

    fiscalyear_id = fields.Many2one('account.fiscalyear', string='Fiscal Year', required=True)
    entry_type = fields.Selection([('purchase', 'Purchase'),
                                   ('sale', 'Sales')], 'Type', default='purchase', required=True)
    date_from = fields.Date("Start Date", required=True)
    date_to = fields.Date("End Date", required=True)
    partner_id = fields.Many2one('res.partner', 'Partner')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env['res.company']._company_default_get('account.report.vat.journal'))

    @api.onchange('fiscalyear_id')
    def onchange_fiscalyear(self):
        if self.fiscalyear_id:
            fiscalyear = self.env['account.fiscalyear'].search([('id', '=', self.fiscalyear_id.id)])
            if fiscalyear:
                self.date_from = fiscalyear.date_start
                self.date_to = fiscalyear.date_stop

    def export_report(self):
        read_obj = self
        fiscalyear_obj = self.env['account.fiscalyear']
        account_obj = self.env['account.account']
        currency_obj = self.env['res.currency']
        period_obj = self.env['account.period']
        move_line_obj = self.env['account.move.line']
        journal_obj = self.env['account.journal']
        invoice_obj = self.env['account.invoice']

        fy = read_obj.fiscalyear_id
        company = read_obj.company_id
        date_from = read_obj.date_from
        date_to = read_obj.date_to
        data = []
        data_dict = {'taxed': [],
                     'ref_taxed': [],
                     'other_taxed': [], }
        total = 0
        total_tax = 0
        total_untaxed = 0
        rowx = 0
        number = 1
        if read_obj.entry_type == 'purchase':
            title_type = _('Purchase - Internal')
            type = ('in_invoice', 'in_refund')
        elif read_obj.entry_type == 'sale':
            title_type = _('Sale')
            type = ('out_invoice', 'out_refund')

        """
            Харилцагч сонгосон эсэхээс хамаарч нэхэмжлэлүүдийг хайна.
        """
        partner_where = ''
        if read_obj.partner_id:
                invoices = invoice_obj.search([('date_invoice', '>=', date_from),
                                               ('date_invoice', '<=', date_to),
                                               ('partner_id', '=', read_obj.partner_id.id),
                                               ('state', 'in', ('open', 'paid')),
                                               ('type', 'in', type), ('company_id', '=', self.company_id.id)], order='journal_id, date_invoice')

                partner_where += " AND l.partner_id in " + str(read_obj.partner_id.id)
        else:
            invoices = invoice_obj.search([('date_invoice', '>=', date_from),
                                           ('date_invoice', '<=', date_to),
                                           ('state', 'in', ('open', 'paid')),
                                           ('type', 'in', type), ('company_id', '=', self.company_id.id)], order='date_invoice')
        """
            НӨАТ буюу is_vat талбар бөглөгдсөн дансуудыг авах
        """
        account_idss = []
        for ac in account_obj.search([('is_vat', '=', True)]):
            if ac:
                account_idss.append(ac.id)
            else:
                raise UserError(_('VAT Account Not Found !!!'))
        """
            НӨАТ-н данс сонгож өгсөн журналын бичилтийг шүүж гаргах
        """

        if read_obj.partner_id:
            aml_objs = move_line_obj.search([('partner_id', '=', read_obj.partner_id.id), ('account_id', 'in', tuple(account_idss)), ('date', '>=', date_from), ('date', '<=', date_to)])
        elif not read_obj.partner_id:
            aml_objs = move_line_obj.search([('account_id', 'in', account_idss), ('date', '>=', date_from), ('date', '<=', date_to)])

        """
                Сонгосон төрлөөс хамааран худалдан авалт эсвэл борлуулалтын захиалгыг сонгож өгөгдлүүдийг авах
            Хэрвээ байрлал өөр бол гадаад, үгүй бол дотоод гэсэн ангилалаар.
        """
        if read_obj.entry_type == 'purchase':
            for invoice in invoices:
                if invoice.tax_line_ids:
                    for tax_line in invoice.tax_line_ids:
                        if tax_line.account_id.is_vat:
                            if invoice.type == 'in_invoice':
                                data_dict['taxed'].append(invoice)
                            elif invoice.type == 'in_refund':
                                data_dict['ref_taxed'].append(invoice)
                is_vat = False
            for move_line in aml_objs:
                if move_line.account_id.internal_type == 'receivable':
                    data_dict['other_taxed'].append(move_line.id)
        elif read_obj.entry_type == 'sale':
            for invoice in invoices:
                is_vat = False
                if invoice.tax_line_ids:
                    for tax_line in invoice.tax_line_ids:
                        if tax_line.account_id.is_vat:
                            is_vat = True
                if is_vat:
                    if invoice.type == 'out_invoice':
                        data_dict['taxed'].append(invoice)
                    elif invoice.type == 'out_refund':
                        data_dict['ref_taxed'].append(invoice)
            for move_line in aml_objs:
                if move_line.account_id.internal_type == 'payable':
                            data_dict['other_taxed'].append(move_line.id)
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        # create name
        report_name = _('Account Vat Journal')
        report_type_name = '%s: %s' % (_('Type'), title_type)
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        # Custom for standart
        format_name = {
            'font_name': 'Times New Roman',
            'font_size': 14,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter'
        }
        # create formats
        format_content_text_footer = {
            'font_name': 'Times New Roman',
            'font_size': 10,
            'align': 'vcenter',
            'valign': 'vcenter',
        }
        format_content_right = {
            'font_name': 'Times New Roman',
            'font_size': 9,
            'align': 'right',
            'valign': 'vcenter',
            'border': 1,
            'num_format': '#,##0.00'
        }
        format_group_center = {
            'font_name': 'Times New Roman',
            'font_size': 10,
            'align': 'center',
            'valign': 'vcenter',
            'border': 1,
        }
        format_group_center_footer = {
            'font_name': 'Times New Roman',
            'font_size': 10,
            'align': 'center',
            'valign': 'vleft'
        }
        format_group = {
            'font_name': 'Times New Roman',
            'font_size': 10,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter',
            'border': 1,
            'bg_color': '#CFE7F5',
            'num_format': '#,##0.00'
        }
        format_group_total = {
            'font_name': 'Times New Roman',
            'font_size': 10,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter',
            'border': 1,
        }
        format_group_center = book.add_format(format_group_center)
        format_name = book.add_format(format_name)
        format_content_text_footer = book.add_format(format_content_text_footer)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_group_right = book.add_format(ReportExcelCellStyles.format_group_right)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_group_left = book.add_format(ReportExcelCellStyles.format_group_left)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_center = book.add_format(ReportExcelCellStyles.format_content_center)
        format_group = book.add_format(format_group)
        format_group_total = book.add_format(format_group_total)
        format_group_center_footer = book.add_format(format_group_center_footer)
        format_content_right = book.add_format(format_content_right)
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('account_vat_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 0

        # compute column
        colx_number = 3
        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 15)
        sheet.set_column('C:C', 15)
        sheet.set_column('D:D', 15)
        sheet.set_column('E:E', 15)
        sheet.set_column('F:F', 15)
        sheet.set_column('G:G', 15)
        sheet.set_column('H:H', 15)
        sheet.set_column('I:I', 15)

        # create name
        sheet.write(rowx, 0, (_('Company name:') + '%s' % (company.name)), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 1, rowx, 8, report_name.upper(), format_name)
        rowx += 1
        sheet.merge_range(rowx, 1, rowx, 8, report_type_name.upper(), format_name)
        # create date
        sheet.write(rowx + 1, 0, _('Filtered: ') + read_obj.date_from + ' - ' + read_obj.date_to, format_filter)
        rowx += 1
        sheet.write(rowx + 1, 0, _('Created on: ') + str(datetime.now().strftime("%Y-%m-%d %H:%M:%S")), format_filter)
        rowx += 3
        sheet.write(rowx, 0, _('№'), format_title)
        sheet.write(rowx, 1, _('Date'), format_title)  # Хөрөнгийн дугаар
        sheet.write(rowx, 2, _('Invoice №'), format_title)  # Үндсэн хөрөнгийн нэр
        sheet.write(rowx, 3, _('Report №'), format_title)  # Элэгдэх тоо
        sheet.write(rowx, 4, _('Partner'), format_title)  # Худалдан авсан огноо
        sheet.write(rowx, 5, _('Register №'), format_title)  # Өртөг
        sheet.write(rowx, 6, _('Total amount'), format_title)  # Өртөг
        sheet.write(rowx, 7, _('Taxed amount'), format_title)  # Өртөг
        sheet.write(rowx, 8, _('Untaxed amount'), format_title)  # Өртөг
        """
            Нэхэмжлэлээр бол
        """
        if data_dict['taxed']:
            if read_obj.entry_type == 'purchase':
                rowx += 1
                sheet.merge_range(rowx, 0, rowx, 8, title_type, format_group)
            elif read_obj.entry_type == 'sale':
                rowx += 1
                sheet.merge_range(rowx, 0, rowx, 8, title_type, format_group)
            sub = 0
            sub_tax = 0
            sub_untaxed = 0
            rowx += 1
            for inv in data_dict['taxed']:
                amount_tax = amount_untaxed = 0
                for tax_line in inv.tax_line_ids:
                    if tax_line.account_id.is_vat:
                        amount_tax += tax_line.amount or 0
                        amount_untaxed += tax_line.base or 0
                if inv.currency_id.id != company.currency_id.id:
                    amount_untaxed = read_obj.company_id.currency_id.compute(amount_untaxed, inv.company_id.currency_id)
                    amount_tax = read_obj.company_id.currency_id.compute(amount_tax, inv.company_id.currency_id)
                amount_total = amount_untaxed + amount_tax
                sheet.write(rowx, 0, number, format_content_number)
                sheet.write(rowx, 1, inv.date_invoice, format_content_text)
                sheet.write(rowx, 2, inv.number, format_content_text)
                sheet.write(rowx, 3, inv.origin or '', format_group_center)
                sheet.write(rowx, 4, inv.partner_id.name, format_group_center)
                sheet.write(rowx, 5, inv.partner_id.register or '', format_content_right)
                sheet.write(rowx, 6, amount_total, format_content_right)
                sheet.write(rowx, 7, amount_tax, format_content_right)
                sheet.write(rowx, 8, amount_untaxed, format_content_right)
                rowx += 1
                number += 1
                sub += amount_total or 0
                sub_tax += amount_tax or 0
                sub_untaxed += amount_untaxed or 0
            sheet.write(rowx, 0, '', format_content_number)
            sheet.write(rowx, 1, '', format_content_text)
            sheet.write(rowx, 2, '', format_content_text)
            sheet.write(rowx, 3, _('AMOUNT'), format_group_center)
            sheet.write(rowx, 4, '', format_group_center)
            sheet.write(rowx, 5, '', format_content_right)
            sheet.write(rowx, 6, sub, format_group_center)
            sheet.write(rowx, 7, sub_tax, format_group_center)
            sheet.write(rowx, 8, sub_untaxed, format_group_center)
            total += sub
            total_tax += sub_tax
            total_untaxed += sub_untaxed

        """
            Буцаалтын нэхэмжлэлээр бол
        """
        if data_dict['ref_taxed']:
            if read_obj.entry_type == 'purchase':
                rowx += 1
                sheet.merge_range(rowx, 0, rowx, 8, _('Purchase Refund'), format_group)
            elif read_obj.entry_type == 'sale':
                rowx += 1
                sheet.merge_range(rowx, 0, rowx, 8, _('Sale Refund'), format_group)
            number = 1
            sub = 0
            sub_tax = 0
            sub_untaxed = 0
            rowx += 1
            for inv in data_dict['ref_taxed']:
                amount_tax = amount_untaxed = 0
                for tax_line in inv.tax_line_ids:
                    if tax_line.account_id.is_vat:
                        amount_tax += tax_line.amount
                        amount_untaxed += tax_line.base or 0
                if inv.currency_id.id != company.currency_id.id:
                    amount_untaxed = currency_obj.compute(inv.currency_id.id, company.currency_id.id, amount_untaxed)
                    amount_tax = currency_obj.compute(inv.currency_id.id, company.currency_id.id, amount_tax)
                amount_total = amount_untaxed + amount_tax
                sheet.write(rowx, 0, number, format_content_number)
                sheet.write(rowx, 1, inv.date_invoice, format_content_text)
                sheet.write(rowx, 2, inv.number, format_content_text)
                sheet.write(rowx, 3, inv.origin, format_group_center)
                sheet.write(rowx, 4, inv.partner_id.name, format_group_center)
                sheet.write(rowx, 5, inv.partner_id.register, format_content_right)
                sheet.write(rowx, 6, amount_total, format_content_right)
                sheet.write(rowx, 7, amount_tax, format_content_right)
                sheet.write(rowx, 8, amount_untaxed, format_content_right)
                rowx += 1
                number += 1
                sub -= inv.amount_total or 0
                sub_tax -= amount_tax or 0
                sub_untaxed -= amount_untaxed or 0
            sheet.write(rowx, 0, '', format_content_number)
            sheet.write(rowx, 1, '', format_content_text)
            sheet.write(rowx, 2, '', format_content_text)
            sheet.write(rowx, 3, _('AMOUNT'), format_group_center)
            sheet.write(rowx, 4, '', format_group_center)
            sheet.write(rowx, 5, '', format_content_right)
            sheet.write(rowx, 6, sub, format_group_center)
            sheet.write(rowx, 7, sub_tax, format_group_center)
            sheet.write(rowx, 8, sub_untaxed, format_group_center)
            rowx += 1
            total += sub
            total_tax += sub_tax
            total_untaxed += sub_untaxed
        """
            Бусад ажил гүйлгээ бол
        """
        if data_dict['other_taxed']:
            sheet.merge_range(rowx, 0, rowx, 8, _('Other'), format_group)
            number = 1
            sub = 0
            sub_tax = 0
            amount_untaxed = amount_tax = amount_total = 0
            sub_untaxed = 0
            rowx += 1
            for line in self.env['account.move.line'].search([('id', 'in', data_dict['other_taxed'])]):
                amount_tax = 0
                amount_tax = line.debit - line.credit
                sheet.write(rowx, 0, number, format_content_number)
                sheet.write(rowx, 1, line.date or '', format_content_text)
                sheet.write(rowx, 2, line.name or '', format_content_text)
                sheet.write(rowx, 3, line.move_id.name or '', format_group_center)
                sheet.write(rowx, 4, line.partner_id.name or '', format_group_center)
                sheet.write(rowx, 5, line.partner_id.register or '', format_content_right)
                sheet.write(rowx, 6, 0, format_content_right)
                sheet.write(rowx, 7, amount_tax, format_content_right)
                sheet.write(rowx, 8, 0, format_content_right)
                rowx += 1
                number += 1
                sub -= 0
                sub_tax -= amount_tax or 0
                sub_untaxed -= amount_untaxed or 0
            sheet.write(rowx, 0, '', format_content_number)
            sheet.write(rowx, 1, '', format_content_text)
            sheet.write(rowx, 2, '', format_content_text)
            sheet.write(rowx, 3, _('AMOUNT'), format_group_center)
            sheet.write(rowx, 4, '', format_group_center)
            sheet.write(rowx, 5, '', format_content_right)
            sheet.write(rowx, 6, sub, format_group_center)
            sheet.write(rowx, 7, sub_tax, format_group_center)
            sheet.write(rowx, 8, sub_untaxed, format_group_center)
            rowx += 1
            total += sub
            total_tax += sub_tax
            total_untaxed += sub_untaxed
        sheet.write(rowx, 0, '', format_content_number)
        sheet.write(rowx, 1, '', format_content_text)
        sheet.write(rowx, 2, '', format_content_text)
        sheet.write(rowx, 3, _('TOTAL AMOUNT'), format_group_total)
        sheet.write(rowx, 4, '', format_group_center)
        sheet.write(rowx, 5, '', format_content_right)
        sheet.write(rowx, 6, total, format_group_center)
        sheet.write(rowx, 7, total_tax, format_group_center)
        sheet.write(rowx, 8, total_untaxed, format_group_center)
        rowx += 3
        sheet.merge_range(rowx, 0, rowx, 8, _('Accountant.................................. (                                   )'), format_group_center_footer)
        rowx += 1
        sheet.merge_range(rowx, 0, rowx, 8, _('General Accountant .................................. (                                   )'), format_group_center_footer)
        sheet.set_zoom(80)
        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report()
