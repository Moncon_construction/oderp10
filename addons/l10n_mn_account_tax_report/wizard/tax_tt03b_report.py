# -*- encoding: utf-8 -*-
##############################################################################
from odoo import api, fields, models, _
from odoo.modules import get_module_resource
from datetime import datetime
import json
import openpyxl
from io import BytesIO
import time
import base64
from operator import itemgetter


class TaxTT03bReport(models.TransientModel):
    """ТТ-03b тайлан
    """
    _name = "tax.tt03b.report"
    _description = "Tax TT-03b Report"

    company_id = fields.Many2one('res.company', string='Company', readonly=True, default=lambda self: self.env.user.company_id)
    date_from = fields.Date(string='Start Date', required=True)
    date_to = fields.Date(string='End Date', required=True)

    def get_balance_by_option(self, option_code):
        amount = {
            'start_debit': 0,
            'start_credit': 0,
            'debit': 0,
            'credit': 0
        }
        data_dict = {}
        move_line_obj = self.env['account.move.line']
        option = self.env['account.tax.report.option'].search([('code', '=', option_code)])
        if option:
            account_ids = option.account_ids.ids
            if account_ids:
                initials = move_line_obj.get_initial_balance(self.company_id.id, account_ids, self.date_from, 'posted')
                balances = move_line_obj.get_balance(self.company_id.id, account_ids, self.date_from, self.date_to, 'posted')
                fetched = initials + balances
                for f in fetched:
                    if f['account_id'] not in data_dict:
                        data_dict[f['account_id']] = {'code': f['code'],
                                                      'name': f['name'],
                                                      'start_debit': 0,
                                                      'start_credit': 0,
                                                      'cur_start_debit': 0,
                                                      'cur_start_credit': 0,
                                                      'debit': 0,
                                                      'credit': 0,
                                                      'cur_debit': 0,
                                                      'cur_credit': 0,
                                                      'end_debit': 0,
                                                      'end_credit': 0,
                                                      'cur_end_debit': 0,
                                                      'cur_end_credit': 0, }
                    data_dict[f['account_id']]['start_debit'] += f['start_debit']
                    data_dict[f['account_id']]['start_credit'] += f['start_credit']
                    data_dict[f['account_id']]['cur_start_debit'] += f['cur_start_debit']
                    data_dict[f['account_id']]['cur_start_credit'] += f['cur_start_credit']
                    data_dict[f['account_id']]['debit'] += f['debit']
                    data_dict[f['account_id']]['credit'] += f['credit']
                    data_dict[f['account_id']]['cur_debit'] += f['cur_debit']
                    data_dict[f['account_id']]['cur_credit'] += f['cur_credit']
                    data_dict[f['account_id']]['end_debit'] += f['start_debit'] + f['debit']
                    data_dict[f['account_id']]['end_credit'] += f['start_credit'] + f['credit']
                    data_dict[f['account_id']]['cur_end_debit'] += f['cur_start_debit'] + f['cur_debit']
                    data_dict[f['account_id']]['cur_end_credit'] += f['cur_start_credit'] + f['cur_credit']

            for val in sorted(data_dict.values(), key=itemgetter('code')):
                amount['start_debit'] += val['start_debit']
                amount['start_credit'] += val['start_credit']
                amount['debit'] += val['debit']
                amount['credit'] += val['credit']

        return amount

    @api.multi
    def export_report(self):
        company = self.company_id
        amount = self.get_balance_by_option('TT03bOPT_5')
        company_dict = {'ttd0': '', 'ttd1': '', 'ttd2': '', 'ttd3': '', 'ttd4': '', 'ttd5': '', 'ttd6': '',
                        'company_name': u' %s' % company.name,
                        'date_str': '%s - %s' % (self.date_from, self.date_to),
                        '{3}': amount['start_credit'],
                        '{4}': amount['start_debit'],
                        '{5}': amount['credit'],
                        '{6}': amount['debit']
                        }
        if company.vat:
            for i in range(0, len(company.vat)):
                company_dict.update({'ttd%s' % (i): company.vat[i]})

        template_file = get_module_resource('l10n_mn_account_tax_report', 'static/src/report', 'TT03b-input.xlsx')
        generate_file = get_module_resource('l10n_mn_account_tax_report', 'static/src/report', 'TT03b-output.xlsx')

        options = self.env['account.tax.report.option'].search([('report', '=', 'tt03b'), ('parent_id', '=', False)])
        lines = self.env['account.tax.report.option'].with_context(date_from=self.date_from, date_to=self.date_to,
                                                                   company_id=self.company_id.id).create_report_data(options)
        wb = openpyxl.load_workbook(template_file)
        sheet = wb.active
        ws = wb.get_sheet_by_name('TT-03b')
        rows = sheet.rows
        for row in rows:
            for cell in row:
                for line in lines:
                    if company_dict.has_key(cell.value):
                        cell.value = company_dict[cell.value]
                    if line['key'] != False and line['key'] == cell.value:
                        if line['tax_percent'] >0:
                            cell.value = line['amount'] if line['amount'] else 0 /100 * line['tax_percent']
                        else:
                            cell.value = line['amount']
                        break
        wb.save(generate_file)

        file_name = "ТТ-03b_%s.xlsx" % (time.strftime('%Y%m%d_%H%M'),)
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix='ТТ-03b', form_title=file_name).create({})
        image = open(generate_file, 'rb')
        image_read = image.read()
        report_excel_output_obj.filedata = base64.encodestring(image_read)
        return report_excel_output_obj.export_report()
