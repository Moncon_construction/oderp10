# -*- encoding: utf-8 -*-
##############################################################################
from __builtin__ import type

import xlsxwriter

from odoo import api, fields, models, _
from odoo.modules import get_module_resource
from datetime import datetime
from openpyxl.styles.borders import Border, Side
from openpyxl.styles import Alignment
import openpyxl
import time
import base64

rowx = 0

class Taxtt04Report(models.TransientModel):
    """ТТ-23 тайлан
    """
    _name = "tax.tt04.report"
    _description = "Tax TT-04 Report"

    company_id = fields.Many2one('res.company', string='Company', readonly=True, default=lambda self: self.env.user.company_id)
    date_from = fields.Date(string='Start Date')
    date_to = fields.Date(string='End Date')
    fiscalyear_id = fields.Many2one('account.fiscalyear', string='Fiscal Year', required=True)
    underpayment = fields.Many2one('account.account', 'Underpayment account', required=True)
    overpayment = fields.Many2one('account.account', 'Overpayment account', required=True)
    executive_sign = fields.Many2one('res.users', 'Executive Signature', required=True)
    general_accountant_sign = fields.Many2one('res.users', 'General Accountant Signature', required=True)

    @api.onchange('fiscalyear_id')
    def onchange_fiscalyear(self):
        # Санхүүгийн жилийн утга өөрчлөгдөхөд компани дээр бүртгэлтэй дансыг харуулах
        if self.fiscalyear_id:
            date_from = self.fiscalyear_id.date_start
            date_to = self.fiscalyear_id.date_stop
            now_day = time.strftime('%Y-%m-%d')
            if now_day > date_from and now_day < date_to:
                date_to = now_day
            self.date_from = date_from
            self.date_to = date_to
            self.company_id = self.fiscalyear_id.company_id
            self.executive_sign = self.fiscalyear_id.company_id.executive_signature
            self.general_accountant_sign = self.fiscalyear_id.company_id.genaral_accountant_signature


    @api.multi
    def export_report(self):
        company = self.company_id
        value_dict = []
        for obj in self:
            value_dict = {'ttd': '',
                            'company_name': u'%s' % (company.name),
                            'province_district': company.province_district_id.name,
                            'sum_khoroo': company.sum_khoroo_id.name,
                            'street': company.street,
                            'fax': company.fax,
                            'phone': company.phone,
                            'ID': company.id,
                            'email': company.email,
                            'door_number': company.door_number,
                            'mail_box': company.mail_box,
                            'year':time.strftime('%Y')
                            }
            if company.company_registry:
                value_dict.update({'ttd': company.company_registry})
        vehicles = []
        template_file = get_module_resource('l10n_mn_account_tax_report', 'static/src/report', 'TT04-input.xlsx')
        generate_file = get_module_resource('l10n_mn_account_tax_report', 'static/src/report', 'TT04-output.xlsx')

        self.env.cr.execute("select truck_load,taxation,tracker_capacity,car_capacity,vehicle_type,dist.name ,dist.code ,asset.passenger_capacity ,tax.tracker_capacity "
                            "from account_vehicle_tax as tax,"
                            "account_vehicle_type as type,"
                            "account_asset_asset as asset,"
                            "res_district as dist "
                            "WHERE tax.vehicle_type_id=type.id and asset.vehicle_tax_id=tax.id and dist.id=tax.location_id and asset.purchase_date <= '"+self.date_to+"'")

        taxations = self.env.cr.dictfetchall()

        last_year = '0201' + str(datetime.today().year)
        date_year_end = datetime.strptime((str(datetime.today().year) + '-12-31'), "%Y-%m-%d").date()
        date = datetime.strptime(last_year, "%d%m%Y").date()

        initial_under = self.env['account.move.line'].get_initial_balance(company.id, self.underpayment.ids, date,
                                                                          'posted')
        underbalance = 0
        overbalance = 0
        if initial_under:
            under_credit = initial_under[0]['start_credit']
            underbalance = under_credit

        initial_over = self.env['account.move.line'].get_initial_balance(company.id, self.overpayment.ids, date,
                                                                         'posted')
        if initial_over:

            over_debit = initial_over[0]['start_debit']
            overbalance = over_debit

        self.env.cr.execute("SELECT asset.vehicle_tax_id as tax_id,"
                            "type.name as vehicle_type,"
                            "asset.vehicle_name as vehicle_name,"
                            "asset.vehicle_reg_code as vehicle_reg_code,"
                            "asset.vehicle_reg_plates as vehicle_reg_plates,"
                            "asset.airpol_cer_number as airpol_cer_number, "
                            "asset.passenger_capacity as passenger_capacity,"
                            "asset.truck_capacity as truck_capacity,"
                            "tax.car_capacity as car_capacity,"
                            "tax.truck_load as truck_load,"
                            "tax.tracker_capacity as tracker_capacity,"
                            "asset.comment as comment ,"
                            "tax.taxation as taxation "
                            "FROM account_vehicle_type as type ,account_vehicle_tax as tax, account_asset_asset as asset "
                            "WHERE type.id=tax.vehicle_type_id and tax.id=asset.vehicle_tax_id and asset.purchase_date <= '"+self.date_to+"'")

        vehicles = self.env.cr.dictfetchall()

        wb = openpyxl.load_workbook(template_file)
        ws = wb.get_sheet_by_name('TT-04')
        sheet = wb.active

        thin_border = Border(left=Side(style='thin'),
                             right=Side(style='thin'),
                             top=Side(style='thin'),
                             bottom=Side(style='thin'))

        col_aph = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H' ,'I', 'J']

        rowx = 29

        #I. Албан татвар ногдуулалт.               
        rowx = self.get_taxations(taxations, ws, rowx, col_aph, thin_border)

        #II. Албан татвар төлөлтийн тооцоолол
        rowx = self.get_tax_calculation(ws, rowx, col_aph, thin_border, sheet,underbalance,overbalance)

        ## Албан татварт хамрагдсан автотээврийн болон өөрөө явагч хэрэгслийн мэдээлэл
        self.fill_vehicle(vehicles, ws, rowx, col_aph, thin_border,sheet)

        ws['%s%s' % ('I', 61)].font = ws['%s%s' % ('I', 61)].font.copy(size=8, bold=False,
                                                                                name='Times New Roman')

        sheet = wb.active
        rows = sheet.rows
        for row in rows:
            for cell in row:
                if value_dict.has_key(cell.value):
                    cell.value = value_dict[cell.value]

        wb.save(generate_file)

        file_name = "ТТ04_%s.xlsx" % (time.strftime('%Y%m%d_%H%M'),)
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('TT04'),
                                                                                     form_title=file_name).create({})
        image = open(generate_file, 'rb')
        image_read = image.read()
        report_excel_output_obj.filedata = base64.encodestring(image_read)
        return report_excel_output_obj.export_report()

    # Албан татварт хамрагдсан автотээврийн болон өөрөө явагч хэрэгслийн мэдээлэл
    def fill_vehicle(self, vehicles, ws, rowx, col_aph, thin_border,sheet):

        rowx += 2
        ws.merge_cells('C%s:I%s' % (rowx, rowx))
        ws['%s%s' % ('C', rowx)] = u'III. Албан татварт хамрагдсан автотээврийн болон өөрөө явагч хэрэгслийн мэдээлэл '

        ws['%s%s' % ('C', rowx)].font = ws['%s%s' % ('C', rowx)].font.copy(size=12, bold=True,
                                                                                         name='Times New Roman')

        rowx += 2

        ws.merge_cells('A%s:A%s' % (rowx, rowx))
        ws.merge_cells('B%s:C%s' % (rowx, rowx))
        ws.merge_cells('D%s:E%s' % (rowx, rowx))
        ws.merge_cells('F%s:F%s' % (rowx, rowx))
        ws.merge_cells('G%s:G%s' % (rowx, rowx))
        ws.merge_cells('H%s:H%s' % (rowx, rowx))
        ws.merge_cells('I%s:J%s' % (rowx, rowx))
        ws.merge_cells('K%s:K%s' % (rowx, rowx))
        ws.merge_cells('L%s:L%s' % (rowx, rowx))
        ws.merge_cells('M%s:M%s' % (rowx, rowx))
        ws['%s%s' % ('A', rowx)].border = thin_border
        ws['%s%s' % ('B', rowx)].border = thin_border
        ws['%s%s' % ('D', rowx)].border = thin_border
        ws['%s%s' % ('F', rowx)].border = thin_border
        ws['%s%s' % ('G', rowx)].border = thin_border
        ws['%s%s' % ('H', rowx)].border = thin_border
        ws['%s%s' % ('I', rowx)].border = thin_border
        ws['%s%s' % ('K', rowx)].border = thin_border
        ws['%s%s' % ('L', rowx)].border = thin_border
        ws['%s%s' % ('M', rowx)].border = thin_border

        ws['%s%s' % ('A', rowx)] = u'Д/Д'
        ws['%s%s' % ('B', rowx)] = u'АТБЄЯХ-ийн тєрєл'
        ws['%s%s' % ('D', rowx)] = u'АТБЄЯХ-ийн нэр марк'
        ws['%s%s' % ('F', rowx)] = u'Улсын дугаар'
        ws['%s%s' % ('G', rowx)] = u'АТБЄЯХ-ийн арлын дугаар'
        ws['%s%s' % ('H', rowx)] = u'АТБӨЯХ, Агаарын бохирдлын төлбөрийн гэрчилгээний дугаар'
        ws['%s%s' % ('I', rowx)] = u'Суудлын машины цил.багтаамж, ачааны машин,чиргїїлийн даац(тн)'
        ws['%s%s' % ('K', rowx)] = u'Татварын хувь хэмжээ'
        ws['%s%s' % ('L', rowx)] = u'Ногдох татвар'
        ws['%s%s' % ('M', rowx)] = u'Тусгай тэмдэглэл'

        ws['%s%s' % ('A', rowx)].font = ws['%s%s' % ('A', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        sheet.row_dimensions[rowx].height = 80
        ws['%s%s' % ('A', rowx)].alignment = Alignment(vertical='center', horizontal='center', wrap_text=True)
        ws['%s%s' % ('B', rowx)].font = ws['%s%s' % ('B', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        sheet.row_dimensions[rowx].height = 80
        ws['%s%s' % ('B', rowx)].alignment = Alignment(vertical='center', horizontal='center', wrap_text=True)
        ws['%s%s' % ('D', rowx)].font = ws['%s%s' % ('D', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        sheet.row_dimensions[rowx].height = 80
        ws['%s%s' % ('D', rowx)].alignment = Alignment(vertical='center', horizontal='center', wrap_text=True)
        ws['%s%s' % ('F', rowx)].font = ws['%s%s' % ('F', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        sheet.row_dimensions[rowx].height = 80
        ws['%s%s' % ('F', rowx)].alignment = Alignment(vertical='center', horizontal='center', wrap_text=True)
        ws['%s%s' % ('G', rowx)].font = ws['%s%s' % ('G', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        sheet.row_dimensions[rowx].height = 80
        ws['%s%s' % ('G', rowx)].alignment = Alignment(vertical='center', horizontal='center', wrap_text=True)
        ws['%s%s' % ('H', rowx)].font = ws['%s%s' % ('H', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        sheet.row_dimensions[rowx].height = 80
        ws['%s%s' % ('H', rowx)].alignment = Alignment(vertical='center', horizontal='center', wrap_text=True)
        ws['%s%s' % ('I', rowx)].font = ws['%s%s' % ('I', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        sheet.row_dimensions[rowx].height = 80
        ws['%s%s' % ('I', rowx)].alignment = Alignment(vertical='center', horizontal='center', wrap_text=True, text_rotation=90)
        ws['%s%s' % ('K', rowx)].font = ws['%s%s' % ('K', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        sheet.row_dimensions[rowx].height = 80
        ws['%s%s' % ('K', rowx)].alignment = Alignment(vertical='center', horizontal='center', wrap_text=True)
        ws['%s%s' % ('L', rowx)].font = ws['%s%s' % ('L', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        sheet.row_dimensions[rowx].height = 80
        ws['%s%s' % ('L', rowx)].alignment = Alignment(vertical='center', horizontal='center', wrap_text=True)
        ws['%s%s' % ('M', rowx)].font = ws['%s%s' % ('M', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        sheet.row_dimensions[rowx].height = 80
        ws['%s%s' % ('M', rowx)].alignment = Alignment(vertical='center', horizontal='center', wrap_text=True)

        rowx+=1


        if vehicles:
            number = 0
            for vehicle in vehicles:
                number += 1
                ws.merge_cells('A%s:A%s' % (rowx, rowx))
                ws.merge_cells('B%s:C%s' % (rowx, rowx))
                ws.merge_cells('D%s:E%s' % (rowx, rowx))
                ws.merge_cells('F%s:F%s' % (rowx, rowx))
                ws.merge_cells('G%s:G%s' % (rowx, rowx))
                ws.merge_cells('H%s:H%s' % (rowx, rowx))
                ws.merge_cells('I%s:J%s' % (rowx, rowx))
                ws.merge_cells('K%s:K%s' % (rowx, rowx))
                ws.merge_cells('L%s:L%s' % (rowx, rowx))
                ws.merge_cells('M%s:M%s' % (rowx, rowx))
                ws['%s%s' % ('A', rowx)].border = thin_border
                ws['%s%s' % ('B', rowx)].border = thin_border
                ws['%s%s' % ('D', rowx)].border = thin_border
                ws['%s%s' % ('F', rowx)].border = thin_border
                ws['%s%s' % ('G', rowx)].border = thin_border
                ws['%s%s' % ('H', rowx)].border = thin_border
                ws['%s%s' % ('I', rowx)].border = thin_border
                ws['%s%s' % ('K', rowx)].border = thin_border
                ws['%s%s' % ('L', rowx)].border = thin_border
                ws['%s%s' % ('M', rowx)].border = thin_border

                ws['%s%s' % ('A', rowx)] = number
                ws['%s%s' % ('B', rowx)] = vehicle['vehicle_type']
                ws['%s%s' % ('D', rowx)] = vehicle['vehicle_name']
                ws['%s%s' % ('F', rowx)] = vehicle['vehicle_reg_code']
                ws['%s%s' % ('G', rowx)] = vehicle['vehicle_reg_plates']
                ws['%s%s' % ('H', rowx)] = vehicle['airpol_cer_number']
                if vehicle['tracker_capacity']:
                    ws['%s%s' % ('I', rowx)] = vehicle['tracker_capacity']
                elif vehicle['truck_load']:
                    ws['%s%s' % ('I', rowx)] = dict(self.env['account.vehicle.tax']._fields['truck_load'].selection).get(vehicle['truck_load'])
                elif vehicle['car_capacity']:
                    ws['%s%s' % ('I', rowx)] = dict(self.env['account.vehicle.tax']._fields['car_capacity'].selection).get(vehicle['car_capacity'])
                else:
                    ws['%s%s' % ('I', rowx)] = ''
                ws['%s%s' % ('K', rowx)] = vehicle['taxation']
                if vehicle['tracker_capacity']:
                    truck_load = int(vehicle['taxation']) * int(vehicle['tracker_capacity'])
                    ws['%s%s' % ('L', rowx)] = truck_load
                elif vehicle['passenger_capacity']:
                    passen_capacity = int(vehicle['taxation']) * int(vehicle['passenger_capacity'])
                    ws['%s%s' % ('L', rowx)] = passen_capacity
                else:
                    ws['%s%s' % ('L', rowx)] = vehicle['taxation']
                ws['%s%s' % ('M', rowx)] = vehicle['comment']

                ws['%s%s' % ('A', rowx)].font = ws['%s%s' % ('A', rowx)].font.copy(size=8, bold=True,
                                                                                   name='Times New Roman')
                ws['%s%s' % ('A', rowx)].alignment = Alignment(horizontal='center')
                ws['%s%s' % ('B', rowx)].font = ws['%s%s' % ('B', rowx)].font.copy(size=8, bold=True,
                                                                                   name='Times New Roman')
                ws['%s%s' % ('B', rowx)].alignment = Alignment(horizontal='center')
                ws['%s%s' % ('D', rowx)].font = ws['%s%s' % ('D', rowx)].font.copy(size=8, bold=True,
                                                                                   name='Times New Roman')
                ws['%s%s' % ('D', rowx)].alignment = Alignment(horizontal='center')
                ws['%s%s' % ('F', rowx)].font = ws['%s%s' % ('F', rowx)].font.copy(size=8, bold=True,
                                                                                   name='Times New Roman')
                ws['%s%s' % ('F', rowx)].alignment = Alignment(horizontal='center')
                ws['%s%s' % ('G', rowx)].font = ws['%s%s' % ('G', rowx)].font.copy(size=8, bold=True,
                                                                                   name='Times New Roman')
                ws['%s%s' % ('G', rowx)].alignment = Alignment(horizontal='center')
                ws['%s%s' % ('H', rowx)].font = ws['%s%s' % ('H', rowx)].font.copy(size=8, bold=True,
                                                                                   name='Times New Roman')
                ws['%s%s' % ('H', rowx)].alignment = Alignment(horizontal='center')
                ws['%s%s' % ('I', rowx)].font = ws['%s%s' % ('I', rowx)].font.copy(size=8, bold=True,
                                                                                   name='Times New Roman')
                ws['%s%s' % ('I', rowx)].alignment = Alignment(horizontal='center')
                ws['%s%s' % ('K', rowx)].font = ws['%s%s' % ('K', rowx)].font.copy(size=8, bold=True,
                                                                                   name='Times New Roman')
                ws['%s%s' % ('K', rowx)].alignment = Alignment(horizontal='center')
                ws['%s%s' % ('L', rowx)].font = ws['%s%s' % ('L', rowx)].font.copy(size=8, bold=True,
                                                                                   name='Times New Roman')
                ws['%s%s' % ('L', rowx)].alignment = Alignment(horizontal='center')
                ws['%s%s' % ('M', rowx)].font = ws['%s%s' % ('M', rowx)].font.copy(size=8, bold=True,
                                                                                   name='Times New Roman')
                ws['%s%s' % ('M', rowx)].alignment = Alignment(horizontal='center')
                rowx += 1
        return rowx

    #I. Албан татвар ногдуулалт.
    def get_taxations(self, taxations, ws, rowx, col_aph, thin_border):
        if taxations:
            number = 0
            vehicle_quantity = {}
            vehicle_taxation = {}
            check_district_dict = []
            wheel_2_q = 0;truck_1_q = 0;truck_2_q = 0;truck_3_q = 0;truck_4_q = 0;truck_5_q = 0;truck_6_q = 0;truck_7_q = 0;truck_8_q = 0;truck_9_q = 0;
            wheel_3_q = 0;truck_10_q = 0;truck_11_q = 0;truck_12_q = 0;truck_13_q = 0;truck_14_q = 0;truck_15_q = 0;truck_16_q = 0;car_cyl_1_q = 0;
            car_cyl_2_q = 0;micro_q = 0;bus_q = 0;emergency_q = 0;tractor_q = 0;tractor_s_q = 0;tractor_load_q = 0;car_cyl_3_q = 0;total_q = 0;
            wheel_2 = 0;truck_1 = 0;truck_2 = 0;truck_3 = 0;truck_4 = 0;truck_5 = 0;truck_6 = 0;truck_7 = 0;truck_8 = 0;truck_9 = 0;wheel_3 = 0;
            truck_10 = 0;truck_11 = 0;truck_12 = 0;truck_13 = 0;truck_14 = 0;truck_15 = 0;truck_16 = 0;car_cyl_1 = 0;car_cyl_2 = 0;
            micro = 0;bus = 0;emergency = 0;tractor = 0;tractor_s = 0;tractor_load = 0;car_cyl_3 = 0;total = 0;

            # 30 аймаг болон дүүрэг тус бүрийн /Аймаг, Нийслэлийн иргэдийн хурлаас тогтоосон албан татварын хэмжээ/ болон /Тоо ширхэг/ мэдээлэл олох хэсэг
            for taxation in taxations:
                if taxation['vehicle_type'] == '2_wheeled':
                    wheel_2_q +=1
                    wheel_2 += taxation['taxation']
                    vehicle_quantity[0] = wheel_2_q
                    vehicle_taxation[0] = wheel_2
                    check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                elif taxation['vehicle_type'] == '3_wheeled':
                    wheel_3_q += 1
                    wheel_3 += taxation['taxation']
                    vehicle_quantity[1] = wheel_3_q
                    vehicle_taxation[1] = wheel_3
                    check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                elif taxation['vehicle_type'] == 'passenger_car':
                    if taxation['car_capacity'] == 'car_cube_one':
                        car_cyl_1_q += 1
                        car_cyl_1 += (int(taxation['taxation']) * int(taxation['passenger_capacity']))
                        vehicle_quantity[3] = car_cyl_1_q
                        vehicle_taxation[3] = car_cyl_1
                        check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                    elif taxation['car_capacity'] == 'car_cube_two':
                        car_cyl_2_q += 1
                        car_cyl_2 += (int(taxation['taxation']) * int(taxation['passenger_capacity']))
                        vehicle_quantity[4] = car_cyl_2_q
                        vehicle_taxation[4] = car_cyl_2
                        check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                    elif taxation['car_capacity'] == 'car_cube_three':
                        car_cyl_3_q += 1
                        car_cyl_3 += (int(taxation['taxation']) * int(taxation['passenger_capacity']))
                        vehicle_quantity[5] = car_cyl_3_q
                        vehicle_taxation[5] = car_cyl_3
                        check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                elif taxation['vehicle_type'] == 'micro_bus':
                    micro_q += 1
                    micro += taxation['taxation']
                    vehicle_quantity[6] = micro_q
                    vehicle_taxation[6] = micro
                    check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                elif taxation['vehicle_type'] == 'bus':
                    bus_q += 1
                    bus += taxation['taxation']
                    vehicle_quantity[7] = bus_q
                    vehicle_taxation[7] = bus
                    check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                elif taxation['vehicle_type'] == 'truck':
                    if taxation['truck_load'] == '1_ton':
                        truck_1_q += 1
                        truck_1 += taxation['taxation']
                        vehicle_quantity[9] = truck_1_q
                        vehicle_taxation[9] = truck_1
                        check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                    elif taxation['truck_load'] == '1_2_ton':
                        truck_2_q += 1
                        truck_2 += taxation['taxation']
                        vehicle_quantity[10] = truck_2_q
                        vehicle_taxation[10] = truck_2
                        check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                    elif taxation['truck_load'] == '2_3_ton':
                        truck_3_q += 1
                        truck_3 += taxation['taxation']
                        vehicle_quantity[11] = truck_3_q
                        vehicle_taxation[11] = truck_3
                        check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                    elif taxation['truck_load'] == '3_5_ton':
                        truck_4_q += 1
                        truck_4 += taxation['taxation']
                        vehicle_quantity[12] = truck_4_q
                        vehicle_taxation[12] = truck_4
                        check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                    elif taxation['truck_load'] == '5_8_ton':
                        truck_5_q += 1
                        truck_5 += taxation['taxation']
                        vehicle_quantity[13] = truck_5_q
                        vehicle_taxation[13] = truck_5
                        check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                    elif taxation['truck_load'] == '8_10_ton':
                        truck_6_q += 1
                        truck_6 += taxation['taxation']
                        vehicle_quantity[14] = truck_6_q
                        vehicle_taxation[14] = truck_6
                        check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                    elif taxation['truck_load'] == '10_20_ton':
                        truck_7_q += 1
                        truck_7 += taxation['taxation']
                        vehicle_quantity[15] = truck_7_q
                        vehicle_taxation[15] = truck_7
                        check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                    elif taxation['truck_load'] == '20_30_ton':
                        truck_8_q += 1
                        truck_8 += taxation['taxation']
                        vehicle_quantity[16] = truck_8_q
                        vehicle_taxation[16] = truck_8
                        check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                    elif taxation['truck_load'] == '30_40_ton':
                        truck_9_q += 1
                        truck_9 += taxation['taxation']
                        vehicle_quantity[17] = truck_9_q
                        vehicle_taxation[17] = truck_9
                        check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                    elif taxation['truck_load'] == '40_50_ton':
                        truck_10_q += 1
                        truck_10 += taxation['taxation']
                        vehicle_quantity[18] = truck_10_q
                        vehicle_taxation[18] = truck_10
                        check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                    elif taxation['truck_load'] == '50_60_ton':
                        truck_11_q += 1
                        truck_11 += taxation['taxation']
                        vehicle_quantity[19] = truck_11_q
                        vehicle_taxation[19] = truck_11
                        check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                    elif taxation['truck_load'] == '60_70_ton':
                        truck_12_q += 1
                        truck_12 += taxation['taxation']
                        vehicle_quantity[20] = truck_12_q
                        vehicle_taxation[20] = truck_12
                        check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                    elif taxation['truck_load'] == '70_80_ton':
                        truck_13_q += 1
                        truck_13 += taxation['taxation']
                        vehicle_quantity[21] = truck_13_q
                        vehicle_taxation[21] = truck_13
                        check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                    elif taxation['truck_load'] == '80_90_ton':
                        truck_14_q += 1
                        truck_14 += taxation['taxation']
                        vehicle_quantity[22] = truck_14_q
                        vehicle_taxation[22] = truck_14
                        check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                    elif taxation['truck_load'] == '90_100_ton':
                        truck_15_q += 1
                        truck_15 += taxation['taxation']
                        vehicle_quantity[23] = truck_15_q
                        vehicle_taxation[23] = truck_15
                        check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                    elif taxation['truck_load'] == '100_ton':
                        truck_16_q += 1
                        truck_16 += taxation['taxation']
                        vehicle_quantity[24] = truck_16_q
                        vehicle_taxation[24] = truck_16
                        check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                elif taxation['vehicle_type'] == 'em_vehicle':
                    emergency_q += 1
                    emergency += taxation['taxation']
                    vehicle_quantity[25] = emergency_q
                    vehicle_taxation[25] = emergency
                    check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                elif taxation['vehicle_type'] == 'other_tractor':
                    tractor_s_q += 1
                    tractor_s += taxation['taxation']
                    vehicle_quantity[26] = tractor_s_q
                    vehicle_taxation[26] = tractor_s
                    check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                elif taxation['vehicle_type'] == 'compact_tractor':
                    tractor_q += 1
                    tractor += taxation['taxation']
                    vehicle_quantity[27] = tractor_q
                    vehicle_taxation[27] = tractor
                    check_district_dict = self.check_district_quantity(taxation, check_district_dict)
                elif taxation['vehicle_type'] == 'tracker_load':
                    tractor_load_q += 1
                    tractor_load += (int(taxation['taxation']) * int(taxation['tracker_capacity']))
                    vehicle_quantity[28] = tractor_load_q
                    vehicle_taxation[28] = tractor_load
                    check_district_dict = self.check_district_quantity(taxation, check_district_dict)

            vehicle_type = ['2 дугуйтай мотоцикл /2000-3000 ,  1800-2700/',' 3 дугуйтай мотоцикл /3000-4500 , 2800-4200/','Суудлын автомашин цилиндрийн багтаамжаар: (4+5+6)',
                            'А. 2000 см куб хүртэл /16-24, 14-21/',' Б. 2001-3000 см куб /18-27 , 16-24/',' В. 3001 дээш см куб /22-33 , 20-30/',
                            'Микро автобус /15 хүний суудалтай/ /35000-52500 , 28000-42000/' , 'Автобус /52000-78000 , 40000-60000/','Ачааны автомашин даацаар:(10+11+12+13+14+15+16+17+18+19+20+21+22+23+24+25)',
                            ' 1. 1 хүртэл тн даацтай /25000-37500 , 20000-30000/','2. 1-2 тн даацтай /35000-52500 , 28000-42000/',' 3. 2-3 тн даацтай /45000-67500 , 36000-54000/',
                            ' 4. 3-5 тн даацтай /55000-82500 , 44000-66000/',' 5. 5-8 тн даацтай /80000-120000 , 64000-96000/',' 6. 8-10 тн даацтай /90000-135000 , 72000-108000/'
                            ,'7. 10-20 тн даацтай /100000-150000 , 80000-120000/','8. 20-30 тн даацтай /140000-210000 , 120000-180000/','9. 30-40 тн даацтай /180000-270000 , 160000-240000/'
                            ,'10. 40-50 тн даацтай /220000-330000 , 200000-300000/','11. 50-60 тн даацтай /280000-420000 , 240000-360000/','12. 60-70 тн даацтай /320000-480000 , 280000-420000/'
                            ,'13. 70-80 тн даацтай /360000-540000 , 320000-480000/','14. 80-90 тн даацтай /400000-600000 , 380000-570000/','15. 90-100 тн даацтай/440000-660000 , 420000-630000/'
                            ,'16. 100 тн-оос дээш даацтай /480000-720000 , 440000-660000/',' Тусгай зориулалтын автомашин /16000-24000 , 15000-22500/',' Трактор өөрөө явагч бусад хэрэгсэл /14000-21000 , 11200-16800/'
                            ,' Бага оврын трактор /7000-10500 ,  5600-8400 /',' Чиргүүл  даацын /1 тн тутамд/ /5500-8250 , 5500-8250/','Төлбөл зохих татварын дүн:  (1+2+3+7+8+9+26+27+28+29)']

            for item in xrange(0,len(vehicle_type)):
                number += 1
                ws.merge_cells('A%s:E%s' % (rowx, rowx))
                ws['%s%s' % ('A', rowx)].border = thin_border
                ws['%s%s' % (col_aph[5], rowx)].border = thin_border
                ws['%s%s' % (col_aph[6], rowx)].border = thin_border
                ws['%s%s' % (col_aph[7], rowx)].border = thin_border
                ws['%s%s' % (col_aph[8], rowx)].border = thin_border
                ws['%s%s' % (col_aph[9], rowx)].border = thin_border

                ws['%s%s' % ('A', rowx)] = vehicle_type[item]
                ws['%s%s' % (col_aph[5], rowx)] = ''
                ws['%s%s' % (col_aph[6], rowx)] = number
                ws['%s%s' % (col_aph[7], rowx)] = ''
                if item in vehicle_quantity:
                    ws['%s%s' % (col_aph[8], rowx)] = vehicle_quantity[item]
                    total_q += vehicle_quantity[item]
                else :
                    ws['%s%s' % (col_aph[8], rowx)] = 0
                if item in vehicle_taxation:
                    ws['%s%s' % (col_aph[9], rowx)] = vehicle_taxation[item]
                    total += vehicle_taxation[item]
                else:
                    ws['%s%s' % (col_aph[9], rowx)] = 0
                if item == 2:
                    passenger_total = 0;passenger_total_q = 0;
                    for idx in xrange(3, 5):
                        if idx in vehicle_quantity:
                            passenger_total_q += vehicle_quantity[idx]
                            ws['%s%s' % (col_aph[8], rowx)] = passenger_total_q
                        if idx in vehicle_taxation:
                            passenger_total += vehicle_taxation[idx]
                            ws['%s%s' % (col_aph[9], rowx)] = passenger_total
                if item == 8:
                    truck_load_total = 0; truck_load_total_q = 0;
                    for idx in xrange(9, 24):
                        if idx in vehicle_quantity:
                            truck_load_total_q +=vehicle_quantity[idx]
                            ws['%s%s' % (col_aph[8], rowx)] = truck_load_total_q
                        if idx in vehicle_taxation:
                            truck_load_total += vehicle_taxation[idx]
                            ws['%s%s' % (col_aph[9], rowx)] = truck_load_total
                if item == 29:
                    ws['%s%s' % (col_aph[8], rowx)] = total_q
                    ws['%s%s' % (col_aph[9], rowx)] = total

                ws['%s%s' % ('A', rowx)].font = ws['%s%s' % ('A', rowx)].font.copy(size=8, bold=True,
                                                                                                 name='Times New Roman')
                ws['%s%s' % ('A', rowx)].alignment = Alignment(horizontal='center')
                ws['%s%s' % (col_aph[5], rowx)].font = ws['%s%s' % (col_aph[5], rowx)].font.copy(size=8, bold=False,
                                                                                                 name='Times New Roman')
                ws['%s%s' % (col_aph[5], rowx)].alignment = Alignment(horizontal='center')
                ws['%s%s' % (col_aph[6], rowx)].font = ws['%s%s' % (col_aph[6], rowx)].font.copy(size=8, bold=False,
                                                                                                 name='Times New Roman')
                ws['%s%s' % (col_aph[6], rowx)].alignment = Alignment(horizontal='center')
                ws['%s%s' % (col_aph[7], rowx)].font = ws['%s%s' % (col_aph[7], rowx)].font.copy(size=8, bold=False,
                                                                                                 name='Times New Roman')
                ws['%s%s' % (col_aph[7], rowx)].alignment = Alignment(horizontal='center')
                ws['%s%s' % (col_aph[8], rowx)].font = ws['%s%s' % (col_aph[8], rowx)].font.copy(size=8, bold=False,
                                                                                                 name='Times New Roman')
                ws['%s%s' % (col_aph[8], rowx)].alignment = Alignment(horizontal='center')
                ws['%s%s' % (col_aph[9], rowx)].font = ws['%s%s' % (col_aph[9], rowx)].font.copy(size=8, bold=False,
                                                                                                 name='Times New Roman')
                ws['%s%s' % (col_aph[9], rowx)].alignment = Alignment(horizontal='center')
                rowx += 1

                for check_district in check_district_dict:
                    if check_district['vehicle_type'] == '2_wheeled' and item == 0:
                        rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                    elif check_district['vehicle_type'] == '3_wheeled' and item == 1:
                        rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                    elif check_district['vehicle_type'] == 'tracker_load' and item == 28:
                        rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                    elif check_district['vehicle_type'] == 'micro_bus' and item == 6:
                        rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                    elif check_district['vehicle_type'] == 'bus' and item == 7:
                        rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                    elif check_district['vehicle_type'] == 'em_vehicle' and item == 5:
                        rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                    elif check_district['vehicle_type'] == 'compact_tractor' and item == 27:
                        rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                    elif check_district['vehicle_type'] == 'other_tractor' and item == 26:
                        rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                    elif check_district['vehicle_type'] == 'truck':
                        if check_district['truck_load'] == '1_ton' and item == 9:
                            rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                        elif check_district['truck_load'] == '1_2_ton' and item == 10:
                            rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                        elif check_district['truck_load'] == '2_3_ton' and item == 11:
                            rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                        elif check_district['truck_load'] == '3_5_ton' and item == 12:
                            rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                        elif check_district['truck_load'] == '5_8_ton' and item == 13:
                            rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                        elif check_district['truck_load'] == '8_10_ton' and item == 14:
                            rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                        elif check_district['truck_load'] == '10_20_ton' and item == 15:
                            rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                        elif check_district['truck_load'] == '20_30_ton' and item == 16:
                            rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                        elif check_district['truck_load'] == '30_40_ton' and item == 17:
                            rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                        elif check_district['truck_load'] == '40_50_ton' and item == 18:
                            rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                        elif check_district['truck_load'] == '50_60_ton' and item == 19:
                            rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                        elif check_district['truck_load'] == '60_70_ton' and item == 20:
                            rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                        elif check_district['truck_load'] == '70_80_ton' and item == 21:
                            rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                        elif check_district['truck_load'] == '80_90_ton' and item == 22:
                            rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                        elif check_district['truck_load'] == '90_100_ton' and item == 23:
                            rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                        elif check_district['truck_load'] == '100_ton' and item == 24:
                            rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                    elif check_district['vehicle_type'] == 'passenger_car':
                        if check_district['car_capacity'] == 'car_cube_one' and item == 3:
                            rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                        elif check_district['car_capacity'] == 'car_cube_two' and item == 4:
                            rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
                        elif check_district['car_capacity'] == 'car_cube_three' and item == 5:
                            rowx = self.get_location_infos(check_district, ws, rowx, col_aph, thin_border)
        return rowx

    # 30 аймаг болон дүүрэг тус бүрийн /Аймаг, Нийслэлийн иргэдийн хурлаас тогтоосон албан татварын хэмжээ/ болон /Тоо ширхэг/ хэвлэх
    def get_location_infos(self,taxation, ws, rowx, col_aph, thin_border):
        ws.merge_cells('A%s:E%s' % (rowx, rowx))
        ws['%s%s' % ('A', rowx)].border = thin_border
        ws['%s%s' % (col_aph[5], rowx)].border = thin_border
        ws['%s%s' % (col_aph[6], rowx)].border = thin_border
        ws['%s%s' % (col_aph[7], rowx)].border = thin_border
        ws['%s%s' % (col_aph[8], rowx)].border = thin_border
        ws['%s%s' % (col_aph[9], rowx)].border = thin_border

        ws['%s%s' % ('A', rowx)] = taxation['name']
        ws['%s%s' % (col_aph[5], rowx)] = ''
        ws['%s%s' % (col_aph[6], rowx)] = ''
        ws['%s%s' % (col_aph[7], rowx)] = taxation['taxation']
        ws['%s%s' % (col_aph[8], rowx)] = taxation['quantity']
        ws['%s%s' % (col_aph[9], rowx)] = ''

        ws['%s%s' % ('A', rowx)].font = ws['%s%s' % ('A', rowx)].font.copy(size=8, bold=False,
                                                                                         name='Times New Roman')
        ws['%s%s' % ('A', rowx)].alignment = Alignment(horizontal='center')
        ws['%s%s' % (col_aph[5], rowx)].font = ws['%s%s' % (col_aph[5], rowx)].font.copy(size=8, bold=False,
                                                                                         name='Times New Roman')
        ws['%s%s' % (col_aph[5], rowx)].alignment = Alignment(horizontal='center')
        ws['%s%s' % (col_aph[6], rowx)].font = ws['%s%s' % (col_aph[6], rowx)].font.copy(size=8, bold=False,
                                                                                         name='Times New Roman')
        ws['%s%s' % (col_aph[6], rowx)].alignment = Alignment(horizontal='center')
        ws['%s%s' % (col_aph[7], rowx)].font = ws['%s%s' % (col_aph[7], rowx)].font.copy(size=8, bold=False,
                                                                                         name='Times New Roman')
        ws['%s%s' % (col_aph[7], rowx)].alignment = Alignment(horizontal='center')
        ws['%s%s' % (col_aph[8], rowx)].font = ws['%s%s' % (col_aph[8], rowx)].font.copy(size=8, bold=False,
                                                                                         name='Times New Roman')
        ws['%s%s' % (col_aph[8], rowx)].alignment = Alignment(horizontal='center')
        ws['%s%s' % (col_aph[9], rowx)].font = ws['%s%s' % (col_aph[9], rowx)].font.copy(size=8, bold=False,
                                                                                         name='Times New Roman')
        ws['%s%s' % (col_aph[9], rowx)].alignment = Alignment(horizontal='center')

        rowx += 1
        return rowx

    #II. Албан татвар төлөлтийн тооцоолол
    def get_tax_calculation(self , ws, rowx, col_aph ,thin_border,sheet,underbalance,overbalance):

        rowx += 2
        ws['%s%s' % (col_aph[0], rowx)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=12, bold=False,
                                                                                         name='Times New Roman')
        ws['%s%s' % (col_aph[0], rowx)] = u"II. Албан татвар төлөлтийн тооцоолол"
        rowx += 2

        ws.merge_cells('A%s:D%s' % (rowx, rowx))
        ws.merge_cells('E%s:F%s' % (rowx, rowx))
        ws.merge_cells('G%s:H%s' % (rowx, rowx))
        ws.merge_cells('I%s:J%s' % (rowx, rowx))
        ws['%s%s' % ('A', rowx)].border = thin_border
        ws['%s%s' % ('E', rowx)].border = thin_border
        ws['%s%s' % ('G', rowx)].border = thin_border
        ws['%s%s' % ('I', rowx)].border = thin_border
        ws['%s%s' % ('J', rowx)].border = thin_border
        ws['%s%s' % ('A', rowx)] = u'Үзүүлэлтийн нэр'
        ws['%s%s' % ('E', rowx)] = u'Мөрийн дугаар'
        ws['%s%s' % ('G', rowx)] = u'Дебет'
        ws['%s%s' % ('I', rowx)] = u'Кредит'

        ws['%s%s' % ('A', rowx)].font = ws['%s%s' % ('A', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('A', rowx)].alignment = Alignment(vertical='center', horizontal='center', wrap_text=True)
        ws['%s%s' % ('E', rowx)].font = ws['%s%s' % ('E', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('E', rowx)].alignment = Alignment(horizontal='center')
        ws['%s%s' % ('G', rowx)].font = ws['%s%s' % ('G', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('G', rowx)].alignment = Alignment(horizontal='center')
        ws['%s%s' % ('I', rowx)].font = ws['%s%s' % ('I', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('I', rowx)].alignment = Alignment(horizontal='center')
        rowx += 1

        ws.merge_cells('A%s:B%s' % (rowx, rowx + 1))
        ws.merge_cells('C%s:D%s' % (rowx, rowx))
        ws.merge_cells('E%s:F%s' % (rowx, rowx))
        ws.merge_cells('G%s:H%s' % (rowx, rowx))
        ws.merge_cells('I%s:J%s' % (rowx, rowx))
        ws['%s%s' % ('A', rowx)].border = thin_border
        ws['%s%s' % ('C', rowx)].border = thin_border
        ws['%s%s' % ('E', rowx)].border = thin_border
        ws['%s%s' % ('G', rowx)].border = thin_border
        ws['%s%s' % ('I', rowx)].border = thin_border
        ws['%s%s' % ('J', rowx)].border = thin_border

        ws['%s%s' % ('A', rowx)] = u'Оны эхний үлдэгдэл'
        ws['%s%s' % ('C', rowx)] = u'Илүү'
        ws['%s%s' % ('E', rowx)] = u'31'
        ws['%s%s' % ('G', rowx)] = ''
        ws['%s%s' % ('I', rowx)] = overbalance

        ws['%s%s' % ('A', rowx)].font = ws['%s%s' % ('A', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        sheet.row_dimensions[rowx + 1].height = 30
        ws['%s%s' % ('A', rowx)].alignment = Alignment(horizontal='center', vertical='center',
                                                       wrap_text=True)
        ws['%s%s' % ('C', rowx)].font = ws['%s%s' % ('C', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('C', rowx)].alignment = Alignment(horizontal='center', vertical='center',
                                                       wrap_text=True)
        ws['%s%s' % ('E', rowx)].font = ws['%s%s' % ('E', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('E', rowx)].alignment = Alignment(horizontal='center', vertical='center',
                                                       wrap_text=True)
        ws['%s%s' % ('G', rowx)].font = ws['%s%s' % ('G', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('G', rowx)].alignment = Alignment(horizontal='center', vertical='center',
                                                       wrap_text=True)
        ws['%s%s' % ('I', rowx)].font = ws['%s%s' % ('I', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('I', rowx)].alignment = Alignment(horizontal='center', vertical='center',
                                                       wrap_text=True)
        rowx += 1

        ws.merge_cells('C%s:D%s' % (rowx, rowx))
        ws.merge_cells('E%s:F%s' % (rowx, rowx))
        ws.merge_cells('G%s:H%s' % (rowx, rowx))
        ws.merge_cells('I%s:J%s' % (rowx, rowx))
        ws['%s%s' % ('C', rowx)].border = thin_border
        ws['%s%s' % ('E', rowx)].border = thin_border
        ws['%s%s' % ('G', rowx)].border = thin_border
        ws['%s%s' % ('I', rowx)].border = thin_border
        ws['%s%s' % ('J', rowx)].border = thin_border

        ws['%s%s' % ('C', rowx)] = u'Дутуу'
        ws['%s%s' % ('E', rowx)] = u'32'
        ws['%s%s' % ('G', rowx)] = underbalance
        ws['%s%s' % ('I', rowx)] = ''

        ws['%s%s' % ('C', rowx)].font = ws['%s%s' % ('C', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('C', rowx)].alignment = Alignment(horizontal='center', vertical='center',
                                                       wrap_text=True)
        ws['%s%s' % ('E', rowx)].font = ws['%s%s' % ('E', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('E', rowx)].alignment = Alignment(horizontal='center', vertical='center',
                                                       wrap_text=True)
        ws['%s%s' % ('G', rowx)].font = ws['%s%s' % ('G', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('G', rowx)].alignment = Alignment(horizontal='center', vertical='center',
                                                       wrap_text=True)
        ws['%s%s' % ('I', rowx)].font = ws['%s%s' % ('I', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('I', rowx)].alignment = Alignment(horizontal='center', vertical='center',
                                                       wrap_text=True)
        rowx += 1

        ws.merge_cells('A%s:D%s' % (rowx, rowx))
        ws.merge_cells('E%s:F%s' % (rowx, rowx))
        ws.merge_cells('G%s:H%s' % (rowx, rowx))
        ws.merge_cells('I%s:J%s' % (rowx, rowx))
        ws['%s%s' % ('A', rowx)].border = thin_border
        ws['%s%s' % ('E', rowx)].border = thin_border
        ws['%s%s' % ('G', rowx)].border = thin_border
        ws['%s%s' % ('I', rowx)].border = thin_border
        ws['%s%s' % ('J', rowx)].border = thin_border
        ws['%s%s' % ('A', rowx)] = u'Тайлант хугацаанд ногдуулсан албан татвар'
        ws['%s%s' % ('E', rowx)] = u'33'
        ws['%s%s' % ('G', rowx)] = '=+J%s' % (rowx-8)
        ws['%s%s' % ('I', rowx)] = ''

        ws['%s%s' % ('A', rowx)].font = ws['%s%s' % ('A', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('A', rowx)].alignment = Alignment(vertical='center', horizontal='center', wrap_text=True)
        ws['%s%s' % ('E', rowx)].font = ws['%s%s' % ('E', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('E', rowx)].alignment = Alignment(horizontal='center')
        ws['%s%s' % ('G', rowx)].font = ws['%s%s' % ('G', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('G', rowx)].alignment = Alignment(horizontal='center')
        ws['%s%s' % ('I', rowx)].font = ws['%s%s' % ('I', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('I', rowx)].alignment = Alignment(horizontal='center')
        rowx += 1

        ws.merge_cells('A%s:D%s' % (rowx, rowx))
        ws.merge_cells('E%s:F%s' % (rowx, rowx))
        ws.merge_cells('G%s:H%s' % (rowx, rowx))
        ws.merge_cells('I%s:J%s' % (rowx, rowx))
        ws['%s%s' % ('A', rowx)].border = thin_border
        ws['%s%s' % ('E', rowx)].border = thin_border
        ws['%s%s' % ('G', rowx)].border = thin_border
        ws['%s%s' % ('I', rowx)].border = thin_border
        ws['%s%s' % ('J', rowx)].border = thin_border
        ws['%s%s' % ('A', rowx)] = u'Тайлангийн хугацаанд төлсөн албан татвар'
        ws['%s%s' % ('E', rowx)] = u'34'
        ws['%s%s' % ('G', rowx)] = ''
        ws['%s%s' % ('I', rowx)] = ''

        ws['%s%s' % ('A', rowx)].font = ws['%s%s' % ('A', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('A', rowx)].alignment = Alignment(vertical='center', horizontal='center', wrap_text=True)
        ws['%s%s' % ('E', rowx)].font = ws['%s%s' % ('E', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('E', rowx)].alignment = Alignment(horizontal='center')
        ws['%s%s' % ('G', rowx)].font = ws['%s%s' % ('G', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('G', rowx)].alignment = Alignment(horizontal='center')
        ws['%s%s' % ('I', rowx)].font = ws['%s%s' % ('I', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('I', rowx)].alignment = Alignment(horizontal='center')
        rowx += 1

        ws.merge_cells('A%s:D%s' % (rowx, rowx))
        ws.merge_cells('E%s:F%s' % (rowx, rowx))
        ws.merge_cells('G%s:H%s' % (rowx, rowx))
        ws.merge_cells('I%s:J%s' % (rowx, rowx))
        ws['%s%s' % ('A', rowx)].border = thin_border
        ws['%s%s' % ('E', rowx)].border = thin_border
        ws['%s%s' % ('G', rowx)].border = thin_border
        ws['%s%s' % ('I', rowx)].border = thin_border
        ws['%s%s' % ('J', rowx)].border = thin_border
        ws['%s%s' % ('A', rowx)] = u'Буцаан олгосон албан татвар'
        ws['%s%s' % ('E', rowx)] = u'35'
        ws['%s%s' % ('G', rowx)] = ''
        ws['%s%s' % ('I', rowx)] = ''

        ws['%s%s' % ('A', rowx)].font = ws['%s%s' % ('A', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('A', rowx)].alignment = Alignment(vertical='center', horizontal='center', wrap_text=True)
        ws['%s%s' % ('E', rowx)].font = ws['%s%s' % ('E', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('E', rowx)].alignment = Alignment(horizontal='center')
        ws['%s%s' % ('G', rowx)].font = ws['%s%s' % ('G', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('G', rowx)].alignment = Alignment(horizontal='center')
        ws['%s%s' % ('I', rowx)].font = ws['%s%s' % ('I', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('I', rowx)].alignment = Alignment(horizontal='center')
        rowx += 1

        ws.merge_cells('A%s:B%s' % (rowx, rowx + 1))
        ws.merge_cells('C%s:D%s' % (rowx, rowx))
        ws.merge_cells('E%s:F%s' % (rowx, rowx))
        ws.merge_cells('G%s:H%s' % (rowx, rowx))
        ws.merge_cells('I%s:J%s' % (rowx, rowx))
        ws['%s%s' % ('A', rowx)].border = thin_border
        ws['%s%s' % ('C', rowx)].border = thin_border
        ws['%s%s' % ('E', rowx)].border = thin_border
        ws['%s%s' % ('G', rowx)].border = thin_border
        ws['%s%s' % ('I', rowx)].border = thin_border
        ws['%s%s' % ('J', rowx)].border = thin_border

        ws['%s%s' % ('A', rowx)] = u'Тайлангийн хугацааны эцсийн үлдэгдэл'
        ws['%s%s' % ('C', rowx)] = u'Илүү  (32-31+33-34-35)<0'
        ws['%s%s' % ('E', rowx)] = u'36'
        ws['%s%s' % ('G', rowx)] = ''
        ws['%s%s' % ('I', rowx)] = ('=IF((G%s-I%s+G%s-I%s-I%s)<0,(G%s-I%s+G%s-I%s-I%s)*(-1),0)' % (str(rowx-4),str(rowx-5),str(rowx-3),str(rowx-2),str(rowx-1),str(rowx-4),str(rowx-5),str(rowx-3),str(rowx-2),str(rowx-1)))

        ws['%s%s' % ('A', rowx)].font = ws['%s%s' % ('G', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        sheet.row_dimensions[rowx + 1].height = 30
        ws['%s%s' % ('A', rowx)].alignment = Alignment(horizontal='center', vertical='center',
                                                       wrap_text=True)
        ws['%s%s' % ('C', rowx)].font = ws['%s%s' % ('H', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('C', rowx)].alignment = Alignment(horizontal='center', vertical='center',
                                                       wrap_text=True)
        ws['%s%s' % ('E', rowx)].font = ws['%s%s' % ('I', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('E', rowx)].alignment = Alignment(horizontal='center', vertical='center',
                                                       wrap_text=True)
        ws['%s%s' % ('G', rowx)].font = ws['%s%s' % ('J', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('G', rowx)].alignment = Alignment(horizontal='center', vertical='center',
                                                       wrap_text=True)
        ws['%s%s' % ('I', rowx)].font = ws['%s%s' % ('K', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('I', rowx)].alignment = Alignment(horizontal='center', vertical='center',
                                                       wrap_text=True)
        rowx += 1

        ws.merge_cells('C%s:D%s' % (rowx, rowx))
        ws.merge_cells('E%s:F%s' % (rowx, rowx))
        ws.merge_cells('G%s:H%s' % (rowx, rowx))
        ws.merge_cells('I%s:J%s' % (rowx, rowx))
        ws['%s%s' % ('A', rowx)].border = thin_border
        ws['%s%s' % ('C', rowx)].border = thin_border
        ws['%s%s' % ('E', rowx)].border = thin_border
        ws['%s%s' % ('G', rowx)].border = thin_border
        ws['%s%s' % ('I', rowx)].border = thin_border
        ws['%s%s' % ('J', rowx)].border = thin_border

        ws['%s%s' % ('C', rowx)] = u'Дутуу (32-31+33-34-35)>0'
        ws['%s%s' % ('E', rowx)] = u'37'
        ws['%s%s' % ('G', rowx)] = ('=IF((G%s-I%s+G%s-I%s-I%s)>0,(G%s-I%s+G%s-I%s-I%s),0)' % (str(rowx-5),str(rowx-6),str(rowx-4),str(rowx-3),str(rowx-2),str(rowx-5),str(rowx-6),str(rowx-4),str(rowx-3),str(rowx-2)))
        ws['%s%s' % ('I', rowx)] = ''

        ws['%s%s' % ('C', rowx)].font = ws['%s%s' % ('H', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('C', rowx)].alignment = Alignment(horizontal='center', vertical='center',
                                                       wrap_text=True)
        ws['%s%s' % ('E', rowx)].font = ws['%s%s' % ('I', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('E', rowx)].alignment = Alignment(horizontal='center', vertical='center',
                                                       wrap_text=True)
        ws['%s%s' % ('G', rowx)].font = ws['%s%s' % ('J', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('G', rowx)].alignment = Alignment(horizontal='center', vertical='center',
                                                       wrap_text=True)
        ws['%s%s' % ('I', rowx)].font = ws['%s%s' % ('K', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('I', rowx)].alignment = Alignment(horizontal='center', vertical='center',
                                                       wrap_text=True)
        rowx += 1

        ws.merge_cells('A%s:C%s' % (rowx, rowx))
        ws.merge_cells('E%s:G%s' % (rowx, rowx))
        ws['%s%s' % ('A', rowx)] = u'Тайланг үнэн зөв гаргасан:'
        ws['%s%s' % ('E', rowx)] = u'Тайланг хүлээн авсан:'

        ws['%s%s' % ('A', rowx)].font = ws['%s%s' % ('A', rowx)].font.copy(size=11, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('A', rowx)].alignment = Alignment(wrap_text=True)
        ws['%s%s' % ('E', rowx)].font = ws['%s%s' % ('G', rowx)].font.copy(size=11, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('E', rowx)].alignment = Alignment(wrap_text=True)
        rowx += 2

        ws.merge_cells('A%s:B%s' % (rowx, rowx))
        ws.merge_cells('C%s:D%s' % (rowx, rowx))
        ws.merge_cells('E%s:F%s' % (rowx, rowx))
        ws.merge_cells('G%s:H%s' % (rowx, rowx))
        ws['%s%s' % ('A', rowx)] = u'Дарга  /Захирал/:   '
        ws['%s%s' % ('C', rowx)] = self.executive_sign.name
        ws['%s%s' % ('E', rowx)] = u'Татварын улсын байцаагч:'
        ws['%s%s' % ('G', rowx)] = '. . . . . . . . . . . . . . . .'

        ws['%s%s' % ('A', rowx)].font = ws['%s%s' % ('A', rowx)].font.copy(size=10, bold=False,
                                                                           name='Times New Roman')
        ws['%s%s' % ('A', rowx)].alignment = Alignment(wrap_text=True)
        ws['%s%s' % ('C', rowx)].font = ws['%s%s' % ('E', rowx)].font.copy(size=8, bold=False,
                                                                           name='Times New Roman')
        ws['%s%s' % ('C', rowx)].alignment = Alignment(horizontal='center')
        ws['%s%s' % ('E', rowx)].font = ws['%s%s' % ('G', rowx)].font.copy(size=10, bold=False,
                                                                           name='Times New Roman')
        ws['%s%s' % ('G', rowx)].font = ws['%s%s' % ('I', rowx)].font.copy(size=10, bold=False,
                                                                           name='Times New Roman')
        ws['%s%s' % ('G', rowx)].alignment = Alignment(horizontal='center')
        rowx += 2

        ws.merge_cells('A%s:B%s' % (rowx, rowx))
        ws.merge_cells('C%s:D%s' % (rowx, rowx))
        ws['%s%s' % ('A', rowx)] = u'Ерөнхий нягтлан бодогч:   '
        ws['%s%s' % ('C', rowx)] = self.general_accountant_sign.name


        ws['%s%s' % ('A', rowx)].font = ws['%s%s' % ('A', rowx)].font.copy(size=10, bold=False,
                                                                           name='Times New Roman')
        ws['%s%s' % ('A', rowx)].alignment = Alignment(wrap_text=True)
        ws['%s%s' % ('C', rowx)].font = ws['%s%s' % ('E', rowx)].font.copy(size=8, bold=False,
                                                                           name='Times New Roman')
        ws['%s%s' % ('C', rowx)].alignment = Alignment(horizontal='center')
        rowx += 2

        ws.merge_cells('A%s:C%s' % (rowx, rowx))
        ws.merge_cells('E%s:H%s' % (rowx, rowx))
        ws['%s%s' % ('A', rowx)] = '%s оны %s сарын %s -ны өдөр' % (time.strftime('%Y'), time.strftime('%m'), time.strftime('%d'))
        ws['%s%s' % ('E', rowx)] = '%s оны %s сарын %s -ны өдөр' % (time.strftime('%Y'), time.strftime('%m'), time.strftime('%d'))

        ws['%s%s' % ('A', rowx)].font = ws['%s%s' % ('A', rowx)].font.copy(size=10, bold=False,
                                                                           name='Times New Roman')
        ws['%s%s' % ('E', rowx)].font = ws['%s%s' % ('E', rowx)].font.copy(size=10, bold=False,
                                                                           name='Times New Roman')
        rowx += 2

        return rowx

    def check_district_quantity(self,taxation,check_district_dicts):

        check_Type = False
        if check_district_dicts:
            for check_district_dict in check_district_dicts:
                if check_district_dict['code'] == taxation['code'] and check_district_dict['vehicle_type'] == taxation['vehicle_type']:
                    check_district_dict['quantity'] += 1
                    check_Type = False
                else :
                    check_Type = True
            if check_Type == True:
                check_district_dicts.append({'code': taxation['code'], 'vehicle_type': taxation['vehicle_type'], 'truck_load': taxation['truck_load'], 'car_capacity': taxation['car_capacity'], 'quantity': 1, 'name': taxation['name'], 'taxation': taxation['taxation']})
        else :
            check_district_dicts.append({'code':taxation['code'],'vehicle_type': taxation['vehicle_type'],'truck_load' : taxation['truck_load'],'car_capacity' : taxation['car_capacity'],'quantity' : 1,'name' : taxation['name'],'taxation' : taxation['taxation']})
        return check_district_dicts