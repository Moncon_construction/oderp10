# -*- encoding: utf-8 -*-
import json
import openpyxl 
import xlsxwriter
import time
import base64
from odoo import api, fields, models, _
from odoo.modules import get_module_resource
from datetime import datetime
from io import BytesIO
from openpyxl.styles.borders import Border, Side
from openpyxl.styles import Alignment, Font
from operator import itemgetter
from odoo.exceptions import UserError

class TaxTT12Report(models.TransientModel):
    """ ТТ-12 Суутгагчийн хувь хүнд олгосон орлогын албан татварын тайлан """
    _name = "tax.tt12.report"
    _description = "Income tax report submitted to individual taxpayers TA-12"
    
    company_id = fields.Many2one('res.company', string='Company', readonly=True, default=lambda self: self.env.user.company_id)
    date_from = fields.Date(string='Start Date', required=True)
    date_to = fields.Date(string='End Date', required=True)
    fiscalyear_id = fields.Many2one('account.fiscalyear', string='Fiscal Year', required=True)
    
    @api.onchange('fiscalyear_id')
    def onchange_fiscalyear(self):
        if self.fiscalyear_id:
            date_from = self.fiscalyear_id.date_start
            date_to = self.fiscalyear_id.date_stop
            now_day = time.strftime('%Y-%m-%d')
            if now_day > date_from and now_day < date_to:
                date_to = now_day
            self.date_from = date_from
            self.date_to = date_to
    
    def get_balance_by_option(self, option_code):
        amount = {
            'start_debit': 0,
            'start_credit': 0,
            'debit': 0,
            'credit': 0
        }
        data_dict = {}
        move_line_obj = self.env['account.move.line']
        option = self.env['account.tax.report.option'].search([('code', '=', option_code)])
        if option:
            account_ids = option.account_ids.ids
            if account_ids:
                initials = move_line_obj.get_initial_balance(self.company_id.id, account_ids, self.fiscalyear_id.date_start, 'posted')
                fetched = initials 
                for f in initials:
                    if f['account_id'] not in data_dict:
                        data_dict[f['account_id']] = {'code': f['code'],
                                                      'name': f['name'],
                                                      'start_debit': 0,
                                                      'start_credit': 0,
                                                      'cur_start_debit': 0,
                                                      'cur_start_credit': 0,
                                                      'debit': 0,
                                                      'credit': 0,
                                                      'cur_debit': 0,
                                                      'cur_credit': 0, }
                    data_dict[f['account_id']]['start_debit'] += f['start_debit']
                    data_dict[f['account_id']]['start_credit'] += f['start_credit']
                    data_dict[f['account_id']]['cur_start_debit'] += f['cur_start_debit']
                    data_dict[f['account_id']]['cur_start_credit'] += f['cur_start_credit']
                    data_dict[f['account_id']]['debit'] += f['debit']
                    data_dict[f['account_id']]['credit'] += f['credit']
                    data_dict[f['account_id']]['cur_debit'] += f['cur_debit']
                    data_dict[f['account_id']]['cur_credit'] += f['cur_credit']

            for val in sorted(data_dict.values(), key=itemgetter('code')):
                amount['start_debit'] += val['start_debit']
                amount['start_credit'] += val['start_credit']
                amount['debit'] += val['debit']
                amount['credit'] += val['credit']

        return amount
    
    @api.multi
    def export_report(self):
        company = self.company_id
        accountant = company.genaral_accountant_signature.name or company.second_sign_cart.name or ''
        partner = self.company_id.partner_id
        amount = self.get_balance_by_option('TT12OPT_12')
        residual = amount['start_debit'] - amount['start_credit']
        company_dict = {'ttd0': '', 'ttd1': '', 'ttd2': '', 'ttd3': '', 'ttd4': '', 'ttd5': '', 'ttd6': '',
                        's1': '', 'y1': '', 'y2': '', 'y3': '', 'y4': '',
                        'company_name': company.name, 'basic_operation_name':'', 'basic_operation_code': '', 'accountant_name': accountant,
                        'date_str':'%s - %s' % (datetime.strptime(self.date_from, '%Y-%m-%d').strftime('%Y.%m.%d'),datetime.strptime(self.date_to, '%Y-%m-%d').strftime('%Y.%m.%d')),
                        '{f1}': residual if residual > 0 else 0,
                        '{f2}': residual if residual < 0 else 0,
                        '{p3}': 0, '{o3}': 0, '{ta3}': 0,
                        '{np}': 0, '{nv}': 0}
        i = 0
        if company.operation_ids:
            for operation in company.operation_ids:
                company_dict.update({'ttd' + str(i):operation.code})
                i+=1
        date_format = "%Y-%m-%d"
        year = datetime.strptime(self.fiscalyear_id.date_start, date_format).year
        for i in range(0, 4):
            company_dict.update({'y%s' % (i+1): list(str(year))[i]})
        end_spring = datetime.strptime('%s-05-31' % year, date_format)
        end_summer = datetime.strptime('%s-08-31' % year, date_format)
        end_autumn = datetime.strptime('%s-11-30' % year, date_format)
        end_winter = datetime.strptime('%s-02-28' % year, date_format)
        season = 1
        current_date = datetime.strptime('%s-%s-%s' % (year, datetime.strptime(self.date_to, date_format).month, datetime.strptime(self.date_to, date_format).day), date_format)
        if current_date <= end_winter and current_date > end_autumn:
            season = 4
        elif current_date <= end_summer and current_date > end_spring:
            season = 2
        elif current_date <= end_autumn and current_date > end_summer:
            season = 3
        company_dict.update({'s1': season})
        template_file = get_module_resource('l10n_mn_account_tax_report', 'static/src/report', 'TT12-input.xlsx')
        generate_file = get_module_resource('l10n_mn_account_tax_report', 'static/src/report', 'TT12-output.xlsx')
        
        options = self.env['account.tax.report.option'].search([('report','=','tt12'),('parent_id','=',False)])
        lines = self.env['account.tax.report.option'].with_context(date_from=self.date_from, date_to=self.date_to,\
                                                                  company_id=self.company_id.id).create_report_data(options, rounding=False)
        wb = openpyxl.load_workbook(template_file)
        thin_border = Border(left=Side(style='thin'), 
                     right=Side(style='thin'), 
                     top=Side(style='thin'), 
                     bottom=Side(style='thin'))
        wrap_alignment = Alignment(wrap_text=True,horizontal='general',vertical='bottom')
        font = Font(name='Times New Roman',size=8, vertAlign=None)
        """ Татвар дээрх татварын тайлангийн төрөл нь ХХОАТ-ТТ-12 (1) төрөлтэй татваруудыг сонгосон нэхэмжлэлүүдийн хувь дах тооцоо"""
        ws1 = wb.get_sheet_by_name('TT-12_(1)')
        rows = ws1.rows
        for row in rows:
            for cell in row:
                for line in lines:
                    if company_dict.has_key(cell.value):
                        ws1[cell.coordinate] = company_dict[cell.value]
        self._cr.execute("SELECT line.id, partner.register, partner.surname, partner.name, tax.id AS tax_id, move.date, "
                         "line.quantity * line.price_unit AS multiple_value, line.price_subtotal, tax.amount "
                         "FROM account_invoice_line_tax rel "
                         "LEFT JOIN account_invoice_line line ON line.id = rel.invoice_line_id "
                         "LEFT JOIN account_invoice invoice ON invoice.id = line.invoice_id "
                         "LEFT JOIN account_tax tax ON tax.id = rel.tax_id "
                         "LEFT JOIN res_partner partner ON partner.id = line.partner_id "
                         "LEFT JOIN account_move move ON move.id = invoice.move_id "
                         "WHERE invoice.state != 'draft' AND line.company_id = %s AND tax.tax_report_type = '1' AND move.date BETWEEN %s AND %s "
                         "ORDER BY line.id;",(self.env.user.company_id.id, self.date_from, self.date_to))
        lines1 = self.env.cr.dictfetchall()

        col_aph = ['A', 'B', 'D', 'F', 'H', 'K','L','M','N','O']
        no = 1
        rowx = 14
        total_income = total_tax = 0
        option_obj = self.env['account.tax.report.option']
        tax_obj = self.env['account.tax']
        for line in lines1:
            option_id = option_obj.search([('tax_id','=',line['tax_id'])], order='sequence', limit=1)
            if not option_id:
                raise UserError(_("Tax options hasn't configurate to %s tax!" % tax_obj.browse(line['tax_id']).name))
            ws1.merge_cells('B%s:C%s' % (rowx, rowx))
            ws1.merge_cells('D%s:E%s' % (rowx, rowx))
            ws1.merge_cells('F%s:G%s' % (rowx, rowx))
            ws1.merge_cells('H%s:J%s' % (rowx, rowx))
            for x in range(0, 10):
                ws1['%s%s' % (col_aph[x],rowx)].border = thin_border
                ws1['%s%s' % (col_aph[x],rowx)].alignment = wrap_alignment
                ws1['%s%s' % (col_aph[x],rowx)].font = font
            tax_value = line['multiple_value'] - line['price_subtotal']
            if tax_value == 0:
                tax_value = (line['amount'] / 100 * line['price_subtotal'])
            ws1['%s%s' % (col_aph[0],rowx)] = no
            ws1['%s%s' % (col_aph[1],rowx)] = line['register']
            ws1['%s%s' % (col_aph[2],rowx)] = line['surname']
            ws1['%s%s' % (col_aph[3],rowx)] = line['name']
            ws1['%s%s' % (col_aph[4],rowx)] = ''
            ws1['%s%s' % (col_aph[5],rowx)].alignment.copy(wrap_text=True)
            ws1['%s%s' % (col_aph[5],rowx)] = option_id[0].name
            ws1['%s%s' % (col_aph[6],rowx)] = line['date']
            ws1['%s%s' % (col_aph[7],rowx)] = abs(line['price_subtotal']) # Татвар шингэсэн ч бай үгүй ч бай татваргүй дүн нь
            ws1['%s%s' % (col_aph[8],rowx)] = abs(line['amount'])
            ws1['%s%s' % (col_aph[9],rowx)] = abs(tax_value)
            total_income += abs(line['price_subtotal'])
            total_tax += abs(tax_value)
            no += 1
            rowx += 1
        ws1.merge_cells('A%s:L%s' % (rowx, rowx))
        ws1['A%s' % rowx].border = thin_border
        ws1['M%s' % rowx].border = thin_border
        ws1['N%s' % rowx].border = thin_border
        ws1['O%s' % rowx].border = thin_border
        ws1['A%s' % rowx].font = font
        ws1['M%s' % rowx].font = font
        ws1['O%s' % rowx].font = font
        ws1['A%s' % rowx] = _('Total')
        ws1['M%s' % rowx] = total_income
        ws1['O%s' % rowx] = total_tax
        rowx += 1
        ws1['%s%s' % (col_aph[0],rowx+2)] = 'Тайлан гаргасан:    ................................ %s' % accountant
        ws1['%s%s' % (col_aph[0],rowx+4)] = 'Тайлан хүлээн авсан:  ..................................... %s' % accountant
        """ Татвар дээрх татварын тайлангийн төрөл нь ХХОАТ-ТТ-12 (2) төрөлтэй татваруудыг сонгосон нэхэмжлэлүүдийн хувь дах тооцоо"""
        ws2 = wb.get_sheet_by_name('TT-12_(2)')
        rows = ws2.rows
        for row in rows:
            for cell in row:
                for line in lines:
                    if company_dict.has_key(cell.value):
                        ws2[cell.coordinate] = company_dict[cell.value]
        self._cr.execute("SELECT line.id, partner.id as partner_id, partner.register, partner.surname, partner.name, tax.id AS tax_id, move.date, "
                         "line.price_subtotal "
                         "FROM account_invoice_line_tax rel "
                         "LEFT JOIN account_invoice_line line ON line.id = rel.invoice_line_id "
                         "LEFT JOIN account_invoice invoice ON invoice.id = line.invoice_id "
                         "LEFT JOIN account_tax tax ON tax.id = rel.tax_id "
                         "LEFT JOIN res_partner partner ON partner.id = line.partner_id "
                         "LEFT JOIN account_move move ON move.id = invoice.move_id "
                         "WHERE invoice.state != 'draft' AND line.company_id = %s AND tax.tax_report_type = '2' AND move.date BETWEEN %s AND %s "
                         "ORDER BY line.id;",(self.env.user.company_id.id, self.date_from, self.date_to))
        lines2 = self.env.cr.dictfetchall()
        
        partner_ids = []
        col_aph = ['A', 'B', 'E', 'G', 'J', 'K','L','M','N']
        total_income = 0
        no = 1
        rowx = 14
        for line in lines2:
            option_id = option_obj.search([('tax_id','=',line['tax_id'])], order='sequence', limit=1)
            if not option_id:
                raise UserError(_("Tax options hasn't configurate to %s tax!" % tax_obj.browse(line['tax_id']).name))
            ws2.merge_cells('B%s:D%s' % (rowx, rowx))
            ws2.merge_cells('E%s:F%s' % (rowx, rowx))
            ws2.merge_cells('G%s:I%s' % (rowx, rowx))
            for x in range(0, 9):
                ws2['%s%s' % (col_aph[x],rowx)].border = thin_border
                ws2['%s%s' % (col_aph[x],rowx)].alignment = wrap_alignment
                ws2['%s%s' % (col_aph[x],rowx)].font = font
            ws2['%s%s' % (col_aph[0],rowx)] = no
            ws2['%s%s' % (col_aph[1],rowx)] = line['register']
            ws2['%s%s' % (col_aph[2],rowx)] = line['surname']
            ws2['%s%s' % (col_aph[3],rowx)] = line['name']
            ws2['%s%s' % (col_aph[4],rowx)] = ''
            ws2['%s%s' % (col_aph[5],rowx)] = ''
            ws2['%s%s' % (col_aph[6],rowx)] = option_id[0].name
            ws2['%s%s' % (col_aph[7],rowx)] = line['date']
            ws2['%s%s' % (col_aph[8],rowx)] = abs(line['price_subtotal'])
            total_income += abs(line['price_subtotal'])
            no += 1
            rowx += 1
            partner_ids.append(line['partner_id'])
        ws2.merge_cells('A%s:M%s' % (rowx, rowx))
        ws2['A%s' % rowx].border = thin_border
        ws2['N%s' % rowx].border = thin_border
        ws2['A%s' % rowx].font = font
        ws2['N%s' % rowx].font = font
        ws2['A%s' % rowx] = _('Total')
        ws2['N%s' % rowx] = total_income
        ws2['%s%s' % (col_aph[0],rowx+2)] = 'Тайлан гаргасан:    ................................ %s' % accountant
        ws2['%s%s' % (col_aph[0],rowx+4)] = 'Тайлан хүлээн авсан:  ..................................... %s' % accountant
        partners = list(set(partner_ids))
        company_dict.update({'{np}': len(partners) or 0})
        company_dict.update({'{nv}': total_income})
        
        """ Татвар дээрх татварын тайлангийн төрөл нь ХХОАТ-ТТ-12 (3) төрөлтэй татваруудыг сонгосон нэхэмжлэлүүдийн хувь дах тооцоо"""
        ws3 = wb.get_sheet_by_name('TT-12_(3)')
        rows = ws3.rows
        for row in rows:
            for cell in row:
                for line in lines:
                    if company_dict.has_key(cell.value):
                        ws3[cell.coordinate] = company_dict[cell.value]
        self._cr.execute("SELECT line.id, partner.register, partner.name, tax.id AS tax_id, move.date, "
                         "line.quantity * line.price_unit AS multiple_value, line.price_subtotal, tax.amount "
                         "FROM account_invoice_line_tax rel "
                         "LEFT JOIN account_invoice_line line ON line.id = rel.invoice_line_id "
                         "LEFT JOIN account_invoice invoice ON invoice.id = line.invoice_id "
                         "LEFT JOIN account_tax tax ON tax.id = rel.tax_id "
                         "LEFT JOIN res_partner partner ON partner.id = line.partner_id "
                         "LEFT JOIN account_move move ON move.id = invoice.move_id "
                         "WHERE invoice.state != 'draft' AND line.company_id = %s AND tax.tax_report_type = '3' AND move.date BETWEEN %s AND %s "
                         "ORDER BY line.id;",(self.env.user.company_id.id, self.date_from, self.date_to))
        lines3 = self.env.cr.dictfetchall()
        
        col_aph = ['A', 'B', 'D', 'H', 'K','L','M','N']
        total_income = total_tax = 0
        no = 1
        rowx = 14
        for line in lines3:
            option_id = option_obj.search([('tax_id','=',line['tax_id'])], order='sequence', limit=1)
            if not option_id:
                raise UserError(_("Tax options hasn't configurate to %s tax!" % tax_obj.browse(line['tax_id']).name))
            ws3.merge_cells('B%s:C%s' % (rowx, rowx))
            ws3.merge_cells('D%s:G%s' % (rowx, rowx))
            ws3.merge_cells('H%s:J%s' % (rowx, rowx))
            for x in range(0, 8):
                ws3['%s%s' % (col_aph[x],rowx)].border = thin_border
                ws3['%s%s' % (col_aph[x],rowx)].alignment = wrap_alignment
                ws3['%s%s' % (col_aph[x],rowx)].font = font
            tax_value = line['multiple_value'] - line['price_subtotal']
            if tax_value == 0:
                tax_value = (line['amount'] / 100 * line['price_subtotal'])
            ws3['%s%s' % (col_aph[0],rowx)] = no
            ws3['%s%s' % (col_aph[1],rowx)] = line['register']
            ws3['%s%s' % (col_aph[2],rowx)] = line['name']
            ws3['%s%s' % (col_aph[3],rowx)] = ''
            ws3['%s%s' % (col_aph[4],rowx)] = option_id[0].name
            ws3['%s%s' % (col_aph[5],rowx)] = line['date']
            ws3['%s%s' % (col_aph[6],rowx)] = abs(line['price_subtotal'])
            ws3['%s%s' % (col_aph[7],rowx)] = abs(tax_value)
            total_income += abs(line['price_subtotal'])
            total_tax += abs(tax_value)
            no += 1
            rowx += 1
        ws3.merge_cells('A%s:L%s' % (rowx, rowx))
        ws3['A%s' % rowx].border = thin_border
        ws3['M%s' % rowx].border = thin_border
        ws3['N%s' % rowx].border = thin_border
        ws3['A%s' % rowx].font = font
        ws3['M%s' % rowx].font = font
        ws3['N%s' % rowx].font = font
        ws3['A%s' % rowx] = _('Total')
        ws3['M%s' % rowx] = total_income
        ws3['N%s' % rowx] = total_tax
        ws3['%s%s' % (col_aph[0],rowx+2)] = 'Тайлан гаргасан:    ................................ %s' % accountant
        ws3['%s%s' % (col_aph[0],rowx+4)] = 'Тайлан хүлээн авсан:  ..................................... %s' % accountant
        
        company_dict.update({'{p3}': len(lines3) or 0})
        company_dict.update({'{o3}': total_income})
        company_dict.update({'{ta3}': total_tax})
        """ TT-12 хуудсан дээр """
        ws = wb.get_sheet_by_name('TT-12')
        rows = ws.rows
        num = 1
        for row in rows:
            found = False
            for cell in row:
                for line in lines:
                    if company_dict.has_key(cell.value):
                        cell.value = company_dict[cell.value]
                    if line['key'] != False and line['key'] == cell.value:
                        cell.value = line['amount']
                        if line['key'] == '{c%s}' % num:
                            cell.value = num
                            found = True
                        break
                    #Хэрвээ key2 байвал татвар төлөгчийн тоон дээр бүртгэнэ.
                    elif line['key2'] != False and line['key2'] == cell.value:
                        cell.value = int(line['total_partner']) or 0
                        break
                
            if found:
                num += 1
        
        wb.save(generate_file)
        file_name = "ТТ-12_%s.xlsx" % (time.strftime('%Y%m%d_%H%M'),)
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('ТТ-12'), form_title=file_name).create({})
        image = open(generate_file, 'rb')
        image_read = image.read()

        report_excel_output_obj.filedata = base64.encodestring(image_read)
        return report_excel_output_obj.export_report()
    
    
    
    
    
    