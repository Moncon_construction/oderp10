# -*- encoding: utf-8 -*-
##############################################################################
from odoo import api, fields, models, _
from odoo.modules import get_module_resource
from datetime import datetime
import json
import openpyxl 
from io import BytesIO
import time
import base64

class TaxTT13Report(models.TransientModel):
    """Суутгагчийн аж ахуйн нэгжид олгосон орлогоос суутгасан албан татварын тайлан
    """
    _name = "tax.tt13.report"
    _description = "Tax TT-13 Report"

    company_id = fields.Many2one('res.company', string='Company', readonly=True, default=lambda self: self.env.user.company_id)
    date_from = fields.Date(string='Start Date', required=True)
    date_to = fields.Date(string='End Date', required=True)
    fiscalyear_id = fields.Many2one('account.fiscalyear', string='Fiscal Year', required=True)
    
    @api.onchange('fiscalyear_id')
    def onchange_fiscalyear(self):
        if self.fiscalyear_id:
            date_from = self.fiscalyear_id.date_start
            date_to = self.fiscalyear_id.date_stop
            now_day = time.strftime('%Y-%m-%d')
            if now_day > date_from and now_day < date_to:
                date_to = now_day
            self.date_from = date_from
            self.date_to = date_to

    @api.multi
    def export_report(self):
        cr = self._cr
        company = self.company_id
        partner = self.company_id.partner_id
        company_dict = {'s1': '', 'y1': '', 'y2': '', 'y3': '', 'y4': '', 'ttd0': '', 'ttd1': '', 'ttd2': '', 'ttd3': '', 'ttd4': '', 'ttd5': '', 'ttd6': '',
                        'company_name': u'2. Нэр: %s'%(company.name), 'basic_operation_name':'', 'basic_operation_code': '',
                        'date_str':'%s - %s' % (datetime.strptime(self.date_from, '%Y-%m-%d').strftime('%Y.%m.%d'),datetime.strptime(self.date_to, '%Y-%m-%d').strftime('%Y.%m.%d'))}
        i = 0
        if company.operation_ids:
            for operation in company.operation_ids:
#                     company_dict.update({'basic_operation_name':operation.name})
                company_dict.update({'ttd' + str(i):operation.code})
                i+=1
        i = 1
        year = self.fiscalyear_id.date_start
        for a in str(year).split('-'):
            for b in list(a):
                company_dict.update({'y' + str(i):b})
                i+=1
            break
        template_file = get_module_resource('l10n_mn_account_tax_report', 'static/src/report', 'TT13-input.xlsx')
        generate_file = get_module_resource('l10n_mn_account_tax_report', 'static/src/report', 'TT13-output.xlsx')
        
        options = self.env['account.tax.report.option'].search([('report','=','tt13'),('parent_id','=',False)])
        invoice_ids = []
        inv_ids = []
        tax_ids = []
        in_ids = []
#         for option in options:
#             if option.tax_id.id:
#                 tax_ids.append(option.tax_id.id)
        lines = self.env['account.tax.report.option'].with_context(date_from=self.date_from, date_to=self.date_to,\
                                                                  company_id=self.company_id.id).create_report_data(options, rounding=False)
        wb = openpyxl.load_workbook(template_file)
        sheet = wb.active
        ws = wb.get_sheet_by_name('TT-13')
        rows = sheet.rows
        string_dict = ''
        for row in rows:
            for cell in row:
                for line in lines:
                    if company_dict.has_key(cell.value):
                        ws[cell.coordinate] = company_dict[cell.value]
                    #Хэрвээ key байвал татвар төлөх дүн дээр бүртгэнэ.
                    if line['key'] != False and line['key'] == cell.value:
                        ws[cell.coordinate] = line['amount']
                        break
                    #Хэрвээ key2 байвал татвар төлөгчийн тоон дээр бүртгэнэ.
                    elif line['key2'] != False and line['key2'] == cell.value:
                        ws[cell.coordinate] = line['total_partner']
                        break
        #ws.cell(row=2, column=2).value = 2
        #cell.alignment = Alignment(horizontal='center', vertical='center')
        wb.save(generate_file)
        
        file_name = "ТТ-13_%s.xlsx" % (time.strftime('%Y%m%d_%H%M'),)
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('ТТ-13'), form_title=file_name).create({})
        image = open(generate_file, 'rb')
        image_read = image.read()
        report_excel_output_obj.filedata = base64.encodestring(image_read)

        return report_excel_output_obj.export_report()

