# -*- encoding: utf-8 -*-
##############################################################################
import xlsxwriter

from odoo import api, fields, models, _
from odoo.modules import get_module_resource
from datetime import datetime
from openpyxl.styles.borders import Border, Side
from openpyxl.styles import Alignment
import openpyxl
import time
import base64

rowx = 0

class TaxTT23Report(models.TransientModel):
    """ТТ-23 тайлан
    """
    _name = "tax.tt23.report"
    _description = "Tax TT-23 Report"

    # company_id = fields.Many2one('res.company', string='Company', readonly=True, default=lambda self: self.env.user.company_id)
    date_from = fields.Date(string='Start Date')
    date_to = fields.Date(string='End Date')
    fiscalyear_id = fields.Many2one('account.fiscalyear', string='Fiscal Year', required=True)
    underpayment = fields.Many2one('account.account', 'Underpayment account', required=True)
    overpayment = fields.Many2one('account.account', 'Overpayment account', required=True)

    @api.onchange('fiscalyear_id')
    def onchange_fiscalyear(self):
        # Санхүүгийн жилийн утга өөрчлөгдөхөд компани дээр бүртгэлтэй дансыг харуулах
        if self.fiscalyear_id:
            if self.fiscalyear_id.company_id.real_state_underpayment_tax_account:
                self.underpayment = self.fiscalyear_id.company_id.real_state_underpayment_tax_account
            if self.fiscalyear_id.company_id.real_state_overpayment_tax_account:
                self.overpayment = self.fiscalyear_id.company_id.real_state_overpayment_tax_account
            date_from = self.fiscalyear_id.date_start
            date_to = self.fiscalyear_id.date_stop
            now_day = time.strftime('%Y-%m-%d')
            if now_day > date_from and now_day < date_to:
                date_to = now_day
            self.date_from = date_from
            self.date_to = date_to

    @api.multi
    def export_report(self):
        company = self.fiscalyear_id.company_id
        value_dict = {'ttd': '',
                        'company_name': u'%s' % (company.name),
                        'fiscal_year': self.fiscalyear_id.name,
                        'province_district': company.province_district_id.name,
                        'sum_khoroo': company.sum_khoroo_id.name,
                        'street1': company.street,
                        'street2': company.street2,
                        'fax': company.fax,
                        'phone': company.phone,
                        'ID': company.id,
                        'second_sign': company.second_sign_cart.name,
                        'door_number': company.door_number,
                        'mail_box': company.mail_box,
                        'date_created': '%s оны %s сарын %s -ны өдөр' % (
                        time.strftime('%Y'), time.strftime('%m'), time.strftime('%d')),
                        'overbalance': 0,
                        'underbalance': 0,
                        'mas_hb': 0, 'mas_fac': 0, 'mas_con': 0, 'mas_other': 0, 'pri_con': 0, 'pub_hb': 0, 'pub_fac': 0,
                        'value_mas_hb': 0, 'value_mas_fac': 0, 'value_mas_con': 0, 'value_mas_other': 0, 'value_pri_con': 0, 'value_pub_hb': 0, 'value_pub_fac': 0,
                        'mas_hb1': 0, 'mas_fac1': 0, 'mas_con1': 0, 'mas_other1': 0, 'pri_con1': 0, 'pub_hb1': 0, 'pub_fac1': 0,
                        'value_mas_hb1': 0, 'value_mas_fac1': 0, 'value_mas_con1': 0, 'value_mas_other1': 0,
                        'value_pri_con1': 0, 'value_pub_hb1': 0, 'value_pub_fac1': 0,
                        'closed_mas_hb': 0, 'closed_mas_fac': 0, 'closed_mas_con': 0, 'closed_mas_other': 0,
                        'closed_pri_con': 0, 'closed_pub_hb': 0, 'closed_pub_fac': 0,
                        'closed_value_mas_hb': 0, 'closed_value_mas_fac': 0, 'closed_value_mas_con': 0,
                        'closed_value_mas_other': 0, 'closed_value_pri_con': 0, 'closed_value_pub_hb': 0,
                        'closed_value_pub_fac': 0, 'closed_value_pri_con_before': 0, 'closed_value_pub_hb_before': 0, 'closed_value_pub_fac_before': 0,
                        'count_value_pri_con_before': 0, 'count_value_pub_hb_before': 0, 'count_value_pub_fac_before': 0}

        if company.company_registry:
            value_dict.update({'ttd': company.company_registry})

        template_file = get_module_resource('l10n_mn_account_tax_report', 'static/src/report', 'TT23-input.xlsx')
        generate_file = get_module_resource('l10n_mn_account_tax_report', 'static/src/report', 'TT23-output.xlsx')

        options = self.env['account.tax.report.option'].search([('report', '=', 'tt23'), ('parent_id', '=', False)])
        lines = self.env['account.tax.report.option'].with_context(date_from=self.date_from, date_to=self.date_to,
                                                                   company_id=self.fiscalyear_id.company_id.id).create_report_data(
            options)

        last_year = '0101' + str(datetime.today().year)
        date_year_end = datetime.strptime((str(datetime.today().year) + '-12-31'), "%Y-%m-%d").date()
        date = datetime.strptime(last_year, "%d%m%Y").date()

        initial_under = self.env['account.move.line'].get_initial_balance(company.id, self.underpayment.ids, date,
                                                                          'posted')

        if initial_under:
            under_credit = initial_under[0]['start_credit']
            under_debit = initial_under[0]['start_debit']
            underbalance = under_credit
            value_dict.update({'underbalance': underbalance})
        initial_over = self.env['account.move.line'].get_initial_balance(company.id, self.overpayment.ids, date,
                                                                         'posted')
        if initial_over:
            over_credit = initial_over[0]['start_credit']
            over_debit = initial_over[0]['start_debit']
            overbalance = over_debit
            value_dict.update({'overbalance': overbalance})

        self.env.cr.execute("SELECT aaa.state AS state, aaa.asset_record_amount AS record, "
                                "aaa.asset_insurance_amount AS insurance, aaa.value AS value, "
                                "aaa.purchase_date AS date, aaa.closed_date AS closed_date, "
                                "aac.real_estate_type AS type, aac.real_estate_intent AS intent, "
                                "aal.asset_tax_percent AS percent, aaa.name AS name, aaa.certificate_number AS number "
                            "FROM account_asset_asset aaa "
                                "FULL JOIN account_asset_category aac "
                                "on aaa.category_id = aac.id "
                                "FULL JOIN account_asset_location aal "
                                "on aaa.location_id = aal.id "
                            "WHERE aaa.company_id = "
                            "'" + str(company.id) + "' "
                                                    "AND aac.is_property = TRUE "
                                                    "AND aaa.state != " + "'" + 'draft' + "'")

        assets = self.env.cr.dictfetchall()

        asset_period = str(datetime.today().year) + '-01-01'
        check_date = datetime.strptime(asset_period, "%Y-%m-%d").date()

        count_mas_hb = 0
        count_mas_fac = 0
        count_mas_con = 0
        count_mas_other = 0
        count_pri_con = 0
        count_pub_hb = 0
        count_pub_fac = 0

        value_mas_hb = 0
        value_mas_fac = 0
        value_mas_con = 0
        value_mas_other = 0
        value_pri_con = 0
        value_pub_hb = 0
        value_pub_fac = 0

        count_mas_hb1 = 0
        count_mas_fac1 = 0
        count_mas_con1 = 0
        count_mas_other1 = 0
        count_pri_con1 = 0
        count_pub_hb1 = 0
        count_pub_fac1 = 0

        value_mas_hb1 = 0
        value_mas_fac1 = 0
        value_mas_con1 = 0
        value_mas_other1 = 0
        value_pri_con1 = 0
        value_pub_hb1 = 0
        value_pub_fac1 = 0

        closed_mas_hb = 0
        closed_mas_fac = 0
        closed_mas_con = 0
        closed_mas_other = 0
        closed_pri_con = 0
        closed_pub_hb = 0
        closed_pub_fac = 0

        closed_value_mas_hb = 0
        closed_value_mas_fac = 0
        closed_value_mas_con = 0
        closed_value_mas_other = 0
        closed_value_pri_con = 0
        closed_value_pub_hb = 0
        closed_value_pub_fac = 0

        closed_value_pri_con_before = 0
        closed_value_pub_hb_before = 0
        closed_value_pub_fac_before = 0

        count_value_pri_con_before = 0
        count_value_pub_hb_before = 0
        count_value_pub_fac_before = 0

        list_mas = []
        list_pri = []
        list_pub = []
        closed_list_mas = []
        closed_list_pri = []
        closed_list_pub = []
        # Хөрөнгө ангилалаар нь ялгах болон тоолох хэсэг
        for asset in assets:
            asset_create_date = datetime.strptime(asset['date'], "%Y-%m-%d").date()
            if asset['closed_date'] == None:
                # Тухайн тайлангын жилийн 1 сарын 1 нд байгаа үндсэн хөрөнгийн тоо болон үнэлгээ
                if asset_create_date <= check_date:
                    if asset['intent'] == 'manufacturing and service':
                        if asset['type'] == 'house, building':
                            count_mas_hb += 1
                            # Үнэлгээ = If ҮХЭХБ - н дүн > 0 then ҮХЭХБ - н дүн else (if ҮХЭХД-н дүн > 0 then ҮХЭХД-н дүн else Нийт үнэ)
                            value_mas_hb += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                        elif asset['type'] == 'facility':
                            count_mas_fac += 1
                            value_mas_fac += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                        elif asset['type'] == 'construction':
                            count_mas_con += 1
                            value_mas_con += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                        else:
                            count_mas_other += 1
                            value_mas_other += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                    elif asset['intent'] == 'private apartment':
                        if asset['type'] == 'construction':
                            count_pri_con += 1
                            value_pri_con += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                    elif asset['intent'] == 'public building':
                        if asset['type'] == 'house, building':
                            count_pub_hb += 1
                            value_pub_hb += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                        elif asset['type'] == 'facility':
                            count_pub_fac += 1
                            value_pub_fac += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                else:
                    # Тухайн тайлангын жилийн 1 сарын 1 ээс хойш байгаа үндсэн хөрөнгийн тоо болон үнэлгээ
                    if asset['intent'] == 'manufacturing and service':
                        list_mas.append(asset)
                        if asset['type'] == 'house, building':
                            count_mas_hb1 += 1
                            value_mas_hb1 += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                        elif asset['type'] == 'facility':
                            count_mas_fac1 += 1
                            value_mas_fac1 += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                        elif asset['type'] == 'construction':
                            count_mas_con1 += 1
                            value_mas_con1 += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                        else:
                            count_mas_other1 += 1
                            value_mas_other1 += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                    elif asset['intent'] == 'private apartment':
                        list_pri.append(asset)
                        if asset['type'] == 'construction':
                            count_pri_con1 += 1
                            value_pri_con1 += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                    elif asset['intent'] == 'public building':
                        list_pub.append(asset)
                        if asset['type'] == 'house, building':
                            count_pub_hb1 += 1
                            value_pub_hb1 += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                        elif asset['type'] == 'facility':
                            count_pub_fac1 += 1
                            value_pub_fac1 += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
            else:
                # Тухайн тайлангын жилийн 1 сарын 1с өмнө бүртгэлд байсан хаагдсан хөрөнгийн тоо болон үнэлгээ
                asset_closed_date = datetime.strptime(asset['closed_date'], "%Y-%m-%d").date()
                if asset_create_date <= check_date < asset_closed_date:
                    if asset['intent'] == 'manufacturing and service':
                        if asset['type'] == 'house, building':
                            count_mas_hb += 1
                            value_mas_hb += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                        elif asset['type'] == 'facility':
                            count_mas_fac += 1
                            value_mas_fac += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                        elif asset['type'] == 'construction':
                            count_mas_con += 1
                            value_mas_con += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                        else:
                            count_mas_other += 1
                            value_mas_other += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                    elif asset['intent'] == 'private apartment':
                        if asset['type'] == 'construction':
                            count_pri_con += 1
                            value_pri_con += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                    elif asset['intent'] == 'public building':
                        if asset['type'] == 'house, building':
                            count_pub_hb += 1
                            value_pub_hb += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                        elif asset['type'] == 'facility':
                            count_pub_fac += 1
                            value_pub_fac += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                elif check_date < asset_create_date <= date_year_end:
                    # Тухайн тайлангын жилийн 1 сарын 1с хойш байгаа хаагдсан хөрөнгийн тоо болон үнэлгээ
                    if asset['intent'] == 'manufacturing and service':
                        closed_list_mas.append(asset)
                        if asset['type'] == 'house, building':
                            closed_mas_hb += 1
                            closed_value_mas_hb += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                        elif asset['type'] == 'facility':
                            closed_mas_fac += 1
                            closed_value_mas_fac += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                        elif asset['type'] == 'construction':
                            closed_mas_con += 1
                            closed_value_mas_con += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                        else:
                            closed_mas_other += 1
                            closed_value_mas_other += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                    elif asset['intent'] == 'private apartment':
                        closed_list_pri.append(asset)
                        if asset['type'] == 'construction':
                            closed_pri_con += 1
                            closed_value_pri_con += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                    elif asset['intent'] == 'public building':
                        closed_list_pub.append(asset)
                        if asset['type'] == 'house, building':
                            closed_pub_hb += 1
                            closed_value_pub_hb += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']
                        elif asset['type'] == 'facility':
                            closed_pub_fac += 1
                            closed_value_pub_fac += asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset['value']

        value_dict.update({
                'mas_hb': count_mas_hb, 'mas_fac': count_mas_fac, 'mas_con': count_mas_con,
                'mas_other': count_mas_other, 'pri_con': count_pri_con, 'pub_hb': count_pub_hb,
                'pub_fac': count_pub_fac, 'value_mas_hb': value_mas_hb, 'value_mas_fac': value_mas_fac,
                'value_mas_con': value_mas_con, 'value_mas_other': value_mas_other, 'value_pri_con': value_pri_con,
                'value_pub_hb': value_pub_hb, 'value_pub_fac': value_pub_fac, 'mas_hb1': count_mas_hb1,
                'mas_fac1': count_mas_fac1, 'mas_con1': count_mas_con1, 'mas_other1': count_mas_other1,
                'pri_con1': count_pri_con1, 'pub_hb1': count_pub_hb1, 'pub_fac1': count_pub_fac1,
                'value_mas_hb1': value_mas_hb1, 'value_mas_fac1': value_mas_fac1, 'value_mas_con1': value_mas_con1,
                'value_mas_other1': value_mas_other1, 'value_pri_con1': value_pri_con1,  'value_pub_hb1': value_pub_hb1,
                'value_pub_fac1': value_pub_fac1, 'closed_mas_hb': closed_mas_hb, 'closed_mas_fac': closed_mas_fac,
                'closed_mas_con': closed_mas_con, 'closed_mas_other': closed_mas_other, 'closed_pri_con': closed_pri_con,
                'closed_pub_hb': closed_pub_hb,  'closed_pub_fac': closed_pub_fac, 'closed_value_mas_hb': closed_value_mas_hb,
                'closed_value_mas_fac': closed_value_mas_fac, 'closed_value_mas_con': closed_value_mas_con,
                'closed_value_mas_other': closed_value_mas_other,  'closed_value_pri_con': closed_value_pri_con,
                'closed_value_pub_hb': closed_value_pub_hb, 'closed_value_pub_fac': closed_value_pub_fac,
                'closed_value_pri_con_before': closed_value_pri_con_before, 'closed_value_pub_hb_before': closed_value_pub_hb_before, 'closed_value_pub_fac_before': closed_value_pub_fac_before,
                'count_value_pri_con_before': count_value_pri_con_before, 'count_value_pub_hb_before': count_value_pub_hb_before, 'count_value_pub_fac_before': count_value_pub_fac_before})

        wb = openpyxl.load_workbook(template_file)
        ws = wb.get_sheet_by_name('TT-23')
        sheet = wb.active

        thin_border = Border(left=Side(style='thin'),
                             right=Side(style='thin'),
                             top=Side(style='thin'),
                             bottom=Side(style='thin'))

        # Тайланд татагдах хөрөнгө өрөх хэсэг
        col_aph = ['A', 'G', 'H', 'I', 'J', 'K']
        year = datetime.now().timetuple().tm_year
        # Өндөр жил тооцож байна
        if year / 4 == 0 or year / 100 == 0:
            if year / 400:
                year = 365
            else:
                year = 366
        else:
            year = 365

        rowx = 73
        row_first = rowx - 1
        rowx = self.fill_asset(list_mas, ws, date_year_end, rowx, col_aph, thin_border, year)
        row_last = rowx - 1

        ws['%s%s' % (col_aph[0], row_first)] = u"I. Үйлдвэрлэл, үйлчилгээний зориулалттай  ДҮН"
        ws['%s%s' % (col_aph[0], row_first)].alignment = Alignment(horizontal='center')
        ws['%s%s' % (col_aph[0], row_first)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=8, bold=True,
                                                                                         name='Times New Roman')
        ws['%s%s' % (col_aph[5], row_first)] = '=SUM('+col_aph[5]+str(row_first + 1)+':'+col_aph[5]+str(row_last)+')' if list_mas else 0

        ws['%s%s' % (col_aph[5], row_first)].number_format = '#,##0.00'
        row_con_first = rowx
        rowx += 1
        rowx = self.fill_asset(list_pri, ws, date_year_end, rowx, col_aph, thin_border, year)
        row_con_last = rowx - 1

        ws.merge_cells('A%s:F%s' % (row_con_first, row_con_first))
        ws['%s%s' % (col_aph[0], row_con_first)].border = thin_border
        ws['%s%s' % (col_aph[1], row_con_first)].border = thin_border
        ws['%s%s' % (col_aph[2], row_con_first)].border = thin_border
        ws['%s%s' % (col_aph[3], row_con_first)].border = thin_border
        ws['%s%s' % (col_aph[4], row_con_first)].border = thin_border
        ws['%s%s' % (col_aph[5], row_con_first)].border = thin_border
        ws['%s%s' % (col_aph[0], row_con_first)] = u"II. Хувийн орон сууц  ДҮН"
        ws['%s%s' % (col_aph[0], row_con_first)].alignment = Alignment(horizontal='center')
        ws['%s%s' % (col_aph[0], row_con_first)].font = ws['%s%s' % (col_aph[0], row_con_first)].font.copy(size=8, bold=True,
                                                                                       name='Times New Roman')
        ws['%s%s' % (col_aph[5], row_con_first)].font = ws['%s%s' % (col_aph[0], row_con_first)].font.copy(size=8, bold=False,
                                                                                         name='Times New Roman')
        ws['%s%s' % (col_aph[5], row_con_first)].alignment = Alignment(horizontal='right')

        ws['%s%s' % (col_aph[5], row_con_first)] = '=SUM('+col_aph[5]+str(row_con_first + 1)+':'+col_aph[5]+str(row_con_last)+')' if list_pri else 0

        row_pub_first = rowx
        rowx += 1
        rowx = self.fill_asset(list_pub, ws, date_year_end, rowx, col_aph, thin_border, year)
        row_pub_last = rowx - 1

        ws.merge_cells('A%s:F%s' % (row_pub_first, row_pub_first))
        ws['%s%s' % (col_aph[0], row_pub_first)].border = thin_border
        ws['%s%s' % (col_aph[1], row_pub_first)].border = thin_border
        ws['%s%s' % (col_aph[2], row_pub_first)].border = thin_border
        ws['%s%s' % (col_aph[3], row_pub_first)].border = thin_border
        ws['%s%s' % (col_aph[4], row_pub_first)].border = thin_border
        ws['%s%s' % (col_aph[5], row_pub_first)].border = thin_border
        ws['%s%s' % (col_aph[0], row_pub_first)] = u"III. Нийтийн эзэмшлийн барилга, байгууламж  ДҮН"
        ws['%s%s' % (col_aph[0], row_pub_first)].alignment = Alignment(horizontal='center')
        ws['%s%s' % (col_aph[0], row_pub_first)].font = ws['%s%s' % (col_aph[0], row_pub_first)].font.copy(size=8, bold=True,
                                                                                         name='Times New Roman')
        ws['%s%s' % (col_aph[5], row_pub_first)].font = ws['%s%s' % (col_aph[0], row_pub_first)].font.copy(size=8, bold=False,
                                                                                         name='Times New Roman')
        ws['%s%s' % (col_aph[5], row_pub_first)].alignment = Alignment(horizontal='right')
        ws['%s%s' % (col_aph[5], row_pub_first)] = '=SUM('+col_aph[5]+str(row_pub_first + 1)+':'+col_aph[5]+str(row_pub_last)+')' if list_pub else 0

        ws.merge_cells('A%s:F%s' % (rowx, rowx))
        ws['%s%s' % (col_aph[0], rowx)].border = thin_border
        ws['%s%s' % (col_aph[1], rowx)].border = thin_border
        ws['%s%s' % (col_aph[2], rowx)].border = thin_border
        ws['%s%s' % (col_aph[3], rowx)].border = thin_border
        ws['%s%s' % (col_aph[4], rowx)].border = thin_border
        ws['%s%s' % (col_aph[5], rowx)].border = thin_border
        ws['%s%s' % (col_aph[5], rowx)].number_format = '#,##0.00'
        ws['%s%s' % (col_aph[0], rowx)] = u"НИЙТ ДҮН"
        ws['%s%s' % (col_aph[0], rowx)].alignment = Alignment(horizontal='center')
        ws['%s%s' % (col_aph[0], rowx)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=9, bold=True,
                                                                                         name='Times New Roman')
        ws['%s%s' % (col_aph[5], rowx)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=8, bold=False,
                                                                                         name='Times New Roman')
        ws['%s%s' % (col_aph[5], rowx)].alignment = Alignment(horizontal='right')
        ws['%s%s' % (col_aph[5], rowx)] = '=SUM('+col_aph[5]+str(row_first)+'+'+col_aph[5]+str(row_con_first)+'+'+col_aph[
            5]+str(row_pub_first)+')'
        all_value_pos = rowx
        rowx += 2
        ws['%s%s' % (col_aph[0], rowx)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=10, bold=True,
                                                                                         name='Times New Roman')
        ws['%s%s' % (col_aph[0], rowx)] = u"Д.Тайлант жилд хасагдсан үл хөдлөх эд хөрөнгийн үлдэх хугацаанд ногдох албан татварын тооцоо /төгрөгөөр/"
        rowx += 2

        ws.merge_cells('A%s:F%s' % (rowx, rowx + 1))
        ws.merge_cells('G%s:I%s' % (rowx, rowx))
        ws.merge_cells('J%s:K%s' % (rowx, rowx))
        ws['%s%s' % ('A', rowx)].border = thin_border
        ws['%s%s' % ('G', rowx)].border = thin_border
        ws['%s%s' % ('J', rowx)].border = thin_border
        ws['%s%s' % ('A', rowx)] = u'Хөрөнгө нэр төрөл'
        ws['%s%s' % ('G', rowx)] = u'Хасагдсан хөрөнгө'
        ws['%s%s' % ('J', rowx)] = u'Татварын тооцоо'
        ws['%s%s' % ('A', rowx)].font = ws['%s%s' % ('A', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        sheet.row_dimensions[rowx + 1].height = 30
        ws['%s%s' % ('A', rowx)].alignment = Alignment(vertical='center', horizontal='center', wrap_text=True)
        ws['%s%s' % ('G', rowx)].font = ws['%s%s' % ('G', rowx)].font.copy(size=8, bold=True,
                                                                                         name='Times New Roman')
        ws['%s%s' % ('G', rowx)].alignment = Alignment(horizontal='center')
        ws['%s%s' % ('J', rowx)].font = ws['%s%s' % ('G', rowx)].font.copy(size=8, bold=True,
                                                                                         name='Times New Roman')
        ws['%s%s' % ('J', rowx)].alignment = Alignment(horizontal='center')
        rowx += 1

        ws['%s%s' % ('G', rowx)].border = thin_border
        ws['%s%s' % ('H', rowx)].border = thin_border
        ws['%s%s' % ('I', rowx)].border = thin_border
        ws['%s%s' % ('J', rowx)].border = thin_border
        ws['%s%s' % ('K', rowx)].border = thin_border

        ws['%s%s' % ('G', rowx)] = u'Он, сар, өдөр'
        ws['%s%s' % ('H', rowx)] = u'Гэрчилгээний дугаар'
        ws['%s%s' % ('I', rowx)] = u'Үнэлгээ'
        ws['%s%s' % ('J', rowx)] = u'Үлдэх хугацаа'
        ws['%s%s' % ('K', rowx)] = u'Ноогдох татвар'

        ws['%s%s' % ('G', rowx)].font = ws['%s%s' % ('G', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('G', rowx)].alignment = Alignment(horizontal='center', vertical='center',
                                                       wrap_text=True)
        ws['%s%s' % ('H', rowx)].font = ws['%s%s' % ('H', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('H', rowx)].alignment = Alignment(horizontal='center', vertical='center',
                                                       wrap_text=True)
        ws['%s%s' % ('I', rowx)].font = ws['%s%s' % ('I', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('I', rowx)].alignment = Alignment(horizontal='center', vertical='center',
                                                       wrap_text=True)
        ws['%s%s' % ('J', rowx)].font = ws['%s%s' % ('J', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('J', rowx)].alignment = Alignment(horizontal='center', vertical='center',
                                                       wrap_text=True)
        ws['%s%s' % ('K', rowx)].font = ws['%s%s' % ('K', rowx)].font.copy(size=8, bold=True,
                                                                           name='Times New Roman')
        ws['%s%s' % ('K', rowx)].alignment = Alignment(horizontal='center', vertical='center',
                                                       wrap_text=True)
        rowx += 1
        ws.merge_cells('A%s:F%s' % (rowx, rowx))
        ws['%s%s' % (col_aph[0], rowx)].border = thin_border
        ws['%s%s' % (col_aph[1], rowx)].border = thin_border
        ws['%s%s' % (col_aph[2], rowx)].border = thin_border
        ws['%s%s' % (col_aph[3], rowx)].border = thin_border
        ws['%s%s' % (col_aph[4], rowx)].border = thin_border
        ws['%s%s' % (col_aph[5], rowx)].border = thin_border

        ws['%s%s' % (col_aph[0], rowx)] = 'I'
        ws['%s%s' % (col_aph[1], rowx)] = 'II'
        ws['%s%s' % (col_aph[2], rowx)] = 'III'
        ws['%s%s' % (col_aph[3], rowx)] = 'IV'
        ws['%s%s' % (col_aph[4], rowx)] = 'V'
        ws['%s%s' % (col_aph[5], rowx)] = 'VI'
        ws['%s%s' % (col_aph[0], rowx)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=8, bold=True,
                                                                                         name='Times New Roman')
        ws['%s%s' % (col_aph[0], rowx)].alignment = Alignment(horizontal='center')
        ws['%s%s' % (col_aph[1], rowx)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=8, bold=True,
                                                                                         name='Times New Roman')
        ws['%s%s' % (col_aph[1], rowx)].alignment = Alignment(horizontal='center')
        ws['%s%s' % (col_aph[2], rowx)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=8, bold=True,
                                                                                         name='Times New Roman')
        ws['%s%s' % (col_aph[2], rowx)].alignment = Alignment(horizontal='center')
        ws['%s%s' % (col_aph[3], rowx)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=8, bold=True,
                                                                                         name='Times New Roman')
        ws['%s%s' % (col_aph[3], rowx)].alignment = Alignment(horizontal='center')
        ws['%s%s' % (col_aph[4], rowx)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=8, bold=True,
                                                                                         name='Times New Roman')
        ws['%s%s' % (col_aph[4], rowx)].alignment = Alignment(horizontal='center')
        ws['%s%s' % (col_aph[5], rowx)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=8, bold=True,
                                                                                         name='Times New Roman')
        ws['%s%s' % (col_aph[5], rowx)].alignment = Alignment(horizontal='center')
        rowx += 1

        closed_mas_first = rowx
        rowx += 1

        rowx = self.fill_closed_asset(closed_list_mas, ws, check_date, rowx, col_aph, thin_border, year)

        closed_mas_last = rowx - 1

        ws.merge_cells('A%s:F%s' % (closed_mas_first, closed_mas_first))
        ws['%s%s' % (col_aph[0], closed_mas_first)].border = thin_border
        ws['%s%s' % (col_aph[1], closed_mas_first)].border = thin_border
        ws['%s%s' % (col_aph[2], closed_mas_first)].border = thin_border
        ws['%s%s' % (col_aph[3], closed_mas_first)].border = thin_border
        ws['%s%s' % (col_aph[4], closed_mas_first)].border = thin_border
        ws['%s%s' % (col_aph[5], closed_mas_first)].border = thin_border
        ws['%s%s' % (col_aph[0], closed_mas_first)] = u"I. Үйлдвэрлэл, үйлчилгээний зориулалттай  ДҮН"
        ws['%s%s' % (col_aph[0], closed_mas_first)].alignment = Alignment(horizontal='center')
        ws['%s%s' % (col_aph[0], closed_mas_first)].font = ws['%s%s' % (col_aph[0], closed_mas_first)].font.copy(size=8,
                                                                                                           bold=True,
                                                                                                           name='Times New Roman')
        ws['%s%s' % (col_aph[5], closed_mas_first)].font = ws['%s%s' % (col_aph[0], closed_mas_first)].font.copy(size=8,
                                                                                                           bold=False,
                                                                                                           name='Times New Roman')
        ws['%s%s' % (col_aph[5], closed_mas_first)].alignment = Alignment(horizontal='right')

        ws['%s%s' % (col_aph[5], closed_mas_first)] = '=SUM(' + col_aph[5] + str(closed_mas_first + 1) + ':' + col_aph[
            5] + str(closed_mas_last) + ')' if closed_list_mas else 0
        ws['%s%s' % (col_aph[5], closed_mas_first)].number_format = '#,##0.00'
        closed_pri_first = rowx
        rowx += 1
        rowx = self.fill_closed_asset(closed_list_pri, ws, check_date, rowx, col_aph, thin_border, year)
        closed_pri_last = rowx - 1

        ws.merge_cells('A%s:F%s' % (closed_pri_first, closed_pri_first))
        ws['%s%s' % (col_aph[0], closed_pri_first)].border = thin_border
        ws['%s%s' % (col_aph[1], closed_pri_first)].border = thin_border
        ws['%s%s' % (col_aph[2], closed_pri_first)].border = thin_border
        ws['%s%s' % (col_aph[3], closed_pri_first)].border = thin_border
        ws['%s%s' % (col_aph[4], closed_pri_first)].border = thin_border
        ws['%s%s' % (col_aph[5], closed_pri_first)].border = thin_border
        ws['%s%s' % (col_aph[0], closed_pri_first)] = u"II. Хувийн орон сууц  ДҮН"
        ws['%s%s' % (col_aph[0], closed_pri_first)].alignment = Alignment(horizontal='center')
        ws['%s%s' % (col_aph[0], closed_pri_first)].font = ws['%s%s' % (col_aph[0], closed_pri_first)].font.copy(size=8,
                                                                                                           bold=True,
                                                                                                           name='Times New Roman')
        ws['%s%s' % (col_aph[5], closed_pri_first)].font = ws['%s%s' % (col_aph[0], closed_pri_first)].font.copy(size=8,
                                                                                                           bold=False,
                                                                                                           name='Times New Roman')
        ws['%s%s' % (col_aph[5], closed_pri_first)].alignment = Alignment(horizontal='right')

        ws['%s%s' % (col_aph[5], closed_pri_first)] = '=SUM(' + col_aph[5] + str(closed_pri_first + 1) + ':' + col_aph[
            5] + str(closed_pri_last) + ')' if closed_list_pri else 0

        ws['%s%s' % (col_aph[5], closed_pri_first)].number_format = '#,##0.00'

        closed_pub_first = rowx
        rowx += 1
        rowx = self.fill_closed_asset(closed_list_pub, ws, check_date, rowx, col_aph, thin_border, year)
        closed_pub_last = rowx - 1

        ws.merge_cells('A%s:F%s' % (closed_pub_first, closed_pub_first))
        ws['%s%s' % (col_aph[0], closed_pub_first)].border = thin_border
        ws['%s%s' % (col_aph[1], closed_pub_first)].border = thin_border
        ws['%s%s' % (col_aph[2], closed_pub_first)].border = thin_border
        ws['%s%s' % (col_aph[3], closed_pub_first)].border = thin_border
        ws['%s%s' % (col_aph[4], closed_pub_first)].border = thin_border
        ws['%s%s' % (col_aph[5], closed_pub_first)].border = thin_border
        ws['%s%s' % (col_aph[0], closed_pub_first)] = u"III. Нийтийн эзэмшлийн барилга, байгууламж  ДҮН"
        ws['%s%s' % (col_aph[0], closed_pub_first)].alignment = Alignment(horizontal='center')
        ws['%s%s' % (col_aph[0], closed_pub_first)].font = ws['%s%s' % (col_aph[0], closed_pub_first)].font.copy(size=8,
                                                                                                                 bold=True,
                                                                                                                 name='Times New Roman')
        ws['%s%s' % (col_aph[5], closed_pub_first)].font = ws['%s%s' % (col_aph[0], closed_pub_first)].font.copy(size=8,
                                                                                                                 bold=False,
                                                                                                                 name='Times New Roman')
        ws['%s%s' % (col_aph[5], closed_pub_first)].alignment = Alignment(horizontal='right')

        ws['%s%s' % (col_aph[5], closed_pub_first)] = '=SUM(' + col_aph[5] + str(closed_pub_first + 1) + ':' + col_aph[
            5] + str(closed_pub_last) + ')' if closed_list_pub else 0

        ws['%s%s' % (col_aph[5], closed_pub_first)].number_format = '#,##0.00'

        ws.merge_cells('A%s:F%s' % (rowx, rowx))
        ws['%s%s' % (col_aph[0], rowx)].border = thin_border
        ws['%s%s' % (col_aph[1], rowx)].border = thin_border
        ws['%s%s' % (col_aph[2], rowx)].border = thin_border
        ws['%s%s' % (col_aph[3], rowx)].border = thin_border
        ws['%s%s' % (col_aph[4], rowx)].border = thin_border
        ws['%s%s' % (col_aph[5], rowx)].border = thin_border
        ws['%s%s' % (col_aph[0], rowx)] = u"НИЙТ ДҮН"
        ws['%s%s' % (col_aph[0], rowx)].alignment = Alignment(horizontal='center')
        ws['%s%s' % (col_aph[0], rowx)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=9, bold=True,
                                                                                         name='Times New Roman')

        ws['%s%s' % (col_aph[5], rowx)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=8, bold=False,
                                                                                         name='Times New Roman')
        ws['%s%s' % (col_aph[5], rowx)].alignment = Alignment(horizontal='right')

        ws['%s%s' % (col_aph[5], rowx)] = '=SUM('+col_aph[5]+str(closed_mas_first)+'+'+col_aph[
            5]+str(closed_pri_first)+'+'+col_aph[5]+str(closed_pub_first)+')'
        ws['%s%s' % (col_aph[5], rowx)].number_format = '#,##0.00'
        closed_all_value_pos = rowx

        ws['%s%s' % (col_aph[3], 39)] = '=' + col_aph[5] + str(all_value_pos)
        ws['%s%s' % (col_aph[3], 41)] = '=' + col_aph[5] + str(closed_all_value_pos)

        rowx += 2
        ws['%s%s' % ('B', rowx)] = u'Тайлан гаргасан: '
        ws['%s%s' % ('D', rowx)] = value_dict['second_sign']
        ws['%s%s' % ('H', rowx)] = u'Тайлан хүлээн авсан: '
        ws['%s%s' % ('J', rowx)] = '/ ______________ /'
        ws['%s%s' % ('B', rowx)].font = ws['%s%s' % ('B', rowx)].font.copy(size=9, bold=True,
                                                                                         name='Times New Roman')
        ws['%s%s' % ('H', rowx)].font = ws['%s%s' % ('H', rowx)].font.copy(size=9, bold=True,
                                                                                         name='Times New Roman')
        ws['%s%s' % (col_aph[0], rowx)].alignment = Alignment(vertical='center')
        rowx += 2
        ws['%s%s' % ('B', rowx)] = u'(Тэмдэг)'
        ws['%s%s' % ('C', rowx)] = '/ ______________ /'
        ws['%s%s' % ('H', rowx)] = u'(Тэмдэг)'
        ws['%s%s' % ('J', rowx)] = '/ ______________ /'
        ws['%s%s' % ('B', rowx)].font = ws['%s%s' % ('B', rowx)].font.copy(size=9, bold=True,
                                                                                         name='Times New Roman')
        ws['%s%s' % ('H', rowx)].font = ws['%s%s' % ('H', rowx)].font.copy(size=9, bold=True,
                                                                                         name='Times New Roman')
        rowx += 2

        ws['%s%s' % ('F', rowx)] = value_dict['date_created']

        sheet = wb.active
        rows = sheet.rows
        for row in rows:
            for cell in row:
                for line in lines:
                    if value_dict.has_key(cell.value):
                        cell.value = value_dict[cell.value]
                    if line['key'] != False and line['key'] == cell.value:
                        cell.value = line['amount']
                        break
        wb.save(generate_file)

        file_name = "TT23_%s.xlsx" % (time.strftime('%Y%m%d_%H%M'),)
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('TT23'),
                                                                                     form_title=file_name).create({})
        image = open(generate_file, 'rb')
        image_read = image.read()
        report_excel_output_obj.filedata = base64.encodestring(image_read)
        return report_excel_output_obj.export_report()
    # Тайлант жилд бүртгэлтэй байгаа нээлттэй хөрөнгө өрөх хэсэг
    def fill_asset(self, assets, ws, date_year_end, rowx, col_aph, thin_border, year):
        if assets:
            for asset in assets:
                date = datetime.strptime(asset['date'], "%Y-%m-%d").date()
                date_dif = date_year_end - date
                value = asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset[
                    'value']
                ws.merge_cells('A%s:F%s' % (rowx, rowx))
                ws['%s%s' % (col_aph[0], rowx)].border = thin_border
                ws['%s%s' % (col_aph[1], rowx)].border = thin_border
                ws['%s%s' % (col_aph[2], rowx)].border = thin_border
                ws['%s%s' % (col_aph[3], rowx)].border = thin_border
                ws['%s%s' % (col_aph[4], rowx)].border = thin_border
                ws['%s%s' % (col_aph[5], rowx)].border = thin_border

                ws['%s%s' % (col_aph[0], rowx)] = asset['name']
                ws['%s%s' % (col_aph[1], rowx)] = date
                ws['%s%s' % (col_aph[2], rowx)] = asset['number']
                ws['%s%s' % (col_aph[3], rowx)] = value
                ws['%s%s' % (col_aph[4], rowx)] = date_dif.days
                ws['%s%s' % (col_aph[5], rowx)] = (value*asset['percent']*date_dif.days/year)/100

                ws['%s%s' % (col_aph[0], rowx)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=8, bold=True,
                                                                                                 name='Times New Roman')
                ws['%s%s' % (col_aph[0], rowx)].alignment = Alignment(horizontal='center')
                ws['%s%s' % (col_aph[1], rowx)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=8, bold=False,
                                                                                                 name='Times New Roman')
                ws['%s%s' % (col_aph[1], rowx)].alignment = Alignment(horizontal='right')
                ws['%s%s' % (col_aph[2], rowx)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=8, bold=False,
                                                                                                 name='Times New Roman')
                ws['%s%s' % (col_aph[2], rowx)].alignment = Alignment(horizontal='right')
                ws['%s%s' % (col_aph[3], rowx)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=8, bold=False,
                                                                                                 name='Times New Roman')
                ws['%s%s' % (col_aph[3], rowx)].alignment = Alignment(horizontal='right')
                ws['%s%s' % (col_aph[4], rowx)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=8, bold=False,
                                                                                                 name='Times New Roman')
                ws['%s%s' % (col_aph[4], rowx)].alignment = Alignment(horizontal='right')
                ws['%s%s' % (col_aph[5], rowx)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=8, bold=False,
                                                                                                 name='Times New Roman')
                ws['%s%s' % (col_aph[5], rowx)].alignment = Alignment(horizontal='right')
                ws['%s%s' % (col_aph[5], rowx)].number_format = '#,##0.00'
                ws['%s%s' % (col_aph[3], rowx)].number_format = '#,##0.00'
                rowx += 1
        return rowx
    # Тайлант жилд хаагдсан хөрөнгө өрөх хэсэг
    def fill_closed_asset(self, assets, ws, check_date, rowx, col_aph, thin_border, year):
        if assets:
            for asset in assets:
                date = datetime.strptime(asset['closed_date'], "%Y-%m-%d").date()
                date_dif = date - check_date
                value = asset['record'] if asset['record'] > 0 else asset['insurance'] if asset['insurance'] > 0 else asset[
                    'value']
                ws.merge_cells('A%s:F%s' % (rowx, rowx))
                ws['%s%s' % (col_aph[0], rowx)].border = thin_border
                ws['%s%s' % (col_aph[1], rowx)].border = thin_border
                ws['%s%s' % (col_aph[2], rowx)].border = thin_border
                ws['%s%s' % (col_aph[3], rowx)].border = thin_border
                ws['%s%s' % (col_aph[4], rowx)].border = thin_border
                ws['%s%s' % (col_aph[5], rowx)].border = thin_border
                ws['%s%s' % (col_aph[0], rowx)] = asset['name']
                ws['%s%s' % (col_aph[1], rowx)] = date
                ws['%s%s' % (col_aph[2], rowx)] = asset['number']
                ws['%s%s' % (col_aph[3], rowx)] = value
                ws['%s%s' % (col_aph[4], rowx)] = date_dif.days
                ws['%s%s' % (col_aph[5], rowx)] = (value*asset['percent']*date_dif.days/year)/100

                ws['%s%s' % (col_aph[0], rowx)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=8, bold=True,
                                                                                                 name='Times New Roman')
                ws['%s%s' % (col_aph[0], rowx)].alignment = Alignment(horizontal='center')
                ws['%s%s' % (col_aph[1], rowx)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=8, bold=False,
                                                                                                 name='Times New Roman')
                ws['%s%s' % (col_aph[1], rowx)].alignment = Alignment(horizontal='right')
                ws['%s%s' % (col_aph[2], rowx)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=8, bold=False,
                                                                                                 name='Times New Roman')
                ws['%s%s' % (col_aph[2], rowx)].alignment = Alignment(horizontal='right')
                ws['%s%s' % (col_aph[3], rowx)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=8, bold=False,
                                                                                                 name='Times New Roman')
                ws['%s%s' % (col_aph[3], rowx)].alignment = Alignment(horizontal='right')
                ws['%s%s' % (col_aph[4], rowx)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=8, bold=False,
                                                                                                 name='Times New Roman')
                ws['%s%s' % (col_aph[4], rowx)].alignment = Alignment(horizontal='right')
                ws['%s%s' % (col_aph[5], rowx)].font = ws['%s%s' % (col_aph[0], rowx)].font.copy(size=8, bold=False,
                                                                                                 name='Times New Roman')
                ws['%s%s' % (col_aph[5], rowx)].alignment = Alignment(horizontal='right')
                ws['%s%s' % (col_aph[5], rowx)].number_format = '#,##0.00'

                ws['%s%s' % (col_aph[3], rowx)].number_format = '#,##0.00'
                rowx += 1
        return rowx
