# -*- encoding: utf-8 -*-
##############################################################################
from odoo import api, fields, models, _
from odoo.modules import get_module_resource
from datetime import datetime
import json
import openpyxl
from io import BytesIO
import time
import base64

class TaxTT24Report(models.TransientModel):
    """ТТ-2 тайлан
    """
    _name = "tax.tt24.report"
    _description = "Tax TT-24 Report"

    company_id = fields.Many2one('res.company', string='Company', readonly=True, default=lambda self: self.env.user.company_id)
    date_from = fields.Date(string='Start Date')
    date_to = fields.Date(string='End Date')
    fiscalyear_id = fields.Many2one('account.fiscalyear', string='Fiscal Year', required=True)
    underpayment = fields.Many2one('account.account', 'Underpayment account', required=True)
    overpayment = fields.Many2one('account.account', 'Overpayment account', required=True)
    executive_sign = fields.Many2one('res.users','Executive Signature',required=True)
    general_accountant_sign = fields.Many2one('res.users', 'General Accountant Signature', required=True)

    @api.onchange('fiscalyear_id')
    def onchange_fiscalyear(self):
        # Санхүүгийн жилийн утга өөрчлөгдөхөд компани дээр бүртгэлтэй дансыг харуулах
        if self.fiscalyear_id:
            date_from = self.fiscalyear_id.date_start
            date_to = self.fiscalyear_id.date_stop
            now_day = time.strftime('%Y-%m-%d')
            if now_day > date_from and now_day < date_to:
                date_to = now_day
            self.date_from = date_from
            self.date_to = date_to
            self.company_id = self.fiscalyear_id.company_id
            self.executive_sign = self.fiscalyear_id.company_id.executive_signature
            self.general_accountant_sign = self.fiscalyear_id.company_id.genaral_accountant_signature

    @api.multi
    def export_report(self):
        company = self.fiscalyear_id.company_id
        value_dict = {'ttd': '',
                        'company_name': u'%s' % (company.name),
                        'fiscal_year': self.fiscalyear_id.name + u' он',
                        'under': 0,
                        'over': 0,
                        'sum1': 0,
                        'gasoline_a': 0,
                        'gasoline_b': 0,
                        'gasoline_c': 0,
                        'gasoline_d': 0,
                        'gasoline_e': 0,
                        'gasoline_f': 0,
                        'diesal_a': 0,
                        'diesal_b': 0,
                        'diesal_c': 0,
                        'diesal_d': 0,
                        'diesal_e': 0,
                        'diesal_f': 0,
                        'law_1': 0,
                        'law_2': 0,
                        'law_3': 0,
                        'law_4': 0,
                        'each_sum': 0,
                        'sign1': self.executive_sign.name,
                        'sign3': self.general_accountant_sign.name,
                        'date': u'%s оны %s сарын %s -ны өдөр' % (time.strftime('%Y'), time.strftime('%m'), time.strftime('%d'))
                      }

        if company.company_registry:
            value_dict.update({'ttd': company.company_registry})

        template_file = get_module_resource('l10n_mn_account_tax_report', 'static/src/report', 'TT24_3-input.xlsx')
        generate_file = get_module_resource('l10n_mn_account_tax_report', 'static/src/report', 'TT24_3-output.xlsx')

        options = self.env['account.tax.report.option'].search([('report', '=', 'tt24'), ('parent_id', '=', False)])
        lines = self.env['account.tax.report.option'].with_context(date_from=self.date_from, date_to=self.date_to,
                                                                   company_id=self.fiscalyear_id.company_id.id).create_report_data(options)

        last_year = '0201' + str(datetime.today().year)
        date_year_end = datetime.strptime((str(datetime.today().year) + '-12-31'), "%Y-%m-%d").date()
        date = datetime.strptime(last_year, "%d%m%Y").date()
        initial_under = self.env['account.move.line'].get_initial_balance(company.id, self.underpayment.ids, date,
                                                                          'posted')
        if initial_under:
            under_credit = initial_under[0]['start_credit']
            underbalance = under_credit
            value_dict.update({'under': underbalance})
        initial_over = self.env['account.move.line'].get_initial_balance(company.id, self.overpayment.ids, date,
                                                                         'posted')                                                            
        if initial_over:
            over_debit = initial_over[0]['start_debit']
            overbalance = over_debit
            value_dict.update({'over': overbalance})

        self.env.cr.execute("SELECT aaa.engine_type as type, aaa.date_of_manufacture as date,"
                            " aaa.automobiles_and_self_moving_vehicles as auto, aaa.co2_emissions as co2, aaa.cylinder_capacitance as cyl "
                            "FROM account_asset_asset aaa "
                            "INNER JOIN account_asset_category aac on aaa.category_id = aac.id "
                            "WHERE aaa.company_id = "
                            "'" + str(company.id) + "' AND aac.is_motor = TRUE AND aaa.state != " + "'" + 'draft' + "'")

        assets = self.env.cr.dictfetchall()

        #Parametr
        gasoline_a = 0
        gasoline_b = 0
        gasoline_c = 0
        gasoline_d = 0
        gasoline_e = 0
        gasoline_f = 0
        diesal_a = 0
        diesal_b = 0
        diesal_c = 0
        diesal_d = 0
        diesal_e = 0
        diesal_f = 0
        law_1 = 0
        law_2 = 0
        law_3 = 0
        law_4 = 0
        each_sum = 0

        #Бараа тоолох хэсэг
        for asset in assets:
            today = datetime.today().year 
            date_manu = datetime.strptime(asset['date'],"%Y-%m-%d").date().year
            dif_year = today - date_manu
            if asset['type'] == 'gasoline':
                check = True
                if asset['co2'] == 'a':
                    gasoline_a += 1
                    if dif_year <= 4:
                        law_2 += 1
                        check = False
                elif asset['co2'] == 'b':
                    gasoline_b += 1
                    if dif_year <= 4:
                        law_3 += 1
                        check = False
                elif asset['co2'] == 'c':
                    gasoline_c += 1
                elif asset['co2'] == 'd':
                    gasoline_d += 1
                elif asset['co2'] == 'e':
                    gasoline_e += 1 
                elif asset['co2'] == 'f':
                    gasoline_f += 1
                else:
                    law_1 += 1
                    check = False
                if check:
                    if asset['auto']:
                        law_4 += 1
            else:
                if asset['cyl'] <= 1500:
                    diesal_a += 1
                elif asset['cyl'] <= 2500:
                    diesal_b += 1
                elif asset['cyl'] <= 3500:
                    diesal_c += 1
                elif asset['cyl'] <= 4500:
                    diesal_d += 1
                elif asset['cyl'] <= 5500:
                    diesal_e += 1
                elif asset['cyl'] > 5500:
                    diesal_f += 1
                if asset['auto']:
                    law_4 += 1
        
        each_sum = ""
        # updates
        value_dict.update({'gasoline_a':gasoline_a,
                        'gasoline_b':gasoline_b,
                        'gasoline_c':gasoline_c,
                        'gasoline_d':gasoline_d,
                        'gasoline_e':gasoline_e,
                        'gasoline_f':gasoline_f,
                        'diesal_a':diesal_a,
                        'diesal_b':diesal_b,
                        'diesal_c':diesal_c,
                        'diesal_d':diesal_d,
                        'diesal_e':diesal_e,
                        'diesal_f':diesal_f,
                        'law_1': law_1,
                        'law_2': law_2,
                        'law_3': law_3,
                        'law_4': law_4,
                        'each_sum': each_sum})
        wb = openpyxl.load_workbook(template_file)
        sheet = wb.active
        rows = sheet.rows
        for row in rows:
            for cell in row:
                for line in lines:
                    if value_dict.has_key(cell.value):
                        cell.value = value_dict[cell.value]
                    if line['key'] != False and line['key'] == cell.value:
                        cell.value = line['amount']
                        break
        
        wb.save(generate_file)
        file_name = "ТТ24_%s.xlsx" % (time.strftime('%Y%m%d_%H%M'),)
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('TT24'),
                                                                                     form_title=file_name).create({})
        image = open(generate_file, 'rb')
        image_read = image.read()
        report_excel_output_obj.filedata = base64.encodestring(image_read)
        return report_excel_output_obj.export_report()