# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################
{
    'name': 'Mongolian Accounting Tax Reports',
    'version': '1.0',
    'depends': [
        'l10n_mn_account_tax_report',
        'l10n_mn_purchase'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """Худалдан авалт дээрх НӨАТ-ын үзүүлэлт""",
    'data': [
        'views/purchase_order_view.xml'
    ],
    "auto_install": True,
    "installable": True,
}
