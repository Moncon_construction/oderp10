# -*- coding: utf-8 -*-
from odoo import models, api, fields


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    def _prepare_invoice_line_from_po_line(self, line):
        res = super(AccountInvoice, self)._prepare_invoice_line_from_po_line(line)
        res.update({'is_taxpayer': True,
                    'vat_indication_id': line.vat_indication_id.id})
        return res
