from odoo import models, api, fields


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    vat_indication_id = fields.Many2one('vat.indication', 'Vat Indication', domain=[('indication_type', '=', 'outcome')])
    is_taxpayer = fields.Boolean(string='Tax Payer', default=lambda self: self.env.user.company_id.partner_id.is_taxpayer)
    is_vat_include = fields.Boolean(default=False, string='Tax Report Type')


class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    vat_indication_id = fields.Many2one('vat.indication', 'Vat Indication', domain=[('indication_type', '=', 'outcome')])
    is_taxpayer = fields.Boolean(string='Tax Payer', default=lambda self: self.env.user.company_id.partner_id.is_taxpayer)
    is_vat_include = fields.Boolean(related='order_id.is_vat_include', string='Is Vat Include')

    @api.onchange('taxes_id')
    def onchange_taxes_id(self):
        for obj in self:
            if obj.is_taxpayer and obj.taxes_id:
                for tax_line in obj.taxes_id:
                    if tax_line.tax_report_type == '4':
                        obj.order_id.is_vat_include = True
                        break
                    else:
                        obj.order_id.is_vat_include = False
            else:
                obj.order_id.is_vat_include = False
