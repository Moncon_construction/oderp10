# -*- coding: utf-8 -*-
from odoo import models, api, fields


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    vat_indication_id = fields.Many2one('vat.indication', 'Vat Indication', domain=[('indication_type', '=', 'income')])
    is_taxpayer = fields.Boolean(string='Tax Payer', default=lambda self: self.env.user.company_id.partner_id.is_taxpayer)
    is_vat_include = fields.Boolean(default=False, string='Tax Report Type')


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    vat_indication_id = fields.Many2one('vat.indication', 'Vat Indication', domain=[('indication_type', '=', 'income')])
    is_taxpayer = fields.Boolean(string='Tax Payer', default=lambda self: self.env.user.company_id.partner_id.is_taxpayer)
    is_vat_include = fields.Boolean(related='order_id.is_vat_include', string='Tax Report Type')

    @api.onchange('tax_id')
    def onchange_tax_id(self):
        for obj in self:
            if obj.is_taxpayer and obj.tax_id:
                for tax_line in obj.tax_id:
                    if tax_line.tax_report_type == '4':
                        obj.order_id.is_vat_include = True
                        break
                    else:
                        obj.order_id.is_vat_include = False
            else:
                obj.order_id.is_vat_include = False

    @api.multi
    def _prepare_invoice_line(self, qty):
        res = super(SaleOrderLine, self)._prepare_invoice_line(qty)
        res.update({'vat_indication_id': self.vat_indication_id.id})
        return res
