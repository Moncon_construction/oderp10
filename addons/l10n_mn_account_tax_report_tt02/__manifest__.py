# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################
{
    'name': 'Mongolian Accounting TT-02 Tax Report',
    'version': '1.0',
    'depends': ['l10n_mn_account_tax_report'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """TT-02 Аж ахуй нэгжийн орлогын албан татварын тайлан - Шинэчилсэн хувилбар, Хавсралт мэдээний хамт гарна""",
    'data': [
        'security/ir.model.access.csv',
        'data/tt02_data.xml',
        'views/report_option_view.xml',
        'views/tt02_tax_report_view.xml',
    ],
    "auto_install": False,
    "installable": True,
}
