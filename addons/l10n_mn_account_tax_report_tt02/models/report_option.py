# -*- encoding: utf-8 -*-
##############################################################################
from odoo import fields, models

"""
   АЖ АХУЙ НЭГЖИЙН ОРЛОГЫН АЛБАН ТАТВАРЫН ТАЙЛАНД ЗОРИУЛСАН БҮРТГЭЛ
"""

ADD_SELECTION = [('tax_tt02', 'New Income Tax Report TT02'),
                 ('xm02_1', 'Income Tax Report TT-02 XM-02(1)'),
                 ('xm02_2', 'Income Tax Report TT-02 XM-02(2)'),
                 ('xm02_3a', 'Income Tax Report TT-02 XM-02(3)A'),
                 ('xm02_3b', 'Income Tax Report TT-02 XM-02(3)B'),
                 ('xm02_3c', 'Income Tax Report TT-02 XM-02(3)C'),
                 ('xm02_9', 'Income Tax Report TT-02 XM-02(9)')]

class AccountTaxReportOption(models.Model):
    _inherit = 'account.tax.report.option'

    report = fields.Selection(selection_add=ADD_SELECTION)

