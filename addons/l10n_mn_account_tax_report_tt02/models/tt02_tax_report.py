# -*- encoding: utf-8 -*-
##############################################################################
import time
from io import BytesIO
import base64
import time
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from operator import itemgetter

from odoo import api, fields, models
from odoo.tools.translate import _
from odoo.exceptions import UserError
from odoo.tools.safe_eval import safe_eval as eval
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles

"""
   АЖ АХУЙ НЭГЖИЙН ОРЛОГЫН АЛБАН ТАТВАРЫН ТАЙЛАНД ЗОРИУЛСАН БҮРТГЭЛ
"""

class TT02TaxReport(models.Model):
    _name = "tt02.tax.report"
    _description = "TT-02 Tax Report"

    name = fields.Char('Name')
    company_id = fields.Many2one('res.company', string='Company', required=True, readonly=True,
                                 default=lambda self: self.env.user.company_id, track_visibility='onchange', states={'confirmed': [('readonly', True)]})
    date_from = fields.Date(string='Start Date', required=True, default=lambda *a: time.strftime('%Y-01-01'), track_visibility='onchange', states={'confirmed': [('readonly', True)]})
    date_to = fields.Date(string='End Date', required=True, default=lambda *a: time.strftime('%Y-%m-%d'), track_visibility='onchange', states={'confirmed': [('readonly', True)]})
    state = fields.Selection([('draft', 'Draft'),
                              ('confirmed', 'Confirmed')], 'State', required=True, default='draft')
    line_ids = fields.One2many('tt02.tax.report.line', 'report_id', string='Lines', copy=False, states={'confirmed': [('readonly', True)]})
    xm1_ids = fields.One2many('tt02.tax.report.xm1', 'report_id', string='TT-02 XM-02(1)', copy=False, states={'confirmed': [('readonly', True)]})
    xm2_ids = fields.One2many('tt02.tax.report.xm2', 'report_id', string='TT-02 XM-02(2)', copy=False, states={'confirmed': [('readonly', True)]})
    xm3a_ids = fields.One2many('tt02.tax.report.xm3a', 'report_id', string='TT-02 XM-02(3)A', copy=False, states={'confirmed': [('readonly', True)]})
    xm3b_ids = fields.One2many('tt02.tax.report.xm3b', 'report_id', string='TT-02 XM-02(3)B', copy=False, states={'confirmed': [('readonly', True)]})
    xm3c_ids = fields.One2many('tt02.tax.report.xm3c', 'report_id', string='TT-02 XM-02(3)C', copy=False, states={'confirmed': [('readonly', True)]})
    xm9_ids = fields.One2many('tt02.tax.report.xm9', 'report_id', string='TT-02 XM-02(9)', copy=False, states={'confirmed': [('readonly', True)]})

    # Компани, эхлэх болон дуусах огноогоор нэр талбар бөглөгдөх
    @api.onchange('date_from', 'date_to')
    def onchange_tax_report(self):
        for report in self:
            name = ''
            if report.company_id:
                name += _('%s is ') % self.company_id.name
            if report.date_from:
                name += _('[%s - ') % self.date_from
            if report.date_to:
                name += _('%s] duration TT-02 Tax Report') % self.date_to
            report.name = name

    def action_draft(self):
        # ТТ-02 тайлангийн мөрүүд бол Хавсралт мэдээний мөрүүдийг хоослох
        self.line_ids = None
        self.xm1_ids = None
        self.xm2_ids = None
        self.xm3a_ids = None
        self.xm3b_ids = None
        self.xm3c_ids = None
        self.xm9_ids = None
        self.write({'state': 'draft'})

    def action_confirm(self):
        self.write({'state': 'confirmed'})

    @api.multi
    def create_lines(self):
        # ТТ-02 тайлангийн мөрүүд бол Хавсралт мэдээний мөрүүдийг хоослох
        self.line_ids = None
        self.xm1_ids = None
        self.xm2_ids = None
        self.xm3a_ids = None
        self.xm3b_ids = None
        self.xm3c_ids = None
        self.xm9_ids = None
        # ТТ-02 тайлангийн мөрүүд бол Хавсралт мэдээний мөрүүдийг үүсгэх
        report_option_obj = self.env['account.tax.report.option']
        report_option_obj._create_lines(self.id, 'tt02.tax.report.line', 'tax_tt02')
        report_option_obj._create_lines(self.id, 'tt02.tax.report.xm1', 'xm02_1')
        report_option_obj._create_lines(self.id, 'tt02.tax.report.xm2', 'xm02_2')
        report_option_obj._create_lines(self.id, 'tt02.tax.report.xm3a', 'xm02_3a')
        report_option_obj._create_lines(self.id, 'tt02.tax.report.xm3b', 'xm02_3b')
        report_option_obj._create_lines(self.id, 'tt02.tax.report.xm3c', 'xm02_3c')
        report_option_obj._create_lines(self.id, 'tt02.tax.report.xm9', 'xm02_9')
        return True

    @api.multi
    def compute(self):
        # ТТ-02 тайлангийн мөрүүд бол Хавсралт мэдээний мөрүүдэд утга оноох
        # Данс болон дансны төрөл бол дүнг олдог хэсэг нэмэх
        # Python тооцоолол үед бодож олох. report_sequence талбарын дагуу тооцоолол хийх
        options = []
        report_option_obj = self.env['account.tax.report.option']
        options += report_option_obj.get_lines('tt02.tax.report.xm1', self.xm1_ids)
        options += report_option_obj.get_lines('tt02.tax.report.xm2', self.xm2_ids)
        options += report_option_obj.get_lines('tt02.tax.report.xm3a', self.xm3a_ids)
        options += report_option_obj.get_lines('tt02.tax.report.xm3b', self.xm3b_ids)
        options += report_option_obj.get_lines('tt02.tax.report.xm3c', self.xm3c_ids)
        options += report_option_obj.get_lines('tt02.tax.report.line', self.line_ids)
        options += report_option_obj.get_lines('tt02.tax.report.xm9', self.xm9_ids)
        self.env['account.tax.report.option'].compute_report(options, self.date_from, self.date_to, self.company_id)
        return True

    @api.multi
    def get_format(self, book):
        # create format
        return {'content_text': book.add_format(ReportExcelCellStyles.format_content_text),
                'content_float': book.add_format(ReportExcelCellStyles.format_content_float),
                'group': book.add_format(ReportExcelCellStyles.format_group),
                }

    @api.multi
    def get_column(self, sheet, add):
        # compute column
        sheet.set_column('A:A', 80)
        sheet.set_column('B:B', 18)
        if add:
            sheet.set_column('C:C', 50)
        return sheet

    @api.multi
    def get_sheet(self, sheet, add):
        # create sheet
        sheet.set_portrait()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 0
        sheet = self.get_column(sheet, add)
        return sheet, rowx

    @api.multi
    def get_header(self, sheet, rowx, format_group, add):
        # Товчоо тайлангийн хүснэгтийн толгой зурах
        sheet.write(rowx, 0, _('Expense Indication'), format_group)
        sheet.write(rowx, 1, _('Amount'), format_group)
        if add:
            sheet.write(rowx, 2, _('Төлбөрт таавар, бооцоот тоглоом, эд мөнгөний хонжворт сугалааны үйл ажиллагааны зардлын дүн'), format_group)
        return sheet

    @api.multi
    def get_value(self, sheet, rowx, format, line, add):
        # Тайлангийн мөрийн утгыг зурах
        sheet.write(rowx, 0, line.report_option_id.name, format['content_text'])
        sheet.write(rowx, 1, line.amount, format['content_float'])
        if add:
            if line.principle_amount > 0:
                sheet.write(rowx, 2, line.principle_amount, format['content_float'])
            else:
                sheet.write(rowx, 2, '', format['content_float'])
        return sheet

    @api.multi
    def export(self, book, sheet, lines, add):
        # create formats
        format = self.get_format(book)
        # create sheet
        sheet, rowx = self.get_sheet(sheet, add)
        # create header
        sheet = self.get_header(sheet, rowx, format['group'], add)
        rowx += 1
        # Тайлангийн мөрийг зурах
        for line in lines:
            if line.report_option_id.type != 'view':
                sheet = self.get_value(sheet, rowx, format, line, add)
                rowx += 1
        return True

    @api.multi
    def export_info(self):
        # ТТ-02 ХМ-2 мэдээ
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        # create name
        report_name = _('TT-02 XM-2 Tax Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('tt02_xm2_tax_report'), form_title=file_name).create({})
        # ТТ-02 ХМ-2 3(А) хэвлэх
        if self.xm3a_ids:
            sheet = book.add_worksheet(_('TT-02 XM-02(3)A'))
            self.export(book, sheet, self.xm3a_ids, False)
        # ТТ-02 ХМ-2 3(Б) хэвлэх
        if self.xm3b_ids:
            sheet2 = book.add_worksheet(_('TT-02 XM-02(3)B'))
            self.export(book, sheet2, self.xm3b_ids, True)
        # ТТ-02 ХМ-2 3(В) хэвлэх
        if self.xm3c_ids:
            sheet3 = book.add_worksheet(_('TT-02 XM-02(3)C'))
            self.export(book, sheet3, self.xm3c_ids, False)
        # create footer
        book.close()
        # set file dat
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report()


class TT02TaxReportLine(models.Model):
    _name = 'tt02.tax.report.line'
    _description = 'TT-02 Tax Report Line'

    report_option_id = fields.Many2one('account.tax.report.option', string='Indications')
    report_id = fields.Many2one('tt02.tax.report', string='TT02 Tax Report', ondelete="cascade")
    sequence = fields.Char('Line Number')
    amount = fields.Float(string='Amount', digits=(16, 2), default=0)
    color = fields.Selection(related='report_option_id.color', readonly=True)
    type = fields.Selection(related='report_option_id.type', readonly=True)


class TT02TaxReportXM1(models.Model):
    _name = 'tt02.tax.report.xm1'
    _description = 'TT-02 XM-02(1) Tax Report'

    sequence = fields.Char('Sequence')
    report_option_id = fields.Many2one('account.tax.report.option', string='Type of Income On Tax Free')
    report_id = fields.Many2one('tt02.tax.report', string='TT02 Tax Report', ondelete="cascade")
    amount = fields.Float(string='Total Income', digits=(16, 2), default=0)
    color = fields.Selection(related='report_option_id.color', readonly=True)
    type = fields.Selection(related='report_option_id.type', readonly=True)


class TT02TaxReportXM2(models.Model):
    _name = 'tt02.tax.report.xm2'
    _description = 'TT-02 XM-02(2) Tax Report'

    sequence = fields.Char('Sequence')
    report_option_id = fields.Many2one('account.tax.report.option', string='Type of Sales Revenue')
    report_id = fields.Many2one('tt02.tax.report', string='TT02 Tax Report', ondelete="cascade")
    amount = fields.Float(string='Total Income', digits=(16, 2), default=0)
    color = fields.Selection(related='report_option_id.color', readonly=True)
    type = fields.Selection(related='report_option_id.type', readonly=True)


class TT02TaxReportXM3A(models.Model):
    _name = 'tt02.tax.report.xm3a'
    _description = 'TT-02 XM-02(3)A Tax Report'

    sequence = fields.Char('Sequence')
    report_option_id = fields.Many2one('account.tax.report.option', string='Indications')
    report_id = fields.Many2one('tt02.tax.report', string='TT02 Tax Report', ondelete="cascade")
    amount = fields.Float(string='Total Expense', digits=(16, 2), default=0)
    color = fields.Selection(related='report_option_id.color', readonly=True)
    type = fields.Selection(related='report_option_id.type', readonly=True)


class TT02TaxReportXM3B(models.Model):
    _name = 'tt02.tax.report.xm3b'
    _description = 'TT-02 XM-02(3)B Tax Report'

    sequence = fields.Char('Sequence')
    report_option_id = fields.Many2one('account.tax.report.option', string='Expense Type')
    report_id = fields.Many2one('tt02.tax.report', string='TT02 Tax Report', ondelete="cascade")
    amount = fields.Float(string='Total Expense', digits=(16, 2), default=0)
    principle_amount = fields.Float(string='Principle Expense', digits=(16, 2))
    color = fields.Selection(related='report_option_id.color', readonly=True)
    type = fields.Selection(related='report_option_id.type', readonly=True)


class TT02TaxReportXM3C(models.Model):
    _name = 'tt02.tax.report.xm3c'
    _description = 'TT-02 XM-02(3)C Tax Report'

    sequence = fields.Char('Sequence')
    report_option_id = fields.Many2one('account.tax.report.option', string='Expense Type')
    report_id = fields.Many2one('tt02.tax.report', string='TT02 Tax Report', ondelete="cascade")
    amount = fields.Float(string='Total Expense', digits=(16, 2), default=0)
    color = fields.Selection(related='report_option_id.color', readonly=True)
    type = fields.Selection(related='report_option_id.type', readonly=True)


class TT02TaxReportXM9(models.Model):
    _name = 'tt02.tax.report.xm9'
    _description = 'TT-02 XM-02(9) Tax Report'

    sequence = fields.Char('Sequence')
    report_option_id = fields.Many2one('account.tax.report.option', string='Expense Type')
    report_id = fields.Many2one('tt02.tax.report', string='TT02 Tax Report', ondelete="cascade")
    amount = fields.Float(string='Amount', digits=(16, 2), default=0)
    color = fields.Selection(related='report_option_id.color', readonly=True)
    type = fields.Selection(related='report_option_id.type', readonly=True)