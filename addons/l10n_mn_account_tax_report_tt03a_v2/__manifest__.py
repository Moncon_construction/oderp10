# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################

{
    'name': 'Mongolian Accounting Tax Reports',
    'version': '1.0',
    'depends': ['l10n_mn_account_tax_report'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """ТТ-03а тайлан журналын бичилтээс гаргах модуль""",
    'data': [
        'views/account_move_view.xml',
    ],
    "auto_install": False,
    "installable": True,
}
