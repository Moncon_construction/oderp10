
# -*- coding: utf-8 -*-
from odoo import models, api, fields


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    def action_invoice_open(self):
        res = super(AccountInvoice, self).action_invoice_open()
        for line in self.invoice_line_ids:
            if line.vat_indication_id:
                for move_line in self.move_id.line_ids.filtered(lambda l: l.account_id.id == line.account_id.id):
                    move_line.write({
                        'vat_indication_id': line.vat_indication_id.id
                    })
        return res
