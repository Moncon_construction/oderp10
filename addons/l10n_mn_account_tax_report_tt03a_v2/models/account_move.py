from odoo import models, api, fields


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    is_taxpayer = fields.Boolean(string='Tax Payer', default=lambda self: self.env.user.company_id.partner_id.is_taxpayer)
    vat_indication_id = fields.Many2one('vat.indication', 'Vat Indication')
    choose_vat_indication = fields.Boolean(related='account_id.choose_vat_indication')

    @api.onchange('account_id')
    def _onchange_account_id(self):
        domain = {}
        account_id = self.account_id
        if account_id and account_id.choose_vat_indication:
            if account_id.internal_type == 'income':
                self._cr.execute("SELECT vat_indication_income_id FROM account_vat_indication_income_rel where account_id = " + str(account_id.id))
            else:
                self._cr.execute("SELECT vat_indication_outcome_id FROM account_vat_indication_outcome_rel where account_id = " + str(account_id.id))
            indications = self._cr.fetchall()
            if len(indications) == 1:
                self.vat_indication_id = indications[0][0]
            if indications:
                domain['vat_indication_id'] = [('id', 'in', indications)]
                return {'domain': domain}
            else:
                domain['vat_indication_id'] = []
                return {'domain': domain}
        else:
            self.vat_indication_id = False
