# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################

from odoo import models, fields, api, _
from odoo.tools.translate import _
import time
from odoo import SUPERUSER_ID
from operator import itemgetter
from odoo.tools.safe_eval import safe_eval as eval


class AccountTaxReportOption(models.Model):
    _inherit = 'account.tax.report.option'

    @api.multi
    def create_report_data(self, options, rounding=False, extradict=None):
        # Override хйиж журналын бичилтээс утгуудаа авдаг болгосон
        ''' Баланc тайлангийн мэдээллийг боловсруулж
            тайлангийн форматад тохируулан python [{},{},{}...]
            загвараар хүснэгтийн мөр багануудын өгөгдлийг боловсруулна.

            @return: cols - Хүснэгтийн толгойн өгөгдлүүд
                     rows - Хүснэгтийн биеийн өгөгдлүүд
        '''
        vat_indication_obj = self.env['vat.indication']
        context = self.env.context
        company_id = context['company_id']
        date_from = context['date_from']
        date_to = context['date_to']

        def get_childs(option, number, level=0):
            childgrid = []
            for child in option.child_ids:
                row = {}
                row['name'] = child.name
                row['number'] = number
                row['level'] = level + 1
                row['code'] = child.code
                row['type'] = child.type
                row['sequence'] = child.sequence
                row['key'] = child.excel_key
                row['key2'] = 0
                row['tax_percent'] = child.tax_percent
                """ tt12-н үзүүлэлтийн мөрийн урт нь 22 байдаг бол tt13-нх 24 байдаг """
                adder = 24
                if child.report == 'tt12':
                    adder = 22
                if child.report in ('tt13', 'tt12'):
                    if row['key']:
                        split1 = child.excel_key.split('{')
                        split2 = split1[1].split('}')
                        int_split = int(split2[0]) + adder
                        string_split = '{' + str(int_split) + '}'
                        row['key2'] = string_split
                a = 0
                b = 0
                if child.type in ('account', 'tax'):
                    """ Татвартай үед, tt12 нь child-ууд дээрээ tax-тай бол tt13 нь parent-үүд дээрээ tax-тай харин child-ууд дээрээ данс сонгох"""
                    if child.parent_id.tax_id or (child.type == 'tax' and child.tax_id):
                        invoice_ids = []
                        inv_line_ids = []
                        inv_ids = []
                        tax_ids = []
                        in_ids = []
                        in_ids2 = []
                        total_partner = []
                        if child.type == 'tax' and child.tax_id:
                            self._cr.execute("SELECT invoice_line_id as invoice_line_id "
                                             "FROM account_invoice_line_tax A "
                                             "LEFT JOIN account_invoice_line line ON line.id = invoice_line_id "
                                             "LEFT JOIN account_invoice invoice ON invoice.id = line.invoice_id "
                                             "LEFT JOIN account_move move ON move.id = invoice.move_id "
                                             "WHERE A.tax_id = %s AND invoice.state in ('open','paid') AND move.date BETWEEN %s AND %s ", (child.tax_id.id, date_from, date_to))
                        else:
                            self._cr.execute('SELECT invoice_line_id as invoice_line_id FROM account_invoice_line_tax A WHERE A.tax_id = %s' % child.parent_id.tax_id.id)
                        in_ids = self._cr.fetchall()
                        if child.account_ids:
                            in_ids2 = self.env['account.invoice.line'].search([('account_id', 'in', child.account_ids.ids), ('id', 'in', in_ids), ('company_id', '=', company_id)])
                        else:
                            in_ids2 = self.env['account.invoice.line'].search([('id', 'in', in_ids), ('company_id', '=', company_id)])
                        for in_id in in_ids2:
                            if in_id:
                                inv_line_ids.append(self.env['account.invoice.line'].browse(in_id.id))
                        for inv_line_id in inv_line_ids:
                            inv_ids.append(inv_line_id.invoice_id)
                        if inv_ids:
                            for inv_id in inv_ids:
                                if inv_id.partner_id.id not in total_partner:
                                    total_partner.append(inv_id.partner_id.id)
                                # тт12 дээр татваргүй дүнгийн нийлбэр бодогдоно
                                if child.type == 'tax':
                                    a = a + inv_id.amount_untaxed
                                else:
                                    a = a + inv_id.amount_total
                            b = len(total_partner)
                        else:
                            a = 0
                    else:
                        if child.account_ids:
                            if child.end_balance:
                                for account in child.account_ids:
                                    initial_bal = self.env['account.move.line'].get_initial_balance(company_id, [account.id], date_from, 'posted')
                                    # initial_bal = self.env['account.account'].get_initial_balance([account.id])
                                    if initial_bal:
                                        a += initial[0]['start_credit'] - initial[0]['start_debit']
                                        # a += initial_bal[account.id]['credit'] - initial_bal[account.id]['debit']
                            else:
                                account_ids = []
                                where = ' '
                                [account_ids.append(account.id) for account in child.account_ids]
                                # account_id = self.env['account.account'].search([('parent_id', 'child_of', account_ids)], order='code')
                                # if account_id:
                                #     account_id = account_id.ids
                                if child.partner_ids:
                                    partner_ids = []
                                    [partner_ids.append(partner.id) for partner in child.partner_ids]
                                    where = 'AND l.partner_id in %s' % tuple(partner_ids)
                                self._cr.execute("SELECT  sum(l.debit) AS debit, sum(l.credit) AS credit, at.type AS type "
                                                 "FROM account_move_line l "
                                                 "LEFT JOIN account_move m ON l.move_id = m.id "
                                                 "JOIN account_journal j ON l.journal_id = j.id "
                                                 "JOIN account_account aa ON l.account_id = aa.id "
                                                 "JOIN account_account_type at ON aa.user_type_id = at.id "
                                                 "WHERE m.date BETWEEN %s AND %s "
                                                 "AND l.company_id = %s AND m.state = 'posted' "
                                                 "AND l.account_id in %s "
                                                 "GROUP BY at.type ", (date_from, date_to, company_id, tuple(account_ids)))
                                lines = self._cr.dictfetchall()
                                for line in lines:
                                    if child.balance_type == 'balance':
                                        if line['type'] in ('expense', 'asset'):
                                            a += line['debit'] - line['credit']
                                        else:
                                            a += line['credit'] - line['debit']
                                    elif child.balance_type == 'debit':
                                        a += line['debit']
                                    elif child.balance_type == 'credit':
                                        a += line['credit']
                if rounding:
                    a /= 1000.0
                if child.type == 'python':
                    row['python'] = child.python_compute
                localdict.setdefault(child.code, a)
                row['amount'] = a
                row['total_partner'] = b
                if child.report == 'tt03a':
                    # Хэрвээ тухайн татаж буй тайлан нь tt03a бол нэхэжмлэхийн мөрүүдээс НӨАТ-ийн үзүүлэлтийн нэрээр шүүж нийт дүнг бодож олно.
                    indication_id = vat_indication_obj.search([('name', '=', row['name'])])
                    if indication_id:
                        if indication_id.indication_type == 'income':
                            select = '-'
                        else:
                            select = '+'
                        query = '''
                            SELECT
                                SUM(''' + select + '''aml.balance) as amount,
                                COUNT(aml.partner_id) as total_partner
                            FROM
                                account_move_line aml
                                LEFT JOIN account_move am ON aml.move_id = am.id
                            WHERE
                                am.state = 'posted' AND
                                am.date BETWEEN '%s' AND '%s' AND
                                am.company_id = %s AND
                                aml.vat_indication_id = %s
                        ''' % (date_from, date_to,
                               company_id,
                               str(indication_id.id))
                        self._cr.execute(query)
                        indications = self._cr.dictfetchall()
                        if indications:
                            for indication in indications:
                                row['amount'] = indication['amount']
                                row['total_partner'] = indication['total_partner']
                if child.show:
                    number += 1
                childgrid2, number = get_childs(child, number, level=level + 1)
                childgrid.append(row)
                childgrid += childgrid2
            return childgrid, number

        def childs_sum(parent_code):
            option = self.search([('code', '=', parent_code)])
            a = 0
            if option:
                childs = self.search([('parent_id', '=', option[0].id)])
                if childs:
                    for c in self.read(childs, ['code']):
                        a += localdict.get(c['code'], 0)
            return a

        if context is None:
            context = {}
        if extradict is None:
            extradict = {}
        rows = []

        localdict = extradict

        if options:
            line_number = 0
            for option in options:
                row = {}
                row['name'] = option.name
                row['number'] = line_number
                row['code'] = option.code
                row['type'] = option.type
                row['sequence'] = option.sequence
                row['show'] = option.show
                row['key'] = option.excel_key
                row['tax_percent'] = option.tax_percent
                """ tt12-н үзүүлэлтийн мөрийн урт нь 22 байдаг бол tt13-нх 24 байдаг """
                adder = 24
                if option.report == 'tt12':
                    adder = 22
                if option.excel_key:
                    split1 = option.excel_key.split('{')
                    split2 = split1[1].split('}')
                    int_split = int(split2[0]) + adder
                    string_split = '{' + str(int_split) + '}'
                    row['key2'] = string_split
                else:
                    row['key2'] = 0
                a = 0
                b = 0
                if option.type in ('account', 'tax'):
                    """ Татвартай үед, tt12 нь child-ууд дээрээ tax-тай бол tt13 нь parent-үүд дээрээ tax-тай харин child-ууд дээрээ данс сонгох"""
                    if option.parent_id.tax_id or (option.type == 'tax' and option.tax_id):
                        invoice_ids = []
                        inv_line_ids = []
                        inv_ids = []
                        tax_ids = []
                        in_ids = []
                        in_ids2 = []
                        total_partner = []
                        if child.type == 'tax' and child.tax_id:
                            self._cr.execute("SELECT invoice_line_id as invoice_line_id "
                                             "FROM account_invoice_line_tax A "
                                             "LEFT JOIN account_invoice_line line ON line.id = invoice_line_id "
                                             "LEFT JOIN account_invoice invoice ON invoice.id = line.invoice_id "
                                             "LEFT JOIN account_move move ON move.id = invoice.move_id "
                                             "WHERE A.tax_id = %s AND invoice.state in ('open','paid') AND move.date BETWEEN %s AND %s ", (option.tax_id.id, date_from, date_to))
                        else:
                            self._cr.execute('SELECT invoice_line_id as invoice_line_id FROM account_invoice_line_tax A WHERE A.tax_id = %s' % option.parent_id.tax_id.id)
                        in_ids = self._cr.fetchall()
                        if option.account_ids:
                            in_ids2 = self.env['account.invoice.line'].search([('account_id', 'in', option.account_ids.ids), ('id', 'in', in_ids), ('company_id', '=', company_id)])
                        else:
                            in_ids2 = self.env['account.invoice.line'].search([('id', 'in', in_ids), ('company_id', '=', company_id)])
                        for in_id in in_ids2:
                            if in_id:
                                inv_line_ids.append(self.env['account.invoice.line'].browse(in_id.id))
                        for inv_line_id in inv_line_ids:
                            inv_ids.append(inv_line_id.invoice_id)
                        if inv_ids:
                            for inv_id in inv_ids:
                                if inv_id.partner_id.id not in total_partner:
                                    total_partner.append(inv_id.partner_id.id)
                                # тт12 дээр татваргүй дүнгийн нийлбэр бодогдоно
                                if child.type == 'tax':
                                    a = a + inv_id.amount_untaxed
                                else:
                                    a = a + inv_id.amount_total
                            b = len(total_partner)
                        else:
                            a = 0
                    else:
                        if option.account_ids:
                            if option.end_balance:
                                for account in option.account_ids:

                                    initial_bal = self.env['account.move.line'].get_initial_balance(company_id, [account.id], date_from, 'posted')
                                    if initial_bal:
                                        a += initial[0]['start_credit'] - initial[0]['start_debit']
                            else:
                                account_ids = []
                                where = ' '
                                [account_ids.append(account.id) for account in option.account_ids]
                                if option.partner_ids:
                                    partner_ids = []
                                    [partner_ids.append(partner.id) for partner in option.partner_ids]
                                    where = 'AND l.partner_id in %s' % tuple(partner_ids)
                                self._cr.execute("SELECT  sum(l.debit) AS debit, sum(l.credit) AS credit, at.type AS type "
                                                 "FROM account_move_line l "
                                                 "LEFT JOIN account_move m ON l.move_id = m.id "
                                                 "JOIN account_journal j ON l.journal_id = j.id "
                                                 "JOIN account_account aa ON l.account_id = aa.id "
                                                 "JOIN account_account_type at ON aa.user_type_id = at.id "
                                                 "WHERE m.date BETWEEN %s AND %s "
                                                 "AND l.company_id = %s AND m.state = 'posted' "
                                                 "AND l.account_id in %s "
                                                 "GROUP BY at.type ", (date_from, date_to, company_id, tuple(account_ids)))
                                lines = self._cr.dictfetchall()
                                for line in lines:
                                    if option.balance_type == 'balance':
                                        if line['type'] in ('expense', 'asset'):
                                            a += line['debit'] - line['credit']
                                        else:
                                            a += line['credit'] - line['debit']
                                    elif option.balance_type == 'debit':
                                        a += line['debit']
                                    elif option.balance_type == 'credit':
                                        a += line['credit']
                if rounding:
                    a /= 1000.0
                if option.type == 'python':
                    row['python'] = option.python_compute
                localdict.setdefault(option.code, a)
                row['amount'] = a
                row['level'] = 0
                row['total_partner'] = b
                if option.show:
                    line_number += 1
                childgrid, line_number = get_childs(option, line_number, level=0)
                rows.append(row)
                rows += childgrid

        localdict['CHILDS_SUM'] = childs_sum
        again_loop = []
        for r in sorted(rows, key=itemgetter('level', 'type'), reverse=True):
            if r.get('type', False) == 'python' and r.get('python', False):
                eval(r['python'], localdict, mode='exec', nocopy=True)
                a = localdict.get('amount', False)
                localdict[r['code']] = a
                r.update({'amount': a})
                if a == 0 or not a:  # Дарааллаас хамаараад уг үзүүлэлтийг тооцоолох болоогүй байж магадгүй.
                    # Тиймээс дахин нэг удаа тооцоолно.
                    again_loop.append(r)

        for r in sorted(again_loop, key=itemgetter('level', 'type', 'sequence'), reverse=True):
            eval(r['python'], localdict, mode='exec', nocopy=True)
            a = localdict.get('amount', False)
            localdict[r['code']] = a
            r.update({'amount': a})

        return sorted(rows, key=itemgetter('number'))
