# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################

{
    'name': 'Mongolian Accounting Vat Partner',
    'version': '1.0',
    'depends': ['l10n_mn_account', 'l10n_mn_account_tax_report'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """Худалдан авалтын нэхэмжлэхийн нөатын авлага дансны харилцагч нь өөр байх""",
    'data': [
        'views/account_vat_partner_view.xml',
    ],
    "auto_install": False,
    "installable": True,
}
