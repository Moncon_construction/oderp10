# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import Warning, UserError
from odoo.tools import float_is_zero

from datetime import datetime

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    value_added_tax_partner = fields.Many2one('res.partner', 'Value Added Tax Partner')

    @api.multi
    def action_move_create(self):
        res = super(AccountInvoice, self).action_move_create()
        for obj in self.filtered(lambda x: x.move_id and x.value_added_tax_partner):
            obj.move_id.mapped('line_ids').filtered(lambda x: x.account_id.is_vat is True).write({'partner_id': obj.value_added_tax_partner.id})
        return res