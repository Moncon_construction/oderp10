# -*- coding: utf-8 -*-
{
    "name": "Mongolian Account Wrong Transaction List",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "description": """
Санхүүгийн алдаатай гүйлгээний жагсаалт
-----------------------------------------------------------------------------------------------
    * Харилцагч сонгох ёстой журналын бичилтүүд
""",
    "website": "http://asterisk-tech.mn",
    "category": "Account",
    "depends": [
        'l10n_mn_account',
        'l10n_mn_account_asset',
    ],
    "data": [
        'security/ir.model.access.csv',
        'security/wrong_transaction_security.xml',
        'views/wrong_transaction_menu_view.xml',
        'views/account_journal_move_lines_menu.xml',
        'views/account_move_lines_menu_view.xml',
        'views/partner_required_move_line.xml',
        'views/stock_account_moves_without_stock_move_view.xml',
        'views/account_move_line_that_not_analytic_account.xml',
        'views/analytic_required_account_invoice_line.xml',

        'views/invoices_that_not_journal_entry.xml',
        'views/missing_transaction_menu_view.xml',
        'views/asset_dep_move_date_diff_view.xml',
        'views/unbalanced_account_move_line.xml',
        'views/account_asset_moves_line_views.xml',
        'views/account_bank_statement_views.xml',
        'views/unrequired_analytic_account_move_line_view.xml',
        'views/asset_move_date_diff_views.xml',
        'views/cash_journal_items_without_bank_statement_views.xml',
        'views/cashflow_wrong_account_move_line.xml',
        'views/cashflow_not_required_journal_items.xml',
        'views/close_asset_move_date_difference.xml',
        'views/closed_period_contain_not_dep_line.xml',
        'views/account_move_line_not_analytic_line_view.xml',
        'views/account_move_analytic_line_analytic_diff_view.xml',
        'views/minus_remaining_receivables_views.xml',
        'wizard/account_move_line_create_analytic_line_view.xml',
        'wizard/account_analytic_line_fix_view.xml',
    ],
    "active": False,
    "installable": True,
}
