#-*- coding: utf-8 -*-
from odoo import fields, models, api, _


class AccountBankStatement(models.Model):
    _inherit = "account.bank.statement"

    @api.multi
    def get_wrong_account_bank_statement(self):
        ids = []
        self.env.cr.execute(
            "select * from account_bank_statement abs left join account_bank_statement_line absl on abs.id = absl.statement_id left join account_move am on absl.id = am.statement_line_id where abs.state = 'confirm' and am.statement_line_id is null")
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]

        tree_id = self.env.ref("account.view_bank_statement_tree")
        form_id = self.env.ref("account.view_bank_statement_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Difference between account bank statement state and account bank statement line state'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.bank.statement',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }
