#-*- coding: utf-8 -*-
from odoo import fields, models, api, _

class AccountAnalyticLine(models.Model):
    _inherit = "account.analytic.line"

    related_analytic_account_id = fields.Many2one(related='move_id.analytic_account_id', string='Account move analytic account')

    @api.multi
    def get_account_move_analytic_line_analytic_diff(self):
        # Шинжилгээний бичилтийн шинжилгээний данс журналын мөрөөс өөр байвал шинжилгээний бичилтийг харуулах функц
        ids = []
        self.env.cr.execute("SELECT * from account_analytic_line aal, account_move_line aml "
                            "WHERE aal.move_id = aml.id AND aml.analytic_account_id IS NOT NULL AND aal.account_id != aml.analytic_account_id ")
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]

        tree_id = self.env.ref("l10n_mn_account_wrong_transaction.view_account_move_analytic_line_analytic_diff")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Account move line and analytic line analytic difference'),
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'account.analytic.line',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree')],
            'target': 'current',
        }





