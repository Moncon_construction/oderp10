#-*- coding: utf-8 -*-
from odoo import fields, models, api, _

class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    @api.multi
    def get_account_move_line_not_analytic_line(self):
        # Шинжилгээний данс сонгосон ч Шинжилгээний бичилт нь үүсээгүй журналыг мөрийг харуулах функц
        ids = []
        company_ids = False
        if self.env.user.company_ids:
            company_ids = self.env.user.company_ids.ids
        if company_ids:
            fetched = self.env['account.move.line'].search([('analytic_account_id', '!=', False), ('analytic_line_ids', '=', False), ('company_id', 'in', tuple(company_ids))])
        else:
            fetched = self.env['account.move.line'].search([('analytic_account_id', '!=', False), ('analytic_line_ids', '=', False)])
        if len(fetched) > 0:
            ids = fetched.ids

        tree_id = self.env.ref("account.view_move_line_tree")
        form_id = self.env.ref("account.view_move_line_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Account move line not analytic line'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move.line',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }



