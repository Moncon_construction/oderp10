#-*- coding: utf-8 -*-
from odoo import fields, models, api, _


class AccountMoveLineThatNotAnalyticAccount(models.Model):
    _name = "account.move.line"
    _inherit = "account.move.line"

    # Компанийн валютаарх дүн
    def _company_currency(self):
        for obj in self:
            from_currency = obj.currency_id
            to_currency = obj.company_id.currency_id
            currency_obj = obj.env['res.currency']

            if obj.amount_currency > 0:
                obj.company_currency = currency_obj._compute(to_currency,from_currency, obj.amount_currency)

    company_currency = fields.Float(compute='_company_currency', string='Company currency')

