#-*- coding: utf-8 -*-
from odoo import api, fields, models,tools

class DepreciationEffectiveDateDifference(models.Model):
    _name = "depreciation.effective.date.difference"
    _auto = False

    name = fields.Char('Name', related="asset_id.name")
    asset_id = fields.Many2one('account.asset.asset', 'Asset')
    depreciation_date = fields.Date('Depreciation Date')
    move_id = fields.Many2one('account.move', 'Account Move')
    effective_date = fields.Date('Account Move Date')
    difference_date = fields.Float('Date Diff')
    company_id = fields.Many2one('res.company', 'Company')

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self._cr, 'depreciation_effective_date_difference')
        self._cr.execute("""
                     CREATE or REPLACE view depreciation_effective_date_difference as 
                    SELECT
                        min(aa.id) as id,
                        aa.id as asset_id,
                        aa.depreciation_date as depreciation_date,
                        aa.move_id as move_id, 
                        aa.date as effective_date,
                        aa.difference_date as difference_date,
                        aa.company_id AS company_id 
                FROM (SELECT ass.id as id,adl.move_id as move_id, ass.company_id AS company_id, 
                        ass.name as asset_name, adl.depreciation_date as depreciation_date, 
                        am.date as date,(am.date - adl.depreciation_date) as difference_date
                        FROM account_asset_depreciation_line adl 
                        JOIN account_asset_asset ass ON (adl.asset_id=ass.id)
                        JOIN account_move am on (am.id = adl.move_id)
                        JOIN account_move_line aml on (aml.move_id = am.id)
                        GROUP BY ass.id, adl.move_id, am.date, adl.depreciation_date, ass.company_id) as aa
                GROUP BY
                    aa.id,
                    aa.asset_name,
                    aa.depreciation_date,
                    aa.date,
                    aa.difference_date, 
                    aa.move_id,
                    aa.company_id 
                Having aa.difference_date != 0
                """)