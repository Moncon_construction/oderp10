#-*- coding: utf-8 -*-
from odoo import fields, models, api, _


class CashflowWrongMoveLine(models.Model):
    _inherit = "account.move.line"

    def get_move_line_wrong_cashflow(self):
        ids = []
        self.env.cr.execute(
            """ select id from account_move_line 
                where cashflow_id in (select id from account_cashflow_type where value_of_amount_type = 'positive') 
                and credit > 0
                UNION
                select id from account_move_line
                where cashflow_id in (select id from account_cashflow_type where value_of_amount_type = 'negative') 
                and debit > 0
                UNION
                select id from account_move_line 
                where account_id in (select id from account_account 
                where user_type_id in (select id from account_account_type where is_cash = True or is_credit = True))
                and cashflow_id is null 
            """)
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]

        tree_id = self.env.ref("l10n_mn_account_wrong_transaction.view_cashflow_incorrectly_selected_move_line")
        form_id = self.env.ref("account.view_move_line_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Cashflow incorrectly selected move line'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move.line',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }