#-*- coding: utf-8 -*-
from odoo import api, fields, models, _, tools

class CloseAssetMoveDateDifference(models.Model):
    _name = "close.asset.move.date.difference"
    _order = 'closed_date'
    _auto = False

    name = fields.Char('Name', related="asset_id.name")
    asset_id = fields.Many2one('account.asset.asset', 'Asset')
    closed_date = fields.Date('Close Date')
    move_id = fields.Many2one('account.move', 'Account Move')
    move_date = fields.Date('Account Move Date')
    date_diff = fields.Float('Date Diff')
    company_id = fields.Many2one('res.company', 'Company')

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self._cr, 'close_asset_move_date_difference')
        self._cr.execute("""
                       CREATE VIEW close_asset_move_date_difference AS (
                         SELECT *
                         FROM 
                    (SELECT DISTINCT ON ( s.id) s.id AS asset_id,
                        s.closed_date AS closed_date,
                        am.id AS move_id,
                        am.date AS move_date,
                        ABS (s.closed_date::date - am.date::date) AS date_diff,
                        MAX(s.id) as id, s.company_id AS company_id 
                    FROM account_move am
                    JOIN account_move_line as ml on ml.move_id = am.id
                    JOIN account_asset_asset as s on s.id = ml.asset_id
                    WHERE  s.closed_date IS NOT NULL
                    GROUP BY s.id, am.id, s.company_id 
                    ORDER BY s.id ,am.date DESC ) as l
                  WHERE CAST (l.closed_date AS DATE) != l.move_date 
                )""")