#-*- coding: utf-8 -*-
from odoo import api, fields, models, _, tools

class ClosedPeriodContainNotDepLine(models.Model):
    _name = "closed.period.contain.not.dep.line"
    _auto = False

    asset_id = fields.Many2one('account.asset.asset', 'Asset')
    date_stop = fields.Date('Date Stop')
    depreciation_date = fields.Date('Depreciation Date')
    depreciated_value = fields.Float('Depreciated Value')
    company_id = fields.Many2one('res.company', 'Company')

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self._cr, 'closed_period_contain_not_dep_line')
        self._cr.execute("""
                       CREATE VIEW closed_period_contain_not_dep_line AS (
                         SELECT *
                         FROM 
                    (
                        SELECT adl.id AS id, adl.asset_id AS asset_id, adl.depreciation_date AS depreciation_date, 
                                adl.depreciated_value AS depreciated_value, ass.company_id AS company_id, 
                                (SELECT date_stop FROM account_period  WHERE state = 'done' ORDER BY date_stop desc limit 1) as date_stop
                        FROM account_asset_depreciation_line adl 
                        JOIN account_asset_asset ass ON (adl.asset_id=ass.id) 
                        WHERE adl.asset_id in (SELECT id FROM account_asset_asset where state = 'open')
                        and adl.move_check = False AND adl.depreciation_date < (SELECT date_stop FROM account_period WHERE state = 'done' ORDER BY date_stop desc limit 1)
                    ) as id)""")