#-*- coding: utf-8 -*-
from odoo import fields, models, api, _


class AccountWrongTransaction(models.Model):
    _inherit = "account.invoice"

    @api.multi
    def get_finance_wrong_transaction_list(self):
        ids = []
        self.env.cr.execute(
            "select * from account_invoice where move_id IS NULL")
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]

        tree_id = self.env.ref("account.invoice_tree")
        form_id = self.env.ref("account.invoice_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Invoices that not created journal entry'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.invoice',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }
