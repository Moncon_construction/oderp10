#-*- coding: utf-8 -*-
from odoo import api, fields, models, _, tools


class MinusRemainingReceivablesReport(models.Model):
    _name = "minus.remaining.receivables.report"
    _auto = False
    _order = "minus_date"

    minus_date = fields.Date(string='Date to minus')
    partner_id = fields.Many2one('res.partner', string='Partner')
    account_id = fields.Many2one('account.account', string='Minus Account')
    move_line_id = fields.Many2one('account.move.line', string='Move line')
    minus_remaining = fields.Float(string='Minus remaining')

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self._cr, 'minus_remaining_receivables_report')
        self._cr.execute("""
                           CREATE OR REPLACE FUNCTION receivable_lines()
                           RETURNS TABLE(
                               minus_date DATE,
                               partner_id INTEGER,
                               account_id INTEGER,
                               move_line_id INTEGER,
                               minus_remaining FLOAT 
                           ) AS $$
                           DECLARE
                               rec RECORD;
                               partner RECORD;
                               account RECORD;
                               initial FLOAT := 0;
                               end_balance FLOAT := 0;
                           BEGIN
                               FOR partner IN (SELECT f.p_id FROM (SELECT l.partner_id as p_id, l.account_id as acc_id 
                               FROM account_move_line l LEFT JOIN account_move m ON 
                                   l.move_id = m.id WHERE m.state='posted' AND 
                                   l.partner_id is not null and l.account_id in (SELECT a.id FROM account_account a
                               LEFT JOIN account_account_type t ON a.user_type_id=t.id where t.type='receivable')
                               GROUP BY l.partner_id, l.account_id HAVING sum(round(l.debit,2))-sum(round(l.credit,2)) < 0 ORDER BY l.partner_id) as f group by f.p_id) 
                               LOOP
                                   FOR account IN (
                                   SELECT f.acc_id FROM (SELECT l.partner_id as p_id, l.account_id as acc_id 
                                   FROM account_move_line l LEFT JOIN account_move m ON 
                                   l.move_id = m.id WHERE m.state='posted' AND 
                                   l.partner_id is not null and l.account_id in (SELECT a.id FROM account_account a
                                   LEFT JOIN account_account_type t ON a.user_type_id=t.id where t.type='receivable')
                                   GROUP BY l.partner_id, l.account_id HAVING sum(round(l.debit,2))-sum(round(l.credit,2)) < 0 ORDER BY l.partner_id) as f group by f.acc_id
                                   )
                                   LOOP                                  
                                       FOR rec in (SELECT l.id as id, l.partner_id as p_id, l.date as date, l.account_id as acc_id, 
                                       l.debit as debit, l.credit as credit 
                                       FROM account_move_line l LEFT JOIN account_move m ON 
                                       l.move_id = m.id WHERE m.state='posted' AND 
                                       l.account_id in (SELECT a.id FROM account_account a
                                       LEFT JOIN account_account_type t ON a.user_type_id=t.id where t.type='receivable') 
                                       AND l.partner_id IN (SELECT f.p_id FROM (SELECT l.partner_id as p_id, l.account_id as acc_id 
                                       FROM account_move_line l LEFT JOIN account_move m ON 
                                       l.move_id = m.id WHERE m.state='posted' AND 
                                       l.partner_id is not null and l.account_id in (SELECT a.id FROM account_account a
                                       LEFT JOIN account_account_type t ON a.user_type_id=t.id where t.type='receivable')
                                       GROUP BY l.partner_id, l.account_id HAVING sum(round(l.debit,2))-sum(round(l.credit,2)) < 0 ORDER BY l.partner_id) as f group by f.p_id)   
                                       ORDER BY l.partner_id, l.date asc, l.id asc)
                                           LOOP
                                               IF (partner.p_id = rec.p_id AND account.acc_id = rec.acc_id) THEN 
                                                   end_balance := end_balance + rec.debit - rec.credit;
                                                   IF (end_balance < 0) THEN 
                                                       move_line_id := rec.id;
                                                       partner_id := rec.p_id;
                                                       minus_date := rec.date;
                                                       account_id := rec.acc_id;
                                                       minus_remaining := rec.credit;
                                                       end_balance := 0;
                                                       initial := 0;  
                                                       EXIT;                                                                                            
                                                   END IF;
                                               END IF;
                                           END LOOP;
                                           end_balance := 0;
                                           initial := 0;
                                           RETURN NEXT;
                                   END LOOP;
                                   end_balance := 0;
                                   initial := 0;
                                   RETURN NEXT;
                               END LOOP;                            
                           END;
                           $$ LANGUAGE plpgsql;""")
        self._cr.execute("""
                    CREATE OR REPLACE VIEW minus_remaining_receivables_report as (
                    SELECT *, ROW_NUMBER() OVER (PARTITION BY true) as id 
                    FROM (SELECT f.minus_date, f.partner_id, f.account_id, f.move_line_id, f.minus_remaining 
                    FROM receivable_lines() as f WHERE f.partner_id is not null 
                    GROUP BY f.partner_id, f.account_id, f.minus_date, f.move_line_id, f.minus_remaining 
                    ORDER BY f.partner_id, f.minus_date) AS list) """)

    @api.multi
    def name_get(self):
        result = []
        for minus in self:
            result.append((minus.id, "%s (%s)" % (minus.partner_id.name or '', minus.minus_remaining or '')))
        return result
