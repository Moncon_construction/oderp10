# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _

class UnbalancedAccountMoveLine(models.Model):
    _inherit = "account.move.line"

    def get_unbalanced_account_move_line(self):
        ids = []
        self.env.cr.execute(""" SELECT move_id  FROM account_move_line GROUP BY move_id HAVING (sum(credit) - sum(debit)) NOT BETWEEN -0.01 AND 0.01 """)
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]

        tree_id = self.env.ref("account.view_move_tree")
        form_id = self.env.ref("account.view_move_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Unbalanced Move Line'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
            'context': {'create': False}
        }
