#-*- coding: utf-8 -*-
from odoo import fields, models, api, _


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    @api.multi
    def get_unrequired_analytic_account_move_line(self):
        ids = []
        self.env.cr.execute(
            "select * from account_move_line aml left join account_account aa on aml.account_id = aa.id where aml.analytic_account_id is not null and (aa.req_analytic_account = False or aa.req_analytic_account is null)")
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]

        tree_id = self.env.ref("account.view_move_line_tree")
        form_id = self.env.ref("account.view_move_line_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Unrequired analytic account on account move line'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move.line',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }
