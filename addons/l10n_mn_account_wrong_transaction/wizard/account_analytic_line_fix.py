# -*- coding: utf-8 -*-
from odoo import fields, api, models, _

class AccountAnalyticLineFix(models.TransientModel):
    _name = 'account.analytic.line.fix'
    _description = 'Account analytic line fix'

    trans_nbr = fields.Integer(string='# of Difference analytic account', readonly=True)

    @api.model
    def default_get(self, fields):
        res = super(AccountAnalyticLineFix, self).default_get(fields)
        data = self.trans_rec_get()
        if 'trans_nbr' in fields:
            res.update({'trans_nbr': data['trans_nbr']})
        return res

    @api.multi
    def trans_rec_get(self):
        context = self._context or {}
        lines = self.env['account.analytic.line'].browse(context.get('active_ids', []))
        number = 0
        for line in lines:
            if line.move_id and line.move_id.analytic_account_id and \
                    line.move_id.analytic_account_id != line.account_id:
                number += 1
        return {'trans_nbr': number}

    @api.multi
    def fix_analytic_line(self):
        # Шинжилгээний бичилтийн шинжилгээний данс журналын мөрөөс өөр байвал журналын мөрийн шинжилгээний дансаар солих функц
        context = self._context or {}
        lines = self.env['account.analytic.line'].browse(context.get('active_ids', []))
        for line in lines:
            if line.move_id and line.move_id.analytic_account_id and \
                    line.move_id.analytic_account_id != line.account_id:
                line.move_id.create_analytic_lines()
        # Жагсаалтыг шинэчлэн харуулах хэсэг
        ids = []
        self.env.cr.execute("SELECT * from account_analytic_line aal, account_move_line aml "
                            "WHERE aal.move_id = aml.id AND aml.analytic_account_id IS NOT NULL AND aal.account_id != aml.analytic_account_id ")
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]

        tree_id = self.env.ref("l10n_mn_account_wrong_transaction.view_account_move_analytic_line_analytic_diff")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Account move line and analytic line analytic difference'),
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'account.analytic.line',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree')],
            'target': 'current',
        }
