# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError

class AccountMoveLineCreateAnalyticLine(models.TransientModel):
    _name = 'account.move.line.create.analytic.line'
    _description = 'Create analytic line'

    trans_nbr = fields.Integer(string='# of Not Analytic line move', readonly=True)

    @api.model
    def default_get(self, fields):
        res = super(AccountMoveLineCreateAnalyticLine, self).default_get(fields)
        data = self.trans_rec_get()
        if 'trans_nbr' in fields:
            res.update({'trans_nbr': data['trans_nbr']})
        return res

    @api.multi
    def trans_rec_get(self):
        context = self._context or {}
        lines = self.env['account.move.line'].browse(context.get('active_ids', []))
        number = 0
        for line in lines:
            if line.analytic_account_id and len(line.analytic_line_ids) == 0:
                number += 1
        return {'trans_nbr': number}

    @api.multi
    def create_analytic_line(self):
        # Журналын мөрд сонгосон шинжилгээний дансаар шинжилгээний бичилт үүсгэх функц
        context = self._context or {}
        lines = self.env['account.move.line'].browse(context.get('active_ids', []))
        for line in lines:
            if line.analytic_account_id and len(line.analytic_line_ids) == 0:
                line.create_analytic_lines()
        # Жагсаалтыг шинэчлэх харуулах хэсэг
        ids = []
        company_ids = False
        if self.env.user.company_ids:
            company_ids = self.env.user.company_ids.ids
        if company_ids:
            fetched = self.env['account.move.line'].search([('analytic_account_id', '!=', False), ('analytic_line_ids', '=', False),('company_id', 'in', tuple(company_ids))])
        else:
            fetched = self.env['account.move.line'].search([('analytic_account_id', '!=', False), ('analytic_line_ids', '=', False)])
        if len(fetched) > 0:
            ids = fetched.ids

        tree_id = self.env.ref("account.view_move_line_tree")
        form_id = self.env.ref("account.view_move_line_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Account move line not analytic line'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move.line',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }

