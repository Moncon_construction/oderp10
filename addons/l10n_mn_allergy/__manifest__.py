# -*- coding: utf-8 -*-

{
    "name": "Mongolian Allergy Registration Module",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "category": "Mongolian Modules",
    "summary": "Allergy Registration",
    'description': "Allergy Registration",
    "website": "http://asterisk-tech.mn",
    'depends': [
        'mail',
        'l10n_mn_base',
    ],
    "data": [
        'security/allergy_security.xml',
        'security/ir.model.access.csv',

        'views/allergy_views.xml',
        'views/allergy_reason_views.xml',
        'views/allergy_symptom_views.xml',
        'views/allergy_precaution_views.xml',
        'views/allergy_tag_views.xml',
        
        'views/menus.xml'
    ],
}
