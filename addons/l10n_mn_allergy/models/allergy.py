# -*- coding: utf-8 -*-

from datetime import datetime

from odoo import models, fields, api, exceptions, _
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT


class AlergyReason(models.Model):
    _name = 'allergy.reason'
    _inherit = ['mail.thread']

    name = fields.Char(string='Reason', required=True)
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    active = fields.Boolean(default=True, track_visibility='onchange', string='Active', help="The active field allows you to hide the category without removing it.")
    detailed_info = fields.Text(string='Detailed Information')
    allergy_count = fields.Integer(compute="_count_allergy")
    
    @api.multi
    def _count_allergy(self):
        # Уг шалтгаан бүртгэгдсэн харшлуудын тоог олох
        for obj in self:
            self._cr.execute("""
                SELECT COUNT(allergy_id) AS count FROM allergy_to_reason WHERE reason_id IN (%s)
            """ % str(self.ids).strip('[]'))
            
            result = self._cr.dictfetchall()
            obj.allergy_count = result[0]['count'] if result else 0
            
    @api.multi
    def show_linked_allergies(self):
        # Уг шалтгаан бүртгэгдсэн харшлууд руу СМАРТ ТОВЧноос үсрэхэд ашиглах функц
        self._cr.execute("""
            SELECT allergy_id FROM allergy_to_reason WHERE reason_id IN (%s)
        """ % str(self.ids).strip('[]'))
        
        allergy_ids = [result['allergy_id'] for result in self._cr.dictfetchall()]

        return {
            'name': _('Allergy'),
            'res_model': 'allergy',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form,graph',
            'views': [
                (self.env.ref('l10n_mn_allergy.allergy_tree_view').id, 'tree'), 
                (self.env.ref('l10n_mn_allergy.allergy_graph_view').id, 'graph'),
                (False, 'form'),
            ],
            'target': 'current',
            'flags': {'search_view': True, 'action_buttons': True},
            'domain': [('id', 'in', allergy_ids)]
        }
        

class AlergySymptom(models.Model):
    _name = 'allergy.symptom'
    _inherit = ['mail.thread']

    name = fields.Char(string='Symptom', required=True)
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    active = fields.Boolean(default=True, track_visibility='onchange', string='Active', help="The active field allows you to hide the category without removing it.")
    detailed_info = fields.Text(string='Detailed Information')
    sub_symptoms = fields.Many2many('allergy.symptom', 'allergy_symptom_to_sub_symptoms', 'symptom_id', 'sub_symptoms_id', string='Sub Symptom')
    allergy_count = fields.Integer(compute="_count_allergy")

    @api.multi
    def _count_allergy(self):
        # Уг шинж тэмдэг бүртгэгдсэн харшлуудын тоог олох
        for obj in self:
            self._cr.execute("""
                SELECT COUNT(allergy_id) AS count FROM allergy_to_symptom 
                WHERE symptom_id IN (%s) OR 
                      symptom_id IN (SELECT symptom_id FROM allergy_symptom_to_sub_symptoms WHERE sub_symptoms_id IN (%s))
            """ % (str(self.ids).strip('[]'), str(self.ids).strip('[]')))
            result = self._cr.dictfetchall()
            
            obj.allergy_count = result[0]['count'] if result else 0
            
    @api.multi
    def show_linked_allergies(self):
        # Уг шинж тэмдэг бүртгэгдсэн харшлууд руу СМАРТ ТОВЧноос үсрэхэд ашиглах функц
        self._cr.execute("""
            SELECT allergy_id FROM allergy_to_symptom 
            WHERE symptom_id IN (%s) OR 
                  symptom_id IN (SELECT symptom_id FROM allergy_symptom_to_sub_symptoms WHERE sub_symptoms_id IN (%s))
        """ % (str(self.ids).strip('[]'), str(self.ids).strip('[]')))
        
        allergy_ids = [result['allergy_id'] for result in self._cr.dictfetchall()]
        
        return {
            'name': _('Allergy'),
            'res_model': 'allergy',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form,graph',
            'views': [
                (self.env.ref('l10n_mn_allergy.allergy_tree_view').id, 'tree'), 
                (self.env.ref('l10n_mn_allergy.allergy_graph_view').id, 'graph'),
                (False, 'form'),
            ],
            'target': 'current',
            'flags': {'search_view': True, 'action_buttons': True},
            'domain': [('id', 'in', allergy_ids)]
        }
        

class AllergyPrecaution(models.Model):
    _name = 'allergy.precaution'
    _inherit = ['mail.thread']

    name = fields.Char(string='Precaution', required=True)
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    active = fields.Boolean(default=True, track_visibility='onchange', string='Active', help="The active field allows you to hide the category without removing it.")
    detailed_info = fields.Text(string='Detailed Information')
    allergy_count = fields.Integer(compute="_count_allergy")

    @api.multi
    def _count_allergy(self):
        # Уг урьдчилан сэргийлэх арга бүртгэгдсэн харшлуудын тоог олох
        for obj in self:
            self._cr.execute("""
                SELECT COUNT(allergy_id) AS count FROM allergy_to_precaution WHERE precaution_id IN (%s)
            """ % str(self.ids).strip('[]'))
            
            result = self._cr.dictfetchall()
            obj.allergy_count = result[0]['count'] if result else 0
            
    @api.multi
    def show_linked_allergies(self):
        # Уг урьдчилан сэргийлэх арга бүртгэгдсэн харшлууд руу СМАРТ ТОВЧноос үсрэхэд ашиглах функц
        self._cr.execute("""
            SELECT allergy_id FROM allergy_to_precaution WHERE precaution_id IN (%s)
        """ % str(self.ids).strip('[]'))
        
        allergy_ids = [result['allergy_id'] for result in self._cr.dictfetchall()]

        return {
            'name': _('Allergy'),
            'res_model': 'allergy',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form,graph',
            'views': [
                (self.env.ref('l10n_mn_allergy.allergy_tree_view').id, 'tree'), 
                (self.env.ref('l10n_mn_allergy.allergy_graph_view').id, 'graph'),
                (False, 'form'),
            ],
            'target': 'current',
            'flags': {'search_view': True, 'action_buttons': True},
            'domain': [('id', 'in', allergy_ids)]
        }
        

class AllergyTag(models.Model):
    _name = 'allergy.tag'
    _inherit = ['mail.thread']

    name = fields.Char(string='Tag Name', required=True, translate=True)
    active = fields.Boolean(default=True, track_visibility='onchange', help="The active field allows you to hide the category without removing it.")
    color = fields.Integer(string='Color Index')
    description = fields.Text(string='Description', translate=True)
    allergy_count = fields.Integer(compute="_count_allergy")
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id) 

    @api.multi
    def _count_allergy(self):
        # Уг пайз бүртгэгдсэн харшлуудын тоог олох
        for obj in self:
            self._cr.execute("""
                SELECT COUNT(allergy_id) AS count FROM allergy_to_tags WHERE tag_id IN (%s)
            """ % str(self.ids).strip('[]'))
            
            result = self._cr.dictfetchall()
            obj.allergy_count = result[0]['count'] if result else 0
            
    @api.multi
    def show_linked_allergies(self):
        # Уг пайз бүртгэгдсэн харшлууд руу СМАРТ ТОВЧноос үсрэхэд ашиглах функц
        self._cr.execute("""
            SELECT allergy_id FROM allergy_to_tags WHERE tag_id IN (%s)
        """ % str(self.ids).strip('[]'))
        
        allergy_ids = [result['allergy_id'] for result in self._cr.dictfetchall()]

        return {
            'name': _('Allergy'),
            'res_model': 'allergy',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form,graph',
            'views': [
                (self.env.ref('l10n_mn_allergy.allergy_tree_view').id, 'tree'), 
                (self.env.ref('l10n_mn_allergy.allergy_graph_view').id, 'graph'),
                (False, 'form'),
            ],
            'target': 'current',
            'flags': {'search_view': True, 'action_buttons': True},
            'domain': [('id', 'in', allergy_ids)]
        }
        

class Allergy(models.Model):
    _name = 'allergy'
    _inherit = ['mail.thread']

    name = fields.Char('Name', required=True)
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    reason_ids = fields.Many2many('allergy.reason', 'allergy_to_reason', 'allergy_id', 'reason_id', string='Reason')
    symptom_ids = fields.Many2many('allergy.symptom', 'allergy_to_symptom', 'allergy_id', 'symptom_id', string='Symptom')
    precaution_ids = fields.Many2many('allergy.precaution', 'allergy_to_precaution', 'allergy_id', 'precaution_id', string='Precaution')
    detailed_info = fields.Text(string='Detailed Information')
    active = fields.Boolean(default=True, track_visibility='onchange', string='Active', help="The active field allows you to hide the category without removing it.")
    tag_ids = fields.Many2many('allergy.tag', 'allergy_to_tags', 'allergy_id', 'tag_id', string='Tags')
    

