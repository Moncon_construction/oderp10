# -*- coding: utf-8 -*-
{
    'name': "Mongolian Analytic",
    'version': '1.0',
    'depends': [
        'analytic',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Mongolian Analytic
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/account_analytic_account_view.xml',
        'views/analytic_category_view.xml',
        'views/res_company_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
