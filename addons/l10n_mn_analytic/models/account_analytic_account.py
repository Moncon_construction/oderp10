# -*- coding: utf-8 -*-
from odoo import api, fields, models


class AccountAnalyticAccount(models.Model):
    _inherit = "account.analytic.account"
    _description = 'Analytic Account'

    category_id = fields.Many2one('account.analytic.account.category', string="Category type", required=False)
    parent_id = fields.Many2one('account.analytic.account', 'Parent', domain=[('type', '=', 'view')])
    type = fields.Selection([('view', 'View'),
                             ('normal', 'Normal')], 'Type', default="normal", required=True)
    children_ids = fields.One2many('account.analytic.account', 'parent_id', 'Account Analytic Value')

    @api.multi
    def _compute_debit_credit_balance(self):
        # Дахин тодорхойлов: Уг функц нь тухайн шинжилгээний данс дахь кредит, дебт талбаруудын утга тооцоолоход ашиглагдана.
        # Дахин тодорхойлсон шалтгаан: Od10 дээр шинжилгээний дансад children_ids буюу хүүхэд данс гэсэн талбар нэмсэн ба
        # Хүүхэд данснуудых нь нийлбэр кредит, дебт дүнгүүдийг уг дансанд мөн оноож өгөхийн тулд уг функцыг core-с дахин тодорхойлов.

        AccountAnalyticLine = self.env['account.analytic.line']

        # get date filter from context
        domain = []
        if self._context.get('from_date', False):
            domain.append(('date', '>=', self._context['from_date']))
        if self._context.get('to_date', False):
            domain.append(('date', '<=', self._context['to_date']))

        # get all account.analytic.account object which can be credit
        credit_groups = AccountAnalyticLine.read_group(
            domain=domain + [('amount', '>=', 0.0)],
            fields=['account_id', 'amount'],
            groupby=['account_id']
        )

        # set dictionary by account.analytic.account object. It's value must be key account.analytic.account object's credit amount
        data_credit = {l['account_id'][0]: l['amount'] for l in credit_groups}

        # get all account.analytic.account object which can be debit
        debit_groups = AccountAnalyticLine.read_group(
            domain=domain + [('amount', '<', 0.0)],
            fields=['account_id', 'amount'],
            groupby=['account_id']
        )

        # set dictionary by account.analytic.account object. It's value must be key account.analytic.account object's debit amount
        data_debit = {l['account_id'][0]: l['amount'] for l in debit_groups}

        for account in self:

            # Дансны өөрийнх нь мөрүүд дэхь нийт кредит, дебт дүнгүүдийг авах
            debit = abs(data_debit.get(account.id, 0.0))
            credit = data_credit.get(account.id, 0.0)

            # Дансны хүүхэд данснуудых нь мөрүүд дэхь нийт кредит, дебт дүнгүүдийг авах
            for child_acc in account.children_ids:
                debit += abs(data_debit.get(child_acc.id, 0.0))
                credit += data_credit.get(child_acc.id, 0.0)

            # Нийт тооцоолсон кредит, дебт дүнгүүдийг тухайн дансад оноох
            account.debit = debit
            account.credit = credit
            account.balance = account.credit - account.debit
