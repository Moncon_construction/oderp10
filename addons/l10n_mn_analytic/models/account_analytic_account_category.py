# -*- coding: utf-8 -*-

from odoo import fields, models


class AccountAnalyticCategory(models.Model):
    _name = "account.analytic.account.category"

    name = fields.Char(string="Category name", index=True, required=True)
