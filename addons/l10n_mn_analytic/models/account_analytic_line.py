# -*- coding: utf-8 -*-
from odoo import api, fields, models


class AccountAnalyticLine(models.Model):
    _inherit = "account.analytic.line"

    account_id = fields.Many2one('account.analytic.account', 'Analytic Account', required=True, ondelete='restrict',
                                 index=True, domain="[('type','=','normal')]")
    cashflow_id = fields.Many2one('account.cashflow.type', string='Cashflow type')
    partner_id = fields.Many2one('res.partner', related='move_id.partner_id', string='Partner', store=True, readonly=True)
