# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class ResCompany(models.Model):
    _inherit = "res.company"

    cost_center = fields.Selection([('department', 'Department'),
                                    ('warehouse', 'Warehouse'),
                                    ('sales_team', 'Sales Team'),
                                    ('product_categ', 'Product Category'),
                                    ('brand', 'Product Brand'),
                                    ('project', 'Project'),
                                    ('technic', 'Technic'),
                                    ('contract', 'Contract'),
                                    ], string="Cost center", default='department')
    default_analytic_account_id = fields.Many2one('account.analytic.account', string="Default Analytic Account", domain=[('type', '=', 'normal')])
    sales_default_analytic_id = fields.Many2one('account.analytic.account', string="Sales Order Default Analytic Account", domain=[('type', '=', 'normal')])
