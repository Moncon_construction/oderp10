# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian module - Second Analytic Account Tree",
    'version': '1.0',
    'depends': [
        'l10n_mn_analytic',
        'l10n_mn_analytic_account'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules/Analytic',
    'description': """
       Second Analytic Account Tree Module
    """,
    'data': [
        'views/analytic_account_views.xml',
        'views/res_company_view.xml',
        'views/analytic_account_line_views.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
