# -*- coding: utf-8 -*-
from odoo import api, fields, models

class AccountAnalyticAccount(models.Model):
    _inherit = "account.analytic.account"
    
    tree_number = fields.Selection([('tree_1', 'Tree #1'), 
                                    ('tree_2', 'Tree #2')], 'Tree Number', default="tree_1", required=True)
    parent_id = fields.Many2one('account.analytic.account', 'Parent', domain="[('type', '=', 'view'),('tree_number','=',tree_number)]")