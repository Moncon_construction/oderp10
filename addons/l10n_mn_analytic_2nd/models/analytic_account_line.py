# -*- coding: utf-8 -*-
from odoo import api, fields, models


class AccountAnalyticAccount(models.Model):
    _inherit = "account.analytic.line"

    tree_number_line = fields.Selection([('tree_1', 'Tree #1'), ('tree_2', 'Tree #2')], 'Tree Number', related='account_id.tree_number', readonly=True, store=True)
