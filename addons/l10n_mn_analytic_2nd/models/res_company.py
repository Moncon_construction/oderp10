# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class ResCompany(models.Model):
    _inherit = "res.company"
    
    cost_center_2nd = fields.Selection([('department', 'Department'),
                                        ('warehouse', 'Warehouse'),
                                        ('sales_team', 'Sales Team'), 
                                        ('product_categ', 'Product Category'),
                                        ('brand', 'Product Brand'),
                                        ('project', 'Project'),
                                        ('technic', 'Technic'),
                                        ('contract', 'Contract'),
                                        ], string="Second Cost Center", default='department')