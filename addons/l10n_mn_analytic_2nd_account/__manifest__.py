# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian module - Account - Second Analytic Account",
    'version': '1.0',
    'depends': [
        'l10n_mn_analytic_2nd',
        'l10n_mn_account_analytic_report',
        'l10n_mn_analytic_account',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules/Analytic',
    'description': """
       Mongolian module - Account Second Analytic Account
    """,
    'data': [
        'views/account_mongolian_balance_sheet_views.xml',
        'views/account_profit_report_views.xml',
        'views/account_cashflow_report_views.xml',
        'views/account_equity_change_report_views.xml',
        'views/account_payment_view.xml',
        'views/account_invoice_view.xml',
        'views/account_move_line_view.xml',
        'views/account_move_view.xml',
        'views/account_transaction_balance_view.xml',
        'views/account_bank_statement_view.xml',
        'views/account_mongolian_balance_sheet_views.xml',
        'views/account_profit_report_views.xml',
        'views/account_cashflow_report_views.xml',
        'views/account_equity_change_report_views.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
