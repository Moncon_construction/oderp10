# -*- coding: utf-8 -*-
from math import copysign

from odoo import _, api, fields, models


class AccountAnalyticLine(models.Model):
    _inherit = 'account.analytic.line'

    def _get_analytic_amount_currency(self):
        if self.company_id.show_analytic_share:
            for line in self:
                for share in line.move_id.analytic_share_ids:
                    if line.account_id.id == share.analytic_account_id.id:
                        line.analytic_amount_currency = abs(line.move_id.amount_currency * share.rate / 100) * copysign(1, line.amount)
                for share in line.move_id.analytic_share_ids2:
                    if line.account_id.id == share.analytic_account_id.id:
                        line.analytic_amount_currency = abs(line.move_id.amount_currency * share.rate / 100) * copysign(1, line.amount)
        else:
            super(AccountAnalyticLine, self)._get_analytic_amount_currency()
