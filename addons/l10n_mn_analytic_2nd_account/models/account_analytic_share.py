# -*- coding: utf-8 -*-
from odoo import fields, models


class AccountAnalyticShare(models.Model):
    _inherit = 'account.analytic.share'

    move_line_id2 = fields.Many2one('account.move.line', 'Move Line', readonly=True)
    invoice_line_id2 = fields.Many2one('account.invoice.line', 'Invoice Line', readonly=True)
    bank_statement_line_id2 = fields.Many2one('account.bank.statement.line', 'Bank Statement Line', readonly=True)
