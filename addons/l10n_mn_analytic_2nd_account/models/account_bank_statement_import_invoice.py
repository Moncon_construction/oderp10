# -*- coding: utf-8 -*-
import base64
from datetime import datetime
from tempfile import NamedTemporaryFile

import xlrd

from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError


class account_statement_import_invoice(models.TransientModel):
    _inherit = "account.bank.statement.import.invoice"

    def prepare_bank_statement_line(self, move_line, amount, bank_account_id, statement, line):
        vals = []
        share_obj = self.env['account.analytic.share']
        res = super(account_statement_import_invoice, self).prepare_bank_statement_line(move_line, amount, bank_account_id, statement, line)
        if line.is_prepayment:
            for share in line.invoice_line_ids.analytic_share_ids2:
                a_ids = share_obj.create({'analytic_account_id': share.analytic_account_id.id, 'rate': share.rate})
                vals.append((4, a_ids.id))
            res.update({'analytic_share_ids2': vals})
        return res
