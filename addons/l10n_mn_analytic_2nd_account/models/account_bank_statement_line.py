# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError


class AccountBankStatementLine(models.Model):
    _inherit = "account.bank.statement.line"

    analytic_2nd_account_id = fields.Many2one('account.analytic.account', 'Analytic Account 2', domain=[('tree_number', '=', 'tree_2')])
    analytic_share_ids2 = fields.One2many('account.analytic.share', 'bank_statement_line_id2', 'Analytic Share 2', ondelete='restrict')

    @api.multi
    def _check_analytic_share(self):
        super(AccountBankStatementLine, self)._check_analytic_share()
        ''' Шинжилгээний тархалт зөв үүссэн эсэхийг шалгах функц '''
        self.ensure_one()
        if self.account_id.req_analytic_account:
            if not self.analytic_share_ids2:
                raise UserError(_("Account %s requires analytic account!") % self.account_id.name)
        if self.analytic_share_ids2:
            total_rate = 0.0
            for share in self.analytic_share_ids2:
                total_rate += share.rate
            if round(total_rate, 2) != 100:
                raise UserError(_('Bank statement does not have a valid analytic share!'))

    @api.multi
    def unlink(self):
        for line in self:
            for share in line.analytic_share_ids2:
                if share.invoice_line_id2 or share.move_line_id2:
                    share.bank_statement_line_id2 = False
                else:
                    share.unlink()
        return super(AccountBankStatementLine, self).unlink()
