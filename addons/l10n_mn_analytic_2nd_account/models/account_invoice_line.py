# -*- coding: utf-8 -*-
from datetime import datetime, timedelta

from pyfcm import FCMNotification

import odoo.addons.decimal_precision as dp
from odoo import _, api, exceptions, fields, models
from odoo.exceptions import UserError, ValidationError


class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'

    analytic_2nd_account_id = fields.Many2one('account.analytic.account', 'Analytic Account 2', domain=[('tree_number', '=', 'tree_2')])
    analytic_share_ids2 = fields.One2many('account.analytic.share', 'invoice_line_id2', 'Analytic Share 2')

    @api.multi
    def write(self, vals):
        if 'analytic_share_ids2' in vals:
            # one2many утга ирэх ба хэрэв [default number, False or id, id or {}] утгатай байвал
            if len(vals['analytic_share_ids2']) >= 2:
                rate = 0.0
                for share in self.analytic_share_ids2:
                    rate += share.rate
                for share in vals['analytic_share_ids2']:
                    # [default number, False or id, {[id, rate], ...}]
                    if len(share) > 2:
                        if share[2] and share[2]['rate']:
                            # [default number, id, {}] бол засах үйлдэл хийж байгаа тул хасна
                            if share[1]:
                                rate -= self.env['account.analytic.share'].browse(share[1]).rate
                            rate += share[2]['rate']
                if round(rate, 2) != 100:
                    raise ValidationError(_("The total distribution of analytical accounts #2nd should be divided into 100%."))
        return super(AccountInvoiceLine, self).write(vals)

    @api.multi
    def unlink(self):
        ''' Нэхэмжлэлийн мөрийг устгах үед шинжилгээний тархалтынх мөр дээр хуулгын болон бичилтийн мөр холбоотой байвал салгах э.тохиолдолд устгах '''
        for line in self:
            for share in line.analytic_share_ids2:
                if share.bank_statement_line_id or share.move_line_id:
                    share.invoice_line_id2 = False
                else:
                    share.unlink()
        return super(AccountInvoiceLine, self).unlink()

    @api.model
    def create(self, vals):
        if 'analytic_share_ids2' in vals:
            # one2many утга ирэх ба хэрэв [default number, False or id, id or {}] утгатай байвал
            if len(vals['analytic_share_ids2']) >= 2:
                rate = 0.0
                for share in vals['analytic_share_ids2']:
                    if share[2]['rate']:
                        rate += share[2]['rate']
                if round(rate, 2) != 100:
                    raise ValidationError(_("The total distribution of analytical accounts #2nd should be divided into 100%."))
        return super(AccountInvoiceLine, self).create(vals)
