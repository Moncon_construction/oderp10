# -*- encoding: utf-8 -*-
from odoo import api, fields, models, _


class AccountMongolianBalanceSheet(models.Model):
    _inherit = 'account.mongolian.balance.sheet'

    cost_center = fields.Selection([('tree_1', 'Tree 1'), ('tree_2', 'Tree 2')], default='tree_1', string='Cost center')
    temporary_cost_center = fields.Selection([('tree_1', 'Tree 1'), ('tree_2', 'Tree 2'), ('nothing', 'Nothing')], compute="compute_temporary_cost_center", string='Cost center for filtering analytic accounts')
    
    def get_analytic_account_ids(self):
        # @Override: Өртгийн төвөөс хамааруулан шүүлтүүр нэмэв.
        analytic_account_ids = super(AccountMongolianBalanceSheet, self).get_analytic_account_ids()
        if self.cost_center:
            analytic_account_ids = analytic_account_ids.filtered(lambda x: x.tree_number == self.cost_center)
        return analytic_account_ids
    
    @api.onchange('cost_center')
    def onchange_cost_center(self):
        if self.cost_center:
            self.analytic_account_ids = self.analytic_account_ids.filtered(lambda x: x.tree_number == self.cost_center)
        
    @api.depends('cost_center')
    def compute_temporary_cost_center(self):
        for obj in self:
            if obj.cost_center == 'tree_1':
                obj.temporary_cost_center = 'tree_2'
            elif obj.cost_center == 'tree_2':
                obj.temporary_cost_center = 'tree_1'
            else:
                obj.temporary_cost_center = 'nothing'
