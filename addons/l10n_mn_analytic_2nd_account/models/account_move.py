# -*- coding: utf-8 -*-


from datetime import datetime, timedelta

from pyfcm import FCMNotification

import odoo.addons.decimal_precision as dp
from odoo import _, api, exceptions, fields, models
from odoo.exceptions import UserError, ValidationError


class AccountMove(models.Model):
    _inherit = 'account.move'

    analytic_2nd_account_id = fields.Many2one('account.analytic.account', 'Second Analytic Account', domain=[('tree_number', '=', 'tree_2')])

    @api.multi
    def button_cancel(self):
        ''' Журналын мөрийг цуцлах үед үүссэн шинжилгээний бичилт дагаж устана.'''
        res = super(AccountMove, self).button_cancel()
        for move in self:
            for line in move.line_ids:
                move_line_ids = self.env['account.analytic.line'].search([('move_id', '=', line.id)])
                if move_line_ids:
                    move_line_ids.unlink()
        return True

    @api.multi
    def _post_validate(self):
        for move in self:
            if move.line_ids:
                if not all([x.company_id.id == move.company_id.id for x in move.line_ids]):
                    raise UserError(_("Cannot create moves for different companies."))
        self.assert_balanced()
        return self._check_lock_date()
