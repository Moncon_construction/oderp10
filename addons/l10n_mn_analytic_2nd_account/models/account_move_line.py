# -*- coding: utf-8 -*-


from datetime import datetime, timedelta

from pyfcm import FCMNotification

import odoo.addons.decimal_precision as dp
from odoo import _, api, exceptions, fields, models
from odoo.exceptions import UserError, ValidationError


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    analytic_2nd_account_id = fields.Many2one('account.analytic.account', 'Analytic Account 2', domain=[('tree_number', '=', 'tree_2')])
    analytic_share_ids2 = fields.One2many('account.analytic.share', 'move_line_id2', 'Analytic Share 2')

    @api.multi
    def create_analytic_lines(self):
        ''' Шинжилгээний тархалт сонгосон бол журналын бичилт үүсгэх функц '''
        res = super(AccountMoveLine, self).create_analytic_lines()
        analytic_line_obj = self.env['account.analytic.line']
        for obj_line in self:
            if obj_line.analytic_share_ids2:
                for share_line in obj_line.analytic_share_ids2:
                    val = (obj_line.credit or 0.0) - (obj_line.debit or 0.0)
                    amt = val * (share_line.rate / 100)
                    vals_line = obj_line._prepare_analytic_line()[0]
                    vals_line.update({
                        'date': obj_line.date,
                        'amount': amt,
                        'general_account_id': obj_line.account_id.id,
                        'account_id': share_line.analytic_account_id.id, })
                    line_id = analytic_line_obj.create(vals_line)
                    share_line.write({'analytic_line_id': line_id.id})
            elif obj_line.analytic_account_id:
                vals_line = obj_line._prepare_analytic_line()[0]
                a = analytic_line_obj.create(vals_line)
        return res

    @api.multi
    def _check_analytic_share(self):
        ''' Шинжилгээний тархалт зөв үүссэн эсэхийг шалгах функц '''
        super(AccountMoveLine, self)._check_analytic_share()
        if self.move_id.statement_line_id and self.account_id.req_analytic_account and not self.analytic_share_ids2:
            self.write({'analytic_share_ids2': [(0, 0, {'analytic_account_id': share.analytic_account_id.id, 'rate': share.rate}) for share in self.move_id.statement_line_id.analytic_share_ids2]})
        if self.account_id.req_analytic_account and self.company_id.show_analytic_share and not self.analytic_share_ids2:
            raise UserError(_("Account %s requires analytic account!") % self.account_id.name)
        if self.analytic_share_ids2:
            total_rate = 0.0
            for share in self.analytic_share_ids2:
                total_rate += share.rate
            if round(total_rate, 2) != 100:
                raise UserError(_('Account move does not have a valid analytic share!'))
