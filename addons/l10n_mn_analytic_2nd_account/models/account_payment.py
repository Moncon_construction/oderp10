# -*- coding: utf-8 -*-


from odoo import api, fields, models, _

class AccountPayment(models.Model):
    _inherit = 'account.payment'
    
    analytic_2nd_account_id = fields.Many2one('account.analytic.account', 'Second Analytic Account', domain=[('tree_number', '=', 'tree_2')])
    
    def get_bank_statement_from_values(self):
        res = super(AccountPayment, self).get_bank_statement_from_values()
        res.update({'analytic_2nd_account_id': self.analytic_2nd_account_id.id})
        return res

    def get_bank_statement_to_values(self):
        res = super(AccountPayment, self).get_bank_statement_to_values()
        res.update({'analytic_2nd_account_id': self.analytic_2nd_account_id.id})
        return res