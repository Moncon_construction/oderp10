# -*- encoding: utf-8 -*-
from odoo import api, fields, models


class AccountTransactionBalance(models.Model):
    _inherit = 'account.transaction.balance'

    tree_number = fields.Selection([('tree_1', 'Tree #1'),
                                    ('tree_2', 'Tree #2')], 'Tree Number', default="tree_1")
    
    @api.multi
    def compute(self):
        # Тооцоолол хийх функц
        self.line_ids = None
        self.currency_line_ids = None
        self.analytic_line_ids = None
        self.currency_analytic_line_ids = None
        account_ids = []
        analytic_account_ids = []

        account_obj = self.env['account.account']
        move_line_obj = self.env['account.move.line']
        line_obj = self.env['account.transaction.balance.line']
        currency_line_obj = self.env['account.transaction.balance.currency.line']
        analytic_line_obj = self.env['account.transaction.balance.analytic.line']
        currency_analytic_line_obj = self.env['account.transaction.balance.currency.analytic.line']

        if self.account_ids:
            for account_id in self.account_ids.ids:
                account_ids.append(account_id)
        elif self.account_type_ids:
            account = account_obj.search([('user_type_id', 'in', self.account_type_ids.ids)])
            if account:
                for account_id in account.ids:
                    account_ids.append(account_id)
            if not account_ids:
                raise UserError(_('There are no accounts in selected account types'))
        else:
            account_ids = account_obj.search([]).ids
        if self.show_analytic_account:
            if self.analytic_account_ids:
                for analytic_account_id in self.analytic_account_ids:
                    analytic_account_ids.append(analytic_account_id.id)
            else:
                """Тайланд сонгосон Шинжилгээний дансны модоор шинжилгээний дансыг шүүдэг болгов"""
                analytic_account_ids = self.env['account.analytic.account'].search([('tree_number','=',self.tree_number)]).ids
            # Шинжилгээний бичилтээс дансны дүнг дуудах
            balances = move_line_obj.with_context(without_profit_revenue=self.without_profit_revenue,
                                                  grouping=self.grouping).get_all_analytic_balance(self.company_id.id, account_ids, analytic_account_ids,
                                                                                                   self.date_from, self.date_to)
        else:
            # Журналын мөрөөс дансны дүнг дуудах
            balances = move_line_obj.with_context(without_profit_revenue=self.without_profit_revenue,
                                                  group_account=self.group_account).get_all_balance(self.company_id.id,  account_ids,  self.date_from,
                                                                                                    self.date_to, self.target_move)
        #if not self.show_analytic_account:
        for line in balances:
            if self.show_analytic_account:
                end_balance = line['start_balance'] - line['debit'] + line['credit']
            else:
                end_balance = line['start_balance'] + line['debit'] - line['credit']
            if (not self.show_analytic_account and self.group_account) or (self.show_analytic_account and self.grouping and self.grouping == 'account_type'):
                type = line['atid']
            else:
                type = False
            if self.currency_transaction and not self.show_analytic_account:
                cur_end_balance = line['cur_start_balance'] + line['cur_debit'] - line['cur_credit']
                currency_line_obj.create({'account_id': line['account_id'],
                                          'account_type_id': type,
                                          'code': line['acode'],
                                          'name': line['aname'],
                                          'currency_id': line['cid'] or self.company_id.currency_id and self.company_id.currency_id.id,
                                          'initial_debit': (line['start_balance'] > 0 and line['start_balance']) or 0.0,
                                          'initial_credit': (line['start_balance'] < 0 and -line['start_balance']) or 0.0,
                                          'initial_currency_debit': (line['cur_start_balance'] > 0 and line['cur_start_balance']) or 0.0,
                                          'initial_currency_credit': (line['cur_start_balance'] < 0 and -line['cur_start_balance']) or 0.0,
                                          'debit': line['debit'] or 0.0,
                                          'credit': line['credit'] or 0.0,
                                          'currency_debit': line['cur_debit'] or 0.0,
                                          'currency_credit': line['cur_credit'] or 0.0,
                                          'end_debit': (end_balance > 0 and end_balance) or 0.0,
                                          'end_credit': (end_balance < 0 and -end_balance) or 0.0,
                                          'end_currency_debit': (cur_end_balance > 0 and cur_end_balance) or 0.0,
                                          'end_currency_credit': (cur_end_balance < 0 and -cur_end_balance) or 0.0,
                                          'transaction_id': self.id
                                          })
            elif not self.currency_transaction and not self.show_analytic_account:
                line_obj.create({'account_id': line['account_id'],
                                 'account_type_id': type,
                                 'code': line['acode'],
                                 'name': line['aname'],
                                 'initial_debit': (line['start_balance'] > 0 and line['start_balance']) or 0.0,
                                 'initial_credit': (line['start_balance'] < 0 and -line['start_balance']) or 0.0,
                                 'debit': line['debit'] or 0.0,
                                 'credit': line['credit'] or 0.0,
                                 'end_debit': (end_balance > 0 and end_balance) or 0.0,
                                 'end_credit': (end_balance < 0 and -end_balance) or 0.0,
                                 'transaction_id': self.id
                                 })
            elif not self.currency_transaction and self.show_analytic_account:
                analytic_line_obj.create({'account_id': line['account_id'],
                                          'account_type_id': type,
                                          'code': line['acode'],
                                          'name': line['aname'],
                                          'analytic_account_id': line['aaid'],
                                          'analytic_account_code': line['aacode'],
                                          'analytic_account_name': line['aaname'],
                                          'initial_debit': (line['start_balance'] < 0 and -line['start_balance']) or 0.0,
                                          'initial_credit': (line['start_balance'] > 0 and line['start_balance']) or 0.0,
                                          'debit': line['debit'] or 0.0,
                                          'credit': line['credit'] or 0.0,
                                          'end_debit': (end_balance < 0 and -end_balance) or 0.0,
                                          'end_credit': (end_balance > 0 and end_balance) or 0.0,
                                          'transaction_id': self.id
                                          })
            elif self.currency_transaction and self.show_analytic_account:
                cur_end_balance = line['cur_start_balance'] - line['cur_debit'] + line['cur_credit']
                currency_analytic_line_obj.create({'account_id': line['account_id'],
                                                   'account_type_id': type,
                                                   'code': line['acode'],
                                                   'name': line['aname'],
                                                   'currency_id': line['cid'] or self.company_id.currency_id and self.company_id.currency_id.id,
                                                   'analytic_account_id': line['aaid'],
                                                   'analytic_account_code': line['aacode'],
                                                   'analytic_account_name': line['aaname'],
                                                   'initial_debit': (line['start_balance'] < 0 and -line['start_balance']) or 0.0,
                                                   'initial_credit': (line['start_balance'] > 0 and line['start_balance']) or 0.0,
                                                   'initial_currency_debit': (line['cur_start_balance'] < 0 and -line['cur_start_balance']) or 0.0,
                                                   'initial_currency_credit': (line['cur_start_balance'] > 0 and line['cur_start_balance']) or 0.0,
                                                   'debit': line['debit'] or 0.0,
                                                   'credit': line['credit'] or 0.0,
                                                   'currency_debit': line['cur_debit'] or 0.0,
                                                   'currency_credit': line['cur_credit'] or 0.0,
                                                   'end_debit': (end_balance < 0 and -end_balance) or 0.0,
                                                   'end_credit': (end_balance > 0 and end_balance) or 0.0,
                                                   'end_currency_debit': (cur_end_balance < 0 and -cur_end_balance) or 0.0,
                                                   'end_currency_credit': (cur_end_balance > 0 and cur_end_balance) or 0.0,
                                                   'transaction_id': self.id
                                                  })
        return True
