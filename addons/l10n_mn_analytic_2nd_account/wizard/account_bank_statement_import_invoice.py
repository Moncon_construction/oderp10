# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from odoo.exceptions import UserError


class AccountBankStatementImportInvoice(models.TransientModel):
    _inherit = 'account.bank.statement.import.invoice'

    def prepare_bank_statement_line(self, move_line, amount, bank_account_id, statement, invoice):
        result = super(AccountBankStatementImportInvoice, self).prepare_bank_statement_line(move_line, amount, bank_account_id, statement, invoice)
        if invoice.company_id.show_analytic_share:
            total = invoice.amount_total
            share_dict = {}
            for line in invoice.invoice_line_ids:
                for share in line.analytic_share_ids2:
                    if share.analytic_account_id.id not in share_dict:
                        share_dict[share.analytic_account_id.id] = line.price_unit * line.quantity / total * share.rate
                    else:
                        share_dict[share.analytic_account_id.id] += line.price_unit * line.quantity / total * share.rate
            vals = []
            total_rate = 0
            for k, v in share_dict.items():
                vals.append((0, 0, {'analytic_account_id': k, 'rate': v}))
                total_rate += v
            if round(total_rate, 2) != 100:
                raise UserError(_('Invoice does not have a valid analytic share!'))
            result.update({'analytic_share_ids2': vals})
        return result
