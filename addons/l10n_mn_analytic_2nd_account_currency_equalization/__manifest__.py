# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################
{
    'name': 'Mongolian Analytic Second Account Currency Equalization',
    'depends': [
        'l10n_mn_analytic_2nd',
        'l10n_mn_analytic_2nd_account',
        'l10n_mn_analytic_account_currency_equalization',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """Ханшийн тэгшитгэл дээр шинжилгээний данс #2 сонгодог болгоно""",
    'data': [
        'wizard/account_currency_equalization_view.xml',
    ],
    "auto_install": False,
    "installable": True,
}
