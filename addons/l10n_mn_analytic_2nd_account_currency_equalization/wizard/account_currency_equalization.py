# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from odoo.exceptions import UserError


class AccountCurrencyEqualization(models.TransientModel):
    _inherit = 'account.currency.equalization'

    analytic_2nd_account_id = fields.Many2one('account.analytic.account', 'Analytic account #2', required=False)
    analytic_2nd_share_ids = fields.One2many('account.currency.equalization.analytic.share', 'account_currency_equalization_id2', 'Analytic share #2')

    @api.multi
    def _get_equalization_move_line_vals(self, description, name, diff_amount, gain_loss_account, partner, cashflow_id, analytic_account_id, line):
        self.ensure_one()
        res = super(AccountCurrencyEqualization, self)._get_equalization_move_line_vals(description, name, diff_amount, gain_loss_account, partner, cashflow_id, analytic_account_id, line)
        for r in res:
            if self.env['account.account'].browse(r[2]['account_id']).req_analytic_account:
                r[2].update({'analytic_2nd_account_id': self.analytic_2nd_account_id and self.analytic_2nd_account_id.id,
                            'analytic_share_ids2': [(0, 0, {'analytic_account_id': s.analytic_account_id.id, 'rate': s.rate}) for s in self.analytic_2nd_share_ids]})
        return res

    @api.multi
    def action_equalize(self):
        if not self.show_analytic_share:
            if not self.analytic_2nd_account_id:
                raise UserError(_('Please select analytic account!'))
        elif self.show_analytic_share:
            if not self.analytic_2nd_share_ids:
                raise UserError(_('Please select analytic share!'))
            else:
                total = 0
                for s in self.analytic_2nd_share_ids:
                    total += s.rate
                if round(total, 2) != 100:
                    raise UserError(_('The sum rate of the analytic shares must be 100%!'))
        return super(AccountCurrencyEqualization, self).action_equalize()
