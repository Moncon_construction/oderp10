# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class AccountCurrencyEqualizationAnalyticShare(models.TransientModel):
    _inherit = 'account.currency.equalization.analytic.share'

    account_currency_equalization_id2 = fields.Many2one('account.currency.equalization', 'Currency equalization #2')
