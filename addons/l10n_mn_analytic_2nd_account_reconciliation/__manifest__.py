# -*- coding: utf-8 -*-
{
    'name': "Mongolian Analytic Second Account Reconciliation",
    'version': '1.0',
    'depends': [
        'l10n_mn_account_reconciliation',
        'l10n_mn_analytic_2nd_account',
        'l10n_mn_analytic_account_reconciliation',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Тулгалтын бичилт шинжилгээний данс #2 сонгох эсвэл шинжилгээний тархалт #2 авч үүснэ
    """,
    'data': [
        'wizard/account_move_line_reconcile_writeoff_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
