# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    # Авлага, Өглөгийн тулгалтын үед журналын мөр үүсгэх функц
    @api.multi
    def _create_move_line(self, date=False, journal_id=False, debit=0, credit=0):
        self.ensure_one()
        res = super(AccountMoveLine, self)._create_move_line(date=date, journal_id=journal_id, debit=debit, credit=credit)
        if self.account_id.req_analytic_account:
            # Тулгалтаас үүсэх шинэ журналын бичилтийн мөр дээр шинжилгээний данс өгнө
            res[2].update({'analytic_2nd_account_id': self.analytic_2nd_account_id.id if self.analytic_2nd_account_id else False})
            # Тулгалтаас үүсэх шинэ журналын бичилтийн мөр дээр шинжилгээний тархалт өгнө
            res[2].update({'analytic_share_ids2': [(0, 0, {'analytic_account_id': s.analytic_account_id.id, 'rate': s.rate}) for s in self.analytic_share_ids2]})
        return res

    # Салгах бичилтийн утгууд цуглуулах функц
    @api.multi
    def _get_partial_move_line_vals(self, par_debit, par_credit, balance_currency):
        self.ensure_one()
        res = super(AccountMoveLine, self)._get_partial_move_line_vals(par_debit, par_credit, balance_currency)
        if self.account_id.req_analytic_account:
            res.update({'analytic_2nd_account_id': self.analytic_2nd_account_id.id if self.analytic_2nd_account_id else False})
            res.update({'analytic_share_ids2': [(0, 0, {'analytic_account_id': s.analytic_account_id.id, 'rate': s.rate}) for s in self.analytic_share_ids2]})
        return res

    @api.multi
    def _prepare_writeoff_first_line_values(self, values):
        res = super(AccountMoveLine, self)._prepare_writeoff_first_line_values(values)
        if self[0].account_id.req_analytic_account:
            if self.company_id.show_analytic_share:
                res.update({
                    'analytic_share_ids2': [(0, 0, {'analytic_account_id': s.analytic_account_id.id, 'rate': s.rate}) for s in self[0].analytic_share_ids2]
                })
            else:
                res.update({
                    'analytic_2nd_account_id': self[0].analytic_2nd_account_id.id
                })
        return res
