# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class AccountMoveLineReconcileWriteoff(models.TransientModel):
    _inherit = 'account.move.line.reconcile.writeoff'

    analytic_2nd_share_ids = fields.One2many('account.move.line.reconcile.writeoff.share', 'write_off_id2', 'Analytic share #2')

    @api.model
    def default_get(self, fields):
        res = super(AccountMoveLineReconcileWriteoff, self).default_get(fields)
        context = dict(self._context or {})
        AccountMoveLine = self.env['account.move.line']
        amls = AccountMoveLine.browse(context.get('line_ids', False))
        debits = amls.filtered(lambda l: l.debit > 0)
        credits = amls.filtered(lambda l: l.credit > 0)
        debit_sum = sum(d.debit for d in debits)
        credit_sum = sum(d.credit for d in credits)
        vals = []
        if debit_sum > credit_sum:
            if len(debits) == 1 and debits.analytic_share_ids2:
                for share in debits.analytic_share_ids2:
                    vals.append((0, 0, {'analytic_account_id': share.analytic_account_id.id, 'rate': share.rate}))
        else:
            if len(credits) == 1 and credits.analytic_share_ids2:
                for share in credits.analytic_share_ids2:
                    vals.append((0, 0, {'analytic_account_id': share.analytic_account_id.id, 'rate': share.rate}))
        if vals:
            res.update({'analytic_2nd_share_ids': vals})
        return res
