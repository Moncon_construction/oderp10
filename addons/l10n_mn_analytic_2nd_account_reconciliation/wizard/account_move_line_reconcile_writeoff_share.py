# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class AccountMoveLineReconcileWriteoffShare(models.TransientModel):
    _inherit = 'account.move.line.reconcile.writeoff.share'

    write_off_id2 = fields.Many2one('account.move.line.reconcile.writeoff', 'Write off')
