# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian module - Second Analytic Account Share Tree",
    'version': '1.0',
    'depends': [
        'l10n_mn_analytic_2nd_account',
        'l10n_mn_analytic_account_share'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules/Analytic',
    'description': """
       Second Analytic Account Tree Module
    """,
    'data': [
        'views/account_invoice_view.xml',
        'views/account_move_line_view.xml',
        'views/account_bank_statement_view.xml',
        'views/account_bank_statement_line_view.xml',
        'views/account_invoice_line_view.xml',
        'views/account_move_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
