# -*- coding: utf-8 -*-
{
    'name': "Mongolian module - Account - Second Analytic Account",
    'version': '1.0',
    'depends': [
        'l10n_mn_analytic_2nd_account',
        'l10n_mn_account_asset',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules/Analytic',
    'description': """
       Mongolian module - Asset Second Analytic Account
    """,
    'data': [
        'views/account_asset_asset_view.xml',
        'views/account_asset_category_view.xml',
        'wizard/account_asset_asset_sell_view.xml',
        'wizard/account_asset_close_wizard_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
