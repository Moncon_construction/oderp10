# -*- coding: utf-8 -*-
from datetime import datetime

from odoo import _, api, fields, models


class AccountAssetAsset(models.Model):
    _inherit = 'account.asset.asset'

    @api.onchange('category_id')
    def _onchange_category_id(self):
        self.analytic_2nd_account_id = self.category_id.analytic_2nd_account_id

    analytic_2nd_account_id = fields.Many2one('account.analytic.account', 'Second Analytic Account', domain="[('tree_number', '=', 'tree_2'), ('company_id', '=', company_id)]")
    account_analytic_id = fields.Many2one(domain="[('tree_number', '=', 'tree_1'), ('company_id', '=', company_id), ('type','=','normal')]")

    # Хөрөнгө актлахад үүсэх журналын бичилтийн утга буцаах функц
    @api.multi
    def _get_move_vals(self, name, asset, account_id, close_date):
        line_vals = []
        category_id = asset.category_id

        analytic_account_id = False
        if asset.account_analytic_id:
            analytic_account_id = asset.account_analytic_id.id
        elif asset.category_id.account_analytic_id:
            analytic_account_id = asset.category_id.account_analytic_id.id
        analytic_2nd_account_id = False
        if asset.analytic_2nd_account_id:
            analytic_2nd_account_id = asset.analytic_2nd_account_id.id
        elif asset.category_id.analytic_2nd_account_id:
            analytic_2nd_account_id = asset.category_id.analytic_2nd_account_id.id

        analytic_share_ids = []
        if analytic_account_id:
            analytic_share_ids.append((0, 0, {'analytic_account_id': analytic_account_id, 'rate': 100}))
        analytic_share_ids2 = []
        if analytic_2nd_account_id:
            analytic_share_ids2.append((0, 0, {'analytic_account_id': analytic_2nd_account_id, 'rate': 100}))

        line_vals.append((0, 0, {
            'name': name,
            'ref': asset.name,
            'account_id': account_id,
            'debit': asset.value if asset.value_depreciated == 0 else (asset.value_residual > 0 and asset.value_residual) or 0,
            'credit': (asset.value_residual < 0 and -asset.value_residual) or 0,
            'journal_id': asset.journal_id.id,
            'currency_id': asset.currency_id.id,
            'date': close_date,
            'asset_id': asset.id,
        }))
        line_vals.append((0, 0, {
            'name': name,
            'ref': asset.name,
            'account_id': asset.category_id.account_asset_id.id,
            'debit': (asset.value < 0 and -asset.value) or 0,
            'credit': (asset.value > 0 and asset.value) or 0,
            'journal_id': asset.journal_id.id,
            'currency_id': asset.currency_id.id,
            'date': close_date,
            'asset_id': asset.id,
        }))
        if asset.value_depreciated != 0:
            line_vals.append([0, 0, {
                'name': name,
                'ref': asset.name,
                'account_id': asset.category_id.account_depreciation_id.id,
                'debit': (asset.value_depreciated > 0 and asset.value_depreciated) or 0,
                'credit': (asset.value_depreciated < 0 and -asset.value_depreciated) or 0,
                'journal_id': asset.journal_id.id,
                'currency_id': asset.currency_id.id,
                'date': close_date,
                'asset_id': asset.id,
            }])

        for l in line_vals:
            if self.env['account.account'].browse(l[2]['account_id']).req_analytic_account:
                if asset.company_id.show_analytic_share:
                    l[2].update({'analytic_share_ids': analytic_share_ids, 'analytic_share_ids2': analytic_share_ids2})
                else:
                    l[2].update({'analytic_account_id': analytic_account_id, 'analytic_2nd_account_id': analytic_2nd_account_id})

        move_vals = {
            'name': asset.journal_id.code + '/' + str(datetime.strptime(close_date, '%Y-%m-%d').year) + '/' + asset.name + '/' + name,
            'date': close_date,
            'ref': asset.name,
            'journal_id': asset.journal_id.id,
            'line_ids': line_vals
        }
        return move_vals

    # Хөрөнгийн элэгдлийн журналын бичилт үүсгэх утга буцаах функц
    @api.multi
    def _get_initial_depreciation_move_vals(self, asset, depreciation_amount, current_currency, entry_date, context, year):
        category_id = asset.category_id

        analytic_account_id = False
        if asset.account_analytic_id:
            analytic_account_id = asset.account_analytic_id.id
        elif asset.category_id.account_analytic_id:
            analytic_account_id = asset.category_id.account_analytic_id.id
        analytic_2nd_account_id = False
        if asset.analytic_2nd_account_id:
            analytic_2nd_account_id = asset.analytic_2nd_account_id.id
        elif asset.category_id.analytic_2nd_account_id:
            analytic_2nd_account_id = asset.category_id.analytic_2nd_account_id.id

        analytic_share_ids = []
        if analytic_account_id:
            analytic_share_ids.append((0, 0, {'analytic_account_id': analytic_account_id, 'rate': 100}))
        analytic_share_ids2 = []
        if analytic_2nd_account_id:
            analytic_share_ids2.append((0, 0, {'analytic_account_id': analytic_2nd_account_id, 'rate': 100}))

        line_ids = [(0, 0, {
            'name': asset.category_id.name,
            'ref': asset.name,
            'account_id': asset.account_depreciation_id.id,
            'debit': 0.0,
            'credit': depreciation_amount,
            'journal_id': asset.journal_id.id,
            'partner_id': asset.partner_id.id,
            'currency_id': False if asset.company_id.currency_id.id != current_currency else current_currency,
            'amount_currency': asset.company_id.currency_id.id != current_currency and -asset.initial_depreciation or 0.0,
            'date': entry_date,
            'asset_id': asset.id,
        }), (0, 0, {
            'name': asset.category_id.name,
            'ref': asset.name,
            'account_id': context['src_account_id'],
            'debit': depreciation_amount,
            'credit': 0.0,
            'journal_id': asset.journal_id.id,
            'partner_id': asset.partner_id.id,
            'currency_id': False if asset.company_id.currency_id.id != current_currency else current_currency,
            'amount_currency': asset.company_id.currency_id.id != current_currency and asset.initial_depreciation or 0.0,
            'date': entry_date,
            'asset_id': asset.id,
        })]

        for l in line_ids:
            if self.env['account.account'].browse(l[2]['account_id']).req_analytic_account:
                if asset.company_id.show_analytic_share:
                    l[2].update({'analytic_share_ids': analytic_share_ids, 'analytic_share_ids2': analytic_share_ids2})
                else:
                    l[2].update({'analytic_account_id': analytic_account_id, 'analytic_2nd_account_id': analytic_2nd_account_id})

        move_vals = {
            'name': asset.journal_id.code + '/' + str(year) + '/' + asset.name,
            'date': entry_date,
            'ref': asset.name,
            'journal_id': asset.journal_id.id,
            'line_ids': line_ids
        }
        return move_vals

    # Хөрөнгө батлахад хөрөнгө орлогодож байгаа журналын бичилт үүсгэх утга буцаах функц
    @api.multi
    def _get_validate_move_vals(self, asset, asset_amount, current_currency, entry_date, context, year):
        category_id = asset.category_id

        analytic_account_id = False
        if asset.account_analytic_id:
            analytic_account_id = asset.account_analytic_id.id
        elif asset.category_id.account_analytic_id:
            analytic_account_id = asset.category_id.account_analytic_id.id
        analytic_2nd_account_id = False
        if asset.analytic_2nd_account_id:
            analytic_2nd_account_id = asset.analytic_2nd_account_id.id
        elif asset.category_id.analytic_2nd_account_id:
            analytic_2nd_account_id = asset.category_id.analytic_2nd_account_id.id

        analytic_share_ids = []
        if analytic_account_id:
            analytic_share_ids.append((0, 0, {'analytic_account_id': analytic_account_id, 'rate': 100}))
        analytic_share_ids2 = []
        if analytic_2nd_account_id:
            analytic_share_ids2.append((0, 0, {'analytic_account_id': analytic_2nd_account_id, 'rate': 100}))

        line_ids = [(0, 0, {
            'name': asset.category_id.name,
            'ref': asset.name,
            'account_id': asset.account_asset_id.id,
            'debit': asset_amount,
            'credit': 0.0,
            'journal_id': asset.journal_id.id,
            'partner_id': asset.partner_id.id,
            'currency_id': current_currency,
            'amount_currency': asset.company_id.currency_id.id != current_currency and asset.value or 0.0,
            'date': entry_date,
            'asset_id': asset.id,
        }), (0, 0, {
            'name': asset.category_id.name,
            'ref': asset.name,
            'account_id': context['src_account_id'],
            'debit': 0.0,
            'credit': asset_amount,
            'journal_id': asset.journal_id.id,
            'partner_id': asset.partner_id.id,
            'currency_id': current_currency,
            'amount_currency': asset.company_id.currency_id.id != current_currency and -asset.value or 0.0,
            'date': entry_date,
            'asset_id': asset.id,
        })]

        for l in line_ids:
            if self.env['account.account'].browse(l[2]['account_id']).req_analytic_account:
                if asset.company_id.show_analytic_share:
                    l[2].update({'analytic_share_ids': analytic_share_ids, 'analytic_share_ids2': analytic_share_ids2})
                else:
                    l[2].update({'analytic_account_id': analytic_account_id, 'analytic_2nd_account_id': analytic_2nd_account_id})

        move_vals = {
            'name': asset.journal_id.code + '/' + str(year) + '/' + asset.name,
            'date': entry_date,
            'ref': asset.code,
            'journal_id': asset.journal_id.id,
            'line_ids': line_ids
        }
        return move_vals
