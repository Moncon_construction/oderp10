from odoo import _, api, fields, models


class AccountAssetCategory(models.Model):
    _inherit = 'account.asset.category'

    analytic_2nd_account_id = fields.Many2one('account.analytic.account', 'Second Analytic Account', domain="[('tree_number', '=', 'tree_2'), ('company_id', '=', company_id)]")
    account_analytic_id = fields.Many2one(domain="[('tree_number', '=', 'tree_1'), ('company_id', '=', company_id)]")
