# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from odoo.tools import float_compare, float_is_zero


class AccountAssetDepreciationLine(models.Model):
    _inherit = 'account.asset.depreciation.line'

    # Хөрөнгийн элэгдэл батлахад журналын бичилт үүсгэх утга буцаах функц
    @api.multi
    def _get_depr_move_vals(self, line, depreciation_date, prec):
        category_id = line.asset_id.category_id
        asset_name = line.asset_id.name + ' (%s/%s)' % (line.sequence, len(line.asset_id.depreciation_line_ids))
        company_currency = line.asset_id.company_id.currency_id
        current_currency = line.asset_id.currency_id
        amount = current_currency.with_context(date=depreciation_date).compute(line.amount, company_currency)
        analytic_share_ids = []
        if category_id.account_analytic_id:
            analytic_share_ids.append((0, 0, {
                'analytic_account_id': category_id.account_analytic_id.id,
                'rate': 100}))
        analytic_share_ids2 = []
        if category_id.analytic_2nd_account_id:
            analytic_share_ids2.append((0, 0, {
                'analytic_account_id': category_id.analytic_2nd_account_id.id,
                'rate': 100}))

        move_line_1 = {
            'name': asset_name,
            'ref': line.name,
            'account_id': category_id.account_depreciation_id.id,
            'debit': 0.0 if float_compare(amount, 0.0, precision_digits=prec) > 0 else -amount,
            'credit': amount if float_compare(amount, 0.0, precision_digits=prec) > 0 else 0.0,
            'journal_id': category_id.journal_id.id,
            'partner_id': line.asset_id.partner_id.id,
            'currency_id': company_currency != current_currency and current_currency.id or False,
            'amount_currency': company_currency != current_currency and -1.0 * line.amount or 0.0,
            'date': depreciation_date,
            'asset_id': line.asset_id.id,
            'analytic_share_ids': analytic_share_ids,
            'analytic_share_ids2': analytic_share_ids2
        }
        move_line_2 = {
            'name': asset_name,
            'ref': line.name,
            'account_id': category_id.account_depreciation_expense_id.id,
            'credit': 0.0 if float_compare(amount, 0.0, precision_digits=prec) > 0 else -amount,
            'debit': amount if float_compare(amount, 0.0, precision_digits=prec) > 0 else 0.0,
            'journal_id': category_id.journal_id.id,
            'partner_id': line.asset_id.partner_id.id,
            'currency_id': company_currency != current_currency and current_currency.id or False,
            'amount_currency': company_currency != current_currency and line.amount or 0.0,
            'date': depreciation_date,
            'asset_id': line.asset_id.id,
            'analytic_share_ids': analytic_share_ids,
            'analytic_share_ids2': analytic_share_ids2
        }
        return {
            'name': asset_name,
            'ref': line.asset_id.code,
            'date': depreciation_date or False,
            'journal_id': category_id.journal_id.id,
            'line_ids': [(0, 0, move_line_1), (0, 0, move_line_2)],
        }
