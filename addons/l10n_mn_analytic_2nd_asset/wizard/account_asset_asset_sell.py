# -*- coding: utf-8 -*-
from datetime import datetime

from odoo import _, api, fields, models


class AccountAssetAssetSell(models.TransientModel):
    _inherit = 'account.asset.asset.sell'

    # Хөрөнгө борлуулахад үүсэх журналын бичилтийн утга буцаах функц
    @api.multi
    def _get_account_move_vals(self, line, value_residual):
        invoice_line_obj = self.env['account.invoice.line']
        asset = line.asset_id
        category_id = asset.category_id

        analytic_account_id = False
        if asset.account_analytic_id:
            analytic_account_id = asset.account_analytic_id.id
        elif category_id.account_analytic_id:
            analytic_account_id = category_id.account_analytic_id.id
        analytic_2nd_account_id = False
        if asset.analytic_2nd_account_id:
            analytic_2nd_account_id = asset.analytic_2nd_account_id.id
        elif category_id.analytic_2nd_account_id:
            analytic_2nd_account_id = category_id.analytic_2nd_account_id.id

        analytic_share_ids = []
        if analytic_account_id:
            analytic_share_ids.append((0, 0, {
                'analytic_account_id': analytic_account_id,
                'rate': 100}))
        analytic_share_ids2 = []
        if analytic_2nd_account_id:
            analytic_share_ids2.append((0, 0, {
                'analytic_account_id': analytic_2nd_account_id,
                'rate': 100}))

        m_line_vals = [(0, 0, {
            'name': self.description,
            'ref': line.asset_id.name,
            'account_id': asset.account_asset_id.id,
            'debit': (line.value < 0 and -line.value) or 0,
            'credit': (line.value > 0 and line.value) or 0,
            'journal_id': self.journal_id.id,
            'currency_id': asset.currency_id.id,
            'date': self.date,
            'asset_id': asset.id,
        }), (0, 0, {
            'name': self.description,
            'ref': asset.name,
            'account_id': asset.category_id.account_depreciation_id.id,
            'debit': (line.value - line.value_residual > 0 and line.value - line.value_residual) or 0,
            'credit': (line.value - line.value_residual < 0 and -line.value - line.value_residual) or 0,
            'journal_id': self.journal_id.id,
            'currency_id': asset.currency_id.id,
            'date': self.date,
            'asset_id': asset.id,
        })]
        inv_line = invoice_line_obj.search([('asset_id', '=', asset.id)])
        if inv_line:
            inv_line = inv_line[0]
        m_line_vals.append((0, 0, {
            'name': self.description,
            'ref': asset.name,
            'account_id': self.journal_id.default_credit_account_id.id,
            'debit': (inv_line.price_subtotal > 0 and inv_line.price_subtotal) or 0,
            'credit': (inv_line.price_subtotal < 0 and -inv_line.price_subtotal) or 0,
            'journal_id': self.journal_id.id,
            'currency_id': asset.currency_id.id,
            'date': self.date,
            'asset_id': asset.id,
        }))
        if line.tax_id:
            debit = credit = 0.0
            account_id = asset.company_id.expense_account_asset
            if line.sell_value == line.asset_id.value_residual:
                debit = line.sell_value - inv_line.price_subtotal
            elif line.sell_value > line.asset_id.value_residual:
                credit = inv_line.price_subtotal - line.asset_id.value_residual
                account_id = asset.company_id.income_account_asset
                if credit < 0:
                    account_id = asset.company_id.expense_account_asset
                    debit = - credit
                    credit = 0
            else:
                debit = line.asset_id.value_residual - inv_line.price_subtotal
            m_line_vals.append((0, 0, {
                'name': self.description,
                'ref': asset.name,
                'account_id': account_id.id,
                'debit': debit,
                'credit': credit,
                'journal_id': self.journal_id.id,
                'currency_id': asset.currency_id.id,
                'date': self.date,
                'asset_id': asset.id,
            }))
        else:
            if value_residual != asset.value_residual:
                debit = credit = 0
                if asset.value_residual - line.sell_value > 0:
                    debit = asset.value_residual - line.sell_value
                else:
                    credit = line.sell_value - asset.value_residual
                req_analytic_account = False
                if value_residual > asset.value_residual:
                    if asset.company_id.income_account_asset.req_analytic_account:
                        req_analytic_account = True
                else:
                    if asset.company_id.expense_account_asset.req_analytic_account:
                        req_analytic_account = True
                m_line_vals.append((0, 0, {
                    'name': self.description,
                    'ref': asset.name,
                    'account_id': asset.company_id.income_account_asset.id if value_residual > asset.value_residual else asset.company_id.expense_account_asset.id,
                    'debit': debit,
                    'credit': credit,
                    'journal_id': self.journal_id.id,
                    'currency_id': asset.currency_id.id,
                    'date': self.date,
                    'asset_id': asset.id,
                }))
        for l in m_line_vals:
            if self.env['account.account'].browse(l[2]['account_id']).req_analytic_account:
                if asset.company_id.show_analytic_share:
                    l[2].update({'analytic_share_ids': analytic_share_ids, 'analytic_share_ids2': analytic_share_ids2})
                else:
                    l[2].update({'analytic_account_id': analytic_account_id, 'analytic_2nd_account_id': analytic_2nd_account_id})
        move_vals = {
            'name': asset.journal_id.code + '/' + str(datetime.strptime(self.date, '%Y-%m-%d').year) + '/' + self.description,
            'date': self.date,
            'ref': self.description,
            'journal_id': asset.journal_id.id,
            'line_ids': m_line_vals
        }
        return move_vals
