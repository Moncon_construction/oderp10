# -*- coding: utf-8 -*-
from datetime import datetime

from odoo import _, api, fields, models


class AccountAssetAssetSellLine(models.TransientModel):
    _inherit = 'account.asset.asset.sell.line'

    @api.multi
    def _get_invoice_line_vals(self, asset, sell):
        self.ensure_one()
        res = super(AccountAssetAssetSellLine, self)._get_invoice_line_vals(asset, sell)

        analytic_account_id = False
        if asset.account_analytic_id:
            analytic_account_id = asset.account_analytic_id.id
        elif asset.category_id.account_analytic_id:
            analytic_account_id = asset.category_id.account_analytic_id.id
        analytic_2nd_account_id = False
        if asset.analytic_2nd_account_id:
            analytic_2nd_account_id = asset.analytic_2nd_account_id.id
        elif asset.category_id.analytic_2nd_account_id:
            analytic_2nd_account_id = asset.category_id.analytic_2nd_account_id.id

        if sell.journal_id.default_credit_account_id.req_analytic_account or sell.account_id.req_analytic_account:
            if asset.company_id.show_analytic_share:
                if analytic_account_id:
                    res[2].update({'analytic_share_ids': [(0, 0, {'analytic_account_id': analytic_account_id, 'rate': 100})]})
                if analytic_2nd_account_id:
                    res[2].update({'analytic_share_ids2': [(0, 0, {'analytic_account_id': analytic_2nd_account_id, 'rate': 100})]})
            else:
                if analytic_account_id:
                    res[2].update({'account_analytic_id': analytic_account_id})
                if analytic_2nd_account_id:
                    res[2].update({'analytic_2nd_account_id': analytic_2nd_account_id})
        return res
