# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class AssetModify(models.TransientModel):
    _inherit = 'asset.modify'

    @api.multi
    def _get_capital_move_line_vals(self, asset):
        self.ensure_one()
        res = super(AssetModify, self)._get_capital_move_line_vals(asset)
        capital_account = self.capital_invoice_line_id.account_id if self.capital_type == 'invoice_line' else self.account_id
        analytic_account_id = False
        if asset.account_analytic_id:
            analytic_account_id = asset.account_analytic_id.id
        elif asset.category_id.account_analytic_id:
            analytic_account_id = asset.category_id.account_analytic_id.id
        analytic_2nd_account_id = False
        if asset.analytic_2nd_account_id:
            analytic_2nd_account_id = asset.analytic_2nd_account_id.id
        elif asset.category_id.analytic_2nd_account_id:
            analytic_2nd_account_id = asset.category_id.analytic_2nd_account_id.id
        if capital_account.req_analytic_account:
            if asset.company_id.show_analytic_share:
                for r in res:
                    if r[2]['account_id'] == capital_account.id:
                        if analytic_account_id:
                            r[2].update({'analytic_share_ids': [(0, 0, {'analytic_account_id': analytic_account_id, 'rate': 100})]})
                        if analytic_2nd_account_id:
                            r[2].update({'analytic_share_ids2': [(0, 0, {'analytic_account_id': analytic_2nd_account_id, 'rate': 100})]})
            else:
                for r in res:
                    if r[2]['account_id'] == capital_account.id:
                        if analytic_account_id:
                            r[2].update({'analytic_account_id': analytic_account_id})
                        if analytic_2nd_account_id:
                            r[2].update({'analytic_2nd_account_id': analytic_2nd_account_id})
        asset_account = asset.category_id.account_asset_id
        if asset_account.req_analytic_account:
            if asset.company_id.show_analytic_share:
                for r in res:
                    if r[2]['account_id'] == asset_account.id:
                        if analytic_account_id:
                            r[2].update({'analytic_share_ids': [(0, 0, {'analytic_account_id': analytic_account_id, 'rate': 100})]})
                        if analytic_2nd_account_id:
                            r[2].update({'analytic_share_ids2': [(0, 0, {'analytic_account_id': analytic_2nd_account_id, 'rate': 100})]})
            else:
                for r in res:
                    if r[2]['account_id'] == asset_account.id:
                        if analytic_account_id:
                            r[2].update({'analytic_account_id': analytic_account_id})
                        if analytic_2nd_account_id:
                            r[2].update({'analytic_2nd_account_id': analytic_2nd_account_id})
        return res
