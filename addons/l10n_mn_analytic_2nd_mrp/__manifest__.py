# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Analytic Second MRP",
    'version': '1.0',
    'depends': [
        'l10n_mn_analytic_balance_mrp',
        'l10n_mn_analytic_2nd_stock',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules/Analytic',
    'description': """
       Mongolian module - Analytic Account Second Balance MRP
    """,
    'data': [
        'views/mrp_production_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
