# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from odoo.exceptions import UserError


class MrpProduction(models.Model):
    _inherit = 'mrp.production'

    analytic_2nd_account_id = fields.Many2one('account.analytic.account', 'Analytic Account #2', domain=[('tree_number', '=', 'tree_2')], required=False)
    cost_center_2nd = fields.Selection(related='company_id.cost_center_2nd')

    def _generate_finished_moves(self):
        move = super(MrpProduction, self)._generate_finished_moves()
        if self.analytic_2nd_account_id:
            move.analytic_2nd_account_id = self.analytic_2nd_account_id.id
            move.analytic_2nd_share_ids = [(0, 0, {'analytic_account_id': self.analytic_2nd_account_id.id, 'rate': 100})]
        return move

    def _generate_raw_move(self, bom_line, line_data):
        move = super(MrpProduction, self)._generate_raw_move(bom_line, line_data)
        if self.analytic_2nd_account_id:
            move.analytic_2nd_account_id = self.analytic_2nd_account_id.id
            move.analytic_2nd_share_ids = [(0, 0, {'analytic_account_id': self.analytic_2nd_account_id.id, 'rate': 100})]
        return move

    @api.model
    def create(self, values):
        if 'analytic_2nd_account_id' not in values or not values['analytic_2nd_account_id']:
            company = self.env['res.company'].browse(values['company_id'])
            if company.cost_center_2nd == 'warehouse':
                location_dest = self.env['stock.location'].browse(values['location_dest_id'])
                if location_dest.get_warehouse().analytic_account_id:
                    values['analytic_2nd_account_id'] = location_dest.get_warehouse().analytic_account_id.id
                else:
                    raise UserError(_('Please choose analytic account for the wareouse %s!') % location_dest.get_warehouse().name)
            elif company.cost_center_2nd == 'brand':
                product = self.env['product.product'].browse(values['product_id'])
                if product.brand_name:
                    if product.brand_name.analytic_account_id:
                        values['analytic_2nd_account_id'] = product.brand_name.analytic_account_id.id
                    else:
                        raise UserError(_('Please choose analytic account for the brand %s!') % product.brand_name.brand_name)
                else:
                    raise UserError(_('Please choose brand for the product %s!') % product.name)
            elif company.cost_center_2nd == 'product_categ':
                product = self.env['product.product'].browse(values['product_id'])
                if product.categ_id.analytic_account_id:
                    values['analytic_2nd_account_id'] = product.categ_id.analytic_account_id.id
                else:
                    raise UserError(_('Please choose analytic account for the product category %s!') % product.categ_id.name)
            elif company.cost_center_2nd in ('department', 'sales_team', 'project', 'technic', 'contract'):
                if company.default_analytic_account_id:
                    values['analytic_2nd_account_id'] = company.default_analytic_account_id.id
                else:
                    raise UserError(_('Please choose default analytic account for the company %s!') % company.name)
        production = super(MrpProduction, self).create(values)
        return production

    @api.onchange('analytic_2nd_account_id')
    def _onchange_analytic_2nd_account_id(self):
        if self.analytic_2nd_account_id:
            for move in self.move_raw_ids:
                move.write({'analytic_2nd_account_id': self.analytic_2nd_account_id.id})
                for share in move.analytic_2nd_share_ids:
                    share.write({'analytic_account_id': self.analytic_2nd_account_id.id})
            for move in self.move_finished_ids:
                move.write({'analytic_2nd_account_id': self.analytic_2nd_account_id.id})
                for share in move.analytic_2nd_share_ids:
                    share.write({'analytic_account_id': self.analytic_2nd_account_id.id})
