# -*- coding: utf-8 -*-
{
    'name': "Analytic Second Payroll",
    'summary': """Mongolian Analytic Second Payroll""",
    'description': """ Payroll with analytic""",
    'author': "Asterisk Technologies LLC",
    'website': "http://www.asterisk-tech.mn",
    'category': 'Payroll',
    'depends': [
        'l10n_mn_analytic_payroll',
        'l10n_mn_analytic_2nd_account',
        'l10n_mn_hr',
        'l10n_mn_hr_payroll',
    ],
    'data': [
        'views/hr_employee_view.xml',
        'views/hr_contract_view.xml',
        'views/hr_salary_rule_view.xml',
    ],
    'auto_install': True,
}
