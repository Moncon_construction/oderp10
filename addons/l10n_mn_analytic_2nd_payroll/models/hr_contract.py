# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class HrContract(models.Model):
    _inherit = 'hr.contract'

    analytic_2nd_account_id = fields.Many2one('account.analytic.account', 'Analytic account #2', domain=[('tree_number', '=', 'tree_2'), ('type', '=', 'normal')])
