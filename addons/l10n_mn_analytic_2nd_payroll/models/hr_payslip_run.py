# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class HrPayslipRun(models.Model):
    _inherit = 'hr.payslip.run'

    @api.multi
    def _get_account_move_line_vals(self, dictionary, name, partner, is_debit=False, employee=False):
        self.ensure_one()
        config = self.env['hr.payroll.config'].search([('company_id', '=', self.company_id.id)], limit=1)
        is_expense_by_analytic = config and config.expense_line_by_analytic or False
        res = super(HrPayslipRun, self)._get_account_move_line_vals(dictionary, name, partner, is_debit)
        analytic_account_id = False
        if not employee and 'emp' in dictionary and dictionary['emp']:
            employee = self.env['hr.employee'].browse(dictionary['emp'])
        if is_expense_by_analytic and self.env['account.account'].browse(dictionary['account']).req_analytic_account:
            if self.company_id.cost_center_2nd == 'department':
                if employee:
                    if employee.department_id and employee.department_id.analytic_2nd_account_id:
                        analytic_account_id = employee.department_id.analytic_2nd_account_id.id
                    elif employee.contract_id and employee.contract_id.analytic_2nd_account_id:
                        analytic_account_id = employee.contract_id.analytic_2nd_account_id.id
            else:
                if 'hsr_id' in dictionary and dictionary['hsr_id']:
                    salary_rule_id = self.env['hr.salary.rule'].browse(dictionary['hsr_id'])
                    if salary_rule_id.analytic_2nd_account_id:
                        analytic_account_id = salary_rule_id.analytic_2nd_account_id.id
                if not analytic_account_id:
                    if employee:
                        if employee.contract_id and employee.contract_id.analytic_2nd_account_id:
                            analytic_account_id = employee.contract_id.analytic_2nd_account_id.id
                        elif employee.department_id and employee.department_id.analytic_2nd_account_id:
                            analytic_account_id = employee.department_id.analytic_2nd_account_id.id
        if analytic_account_id:
            if not self.company_id.show_analytic_share:
                res[2].update({'analytic_2nd_account_id': analytic_account_id})
            else:
                res[2].update({'analytic_2nd_share_ids': [(0, 0, {'analytic_account_id': analytic_account_id, 'rate': 100})]})
        return res

    @api.multi
    def _group_account_move_line_vals(self, line_ids):
        self.ensure_one()
        line_ids2 = []
        for line in line_ids:
            added = False
            if not self.company_id.show_analytic_share:
                for line2 in line_ids2:
                    if line2[2]['analytic_account_id'] == line[2]['analytic_account_id'] and line2[2]['analytic_2nd_account_id'] == line[2]['analytic_2nd_account_id'] and line2[2]['account_id'] == line[2]['account_id'] and line2[2]['partner_id'] == line[2]['partner_id'] and line2[2]['journal_id'] == line[2]['journal_id']:
                        if line[2]['debit'] > 0:
                            line2[2]['debit'] += line[2]['debit']
                            added = True
                        elif line[2]['credit'] > 0:
                            line2[2]['credit'] += line[2]['credit']
                            added = True
                if not added:
                    line_ids2.append([0, 0, {
                        'name': line[2]['name'],
                        'date': line[2]['date'],
                        'partner_id': line[2]['partner_id'],
                        'account_id': line[2]['account_id'],
                        'journal_id': line[2]['journal_id'],
                        'debit': line[2]['debit'],
                        'credit': line[2]['credit'],
                        'analytic_account_id': line[2]['analytic_account_id'],
                        'analytic_2nd_account_id': line[2]['analytic_2nd_account_id'],
                    }])
            else:
                for line2 in line_ids2:
                    if 'analytic_share_ids' in line[2] and 'analytic_2nd_share_ids' in line[2] and line2[2]['analytic_share_ids'] == line[2]['analytic_share_ids'] and line2[2]['analytic_2nd_share_ids'] == line[2]['analytic_2nd_share_ids'] and line2[2]['account_id'] == line[2]['account_id'] and line2[2]['partner_id'] == line[2]['partner_id'] and line2[2]['journal_id'] == line[2]['journal_id']:
                        if line[2]['debit'] > 0:
                            line2[2]['debit'] += line[2]['debit']
                            added = True
                        elif line[2]['credit'] > 0:
                            line2[2]['credit'] += line[2]['credit']
                            added = True
                if not added:
                    line_ids2.append([0, 0, {
                        'name': line[2]['name'],
                        'date': line[2]['date'],
                        'partner_id': line[2]['partner_id'],
                        'account_id': line[2]['account_id'],
                        'journal_id': line[2]['journal_id'],
                        'debit': line[2]['debit'],
                        'credit': line[2]['credit'],
                        'analytic_share_ids': line[2]['analytic_share_ids'] if 'analytic_share_ids' in line[2] else False,
                        'analytic_2nd_share_ids': line[2]['analytic_2nd_share_ids'] if 'analytic_2nd_share_ids' in line[2] else False,
                    }])
        return line_ids2
