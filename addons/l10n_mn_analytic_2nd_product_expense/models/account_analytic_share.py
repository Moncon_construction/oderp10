# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class AccountAnalyticShare(models.Model):
    _inherit = 'account.analytic.share'

    product_expense_line_2nd_id = fields.Many2one('product.expense.line', 'Product expense line #2', readonly=True)
