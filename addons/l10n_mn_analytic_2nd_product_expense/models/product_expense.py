# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class ProductExpense(models.Model):
    _inherit = 'product.expense'

    analytic_2nd_account_id = fields.Many2one('account.analytic.account', 'Analytic account #2')

    @api.onchange('analytic_2nd_account_id')
    def _onchange_analytic_2nd_account_id(self):
        for line in self.expense_line:
            line.analytic_2nd_account_id = self.analytic_2nd_account_id

    @api.onchange('department')
    def onchange_department(self):
        # Set analytic 2nd account
        analytic_2nd_account_id = False
        if self.cost_center == 'department':
            if self.department.analytic_account_id:
                analytic_2nd_account_id = self.department.analytic_account_id.id
        self.analytic_2nd_account_id = analytic_2nd_account_id
        for line in self.expense_line:
            line.analytic_2nd_account_id = analytic_2nd_account_id
        super(ProductExpense, self).onchange_department()

    @api.onchange('warehouse')
    def onchange_warehouse(self):
        # Set analytic 2nd account
        analytic_2nd_account_id = False
        if self.cost_center == 'warehouse':
            if self.warehouse.analytic_account_id:
                analytic_2nd_account_id = self.warehouse.analytic_account_id.id
        self.analytic_2nd_account_id = analytic_2nd_account_id
        for line in self.expense_line:
            line.analytic_2nd_account_id = analytic_2nd_account_id
        super(ProductExpense, self).onchange_warehouse()
