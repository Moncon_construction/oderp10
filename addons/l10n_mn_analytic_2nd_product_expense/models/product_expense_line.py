# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from odoo.exceptions import UserError


class ProductExpenseLine(models.Model):
    _inherit = 'product.expense.line'

    analytic_2nd_account_id = fields.Many2one('account.analytic.account', 'Analytic account #2')
    analytic_2nd_share_ids = fields.One2many('account.analytic.share', 'product_expense_line_2nd_id', 'Analytic Share #2', ondelete='restrict')

    @api.onchange('product')
    def _onchange_product(self):
        super(ProductExpenseLine, self)._onchange_product()
        if self.cost_center == 'product_categ' and self.product.categ_id and self.product.categ_id.analytic_account_id:
            self.analytic_2nd_account_id = self.product.categ_id.analytic_account_id.id
        elif self.cost_center == 'brand' and self.product.brand_name and self.product.brand_name.analytic_account_id:
            self.analytic_2nd_account_id = self.product.brand_name.analytic_account_id

    @api.multi
    def _get_stock_move_vals(self, picking_id, location_id, location_dest_id):
        res = super(ProductExpenseLine, self)._get_stock_move_vals(picking_id, location_id, location_dest_id)
        res['analytic_2nd_share_ids'] = []
        res['analytic_2nd_account_id'] = self.analytic_2nd_account_id.id
        for share in self.analytic_2nd_share_ids:
            res['analytic_2nd_share_ids'].append((0, 0, {'analytic_account_id': share.analytic_account_id.id, 'rate': share.quantity / self.quantity * 100}))
        return res

    @api.model
    def _check_share_quantities(self):
        super(ProductExpenseLine, self)._check_share_quantities()
        if self.expense.company.show_analytic_share and self.analytic_2nd_share_ids and sum(a.quantity for a in self.analytic_2nd_share_ids) != self.quantity:
            raise UserError(_('The sum of share line quantities must be equal to expense line\'s quantity!'))
