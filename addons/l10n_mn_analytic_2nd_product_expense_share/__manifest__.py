# -*- coding: utf-8 -*-
{
    'name': "Analytic Second Product Expense Share",
    'summary': """Mongolian Analytic Second Product Expense Share""",
    'description': """ Expense products with analytic share""",
    'author': "Asterisk Technologies LLC",
    'website': "http://www.asterisk-tech.mn",
    'category': 'Product Expense',
    'depends': [
        'l10n_mn_analytic_2nd_product_expense',
        'l10n_mn_analytic_product_expense_share',
    ],
    'data': [
        'views/product_expense_view.xml',
    ]
}
