# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian module - Purchase Second Analytic Account",
    'version': '1.0',
    'depends': [
        'l10n_mn_analytic_2nd_account',
        'l10n_mn_analytic_2nd_stock',
        'l10n_mn_analytic_balance_purchase',
        'l10n_mn_purchase_extra_cost',
        'l10n_mn_purchase_shipment',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules/Analytic',
    'description': """
       Mongolian module - Purchase Second Analytic Account
    """,
    'data': [
        'views/purchase_order_view.xml',
        'views/purchase_shipment_view.xml',
        'views/purchase_extra_cost_view.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
