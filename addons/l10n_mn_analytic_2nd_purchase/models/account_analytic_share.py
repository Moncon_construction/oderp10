# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from odoo.exceptions import UserError


class AccountAnalyticShare(models.Model):
    _inherit = 'account.analytic.share'

    purchase_extra_cost_id2 = fields.Many2one('purchase.extra.cost', 'Purchase Extra Cost', readonly=True)

    _sql_constraints = [
        ('unique_for_purchase_extra_cost_id2', 'unique(analytic_account_id, purchase_extra_cost_id2)', 'Analytic shares must be unique!'),
    ]
