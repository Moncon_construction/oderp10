# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.model
    def create(self, values):
        if 'invoice_line_ids' in values and values['invoice_line_ids']:
            for line in values['invoice_line_ids']:
                if 'analytic_share_ids2' in line[2] and len(line[2]['analytic_share_ids2']) == 1 and line[2]['analytic_share_ids2'][0] == [5] and 'analytic_2nd_account_id' in line[2] and line[2]['analytic_2nd_account_id']:
                    line[2].update({'analytic_share_ids2': [(0, 0, {'analytic_account_id': line[2]['analytic_2nd_account_id'], 'rate': 100})]})
        return super(AccountInvoice, self).create(values)

    def _prepare_invoice_line_from_po_line(self, line):
        data = super(AccountInvoice, self)._prepare_invoice_line_from_po_line(line)
        if line.analytic_2nd_account_id:
            data['analytic_2nd_account_id'] = line.analytic_2nd_account_id.id
        return data
