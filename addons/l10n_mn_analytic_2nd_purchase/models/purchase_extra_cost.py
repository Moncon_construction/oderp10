# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.tools.translate import _
from odoo.addons.l10n_mn_extra_cost_item.models.extra_cost_item import SPLIT_METHOD_EC as ec_item
from odoo.exceptions import ValidationError


class PurchaseExtraCost(models.Model):
    _inherit = 'purchase.extra.cost'

    analytic_2nd_account_id = fields.Many2one('account.analytic.account', 'Analytic account #2', domain=[('tree_number', '=', 'tree_2')])
    analytic_2nd_share_ids = fields.One2many('account.analytic.share', 'purchase_extra_cost_id2', 'Analytic Share #2', ondelete='restrict')

    @api.multi
    def make_invoice(self):
        product_uom_unit = self.env['product.uom']
        inv_obj = self.env['account.invoice']
        for extra in self:
            order = extra.order_id
            if order:
                journal_ids = self.env['account.journal'].search(
                    [('type', '=', 'purchase'),
                     ('company_id', '=', self.env.user.company_id.id)],
                    limit=1
                )
                if not journal_ids:
                    raise ValidationError(_("Define purchase journal for this company! %s (id:%d).") % (self.env.user.company_id.name, self.env.user.company_id.id))
                vals = []
                vals2 = []
                share_obj = self.env['account.analytic.share']
                account = order.partner_id.property_account_payable_id
                invoice_line_ids = []
                if extra.account_analytic_id:
                    a_ids = share_obj.create({'analytic_account_id': extra.account_analytic_id.id or False, 'rate': 100})
                    vals.append((4, a_ids.id))
                if extra.analytic_2nd_account_id:
                    a_ids2 = share_obj.create({'analytic_account_id': extra.analytic_2nd_account_id.id or False, 'rate': 100})
                    vals2.append((4, a_ids2.id))
                if not extra.item_id.non_cost_calc and extra.allocmeth in ('equal', 'by_quantity', 'by_subtotal'):
                    accounts = []
                    if extra.check_module_installed('l10n_mn_stock_account_cost_for_each_wh'):
                        if order.picking_type_id.warehouse_id.stock_account_input_id.id not in accounts:
                            accounts.append(order.picking_type_id.warehouse_id.stock_account_input_id.id)
                    else:
                        for line in order.order_line:
                            if line.product_id.categ_id.property_stock_account_input_categ_id.id not in accounts:
                                accounts.append(line.product_id.categ_id.property_stock_account_input_categ_id.id)
                    for acc in accounts:
                        if extra.allocmeth == 'equal':
                            if extra.check_module_installed('l10n_mn_stock_account_cost_for_each_wh'):
                                amount = extra.amount / len(
                                    order.order_line) * sum(
                                    1 if
                                    order.picking_type_id.warehouse_id.stock_account_input_id.id == acc else 0
                                    for line in order.order_line)
                            else:
                                amount = extra.amount / len(
                                    order.order_line) * sum(
                                    1 if
                                    line.product_id.categ_id.property_stock_account_input_categ_id.id == acc else 0
                                    for line in order.order_line)
                        elif extra.allocmeth == 'by_quantity':
                            if extra.check_module_installed('l10n_mn_stock_account_cost_for_each_wh'):
                                amount = extra.amount / sum(
                                    line.product_qty for line in order.order_line) * sum(
                                    line.product_qty if
                                    order.picking_type_id.warehouse_id.stock_account_input_id.id == acc else 0
                                    for line in order.order_line)
                            else:
                                amount = extra.amount / sum(
                                    line.product_qty for line in order.order_line) * sum(
                                    line.product_qty if
                                    line.product_id.categ_id.property_stock_account_input_categ_id.id == acc else 0
                                    for line in order.order_line)
                        else:
                            if extra.check_module_installed('l10n_mn_stock_account_cost_for_each_wh'):
                                amount = extra.amount / sum(
                                    line.price_subtotal for line in order.order_line) * sum(
                                    line.price_subtotal if
                                    order.picking_type_id.warehouse_id.stock_account_input_id.id == acc else 0
                                    for line in order.order_line)
                            else:
                                amount = extra.amount / sum(
                                    line.price_subtotal for line in order.order_line) * sum(
                                    line.price_subtotal if
                                    line.product_id.categ_id.property_stock_account_input_categ_id.id == acc else 0
                                    for line in order.order_line)
                        invoice_line_ids.append((0, 0, {
                            'name': extra.name,
                            'account_id': acc,
                            'price_unit': amount,
                            'quantity': 1.0,
                            'product_id': False,
                            'uom_id': product_uom_unit,
                            'account_analytic_id': extra.account_analytic_id.id or False,
                            'invoice_line_tax_ids': [(6, 0, extra.taxes_id.ids)],
                            'analytic_share_ids': vals,
                            'analytic_share_ids2': vals2}))
                else:
                    invoice_line_ids.append((0, 0, {
                        'name': extra.name,
                        'account_id': extra.account_id.id,
                        'price_unit': extra.amount or 0.0,
                        'quantity': 1.0,
                        'product_id': False,
                        'uom_id': product_uom_unit,
                        'invoice_line_tax_ids': [(6, 0, extra.taxes_id.ids)],
                        'analytic_share_ids': vals,
                        'analytic_share_ids2': vals2}))
                inv_vals = {
                    'purchase_id': order.id,
                    'name': order.partner_ref or order.name,
                    'reference': order.partner_ref or order.name,
                    'account_id': account.id,
                    'type': 'in_invoice',
                    'partner_id': extra.partner_id.id,
                    'currency_id': extra.currency_id.id,
                    'journal_id': len(journal_ids) and journal_ids[0].id or False,
                    'invoice_line_ids': invoice_line_ids,
                    'origin': order.name,
                    'fiscal_position_id': order.fiscal_position_id.id or False,
                    'payment_term_id': False,
                    'company_id': self.env.user.company_id.id,
                    'date_invoice': extra.date_invoice or order.date_order
                }

                inv_id = inv_obj.create(inv_vals)
                order.invoice_ids = inv_id

                order.write({'invoice_ids': [(4, inv_id.id)]})
                extra.write({'invoice_id': inv_id.id})
        product_uom_unit = self.env['product.uom']
        inv_obj = self.env['account.invoice']
        purchase_orders = self.env['purchase.order']
        for extra in self:
            if not extra.order_id and extra.shipment_id:
                journal_ids = self.env['account.journal'].search(
                    [('type', '=', 'purchase'),
                     ('company_id', '=', self.env.user.company_id.id)],
                    limit=1
                )
                if not journal_ids:
                    raise ValidationError(_("Define purchase journal for this company! %s (id:%d).") % (self.env.user.company_id.name, self.env.user.company_id.id))
                vals = []
                vals2 = []
                account = extra.partner_id.property_account_payable_id
                invoice_line_ids = []
                share_obj = self.env['account.analytic.share']
                if extra.account_analytic_id:
                    a_ids = share_obj.create({'analytic_account_id': extra.account_analytic_id.id or False, 'rate': 100})
                    vals.append((4, a_ids.id))
                if extra.analytic_2nd_account_id:
                    a_ids2 = share_obj.create({'analytic_account_id': extra.analytic_2nd_account_id.id or False, 'rate': 100})
                    vals2.append((4, a_ids2.id))
                if not extra.item_id.non_cost_calc and extra.allocmeth in ('equal', 'by_quantity', 'by_subtotal'):
                    accounts = []
                    purchase_orders = purchase_orders.browse(set([line.purchase_line_id.order_id.id for line in extra.shipment_id.line_ids if line.purchase_line_id]))
                    if extra.check_module_installed('l10n_mn_stock_account_cost_for_each_wh'):
                        for order in purchase_orders:
                            if order.picking_type_id.warehouse_id.stock_account_input_id.id not in accounts:
                                accounts.append(order.picking_type_id.warehouse_id.stock_account_input_id.id)
                    else:
                        for line in extra.shipment_id.line_ids:
                            if line.purchase_line_id.product_id.categ_id.property_stock_account_input_categ_id.id not in accounts:
                                accounts.append(line.purchase_line_id.product_id.categ_id.property_stock_account_input_categ_id.id)
                    for acc in accounts:
                        for order in purchase_orders:
                            if extra.allocmeth == 'equal':
                                if extra.check_module_installed('l10n_mn_stock_account_cost_for_each_wh'):
                                    amount = extra.amount / len(extra.shipment_id.line_ids) * sum(1 if order.picking_type_id.warehouse_id.stock_account_input_id.id == acc and line.shipment_id == extra.shipment_id else 0 for line in order.order_line)
                                else:
                                    amount = extra.amount / len(extra.shipment_id.line_ids) * sum(1 if line.product_id.categ_id.property_stock_account_input_categ_id.id == acc and line.shipment_id == extra.shipment_id else 0 for line in order.order_line)
                            elif extra.allocmeth == 'by_quantity':
                                if extra.check_module_installed('l10n_mn_stock_account_cost_for_each_wh'):
                                    amount = extra.amount / sum(line.purchase_line_id.product_qty for line in extra.shipment_id.line_ids) * sum(line.product_qty if order.picking_type_id.warehouse_id.stock_account_input_id.id == acc and line.shipment_id == extra.shipment_id else 0 for line in order.order_line)
                                else:
                                    amount = extra.amount / sum(line.purchase_line_id.product_qty for line in extra.shipment_id.line_ids) * sum(line.product_qty if line.product_id.categ_id.property_stock_account_input_categ_id.id == acc and line.shipment_id == extra.shipment_id else 0 for line in order.order_line)
                            else:
                                if extra.check_module_installed('l10n_mn_stock_account_cost_for_each_wh'):
                                    amount = extra.amount / sum(line.purchase_line_id.price_subtotal for line in extra.shipment_id.line_ids) * sum(line.price_subtotal if order.picking_type_id.warehouse_id.stock_account_input_id.id == acc and line.shipment_id == extra.shipment_id else 0 for line in order.order_line)
                                else:
                                    amount = extra.amount / sum(line.purchase_line_id.price_subtotal for line in extra.shipment_id.line_ids) * sum(line.price_subtotal if line.product_id.categ_id.property_stock_account_input_categ_id.id == acc and line.shipment_id == extra.shipment_id else 0 for line in order.order_line)
                            invoice_line_ids.append((0, 0, {
                                'name': extra.name,
                                'account_id': acc,
                                'price_unit': amount,
                                'quantity': 1.0,
                                'product_id': False,
                                'uom_id': product_uom_unit,
                                'invoice_line_tax_ids': [(6, 0, extra.taxes_id.ids)],
                                'analytic_share_ids': vals,
                                'analytic_share_ids2': vals2}))
                else:
                    invoice_line_ids.append((0, 0, {
                        'name': extra.name,
                        'account_id': extra.account_id.id,
                        'price_unit': extra.amount or 0.0,
                        'quantity': 1.0,
                        'product_id': False,
                        'uom_id': product_uom_unit,
                        'invoice_line_tax_ids': [(6, 0, extra.taxes_id.ids)],
                        'analytic_share_ids': vals,
                        'analytic_share_ids2': vals2}))
                inv_vals = {
                    'name': extra.shipment_id.name,
                    'reference': extra.shipment_id.name,
                    'account_id': account.id,
                    'type': 'in_invoice',
                    'partner_id': extra.partner_id.id,
                    'currency_id': extra.currency_id.id,
                    'journal_id': len(journal_ids) and journal_ids[0].id or False,
                    'invoice_line_ids': invoice_line_ids,
                    'origin': extra.shipment_id.name,
                    'payment_term_id': False,
                    'company_id': self.env.user.company_id.id,
                    'date_invoice': extra.date_invoice
                }

                inv_id = inv_obj.create(inv_vals)
                for order in purchase_orders:
                    order.invoice_ids = inv_id
                    order.write({'invoice_ids': [(4, inv_id.id)]})
                extra.write({'invoice_id': inv_id.id})
        return True
