# -*- coding: utf-8 -*-
from odoo import _, api, exceptions, fields, models


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    analytic_2nd_account_id = fields.Many2one('account.analytic.account', 'Analytic account #2', domain=[('tree_number', '=', 'tree_2')])
    account_analytic_id = fields.Many2one(domain=[('tree_number', '=', 'tree_1')])
    cost_center_2nd = fields.Selection(related='company_id.cost_center_2nd')

    @api.onchange('analytic_2nd_account_id')
    def _onchange_analytic_2nd_account_id(self):
        for line in self.order_line:
            line.analytic_2nd_account_id = self.analytic_2nd_account_id.id

    @api.onchange('warehouse_id')
    def _onchange_warehouse_id(self):
        if self.warehouse_id and self.company_id.cost_center_2nd == 'warehouse':
            if self.warehouse_id.account_analytic_id:
                self.analytic_2nd_account_id = self.warehouse_id.account_analytic_id.id
            else:
                raise UserError(_('Please choose analytic account for the warehouse %s!') % self.warehouse_id.name)

    @api.multi
    def action_view_invoice(self):
        result = super(PurchaseOrder, self).action_view_invoice()
        result['context'].update({'default_analytic_2nd_account_id': self.analytic_2nd_account_id.id})
        return result

    @api.model
    def create(self, values):
        if 'contract_id' in values and values['contract_id'] and 'contract.management' in self.env and self.env.user.company_id.cost_center_2nd == 'contract':
            analytic_2nd_account_id = self.env['contract.management'].browse(values['contract_id']).analytic_account_id.id
            values['analytic_2nd_account_id'] = analytic_2nd_account_id
            if 'order_line' in values and values['order_line']:
                for line in values['order_line']:
                    if 'analytic_2nd_account_id' not in line[2]:
                        line[2]['analytic_2nd_account_id'] = analytic_2nd_account_id
        return super(PurchaseOrder, self).create(values)
