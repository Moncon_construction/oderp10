# -*- coding: utf-8 -*-
from odoo import _, api, exceptions, fields, models


class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    @api.onchange('product_id')
    def _onchange_product_id(self):
        if self.product_id:
            if self.order_id.company_id.cost_center_2nd == 'product_categ':
                if self.product_id.categ_id.analytic_account_id:
                    self.analytic_2nd_account_id = self.product_id.categ_id.analytic_account_id.id
                else:
                    raise UserError(_('Please choose analytic account for the product category %s!') % self.product_id.categ_id.name)
            elif self.env.user.company_id.cost_center_2nd == 'brand':
                if self.product_id.brand_name:
                    if self.product_id.brand_name.analytic_account_id:
                        self.analytic_2nd_account_id = self.product_id.brand_name.analytic_account_id.id
                    else:
                        raise UserError(_('Please choose analytic account for the brand %s!') % self.product_id.brand_name.brand_name)
                else:
                    raise UserError(_('Please choose brand for the product %s!') % self.product_id.name)

    account_analytic_id = fields.Many2one(domain=[('tree_number', '=', 'tree_1')])
    analytic_2nd_account_id = fields.Many2one('account.analytic.account', 'Analytic account #2', domain=[('tree_number', '=', 'tree_2')])

    @api.multi
    def _prepare_stock_moves(self, picking):
        res = super(PurchaseOrderLine, self)._prepare_stock_moves(picking)
        self.ensure_one()
        for move in res:
            if move['purchase_line_id'] == self.id and self.analytic_2nd_account_id:
                move['analytic_2nd_account_id'] = self.analytic_2nd_account_id.id
                move['analytic_2nd_share_ids'] = [(0, 0, {'analytic_account_id': self.analytic_2nd_account_id.id, 'rate': 100})]
        return res
