# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian module - Purchase Second Analytic Account Share",
    'version': '1.0',
    'depends': [
        'l10n_mn_analytic_balance_purchase_share', 'l10n_mn_analytic_2nd_purchase'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules/Analytic',
    'description': """
       Mongolian module - Purchase Second Analytic Account
    """,
    'data': [
        'views/purchase_extra_cost_view.xml',
        'views/purchase_shipment_view.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
