# -*- coding: utf-8 -*-
{
    'name': "Mongolian module - Sales Second Analytic Account",
    'version': '1.0',
    'depends': [
        'l10n_mn_analytic_2nd',
        'l10n_mn_analytic_2nd_account',
        'sale',
        'l10n_mn_sale_stock',
        'l10n_mn_analytic_2nd_stock',
        'l10n_mn_analytic_balance_sale',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules/Analytic',
    'description': """
       Mongolian module - Sales Second Analytic Account
    """,
    'data': [
        'views/sale_order_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
