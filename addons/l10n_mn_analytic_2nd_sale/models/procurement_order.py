# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import _, api, fields, models, registry


class ProcurementOrder(models.Model):
    _inherit = "procurement.order"

    def _get_stock_move_values(self):
        stock_move = super(ProcurementOrder, self)._get_stock_move_values()
        if self.sale_line_id and self.sale_line_id.analytic_2nd_account_id:
            stock_move.update({
                'analytic_2nd_account_id': self.sale_line_id.analytic_2nd_account_id.id,
                'analytic_2nd_share_ids': [(0, 0, {'analytic_account_id': self.sale_line_id.analytic_2nd_account_id.id, 'rate': 100})]})
        return stock_move
