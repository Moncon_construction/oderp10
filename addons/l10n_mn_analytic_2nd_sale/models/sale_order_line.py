# -*- coding: utf-8 -*-
from odoo import _, api, exceptions, fields, models


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    analytic_account_id = fields.Many2one(domain=[('tree_number', '=', 'tree_1')])
    analytic_2nd_account_id = fields.Many2one('account.analytic.account', 'Analytic account #2', domain=[('tree_number', '=', 'tree_2')])
    analytic_2nd_share_ids = fields.One2many('account.analytic.share', 'sale_order_line_id2', 'Analytic Share #2', ondelete='restrict')

    @api.onchange('product_id')
    def _onchange_product_id(self):
        super(SaleOrderLine, self)._onchange_product_id()
        if self.product_id:
            if self.order_id.company_id.cost_center_2nd == 'product_categ' and self.product_id.categ_id:
                if self.product_id.categ_id.analytic_account_id:
                    self.analytic_2nd_account_id = self.product_id.categ_id.analytic_account_id.id
                else:
                    self.analytic_2nd_account_id = False
                    raise UserError(_('Please choose analytic account for the product category %s!') % self.product_id.categ_id.name)
            elif self.order_id.company_id.cost_center_2nd == 'brand':
                if self.product_id.brand_name:
                    if self.product_id.brand_name.analytic_account_id:
                        self.analytic_2nd_account_id = self.product_id.brand_name.analytic_account_id.id
                    else:
                        self.analytic_2nd_account_id = False
                        raise UserError(_('Please choose analytic account for the brand %s!') % self.product_id.brand_name.brand_name)
                else:
                    self.analytic_2nd_account_id = False
                    raise UserError(_('Please choose brand for the product %s!') % self.product_id.name)

    @api.multi
    def _prepare_invoice_line(self, qty):
        self.ensure_one()
        res = super(SaleOrderLine, self)._prepare_invoice_line(qty)
        # If sale order line has analytic account update invoice's analytic account
        if self.analytic_2nd_account_id:
            res.update({
                'analytic_2nd_account_id': self.analytic_2nd_account_id.id})
        if self.analytic_2nd_share_ids:
            res.update({
                'analytic_share_ids2': [(0, 0, {'analytic_account_id': share.analytic_account_id.id, 'rate': share.rate}) for share in self.analytic_2nd_share_ids]})
        return res
