# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from odoo.exceptions import UserError


class SaleAdvancePaymentInv(models.TransientModel):
    _inherit = "sale.advance.payment.inv"

    @api.multi
    def _create_invoice(self, order, so_line, amount):
        invoice = super(SaleAdvancePaymentInv, self)._create_invoice(order, so_line, amount)
        for line in invoice.invoice_line_ids:
            if so_line in line.sale_line_ids:
                if so_line.analytic_2nd_account_id:
                    line.write({
                        'analytic_2nd_account_id': so_line.analytic_2nd_account_id.id})
                if so_line.analytic_2nd_share_ids:
                    line.write({
                        'analytic_share_ids2': [(0, 0, {'analytic_account_id': share.analytic_account_id.id, 'rate': share.rate}) for share in so_line.analytic_2nd_share_ids]})
        return invoice

    def _get_sale_order_line_values(self, amount, order, tax_ids):
        self.ensure_one()
        res = super(SaleAdvancePaymentInv, self)._get_sale_order_line_values(amount, order, tax_ids)
        total = order.amount_total
        share_dict = {}
        for line in order.order_line:
            for share in line.analytic_2nd_share_ids:
                divide = line.price_total * share.rate
                if share.analytic_account_id.id not in share_dict:
                    share_dict[share.analytic_account_id.id] = (divide / total) if divide > 0 else 0
                else:
                    share_dict[share.analytic_account_id.id] += (divide / total) if divide > 0 else 0
        vals = []
        if total > 0:
            total_rate = 0
            for k, v in share_dict.items():
                vals.append((0, 0, {'analytic_account_id': k, 'rate': v}))
                total_rate += v
            if round(total_rate, 2) != 100:
                raise UserError(_('Sale order does not have a valid analytic share!'))
        res.update({'analytic_2nd_share_ids': vals if total > 0 else False,
                    'analytic_2nd_account_id': order.analytic_2nd_account_id.id if order.analytic_2nd_account_id else False})
        return res
