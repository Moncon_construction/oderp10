# -*- coding: utf-8 -*-
{
    'name': "Mongolian module - Sales Second Analytic Account Share",
    'version': '1.0',
    'depends': [
        'l10n_mn_analytic_2nd_sale',
        'l10n_mn_analytic_balance_sale_share',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules/Analytic',
    'description': """
       Mongolian module - Sales Second Analytic Account Share
    """,
    'data': [
        'views/sale_order_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
