# -*- coding: utf-8 -*-
{
    'name': 'Mongolian Balance Stock Second Analytic',
    'version': '1.0',
    'depends': [
        'l10n_mn_analytic_balance_stock',
        'l10n_mn_analytic_2nd_account',
        'l10n_mn_sale_stock',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """Mongolian Balance Stock Second Analytic""",
    'data': [
        'views/stock_move_view.xml',
        'views/stock_warehouse_view.xml',
    ],
    "auto_install": False,
    "installable": True,
}
