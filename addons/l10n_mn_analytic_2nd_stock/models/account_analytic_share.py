from odoo import _, api, fields, models


class AccountAnalyticShare(models.Model):
    _inherit = 'account.analytic.share'

    stock_move_2nd_id = fields.Many2one('stock.move', 'Stock move #2', readonly=True)
