# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class StockWarehouse(models.Model):
    _inherit = 'stock.warehouse'

    cost_center_2nd = fields.Selection(related='company_id.cost_center_2nd')
    analytic_account_id = fields.Many2one(domain=[('tree_number', '=', 'tree_1')])
    analytic_2nd_account_id = fields.Many2one('account.analytic.account', 'Analytic account #2', domain=[('tree_number', '=', 'tree_2')])
