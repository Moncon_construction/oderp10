# -*- coding: utf-8 -*-
{
    'name': 'Mongolian Balance Stock Second Analytic Share',
    'version': '1.0',
    'depends': [
        'l10n_mn_analytic_2nd_stock',
        'l10n_mn_analytic_balance_stock_share',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """Mongolian Balance Stock Second Analytic Share""",
    'data': [
        'views/stock_move_view.xml',
    ],
    "auto_install": False,
    "installable": True,
}
