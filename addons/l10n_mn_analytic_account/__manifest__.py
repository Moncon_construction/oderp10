# -*- coding: utf-8 -*-
{
    'name': "Mongolian Analytic Account",
    'version': '1.0',
    'depends': [
        'l10n_mn_account',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Mongolian Analytic Account
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/account_bank_statement_line_view.xml',
        'views/account_config_settings_view.xml',
        'views/account_invoice_line_view.xml',
        'views/action.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
