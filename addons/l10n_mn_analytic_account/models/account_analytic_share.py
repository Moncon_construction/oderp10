# -*- coding: utf-8 -*-
from odoo import fields, models


class AccountAnalyticShare(models.Model):
    _name = "account.analytic.share"

    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic Account', required=True, index=True, domain="[('type','=','normal')]")
    analytic_line_id = fields.Many2one('account.analytic.line', 'Analytic Line', readonly=True)
    rate = fields.Float('Rate (%)', required=True, default=100)
    move_line_id = fields.Many2one('account.move.line', 'Move Line', readonly=True)
    invoice_line_id = fields.Many2one('account.invoice.line', 'Invoice Line', readonly=True)
    bank_statement_line_id = fields.Many2one('account.bank.statement.line', 'Bank Statement Line', readonly=True)

    _sql_constraints = [
        ('positive_rate', 'CHECK(rate > 0)', 'Analytic share\'s rate must be higher than 0!'),
        ('unique_for_account_invoice_line', 'unique(analytic_account_id, invoice_line_id)', 'Analytic shares must be unique!'),
    ]
