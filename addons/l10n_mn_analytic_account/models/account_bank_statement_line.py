# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError


class AccountBankStatementLine(models.Model):
    _inherit = "account.bank.statement.line"

    analytic_share_ids = fields.One2many('account.analytic.share', 'bank_statement_line_id', 'Analytic Share', ondelete='restrict')

    @api.multi
    def _check_analytic_share(self):
        ''' Шинжилгээний тархалт зөв үүссэн эсэхийг шалгах функц '''
        self.ensure_one()
        if self.account_id.req_analytic_account and self.statement_id.company_id.show_analytic_share:
            if not self.analytic_share_ids:
                raise UserError(_("Account %s requires analytic account!") % self.account_id.name)
        if self.analytic_share_ids:
            total_rate = 0.0
            for share in self.analytic_share_ids:
                total_rate += share.rate
            if round(total_rate, 2) != 100:
                raise UserError(_('Bank statement does not have a valid analytic share!'))

    @api.multi
    def write(self, vals):
        res = super(AccountBankStatementLine, self).write(vals)
        for record in self:
            record._check_analytic_share()
        return res

    @api.multi
    def unlink(self):
        for line in self:
            for share in line.analytic_share_ids:
                if share.invoice_line_id or share.move_line_id:
                    share.bank_statement_line_id = False
                else:
                    share.unlink()
        return super(AccountBankStatementLine, self).unlink()

    @api.model
    def create(self, vals):
        res = super(AccountBankStatementLine, self).create(vals)
        res._check_analytic_share()
        return res
