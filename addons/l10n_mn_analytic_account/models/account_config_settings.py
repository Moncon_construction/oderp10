from odoo import _, api, fields, models


class AccountConfigSettings(models.TransientModel):
    _inherit = 'account.config.settings'

    show_analytic_share = fields.Boolean('Show analytic share', related='company_id.show_analytic_share')

    @api.multi
    def execute(self):
        module = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_analytic_account_share')])
        if module:
            if self.show_analytic_share:
                if module.state == 'uninstalled':
                    module.sudo().button_immediate_install()
                elif module.state == 'installed':
                    module.sudo().button_immediate_upgrade()
            else:
                if module.state in ('installed', 'to upgrade'):
                    module.sudo().button_immediate_uninstall()
                elif module.state == 'to install':
                    module.sudo().button_install_cancel()
        module = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_analytic_2nd_account')])
        if module and module.state in ('installed', 'to upgrade'):
            module2 = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_analytic_2nd_account_share')])
            if self.show_analytic_share:
                if module2 and module2.state in ('uninstalled'):
                    module2.sudo().button_immediate_install()
                elif module2 and module2.state in ('installed', 'to upgrade'):
                    module2.sudo().button_immediate_upgrade()
            else:
                if module2 and module2.state in ('installed', 'to upgrade'):
                    module2.sudo().button_immediate_uninstall()
                elif module2.state == 'to install':
                    module.sudo().button_install_cancel()
        module = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_analytic_balance_stock')])
        if module and module.state in ('installed', 'to upgrade'):
            module2 = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_analytic_balance_stock_share')])
            if self.show_analytic_share:
                if module2 and module2.state in ('uninstalled'):
                    module2.sudo().button_immediate_install()
                elif module2 and module2.state in ('installed', 'to upgrade'):
                    module2.sudo().button_immediate_upgrade()
            else:
                if module2 and module2.state in ('installed', 'to upgrade'):
                    module2.sudo().button_immediate_uninstall()
                elif module2.state == 'to install':
                    module.sudo().button_install_cancel()
        module = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_analytic_2nd_stock')])
        if module and module.state in ('installed', 'to upgrade'):
            module2 = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_analytic_2nd_stock_share')])
            if self.show_analytic_share:
                if module2 and module2.state in ('uninstalled'):
                    module2.sudo().button_immediate_install()
                elif module2 and module2.state in ('installed', 'to upgrade'):
                    module2.sudo().button_immediate_upgrade()
            else:
                if module2 and module2.state in ('installed', 'to upgrade'):
                    module2.sudo().button_immediate_uninstall()
                elif module2.state == 'to install':
                    module.sudo().button_install_cancel()
        module = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_analytic_product_expense')])
        if module and module.state in ('installed', 'to upgrade'):
            module2 = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_analytic_product_expense_share')])
            if self.show_analytic_share:
                if module2 and module2.state in ('uninstalled'):
                    module2.sudo().button_immediate_install()
                elif module2 and module2.state in ('installed', 'to upgrade'):
                    module2.sudo().button_immediate_upgrade()
            else:
                if module2 and module2.state in ('installed', 'to upgrade'):
                    module2.sudo().button_immediate_uninstall()
                elif module2.state == 'to install':
                    module.sudo().button_install_cancel()
        module = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_analytic_2nd_product_expense')])
        if module and module.state in ('installed', 'to upgrade'):
            module2 = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_analytic_2nd_product_expense_share')])
            if self.show_analytic_share:
                if module2 and module2.state in ('uninstalled'):
                    module2.sudo().button_immediate_install()
                elif module2 and module2.state in ('installed', 'to upgrade'):
                    module2.sudo().button_immediate_upgrade()
            else:
                if module2 and module2.state in ('installed', 'to upgrade'):
                    module2.sudo().button_immediate_uninstall()
                elif module2.state == 'to install':
                    module.sudo().button_install_cancel()
        module = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_analytic_balance_sale')])
        if module and module.state in ('installed', 'to upgrade'):
            module2 = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_analytic_balance_sale_share')])
            if self.show_analytic_share:
                if module2 and module2.state in ('uninstalled'):
                    module2.sudo().button_immediate_install()
                elif module2 and module2.state in ('installed', 'to upgrade'):
                    module2.sudo().button_immediate_upgrade()
            else:
                if module2 and module2.state in ('installed', 'to upgrade'):
                    module2.sudo().button_immediate_uninstall()
                elif module2.state == 'to install':
                    module.sudo().button_install_cancel()
        module = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_analytic_2nd_sale')])
        if module and module.state in ('installed', 'to upgrade'):
            module2 = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_analytic_2nd_sale_share')])
            if self.show_analytic_share:
                if module2 and module2.state in ('uninstalled'):
                    module2.sudo().button_immediate_install()
                elif module2 and module2.state in ('installed', 'to upgrade'):
                    module2.sudo().button_immediate_upgrade()
            else:
                if module2 and module2.state in ('installed', 'to upgrade'):
                    module2.sudo().button_immediate_uninstall()
                elif module2.state == 'to install':
                    module.sudo().button_install_cancel()
        return super(AccountConfigSettings, self).execute()
