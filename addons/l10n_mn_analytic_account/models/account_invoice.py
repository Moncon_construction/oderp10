# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from odoo.exceptions import UserError

MAGIC_COLUMNS = ('id', 'create_uid', 'create_date', 'write_uid', 'write_date')


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.model
    def line_get_convert(self, line, part):
        ''' Корийн функцэнд тархалтын id-г нэмэв '''
        res = super(AccountInvoice, self).line_get_convert(line, part)
        if 'invl_id' in line:
            invoice_line = self.env['account.invoice.line'].browse(line['invl_id'])
            if invoice_line and invoice_line.account_id and invoice_line.account_id.req_analytic_account and self.company_id.show_analytic_share and invoice_line.analytic_share_ids:
                res.update({'analytic_share_ids': [(0, 0, {'analytic_account_id': s.analytic_account_id.id, 'rate': s.rate}) for s in invoice_line.analytic_share_ids]})
        return res

    @api.multi
    def write(self, vals):
        res = super(AccountInvoice, self).write(vals)
        # 1. Санхүүгийн тохиргоонд шинжилгээний тархалт харуулахыг чеклэсэн
        # 2. Нэхэмжлэлийн мөр дээрх данс нь шинжилгээний данс шаардах
        # 3. Мөрийн шинжилгээний тархалт талбар хоосон
        # дээрх 3 нөхцөл биелэж байвал анхааруулга өгнө.
        for record in self:
            if record.company_id.show_analytic_share:
                for line in record.invoice_line_ids:
                    if line.analytic_share_ids:
                        total = 0
                        for share in line.analytic_share_ids:
                            total += share.rate
                        if round(total, 2) != 100:
                            raise UserError(_('The sum rate of the analytic shares must be 100%!'))
        return res

    @api.model
    def _refund_cleanup_lines(self, lines):
        # Буцаалтын нэхэмжлэл үүсгэхэд шинжилгээний тархалт авч үүсдэг болгохын тулд override хийсэн.
        result = []
        for line in lines:
            values = {}
            for name, field in line._fields.iteritems():
                if name in MAGIC_COLUMNS:
                    continue
                elif field.type == 'many2one':
                    values[name] = line[name].id
                elif field.type not in ['many2many', 'one2many']:
                    values[name] = line[name]
                elif name == 'invoice_line_tax_ids':
                    values[name] = [(6, 0, line[name].ids)]
                # starts here
                elif name == 'analytic_share_ids':
                    values[name] = []
                    for share in line[name]:
                        values[name].append((0, 0, {'analytic_account_id': share.analytic_account_id.id, 'rate': share.rate}))
                # ends here
            result.append((0, 0, values))
        return result

    def action_invoice_open(self):
        for record in self:
            if record.company_id.show_analytic_share:
                for line in record.invoice_line_ids:
                    account = line.account_id
                    if account.req_analytic_account and not line.analytic_share_ids:
                        raise UserError(_('Account %s requires analytic account. Please choose analytic share for the line!') % account.name)
        res = super(AccountInvoice, self).action_invoice_open()
        return res

    @api.multi
    def finalize_invoice_move_lines(self, move_lines):
        res = super(AccountInvoice, self).finalize_invoice_move_lines(move_lines)
        for line in res:
            # set analytic account or analytic share for the receivable or payable account move line
            if len(line) > 2 and line[2] and 'account_id' in line[2] and line[2]['account_id'] and line[2]['account_id'] == self.account_id.id and self.account_id.req_analytic_account:
                if self.company_id.show_analytic_share:
                    total = self.amount_total
                    share_dict = {}
                    for l in self.invoice_line_ids:
                        for share in l.analytic_share_ids:
                            if share.analytic_account_id.id not in share_dict:
                                share_dict[share.analytic_account_id.id] = l.quantity * l.price_unit / total * share.rate
                            else:
                                share_dict[share.analytic_account_id.id] += l.quantity * l.price_unit / total * share.rate
                    vals = []
                    total_rate = 0
                    for k, v in share_dict.items():
                        vals.append((0, 0, {'analytic_account_id': k, 'rate': v}))
                        total_rate += v
                    if round(total_rate, 2) not in (100, 0):
                        raise UserError(_('Invoice does not have a valid analytic share!'))
                    line[2].update({
                        'analytic_share_ids': vals
                    })
                else:
                    if not self.company_id.default_analytic_account_id:
                        raise UserError(_('Please choose default analytic account for the company %s!') % self.company_id.name)
                    line[2].update({
                        'analytic_account_id': self.company_id.default_analytic_account_id.id
                    })
        return res
