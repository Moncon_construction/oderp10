# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError, Warning


class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"

    analytic_share_ids = fields.One2many('account.analytic.share', 'invoice_line_id', 'Analytic Share')

    @api.multi
    def write(self, vals):
        if 'account_id' in vals:
            for record in self:
                account = self.env['account.account'].browse(vals['account_id'])
                if 'invoice_id' in vals and vals['invoice_id']:
                    invoice = self.env['account.invoice'].browse(vals['invoice_id'])
                else:
                    invoice = record.invoice_id
        if 'analytic_share_ids' in vals:
            # one2many утга ирэх ба хэрэв [default number, False or id, id or {}] утгатай байвал
            if len(vals['analytic_share_ids']) >= 2:
                rate = 0.0
                for share in self.analytic_share_ids:
                    rate += share.rate
                for share in vals['analytic_share_ids']:
                    # [default number, False or id, {[id, rate], ...}]
                    if len(share) > 2:
                        if share[2] and share[2]['rate']:
                            # [default number, id, {}] бол засах үйлдэл хийж байгаа тул хасна
                            if share[1]:
                                rate -= self.env['account.analytic.share'].browse(share[1]).rate
                            rate += share[2]['rate']
                if round(rate, 2) != 100:
                    raise UserError(_('The sum rate of the analytic shares must be 100%!'))
        return super(AccountInvoiceLine, self).write(vals)

    @api.multi
    def unlink(self):
        ''' Нэхэмжлэлийн мөрийг устгах үед шинжилгээний тархалтынх мөр дээр хуулгын болон бичилтийн мөр холбоотой байвал салгах э.тохиолдолд устгах '''
        for line in self:
            for share in line.analytic_share_ids:
                if share.bank_statement_line_id or share.move_line_id:
                    share.invoice_line_id = False
                else:
                    share.unlink()
        return super(AccountInvoiceLine, self).unlink()

    @api.model
    def create(self, vals):
        account = self.env['account.account'].browse(vals['account_id'])
        invoice = self.env['account.invoice'].browse(vals['invoice_id'])
        if 'analytic_share_ids' in vals:
            # one2many утга ирэх ба хэрэв [default number, False or id, id or {}] утгатай байвал
            if len(vals['analytic_share_ids']) >= 2:
                rate = 0.0
                for share in vals['analytic_share_ids']:
                    if share[2]['rate']:
                        rate += share[2]['rate']
                if round(rate, 2) != 100:
                    raise UserError(_('The sum rate of the analytic shares must be 100%!'))
        return super(AccountInvoiceLine, self).create(vals)
