# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError


class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    analytic_share_ids = fields.One2many('account.analytic.share', 'move_line_id', 'Analytic Share')

    @api.multi
    def _check_analytic_share(self):
        ''' Шинжилгээний тархалт зөв үүссэн эсэхийг шалгах функц '''
        self.ensure_one()
        if self.company_id.show_analytic_share and self.move_id.statement_line_id and self.account_id.req_analytic_account and not self.analytic_share_ids:
            self.write({'analytic_share_ids': [(0, 0, {'analytic_account_id': share.analytic_account_id.id, 'rate': share.rate}) for share in self.move_id.statement_line_id.analytic_share_ids]})
        if self.account_id.req_analytic_account and self.company_id.show_analytic_share and not self.analytic_share_ids:
            raise UserError(_("Account %s requires analytic account!") % self.account_id.name)
        if self.analytic_share_ids:
            total_rate = 0.0
            for share in self.analytic_share_ids:
                total_rate += share.rate
            if round(total_rate, 2) != 100:
                raise UserError(_('Account move does not have a valid analytic share!'))

    @api.multi
    def create_analytic_lines(self):
        ''' Шинжилгээний тархалт сонгосон бол журналын бичилт үүсгэх функц '''
        self.mapped('analytic_line_ids').unlink()
        analytic_line_obj = self.env['account.analytic.line']
        for obj_line in self:
            if obj_line.analytic_share_ids:
                for share_line in obj_line.analytic_share_ids:
                    val = (obj_line.credit or 0.0) - (obj_line.debit or 0.0)
                    amt = val * (share_line.rate / 100)
                    vals_line = obj_line._prepare_analytic_line()[0]
                    vals_line.update({
                        'date': obj_line.date,
                        'amount': amt,
                        'general_account_id': obj_line.account_id.id,
                        'account_id': share_line.analytic_account_id.id, })
                    line_id = analytic_line_obj.create(vals_line)
                    share_line.write({'analytic_line_id': line_id.id})
            elif obj_line.analytic_account_id:
                vals_line = obj_line._prepare_analytic_line()[0]
                a = analytic_line_obj.create(vals_line)

    @api.multi
    def write(self, vals):
        res = super(AccountMoveLine, self).write(vals)
        for record in self:
            if 'currency_rate' in vals and len(vals) == 1:
                continue
            elif 'register_date' in vals and len(vals) == 1:
                continue
            else:
                record._check_analytic_share()
        return res

    @api.model
    def create(self, vals):
        res = super(AccountMoveLine, self).create(vals)
        res._check_analytic_share()
        res.create_analytic_lines()
        return res
