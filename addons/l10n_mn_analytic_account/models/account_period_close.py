# -*- coding: utf-8 -*-
from openerp import _, api, fields, models


class AccountPeriodClose(models.TransientModel):
    _inherit = 'account.period.close'

    @api.model
    def action_close(self, period_ids):
        ''' Тайлант хугацааг хааж орлого зарлагын түр дансуудын
            үлдэгдлийг орлого зарлагын нэгдсэн дансанд хаана.
        '''
        obj_acc_move = self.env['account.move']
        if not self.journal_id.default_debit_account_id or not self.journal_id.default_credit_account_id:
            raise ValidationError(_('There is no Default Debit/Credit Account defined on your selected journal!'))
        if not self.journal_id.profit_account_id or not self.journal_id.loss_account_id:
            raise ValidationError(_('There is no Reserve & Profit/Loss Account defined on your selected journal!'))
        res = []
        for period in self.env['account.period'].browse(period_ids):
            # Орлого, Зарлагын түр дансны тайлант хугацааны хоорондох дүнг шинжилгээний дансаар олно
            self.env.cr.execute('''
                select aa.id AS aid,
                    aaa.id AS aaid,
                    aml.currency_id AS cid,
                    sum(aal.amount) as amount,
                    sum(aal.amount_currency) as amount_currency
                from account_analytic_line aal
                    left join account_move_line aml on aml.id = aal.move_id
                    left join account_account aa on aa.id = aml.account_id
                    left join account_analytic_account aaa on aal.account_id = aaa.id
                    left join account_move am on am.id = aml.move_id
                where am.state = 'posted'
                    and aa.internal_type in ('income', 'expense')
                    and aml.date BETWEEN \'''' + period.date_start + '''\'
                    AND \'''' + period.date_stop + '''\'
                    AND aml.company_id = ''' + str(period.company_id.id) + '''
                    AND aa.id != ''' + str(self.journal_id.default_debit_account_id.id) + '''
                group by aa.id,
                    aaa.id,
                    aml.currency_id
            ''')
            account_sum_dict = self.env.cr.dictfetchall()
            reverse_debit = reverse_credit = 0.0
            move_lines = []
            for line in account_sum_dict:
                # account_move руу бичих өгөгдөл
                if line['amount'] < 0:
                    debit = -line['amount']
                    credit = 0
                else:
                    credit = line['amount']
                    debit = 0
                balance = credit - debit
                amount_currency = line['amount_currency']
                if (credit > debit and line['amount_currency'] < 0) or (credit < debit and line['amount_currency'] > 0):
                    amount_currency = -amount_currency
                # Орлого зарлагын түр дансуудыг хаана.
                move_lines.append((0, 0, {
                    'debit': (credit > debit and abs(balance)) or 0.0,
                    'credit': (debit > credit and abs(balance)) or 0.0,
                    'name': self.description,
                    'date': period.date_stop,
                    'journal_id': self.journal_id.id,
                    'amount_currency': amount_currency,
                    'currency_id': line['cid'] or False,
                    'account_id': line['aid'],
                    'company_id': self.company_id.id,
                    'analytic_account_id': line['aaid'] or False,
                    'analytic_share_ids': [(0, 0, {'analytic_account_id': line['aaid'], 'rate': 100})] if line['aaid'] else False
                }))
                reverse_debit += (credit > debit and abs(balance)) or 0.0
                reverse_credit += (debit > credit and abs(balance)) or 0.0
            if reverse_credit > reverse_debit:
                # create move lines
                move_lines.append((0, 0, {
                    'debit': abs(reverse_credit - reverse_debit),
                    'credit': 0,
                    'name': self.description,
                    'date': period.date_stop,
                    'create_date': period.date_stop,
                    'journal_id': self.journal_id.id,
                    'period_id': period.id,
                    'account_id': self.journal_id.default_debit_account_id.id,
                    'company_id': self.company_id.id,
                }))
                move_lines.append((0, 0, {
                    'debit': 0,
                    'credit': abs(reverse_credit - reverse_debit),
                    'name': self.description,
                    'date': period.date_stop,
                    'create_date': period.date_stop,
                    'journal_id': self.journal_id.id,
                    'period_id': period.id,
                    'account_id': self.journal_id.default_debit_account_id.id,
                    'company_id': self.company_id.id,
                }))
                move_lines.append((0, 0, {
                    'debit': abs(reverse_credit - reverse_debit),
                    'credit': 0,
                    'name': self.description,
                    'date': period.date_stop,
                    'create_date': period.date_stop,
                    'journal_id': self.journal_id.id,
                    'period_id': period.id,
                    'account_id': self.journal_id.loss_account_id.id,
                    'company_id': self.company_id.id,
                }))
            elif reverse_debit > reverse_credit:
                move_lines.append((0, 0, {
                    'debit': 0,
                    'credit': abs(reverse_debit - reverse_credit),
                    'name': self.description,
                    'date': period.date_stop,
                    'date_created': period.date_stop,
                    'journal_id': self.journal_id.id,
                    'period_id': period.id,
                    'account_id': self.journal_id.default_credit_account_id.id,
                    'company_id': self.company_id.id,
                }))
                move_lines.append((0, 0, {
                    'debit': abs(reverse_debit - reverse_credit),
                    'credit': 0,
                    'name': self.description,
                    'date': period.date_stop,
                    'date_created': period.date_stop,
                    'journal_id': self.journal_id.id,
                    'period_id': period.id,
                    'account_id': self.journal_id.default_credit_account_id.id,
                    'company_id': self.company_id.id,
                }))
                move_lines.append((0, 0, {
                    'debit': 0,
                    'credit': abs(reverse_debit - reverse_credit),
                    'name': self.description,
                    'date': period.date_stop,
                    'date_created': period.date_stop,
                    'journal_id': self.journal_id.id,
                    'period_id': period.id,
                    'account_id': self.journal_id.profit_account_id.id,
                    'company_id': self.company_id.id,
                }))
            if move_lines:
                move_id = obj_acc_move.create({
                    'journal_id': self.journal_id.id,
                    'date': period.date_stop,
                    'from_closing_period': True,
                    'state': 'posted',
                    'narration': u'%s мөчлөгийн хаалтын гүйлгээ.' % (period.name,),
                    'line_ids': move_lines
                })
                res.append(move_id)
        return res
