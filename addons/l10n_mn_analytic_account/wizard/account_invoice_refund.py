# -*- coding: utf-8 -*-
import time

from odoo import _, api, fields, models


class AccountInvoiceRefund(models.TransientModel):
    _inherit = "account.invoice.refund"
