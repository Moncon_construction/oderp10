# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class AccountCurrencyEqualizationAnalyticShare(models.TransientModel):
    _name = 'account.currency.equalization.analytic.share'

    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic account', required=True)
    rate = fields.Float('Rate %', required=True, default=100)
    account_currency_equalization_id = fields.Many2one('account.currency.equalization', 'Currency equalization')
