# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class AccountMoveLineReconcileWriteoffShare(models.TransientModel):
    _name = 'account.move.line.reconcile.writeoff.share'

    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic account', required=True)
    rate = fields.Float('Rate', default=100)
    write_off_id = fields.Many2one('account.move.line.reconcile.writeoff', 'Write off')
