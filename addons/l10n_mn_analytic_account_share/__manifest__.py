# -*- coding: utf-8 -*-
{
    'name': "Mongolian Analytic Share",
    'depends': [
        'l10n_mn_analytic',
        'l10n_mn_account',
        'l10n_mn_analytic_account',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Mongolian Analytic Share
    """,
    'data': [
        'views/account_bank_statement_view.xml',
        'views/account_bank_statement_line_view.xml',
        'views/account_invoice_line_view.xml',
        'views/account_invoice_view.xml',
        'views/account_move_line_view.xml',
        'views/account_move_view.xml',
    ],
}
