# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################

{
    'name': 'Mongolian Balance Account Analytic',
    'version': '1.0',
    'depends': [
        'l10n_mn_account',
        'l10n_mn_account_asset',
        'l10n_mn_analytic_account',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """Балансын дансан дээр шинжилгээний данс сонгодог болгоно""",
    'data': [
        'views/account_journal.xml',
        'views/account_asset_location_view.xml',
    ],
    "auto_install": False,
    "installable": True,
}
