# -*- coding: utf-8 -*-
from datetime import datetime

from odoo import _, api, models
from odoo.exceptions import UserError, ValidationError


class AccountAssetAsset(models.Model):
    _inherit = 'account.asset.asset'
    
    @api.onchange('location_id')
    def onchange_location_id(self):
        # Хөрөнгийн байрлал дээр шинжилгээний данс тохируулсан бол хөрөнгө дээр default-р олгох
        if self.location_id and self.location_id.account_analytic_id:
            self.account_analytic_id = self.location_id.account_analytic_id.id
        else:
            self.onchange_category()
            
    @api.multi
    def validate(self, context=None):
        # Хөрөнгийг батлах үед хөрөнгийн нийт үнэ болон эхний элэгдэлийн дүнгээр журналын бичилт үүснэ.
        # /Шинжилгээний дансыг хөрөнгөөс, хөрөнгөд байхгүй бол хөрөнгийн байрлалаас, байрлалд байхгүй бол хөрөнгийн ангилалаас авна. Хэрвээ 3 ууланд бхгүй байвал анхааруулга өгнө.
        res = super(AccountAssetAsset, self).validate(context)
        account_move_line_obj = self.env.get('account.move.line')
        for asset in self:
            if asset.account_analytic_id:
                account_analytic = asset.account_analytic_id
            elif asset.location_id and asset.location_id.account_analytic_id:
                account_analytic = asset.location_id.account_analytic_id
            elif asset.category_id.account_analytic_id:
                account_analytic = asset.category_id.account_analytic_id
            else:
                raise UserError(_('Please set a analytic account for the asset or asset location or asset category. Asset code: %s ' % asset.code))
            
            account_moves = account_move_line_obj.search([('asset_id', '=', asset.id)]) 
            if account_moves:
                for move in account_moves:
                    move.write({'analytic_account_id':account_analytic.id})
        return res
        
class AccountAssetDepreciationLine(models.Model):
    _inherit = 'account.asset.depreciation.line'
    
    @api.multi
    def create_move(self, post_move=True):
        # Элэгдэлийг батлаж, журналын бичилт үүсгэх/Шинжилгээний дансыг хөрөнгөөс, хөрөнгөд байхгүй бол хөрөнгийн байрлалаас, байрлалд байхгүй бол хөрөнгийн ангилалаас авна. Хэрвээ 3 ууланд бхгүй байвал анхааруулга өгнө./
        created_moves = super(AccountAssetDepreciationLine, self).create_move(post_move)
        account_move_line_obj = self.env.get('account.move.line')
        asset_depreciation_line_obj = self.env.get('account.asset.depreciation.line')
        if created_moves:
            for move_id in created_moves:
                account_depreciation_line = asset_depreciation_line_obj.search([('move_id', '=', move_id)]) 
                account_move_lines = account_move_line_obj.search([('move_id', '=', move_id)]) 
                if account_depreciation_line:
                    if account_depreciation_line.asset_id.account_analytic_id:
                        account_analytic = account_depreciation_line.asset_id.account_analytic_id
                    elif account_depreciation_line.asset_id.location_id and account_depreciation_line.asset_id.location_id.account_analytic_id:
                        account_analytic = account_depreciation_line.asset_id.location_id.account_analytic_id
                    elif account_depreciation_line.asset_id.category_id.account_analytic_id:
                        account_analytic = account_depreciation_line.asset_id.category_id.account_analytic_id
                    else:
                        raise UserError(_('Please set a analytic account for the asset or asset location or asset category. Asset code: %s ' % account_depreciation_line.asset_id.code))
                for line in account_move_lines:
                    line.write({'analytic_account_id':account_analytic.id})
        return created_moves
