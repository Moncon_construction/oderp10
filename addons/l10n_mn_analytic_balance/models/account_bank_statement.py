# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from datetime import date, datetime
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from odoo.exceptions import UserError, ValidationError
from odoo.tools import float_is_zero


class AccountBankStatementLine(models.Model):
    _inherit = "account.bank.statement.line"

    def _prepare_reconciliation_move_line(self, move, amount):
        res = super(AccountBankStatementLine, self)._prepare_reconciliation_move_line(move, amount)

        if not self.statement_id.journal_id.account_analytic_id:
            raise UserError(_('Analytic Account of this journal is not selected.'))

        res['analytic_account_id'] = self.statement_id.journal_id.account_analytic_id.id

        return res
