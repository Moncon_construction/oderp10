# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.exceptions import UserError, ValidationError


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.model
    def _default_account_analytic(self):
        if self._context.get('default_account_analytic_id', False):
            return self.env['account.analytic.account'].browse(self._context.get('default_account_analytic_id'))

    account_analytic_id = fields.Many2one('account.analytic.account', string='Analytic Account', default=_default_account_analytic)
