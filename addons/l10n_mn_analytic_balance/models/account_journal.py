# -*- coding: utf-8 -*-
# Part of OdERP. See LICENSE file for full copyright and licensing details.

from odoo import models, api, fields

class account_journal(models.Model):
    _inherit = 'account.journal'

    account_analytic_id = fields.Many2one('account.analytic.account', string='Analytic Account', ondelete='restrict')