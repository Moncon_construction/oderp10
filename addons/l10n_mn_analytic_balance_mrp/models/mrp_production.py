# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from odoo.exceptions import UserError


class MrpProduction(models.Model):
    _inherit = 'mrp.production'

    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic Account', required=False)
    cost_center = fields.Selection(related='company_id.cost_center', readonly=True)

    @api.multi
    def button_journal_entries(self):
        move_ids = self.env['account.move.line'].search([('stock_move_id', 'in', (self.move_raw_ids | self.move_finished_ids).ids or [])]).mapped('move_id')
        return {
            'name': _('Journal Entries'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', move_ids.ids)],
        }

    def _generate_finished_moves(self):
        move = super(MrpProduction, self)._generate_finished_moves()
        if self.analytic_account_id and move:
            move.analytic_account_id = self.analytic_account_id.id
            move.analytic_share_ids = [(0, 0, {'analytic_account_id': self.analytic_account_id.id, 'rate': 100})]
        return move

    def _generate_raw_move(self, bom_line, line_data):
        move = super(MrpProduction, self)._generate_raw_move(bom_line, line_data)
        if self.analytic_account_id and move:
            move.analytic_account_id = self.analytic_account_id.id
            move.analytic_share_ids = [(0, 0, {'analytic_account_id': self.analytic_account_id.id, 'rate': 100})]
        return move

    def _prepare_analytic_account(self, company_id, location_id, product_id):
        analytic_account_id = False
        if company_id.cost_center == 'warehouse':
            if location_id.get_warehouse().analytic_account_id:
                analytic_account_id = location_id.get_warehouse().analytic_account_id
            else:
                raise UserError(_('Please choose analytic account for the wareouse %s!') % location_id.get_warehouse().name)
        elif company_id.cost_center == 'brand':
            if product_id.brand_name:
                if product_id.brand_name.analytic_account_id:
                    analytic_account_id = product_id.brand_name.analytic_account_id
                else:
                    raise UserError(_('Please choose analytic account for the brand %s!') % product_id.brand_name.brand_name)
            else:
                raise UserError(_('Please choose brand for the product %s!') % product_id.name)
        elif company_id.cost_center == 'product_categ':
            if product_id.categ_id.analytic_account_id:
                analytic_account_id = product_id.categ_id.analytic_account_id
            else:
                raise UserError(_('Please choose analytic account for the product category %s!') % product_id.categ_id.name)
        elif company_id.cost_center in ('department', 'sales_team', 'project', 'technic', 'contract'):
            if company_id.default_analytic_account_id:
                analytic_account_id = company_id.default_analytic_account_id
            else:
                raise UserError(_('Please choose default analytic account for the company %s!') % company_id.name)
        return analytic_account_id
                
    @api.model
    def create(self, values):
        if 'analytic_account_id' not in values or not values['analytic_account_id']:
            company = self.env['res.company'].browse(values['company_id'])
            location_dest = self.env['stock.location'].browse(values['location_dest_id'])
            product = self.env['product.product'].browse(values['product_id'])
            analytic_account_id = self._prepare_analytic_account(company, location_dest, product)
            values['analytic_account_id'] = analytic_account_id.id or False
        production = super(MrpProduction, self).create(values)
        return production

    @api.onchange('analytic_account_id')
    def _onchange_analytic_account_id(self):
        if self.analytic_account_id:
            for move in self.move_raw_ids:
                move.write({'analytic_account_id': self.analytic_account_id.id})
                for share in move.analytic_share_ids:
                    share.write({'analytic_account_id': self.analytic_account_id.id})
            for move in self.move_finished_ids:
                move.write({'analytic_account_id': self.analytic_account_id.id})
                for share in move.analytic_share_ids:
                    share.write({'analytic_account_id': self.analytic_account_id.id})
                    
    @api.multi  
    def build_mrp_after_query_creation(self):
        # @Override: ШД-г оноохоор дахин тодорхойлов.
        mrp_ids = super(MrpProduction, self).build_mrp_after_query_creation()
        for mrp_id in mrp_ids:
            mrp_id.analytic_account_id = mrp_id._prepare_analytic_account(mrp_id.company_id, mrp_id.location_dest_id, mrp_id.product_id)
        self._cr.execute("""
            UPDATE stock_move sm SET analytic_account_id = mrp.analytic_account_id
            FROM mrp_production mrp
            WHERE (mrp.id = sm.raw_material_production_id OR mrp.id = production_id)
            AND mrp.id IN (%s)
        """ % ", ".join(str(mrp_id.id) for mrp_id in mrp_ids))
        return mrp_ids
    
