# -*- coding: utf-8 -*-
{
    'name': "Mongolian Balance Analytic - Payroll",
    'version': '1.0',
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'depends': [
        'l10n_mn_hr_payroll', 
        'l10n_mn_analytic',
    ],
    'category': 'Mongolian Modules',
    'description': """
        Дараах боломжуудыг олгоно:
           * Цалингийн хуудасны багц батлахад үүсэх журнал бичилт дээр шинжилгээний данс автомат сонгогдох
    """,
    'data': [],
    'application': True,
    'installable': True,
}
