# -*- coding: utf-8 -*-

import time
from datetime import datetime, timedelta

from odoo import _, api, exceptions, fields, models, tools
from odoo.exceptions import UserError


class HrPayslipRun(models.Model):
    _inherit = 'hr.payslip.run'
    
    def _get_analytic_account_qry(self, no_payslip_part=False):
        self.ensure_one()
        
        if self.company_id.cost_center == 'department':
            analytic_select_qry = ", hd.analytic_account_id AS analytic_account"
            analytic_from_qry = ", hr_payslip ps, hr_department hd" if not no_payslip_part else ", hr_department hd"
            analytic_where_qry = " AND pl.slip_id = ps.id AND ps.department_id = hd.id" if not no_payslip_part else " AND ps.department_id = hd.id"
            analytic_group_qry = ", hd.analytic_account_id"
        else:
            analytic_select_qry = ", sr.analytic_account_id AS analytic_account"
            analytic_from_qry = ""
            analytic_where_qry = ""
            analytic_group_qry = ", sr.analytic_account_id"
            
        return analytic_select_qry, analytic_from_qry, analytic_where_qry, analytic_group_qry
        
    def _get_advance_last_slry_credit_qry_with_acc_not_in(self, acc_ids, create_move_in_advance=False):
        # @Override: УРЬДЧИЛГАА/СҮҮЛ төрөлтэй цалингийн КТ талын бичилтийн өгөгдлийг авах query /Шинжилгээний дансыг өртгийн төвөөс хамааруулан автомат сонгоно./
        # Цалингийн бодолтын мөрүүдийг Кредит талдаа дансаар нь бүлэглэж үүсгэнэ, Цалин өглөг, ХХОАТ өглөг, НДШ өглөг гэх мэт
        self.ensure_one()
        advance_where = " AND sr.create_move_in_advance = TRUE" if create_move_in_advance else ""
        analytic_select_qry, analytic_from_qry, analytic_where_qry, analytic_group_qry = self._get_analytic_account_qry()
            
        return '''
                SELECT sr.account_credit AS account, SUM(pl.total) AS total, MIN(sr.transaction_description) AS dscrtpn %s
                FROM hr_payslip_line pl, hr_salary_rule sr %s
                WHERE pl.slip_id in (SELECT id FROM hr_payslip WHERE payslip_run_id = %s)
                AND pl.salary_rule_id = sr.id AND pl.total <> 0 AND sr.account_credit <> 0 AND sr.account_credit NOT IN %s %s %s
                GROUP BY sr.account_credit %s
                ORDER BY sr.account_credit''' % (analytic_select_qry, analytic_from_qry, self.id, acc_ids, advance_where, analytic_where_qry, analytic_group_qry)
                
    def _get_advance_last_slry_credit_qry_with_acc_in(self, acc_ids, create_move_in_advance=False):
        # @Override:  УРЬДЧИЛГАА/СҮҮЛ төрөлтэй цалингийн КТ талын бичилтийн өгөгдлийг авах query /Шинжилгээний дансыг өртгийн төвөөс хамааруулан автомат сонгоно./
        # Цалингийн бодолтын мөрүүдийг Кредит талдаа тусдаа тусдаа бичилт үүсгэнэ, Ажилчдаас авах авлага гэх мэт
        self.ensure_one()
        advance_where = " AND sr.create_move_in_advance = TRUE" if create_move_in_advance else ""
        analytic_select_qry, analytic_from_qry, analytic_where_qry, analytic_group_qry = self._get_analytic_account_qry(no_payslip_part=True)
        
        return '''
                SELECT ps.employee_id AS emp, sr.account_credit AS account, pl.total AS total, sr.transaction_description AS dscrtpn %s
                FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip ps %s
                WHERE pl.salary_rule_id = sr.id AND pl.total <> 0 AND sr.account_credit <> 0 AND ps.payslip_run_id = %s AND sr.account_credit IN %s AND pl.slip_id = ps.id %s %s
                ORDER BY ps.employee_id ASC, sr.account_credit ASC''' % (analytic_select_qry, analytic_from_qry, self.id, acc_ids, advance_where, analytic_where_qry)
                
    def _get_advance_last_slry_debit_qry_with_acc_not_in_without_expense(self, acc_ids):
        # @Override:  УРЬДЧИЛГАА/СҮҮЛ төрөлтэй цалингийн ДТ талын бичилтийн өгөгдлийг авах query /Шинжилгээний дансыг өртгийн төвөөс хамааруулан автомат сонгоно./
        # Цалингийн бодолтын мөрүүдийг Дебит талдаа дансаар нь бүлэглэж үүсгэнэ, Цалин өглөг, Цалин зардал, НДШ зардал гэх мэт
        self.ensure_one()
        analytic_select_qry, analytic_from_qry, analytic_where_qry, analytic_group_qry = self._get_analytic_account_qry()
            
        return '''
                SELECT sr.account_debit AS account, SUM(pl.total) AS total, aa.name, MIN(sr.transaction_description) AS dscrtpn %s
                FROM hr_payslip_line pl, hr_salary_rule sr, account_account aa, account_account_type at %s
                WHERE pl.slip_id in (SELECT id FROM hr_payslip WHERE payslip_run_id = %s) AND aa.id = sr.account_debit AND aa.user_type_id = at.id AND at.type <> 'expense'
                AND pl.salary_rule_id = sr.id AND pl.total <> 0 AND sr.account_debit <> 0 AND sr.account_debit NOT IN %s %s
                GROUP BY sr.account_debit, aa.name %s
                ORDER BY sr.account_debit''' % (analytic_select_qry, analytic_from_qry, self.id, acc_ids, analytic_where_qry, analytic_group_qry)
                
    def _get_advance_last_slry_debit_qry_with_acc_not_in(self, acc_ids, create_move_in_advance=False):
        # @Override:  УРЬДЧИЛГАА/СҮҮЛ төрөлтэй цалингийн ДТ талын бичилтийн өгөгдлийг авах query /Шинжилгээний дансыг өртгийн төвөөс хамааруулан автомат сонгоно./
        self.ensure_one()
        advance_where = " AND sr.create_move_in_advance = TRUE" if create_move_in_advance else ""
        analytic_select_qry, analytic_from_qry, analytic_where_qry, analytic_group_qry = self._get_analytic_account_qry()
            
        return '''
                SELECT sr.account_debit AS account, SUM(pl.total) AS total, MIN(sr.transaction_description) AS dscrtpn %s
                FROM hr_payslip_line pl, hr_salary_rule sr %s
                WHERE pl.slip_id in (SELECT id FROM hr_payslip WHERE payslip_run_id = %s)
                AND pl.salary_rule_id = sr.id AND pl.total <> 0 AND sr.account_debit <> 0 AND sr.account_debit NOT IN %s %s %s
                GROUP BY sr.account_debit %s
                ORDER BY sr.account_debit''' % (analytic_select_qry, analytic_from_qry, self.id, acc_ids, advance_where, analytic_where_qry, analytic_group_qry)
                
    def _get_last_slry_debit_qry_by_each_emp(self, acc_ids):
        # СҮҮЛ цалингийн 'expense' төрөлтэй данс бүхий ДТ талын бичилтүүдэд шинжилгээний данс олгох
        self.ensure_one()
        analytic_select_qry, analytic_from_qry, analytic_where_qry, analytic_group_qry = self._get_analytic_account_qry(no_payslip_part=True)
        
        return '''
                SELECT he.id AS emp, sr.account_debit AS account, SUM(pl.total) AS total, pl.id, MIN(sr.transaction_description) AS dscrtpn %s
                FROM hr_payslip_line pl, hr_salary_rule sr, account_account aa, account_account_type at, hr_payslip ps, hr_employee he %s
                WHERE pl.slip_id IN (SELECT id FROM hr_payslip WHERE payslip_run_id = %s) AND aa.id = sr.account_debit AND aa.user_type_id = at.id
                AND at.type = 'expense' AND pl.salary_rule_id = sr.id AND pl.total <> 0 AND sr.account_debit <> 0
                AND sr.account_debit NOT IN %s AND pl.slip_id = ps.id AND he.id = ps.employee_id %s
                GROUP BY pl.id, sr.account_debit, he.id %s
        ''' % (analytic_select_qry, analytic_from_qry, self.id, acc_ids, analytic_where_qry, analytic_group_qry)
        
    def _get_advance_last_slry_debit_qry_with_acc_in(self, acc_ids, create_move_in_advance=False):
        # @Override:  УРЬДЧИЛГАА/СҮҮЛ төрөлтэй цалингийн КТ талын бичилтийн өгөгдлийг авах query /Шинжилгээний дансыг өртгийн төвөөс хамааруулан автомат сонгоно./
        # Цалингийн бодолтын мөрүүдийг Дебит талдаа тусдаа тусдаа бичилт үүсгэнэ,
        self.ensure_one()
        advance_where = " AND sr.create_move_in_advance = TRUE" if create_move_in_advance else ""
        analytic_select_qry, analytic_from_qry, analytic_where_qry, analytic_group_qry = self._get_analytic_account_qry(no_payslip_part=True)
        
        return '''
                SELECT ps.employee_id AS emp, sr.account_debit AS account, pl.total AS total, sr.transaction_description AS dscrtpn %s
                FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip ps %s
                WHERE pl.salary_rule_id = sr.id AND pl.total <> 0 AND sr.account_debit <> 0 AND ps.payslip_run_id = %s AND sr.account_debit IN %s AND pl.slip_id = ps.id %s %s
                ORDER BY ps.employee_id ASC, sr.account_debit ASC''' % (analytic_select_qry, analytic_from_qry, self.id, acc_ids, advance_where, analytic_where_qry)
                
    def _get_maternity_hchta_slry_credit_qry(self):
        # @Override:  ЖАТ/ХЧТАТ төрөлтэй цалингийн КТ талын бичилтийн өгөгдлийг авах query /Шинжилгээний дансыг өртгийн төвөөс хамааруулан автомат сонгоно./
        # Цалингийн бодолтын мөрүүдийг Кредит талдаа тусдаа тусдаа бичилт үүсгэнэ, Ажилчдаас авах авлага гэх мэт
        self.ensure_one()
        analytic_select_qry, analytic_from_qry, analytic_where_qry, analytic_group_qry = self._get_analytic_account_qry(no_payslip_part=True)

        return '''
                SELECT ps.employee_id AS emp, sr.account_credit AS account, SUM(pl.total) AS total, MIN(sr.transaction_description) AS dscrtpn %s 
                FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip ps %s
                WHERE pl.salary_rule_id = sr.id AND pl.total <> 0 AND sr.account_credit <> 0 AND ps.payslip_run_id = %s AND pl.slip_id = ps.id %s
                GROUP BY ps.employee_id, sr.account_credit %s
                ORDER BY ps.employee_id ASC, sr.account_credit ASC''' % (analytic_select_qry, analytic_from_qry, self.id, analytic_where_qry, analytic_group_qry)
                
    def _get_maternity_slry_debit_qry_without_expense(self):
        # @Override:  ЖАТ төрөлтэй цалингийн ДТ талын бичилтийн өгөгдлийг авах query /Шинжилгээний дансыг өртгийн төвөөс хамааруулан автомат сонгоно./
        # Цалингийн бодолтын мөрүүдийг Дебит талдаа дансаар нь бүлэглэж үүсгэнэ, Цалин өглөг, Цалин зардал, НДШ зардал гэх мэт
        self.ensure_one()
        analytic_select_qry, analytic_from_qry, analytic_where_qry, analytic_group_qry = self._get_analytic_account_qry()
        return '''
                SELECT sr.account_debit AS account, SUM(pl.total) AS total, MIN(sr.transaction_description) AS dscrtpn %s
                FROM hr_payslip_line pl, hr_salary_rule sr, account_account aa, account_account_type at %s
                WHERE pl.slip_id in (SELECT id FROM hr_payslip WHERE payslip_run_id = %s) AND aa.id = sr.account_debit AND aa.user_type_id = at.id AND at.type <> 'expense'
                AND pl.salary_rule_id = sr.id AND pl.total <> 0 AND sr.account_debit <> 0 %s
                GROUP BY sr.account_debit %s
                ORDER BY sr.account_debit''' % (analytic_select_qry, analytic_from_qry, self.id, analytic_where_qry, analytic_group_qry)
    
    def _get_hchta_slry_credit_qry(self, acc_ids):
        # @Override:  ХЧТАТ төрөлтэй цалингийн КТ талын бичилтийн өгөгдлийг авах query /Шинжилгээний дансыг өртгийн төвөөс хамааруулан автомат сонгоно./
        self.ensure_one()
        analytic_select_qry, analytic_from_qry, analytic_where_qry, analytic_group_qry = self._get_analytic_account_qry(no_payslip_part=True)

        return '''
                SELECT ps.employee_id AS emp, sr.account_credit AS account, SUM(pl.total) AS total, MIN(sr.transaction_description) AS dscrtpn %s 
                FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip ps %s
                WHERE pl.salary_rule_id = sr.id AND pl.total <> 0 AND sr.account_credit <> 0 AND ps.payslip_run_id = %s AND sr.account_credit in %s AND pl.slip_id = ps.id %s
                GROUP BY ps.employee_id, sr.account_credit %s
                ORDER BY ps.employee_id ASC, sr.account_credit ASC''' % (analytic_select_qry, analytic_from_qry, self.id, acc_ids, analytic_where_qry, analytic_group_qry)         
       
    def _get_hchta_slry_debit_qry(self, acc_ids):
        # @Override:  ХЧТАТ төрөлтэй цалингийн ДТ талын бичилтийн өгөгдлийг авах query /Шинжилгээний дансыг өртгийн төвөөс хамааруулан автомат сонгоно./
        self.ensure_one()
        analytic_select_qry, analytic_from_qry, analytic_where_qry, analytic_group_qry = self._get_analytic_account_qry()
        
        return '''
                SELECT sr.account_debit AS account, SUM(pl.total) AS total, MIN(sr.transaction_description) AS dscrtpn %s
                FROM hr_payslip_line pl, hr_salary_rule sr %s
                WHERE pl.slip_id in (SELECT id FROM hr_payslip WHERE payslip_run_id = %s) AND sr.account_debit NOT IN %s 
                AND pl.salary_rule_id = sr.id AND pl.total <> 0 AND sr.account_debit <> 0 %s
                GROUP BY sr.account_debit %s
                ORDER BY sr.account_debit''' % (analytic_select_qry, analytic_from_qry, self.id, acc_ids, analytic_where_qry, analytic_group_qry)
                
    def _get_hchta_slry_debit_qry_without_expense(self, acc_ids):
        # @Override:  ХЧТАТ төрөлтэй цалингийн ДТ талын бичилтийн өгөгдлийг авах query /Шинжилгээний дансыг өртгийн төвөөс хамааруулан автомат сонгоно./
        # Цалингийн бодолтын мөрүүдийг Дебит талдаа дансаар нь бүлэглэж үүсгэнэ, Цалин өглөг, Цалин зардал, НДШ зардал гэх мэт
        self.ensure_one()
        analytic_select_qry, analytic_from_qry, analytic_where_qry, analytic_group_qry = self._get_analytic_account_qry()
        
        return '''
                SELECT sr.account_debit AS account, SUM(pl.total) AS total, MIN(sr.transaction_description) AS dscrtpn %s
                FROM hr_payslip_line pl, hr_salary_rule sr, account_account aa, account_account_type at %s
                WHERE pl.slip_id in (SELECT id FROM hr_payslip WHERE payslip_run_id = %s) AND aa.id = sr.account_debit AND aa.user_type_id = at.id AND at.type <> 'expense'
                AND pl.salary_rule_id = sr.id AND pl.total <> 0 AND sr.account_debit <> 0 AND sr.account_debit NOT IN %s %s
                GROUP BY sr.account_debit %s
                ORDER BY sr.account_debit''' % (analytic_select_qry, analytic_from_qry, self.id, acc_ids, analytic_where_qry, analytic_group_qry)
                
                
