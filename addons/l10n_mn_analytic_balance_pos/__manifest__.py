# -*- coding: utf-8 -*-
{
    'name': "Mongolian Balance Analytic - POS",
    'version': '1.0',
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'depends': [
        'l10n_mn_point_of_sale_analytic',
    ],
    'category': 'Mongolian Modules',
    'description': """
        Дараах боломжуудыг олгоно:
           * ПОС-ын гүйлгээ хаахад балансын данс дээр шинжилгээний данс автомат сонгогдох
    """,
    'data': [],
    'application': True,
    'installable': True,
}
