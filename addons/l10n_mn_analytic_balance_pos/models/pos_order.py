# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _


class PosOrder(models.Model):
    _inherit = "pos.order"

    def get_group_key(self, data_type, values):
        key = False
        if data_type == 'product':
            key = ('product', values['partner_id'], (values['product_id'], tuple(values['tax_ids'][0][2]), values['name']), values['debit'] > 0, (values['analytic_account_id'] if 'analytic_account_id' in values.keys() else False))
        elif data_type == 'tax':
            key = ('tax', values['partner_id'], values['tax_line_id'], values['debit'] > 0, (values['analytic_account_id'] if 'analytic_account_id' in values.keys() else False))
        elif data_type == 'counter_part':
            key = ('counter_part', values['partner_id'], values['account_id'], values['debit'] > 0, (values['analytic_account_id'] if 'analytic_account_id' in values.keys() else False))
        return key
    
    def insert_balance_data(self, data_type, order, line, move, grouped_data, values):
        if data_type in ('product', 'tax'):
            if data_type == 'tax':
                values.update({'analytic_account_id': self._prepare_analytic_account(line)})
            grouped_data = self.insert_data(data_type, order, move, grouped_data, values)
            
            IrProperty = self.env['ir.property']
            ResPartner = self.env['res.partner']
            account_def = IrProperty.get('property_account_receivable_id', 'res.partner')
            order_account = order.partner_id.property_account_receivable_id.id or account_def and account_def.id
            partner_id = ResPartner._find_accounting_partner(order.partner_id).id or False
            # counterpart
            grouped_data = self.insert_data('counter_part', order, move, grouped_data, {
                'name': _("Trade Receivables"),  # order.name,
                'account_id': order_account,
                'analytic_account_id': values['analytic_account_id'] if 'analytic_account_id' in values.keys() else False,
                'credit': values['debit'] or 0.0,
                'debit': values['credit'] or 0.0,
                'partner_id': partner_id
            })
        elif data_type == 'counter_part':
            pass
        
        return grouped_data
        
    def create_bank_statement_lines(self, args):
        """
            @note: ПОС-ын төлбөр төлөгдөхөд үүсдэг хуулгын мөр дээрх ШД-г автомат онооно.
                   Компаны өртгийн төв нь:
                       1. "Барааны ангилал", "Барааны бренд" бол төлбөрийн дүнг нийт захиалгын дүнтэй харьцуулж ангилал, бренд бүрээр салгаж хуулгын мөрийг үүсгэнэ.
                       2. Дээрхээс бусад үед сэшний тохиргоон дахь ШД-г авч 1 л хуулга үүснэ.
        """
        precision = self.env['decimal.precision'].precision_get('Account') or 2
        context = dict(self.env.context)
        context.pop('pos_session_id', False)
        
        datas = {}
        config_analytic = self.session_id.config_id.analytic_account_id.id or False
        datas[config_analytic] = args
        
        if self.company_id.cost_center in ('product_categ', 'brand'):
            total_amount = self.amount_total
            cash_amount = datas[config_analytic]['amount']
            if total_amount:
                per_unit = cash_amount / total_amount
                datas[config_analytic]['amount'] = 0
                for line_id in self.lines:
                    if self.company_id.cost_center == 'product_categ' and line_id.product_id.categ_id and line_id.product_id.categ_id.analytic_account_id:
                        if datas.get(line_id.product_id.categ_id.analytic_account_id.id, False):
                            datas[line_id.product_id.categ_id.analytic_account_id.id]['amount'] += per_unit * line_id.price_unit
                        else:
                            new_dict_by_analytic_account = args.copy()
                            new_dict_by_analytic_account['amount'] = per_unit * line_id.price_unit
                            datas[line_id.product_id.categ_id.analytic_account_id.id] = new_dict_by_analytic_account
                    elif self.company_id.cost_center == 'brand' and line_id.product_id.brand_name and line_id.product_id.brand_name.analytic_account_id:
                        if datas.get(line_id.product_id.brand_name.analytic_account_id.id, False):
                            datas[line_id.product_id.brand_name.analytic_account_id.id]['amount'] += per_unit * line_id.price_unit
                        else:
                            new_dict_by_analytic_account = args.copy()
                            new_dict_by_analytic_account['amount'] = per_unit * line_id.price_unit
                            datas[line_id.product_id.brand_name.analytic_account_id.id] = new_dict_by_analytic_account
                    else:
                        datas[config_analytic]['amount'] += per_unit * line_id.price_unit
                  
            # Оронгийн нарийвчлал зэргээс хамаараад төлбөрийн дүнгээс зөрсөн дүнг үндсэн хуулгын мөрт шингээх      
            allocated_amounts = 0
            for key in datas.keys():
                allocated_amounts += datas[key]['amount']
            
            amount_diff = round(cash_amount - allocated_amounts, precision)
            if amount_diff != 0:
                datas[config_analytic]['amount'] += amount_diff 
                    
        for key in datas.keys():
            # ШД-г оноож хуулга үүсгэнэ.
            data = datas[key]
            if round(data['amount'], precision) == 0:
                continue
            if key:
                data['analytic_account_id'] = key
            self.env['account.bank.statement.line'].with_context(context).create(data)
         
    def add_payment(self, data):
        """
            @Override: ПОС-ын төлбөр төлөгдөхөд үүсдэг хуулгын мөр дээрх ШД-г автомат оноохын тулд override хийв.
            TODO: pos_mercury модулийг суулгах шаардлага гарахгүй гэж үзээд override хийв. Суулгах шаардлага гарвал тэр үед нь линкер модуль үүсгэх
        """
        args = self._prepare_bank_statement_line_payment_values(data)
        self.create_bank_statement_lines(args)
        return args.get('statement_id', False)
    
    
