# -*- coding: utf-8 -*-
{
    'name': "Mongolian Balance Analytic - POS Loyalty Card",
    'version': '1.0',
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'depends': [
        'l10n_mn_analytic_balance_pos',
        'l10n_mn_pos_loyalty_card',
    ],
    'category': 'Mongolian Modules',
    'description': """
        l10n_mn_analytic_balance_pos, l10n_mn_pos_loyalty_card модулиуд зэрэг суусан үед дундын функцүүдийн хэвийн ажиллагааг хангах зорилготой холбогч модуль.
    """,
    'data': [],
    'application': True,
    'installable': True,
    'auto_install': True
}
