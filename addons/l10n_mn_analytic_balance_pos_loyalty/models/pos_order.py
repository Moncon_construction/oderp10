# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _


class PosOrder(models.Model):
    _inherit = "pos.order"

    def get_group_key(self, data_type, values):
        # @Override: 'l10n_mn_pos_loyalty_card' модульд тодорхойлсон функцаар ажиллана.
        key = False
        if data_type == 'product':
            key = ('product', values['partner_id'],
                   (values['product_id'], tuple(values['tax_ids'][0][2]), values['name']),
                   values['analytic_account_id'], values['debit'] > 0)
        elif data_type == 'tax':
            key = ('tax', values['partner_id'], values['tax_line_id'], values['debit'] > 0)
        elif data_type == 'counter_part':
            key = ('counter_part', values['partner_id'], values['account_id'], values['debit'] > 0)
        elif data_type == 'loyalty_credit':
            key = ('loyalty_credit', values['partner_id'], values['account_id'], values['credit'] > 0)
        elif data_type == 'loyalty_debit':
            key = ('loyalty_debit', values['partner_id'], values['account_id'], values['debit'] > 0)
        return key
    
    def insert_balance_data(self, data_type, order, line, move, grouped_data, values):
        # @Override: 'l10n_mn_pos_loyalty_card' модуль суусан бол 'l10n_mn_analytic_balance_pos' модулийн функцыг дуудахгүй
        grouped_data = self.insert_data(data_type, order, move, grouped_data, values)
        return grouped_data
