# -*- coding: utf-8 -*-
##############################################################################
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################
{
    'name': 'Mongolian Balance Analytic - Purchase',
    'version': '1.0',
    'depends': [
        'l10n_mn_analytic_balance',
        'l10n_mn_purchase',
        'l10n_mn_analytic_balance_stock',
        'l10n_mn_analytic_account',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """Балансын данс дээр шинжилгээний данс сонгох - Худалдан авалтын модуль""",
    'data': [
        'views/purchase_order_view.xml'
    ],
    "auto_install": False,
    "installable": True,
}
