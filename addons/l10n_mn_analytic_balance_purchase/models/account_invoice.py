# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.model
    def create(self, values):
        if 'invoice_line_ids' in values and values['invoice_line_ids']:
            for line in values['invoice_line_ids']:
                if 'analytic_share_ids' in line[2] and len(line[2]['analytic_share_ids']) == 1 and line[2]['analytic_share_ids'][0] == [5] and 'account_analytic_id' in line[2] and line[2]['account_analytic_id']:
                    line[2].update({'analytic_share_ids': [(0, 0, {'analytic_account_id': line[2]['account_analytic_id'], 'rate': 100})]})
        return super(AccountInvoice, self).create(values)
