# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.tools.translate import _
from odoo.addons.l10n_mn_extra_cost_item.models.extra_cost_item import SPLIT_METHOD_EC as ec_item  # @UnresolvedImport
from odoo.exceptions import ValidationError


class PurchaseExtraCost(models.Model):
    _inherit = 'purchase.extra.cost'
    
    analytic_share_ids = fields.One2many('account.analytic.share', 'purchase_extra_cost_id', 'Analytic Share', ondelete='restrict')