# -*- coding: utf-8 -*-
from datetime import datetime
from mimetools import choose_boundary

from dateutil.relativedelta import relativedelta

from odoo import SUPERUSER_ID, _, api, fields, models
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    @api.model
    def _default_account_analytic_id(self):
        if self.env.user.company_id.cost_center in ('department', 'sales_team', 'technic'):
            if self.env.user.company_id.default_analytic_account_id:
                return self.env.user.company_id.default_analytic_account_id.id
            else:
                raise UserError(_('Please choose default analytic account for the company %s!') % self.env.user.company_id.name)

    account_analytic_id = fields.Many2one('account.analytic.account', string='Analytic Account', default=_default_account_analytic_id)
    cost_center = fields.Selection(related='company_id.cost_center', readonly=True)

    @api.onchange('account_analytic_id')
    def _onchange_account_analytic_id(self):
        for line in self.order_line:
            line.account_analytic_id = self.account_analytic_id.id

    @api.onchange('warehouse_id')
    def _onchange_warehouse_id(self):
        if self.warehouse_id and self.company_id.cost_center == 'warehouse':
            if self.warehouse_id.analytic_account_id:
                self.account_analytic_id = self.warehouse_id.analytic_account_id.id
            else:
                raise UserError(_('Please choose analytic account for the warehouse %s!') % self.warehouse_id.name)

    @api.multi
    def action_view_invoice(self):
        result = super(PurchaseOrder, self).action_view_invoice()
        result['context'].update({'default_account_analytic_id': self.account_analytic_id.id})
        return result

    @api.model
    def create(self, values):
        if 'contract_id' in values and values['contract_id'] and 'contract.management' in self.env and self.env.user.company_id.cost_center == 'contract':
            analytic_account_id = self.env['contract.management'].browse(values['contract_id']).analytic_account_id.id
            values['account_analytic_id'] = analytic_account_id
            if 'order_line' in values and values['order_line']:
                for line in values['order_line']:
                    if 'account_analytic_id' not in line[2]:
                        line[2]['account_analytic_id'] = analytic_account_id
        return super(PurchaseOrder, self).create(values)
