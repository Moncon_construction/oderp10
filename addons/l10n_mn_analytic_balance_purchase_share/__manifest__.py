# -*- coding: utf-8 -*-
{
    'name': 'Mongolian Balance Purchase Analytic Share',
    'version': '1.0',
    'depends': [
        'l10n_mn_analytic_balance_purchase',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """Mongolian Balance Stock Analytic Share""",
    'data': [
        'views/purchase_extra_cost_view.xml',
        'views/purchase_shipment_view.xml'
    ],
    "auto_install": False,
    "installable": True,
}
