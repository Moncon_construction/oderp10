# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.tools.translate import _
from odoo.addons.l10n_mn_extra_cost_item.models.extra_cost_item import SPLIT_METHOD_EC as ec_item  # @UnresolvedImport
from odoo.exceptions import ValidationError, UserError


class PurchaseExtraCost(models.Model):
    _inherit = 'purchase.extra.cost'
    
    @api.multi
    def make_invoice(self):
        res = super(PurchaseExtraCost, self).make_invoice()
        for extra in self:
            for line in extra.invoice_id.invoice_line_ids:
                line.write({'analytic_share_ids': [(0, 0, {'analytic_account_id': share.analytic_account_id.id, 'rate': share.rate}) for share in extra.analytic_share_ids]})
        return res
    
    @api.multi
    def calculate_shipment_cost(self):
        res = super(PurchaseExtraCost, self).calculate_shipment_cost()
        share_obj = self.env['account.analytic.share']
        for order in self:
            if order.invoice_id:
                raise UserError(_('Please unlink invoice before you cost calculate.'))
            else:
                analytic_shares = []
                analytic_share_dict = {}
                for cost_line in order.adjustment_lines:
                    if not cost_line.purchase_line_id.account_analytic_id:
                        raise UserError(_('There is no analytic account in purchase order line %s.') % cost_line.purchase_line_id.name)
                    if not cost_line.purchase_line_id.analytic_2nd_account_id:
                        raise UserError(_('There is no analytic #2 account in purchase order line %s.') % cost_line.purchase_line_id.name)
                    if cost_line.purchase_line_id.account_analytic_id.id in analytic_shares:
                        analytic_share_dict[cost_line.purchase_line_id.account_analytic_id.id] += cost_line.total_cost/order.amount*100
                    else:
                        analytic_share_dict[cost_line.purchase_line_id.account_analytic_id.id] = cost_line.total_cost/order.amount*100
                        analytic_shares.append(cost_line.purchase_line_id.account_analytic_id.id)
                if order.analytic_share_ids:
                    order.analytic_share_ids.unlink()
                    for key, value in analytic_share_dict.iteritems():
                        share_obj.create({'analytic_account_id': key, 'rate': value, 'purchase_extra_cost_id': order.id})
                else:
                    for key, value in analytic_share_dict.iteritems():
                        share_obj.create({'analytic_account_id': key, 'rate': value, 'purchase_extra_cost_id': order.id})
        return res