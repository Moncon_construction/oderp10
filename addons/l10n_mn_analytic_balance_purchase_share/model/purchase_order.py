# -*- coding: utf-8 -*-
from datetime import datetime
from mimetools import choose_boundary

from dateutil.relativedelta import relativedelta

from odoo import SUPERUSER_ID, _, api, fields, models
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'
    
    @api.multi
    def button_compute_extra_cost(self):
        res = super(PurchaseOrder, self).button_compute_extra_cost()
        share_obj = self.env['account.analytic.share']
        for order in self:
            for cost in order.extra_cost_ids:
                if cost.invoice_id:
                    raise UserError(_('Please unlink invoice before you cost calculate.'))
                else:
                    analytic_shares = []
                    analytic_share_dict = {}
                    for cost_line in cost.adjustment_lines:
                        if not cost_line.purchase_line_id.account_analytic_id:
                            raise UserError(_('There is no analytic account in purchase order line %s.') % cost_line.purchase_line_id.name)
                        if not cost_line.purchase_line_id.analytic_2nd_account_id:
                            raise UserError(_('There is no analytic #2 account in purchase order line %s.') % cost_line.purchase_line_id.name)
                        if cost_line.purchase_line_id.account_analytic_id.id in analytic_shares:
                            analytic_share_dict[cost_line.purchase_line_id.account_analytic_id.id] += cost_line.total_cost/cost.amount*100
                        else:
                            analytic_share_dict[cost_line.purchase_line_id.account_analytic_id.id] = cost_line.total_cost/cost.amount*100
                            analytic_shares.append(cost_line.purchase_line_id.account_analytic_id.id)
                    if cost.analytic_share_ids:
                        cost.analytic_share_ids.unlink()
                        for key, value in analytic_share_dict.iteritems():
                            share_obj.create({'analytic_account_id': key, 'rate': value, 'purchase_extra_cost_id': cost.id})
                    else:
                        for key, value in analytic_share_dict.iteritems():
                            share_obj.create({'analytic_account_id': key, 'rate': value, 'purchase_extra_cost_id': cost.id})
        return res
