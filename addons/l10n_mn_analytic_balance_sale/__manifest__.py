# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################

{
    'name': 'Mongolian Balance Analytic - Sales',
    'version': '1.0',
    'depends': [
        'l10n_mn_analytic_account',
        'l10n_mn_analytic_balance_stock',
        'l10n_mn_sale',
        'l10n_mn_sale_stock',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """Борлуулалтын модулиас үүсдэг журналын бичилтийн балансын данс дээр шинжилгээний данс автомат сонгогддог болгоно""",
    'data': [
        'views/sale_order_view.xml',
        'views/crm_team_view.xml',
    ],
    "auto_install": False,
    "installable": True,
}
