# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from odoo.exceptions import UserError


class AccountAnalyticShare(models.Model):
    _inherit = 'account.analytic.share'

    sale_order_line_id = fields.Many2one('sale.order.line', 'Sale order Line', readonly=True)

    _sql_constraints = [
        ('unique_for_sale_order_line', 'unique(analytic_account_id, sale_order_line_id)', 'Analytic shares must be unique!'),
    ]
