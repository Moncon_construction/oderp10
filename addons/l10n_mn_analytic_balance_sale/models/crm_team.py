# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class CrmTeam(models.Model):
    _inherit = "crm.team"

    company_id = fields.Many2one(required=True)
    cost_center = fields.Selection(related='company_id.cost_center', readonly=True)
