# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class ProcurementOrder(models.Model):
    _inherit = "procurement.order"

    def _get_stock_move_values(self):
        stock_move = super(ProcurementOrder, self)._get_stock_move_values()
        if self.sale_line_id:
            if self.sale_line_id.analytic_account_id:
                stock_move.update({
                    'analytic_account_id': self.sale_line_id.analytic_account_id.id})
            if self.sale_line_id.analytic_share_ids:
                stock_move.update({
                    'analytic_share_ids': [(0, 0, {'analytic_account_id': share.analytic_account_id.id, 'rate': share.rate}) for share in self.sale_line_id.analytic_share_ids]})
        return stock_move
