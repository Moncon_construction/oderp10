# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from odoo.exceptions import UserError


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    cost_center = fields.Selection(related='company_id.cost_center', readonly=True)

    @api.onchange('project_id')
    def _onchange_project_id(self):
        if self.order_line:
            for line in self.order_line:
                line.analytic_account_id = self.project_id

    @api.onchange('user_id')
    def _onchange_user_id(self):
        if self.company_id and self.company_id.cost_center == 'department':
            if self.user_id.department_id and self.user_id:
                if self.user_id.department_id.analytic_account_id:
                    self.project_id = self.user_id.department_id.analytic_account_id.id
                    if self.order_line:
                        for line in self.order_line:
                            line.analytic_account_id = self.project_id
                else:
                    raise UserError(_('Please choose analytic account for the department %s!') % self.user_id.department_id.name)
            else:
                self.project_id = False
                if self.order_line:
                    for line in self.order_line:
                        line.analytic_account_id = self.project_id

    @api.onchange('warehouse_id')
    def _onchange_warehouse_id(self):
        super(SaleOrder, self)._onchange_warehouse_id()
        if self.company_id and self.company_id.cost_center == 'warehouse':
            if self.warehouse_id:
                if self.warehouse_id.analytic_account_id:
                    self.project_id = self.warehouse_id.analytic_account_id.id
                    if self.order_line:
                        for line in self.order_line:
                            line.analytic_account_id = self.project_id
                else:
                    raise UserError(_('Please choose analytic account for the warehouse %s!') % self.warehouse_id.name)
            else:
                self.project_id = False
                if self.order_line:
                    for line in self.order_line:
                        line.analytic_account_id = self.project_id

    @api.onchange('team_id')
    def _onchange_team_id(self):
        if self.company_id and self.company_id.cost_center == 'sales_team':
            if self.team_id:
                self.project_id = self.team_id.account_analytic_id.id
                if self.order_line:
                    for line in self.order_line:
                        line.analytic_account_id = self.project_id
            else:
                self.project_id = False
                if self.order_line:
                    for line in self.order_line:
                        line.analytic_account_id = self.project_id

    @api.model
    def create(self, values):
        if 'contract_id' in values and values['contract_id'] and 'contract.management' in self.env and self.env.user.company_id.cost_center == 'contract':
            analytic_account_id = self.env['contract.management'].browse(values['contract_id']).analytic_account_id.id
            values['project_id'] = analytic_account_id
            if 'order_line' in values and values['order_line']:
                for line in values['order_line']:
                    if 'analytic_account_id' not in line[2]:
                        line[2]['analytic_account_id'] = analytic_account_id
        return super(SaleOrder, self).create(values)

    @api.multi
    def write(self, values):
        res = super(SaleOrder, self).write(values)
        if 'order_line' in values and len(values['order_line']) > 0 and len(values['order_line'][0]) > 2 and values['order_line'][0][2] and 'analytic_account_id' in values['order_line'][0][2]:
            for record in self:
                if not record.invoice_ids.filtered(lambda l: l.state != 'draft') and not record.picking_ids.filtered(lambda l: l.state == 'done'):
                    for line in record.order_line:
                        for picking in record.picking_ids:
                            for move in picking.move_lines:
                                if move.procurement_id.sale_line_id.id == line.id:
                                    move.write({
                                        'analytic_account_id': values['order_line'][0][2]['analytic_account_id']})
                                    move.analytic_share_ids.sudo().unlink()
                                    move.write({
                                        'analytic_share_ids': [(0, 0, {'analytic_account_id': values['order_line'][0][2]['analytic_account_id'], 'rate': 100})], })

        return res
