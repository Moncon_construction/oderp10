# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from odoo.exceptions import UserError


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    @api.multi
    def _compute_analytic_account_id_readonly(self):
        for record in self:
            analytic_account_id_readonly = True
            if not record.order_id.invoice_ids.filtered(lambda l: l.state != 'draft') and not record.order_id.picking_ids.filtered(lambda l: l.state == 'done'):
                analytic_account_id_readonly = False
            record.analytic_account_id_readonly = analytic_account_id_readonly

    analytic_account_id_readonly = fields.Boolean(compute='_compute_analytic_account_id_readonly')
    analytic_share_ids = fields.One2many('account.analytic.share', 'sale_order_line_id', 'Analytic Share', ondelete='restrict')

    @api.onchange('product_id')
    def _onchange_product_id(self):
        if self.product_id:
            if self.order_id.company_id.cost_center == 'product_categ' and self.product_id.categ_id:
                if self.product_id.categ_id.analytic_account_id:
                    self.analytic_account_id = self.product_id.categ_id.analytic_account_id.id
                else:
                    self.analytic_account_id = False
                    raise UserError(_('Please choose analytic account for the product category %s!') % self.product_id.categ_id.name)
            elif self.order_id.company_id.cost_center == 'brand':
                if self.product_id.brand_name:
                    if self.product_id.brand_name.analytic_account_id:
                        self.analytic_account_id = self.product_id.brand_name.analytic_account_id.id
                    else:
                        self.analytic_account_id = False
                        raise UserError(_('Please choose analytic account for the brand %s!') % self.product_id.brand_name.brand_name)
                else:
                    self.analytic_account_id = False
                    raise UserError(_('Please choose brand for the product %s!') % self.product_id.name)

    @api.multi
    def _prepare_invoice_line(self, qty):
        self.ensure_one()
        res = super(SaleOrderLine, self)._prepare_invoice_line(qty)
        # If sale order line has analytic account update invoice's analytic account
        if self.analytic_account_id:
            res.update({
                'account_analytic_id': self.analytic_account_id.id})
        if self.analytic_share_ids:
            res.update({
                'analytic_share_ids': [(0, 0, {'analytic_account_id': share.analytic_account_id.id, 'rate': share.rate}) for share in self.analytic_share_ids]})
        return res

    @api.multi
    def write(self, vals):
        res = super(SaleOrderLine, self).write(vals)
        for record in self:
            if record.order_id.company_id.show_analytic_share:
                if record.analytic_share_ids:
                    total = 0
                    for share in record.analytic_share_ids:
                        total += share.rate
                    if round(total, 2) != 100:
                        raise UserError(_('The sum rate of the analytic shares must be 100%!'))
        return res

    @api.model
    def create(self, vals):
        if 'analytic_share_ids' in vals:
            # one2many утга ирэх ба хэрэв [default number, False or id, id or {}] утгатай байвал
            if vals['analytic_share_ids'] and len(vals['analytic_share_ids']) > 1:
                rate = 0.0
                for share in vals['analytic_share_ids']:
                    if share[2]['rate']:
                        rate += share[2]['rate']
                if round(rate, 2) != 100:
                    raise UserError(_('The sum rate of the analytic shares must be 100%!'))
        return super(SaleOrderLine, self).create(vals)
