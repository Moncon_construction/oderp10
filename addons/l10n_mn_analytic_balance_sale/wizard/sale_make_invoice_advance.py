# -*- coding: utf-8 -*-
import time

from odoo import _, api, fields, models
from odoo.exceptions import UserError

class SaleAdvancePaymentInv(models.TransientModel):
    _inherit = "sale.advance.payment.inv"

    @api.multi
    def _create_invoice(self, order, so_line, amount):
        invoice = super(SaleAdvancePaymentInv, self)._create_invoice(order, so_line, amount)
        for line in invoice.invoice_line_ids:
            if so_line in line.sale_line_ids:
                if so_line.analytic_account_id:
                    line.write({
                        'account_analytic_id': so_line.analytic_account_id.id})
                if so_line.analytic_share_ids:
                    line.write({
                        'analytic_share_ids': [(0, 0, {'analytic_account_id': share.analytic_account_id.id, 'rate': share.rate}) for share in so_line.analytic_share_ids]})
        return invoice

    def _get_sale_order_line_values(self, amount, order, tax_ids):
        self.ensure_one()
        total = order.amount_total
        share_dict = {}
        for line in order.order_line:
            for share in line.analytic_share_ids:
                divide = line.price_total * share.rate
                if share.analytic_account_id.id not in share_dict:
                    share_dict[share.analytic_account_id.id] = (divide / total) if divide > 0 else 0
                else:
                    share_dict[share.analytic_account_id.id] += (divide / total) if divide > 0 else 0
        vals = []
        if total > 0:
            total_rate = 0
            for k, v in share_dict.items():
                vals.append((0, 0, {'analytic_account_id': k, 'rate': v}))
                total_rate += v
            if round(total_rate, 2) != 100:
                raise UserError(_('Sale order does not have a valid analytic share!'))
        return {
            'name': _('Advance: %s') % (time.strftime('%m %Y'),),
            'price_unit': amount,
            'product_uom_qty': 0.0,
            'order_id': order.id,
            'discount': 0.0,
            'product_uom': self.product_id.uom_id.id,
            'product_id': self.product_id.id,
            'tax_id': [(6, 0, tax_ids)],
            'analytic_account_id': order.project_id.id if order.project_id else False,
            'analytic_share_ids': vals if total > 0 else False, }

    @api.multi
    def create_invoices(self):
        sale_orders = self.env['sale.order'].browse(self._context.get('active_ids', []))

        if self.advance_payment_method == 'delivered':
            sale_orders.action_invoice_create()
        elif self.advance_payment_method == 'all':
            sale_orders.action_invoice_create(final=True)
        else:
            # Create deposit product if necessary
            if not self.product_id:
                vals = self._prepare_deposit_product()
                self.product_id = self.env['product.product'].create(vals)
                self.env['ir.values'].sudo().set_default('sale.config.settings', 'deposit_product_id_setting', self.product_id.id)

            sale_line_obj = self.env['sale.order.line']
            for order in sale_orders:
                if self.advance_payment_method == 'percentage':
                    amount = order.amount_untaxed * self.amount / 100
                else:
                    amount = self.amount
                if self.product_id.invoice_policy != 'order':
                    raise UserError(_('The product used to invoice a down payment should have an invoice policy set to "Ordered quantities". Please update your deposit product to be able to create a deposit invoice.'))
                if self.product_id.type != 'service':
                    raise UserError(_("The product used to invoice a down payment should be of type 'Service'. Please use another product or update this product."))
                taxes = self.product_id.taxes_id.filtered(lambda r: not order.company_id or r.company_id == order.company_id)
                if order.fiscal_position_id and taxes:
                    tax_ids = order.fiscal_position_id.map_tax(taxes).ids
                else:
                    tax_ids = taxes.ids
                context = {'lang': order.partner_id.lang}
                so_line = sale_line_obj.create(self._get_sale_order_line_values(amount, order, tax_ids))
                del context
                self._create_invoice(order, so_line, amount)
        if self._context.get('open_invoices', False):
            return sale_orders.action_view_invoice()
        return {'type': 'ir.actions.act_window_close'}
