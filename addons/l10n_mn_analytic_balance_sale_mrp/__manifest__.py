# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian module - Analytic Account Balance Sale MRP",
    'version': '1.0',
    'depends': ['l10n_mn_analytic_balance_sale', 'l10n_mn_analytic_balance_mrp'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules/Analytic',
    'description': """
       Mongolian module - Analytic Account Balance Sale MRP
    """,
    'data': [
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
