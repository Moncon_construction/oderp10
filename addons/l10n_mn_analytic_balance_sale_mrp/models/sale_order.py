# -*- coding: utf-8 -*-


from odoo import api, fields, models, _


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def action_confirm(self):
        """ Борлуулалтын захиалга батлахад үйлдвэрлэлийн захиалга үүсвэл тухайн үйлдвэрлэлийн захиалга нь борлуулалтын захиалгын мөрийн шинжилгээний дансыг авна."""
        res = super(SaleOrder, self).action_confirm()
        for order in self:
            if not order.company_id.show_analytic_share:
                for line in order.order_line:
                    productions = self.env['mrp.production'].search([('sale_order_id', '=', order.id), ('product_id', '=', line.product_id.id)])
                    if productions:
                        for production in productions:
                            production.analytic_account_id = line.analytic_account_id
        return res
