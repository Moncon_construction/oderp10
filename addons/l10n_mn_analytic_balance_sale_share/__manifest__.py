# -*- coding: utf-8 -*-
{
    'name': 'Mongolian Balance Analytic Share- Sales',
    'version': '1.0',
    'depends': [
        'l10n_mn_analytic_balance_sale',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """Борлуулалтын модулиас үүсдэг журналын бичилтийн балансын данс дээр шинжилгээний данс автомат сонгогддог болгоно""",
    'data': [
        'views/sale_order_view.xml',
    ],
    "auto_install": False,
    "installable": True,
}
