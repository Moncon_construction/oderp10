# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################

{
    'name': 'Mongolian Balance Stock Analytic',
    'version': '1.0',
    'depends': [
        'l10n_mn_analytic_balance',
        'l10n_mn_analytic',
        'l10n_mn_stock_account',
        'l10n_mn_analytic_account',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """Агуулахаас үүсэх ажил гүйлгээний балансын данс дээр шинжилгээний данс сонгодог болгох. """,
    'data': [
        'views/stock_move_view.xml',
        'views/product_category_view.xml',
        'views/product_brand_view.xml',
    ],
    "auto_install": False,
    "installable": True,
}
