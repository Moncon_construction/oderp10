# -*- coding: utf-8 -*-
from odoo import fields, models


class AccountAnalyticShare(models.Model):
    _inherit = "account.analytic.share"

    stock_move_id = fields.Many2one('stock.move', 'Stock move', readonly=True)
