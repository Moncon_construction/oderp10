# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class ProductBrand(models.Model):
    _inherit = 'product.brand'

    cost_center = fields.Selection(related='company_id.cost_center', readonly=True)
    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic account')
