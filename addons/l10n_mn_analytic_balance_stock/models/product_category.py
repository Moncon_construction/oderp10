# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class ProductCategory(models.Model):
    _inherit = 'product.category'

    cost_center = fields.Selection(related='company_id.cost_center', readonly=True)
    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic account')
