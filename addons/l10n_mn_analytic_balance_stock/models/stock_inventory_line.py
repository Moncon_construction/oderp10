# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from collections import defaultdict
from datetime import datetime
from odoo import api, fields, models, _
from odoo.exceptions import UserError
import logging
_logger = logging.getLogger(__name__)


class StockInventoryLine(models.Model):
    _inherit = "stock.inventory.line"

    def _get_move_values(self, qty, location_id, location_dest_id):
        # Function to retrieve stock move values
        self.ensure_one()
        res = super(StockInventoryLine, self)._get_move_values(qty, location_id, location_dest_id)
        if self.company_id.cost_center == 'warehouse':
            warehouse = self.inventory_id.location_id.get_warehouse()
            if not warehouse:
                raise UserError(_('Warehouse of inventory location %s is not found') % self.inventory_id.location_id.name)
            if not warehouse.analytic_account_id:
                raise UserError(_('Please select analytic account of this warehouse: %s') % warehouse.name)
            res['analytic_account_id'] = warehouse.analytic_account_id.id
            res['analytic_share_ids'] = [(0, 0, {'analytic_account_id': warehouse.analytic_account_id.id, 'rate': 100})]
        elif self.company_id.cost_center == 'brand':
            if not self.product_id.brand_name:
                raise UserError(_('Please select brand of this product: [%s] %s') % (self.product_id.default_code, self.product_id.name))
            if not self.product_id.brand_name.analytic_account_id:
                raise UserError(_('Please select analytic account of this brand: %s') % (self.product_id.brand_name.brand_name))
            res['analytic_account_id'] = self.product_id.brand_name.analytic_account_id.id
            res['analytic_share_ids'] = [(0, 0, {'analytic_account_id': self.product_id.brand_name.analytic_account_id.id, 'rate': 100})]
        elif self.company_id.cost_center == 'product_categ':
            if not self.product_id.categ_id:
                raise UserError(_('Please select category of this product: [%s] %s') % (self.product_id.default_code, self.product_id.name))
            if not self.product_id.categ_id.analytic_account_id:
                raise UserError(_('Please select analytic account of this category: %s') % (self.product_id.categ_id.name))
            res['analytic_account_id'] = self.product_id.categ_id.analytic_account_id.id
            res['analytic_share_ids'] = [(0, 0, {'analytic_account_id': self.product_id.categ_id.analytic_account_id.id, 'rate': 100})]
        elif self.company_id.cost_center in ['department', 'sales_team', 'project', 'technic', 'contract']:
            if not self.company_id.default_analytic_account_id:
                raise UserError(_('Please select default analytic account of this company: %s') % self.company_id.name)
            res['analytic_account_id'] = self.company_id.default_analytic_account_id.id
            res['analytic_share_ids'] = [(0, 0, {'analytic_account_id': self.company_id.default_analytic_account_id.id, 'rate': 100})]
        return res
