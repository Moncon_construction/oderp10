# -*- coding: utf-8 -*-
import logging
from collections import defaultdict
from datetime import datetime

from odoo import _, api, fields, models
from odoo.exceptions import UserError


class StockMove(models.Model):
    _inherit = "stock.move"

    analytic_share_ids = fields.One2many('account.analytic.share', 'stock_move_id', 'Analytic share', copy=True)

    def _prepare_account_move_line(self, qty, cost, credit_account_id, debit_account_id):
        res = super(StockMove, self)._prepare_account_move_line(qty, cost, credit_account_id, debit_account_id)
        # Update analytic_account_id and analytic_share_ids
        for record in res:
            vals = []
            share_obj = self.env['account.analytic.share']
            if self.analytic_share_ids:
                for share in self.analytic_share_ids:
                    a_ids = share_obj.create({
                        'analytic_account_id': share.analytic_account_id.id,
                        'rate': share.rate,
                    })
                    vals.append((4, a_ids.id))
            if self.analytic_account_id:
                record[2].update({'analytic_account_id': self.analytic_account_id.id})
            record[2].update({'analytic_share_ids': vals})
        if self.company_id.cost_center:
            if self.company_id.cost_center == 'warehouse':
                is_cost_for_each_wh = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_stock_account_cost_for_each_wh')], limit=1)
                if self.picking_id and self.picking_id.transit_order_id and is_cost_for_each_wh.state == 'installed':
                    road_analytic_account_id = self.picking_id.transit_order_id.warehouse_id.analytic_account_id.id
                    if self.picking_id.picking_type_id.id == self.picking_id.transit_order_id.supply_picking_type_id.id:
                        if not self.picking_id.transit_order_id.supply_warehouse_id.analytic_account_id:
                            raise UserError(_('Please select analytic account of supply warehouse'))
                        account_analytic_id = self.picking_id.transit_order_id.supply_warehouse_id.analytic_account_id.id
                        for i in res:
                            if i[2]['account_id'] == self.picking_id.transit_order_id.supply_warehouse_id.stock_valuation_account_id.id:
                                i[2]['analytic_account_id'] = account_analytic_id
                                i[2]['analytic_share_ids'] = [(0, 0, {'analytic_account_id': account_analytic_id, 'rate': 100})]
                            elif i[2]['account_id'] == self.picking_id.transit_order_id.supply_warehouse_id.replenishment_account_on_the_road_id.id:
                                i[2]['analytic_account_id'] = road_analytic_account_id
                                i[2]['analytic_share_ids'] = [(0, 0, {'analytic_account_id': road_analytic_account_id, 'rate': 100})]
                    elif self.picking_id.picking_type_id.id == self.picking_id.transit_order_id.receive_picking_type_id.id:
                        if not self.picking_id.transit_order_id.warehouse_id.analytic_account_id:
                            raise UserError(_('Please select analytic account of receiving warehouse'))
                        account_analytic_id = self.picking_id.transit_order_id.warehouse_id.analytic_account_id.id
                        for i in res:
                            if i[2]['account_id'] == self.picking_id.transit_order_id.warehouse_id.stock_valuation_account_id.id:
                                i[2]['analytic_account_id'] = account_analytic_id
                                i[2]['analytic_share_ids'] = [(0, 0, {'analytic_account_id': account_analytic_id, 'rate': 100})]
                            elif i[2]['account_id'] == self.picking_id.transit_order_id.warehouse_id.replenishment_account_on_the_road_id.id:
                                i[2]['analytic_account_id'] = road_analytic_account_id
                                i[2]['analytic_share_ids'] = [(0, 0, {'analytic_account_id': road_analytic_account_id, 'rate': 100})]
                elif self.picking_id and self.picking_id.expense and is_cost_for_each_wh.state == 'installed':
                    if not self.picking_id.expense.warehouse.analytic_account_id:
                        raise UserError(_('Please select analytic account of supply warehouse'))
                    account_analytic_id = self.picking_id.expense.warehouse.analytic_account_id.id
                    for i in res:
                        if i[2]['account_id'] == self.picking_id.expense.warehouse.stock_valuation_account_id.id:
                            i[2]['analytic_account_id'] = account_analytic_id
                            i[2]['analytic_share_ids'] = [(0, 0, {'analytic_account_id': account_analytic_id, 'rate': 100})]
        return res
