# -*- coding: utf-8 -*-
import logging
from collections import defaultdict
from datetime import datetime

from odoo import _, api, fields, models
from odoo.exceptions import UserError


class StockTransitOrderLine(models.Model):
    _inherit = 'stock.transit.order.line'

    @api.multi
    def _prepare_stock_moves(self, picking):
        # Function to retrieve in picking's stock move values
        res = super(StockTransitOrderLine, self)._prepare_stock_moves(picking)
        if self.company_id.cost_center == 'warehouse':
            is_cost_for_each_wh = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_stock_account_cost_for_each_wh')], limit=1)
            if is_cost_for_each_wh.state == 'installed':
                if not self.transit_order_id.warehouse_id.analytic_account_id:
                    raise UserError(_('Please select analytic account of receiving warehouse'))
                account_analytic_id = self.transit_order_id.warehouse_id.analytic_account_id.id
                for i in res:
                    i['analytic_account_id'] = account_analytic_id
                    i['analytic_share_ids'] = [(0, 0, {'analytic_account_id': account_analytic_id, 'rate': 100})]
        elif self.company_id.cost_center == 'department':
            employee = self.env['hr.employee'].search([('user_id', '=', self.transit_order_id.create_uid.id)], limit=1)
            if employee and employee.department_id and employee.department_id.analytic_account_id:
                for i in res:
                    i['analytic_account_id'] = employee.department_id.analytic_account_id.id
                    i['analytic_share_ids'] = [(0, 0, {'analytic_account_id': employee.department_id.analytic_account_id.id, 'rate': 100})]
            else:
                raise UserError(_('The department of created user\'s employee does not have analytic account!'))
        elif self.company_id.cost_center in ('sales_team', 'project', 'contract', 'technic'):
            if self.company_id.default_analytic_account_id:
                for i in res:
                    i['analytic_account_id'] = self.company_id.default_analytic_account_id.id
                    i['analytic_share_ids'] = [(0, 0, {'analytic_account_id': self.company_id.default_analytic_account_id.id, 'rate': 100})]
            else:
                raise UserError(_('The company does not have default analytic account!'))
        elif self.company_id.cost_center == 'product_categ':
            for i in res:
                product = self.env['product.product'].browse(i['product_id'])
                if product.categ_id.analytic_account_id:
                    for i in res:
                        i['analytic_account_id'] = product.categ_id.analytic_account_id.id
                        i['analytic_share_ids'] = [(0, 0, {'analytic_account_id': product.categ_id.analytic_account_id.id, 'rate': 100})]
                else:
                    raise UserError(_('Category of product %s does not have analytic account!') % product.name)
        elif self.company_id.cost_center == 'brand':
            for i in res:
                product = self.env['product.product'].browse(i['product_id'])
                if product.brand_name and product.brand_name.analytic_account_id:
                    for i in res:
                        i['analytic_account_id'] = product.brand_name.analytic_account_id.id
                        i['analytic_share_ids'] = [(0, 0, {'analytic_account_id': product.brand_name.analytic_account_id.id, 'rate': 100})]
                else:
                    raise UserError(_('The brand of product %s does not have analytic account!') % product.name)
        return res

    @api.multi
    def _prepare_out_stock_moves(self, picking):
        # Function to retrieve out picking's stock move values
        res = super(StockTransitOrderLine, self)._prepare_out_stock_moves(picking)
        if self.company_id.cost_center == 'warehouse':
            is_cost_for_each_wh = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_stock_account_cost_for_each_wh')], limit=1)
            if is_cost_for_each_wh.state == 'installed':
                if not self.transit_order_id.supply_warehouse_id.analytic_account_id:
                    raise UserError(_('Please select analytic account of receiving warehouse'))
                account_analytic_id = self.transit_order_id.supply_warehouse_id.analytic_account_id.id
                for i in res:
                    i['analytic_account_id'] = account_analytic_id
                    i['analytic_share_ids'] = [(0, 0, {'analytic_account_id': account_analytic_id, 'rate': 100})]
        elif self.company_id.cost_center == 'department':
            employee = self.env['hr.employee'].search([('user_id', '=', self.transit_order_id.create_uid.id)], limit=1)
            if employee and employee.department_id and employee.department_id.analytic_account_id:
                for i in res:
                    i['analytic_account_id'] = employee.department_id.analytic_account_id.id
                    i['analytic_share_ids'] = [(0, 0, {'analytic_account_id': employee.department_id.analytic_account_id.id, 'rate': 100})]
            else:
                raise UserError(_('The department of created user\'s employee does not have analytic account!'))
        elif self.company_id.cost_center in ('sales_team', 'project', 'contract', 'technic'):
            if self.company_id.default_analytic_account_id:
                for i in res:
                    i['analytic_account_id'] = self.company_id.default_analytic_account_id.id
                    i['analytic_share_ids'] = [(0, 0, {'analytic_account_id': self.company_id.default_analytic_account_id.id, 'rate': 100})]
            else:
                raise UserError(_('The company does not have default analytic account!'))
        elif self.company_id.cost_center == 'product_categ':
            for i in res:
                product = self.env['product.product'].browse(i['product_id'])
                if product.categ_id.analytic_account_id:
                    for i in res:
                        i['analytic_account_id'] = product.categ_id.analytic_account_id.id
                        i['analytic_share_ids'] = [(0, 0, {'analytic_account_id': product.categ_id.analytic_account_id.id, 'rate': 100})]
                else:
                    raise UserError(_('Category of product %s does not have analytic account!') % product.name)
        elif self.company_id.cost_center == 'brand':
            for i in res:
                product = self.env['product.product'].browse(i['product_id'])
                if product.brand_name and product.brand_name.analytic_account_id:
                    for i in res:
                        i['analytic_account_id'] = product.brand_name.analytic_account_id.id
                        i['analytic_share_ids'] = [(0, 0, {'analytic_account_id': product.brand_name.analytic_account_id.id, 'rate': 100})]
                else:
                    raise UserError(_('The brand of product %s does not have analytic account!') % product.name)
        return res
