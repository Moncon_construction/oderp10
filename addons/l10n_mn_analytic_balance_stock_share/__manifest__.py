# -*- coding: utf-8 -*-
{
    'name': 'Mongolian Balance Stock Analytic Share',
    'version': '1.0',
    'depends': [
        'l10n_mn_analytic_balance_stock',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """Mongolian Balance Stock Analytic Share""",
    'data': [
        'views/stock_move_view.xml',
    ],
    "auto_install": False,
    "installable": True,
}
