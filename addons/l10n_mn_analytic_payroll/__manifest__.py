# -*- coding: utf-8 -*-
{
    'name': "Analytic Payroll",
    'summary': """Mongolian Analytic Payroll""",
    'description': """ Payroll with analytic""",
    'author': "Asterisk Technologies LLC",
    'website': "http://www.asterisk-tech.mn",
    'category': 'Payroll',
    'depends': [
        'l10n_mn_hr_payroll',
        'l10n_mn_analytic_account',
    ],
    'data': [
    ]
}
