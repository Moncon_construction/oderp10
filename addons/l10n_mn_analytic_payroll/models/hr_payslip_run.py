# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class HrPayslipRun(models.Model):
    _inherit = 'hr.payslip.run'

    @api.multi
    def _get_account_move_line_vals(self, dictionary, name, partner, is_debit=False, employee=False):
        self.ensure_one()
        config = self.env['hr.payroll.config'].search([('company_id', '=', self.company_id.id)], limit=1)
        is_expense_by_analytic = config and config.expense_line_by_analytic or False
        res = super(HrPayslipRun, self)._get_account_move_line_vals(dictionary, name, partner, is_debit)
        analytic_account_id = False
        if not employee and 'emp' in dictionary and dictionary['emp']:
            employee = self.env['hr.employee'].browse(dictionary['emp'])
        if is_expense_by_analytic and self.env['account.account'].browse(dictionary['account']).req_analytic_account:
            if self.company_id.cost_center == 'department':
                if employee:
                    if employee.department_id and employee.department_id.analytic_account_id:
                        analytic_account_id = employee.department_id.analytic_account_id.id
                    elif employee.contract_id and employee.contract_id.analytic_account_id:
                        analytic_account_id = employee.contract_id.analytic_account_id.id
            else:
                if 'hsr_id' in dictionary and dictionary['hsr_id']:
                    salary_rule_id = self.env['hr.salary.rule'].browse(dictionary['hsr_id'])
                    if salary_rule_id.analytic_account_id:
                        analytic_account_id = salary_rule_id.analytic_account_id.id
                if not analytic_account_id:
                    if employee:
                        if employee.contract_id and employee.contract_id.analytic_account_id:
                            analytic_account_id = employee.contract_id.analytic_account_id.id
                        elif employee.department_id and employee.department_id.analytic_account_id:
                            analytic_account_id = employee.department_id.analytic_account_id.id
        if analytic_account_id:
            if not self.company_id.show_analytic_share:
                res[2].update({'analytic_account_id': analytic_account_id})
            else:
                res[2].update({'analytic_share_ids': [(0, 0, {'analytic_account_id': analytic_account_id, 'rate': 100})]})
        return res

    def _get_advance_last_slry_credit_qry_with_acc_not_in(self, acc_ids, create_move_in_advance=False):
        # УРЬДЧИЛГАА/СҮҮЛ төрөлтэй цалингийн КТ талын бичилтийн өгөгдлийг авах query
        # Цалингийн бодолтын мөрүүдийг Кредит талдаа дансаар нь бүлэглэж үүсгэнэ, Цалин өглөг, ХХОАТ өглөг, НДШ өглөг гэх мэт
        self.ensure_one()
        advance_where = " AND sr.create_move_in_advance = TRUE" if create_move_in_advance else ""
        return '''
                SELECT ps.employee_id AS emp,
                    sr.account_credit AS account,
                    pl.total AS total,
                    sr.transaction_description AS dscrtpn,
                    sr.id as hsr_id
                FROM hr_payslip_line pl,
                    hr_salary_rule sr,
                    hr_payslip ps
                WHERE pl.salary_rule_id = sr.id
                    AND pl.total <> 0
                    AND sr.account_credit <> 0
                    AND ps.payslip_run_id = %s
                    AND sr.account_credit NOT IN %s
                    AND pl.slip_id = ps.id %s
                ORDER BY ps.employee_id ASC,
                    sr.account_credit ASC''' % (self.id, acc_ids, advance_where)

    def _get_advance_last_slry_debit_qry_with_acc_not_in(self, acc_ids, create_move_in_advance=False):
        # УРЬДЧИЛГАА/СҮҮЛ төрөлтэй цалингийн ДТ талын бичилтийн өгөгдлийг авах query
        self.ensure_one()
        advance_where = " AND sr.create_move_in_advance = TRUE" if create_move_in_advance else ""

        return '''
                SELECT
                    sr.account_debit AS account,
                    sum(pl.total) AS total,
                    min(sr.transaction_description) AS dscrtpn,
                    sr.id as hsr_id,
                    hp.employee_id AS emp
                FROM
                    hr_salary_rule sr,
                    hr_payslip_line pl
                    left join hr_payslip hp on hp.id = pl.slip_id
                WHERE
                    pl.slip_id in (
                        SELECT
                            id
                        FROM
                            hr_payslip
                        WHERE
                            payslip_run_id = %s)
                    AND pl.salary_rule_id = sr.id
                    AND pl.total <> 0
                    AND sr.account_debit <> 0
                    AND sr.account_debit NOT IN %s %s
                group by hp.employee_id,
                    sr.id,
                    sr.account_debit
                ORDER BY sr.account_debit''' % (self.id, acc_ids, advance_where)

    def _get_advance_last_slry_debit_qry_with_acc_not_in_without_expense(self, acc_ids):
        # УРЬДЧИЛГАА/СҮҮЛ төрөлтэй цалингийн ДТ талын бичилтийн өгөгдлийг авах query
        # Цалингийн бодолтын мөрүүдийг Дебит талдаа дансаар нь бүлэглэж үүсгэнэ, Цалин өглөг, Цалин зардал, НДШ зардал гэх мэт
        self.ensure_one()
        return '''
                SELECT
                    sr.account_debit AS account,
                    SUM(pl.total) AS total,
                    aa.name,
                    MIN(sr.transaction_description) AS dscrtpn,
                    sr.id as hsr_id,
                    hp.employee_id AS emp
                FROM
                    hr_salary_rule sr,
                    account_account aa,
                    account_account_type at,
                    hr_payslip_line pl
                    left join hr_payslip hp on hp.id = pl.slip_id
                WHERE
                    pl.slip_id in (
                        SELECT
                            id
                        FROM
                            hr_payslip
                        WHERE
                            payslip_run_id = %s)
                    AND aa.id = sr.account_debit
                    AND aa.user_type_id = at.id
                    AND at.type <> 'expense'
                    AND pl.salary_rule_id = sr.id
                    AND pl.total <> 0
                    AND sr.account_debit <> 0
                    AND sr.account_debit NOT IN %s
                GROUP BY
                    hp.employee_id,
                    sr.id,
                    aa.name
                ORDER BY
                    sr.account_debit''' % (self.id, acc_ids)

    @api.multi
    def _group_account_move_line_vals(self, line_ids):
        self.ensure_one()
        line_ids = super(HrPayslipRun, self)._group_account_move_line_vals(line_ids)
        line_ids2 = []
        for line in line_ids:
            if not self.company_id.show_analytic_share:
                added = False
                for line2 in line_ids2:
                    if line2[2]['analytic_account_id'] == line[2]['analytic_account_id'] and line2[2]['account_id'] == line[2]['account_id'] and line2[2]['partner_id'] == line[2]['partner_id'] and line2[2]['journal_id'] == line[2]['journal_id']:
                        if line[2]['debit'] > 0:
                            line2[2]['debit'] += line[2]['debit']
                            added = True
                        elif line[2]['credit'] > 0:
                            line2[2]['credit'] += line[2]['credit']
                            added = True
                if not added:
                    line_ids2.append([0, 0, {
                        'name': line[2]['name'],
                        'date': line[2]['date'],
                        'partner_id': line[2]['partner_id'],
                        'account_id': line[2]['account_id'],
                        'journal_id': line[2]['journal_id'],
                        'debit': line[2]['debit'],
                        'credit': line[2]['credit'],
                        'analytic_account_id': line[2]['analytic_account_id'],
                    }])
            else:
                added = False
                for line2 in line_ids2:
                    if 'analytic_share_ids' in line[2] and line2[2]['analytic_share_ids'] == line[2]['analytic_share_ids'] and line2[2]['account_id'] == line[2]['account_id'] and line2[2]['partner_id'] == line[2]['partner_id'] and line2[2]['journal_id'] == line[2]['journal_id']:
                        if line[2]['debit'] > 0:
                            line2[2]['debit'] += line[2]['debit']
                            added = True
                        elif line[2]['credit'] > 0:
                            line2[2]['credit'] += line[2]['credit']
                            added = True
                if not added:
                    line_ids2.append([0, 0, {
                        'name': line[2]['name'],
                        'date': line[2]['date'],
                        'partner_id': line[2]['partner_id'],
                        'account_id': line[2]['account_id'],
                        'journal_id': line[2]['journal_id'],
                        'debit': line[2]['debit'],
                        'credit': line[2]['credit'],
                        'analytic_share_ids': line[2]['analytic_share_ids'] if 'analytic_share_ids' in line[2] else False,
                    }])
        return line_ids2
