# -*- coding: utf-8 -*-
{
    'name': "Analytic Product Expense",
    'summary': """Mongolian Analytic Product Expense""",
    'description': """ Expense products with analytic""",
    'author': "Asterisk Technologies LLC",
    'website': "http://www.asterisk-tech.mn",
    'category': 'Product Expense',
    'depends': [
        'l10n_mn_product_expense',
        'l10n_mn_analytic_balance_stock',
    ],
    'data': [
        'views/stock_warehouse_view.xml',
        'views/product_expense_view.xml',
    ]
}
