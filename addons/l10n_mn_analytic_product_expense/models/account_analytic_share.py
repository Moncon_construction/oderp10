# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class AccountAnalyticShare(models.Model):
    _inherit = 'account.analytic.share'

    product_expense_line_id = fields.Many2one('product.expense.line', 'Product expense line', readonly=True)
    quantity = fields.Float('Quantity')
