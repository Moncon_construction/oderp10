# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class ProductExpense(models.Model):
    _inherit = 'product.expense'

    @api.onchange('department')
    def onchange_department(self):
        # Set analytic account
        analytic_account_id = False
        if self.cost_center == 'department':
            if self.department.analytic_account_id:
                analytic_account_id = self.department.analytic_account_id.id
        self.analytic_account = analytic_account_id
        for line in self.expense_line:
            line.analytic_account = analytic_account_id
        super(ProductExpense, self).onchange_department()

    @api.onchange('warehouse')
    def onchange_warehouse(self):
        # Set analytic account
        analytic_account_id = False
        if self.cost_center == 'warehouse':
            if self.warehouse.analytic_account_id:
                analytic_account_id = self.warehouse.analytic_account_id.id
        self.analytic_account = analytic_account_id
        for line in self.expense_line:
            line.analytic_account = analytic_account_id
        super(ProductExpense, self).onchange_warehouse()

    @api.model
    def _check_analytic_account(self):
        for line in self.expense_line:
            if line.account and line.account.req_analytic_account:
                if line.analytic_account or line.expense.company.show_analytic_share:
                    continue
                else:
                    raise UserError(_('Сонгосон данс шинжилгээний данс шаардах данс тул шинжилгээний дансыг мөрөн дээр сонгож өгнө үү'))
