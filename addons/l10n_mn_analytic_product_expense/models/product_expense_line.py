# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from odoo.exceptions import UserError


class ProductExpenseLine(models.Model):
    _inherit = 'product.expense.line'

    analytic_share_ids = fields.One2many('account.analytic.share', 'product_expense_line_id', 'Analytic Share', ondelete='restrict')

    @api.onchange('product')
    def _onchange_product(self):
        self.name = self.product.name if self.product.name else '/'
        if self.cost_center == 'product_categ' and self.product.categ_id and self.product.categ_id.analytic_account_id:
            self.analytic_account = self.product.categ_id.analytic_account_id.id
        elif self.cost_center == 'brand' and self.product.brand_name and self.product.brand_name.analytic_account_id:
            self.analytic_account = self.product.brand_name.analytic_account_id

    @api.multi
    def _get_stock_move_vals(self, picking_id, location_id, location_dest_id):
        res = super(ProductExpenseLine, self)._get_stock_move_vals(picking_id, location_id, location_dest_id)
        res['analytic_share_ids'] = []
        for share in self.analytic_share_ids:
            res['analytic_share_ids'].append((0, 0, {'analytic_account_id': share.analytic_account_id.id, 'rate': share.quantity / self.quantity * 100}))
        return res

    @api.model
    def _check_share_quantities(self):
        if self.expense.company.show_analytic_share and self.analytic_share_ids and sum(a.quantity for a in self.analytic_share_ids) != self.quantity:
            raise UserError(_('The sum of share line quantities must be equal to expense line\'s quantity!'))

    @api.multi
    def write(self, vals):
        res = super(ProductExpenseLine, self).write(vals)
        self._check_share_quantities()
        return res

    @api.model
    def create(self, vals):
        res = super(ProductExpenseLine, self).create(vals)
        res._check_share_quantities()
        return res
