# -*- coding: utf-8 -*-

from odoo import models, fields, api


class StockWareHouse(models.Model):
    _inherit = 'stock.warehouse'

    @api.one
    def _compute_show_analytic_account(self):
        self.show_analytic_account = False
        if self.company_id.cost_center == 'warehouse':
            self.show_analytic_account = True

    show_analytic_account = fields.Boolean('Show analytic account', compute='_compute_show_analytic_account')
    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic account')
