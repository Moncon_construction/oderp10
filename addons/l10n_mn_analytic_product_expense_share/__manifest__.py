# -*- coding: utf-8 -*-
{
    'name': "Analytic Product Expense Share",
    'summary': """Mongolian Analytic Product Expense Share""",
    'description': """ Expense products with analytic share""",
    'author': "Asterisk Technologies LLC",
    'website': "http://www.asterisk-tech.mn",
    'category': 'Product Expense',
    'depends': [
        'l10n_mn_analytic_product_expense',
    ],
    'data': [
        'views/product_expense_view.xml',
    ]
}
