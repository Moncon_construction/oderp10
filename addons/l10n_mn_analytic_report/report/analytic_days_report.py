# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://asterisk-tech.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

from odoo import models, fields, api, _
from odoo.tools.translate import _
from xlsxwriter.utility import xl_rowcol_to_cell
import xlsxwriter
from io import BytesIO
import base64
import time
from datetime import datetime, timedelta
from itertools import *
from odoo.exceptions import UserError, ValidationError
from calendar import monthrange
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class AnalyticAccountReport(models.TransientModel):
    _inherit = "analytic.account.report"

    is_view_days = fields.Boolean('Is check days')

    def export(self):
        if self.is_view_days:
            this = self
            fiscal_year = datetime.strptime("{0}".format(this.fiscalyear_id.date_start), '%Y-%m-%d').year
            if this.filter == 'filter_period':
                if not this.period_from or not this.period_to:
                    raise UserError(_("You have not select period !!!"))
                if datetime.strptime("{0}".format(this.period_from.date_start), '%Y-%m-%d').year != fiscal_year:
                    raise UserError(_("The fiscal year of your chosen period's year should be the same !!!"))
            else:
                if not this.date_from or not this.date_to:
                    raise UserError(_("You have not select date !!!"))
                if datetime.strptime("{0}".format(this.date_from), '%Y-%m-%d').year != fiscal_year:
                    raise UserError(_("The fiscal year of your chosen date's year should be the same !!!"))
                if datetime.strptime("{0}".format(this.date_from), '%Y-%m-%d').year != fiscal_year:
                    raise UserError(_("The fiscal year of your chosen date's year should be the same !!!"))

            sheetname_1 = '1'
            output = BytesIO()

            report_name = _('Account Analytic Report')
            file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
            report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(
                filename_prefix=('account_analytic_report'), form_title=file_name).create({})

            workbook = xlsxwriter.Workbook(output)
            worksheet2 = workbook.add_worksheet(sheetname_1)
            title = workbook.add_format(ReportExcelCellStyles.title)
            title1 = workbook.add_format(ReportExcelCellStyles.title1)
            title2 = workbook.add_format(ReportExcelCellStyles.title2)
            header3 = workbook.add_format(ReportExcelCellStyles.header3)
            header4 = workbook.add_format(ReportExcelCellStyles.header4)
            header5 = workbook.add_format(ReportExcelCellStyles.header5)
            cell_format = workbook.add_format(ReportExcelCellStyles.cell_format)
            cell_format1 = workbook.add_format(ReportExcelCellStyles.cell_format1)
            cell_format3 = workbook.add_format(ReportExcelCellStyles.cell_format3)

            worksheet2.set_column('A:A', 5)
            worksheet2.set_column('B:B', 15)

            date_format = workbook.add_format()

            row = 1
            col = 0
            enter = 0

            timestr = time.strftime('%Y-%m-%d')
            now = datetime.strptime(timestr, "%Y-%m-%d")

            if this.filter == 'filter_period':
                date_start = datetime.strptime("{0}".format(this.period_from.date_start), '%Y-%m-%d')
                date_end = datetime.strptime("{0}".format(this.period_to.date_stop), '%Y-%m-%d')
            else:
                date_start = datetime.strptime("{0}".format(this.date_from), '%Y-%m-%d')
                date_end = datetime.strptime("{0}".format(this.date_to), '%Y-%m-%d')

            worksheet2.write(0, 0, this.company_id.name, title1)
            worksheet2.write(row + 1, col + 1, u'Шинжилгээний дансны тайлан', title)
            worksheet2.write(row + 2, col + 1, u'Хугацаа: %s - %s' % (date_start, date_end), title1)
            worksheet2.write(row + 3, col + 1, u'/by MNT/', title1)
            worksheet2.write(row + 4, col + 1, u'Огноо: %s' % (timestr), title1)

            row = 8

            if this.group == 'analytic':
                analytic_account_ids = self.env['account.analytic.account'].search(
                    ['|', ('id', 'in', this.analytic_ids.ids), ('parent_id', 'in', this.analytic_ids.ids)] if len(
                        this.analytic_ids) > 0 else [], order='code asc')
            else:
                a_account_ids = self.env['account.account'].search(
                    [('id', 'in', this.account_ids.ids)] if len(this.account_ids) > 0 else [], order='code asc')
                where = ''
                if len(this.account_ids) == 1:
                    where += 'and aa.id = ' + str(a_account_ids.id)
                elif len(this.account_ids) > 1:
                    where += 'and aa.id in ' + str(tuple(a_account_ids.ids))
                self._cr.execute("""SELECT aa.id, aa.code, aa.name
                                                FROM account_analytic_line l
                                                LEFT JOIN account_analytic_account a ON l.account_id=a.id
                                                LEFT JOIN account_account aa ON l.general_account_id = aa.id
                                                WHERE a.type not in ('view') AND
                                                l.date BETWEEN '%s' and '%s' %s
                                                GROUP BY aa.id ORDER BY aa.code """ % (date_start, date_end, where))
                account_account_ids = self._cr.fetchall()

            periods = self.env['account.period'].search([('fiscalyear_id', '=', this.fiscalyear_id.id)],order='date_start asc')

            worksheet2.merge_range(row, col, row + 1, col, u'Код', title2)
            worksheet2.merge_range(row, col + 1, row + 1, col + 1,
                                   u'Санхүүгийн данс' if this.group == 'analytic' else u'Шинжилгээний данс', title2)

            col = 3

            income_col_total = {}
            expense_col_total = {}
            period_dic = {}
            for period in periods:
                if self.filter == 'filter_date':
                    period_start = datetime.strptime("{0}".format(period.date_start), '%Y-%m-%d')
                    date_from = datetime.strptime("{0}".format(self.date_from), '%Y-%m-%d')
                    if period_start.month == date_from.month:
                        month = monthrange(date_from.year, date_from.month)
                        day = 1
                        while day <= month[1]:
                            worksheet2.write(row + 2, col, u"{0}-р өдөр".format(day), title2)
                            day += 1
                            col += 1
                        worksheet2.write(row + 2, col, u'Нийт', title2)
                        worksheet2.merge_range(row + 1, col - month[1], row + 1, col,
                                               u"{0}-р сар".format(period_start.month), title2)
                        key = str(period_start.year) + '-' + str(period_start.month)
                        period_dic[key] = col
                        income_col_total[key] = 0
                        expense_col_total[key] = 0
                    else:
                        worksheet2.write(row + 1, col, u"{0}-р сар".format(period_start.month), title2)
                        key = str(period_start.year) + '-' + str(period_start.month)
                        period_dic[key] = col
                        income_col_total[key] = 0
                        expense_col_total[key] = 0
                    col += 1
                elif self.filter == 'filter_period':
                    period_start = datetime.strptime("{0}".format(period.date_start), '%Y-%m-%d')
                    date_from = datetime.strptime("{0}".format(this.period_from.date_start), '%Y-%m-%d')
                    if period_start.month == date_from.month:
                        month = monthrange(date_from.year, date_from.month)
                        day = 1
                        while day <= month[1]:
                            worksheet2.write(row + 2, col, u"{0}-р өдөр".format(day), title2)
                            day += 1
                            col += 1
                        worksheet2.write(row + 2, col, u'Нийт', title2)
                        worksheet2.merge_range(row + 1, col - month[1], row + 1, col,
                                               u"{0}-р сар".format(period_start.month), title2)
                        key = str(period_start.year) + '-' + str(period_start.month)
                        period_dic[key] = col
                        income_col_total[key] = 0
                        expense_col_total[key] = 0
                    else:
                        worksheet2.write(row + 1, col, u"{0}-р сар".format(period_start.month), title2)
                        key = str(period_start.year) + '-' + str(period_start.month)
                        period_dic[key] = col
                        income_col_total[key] = 0
                        expense_col_total[key] = 0
                    col += 1

            worksheet2.merge_range(row, 3, row, col - 1, u"{0} он".format(this.fiscalyear_id.name), title2)
            worksheet2.merge_range(row, 2, row + 1, 2, u"Нийт", title2)
            row += 3

            if this.group == 'analytic':
                for aa_id in analytic_account_ids:
                    income_col_subtotal = {}
                    expense_col_subtotal = {}
                    income_row_subtotal = {}
                    i = 0
                    col = 3
                    period_dic = {}
                    period_day_dic = {}
                    for period in periods:
                        period_start = datetime.strptime("{0}".format(period.date_start), '%Y-%m-%d')
                        if self.filter == 'filter_date':
                            date_from = datetime.strptime("{0}".format(self.date_from), '%Y-%m-%d')
                        else:
                            date_from = datetime.strptime("{0}".format(self.period_from.date_start), '%Y-%m-%d')
                        if period_start.month == date_from.month:
                            month = monthrange(date_from.year, date_from.month)
                            day = 1
                            key = str(period_start.year) + '-' + str(period_start.month)
                            period_dic[key] = col + month[1]
                            income_col_subtotal[key] = 0
                            expense_col_subtotal[key] = 0
                            while day <= month[1]:
                                day_key = datetime.strptime((str(period_start.year) + '-' + str(period_start.month) + '-' +str(day)), '%Y-%m-%d').date()
                                period_day_dic[str(day_key)] = col
                                day += 1
                                col += 1
                            col += 1
                        else:
                            key = str(period_start.year) + '-' + str(period_start.month)
                            period_dic[key] = col
                            income_col_subtotal[key] = 0
                            expense_col_subtotal[key] = 0
                            col += 1
                    worksheet2.merge_range(row, 0, row, 14+month[1], (aa_id.code if aa_id.code else '') + ' ' + aa_id.name,
                                           header3)
                    worksheet2.write(row, 2, '', header3)
                    for j in period_dic.keys():
                        worksheet2.write(row, period_dic[j]+month[1], '', header3)
                    records = {}
                    self._cr.execute("SELECT a.code, a.name, EXTRACT(year from l.date) || '-' || EXTRACT(month from l.date), \
                                SUM(l.amount), aa.code AS account_code, aa.name AS account_name, aa.id, a.id \
                                FROM account_analytic_line l, account_analytic_account a, account_account aa \
                                WHERE l.account_id=a.id AND l.general_account_id = aa.id AND a.type not in ('view') AND \
                                l.date BETWEEN '%s' and '%s' AND l.account_id = %s \
                                GROUP BY EXTRACT(year from l.date) || '-' || EXTRACT(month from l.date), a.id, aa.id \
                                 ORDER BY aa.code" % (date_start, date_end, aa_id.id))
                    records = self._cr.fetchall()
                    temp_account_ids = {}
                    for record in records:
                        if record[6] not in temp_account_ids:
                            temp_account_ids[record[6]] = {'line': [record]}
                        else:
                            temp_account_ids[record[6]]['line'].append(record)

                    col = 0
                    for line in temp_account_ids:
                        prev_record = 0
                        for result in temp_account_ids[line]['line']:
                            if prev_record != result[6]:
                                prev_record = result[6]

                                row += 1
                                worksheet2.write(row, col + i, '%s' % result[4], cell_format3)
                                i += 1
                                worksheet2.write(row, col + i, '%s' % result[5], cell_format)
                                i += 1
                                if result[2] == str(date_from.year) + '-' + str(date_from.month):
                                    for days_key, value in period_day_dic.items():
                                        worksheet2.write(row, value, self.days_count(days_key, days_key,result[6],result[7]), cell_format1)
                                for j in period_dic.keys():
                                    worksheet2.write(row, period_dic[j], '', cell_format1)
                                worksheet2.write(row, period_dic[result[2]], result[3], cell_format1)
                                if result[3] > 0:
                                    income_col_subtotal[result[2]] += result[3]
                                else:
                                    expense_col_subtotal[result[2]] += result[3]

                                if result[6] not in income_row_subtotal.keys():

                                    income_row_subtotal[result[6]] = result[3]
                                else:
                                    income_row_subtotal[result[6]] += result[3]

                                worksheet2.write(row, 2, income_row_subtotal[result[6]], cell_format1)
                                i = 0
                            else:
                                i += 2
                                worksheet2.write(row, period_dic[result[2]], result[3], cell_format1)
                                if result[3] > 0:
                                    income_col_subtotal[result[2]] += result[3]
                                else:
                                    expense_col_subtotal[result[2]] += result[3]
                                if result[6] not in income_row_subtotal.keys():
                                    income_row_subtotal[result[6]] = result[3]
                                else:
                                    income_row_subtotal[result[6]] += result[3]
                                worksheet2.write(row, 2, income_row_subtotal[result[6]], cell_format1)
                                i = 0
                    row += 1
                    worksheet2.merge_range(row, 0, row, 1, u"Нийт орлого", header4)
                    total_income = 0
                    for j in period_dic.keys():
                        if j == str(date_from.year) + '-' + str(date_from.month):
                            for days_key, value in period_day_dic.items():
                                worksheet2.write(row, value, '', header4)
                        worksheet2.write(row, period_dic[j], income_col_subtotal[j], header4)
                        income_col_total[j] += income_col_subtotal[j]
                        total_income += income_col_subtotal[j]
                    worksheet2.write(row, 2, total_income, header4)
                    row += 1
                    worksheet2.merge_range(row, 0, row, 1, u"Нийт зардал", header4)
                    total_expense = 0
                    for j in period_dic.keys():
                        if j == str(date_from.year) + '-' + str(date_from.month):
                            for days_key, value in period_day_dic.items():
                                worksheet2.write(row, value, '', header4)
                        worksheet2.write(row, period_dic[j], expense_col_subtotal[j], header4)
                        expense_col_total[j] += expense_col_subtotal[j]
                        total_expense += expense_col_subtotal[j]
                    worksheet2.write(row, 2, total_expense, header4)

                    row += 1

                total = 0
                worksheet2.merge_range(row, 0, row, 1, u"Нийт орлогын дүн", header5)
                worksheet2.write(row, 2, '', header5)
                for j in period_dic.keys():
                    worksheet2.write(row, period_dic[j], income_col_total[j], header5)
                    total += income_col_total[j]
                worksheet2.write(row, 2, total, header5)

                row += 1

                total = 0
                worksheet2.merge_range(row, 0, row, 1, u"Нийт зардлын дүн", header5)
                worksheet2.write(row, 2, '', header5)
                for j in period_dic.keys():
                    worksheet2.write(row, period_dic[j], expense_col_total[j], header5)
                    total += expense_col_total[j]
                worksheet2.write(row, 2, total, header5)
            else:
                for aa_id in account_account_ids:
                    income_col_subtotal = {}
                    expense_col_subtotal = {}
                    income_row_subtotal = {}
                    i = 0
                    col = 3
                    period_dic = {}
                    period_day_dic = {}
                    for period in periods:
                        period_start = datetime.strptime("{0}".format(period.date_start), '%Y-%m-%d')
                        if self.filter == 'filter_date':
                            date_from = datetime.strptime("{0}".format(self.date_from), '%Y-%m-%d')
                        else:
                            date_from = datetime.strptime("{0}".format(self.period_from.date_start), '%Y-%m-%d')
                        if period_start.month == date_from.month:
                            month = monthrange(date_from.year, date_from.month)
                            day = 1
                            key = str(period_start.year) + '-' + str(period_start.month)
                            period_dic[key] = col + month[1]
                            income_col_subtotal[key] = 0
                            expense_col_subtotal[key] = 0
                            while day <= month[1]:
                                day_key = datetime.strptime((str(period_start.year) + '-' + str(period_start.month) + '-' +str(day)), '%Y-%m-%d').date()
                                period_day_dic[str(day_key)] = col
                                day += 1
                                col += 1
                            col += 1
                        else:
                            col += 1
                            key = str(period_start.year) + '-' + str(period_start.month)
                            period_dic[key] = col
                            income_col_subtotal[key] = 0
                            expense_col_subtotal[key] = 0

                    worksheet2.merge_range(row, 0, row, 14+month[1], (aa_id[1] if aa_id[1] else '') + ' ' + str(aa_id[2]), header3)
                    worksheet2.write(row, 2, '', header3)
                    for j in period_dic.keys():
                        worksheet2.write(row, period_dic[j], '', header3)
                    records = {}
                    self._cr.execute("SELECT a.code, a.name, EXTRACT(year from l.date) || '-' || EXTRACT(month from l.date), \
                                        SUM(l.amount), aa.code AS account_code, aa.name AS account_name, aa.id, a.id  \
                                        FROM account_analytic_line l, account_analytic_account a, account_account aa \
                                        WHERE l.account_id=a.id AND l.general_account_id = aa.id AND a.type not in ('view') AND \
                                        l.date BETWEEN '%s' and '%s' AND l.general_account_id = %s \
                                        GROUP BY EXTRACT(year from l.date) || '-' || EXTRACT(month from l.date), a.id, aa.id \
                                        ORDER BY aa.code" % (date_start, date_end, aa_id[0]))
                    records = self._cr.fetchall()

                    temp_account_ids = {}
                    for record in records:
                        if record[7] not in temp_account_ids:
                            temp_account_ids[record[7]] = {'line': [record]}
                        else:
                            temp_account_ids[record[7]]['line'].append(record)

                    col = 0
                    for line in temp_account_ids:
                        prev_record = 0
                        for result in temp_account_ids[line]['line']:
                            if prev_record != result[7]:
                                prev_record = result[7]
                                row += 1
                                worksheet2.write(row, col + i, '%s' % result[0], cell_format3)
                                i += 1
                                worksheet2.write(row, col + i, '%s' % result[1], cell_format)
                                i += 1
                                for j in period_dic.keys():
                                    worksheet2.write(row, period_dic[j], '', cell_format1)
                                if result[2] == str(date_from.year) + '-' + str(date_from.month):
                                    for days_key, value in period_day_dic.items():
                                        worksheet2.write(row, value, self.days_count(days_key, days_key, record[6], record[7]), cell_format1)
                                worksheet2.write(row, period_dic[result[2]], result[3], cell_format1)
                                if result[3] > 0:
                                    income_col_subtotal[result[2]] += result[3]
                                else:
                                    expense_col_subtotal[result[2]] += result[3]

                                if result[7] not in income_row_subtotal.keys():

                                    income_row_subtotal[result[7]] = result[3]
                                else:
                                    income_row_subtotal[result[7]] += result[3]

                                worksheet2.write(row, 2, income_row_subtotal[result[7]], cell_format1)
                                i = 0
                            else:
                                i += 2
                                worksheet2.write(row, period_dic[result[2]], result[3], cell_format1)
                                if result[3] > 0:
                                    income_col_subtotal[result[2]] += result[3]
                                else:
                                    expense_col_subtotal[result[2]] += result[3]
                                if result[7] not in income_row_subtotal.keys():
                                    income_row_subtotal[result[7]] = result[3]
                                else:
                                    income_row_subtotal[result[7]] += result[3]
                                worksheet2.write(row, 2, income_row_subtotal[result[7]], cell_format1)
                                i = 0
                    row += 1
                    worksheet2.merge_range(row, 0, row, 1, u"Дүн", header4)
                    total_income = 0
                    total_expense = 0
                    for j in period_dic.keys():
                        worksheet2.write(row, period_dic[j], income_col_subtotal[j], header4)
                        income_col_total[j] += income_col_subtotal[j]
                        expense_col_total[j] += expense_col_subtotal[j]
                        total_income += income_col_subtotal[j]
                        total_expense += expense_col_subtotal[j]
                        if j == str(date_from.year) + '-' + str(date_from.month):
                            for days_key, value in period_day_dic.items():
                                worksheet2.write(row, value, '', header4)
                    worksheet2.write(row, 2, total_income + total_expense, header4)

                    row += 1

                total = 0
                worksheet2.merge_range(row, 0, row, 1, u"Нийт орлого", header5)
                worksheet2.write(row, 2, '', header5)
                for j in period_dic.keys():
                    worksheet2.write(row, period_dic[j], income_col_total[j], header5)
                    total += income_col_total[j]
                worksheet2.write(row, 2, total, header5)

                row += 1

                total = 0
                worksheet2.merge_range(row, 0, row, 1, u"Нийт зардал", header5)
                worksheet2.write(row, 2, '', header5)
                for j in period_dic.keys():
                    worksheet2.write(row, period_dic[j], expense_col_total[j], header5)
                    total += expense_col_total[j]
                worksheet2.write(row, 2, total, header5)

            worksheet2.set_zoom(100)

            workbook.close()
            out = base64.encodestring(output.getvalue())
            timestr = time.strftime('%Y-%m-%d')
            now = datetime.strptime(timestr, "%Y-%m-%d")
            file_name = u'Шинжилгээний дансны тайлан '

            report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

            # call export function
            return report_excel_output_obj.export_report()

            excel_id = self.env['report.excel.output'].create({'data': out, 'name': file_name + '.xlsx'})

            return {
                'name': 'Export Result',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'report.excel.output',
                'res_id': excel_id,
                'view_id': False,
                'context': context,
                'type': 'ir.actions.act_window',
                'target': 'new',
                'nodestroy': True,
            }
        else:
            return super(AnalyticAccountReport, self).export()

    def days_count(self, date_start,date_end, aa_id, account_id):
        self._cr.execute("SELECT a.code, a.name, l.date, \
                                                            SUM(l.amount), aa.code AS account_code, aa.name AS account_name, aa.id, a.id \
                                                            FROM account_analytic_line l, account_analytic_account a, account_account aa \
                                                            WHERE l.account_id=a.id AND l.general_account_id = aa.id AND a.type not in ('view') AND \
                                                            l.date BETWEEN '%s' and '%s' AND l.general_account_id = %s  AND l.account_id = %s \
                                                            GROUP BY l.date, a.id, aa.id \
                                                             ORDER BY aa.code" % (date_start, date_end, aa_id, account_id))
        days_records = self._cr.fetchall()
        return days_records[0][3] if days_records else 0