# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################

{
    'name': 'Mongolian Analytic Account Cash Report',
    'version': '1.0',
    'depends': ['l10n_mn_analytic_report'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """Мөнгөний мэдээний тайлан""",
    'data': [
        'wizard/cash_report_view.xml',
    ],
    "auto_install": False,
    "installable": True,
}
