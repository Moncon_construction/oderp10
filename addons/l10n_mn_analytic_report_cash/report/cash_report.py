# -*- encoding: utf-8 -*-
##############################################################################

import time
import base64
import xlsxwriter
import pytz
from xlsxwriter.utility import xl_rowcol_to_cell
from io import BytesIO
from datetime import datetime

from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles  # @UnresolvedImport


class CashReport(models.TransientModel):
    """
       Мөнгөний Мэдээ
    """
    _name = 'cash.report'
    _description = "Mongolian Cash Report"

    company_id = fields.Many2one('res.company', string='Company', required=True,default=lambda self: self.env['res.company']._company_default_get('cash.report'))
    date_from = fields.Date("Start Date", required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    date_to = fields.Date("End Date", required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    report_type = fields.Selection([('group_day', 'Group by Day'),
                                    ('group_month', 'Group By Month'),
                                    ('group_journal', 'Group By Journal')], string='Report Type', default='group_month', required=True)
    analytic_account_ids = fields.Many2many('account.analytic.account', string='Contract/Analytic')
    journal_ids = fields.Many2many('account.journal', string='Cash, Bank')

    def get_bank_statemet_lines(self, where):
        # Кассын орлого тооцоолох функц /өдрөөр/
        groupby = ", l.date, date_part('year', l.date) "
        select = " l.date as date, date_part('year', l.date) as year, "
        self._cr.execute(
            "SELECT" + select + " sum(l.amount) as amount, l.date as date, an.id as analytic_id, an.name as aa_name, l.account_id as account_id, acc.code as code, acc.name as account_name "
                                "FROM account_bank_statement_line l "
                                "LEFT JOIN account_analytic_share aa ON l.id = aa.bank_statement_line_id "
                                "LEFT JOIN account_analytic_account an ON an.id = aa.analytic_account_id "
                                "LEFT JOIN account_bank_statement s ON s.id = l.statement_id "
                                "LEFT JOIN account_account acc ON acc.id = l.account_id "
                                "WHERE s.company_id = %s AND l.date >= %s AND l.date <= %s and l.account_id is not null " + where +
            "GROUP BY an.id, l.account_id, an.name, acc.code, acc.name " + groupby +
            "ORDER BY an.id, l.account_id, l.date ", (self.company_id.id, self.date_from, self.date_to))
        return self._cr.dictfetchall()

    def get_bank_statemet_month_lines(self, where):
        # Кассын орлого тооцоолох функц /сар/
        self._cr.execute(
            "SELECT date_part('month', l.date) as date, date_part('year', l.date) as year, sum(l.amount) as amount, an.id as analytic_id, an.name as aa_name "
            "FROM account_bank_statement_line l "
            "LEFT JOIN account_analytic_share aa ON l.id = aa.bank_statement_line_id "
            "LEFT JOIN account_analytic_account an ON an.id = aa.analytic_account_id "
            "LEFT JOIN account_bank_statement s ON s.id = l.statement_id "
            "WHERE s.company_id = %s AND l.date >= %s AND l.date <= %s " + where +
            "GROUP BY an.id, an.name, date_part('month', l.date), date_part('year', l.date) "
            "ORDER BY an.id ", (self.company_id.id, self.date_from, self.date_to))
        return self._cr.dictfetchall()

    def get_sale_invoice_amount(self, analytic_id, date, year):
        # Борлуулалт, хөнгөлөлтийн дүнг буцаана
        where = "EXTRACT(month FROM ai.date) = %s AND " if self.report_type == 'group_month' else "ai.date = %s AND "
        end_date = datetime.strptime(self.date_to, DEFAULT_SERVER_DATE_FORMAT).replace(year=int(year), month=int(date), day=31) if self.report_type == 'group_month' else self.date_to
        if date == int(self.date_to[5:7]):
            end_date = self.date_to
        # Борлуулалт = Нэгж үнэ * Захиалсан тоо хэмжээ - борлуулалтын хөнгөлөлтийн дүн - буцаалтын нэхэмжлэхүүдийн дүнг
        self._cr.execute(
            "SELECT (sum(ail.quantity * ail.price_unit - ail.discount) - sum(table1.refund_total)) as amount, sum(ail.discount) as discount "
            "FROM account_invoice ai "
            "LEFT JOIN account_invoice_line ail on ail.invoice_id = ai.id "
            "LEFT JOIN account_analytic_share aa ON ail.id = aa.invoice_line_id "
            "LEFT JOIN sale_order_line_invoice_rel solir on solir.invoice_line_id = ail.id "
            "LEFT JOIN sale_order_line sol on  sol.id = solir.order_line_id "
            "LEFT JOIN "
            "(SELECT a.amount_total as refund_total, a.refund_invoice_id as refund_invoice_id "
            "FROM account_invoice a "
            "WHERE a.state in ('open', 'paid'))  table1 on table1.refund_invoice_id = ai.id "
            "WHERE " + where + " ail.company_id = %s AND aa.analytic_account_id = %s AND ai.state in ('open', 'paid') and ai.refund_invoice_id is null and ai.date <=  %s" ,
            (int(date) if self.report_type == 'group_month' else date, self.company_id.id, analytic_id, end_date))

        result = self._cr.dictfetchall()
        amount = result[0]['amount'] if len(result) > 0 else 0
        discount = result[0]['discount'] if len(result) > 0 else 0
        return amount, discount

    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Cash Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title_float = book.add_format(ReportExcelCellStyles.format_sub_title_float)
        format_title_center = book.add_format(ReportExcelCellStyles.format_title_small)
        format_title_center_left = book.add_format(ReportExcelCellStyles.format_title_left)
        format_title_center_right = book.add_format(ReportExcelCellStyles.format_title_float)
        format_content_left = book.add_format(ReportExcelCellStyles.format_content_left)
        format_content_text_footer = book.add_format(ReportExcelCellStyles.format_filter)
        format_content_center = book.add_format(ReportExcelCellStyles.format_content_center)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_sub_title = book.add_format(ReportExcelCellStyles.format_sub_title)
        format_sub_title_center = book.add_format(ReportExcelCellStyles.format_sub_title_center)

        # parameters
        rowx = 0
        col = 0
        rowlist = []

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('cash_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet('Cash Report')
        sheet.set_portrait()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.39, 0.39, 0.39, 0.39)  # 1cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})

        # compute column
        sheet.set_column('A:A', 20)
        sheet.set_column('B:B', 20)
        sheet.set_column('C:C', 20)
        sheet.set_column('D:D', 20)

        # get timezone
        local = pytz.UTC
        if self.env.user.tz:
            local = pytz.timezone(self.env.user.tz)

        # create name
        sheet.merge_range(rowx, 0, rowx, 3, _(u'Company Name: %s') % self.company_id.name, format_filter)
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, 3, report_name.upper(), format_name)
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, 3, _(u'Duration: %s - %s') % (
            pytz.utc.localize(datetime.strptime(self.date_from, '%Y-%m-%d')).astimezone(local).strftime('%Y-%m-%d'),
            pytz.utc.localize(datetime.strptime(self.date_to, '%Y-%m-%d')).astimezone(local).strftime('%Y-%m-%d')), format_filter)

        rowx += 1
        sheet.write(rowx, col, _('Date'), format_title_center)
        sheet.write(rowx, col + 1, _('Income'), format_title_center)
        sheet.write(rowx, col + 2, _('Sale'), format_title_center)
        sheet.write(rowx, col + 3, _('Discount '), format_title_center)

        where = ''
        if len(self.analytic_account_ids) > 0:
            analytic_ids = self.analytic_account_ids.ids
            where += ' AND aa.id in (' + ','.join(map(str, analytic_ids)) + ') '

        if len(self.journal_ids) > 0:
            journal_ids = self.journal_ids.ids
            where += ' AND l.journal_id in (' + ','.join(map(str, journal_ids)) + ') '

        lines = self.get_bank_statemet_month_lines(where) if self.report_type == 'group_month' else self.get_bank_statemet_lines(where)
        rowx += 1
        if lines:
            analytic_id = False
            date = False
            account_id = False
            qty = 0
            counter = 1
            if self.report_type == 'group_journal':
                # Журналаар бүлэглэх
                for line in lines:
                    # Эхний ангиллыг/данс зурна
                    if not analytic_id and not account_id:
                        sheet.merge_range(rowx, 0, rowx, 3, _(u'Business') + u": %s" % line['aa_name'], format_sub_title)
                        rowd = rowx
                        rowx += 1
                        sheet.merge_range(rowx, 0, rowx, 3, line['code'] + ' ' + line['account_name'], format_content_left)
                        rowx += 1
                    # Дараагийн ангиллыг/данс зурна
                    elif analytic_id and analytic_id != line['analytic_id']:
                        sheet.write(rowx, 0, _('Sub Total'), format_sub_title_center)
                        for h in range(1, 4):
                            sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_title_float)

                        if rowx not in rowlist:
                            rowlist.append(rowx)
                        rowx += 1
                        sheet.merge_range(rowx, 0, rowx, 3, _(u'Business') + u": %s" % line['aa_name'], format_sub_title)
                        rowd = rowx
                        rowx += 1
                        sheet.merge_range(rowx, 0, rowx, 3, line['code'] + ' ' + line['account_name'], format_content_left)
                        rowx += 1
                    # Дараагийн ангиллыг/данс зурна
                    elif account_id and account_id != line['account_id']:
                        sheet.write(rowx, 0, _('Sub Total'), format_sub_title_center)
                        for h in range(1, 4):
                            sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_title_float)
                        rowx += 1
                        sheet.merge_range(rowx, 0, rowx, 3, line['code'] + ' ' + line['account_name'], format_content_left)
                        rowd = rowx
                        rowx += 1
                    amount, discount = self.get_sale_invoice_amount(line['analytic_id'], line['date'], line['year'])
                    if date != line['date']:
                        sheet.write(rowx, 0, line['date'], format_content_center)
                        sheet.write(rowx, 1, line['amount'], format_content_float)
                        sheet.write(rowx, 2, amount, format_content_float)
                        sheet.write(rowx, 3, discount, format_content_float)
                        date = line['date']
                    else:
                        sheet.write(rowx, 0, line['date'], format_content_center)
                        sheet.write(rowx, 1, line['amount'], format_content_float)
                        sheet.write(rowx, 2, amount, format_content_float)
                        sheet.write(rowx, 3, discount, format_content_float)
                        date = line['date']
                    rowx += 1
                    counter += 1
                    analytic_id = line['analytic_id']
                    account_id = line['account_id']
            else:
                # Өдрөөр, сараар бүлэглэх
                for line in lines:
                    # Эхний ангиллыг зурна
                    if not analytic_id:
                        sheet.merge_range(rowx, 0, rowx, 3, _(u'Business') + u": %s" % line['aa_name'], format_sub_title)
                        rowd = rowx
                        rowx += 1
                    # Дараагийн ангиллыг зурна
                    elif analytic_id and analytic_id != line['analytic_id']:
                        sheet.write(rowx, 0, _('Sub Total'), format_sub_title_center)
                        for h in range(1, 4):
                            sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1,  h) + ')}', format_title_float)

                        if rowx not in rowlist:
                            rowlist.append(rowx)
                        rowx += 1
                        sheet.merge_range(rowx, 0, rowx, 3, _(u'Business') + u": %s" % line['aa_name'], format_sub_title)
                        rowd = rowx
                        rowx += 1
                    amount, discount = self.get_sale_invoice_amount(line['analytic_id'], line['date'], line['year'])
                    if date != line['date']:
                        date11 = str(int(line['year'])) + '-' + '{:02}'.format(int(line['date'])) + _(u' month') if self.report_type == 'group_month' else line['date']
                        sheet.write(rowx, 0, date11, format_content_center)
                        sheet.write(rowx, 1, line['amount'], format_content_float)
                        sheet.write(rowx, 2, amount, format_content_float)
                        sheet.write(rowx, 3, discount, format_content_float)
                        date = line['date']
                    else:
                        date11 = str(int(line['year'])) + '.' + '{:02}'.format(int(line['date'])) + _(u' month') if self.report_type == 'group_month' else line['date']
                        sheet.write(rowx, 0, date11, format_content_center)
                        sheet.write(rowx, 1, line['amount'], format_content_float)
                        sheet.write(rowx, 2, amount, format_content_float)
                        sheet.write(rowx, 3, discount, format_content_float)
                        date = line['date']
                    rowx += 1
                    counter += 1
                    analytic_id = line['analytic_id']

            sheet.write(rowx, 0, _('Sub Total'), format_sub_title_center)
            for h in range(1, 4):
                sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_title_float)
            if rowx not in rowlist:
                rowlist.append(rowx)
            rowx += 1

        # Нийт дүнг олох хэсэг
        sheet.write(rowx, 0, _('Total'), format_title_center_left)
        i = 1
        for n in range(1, 4):
            formula = '{=SUM('
            for r in rowlist:
                formula = formula + xl_rowcol_to_cell(r, i) + ','
            formula = formula[:-1] + ')}'
            sheet.write_formula(rowx, i, formula, format_title_center_right)
            i += 1

        rowx += 3
        sheet.merge_range(rowx, 0, rowx, 3, _('Made by:') + '........................' + '/' + self.env.user.name + '/', format_content_text_footer)
        rowx += 1
        sheet.merge_range(rowx, 0, rowx, 3, _('Check by:') + '........................' + '/', format_content_text_footer)
        sheet.write(rowx, 4, '/', format_content_text_footer)

        sheet.set_zoom(100)
        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()