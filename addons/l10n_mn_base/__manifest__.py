# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Base",
    'version': '1.0',
    'depends': [
        'base',
        'auth_signup'
    ],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Modules',
    'description': """
    Mongolian Base Module.
        - Mongolian Base Currency
    """,
    'data': [
        'res/res.lang.csv',
        'security/base_security.xml',
        'security/ir.model.access.csv',
        'data/res_province_district_data.xml',
        'data/res_sum_data.xml',
        'res/res_currency_view.xml',
        'res/res_company_view.xml',
        'res/res_town_view.xml',
        'res/res_district_view.xml',
        'res/res_partner_view.xml',
        'res/download_currency_cron.xml',
        'res/res_partner_category_view.xml',
        'res/res_sector_view.xml',
        'views/res_users_views.xml',

    ],
    'license': 'GPL-3',
}
