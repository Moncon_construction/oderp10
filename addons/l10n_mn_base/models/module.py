# -*- coding: utf-8 -*-
import odoo
from odoo import api, fields, models

class Module(models.Model):
    _inherit = "ir.module.module"

    @api.multi
    def button_install(self):
        res = super(Module, self).button_install()
        for module in self:
            if module.name == 'l10n_mn_account_cashbudget':
                companies = self.env['res.company'].search([])
                for company in companies:
                    company.write({'cashbudget_installed_date': fields.Date.context_today(self)})
        return res