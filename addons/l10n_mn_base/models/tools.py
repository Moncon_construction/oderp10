# -*- coding: utf-8 -*-
from odoo import _, api, fields


def get_qry_value(value):
    # avoid syntax error while executing query
    if isinstance(value, basestring):
        return value.replace("'", "''")
    elif value:
        return value
    return 'NULL'
