from odoo import api, fields, models


class Company(models.Model):
    _inherit = 'res.company'

    door_number = fields.Char(related='partner_id.door_number')
    mail_box = fields.Char(related='partner_id.po_box_number')
    province_district_id = fields.Many2one(related='partner_id.district')
    sum_khoroo_id = fields.Many2one(related='partner_id.sumkhoroo')
    logo_short = fields.Binary(string="Logo short")
    cashbudget_installed_date = fields.Date("Cash Budget Installed Date")

    @api.onchange('sum_khoroo_id')
    def _onchange_sum_khoroo_id(self):
        if self.sum_khoroo_id:
            self.province_district_id = self.sum_khoroo_id.district_id
            self.country_id = self.province_district_id.country_id

    @api.onchange('province_district_id')
    def _onchange_province_district_id(self):
        if self.province_district_id:
            if self.sum_khoroo_id.district_id != self.province_district_id:
                self.sum_khoroo_id = False
            self.country_id = self.province_district_id.country_id
        self.country_id = self.province_district_id.country_id

    first_sign = fields.Many2one('res.users', string="First signature")
    second_sign_cart = fields.Many2one('res.users', string="Second signature")
    executive_signature = fields.Many2one('res.users', string="Signature of Executive Director")
    genaral_accountant_signature = fields.Many2one('res.users', string="Signature of General Accountant")
    accountant_signature = fields.Many2one('res.users', string="Signature of accountant")
    print_image_signature = fields.Boolean(string='Print image signature')
    is_currency_cron_automate = fields.Boolean(string='Is currency rate cron automate?', default=True)