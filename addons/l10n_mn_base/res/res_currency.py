# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import re
import logging
import urllib
from datetime import datetime, timedelta

from odoo import api, fields, models, _
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)


class Currency(models.Model):
    _inherit = "res.currency"

    integer = fields.Char(string='Integer', size=128)
    divisible = fields.Char(string='Divisible', size=128)
    """
    Монгол банкны сайтаас үсгэн код, жил, сар,өдрөөр
    тухайн өдрийн ханшийн өгөгдлийг татагч функц.
    """

    def get_currency_data(self, code, year, month, day):
        REGX_PATTERN = u"<tr>\\s*<td.*>.*</td>\\s*<td.*>(?P<name>.*)</td>\\s*<td.*><span id=\"ContentPlaceHolder1_lbl(?P<code>\\" + code + u")\">(?P<rate>.*)</span></td></tr>"
        REGX_PATTERN = u"<tr>\\s*<td.*>.*</td>\\s*<td.*>(?P<name>.*)</td>\\s*<td.*><span id=\"ContentPlaceHolder1_lbl" + code + "(?P<code>\\.*)\">(?P<rate>.*)</span></td></tr>"
        contents = ''
        url = 'https://www.mongolbank.mn/dblistofficialdailyrate.aspx?vYear=' + year + '&vMonth=' + month + '&vDay=' + day
        browser = urllib.urlopen(url)
        response = browser.getcode()
        data = []
        if response == 200:
            contents = browser.read()
            html_text = contents
            if html_text is not None:
                pattern = re.compile(REGX_PATTERN)
                for r in pattern.finditer(html_text):
                    data.append(r.groupdict())
            else:
                raise UserError(_("Connect Error"))
        else:
            raise UserError(_("Connect Error"))
        return data

    """
    Ханш татсан өдрөөс өмнөх 7 хоногийн валютын ханшийг
    Монгол банкны сайтаас татаж үүсгэгдээгүй бүртгэгдээгүй
    ханшийг үүсгэнэ.
    """

    @api.multi
    def download_currency(self):
        if self:
            rate_obj = self.env['res.currency.rate']
            day = timedelta(days=1)
            created_date_val = (datetime.now() - day).replace(hour=0, minute=0, second=0, microsecond=0)

            for i in range(7):
                rates = rate_obj.search([('name', '=', str(created_date_val + day)), ('currency_id', '=', self.id)])
                if not rates:
                    year_i = created_date_val.year
                    month_i = created_date_val.month
                    day_i = created_date_val.day
                    datas = self.get_currency_data(self.name, str(year_i), str(month_i), str(day_i))
                    for data in datas:
                        vals = {}
                        vals['currency_id'] = self.id,
                        vals['name'] = str(created_date_val + day)
                        alter_rate = data['rate'].replace(",", "")
                        vals['alter_rate'] = alter_rate
                        vals['company_id'] = False
                        rate_obj.create(vals)
                        _logger.info('')
                        _logger.info('CURRENCY DOWNLOADING')
                        _logger.info('CURRENCY CODE:::> %s' % self.name)
                        _logger.info('CURRENCY RATE:::> %s' % alter_rate)
                        _logger.info('CURRENCY DATE:::> %s' % str(created_date_val + day))
                created_date_val = (created_date_val - day).replace(hour=0, minute=0, second=0, microsecond=0)

    @api.multi
    def download_currency_rate(self):
        company_id = self._context.get('company_id') or self.env['res.users']._get_company().id
        if company_id:
            check = self.env['res.company'].browse(company_id).is_currency_cron_automate
            if check:
                active_currs = self.search([('active', '=', True)])
                for curr in active_currs:
                    curr.download_currency()

    @api.multi
    def _compute_current_rate(self):
        date = self._context.get('date') or fields.Datetime.now()
        company_id = self._context.get('company_id') or self.env['res.users']._get_company().id
        # the subquery selects the last rate before 'date' for the given currency/company
        query = """SELECT c.id, (SELECT r.alter_rate FROM res_currency_rate r
                                  WHERE r.currency_id = c.id AND r.name <= %s
                                    AND (r.company_id IS NULL OR r.company_id = %s)
                               ORDER BY r.name DESC
                                  LIMIT 1) AS rate
                   FROM res_currency c
                   WHERE c.id IN %s"""
        self._cr.execute(query, (date, company_id, tuple(self.ids)))
        currency_rates = dict(self._cr.fetchall())
        for currency in self:
            currency.rate = currency_rates.get(currency.id) or 1.0

    @api.multi
    def compute(self, from_amount, to_currency, round=True):
        """ Convert `from_amount` from currency `self` to `to_currency`. """
        self, to_currency = self or to_currency, to_currency or self
        assert self, "compute from unknown currency"
        assert to_currency, "compute to unknown currency"
        # apply conversion rate
        if self == to_currency:
            to_amount = from_amount
        else:
            to_amount = from_amount * self._get_conversion_rate(to_currency, self)
        # apply rounding
        return to_currency.round(to_amount) if round else to_amount


class CurrencyRate(models.Model):
    _inherit = "res.currency.rate"

    _sql_constraints = [
        ('model_name_uniq', 'unique(name,currency_id)',
         ("There is already a rule defined on this model\n"
          "You cannot define another: please edit the existing one."))
    ]

    rate = fields.Float(digits=(12, 6), help='The rate of the currency to the currency of rate 1')
    alter_rate = fields.Float(digits=(12, 6), string='Rate', help='The rate of the currency to the currency of rate 1')
    company_id = fields.Many2one('res.company', string='Company',
                                 default=lambda self: self.env.user.company_id.id, readonly=True)

    @api.onchange('alter_rate')
    def onchange_alter_rate(self):
        rate = 0
        if self.alter_rate > 0:
            rate = 1.0 / self.alter_rate
        self.rate = rate
