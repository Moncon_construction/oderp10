# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class District(models.Model):
    _name = 'res.district'
    _description = 'Province/District'

    name = fields.Char(string='Province/District name', required=True, translate=True, help='Full name of Province/District')
    country_id = fields.Many2one('res.country', string='Country', required=True)
    code = fields.Char("Province/District code")

    @api.multi
    @api.constrains('name', 'code')
    def _check_district_name_code(self):
        for record in self:
            res_district = self.env['res.district'].search([('code', '=', record.code), ('name', '=', record.name)])
            if len(res_district) > 1:
                raise ValidationError(_("This district already exists"))


class SumKhoroo(models.Model):
    _name = 'res.khoroo'
    _description = 'Sum/Khoroo'

    name = fields.Char(string='Sum/Khoroo name', required=True, translate=True, help='Full name of Sum/Khoroo.')
    district_id = fields.Many2one('res.district', string='Province/District', required=True)
    code = fields.Char("Sum/Khoroo code")

    @api.multi
    @api.constrains('name', 'code')
    def _check_khoroo_name_code(self):
        for record in self:
            res_khoroo = self.env['res.khoroo'].search([('code', '=', record.code), ('name', '=', record.name)])
            if len(res_khoroo) > 1:
                raise ValidationError(_("This khoroo already exists"))