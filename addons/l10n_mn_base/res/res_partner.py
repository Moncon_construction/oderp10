# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo import exceptions
from odoo.exceptions import UserError, AccessError

class Partner(models.Model):
    _inherit = "res.partner"

    @api.onchange('name')
    def onchange_company(self):
        for res in self:
            self.customer = True

    door_number = fields.Char("Door/House number")
    po_box_number = fields.Char("PO box number")
    district = fields.Many2one('res.district', string='Province/District')
    sumkhoroo = fields.Many2one('res.khoroo', string='Sum/Khoroo name')
    town_id = fields.Many2one('res.town', string='Town name')
    is_resident = fields.Boolean(default=True, string="Is a resident of Mongolia")
    surname = fields.Char('Surname')
    vat_document_number = fields.Char('VAT document number')
    is_create_partner = fields.Boolean('Create same partner', default=False)
    customer = fields.Boolean(string='Is a Customer', track_visibility='onchange',
                              help="Check this box if this contact is a customer.")
    supplier = fields.Boolean(string='Is a Vendor', default=True,
                              help="Check this box if this contact is a vendor. "
                                   "If it's not checked, purchase people will not see it when encoding a purchase order.")

    category_hch_id = fields.Many2one('res.partner.category.hierarchy', string="Category Hierarchy", invisible="1")
    category_hierarchy_id = fields.Many2one('res.partner.category.hierarchy', string="Category",company_dependent=True,store=True)
    facebook = fields.Char(string='Facebook Account')
    twitter_account = fields.Char(string='Twitter Account')
    linkedin_account = fields.Char(string='Linkedin Account')
    wechat_account = fields.Char(string='WeChat Account')

    @api.onchange('category_hierarchy_id')
    def onchange_category(self):
        if self.category_hierarchy_id.property_account_receivable_id and self.category_hierarchy_id.property_account_payable_id:
            self.property_account_receivable_id = self.category_hierarchy_id.property_account_receivable_id
            self.property_account_payable_id = self.category_hierarchy_id.property_account_payable_id

    @api.onchange('district')
    def set_detail(self):
        for obj in self:
            if obj.sumkhoroo.district_id != obj.district:
                obj.sumkhoroo = None
            obj.country_id = self.district.country_id

    @api.onchange('country_id')
    def set_district(self):
        for obj in self:
            if obj.district.country_id != obj.country_id:
                obj.district = None

    @api.multi
    def write(self, vals):
        # Захиалагч дээр утасны дугаар нь өмнө нь бүртгэгдсэн эсэхийг шалгана.
        if vals.get('phone') is not False and vals.get('phone') is not None or vals.get('mobile') is not False and vals.get('mobile') is not None:
            phone = '%' + ((vals.get('mobile') if vals.get('phone') is False else vals.get('phone')) if 'phone' in vals else vals.get('mobile')) + '%'
            mobile = '%' + ((vals.get('phone') if vals.get('mobile') is False else vals.get('mobile')) if 'mobile' in vals else vals.get('phone')) + '%'
            self._cr.execute(""" SELECT name
                               FROM res_partner
                               WHERE mobile  LIKE %s or phone LIKE %s and id != %s
                               """, (phone, mobile, self.id))
            partners = self._cr.fetchall()
            if len(partners) != 0:
                p_name = ''
                for partner in partners:
                    p_name = p_name + ',' + partner[0].strip()
                desc = _("The partner's number [%s] is registered in the system. \n Please check duplicate create partner.!") % (p_name)
                if 'is_create_partner' in vals:
                    if vals.get('is_create_partner') is False:
                        raise UserError(desc)
                elif self.is_create_partner is False:
                    raise UserError(desc)
        return super(Partner, self).write(vals)

    @api.model
    def create(self, vals):
        if not self.env.user.has_group('base.group_partner_manager') and ('is_company' not in vals.keys() or ('is_company' in vals.keys() and vals['is_company'] == False)):
            IrModuleModule = self.env['ir.module.module']
            stock = IrModuleModule.sudo().search([('name', '=', 'stock')])
            if stock:
                if stock.state != 'to install':
                    raise AccessError(_('You have no right for create partner !!! User: %s') % str(self.env.user.login))
            else:
                raise AccessError(_('You have no right for create partner !!! User: %s') % str(self.env.user.login))
        # Захиалагч дээр утасны дугаар нь өмнө нь бүртгэгдсэн эсэхийг шалгана.
        if (vals.get('phone') is not False and vals.get('phone') is not None) or (vals.get('mobile') is not False and vals.get('mobile') is not None):
            phone = '%' + ((vals.get('mobile') if vals.get('phone') is False else vals.get('phone')) if 'phone' in vals else vals.get('mobile')) + '%'
            mobile = '%' + ((vals.get('phone') if vals.get('mobile') is False else vals.get('mobile')) if 'mobile' in vals else vals.get('phone')) + '%'
            self._cr.execute(""" SELECT name
                                       FROM res_partner
                                       WHERE mobile  LIKE %s or phone LIKE %s
                                       """, (phone, mobile))
            partners = self._cr.fetchall()
            if len(partners) != 0:
                if 'is_create_partner' in vals:
                    if vals.get('is_create_partner') is False:
                        p_name = ''
                        for partner in partners:
                            p_name = p_name + ',' + partner[0].strip()
                        desc = _("The partner's number [%s] is registered in the system. \n Please check duplicate create partner.!") % (p_name)
                        raise UserError(desc)
        return super(Partner, self).create(vals)

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        res = super(Partner, self).name_search(name, args, operator, limit)
        partner_search = self.env['res.partner']
        if name:
            partner_search = self.search(['|', ('phone', operator, name), '|', ('mobile', operator, name), ('surname', operator, name)], limit=limit)
            partner_search = partner_search.name_get()
        res.extend(partner_search)
        res = list(set(res))
        return res

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            name = ''
            if record.category_hierarchy_id:
                name = "%s, %s" % (record.name, record.category_hierarchy_id.name)
                if record.parent_id:
                    name = "%s, %s, %s" % (record.parent_id.name, record.name, record.category_hierarchy_id.name)
            else:
                name = "%s" % record.name
                if record.parent_id:
                    name = " %s, %s" % (record.parent_id.name, record.name)
            result.append((record.id, "%s" % name))
        return result
