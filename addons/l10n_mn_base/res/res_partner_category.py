# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################
from odoo import api, fields, models


class ResPartnerCategoryHierarchy(models.Model):
    _name = "res.partner.category.hierarchy"
    _description = "Partner Category Hierarchy"

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            result.append((record.id, "[%s] %s" % (record.code, record.name)))
        return result

    code = fields.Char("Code", required=True)
    name = fields.Char("Name", required=True)
    type = fields.Selection([('view', 'View'),
                             ('normal', 'Normal')], default='normal', required=True, string="Type")
    parent_id = fields.Many2one('res.partner.category.hierarchy', string='Parent Partner Category', domain=[('type', '=', 'view')])
    property_account_receivable_id = fields.Many2one('account.account',string="Account Receivable",required=True)
    property_account_payable_id = fields.Many2one('account.account', string="Account Payable",required=True)