# -*- coding: utf-8 -*-

from odoo import fields, models

class Sector(models.Model):
    _name = 'res.sector'
    _description = 'Sector'

    name = fields.Char(string='Sector name', required=True)
    company_id = fields.Many2one('res.company', string='Company')