# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError

class Town(models.Model):
    _name = 'res.town'
    _description = 'Town'

    name = fields.Char(string='Town name', required=True, translate=True, help='Full name of Town.')
    sumkhoroo_id = fields.Many2one('res.khoroo', string='Sum/Khoroo', required=True)
    code = fields.Char("Town code", required=True)