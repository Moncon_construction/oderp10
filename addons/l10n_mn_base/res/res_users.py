# -*- coding: utf-8 -*-

from odoo import fields, models, api, _


class ResUsers(models.Model):
    _inherit = 'res.users'

    signature_image = fields.Binary(attachment=True, string='Signature image')
    is_checked_company_user_signature_image = fields.Boolean(related="company_id.print_image_signature",
                                                             string='Is checked signature image')
    users_log_ids = fields.One2many('res.users.change.log', 'log_user_id', string='Users')

    @api.multi
    def write(self, vals):
        log_obj = self.env['res.users.change.log']
        group_obj = self.env['res.groups']

        for user in self:
            for k, v in vals.items():
                new_categ = ''
                old_categ = ''
                old = '-'
                new = '-'
                if k[:11] == 'sel_groups_':
                    groups = k[11:].split('_')
                    for g in groups:
                        self._cr.execute('''SELECT uid, gid FROM res_groups_users_rel WHERE uid = ''' + str(
                            user.id) + ''' and gid = ''' + str(g) + '''''')
                        fetched = self._cr.fetchone()
                        if fetched:
                            group = group_obj.browse(fetched[1])
                            if group.category_id:
                                old_categ = group.category_id.name + ' / '
                            old = old_categ + group.name
                    if v:
                        group = group_obj.browse(v)
                        if group.category_id:
                            new_categ = group.category_id.name + ' / '
                        new = new_categ + group.name
                elif k[:9] == 'in_group_':
                    in_group = k[9:]
                    self._cr.execute('''SELECT uid, gid FROM res_groups_users_rel WHERE uid = ''' + str(
                        user.id) + ''' and gid = ''' + in_group + ''' ''')
                    fetched = self._cr.fetchone()
                    if fetched:
                        group = group_obj.browse(fetched[1])
                        if group.category_id:
                            old_categ = group.category_id.name + ' / '
                        old = old_categ + group.name
                    if v:
                        group = group_obj.browse(int(in_group))
                        if group.category_id:
                            new_categ = group.category_id.name + ' / '
                        new = new_categ + group.name
                if old != '-' and new != '-':
                    log_vals = {
                        'log_user_id': user.id,
                        'old_settings': old,
                        'new_settings': new
                    }
                    log_obj.create(log_vals)
        res = super(ResUsers, self).write(vals)
        return res


class ResUsersChangeLog(models.Model):
    _name = "res.users.change.log"

    log_user_id = fields.Many2one('res.users', 'User')
    old_settings = fields.Char(string='Old settings')
    new_settings = fields.Char(string='New settings')
