# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Auto Inter-Company Transfer-Transection App ",
    'version': '1.0',
    'category': 'Mongolian Modules',
    'description': """
            Mongolian Auto Inter-Company Transfer-Transection App
    """,
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'depends': ['base', 'purchase', 'stock', 'account', 'base_setup'],
    'data': [
        'security/int_security.xml',
            'security/ir.model.access.csv',
            'data/inter_company_transfer_sequence.xml',
            'views/recompanysettingInherit.xml',
            'views/internal_company_transfer.xml',
            'views/return_inter_company_transfer.xml',
            'views/company_inherit_views.xml',
    ],
    'qweb': [],
    'auto_install': False,
    'installable': True,
    'images': ["static/description/Banner.png"],
}
