# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from itertools import groupby
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models, SUPERUSER_ID, _
from odoo.exceptions import UserError, ValidationError, Warning
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.misc import formatLang
from odoo.tools import html2plaintext
import odoo.addons.decimal_precision as dp


class PurchaseOrderInherit(models.Model):
    _inherit = 'purchase.order'

    @api.depends('internal_id')
    def _compute_internal(self):
        for internal in self:
            internal_transfer = self.env['inter.transfer.company'].search(
                [('id', '=', internal.internal_id.id)])
            if internal_transfer:
                internal.inter_transfer_count = len(internal_transfer)

    internal_id = fields.Many2one('inter.transfer.company', copy=False)
    inter_transfer_count = fields.Integer(
        string="Internal Transfer", compute="_compute_internal", copy=False, default=0, store=True)

    def action_view_internal(self):
        action = self.env.ref(
            'l10n_mn_bi_inter_company_transfer.stock_inter_company_transfer_action').read()[0]
        domain = [('id', '=', self.internal_id.id)]
        transfer = self.env['inter.transfer.company'].search(domain)
        action['domain'] = [('id', '=', transfer.id)]
        return action

    @api.multi
    def button_confirm(self):
        res = super(PurchaseOrderInherit, self).button_confirm()

        company_partner_id = self.env['res.company'].search(
            [('partner_id', '=', self.partner_id.id)])
        so_available = self.env['sale.order'].search(
            [('client_order_ref', '=', self.name)])
        setting_id = self.env.user.company_id
        invoice_object = self.env['account.invoice']
        invoice_line_obj = self.env['account.invoice.line']
        journal = self.env['account.journal'].search(
            [('type', '=', 'purchase')], limit=1)
        internal_id = self.env['inter.transfer.company']
        inter_transfer_lines = self.env['inter.transfer.company.line']
        picking_access = False
        create_invoice = False
        validate_invoice = False
        bill_id = False
        if self.env.user.has_group('l10n_mn_bi_inter_company_transfer.group_ict_manager_access') and setting_id.allow_auto_intercompany:

            if company_partner_id.id:
                if not so_available.id:
                    if setting_id.validate_picking:
                        for receipt in self.picking_ids:
                            for move in receipt.move_ids_without_package:
                                move.write(
                                    {'quantity_done': move.product_uom_qty})

                                if self.internal_id.id == False and self.partner_ref == False:
                                    data = inter_transfer_lines._prepare_internal_from_move_line(
                                        move)
                                    internal_line = inter_transfer_lines.new(
                                        data)
                                    inter_transfer_lines += internal_line
                            receipt.button_validate()
                            if receipt.state == 'done':
                                picking_access = True
                    else:
                        for receipt in self.picking_ids:
                            for move in receipt.move_ids_without_package:

                                if self.internal_id.id == False and self.partner_ref == False:
                                    data = inter_transfer_lines._prepare_internal_from_move_line(
                                        move)
                                    internal_line = inter_transfer_lines.new(
                                        data)
                                    inter_transfer_lines += internal_line

                    if setting_id.create_invoice:
                        if setting_id.create_invoice:
                            ctx = dict(self._context or {})

                            ctx.update({
                                'type': 'in_invoice',
                                'default_purchase_id': self.id,
                                'default_currency_id': self.currency_id.id,
                                'default_origin': self.name,
                                'default_reference': self.name,
                            })
                            bill_id = invoice_object.with_context(ctx).create({'partner_id': self.partner_id.id,
                                                                               'currency_id': self.currency_id.id,
                                                                               'company_id': self.company_id.id,
                                                                               'type': 'in_invoice',
                                                                               'journal_id': journal.id,
                                                                               'vendor_bill_purchase_id': self.id,
                                                                               'purchase_id': self.id,
                                                                               'reference': self.name})
                            for line in self.order_line - invoice_object.invoice_line_ids.mapped('purchase_line_id'):
                                data = bill_id._prepare_invoice_line_from_po_line(
                                    line)
                                data.update({
                                    'purchase_id': self.id,
                                    'invoice_id': bill_id.id
                                })
                                invoice_line = invoice_line_obj.new(data)
                                invoice_line._set_additional_fields(self)
                                bill_id.invoice_line_ids += invoice_line
                            bill_id.payment_term_id = self.payment_term_id
                            bill_id.origin = ', '.join(self.mapped('name'))
                            bill_id.reference = ', '.join(self.filtered(
                                'partner_ref').mapped('partner_ref')) or bill_id.reference
                        # else:
                        #     raise Warning(_('Please Check Your delivery not completed.'))
                    if setting_id.validate_invoice:
                        if bill_id:
                            bill_id.action_invoice_open()
                        else:

                            raise Warning(
                                _('Please First give access to Create Bill.'))

                    if self.internal_id.id == False and self.partner_ref == False:
                        if bill_id:
                            internal_transfer_id = internal_id.create({
                                'purchase_id': self.id,
                                'state': 'process',
                                'apply_type': 'sale',
                                'currency_id': self.currency_id.id,
                                'invoice_id': [(6, 0, bill_id.ids)],
                                'to_warehouse': self.picking_type_id.warehouse_id.id})
                            self.internal_id = internal_transfer_id.id
                            internal_transfer_id.product_lines += inter_transfer_lines
                        else:
                            internal_transfer_id = internal_id.create({
                                'purchase_id': self.id,
                                'state': 'process',
                                'apply_type': 'sale',
                                'currency_id': self.currency_id.id,
                                'to_warehouse': self.picking_type_id.warehouse_id.id})

                            self.internal_id = internal_transfer_id.id
                            internal_transfer_id.product_lines += inter_transfer_lines
                    else:
                        created_id = internal_id.search(
                            [('id', '=', self.internal_id.id)])
                        created_id.write({
                            'purchase_id': self.id,
                            'invoice_id': [(6, 0, bill_id.ids)]
                        })
                    if self.internal_id.id:
                        self.internal_id = self.internal_id.id

            if not so_available.id:
                if company_partner_id.id:
                    if self._context.get('stop_so') == True:
                        pass

                    else:

                        receipt = self._create_so_from_po(company_partner_id)
        return True

    def _create_so_from_po(self, company):
        self = self.with_context(
            force_company=company.id, company_id=company.id)
        company_partner_id = self.env['res.company'].search(
            [('partner_id', '=', self.partner_id.id)])
        current_company_id = self.env.user.company_id
        sale_order = self.env['sale.order']
        picking_validate = False
        invoice = False
        setting_id = self.env.user.company_id
        sale_order_line = self.env['sale.order.line']
        so_vals = self.sudo().get_so_values(
            self.name, company_partner_id, current_company_id)
        so_id = sale_order.sudo().create(so_vals)
        for line in self.order_line.sudo():
            so_line_vals = self.sudo().get_so_line_data(company_partner_id, so_id.id, line)
            sale_order_line.sudo().create(so_line_vals)
        if so_id.client_order_ref:
            so_id.client_order_ref = self.name
            so_id.partner_ref = self.name
        ctx = dict(self._context or {})
        ctx.update({
            'company_partner_id': company_partner_id.id,
            'current_company_id': current_company_id.id
        })
        so_id.sudo().action_confirm()
        if setting_id.validate_picking:
            for picking in so_id.picking_ids:
                for move in picking.move_ids_without_package:
                    move.write({'quantity_done': move.product_uom_qty})
                picking.button_validate()
                if picking.state == 'done':
                    picking_validate = True
        if setting_id.create_invoice:

            invoice = so_id.sudo().action_invoice_create()
            # else:
            #     raise Warning(_('Please Check Your delivery not completed.'))

        if setting_id.validate_invoice:
            if invoice:
                invoice_id = self.env['account.invoice'].browse(invoice)
                invoice_id.sudo().action_invoice_open()
            else:
                raise Warning(_('Please First give access to Create invoice.'))
        setting_id = self.env['res.config.settings'].search(
            [], order="id desc", limit=1)

        if self.internal_id.id:
            bill_details = []
            bill_details.append(so_id.invoice_ids.id)
            if len(self.internal_id.invoice_id) > 0:
                for inv in self.internal_id.invoice_id:
                    bill_details.append(inv.id)
            self.internal_id.update({
                'sale_id': so_id.id,
                'invoice_id': [(6, 0, bill_details)],
                'from_warehouse': so_id.warehouse_id.id
            })
            so_id.internal_id = self.internal_id.id
        return so_id

    @api.model
    def get_so_line_data(self, company, sale_id, line):

        fpos = line.order_id.fiscal_position_id or line.order_id.partner_id.property_account_position_id
        taxes = line.product_id.taxes_id.filtered(
            lambda r: not line.company_id or r.company_id == company)
        tax_ids = fpos.map_tax(
            taxes, line.product_id, line.order_id.partner_shipping_id) if fpos else taxes
        quantity = line.product_uom._compute_quantity(
            line.product_qty, line.product_id.uom_id)
        price = line.price_unit or 0.0
        price = line.product_uom._compute_price(price, line.product_id.uom_id)
        return {
            'name': line.name,
            'customer_lead': line.product_id and line.product_id.sale_delay or 0.0,
            'company_id': company.id,
            'tax_id': [(6, 0, tax_ids.ids)],
            'order_id': sale_id,
            'product_uom_qty': quantity,
            'product_id': line.product_id and line.product_id.id or False,
            'product_uom': line.product_id and line.product_id.uom_id.id or line.product_uom.id,
            'price_unit': price,
        }

    def get_so_values(self, name, company_partner_id, current_company_id):
        warehouse_id = self.env['stock.warehouse'].search(
            [('company_id', '=', company_partner_id.id)], limit=1)
        current_cmp_warehouse = self.env['stock.warehouse'].search(
            [('company_id', '=', current_company_id.id)], limit=1)
        if company_partner_id:
            if not company_partner_id.intercompany_warehouse_id:

                raise Warning(
                    _('Please Select Intercompany Warehouse On  %s.') % company_partner_id.name)
        other_cmp_warehouse = company_partner_id.intercompany_warehouse_id
        picking_type_id = self.env['stock.picking.type'].search([
            ('code', '=', 'internal'), ('default_location_src_id', '=', current_cmp_warehouse.lot_stock_id.id), (
                'default_location_dest_id', '=', other_cmp_warehouse.lot_stock_id.id)
        ], limit=1)
        so_name = self.env['ir.sequence'].sudo(
        ).next_by_code('sale.order') or '/'
        if self.internal_id.id:
            if self.internal_id.pricelist_id.id:
                pricelist_id = self.internal_id.pricelist_id.id
            else:
                pricelist_id = current_company_id.partner_id.property_product_pricelist.id
        else:
            pricelist_id = current_company_id.partner_id.property_product_pricelist.id
        return {
            'name': so_name,
            'partner_invoice_id': current_company_id.partner_id.id,
            'date_order': self.date_order,
            'fiscal_position_id': current_company_id.partner_id.property_account_position_id.id,
            'payment_term_id': current_company_id.partner_id.property_payment_term_id.id,
            'user_id': False,
            'company_id': company_partner_id.id,
            'warehouse_id': warehouse_id.id,
            'client_order_ref': name,
            'partner_id': current_company_id.partner_id.id,
            'pricelist_id': pricelist_id,
            'partner_shipping_id': current_company_id.partner_id.id
        }
