# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Business Plan",
    'version': '1.0',
    'depends': ['l10n_mn_project','l10n_mn_project_plan','l10n_mn_account_period','web_gantt'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
          Mongolian Business Plan
    """,
    'data': [
        'security/security_view.xml',
        'views/business_plan_view.xml',
        'views/business_plan_target_view.xml',
        'views/project_project_view.xml',
        'views/project_project_gantt_view.xml',
        'security/ir.model.access.csv',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False

}
