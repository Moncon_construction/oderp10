# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, _
from datetime import date, datetime
from odoo.exceptions import UserError

STATE_SELECTION = [
    ('draft', 'Draft'),
    ('sent', 'Sent'),
    ('approved', 'Approved'),
    ('done', 'Done'),
    ('cancel', 'Cancelled')
]

class BusinessPlan(models.Model):
    _name = "business.plan"
    _inherit = ['mail.thread']
    _description = "Business Plan"

    @api.multi
    def _compute_target_status_count(self):
        # Төлөвлөгөө дээр сонгосон зорилтуудын төлөв шалгаж тоолно
        for obj in self:
            total_performed_time = total_open_performed_time = process_value = total_planned_time = 0
            if obj.plan_line_ids:
                total_planned_time = len(obj.plan_line_ids)
                for line in obj.plan_line_ids:
                    process_value += line.percent * line.process_value_projects
                    if line.state == 'done':
                        total_performed_time += 1
                    else:
                        total_open_performed_time += 1
            obj.total_planned_time = total_planned_time
            obj.total_performed_time = total_performed_time
            obj.total_open_performed_time = total_open_performed_time
            obj.process_value = process_value/100

    name = fields.Char('Name', required=True, track_visibility='onchange')
    department_id = fields.Many2one('hr.department', 'Department', track_visibility='onchange')
    company_id = fields.Many2one('res.company', string='Company', readonly=True, default=lambda self: self.env.user.company_id)
    year_id = fields.Many2one('account.fiscalyear', 'Fiscal Year', required=True, track_visibility='onchange')
    state = fields.Selection(STATE_SELECTION, 'State', default='draft', track_visibility='onchange')
    total_planned_time = fields.Float('Total Planned Time', compute='_compute_target_status_count')
    total_performed_time = fields.Float('Total Performed Time', compute='_compute_target_status_count')
    total_open_performed_time = fields.Float('Total Open Performance Percent', compute='_compute_target_status_count')
    process_value = fields.Float('Process Value', compute='_compute_target_status_count')
    plan_line_ids = fields.Many2many('business.plan.target', 'business_plan_plan_target_rel','plan_id', 'target_id','Plan Lines', copy=False, domain="[('plan_id','=', False)]")
    datetime = fields.Datetime(string="Date", default=fields.Datetime.now, copy=True)
    approved_datetime = fields.Datetime(string='Approved Date', copy=False)
    
    @api.onchange('department_id', 'year_id')
    def onchange_type(self):
        # Хэлтэс болон жил оруулахад нэрийг автоматаар өгнө.
        if self.department_id and self.year_id:
            self.update(
                {'name': u'%s хэлтэсийн %s -н төлөвлөгөө.' % (self.department_id.name, self.year_id.name)})
    
    @api.multi
    def action_send(self):
        # Илгээх функц "Илгээгдсэн" төлөвт оруулна
        for obj in self:
            obj.state = 'sent'
            obj.line_state('sent')

    @api.multi
    def action_draft(self):
        # Ноороглох функц "Ноорог" төлөвт оруулна
        for obj in self:
            obj.state = 'draft'
            obj.line_state('draft')

    @api.multi
    def action_confirm(self):
        # Батлах функц "Батлагдсан" төлөвт оруулна
        for obj in self:
            obj.state = 'approved'
            obj.approved_datetime = fields.Datetime.now()
            obj.line_state('approved')

    @api.multi
    def action_done(self):
        # Хийгдсэн болгох функц "Хийгдсэн" төлөвт оруулна
        for obj in self:
            obj.state = 'done'
            obj.line_state('done')

    @api.multi
    def action_cancel(self):
        # Цуцлах функц "Цуцлагдсан" төлөвт оруулна
        for obj in self:
            obj.state = 'cancel'
            obj.line_state('cancel')
            
    @api.multi
    def line_state(self, state):
        # Мөрүүдийн төлөвийг өөрчлөх функц
        for obj in self:
            for line in obj.plan_line_ids:
                line.state = state
                
    @api.model
    def create(self, vals):
        # Төлөвлөгөө үүсгэхэд төлөвлөгөөний мөрүүдийн эзлэх хувь 100с хэтэрсэн эсэхийг шалгаж анхааруулга өгнө.
        context = dict(self._context or {})
        context['created_from_plan'] = True
        plan = super(BusinessPlan, self).create(vals)
        total_percent = 0
        if plan.plan_line_ids:
            for line in plan.plan_line_ids:
                total_percent += line.percent
                line.with_context(context).write({'plan_id':plan.id})
            if total_percent > 100:
                raise UserError(_('The total percentage is over 100. Check the percentage of goals or objectives.'))
        return plan
    
    @api.multi
    def write(self, vals):
        # Төлөвлөгөө засахад төлөвлөгөөний мөрүүдийн эзлэх хувь 100с хэтэрсэн эсэхийг шалгаж анхааруулга өгнө.
        context = dict(self._context or {})
        context['created_from_plan'] = True
        total_percent = 0
        if 'plan_line_ids' in vals:
            old_plan_line_ids = self.plan_line_ids
            res = super(BusinessPlan, self).write(vals)
            diff_order_lines = old_plan_line_ids - self.plan_line_ids
            for remove_line in diff_order_lines:
                remove_line.plan_id = False
        else:
            res = super(BusinessPlan, self).write(vals)
        for line in self.plan_line_ids:
            total_percent += line.percent
            line.with_context(context).write({'plan_id':self.id})
        if total_percent > 100:
            raise UserError(_('The total percentage is over 100. Check the percentage of goals or objectives.'))
        return res