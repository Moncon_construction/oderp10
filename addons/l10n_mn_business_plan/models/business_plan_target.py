# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, _
from odoo.exceptions import UserError

STATE_SELECTION = [
    ('draft', 'Draft'),
    ('sent', 'Sent'),
    ('approved', 'Approved'),
    ('done', 'Done'),
    ('cancel', 'Cancelled')
]

class Project(models.Model):
    _inherit = 'project.project'
    
    @api.multi
    def _compute_project_real_progress(self):
        # Төслийн төлөвлөгөөнөөс бодит явц болон мөчлөгүүдийн нэрийг тооцоолно
        for obj in self:
            period_names = ''
            if obj.project_plan_lines:
                for line in obj.project_plan_lines:
                    period_names += '%s,' % (line.period_id.name,)
                plan_line = self.env['project.plan.line'].search([('project_id', '=', obj.id)], order='period_progress desc', limit=1)
                obj.real_progress = plan_line.period_progress
                obj.period_names = period_names
            else:
                obj.real_progress = 0
                obj.period_names = period_names
    
    business_target_id = fields.Many2one('business.plan.target',string="Target")
    respondent_department_id = fields.Many2one('hr.department',string="Respondent Department")
    support_department_ids = fields.Many2many('hr.department','project_project_department_rel', 'project_id', 'department_id', string="Support Departments")
    measurement_parameters = fields.Text(string="Measurement Parameters")
    period_names = fields.Char(string="Period Names", compute='_compute_project_real_progress')
    real_progress = fields.Float(string="Real Progress", compute='_compute_project_real_progress')
    progress_percent = fields.Float(string="Progress Percent")
    
class BusinessPlanTarget(models.Model):
    _name = "business.plan.target"
    _inherit = ['mail.thread']
                
    @api.multi
    def _compute_project_status(self):
        # Зорилт дээр сонгосон төслүүдийн төлөв шалгаж тоолно
        for obj in self:
            number_of_done_projects = number_of_open_projects = process_value = project_line_number = 0
            if obj.project_line_ids:
                project_line_number = len(obj.project_line_ids)
                for project in obj.project_line_ids:
                    process_value += project.progress_percent * project.real_progress
                    if project.state == 'done':
                        number_of_done_projects += 1
                    else:
                        number_of_open_projects += 1
            obj.project_line_number = project_line_number
            obj.number_of_done_projects = number_of_done_projects
            obj.number_of_open_projects = number_of_open_projects
            obj.process_value_projects = process_value/100
    
    name = fields.Char(string="Name")
    plan_id = fields.Many2one('business.plan', 'Plan',  track_visibility='onchange', domain="[('state','=', 'draft')]")
    department_id = fields.Many2one('hr.department', 'Department', track_visibility='onchange')
    company_id = fields.Many2one('res.company', string='Company', readonly=True, default=lambda self: self.env.user.company_id)
    year_id = fields.Many2one('account.fiscalyear', 'Fiscal Year')
    percent = fields.Float(string='Percent')
    result = fields.Char(string="Result")
    work_to_done = fields.Char(string="Work to be Done")
    project_line_ids = fields.One2many('project.project', 'business_target_id', string='Projects')
    project_line_number = fields.Integer(string='Projects Number', compute='_compute_project_status')
    number_of_done_projects = fields.Float(string='Number of done Projects', compute='_compute_project_status')
    number_of_open_projects = fields.Float(string='Number of open Projects', compute='_compute_project_status')
    process_value_projects = fields.Float(string='Process Value Projects', compute='_compute_project_status')
    state = fields.Selection(STATE_SELECTION, string='Status', default='draft', copy=False, index=True, readonly=True)
    
    @api.model
    def create(self, vals):
        # Төлөвлөгөө үүсгэхэд төлөвлөгөөний мөрүүдийн эзлэх хувь 100с хэтэрсэн эсэхийг шалгаж анхааруулга өгнө.
        context = dict(self._context)
        created_from_plan = context.get('created_from_plan', False)
        if created_from_plan == False:
            target = super(BusinessPlanTarget, self).create(vals)
            if target.plan_id:
                target.plan_id.write({
                        'plan_line_ids': [(4, target.id)]
                    })
                
        return target
    
    @api.multi
    def write(self, vals):
        context = dict(self._context)
        created_from_plan = context.get('created_from_plan', False)
        if created_from_plan == False:
            if 'plan_id' in vals and vals['plan_id'] == False:
                self.plan_id.write({
                        'plan_line_ids': [(3, self.id)]
                    })
            elif 'plan_id' in vals and vals['plan_id'] != False:
                if self.plan_id:
                    self.plan_id.write({
                            'plan_line_ids': [(3, self.id)]
                        })
                plan_id = self.env['business.plan'].search([('id', '=', vals['plan_id'])])
                if plan_id:
                    plan_id.write({
                            'plan_line_ids': [(4, self.id)]
                        })
        return super(BusinessPlanTarget, self).write(vals)
    
    @api.onchange('plan_id')
    def onchange_plan_id(self):
        # Төлөвлөгөө сонгоход Жил, Хэлтэс талбаруудыг бизнесийн төлөвлөгөөтэй таарч байгаа эсэхийг шалгаж анхааруулга өгнө
        if self.plan_id and self.year_id and self.department_id :
            if self.plan_id.year_id.id != self.year_id.id:
                raise UserError(_('Business Plan Year and Target Year are different '))
            if self.plan_id.department_id.id != self.department_id.id:
                raise UserError(_('Business Plan Department and Target Department are different '))
    
    @api.multi
    def action_send(self):
        # Илгээх функц "Илгээгдсэн" төлөвт оруулна
        for obj in self:
            obj.state = 'sent'
            obj.line_state('waiting')

    @api.multi
    def action_draft(self):
        # Ноороглох функц "Ноорог" төлөвт оруулна
        for obj in self:
            obj.state = 'draft'
            obj.line_state('draft')

    @api.multi
    def action_confirm(self):
        # Батлах функц "Батлагдсан" төлөвт оруулна
        for obj in self:
            obj.state = 'approved'
            obj.line_state('in_progress')

    @api.multi
    def action_done(self):
        # Хийгдсэн болгох функц "Хийгдсэн" төлөвт оруулна
        for obj in self:
            obj.state = 'done'
            obj.line_state('done')

    @api.multi
    def action_cancel(self):
        # Цуцлах функц "Цуцлагдсан" төлөвт оруулна
        for obj in self:
            obj.state = 'cancel'
            obj.line_state('closed')
            
    @api.multi
    def line_state(self, state):
        # Мөрүүдийн төлөвийг өөрчлөх функц
        for obj in self:
            for line in obj.project_line_ids:
                line.state = state