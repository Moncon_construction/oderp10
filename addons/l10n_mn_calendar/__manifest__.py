# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Mongolian Calendar',
    'version': '1.0',
    'sequence': 130,
    'author': 'Asterisk Technologies LLC',
    'website' : 'http://asterisk-tech.mn',
    'depends': ['calendar'],
    'summary': 'Personal & Shared Calendar',
    'description': """
This is a full-featured calendar system.
========================================

It supports:
------------
    - Calendar of events
    - Recurring events

If you need to manage your meetings, you should install the CRM module.
    """,
    'category': 'Extra Tools',
    'data': [
        'security/ir.model.access.csv',
        'views/assets.xml',
        'views/calendar_event_room.xml',
        'views/calendar.xml',
        'views/calendar_view.xml',
        'data/calendar_data.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
