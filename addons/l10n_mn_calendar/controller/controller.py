# -*- coding: utf-8 -*-
import json
import xml.etree.ElementTree as ET

import odoo
from odoo import http
import odoo.modules.graph
from odoo.http import request


class MnCalendarDaygrid(http.Controller):

    @http.route('/l10n_mn_calendar/calendar_daygrid/', auth='user')
    def index(self, **kw):
        return http.request.render('l10n_mn_calendar.calendar_daygrid')

    @http.route('/l10n_mn_calendar/daygrid', type='json', auth='user')
    def get_graph(self):
        cr = http.request.cr
        rod_obj = request.env['mn_calendar_daygrid.calendar'].sudo().search([])
        response = {}
        resource = []
        event = []
        cr.execute("""SELECT 
                            calendar_event_id AS event_id, 
                            res_partner_id AS partner_id, 
                            ce.name AS event_name, 
                            rp.name AS partner_name, 
                            ce.start_datetime AT TIME ZONE 'UCT' AS start, 
                            ce.stop_datetime  AT TIME ZONE 'UCT' AS end, 
                            ce.start_date AS start_date, 
                            ce.stop_date AS end_date, 
                            ce.allday AS allday 
                      FROM calendar_event_res_partner_rel ce_rel 
                      LEFT JOIN calendar_event ce on ce_rel.calendar_event_id = ce.id 
                      RIGHT JOIN res_partner rp on ce_rel.res_partner_id = rp.id and ce_rel.res_partner_id in %s;""", (tuple(rod_obj.partner_ids.ids),))
        results = cr.fetchall()
        for p in rod_obj.partner_ids:
            resource.append({'id': p.id, 'title': p.name})
        for result in results:
            event_id, partner_id, event_name, partner_name, start, end, start_date, end_date, allday = result
            if partner_id in rod_obj.partner_ids.ids:
                if not allday:
                    event.append({
                                'resourceId': partner_id,
                                'start': str(start)[0:19],
                                'end': str(end)[0:19],
                                'title': event_name
                                })
                else:
                    event.append({
                        'resourceId': partner_id,
                        'start': start_date,
                        'end': end_date,
                        'allDay': True,
                        'title': event_name
                    })
        response['resource'] = resource
        response['event'] = event

        return json.dumps(response)