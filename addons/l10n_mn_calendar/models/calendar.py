# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError

class mn_calendar_daygrid(models.Model):
    _name = 'mn_calendar_daygrid.calendar'

    partner_ids = fields.Many2many('res.partner', 'mn_calendar_daygrid_res_partner', 'mn_calendar_daygrid_id',
                                  'partner_id')


class Meeting(models.Model):
    _inherit = 'calendar.event'

    @api.depends('task_ids')
    def _compute_task_count(self):
        for this in self:
            if len(this.task_ids) > 0:
                this.is_task = True

    
    location_room_id = fields.Many2one('calendar.event.room', 'Location Room')
    is_outside_work = fields.Boolean(string='Is outside work', default=False)
    decision = fields.Text('Decision')
    file_name = fields.Char('File name')
    attach_file = fields.Binary()
    task_ids = fields.Many2many('project.task', 'project_task_meeting_rel', 'task_id', 'meeting_id', 'Tasks')
    company_id = fields.Many2one('res.company', 'Company',
                                 default=lambda self: self.user_id.company_id or self.env.user.company_id)

    is_task = fields.Boolean(compute='_compute_task_count', string='Has task?', store=True, default=False)
    meeting_description = fields.Char('Description')

    @api.model
    def create(self, vals):
        res = super(Meeting, self).create(vals)
        # @Override: Календар харагдацаас сараар нь бүлэглээд event үүсгэх үед allday=True ч stop-оо тооцоолохгүй байсныг тооцоолдог болгов
        # FIXME: Гэхдээ start, stop-н default цагийг 08:00/18:00 гэж тооцож байгааг засах шаардлагатай
        if vals.get('start') and vals.get('stop') and vals.get('start') == vals.get('stop') and vals.get('allday'):
            res._inverse_dates()
        return res
        
    @api.constrains('location_room_id', 'start', 'stop')
    def _check_same_date_room(self):
        for meeting in self:
            if meeting.location_room_id and meeting.start and meeting.stop:
                meetings = self.search([('stop', '>', meeting.start), 
                                          ('start', '<', meeting.stop), 
                                          ('location_room_id', '=', meeting.location_room_id.id),
                                          ('id', '!=', meeting.id)])
                if meetings:
                    raise ValidationError(_("The meeting time overlaps the room"))

        return {
            'view_type': 'form',
            'view_mode': 'form,tree',
            'res_model': 'hr.meeting',
            "views": [[False, "tree"], [False, "form"]],
            'type': 'ir.actions.act_window',
            'target': 'current',
            'flags': {'action_buttons': True},
        }