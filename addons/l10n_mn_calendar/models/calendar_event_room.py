# -*- coding: utf-8 -*-
from odoo import api, fields, models

class CalendarEventRoom(models.Model):
    _name = 'calendar.event.room'
    _description = 'Event room'

    name = fields.Char('Name', required=True, size=128)