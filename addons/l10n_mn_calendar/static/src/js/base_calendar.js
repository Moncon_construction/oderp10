odoo.define('l10n_mn_calendar.base_calendar', function (require) {
    "use strict";
    var core = require('web.core');
    var Notification = require('web.notification').Notification;
    var session = require('web.session');
    var WebClient = require('web.WebClient');
    var mobile = require('web_mobile.rpc');
    var Notification = require('web.notification').Notification;

    var _t = core._t;
    var _lt = core._lt;
    var QWeb = core.qweb;

    var CalendarNotification = Notification.extend({
        template: "CalendarNotification",

        init: function(parent, title, text, eid) {
            this._super(parent, title, text, true);
            this.eid = eid;

            this.events = _.extend(this.events || {}, {
                'click .link2event': function() {
                    var self = this;

                    this.rpc("/web/action/load", {
                        action_id: "calendar.action_calendar_event_notify",
                    }).then(function(r) {
                        r.res_id = self.eid;
                        return self.do_action(r);
                    });
                },

                'click .link2recall': function() {
                    this.destroy(true);
                },

                'click .link2showed': function() {
                    this.destroy(true);
                    this.rpc("/calendar/notify_ack");
                },
            });
        },
    });

    WebClient.include({
        display_calendar_notif: function(notifications) {
            var self = this;
            var last_notif_timer = 0;

            // Clear previously set timeouts and destroy currently displayed calendar notifications
            clearTimeout(this.get_next_calendar_notif_timeout);
            _.each(this.calendar_notif_timeouts, clearTimeout);
            _.each(this.calendar_notif, function(notif) {
                if (!notif.isDestroyed()) {
                    notif.destroy();
                }
            });
            this.calendar_notif_timeouts = {};
            this.calendar_notif = {};

            // For each notification, set a timeout to display it
            _.each(notifications, function(notif) {
                self.calendar_notif_timeouts[notif.event_id] = setTimeout(function() {
                    if(mobile.methods.showNotification){
                        mobile.methods.showNotification({'title': notif.title, 'message':notif.message});
                        mobile.methods.vibrate({'duration':1000});
                    }
                    else{
                        var notification = new CalendarNotification(self.notification_manager, notif.title, notif.message, notif.event_id);
                        self.notification_manager.display(notification);
                        self.calendar_notif[notif.event_id] = notification;
                    }
                }, notif.timer * 1000);
                last_notif_timer = Math.max(last_notif_timer, notif.timer);
            });

            // Set a timeout to get the next notifications when the last one has been displayed
            if (last_notif_timer > 0) {
                this.get_next_calendar_notif_timeout = setTimeout(this.get_next_calendar_notif.bind(this), last_notif_timer * 1000);
            }
        }
   });

});