# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Cash Expense",
    'version': '1.0',
    'depends': ['l10n_mn_account', 'l10n_mn_workflow_config', 'mail'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
           Cash Expense
    """,
    'data': [
        'security/ir.model.access.csv',
        'security/ir_rule.xml',
        'wizard/cash_expense_refuse_view.xml',
        'wizard/hr_expense_custom_report.xml',
        'wizard/account_statement_import_expense_view.xml',
        'views/res_company_view.xml',
        'views/account_config_settings.xml',
        'views/cash_expense_view.xml',
        'views/account_view.xml',
        'views/account_journal_view.xml',
        'views/cash_expense_type_view.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False

}
