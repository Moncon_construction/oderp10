# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import fields, models, api, _
from odoo.exceptions import UserError
import time

class AccountBankStatementLine(models.Model):
    _name = 'account.bank.statement.line'
    _inherit = ['account.bank.statement.line', 'mail.thread']

    cash_expense_id = fields.Many2one('hr.expense.cash', string='Cash Expense', readonly=True)

    def process_reconciliation(self, counterpart_aml_dicts=None, payment_aml_rec=None, new_aml_dicts=None):
        res = super(AccountBankStatementLine, self).process_reconciliation(counterpart_aml_dicts, payment_aml_rec, new_aml_dicts)
        for line in self:
            if line.cash_expense_id:
                if line.cash_expense_id.residual == 0:
                    expense_state = 'done'
                else:
                    expense_state = 'waiting_payment'
                if line.cash_expense_id.after_report_ok:
                    expense_state = 'after'
                line.cash_expense_id.validate_paid(expense_state)
        return res
        
    @api.multi
    def button_cancel_reconciliation(self):
        #Касс харилцахын мөрийг цуцлахад бэлэн мөнгөний хүсэлтийн төлөвийг өөрчлөх
        for st_line in self:
            if st_line.cash_expense_id:
                st_line.cash_expense_id.write({'state': 'waiting_payment'})
                
        return super(AccountBankStatementLine, self).button_cancel_reconciliation()