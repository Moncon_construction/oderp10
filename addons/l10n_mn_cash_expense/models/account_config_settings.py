# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, _
from odoo.exceptions import UserError
class AccountConfigSettings(models.TransientModel):
    _inherit = 'account.config.settings'
    _description = 'Account settings'

    cash_expense_workflow = fields.Selection([('from_dept', 'From department'),
                            ('from_journal', 'From journal')], related="company_id.cash_expense_workflow", default=lambda self: self.company_id.cash_expense_workflow,
                             string=_("Cash expense workflows"))