# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import fields, models, api, _
from odoo.exceptions import UserError
import time


class AccountJournal(models.Model):
    _inherit = 'account.journal'

    workflow_id = fields.Many2one('workflow.config', string='Cash Expense Workflow')
