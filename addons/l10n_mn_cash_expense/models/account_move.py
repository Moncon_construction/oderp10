# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import fields, models, api, _
from odoo.exceptions import UserError
import time

class AccountMove(models.Model):
    _inherit = 'account.move'
    cash_expense_id = fields.Many2one('hr.expense.cash', string='Cash Expense', readonly=True)

    @api.multi
    def button_cancel(self):
        res = super(AccountMove, self).button_cancel()
        for move in self:
            if move.cash_expense_id:
                raise UserError(_("Move cannot be canceled if linked to an cash expense. (Cash expense: %s - Move ID:%s)") % (move.cash_expense_id.name, move.name))
        
    