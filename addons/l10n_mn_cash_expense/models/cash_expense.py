# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.addons.l10n_mn_report.models.report_helper import verbose_numeric, comma_me, convert_curr
STATE_SELECTION = [
    ('draft', 'Draft'),
    ('waiting_approve', 'Waiting approve'),
    ('approved', 'Approved'),
    ('waiting_payment', 'Waiting Payment'),
    ('done', 'Done'),
    ('after', 'After report'),
    ('after_check', 'After report check'),
    ('cancel', 'Cancelled')
]

class HrExpenseCash(models.Model):
    _name = "hr.expense.cash"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = "Cash Expense"
    _order = 'date desc'

    @api.multi
    def _compute_amount(self):
        for expense in self:
            total = 0
            atotal = 0
            rtotal = 0
            for line in expense.expense_line_ids:
                total += line.sub_total
            for aline in expense.after_report_line_ids:
                atotal += aline.spent_amount
                rtotal += aline.request_amount
            expense.total_amount = total
            expense.spent_total_amount = atotal
            expense.request_total_amount = rtotal
            
    @api.multi
    def _compute_residual(self):
        residual = 0.0
        bl_amount = 0.0
        for expense in self:
            bank_lines = self.env['account.bank.statement.line'].search([('cash_expense_id','=',expense.id)])
            if bank_lines:
                for line in bank_lines:
                    bl_amount += line.amount
            residual = expense.total_amount + bl_amount
            expense.residual = residual

    name = fields.Char(string='Expense Description', readonly=True, required=True, states={'draft': [('readonly', False)], 'cancel': [('readonly', False)]})
    description = fields.Text()
    employee_id = fields.Many2one('hr.employee', string="Employee", required=True, readonly=True, states={'draft': [('readonly', False)], 'cancel': [('readonly', False)]}, default=lambda self: self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1))
    state = fields.Selection(STATE_SELECTION, string='Status', default='draft', copy=False, index=True, readonly=True, help="Status of the expense cash.", track_visibility='always')
    department_id = fields.Many2one('hr.department', string='Department', readonly=True, states={'draft': [('readonly', False)], 'cancel': [('readonly', False)]}, required=True)
    date = fields.Date(readonly=True, states={'draft': [('readonly', False)], 'cancel': [('readonly', False)]}, required=True, default=fields.Date.context_today, string="Date")
    company_id = fields.Many2one('res.company', string='Company', readonly=True, states={'draft': [('readonly', False)], 'cancel': [('readonly', False)]}, default=lambda self: self.env.user.company_id)
    currency_id = fields.Many2one('res.currency', string='Currency', readonly=True, states={'draft': [('readonly', False)], 'cancel': [('readonly', False)]}, required=True, default=lambda self: self.env.user.company_id.currency_id)
    total_amount = fields.Float(string='Total', compute='_compute_amount', currency_field='currency_id')
    spent_total_amount = fields.Float(string='Spent Total', compute='_compute_amount', currency_field='currency_id')
    request_total_amount = fields.Float(string='Request Total', compute='_compute_amount', currency_field='currency_id')
    journal_id = fields.Many2one('account.journal', string='Expense Journal', readonly=True, states={'after_check': [('readonly', False)]}, help="The journal used when the expense is done.")
    account_id = fields.Many2one('account.account', string='Account', readonly=True, states={'approved': [('readonly', False)]})
    is_require_analytic = fields.Boolean(related="account_id.req_analytic_account", string="Require Analytic")
    analytic_account_id = fields.Many2one('account.analytic.account', string='Analytic Account', readonly=True, states={'approved': [('readonly', False)]})
    after_report_ok = fields.Boolean('After Report OK?', default=False, readonly=True, states={'approved': [('readonly', False)]})
    workflow_id = fields.Many2one('workflow.config', string='Workflow', required=True)
    check_sequence = fields.Integer(string='Workflow Step', copy=False, default=0)
    check_users = fields.Many2many('res.users', 'hr_cash_expense_check_user_rel', 'expense_id', 'user_id', 'Checkers', readonly=True, copy=False)
    show_approve_button = fields.Boolean(string='Show Approve Button?', compute='_show_approve_button')
    step_is_final = fields.Boolean(string='Final Step', default=False)
    move_ids = fields.One2many('account.move', 'cash_expense_id', readonly=True, copy=False, ondelete='restrict')
    cash_journal_id = fields.Many2one('account.journal', string='Cash Journal', domain="[('type', '=', 'cash')]")
    show_journal = fields.Boolean(string='Show journal', default=False)
    type_id = fields.Many2one('cash.expense.type', 'Cash Expense Type')
    residual = fields.Float(string='Amount Due',compute='_compute_residual')

    @api.onchange('company_id')
    def _onchange_company(self):
        if self.company_id and self.company_id.cash_expense_workflow != 'from_dept':
            self.show_journal = True
        else:
            self.show_journal = False

    @api.onchange('cash_journal_id')
    def _onchange_cash_journal_id(self):
        if self.cash_journal_id:
            if self.cash_journal_id.workflow_id:
                self.workflow_id = self.cash_journal_id.workflow_id
            else:
                raise UserError(_("There is no configured cash expense workflow in this journals!!!"))

    @api.onchange('after_report_ok')
    def _onchange_after_report_ok(self):
        acc_ids = []
        type='expense'
        if self.after_report_ok:
            type='receivable'
            self.analytic_account_id=False
        accounts = self.env['account.account'].search([('internal_type','=',type),('company_id','=',self.company_id.id)])
        if accounts:
            acc_ids = accounts.ids
        return {'domain': {'account_id': [('id', 'in', acc_ids)]}}

    @api.multi
    def button_journal_entries(self):
        return {
            'name': _('Journal Items'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': [('cash_expense_id', 'in', self.ids)],
        }

    @api.multi
    def _show_approve_button(self):
        res = {}
        history_obj = self.env['workflow.history']
        for expense in self:
            history = history_obj.search([('cash_expense_id', '=', expense.id), (
                'line_sequence', '=', expense.check_sequence)], limit=1, order='sent_date DESC')
            if history:
                expense.show_approve_button = (
                    expense.state == 'waiting_approve' and self.env.user.id in map(lambda x: x.id, history.user_ids))
            else:
                expense.show_approve_button = False
        return res

    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        self.department_id = self.employee_id.department_id
        if self.employee_id.department_id:
            workflow_id = self.env['workflow.config'].get_workflow(
                'department', 'hr.expense.cash', employee_id=None, department_id=self.employee_id.department_id.id)
            if workflow_id:
                self.workflow_id = workflow_id

    @api.multi
    def submit_expenses(self):
        for obj in self:
            if not obj.expense_line_ids:
                raise UserError(_("The request line can not be empty."))
            
            if obj.workflow_id:
                workflow_obj = self.env['workflow.config']
                success, current_sequence = workflow_obj.send(
                    'workflow.history', 'cash_expense_id', obj, self.env.user.id)
                is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(obj.workflow_id.id, obj, self.env.user.id, obj.check_sequence + 1, 'next')
                if is_next:
                    obj.check_users = [(6, 0, next_user_ids)]
                if success:
                    obj.check_sequence = current_sequence
                    obj.ensure_one()
                    obj.state = 'waiting_approve'
                    self._line_state_update('waiting_approve')

    @api.multi
    def validate(self):
        self.ensure_one()
        workflow_obj = self.env['workflow.config']
        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve(
                'workflow.history', 'cash_expense_id', self, self.env.user.id)
            is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(self.workflow_id.id, self, self.env.user.id, self.check_sequence + 1, 'next')
            if is_next:
                self.check_users = [(6, 0, next_user_ids)]
            if success:
                if sub_success:
                    self.state = 'approved'
                    self._line_state_update('approved')
                    self.step_is_final = True
                else:
                    self.check_sequence = current_sequence
                    self.validating_user_id = self.env.user.id
                    self.show_approve_button = True
            return True

    @api.multi
    def refuse_expenses(self, reason):
        self.write({'state': 'cancel'})
        self._line_state_update('cancel')
        for sheet in self:
            body = (_("Your Expense %s has been refused.<br/><ul class=o_timeline_tracking_value_list><li>Reason<span> : </span><span class=o_timeline_tracking_value>%s</span></li></ul>") % (sheet.name, reason))
            sheet.message_post(body=body)

    @api.multi
    def action_to_draft(self):
        self.write({'state': 'draft', 'check_sequence':0})
        self._line_state_update('draft')

    @api.multi
    def action_move_create(self):
        for obj in self:
            journal = obj.journal_id
            acc_date = obj.date
            move = self.env['account.move'].create({
                'journal_id': journal.id,
                'company_id': self.env.user.company_id.id,
                'date': acc_date,
                'ref': obj.name,
                'name': '/',
                'cash_expense_id':obj.id
            })
            company_currency = obj.company_id.currency_id
            diff_currency_p = obj.currency_id != company_currency
            move_lines, diff_amount = obj._move_line_get()
            total, total_currency, move_lines = obj._compute_expense_totals(company_currency, move_lines, acc_date)
            emp_account = obj.account_id.id
            aml_name = obj.employee_id.name + ': ' + obj.name.split('\n')[0][:64]
            partner_id=False
            if obj.employee_id.address_home_id:
                partner_id = obj.employee_id.address_home_id.id
            if diff_amount > 0.0:
                move_lines.append({
                        'type': 'dest',
                        'name': aml_name,
                        'price': total+diff_amount,
                        'account_id': emp_account,
                        'date_maturity': acc_date,
                        'amount_currency': diff_currency_p and total_currency or False,
                        'currency_id': diff_currency_p and obj.currency_id.id or False,
                        'partner_id':partner_id
                        })
                if not obj.employee_id.address_home_id:
                     raise UserError(_("No Home Address found for the employee %s, please configure one.") % (obj.employee_id.name))
                 
                emp_payable_account = obj.employee_id.address_home_id.property_account_payable_id
                aml_name=u'%s - зөрүү'%(obj.name)
                move_lines.append({
                        'type': 'dest',
                        'name': aml_name,
                        'price': -diff_amount,
                        'account_id': emp_payable_account.id,
                        'date_maturity': acc_date,
                        'amount_currency': diff_currency_p and total_currency or False,
                        'currency_id': diff_currency_p and obj.currency_id.id or False,
                        'partner_id':partner_id
                        })
            else:
                move_lines.append({
                        'type': 'dest',
                        'name': aml_name,
                        'price': total,
                        'account_id': emp_account,
                        'date_maturity': acc_date,
                        'amount_currency': diff_currency_p and total_currency or False,
                        'currency_id': diff_currency_p and obj.currency_id.id or False,
                        'partner_id':partner_id
                        })
            lines = map(lambda x: (0, 0, obj._prepare_move_line(x)), move_lines)
            move.with_context(dont_create_taxes=True).write({'line_ids': lines})
            move.post()
        return True
 
    def _prepare_move_line(self, line):
        '''
        This function prepares move line of account.move related to an expense
        '''
        return {
            'date_maturity': line.get('date_maturity'),
            'partner_id': line.get('partner_id'),
            'name': line['name'][:64],
            'debit': line['price'] > 0 and line['price'],
            'credit': line['price'] < 0 and - line['price'],
            'account_id': line['account_id'],
            'analytic_line_ids': line.get('analytic_line_ids'),
            'amount_currency': line['price'] > 0 and abs(line.get('amount_currency')) or - abs(line.get('amount_currency')),
            'currency_id': line.get('currency_id'),
            'quantity': line.get('quantity', 1.00),
            'product_id': line.get('product_id'),
            'product_uom_id': line.get('uom_id'),
            'analytic_account_id': line.get('analytic_account_id'),
        }
 
    @api.multi
    def _compute_expense_totals(self, company_currency, account_move_lines, move_date):
        self.ensure_one()
        total = 0.0
        total_currency = 0.0
        for line in account_move_lines:
            line['currency_id'] = False
            line['amount_currency'] = False
            if self.currency_id != company_currency:
                line['currency_id'] = self.currency_id.id
                line['amount_currency'] = line['price']
                line['price'] = self.currency_id.with_context(date=move_date or fields.Date.context_today(self)).compute(line['price'], company_currency)
            total -= line['price']
            total_currency -= line['amount_currency'] or line['price']
        return total, total_currency, account_move_lines
 
    @api.multi
    def _move_line_get(self):
        account_move = []
        diff_amount = 0.0
        for expense in self:
            for line in expense.after_report_line_ids:
                amount = line.spent_amount
                if line.spent_amount > line.request_amount:
                    diff_amount += line.spent_amount - line.request_amount
                    
                account = line.account_id
                aml_name = expense.employee_id.name + ': ' + line.name.split('\n')[0][:64]
                partner_id = expense.employee_id.address_home_id.id
                if line.partner_id:
                    partner_id = line.partner_id.id 
                move_line = {
                        'partner_id':partner_id,
                        'type': 'src',
                        'name': aml_name,
                        'price_unit': amount,
                        'quantity': 1,
                        'price': amount,
                        'account_id': account.id,
                        'product_id': False,
                        'uom_id': False,
                        'analytic_account_id': expense.analytic_account_id.id if expense.analytic_account_id else False,
                }
                account_move.append(move_line)
        return account_move, diff_amount

    @api.multi
    def validate_account(self):
        for obj in self:
            obj.state = 'waiting_payment'
            self._line_state_update('waiting_payment')
        return True

    @api.multi
    def validate_paid(self, state):
        self.write({'state':state})
        self._line_state_update(state)

    @api.multi
    def import_expense_line(self):
        for obj in self:
            for line in obj.expense_line_ids:
                self.env['hr.expense.after.report.line'].create({'expense_id':obj.id, 
                                                                 'name': line.name, 
                                                                 'request_amount': line.unit_amount,
                                                                 'currency_id': line.currency_id.id,
                                                                 'spent_amount':line.unit_amount,
                                                                 'state':line.state})

    @api.multi
    def send_after_report(self):
        for obj in self:
            if not obj.after_report_line_ids:
                raise UserError(_("The after report line can not be empty."))
            account_id = False
            bslobj = self.env['account.bank.statement.line'].sudo().search([('cash_expense_id','=',obj.id)], limit=1)
            if bslobj and bslobj.sudo().account_id:
                account_id = bslobj.sudo().account_id.id
            obj.sudo().write({'state': 'after_check', 'account_id':account_id})
            obj._line_state_update('after_check')

    @api.multi
    def action_done(self):
        for obj in self:
            if not obj.after_report_line_ids:
                raise UserError(_("The after report line can not be empty."))
            obj.action_move_create()
            obj.write({'state':'done'})
            obj._line_state_update('done')

    @api.multi
    def _line_state_update(self, state):
        for obj in self:
            obj.expense_line_ids.write({'state': state})
            if obj.after_report_line_ids:
                obj.after_report_line_ids.write({'state': state})

    @api.multi
    def action_open_journal_entries(self):
        res = self.env['ir.actions.act_window'].for_xml_id('account', 'action_move_journal_line')
        res['domain'] = [('id', 'in', self.mapped('account_move_id').ids)]
        res['context'] = {}
        return res

    @api.multi
    def action_cancel(self):
        moves = self.env['account.move']
        for obj in self:
            bank_statement_line = self.env['account.bank.statement.line'].search([('cash_expense_id', '=', obj.id)], limit=1)
            if bank_statement_line:
                if bank_statement_line.statement_id.state == 'confirm':
                    raise UserError(_("Please cancel the bank statement first!"))
            if obj.after_report_ok:
                for move in obj.move_ids:
                    move.write({'cash_expense_id': False})
                    move.button_cancel()
                    move.unlink()
                obj.write({'state': 'after_check'})
                obj._line_state_update('after_check')
            if obj.state == 'waiting_payment':
                obj.write({'state': 'cancel'})
        return True

class HrExpenseCashLine(models.Model):
    _name = "hr.expense.cash.line"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = "Cash Expense Line"
    
    @api.depends('qty','unit_amount')
    def compute_sub_total(self):
        for obj in self:
            obj.sub_total = obj.qty * obj.unit_amount

    expense_id = fields.Many2one('hr.expense.cash', string='Expense', readonly=True, copy=False, ondelete='cascade')
    name = fields.Char(string="Reference", required=True, readonly=True, states={'draft': [('readonly', False)], 'cancel': [('readonly', False)]})
    unit_amount = fields.Float(string='Unit Price', readonly=True, required=True, states={'draft': [('readonly', False)], 'cancel': [('readonly', False)]}, currency_field='currency_id')
    currency_id = fields.Many2one('res.currency', string='Currency', readonly=True, states={'draft': [('readonly', False)], 'cancel': [('readonly', False)]}, default=lambda self: self.env.user.company_id.currency_id)
    state = fields.Selection(STATE_SELECTION, string='Status', default='draft', copy=False, index=True, readonly=True, help="Status of the expense cash.")
    tax_ids = fields.Many2many('account.tax', 'expense_cash_tax', 'expense_id', 'tax_id', string='Taxes', readonly=True, states={'draft': [('readonly', False)], 'approved': [('readonly', False)]})
    qty = fields.Float(string='Qty', required=True, default= 1 )
    sub_total = fields.Float('Subtotal', compute=compute_sub_total)
    uom_id = fields.Many2one('product.uom', string='Unit of Measure')

class HrExpenseAfterReportLine(models.Model):
    _name = "hr.expense.after.report.line"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = "Cash Expense After Report Line"

    def _default_partner(self):
        partner_id=False
        emp = self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)
        if emp and emp.address_home_id:
            partner_id = emp.address_home_id.id
        return partner_id
    
    expense_id = fields.Many2one('hr.expense.cash', string="Expense", readonly=True, copy=False, ondelete='cascade')
    name = fields.Char(string="Reference", required=True, readonly=True, states={'after': [('readonly', False)]})
    request_amount = fields.Float(string='Request Price', readonly=True, required=True, states={'after': [('readonly', False)]}, currency_field='currency_id')
    currency_id = fields.Many2one('res.currency', string='Currency', readonly=True, states={'after': [('readonly', False)]}, default=lambda self: self.env.user.company_id.currency_id)
    analytic_account_id = fields.Many2one('account.analytic.account', string='Analytic Account', readonly=True, states={'after': [('readonly', False)], 'after_check': [('readonly', False)]})
    spent_amount = fields.Float(string='Spent Price', readonly=True, required=True, states={'after': [('readonly', False)], 'after_check': [('readonly', False)]}, currency_field='currency_id')
    account_id = fields.Many2one('account.account', string='Account', readonly=True, states={'after_check': [('readonly', False)]})
    account_type = fields.Selection(string='Account Type', readonly=True, related='account_id.user_type_id.type')
    state = fields.Selection(STATE_SELECTION, string='Status', copy=False, index=True, readonly=True, default='after')
    attachment_number = fields.Integer(compute='_compute_attachment_number', string='Number of Attachments')
    partner_id = fields.Many2one('res.partner', string='Partner', default=_default_partner, readonly=True, states={'after': [('readonly', False)], 'after_check': [('readonly', False)]})

    @api.multi
    def _compute_attachment_number(self):
        attachment_data = self.env['ir.attachment'].read_group(
            [('res_model', '=', 'hr.expense.after.report.line'), ('res_id', 'in', self.ids)], ['res_id'], ['res_id'])
        attachment = dict((data['res_id'], data['res_id_count'])
                          for data in attachment_data)
        for expense in self:
            expense.attachment_number = attachment.get(expense.id, 0)

    @api.multi
    def action_get_attachment_view(self):
        self.ensure_one()
        res = self.env['ir.actions.act_window'].for_xml_id(
            'base', 'action_attachment')
        res['domain'] = [
            ('res_model', '=', 'hr.expense.after.report.line'), ('res_id', 'in', self.ids)]
        res['context'] = {
            'default_res_model': 'hr.expense.after.report.line', 'default_res_id': self.id}
        return res

class WorkflowHistory(models.Model):
    _inherit = 'workflow.history'
    cash_expense_id = fields.Many2one('hr.expense.cash', 'Cash Expense')


class InheritHrExpenseCash(models.Model):
    _inherit = "hr.expense.cash"

    expense_line_ids = fields.One2many('hr.expense.cash.line', 'expense_id', string='Expense Lines', readonly=True, states={'draft': [('readonly', False)]}, copy=False)
    after_report_line_ids = fields.One2many('hr.expense.after.report.line', 'expense_id', string='After report lines', readonly=True, states={'after': [('readonly', False)], 'after_check': [('readonly', False)]}, copy=False)
    workflow_history_ids = fields.One2many(
        'workflow.history', 'cash_expense_id', string='History', readonly=True)
    verbose_amount = fields.Char(string='Verbose Amount', compute='_get_verbose_amount')

    @api.multi
    def unlink(self):
        for expense in self:
            if expense.state != "draft":
                raise UserError(_("You can only delete a draft expense."))
        super(HrExpenseCash, self).unlink()

    @api.multi
    def _get_verbose_amount(self):
        for expense in self:
            amounts = {}
            currency = {}
            verbose_dict = {}
            if expense.currency_id:
                curr = expense.currency_id.integer
                div_curr = expense.currency_id.divisible
            list = verbose_numeric(abs(expense.total_amount))
            verbose_dict['my_verbose'] = convert_curr(list, curr, div_curr)

            expense.verbose_amount = verbose_dict['my_verbose']







