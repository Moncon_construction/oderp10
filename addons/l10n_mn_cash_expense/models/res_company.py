from odoo import fields, models, api, _
from odoo.exceptions import UserError

class ResCompany(models.Model):
    _inherit = 'res.company'

    cash_expense_workflow = fields.Selection([('from_dept', 'From department'),
                            ('from_journal', 'From journal')], default = 'from_dept')