# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class AccountStatementImportExpense(models.TransientModel):
    _name = "account.statement.import.expense"
    _description = "Account statement at import expense"

    @api.model
    def _default_currency_id(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', [])
        bank_statement = self.env['account.bank.statement'].browse(active_ids)
        return bank_statement.journal_id.currency_id.id if bank_statement.journal_id.currency_id else bank_statement.company_id.currency_id.id

    import_expenses = fields.Many2many('hr.expense.cash', 'hr_expense_cash_import_acc_statement', 'wizard_id', 'expense_id', 'Expenses')
    currency_id = fields.Many2one('res.currency', string='Currency', default=_default_currency_id)

    @api.multi
    def import_expense(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', [])
        if not active_ids:
            raise UserError(_('Active ids null!'))
        statement = self.env['account.bank.statement'].browse(active_ids[0])
        bank_st_line_obj = self.env['account.bank.statement.line']
        if self.import_expenses:
            for expense in self.import_expenses:
                bank_st_line_obj.create({
                    'statement_id': statement.id,
                    'cash_expense_id': expense.id,
                    'name': expense.name,
                    'partner_id': expense.employee_id.address_home_id.commercial_partner_id.id,
                    'account_id': expense.account_id.id if expense.account_id else False,
                    'analytic_account_id': expense.analytic_account_id.id if expense.analytic_account_id else False,
                    'amount': -expense.residual})
        return {}
