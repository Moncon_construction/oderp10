# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class HrExpenseCashRefuseWizard(models.TransientModel):

    _name = "hr.expense.cash.refuse.wizard"
    _description = "Hr Expense Cash refuse Reason wizard"

    description = fields.Char(string='Reason', required=True)

    @api.multi
    def expense_refuse_reason(self):
        self.ensure_one()

        context = dict(self._context or {})
        active_ids = context.get('active_ids', [])
        expense_cash = self.env['hr.expense.cash'].browse(active_ids)
        expense_cash.refuse_expenses(self.description)
        return {'type': 'ir.actions.act_window_close'}
