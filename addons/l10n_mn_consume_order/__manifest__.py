    # -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Monglolian Consume Order",
    'version': '1.0',
    'depends': ['l10n_mn_stock_account'],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Account Modules',
    'description': """
         Бага үнэтэй түргэн элэгдэх зүйлс бүртгэх.
    """,
    'website' : 'http://asterisk-tech.mn',
    'data': [
        'security/security_view.xml',
        'security/ir.model.access.csv',
        'data/consume_order_data.xml',
        'wizard/consumable_material_wizard_view.xml',
        'views/product_template_view.xml',
        'views/consumable_material_expense_view.xml',
        'views/consume_material_in_use_view.xml',
        'wizard/consumable_material_wizard_view.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}