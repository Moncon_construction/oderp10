# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError
import math


class ConsumableMaterialExpense(models.Model):
    _name = 'consumable.material.expense'
    _order = 'date DESC'
    
    doc_number = fields.Char(
        'Document number', copy=False, readonly=True,default=lambda x: _('New'))
    date = fields.Date('Transaction Date',default=fields.Datetime.now) 
    state = fields.Selection([('draft','Draft'),
                              ('progress','Progress'),
                              ('confirm','Confirmed'),
                              ('reject','Reject'),
                              ('done','Done')],String='Status',default='draft')
    
    expense_product_list_ids = fields.One2many('consumable.material.expense.line', 'consumable_material_id','Product list')
    consume_count = fields.Integer(compute='_compute_workorder_count')
    warehouse_id = fields.Many2one('stock.warehouse', 'Deliver To')
    related_product_move_id = fields.Many2one('stock.move')

    
    @api.multi
    def _compute_workorder_count(self):
#       Ашиглалтанд байгаа хангамжийн материал тоолох функц
        qty = 0
        for line in self:
            res = self.env['consumable.material.in.use'].search([('doc_number', '=', line.doc_number)])
            for i in res:
                qty += 1
        self.consume_count = qty or 0
        
    @api.model
    def create(self, vals):
        if not vals.get('doc_number'):
            vals['doc_number'] = self.env['ir.sequence'].next_by_code('consumable.material.expense') or _('New')
        return super(ConsumableMaterialExpense, self).create(vals)
    
    @api.multi
    def button_consume_order(self):
#        ухаалаг даруул дээр дарахад дуудагдах функц
        res = []
        for line in self:
            res = self.env['consumable.material.in.use'].search([('doc_number', '=', line.doc_number)])
            return{
                'type': 'ir.actions.act_window',
                'name': _('Consumable Material'),
                'res_model': 'consumable.material.in.use',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'view_id': False,
                'domain':[('id', 'in', res.ids)],
                }
     
    @api.multi
    def button_confirm(self):
#        Ашиглалтанд байгаа хангамжийн материал, барааны хөдөлгөөн үүснэ.
        check = False
        end_date = '2015-08-14'
        for line in self.expense_product_list_ids:#барааны үлдэгдэл нь боломжит тоо хэмжээнээс бага байгаа эсэхийг шалгах 
            if line.quantity_available < line.quantity:
               check = True 
        if check == False:#бага байгаа тохиолдолд ашиглалтанд байгаа хангамжийн материал, барааны хөдөлгөөн үүснэ.
            
            self.state = 'confirm'
            consume_obj = self.env['consumable.material.in.use']
            for line in self.expense_product_list_ids:
                if line.is_depreciate == True:
                    end_date = line.end_date
                else:
                    end_date = False

                for row in range(int(line.quantity)):
                    consume = consume_obj.create({
                        'doc_number':self.doc_number,
                        'product_id':line.product_id.id or False,
                        'owner_id':line.owner_id.id or False,
                        'transaction_date':self.date,
                        'date':line.start_date,
                        'end_date':end_date,
                        'is_depreciate':line.is_depreciate,
                        'state':'progress',
                        })
                    consume.button_progress()
        else:
            raise UserError(_(u'Not enough product quantity!'))

        
    @api.multi
    def button_reject(self):
#        Холбоотойгоор үүссэн ашиглалтанд байгаа хангамжийн материалууд ноорог төлөвтэй байвал цуцлах
        check_value = False
        for line in self:
            res = self.env['consumable.material.in.use'].search([('doc_number', '=', line.doc_number)])
            for i in res:
                if i.state != 'draft':
                    check_value = True
        if check_value == False:
            for i in res:
                i.unlink()
            self.state = 'reject'
        else:
            raise UserError(_(u'Please, draft related consume order!'))
    
    @api.multi
    def button_draft(self):
        self.state = 'draft'

class ConsumableMaterialExpenseLine(models.Model):  
    _name = 'consumable.material.expense.line'
#     бүрэлдэхүүн хэсгүүд
    
    @api.onchange('product_id')
    def onchange_check_start(self):
        i = 0
        stock_quant = []
        for product in self:
            i
            stock_quant = self.env['stock.quant'].search([('product_id.name','=',self.product_id.name)])
            if stock_quant:
                for line in stock_quant:
                    self.quantity_available += line.qty
            i += 1
    product_id = fields.Many2one('product.template','Product')
    quantity_available = fields.Float('Quantity Available', track_visibility='onchange')
    quantity = fields.Float('Quantity')
    owner_id = fields.Many2one('res.partner','Owner')
    is_depreciate = fields.Boolean('Is Depreciate', related='product_id.is_depreciate')
    consumable_material_id = fields.Many2one('consumable.material.expense','consumable material')
    start_date = fields.Date('Start Date',default=fields.Datetime.now) 
    end_date = fields.Date('End Date')  

    
    @api.model
    def create(self, vals):
        res = super(ConsumableMaterialExpenseLine, self).create(vals)
        temp = 0
        if vals.get('product_id'):
            stock_quant = self.env['stock.quant'].search([('product_id.name','=',res.product_id.name)])
            if stock_quant:
                for line in stock_quant:
                    temp += line.qty
        vals.update({'quantity_available': temp})
        res.quantity_available = temp
        return res
        
      
    @api.multi
    def write(self, vals):
        temp = 0
        if not self.quantity_available:
            for product in self:
                stock_quant = self.env['stock.quant'].search([('product_id.name','=',self.product_id.name)])
                if stock_quant:
                    for line in stock_quant:
                        temp += line.qty
            vals['quantity_available'] = temp

        res = super(ConsumableMaterialExpenseLine, self).write(vals)
        
        return res
