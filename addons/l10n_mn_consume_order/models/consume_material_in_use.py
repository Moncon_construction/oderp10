
# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError
import datetime


class ConsumableMaterialInUse(models.Model):
    _name = 'consumable.material.in.use'
    _order = 'date DESC'
    
    doc_number = fields.Char('Document number')
    product_id = fields.Many2one('product.template','Product')
    owner_id = fields.Many2one('res.partner','Owner')
    related_product_move_id = fields.Many2one('stock.picking')
    transaction_date = fields.Date('Transaction Date') 
    date = fields.Date('Date') 
    end_date = fields.Date('End Date') 
    state = fields.Selection([('draft','Draft'),
                              ('progress','Progress'),
                              ('progress_done','Progress Done')],String='Status',default='draft')
    is_depreciate = fields.Boolean('Is Depreciate',default=False)
    depreciation_line_ids = fields.One2many('consumable.material.in.use.deprecaition.line','parent_id','History')
    account_count = fields.Integer(compute='_compute_account_count')
    @api.multi
    def _compute_account_count(self):
#        Журналын бичилт тоолох функц
        res = []
        for line in self:
            res = self.env['account.move'].search_count([('ref','=',line.id)])
            line.account_count = res
    
    @api.multi
    def button_account_move(self):
#         Журналын бичилт -> Ашиглалтанд буй хангамжийн матералын ухаалаг даруул дээр дархад дуудагдах функц
        res = []
        for line in self:
            res = self.env['account.move'].search([('ref','=',line.id)])
            return{
                'type': 'ir.actions.act_window',
                'name': _('Mrp Workorder '),
                'res_model': 'account.move',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'view_id': False,
                'domain':[('id', 'in', res.ids)],
                }

    @api.multi
    def button_progress(self):
        #Элэгдүүлэх бараа байвал дуусах хугацаа хүртэл элэгдэл тооцох, барааны хөдөлгөөн үүсгэх
        for line in self:
            consume = line.env['consumable.material.expense'].search([('doc_number','=',line.doc_number)],limit=1)
            
            stock_picking = self.env['stock.picking']
            stock_move = self.env['stock.move']
            stock_picking_type = self.env['stock.picking.type']
            
            picking_obj = self.env['stock.picking.type'].search([('warehouse_id.id','=',consume.warehouse_id.id),('code','=','outgoing')],limit=1)
            if picking_obj:
                for object in self:
                    if picking_obj.default_location_src_id and picking_obj.default_location_dest_id:
                        note = 10000 + int(object.id)
                        values = {
                                  'origin': 'Consume Material',
                                  'company_id':1,
                                  'picking_type_id': picking_obj.id,
                                  'location_dest_id': picking_obj.default_location_dest_id.id,
                                  'location_id': picking_obj.default_location_src_id.id,
                                  'note':note,
                                  'state':'waiting'}
                        stock_picking_object = stock_picking.create(values)
                        if consume.expense_product_list_ids:
                            move_lines_values = {'name': consume.expense_product_list_ids.product_id.name,
                                                 'picking_id': stock_picking_object.id,
                                                 'product_id': consume.expense_product_list_ids.product_id.product_variant_id.id,
                                                 'product_uom': consume.expense_product_list_ids.product_id.uom_id.id,
                                                 'location_id': picking_obj.default_location_src_id.id,
                                                 'location_dest_id': picking_obj.default_location_dest_id.id,
                                                 'product_uom_qty': 1,
                                                 'state':'waiting'}
                            stock_move.create(move_lines_values)
                        object.related_product_move_id = stock_picking_object.id or False
                    else:
                        raise UserError(_(u'No location id of picking type.'))
                
                if line.is_depreciate == True: #Элэгдэл тооцох хэсэг
                    if line.end_date:
                        depreciation_line_obj = line.env['consumable.material.in.use.deprecaition.line']
                        d1 = datetime.datetime.strptime(line.end_date,'%Y-%m-%d')
                        d2 = datetime.datetime.strptime(line.date,'%Y-%m-%d')
                        period = (d1.year - d2.year) * 12 + d1.month - d2.month
                        if period == 0:
                            period = 1
                        percent = 100/period
                        percent1 = 100/period
                        amount = line.product_id.standard_price/period
                        amount1 = line.product_id.standard_price/period
                        amount2 = 0
                        for i in range(period):
                            amount2 = line.product_id.standard_price - amount
                            for dep in line:
                                vals={
                                    'depreciation_date':datetime.datetime.now(),
                                    'depreciated_percent':percent,
                                    'amount':amount,
                                    'balance':amount2,
                                    'parent_id': line.id,
                                    'owner_id':line.owner_id.id,
                                    }
                                line_id = depreciation_line_obj.create(vals)
                            
                            percent += percent1
                            amount += amount1
                            
                        line.state = 'progress'
                    else:
                        raise UserError(_(u'Please set the end date.'))
                else:
                    line.state = 'progress'
            else:
                raise UserError(_(u'Please set picking type id '))
                
    @api.multi
    def button_done(self):
#         Данс асуух wizard дуудна
        for consume_material in self:
            view = consume_material.env.ref('l10n_mn_consume_order.view_consumable_material_in_use_wizard')
            return {
                'name': _('Account?'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'consumable.material.in.use.wizard',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new'
            }
            
    @api.multi
    def button_draft(self):
        for line in self:
            line.state = 'draft'
    
    @api.model
    def create(self, vals):
        return super(ConsumableMaterialInUse, self).create(vals)

class ConsumableMaterialInUseDeprecaitionLine(models.Model):
    _name = 'consumable.material.in.use.deprecaition.line'   
      
    @api.model
    def _get_employee(self):
        employee = self.env['hr.employee'].search([('user_id', '=', self.env.uid)])
        if employee:
            return employee[0].id
        else:
            return False
            
    amount = fields.Float('Amount')
    depreciation_date = fields.Date('Depreciation Date',default=lambda self: fields.Datetime.now())
    depreciation_period = fields.Many2one('depreciation.period')#Элэгдүүлэх мөчлөг
    balance = fields.Float('Balance')
    owner_id = fields.Many2one('hr.employee', string='Employee',default=_get_employee)
    depreciated_percent = fields.Float('Depreciated Percent')
    parent_id = fields.Many2one('consumable.material.in.use', string='Parent')

class DepreciationPeriod(models.Model):
    _name = 'depreciation.period'
    
    name = fields.Char('Name')
    fiscal_year = fields.Char('Fiscal Year')
    date_start =fields.Date('Duration Time')
    date_stop =fields.Date('Duration Time')