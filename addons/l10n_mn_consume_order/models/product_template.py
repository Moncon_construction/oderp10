# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class ProductTemplate(models.Model):
    _inherit = 'product.template'
    
    is_depreciate = fields.Boolean('Depreciation',default=False)
    register_on_card = fields.Boolean('Register On Card',default=False)