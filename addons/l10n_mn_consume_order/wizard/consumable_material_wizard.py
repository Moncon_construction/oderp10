# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
import datetime

class SaleOrderWizard(models.TransientModel):
    _name = 'consumable.material.in.use.wizard'
    _description = 'consumable.material.in.use.wizard'
    
    account_id = fields.Many2one('account.account', 'Account')
    date = fields.Date('Date', default=lambda self: fields.Datetime.now(), track_visibility='onchange')

    @api.multi
    def process(self):
#        Товч дархад ашиглалтанд буй хангамжийн материал дээр журналын бичилт үүсгэж холбоотой барааны хөдөлгөөнийг хийгдсэн төлөвт оруулна.
        for line in self:
            if line.account_id:
                context=dict(line._context)
                consumable_obj = line.env.get('consumable.material.in.use')
                consumable_id = context.get('active_id', False)
                form = self.browse(self.id)
                if consumable_id:
                    consumable = consumable_obj.browse(consumable_id)
                    consumable.end_date = datetime.datetime.now()
                    note = 10000 + int(consumable.id)
                    stock_obj = consumable.env['stock.picking'].search([('note','=',note)],limit=1)
                    check_value = False
                    res = []
                    for line1 in consumable:
                        consume_obj = consumable.env['consumable.material.expense'].search([('doc_number', '=', line1.doc_number)])
                        res = consumable.env['consumable.material.in.use'].search([('doc_number', '=', line1.doc_number)])
                        for i in res:
                            if i.state != 'progress_done':
                                check_value = True
                    if check_value == False:
                        consume_obj.state = 'done'
                    if stock_obj:
                        stock_obj.action_confirm()
                        stock_obj.force_assign()
                        wiz = self.env['stock.immediate.transfer'].create({'pick_id': stock_obj.id})
                        wiz.force_date = self.date
                        wiz.process()
                        if stock_obj.state == 'done':
                            consumable.state = 'progress_done'
                return True 
            
class SaleOrderWizardAll(models.TransientModel):
    _name = 'consumable.material.in.use.wizard.all'
    _description = 'consumable.material.in.use.wizard.all'
    
    account_id = fields.Many2one('account.account', 'Account')
    date = fields.Date('Date', default=lambda self: fields.Datetime.now(), track_visibility='onchange')

    @api.multi
    def process(self):
#   Олноор сонгоод батлах товч дархад ашиглалтанд буй хангамжийн материал дээр журналын бичилт үүсгэж холбоотой барааны хөдөлгөөнийг хийгдсэн төлөвт оруулна.
        context=dict(self._context)
        active_ids = self._context.get('active_ids', False) or []
        consumable_obj = self.env.get('consumable.material.in.use')
        for wizard in self:
            for consumable in consumable_obj.browse(active_ids):
                consumable.state = 'progress_done'
                consumable.end_date = datetime.datetime.now()
                note = 10000 + int(consumable.id)
                stock_obj = consumable.env['stock.picking'].search([('note','=',note)],limit=1)
                check_value = False
                res = []
                for line in consumable:
                    consume_obj = consumable.env['consumable.material.expense'].search([('doc_number', '=', line.doc_number)])
                    res = consumable.env['consumable.material.in.use'].search([('doc_number', '=', line.doc_number)])
                    for i in res:
                        if i.state != 'progress_done':
                            check_value = True
                if check_value == False:
                    consume_obj.state = 'done'
                if stock_obj:
                        stock_obj.action_confirm()
                        stock_obj.force_assign()
                        wiz = self.env['stock.immediate.transfer'].create({'pick_id': stock_obj.id})
                        wiz.force_date = self.date
                        wiz.process()
                        if stock_obj.state == 'done':
                            consumable.state = 'progress_done'
                    
            return True 