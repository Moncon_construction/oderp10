# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Partner Category",
    'version': '1.0',
    'depends': ['l10n_mn_base'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """ Харилцагчийн ангилал талбар нь компани болгоноор үүсдэг болсон
    """,
    'data': [
        'views/res_partner_views.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
