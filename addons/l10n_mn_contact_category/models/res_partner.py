# -*- coding: utf-8 -*-
from odoo import api, fields, models


class ResPartnerCategoryHierarchy(models.Model):
    _inherit = "res.partner.category.hierarchy"

    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id)


class ResPartner(models.Model):
    _inherit = 'res.partner'

    category_hierarchy_id = fields.Many2one('res.partner.category.hierarchy', string="Category", company_dependent=False)