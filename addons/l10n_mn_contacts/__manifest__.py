# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Contacts",
    'version': '1.0',
    'depends': ['contacts', 'mail'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
      Contacts Additional Features
    """,
    'data': [
        'views/res_partner_views.xml',
        'views/res_company_view.xml',
        'data/birthday_mail_cron.xml',
        'email_templates/birthday_email_template.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
