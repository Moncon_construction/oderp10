# -*- coding: utf-8 -*-

from odoo import fields, models


class ResCompany(models.Model):
    _inherit = 'res.company'

    groups_to_send_birthday_alert = fields.Many2many('res.groups')
