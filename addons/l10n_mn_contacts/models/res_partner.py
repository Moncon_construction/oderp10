# -*- coding: utf-8 -*-

from datetime import datetime
import logging
import time

from odoo import api, fields, models, exceptions, _

_logger = logging.getLogger(__name__)


class ResPartner(models.Model):
    _inherit = 'res.partner'

    register = fields.Char('Register')
    birthday = fields.Date('Date of birth')
    certificate_number = fields.Char(string='Certificate number')
    taxpayer_number = fields.Char(string='Taxpayer number')
    is_taxpayer = fields.Boolean(string='Is taxpayer')

    @api.multi
    def send_alarm(self):
        current_date = time.strftime('%Y-%m-%d')
        month = datetime.strptime(current_date, '%Y-%m-%d').month
        day = datetime.strptime(current_date, '%Y-%m-%d').day
        query = ('SELECT id  FROM res_partner WHERE extract(month from birthday) = %s  and extract(day from birthday) = %s ' % (month, day))
        self._cr.execute(query)

        fetch = map(lambda x: x[0], self._cr.fetchall())
        if fetch:
            for people in fetch:
                template = self.env.ref('l10n_mn_contacts.birthday_alarm_email_template')
                template.send_mail(people, force_send=True)
                self.send_alarm_to_user(people)

    def send_alarm_to_user(self, partner):
        group_ids = self.env.user.company_id.groups_to_send_birthday_alert.ids
        if group_ids:
            group_ids = str(group_ids)
            group_ids = group_ids.replace("[", "(")
            group_ids = group_ids.replace("]", ")")

            query = ('SELECT uid FROM res_groups_users_rel WHERE gid in %s ' % group_ids)
            self._cr.execute(query)

            fetch = map(lambda x: x[0], self._cr.fetchall())
            for obj in fetch:
                template = self.env.ref('l10n_mn_contacts.alarm_email_partner_birthday_template')
                data = {}
                if partner:
                    partner_id = self.env["res.partner"].browse(partner)
                    data = {
                        'partner_name': partner_id.display_name or partner_id.name
                    }
                template.with_context(data).send_mail(obj, force_send=True)

    @api.multi
    def cron_send_alarms(self):
        return self.send_alarm()

    @api.constrains('register')
    def _check_register(self):
        for partner in self:
            if partner.register:
                other_partners = self.env['res.partner'].search([
                    ('register', '=', partner.register),
                    ('id', '!=', partner.id),
                    ('company_id', '=', partner.company_id.id)])
                for other_partner in other_partners:
                    if other_partner:
                        exception = _('Register duplicated: ') + other_partners.register
                        raise exceptions.ValidationError(exception)

    @api.constrains('certificate_number')
    def _check_certificate_number(self):
        for partner in self:
            if partner.certificate_number:
                other_partners = self.env['res.partner'].search([
                    ('certificate_number', '=', partner.certificate_number),
                    ('id', '!=', partner.id)])
                for other_partner in other_partners:
                    if other_partner:
                        exception = _('Certificate Number duplicated: ') + other_partners.certificate_number
                        raise exceptions.ValidationError(exception)

    @api.constrains('ref')
    def _check_ref(self):
        for partner in self:
            if partner.ref:
                other_partners = self.env['res.partner'].search([
                    ('ref', '=', partner.ref),
                    ('id', '!=', partner.id)])
                for other_partner in other_partners:
                    if other_partner:
                        exception = _('Ref duplicated: ') + other_partners.ref
                        raise exceptions.ValidationError(exception)

    @api.constrains('taxpayer_number')
    def _check_validity(self):
        """ verifies if taxpayer number duplicated. """
        for partner in self:
            if partner.taxpayer_number:
                partner_id = self.env['res.partner'].search([
                    ('taxpayer_number', '=', partner.taxpayer_number),
                    ('id', '!=', partner.id)])
                for partners in partner_id:
                    if partners:
                        exception = _('Taxpayer number duplicated: ') + partner_id.taxpayer_number
                        raise exceptions.ValidationError(exception)
