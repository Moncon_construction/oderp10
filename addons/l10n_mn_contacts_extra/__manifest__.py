# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Contacts Extra",
    'version': '1.0',
    'depends': ['l10n_mn_contacts', 'l10n_mn_base'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
      Contacts Additional Features
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/res_partner_views.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
