# -*- coding: utf-8 -*-

import logging

from odoo import fields, models, _

_logger = logging.getLogger(__name__)


class ResPartner(models.Model):
    _inherit = 'res.partner'

    residence_address = fields.Char('Residence Address')
    company_address = fields.Char('Company Address')
    education = fields.Char('Education')
    is_married = fields.Boolean('Is Married')
    employment_status = fields.Selection([('corp_emp', 'Corporate Employee'),#Аж ахуй нэгжийн ажилтан
                                          ('farmer', 'Farmer'),#Малчин
                                          ('student', 'Student'),#Оюутан
                                          ('government_emp', 'Government Employee'),#Төрийн албан хаагчид
                                          ('retired', 'Retired'),#Тэтгэвэрт гарсан
                                          ('company', 'Company'),#Хуулийн этгээд
                                          ('self_emp', 'Self Employee'),#Хувиараа хөдөлмөр эрхэлдэг
                                          ('other', 'Other')], copy=False, string="State")
    family_members = fields.One2many('res.partner.family.line', 'res_partner', string='Family Members')
    house_expense = fields.Float('House Expense')
    house_income = fields.Float('House Income')
    duration_of_employment = fields.Char('Duration of Employment')#Ажилласан жил
    manager_name = fields.Char('Manager Name')#Холбогдох дээд албан тушаалтаны нэр
    manager_phone = fields.Integer('Manager Phone')#Холбогдох дээд албан тушаалтаны утасны дугаар
    is_paid_animal_tax = fields.Boolean('Is Paid Animal Tax')#Малын хөлийн татвар төлсөн эсэх
    duration_of_farmer = fields.Char('Duration of Farmer')#Мал малласан жил
    is_registered_animal = fields.Boolean('Is Registered Animal', default=False)#Малын тоо тооллогод бүртгүүлсэн эсэх
    is_insurance_animal = fields.Boolean('Is Insurance Animal', default=False)#Малын даатгалтай эсэх
    is_insurance = fields.Boolean('Is Insurance', default=False)#Даатгалтай эсэх
    is_reported_tax = fields.Boolean('Is Reported Tax', defualt=False)#Татварт тайлагнадаг эсэх
    profession = fields.Char('Profession')#Сурч буй мэргэжил
    is_than_score = fields.Boolean('Is Over Than 2.8 Score')#Голч дүн 2,8 давсан эсэх
    school = fields.Char('School Name')#Сургуулийн нэр
    course = fields.Selection([('course1', '1'),
                              ('course2', '2'),
                              ('course3', '3'),
                              ('course4', '4, 4+')], copy=False, string="Course")#Хэддүгээр анги
    pension_book_number = fields.Char('Pension Book Number')#Тэтгэврийн дэвтэрийн дугаар
    pension_date = fields.Date('Pension Date')#Тэтгэвэрт гарсан огноо
    operation_year = fields.Char('Operation Year')
    is_mongolian_citizen = fields.Boolean('Is Mongolian Citizen', defualt=False)#Монгол улсын иргэн эсэх
    manager_id = fields.Many2one('res.partner', 'Manager')#Гүйцэтгэх удирдлага
    manager_register = fields.Char(related='manager_id.register', string='Manager Register')#Гүйцэтгэх удирдлагын регистрийн дугаар, Гэр бүлийн регистерийн дугаар
    operation_type = fields.Selection([
        ('a', 'Agriculture, forestry, fishing and hunting'),
        ('b', 'Mining and quarrying'),
        ('c', 'Manufacturing'),
        ('d', 'Electricity, gas, steam and ventilation supply'),
        ('e', 'Water supply; wastewater, waste and waste management and treatment activities'),
        ('f', 'Building'),
        ('g', 'Wholesale and retail trade; car and motorcycle maintenance'),
        ('h', 'Transportation and warehousing operations'),
        ('i', 'Accommodation and catering services'),
        ('j', 'Information and communication'),
        ('k', 'Financial and insurance activities'),
        ('l', 'Real estate activities'),
        ('n', 'Administrative and support activities'),
        ('p', 'Education'),
        ('q', 'Administrative and support activitiesHuman health and social activities'),
        ('o', 'Public administration and defense activities, compulsory social protection'),
        ('m', 'Professional, scientific and technical activities'),
        ('r', 'Arts, entertainment and games'),
        ('s', 'Other service activities'),
        ('t', 'Hired household activities; Products and services produced for household consumption that cannot be quantified'),
        ('u', 'Activities of international organizations and permanent representatives'),
        ('24', 'Consumer loans'),
    ], copy=False, string="Type Of Operation")  # Үйл ажиллагааны чиглэл
    responsibility_type = fields.Selection([
        ('1', 'Limited company'),
        ('2', 'Limited liability company'),
        ('3', 'Savings and credit cooperatives'),
        ('4', 'Cooperative'),
        ('5', 'Non-governmental organization'),
        ('6', 'ONB'),
        ('7', 'Foundation'),
        ('8', 'Other'),
    ], copy=False, string="Type Of Operation")  # Компаний үйл ажиллагааны төрөл
    shareholder_id = fields.Many2one('res.partner', 'Shareholder')#Хувь эзэмшигч
    shareholder_ids = fields.Many2many('res.partner', 'shareholder_id', 'shareholder_rel_partner', 'shareholder_rel_partner2','Shareholders')#Хувь эзэмшигчид


class ResPartnerFamilyLine(models.Model):
    _name = "res.partner.family.line"
    _description = "Partner Family Lines"

    family_member_id = fields.Many2one('res.partner.family.member', string='Family Member')
    res_partner = fields.Many2one('res.partner')
    name = fields.Char('Name')
    age = fields.Date('Birth year')
    current_organization = fields.Char('Organization')
    current_job = fields.Char('Job')
    contact = fields.Char('Contact phone', size=16)
    place_of_birth = fields.Char('Place of Birth')
    is_relative = fields.Boolean('Is Relative', default=False)

class ResPartnerFamilyLine(models.Model):
    _name = "res.partner.family.member"
    _description = "Partner Family Member"

    name = fields.Char('Name', required=True)
    is_child = fields.Boolean(string='Is child', default=False)