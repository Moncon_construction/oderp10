# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Contract",
    'version': '1.0',
    'depends': ['l10n_mn_workflow_config', 'l10n_mn_report'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'maintainer': 'chinzorig.o@asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
    Description texts
    """,
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'data/contract_management_email_template.xml',
        'wizard/print_template.xml',
        'views/contract_management_view.xml',
        'views/contract_category_view.xml',
        'views/contract_cron.xml',
        'views/contract_inherited_account_invoice_view.xml',
        'views/contract_participation_view.xml',
        'views/inherited_res_partner_view.xml',
        'views/warranty_period_view.xml',
        'data/ir_cron_data.xml',
    ],
    'demo': [
    ],
}
