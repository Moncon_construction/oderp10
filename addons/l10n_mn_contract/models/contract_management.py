# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import time
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
import datetime

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from dateutil.relativedelta import relativedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT


class ContractManagement(models.Model):
    _name = 'contract.management'
    _inherit = ['mail.thread']

    @api.depends('contract_term_ids')
    @api.multi
    def _contract_term_total_amount(self):
        for obj in self:
            amount = 0.0
            for line in obj.contract_term_ids:
                amount += line.sub_total
            obj.contract_term_total_amount = amount

    @api.multi
    def _show_approve_button(self):
        res = {}
        history_obj = self.env['workflow.history']
        for contract in self:
            history = history_obj.search([('contract_id', '=', contract.id), (
                'line_sequence', '=', contract.check_sequence)], limit=1, order='sent_date DESC')
            if history:
                contract.show_approve_button = (
                    contract.state == 'send' and self.env.user.id in map(lambda x: x.id, history.user_ids))
            else:
                contract.show_approve_button = False
        return res

    @api.multi
    def _invoice_count(self):
        '''Гэрээний тоо
        '''
        for contract in self:
            contract.invoice_count = len(self.env['account.invoice'].sudo().search([('contract_id','=',contract.id)])) or 0

    @api.multi
    @api.depends('category_id')
    def _is_number_contract(self):
        # Гэрээг автомат дугаарлах эсэх
        for obj in self:
            if obj.category_id.is_auto_number:
                obj.is_number = True
                obj.name = '/'
            else:
                obj.is_number = False

    # Create method
    @api.model
    def create(self, vals):
        categ_id = self.env['contract.category'].search([('id', '=', vals['category_id'])])
        if categ_id.is_auto_number:
            seq_name = categ_id.contract_number.next_by_code(categ_id.contract_number.code)
            vals.update({'name': seq_name})
        return super(ContractManagement, self).create(vals)

    @api.multi
    def _compute_cash_amount(self):
        '''
        Гэрээний бэлэн мөнгөөр төлөгдөх дүн: Гэрээний нийт дүнгээс бартерын хувийг бодож хассан дүн
        '''
        for contract in self:
            barter_amount = (contract.untaxed_amount * contract.barter_percent) / 100
            contract.cash_amount = contract.untaxed_amount - barter_amount

    @api.multi
    @api.depends('barter_percent','total_amount')
    def _compute_untaxed_amount(self):
        '''
        Гэрээний татваргүй дүн
        '''
        for obj in self:
            computed_tax_amount = obj.untaxed_amount
            if obj.tax_id:
                tax_amount = obj.tax_id.compute_all(obj.total_amount)
                if obj.tax_id.price_include:
                    computed_tax_amount = tax_amount['total_excluded']
                else:
                    computed_tax_amount = tax_amount['total_included']
            obj.untaxed_amount = computed_tax_amount

            barter_amount = (obj.untaxed_amount * obj.barter_percent) / 100
            obj.cash_amount = obj.untaxed_amount - barter_amount

    state = fields.Selection([
                              ('draft', 'Draft'),
                              ('send', 'Send'),
                              ('confirm','Confirm'),
                              ('done','Done'),
                              ('closed','Closed')],
                             'State',default='draft',required=True, track_visibility='onchange')
    name = fields.Char('Name', required=True, readonly=True, track_visibility='onchange')
    analytic_account_id = fields.Many2one('account.analytic.account',
                                          string='Contract/Analytic',
                                          help="Link this project to an analytic account if you need financial management on contracts. "
                                          , required=True, track_visibility='onchange')

    category_id = fields.Many2one('contract.category', 'Category', required=True)
    partner_id = fields.Many2one('res.partner','Partner', required=True, track_visibility='onchange')
    company_id = fields.Many2one('res.company','Company', default=lambda self: self.env.user.company_id)
    start_date = fields.Date('Start date', default=fields.Date.context_today, required=True, track_visibility='onchange')
    end_date = fields.Date('End date', track_visibility='onchange')
    user_id = fields.Many2one('res.users','User', default=lambda self: self.env.uid, track_visibility='onchange')
    contract_id = fields.Many2one('contract.management','Contract', track_visibility='onchange', readonly=True)
    total_amount = fields.Float('Contract Total Amount', track_visibility='onchange')
    participation = fields.Many2one('contract.participation','Participation', required=True, track_visibility='onchange')
    auto_invoice = fields.Boolean('Auto Invoice', default=True)
    document = fields.One2many('contract.document','contract_id','Document')
    version_document = fields.One2many('contract.document','version_contract_id','Document')
    contract_term_total_amount = fields.Float(compute=_contract_term_total_amount, string='Contract term amount')
    contract_term_ids = fields.One2many('contract.term','contract_id','Contract Term')
    recurring_invoices = fields.Boolean('Generate recurring invoices automatically', default=False)
    recurring_invoice_line_ids = fields.One2many('contract.invoice.line', 'contract_id', 'Invoice Lines', copy=True)
    recurring_rule_type = fields.Selection([
                                        ('daily', 'Day(s)'),
                                        ('weekly', 'Week(s)'),
                                        ('monthly', 'Month(s)'),
                                        ('yearly', 'Year(s)'),
                                        ], 'Recurrency', help="Invoice automatically repeat at specified interval", default='monthly')
    recurring_interval = fields.Integer('Repeat Every', help="Repeat every (Days/Week/Month/Year)", default=1)
    recurring_next_date = fields.Date('Date of Next Invoice', default=fields.Date.context_today)
    workflow_id = fields.Many2one('workflow.config', 'Workflow', domain=[('model_id.model','=','contract.management')], required=True)
    check_sequence = fields.Integer(string='Workflow Step', default=0)
    check_users = fields.Many2many('res.users', 'contract_management_check_user_rel', 'contract_id', 'user_id', 'Checkers', readonly=True, copy=False)
    show_approve_button = fields.Boolean(string='Show Approve Button?', compute='_show_approve_button')
    step_is_final = fields.Boolean(string='Final Step', default=False)
    invoice_count = fields.Integer(compute=_invoice_count, string='Invoice Count')
    create_date = fields.Date('Create Date', default=fields.Date.context_today)
    tax_id = fields.Many2one('account.tax', 'Account Tax')
    untaxed_amount = fields.Float('Untaxed Amount', readonly=True, compute="_compute_untaxed_amount")
    barter = fields.Char('Barter')
    barter_percent = fields.Float('Barter Percent')
    cash_amount = fields.Float('Contract Cash Amount', compute="_compute_cash_amount", readonly=True)
    contract_user_id = fields.Many2one('res.users', 'Contract User', required=True)
    department_id = fields.Many2one('hr.department', 'Department')
    warranty_date = fields.Many2one('warranty.period', 'Warranty Date')
    undue_loss_rate = fields.Float('Undue loss rate')
    contract_approved_doc = fields.Binary('Contract Approved Document')
    file_name = fields.Char('File name')
    days_to_end = fields.Integer(compute='_compute_days_to_end')
    is_number =  fields.Boolean(string='Is Number', compute="_is_number_contract")
    description = fields.Text('Description')

    @api.multi
    def _compute_days_to_end(self):
        for obj in self:
            expire_days = 0
            if self.end_date:
                expire_days = (datetime.datetime.strptime(self.end_date, DEFAULT_SERVER_DATE_FORMAT).date() - datetime.datetime.now().date()).days
            obj.days_to_end = expire_days

    @api.onchange('contract_user_id')
    def _onchange_contract_user(self):
        for line in self:
            line.department_id = line.contract_user_id.department_id

    @api.multi
    def check_invoices(self, invoices):
        unpaid_invoice_count = 0
        for invoice in invoices:
            if invoice.state and invoice.state not in ('paid','cancel'):
                unpaid_invoice_count += 1
        if unpaid_invoice_count == 0:
            return True
        else:
            return False

    @api.multi
    def get_invoice(self, line_ids):
        lines = []
        for line in line_ids:
            lines.append(line.invoice_id)
        return lines

    @api.multi
    def action_state(self, state):
        for obj in self:
            if obj.recurring_invoices == False:
                invoices = obj.get_invoice(obj.contract_term_ids)
                if obj.check_invoices(invoices) == True:
                    obj.state = state
                else:
                    raise UserError(_('There is unpaid invoice!'))
            else:
                invoice_ids = self.env['account.invoice'].search([('contract_id','=',obj.id)])
                if obj.check_invoices(invoice_ids) == True:
                    obj.state = state
                else:
                    raise UserError(_('There is unpaid invoice!'))

    @api.multi
    def action_close(self):
        self.action_state('closed')

    @api.multi
    def action_done(self):
        self.action_state('done')

    @api.onchange('category_id')
    def onchange_category(self):
        for obj in self:
            if obj.category_id:
                if obj.category_id.workflow_id:
                    obj.workflow_id = obj.category_id.workflow_id.id
                else:
                    raise UserError(_("You must first select a Workflow for Contract Category %s!"%obj.category_id.name))

    @api.multi
    def action_send(self):
        for obj in self:
            if round(obj.total_amount, 2) != round(obj.contract_term_total_amount, 2):
                raise UserError(_('The total amount of the contract term is not equal to the total amount of the contract.'))
            if obj.workflow_id:
                workflow_obj = self.env['workflow.config']
                success, current_sequence = workflow_obj.send('workflow.history', 'contract_id', obj, self.env.user.id)
                is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(obj.workflow_id.id, obj, self.env.user.id, obj.check_sequence + 1, 'next')
                if is_next:
                    obj.check_users = [(6, 0, next_user_ids)]
                if success:
                    obj.check_sequence = current_sequence
                    obj.ensure_one()
                    obj.state = 'send'
                    self._line_state_update('send')
        return True

    @api.multi
    def action_confirm(self):
        self.ensure_one()
        workflow_obj = self.env['workflow.config']
        if not self.contract_approved_doc:
            raise UserError(_('Please, Attach the approved version of the contract.'))
        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve(
                'workflow.history', 'contract_id', self, self.env.user.id)
            is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(self.workflow_id.id, self, self.env.user.id, self.check_sequence + 1, 'next')
            if is_next:
                self.check_users = [(6, 0, next_user_ids)]
            if success:
                if sub_success:
                    self.state = 'confirm'
                    self._line_state_update('confirm')
                    self.step_is_final = True
                else:
                    self.check_sequence = current_sequence
                    self.show_approve_button = True
            return True

    @api.multi
    def action_draft(self):
        for obj in self:
            obj.state = 'draft'
            obj._line_state_update('draft')
            if obj.workflow_history_ids:
                obj.workflow_history_ids.unlink()
                obj.check_sequence = 0

    @api.multi
    def _line_state_update(self, state):
        for obj in self:
            for line in obj.contract_term_ids:
                line.write({'state': state})

    @api.multi
    def unlink(self):
        for contract in self:
            if contract.state != 'draft':
                        raise UserError(_('You can delete only draft contract'))
        return super(ContractManagement, self).unlink()

# ===========================================Давтагдах нэхэмжилэх үүсгэх======================================================
    @api.multi
    def _prepare_invoice_data(self, contract):

        journal_obj = self.env['account.journal']
        fpos_obj = self.env['account.fiscal.position']
        partner = contract.partner_id

        invoice_type = contract.participation.invoice_type

        if not partner:
            raise UserError(_('No Customer Defined!'),_("You must first select a Customer for Contract %s!") % contract.name )

        fpos_id = fpos_obj.get_fiscal_position(partner.id)
        journal_ids = journal_obj.search([('type', '=','sale'),('company_id', '=', contract.company_id.id or False)], limit=1)
        if not journal_ids:
            raise UserError(_('Error!'),_('Please define a sale journal for the company "%s".') % (contract.company_id.name or ''))

        partner_payment_term = partner.property_payment_term_id.id if partner.property_payment_term_id else False

        currency_id = contract.company_id.currency_id.id

        invoice = {
           'account_id': partner.property_account_receivable_id.id if invoice_type=='out_invoice' else partner.property_account_payable_id.id,
           'type': invoice_type,
           'partner_id': partner.id,
           'currency_id': currency_id,
           'journal_id': journal_ids[0].id or False,
           'date_invoice': contract.recurring_next_date,
           'origin': contract.name,
           'name': contract.name,
           'fiscal_position_id': fpos_id,
           'payment_term_id': partner_payment_term,
           'company_id': contract.company_id.id or False,
           'user_id': contract.user_id.id or self._uid,
           'contract_id' : contract.id
        }
        return invoice

    def _prepare_invoice_line(self, line):
        fpos_obj = self.env['account.fiscal.position']
        res = line.product_id
        invoice_type = line.contract_id.participation.invoice_type

        account_id = res.property_account_income_id.id if invoice_type == 'out_invoice' else res.property_account_expense_id.id
        if not account_id:
            account_id = res.categ_id.property_account_income_categ_id.id if invoice_type == 'out_invoice' else res.categ_id.property_account_expense_categ_id.id
        account_id = fpos_obj.map_account(account_id)

        taxes = res.taxes_id or False
        if taxes:
            tax_id = fpos_obj.map_tax(taxes)
        values = {
            'name': line.name,
            'account_id': account_id,
            'account_analytic_id': line.contract_id.analytic_account_id.id,
            'price_unit': line.price_unit or 0.0,
            'quantity': line.quantity,
            'uom_id': line.uom_id.id or False,
            'product_id': line.product_id.id or False,
            'invoice_line_tax_ids': [(6, 0, tax_id.ids)] if tax_id else [],
        }
        return values

    @api.multi
    def _prepare_invoice_lines(self, contract):
        invoice_lines = []
        for line in contract.recurring_invoice_line_ids:
            values = self._prepare_invoice_line(line)
            invoice_lines.append((0, 0, values))
        return invoice_lines

    @api.multi
    def _prepare_invoice(self, contract):
        invoice = self._prepare_invoice_data(contract)
        invoice['invoice_line_ids'] = self._prepare_invoice_lines(contract)
        return invoice

    @api.multi
    def _cron_reccuring_create_invoice(self):
        current_date =  time.strftime('%Y-%m-%d')
        contract_ids = self.search([('recurring_next_date','<=', current_date), ('state','=', 'confirm'), ('recurring_invoices','=', True)])
        if contract_ids:
            self._recurring_create_invoice(contract_ids)

    @api.multi
    def recurring_create_invoice(self):
        self._recurring_create_invoice(self)

    @api.multi
    def _recurring_create_invoice(self, contract_ids):
        invoice_ids = []
        current_date =  time.strftime('%Y-%m-%d')
        if contract_ids:
            for contract in contract_ids:
                invoice_values = self._prepare_invoice(contract)
                invoice_ids.append(self.env['account.invoice'].create( invoice_values))
                next_date = datetime.datetime.strptime(contract.recurring_next_date or current_date, "%Y-%m-%d")
                interval = contract.recurring_interval
                if contract.recurring_rule_type == 'daily':
                    new_date = next_date+relativedelta(days=+interval)
                elif contract.recurring_rule_type == 'weekly':
                    new_date = next_date+relativedelta(weeks=+interval)
                elif contract.recurring_rule_type == 'monthly':
                    new_date = next_date+relativedelta(months=+interval)
                else:
                    new_date = next_date+relativedelta(years=+interval)
                contract.recurring_next_date = new_date.strftime('%Y-%m-%d')
        return invoice_ids

    def notification_expiration_email_by_month(self):
        email_template = self.env.ref('l10n_mn_contract.contract_management_expiration_email_template', False)
        contracts = self.search([('end_date', '=', datetime.datetime.now().date() + relativedelta(months=1))])  # төлөв хамаарахгүй явуулах
        for contract in contracts:
            email = self.env['mail.mail'].browse(email_template.send_mail(contract.id))
            email.recipient_ids = contract.message_follower_ids.mapped('partner_id').ids
            email.send()

    def notification_expiration_email_by_week(self):
        email_template = self.env.ref('l10n_mn_contract.contract_management_expiration_email_template', False)
        contracts = self.search([('end_date', '=', datetime.datetime.now().date() + datetime.timedelta(weeks=1))])  # төлөв хамаарахгүй явуулах
        for contract in contracts:
            email = self.env['mail.mail'].browse(email_template.send_mail(contract.id))
            email.recipient_ids = contract.message_follower_ids.mapped('partner_id').ids
            email.send()

# ===========================================Гэрээний дуусах хугацааны сануулга ======================================================

    def get_after_record_ids(self, user_id):
        records=[]
        after_categ_ids = self.env['contract.category'].search([('is_after','=','True')])
        if after_categ_ids:
            for category in after_categ_ids:
                count =  category.after_date_count
                date_check =  datetime.now() - timedelta(days=count)
                date_end_check = date_check.strftime('%Y.%m.%d')
                after_contract_ids = self.env['contract.management'].search([
                                                                            ('contract_user_id','=', user_id.id),
                                                                                  ('state','=','confirm'),
                                                                                ('category_id','=', category.id),
                                                                                  ('end_date','=', date_end_check)
                                                                                  ])
                for contract_id in after_contract_ids:
                    contracts={}
                    if contract_id:
                        contracts['category'] =  contract_id.category_id.name
                        contracts['name'] = contract_id.name
                        contracts['partner_name'] = contract_id.partner_id.name
                        contracts['partner_email'] = contract_id.partner_id.email
                        contracts['partner_phone'] = contract_id.partner_id.phone
                        contracts['start_date'] = contract_id.start_date
                        contracts['end_date'] = contract_id.end_date
                        records.append(contracts)
        return records

    def get_before_record_ids(self, user_id):
        records=[]
        before_categ_ids = self.env['contract.category'].search([('is_before','=','True')])
        if before_categ_ids:
            for category in before_categ_ids:
                count =  category.before_date_count
                date_check =  datetime.now()+ timedelta(days=count)
                date_end_check = date_check.strftime('%Y.%m.%d')
                before_contract_ids = self.env['contract.management'].search([
                                                                            ('contract_user_id','=', user_id.id),
                                                                                  ('state','=','confirm'),
                                                                                ('category_id','=', category.id),
                                                                                  ('end_date','=', date_end_check)
                                                                                  ])
                for contract_id in before_contract_ids:
                    contracts={}
                    if contract_id:
                        contracts['category'] =  contract_id.category_id.name
                        contracts['name'] = contract_id.name
                        contracts['partner_name'] = contract_id.partner_id.name
                        contracts['partner_email'] = contract_id.partner_id.email
                        contracts['partner_phone'] = contract_id.partner_id.phone
                        contracts['start_date'] = contract_id.start_date
                        contracts['end_date'] = contract_id.end_date
                        records.append(contracts)
        return records

    @api.multi
    def send_mail(self, user_id, contract_ids):
        out_template = self.env.ref('l10n_mn_contract.contract_management_expiration_email_contract_user_template', False)
        email = self.env['mail.template'].browse(out_template.id).send_mail(contract_ids[0].id)
        mail = self.env['mail.mail'].browse(email)
        mail.send()

    @api.multi
    def _cron_alert(self):
        self._cr.execute("select distinct(u.id) from res_users u left join contract_management as cm on cm.contract_user_id = u.id where u.id=cm.contract_user_id")
        contract_users = self._cr.fetchall()
        for user_id in contract_users:
            contract_ids = []
            user = self.env['res.users'].search([('id', '=', user_id[0])])
            if user:
                after_categ_ids = self.env['contract.category'].search([('is_after','=','True')])
                if after_categ_ids:
                    for category in after_categ_ids:
                        count =  category.after_date_count
                        date_check =  datetime.now()- timedelta(days=count)
                        date_end_check = date_check.strftime('%Y.%m.%d')
                        contract_id = self.env['contract.management'].search([
                                                                            ('contract_user_id','=', user.id),
                                                                                  ('state','=','confirm'),
                                                                                ('category_id','=', category.id),
                                                                                  ('end_date','<=', date_end_check)
                                                                                  ])
                        if contract_id:
                            if len(contract_id)>1:
                                for contract in contract_id:
                                    contract_ids.append(contract)
                            else:
                                contract_ids.append(contract_id)
                before_categ_ids = self.env['contract.category'].search([('is_before','=','True')])
                if before_categ_ids:
                    for category in before_categ_ids:
                        count =  category.before_date_count
                        date_check =  datetime.now()+ timedelta(days=count)
                        date_end_check = date_check.strftime('%Y.%m.%d')
                        contract_id = self.env['contract.management'].search([
                                                                            ('contract_user_id','=', user.id),
                                                                                  ('state','=','confirm'),
                                                                                ('category_id','=', category.id),
                                                                                  ('end_date','=', date_end_check)
                                                                                  ])
                        if contract_id:
                            if len(contract_id)>1:
                                for contract in contract_id:
                                    contract_ids.append(contract)
                            else:
                                contract_ids.append(contract_id)
                if contract_ids:
                    self.send_mail(user, contract_ids)

# class ContractConfig(modesl.Model):
#     _name = 'contract.config'
#     
class ContractInvoiceLine(models.Model):
    _description = "Contract Invoice Line"
    _name = 'contract.invoice.line'

    def _amount_line(self):
        for line in self:
            line.price_subtotal = line.quantity * line.price_unit

    product_id = fields.Many2one('product.product', 'Product', required=True)
    contract_id = fields.Many2one('contract.management', 'Contract', ondelete='cascade')
    name = fields.Text('Description', required=True)
    quantity = fields.Float('Quantity', default=1, required=True)
    uom_id = fields.Many2one('product.uom', 'Unit of Measure',required=True)
    price_unit = fields.Float('Unit Price', required=True)
    price_subtotal = fields.Float(compute=_amount_line, string='Sub Total')

    @api.onchange('product_id')
    def onchange_product_id(self):
        for line in self:
            line.name = line.product_id.name
            line.uom_id = line.product_id.product_tmpl_id.uom_id.id

class InheritedAccountInvoice(models.Model):
    _inherit = 'account.invoice'

    contract_id = fields.Many2one('contract.management', 'Contract', readonly=True)

class ContractCategory(models.Model):
    _description = "Contract Category"
    _name = 'contract.category'
    _inherit = ['mail.thread']

    name = fields.Char('Name', required = True, track_visibility='onchange')
    description = fields.Char('Description', track_visibility='onchange')
    workflow_id = fields.Many2one('workflow.config', 'Workflow', domain=[('model_id.model','=','contract.management')], required=True, track_visibility='onchange')
    is_before = fields.Boolean('Before End Date', track_visibility='onchange')
    before_date_count = fields.Integer('Before Date Count', track_visibility='onchange')
    is_after = fields.Boolean('After End Date', track_visibility='onchange')
    after_date_count = fields.Integer('After Date Count', track_visibility='onchange')
    is_auto_number =  fields.Boolean('Is Number')
    contract_number = fields.Many2one('ir.sequence', 'Number',)


    _sql_constraints = [
            ('name_uniq', 'unique (name)', "Contract Category already exists !"),
    ]

    @api.multi
    def unlink(self):
        for categ in self:
            contract_ids = self.env['contract.management'].search([('category_id','=',categ.id)])
            if contract_ids:
                        raise UserError(_('You can not delete used category'))
        return super(ContractCategory, self).unlink()

    attachment_id = fields.Many2one('ir.attachment', string=u'Загвар файл')
class ContractDocument(models.Model):
    _name = 'contract.document'

    name = fields.Char('Name', required=True)
    date = fields.Datetime('Date',readonly=True, default=fields.Date.context_today)
    data = fields.Binary('Data', required=True)
    user_id = fields.Many2one('res.users', 'User', default=lambda self: self.env.uid, readonly=True)
    contract_id = fields.Many2one('contract.management', 'Contract')
    version_contract_id = fields.Many2one('contract.management', 'Contract')
    file_name = fields.Char('File name')
class ContractParticipation(models.Model):
    _name = 'contract.participation'
    _inherit = ['mail.thread']

    name = fields.Char('Name', required=True)
    invoice_type = fields.Selection([('out_invoice', 'Out invoice'),
                                     ('in_invoice', 'In invoice')],
                                    'Invoice type', default='out_invoice', required=True)
    _sql_constraints = [
            ('name_uniq', 'unique (name)', "Contract Participation already exists !"),
    ]

    @api.multi
    def unlink(self):
        for part in self:
            contract_ids = self.env['contract.management'].search([('participation','=',part.id)])
            if contract_ids:
                        raise UserError(_('You can not delete used participation'))
        return super(ContractParticipation, self).unlink()

class WorkflowHistory(models.Model):
    _inherit = 'workflow.history'

    contract_id = fields.Many2one('contract.management', 'Cash Expense')


class InheritHrExpenseCash(models.Model):
    _inherit = "contract.management"

    workflow_history_ids = fields.One2many(
        'workflow.history', 'contract_id', string='History', readonly=True)

class WarrantyPeriod(models.Model):
    _name = 'warranty.period'
    _rec_name = 'period'
    _order = 'sequence'

    period = fields.Char('Warranty period', required=True)
    sequence = fields.Integer('Sequence', required=True)
