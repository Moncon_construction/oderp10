# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import time
import datetime

from odoo import api, fields, models, _
from odoo.exceptions import UserError

class ContractTerm(models.Model):
    _name = 'contract.term'

    @api.multi
    def _compute_paid_total(self):
        for obj in self:
            if obj.invoice_id:
                if obj.invoice_id.state == 'draft':
                    obj.paid_total = 0.0
                else:
                    if obj.invoice_id.state == 'paid':
                        obj.paid_total = obj.sub_total
                    else:
                        obj.paid_total = obj.invoice_id.amount_total - obj.invoice_id.residual
            else: 
                obj.paid_total = 0.0

    @api.multi
    def _compute_subtotal(self):
        for obj in self:
            if obj.amount_type == 'money':
                obj.sub_total = obj.amount - (obj.amount * (obj.discount/100))
            elif obj.amount_type == 'percent':
                obj.sub_total = (obj.contract_id.total_amount or 0.0) * (obj.amount/100)

    @api.onchange('amount_type')
    def onchange_amount_type(self):
        for obj in self:
            if obj.amount_type == 'percent':
                obj.discount = 0.0

    @api.onchange('product_id')
    def onchange_product_id(self):
        for obj in self:
            obj.name = obj.product_id.name

    state = fields.Selection([('draft', 'Draft'),
                              ('send', 'Send'),
                              ('confirm', 'Confirm'),
                              ('invoiced', 'Invoiced')],
                             'State', default='draft', required=True, readonly=True)
    product_id = fields.Many2one('product.product', 'Product', required=True)
    name = fields.Char('Name', required=True)
    invoice_date = fields.Date('Invoice Date', required=True)
    end_date = fields.Date('Final Date', required=True)
    amount = fields.Float('Amount', required=True)
    amount_type = fields.Selection([('money','Money'),
                                    ('percent','Percent')], default='money', required=True)
    discount = fields.Float('Discount')
    sub_total = fields.Float(compute=_compute_subtotal, string='Sub Total')
    paid_total = fields.Float(compute=_compute_paid_total, string='Paid Total')
    invoice_id = fields.Many2one('account.invoice', 'Invoice', readonly=True)
    contract_id = fields.Many2one('contract.management', 'Contract')
    currency_id = fields.Many2one('res.currency', 'Currency', default=lambda self: self.env.user.company_id.currency_id)

    @api.multi
    def _prepare_invoice_data(self, term_id):

        journal_obj = self.env['account.journal']
        fpos_obj = self.env['account.fiscal.position']
        partner = term_id.contract_id.partner_id
        invoice_type = term_id.contract_id.participation.invoice_type
        if not partner:
            raise UserError(_("You must first select a Customer for Contract %s!") % term_id.contract_id.name )

        fpos_id = fpos_obj.get_fiscal_position(partner.id)
        journal_ids = []
        
        if term_id.contract_id.participation.invoice_type == 'out_invoice':
            journal_ids = journal_obj.search([('type', '=','sale'),('company_id', '=', term_id.contract_id.company_id.id or False)], limit=1)
        else:
            journal_ids = journal_obj.search([('type', '=','purchase'),('company_id', '=', term_id.contract_id.company_id.id or False)], limit=1)
            
        if not journal_ids:
            raise UserError(_('Error!'))

        partner_payment_term = partner.property_payment_term_id.id if partner.property_payment_term_id else False

        invoice = {
           'account_id': partner.property_account_receivable_id.id if invoice_type=='out_invoice' else partner.property_account_payable_id.id,
           'type': invoice_type,
           'partner_id': partner.id,
           'currency_id': term_id.currency_id.id or term_id.contract_id.company_id.currency_id.id or False,
           'journal_id': journal_ids[0].id or False,
           'date_invoice': term_id.invoice_date,
           'origin': term_id.contract_id.name,
           'fiscal_position_id': fpos_id,
           'payment_term_id': partner_payment_term,
           'company_id': term_id.contract_id.company_id.id or False,
           'user_id': term_id.contract_id.user_id.id or self._uid,
           'contract_id' : term_id.contract_id.id,
           'name': term_id.name,
           'date_due': term_id.end_date
        }
        return invoice

    def _prepare_invoice_line(self, term_id):
        fpos_obj = self.env['account.fiscal.position']
        res = term_id.product_id
        invoice_type = term_id.contract_id.participation.invoice_type
        company = term_id.contract_id.company_id
        
        account = self.env['account.invoice.line'].get_invoice_line_account(invoice_type, term_id.product_id, False, company)

        if not account:
            raise UserError(_('No account detected product or product category %s!')%term_id.product_id.name )

        taxes = res.taxes_id if invoice_type == 'out_invoice' else res.supplier_taxes_id
        taxes = taxes.filtered(lambda r: not company or r.company_id == company) if taxes else False
        taxes = fpos_obj.map_tax(taxes) if taxes else False

        values = {
            'name': term_id.name,
            'account_id': account.id,
            'account_analytic': term_id.contract_id.analytic_account_id.id,
            'price_unit': term_id.sub_total or 0.0,
            'quantity': 1,
            'uom_id': term_id.product_id.product_tmpl_id.uom_id.id or False,
            'product_id': term_id.product_id.id or False,
            'invoice_line_tax_ids': [(6, 0, taxes.ids)] if taxes else [],
        }
        return values

    @api.multi
    def _prepare_invoice_lines(self, term_id):
        invoice_lines = []
        values = self._prepare_invoice_line(term_id)
        invoice_lines.append((0, 0, values))
        return invoice_lines

    @api.multi
    def _prepare_invoice(self, term_id):
        invoice = self._prepare_invoice_data(term_id)
        invoice['invoice_line_ids'] = self._prepare_invoice_lines(term_id)
        return invoice

    @api.multi
    def _cron_create_invoice(self):
        current_date =  time.strftime('%Y-%m-%d')
        term_ids = self.search([('invoice_date','<=', current_date), ('state','=', 'confirm'),
                                ('contract_id.auto_invoice','=', True), ('contract_id.recurring_invoices', '=', False)])
        self._create_invoice(term_ids)

    @api.multi
    def create_invoice(self):
        for obj in self:
            self._create_invoice(obj)
 
    @api.multi
    def _create_invoice(self, term_ids):
        invoice_ids = []
        if term_ids:
            for term_id in term_ids:
                invoice_values = self._prepare_invoice(term_id)
                invoice = self.env['account.invoice'].create(invoice_values)
                invoice.action_invoice_open()
                invoice_ids.append(invoice)
                term_id.invoice_id = invoice.id
                term_id.state = 'invoiced'
        return invoice_ids

    @api.multi
    def send_before_mail(self, term_ids):
        out_template = self.env.ref('l10n_mn_contract.contract_management_before_email_template', False)
        in_template = self.env.ref('l10n_mn_contract.in_contract_management_before_email_template', False)
        current_date =  time.strftime('%Y-%m-%d')
        for term_id in term_ids:
            date = (datetime.datetime.strptime(current_date, '%Y-%m-%d') + datetime.timedelta(days=term_id.contract_id.category_id.before_date_count)).strftime('%Y-%m-%d')
            if date == term_id.end_date:
                data = {
                        'partner': term_id.contract_id.partner_id,
                        'name': term_id.contract_id.name,
                        'start_date':term_id.invoice_date,
                        'end_date':term_id.end_date,
                        'sname':term_id.name,
                        }
                if term_id.contract_id.participation.invoice_type == 'out_invoice':
                    self.env['mail.template'].browse(out_template.id).send_mail(term_id.id, email_values=data)
                elif term_id.contract_id.participation.invoice_type == 'in_invoice':
                    self.env['mail.template'].browse(in_template.id).send_mail(term_id.id, email_values=data)

    @api.multi
    def send_after_mail(self, term_ids):
        out_template = self.env.ref('l10n_mn_contract.contract_management_after_email_template', False)
        in_template = self.env.ref('l10n_mn_contract.in_contract_management_after_email_template', False)
        current_date =  time.strftime('%Y-%m-%d')
        for term_id in term_ids:
            date = (datetime.datetime.strptime(current_date, '%Y-%m-%d') - datetime.timedelta(days=term_id.contract_id.category_id.after_date_count)).strftime('%Y-%m-%d')
            if date == term_id.end_date:
                data = {
                        'partner': term_id.contract_id.partner_id,
                        'name': term_id.contract_id.name,
                        'start_date':term_id.invoice_date,
                        'end_date':term_id.end_date,
                        'sname':term_id.name,
                        }
                if term_id.contract_id.participation.invoice_type == 'out_invoice':
                    self.env['mail.template'].browse(out_template.id).send_mail(term_id.id, email_values=data)
                elif term_id.contract_id.participation.invoice_type == 'in_invoice':
                    self.env['mail.template'].browse(in_template.id).send_mail(term_id.id, email_values=data)

    @api.multi
    def _cron_alert(self):
        current_date =  time.strftime('%Y-%m-%d')
        before_term_ids = self.env['contract.term'].search([('contract_id.category_id.is_before','=',True),('invoice_id.state','!=','paid'),('end_date','>=',current_date)])
        after_term_ids = self.env['contract.term'].search([('contract_id.category_id.is_after','=',True),('invoice_id.state','!=','paid'),('end_date','<=',current_date)])
        if before_term_ids:
            self.send_before_mail(before_term_ids)
        if after_term_ids:
            self.send_after_mail(after_term_ids)
