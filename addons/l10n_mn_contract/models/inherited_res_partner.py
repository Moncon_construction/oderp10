# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models

class InheritedResPartner(models.Model):
    _inherit = 'res.partner'

    contract_count = fields.Integer(compute='_contract_count' ,string = 'Contract Count')

    @api.multi
    def _contract_count(self):
        for partner in self:
            partner.contract_count = len(self.env['contract.management'].sudo().search([('partner_id','=',partner.id)])) or 0
        return True