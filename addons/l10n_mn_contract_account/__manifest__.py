# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Contract Account",
    'version': '1.0',
    'depends': ['l10n_mn_contract', 'l10n_mn_account'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Contract Account Additional Features
    """,
    'data': [
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}