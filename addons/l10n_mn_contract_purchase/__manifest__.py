# -*- coding: utf-8 -*-

{
    "name": "Гэрээний худалдан авалт",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "description": """
        Гэрээнээс худалдан авалт үүсэх нэмэлт хөгжүүлэлт хийсэн модуль
""",
    'website': "http://www.asterisk-tech.mn",
    "category": "Mongolian Modules",
    "depends": ['l10n_mn_contract_term_extend','l10n_mn_purchase'],
    "init": [],
    "data": [
        'views/contract_management_view.xml',
        'views/contract_purchase_view.xml'
    ],
}
