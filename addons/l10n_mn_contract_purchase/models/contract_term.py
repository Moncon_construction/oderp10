# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class ContractManagement(models.Model):
    _inherit = 'contract.management'

    purchase_warehouse_id = fields.Many2one('stock.warehouse', string='Хүлээн авах агуулах')
    purchase_picking_type_id = fields.Many2one('stock.picking.type', string='Хүлээн авах байрлал', domain="[('code', '=', 'incoming'),('warehouse_id','=',purchase_warehouse_id)]")

    @api.onchange('purchase_warehouse_id')
    def _onchange_purchase_warehouse_id(self):
        domain = {}
        _warehouses = []
        if self.purchase_warehouse_id:
            purchase_warehouse_id = self.env['stock.picking.type'].search([('warehouse_id', '=', self.purchase_warehouse_id.id), ('code' ,'=', 'incoming')],
                                                                       limit=1)
            if purchase_warehouse_id:
                self.purchase_picking_type_id = purchase_warehouse_id.id

        for warehouse in self.env.user.allowed_warehouses:
            _warehouses.append(warehouse.id)
        if _warehouses:
            domain['purchase_picking_type_id'] = [('id', 'in', _warehouses)]
        return {'domain': domain}

    @api.multi
    def _purchase_count(self):
        '''Гэрээний тоо
        '''
        for contract in self:
            contract.purchase_count = len(self.env['purchase.order'].sudo().search([('contract_id','=',contract.id)])) or 0

    purchase_count = fields.Integer(compute=_purchase_count, string='Purchase Count')


class ContractTerm(models.Model):
    _inherit = 'contract.term'

    @api.multi
    def _compute_invoice_type(self):
        for obj in self:
            if obj.contract_id.participation.invoice_type == 'purchase':
                obj.type = 'purchase'
            elif obj.contract_id.participation.invoice_type == 'sale':
                obj.type = 'sale'
            elif obj.contract_id.participation.invoice_type == 'in_invoice' or obj.contract_id.participation.invoice_type == 'out_invoice':
                obj.type = 'invoice'

    type = fields.Selection(selection_add=[('purchase', 'Purchase')], compute=_compute_invoice_type)
    purchase_id = fields.Many2one('purchase.order', string='Purchase')
    state = fields.Selection(selection_add=[('purchased', 'Purchased')])

    @api.multi
    def _compute__purchase_paid_total(self):
        for obj in self:
            if obj.purchase_id:
                paid_total = 0
                for invoice_id in obj.purchase_id.invoice_ids:
                    if invoice_id:
                        if obj.invoice_id.state == 'draft':
                            paid_total = 0.0
                        elif obj.invoice_id.state == 'paid':
                            paid_total = obj.sub_total
                        else:
                            paid_total = invoice_id.amount_total - invoice_id.residual
                    else:
                        paid_total = 0.0
                obj.paid_total = paid_total

    @api.multi
    def _prepare_purchase_data(self, term_id):

        partner = term_id.contract_id.partner_id
        if not partner:
            raise UserError(_("You must first select a Customer for Contract %s!") % term_id.contract_id.name)

        partner_payment_term = partner.property_payment_term_id.id if partner.property_payment_term_id else False

        currency_id = term_id.contract_id.company_id.currency_id.id
        purchase = {
            'partner_id': partner.id,
            'currency_id': currency_id,
            'date_invoice': term_id.invoice_date,
            'origin': term_id.contract_id.name,
            'payment_term_id': partner_payment_term,
            'company_id': term_id.contract_id.company_id.id or False,
            'user_id': term_id.contract_id.user_id.id or self._uid,
            'contract_id': term_id.contract_id.id,
            'date_order': term_id.end_date,
            'date_planned': term_id.end_date,
            'picking_type_id': term_id.contract_id.purchase_picking_type_id.id
        }
        return purchase

    def _prepare_purchase_line(self, term_id):
        values = {
            'name': term_id.product_id.display_name,
            'price_unit': term_id.amount or 0.0,
            'product_qty': term_id.quantity,
            'product_uom': term_id.uom_id.id or False,
            'product_id': term_id.product_id.id or False,
            'date_planned': term_id.end_date,
            'discount': term_id.discount or 0.0
        }
        return values

    @api.multi
    def _prepare_purchase_lines(self, term_id):
        purchase_lines = []
        values = self._prepare_purchase_line(term_id)
        purchase_lines.append((0, 0, values))
        return purchase_lines

    @api.multi
    def _prepare_purchase(self, term_id):
        purchase = self._prepare_purchase_data(term_id)
        purchase['order_line'] = self._prepare_purchase_lines(term_id)
        return purchase

    @api.multi
    def _create_invoice(self, term_ids):
        ids = []
        if term_ids:
            for term_id in term_ids:
                if term_id.type == 'invoice':
                    invoice_values = self._prepare_invoice(term_id)
                    invoice = self.env['account.invoice'].create(invoice_values)
                    invoice.action_invoice_open()
                    ids.append(invoice)
                    term_id.invoice_id = invoice.id
                    term_id.state = 'invoiced'
                elif term_id.type == 'sale':
                    sale_values = self._prepare_sale(term_id)
                    sale = self.env['sale.order'].create(sale_values)
                    ids.append(sale)
                    term_id.sale_id = sale.id
                    term_id.state = 'confirm'
                elif term_id.type == 'purchase':
                    purchase_values = self._prepare_purchase(term_id)
                    purchase = self.env['purchase.order'].create(purchase_values)
                    term_id.purchase_id = purchase.id
                    ids.append(purchase)
                    term_id.state = 'confirm'
        return ids


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    contract_id = fields.Many2one('contract.management', 'Contract', readonly=True)

class ContractParticipation(models.Model):
    _inherit = 'contract.participation'

    invoice_type = fields.Selection(selection_add=[('purchase', 'Purchase')])