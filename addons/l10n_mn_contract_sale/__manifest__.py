# -*- coding: utf-8 -*-

{
    "name": "Гэрээний Борлуулалт",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "description": """
        Гэрээнээс Борлуулалт үүсэх нэмэлт хөгжүүлэлт хийсэн модуль
""",
    'website': "http://www.asterisk-tech.mn",
    "category": "Mongolian Modules",
    "depends": ['l10n_mn_contract_term_extend'],
    "init": [],
    "data": [
        'views/contract_management_view.xml',
        'views/contract_sale_view.xml'
    ],
}
