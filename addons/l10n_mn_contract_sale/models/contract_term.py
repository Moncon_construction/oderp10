# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError
import time


class ContractManagement(models.Model):
    _inherit = 'contract.management'

    sale_warehouse_id = fields.Many2one('stock.warehouse',  string="Гарах Агуулах")
    sale_picking_type_id = fields.Many2one('stock.picking.type', string='Гарах байрлал',
                                           domain="[('code', '=', 'outgoing'),('warehouse_id','=',sale_warehouse_id)]")

    @api.onchange('sale_warehouse_id')
    def _onchange_sale_warehouse_id(self):
        domain = {}
        _warehouses = []
        if self.sale_warehouse_id:
            sale_picking_type_id = self.env['stock.picking.type'].search([('warehouse_id', '=', self.sale_warehouse_id.id), ('code', '=', 'outgoing')],
                                                                       limit=1)
            if sale_picking_type_id:
                self.sale_picking_type_id = sale_picking_type_id.id

        for warehouse in self.env.user.allowed_warehouses:
            _warehouses.append(warehouse.id)
        if _warehouses:
            domain['sale_warehouse_id'] = [('id', 'in', _warehouses)]
        return {'domain': domain}

    @api.multi
    def _sale_count(self):
        '''Гэрээний тоо
        '''
        for contract in self:
            contract.sale_count = len(self.env['sale.order'].sudo().search([('contract_id', '=', contract.id)])) or 0

    sale_count = fields.Integer(compute=_sale_count, string='Sale Count')


class ContractTerm(models.Model):
    _inherit = 'contract.term'

    @api.multi
    def _compute_invoice_type(self):
        for obj in self:
            if obj.contract_id.participation.invoice_type == 'purchase':
                obj.type = 'purchase'
            elif obj.contract_id.participation.invoice_type == 'sale':
                obj.type = 'sale'
            elif obj.contract_id.participation.invoice_type == 'in_invoice' or obj.contract_id.participation.invoice_type == 'out_invoice':
                obj.type = 'invoice'

    type = fields.Selection(selection_add=[('sale', 'Sale')], compute=_compute_invoice_type)
    sale_id = fields.Many2one('sale.order', string='Sale')
    state = fields.Selection(selection_add=[('sales', 'Sales')])

    @api.multi
    def _compute__sale_paid_total(self):
        for obj in self:
            if obj.sale_id:
                paid_total = 0
                for invoice_id in obj.sale_id.invoice_ids:
                    if invoice_id:
                        if obj.invoice_id.state == 'draft':
                            paid_total = 0.0
                        elif obj.invoice_id.state == 'paid':
                            paid_total = obj.sub_total
                        else:
                            paid_total = invoice_id.amount_total - invoice_id.residual
                    else:
                        paid_total = 0.0
                obj.paid_total = paid_total

    @api.multi
    def _prepare_sale_data(self, term_id):

        partner = term_id.contract_id.partner_id
        if not partner:
            raise UserError(_("You must first select a Customer for Contract %s!") % term_id.contract_id.name)

        partner_payment_term = partner.property_payment_term_id.id if partner.property_payment_term_id else False

        sale = {
            'partner_id': partner.id,
            'partner_invoice_id': partner.id,
            'partner_shipping_id': partner.id,
            'pricelist_id': partner.property_product_pricelist and partner.property_product_pricelist.id or False,
            'origin': term_id.contract_id.name,
            'payment_term_id': partner_payment_term,
            'company_id': term_id.contract_id.company_id.id or False,
            'user_id': term_id.contract_id.user_id.id or self._uid,
            'contract_id': term_id.contract_id.id,
            'date_order': term_id.end_date,
            'validity_date': term_id.end_date,
            'warehouse_id': term_id.contract_id.sale_warehouse_id.id,
            'stock_picking_type': term_id.contract_id.sale_picking_type_id.id
        }
        return sale

    def _prepare_sale_line(self, term_id):
        values = {
            'name': term_id.product_id.display_name,
            'price_unit': term_id.amount or 0.0,
            'product_uom_qty': term_id.quantity,
            'product_uom': term_id.uom_id.id or False,
            'product_id': term_id.product_id.id or False,
            'discount': term_id.discount or 0.0
        }
        return values

    @api.multi
    def _prepare_sale_lines(self, term_id):
        sale_lines = []
        values = self._prepare_sale_line(term_id)
        sale_lines.append((0, 0, values))
        return sale_lines

    @api.multi
    def _prepare_sale(self, term_id):
        sale = self._prepare_sale_data(term_id)
        sale['order_line'] = self._prepare_sale_lines(term_id)
        return sale

    @api.multi
    def _create_invoice(self, term_ids):
        ids = []
        if term_ids:
            for term_id in term_ids:
                if term_id.type == 'invoice':
                    invoice_values = self._prepare_invoice(term_id)
                    invoice = self.env['account.invoice'].create(invoice_values)
                    invoice.action_invoice_open()
                    ids.append(invoice)
                    term_id.invoice_id = invoice.id
                    term_id.state = 'invoiced'
                elif term_id.type == 'sale':
                    sale_values = self._prepare_sale(term_id)
                    sale = self.env['sale.order'].create(sale_values)
                    ids.append(sale)
                    term_id.sale_id = sale.id
                    term_id.state = 'sales'
                elif term_id.type == 'purchase':
                    purchase_values = self._prepare_purchase(term_id)
                    purchase = self.env['purchase.order'].create(purchase_values)
                    ids.append(purchase)
                    term_id.purchase_id = purchase.id
                    term_id.state = 'purchased'
        return ids


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    contract_id = fields.Many2one('contract.management', 'Contract', readonly=True)

class ContractParticipation(models.Model):
    _inherit = 'contract.participation'

    invoice_type = fields.Selection(selection_add=[('sale', 'Sale')])