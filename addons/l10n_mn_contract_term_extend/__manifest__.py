# -*- coding: utf-8 -*-

{
    "name": "Гэрээний менежмент",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "description": """
        Гэрээний менежмент зориулан хийсэн модуль
""",
    'website': "http://www.asterisk-tech.mn",
    "category": "Mongolian Modules",
    "depends": ['l10n_mn_contract'],
    "init": [],
    "data": [
        'views/contract_term_view.xml',
    ],
    "demo_xml": [
    ],
}
