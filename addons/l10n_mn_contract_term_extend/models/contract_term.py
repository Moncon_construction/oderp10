# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class ContractTerm(models.Model):
    _inherit = 'contract.term'

    @api.multi
    @api.depends('discount')
    def _compute_subtotal(self):
        for obj in self:
            obj.sub_total = obj.quantity * obj.amount
            obj.sub_total = obj.sub_total * (1 - (obj.discount or 0.0) / 100.0)

    type = fields.Selection([('invoice', 'Invoice')], string='Type', default='invoice')
    uom_id = fields.Many2one('product.uom', related='product_id.uom_id', string='Product Uom')
    quantity = fields.Integer('Quantity')
    sub_total = fields.Float(compute=_compute_subtotal, string='Sub Total')

    @api.multi
    def _compute_paid_total(self):
        super(ContractTerm, self)._compute_paid_total()
        self._compute__purchase_paid_total()
        self._compute__sale_paid_total()

    paid_total = fields.Float(compute=_compute_paid_total, string='Paid Total')

