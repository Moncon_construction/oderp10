# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
{
    "name": "Mongolian Cost Control",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "description": """
        Өртгийн хяналт.
""",
    'website': "http://www.asterisk-tech.mn",
    "category": "Mongolian Modules",
    "depends": [
        'l10n_mn_hr_payroll',
        'l10n_mn_task',
        'l10n_mn_workflow_config',
        'l10n_mn_technic',
        'l10n_mn_stock',
    ],
    "init": [],
    "data": [
        'security/cost_control_security.xml',
        'security/ir.model.access.csv',
        'views/cost_worksheet_view.xml',
        'views/cost_worksheet_product_view.xml',
        'views/cost_worksheet_salary_view.xml',
        'views/cost_worksheet_technic_view.xml',
        'views/overhead_expenses_norm_veiw.xml',
        'views/config_settings.xml',
        'views/hr_employee.xml',
        'views/menu_view.xml',
        'views/technic_view.xml',
        'views/technic_norm_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
