# -*- encoding: utf-8 -*-

from odoo import fields, models

class AccountMove(models.Model):
    _inherit = "account.move"

    overhead_expenses = fields.Many2one('cost.worksheet.overhead.expenses', string='Overhead Expenses')