# -*- coding: utf-8 -*-
from odoo import models, fields


class WorksheetConfigSettings(models.TransientModel):
    _name = 'worksheet.config.settings'
    _inherit = 'res.config.settings'

    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    direct_labor_account = fields.Many2one('account.account', string="Direct Labor Account", domain=[('user_type_id.type', '=', 'other')], related='company_id.direct_labor_account')
    direct_valuation_account = fields.Many2one('account.account', string="Direct Valuation Account", domain=[('user_type_id.type', '=', 'other')], related='company_id.direct_valuation_account')
    overhead_expense_account = fields.Many2one('account.account', string="Overhead Expense Account", domain=[('user_type_id.type', '=', 'other')], related='company_id.overhead_expense_account')
    percent = fields.Float(string="Percentage", related='company_id.percent', digits=(16, 1))
    overhead_journal_id = fields.Many2one(related='company_id.overhead_journal_id', string='Overhead Expense Journal')
    direct_labor_expense_account_id = fields.Many2one('account.account', 'Direct labor expense account', domain=[('user_type_id.type', '=', 'other')], related='company_id.direct_labor_expense_account_id')
    direct_salary_journal_id = fields.Many2one(related='company_id.direct_salary_journal_id', string='Direct Salary Journal')
    camp_salary_category_line_ids = fields.Many2many('camp.salary.config.line', related='company_id.camp_salary_category_line_ids', string='Lines')
    rental_expense_account_id = fields.Many2one('account.account', string="Rental technic expense account", domain=[('user_type_id.type', '=', 'other')], related='company_id.rental_expense_account_id')
    cost_entry_create_time = fields.Selection([
        ('by_day', "Create a journal entry by day"),
        ('by_package', "Create a journal entry by package")
    ], "Cost entry create time", default='by_day',
        related="company_id.cost_entry_create_time")
    package_journal_id = fields.Many2one(related='company_id.package_journal_id', string='Package Account Move Journal')

