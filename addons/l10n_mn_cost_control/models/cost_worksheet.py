# -*- coding: utf-8 -*-
import time
from calendar import monthrange
from datetime import datetime, timedelta

from odoo import _, api, exceptions, fields, models
from odoo.exceptions import UserError
from lxml import etree

STATE_SELECTION = [('draft', 'Draft'),
                   ('waiting_approve', 'Waiting Approve'),
                   ('approved', 'Approved'),
                   ('account_move_created', 'Account move created'),
                   ('rejected', 'rejected'),
                   ('cancel', 'Cancel')]


class CostWorksheet(models.Model):
    _name = 'cost.worksheet'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = 'Cost Worksheet'

    name = fields.Char('Name', required=True, track_visibility='onchange', states={'approved,account_move_created': [('readonly', True)]})
    start_date = fields.Date('Start Date', required=True, default=lambda *a: time.strftime('%Y-%m-%d'),
                             track_visibility='onchange', states={'approved,account_move_created': [('readonly', True)]})
    end_date = fields.Date('End Date', required=True, default=lambda *a: time.strftime('%Y-%m-%d'),
                           track_visibility='onchange', states={'approved,account_move_created': [('readonly', True)]})
    company_id = fields.Many2one('res.company', string='Company', required=True,
                                 default=lambda self: self.env.user.company_id)
    user_id = fields.Many2one('res.users', string='Responsible', default=lambda self: self.env.user, readonly=True)
    product_line_ids = fields.One2many('cost.worksheet.product', 'cost_id', string='Products', states={'approved,account_move_created': [('readonly', True)]}, copy=False)
    all_salary_line_ids = fields.One2many('cost.worksheet.employee', 'cost_id', string='Labors', states={'approved,account_move_created': [('readonly', True)]}, copy=False)
    salary_line_ids = fields.One2many('cost.worksheet.camp.salary', 'salary_cost_id', string='Labors', states={'approved,account_move_created': [('readonly', True)]}, copy=False)
    technic_line_ids = fields.One2many('cost.worksheet.technic', 'cost_id', string='Technic', states={'approved,account_move_created': [('readonly', True)]}, copy=False)
    rental_technic_line_ids = fields.One2many('cost.worksheet.rental.summary', 'cost_id', string='Rental Technic', states={'approved,account_move_created': [('readonly', True)]}, copy=False)
    overhead_expenses_line_ids = fields.One2many('cost.worksheet.overhead.expenses', 'cost_id', string='Overhead Expenses',
                                                 states={'approved,account_move_created': [('readonly', True)]}, copy=False)
    technic_motohour_ids = fields.One2many('cost.worksheet.technic.motohour', 'cost_id', string='Technic motohour', states={'approved,account_move_created': [('readonly', True)]}, copy=False)
    history_line_ids = fields.One2many('cost.worksheet.workflow.history', 'cost_worksheet_id', 'Workflow History', copy=False)
    workflow_id = fields.Many2one('workflow.config', 'Workflow', copy=False)
    check_sequence = fields.Integer('Workflow Step', default=0, copy=False)
    is_validator = fields.Boolean(compute='_compute_is_validator')
    is_creator = fields.Boolean(compute='_compute_is_creator')
    state = fields.Selection(STATE_SELECTION, 'State', default='draft')
    move_id = fields.Many2one('account.move', 'Account move')
    is_account_move_create_by_day = fields.Boolean(compute='_compute_is_acc_move_create_by_day')

    @api.model
    def create(self, vals):
        creation = super(CostWorksheet, self).create(vals)
        employee = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)])
        workflow_id = self.env['workflow.config'].get_workflow('employee', 'cost.worksheet', employee.id, None)
        if not workflow_id:
            raise exceptions.Warning(_('There is no workflow defined!'))
        creation.workflow_id = workflow_id
        return creation

    @api.multi
    def send(self):
        # Ажлын хуудас илгээх, Ажлын урсгалд тохируулсан ажилчдад илгээгдэнэ.
        if self.workflow_id:
            success, current_sequence = self.env['workflow.config'].send('cost.worksheet.workflow.history', 'cost_worksheet_id', self, self.create_uid.id)
            if success:
                self.check_sequence = current_sequence
                self.state = 'waiting_approve'

    description = fields.Text('Description')

    @api.depends('check_sequence')
    def _compute_is_validator(self):
        for rec in self:
            history_obj = self.env['cost.worksheet.workflow.history']
            validators = history_obj.search([('cost_worksheet_id', '=', rec.id), ('line_sequence', '=', rec.check_sequence)], limit=1, order='sent_date DESC').user_ids
            if self.env.user in validators:
                rec.is_validator = True
            else:
                rec.is_validator = False

    @api.multi
    def _compute_is_creator(self):
        for rec in self:
            if rec.create_uid == self.env.user:
                rec.is_creator = True
            else:
                rec.is_creator = False

    @api.multi
    def _compute_is_acc_move_create_by_day(self):
        for rec in self:
            if rec.company_id.cost_entry_create_time == 'by_day':
                rec.is_account_move_create_by_day = True
            else:
                rec.is_account_move_create_by_day = False

    @api.multi
    def unlink(self):
        for cost in self:
            if cost.state != 'draft':
                raise UserError(_('Delete only draft in state'))
        return super(CostWorksheet, self).unlink()

    @api.multi
    def _create_product_moves(self):
        self.ensure_one()
        for product in self.product_line_ids:
            if not self.company_id.direct_valuation_account:
                raise UserError(_('You must set direct valuation account on the settings of cost control!'))
            if not product.warehouse_id.stock_journal:
                raise UserError(_('You must set journal on the warehouse %s!') % product.warehouse_id.name)
            debit = []
            total_perf_qty = 0
            for line in product.line_ids:
                total_perf_qty += line.perf_production_qty
            self._cr.execute("SELECT id FROM ir_module_module WHERE name = 'l10n_mn_stock_account_cost_for_each_wh' AND state IN ('installed', 'to upgrade')")
            results = self._cr.dictfetchall()
            account_id = False
            if results and len(results) > 0:
                if not product.warehouse_id.stock_account_expense_id:
                    raise UserError(_('You must set expense account on the warehouse %s!') % product.warehouse_id.name)
                account_id = product.warehouse_id.stock_account_expense_id.id
            else:
                categ_id = product.product_id.categ_id
                while categ_id and not account_id:
                    if categ_id.property_account_expense_categ_id:
                        account_id = categ_id.property_account_expense_categ_id.id
                    else:
                        categ_id = categ_id.parent_id
                if not account_id:
                    raise UserError(_('You must choose expense account on the cateogry of the product %s') % product.product_id.name)
            credit = self._prepare_move_line(account_id, 'product', self.end_date, product.warehouse_id.stock_journal.id,
                                             False, 0, 0, False, True)

            for line in product.line_ids:
                if not line.analytic_account_id:
                    raise UserError(_('You must set analytic account on the task type %s!') % line.task_type_id.name)
                debit.append(self._prepare_move_line(self.company_id.direct_valuation_account.id, 'product', self.end_date,
                                                product.warehouse_id.stock_journal.id, line.analytic_account_id.id,
                                                product.total_expense / total_perf_qty * line.perf_production_qty if total_perf_qty else 0, 0, False, True))
                credit[2]['credit'] += product.total_expense / total_perf_qty * line.perf_production_qty if total_perf_qty else 0
            if debit:
                debit.append(credit)
                product.move_id = self.env['account.move'].create({
                    'journal_id': product.warehouse_id.stock_journal.id,
                    'date': self.end_date,
                    'line_ids': debit,
                })

    @api.multi
    def _create_overhead_moves(self):
        self.ensure_one()
        if not self.company_id.overhead_expense_account:
            raise UserError(_('You must set overhead expense account on the settings of cost control!'))
        if not self.company_id.overhead_journal_id:
            raise UserError(_('You must set overhead journal on the settings of cost control!'))
        for overhead in self.overhead_expenses_line_ids:
            if not overhead.account_id:
                raise UserError(_('You must set account on the overhead %s!') % overhead.name)
            conf_account_ids = []
            for conf in self.company_id.camp_salary_category_line_ids:
                conf_account_ids.append(conf.account_id.id)
            if overhead.account_id not in conf_account_ids:
                debit = []
                credit = self._prepare_move_line(overhead.account_id.id, overhead.name, self.end_date,
                                                 False, False, 0, 0, False, False)
                for line in overhead.overhead_expenses_analytic_ids:
                    if not line.account_analytic_id:
                        raise UserError(_('You must set analytic account on the task type %s!') % line.task_type_id.name)
                    debit.append(self._prepare_move_line(self.company_id.overhead_expense_account.id, overhead.name, self.end_date,
                                                    False, line.account_analytic_id.id, line.assign_amount, 0, False, False))
                    credit[2]['credit'] += line.assign_amount
                if debit:
                    debit.append(credit)
                    overhead.move_id = self.env['account.move'].create({
                        'journal_id': self.company_id.overhead_journal_id.id,
                        'date': self.end_date,
                        'line_ids': debit,
                    })

    @api.multi
    def _create_technic_moves(self):
        self.ensure_one()
        if not self.company_id.rental_expense_account_id:
            raise UserError(_('You must set rental technic expense account on the settings of cost control!'))
        if not self.company_id.overhead_expense_account:
            raise UserError(_('You must set overhead expense account on the settings of cost control!'))
        if not self.company_id.overhead_journal_id:
            raise UserError(_('You must set overhead journal on the settings of cost control!'))
        for technic in self.rental_technic_line_ids:
            credit = self._prepare_move_line(self.company_id.rental_expense_account_id.id,
                                             'rental technic', self.end_date,
                                             False, False, 0, 0, False, False)
            debit = []
            for line in technic.line_ids:
                if not line.analytic_account_id:
                    raise UserError(_('You must set analytic account on the task type %s!') % line.task_type_id.name)
                debit_val = self._prepare_move_line(self.company_id.overhead_expense_account.id,
                                                     'rental technic', self.end_date,
                                                     False, line.analytic_account_id.id, line.sub_total, 0, False, False)
                debit.append(debit_val)
                credit[2]['credit'] += line.sub_total
            if debit:
                debit.append(credit)
                technic.move_id = self.env['account.move'].create({
                    'journal_id': self.company_id.overhead_journal_id.id,
                    'date': self.end_date,
                    'line_ids': debit,
                })

    @api.multi
    def approve(self):
        # Ажлын хуудас батлах
        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve('cost.worksheet.workflow.history', 'cost_worksheet_id', self, self.env.user.id)
            if success:
                if sub_success:
                    account_move_obj = self.env['account.move']
                    if self.env.user.company_id.cost_entry_create_time == 'by_day':
                        for obj in self:
                            obj._create_product_moves()
                            obj._create_technic_moves()
                            for l in obj.all_salary_line_ids:
                                credit = obj._prepare_move_line(obj.company_id.direct_labor_expense_account_id.id,'work',obj.end_date,
                                                                obj.company_id.direct_salary_journal_id.id,False,0,0, False, True)
                                debit = []
                                for sal in l.line_ids:
                                    debit.append(obj._prepare_move_line(obj.company_id.direct_labor_account.id,'work',obj.end_date,
                                                                   obj.company_id.direct_salary_journal_id.id,sal.analytic_account_id.id,sal.perf_salary or 0,0,False, True))
                                    credit[2]['credit'] += sal.perf_salary
                                if debit:
                                    debit.append(credit)
                                    l.move_id = account_move_obj.create({
                                        'journal_id': obj.company_id.direct_salary_journal_id and obj.company_id.direct_salary_journal_id.id,
                                        'date': obj.end_date,
                                        'line_ids': debit,
                                    })
                            for l in obj.salary_line_ids:
                                if l.employee_id and l.employee_id.camp_salary_category_id:
                                    for conf in obj.company_id.camp_salary_category_line_ids:
                                        if conf.category_id.id == l.employee_id.camp_salary_category_id.id and conf.account_id:
                                            debit = obj._prepare_move_line(conf.account_id.id,'camp',obj.end_date,
                                                                           False,False,l.perf_salary,0,False, False)
                                            credit = obj._prepare_move_line(obj.company_id.overhead_expense_account.id,'camp',obj.end_date,
                                                                            False,False,0,l.perf_salary,False, False)
                                            l.move_id = account_move_obj.create({
                                                'journal_id': obj.company_id.overhead_journal_id and obj.company_id.overhead_journal_id.id,
                                                'date': obj.end_date,
                                                'line_ids': [debit, credit]
                                            })
                            obj._update_overhead_expense()
                            obj._create_overhead_moves()
                        self.state = 'account_move_created'
                    else:
                        self.state = 'approved'
                else:
                    self.check_sequence = current_sequence

    @api.multi
    def _prepare_move_line(self, account_id, name, date, journal_id, analytic_account_id, debit, credit, move_id, is_journal):
        line_vals = {
            'account_id': account_id,
            'name': name,
            'debit': debit,
            'credit': credit,
            'analytic_account_id': analytic_account_id,
            'amount_currency': 0,
            'currency_id': False,
            'date': date,
            'move_id': move_id,
        }
        if is_journal:
            line_vals.update({'journal_id': journal_id})
        res = (0, 0, line_vals)
        return res

    @api.multi
    def action_create_account_move(self):
        not_approved_sheets = []
        dates = []
        account_move_obj = self.env['account.move']
        account_move_line_obj = self.env['account.move.line']
        for sheet in self:
            if not sheet.company_id.package_journal_id:
                raise UserError(_('You must set package journal on the settings of cost control!'))
            if not sheet.company_id.direct_valuation_account:
                raise UserError(_('You must set direct valuation account on the settings of cost control!'))
            if not sheet.company_id.overhead_expense_account:
                raise UserError(_('You must set overhead expense account on the settings of cost control!'))
            if not sheet.company_id.overhead_journal_id:
                raise UserError(_('You must set overhead journal on the settings of cost control!'))
            if not sheet.company_id.rental_expense_account_id:
                raise UserError(_('You must set rental technic expense account on the settings of cost control!'))

            if sheet.state != 'approved':
                not_approved_sheets.append(sheet.id)
            else:
                if sheet.end_date not in dates:
                    dates.append(sheet.end_date)
        if len(not_approved_sheets) > 0:
            raise UserError(_('Only approved state worksheets are allowed!!!'))
        date = max(dates)
        for sheet in self:
            move = account_move_obj.create({
                'journal_id': sheet.company_id.package_journal_id and sheet.company_id.package_journal_id.id,
                'date': date,
            })
            sheet.move_id = move
            for l in sheet.all_salary_line_ids:
                credit = sheet._prepare_move_line(sheet.company_id.direct_labor_expense_account_id.id, 'work', date,
                                                  sheet.company_id.direct_salary_journal_id.id, False, 0, 0, move.id, True)

                debit = []
                for sal in l.line_ids:
                    debit.append(sheet._prepare_move_line(sheet.company_id.direct_labor_account.id,'work',date,
                                                     sheet.company_id.direct_salary_journal_id.id,sal.analytic_account_id.id,
                                                     sal.perf_salary,0,move.id, True))
                    credit[2]['credit'] += sal.perf_salary
                if debit:
                    debit.append(credit)
                    for d in debit:
                        account_move_line_obj.create(d[2])

            for l in sheet.salary_line_ids:
                if l.employee_id and l.employee_id.camp_salary_category_id:
                    for conf in sheet.company_id.camp_salary_category_line_ids:
                        if conf.category_id.id == l.employee_id.camp_salary_category_id.id and conf.account_id:
                            debit = sheet._prepare_move_line(conf.account_id.id, 'camp',date,
                                                             False,False,l.perf_salary,0,move.id, False)
                            credit = sheet._prepare_move_line(sheet.company_id.overhead_expense_account.id,'camp',date,
                                                              False,False,0,l.perf_salary,move.id, False)
                            if debit and credit:
                                account_move_line_obj.create(debit[2])
                                account_move_line_obj.create(credit[2])

            for product in sheet.product_line_ids:
                if not product.warehouse_id.stock_journal:
                    raise UserError(_('You must set journal on the warehouse %s!') % product.warehouse_id.name)
                debit = []
                total_perf_qty = 0
                for line in product.line_ids:
                    total_perf_qty += line.perf_production_qty
                self._cr.execute(
                    "SELECT id FROM ir_module_module WHERE name = 'l10n_mn_stock_account_cost_for_each_wh' AND state IN ('installed', 'to upgrade')")
                results = self._cr.dictfetchall()
                account_id = False
                if results and len(results) > 0:
                    if not product.warehouse_id.stock_account_expense_id:
                        raise UserError(_('You must set expense account on the warehouse %s!') % product.warehouse_id.name)
                    account_id = product.warehouse_id.stock_account_expense_id.id
                else:
                    categ_id = product.product_id.categ_id
                    while categ_id and not account_id:
                        if categ_id.property_account_expense_categ_id:
                            account_id = categ_id.property_account_expense_categ_id.id
                        else:
                            categ_id = categ_id.parent_id
                    if not account_id:
                        raise UserError(_(
                            'You must choose expense account on the cateogry of the product %s') % product.product_id.name)

                credit = sheet._prepare_move_line(account_id,'product',date,product.warehouse_id.stock_journal.id,False,
                                                  0,0,move.id, True)

                for line in product.line_ids:
                    if not line.analytic_account_id:
                        raise UserError(
                            _('You must set analytic account on the task type %s!') % line.task_type_id.name)
                    debit.append(sheet._prepare_move_line(sheet.company_id.direct_valuation_account.id,'product',date,
                                                     product.warehouse_id.stock_journal.id,line.analytic_account_id.id,
                                                     product.total_expense / total_perf_qty * line.perf_production_qty if total_perf_qty else 0,0,move.id,True))
                    credit[2]['credit'] += product.total_expense / total_perf_qty * line.perf_production_qty if total_perf_qty else 0
                if debit:
                    debit.append(credit)
                    for d in debit:
                        account_move_line_obj.create(d[2])

            for technic in sheet.rental_technic_line_ids:
                credit = sheet._prepare_move_line(sheet.company_id.rental_expense_account_id.id, 'rental technic',date,
                                                  False,False,0,0,move.id, False)
                debit = []
                for line in technic.line_ids:
                    if not line.analytic_account_id:
                        raise UserError(_('You must set analytic account on the task type %s!') % line.task_type_id.name)
                    debit.append(sheet._prepare_move_line(sheet.company_id.overhead_expense_account.id, 'rental technic', date,
                                                     False,line.analytic_account_id.id,line.sub_total,0,move.id, False))
                    credit[2]['credit'] += line.sub_total
                if debit:
                    debit.append(credit)
                    for d in debit:
                        account_move_line_obj.create(d[2])

            sheet.overhead_expenses_line_ids.unlink()
            object_oe = self.env['cost.worksheet.overhead.expenses']
            norm_ids = self.env['overhead.expenses.norm'].search([('start_date', '<=', date), ('end_date', '>=', date), ('state', '=', 'approved')])
            for norm in norm_ids:
                for line in norm.line_ids:
                    standard_cost = 0.00
                    if line.compute_account_check:
                        balance_ids = self.env['account.move.line'].get_balance(sheet.company_id.id, line.account_id.ids,
                                                                                sheet.start_date, sheet.end_date,
                                                                                target_move='posted')
                        for balance_id in balance_ids:
                            standard_cost += balance_id['debit'] - balance_id['credit']
                    else:
                        standard_cost = line.standard_cost
                    line_vals = []
                    if line.assign_type == 'direct':
                        line_vals = [(0, 0, {
                            'account_analytic_id': line.analytic_account_id.id,
                            'assign_amount': line.standard_cost,
                        })]
                    vals = {'cost_id': sheet.id,
                            'state': sheet.state,
                            'name': line.name,
                            'account_id': line.account_id.id if line.account_id else False,
                            'compute_account_check': line.compute_account_check,
                            'standard_cost_day': standard_cost if standard_cost else 0.0,
                            'assign_type': line.assign_type,
                            'overhead_expenses_analytic_ids': line_vals}
                    object_oe.create(vals)

            for overhead in sheet.overhead_expenses_line_ids:
                if not overhead.account_id:
                    raise UserError(_('You must set account on the overhead %s!') % overhead.name)
                conf_account_ids = []
                for conf in sheet.company_id.camp_salary_category_line_ids:
                    conf_account_ids.append(conf.account_id.id)
                if overhead.account_id not in conf_account_ids:
                    debit = []
                    credit = sheet._prepare_move_line(overhead.account_id.id,overhead.name,date,
                                                      False,False,0,0,move.id, False)
                    for line in overhead.overhead_expenses_analytic_ids:
                        if not line.account_analytic_id:
                            raise UserError(
                                _('You must set analytic account on the task type %s!') % line.task_type_id.name)
                        debit.append(sheet._prepare_move_line(sheet.company_id.overhead_expense_account.id,overhead.name,date,
                                                         False,line.account_analytic_id.id,line.assign_amount,0,move.id, False))
                        credit[2]['credit'] += line.assign_amount
                    if debit:
                        debit.append(credit)
                        for d in debit:
                            account_move_line_obj.create(d[2])
            sheet.move_id.journal_id = sheet.company_id.package_journal_id
            sheet.state = 'account_move_created'

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(CostWorksheet, self).fields_view_get(view_id, view_type, toolbar=toolbar, submenu=False)
        if self.env.user.company_id.cost_entry_create_time == 'by_day':
            if res.get('toolbar'):
                for action in res.get('toolbar').get('action'):
                    if action.get('xml_id'):
                        if action['xml_id'] == 'l10n_mn_cost_control.action_create_account_move_cc':
                            res['toolbar']['action'].remove(action)
        return res


    @api.multi
    def refuse(self):
        # Ажлын хуудас татгалзах
        if self.workflow_id:
            success = self.env['workflow.config'].reject('cost.worksheet.workflow.history', 'cost_worksheet_id', self, self.env.user.id)
            if success:
                self.state = 'rejected'

    @api.multi
    def previous(self):
        # Ажлын хуудаснаас буцаах
        if self.workflow_id:
            success, current_sequence = self.env['workflow.config'].action_return('cost.worksheet.workflow.history', 'cost_worksheet_id', self, self.env.user.id)
            if success:
                self.check_sequence = current_sequence

    @api.multi
    def _unlink_account_move(self):
        AccountMoveLine = self.env['account.move.line']
        for record in self:
            for product in record.product_line_ids:
                product.move_id.button_cancel()
                product.move_id.unlink()
            for overhead in record.overhead_expenses_line_ids:
                amls = AccountMoveLine.search([('account_id', '=', overhead.account_id.id), ('date', '>=', record.start_date), ('date', '<=', record.end_date)])
                if amls:
                    ams = amls.mapped('move_id')
                    for m in ams:
                        m.button_cancel()
                        m.unlink()
            for technic in record.rental_technic_line_ids:
                technic.move_id.button_cancel()
                technic.move_id.unlink()

    @api.multi
    def cancel(self):
        # Ажлын хуудсыг цуцлах
        if self.env.user.company_id.cost_entry_create_time == 'by_day':
            self._unlink_account_move()
        else:
            self.move_id.unlink()
        self.state = 'cancel'

    @api.multi
    def draft(self):
        # Ажлын хуудсыг ноороглох
        self.ensure_one()
        if self.env.user.company_id.cost_entry_create_time == 'by_day':
            self._unlink_account_move()
        else:
            self.move_id.unlink()
        self.check_sequence = 0
        self.state = 'draft'

    @api.multi
    def _update_overhead_expense(self):
        self.ensure_one()
        self.overhead_expenses_line_ids.unlink()
        object_oe = self.env['cost.worksheet.overhead.expenses']
        norm_ids = self.env['overhead.expenses.norm'].search([('start_date', '<=', self.end_date), ('end_date', '>=', self.end_date), ('state', '=', 'approved')])
        for norm in norm_ids:
            for line in norm.line_ids:
                standard_cost = 0.00
                if line.compute_account_check:
                    balance_ids = self.env['account.move.line'].get_balance(self.company_id.id, line.account_id.ids, self.start_date, self.end_date, target_move='posted')
                    for balance_id in balance_ids:
                        standard_cost += balance_id['debit'] - balance_id['credit']
                else:
                    standard_cost = line.standard_cost
                line_vals = []
                if line.assign_type == 'direct':
                    line_vals = [(0, 0, {
                        'account_analytic_id': line.analytic_account_id.id,
                        'assign_amount': line.standard_cost,
                    })]
                vals = {'cost_id': self.id,
                        'state': self.state,
                        'name': line.name,
                        'account_id': line.account_id.id if line.account_id else False,
                        'compute_account_check': line.compute_account_check,
                        'standard_cost_day': standard_cost if standard_cost else 0.0,
                        'assign_type': line.assign_type,
                        'overhead_expenses_analytic_ids': line_vals}
                object_oe.create(vals)

    @api.multi
    def cost_compute(self):
        # ҮНЗ нормын автомат үүсгэх хэсэг
        for obj in self:
            obj.technic_line_ids.unlink()
            obj.rental_technic_line_ids.unlink()
            obj.all_salary_line_ids.unlink()
            obj.product_line_ids.unlink()
            obj.technic_motohour_ids.unlink()
            obj._update_overhead_expense()
            # Ажлын хуудасны бараа табны нэгтгэл
            task_ids = self.env['task.task'].search([('date_deadline', '>=', obj.start_date), ('date_deadline', '<=', obj.end_date)])
            cost_worksheet_product_id = self.env['cost.worksheet.product']
            cost_worksheet_product_line = self.env['cost.worksheet.product.line']
            query = '''
            SELECT
                    t.warehouse_id as warehouse_id, 
                    ttt.task_type_id as task_type_id, 
                    tpnl.product_id as product_id, 
                    tt.is_auto as is_auto, 
                    sum(ttt.perf_hours) as perf_qty, 
                    sum(ttt.perf_km) as perf_km, 
                    sum(tpnl.production_qty) as production_qty, 
                    sum(tpnl.product_qty) as product_qty, 
                    tpnl.product_uom as product_uom,
                    tpnl.production_uom as production_uom
                FROM task_task_technic ttt
                LEFT JOIN technic t 
                    ON ttt.technic_id = t.id
                LEFT JOIN  task_product_norm_line tpnl
                    ON tpnl.task_type_id = ttt.task_type_id and ttt.technic_id = tpnl.technic_id
                LEFT JOIN technic_type tt
                    ON tt.id = t.technic_type_id
                LEFT JOIN  task_product_norm tpn
                    ON tpn.id = tpnl.product_norm_id
                WHERE  tpnl.product_id is not null AND tpn.state = 'approved'
                        AND ttt.date BETWEEN tpn.start_date AND tpn.end_date AND ttt.date BETWEEN \'''' + obj.start_date + '''\' AND \'''' + obj.end_date + '''\' 
                        group by t.warehouse_id, tt.is_auto, tpnl.product_id, ttt.task_type_id, tpnl.production_uom, tpnl.product_uom ORDER BY t.warehouse_id, tpnl.product_id              
            '''

            self._cr.execute(query)
            fetched = self._cr.dictfetchall()
            warehouse_id = False
            product_id = False
            for value in fetched:
                if warehouse_id != value['warehouse_id'] or value['product_id'] != product_id:
                    warehouse_id = value['warehouse_id']
                    product_id = value['product_id']
                    # if not value['norm_id']:
                    #     raise UserError(_('%s Not product norm!!!'))
                    estimate_product_qty = value['product_qty'] / value['production_qty'] * (value['perf_qty'] if not value['is_auto'] else value['perf_km']) if value[
                        'production_qty'] else 0
                    line_vals = [(0, 0, {
                        'task_type_id': value['task_type_id'],
                        'norm_production_qty': value['production_qty'],
                        'perf_production_qty': value['perf_qty'] if not value['is_auto'] else value['perf_km'],
                        'norm_production_uom': value['production_uom'],  # Бүтээлийн хэмжих нэгж
                        'norm_product_qty': value['product_qty'],
                        'estimate_product_qty': estimate_product_qty
                    })]
                    vals = {
                        'cost_id': obj.id,
                        'warehouse_id': value['warehouse_id'],
                        'product_id': value['product_id'],
                        'product_uom': value['product_uom'],
                        'line_ids': line_vals
                    }
                    worksheet_product_id = cost_worksheet_product_id.create(vals)
                else:
                    estimate_product_qty = value['product_qty'] / value['production_qty'] * value['perf_qty'] if value[
                        'production_qty'] else 0
                    line_vals = {
                        'cost_product_id': worksheet_product_id.id,
                        'task_type_id': value['task_type_id'],
                        'norm_production_qty': value['production_qty'],
                        'perf_production_qty': value['perf_qty'] if not value['is_auto'] else value['perf_km'],
                        'norm_production_uom': value['production_uom'],  # Бүтээлийн хэмжих нэгж
                        'norm_product_qty': value['product_qty'],
                        'estimate_product_qty': estimate_product_qty,
                    }

                    cost_worksheet_product_line.create(line_vals)
                    warehouse_id = value['warehouse_id']

            # Ажлын хуудасны техник табны нэгтгэл
            technic_line_object = self.env['cost.worksheet.technic']
            rental_technic_summary_object = self.env['cost.worksheet.rental.summary']
            rental_technic_line_object = self.env['cost.worksheet.rental.technic']
            total_salary_dict = {}
            total_rental_technic_dict = {}
            technic_motohour_dict = {}
            mining_daily_entry_ids = self.env['mining.daily.entry'].search(
                [('date', '>=', obj.start_date), ('date', '<=', obj.end_date), ('state', '=', 'approved')])
            for task in task_ids:
                # employee = self.env['hr.employee'].search([('user_id', '=', task.user_id.id)], limit=1)
                for technic_line in task.technic_line_ids:
                    norm_uom_id = self.env['production.uom.line'].search(
                        [('technic_norm_id', '=', technic_line.technic_id.technic_norm_id.id),
                         ('task_type_id', '=', technic_line.task_type_id.id)], limit=1)
                    if not norm_uom_id:
                        raise UserError(
                            _('%s Not technic norm!!!') % (technic_line.technic_id.name))
                    if technic_line.technic_id.ownership_type in ('own', 'leasing'):
                        technic_line_vals = {
                            'cost_id': obj.id,
                            'technic_id': technic_line.technic_id.id,
                            'employee_id': technic_line.employee_id.id,
                            'task_type_id': task.task_type_id.id,
                            'norm_qty': norm_uom_id.production_norm,
                            'perf_qty': technic_line.perf_qty,
                            'norm_uom': technic_line.norm_uom_id.id,
                            'perf_hours': technic_line.perf_hours,
                            'plan_hours': norm_uom_id.norm_hour / norm_uom_id.production_norm * technic_line.perf_qty if norm_uom_id.production_norm * technic_line.perf_qty else 0,
                            'total_freight_tn_km': technic_line.total_freight_tn_km
                        }
                        technic_line_object.create(technic_line_vals)
                    if technic_line.technic_id.ownership_type in ('partner', 'rental'):
                        if technic_line.technic_id.id not in total_rental_technic_dict:
                            vals = {
                                'cost_id': obj.id,
                                'technic_id': technic_line.technic_id.id,
                                'employee_id': technic_line.employee_id.id if technic_line.employee_id else False,
                                'task_type_id': task.task_type_id.id,
                                'norm_qty': norm_uom_id.production_norm,
                                'perf_qty': technic_line.perf_qty,
                                'norm_uom': technic_line.norm_uom_id.id,
                                'perf_hours': technic_line.perf_hours,
                                'plan_hours': norm_uom_id.norm_hour / norm_uom_id.production_norm * technic_line.perf_qty if norm_uom_id.production_norm * technic_line.perf_qty else 0,
                                'price_unit': technic_line.technic_id.product_id.lst_price if technic_line.technic_id.product_id else 0.0,
                                'total_freight_tn_km': technic_line.total_freight_tn_km
                            }
                            line_id = rental_technic_summary_object.create(vals)
                            total_rental_technic_dict[technic_line.technic_id.id] = line_id
                            technic_line_vals = {
                                'summary_id': line_id.id,
                                'technic_id': technic_line.technic_id.id,
                                'employee_id': technic_line.employee_id.id if technic_line.employee_id else False,
                                'task_type_id': task.task_type_id.id,
                                'norm_qty': norm_uom_id.production_norm,
                                'perf_qty': technic_line.perf_qty,
                                'norm_uom': technic_line.norm_uom_id.id,
                                'perf_hours': technic_line.perf_hours,
                                'plan_hours': norm_uom_id.norm_hour / norm_uom_id.production_norm * technic_line.perf_qty if norm_uom_id.production_norm * technic_line.perf_qty else 0,
                                'price_unit': technic_line.technic_id.product_id.lst_price if technic_line.technic_id.product_id else 0.0,
                                'total_freight_tn_km': technic_line.total_freight_tn_km
                            }
                            rental_technic_line_object.create(technic_line_vals)
                        else:
                            technic_line_vals = {
                                'summary_id': total_rental_technic_dict[technic_line.technic_id.id].id,
                                'technic_id': technic_line.technic_id.id,
                                'employee_id': technic_line.employee_id.id if technic_line.employee_id else False,
                                'task_type_id': task.task_type_id.id,
                                'norm_qty': norm_uom_id.production_norm,
                                'perf_qty': technic_line.perf_qty,
                                'norm_uom': technic_line.norm_uom_id.id,
                                'perf_hours': technic_line.perf_hours,
                                'plan_hours': norm_uom_id.norm_hour / norm_uom_id.production_norm * technic_line.perf_qty if norm_uom_id.production_norm * technic_line.perf_qty else 0,
                                'price_unit': technic_line.technic_id.product_id.lst_price if technic_line.technic_id.product_id else 0.0,
                                'total_freight_tn_km': technic_line.total_freight_tn_km
                            }
                            rental_technic_line_object.create(technic_line_vals)

                    # Ажлын хуудасны хөдөлмөр таб
                    if technic_line.employee_id:
                        salary_norm_id = self.env['task.salary.norm.line'].search(
                            [('task_type_id', '=', task.task_type_id.id), ('salary_norm_id.state', '=', 'approved'),
                             ('employee_id', '=', technic_line.employee_id.id),
                             ('salary_norm_id.start_date', '<=', task.date_deadline),
                             ('salary_norm_id.end_date', '>=', task.date_deadline)], limit=1)
                        if technic_line.employee_id not in total_salary_dict:
                            vals = {
                                'cost_id': obj.id,
                                'employee_id': technic_line.employee_id.id,

                            }
                            line_id = self.env['cost.worksheet.employee'].create(vals)
                            total_salary_dict[technic_line.employee_id] = line_id
                            cost_worksheet_salary_vals = {
                                'employee_line_id': line_id.id,
                                'date': technic_line.date,
                                'employee_id': technic_line.employee_id.id,
                                'technic_id': technic_line.technic_id.id,
                                'task_type_id': technic_line.task_type_id.id,
                                'norm_hours': salary_norm_id.plan_hours,
                                'perf_hours': technic_line.perf_hours,
                                'norm_qty': salary_norm_id.production_qty,
                                'perf_qty': technic_line.perf_qty,
                                'norm_salary': salary_norm_id.norm_salary,
                                'salary_calculate_type': salary_norm_id.salary_calculate_type,
                            }
                            self.env['cost.worksheet.salary'].create(cost_worksheet_salary_vals)
                        else:
                            cost_worksheet_salary_vals = {
                                'employee_line_id': total_salary_dict[technic_line.employee_id].id,
                                'date': technic_line.date,
                                'employee_id': technic_line.employee_id.id,
                                'technic_id': technic_line.technic_id.id,
                                'task_type_id': technic_line.task_type_id.id,
                                'norm_hours': salary_norm_id.plan_hours,
                                'perf_hours': technic_line.perf_hours,
                                'norm_qty': salary_norm_id.production_qty,
                                'perf_qty': technic_line.perf_qty,
                                'norm_salary': salary_norm_id.norm_salary,
                                'salary_calculate_type': salary_norm_id.salary_calculate_type,
                            }
                            self.env['cost.worksheet.salary'].create(cost_worksheet_salary_vals)
                    # Техникийн нийт цагийм мөрийг мот цагийн шалгаанаас үүсгэх.
                    if technic_line.technic_id.id not in technic_motohour_dict:
                        total_worked_hour = 0.0
                        production_time = 0.0
                        unproductive_time = 0.0
                        off_time = 0.0
                        motohour_line_ids = self.env['mining.motohour.entry.line'].search([('motohour_id', 'in', mining_daily_entry_ids.ids), ('technic_id', '=', technic_line.technic_id.id)])
                        cause_line_ids = self.env['mining.motohour.entry.cause.line'].search([('motohour_cause_id', 'in', motohour_line_ids.ids)])
                        for cause_line in cause_line_ids:
                            if cause_line.cause_id.calc_production:
                                production_time += cause_line.diff_time
                            else:
                                unproductive_time += cause_line.diff_time
                            if cause_line.cause_id.cause_type.type == 'non_smu':
                                off_time += cause_line.diff_time
                            total_worked_hour += cause_line.diff_time
                        technic_motohour_dict[technic_line.technic_id.id] = {
                            'cost_id': obj.id,
                            'technic_id': technic_line.technic_id.id,
                            'total_worked_hour': total_worked_hour,
                            'production_time': production_time,
                            'unproductive_time': unproductive_time,
                            'off_time': off_time,
                        }
                        self.env['cost.worksheet.technic.motohour'].create(technic_motohour_dict[technic_line.technic_id.id])
        obj.unz_compute_cost()

    # ҮНЗ-ын хувиарлах тооцоолуур
    @api.multi
    def unz_compute_cost(self):
        task_type_ids = {}
        technic_line_ids = {}
        human_hour = 0
        perf_qty = 0
        perf_hours = 0
        total_freight_tn_km = 0

        for salary_line in self.all_salary_line_ids:
            for line in salary_line.line_ids:
                if line.task_type_id.analytic_account_id.parent_id not in task_type_ids:
                    task_type_ids[line.task_type_id.analytic_account_id.parent_id] = {
                        'analytic_account_id': line.task_type_id.analytic_account_id.parent_id,
                        'assign_amount': line.perf_hours,
                    }
                else:
                    task_type_ids[line.task_type_id.analytic_account_id.parent_id]['assign_amount'] += line.perf_hours
                human_hour += line.perf_hours
        for technic_line in self.technic_line_ids:
            if technic_line.task_type_id.analytic_account_id.parent_id not in technic_line_ids:
                technic_line_ids[technic_line.task_type_id.analytic_account_id.parent_id] = {
                    'analytic_account_id': technic_line.task_type_id.analytic_account_id.parent_id,
                    'assign_amount': technic_line.perf_qty,
                    'perf_hours': technic_line.perf_hours,
                    'total_freight_tn_km': technic_line.total_freight_tn_km
                }
            else:
                technic_line_ids[technic_line.task_type_id.analytic_account_id.parent_id]['assign_amount'] += technic_line.perf_qty
                technic_line_ids[technic_line.task_type_id.analytic_account_id.parent_id]['perf_hours'] += technic_line.perf_hours
                technic_line_ids[technic_line.task_type_id.analytic_account_id.parent_id]['total_freight_tn_km'] += technic_line.total_freight_tn_km
            perf_qty += technic_line.perf_qty
            total_freight_tn_km += technic_line.total_freight_tn_km
            perf_hours += technic_line.perf_hours

        for overhead_expenses in self.overhead_expenses_line_ids:
            if overhead_expenses.assign_type == 'People_Per_Hour':
                for key, value in task_type_ids.items():
                    vals = {
                        'overhaed_expenses_id': overhead_expenses.id,
                        'account_analytic_id': value['analytic_account_id'].id,
                        'assign_amount': value['assign_amount'] / human_hour * overhead_expenses.standard_cost_day if human_hour and overhead_expenses.standard_cost_day else 0
                    }
                    self.env['cost.worksheet.overhead.expenses.line'].create(vals)

            elif overhead_expenses.assign_type == 'production_quantity':
                for key, value in technic_line_ids.items():
                    vals = {
                        'overhaed_expenses_id': overhead_expenses.id,
                        'account_analytic_id': value['analytic_account_id'].id,
                        'assign_amount': value['assign_amount'] / perf_qty * overhead_expenses.standard_cost_day if perf_qty and overhead_expenses.standard_cost_day else 0
                    }
                    self.env['cost.worksheet.overhead.expenses.line'].create(vals)
            elif overhead_expenses.assign_type == 'usage_hour_tn_km':
                for key, value in technic_line_ids.items():
                    vals = {
                        'overhaed_expenses_id': overhead_expenses.id,
                        'account_analytic_id': value['analytic_account_id'].id,
                        'assign_amount': value['total_freight_tn_km'] / total_freight_tn_km * overhead_expenses.standard_cost_day if total_freight_tn_km and overhead_expenses.standard_cost_day else 0
                    }
                    self.env['cost.worksheet.overhead.expenses.line'].create(vals)
            elif overhead_expenses.assign_type == 'usage_hour_mot_hour':
                for key, value in technic_line_ids.items():
                    vals = {
                        'overhaed_expenses_id': overhead_expenses.id,
                        'account_analytic_id': value['analytic_account_id'].id,
                        'assign_amount': value['perf_hours'] / perf_hours * overhead_expenses.standard_cost_day if perf_hours and overhead_expenses.standard_cost_day else 0
                    }
                    self.env['cost.worksheet.overhead.expenses.line'].create(vals)

    @api.multi
    def import_camp_employee(self):
        for obj in self:
            employee_ids = self.env['hr.employee'].search([('is_camp_employee', '=', True),
                                                           ('state_id.type', 'in', ('basis', 'trial', 'contract', 'trainee')),
                                                           ('company_id', '=', obj.company_id.id)])
            for employee in employee_ids:
                if employee.contract_id:
                    if employee.contract_id.working_hours:
                        line_id = self.env['cost.worksheet.camp.salary'].search([('salary_cost_id', '=', obj.id), ('employee_id', '=', employee.id)])
                        if not line_id:
                            vals = {
                                'salary_cost_id': obj.id,
                                'employee_id': employee.id,
                                'date': obj.start_date,
                                'norm_hour': employee.contract_id.working_hours.work_hours_day,
                                'norm_hour_salary': employee.contract_id.hour_salary,
                                'norm_salary': employee.contract_id.hour_salary * employee.contract_id.working_hours.work_hours_day,
                                'perf_hours': employee.contract_id.working_hours.work_hours_day,
                            }
                            self.env['cost.worksheet.camp.salary'].create(vals)
                        else:
                            line_id.write({
                                'norm_hour': employee.contract_id.working_hours.work_hours_day,
                                'norm_hour_salary': employee.contract_id.hour_salary,
                                'norm_salary': employee.contract_id.hour_salary * employee.contract_id.working_hours.work_hours_day
                            })
                    else:
                        raise UserError(_('The employee does not have an working hours. %s' % employee.name))
                else:
                    raise UserError(_('The employee does not have an contract. %s' % employee.name))


class CostWorksheetWorkflowHistory(models.Model):
    _name = 'cost.worksheet.workflow.history'
    """Ажлын хуудас ажлын урсгалын түүх"""

    cost_worksheet_id = fields.Many2one('cost.worksheet', 'Cost worksheet', readonly=True, ondelete='cascade')
    line_sequence = fields.Integer('Workflow Step')
    name = fields.Char('Verification Step', readonly=True)
    user_ids = fields.Many2many('res.users', 'res_users_cost_worksheet_workflow_history_ref', 'history_id', 'user_id',
                                'Validators')
    sent_date = fields.Datetime('Sent date', required=True, readonly=True)
    user_id = fields.Many2one('res.users', 'Validator', readonly=True)
    action_date = fields.Datetime('Action date', readonly=True)
    action = fields.Selection(STATE_SELECTION, 'Action', readonly=True)
