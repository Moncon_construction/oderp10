# -*- coding: utf-8 -*-
import time

from odoo import _, api, fields, models
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError


class CostWorkSheetEmployee(models.Model):
    _name = 'cost.worksheet.employee'

    def _get_total(self):
        for obj in self:
            total_perf_salary = 0.0
            total_norm_salary = 0.0
            total_bndsh = 0.0
            total_h = 0.0
            for line in obj.line_ids:
                total_perf_salary += line.perf_salary
                total_norm_salary += line.norm_salary
                total_h += line.perf_hours
                total_bndsh += line.bndsh
            obj.total_perf_hours = total_h
            obj.total_perf_salary = total_perf_salary
            obj.total_diff_salary = total_norm_salary - total_perf_salary
            obj.total_bndsh = total_bndsh

    cost_id = fields.Many2one('cost.worksheet', string='Cost Worksheet', ondelete='cascade', index=True, copy=False)
    employee_id = fields.Many2one('hr.employee', string='Employee', required=True)
    total_perf_salary = fields.Float(string='Performance Salary', compute=_get_total, default=0)
    total_perf_hours = fields.Float(string='Performance Hours', compute=_get_total, default=0)
    total_diff_salary = fields.Float(string='Difference Salary', compute=_get_total, default=0)
    total_bndsh = fields.Float(string='BNDSH', compute=_get_total)
    norm_id = fields.Many2one('task.salary.norm', string='Norm UoM')
    line_ids = fields.One2many('cost.worksheet.salary', 'employee_line_id', 'Lines')
    move_id = fields.Many2one('account.move', 'Account move')
