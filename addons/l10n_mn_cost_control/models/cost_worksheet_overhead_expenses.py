# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
from odoo import _, api, fields, models


class CostWorksheetOverheadExpenses(models.Model):
    _name = 'cost.worksheet.overhead.expenses'

    cost_id = fields.Many2one('cost.worksheet', string='Cost Worksheet', ondelete='cascade', index=True, copy=False)
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    state = fields.Selection([('draft', 'Draft'),
                              ('approved', 'Approved')], related='cost_id.state', string='Worksheet Status', readonly=True, copy=False, store=True, default='draft')
    name = fields.Char(string='OE name')
    account_id = fields.Many2one('account.account', string='Account')
    standard_cost_day = fields.Float(string='Standard Cost/Day/', default=0.0)
    compute_account_check = fields.Boolean('Compute account check')
    assign_type = fields.Selection([('People_Per_Hour', 'People Per Hour'),
                                    ('production_quantity', 'Production Quantity'),
                                    ('usage_hour_tn_km', 'Usage Hour/tm/km/'),
                                    ('usage_hour_mot_hour', 'Usage Hour/mot/hour/'),
                                    ('direct', 'Direct')
                                    ], string='Assign type', required=True, track_visibility='onchange')
    overhead_expenses_analytic_ids = fields.One2many('cost.worksheet.overhead.expenses.line', 'overhaed_expenses_id',
                                                     string='Overhead Expenses Analytic')
    account_move_count = fields.Integer(compute='_compute_overhead_expenses_account_move', string='Receptions', default=0)
    account_move_ids = fields.Many2many('account.move', compute='_compute_overhead_expenses_account_move', string='Productions', copy=False)

    # Smart button дээр журналын бичилтын хэд байгаа тоог харуулна
    def _compute_overhead_expenses_account_move(self):
        for obj in self:
            AccountMoveLine = self.env['account.move.line']
            amls = AccountMoveLine.search([('account_id', '=', obj.account_id.id), ('date', '>=', obj.cost_id.start_date), ('date', '<=', obj.cost_id.end_date)])
            ams = False
            if amls:
                ams = amls.mapped('move_id')
            if ams:
                obj.account_move_ids = ams
                obj.account_move_count = len(ams)

    # Smart button дарахад ЖБ-рүү үсрэнэ
    @api.multi
    def view_overhead_expenses_moves(self):
        for obj in self:
            action = self.env.ref('account.action_move_line_form')
            result = action.read()[0]

            # override the context to get rid of the default filtering on account move
            result.pop('id', None)
            result['context'] = {}
            account_move_ids = sum([obj.account_move_ids.ids for obj in self], [])
            if account_move_ids:
                result['domain'] = "[('id','in',[" + ','.join(map(str, account_move_ids)) + "])]"
            else:
                result['domain'] = "[('id','in',[])]"
            return result


class CostWorksheetOverheadExpensesAnalyticAccount(models.Model):
    _name = 'cost.worksheet.overhead.expenses.line'

    overhaed_expenses_id = fields.Many2one('cost.worksheet.overhead.expenses', string='Cost Worksheet', ondelete='cascade', index=True, copy=False)
    account_analytic_id = fields.Many2one('account.analytic.account', string='Analytic Account')
    assign_amount = fields.Float(string="Assign amount")
