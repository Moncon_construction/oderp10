# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
import time

from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.addons import decimal_precision as dp  # @UnresolvedImport


class CostWorksheetProduct(models.Model):
    _name = 'cost.worksheet.product'
    _description = 'Cost Worksheet Product'
    _rec_name = 'product_id'
    _order = "product_id"

    @api.depends('line_ids', 'line_ids.estimate_product_qty')
    def _compute_expense(self):
        for cost in self:
            expense_qty = 0
            for line in cost.line_ids:
                expense_qty += line.estimate_product_qty
            cost.expense_qty = expense_qty

    @api.depends('initial_qty', 'income_qty', 'expense_qty', 'price_unit')
    def _compute_end(self):
        for cost in self:
            cost.end_qty = cost.initial_qty + cost.income_qty - cost.expense_qty
            cost.total_expense = cost.price_unit * cost.expense_qty

    @api.one
    @api.depends('product_id')
    def _compute_standard_price(self):
        module = self.sudo().env['ir.module.module'].search(
            [('name', '=', 'l10n_mn_stock_account_cost_for_each_wh'),
             ('state', 'in', ('installed', 'to upgrade'))])
        if module:
            # Агуулах бүрээр өртөг тооцоолдог үед тухайн барааны агуулахаарх өртөг шаардах хуудасын
            # мөрүүдэд өртгөө авна
            if self.product_id and self.cost_id.company_id and self.warehouse_id:
                standard_price = self.env['product.warehouse.standard.price'].search(
                    [('company_id', '=', self.cost_id.company_id.id),
                     ('warehouse_id', '=', self.warehouse_id.id),
                     ('product_id', '=', self.product_id.id)], limit=1).standard_price
                self.price_unit = standard_price
        else:
            self.price_unit = self.product_id.standard_price

    @api.depends('product_id', 'cost_id.start_date')
    def _compute_available_qty(self):
        for obj in self:
            stock_picking_type = self.env['stock.picking.type'].search(
                [('warehouse_id', '=', obj.warehouse_id.id), ('code', '=', 'outgoing')], limit=1)
            if obj.product_id:
                obj.initial_qty = obj.product_id.get_qty_availability([stock_picking_type.default_location_src_id.id], obj.cost_id.start_date)
            else:
                obj.initial_qty = 0

    @api.depends('product_id', 'cost_id.start_date')
    def _compute_income_qty(self):
        for obj in self:
            qty_dp_digit = (self.env['decimal.precision'].precision_get('Product Unit of Measure') or 2)
            stock_picking_type = self.env['stock.picking.type'].search(
                [('warehouse_id', '=', obj.warehouse_id.id), ('code', '=', 'outgoing')], limit=1)
            location_ids = ", ".join(str(id) for id in [stock_picking_type.default_location_src_id.id])
            qry = """
                                SELECT ROUND(COALESCE(SUM(table1.in_qty))::DECIMAL, %s) AS in_qty
                                FROM
                                (
                                    SELECT 
                                        /* Тухайн байрлалд орж ирсэн /done/ хөдөлгөөн буюу орлого */
                                        m.product_uom_qty/uom_m.factor*uom_t.factor AS in_qty
                                    FROM stock_move m
                                    LEFT JOIN product_product pp ON (pp.id = m.product_id)
                                    LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id)
                                    LEFT JOIN product_uom uom_m ON (uom_m.id = m.product_uom)
                                    LEFT JOIN product_uom uom_t ON (uom_t.id = pt.uom_id)
                                    WHERE (m.location_dest_id IN (%s)) AND m.location_id != m.location_dest_id
                                    AND m.state = 'done' AND m.date BETWEEN '%s' AND '%s' AND pp.id = %s
                                ) table1
                            """ % (
                qty_dp_digit, location_ids, obj.cost_id.start_date, obj.cost_id.end_date, obj.product_id.id)
            self._cr.execute(qry)
            results = self._cr.dictfetchall()
            if results and len(results) > 0 and results[0]['in_qty']:
                obj.income_qty = results[0]['in_qty']

    cost_id = fields.Many2one('cost.worksheet', string='Cost Worksheet', ondelete='cascade', index=True, copy=False)
    date = fields.Date('Date', required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    warehouse_id = fields.Many2one('stock.warehouse', string='Technic', required=True)
    product_id = fields.Many2one('product.product', string='Product', ondelete='restrict', required=True)
    product_uom = fields.Many2one('product.uom', string='Unit of Measure', required=True)
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    #plan_hours = fields.Float(string='Plan Hours', default=0)
    #perf_hours = fields.Float(string='Performance Hours', default=0)
    #qty_norm = fields.Float(string='Norm', digits=dp.get_precision('Product Unit of Measure'), default=0)
    initial_qty = fields.Float(compute='_compute_available_qty', string='Initial Balance Quantity', digits=dp.get_precision('Product Unit of Measure'), default=0)
    income_qty = fields.Float(compute='_compute_income_qty', string='Income Quantity', digits=dp.get_precision('Product Unit of Measure'), default=0)
    expense_qty = fields.Float(compute='_compute_expense', string='Expense Quantity', digits=dp.get_precision('Product Unit of Measure'), default=0)
    end_qty = fields.Float(compute='_compute_end', string='End Balance Quantity', digits=dp.get_precision('Product Unit of Measure'), default=0)
    price_unit = fields.Float(compute='_compute_standard_price', string='Price Unit', digits=dp.get_precision('Product Unit of Measure'))
    total_expense = fields.Float(compute='_compute_end', string='Total Expense', readonly=True)
    line_ids = fields.One2many('cost.worksheet.product.line', 'cost_product_id', string='Lines', copy=False)
    state = fields.Selection([('draft', 'Draft'),
                              ('approved', 'Approved')], related='cost_id.state', string='Worksheet Status', readonly=True, copy=False, store=True, default='draft')
    move_id = fields.Many2one('account.move', 'Account move')

    @api.onchange('product_id')
    def onchange_product_id(self):
        result = {}
        if not self.product_id:
            return result
        self.product_uom = self.product_id.uom_po_id or self.product_id.uom_id
        result['domain'] = {'product_uom': [('category_id', '=', self.product_id.uom_id.category_id.id)]}
        return result


class CostWorksheetProductLine(models.Model):
    _name = 'cost.worksheet.product.line'
    _description = 'Cost Worksheet Product Line'

    cost_product_id = fields.Many2one('cost.worksheet.product', string='Cost Product', ondelete='cascade', index=True, copy=False)
    product_id = fields.Many2one('product.product', string='Product', related='cost_product_id.product_id')
    warehouse_id = fields.Many2one('stock.warehouse', string='Technic', related='cost_product_id.warehouse_id')
    task_type_id = fields.Many2one('task.type', required=True, string='Task Type')
    analytic_account_id = fields.Many2one(related='task_type_id.analytic_account_id')
    norm_id = fields.Many2one('task.product.norm', string='Product Norm')
    norm_production_uom = fields.Many2one('product.uom', string='Norm UoM', required=True)
    norm_production_qty = fields.Float(string='Norm Production Quantity', digits=dp.get_precision('Product Unit of Measure'), default=0)
    perf_production_qty = fields.Float(string='Performance Production Quantity', digits=dp.get_precision('Product Unit of Measure'), default=0)
    norm_product_qty = fields.Float(string='Norm Product Quantity', digits=dp.get_precision('Product Unit of Measure'), default=0)
    estimate_product_qty = fields.Float(string='Estimate Expense Quantity', digits=dp.get_precision('Product Unit of Measure'), default=0)
