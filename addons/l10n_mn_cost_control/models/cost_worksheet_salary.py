# -*- coding: utf-8 -*-
import time

from odoo import _, api, fields, models
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError


class CostWorksheetSalary(models.Model):
    _name = 'cost.worksheet.salary'
    _description = 'Cost Worksheet Salary'
    _rec_name = 'employee_id'

    @api.multi
    def _get_bndsh(self):
        for obj in self:
            obj.bndsh = obj.perf_salary * obj.employee_line_id.cost_id.company_id.percent / 100

    employee_line_id = fields.Many2one('cost.worksheet.employee', string='Cost Worksheet', ondelete='cascade', index=True, copy=False)
    norm_id = fields.Many2one('task.salary.norm', string='Norm UoM')
    salary_cost_id = fields.Many2one('cost.worksheet', string='Cost Worksheet', ondelete='cascade', index=True, copy=False)
    date = fields.Date('Date', required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    employee_id = fields.Many2one('hr.employee', string='Employee', required=True)
    technic_id = fields.Many2one('technic', string='Technic')
    task_type_id = fields.Many2one('task.type', required=True, string='Task Type')
    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic account', related='task_type_id.analytic_account_id')
    norm_salary = fields.Float(string='Norm Salary', default=0)
    perf_salary = fields.Float(compute='_perf_salary', string='Performance Salary', default=0)
    diff_salary = fields.Float(compute='_perf_salary', string='Difference Salary', default=0)
    norm_hours = fields.Float(string='Norm Hour', default=0)
    perf_hours = fields.Float(string='Performance Hours', default=0)
    norm_qty = fields.Float(string='Norm Quantity')
    perf_qty = fields.Float(string='Performance Quantity')
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    state = fields.Selection([('draft', 'Draft'),
                              ('approved', 'Approved')], related='salary_cost_id.state', string='Worksheet Status', readonly=True, copy=False, store=True, default='draft')
    bndsh = fields.Float(string='BNDSH', compute=_get_bndsh)
    salary_calculate_type = fields.Selection([('hour', 'Hour'),
                                              ('creation', 'Creation')], string='Salary Calculate Type', default='hour')

    @api.depends('salary_calculate_type')
    def _perf_salary(self):
        for obj in self:
            if obj.salary_calculate_type == 'hour':
                perf = obj.norm_salary / obj.norm_hours * obj.perf_hours if obj.norm_hours * obj.perf_hours else 0
                obj.perf_salary = perf
                obj.diff_salary = obj.norm_salary - perf
            elif obj.salary_calculate_type == 'creation':
                perf = obj.norm_salary / obj.norm_qty * obj.perf_qty if obj.norm_qty * obj.perf_qty else 0
                obj.perf_salary = perf
                obj.diff_salary = obj.norm_salary - perf


class CostWorksheetCampSalary(models.Model):
    _name = 'cost.worksheet.camp.salary'
    _description = 'Cost Worksheet Camp Salary'
    _rec_name = 'employee_id'

    @api.depends('perf_hours', 'norm_salary')
    def _diff_salary(self):
        for obj in self:
            obj.diff_salary = obj.perf_salary - obj.norm_salary

    @api.depends('salary_calculate_type', 'perf_hours', 'norm_hour_salary')
    def _perf_salary(self):
        for obj in self:
            if obj.salary_calculate_type == 'hour':
                obj.perf_salary = obj.norm_hour_salary * obj.perf_hours
            else:
                pass

    @api.multi
    def _get_bndsh(self):
        for obj in self:
            obj.bndsh = obj.perf_salary * obj.salary_cost_id.company_id.percent / 100

    salary_cost_id = fields.Many2one('cost.worksheet', string='Cost Worksheet', ondelete='cascade', index=True, copy=False)
    date = fields.Date('Date', required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    norm_hour = fields.Float(string='Norm Hour', default=0)
    employee_id = fields.Many2one('hr.employee', string='Employee', required=True)
    norm_hour_salary = fields.Float(string='Norm Hour Salary', default=0)
    norm_salary = fields.Float(string='Norm Salary', default=0)
    perf_salary = fields.Float(compute='_perf_salary', string='Performance Salary', store=True)
    perf_hours = fields.Float(string='Performance Hours', default=0)
    diff_salary = fields.Float(string='Difference Salary', compute=_diff_salary, store=True)
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    bndsh = fields.Float(string='BNDSH', compute=_get_bndsh)
    salary_calculate_type = fields.Selection([('hour', 'Hour'),
                                              ('creation', 'Creation')], string='Salary Calculate Type', default='hour')
    move_id = fields.Many2one('account.move', 'Account move')
