# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.addons import decimal_precision as dp  # @UnresolvedImport


class CostWorksheetTechnic(models.Model):
    _name = 'cost.worksheet.technic'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = 'Cost Worksheet Technic'

    @api.depends('plan_hours', 'perf_hours')
    def _compute_diff_hours(self):
        for cost in self:
            cost.diff_hours = cost.plan_hours - cost.perf_hours

    cost_id = fields.Many2one('cost.worksheet', string='Cost Worksheet', ondelete='cascade', index=True, copy=False)
    employee_id = fields.Many2one('hr.employee', string='Employee', required=True)
    technic_id = fields.Many2one('technic', string='Technic')
    task_type_id = fields.Many2one('task.type', required=True, string='Task Type')
    norm_qty = fields.Float(string='Norm Quantity', digits=dp.get_precision('Product Unit of Measure'), default=0)
    perf_qty = fields.Float(string='Performance Quantity', digits=dp.get_precision('Product Unit of Measure'), default=0)
    norm_uom = fields.Many2one('product.uom', string='Norm UoM', required=True)
    plan_hours = fields.Float(string='Plan Hours', default=0)
    perf_hours = fields.Float(string='Performance Hours', default=0)
    diff_hours = fields.Float(compute='_compute_diff_hours', string='Difference Hours')
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    total_freight_tn_km = fields.Float('Total freight tn/km')
    state = fields.Selection([('draft', 'Draft'),
                              ('approved', 'Approved')], related='cost_id.state', string='Worksheet Status', readonly=True, copy=False, store=True, default='draft')


class CostWorksheetRentalTechnic(models.Model):
    _name = 'cost.worksheet.rental.technic'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = 'Cost Worksheet Rental Technic'

    @api.depends('plan_hours', 'perf_hours')
    def _compute_diff_hours(self):
        for cost in self:
            cost.diff_hours = cost.plan_hours - cost.perf_hours

    @api.depends('price_unit', 'perf_hours')
    def _sub_total(self):
        for cost in self:
            cost.sub_total = cost.perf_hours * cost.price_unit

    summary_id = fields.Many2one('cost.worksheet.rental.summary', string='Summary Line', ondelete='cascade', index=True, copy=False)
    employee_id = fields.Many2one('hr.employee', string='Employee')
    technic_id = fields.Many2one('technic', string='Technic')
    task_type_id = fields.Many2one('task.type', required=True, string='Task Type')
    analytic_account_id = fields.Many2one(related='task_type_id.analytic_account_id')
    norm_qty = fields.Float(string='Norm Quantity', digits=dp.get_precision('Product Unit of Measure'), default=0)
    perf_qty = fields.Float(string='Performance Quantity', digits=dp.get_precision('Product Unit of Measure'), default=0)
    norm_uom = fields.Many2one('product.uom', string='Norm UoM', required=True)
    plan_hours = fields.Float(string='Plan Hours', default=0)
    perf_hours = fields.Float(string='Performance Hours', default=0)
    diff_hours = fields.Float(compute='_compute_diff_hours', string='Difference Hours')
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    price_unit = fields.Float('Price Unit')
    sub_total = fields.Float('Sub Total', compute=_sub_total, store=True)
    total_freight_tn_km = fields.Float('Total freight tn/km')
    state = fields.Selection([('draft', 'Draft'),
                              ('approved', 'Approved')], related='summary_id.state', string='Worksheet Status', readonly=True, copy=False, store=True, default='draft')


class CostWorksheetRentalTechnicLine(models.Model):
    _name = 'cost.worksheet.rental.summary'

    def _get_total(self):
        for obj in self:
            total_perf_qty = 0.0
            total_plan_hours = 0.0
            total_perf_hours = 0.0
            total_diff_hours = 0.0
            total_total_freight_tn_km = 0.0
            total_sub_total = 0.0
            for line in obj.line_ids:
                total_perf_qty += line.perf_qty
                total_plan_hours += line.plan_hours
                total_perf_hours += line.perf_hours
                total_diff_hours += line.diff_hours
                total_total_freight_tn_km += line.total_freight_tn_km
                total_sub_total += line.sub_total
            obj.perf_qty = total_perf_qty
            obj.plan_hours = total_plan_hours
            obj.perf_hours = total_perf_hours
            obj.diff_hours = total_diff_hours
            obj.total_freight_tn_km = total_total_freight_tn_km
            obj.sub_total = total_sub_total

    cost_id = fields.Many2one('cost.worksheet', string='Cost Worksheet', ondelete='cascade', index=True, copy=False)
    technic_id = fields.Many2one('technic', string='Technic')
    perf_qty = fields.Float(string='Total Performance Quantity', compute=_get_total, digits=dp.get_precision('Product Unit of Measure'), default=0)
    norm_uom = fields.Many2one('product.uom', string='Norm UoM', required=True)
    plan_hours = fields.Float(string='Total Plan Hours', compute=_get_total)
    perf_hours = fields.Float(string='Total Performance Hours', compute=_get_total)
    diff_hours = fields.Float(string='Total Difference Hours', compute=_get_total)
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    price_unit = fields.Float('Price Unit')
    sub_total = fields.Float('Sub Total', compute=_get_total)
    total_freight_tn_km = fields.Float('Total freight tn/km', compute=_get_total)
    state = fields.Selection([('draft', 'Draft'),
                              ('approved', 'Approved')], related='cost_id.state', string='Worksheet Status', readonly=True, copy=False, store=True, default='draft')
    line_ids = fields.One2many('cost.worksheet.rental.technic', 'summary_id', 'Lines')
    move_id = fields.Many2one('account.move', 'Account move')
