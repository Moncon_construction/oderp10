# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
import time

from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.addons import decimal_precision as dp  # @UnresolvedImport

class CostWorksheetTechnicMotohour(models.Model):
    _name = 'cost.worksheet.technic.motohour'
    _description = 'Cost Worksheet technic motohour'
    _rec_name = 'technic_id'

    cost_id = fields.Many2one('cost.worksheet', string='Cost Worksheet', ondelete='cascade', index=True, copy=False)
    technic_id = fields.Many2one('technic', string='Technic')
    total_worked_hour = fields.Float('Total worked hour')
    production_time = fields.Float('Production Time')
    unproductive_time = fields.Float('Unproductive Time')
    off_time = fields.Float('OFF time')
