# -*- coding: utf-8 -*-
from odoo.exceptions import UserError
from odoo import api, fields, models, _


class CampSalaryCategory(models.Model):
    _name = 'camp.salary.category'
    _inherit = ['mail.thread']

    name = fields.Char('Name', required=True)
    company_id = fields.Many2one('res.company', 'Company', required=True, default=lambda self: self.env.user.company_id)


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    camp_salary_category_id = fields.Many2one('camp.salary.category', 'Salary Category')


class CampSalaryConfigLine(models.Model):
    _name = 'camp.salary.config.line'

    category_id = fields.Many2one('camp.salary.category', 'Category', required=True)
    account_id = fields.Many2one('account.account', 'Account', required=True, domain=[('internal_type', '=', 'other')])
