# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
import time

from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.addons import decimal_precision as dp  # @UnresolvedImport

class OverheadExpensesNorm(models.Model):
    _name = 'overhead.expenses.norm'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = 'Task Product Norm'

    name = fields.Char('Name', required=True,track_visibility='onchange', states={'approved': [('readonly', True)]})
    start_date = fields.Date('Start Date', required=True, default=lambda *a: time.strftime('%Y-%m-01'),track_visibility='onchange', states={'approved': [('readonly', True)]})
    end_date = fields.Date('End Date', required=True, default=lambda *a: time.strftime('%Y-%m-%d'), track_visibility='onchange', states={'approved': [('readonly', True)]})
    line_ids = fields.One2many('overhead.expenses.norm.line', 'overhead_expenses_norm_id', string='Lines', copy=False, states={'approved': [('readonly', True)]})
    user_id = fields.Many2one('res.users', string='Responsible', default=lambda self: self.env.user, readonly=True)
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    state = fields.Selection([('draft', 'Draft'),
                              ('approved', 'Approved')], string='State', default='draft', required=True, track_visibility='onchange')
    description = fields.Text('Description')

    @api.multi
    def unlink(self):
        for cost in self:
            if cost.state != 'draft':
                raise UserError(_('Delete only draft in state'))
        return super(OverheadExpensesNorm, self).unlink()

    @api.multi
    def validate(self):
        # Батлах
        self.write({'state': 'approved'})

    @api.multi
    def action_to_draft(self):
        # Ноороглох
        self.write({'state': 'draft'})


class OverheadExpensesNormLine(models.Model):
    _name = 'overhead.expenses.norm.line'
    _description = 'Task Product Norm Line'

    overhead_expenses_norm_id = fields.Many2one('overhead.expenses.norm', string='Overhead Expenses', ondelete='cascade', index=True, copy=False)
    name = fields.Char(string='OE name')
    standard_cost = fields.Float(string='Standard Cost')
    account_id = fields.Many2one('account.account', string='Account')
    assign_type = fields.Selection([('People_Per_Hour', 'People Per Hour'),
                                    ('production_quantity', 'Production Quantity'),
                                    ('usage_hour_tn_km', 'Usage Hour/tm/km/'),
                                    ('usage_hour_mot_hour', 'Usage Hour/mot/hour/'),
                                    ('direct', 'Direct')
                                    ], string='Assign type', required=True, track_visibility='onchange')
    analytic_account_id = fields.Many2one('account.analytic.account', string='Analytic account')
    compute_account_check = fields.Boolean('Compute account check')