# -*- coding: utf-8 -*-
from odoo.exceptions import UserError
from odoo import api, fields, models, _


class ResCompany(models.Model):
    _inherit = "res.company"

    cost_center = fields.Selection(selection_add=[('task_type', 'Task Type')])
    direct_labor_account = fields.Many2one('account.account', string="Direct Labor Account", domain=[('user_type_id.type', '=', 'other')])
    direct_valuation_account = fields.Many2one('account.account', string="Direct Valuation Account", domain=[('user_type_id.type', '=', 'other')])
    overhead_expense_account = fields.Many2one('account.account', string="Overhead Expense Account", domain=[('user_type_id.type', '=', 'other')])
    percent = fields.Float(string="Percentage", digits=(16, 1))
    overhead_journal_id = fields.Many2one('account.journal', string='Overhead Expense Journal')
    direct_labor_expense_account_id = fields.Many2one('account.account', 'Direct labor expense account', domain=[('user_type_id.type', '=', 'other')])
    direct_salary_journal_id = fields.Many2one('account.journal', string='Direct Salary Journal')
    camp_salary_category_line_ids = fields.Many2many('camp.salary.config.line', 'res_company_camp_salay_account_rel', 'company_id', 'category_id', 'Lines')
    rental_expense_account_id = fields.Many2one('account.account', string="Rental technic expense account", domain=[('user_type_id.type', '=', 'other')])
    cost_entry_create_time = fields.Selection([
        ('by_day', "Create a journal entry by day"),
        ('by_package', "Create a journal entry by package")
    ], "Cost entry create time", default='by_day')
    package_journal_id = fields.Many2one('account.journal', string='Package Account Move Journal')


class StockTransitOrderLine(models.Model):
    _inherit = 'stock.transit.order.line'

    @api.multi
    def _prepare_stock_moves(self, picking):
        # Function to retrieve in picking's stock move values
        res = super(StockTransitOrderLine, self)._prepare_stock_moves(picking)
        if self.company_id.default_analytic_account_id:
            if self.company_id.cost_center == 'task_type':
                res['analytic_account_id'] = self.company_id.default_analytic_account_id.id
                res['analytic_share_ids'] = [(0, 0, {'analytic_account_id': self.company_id.default_analytic_account_id.id, 'rate': 100})]
        else:
            raise UserError(_('The company does not have default analytic account!'))
        return res

    @api.multi
    def _prepare_out_stock_moves(self, picking):
        # Function to retrieve out picking's stock move values
        res = super(StockTransitOrderLine, self)._prepare_out_stock_moves(picking)
        if self.company_id.default_analytic_account_id:
            if self.company_id.cost_center == 'task_type':
                res['analytic_account_id'] = self.company_id.default_analytic_account_id.id
                res['analytic_share_ids'] = [(0, 0, {'analytic_account_id': self.company_id.default_analytic_account_id.id, 'rate': 100})]
        else:
            raise UserError(_('The company does not have default analytic account!'))
        return res


class StockInventoryLine(models.Model):
    _inherit = "stock.inventory.line"

    def _get_move_values(self, qty, location_id, location_dest_id):
        # Function to retrieve stock move values
        self.ensure_one()
        res = super(StockInventoryLine, self)._get_move_values(qty, location_id, location_dest_id)
        if self.company_id.default_analytic_account_id:
            if self.company_id.cost_center == 'task_type':
                res['analytic_account_id'] = self.company_id.default_analytic_account_id.id
                res['analytic_share_ids'] = [(0, 0, {'analytic_account_id': self.company_id.default_analytic_account_id.id, 'rate': 100})]
        else:
            raise UserError(_('The company does not have default analytic account!'))
        return res
