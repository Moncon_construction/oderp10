# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

from odoo import models, fields, api, _

class ProductionUomLine(models.Model):
    _inherit = "production.uom.line"

    task_type_id = fields.Many2one('task.type', string="Task type", required=True)
    norm_hour = fields.Float(string="Norm hour", required=True)
