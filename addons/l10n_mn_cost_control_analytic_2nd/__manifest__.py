# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

{
    "name": "Mongolian Cost Control",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "description": """
        Өртгийн хяналт. Шинжилгээний Данс 2
""",
    'website': "http://www.asterisk-tech.mn",
    "category": "Mongolian Modules",
    "depends": ['l10n_mn_cost_control',
                'l10n_mn_analytic_2nd'],
    "init": [],
    "data": [],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}