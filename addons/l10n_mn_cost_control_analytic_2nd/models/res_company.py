# -*- coding: utf-8 -*-
from odoo.exceptions import UserError
from odoo import api, fields, models, _

class ResCompany(models.Model):
    _inherit = "res.company"

    cost_center_2nd = fields.Selection(selection_add=[('task_type', 'Task Type')])


class StockTransitOrderLine(models.Model):
    _inherit = 'stock.transit.order.line'

    @api.multi
    def _prepare_stock_moves(self, picking):
        res = super(StockTransitOrderLine, self)._prepare_stock_moves(picking)
        if self.company_id.cost_center_2nd == 'task_type':
            if self.company_id.default_analytic_account_id:
                res['analytic_2nd_account_id'] = self.company_id.default_analytic_account_id.id
                res['analytic_2nd_share_ids'] = [(0, 0, {'analytic_account_id': self.company_id.default_analytic_account_id.id, 'rate': 100})]
            else:
                raise UserError(_('The company does not have default analytic account!'))
        return res

    @api.multi
    def _prepare_out_stock_moves(self, picking):
        res = super(StockTransitOrderLine, self)._prepare_out_stock_moves(picking)
        if self.company_id.cost_center_2nd == 'task_type':
            if self.company_id.default_analytic_account_id:
                res['analytic_2nd_account_id'] = self.company_id.default_analytic_account_id.id
                res['analytic_2nd_share_ids'] = [(0, 0, {'analytic_account_id': self.company_id.default_analytic_account_id.id, 'rate': 100})]
            else:
                raise UserError(_('The company does not have default analytic account!'))
        return res


class StockInventoryLine(models.Model):
    _inherit = "stock.inventory.line"

    def _get_move_values(self, qty, location_id, location_dest_id):
        # Function to retrieve stock move values
        self.ensure_one()
        res = super(StockInventoryLine, self)._get_move_values(qty, location_id, location_dest_id)
        if self.company_id.cost_center_2nd == 'task_type':
            if not self.company_id.default_analytic_account_id:
                raise UserError(_('Please select default analytic account of this company: %s') % self.company_id.name)
            res['analytic_2nd_account_id'] = self.company_id.default_analytic_account_id.id
            res['analytic_2nd_share_ids'] = [(0, 0, {'analytic_account_id': self.company_id.default_analytic_account_id.id, 'rate': 100})]
        return res