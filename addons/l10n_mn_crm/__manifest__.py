# -*- coding: utf-8 -*-

{
    'name': 'Mongolian CRM',
    'category': 'Sales',
    'author': 'Asterisk Technologies LLC',
    'website' : 'http://asterisk-tech.mn',
    'version': '1.0',
    'description': """
        * Харилцагч нэгтгэх групп нэмэв.
    """,
    'depends': ['crm'],
    'data': [
        'security/base_security.xml',
        'wizard/base_partner_merge_views.xml',
    ],
    'installable': True,
    'auto_install': False,
}
