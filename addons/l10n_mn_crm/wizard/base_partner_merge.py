# -*- coding: utf-8 -*-
from odoo import api, fields, models


class MergePartnerAutomatic(models.TransientModel):

    _inherit = 'base.partner.merge.automatic.wizard'

    @api.model
    def _update_values(self, src_partners, dst_partner):
        # Override: Харилцагч давхардуулж үүсгэх чек нэхэлгүй ажилладаг болгов
        is_changed = False
        if hasattr(self.env.get('res.partner'), 'is_create_partner') and not dst_partner.is_create_partner:
            dst_partner.write({'is_create_partner': True})
            is_changed = True
        
        res = super(MergePartnerAutomatic, self)._update_values(src_partners, dst_partner)
        
        if hasattr(self.env.get('res.partner'), 'is_create_partner') and is_changed:
            dst_partner.write({'is_create_partner': False})
        return res
    
    def _merge(self, partner_ids, dst_partner=None):
        # Override: core дээр журнал бичилт үүссэн бол заавал админ нэгтгэж буй эсэхийг шалгаж байсан тул sudo() ашиглав.
        return super(MergePartnerAutomatic, self.sudo())._merge(partner_ids, dst_partner=dst_partner)