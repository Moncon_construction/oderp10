# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-Today Asterisk Technologies LLC Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

{
    'name': 'Борлуулалтын суваг бүртгэнэ',
    'category': 'CRM',
    'author': 'Asterisk Technologies LLC',
    'website' : 'http://asterisk-tech.mn',
    'version': '1.0',
    'description': """
Борлуулалтын суваг бүртгэнэ. Сувгийг борлуулалтын баг дээр сонгодог болгоно.
    """,
    'depends': ['crm'],
    'data': [
        'views/crm_channel_view.xml',
        'security/ir.model.access.csv',
    ],
    'installable': True,
    'auto_install': False,
}
