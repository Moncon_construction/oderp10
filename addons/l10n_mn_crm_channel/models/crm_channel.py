# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-Today Asterisk Technologies LLC Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class CrmChannel(models.Model):
    _name = 'crm.channel'

    name = fields.Char('Sales Channel')
    company_id = fields.Many2one('res.company', 'Company')

class CrmTeam(models.Model):
    _inherit = 'crm.team'

    sales_channel_id = fields.Many2one('crm.channel', 'Sales Channel')


