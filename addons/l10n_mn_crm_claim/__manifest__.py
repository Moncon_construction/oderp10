# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Mongolian Claims Management',
    'version': '1.0',
    'author': 'Asterisk Technologies LLC',
    'website' : 'http://asterisk-tech.mn',
    'category': 'Customer Relationship Management',
    'sequence': 40,
    'summary': 'Support, Bug Tracker, Helpdesk',
    'description': """
Manage Customer Claims.
=========================================
This application allows you to track your customers/suppliers claims and grievances.

It is fully integrated with the email gateway so that you can create
automatically new claims based on incoming emails.
    """,
    'depends': [
        'crm'
    ],
    'data': [
        'report/crm_claim_report_views.xml',
        'views/crm_claim_views.xml',
        'views/res_partner_views.xml',
        'security/crm_claim_security.xml',
        'security/ir.model.access.csv',
    ],
    'application': True,
    'installable': True,
    'auto_install': False
}
