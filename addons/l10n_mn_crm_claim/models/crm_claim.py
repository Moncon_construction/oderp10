# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import odoo
from odoo import api, fields, models, tools, _
from odoo.exceptions import AccessError


class CrmClaimStage(models.Model):


    _name = "crm.claim.stage"
    _description = "Claim stages"
    _rec_name = 'name'
    _order = "sequence"

    name = fields.Char('Stage Name', required=True, translate=True)
    sequence = fields.Integer(
        'Sequence', help="Used to order stages. Lower is better.", default='1')
    team_ids = fields.Many2many('crm.team', 'team_claim_stage_rel', 'stage_id', 'team_id', string='Teams',
                        help="Link between stages and sales teams. When set, this limitate the current stage to the selected sales teams.")
    case_default = fields.Boolean('Common to All Teams',
                        help="If you check this field, this stage will be proposed by default on each sales team. It will not assign this stage to existing teams.")


class CrmClaim(models.Model):
    _name = "crm.claim"
    _description = "Claims"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _order = "priority desc, create_date desc"
    _mail_post_access = 'read'

    @api.model
    def _get_default_stage_id(self):
        team_id = self.env.context.get('default_team_id')
        if not team_id:
            return False
        return self.stage_find(team_id, [('fold', '=', False)])


    kanban_state = fields.Selection([('normal', 'Normal'), ('blocked', 'Blocked'), ('done', 'Ready for next stage')], string='Kanban State',
                                    track_visibility='onchange', required=True, default='normal',
                                    help="""An Issue's kanban state indicates special situations affecting it:\n
                                           * Normal is the default situation\n
                                           * Blocked indicates something is preventing the progress of this issue\n
                                           * Ready for next stage indicates the issue is ready to be pulled to the next stage""")

    date_open = fields.Datetime(string='Assigned', readonly=True, index=True)
    date_closed = fields.Datetime(string='Closed', readonly=True, index=True)
    date_last_stage_update = fields.Datetime(
        string='Last Stage Update', index=True, default=fields.Datetime.now)
    # TDE note: is it still used somewhere ?
    channel = fields.Char(string='Channel', help="Communication channel.")

    duration = fields.Float('Duration')
    color = fields.Integer('Color Index')
    user_email = fields.Char(related='user_id.email',
                             string='User Email', readonly=True)
    date_action_last = fields.Datetime(string='Last Action', readonly=True)

    name = fields.Char(string='Claim Subject', required=True)
    active = fields.Boolean(default=True)
    action_next = fields.Char('Next Action')
    date_action_next = fields.Datetime(string='Next Action Date')
    description = fields.Text('Private Note')
    resolution = fields.Text('Resolution')
    create_date = fields.Datetime('Creation Date', readonly=True)
    write_date = fields.Datetime('Update Date')
    date_deadline = fields.Date(string='Deadline', required=True)
    date_closed = fields.Datetime('Closed', readonly=True)
    date = fields.Datetime(
        'Created Date', default=lambda self: fields.datetime.now())
    ref = fields.Reference(
        selection=odoo.addons.base.res.res_request.referenceable_models)
    categ_id = fields.Many2one('crm.lead.tag', 'Category', index=True)
    priority = fields.Selection(
        [('0', 'Low'), ('1', 'Normal'), ('2', 'High')], 'Priority', index=True, default='1')
    type_action = fields.Selection(
        [('correction', 'Corrective Action'), ('prevention', 'Preventive Action')], 'Action Type')
    user_id = fields.Many2one('res.users', string='Created Manager', required=True,
                              index=True, track_visibility='onchange', default=lambda self: self.env.uid)
    user_fault_id = fields.Many2one('hr.employee', 'Trouble Responsible')
    team_id = fields.Many2one('crm.team', 'Sales Team',
                        index=True, help="Responsible sales team."
                                " Define Responsible user and Email account for"
                                " mail gateway.")  # section_id
    company_id = fields.Many2one(
        'res.company', string='Company', default=lambda self: self.env.user.company_id)
    partner_id = fields.Many2one('res.partner', string='Partner', index=True)
    email_cc = fields.Char(string='Watchers Emails', help="""These email addresses will be added to the CC field of all inbound
        and outbound emails for this record before being sent. Separate multiple email addresses with a comma""")
    email_from = fields.Char(
        string='Email', help="These people will receive email.", index=True)

    partner_phone = fields.Char('Phone')
    stage_id = fields.Many2one('crm.claim.stage', string='Stage', track_visibility='onchange', index=True,
                               domain="['|', ('team_ids', '=', team_id), ('case_default', '=', True)]", default=_get_default_stage_id)
    cause = fields.Text('Root Cause')

    @api.model
    def _read_group_stage_ids(self, stages, domain, order):
        search_domain = [('id', 'in', stages.ids)]
        # retrieve team_id from the context, add them to already fetched
        # columns (ids)
        if 'default_team_id' in self.env.context:
            search_domain = ['|', ('team_ids', '=', self.env.context[
                                   'default_team_id'])] + search_domain

        # perform search
        return stages.search(search_domain, order=order)

    @api.onchange('partner_id')
    def _onchange_partner_id(self):

        self.email_from = self.partner_id.email
        self.partner_phone = self.partner_id.phone

    @api.multi
    def copy(self, default=None):
        if default is None:
            default = {}
        default.update(name=_('%s (copy)') % (self.name))
        return super(CrmClaim, self).copy(default=default)

    @api.model
    def create(self, vals):
        context = dict(self.env.context)
        if vals.get('team_id') and not self.env.context.get('default_team_id'):
            context['default_team_id'] = vals.get('team_id')
        return super(CrmClaim, self.with_context(context)).create(vals)

    def stage_find(self, team_id, domain=None, order='sequence'):
        search_domain = list(domain) if domain else []
        if team_id:
            search_domain += [('team_ids', '=', team_id)]
        team_stage = self.env['crm.stage'].search(
            search_domain, order=order, limit=1)
        return team_stage.id

    @api.multi
    def message_get_suggested_recipients(self):
        recipients = super(CrmClaim, self).message_get_suggested_recipients()
        try:
            for claim in self:
                if claim.partner_id:
                    claim._message_add_suggested_recipient(
                        recipients, partner=issue.partner_id, reason=_('Customer'))
                elif claim.email_from:
                    claim._message_add_suggested_recipient(
                        recipients, email=issue.email_from, reason=_('Customer Email'))
        except AccessError:
            pass
        return recipients

    @api.multi
    def email_split(self, msg):
        email_list = tools.email_split(
            (msg.get('to') or '') + ',' + (msg.get('cc') or ''))
        # check left-part is not already an alias
        return filter(lambda x: x.split('@')[0] not in self.mapped('team_id.alias_name'), email_list)

    @api.model
    def message_new(self, msg, custom_values=None):

        create_context = dict(self.env.context or {})
        create_context['default_user_id'] = False
        defaults = {
            'name':  msg.get('subject') or _("No Subject"),
            'email_from': msg.get('from'),
            'email_cc': msg.get('cc'),
            'partner_id': msg.get('author_id', False),
        }
        if custom_values:
            defaults.update(custom_values)

        res_id = super(CrmClaim, self.with_context(create_context)
                       ).message_new(msg, custom_values=defaults)
        claim = self.browse(res_id)
        email_list = claim.email_split(msg)
        partner_ids = filter(None, claim._find_partner_from_emails(email_list))
        claim.message_subscribe(partner_ids)
        return res_id

    @api.multi
    def message_update(self, msg, update_vals=None):
        """ Override to update the issue according to the email. """
        email_list = self.email_split(msg)
        partner_ids = filter(None, self._find_partner_from_emails(email_list))
        self.message_subscribe(partner_ids)
        return super(CrmClaim, self).message_update(msg, update_vals=update_vals)

    @api.multi
    @api.returns('mail.message', lambda value: value.id)
    def message_post(self, subtype=None, **kwargs):
        self.ensure_one()
        mail_message = super(CrmClaim, self).message_post(
            subtype=subtype, **kwargs)
        if subtype:
            self.sudo().write({'date_action_last': fields.Datetime.now()})
        return mail_message

    def clon_claim_warning(self):
        norm_id = self.env['shtm.norm'].search([])
        norm = self.env['shtm.norm'].browse(norm_id)
