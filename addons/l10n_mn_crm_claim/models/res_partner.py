# -*- coding: utf-8 -*-
from odoo import fields, models


class ResPartner(models.Model):
    _inherit = 'res.partner'
    claim_count = fields.Integer(compute='_compute_claim_count', string='# Claims')

    def _compute_claim_count(self):
        Claim = self.env['crm.claim']
        for partner in self:
            partner.claim_count = Claim.search_count(
                [('partner_id', 'child_of', partner.commercial_partner_id.id)])
