# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, tools


class CrmClaimReport(models.Model):
    _name = "crm.claim.report"
    _auto = False

    user_id = fields.Many2one('res.users', 'Responsible', readonly=True)
    team_id = fields.Many2one('crm.team', 'Sales Team', readonly=True)
    nbr = fields.Integer('# of Claims', readonly=True)
    company_id = fields.Many2one('res.company', 'Company', readonly=True)
    create_date = fields.Datetime('Create Date', readonly=True)
    claim_date = fields.Datetime('Claim Date', readonly=True)
    delay_close = fields.Float('Avg. Delay to Close', digits=(16, 2), readonly=True, group_operator="avg",
                               help="Number of Days to close the case")
    stage_id = fields.Many2one('crm.claim.stage', 'Stage')
    categ_id = fields.Many2one('crm.lead.tag', 'Category',domain="[('team_id','=',team_id),\
                        ('object_id.model', '=', 'crm.claim')]", readonly=True)
    partner_id = fields.Many2one('res.partner', 'Contact')
    priority = fields.Selection([('0', 'Low'), ('1', 'Normal'), ('2', 'High')])
    type_action = fields.Selection([('correction', 'Corrective Action'), ('prevention', 'Preventive Action')], 'Action Type')
    date_closed = fields.Datetime('Closed', readonly=True)
    date_deadline = fields.Date(string='Deadline', readonly=True, index=True)
    delay_expected = fields.Float('Overpassed Deadline', digits=(16, 2), readonly=True, group_operator="avg")
    email = fields.Integer('# Emails', readonly=True)
    subject = fields.Char('Claim Subject', readonly=True)


    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self._cr, 'crm_claim_report')
        self._cr.execute("""
            CREATE OR REPLACE VIEW crm_claim_report AS (
                SELECT
                    c.id as id,
                    c.date as claim_date,
                    c.date_closed as date_closed,
                    c.date_deadline as date_deadline,
                    c.user_id,
                    c.stage_id,
                    c.team_id,
                    c.partner_id,
                    c.company_id,
                    c.categ_id,
                    c.name as subject,
                    count(*) as nbr,
                    c.priority as priority,
                    c.type_action as type_action,
                    c.create_date as create_date,
                    avg(extract('epoch' from (c.date_closed-c.create_date)))/(3600*24) as  delay_close,
                    extract('epoch' from (c.date_deadline - c.date_closed))/(3600*24) as  delay_expected,
                    (SELECT count(id) FROM mail_message WHERE model='crm.claim' AND message_type IN ('email', 'comment') AND res_id=c.id) AS email

                FROM
                    crm_claim c
                    group by c.date,\
                        c.user_id,c.team_id, c.stage_id,\
                        c.categ_id,c.partner_id,c.company_id,c.create_date,
                        c.priority,c.type_action,c.date_deadline,c.date_closed,c.id
            )""")
