# -*- coding: utf-8 -*-
{
    'name': 'Тусламжийн төв',
    'category': 'Customer Relationship Management',
    'author': 'Asterisk Technologies LLC',
    'website' : 'http://asterisk-tech.mn',
    'version': '1.0',
    'description': """
Тусламжийн төв

Баримт бичлэгийн бүртгэл, боловсруулалтын адилаар Тусламжийн төв болон Тусламжийн нь сайн хэрэгсэл юм.
Таны үйл ажиллагааг хянах. Энэ цэс аман харилцаанд илүү тохирсон.
Энэ нь зайлшгүй шаардах зүйл биш юм. Харилцагчийг сонгоод тэмдэглэл нэмнэ.
    """,
    'depends': ['crm'],
    'data': [
        'views/crm_helpdesk_view.xml',
        'views/crm_helpdesk_menu.xml',
        'security/ir.model.access.csv',
        'report/crm_helpdesk_report_view.xml'
    ],
    'installable': True,
    'auto_install': False,
}
