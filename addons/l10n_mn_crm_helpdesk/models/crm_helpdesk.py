# -*- coding: utf-8 -*-
import odoo
from odoo import api, fields, models
from odoo.tools.translate import _
from odoo.tools import html2plaintext


class CrmTrackingMedium(models.Model):
    _name = "crm.tracking.medium"
    _description = "Channels"
    _order = 'name'

    name = fields.Char(string='Channel Name', required=True)
    active = fields.Boolean(string='Active', default='1')


class CrmCaseSection(models.Model):
    _name = "crm.case.section"

    name = fields.Char(string='Sales Team', size=64, required=True)
    code = fields.Char(string='Code', size=8)
    active = fields.Boolean(string='Active', help="If the active field is set to "
                            "false, it will allow you to hide the sales team without removing it.")
    change_responsible = fields.Boolean(
        string='Reassign Escalated', help="When escalating to this team override the salesman with the team leader.")
    user_id = fields.Many2one('res.users', string='Team Leader')
    member_ids = fields.Many2many(
        'res.users', 'sale_member_rel', 'section_id', 'member_id', string='Team Members')
    reply_to = fields.Char(
        'Reply-To', size=64, help="The email address put in the 'Reply-To' of all emails sent by Odoo about cases in this sales team")
    parent_id = fields.Many2one('crm.case.section', string='Parent Team')
    child_id = fields.One2many('crm.case.section', 'parent_id', string='Child Teams')
    note = fields.Text(string='Description')
    working_hours = fields.Float(string='Working Hours', digits=(16, 2))
    color = fields.Integer(string='Color Index')


class CrmHelpdesk(models.Model):
    """ Helpdesk Cases """
    _name = "crm.helpdesk"
    _description = "Helpdesk"
    _order = "id desc"
    _inherit = ['mail.thread']

    name = fields.Char(string='Query', required=True)
    active = fields.Boolean(
        string='Active', required=False, default=lambda self: 1)
    date_action_last = fields.Datetime(string='Last Action', readonly=1)
    date_action_next = fields.Datetime(string='Next Action', readonly=1)
    description = fields.Text(string='Description')
    create_date = fields.Datetime(string='Create date', readonly=True)
    write_date = fields.Datetime(string='Write date', readonly=True)
    date_deadline = fields.Date(string='Deadline')
    user_id = fields.Many2one(
        'res.users', string='Respondent', default=lambda self: self.env.user)
    section_id = fields.Many2one('crm.case.section', string='Sales Team',
                                 Index=True, help='Responsible sales team. Define Responsible user and Email account for mail gateway.')
    company_id = fields.Many2one(
        'res.company', string='Company', default=lambda self: self.env.user.company_id.id)
    date_closed = fields.Datetime(string='Date closed', readonly=True)
    partner_id = fields.Many2one(
        'res.partner', string='Partner', track_visibility='onchange')
    email_cc = fields.Text(string='Watchers Emails', size=252,
                           help="These email addresses will be added to the CC field of all inbound and outbound emails for this record before being sent. Separate multiple email addresses with a comma")
    email_from = fields.Char(
        string='Email form', size=128, help="Destination email for email gateway")
    date = fields.Datetime(default=lambda self: fields.datetime.now())
    ref = fields.Reference(
        selection=odoo.addons.base.res.res_request.referenceable_models)
    ref2 = fields.Reference(
        selection=odoo.addons.base.res.res_request.referenceable_models)
    channel_id = fields.Many2one(
        'crm.tracking.medium', string='Channel', help="Communication channel.")
    planned_revenue = fields.Float(string='Planned Revenue')
    planned_cost = fields.Float(string='Planned Costs')
    priority = fields.Selection(
        [('0', 'Low'), ('1', 'Normal'), ('2', 'High')], string='Probability', default='1')
    probability = fields.Float(string='Probability (%)')
    duration = fields.Float(string='Duration', states={
                            'done': [('readonly', True)]})
    state = fields.Selection([('draft', 'New'),
                              ('open', 'In Progress'),
                              ('pending', 'Pending'),
                              ('done', 'Closed'),
                              ('cancel', 'Cancelled')], string='State', readonly=True, track_visibility='onchange', default='draft',
                             help='The status is set to \'Draft\', when a case is created.\
                                              \nIf the case is in progress the status is set to \'Open\'.\
                                              \nWhen the case is over, the status is set to \'Done\'.\
                                              \nIf the case needs to be reviewed then the status is set to \'Pending\'.')
    color = fields.Integer(string="Color")

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        self.email_from = self.partner_id.email

    @api.multi
    def write(self, values):
        if values.get('state'):
            if values.get('state') in ['draft', 'open'] and not values.get('date_open'):
                values['date_open'] = fields.Datetime.now()
            elif values.get('state') == 'done' and not values.get('date_closed'):
                values['date_closed'] = fields.Datetime.now()
        return super(CrmHelpdesk, self).write(values)

    @api.model
    def message_new(self, msg_dict, custom_values=None):
        """ Overrides mail_thread message_new that is called by the mailgateway
            through message_process.
            This override updates the document according to the email.
        """
        if custom_values is None:
            custom_values = {}

        defaults = {
            'name': msg_dict.get('subject') or _("No Subject"),
            'description': desc,
            'partner_id': msg_dict.get('author_id', False),
            'user_id': False,
            'email_cc': msg_dict.get('cc'),
            'email_from': msg_dict.get('from'),
        }
        desc = html2plaintext(msg_dict.get(
            'body')) if msg_dict.get('body') else ''
        default.update(custom_values)
        return super(CrmHelpdesk, self).message_new(msg_dict, custom_values=defaults)
