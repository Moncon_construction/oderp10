# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo import tools


class CrmHelpdeskReport(models.Model):

    _name = "crm.helpdesk.report"
    _description = "Helpdesk report after Sales Services"
    _auto = False

    name = fields.Char(string='Query', required=True)
    date = fields.Datetime(string='Date', readonly=True)
    user_id = fields.Many2one('res.users', string='User', readonly=True)
    section_id = fields.Many2one(
        'crm.case.section', string='Section', readonly=True)
    request = fields.Char(string='Request', readonly=True)
    nbr = fields.Integer(string='Query number', readonly=True)
    state = fields.Selection([('draft', 'Draft'),
                              ('open', 'Open'),
                              ('cancel', 'Cancelled'),
                              ('done', 'Closed'),
                              ('pending', 'Pending')], string='Status', default='draft', readonly=True)
    delay_close = fields.Float(string='Delay to Close', digits=(
        16, 2), readonly=True, group_operator="avg")
    partner_id = fields.Many2one(
        'res.partner', string='Partner', readonly=True)
    company_id = fields.Many2one(
        'res.company', string='Company', readonly=True)
    date_deadline = fields.Date(string='Deadline', index=True)
    priority = fields.Selection([('5', 'Lowest'),
                                 ('4', 'Low'),
                                 ('3', 'Normal'),
                                 ('2', 'High'),
                                 ('1', 'Highest')], string='Priority')
    channel_id = fields.Many2one('crm.tracking.medium', string='Channel')
    planned_cost = fields.Float(string='Planned Costs')
    create_date = fields.Datetime(
        string='Creation Date', readonly=True, index=True)
    date_closed = fields.Datetime(
        string='Close Date', readonly=True, index=True)
    delay_expected = fields.Float(string='Overpassed Deadline', digits=(
        16, 2), readonly=True, group_operator="avg")
    email = fields.Integer(string='Emails', size=128, readonly=True)

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self._cr, 'crm_helpdesk_report')
        self._cr.execute("""
            CREATE OR REPLACE VIEW crm_helpdesk_report as(
                select
                    min(c.id) as id,
                    c.date as date,
                    c.create_date as create_date ,
                    c.date_closed as date_closed,
                    c.state as state,
                    c.user_id as user_id,
                    c.section_id as section_id,
                    c.partner_id as partner_id,
                    c.company_id as company_id,
                    c.priority as priority,
                    c.date_deadline as date_deadline,
                    c.channel_id as channel_id,
                    c.planned_cost as planned_cost,
                    c.name as name,
                    count(*) as nbr,
                    extract('epoch' from (c.date_closed-c.create_date))/(3600*24) as  delay_close,
                    (SELECT count(id) FROM mail_message WHERE model='crm.helpdesk' AND res_id=c.id) AS email,
                    abs(avg(extract('epoch' from (c.date_deadline - c.date_closed)))/(3600*24)) as delay_expected
                from
                    crm_helpdesk c
                -- left join res_partner as rp on rp.id = c.partner_id
                where c.active = 'true'
                group by c.date,c.name,\
                     c.state, c.user_id,c.section_id,c.priority,\
                     c.partner_id,c.company_id,c.date_deadline,c.create_date,c.date,c.date_closed,\
                     c.channel_id,c.planned_cost,c.id
            )""")
