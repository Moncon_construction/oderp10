# -*- coding: utf-8 -*-
{
    'name': "Дуудлага бүртгэл",
    'version': '1.0',
    'depends': ['crm', 'l10n_mn_crm_helpdesk', 'calendar'],
    'author': 'Asterisk Technologies LLC',
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
    Description texts
    """,
    'data': [
        'wizard/crm_phonecall_to_phonecall_view.xml',
        'views/crm_phonecall_view.xml',
        'security/ir.model.access.csv',
        'views/crm_phonecall_data.xml',
        'views/crm_phonecall_view.xml',
        'views/crm_case_categ_view.xml',
        'wizard/crm_phonecall_to_meeting_view.xml',
        'report/crm_phonecall_report_view.xml'
    ],
}
