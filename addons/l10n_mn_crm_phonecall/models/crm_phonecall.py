# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT
from datetime import datetime
from odoo.exceptions import UserError
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"


class CrmCaseCateg(models.Model):
    """ Category of Case """
    _name = "crm.case.categ"
    _description = "Category of Case"
    name = fields.Char(string='Name', required=True)
    team_id = fields.Many2one('crm.team', oldname='section_id', string='Sales Team')
    object_id = fields.Many2one('ir.model', string='Object Name')


class CrmPhonecall(models.Model):
    """ Model for CRM phonecalls """
    _name = "crm.phonecall"
    _description = "Phonecall"
    _order = "id desc"
    _inherit = ['mail.thread']
    team_id = fields.Many2one('crm.team', string='Sales Team', index=True, help='Sales team to which Case belongs to.', oldname='section_id')
    user_id = fields.Many2one(
        'res.users', string='Responsible', default=lambda self: self.env.user)
    categ_id = fields.Many2one('crm.case.categ', string='Category')
    partner_id = fields.Many2one('res.partner', string='Partner')
    company_id = fields.Many2one('res.company', string='Company')
    description = fields.Text(string='Description')
    state = fields.Selection(
        [('open', 'New'),
         ('cancel', 'Cancelled'),
         ('pending', 'Pending'),
         ('done', 'Gone')
         ], string='Status', default='open', readonly=True, track_visibility='onchange',
        help='The status is set to Confirmed, when a case is created.\n'
             'When the call is over, the status is set to Held.\n'
             'If the callis not applicable anymore, the status can be set to Cancelled.')
    email_from = fields.Char(string='Email', size=128, help="These people will receive email.")
    date_open =  fields.Datetime(string='Opened', readonly=True)
    name = fields.Char(string='Call Summary', required=True)
    duration = fields.Float(string='Duration', help='Duration in minutes and seconds.')
    partner_phone = fields.Char(string='Phone')
    partner_mobile = fields.Char(string='Mobile')
    priority = fields.Selection([('0', 'Low'), ('1', 'Normal'), ('2', 'High')], string='Priority', default='1')
    date_closed = fields.Datetime(string='Closed', readonly=True)
    date = fields.Datetime(string='Date', default=fields.Datetime.now)
    opportunity_id = fields.Many2one('crm.lead', string='Lead/Opportunity')
    calendar_id = fields.Many2one('calendar.event', string='Event', ondelete='cascade')
    partner_ids = fields.Many2many('res.partner', 'crm_phonecall_res_partner_rel', string='Partners')
    create_date = fields.Datetime(string='Creation Date' , readonly=True)

    @api.multi
    def _get_default_state(self, context=None):
        if context and context.get('default_state'):
            return context.get('default_state')
        return 'open'

    @api.onchange('partner_id')
    def on_change_partner_id(self):
        self.partner_phone = self.partner_id.phone
        self.partner_mobile = self.partner_id.mobile

    @api.multi
    def write(self, values):
        if values.get('state'):
            if values.get('state') == 'done':
                values['date_closed'] = fields.Datetime.now()
            elif values.get('state') == 'open':
                values['date_open'] = fields.Datetime.now()
                values['duration'] = 0.0
        return super(CrmPhonecall, self).write(values)

    @api.multi
    def compute_duration(self, values):
        if self.duration <= 0:
            duration = datetime.now() - datetime.strptime(self.date, DEFAULT_SERVER_DATETIME_FORMAT)
            values = {'duration': duration.seconds / Float(60)}
            self.write(self.id, values)
        return True

    @api.multi
    def schedule_another_phonecall(self,active_id, schedule_time, call_summary, user_id=False, team_id=False, categ_id=False, action='schedule'):
        model_data = self.env['ir.model.data']
        phonecall_dict = {}
        if not categ_id:
            try:
                res_id = model_data._get_id('crm.team', 'categ_phone2')
                categ_id = model_data.browse(self, res_id).res_id
            except ValueError:
                pass
        for call in self.browse(active_id):
            if not team_id:
                team_id = self.team_id and self.team_id.id or False
            if not user_id:
                user_id = self.user_id and self.user_id.id or False
            if not schedule_time:
                schedule_time = self.date
            vals = {
                'name': call_summary,
                'user_id': user_id or False,
                'categ_id': categ_id or False,
                'description': call.description or False,
                'date': schedule_time,
                'team_id': team_id or False,
                'partner_id': call.partner_id and call.partner_id.id or False,
                'partner_phone': call.partner_phone,
                'partner_mobile': call.partner_mobile,
                'priority': call.priority,
                'opportunity_id': call.opportunity_id and call.opportunity_id.id or False,
            }
            new_id = self.create(vals)
            if action == 'log':
                self.write(self, [new_id.id], {'state': 'done'})
            phonecall_dict[call.id] = new_id.id
        return phonecall_dict

    @api.multi
    def _call_create_partner(self, phonecall):
        partner = self.env['res.partner']
        partner_id = partner.create(self, {
            'name': phonecall.name,
            'user_id': phonecall.user_id.id,
            'comment': phonecall.description,
            'address': []
        })
        return partner_id

    @api.onchange('opportunity_id')
    def onchange_opportunity(self):
        self.partner_phone = self.opportunity_id.phone
        self.partner_mobile = self.opportunity_id.mobile
        self.partner_id = self.opportunity_id.partner_id

    @api.multi
    def _call_set_partner(self, partner_id):
        write_res = self.write(self, {'partner_id': partner_id})
        self._call_set_partner_send_note(self)
        return write_res

    @api.multi
    def _call_create_partner_address(self, phonecall, partner_id):
        address = self.env['res.partner']
        return address.create(self, {
            'parent_id': partner_id,
            'name': phonecall.name,
            'phone': phonecall.partner_phone,
        })

    @api.multi
    def handle_partner_assignation(self, action='create', partner_id=False):
        partner_ids = {}
        force_partner_id = partner_id
        for call in self.browse(self):
            if action == 'create':
                partner_id = force_partner_id or self._call_create_partner(
                    self, call)
                self._call_create_partner_address(self, call, partner_id)
            self._call_set_partner(self, [call.id], partner_id)
            partner_ids[call.id] = partner_id
        return partner_ids

    @api.multi
    def redirect_phonecall_view(self, phonecall_id):
        model_data = self.env['ir.model.data']
        tree_view = model_data.get_object_reference('l10n_mn_crm_phonecall', 'crm_case_phone_tree_view')
        form_view = model_data.get_object_reference('l10n_mn_crm_phonecall', 'crm_case_phone_form_view')
        search_view = model_data.get_object_reference('l10n_mn_crm_phonecall', 'view_crm_case_phonecalls_filter')
        value = {
            'name': ('Phone Call'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'crm.phonecall',
            'res_id': phonecall_id,
            'views': [(form_view and form_view[1] or False, 'form'), (tree_view and tree_view[1] or False, 'tree'), (False, 'calendar')],
            'type': 'ir.actions.act_window',
            'search_view_id': search_view and search_view[1] or False
        }
        return value

    @api.multi
    def convert_opportunity(self, opportunity_summary=False, partner_id=False, planned_revenue=0.0, probability=0.0):
        partner = self.env['res.partner']
        opportunity = self.env['crm.lead']
        opportunity_dict = {}
        default_contact = False
        for call in self:
            if not partner_id:
                partner_id = call.partner_id and call.partner_id.id or False
            else:
                address_id = partner_id.address_get([partner_id])
                if address_id:
                    default_contact = partner.browse(address_id)
            opportunity_id = opportunity.create({
                        'name': opportunity_summary or call.name,
                        'planned_revenue': planned_revenue,
                        'probability': probability,
                        'partner_id': partner_id or False,
                        'mobile': default_contact and default_contact.mobile,
                        'team_id': call.team_id and call.team_id.id or False,
                        'description': call.description or False,
                        'priority': call.priority,
                        'type': 'opportunity',
                        'phone': call.partner_phone or False,
                        'email_from': default_contact and default_contact.email
                        })
            vals = {
                'partner_id': partner_id,
                'opportunity_id': opportunity_id.id,
                'state': 'done'
            }
            self.write(vals)
            opportunity_dict[call.id] = opportunity_id
        return opportunity_dict

    @api.multi
    def action_make_meeting(self):
        self.ensure_one()
        action = self.env.ref('calendar.action_calendar_event').read()[0]
        partner_ids = [self.env['res.users'].browse().partner_id.id]
        print'partner_ids:', partner_ids
        if self.partner_id:
            partner_ids.append(self.partner_id.id)
        action['context'] = {
            'phonecall_id': self.id,
            'partner_ids': partner_ids,
            'user_id': self._uid,
            'email_from': self.email_from,
            'name': self.name,
        }
        return action

    @api.multi
    def action_button_convert2opportunity(self):
        if len(self) != 1:
            raise UserError(('Warning!'), (
                'It\'s only possible to convert one phonecall at a time.'))
        opportunity_dict = self.convert_opportunity()
        for dict_cont in opportunity_dict:
            return opportunity_dict[dict_cont].redirect_opportunity_view()

    @api.multi
    def _call_set_partner_send_note(self):
        return self.message_post(self, body=_("Partner has been <b>created</b>."))