# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo import tools

AVAILABLE_STATES = [
    ('draft', 'Draft'),
    ('open', 'Todo'),
    ('cancel', 'Cancelled'),
    ('done', 'Held'),
    ('pending', 'Pending')
]


class CrmPhonecallReport(models.Model):
    """ Phone calls by user and section """

    _name = "crm.phonecall.report"
    _description = "Phone calls by user and section"
    _auto = False

    user_id = fields.Many2one('res.users', strinig='User', readonly=True)
    team_id = fields.Many2one('crm.case.section', strinig='Team', oldname='section_id', readonly=True)
    priority = fields.Selection([('0','Low'), ('1','Normal'), ('2','High')], strinig='Priority')
    nbr = fields.Integer(strinig='# of Cases', readonly=True)
    state = fields.Selection(AVAILABLE_STATES, strinig='Status', readonly=True)
    create_date = fields.Datetime(strinig='Create Date', readonly=True, index=True)
    delay_close = fields.Float(strinig='Delay to close', digits=(16,2),readonly=True, group_operator="avg",help="Number of Days to close the case")
    duration = fields.Float(strinig='Duration', digits=(16,2),readonly=True, group_operator="avg")
    delay_open = fields.Float(strinig='Delay to open',digits=(16,2),readonly=True, group_operator="avg",help="Number of Days to open the case")
    categ_id = fields.Many2one('crm.case.categ', strinig='Category', domain="[('team_id','=',team_id),('object_id.model', '=', 'crm.phonecall')]")
    partner_id = fields.Many2one('res.partner', strinig='Partner' , readonly=True)
    company_id = fields.Many2one('res.company', strinig='Company', readonly=True)
    opening_date = fields.Datetime(strinig='Opening Date', readonly=True, index=True)
    date_closed = fields.Datetime(strinig='Close Date', readonly=True, index=True)

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self._cr, 'crm_phonecall_report')
        self._cr.execute("""
            CREATE OR REPLACE VIEW crm_phonecall_report as (
                select
                    id,
                    c.date_open as opening_date,
                    c.date_closed as date_closed,
                    c.state,
                    c.user_id,
                    c.team_id,
                    c.categ_id,
                    c.partner_id,
                    c.duration,
                    c.company_id,
                    c.priority,
                    1 as nbr,
                    c.create_date as create_date,
                    extract('epoch' from (c.date_closed-c.create_date))/(3600*24) as  delay_close,
                    extract('epoch' from (c.date_open-c.create_date))/(3600*24) as  delay_open
                from
                    crm_phonecall c
            )""")
