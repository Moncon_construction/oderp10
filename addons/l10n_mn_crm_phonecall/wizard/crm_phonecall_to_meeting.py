# -*- coding: utf-8 -*-
from odoo import api, models


class CrmPhonecall2meeting(models.Model):
    """ Phonecall to Meeting """

    _name = 'crm.phonecall2meeting'
    _description = 'Phonecall To Meeting'

    @api.multi
    def action_cancel(self):
        return {'type': 'ir.actions.act_window_close'}

    @api.multi
    def action_make_meeting(self):
        self.ensure_one()
        action = self.env.ref('calendar.action_calendar_event').read()[0]
        partner_ids = self.env.user.partner_id.ids
        if self.partner_id:
            partner_ids.append(self.partner_id.id)
        action['context'] = {
            'phonecall_id': self.id,
            'partner_ids': partner_ids,
            'user_id': self._uid,
            'email_from': self.email_from,
            'name': self.name,
        }
        return action
