# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.exceptions import UserError

import time


class CrmPhonecall2phonecall(models.TransientModel):
    _name = 'crm.phonecall2phonecall'
    _description = 'Phonecall To Phonecall'

    name = fields.Char(string='Call summary', delegate=True, required=True, index=True)
    user_id = fields.Many2one('res.users', string="Assign To")
    contact_name = fields.Char(string='Contact')
    phone = fields.Char(string='Phone')
    categ_id = fields.Many2one('crm.case.categ', string='Category', domain="['|',('team_id','=',False),('team_id','=',team_id)]")
    date = fields.Datetime(string='Date')
    team_id = fields.Many2one('crm.team', string='Sales Team', oldname='section_id')
    action = fields.Selection([('schedule', 'Schedule a call'), ('log', 'Log a call')], string='Action', required=True)
    partner_id = fields.Many2one('res.partner', string="Partner")
    note = fields.Text(string='Note')

    @api.multi
    def action_cancel(self):
        return {'type': 'ir.actions.act_window_close'}

    @api.multi
    def action_schedule(self):
        context = dict(self.env.context or {})
        phonecall = self.env['crm.phonecall']
        active_id = context and context.get('active_ids', [])
        phocall_ids = phonecall.schedule_another_phonecall(active_id,self.date, self.name, \
                    self.user_id and self.user_id.id or False, \
                    self.team_id and self.team_id.id or False, \
                    self.categ_id and self.categ_id.id or False, self.action)
        return phonecall.redirect_phonecall_view(phocall_ids[active_id[0]])

    @api.model
    def default_get(self, fields):
        """
        Дуудлага бүртгэл дээрх мэдээллийг авна. 
        
        """
        res = super(CrmPhonecall2phonecall, self).default_get(fields)
        context = dict(self._context or {})        
        record_id = context.get('active_id', False) or False
        res.update({'action': 'schedule', 'date': time.strftime('%Y-%m-%d %H:%M:%S')})
        if record_id:
            phonecall = self.env.get('crm.phonecall').browse(record_id)

            categ_id = False
            data_obj = self.env.get('ir.model.data')
            try:
                res_id = data_obj._get_id('crm_team', 'categ_phone2')
                categ_id = data_obj.browse(res_id).res_id
            except ValueError:
                pass

            if 'name' in fields:
                res.update({'name': phonecall.name})
            if 'user_id' in fields:
                res.update({'user_id': phonecall.user_id and phonecall.user_id.id or False})
            if 'date' in fields:
                res.update({'date': False})
            if 'team_id' in fields:
                res.update({'team_id': phonecall.team_id and phonecall.team_id.id or False})
            if 'categ_id' in fields:
                res.update({'categ_id': categ_id})
            if 'partner_id' in fields:
                res.update({'partner_id': phonecall.partner_id and phonecall.partner_id.id or False})
        return res
