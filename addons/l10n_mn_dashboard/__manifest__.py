# -*- coding: utf-8 -*-
# Part of OdERP. See LICENSE file for full copyright and licensing details
{

    'name' : 'Mongolian Dashboard',
    'version' : '1.0',
    'author' : 'Asterisk Technologies LLC',
    'category' : 'Dashboard',
    'description' : """
Dashboard

""",
    'website': 'http://www.asterisk-tech.mn',
    'images' : [''],
    'depends' : ['base', 'web'],
    'data':[
    ],
    'installable': True,
    'auto_install': False,
}