openerp.l10n_mn_dashboard = function(instance) {
    var QWeb = instance.web.qweb;
    var _t = instance.web._t;

    instance.l10n_mn_dashboard.MNO_DASHBOARD = instance.web.form.FormWidget.extend(instance.web.form.ReinitializeWidgetMixin, {
        events: {
            "click .oe_mno_dash_see": "go_to",

        },
        init: function() {
            // Pie Chart
            $("head").append('<link rel="stylesheet" href="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/styles/jqx.base.css" type="text/css" />');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxcore.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxdraw.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxchart.core.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxdata.js"></script>');
            // Grid
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxscrollbar.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxbuttons.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxmenu.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxgrid.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxgrid.selection.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxgrid.columnsresize.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxgrid.pager.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxlistbox.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxdropdownlist.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxdata.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxdatatable.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/scripts/demos.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxdata.export.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxdatetimeinput.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxcalendar.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxtooltip.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/globalization/globalize.js"></script>');
            

            $("head").append('<link rel="stylesheet" href="l10n_mn_dashboard/static/src/jqwidgets/datepicker/daterangepicker.css" type="text/css" />');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/datepicker/moment.min.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/datepicker/jquery.daterangepicker.js"></script>');


            this._super.apply(this, arguments);
            var self = this
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; 
            var yyyy = today.getFullYear();
            this.set({
                date_from: yyyy+'-'+mm+'-01',
                date_to: yyyy+'-'+mm+'-'+dd,
                full_datetodate: yyyy+'-'+mm+'-01 to '+yyyy+'-'+mm+'-'+dd
            });
            // this.set({
            //     date_from:'2015-09-01',
            //     date_to: '2015-09-20',
            //     full_datetodate: '2015-09-01 to 2015-09-10'
            // });
            this.updating = false;
            this.clicked_cause = [];
            this.defs = [];

            this.res_o2m_drop = new instance.web.DropMisordered();
            this.render_drop = new instance.web.DropMisordered();
            this.description_line = _t("/");
            
            // Original save function is overwritten in order to wait all running deferreds to be done before actually applying the save.
            this.view.original_save = _.bind(this.view.save, this.view);
            this.view.save = function(prepend_on_create){
                self.prepend_on_create = prepend_on_create;
                return $.when.apply($, self.defs).then(function(){
                    return self.view.original_save(self.prepend_on_create);
                });
            };
            
        },
        go_to: function(event) {
            dateto  = $('#datetodate').val().split(' ', 3);
            this.set({"date_from": dateto[0]});
            this.set({"date_to": dateto[2]});
            this.set({"full_datetodate":dateto})
        },
        initialize_field: function() {
            instance.web.form.ReinitializeWidgetMixin.initialize_field.call(this);
            var self = this;
            self.on("change:full_datetodate", self, self.initialize_content);
            // self.on("change:date_to", self, self.initialize_content);
        },
        initialize_content: function() {
            var self = this;
            this.destroy_content();
            var set_date_from = self.get('date_from');
            var set_date_to = self.get('date_to');
            var mining_plan_actual;
            var technic_detail;
            var money_detail;
            var budget_detail;
            var purchase_detail;
            var coal_detail;
            var salary_detail;
            return new instance.web.Model("dashboard.dashboard")
            .call("get_mining_plan_actual", [set_date_from,set_date_to,new instance.web.CompoundContext()])
            .then(function(min_detial){
                mining_plan_actual = min_detial
                
                return new instance.web.Model("dashboard.dashboard")
                .call("get_technic_detail", [set_date_from,set_date_to,new instance.web.CompoundContext()])
                .then(function(detail) {
                    technic_detail = detail;
                    return new instance.web.Model("dashboard.dashboard")
                    .call("executive_report_dashboar_cash", [set_date_from,set_date_to,new instance.web.CompoundContext()])
                    .then(function(mon_detail) {
                        money_detail = mon_detail;
                        return new instance.web.Model("dashboard.dashboard")
                        .call("get_budget_detail", [set_date_from,set_date_to,new instance.web.CompoundContext()])
                        .then(function(bud_detail) {
                            budget_detail = bud_detail;
                            return new instance.web.Model("dashboard.dashboard")
                            .call("get_purchase_data", [set_date_from,set_date_to,new instance.web.CompoundContext()])
                            .then(function(pur_detail) {
                                purchase_detail = pur_detail;
                                return new instance.web.Model("dashboard.dashboard")
                                .call("get_coal_sales", [set_date_from,set_date_to,new instance.web.CompoundContext()])
                                .then(function(co_detail) {
                                    coal_detail = co_detail;
                                    return new instance.web.Model("dashboard.dashboard")
                                    .call("get_salary_detail", [set_date_from,set_date_to,new instance.web.CompoundContext()])
                                    .then(function(sal_detail) {
                                        salary_detail = sal_detail;
                                    });
                                });
                            });
                        });
                    });
                });
                

            }).then(function(res){
                self.mining_plan_actual = mining_plan_actual;
                self.technic_detail = technic_detail;
                self.money_detail = money_detail;
                self.budget_detail = budget_detail;
                self.purchase_detail = purchase_detail;
                self.coal_detail = coal_detail;
                self.salary_detail = salary_detail;
                self.display_data();
            });
            
        },
        destroy_content: function() {
            if (this.dfm) {
                this.dfm.destroy();
                this.dfm = undefined;
            }
        },
        get_datatable: function(prod, m3_cost, motohour_cost){
            var mineral = "";
            if (prod.project_id==8) {mineral = "Элс, м3"} else {mineral="Нүүрс, тн"};
            var data = [{
                        first_col: 'Уулын цул, м3', 
                        plan: prod.plan_m3, 
                        actual: prod.actual_m3, 
                        zuruu: (prod.actual_m3-prod.plan_m3),
                        perc: (prod.actual_m3*100)/prod.plan_m3,
                    },
                    {
                        first_col: mineral, 
                        plan: prod.mineral_plan, 
                        actual: prod.mineral_actual, 
                        zuruu: (prod.mineral_actual-prod.mineral_plan),
                        perc: '',
                    },
                    {
                        first_col: "1 м3 өртөг, ₮", 
                        plan: '', 
                        actual: m3_cost, 
                        zuruu: '',
                        perc: '',
                    },
                    ];
            
            var source =
            {
                localData: data,
                dataType: "array"
            };
            
            var dataAdapter = new $.jqx.dataAdapter(source);
            var settings_datatable= {
                width: 350,
                // height: 120,
                source: dataAdapter,
                aggregatesHeight: 70,
                enableHover: false,
                autoRowHeight: true,
                columns: [
                      {
                          text: '', align: 'center', dataField: 'first_col', width:100,
                      },
                      {
                          text: 'Plan', align: 'center', dataField: 'plan', cellsformat: 'f', width: 65,
                                                },
                      {
                          text: 'Actual', align: 'center', dataField: 'actual', cellsformat: 'f', width: 65, 
                            cellsRenderer: function (row, column, value, rowData) {
                                if (row==2 ){
                                    console.log(value)
                                    if (value!='Infinity')
                                        return '<span style="font-weight: bold; margin-bottom: 8px; float: left; ">' + value + '₮</span>';
                                    else
                                        return '<span style="font-weight: bold; margin-bottom: 8px; float: left; ">0 ₮</span>';
                                }else
                                    return '<span style=" margin-bottom: 8px; float: left; ">'+value+'</span>';
                            },
                      },
                      {
                          text: 'Var', align: 'center', dataField: 'zuruu', cellsformat: 'f', width: 65,
                      },
                      {
                          text: '%', align: 'center', dataField: 'perc',
                          cellsRenderer: function (row, column, value, rowData) {
                            var div='';
                                if (row==0){
                                    div = "<div id=sparklineContainer" + prod.project_id + " style='margin: 0px; margin-bottom: 0px; width: 100%; height: 25px;'></div>";
                                }
                              return div;
                          }
                      }
                ],
                rendering: function () {
                    if ($(".jqx-chart").length > 0) {
                        $(".jqx-chart").jqxChart('destroy');
                    }
                }                ,
                rendered: function () {
                    createPartialPie('#sparklineContainer' + prod.project_id, data[0].perc);
                }
            };

            function createPartialPie(selector, data)
            {
                var dataStatCounter =
                [
                    { Type: 'Гүйцэтгэл', Expense: 100-Math.round(data) },
                    { Type: 'Үлдэгдэл', Expense:  Math.round(data)},
                    
                ];
                var settings = {
                    title: '',
                    description: '',
                    enableAnimations: true,
                    showLegend: false,
                    showBorderLine: false,
                    source: dataStatCounter,
                    padding: { left: 0, top: -45, right: 0, bottom: 0 },
                    showToolTips: true,
                    seriesGroups:
                        [
                            {
                                type: 'pie',
                                showLabels: true,
                                series:
                                    [
                                        { 
                                            dataField: 'Expense',
                                            displayText: 'Type',
                                            showLabels: true,
                                            labelRadius: 18,
                                            labelLinesEnabled: true,
                                            labelLinesAngles: true,
                                            labelsAutoRotate: false,
                                            initialAngle: 0,
                                            radius: 23,
                                            minAngle: 0,
                                            maxAngle: 180,
                                            centerOffset: 0,
                                            offsetY: 70,
                                            formatFunction: function (value, itemIdx, serieIndex, groupIndex) {
                                                if (isNaN(value))
                                                    return value;
                                                if (itemIdx==1)
                                                    return value + '%';
                                                else
                                                    return '';
                                            },
                                            colorFunction: function (value, itemIndex, serie, group) { return (itemIndex == 0) ? 'Red':'GreenYellow' },
                                        }
                                    ]
                            }
                        ]
                };
                $(selector).jqxChart(settings);
            }; // createSparkline
            return settings_datatable;
        },
        get_chart_pie: function(bud ,prod, salary_amount, sum_expense){
            var dataStatCounter = []
            var color_data = ['coral','Crimson','DarkCyan','DarkGreen','Brown','DarkOrchid','Gold','Indigo','MediumSlateBlue','OrangeRed','Purple','Navy','LightCoral','MediumTurquoise','MediumSlateBlue','Navy','MediumTurquoise']
            // for (var i = 0; i < 15; i += 1){
            //     color_data
            // }
            other_amount = bud.other_amount+bud.communication_amount +bud.insurance_amount+bud.security_amount+bud.fuel_lubricant_amount+bud.running_amount;
            dataStatCounter =
                [
                    { Type: 'Шатахуун '+$.jqx.dataFormat.formatnumber(Math.round(bud.fuel_amount/1000000), 'f'), Expense: bud.fuel_amount },
                    { Type: 'Сэлбэг '+$.jqx.dataFormat.formatnumber(Math.round(bud.equipment_amount/1000000), 'f'), Expense: bud.equipment_amount },
                    { Type: 'Цалин '+$.jqx.dataFormat.formatnumber(Math.round(salary_amount/1000000), 'f'), Expense: salary_amount },
                    { Type: 'Өрөмдлөг '+$.jqx.dataFormat.formatnumber(Math.round(prod.drill/1000000), 'f'), Expense: prod.drill },
                    { Type: 'Тэсэлгээ '+$.jqx.dataFormat.formatnumber(Math.round(prod.blasted/1000000), 'f'), Expense: prod.blasted},
                    // { Type: 'Харуул хамгаалалт '+$.jqx.dataFormat.formatnumber(Math.round(bud.security_amount/1000000), 'f'), Expense: bud.security_amount},
                    { Type: 'Катеринг '+$.jqx.dataFormat.formatnumber(Math.round(bud.catering_amount/1000000), 'f'), Expense: bud.catering_amount },
                    { Type: 'Элэгдэл '+$.jqx.dataFormat.formatnumber(Math.round(bud.erosion_amount/1000000), 'f'), Expense: bud.erosion_amount },
                    // { Type: 'Ашиглалт '+$.jqx.dataFormat.formatnumber(Math.round(bud.running_amount/1000000), 'f'), Expense: bud.running_amount },
                    // { Type: 'ШТМ '+$.jqx.dataFormat.formatnumber(Math.round(bud.fuel_lubricant_amount/1000000), 'f'), Expense: bud.fuel_lubricant_amount },
                    { Type: 'Техник түрээс '+$.jqx.dataFormat.formatnumber(Math.round(bud.technic_rent_amount/1000000), 'f'), Expense: bud.technic_rent_amount },
                    // { Type: 'Зээлийн хүү '+$.jqx.dataFormat.formatnumber(Math.round(bud.loan_interest_amount/1000000), 'f'), Expense: bud.loan_interest_amount },
                    // { Type: 'С.Т хүү '+$.jqx.dataFormat.formatnumber(Math.round(bud.leasing_rate/1000000), 'f'), Expense: bud.leasing_rate },
                    // { Type: 'Даатгал '+$.jqx.dataFormat.formatnumber(Math.round(bud.insurance_amount/1000000), 'f'), Expense: bud.insurance_amount },
                    { Type: 'Хүүгийн зардал '+$.jqx.dataFormat.formatnumber(Math.round((bud.loan_interest_amount + bud.leasing_rate)/1000000), 'f'), Expense: (bud.loan_interest_amount + bud.leasing_rate) },
                    { Type: 'Бусад '+$.jqx.dataFormat.formatnumber(Math.round(other_amount/1000000), 'f'), Expense: other_amount },
                ];
            var sources =
            {
                datafields: [
                    { name: 'Expense' },
                    { name: 'Type' }
                ],
                localdata: dataStatCounter
            };
            var dataAdapter = new $.jqx.dataAdapter(sources);
             var settings = {
                title: '',
                description: 'Нийт '+$.jqx.dataFormat.formatnumber(Math.round(sum_expense/1000000), 'f')+', сая ₮',
                enableAnimations: true,
                showLegend: true,
                legendLayout: { left: 190, top: 0, width: 150, height: 210, flow: 'horizontal' },
                padding: { left: 0, top: 0, right: 160, bottom: 30 },
                source: dataAdapter,
                colorScheme: 'scheme05',
                showToolTips: false,
                seriesGroups:
                    [
                        {
                            type: 'pie',
                            showLabels: true,
                            series:
                                [
                                    { 
                                        dataField: 'Expense',
                                        displayText: 'Type',
                                        labelRadius: 60,
                                        initialAngle: 5,
                                        radius: 55,
                                        centerOffset: 2,
                                        formatFunction: function (value) {
                                            if (isNaN(value))
                                                return value;
                                            return $.jqx.dataFormat.formatnumber(Math.round(value/1000000), 'f');
                                        },
                                        colorFunction: function (value, itemIndex) { 
                                            return color_data[itemIndex]; 
                                        },
                                    }
                                ]
                        }
                    ]
            };
            return settings;
        },
        get_chart_donut: function(index){
            var self = this;
            var data = [];
            var title = ''
            var money_detail = self.money_detail;
            var sum_expense_income = 0;
            if (money_detail[index].length>2){
                for (var i = 0; i < money_detail[index].length; i += 1){
                    if (index==0){
                        title='Мөнгөн Зарлага';
                        data[i] = {Type: money_detail[index][i].project_name, Expense: money_detail[index][i].expense };
                        sum_expense_income += money_detail[index][i].expense;
                    }
                    if (index==1){
                        title='Мөнгөн Орлого';
                        data[i]={Type: money_detail[index][i].project_name, Expense: money_detail[index][i].income };
                        sum_expense_income += money_detail[index][i].income;
                    }
                    
                }
            }
            var sources =
            {
                datafields: [
                    { name: 'Type' },
                    { name: 'Expense' }
                ],
                localdata: data,
            };
            var dataAdapter = new $.jqx.dataAdapter(sources, { async: false, autoBind: true, loadError: function (xhr, status, error) { alert('Error loading  : ' + error); } });
            var settings = {
                title: '',
                description: title+' /'+$.jqx.dataFormat.formatnumber(Math.round(sum_expense_income/1000000), 'f')+', сая ₮/',
                enableAnimations: true,
                showLegend: false,
                source: data,
                colorScheme: 'scheme5',
                showToolTips: false,
                seriesGroups:
                    [
                        {

                            type: 'donut',
                            showLabels: true,
                            series:
                                [
                                    {
                                        dataField: 'Expense',
                                        displayText: 'Type',
                                        labelRadius: 70,
                                        initialAngle: 10,
                                        radius: 100,
                                        innerRadius: 40,
                                        centerOffset: 5,
                                        formatFunction: function (value, position) {
                                            return data[position].Type+'<br><br><br>'+$.jqx.dataFormat.formatnumber(Math.round(value/1000000), 'f');
                                        },
                                    }
                                ]
                        },
                        
                    ]
            };
            return settings;
        },
        get_chart_pie_partner: function(index){
            var self = this;
            var data = [];
            var title = '';
            var money_detail = self.money_detail;
            var sum_expense_income = 0;
            if (index==2){
                    title='Мөнгөн Өглөг Харилцагчаар';
                }
            if (index==3){
                    title='Мөнгөн Зарлага Харилцагчаар';
                }
            for (var i = 0; i < money_detail[index].length; i += 1){
                data[i] = {Type: money_detail[index][i].partner+' '+$.jqx.dataFormat.formatnumber(Math.round(money_detail[index][i].payable/1000000), 'f'), Expense: money_detail[index][i].payable };
                sum_expense_income += money_detail[index][i].payable;
            }
            var sources =
            {
                datafields: [
                    { name: 'Expense' },
                    { name: 'Type' }
                ],
                localdata: data
            };
            var dataAdapter = new $.jqx.dataAdapter(sources);
            var settings = {
                title: '',
                description: title+' /'+$.jqx.dataFormat.formatnumber(Math.round(sum_expense_income/1000000), 'f')+', сая ₮/',
                enableAnimations: true,
                showLegend: true,
                legendLayout: { left: 0, top: 250, width: 350, height: 250, flow: 'vertical' },
                padding: { left: 0, top: 0, right: 0, bottom: 270 },
                source: dataAdapter,
                colorScheme: 'scheme25',
                showToolTips: false,
                seriesGroups:
                    [
                        {
                            type: 'pie',
                            showLabels: true,
                            series:
                                [
                                    { 
                                        dataField: 'Expense',
                                        displayText: 'Type',
                                        labelRadius: 120,
                                        initialAngle: 1,
                                        radius: 95,
                                        centerOffset: 2,
                                        formatFunction: function (value) {
                                            if (isNaN(value))
                                                return value;
                                            return $.jqx.dataFormat.formatnumber(Math.round(value/1000000), 'f');
                                        },
                                        // labelRadius: 80,
                                        // initialAngle: 5,
                                        // radius: 100,
                                        // centerOffset: 0,
                                        // formatFunction: function (value, position) {
                                        //     return $.jqx.dataFormat.formatnumber(Math.round(value/1000000), 'f');
                                        //     // if (-20<=(value/1000000) && (value/1000000)<=10){ return '';}
                                        //     // return data[position].Type+'<br><br><br>'+$.jqx.dataFormat.formatnumber(Math.round(value/1000000), 'f');
                                        // },
                                    }
                                ]
                        }
                    ]
            };
            return settings;
        },
        get_chart_negative_bar: function(income, expense, project_name, une){
            var sampleData = [
                { ashig: income-expense, expense: expense, income: income }];
            // minValue = income-expense;
            // if (minValue>0){
            //     minValue = 0
            // };
            var settings = {
                title: '',
                description: une,
                showLegend: true,
                enableAnimations: true,
                padding: { left: 20, top: 5, right: 20, bottom: 5 },
                titlePadding: { left: 90, top: 0, right: 0, bottom: 10 },
                source: sampleData,
                showToolTips: false,
                valueAxis:
                {
                    // minValue: minValue,
                    labels: {visible: true}, 
                    // unitInterval: 500,
                    title: { visible: false },
                },
                 xAxis:
                {
                    labels: {visible: false}, 
                },
                seriesGroups:
                    [
                        {
                            type: 'column',
                            columnsGapPercent: 10,
                            // seriesGapPercent: 20,
                            // columnsTopWidthPercent: 100,
                            // columnsBottomWidthPercent: 40,
                            series: [
                                    { dataField: 'income', displayText: 'Орлого', labels: {
                                            visible: true,
                                            verticalAlignment: 'top',
                                            offset: { x: 0, y: -20 }},
                                            colorFunction: function (value, itemIndex, serie, group) { return  'dodgerblue'; },
                                            formatFunction: function (value, position) {return $.jqx.dataFormat.formatnumber(Math.round(value), 'f');},
                                    },
                                    { dataField: 'expense', displayText: 'Зардал', labels: {
                                            visible: true,
                                            verticalAlignment: 'top',
                                            offset: { x: 0, y: -20 }},
                                            colorFunction: function (value, itemIndex, serie, group) { return  'gold'; },
                                            formatFunction: function (value, position) {return $.jqx.dataFormat.formatnumber(Math.round(value), 'f');},
                                    },
                                    { dataField: 'ashig', displayText: 'Ашиг', labels: {
                                            visible: true,
                                            verticalAlignment: 'top',
                                            offset: { x: 0, y: -20 }},
                                            colorFunction: function (value, itemIndex, serie, group) { return (value < 0) ? 'red':'MediumSeaGreen'}, //'#AA4643' : '#0098EE'},
                                            formatFunction: function (value, position) {return $.jqx.dataFormat.formatnumber(Math.round(value), 'f');},
                                    },
                                ]
                        }
                    ]
            };
            return settings;
        },
        get_grid_purchase: function(pur){
            var data = [];
            for (var i = 0; i < pur.length; i += 1){
                data.push({
                    project_name: pur[i].department_name, 
                    ex_amount: pur[i].ex_amount,
                    ex_count: pur[i].ex_count,
                    po_count: pur[i].po_count,
                    pr_count: pur[i].pr_count
                    });
            }
            var source =
            {
                datafields: [
                    { name: 'project_name' },
                    { name: 'ex_amount', type: 'float' },
                    { name: 'ex_count', type: 'float'},
                    { name: 'po_count', type: 'float'},
                    { name: 'pr_count', type: 'float'},
                ],
                colorScheme: 'scheme05',
                localdata: data,
            };
            var dataAdapter = new $.jqx.dataAdapter(source);
            return {
                width: 1050,
                // height: 305,
                source: dataAdapter,
                // autorowheight: true,
                autoheight: true,
                keyboardnavigation: false,
                columns: [
                    { text: 'Төсөл', datafield: 'project_name', width:240 },
                    { text: 'Хангамжийн Захиалга PO тоо', datafield: 'po_count', cellsformat: 'f'},
                    { text: 'Захиалга биелэлтийн тоо', datafield: 'pr_count', cellsformat: 'f'},
                    { text: 'Зарлагдсан тоо', datafield: 'ex_count', cellsformat: 'f'},
                    { text: 'Нийт үнийн дүн', datafield: 'ex_amount', cellsformat: 'f'},
                ]
            };
        },
        
        get_chart_repair: function(tech){
            var sampleData = [];
            var series_data = [];
            var tech_obj = {}
            for (var i = 0; i < tech.datas.length; i += 1){
                // tech_obj['technic'] = tech.datas[i].stopped_time;
                sampleData.push({technic: tech.datas[i].technic_name, stopped_time:tech.datas[i].stopped_time})
                // series_data.push({
                //     dataField: 'technic', 
                //     displayText: tech.datas[i].technic_name, 
                //     labels: {visible: true }}//, verticallAlignment: 'right',offset: { x: 200, y: 0 }} 
                // });
            };
            // sampleData.push(tech_obj);
            var settings = {
                title: '',
                description: tech.project_name,
                enableAnimations: false,
                showToolTips: true,
                showLegend: false,
                // legendLayout: { left: 0, top: 100, width: 150, height: 120, flow: 'horizontal' },
                padding: { left: 0, top: 0, right: 0, bottom: 20 },
                titlePadding: { left: 0, top: 10, right: 0, bottom: 10 },
                source: sampleData,
                xAxis:
                {
                    dataField: 'technic',
                    labels: { visible: true,
                        angle:90,
                        horizontalAlignment: 'right',
                        verticalAlignment: 'center',
                    },
                    unitInterval: 1,
                },
                valueAxis:
                {
                    flip: true,
                    unitInterval: 200,
                    minValue: 0,
                    labels: { visible: false },
                    tickMarks: { color: '#BCBCBC' }
                },
                colorScheme: 'scheme24',
                seriesGroups:
                    [{
                            type: 'column',
                            columnsGapPercent: 100,
                            orientation: 'horizontal',
                            series: [{ 
                                dataField: 'stopped_time', 
                                displayText: 'Зогсолт', 
                                labels:{visible:true},
                                colorFunction: function (value, itemIndex, serie, group) {
                                    switch (itemIndex) {
                                        case 0: return '#880000';
                                        case 1: return '#bd1d02';
                                        case 2: return '#d7310c';
                                        case 3: return '#e54c0f';
                                        case 4: return '#ff6511';
                                    }
                                },
                                formatFunction: function (value, itemIndex) {
                                    return $.jqx.dataFormat.formatnumber(Math.round(value), 'f')
                                },
                                toolTipFormatFunction: function (value, itemIndex) {
                                        return '<DIV style="text-align:left; width: 150px;"><b>Техник:  </b>'+tech.datas[itemIndex].technic_name+
                                        '<br><b>Зогсолтын цаг: </b>'+$.jqx.dataFormat.formatnumber(Math.round(value), 'f')+'<br><br></DIV>';
                                },
                            }]
                    }]
            };
            return settings;
        },
        get_chart_coal: function(index){
            var self = this;
            var coal_detail = self.coal_detail;
            sampleData = [];
            for (var i = 0; i < coal_detail[index].plan_actual.length; i += 1){
                new_date = new Date(coal_detail[index].plan_actual[i].date);
                sampleData.push({
                    date: new_date, 
                    plan_tn: coal_detail[index].plan_actual[i].plan_tn, 
                    actual_tn: coal_detail[index].plan_actual[i].actual_tn});
            };
            var plan_tn = $.jqx.dataFormat.formatnumber(Math.round(coal_detail[index].plan_tn), 'f');
            var actual_tn = $.jqx.dataFormat.formatnumber(Math.round(coal_detail[index].actual_tn), 'f');
            var sales_tn = $.jqx.dataFormat.formatnumber(Math.round(coal_detail[index].sales_tn), 'f');
            var before_balance_tn = $.jqx.dataFormat.formatnumber(Math.round(coal_detail[index].before_balance_tn), 'f');
            var zuruu = $.jqx.dataFormat.formatnumber(Math.round(coal_detail[index].actual_tn-coal_detail[index].plan_tn), 'f');
            var measure_zuruu = $.jqx.dataFormat.formatnumber(Math.round(coal_detail[index].balance_tn-(coal_detail[index].before_balance_tn+coal_detail[index].actual_tn-coal_detail[index].sales_tn)), 'f');
            var balance_tn = $.jqx.dataFormat.formatnumber(Math.round(coal_detail[index].balance_tn), 'f');
            var settings = {
                title: coal_detail[index].material_name+' тонн',
                description: 'Төлөвлөгөө: '+plan_tn+'<br> Гүйцэтгэл  : '+actual_tn+'<br>  Зөрүү      : '+zuruu,
                enableAnimations: true,
                showLegend: false,
                source: sampleData,
                colorScheme: 'scheme17',
                borderLineColor: '#888888',
                backgroundColor: '#E9EAED',
                showToolTips: true,
                xAxis:
                {
                    description:  
                    'Эхний үлдэгдэл  : '+before_balance_tn+
                    '<br>Олборлосон      : '+actual_tn+
                    '<br>Экспортлосон    : '+sales_tn+
                    '<br>Үлдэгдэл        : '+balance_tn+
                    '<br>Хэмжилтийн зөрүү: '+measure_zuruu,
                    dataField: 'date',
                    type: 'date',
                    labels:{visible: false,},
                    baseUnit: 'day',
                    unitInterval: 3,
                    // tickMarksColor: '#888888'
                },
                valueAxis:
                {
                    visible: true,
                    minValue: 0,
                    tickMarks: {color: '#888888'},
                    gridLines: {color: '#888888'},
                    axisSize: 'auto'
                },

                seriesGroups:
                    [{
                            type: 'spline',
                            series: [
                                    { dataField: 'plan_tn', displayText: 'Төлөвлөгөө', opacity: 0.5, labels: {visible: false, },opacity: 1.0, lineWidth: 3, dashStyle: '3,3',
                                    colorFunction: function (value, itemIndex, serie, group) { return '#DF7401' },
                                    toolTipFormatFunction: function (value, itemIndex) {
                                        return '<DIV style="text-align:left; width: 130px;"><b>Төлөвлөгөө: </b>'+$.jqx.dataFormat.formatnumber(Math.round(value), 'f')+
                                        '<br><b>Өдөр:</b>'+coal_detail[index].plan_actual[itemIndex].date+'<br><br></DIV>';
                                    }
                                    }                                    
                                ]
                        },
                        {
                            type: 'column',
                            columnsGapPercent: 40,
                            seriesGapPercent: 10,
                            series: [
                                    { dataField: 'actual_tn', displayText: 'Гүйцэтгэл' , labels: {visible:false},
                                    colorFunction: function (value, itemIndex, serie, group) {
                                        value_plan = 0
                                        if (value){
                                            value_plan = sampleData[itemIndex].plan_tn;
                                        }
                                        return (value > value_plan) ? '#568f56' : '#AA4643' },
                                    toolTipFormatFunction: function (value, itemIndex) {
                                        return '<DIV style="text-align:left; width: 120px;"><b>Гүйцэтгэл: </b>'+$.jqx.dataFormat.formatnumber(Math.round(value), 'f')+
                                        '<br><b>Өдөр:</b>'+coal_detail[index].plan_actual[itemIndex].date+'<br><br></DIV>';
                                    }
                                },]
                            
                    }]
            };
            return settings;
        },
        display_data: function() {
            var self = this;
            self.$el.html(QWeb.render("l10n_mn_dashboard.MNO_DASHBOARD", {widget: self}));
            
            var date_from = self.get('date_from');
            var date_to = self.get('date_to');
            try{
                $('#datetodate').dateRangePicker({
                    separator: ' to ',
                    startOfWeek: 'monday',
                    batchMode: true,
                });
                $('#datetodate').data('dateRangePicker').setDateRange(date_from, date_to, true);
                
                var mining_production = self.mining_plan_actual;
                var technic_production = self.technic_detail;
                var budget_detail = self.budget_detail;
                var purchase_detail = self.purchase_detail;
                var salary_detail = self.salary_detail;
                for (var i = 0; i < mining_production.length; i += 1){
                    $('#project_fixed_name'+(i+1)).html('<h3>'+mining_production[i].project_name+'</h3>');
                    sum_expense = budget_detail[i].fuel_amount  + 
                    budget_detail[i].equipment_amount + salary_detail[i].salary_amount + 
                    budget_detail[i].other_amount  + budget_detail[i].catering_amount + budget_detail[i].security_amount+
                    budget_detail[i].erosion_amount + budget_detail[i].running_amount + budget_detail[i].fuel_lubricant_amount+
                    budget_detail[i].technic_rent_amount+budget_detail[i].insurance_amount+budget_detail[i].loan_interest_amount+
                    budget_detail[i].leasing_rate + budget_detail[i].communication_amount+
                    mining_production[i].blasted + mining_production[i].drill;
                    sum_soil =  mining_production[i].actual_m3;
                    console.log(salary_detail[i].salary_amount);
                    $('#chart_project'+(i+1)).jqxChart(self.get_chart_pie(budget_detail[i], mining_production[i], salary_detail[i].salary_amount, sum_expense));
                    sum_expense = sum_expense*-1;
                    $('#grid_project'+(i+1)).jqxDataTable(self.get_datatable(mining_production[i], Math.round(sum_expense/sum_soil), Math.round(sum_expense/mining_production[i].motohour_time)));
                    une = 0;
                    usd_currency = ''
                    project_income = 0.0
                    switch (budget_detail[i].project_id) {
                        // Баруунцанхи
                        case 10:
                            une = 6800/1.1111111111
                            project_income = (mining_production[i].actual_m3*une)/1000000
                            une = $.jqx.dataFormat.formatnumber(Math.round(une), 'f')
                            break;
                        // Хүрэншанд
                        case 9:
                            une = (mining_production[i].ksp_une*mining_production[i].avg_currency)/1.1111111111
                            usd_currency = '$'+mining_production[i].ksp_une+'*₮'+Math.round(mining_production[i].avg_currency,2)
                            project_income = (mining_production[i].actual_m3*une)/1000000
                            une = $.jqx.dataFormat.formatnumber(Math.round(une), 'f')
                            break;
                        // Баянгол
                        case 8:
                            une = ''
                            project_income = mining_production[i].metal_income/1000000
                            break; 
                    }
                    $('#chart_project_income'+(i+1)).jqxChart(self.get_chart_negative_bar(project_income,sum_expense/1000000, budget_detail[i].project_name, '/ '+une+' ₮/ '));
                   
                }

                $('#chart_money1').jqxChart(self.get_chart_donut(1));
                $('#chart_money2').jqxChart(self.get_chart_donut(0));
                $('#chart_partner1').jqxChart(self.get_chart_pie_partner(2));
                $('#chart_partner2').jqxChart(self.get_chart_pie_partner(3));

                $('#chart_coal_sales1').jqxChart(self.get_chart_coal(0));
                $('#chart_coal_sales2').jqxChart(self.get_chart_coal(1));
                $('#chart_coal_sales3').jqxChart(self.get_chart_coal(2));
                var sampleData = [];
                for (var i = 0; i < technic_production.length; i += 1){
                    str = technic_production[i].project_name+' p';
                    sampleData.push({project: str, plan_hour:technic_production[i].plan_hour, actual_hour: technic_production[i].actual_hour, repair_hour:technic_production[i].repair_hour}); 
                    $('#chart_repair'+(i+1)).jqxChart(self.get_chart_repair(technic_production[i]));
                }
                var settings = {
                    title: "Техникийн ажиласан цаг",
                    description: "",
                    enableAnimations: true,
                    showLegend: true,
                    legendLayout: { left: 0, top: 170, width: 300, height: 20, flow: 'vertical' },
                    padding: { left: 0, top: 0, right: 0, bottom: 20 },
                    source: sampleData,
                    colorScheme: 'scheme17',
                    borderLineColor: '#888888',
                    showToolTips: false,
                    xAxis:
                    {
                        dataField: 'project',
                        showGridLines: true,
                    },
                    valueAxis:
                    {
                        visible: true,
                        minValue: 0,
                        tickMarks: {color: '#888888'},
                        gridLines: {color: '#888888'},
                        axisSize: 'auto'
                    },

                    seriesGroups:
                        [
                            {
                                type: 'splinearea',
                                series: [
                                        { dataField: 'plan_hour', displayText: 'Төлөвлөгөө', opacity: 0.5,
                                        labels:
                                        {
                                            visible: true,
                                            backgroundColor: '#FEFEFE',
                                            backgroundOpacity: 0.2,
                                            borderColor: '#7FC4EF',
                                            borderOpacity: 0.7,
                                            padding: { left: 5, right: 5, top: 0, bottom: 0 }
                                        } ,
                                        formatFunction: function (value) {return $.jqx.dataFormat.formatnumber(Math.round(value), 'f');},
                                    }
                                        
                                    ]

                            },
                            {
                                type: 'column',
                                columnsGapPercent: 100,
                                seriesGapPercent: 5,
                                series: [
                                        { dataField: 'actual_hour', displayText: 'Гүйцэтгэл' , labels: {visible:true},
                                        formatFunction: function (value) {return $.jqx.dataFormat.formatnumber(Math.round(value), 'f');},},
                                        { dataField: 'repair_hour', displayText: 'Засварын цаг' , labels: {visible:true},
                                    formatFunction: function (value) {return $.jqx.dataFormat.formatnumber(Math.round(value), 'f');},}
                                    ]
                                
                            }
                        ]
                };
                $('#chart_technic1').jqxChart(settings);
                $('#grid_purchase').jqxGrid(self.get_grid_purchase(purchase_detail));
                if(navigator.userAgent.indexOf("Firefox") != -1 ) 
                {
                     
                }else{
                    $("#report tr.fixed_row").next().toggle();
                    $("#report tr.fixed_row").next().next().toggle();
                }
                $("#report tr.fixed_row").click(function(){
                    $(this).next("tr").toggle();
                    $(this).next("tr").next("tr").toggle();

                });
            }
            catch(err) {
                console.log(err.message);
            }
        },

    });
    instance.web.form.custom_widgets.add('mno_dashboard', 'instance.l10n_mn_dashboard.MNO_DASHBOARD');

};
