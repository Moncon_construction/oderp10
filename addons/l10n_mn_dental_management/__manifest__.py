# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.


{
    "name": "Mongolian Dental Management",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "category": "Mongolian Modules",
    "summary": "Шүдний эмнэлэг",
    'description': """
        - Цаг олголт
        - Эмчилгээ, оношийн бүртгэл
        - Өвчтөний картын бүртгэл
        - Эмчилгээний бүртгэлийг шүдний гадаргуй бүрээр бүртгэх
        - Зөвлөгөөний драфт
        - Шүдний дэлгэрэнгүй бүртгэл
        - Эмчилгээний орлогын тайлан
        - Хэцүү өвчтөний бүртгэл
        - Диспансерын хяналтын систем
        - Эмийн жор
    """,
    "website": "http://asterisk-tech.mn",
    'depends': [
        'base',
        'mail',
        'product',
        'sale',
        'account',
        'l10n_mn_contacts',
        'l10n_mn_product',
        'l10n_mn_report',
        'l10n_mn_hr',
        'l10n_mn_account_period',
        'l10n_mn_crm_helpdesk',
        'l10n_mn_base',
        'l10n_mn_hr_roster',
        'l10n_mn_allergy',
    ],
    "data": [
        'security/dental_security.xml',
        'security/ir.model.access.csv',

        'views/hr_department_views.xml',
        'views/hr_job_views.xml',
        'views/res_users_views.xml',

        'views/dental_customer_views.xml',

        'data/tooth.xml',
        'data/tooth_surface.xml',
        'data/diagnoses.xml',
        'data/dental_customer_sequence.xml',

        'views/dental_tooth_views.xml',
        'views/dental_service_views.xml',
        'views/dental_diagnosis_views.xml',
        'views/dental_test_views.xml',
        'views/dental_advice_views.xml',
        'views/dental_customer_attribute_views.xml',
        'views/dental_medicine_recipe_views.xml',
        'views/dental_sent_document_views.xml',

        'views/dental_appointment_views.xml',
        'views/dental_schedule_views.xml',
        'views/allergy_views.xml',

        'views/sale_order_views.xml',
        'views/schedule_reminder_views.xml',


        'report/print_advice.xml',
        'report/dental_permission_sheet.xml',
        'report/print_sent_document_paperformat.xml',
        'report/print_sent_document.xml',
        'report/print_dental_appointment.xml',

        'views/menu.xml',
    ],
    'qweb': ['static/src/xml/*.xml'],
}
