# -*- coding: utf-8 -*

from odoo.addons.bus.controllers.main import BusController
from odoo.http import request


class ScheduleBusController(BusController):
    # --------------------------
    # Extends BUS Controller Poll
    # --------------------------
    def _poll(self, dbname, channels, last, options):
        if request.session.uid:
            channels = list(channels)
            channels.append((request.db, 'schedule.signal', request.env.user.sudo().partner_id.id))
        return super(ScheduleBusController, self)._poll(dbname, channels, last, options)
