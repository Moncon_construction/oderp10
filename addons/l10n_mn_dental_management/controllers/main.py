# -*- coding: utf-8 -*-

# Part of Odoo. See LICENSE file for full copyright and licensing details.

import json

from odoo import http
from odoo.http import request


class DentalController(http.Controller):

    @http.route(['/dental'], type='json', auth="public", methods=['POST'], website=True)
    def get_tooth_surface(self, **kwargs):
        app_id = kwargs.get('app_id')
        request.cr.execute("select surface, not_exist, is_healthy, is_decay from dental_inspection where surface is not null and appointment = '" + app_id + "'")
        data = {}
        for surface in request.cr.dictfetchall():
            surface_id = surface['surface']
            not_exist = surface['not_exist']
            is_healthy = surface['is_healthy']
            is_decay = surface['is_decay']
            tooth_surface = request.env['dental.tooth.surface'].search([('id', '=', surface_id)])
            data.setdefault(tooth_surface.tooth.name + '-' + tooth_surface.name, [])
            if tooth_surface.tooth.name + '-' + tooth_surface.name in data:
                data[tooth_surface.tooth.name + '-' + tooth_surface.name] = {
                    'not_exist': not_exist,
                    'is_healthy': is_healthy,
                    'is_decay': is_decay
                }
            else:
                data.update({tooth_surface.tooth.name + '-' + tooth_surface.name: {
                    'not_exist': not_exist,
                    'is_healthy': is_healthy,
                    'is_decay': is_decay
                }})
        request.cr.execute("select surface from dental_inspection_not_exist where surface is not null and appointment_id = '" + app_id + "'")
        for surface in request.cr.dictfetchall():
            surface_id = surface['surface']
            tooth_surface = request.env['dental.tooth.surface'].search([('id', '=', surface_id)])
            data[tooth_surface.tooth.name + '-' + tooth_surface.name] = {
                'not_exist': True,
                'is_healthy': False,
                'is_decay': False
            }
        json_data = json.dumps(data)
        return json_data

    @http.route(['/perio'], type='json', auth="public", methods=['POST'], website=True)
    def get_periodont_plaque(self, **kwargs):
        app_id = kwargs.get('app_id')
        request.cr.execute("select * from dental_periodont_plaque where appointment = '" + app_id + "'")
        data = {}
        for rec in request.cr.dictfetchall():
            tooth = request.env['dental.tooth'].browse(rec['tooth'])
            plaques = []
            if rec['plaque_b']:
                plaques.append('b')
            if rec['plaque_m']:
                plaques.append('m')
            if rec['plaque_p']:
                plaques.append('p')
            if rec['plaque_d']:
                plaques.append('d')
            if int(tooth.name) < 49:
                data.update({
                    tooth.name: {
                        'bop1': rec['bop1'],
                        'bop2': rec['bop2'],
                        'greyed_out': rec['greyed_out'],
                        'mobility': rec['mobility'],
                        'pd': {
                            '1': rec['pocket_depth1'],
                            '2': rec['pocket_depth2'],
                            '3': rec['pocket_depth3'],
                            '4': rec['pocket_depth4'],
                            '5': rec['pocket_depth5'],
                            '6': rec['pocket_depth6'],
                        },
                        'plaque': plaques
                    }
                })
            else:
                data.update({
                    tooth.name: {
                        'plaque': plaques,
                        'greyed_out': rec['greyed_out'],
                    }
                })
        request.cr.execute("select milk_teeth, permanent_teeth from dental_appointment where id = " + str(app_id))
        result = request.cr.dictfetchall()
        data['milk'] = result[0]['milk_teeth']
        data['permanent'] = result[0]['permanent_teeth']
        json_data = json.dumps(data)
        return json_data

    # Function used, in RPC to check every 5 minutes, if notification to do for an schedule or not
    @http.route('/schedule/notify', type='json', auth="user")
    def notify(self):
        return request.env['dental.alarm_manager'].get_next_notif()

    @http.route('/schedule/notify_seen', type='json', auth="user")
    def notify_ack(self, schedule_id):
        if schedule_id:
            request.env['dental.schedule'].browse(schedule_id).write({
                'doctor_seen_after_arrived': True
            })
