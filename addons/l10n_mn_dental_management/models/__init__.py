# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

import hr_department
import hr_job
import res_users
import dental_customer
import dental_tooth
import dental_service
import product_attribute
import dental_diagnosis
import dental_test
import dental_inspection
import dental_periodont_plaque
import dental_treatment
import dental_advice
import dental_medicine_recipe
import dental_sent_document

import sale_order

import dental_appointment
import dental_schedule
import allergy
import dental_alarm_manager
import dental_inspection_not_exist
import hr_employee
