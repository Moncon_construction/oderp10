# -*- coding: utf-8 -*-

from datetime import datetime

from odoo import models, fields, api, exceptions, _
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT


class Allergy(models.Model):
    _inherit = 'allergy'

    customer_count = fields.Integer(compute="_count_customer")
    med_recipe_count = fields.Integer(compute="_count_med_recipe")
    
    @api.multi
    def _count_customer(self):
        # Уг харшил бүртгэгдсэн үйлчлүүлэгчдийн тоог олох
        for obj in self:
            self._cr.execute("""
                SELECT COUNT(customer_id) AS count FROM customer_to_allergy WHERE allergy_id IN (%s)
            """ % str(self.ids).strip('[]'))
            
            result = self._cr.dictfetchall()
            obj.customer_count = result[0]['count'] if result else 0
            
    @api.multi
    def _count_med_recipe(self):
        # Уг харшил бүртгэгдсэн эмйин жоруудын тоог олох
        for obj in self:
            self._cr.execute("""
                SELECT COUNT(recipe_id) AS count FROM medicine_recipe_to_allergy WHERE allergy_id IN (%s)
            """ % str(self.ids).strip('[]'))
            
            result = self._cr.dictfetchall()
            obj.med_recipe_count = result[0]['count'] if result else 0
            
    @api.multi
    def show_linked_customers(self):
        # Уг харшил бүртгэгдсэн үйлчлүүлэгчид рүү СМАРТ ТОВЧноос үсрэхэд ашиглах функц
        self._cr.execute("""
            SELECT customer_id FROM customer_to_allergy WHERE allergy_id IN (%s)
        """ % str(self.ids).strip('[]'))
        
        customer_ids = [result['customer_id'] for result in self._cr.dictfetchall()]

        return {
            'name': _('Dental Customer'),
            'res_model': 'dental.customer',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'views': [
                (self.env.ref('l10n_mn_dental_management.dental_customer_view_tree').id, 'tree'), 
                (False, 'form'),
            ],
            'target': 'current',
            'flags': {'search_view': True, 'action_buttons': True},
            'domain': [('id', 'in', customer_ids)]
        }
        
    @api.multi
    def show_linked_med_recipes(self):
        # Уг харшил бүртгэгдсэн эмийн жорууд рүү СМАРТ ТОВЧноос үсрэхэд ашиглах функц
        self._cr.execute("""
            SELECT recipe_id FROM medicine_recipe_to_allergy WHERE allergy_id IN (%s)
        """ % str(self.ids).strip('[]'))
        
        recipe_ids = [result['recipe_id'] for result in self._cr.dictfetchall()]

        return {
            'name': _('Medicine Recipe'),
            'res_model': 'dental.medicine.recipe',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'views': [
                (self.env.ref('l10n_mn_dental_management.view_dental_medicine_recipe_tree').id, 'tree'), 
                (False, 'form'),
            ],
            'target': 'current',
            'flags': {'search_view': True, 'action_buttons': True},
            'domain': [('id', 'in', recipe_ids)]
        }
        



    

