# -*- coding: utf-8 -*-

from odoo import models, fields, api

class DentalAdvice(models.Model):
    _name = 'dental.advice'

    title = fields.Char(string='Title')
    content = fields.Html(string='Content')

    @api.multi
    def print_advice(self):
        self.ensure_one()
        return self.env['report'].get_action(self, 'l10n_mn_dental_management.print_advice')
