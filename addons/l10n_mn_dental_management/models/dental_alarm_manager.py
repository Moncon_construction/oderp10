# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

from odoo import fields, models
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.tools.translate import _


class AlarmManager(models.AbstractModel):

    _name = 'dental.alarm_manager'

    def get_next_notif(self):
        partner = self.env.user.partner_id
        all_notif = []

        if not partner:
            return []
        
        current_emp = self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)
        if current_emp:
            for schedule in self.env['dental.schedule'].search([('dentist', '=', current_emp.sudo().id), ('arrived', '=', True), ('doctor_seen_after_arrived', '=', False), ('can_seen', '=', True)]):
                delta = 1
                notify_at = datetime.now() + relativedelta(seconds=+delta)
                
                day = str(get_day_like_display(str(schedule.time_from)[0:19], schedule.dentist.user_id))[0:10]
                time_from = str(get_day_like_display(str(schedule.time_from)[0:19], schedule.dentist.user_id))[11:16]
                time_to = str(get_day_like_display(str(schedule.time_to)[0:19], schedule.dentist.user_id))[11:16]
                message = u"%s:\t%s (%s~%s)" %(schedule.customer_name, day, time_from, time_to)
                
                all_notif.append({
                    'event_id': schedule.id,
                    'customer_id': schedule.customer.id,
                    'appointment_id': schedule.appointment.id,
                    'title': _("Customer arrived !!!"),
                    'message': message,
                    'timer': delta,
                    'notify_at': fields.Datetime.to_string(notify_at),
                })

        return all_notif



     