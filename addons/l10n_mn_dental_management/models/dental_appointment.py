# -*- coding: utf-8 -*-

from odoo import models, fields, api, _, exceptions
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as dsdtf
from datetime import timedelta
from datetime import datetime
from odoo.addons.l10n_mn_web.models.time_helper import *

class DentalAppointment(models.Model):
    _name = 'dental.appointment'
    _description = 'Dental Appointment'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _order = 'start_time desc'

    @api.one
    @api.depends('start_time')
    def _compute_editable(self):
        editable = True
        if self.start_time and self.state == 'done':
            start_dt = datetime.strptime(self.start_time, DEFAULT_SERVER_DATETIME_FORMAT)
            end_dt = datetime.now()
            diff = end_dt - start_dt
            if diff.total_seconds() > 86400:  # 24 цагаас их хугацаа өнгөрсөн бол засахгүй
                editable = False
        self.editable = editable

    def _default_dentist(self):
        employee = self.env['hr.employee'].search([('user_id', '=', self._uid)])
        if len(employee) > 1:
            employee = employee[0]
        return employee.id

    @api.one
    def _compute_uncompleted_plans(self):
        plans = self.env['dental.appointment.plan'].search([('customer', '=', self.customer.id), ('appointment', '!=', self.id), ('state', '=', 'planned')])
        self.uncompleted_plans = plans

    company = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id.id)
    department = fields.Many2one('hr.department', 'Department', domain=[('is_dental_department', '=', True)], states={'done': [('readonly', True)]})
    dentist = fields.Many2one('hr.employee', 'Dentist', domain=[('job_id.is_dentist', '=', True)], default=_default_dentist, states={'done': [('readonly', True)]} )
    customer = fields.Many2one('dental.customer', 'Customer', required=True, states={'done': [('readonly', True)]})
    customer_attributes = fields.Many2many(related='customer.attributes', string='Customer Attributes')
    state = fields.Selection([('draft', 'Draft'), ('done', 'Done')], string='Status', copy=False, default='draft', track_visibility='onchange')
    start_time = fields.Datetime('Start Time', required=True, states={'done': [('readonly', True)]})
    end_time = fields.Datetime('End date', required=True, states={'done': [('readonly', True)]})
    full_examination = fields.Boolean('Oral Examination', default=False, states={'done': [('readonly', True)]})
    periodont_plaque = fields.Boolean('Periodont and Plaque', default=False, states={'done': [('readonly', True)]})
    tests = fields.One2many('dental.test.request', 'appointment', string='Dental Test Requests', states={'done': [('readonly', True)]})
    inspections = fields.One2many('dental.inspection', 'appointment', string='Inspections', states={'done': [('readonly', True)]})
    periodont_plaques = fields.One2many('dental.periodont.plaque', 'appointment', string='Periodont and Plaques', states={'done': [('readonly', True)]})
    treatments = fields.One2many('dental.treatment', 'appointment', string='Treatments', states={'done': [('readonly', True)]})
    advices = fields.Many2many('dental.advice', string='Advices')
    description = fields.Text('Description', track_visibility='onchange', default='S: \nO: \nA: \nP: ')
    plan = fields.Text('Plan', track_visibility='onchange')
    plan_state = fields.Selection([
        ('planned', 'Planned'),
        ('changed', 'Changed'),
        ('completed', 'Completed')
    ], string='Status', copy=False, required=True, default='planned')
    uncompleted_plans = fields.One2many('dental.appointment.plan', 'appointment', string='Uncompleted Plans', compute="_compute_uncompleted_plans")
    editable = fields.Boolean(compute='_compute_editable', string='Can edit?')
    in_control = fields.Boolean('In Dispenser Control', related='customer.in_control')
    sale_order = fields.Many2one('sale.order', 'Sale Order')
    allergy_ids = fields.Many2many(related='customer.allergy_ids', string='Allergy')
    medical_recipes = fields.Many2many('dental.medicine.recipe', 'apntmnt_to_medical_recipe', 'apntmnt_id', 'medical_recipe_id', string='Medicine Recipe')
    jaw_teeth_deformity_state = fields.Selection([
        ('normal', 'Normal Not observed yet'),
        ('need_observed', 'Need to be observed'),
        ('if_you_want_to_improve,', 'If you want to improve your beauty'),
        ('required_future,', 'Treatment is required in the future'),
        ('required_near_future,', 'Treatment is required in the near future')
    ], string='Teeth jaw deformity', copy=False)
    milk_teeth = fields.Boolean('Milk teeth')
    permanent_teeth = fields.Boolean('Permanent teeth')

    @api.multi
    def name_get(self):
        result = []
        for app in self:
            name = ''
            if app.start_time:
                name = app.start_time
            if app.customer:
                name = '%s - %s' % (name, app.customer.name)
            result.append((app.id, name))
        return result

    @api.onchange('dentist')
    def onchange_dentist(self):
        if self.dentist and self.dentist.department_id:
            self.department = self.dentist.department_id

    @api.multi
    def write(self, vals):
        res = super(DentalAppointment, self).write(vals)

        for appointment in self:
            # Update plan
            if 'plan' in vals or 'plan_state' in vals:
                plan = self.env['dental.appointment.plan'].search([('appointment', '=', appointment.id)], limit=1)

                if appointment.plan:
                    if not plan:
                        self.env['dental.appointment.plan'].create({
                            'appointment': appointment.id,
                            'customer': appointment.customer.id,
                            'dentist': appointment.dentist.id,
                            'plan': appointment.plan,
                            'state': appointment.plan_state
                        })
                    else:
                        plan.write({
                            'plan': appointment.plan,
                            'state': appointment.plan_state
                        })
                elif plan:
                    plan.unlink()

        return res

    def have_common_data(self, list1, list2):
        # 2 жагсаалтад давхардсан утга байгаа эсэхийг шалгах
        for x in list1: 
            for y in list2: 
                if x == y: 
                    return True  
                      
        return False

    @api.model
    def get_start_time(self, department_id=False, dentist_id=False, date=None):
        # string to date
        if isinstance(date, unicode):
            date = datetime.strptime(date, dsdf)

        # get start and end time
        start_time = date
        end_time = start_time + timedelta(days=1)

        last_schedule = self.env['dental.schedule'].search([
            ('time_from', '>', start_time.strftime(dsdtf)),
            ('time_from', '<', end_time.strftime(dsdtf)),
            ('department', '=', department_id.id),
            ('dentist', '=', dentist_id.id)], limit=1, order='time_to DESC')

        if len(last_schedule) > 0:
            return last_schedule.time_to
        else:
            # TODO: Get from shift
            return (start_time + timedelta(hours=1)).strftime(dsdtf)

    @api.model
    def get_end_time(self, department_id=False, dentist_id=False, date=None):
        return (datetime.strptime(
            self.get_start_time(department_id=department_id, dentist_id=dentist_id, date=date),
            dsdtf) + timedelta(minutes=30)).strftime(dsdtf)

    @api.multi
    def action_done(self):
        self.ensure_one()

        # get products
        products = []
        for treatment in self.treatments:
            for procedure_line in treatment.procedure_lines:
                products.append({
                    'product_id': procedure_line.procedure.product_variant_id.id,
                    'qty': procedure_line.qty
                })
                if procedure_line.procedure.recurrent == True:
                    end_time = self.end_time
                    day = procedure_line.procedure.min_duration_for_notify
                    next_date = datetime.strptime(end_time, '%Y-%m-%d %H:%M:%S') + timedelta(days=day)
                    note = str(get_day_like_display(end_time, self.env.user)) + ' өдрийн үзлэгээс хойш ' + str(
                        day) + ' өдрийн дараа ' + procedure_line.procedure.name + ' хийлгэх '
                    self.env['dental.schedule'].sudo().create({
                        'customer_name': self.customer.name,
                        'customer': self.customer.id,
                        'date': next_date,
                        'time_from': self.get_start_time(department_id=self.department, dentist_id=self.dentist,
                                                         date=next_date),
                        'time_to': self.get_end_time(department_id=self.department, dentist_id=self.dentist,
                                                     date=next_date),
                        'department': self.department.id,
                        'note': note,
                        'auto_create': True
                    })

            for product_line in treatment.product_lines:
                products.append({
                    'product_id': product_line.product.id,
                    'qty': product_line.qty
                })
        # check dental warehouse from user
        if not self.env.user.dental_warehouse:
            raise exceptions.ValidationError(_('Please configure dental warehouse for user.'))

        # create sale order
        so = self.env['sale.order'].sudo().create({
            'partner_id': self.customer.partner.id,
            'date_order': self.start_time,
            'user_id': self.dentist.user_id.id,
            'picking_policy': 'direct',
            'invoice_policy': 'to_product',
            'warehouse_id': self.env.user.dental_warehouse.id
        })

        # create lines
        for line in products:
            self.env['sale.order.line'].sudo().create({
                'order_id': so.id,
                'product_id': line['product_id'],
                'product_uom_qty': line['qty']
            })

        # set sale order
        self.sale_order = so

        # set state
        self.state = 'done'

        # set done for schedule
        self.env['dental.schedule'].search([('appointment', '=', self.id)]).write({'done': True})

    @api.multi
    def action_cancel(self):
        self.ensure_one()
        if (self.sudo().sale_order and self.sudo().sale_order.state == 'cancel') or not self.sudo().sale_order:
            self.state = 'draft'

            # set done for schedule
            self.env['dental.schedule'].search([('appointment', '=', self.id)]).write({'done': False})
        else:
            raise exceptions.ValidationError(_('You can only cancel a appointment, that has cancelled sale orders.'))

    @api.multi
    def unlink(self):
        for app in self:
            if app.state != 'draft':
                raise exceptions.ValidationError(_('%s is not draft. You can delete only draft appointments.') % app.name_get()[0][1])
        return super(DentalAppointment, self).unlink()

    @api.onchange('medical_recipes')
    def _is_customer_allergied_from_recipe(self):
        # Үзлэг дээр эмийн жор сонгосон байгаад тухайн үзлэгд харгалзах үйлчлүүлэгчийн харшлыг дараа нь жороос сонгосон бол алерт өгөх
        for obj in self:
            if obj.medical_recipes and obj.allergy_ids:
                for recipe in obj.medical_recipes:
                    if recipe.allergy_ids and self.have_common_data(recipe.allergy_ids, obj.allergy_ids):
                        raise exceptions.ValidationError(_(u'%s customer allergied from this recipe !!!') %(obj.customer and obj.customer.name or ''))
            
    
    @api.multi
    def print_appointment(self):
        return self.env['report'].get_action(self, 'l10n_mn_dental_management.report_dental_appointment')
    
class DentalAppointmentPlan(models.Model):
    _name = 'dental.appointment.plan'
    _description = 'Dental Appointment Plan'
    _order = 'create_date desc'

    appointment = fields.Many2one('dental.appointment', 'Dental Appointment')
    customer = fields.Many2one('dental.customer', string='Customer')
    dentist = fields.Many2one('hr.employee', string='Dentist')
    plan = fields.Text('Plan')
    state = fields.Selection([
        ('planned', 'Planned'),
        ('changed', 'Changed'),
        ('completed', 'Completed')
    ], string='Status', copy=False, default='planned')

    @api.multi
    def to_changed(self):
        self.write({'state': 'changed'})
        for plan in self:
            self.env.cr.execute("UPDATE dental_appointment SET plan_state = 'changed' WHERE id = %s" % plan.appointment.id)

    @api.multi
    def to_completed(self):
        self.write({'state': 'completed'})
        for plan in self:
            self.env.cr.execute("UPDATE dental_appointment SET plan_state = 'completed' WHERE id = %s" % plan.appointment.id)


class DentalCustomer(models.Model):
    _inherit = 'dental.customer'

    @api.one
    def _compute_uncompleted_plans(self):
        plans = self.env['dental.appointment.plan'].search([('customer', '=', self.id), ('state', 'in', ['planned', 'changed'])])
        self.uncompleted_plans = plans

    appointments = fields.One2many('dental.appointment', 'customer', string='Appointments')
    plans = fields.One2many('dental.appointment.plan', 'customer', string='Appointment Plans')
    uncompleted_plans = fields.One2many('dental.appointment.plan', 'customer', string='Uncompleted Plans', compute="_compute_uncompleted_plans")
    
    
