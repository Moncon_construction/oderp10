# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

from datetime import datetime

from odoo import models, fields, api, exceptions, _
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT

class DentalCustomer(models.Model):
    _name = 'dental.customer'
    _inherits = {
        'res.partner': 'partner',
    }

    partner = fields.Many2one('res.partner', string='Partner', ondelete='cascade', required=True)
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female')], 'Gender', required=True)
    age = fields.Integer('Age')
    code = fields.Char('Code', size=6)
    attributes = fields.Many2many('dental.customer.attribute', string='Customer Attributes')
    in_control = fields.Boolean('In Dispenser Control', default=False)
    allergy_ids = fields.Many2many('allergy', 'customer_to_allergy', 'customer_id', 'allergy_id', string='Allergy')
    
    @api.multi
    def name_get(self):
        result = []
        for customer in self:
            name = customer.name

            if customer.surname:
                name = '%s %s' % (customer.surname, name)

            if customer.code:
                name = '[%s] %s' % (customer.code, name)

            if customer.mobile:
                name = '%s - %s' % (name, customer.mobile)
            elif customer.phone:
                name = '%s - %s' % (name, customer.phone)

            result.append((customer.id, name))
        return result

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        domain = []
        if name:
            domain = ['|', '|', '|', '|', '|', ('name', operator, name), ('code', operator, name), ('phone', operator, name), ('mobile', operator, name), ('register', operator, name), ('birthday', operator, name.replace('.', '-'))]
        customers = self.search(domain + args, limit=limit)
        return customers.name_get()

    @api.model
    def create(self, vals):

        if 'code' not in vals:
            # get code
            vals['code'] = self.env['ir.sequence'].next_by_code('dental.customer') or ''

        customer = super(DentalCustomer, self).create(vals)

        # set reference
        customer.partner.ref = customer.code

        return customer

    @api.multi
    def write(self, vals):
        res = super(DentalCustomer, self).write(vals)
        # update partner category
        if 'attributes' in vals:
            for customer in self:
                customer.sudo().partner.category_id = customer.attributes.mapped('partner_category')
        return res

    @api.onchange('birthday')
    def onchange_birthday(self):
        if self.birthday:
            now = datetime.now()
            birthday = datetime.strptime(self.birthday, DEFAULT_SERVER_DATE_FORMAT)
            if now < birthday:
                raise exceptions.UserError(_('Please insert correct birthday'))
            else:
                self.age = ((now - birthday).days) / 365.25

    @api.multi
    def unlink(self):
        # get related partner
        related_partners = self.env['res.partner']
        for dc in self:
            related_partners = related_partners | dc.partner

        # call super
        res = super(DentalCustomer, self).unlink()

        # delete related partners
        related_partners.unlink()

        return res


class DentalCustomerAttribute(models.Model):
    _name = 'dental.customer.attribute'
    _inherits = {
        'res.partner.category': 'partner_category',
    }

    partner_category = fields.Many2one('res.partner.category', string='Partner category', ondelete='cascade', required=True)
    color_picker = fields.Selection([
        ('0', 'Хар'),
        ('1', 'Гүн ногоон'),
        ('2', 'Шар'),
        ('3', 'Улбар'),
        ('4', 'Улаан'),
        ('5', 'Үзмэн ягаан'),
        ('6', 'Хөх'),
        ('7', 'Цэнхэр'),
        ('8', 'Ногоон'),
        ('9', 'Ягаан')], string="Color")

    @api.model
    def create(self, vals):
        vals['color'] = vals['color_picker']
        return super(DentalCustomerAttribute, self).create(vals)

    @api.multi
    def write(self, vals):
        vals['color'] = vals.get('color_picker')
        return super(DentalCustomerAttribute, self).write(vals)

    @api.multi
    def unlink(self):
        self.partner_category.unlink()
        return super(DentalCustomerAttribute, self).unlink()

