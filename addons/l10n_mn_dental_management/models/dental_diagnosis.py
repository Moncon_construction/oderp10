# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api

class DentalDiagnosis(models.Model):

    _name = 'dental.diagnosis'

    name = fields.Char('Name', required=True)
    code = fields.Char('Code')
    is_general = fields.Boolean('Is General Diagnosis')
    general_diagnosis = fields.Many2one('dental.diagnosis', 'General Diagnosis', domain=[('is_general', '=', True)])
    service = fields.Many2one('dental.service', 'Dental Service')
    is_healthy = fields.Boolean('Is Healthy', default=False)
    not_exist = fields.Boolean('Not Exist', default=False)
    is_decay = fields.Boolean('Is Decay', default=False)
    is_stone = fields.Boolean('Is Stone', default=False)
    is_teethridge = fields.Boolean('Is Teethridge', default=False)
    level = fields.Selection([
        ('h', 'H'),
        ('c0', 'C0'),
        ('d', 'D'),
        ('f', 'F')
    ], string='Level', copy=False)

    @api.multi
    def name_get(self):
        result = []
        for diag in self:
            if diag.code:
                name = '%s - %s' % (diag.code, diag.name)
            else:
                name = diag.name
            result.append((diag.id, name))
        return result

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('name', operator, name), ('code', operator, name)]
        giagnoses = self.search(domain + args, limit=limit)
        return giagnoses.name_get()

    @api.onchange('general_diagnosis')
    def onchange_general_diagnosis(self):
        if self.general_diagnosis:
            self.service = self.general_diagnosis.service

    @api.multi
    def unlink(self):
        can_delete = self.filtered(lambda r: not r.is_healthy and not r.not_exist)
        return super(DentalDiagnosis, can_delete).unlink()
