# -*- coding: utf-8 -*-
from odoo import models, fields, api

class DentalInspection(models.Model):
    _name = 'dental.inspection'

    appointment = fields.Many2one('dental.appointment', 'Appointment')
    tooth = fields.Many2one('dental.tooth', 'Tooth')
    surface = fields.Many2one('dental.tooth.surface', 'Tooth Surface')
    not_exist = fields.Boolean('Not Exist', default=False)
    is_healthy = fields.Boolean('Is Healthy', default=False)
    is_decay = fields.Boolean('Is Decay', default=False)
    general_diagnosis = fields.Many2one('dental.diagnosis', 'General Diagnosis', domain=[('is_general', '=', True)])
    detailed_diagnosis = fields.Many2one('dental.diagnosis', 'Detailed Diagnosis', domain=[('is_general', '=', False)])
    description = fields.Text('Description')

    @api.model
    def create(self, vals):
        ins = super(DentalInspection, self).create(vals)
        if ins.general_diagnosis:
            ins.is_decay = ins.general_diagnosis.is_decay
        else:
            ins.is_decay = False
        return ins

    @api.multi
    def write(self, vals):
        if 'general_diagnosis' in vals:
            gen_diag = self.env['dental.diagnosis'].search([('id', '=', vals.get('general_diagnosis'))], limit=1)
            vals.update({'is_decay': gen_diag.is_decay})
        super(DentalInspection, self).write(vals)

    @api.onchange('general_diagnosis')
    def onchange_general_diagnosis(self):
        detailed_diagnoses = self.env['dental.diagnosis'].search([('is_general', '=', False)])
        if self.general_diagnosis:
            detailed_diagnoses = detailed_diagnoses.filtered(lambda r: r.general_diagnosis == self.general_diagnosis)
        self.detailed_diagnosis = False
        return {
            'domain': {
                'detailed_diagnosis': [('id', 'in', detailed_diagnoses.ids)]
            }
        }

    @api.model
    def toggle_not_exist(self, app_id, tooth_number):
        app_id = int(app_id)
        inspections = self.search([('appointment', '=', app_id), ('tooth.name', '=', tooth_number)])
        surfaces = self.env['dental.tooth.surface'].search([('tooth.name', '=', tooth_number)])
        if inspections:
            if not any(inspections.mapped('not_exist')):
                inspections.write({'not_exist': True, 'is_healthy': False})
                surfaces_create = surfaces.filtered(lambda r: r not in inspections.mapped('surface'))
                for surface in surfaces_create:
                    inspections |= self.create({
                        'appointment': app_id,
                        'tooth': surface.tooth.id,
                        'surface': surface.id,
                        'not_exist': True
                    })
            else:
                inspections.unlink()
                return False  # Арилгаж байгаа бол False буцаана
        else:
            for surface in surfaces:
                inspections |= self.create({
                    'appointment': int(app_id),
                    'tooth': surface.tooth.id,
                    'surface': surface.id,
                    'not_exist': True
                })

        # set diagnosis
        diag_not_exist = self.env['dental.diagnosis'].search([('not_exist', '=', True)], limit=1)
        inspections.write({
            'general_diagnosis': diag_not_exist.id,
            'detailed_diagnosis': False,
            'description': False
        })

        return True  # Нэмж байгаа бол True буцаана

    @api.model
    def toggle_healthy(self, app_id, tooth_number):
        app_id = int(app_id)
        inspections = self.search([('appointment', '=', app_id), ('tooth.name', '=', tooth_number)])
        surfaces = self.env['dental.tooth.surface'].search([('tooth.name', '=', tooth_number)])
        if inspections:
            if not any(inspections.mapped('is_healthy')):
                inspections.write({'is_healthy': True, 'not_exist': False})
                surfaces_create = surfaces.filtered(lambda r: r not in inspections.mapped('surface'))
                for surface in surfaces_create:
                    inspections |= self.create({
                        'appointment': app_id,
                        'tooth': surface.tooth.id,
                        'surface': surface.id,
                        'is_healthy': True
                    })
            else:
                inspections.unlink()
                return False  # Арилгаж байгаа бол False буцаана
        else:
            for surface in surfaces:
                inspections |= self.create({
                    'appointment': int(app_id),
                    'tooth': surface.tooth.id,
                    'surface': surface.id,
                    'is_healthy': True
                })

        # set diagnosis
        diag_healthy = self.env['dental.diagnosis'].search([('is_healthy', '=', True)], limit=1)
        inspections.write({
            'general_diagnosis': diag_healthy.id,
            'detailed_diagnosis': False,
            'description': False
        })

        return True  # Нэмж байгаа бол True буцаана


class RegisterDiagnosis(models.TransientModel):
    _name = 'dental.register.diagnosis'

    @api.model
    def _default_teeth(self):
        teeth = self.env['dental.tooth']
        list_surfaces = []
        if self.env.context.get('surfaces'):
            list_surfaces = list(self.env.context.get('surfaces'))
            for tooth_name in list_surfaces:
                tooth = self.env['dental.tooth'].search([('name', '=', tooth_name)])
                if tooth not in teeth:
                    teeth |= tooth
        return teeth and teeth.ids or False

    @api.model
    def _default_surfaces(self):
        surfaces = self.env['dental.tooth.surface']
        list_surfaces = []
        dict_surfaces = {}
        if self.env.context.get('surfaces'):
            list_surfaces = list(self.env.context.get('surfaces'))
            dict_surfaces = dict(self.env.context.get('surfaces'))
            for tooth in list_surfaces:
                surfaces |= self.env['dental.tooth.surface'].search([('tooth.name', '=', tooth), ('name', 'in', dict_surfaces[tooth])])
        return surfaces and surfaces.ids or False

    teeth = fields.Many2many('dental.tooth', default=_default_teeth)
    surfaces = fields.Many2many('dental.tooth.surface', string='Surfaces', default=_default_surfaces)
    general_diagnosis = fields.Many2one('dental.diagnosis', 'General Diagnosis', domain=[('is_general', '=', True), ('is_healthy', '=', False), ('not_exist', '=', False)])
    detailed_diagnosis = fields.Many2one('dental.diagnosis', 'Detailed Diagnosis', domain=[('is_general', '=', False)])
    description = fields.Text('Description')

    @api.onchange('general_diagnosis')
    def onchange_general_diagnosis(self):
        detailed_diagnoses = self.env['dental.diagnosis'].search([('is_general', '=', False)])
        if self.general_diagnosis:
            detailed_diagnoses = detailed_diagnoses.filtered(lambda r: r.general_diagnosis == self.general_diagnosis)
        self.detailed_diagnosis = False
        return {
            'domain': {
                'detailed_diagnosis': [('id', 'in', detailed_diagnoses.ids)]
            }
        }

    def insert_diagnosis(self):
        app_id = self.env.context.get('active_id')
        if app_id:
            app_id = int(app_id)

        self.calculate_diagnosis()

        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'dental.appointment',
            'res_id': app_id,
            'view_id': self.env.ref('l10n_mn_dental_management.dental_appointment_view_form').id,
            'context': self.env.context,
            'flags': {'initial_mode': 'edit'},
        }

    def calculate_diagnosis(self):
        app_id = self.env.context.get('active_id')
        if app_id:
            app_id = int(app_id)

        #  Онош сонгогдоогүй бол үзлэг үүсгэхгүй
        if not self.general_diagnosis and not self.detailed_diagnosis:
            return

        if self.surfaces or self.teeth:
            all_surfaces = self.surfaces
            if self.teeth and not self.env.context.get('single_surface'):
                all_surfaces = all_surfaces | self.env['dental.tooth.surface'].search([('tooth', 'in', self.teeth.ids)])

            new_surfaces = all_surfaces

            # update existing
            existing_inspections = self.env['dental.inspection'].search([('appointment', '=', app_id), ('surface', 'in', all_surfaces.ids)])
            for ins in existing_inspections:
                ins.general_diagnosis = self.general_diagnosis
                ins.detailed_diagnosis = self.detailed_diagnosis
                ins.description = self.description
                ins.is_healthy = False
                ins.not_exist = False

                new_surfaces -= ins.surface

            # create new diagnosis
            for new_surface in new_surfaces:
                ins_vals = {
                    'appointment': app_id,
                    'tooth': new_surface.tooth.id,
                    'surface': new_surface.id,
                    'general_diagnosis': self.general_diagnosis.id,
                    'detailed_diagnosis': self.detailed_diagnosis and self.detailed_diagnosis.id or False,
                    'description': self.description
                }
                self.env['dental.inspection'].create(ins_vals)
        else:
            # create new diagnosis
            ins_vals = {
                'appointment': app_id,
                'general_diagnosis': self.general_diagnosis.id,
                'detailed_diagnosis': self.detailed_diagnosis and self.detailed_diagnosis.id or False,
                'description': self.description
            }
            self.env['dental.inspection'].create(ins_vals)

