# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class DentalInspectionNotExist(models.Model):
    _name = 'dental.inspection.not.exist'

    appointment_id = fields.Many2one('dental.appointment', 'Appointment')
    tooth = fields.Many2one('dental.tooth', 'Tooth')
    surface = fields.Many2one('dental.tooth.surface', 'Tooth Surface')

    @api.model
    def toggle_not_exist(self, app_id, tooth_number):
        app_id = int(app_id)
        inspections = self.search([('appointment_id', '=', app_id), ('tooth.name', '=', tooth_number)])
        surfaces = self.env['dental.tooth.surface'].search([('tooth.name', '=', tooth_number)])
        if inspections:
            if not any(inspections):
                surfaces_create = surfaces.filtered(lambda r: r not in inspections.mapped('surface'))
                for surface in surfaces_create:
                    inspections |= self.create({
                        'appointment_id': app_id,
                        'tooth': surface.tooth.id,
                        'surface': surface.id,
                    })
            else:
                inspections.unlink()
                return False  # Арилгаж байгаа бол False буцаана
        else:
            for surface in surfaces:
                inspections |= self.create({
                    'appointment_id': int(app_id),
                    'tooth': surface.tooth.id,
                    'surface': surface.id,
                })
            self.env['dental.inspection'].search([('appointment', '=', app_id), ('tooth.name', '=', tooth_number)]).unlink()
        return True  # Нэмж байгаа бол True буцаана
