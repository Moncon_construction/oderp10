# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.tools.translate import _

class DentalMedicineRecipe(models.Model):
    _name = 'dental.medicine.recipe'

    medicine_name = fields.Char(string='Medicine name')
    size = fields.Char(string='Size')
    frequency = fields.Char(string='Frequency', help="This field describe frequency to use medicine in day")
    day = fields.Float(string='Day', help="This field describe days to use medicine")
    when = fields.Selection([
        ('before_food', 'Before Food'),
        ('with_food', 'With Food'),
        ('after_food', 'After Food'),
        ('morning', 'Morning'),
        ('evening', 'Evening')], string='When')
    allergy_ids = fields.Many2many('allergy', 'medicine_recipe_to_allergy', 'recipe_id', 'allergy_id', string='Allergy')
    appntmnt_ids = fields.Many2many('dental.appointment', 'apntmnt_to_medical_recipe', 'medical_recipe_id', 'apntmnt_id', string='Dental Appointment')
    appntmnt_count = fields.Integer(compute="_count_allergy")
    
    @api.multi
    def _count_allergy(self):
        for obj in self:
            self._cr.execute("""
                SELECT COUNT(allergy_id) AS count FROM medicine_recipe_to_allergy WHERE recipe_id IN (%s)
            """ % str(self.ids).strip('[]'))
            
            result = self._cr.dictfetchall()
            obj.allergy_count = result[0]['count'] if result else 0
            
    @api.multi
    def show_linked_appntmnts(self):
        self._cr.execute("""
            SELECT apntmnt_id FROM apntmnt_to_medical_recipe WHERE medical_recipe_id IN (%s)
        """ % str(self.ids).strip('[]'))
        
        apntmnt_ids = [result['apntmnt_id'] for result in self._cr.dictfetchall()]

        return {
            'name': _('Dental Appointment'),
            'res_model': 'dental.appointment',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree',
            'view_id': self.env.ref('l10n_mn_dental_management.dental_appointment_view_tree').id,
            'target': 'current',
            'flags': {'search_view': True, 'action_buttons': True},
            'domain': [('id', 'in', apntmnt_ids)]
        }
        
