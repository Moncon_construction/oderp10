# -*- coding: utf-8 -*-
from odoo import models, fields, api

class DentalPeriodontPlaque(models.Model):
    _name = 'dental.periodont.plaque'

    appointment = fields.Many2one('dental.appointment', 'Appointment')
    tooth = fields.Many2one('dental.tooth', 'Tooth')
    bop1 = fields.Boolean('BOP 1', default=False)
    bop2 = fields.Boolean('BOP 2', default=False)
    greyed_out = fields.Boolean('Have Tooth', default=False)
    mobility = fields.Char('Mobility', default='')
    pocket_depth1 = fields.Char('Pocket Depth 1', default='')
    pocket_depth2 = fields.Char('Pocket Depth 2', default='')
    pocket_depth3 = fields.Char('Pocket Depth 3', default='')
    pocket_depth4 = fields.Char('Pocket Depth 4', default='')
    pocket_depth5 = fields.Char('Pocket Depth 5', default='')
    pocket_depth6 = fields.Char('Pocket Depth 6', default='')
    plaque_b = fields.Boolean('Plaque b', default=False)
    plaque_m = fields.Boolean('Plaque m', default=False)
    plaque_p = fields.Boolean('Plaque p', default=False)
    plaque_d = fields.Boolean('Plaque d', default=False)

    @api.model
    def save_js(self, app_id, data):
        data = data[0]
        if not data:
            return

        # clear old values
        self.search([('appointment', '=', app_id)]).unlink()
        self.env['dental.appointment'].browse(app_id).milk_teeth = data['milk']
        self.env['dental.appointment'].browse(app_id).permanent_teeth = data['permanent']
        # create records according json values
        for i in range(11, 49):
            t_i = str(i)
            if t_i in data:
                tooth = self.env['dental.tooth'].search([('name', '=', t_i)], limit=1)
                self.create({
                    'appointment': app_id,
                    'tooth': tooth.id,
                    'bop1': data[t_i].get('bop1'),
                    'bop2': data[t_i].get('bop2'),
                    'greyed_out': data[t_i].get('greyed_out'),
                    'mobility': data[t_i].get('mobility'),
                    'pocket_depth1': data[t_i]['pd']['1'],
                    'pocket_depth2': data[t_i]['pd']['2'],
                    'pocket_depth3': data[t_i]['pd']['3'],
                    'pocket_depth4': data[t_i]['pd']['4'],
                    'pocket_depth5': data[t_i]['pd']['5'],
                    'pocket_depth6': data[t_i]['pd']['6'],
                    'plaque_b': 'b' in data[t_i]['plaque'],
                    'plaque_m': 'm' in data[t_i]['plaque'],
                    'plaque_p': 'p' in data[t_i]['plaque'],
                    'plaque_d': 'd' in data[t_i]['plaque']
                })

        for i in range(51, 86):
            t_i = str(i)
            if t_i in data:
                tooth = self.env['dental.tooth'].search([('name', '=', t_i)], limit=1)
                self.create({
                    'appointment': app_id,
                    'tooth': tooth.id,
                    'greyed_out': data[t_i].get('greyed_out'),
                    'plaque_b': 'b' in data[t_i]['plaque'],
                    'plaque_m': 'm' in data[t_i]['plaque'],
                    'plaque_p': 'p' in data[t_i]['plaque'],
                    'plaque_d': 'd' in data[t_i]['plaque']
                })
