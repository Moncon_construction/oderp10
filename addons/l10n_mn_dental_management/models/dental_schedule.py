# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

from datetime import datetime, timedelta
from time import strptime

import pytz

from odoo import _, api, fields, models, tools
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as dsdf
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as dsdtf
from dateutil.relativedelta import relativedelta


class DentalSchedule(models.Model):
    _name = 'dental.schedule'
    _rec_name = 'customer_name'
    _description = 'Dental Schedule'
    _order = 'arrived DESC, time_from DESC'

    @api.model
    def get_start_time(self, department_id=False, dentist_id=False, date=None):
        # string to date
        if isinstance(date, unicode):
            date = datetime.strptime(date, dsdf)

        # get start and end time
        start_time = date or datetime.today()
        end_time = start_time + timedelta(days=1)

        last_schedule = self.search([
            ('time_from', '>', start_time.strftime(dsdtf)),
            ('time_from', '<', end_time.strftime(dsdtf)),
            ('department', '=', department_id),
            ('dentist', '=', dentist_id)], limit=1, order='time_to DESC')

        if len(last_schedule) > 0:
            return last_schedule.time_to
        else:
            # TODO: Get from shift
            return (start_time + timedelta(hours=1)).strftime(dsdtf)

    @api.model
    def get_end_time(self, department_id=False, dentist_id=False, date=None):
        return (datetime.strptime(self.get_start_time(department_id=department_id, dentist_id=dentist_id, date=date), dsdtf) + timedelta(minutes=30)).strftime(dsdtf)

    # @api.one
    # @api.constrains('time_from', 'time_to')
    # def _check_date(self):
    #         for event in self:
    #             domain = [
    #                 ('time_from', '<', event.time_to),
    #                 ('time_to', '>', event.time_from),
    #                 ('dentist', '=', event.dentist.id),
    #                 ('id', '!=', event.id),
    #             ]
    #             nevents = self.search_count(domain)
    #             if nevents:
    #                 raise ValidationError(_("%s has already scheduled on this time!" % event.dentist.name))

    date = fields.Date('Date')
    time_from = fields.Datetime('Time From', required=True, default=lambda self: self.get_start_time())
    time_to = fields.Datetime('Time To', required=True, default=lambda self: self.get_end_time())
    customer_name = fields.Char('Customer Name', required=True)
    customer = fields.Many2one('dental.customer', 'Customer')
    dentist = fields.Many2one('hr.employee', 'Dentist', domain=[('job_id.is_dentist', '=', True)])
    department = fields.Many2one('hr.department', 'Department', domain=[('is_dental_department', '=', True)], required=True)
    note = fields.Text('Note')
    color = fields.Integer(related='department.color', string='Color Index')
    plans = fields.One2many(related='customer.uncompleted_plans', string='Appointment Plans')
    uncompleted_plans = fields.One2many('dental.appointment.plan', 'appointment', string='Uncompleted Plans', compute="_compute_uncompleted_plans")
    appointment = fields.Many2one('dental.appointment', 'Appointment')
    done = fields.Boolean('Done', default=False)
    arrived = fields.Boolean('Arrived', default=False)
    doctor_seen_after_arrived = fields.Boolean('Doctor Seen', default=False) # field for notification
    can_seen = fields.Boolean(default=False) # field for notification
    lag = fields.Boolean('Lag', default=False)
    informed_blood_diagnosis = fields.Boolean('Informed about Blood(B,C) diagnosis', default=False)
    state = fields.Selection([('new', 'New'), ('confirmed', 'Confirmed'), ('cancel', 'Cancelled')], string='State', default = 'new')
    auto_create = fields.Boolean(default = False)


    @api.multi
    @api.constrains('time_to')
    def date_constrains(self):
        for rec in self:
            if rec.time_to:
                if rec.time_to < rec.time_from:
                    raise ValidationError(_('Finish date must be later than Start date!!!'))

    @api.model
    def create(self, vals):
        return super(DentalSchedule, self).create(vals)

    @api.multi
    def write(self, vals):
        if 'date' in vals:
            vals.update({
                'time_from': self.get_start_time(department_id=vals.get('department'), dentist_id=vals.get('dentist'), date=vals.get('date')),
                'time_to': self.get_end_time(department_id=vals.get('department'), dentist_id=vals.get('dentist'), date=vals.get('date'))
            })

        # update appointment fields
        for sch in self:
            if sch.appointment:
                app_vals = {}
                if 'dentist' in vals:
                    app_vals.update({'dentist': vals.get('dentist')})
                if 'department' in vals:
                    app_vals.update({'department': vals.get('department')})
                if 'customer' in vals:
                    app_vals.update({'customer': vals.get('customer')})
                if 'time_from' in vals:
                    app_vals.update({'start_time': vals.get('time_from')})
                if 'time_to' in vals:
                    app_vals.update({'end_time': vals.get('time_to')})
                sch.appointment.write(app_vals)

        return super(DentalSchedule, self).write(vals)

    @api.onchange('time_from')
    def onchange_time_from(self):
        if self.time_from:
            self.time_to = datetime.strptime(self.time_from, dsdtf) + timedelta(minutes=30)
        else:
            self.time_to = False

    @api.onchange('date', 'dentist')
    def onchange_date_or_dentist(self):
        if self.date:
            self.time_from = self.get_start_time(department_id=self.department and self.department.id or False, dentist_id=self.dentist and self.dentist.id or False, date=self.date)
            self.time_to = self.get_end_time(department_id=self.department and self.department.id or False, dentist_id=self.dentist and self.dentist.id or False, date=self.date)
        else:
            self.time_from = False
            self.time_to = False

        if self.dentist and self.dentist.department_id:
            self.department = self.dentist.department_id

    @api.onchange('department')
    def onchange_department(self):
        if self.department:
            if self.dentist.department_id != self.department:
                self.dentist = False
            return {
                'domain': {
                    'dentist': [('job_id.is_dentist', '=', True), ('department_id', '=', self.department.id)]
                }
            }
        else:
            return {
                'domain': {
                    'dentist': [('job_id.is_dentist', '=', True)]
                }
            }

    @api.onchange('customer')
    def onchange_customer(self):
        if self.customer:
            name = self.customer.name
            if self.customer.surname:
                name = '%s %s' % (self.customer.surname, name)
            if self.customer.code:
                name = '[%s] %s' % (self.customer.code, name)
            self.customer_name = name

    @api.one
    def is_arrived(self):
        # check customer
        if not self.customer:
            raise ValidationError(_('Please select customer.'))

        # change date to today
        today = datetime.today()
        start_time = datetime.strptime(self.time_from, dsdtf).replace(year=today.year, month=today.month, day=today.day)
        end_time = datetime.strptime(self.time_to, dsdtf).replace(year=today.year, month=today.month, day=today.day)

        # create appointment
        app = self.env['dental.appointment'].create({
            'dentist': self.dentist and self.dentist.id or False,
            'customer': self.customer.id,
            'start_time': start_time.strftime(dsdtf),
            'end_time': end_time.strftime(dsdtf),
            'department': self.department.id
        })

        self.appointment = app
        self.arrived = True
        self.state = 'confirmed'

        # check lag
        if datetime.strptime(self.time_from, dsdtf) < datetime.now():
            self.lag = True
            
        # create new notification for dentist & save it through bus.bus
        if self.dentist and self.dentist.user_id:

            self.can_seen = True # used for identify schedule's customer arrived after this development or not
            
            delta = 1
            notify_at = datetime.now() + relativedelta(seconds=+delta)
            
            day = str(get_day_like_display(str(self.time_from)[0:19], self.dentist.user_id))[0:10]
            time_from = str(get_day_like_display(str(self.time_from)[0:19], self.dentist.user_id))[11:16]
            time_to = str(get_day_like_display(str(self.time_to)[0:19], self.dentist.user_id))[11:16]
            message = u"%s:\t%s (%s~%s)" %(self.customer_name, day, time_from, time_to)
            
            # create notification data
            notif = [{
                'event_id': self.id,
                'customer_id': self.customer.id,
                'appointment_id': self.appointment.id,
                'title': _("Customer arrived !!!"),
                'message': message,
                'timer': delta,
                'notify_at': fields.Datetime.to_string(notify_at),
            }]
            
            # send notification to bus.bus & it will display all notifications using thread function
            notifs = [[(self._cr.dbname, 'schedule.signal', self.dentist.user_id.sudo().partner_id.id), notif]]
            self.env['bus.bus'].sendmany(notifs)

    def button_confirm(self):
        self.state = 'confirmed'
        self.env['dental.schedule.list'].init()

    def button_cancel(self):
        self.state = 'cancel'
            

class DentalScheduleList(models.Model):
    _name = 'dental.schedule.list'
    _auto = False

    time_from = fields.Datetime('Time From')
    time_to = fields.Datetime('Time To')
    event_name = fields.Char('Event name')
    employee = fields.Many2one('hr.employee', 'Dentist')
    department = fields.Many2one('hr.department', 'Department')
    department_color = fields.Integer('Color Index')
    customer = fields.Many2one('dental.customer', 'Customer')
    app_type = fields.Selection([('app', 'Appointment'), ('schedule', 'Schedule')], string='Appointment Type')
    schedule = fields.Many2one('dental.schedule', 'Schedule')
    arrived = fields.Boolean('Arrived')
    lag = fields.Boolean('Lag')
    done = fields.Boolean('Done')
    appointment = fields.Many2one('dental.appointment', 'Appointment')

    @api.model
    def get_shift_start_time(self):
        # TODO: Get from shift
        start_date = fields.Date.context_today(self)
        return datetime.strptime(start_date, dsdf) + timedelta(hours=1)

    @api.model
    def get_shift_end_time(self):
        # TODO: Get from shift
        end_date = fields.Date.context_today(self)
        return datetime.strptime(end_date, dsdf) + timedelta(hours=10)

    @api.model_cr
    def init(self):
        user_tz = pytz.utc
        if self.env.user.partner_id.tz:
            user_tz = pytz.timezone(self.env.user.partner_id.tz)
        now = datetime.now()
        offset_now = user_tz.utcoffset(now)
        offset_now_hours = offset_now.seconds / 3600.0

        tools.drop_view_if_exists(self._cr, 'dental_schedule_list')
        self._cr.execute("""
            CREATE OR REPLACE FUNCTION department_schedule_through_365days()
            RETURNS TABLE(
                employee INTEGER,
                department INTEGER,
                event_name VARCHAR,
                app_type VARCHAR,
                time_from TIMESTAMP,
                time_to TIMESTAMP,
                customer INTEGER,
                department_color INTEGER,
                schedule INTEGER,
                arrived BOOLEAN,
                lag BOOLEAN,
                done BOOLEAN
            ) AS $$
            DECLARE
                rec RECORD;
            BEGIN
                FOR rec IN (SELECT
                  id as department,
                  SUBSTRING(name, 1, 26) as event_name,
                  color
                  FROM hr_department
                  WHERE is_dental_department = 't')
                LOOP
                    FOR counter IN 0..365
                    LOOP
                        employee := NULL;
                        department := rec.department;
                        event_name := rec.event_name;
                        app_type := 'schedule';
                        time_from := current_date + INTERVAL '24 hours' * counter;
                        time_to := current_date + INTERVAL '24 hours' * counter + INTERVAL '10 hours';
                        customer := null;
                        department_color := rec.color;
                        schedule := null;
                        arrived := null;
                        lag := null;
                        done := null;
                        RETURN NEXT;
                    END LOOP;
                END LOOP;
            END;
            $$ LANGUAGE plpgsql;""")
        self._cr.execute("""
            CREATE OR REPLACE VIEW dental_schedule_list as (
                SELECT *, ROW_NUMBER() OVER (PARTITION BY true) as id
                FROM(
                     SELECT * FROM department_schedule_through_365days()
                     UNION ALL
                     SELECT
                          emp.id as employee,
                          dep.id as department,
                          SUBSTRING(emp.name_related, 1, 26) as event_name,
                          'schedule' as app_type,
                          rel.from_time as time_from,
                          rel.to_time as time_to,
                          null::integer as customer,
                          dep.color as color,
                          null::integer as schedule,
                          null::boolean as arrived,
                          null::boolean as lag,
                          null::boolean as done
                          FROM hr_roster_line as roster
                          INNER JOIN hr_employee as emp ON emp.id = roster.employee_id
                          INNER JOIN hr_department as dep ON emp.department_id = dep.id
                          INNER JOIN hr_roster_employee_line as rel ON rel.line_id = roster.id
                          INNER JOIN hr_job as job ON job.id = emp.job_id
                          WHERE job.is_dentist = 't' and rel.hr_shift_id IS NOT NULL
                     UNION ALL
                     SELECT
                          emp.id as employee,
                          sch.department as department,
                          SUBSTRING(customer_name, 1, 26) as event_name,
                          'app' as app_type,
                          time_from,
                          time_to,
                          customer,
                          dep.color as color,
                          sch.id as schedule,
                          sch.arrived as arrived,
                          sch.lag as lag,
                          sch.done as done
                          FROM dental_schedule as sch
                          LEFT JOIN hr_employee as emp ON emp.id = sch.dentist
                          INNER JOIN hr_department as dep ON dep.id = sch.department
                     ) AS schedule
                ORDER BY id
        ) """.format(offset_now_hours, self.get_shift_start_time(), self.get_shift_end_time()))

    @api.model
    def call_wizard(self):
        vals = {}
        view = self.env.ref('l10n_mn_dental_management.dental_schedule_view_form')
        wiz = self.env['dental.schedule'].create(vals)
        return {
            'name': _('Dental Schedule'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'dental.schedule',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'target': 'new',
            'res_id': wiz.id,
        }
