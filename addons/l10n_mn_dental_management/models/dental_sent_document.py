# -*- coding: utf-8 -*-

from odoo import models, fields, api

class DentalSentDocument(models.Model):
    _name = 'dental.sent.document'

    title = fields.Char(string='Title', required=True)
    content = fields.Text(string='Content', required=True)

    @api.multi
    def print_document(self):
        self.ensure_one()
        return self.env['report'].get_action(self, 'l10n_mn_dental_management.print_sent_document')
