# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _, exceptions

class DentalService(models.Model):
    _name = 'dental.service'
    _order = 'sequence'

    name = fields.Char('Service Name', required=True)
    product_category = fields.Many2one('product.category', required=True)
    sequence = fields.Integer(index=True, default=1)
    procedures = fields.One2many('dental.procedure', 'service', 'Dental Procedures')

class DentalProcedure(models.Model):
    _name = 'dental.procedure'
    _inherits = {
        'product.template': 'product_template',
    }

    service = fields.Many2one('dental.service', 'Dental Service', ondelete='cascade', required=True)
    product_template = fields.Many2one('product.template', 'Product Template', ondelete='cascade', required=True)
    recurrent = fields.Boolean('Is Recurrent', default=False, help='System will automatically send notification to dentist at specific duration')
    min_duration_for_notify = fields.Integer('Minimum Duration for Notification (In Days)', default=1, help='Minimum duration since last procedure time notify to dentist')
    is_gel = fields.Boolean('Is Gel', default=False)

    @api.model
    def create(self, vals):
        # check notification duration
        if 'min_duration_for_notify' in vals and not vals.get('min_duration_for_notify'):
            raise exceptions.UserError(_('Please insert minimum duration for notification'))

        # update type
        vals.update({
            'type': 'service',
            'sale_ok': True,
            'purchase_ok': False
        })

        # update category
        if vals.get('service'):
            vals.update({
                'categ_id': self.env['dental.service'].search([('id', '=', vals.get('service'))], limit=1).product_category.id
            })

        return super(DentalProcedure, self).create(vals)

    @api.multi
    def write(self, vals):
        # check notification duration
        if 'min_duration_for_notify' in vals and not vals.get('min_duration_for_notify'):
            raise exceptions.UserError(_('Please insert minimum duration for notification'))
        return super(DentalProcedure, self).write(vals)

    @api.multi
    def unlink(self):
        # get related templates
        related_product_templates = self.env['product.template']
        for dp in self:
            related_product_templates = related_product_templates | dp.product_template

        # call super
        res = super(DentalProcedure, self).unlink()

        # delete related product templates
        related_product_templates.unlink()

        return res

class DentalProcedureAttribute(models.Model):
    _name = 'dental.procedure.attribute'
    _description = 'Dental Procedure Attribute'
    _order = 'sequence'

    name = fields.Char('Attribute Name', required=True)
    values = fields.One2many('dental.procedure.attribute.value', 'attribute', 'Attribute Values')
    sequence = fields.Integer(index=True, default=1)

class DentalProcedureAttributeValue(models.Model):
    _name = 'dental.procedure.attribute.value'
    _description = 'Dental Procedure Attribute Value'
    _order = 'attribute,sequence'

    name = fields.Char('Value Name', required=True)
    attribute = fields.Many2one('dental.procedure.attribute', 'Attribute')
    sequence = fields.Integer(index=True, default=1)

    @api.multi
    def name_get(self):
        result = []
        for value in self:
            name = '%s => %s' % (value.attribute.name, value.name)
            result.append((value.id, name))
        return result

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('name', operator, name), ('attribute', operator, name)]
        values = self.search(domain + args, limit=limit)
        return values.name_get()
