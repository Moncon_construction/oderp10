# -*- coding: utf-8 -*-

from datetime import datetime

from odoo import models, fields, api, _
from odoo import exceptions


class DentalTest(models.Model):
    _name = 'dental.test'
    _description = 'Dental Test'
    _inherits = {
        'product.template': 'product_template',
    }

    product_template = fields.Many2one('product.template', 'Product Template', ondelete='cascade', required=True)
    xray_add = fields.Float('X-ray add', default=0)

    @api.model
    def create(self, vals):
        # update type
        vals.update({
            'type': 'service',
            'sale_ok': True,
            'purchase_ok': False
        })
        return super(DentalTest, self).create(vals)

    @api.multi
    def unlink(self):
        # get related templates
        related_product_templates = self.env['product.template']
        for da in self:
            related_product_templates = related_product_templates | da.product_template

        # call super
        res = super(DentalTest, self).unlink()

        # delete related product templates
        related_product_templates.unlink()

        return res


class DentalTestRequest(models.Model):
    _name = 'dental.test.request'
    _descripion = 'Dental Test Request'
    _order = 'create_date DESC'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    def _default_nurse(self):
        employee = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)], limit=1)
        if employee and employee.job_id and employee.job_id.is_nurse:
            return self.env.user.id
        return False

    company = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id.id)
    state = fields.Selection([('draft', 'Draft'), ('done', 'Done')], string='Status', copy=False, default='draft', track_visibility='onchange')
    department = fields.Many2one('hr.department', 'Department', domain=[('is_dental_department', '=', True)], states={'done': [('readonly', True)]})
    customer = fields.Many2one('dental.customer', 'Customer', required=True, states={'done': [('readonly', True)]})
    appointment = fields.Many2one('dental.appointment', 'Appointment')
    teeth = fields.Many2many('dental.tooth', string='Teeth', states={'done': [('readonly', True)]})
    test = fields.Many2one('dental.test', 'Dental Test', required=True, states={'done': [('readonly', True)]})
    uom = fields.Many2one(related='test.uom_id', string='Unit of Measure', readonly=True)
    quantity = fields.Float('Quantity', required=True, default=1, states={'done': [('readonly', True)]})
    dentist_note = fields.Text('Dentist Note', states={'done': [('readonly', True)]})
    nurse = fields.Many2one('res.users', 'Nurse', default=_default_nurse, states={'done': [('readonly', True)]})
    attachments = fields.Many2many('ir.attachment', string="Attachment Files", states={'done': [('readonly', True)]})
    nurse_note = fields.Text('Nurse Note', states={'done': [('readonly', True)]})
    sale_order = fields.Many2one('sale.order', 'Sale Order')

    @api.multi
    def name_get(self):
        result = []
        for tr in self:
            name = '%s - %s(%s)' % (tr.customer.name, tr.test.name, tr.create_date)
            result.append((tr.id, name))
        return result

    @api.model
    def create(self, vals):
        if 'nurse' not in vals:
            vals.update({
                'nurse': self.env.user.id
            })
        if 'appointment' in vals:
            appointment = self.env['dental.appointment'].search([('id', '=', vals.get('appointment'))])
            vals.update({
                'customer': appointment.customer.id,
                'department': appointment.department.id
            })
        return super(DentalTestRequest, self).create(vals)

    @api.onchange('nurse')
    def onchange_nurse(self):
        employee = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)], limit=1)
        if employee and employee.department_id:
            self.department = employee.department_id

    @api.multi
    def action_done(self):
        self.ensure_one()

        so = self.env['sale.order'].sudo().create({
            'partner_id': self.customer.partner.id,
            'date_order': datetime.now(),
            'user_id': self.nurse.id,
            'picking_policy': 'direct',
            'invoice_policy': 'to_product',
        })

        self.env['sale.order.line'].sudo().create({
            'order_id': so.id,
            'product_id': self.test.product_variant_id.id,
            'product_uom_qty': self.quantity
        })
        self.sale_order = so
        # so.action_confirm()

        self.state = 'done'

    @api.multi
    def action_cancel(self):
        self.ensure_one()
        if (self.sudo().sale_order and self.sudo().sale_order.state == 'cancel') or not self.sudo().sale_order:
            self.state = 'draft'
        else:
            raise exceptions.ValidationError(_('You can only cancel a test request, that has cancelled sale orders.'))

    @api.multi
    def unlink(self):
        for req in self:
            if req.state != 'draft':
                raise exceptions.ValidationError(_('%s is not draft. You can delete only draft test request.') % req.name_get()[0][1])
        return super(DentalTestRequest, self).unlink()


class DentalCustomer(models.Model):
    _inherit = 'dental.customer'

    tests = fields.One2many('dental.test.request', 'customer', string='Test Requests')
