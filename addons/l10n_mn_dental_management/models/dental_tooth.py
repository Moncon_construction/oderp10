# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api

class DentalTooth(models.Model):
    _name = 'dental.tooth'

    name = fields.Char('Tooth Name', required=True)

class ToothSurface(models.Model):
    _name = 'dental.tooth.surface'

    tooth = fields.Many2one('dental.tooth', 'Tooth', required=True)
    name = fields.Char('Surface Name', required=True)

    @api.multi
    def name_get(self):
        result = []
        for surface in self:
            name = '%s - %s' % (surface.tooth.name, surface.name)
            result.append((surface.id, name))
        return result


    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('name', operator, name), ('tooth.name', operator, name)]
        surfaces = self.search(domain + args, limit=limit)
        return surfaces.name_get()
