# -*- coding: utf-8 -*-
from odoo import models, fields, api


class DentalTreatment(models.Model):
    _name = 'dental.treatment'

    appointment = fields.Many2one('dental.appointment', 'Appointment')
    teeth = fields.Many2many('dental.tooth', string='Teeth')
    surfaces = fields.Many2many('dental.tooth.surface', string='Surfaces')
    general_diagnosis = fields.Many2one('dental.diagnosis', 'General Diagnosis', domain=[('is_general', '=', True), ('is_healthy', '=', False), ('not_exist', '=', False)])
    detailed_diagnosis = fields.Many2one('dental.diagnosis', 'Detailed Diagnosis', domain=[('is_general', '=', False)])
    procedure_lines = fields.One2many('dental.treatment.procedure.line', 'treatment', string='Dental Procedures')
    product_lines = fields.One2many('dental.treatment.product.line', 'treatment', string='Products')
    treatment_packs = fields.One2many('dental.treatment.pack.line', 'treatment', string='Treatment Pack')

    @api.onchange('general_diagnosis')
    def onchange_general_diagnosis(self):
        detailed_diagnoses = self.env['dental.diagnosis'].search([('is_general', '=', False)])
        if self.general_diagnosis:
            detailed_diagnoses = detailed_diagnoses.filtered(lambda r: r.general_diagnosis == self.general_diagnosis)
        self.detailed_diagnosis = False
        return {
            'domain': {
                'detailed_diagnosis': [('id', 'in', detailed_diagnoses.ids)]
            }
        }

    @api.model
    def create(self, vals):
        treatment = super(DentalTreatment, self).create(vals)

        if vals.get('general_diagnosis'):
            drd = self.env['dental.register.diagnosis'].create({
                'teeth': vals.get('teeth'),
                'surfaces': vals.get('surfaces'),
                'general_diagnosis': vals.get('general_diagnosis'),
                'detailed_diagnosis': vals.get('detailed_diagnosis')
            })
            drd.with_context({'active_id': vals.get('appointment')}).calculate_diagnosis()

        return treatment

    @api.multi
    def write(self, vals):
        super(DentalTreatment, self).write(vals)

        for treatment in self:
            drd = self.env['dental.register.diagnosis'].create({
                'general_diagnosis': treatment.general_diagnosis.id,
                'detailed_diagnosis': treatment.detailed_diagnosis.id
            })
            drd.teeth = treatment.teeth
            drd.surfaces = treatment.surfaces
            drd.with_context({'active_id': treatment.appointment.id}).calculate_diagnosis()

    @api.onchange('treatment_packs')
    def onchange_treatment_packs(self):
        procedure_lines = []
        product_lines = []
        for pack_line in self.treatment_packs:
            for procedure in pack_line.treatment_pack.procedures:
                procedure_lines.append([procedure, pack_line.qty])
            for product in pack_line.treatment_pack.products:
                product_lines.append([product, pack_line.qty])
        self.procedure_lines = [(0, 0, {'procedure': line[0].procedure.id,
                                        'qty': line[0].qty * line[1],
                                        'service': line[0].service.id,
                                        'uom': line[0].uom.id}) for line in procedure_lines]
        self.product_lines = [(0, 0, {'product': line[0].product.id,
                                      'qty': line[0].qty * line[1],
                                      'uom': line[0].uom.id}) for line in product_lines]


class DentalTreatmentPackLine(models.Model):
    _name = 'dental.treatment.pack.line'

    treatment_pack = fields.Many2one('dental.treatment.pack', string='Treatment Pack')
    qty = fields.Float('Quantity', default=1.0, required=True)
    treatment = fields.Many2one('dental.treatment', 'Treatment')

    @api.multi
    def name_get(self):
        result = []
        for app in self:
            name = ''
            for treat_pack in app.treatment_pack:
                name += (', ' + treat_pack.name)
            result.append((app.id, name[2:]))
        return result


class DentalTreatmentPack(models.Model):
    _name = 'dental.treatment.pack'

    name = fields.Char('Name', required=True)
    procedures = fields.One2many('dental.treatment.pack.procedure', 'treatment_pack', string='Procedures')
    products = fields.One2many('dental.treatment.pack.product', 'treatment_pack', string='Products')


class DentalTreatmentPackProcedure(models.Model):
    _name = 'dental.treatment.pack.procedure'
    _rec_name = 'procedure'

    treatment_pack = fields.Many2one('dental.treatment.pack', string="Treatment Pack")
    procedure = fields.Many2one('dental.procedure', string='Dental Procedure', required=True)
    qty = fields.Float('Quantity', default=1.0, required=True)
    service = fields.Many2one('dental.service', 'Dental Service')
    uom = fields.Many2one(related='procedure.uom_id', string='Unit of Measure', readonly=True)


class DentalTreatmentPackProduct(models.Model):
    _name = 'dental.treatment.pack.product'
    _rec_name = 'product'

    treatment_pack = fields.Many2one('dental.treatment.pack', string="Treatment Pack")
    product = fields.Many2one('product.product', domain=[('type', '=', 'product'), ('sale_ok', '=', True)], string='Product', required=True)
    uom = fields.Many2one(related='product.uom_id', string='Unit of Measure', readonly=True)
    qty = fields.Float('Quantity', default=1.0, required=True)


class DentalTreatmentProcedureLine(models.Model):
    _name = 'dental.treatment.procedure.line'

    treatment = fields.Many2one('dental.treatment', 'Treatment')
    service = fields.Many2one('dental.service', 'Dental Service')
    procedure = fields.Many2one('dental.procedure', string='Dental Procedure', required=True)
    uom = fields.Many2one(related='procedure.uom_id', string='Unit of Measure', readonly=True)
    qty = fields.Float('Quantity', default=1.0, required=True)
    attributes = fields.Many2many('dental.procedure.attribute.value', 'dental_procedure_attribute_value_treatment_procedure_line_rel', string='Attributes')
    teeth = fields.Many2many('dental.tooth', related='treatment.teeth', string='Teeth')
    general_diagnosis = fields.Many2one('dental.diagnosis', string='General Diagnosis', related='treatment.general_diagnosis')
    customer = fields.Many2one('dental.customer', string='Customer', related='treatment.appointment.customer')
    dentist = fields.Many2one('hr.employee', string='Dentist', related='treatment.appointment.dentist')
    description = fields.Text('Description', related='treatment.appointment.description')
    start_time = fields.Datetime(related='treatment.appointment.start_time', string='Start Time')

    @api.multi
    def name_get(self):
        result = []
        for line in self:
            name = '%s(%s)' % (line.procedure.name, line.qty)
            result.append((line.id, name))
        return result

    @api.onchange('service')
    def onchange_service(self):
        # filter procedures
        procedures = self.env['dental.procedure'].search([])
        if self.service:
            procedures = procedures.filtered(lambda r: r.service == self.service)

        return {
            'domain': {
                'procedure': [('id', 'in', procedures.ids)]
            }
        }


class DentalTreatmentProductLine(models.Model):
    _name = 'dental.treatment.product.line'

    treatment = fields.Many2one('dental.treatment', 'Treatment')
    product = fields.Many2one('product.product', domain=[('type', '=', 'product'), ('sale_ok', '=', True)], string='Product', required=True)
    uom = fields.Many2one(related='product.uom_id', string='Unit of Measure', readonly=True)
    qty = fields.Float('Quantity', default=1.0, required=True)
    teeth = fields.Many2many('dental.tooth', related='treatment.teeth', string='Teeth')
    general_diagnosis = fields.Many2one('dental.diagnosis', string='General Diagnosis', related='treatment.general_diagnosis')
    customer = fields.Many2one('dental.customer', string='Customer', related='treatment.appointment.customer')
    dentist = fields.Many2one('hr.employee', string='Dentist', related='treatment.appointment.dentist')
    start_time = fields.Datetime(related='treatment.appointment.start_time', string='Start Time')

    @api.multi
    def name_get(self):
        result = []
        for line in self:
            name = '%s(%s)' % (line.product.name, line.qty)
            result.append((line.id, name))
        return result

class DentalCustomer(models.Model):
    _inherit = 'dental.customer'

    procedures = fields.One2many('dental.treatment.procedure.line', 'customer', string='Dental Procedures')
    products = fields.One2many('dental.treatment.product.line', 'customer', string='Products')
