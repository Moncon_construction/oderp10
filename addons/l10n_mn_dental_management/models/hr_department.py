# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

from odoo import models, fields

class HrDepartment(models.Model):
    _inherit = "hr.department"

    is_dental_department = fields.Boolean('Is Dental Department')
