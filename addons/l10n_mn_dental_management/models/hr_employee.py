from odoo import _, api, fields, models


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    @api.multi
    def compute_denttest_salary(self, period_id):
        period = self.env['account.period'].browse(period_id)
        return sum(request.test.xray_add for request in self.env['dental.test.request'].search([('create_date', '>=', period.date_start), ('create_date', '<=', period.date_stop), ('nurse', '=', self.user_id.id)]))
