# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

from odoo import models, fields

class HrJob(models.Model):
    _inherit = "hr.job"

    is_dentist = fields.Boolean('Is Dentist')
    is_nurse = fields.Boolean('Is Nurse')
