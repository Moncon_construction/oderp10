# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

from odoo import models, api

class ProductAttributevalue(models.Model):
    _inherit = 'product.attribute.value'
    
    @api.one
    def _compute_price_extra(self):
        super(ProductAttributevalue, self)._compute_price_extra()
        
        dpa = self.env['dental.procedure.attribute'].search([('product_attribute', '=', self.attribute_id.id)], limit=1)
        if dpa:
            price = self.price_ids.filtered(lambda price: price.product_tmpl_id == dpa.procedure.product_template)
            self.price_extra = price.price_extra


    def _set_price_extra(self):
        super(ProductAttributevalue, self)._set_price_extra()

        dpa = self.env['dental.procedure.attribute'].search([('product_attribute', '=', self[0].attribute_id.id)], limit=1)
        if not dpa:
            return

        AttributePrice = self.env['product.attribute.price']
        prices = AttributePrice.search([('value_id', 'in', self.ids), ('product_tmpl_id', '=', dpa.procedure.product_template.id)])
        updated = prices.mapped('value_id')
        if prices:
            prices.write({'price_extra': self.price_extra})
        else:
            for value in self - updated:
                AttributePrice.create({
                    'product_tmpl_id': dpa.procedure.product_template.id,
                    'value_id': value.id,
                    'price_extra': self.price_extra,
                })

    @api.multi
    def unlink(self):
        if self.env.context.get('from_dental'):
            linked_products = self.env['product.product'].with_context(active_test=False).search([('attribute_value_ids', 'in', self.ids)])
            linked_products.unlink()
        return super(ProductAttributevalue, self).unlink()


    @api.multi
    def _variant_name(self, variable_attributes):
        return ", ".join([u'%s: %s' % (v.attribute_id.name, v.name) for v in self.sorted(key=lambda r: r.attribute_id.name) if v.attribute_id in variable_attributes])

