# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class ResUsers(models.Model):
    _inherit = 'res.users'

    dental_warehouse = fields.Many2one('stock.warehouse', string='Dental Warehouse', help='This warehouse is used for creating picking from sale order when finish dental appointment.')
