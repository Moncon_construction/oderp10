# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

from odoo import models, fields

class SaleOrder(models.Model):
    _inherit = "sale.order"

    partner_category_id = fields.Many2many('res.partner.category', related='partner_id.category_id')
