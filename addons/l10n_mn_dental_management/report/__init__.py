# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

import print_advice
import dental_permission_sheet
import print_sent_document
import print_dental_appointment
