# -*- coding: utf-8 -*-
from odoo import fields, models, api
import base64
import xlsxwriter
import time
from io import BytesIO
from operator import itemgetter
import time
from datetime import datetime, timedelta
import base64
from StringIO import StringIO
from docx import Document
from odoo.modules import get_module_resource
import zipfile
import os

class DentalPermissionSheet(models.TransientModel):
    _name = 'dental.permission.sheet.wizard'

    @api.multi
    def set_doctor(self):
        doctor = self.env['hr.employee'].search([('user_id','=',self.env.user.id)])
        job = self.env['hr.job'].search([('id','=',doctor.job_id.id)])
        if job.is_dentist == unicode('t'):
            return doctor.id
        return False

    doctor_employee_id = fields.Many2one('hr.employee', string='Employee', domain="[('job_id.is_dentist','=','t')]", default=set_doctor)
    customer_id = fields.Many2one('dental.customer','Customer')
    date = fields.Date('Current Date', default=lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'))

    @api.multi
    def export_report(self):
        context = self._context
        report_type=''
        # if self.attachment_id.datas:
        report ,template_name= self.create_doc_source(self.doctor_employee_id, self.customer_id, self.date)
        out = base64.encodestring(report)
        report_type='.docx'

        excel_id = self.env['oderp.report.excel.output'].create( {
        'filedata':out,
        'filename':template_name + report_type,
        })
        return {
                'name': 'Export Report',
                'view_type':'form',
                'view_mode':'form',
                'res_model':'oderp.report.excel.output',
                'res_id':excel_id.id,
                'view_id':False,
                'context':self._context,
                'type': 'ir.actions.act_window',
                'target':'new',
                'nodestroy': True,
                }

    def create_doc_source(self, doctor_employee_id, customer_id, date):
        tmp_folder_name = '/tmp/docx_to_pdf/'
        self._delete_temp_folder(tmp_folder_name)
        self._create_temp_folder(tmp_folder_name)
        convert_path, template_name = self.convert_docx_from_base(tmp_folder_name,doctor_employee_id,customer_id,date)
        report = self._get_convert_file(convert_path )
        return report ,template_name

    @api.multi
    def convert_docx_from_base(self, tmp_folder_name, doctor_employee_id, customer_id, date):
        docx_template_name = 'template_1.docx'
        template_path = tmp_folder_name + docx_template_name
        name = 'Гажиг заслын таниулсан зөвшөөрөл'
        convert_path = tmp_folder_name + 'template.docx'
        replaceText = {}

        last_name = customer_id.partner.name
        last_name = last_name.encode(encoding='UTF-8') if type(last_name) in (str, unicode) else last_name
        replaceText.update({'last_name' : last_name })

        first_name = customer_id.partner.surname
        first_name = first_name.encode(encoding='UTF-8') if type(first_name) in (str, unicode) else first_name
        replaceText.update({'firstname' : first_name })

        gender = customer_id.gender
        gender = gender.encode(encoding='UTF-8') if type(gender) in (str, unicode) else gender
        replaceText.update({str(gender) : gender })

        age = customer_id.age
        age = age.encode(encoding='UTF-8') if type(age) in (str, unicode) else age
        replaceText.update({age : age })

        doctor_name = doctor_employee_id.name_related
        doctor_name = doctor_name.encode(encoding='UTF-8') if type(doctor_name) in (str, unicode) else doctor_name
        replaceText.update({'doctor_name' : doctor_name })
        date = datetime.strptime(date,'%Y-%m-%d')
        replaceText.update({'date' : str(date.year) + ' он ' + str(date.month) + ' сар ' + str(date.day) + ' өдөр'})
        self.create_doc(template_path,name,tmp_folder_name,convert_path,replaceText)
        return convert_path , name

    @api.multi
    def create_doc(self,template_file,name,tmp_folder_name,out_file,replaceText):
        template_file = get_module_resource('l10n_mn_dental_management', 'static/src/doc', 'Гажиг заслын таниулсан зөвшөөрөл.docx')
        templateDocx = zipfile.ZipFile(template_file)
        newDocx = zipfile.ZipFile(out_file, "w")
        for file in templateDocx.filelist:
            content = templateDocx.read(file)
            for key in replaceText.keys():
                content = content.replace(str(key), str(replaceText[key]))
            newDocx.writestr(file.filename, content)
        templateDocx.close()
        newDocx.close()
        return newDocx

    def _get_convert_file(self, convert_path):
        input_stream = open(convert_path, 'r')
        try:
            report = input_stream.read()
        finally:
            input_stream.close()

        return report

    def _convert_docx_to_pdf(self, tmp_folder_name,convert_docx_file_name):
            cmd = "soffice --headless --convert-to pdf --outdir " +convert_docx_file_name +" "+ tmp_folder_name
            os.system(cmd)

    def _create_temp_folder(self, tmp_folder_name):
        if not os.path.exists(tmp_folder_name):
            os.makedirs(tmp_folder_name)

    def _delete_temp_folder(self, tmp_folder_name):
        cmd = 'rm -rf ' + tmp_folder_name
        os.system(cmd)
        if os.path.exists(tmp_folder_name):
            os.rmdir(tmp_folder_name)
