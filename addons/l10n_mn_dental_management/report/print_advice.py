# -*- coding: utf-8 -*-

from odoo import tools
from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.addons.l10n_mn_report.models.report_helper import verbose_numeric, comma_me, convert_curr


class PrintAdvice(models.AbstractModel):
    _name = 'report.l10n_mn_dental_management.print_advice'

    @api.multi
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('l10n_mn_dental_management.print_advice')
        advice_obj = self.env['dental.advice']
        adv = advice_obj.browse(docids)
        title = adv.title
        content = adv.content

        docargs = {
            'doc_ids': docids,
            'doc_model': report.model,
            'title': title,
            'content': content,
            'docs': adv
        }

        return report_obj.render('l10n_mn_dental_management.print_advice', docargs)
