# -*- coding: utf-8 -*-

from odoo import api, models,_
from __builtin__ import True

class ReportDentalAppointment(models.AbstractModel):
    _name = 'report.l10n_mn_dental_management.report_dental_appointment'
    
    @api.multi
    def get_len(self,docids):
        appointment_obj = self.env['dental.appointment']
        appointment = appointment_obj.browse(docids)
        return len(appointment.inspections)

    @api.multi
    def render_html(self, docids, data=None):
        report = self.env['report']._get_report_from_name('l10n_mn_dental_management.report_dental_appointment')
        appointment_obj = self.env['dental.appointment']
        inspection_obj = self.env['dental.inspection']
        periodont_plaque_obj = self.env['dental.periodont.plaque']
        appointment = appointment_obj.browse(docids)
        lines = []
        plaq_number = plaque_score = 0
        plaq = mobility = general_diagnosis = tooth = level = descriptions = ''
        show_permanent_teeth = show_milk_teeth = is_gel = is_stone = is_teethridge = False
        partner = appointment.company.partner_id
        address = partner.district.name + ', ' + partner.sumkhoroo.name + ', ' + partner.town_id.name + ', ' + partner.door_number
        phone_number = partner.phone + ', ' + partner.mobile
        l = 1
        len = self.get_len(docids)
        if appointment.full_examination == True:
            inspections = inspection_obj.search([('appointment', '=', appointment.id)], order='tooth')
            inspections_objs = inspection_obj.browse([('id', '=', inspections)])
            for line in inspections:
                if l==1:
                    tooth = line.tooth.name
                    tooth_id = line.tooth.id
                    level = line.general_diagnosis.level
                    general_diagnosis = line.general_diagnosis.name
                if line.general_diagnosis.is_stone == True:
                    is_stone = True
                if line.general_diagnosis.is_teethridge == True:
                    is_teethridge = True
                if line.tooth.name not in ('S1','S2','S3','S4'):
                    if int(line.tooth.name) > 55:
                        show_milk_teeth = True
                    else:
                        show_permanent_teeth = True
                if tooth == line.tooth.name:
                    if level == 'h' and line.general_diagnosis.level =='c0':
                        general_diagnosis = line.general_diagnosis.name
                        level =line.general_diagnosis.level
                    elif level == 'h' and line.general_diagnosis.level =='d':
                        general_diagnosis = line.general_diagnosis.name
                        level =line.general_diagnosis.level
                    elif level == 'h' and line.general_diagnosis.level =='f':
                        general_diagnosis = line.general_diagnosis.name
                        level =line.general_diagnosis.level
                    elif level == 'c0' and line.general_diagnosis.level =='h':
                        general_diagnosis = general_diagnosis
                        level = level
                    elif level == 'c0' and line.general_diagnosis.level =='d':
                        general_diagnosis = general_diagnosis
                        level = level
                    elif level == 'c0' and line.general_diagnosis.level =='f':
                        general_diagnosis = line.general_diagnosis.name
                        level = line.general_diagnosis.level
                    elif level == 'd' and line.general_diagnosis.level =='f':
                        general_diagnosis = line.general_diagnosis.name
                        level =line.general_diagnosis.level
                    elif level == 'd' and line.general_diagnosis.level =='h':
                        general_diagnosis = general_diagnosis
                        level = level
                    elif level == 'd' and line.general_diagnosis.level =='c0':
                        general_diagnosis = general_diagnosis
                        level = level
                    elif level == 'f':
                        general_diagnosis = general_diagnosis
                        level = level
                    if line.description != False:
                        descriptions += str(line.description) + ', '
                else:
                    if level == 'h':
                        level = 'H'
                    if level == 'c0':
                        level = 'C0'
                    if level == 'd':
                        level = 'D'
                    if level == 'f':
                        level = 'F'
                    periodont_plaques = periodont_plaque_obj.search([('appointment', '=', appointment.id),('tooth','=',tooth_id)])
                    plaq = ''
                    for plaques in periodont_plaques:
                        if plaques.plaque_m == True:
                            plaq += str('M') + ', '
                        if plaques.plaque_d == True:
                            plaq += str('D') + ', '
                        if plaques.plaque_p == True:
                            plaq += str('P') + ', '
                        if plaques.plaque_b == True:
                            plaq += str('B') + ', '
                        if plaques.mobility == '0':
                            mobility = _('Normal')
                        elif plaques.mobility == '1':
                            mobility = _('Small')
                        elif plaques.mobility == '2':
                            mobility = _('Medium')
                        elif plaques.mobility == '3':
                            mobility = _('Large')
                    inspection_line = {'tooth':tooth,
                                        'general_diagnosis':general_diagnosis,
                                        'descriptions':descriptions,
                                        'level':level,
                                        'mobility':mobility,
                                        'plaq':plaq
                                        }
                    lines.append(inspection_line)
                    general_diagnosis = tooth = level = descriptions = ''
                    tooth = line.tooth.name
                    tooth_id = line.tooth.id
                    level = line.general_diagnosis.level
                    general_diagnosis = line.general_diagnosis.name
                    plaq
                    if line.description != False:
                        descriptions = str(line.description) + ', '
                if l == len:
                    if level == 'h':
                        level = 'H'
                    if level == 'c0':
                        level = 'C0'
                    if level == 'd':
                        level = 'D'
                    if level == 'f':
                        level = 'F'
                    periodont_plaques = periodont_plaque_obj.search([('appointment', '=', appointment.id),('tooth','=',tooth_id)])
                    plaq = ''
                    for plaques in periodont_plaques:
                        if plaques.plaque_m == True:
                            plaq += str('M') + ', '
                        if plaques.plaque_d == True:
                            plaq += str('D') + ', '
                        if plaques.plaque_p == True:
                            plaq += str('P') + ', '
                        if plaques.plaque_b == True:
                            plaq += str('B') + ', '
                        if plaques.mobility == '0':
                            mobility = _('Normal')
                        elif plaques.mobility == '1':
                            mobility = _('Small')
                        elif plaques.mobility == '2':
                            mobility = _('Medium')
                        elif plaques.mobility == '3':
                            mobility = _('Large')
                    inspection_line = {'tooth':tooth,
                                        'general_diagnosis':general_diagnosis,
                                        'descriptions':descriptions,
                                        'level':level,
                                        'mobility':mobility,
                                        'plaq':plaq
                                        }
                    lines.append(inspection_line)
                l += 1
        if appointment.full_examination == False and appointment.periodont_plaque == True:
            periodont_plaques = periodont_plaque_obj.search([('appointment', '=', appointment.id)])
            plaq = ''
            for plaques in periodont_plaques:
                tooth = plaques.tooth.name
                if plaques.plaque_m == True:
                    plaq += str('M') + ', '
                if plaques.plaque_d == True:
                    plaq += str('D') + ', '
                if plaques.plaque_p == True:
                    plaq += str('P') + ', '
                if plaques.plaque_b == True:
                    plaq += str('B') + ', '
                if plaques.mobility == '0':
                    mobility = _('Normal')
                elif plaques.mobility == '1':
                    mobility = _('Small')
                elif plaques.mobility == '2':
                    mobility = _('Medium')
                elif plaques.mobility == '3':
                    mobility = _('Large')
                if plaques.plaque_m == True or plaques.plaque_d == True or plaques.plaque_p == True or plaques.plaque_b == True or mobility!='':
                    if int(plaques.tooth.name) > 55:
                        show_milk_teeth = True
                    else:
                        show_permanent_teeth = True
                    inspection_line = {'tooth':tooth,
                                        'general_diagnosis':'',
                                        'descriptions':'',
                                        'level':'',
                                        'mobility':mobility,
                                        'plaq':plaq
                                        }
                    lines.append(inspection_line)
                    plaq = mobility = tooth =  ''
        periodont_plaques = periodont_plaque_obj.search([('appointment', '=', appointment.id)])
        for plaques in periodont_plaques:
            if plaques.plaque_m == True:
                plaq_number += 1
            if plaques.plaque_d == True:
                plaq_number += 1
            if plaques.plaque_p == True:
                plaq_number += 1
            if plaques.plaque_b == True:
                plaq_number += 1
        plaque_score = round((float(plaq_number)/208*100),1)        
        for treatment in appointment.treatments:
            for line in treatment.procedure_lines:
                if line.procedure.is_gel == True:
                    is_gel = True

        docargs = {
            'doc_ids': docids,
            'doc_model': report.model,
            'docs': appointment,
            'lines': lines,
            'show_milk_teeth': show_milk_teeth,
            'show_permanent_teeth': show_permanent_teeth,
            'address': address,
            'phone_number': phone_number,
            'is_gel': is_gel,
            'is_stone': is_stone,
            'is_teethridge': is_teethridge,
            'plaque_score': plaque_score,
        }
        return self.env['report'].render('l10n_mn_dental_management.report_dental_appointment', docargs)