# -*- coding: utf-8 -*-

from odoo import models, api, _
from odoo import exceptions

class AccountEnerelReport(models.AbstractModel):
    _name = 'report.l10n_mn_dental_management.print_sent_document'

    @api.multi
    def render_html(self, docids, data=None):
        report = self.env['report']._get_report_from_name('l10n_mn_dental_management.print_sent_document')
        document = self.env['dental.sent.document'].browse(docids)
        dentist = self.env['hr.employee'].search([('user_id', '=', document.create_uid.id)], limit=1)
        if not dentist:
            raise exceptions.UserError('Employee not exist for this document user.')
        dentist_name = dentist.name
        job_name = _('Doctor')
        if dentist.last_name:
            dentist_name = '%s.%s' % (dentist.last_name[:1], dentist_name)
        if dentist.job_id:
            job_name = dentist.job_id.name
        docargs = {
            'doc_ids': docids,
            'doc_model': report.model,
            'docs': document,
            'dentist': dentist_name,
            'job': job_name
        }

        return self.env['report'].render('l10n_mn_dental_management.print_sent_document', docargs)
