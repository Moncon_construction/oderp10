odoo.define('l10n_mn_dental_management.dental_schedule', function (require) {
"use strict";

	var bus = require('bus.bus').bus;
	var core = require('web.core');
	var Notification = require('web.notification').Notification;
	var session = require('web.session');
	var WebClient = require('web.WebClient');

	var ScheduleNotification = Notification.extend({
	    template: "ScheduleNotification",

	    init: function(parent, title, text, sid, cid, aid) {
	        this._super(parent, title, text, true);
	        this.sid = sid;
	        this.cid = cid;
	        this.aid = aid;

	        this.events = _.extend(this.events || {}, {
	        	
	        	'click .doctor_seen': function() {
	                this.destroy(true);
	                this.rpc("/schedule/notify_seen", {schedule_id: sid});
	            },
	            
	            'click .link2schedule': function() {
	                var self = this;

	                this.rpc("/web/action/load", {
	                    action_id: "l10n_mn_dental_management.link2schedule_on_dental_schedule",
	                }).then(function(r) {
	                    r.res_id = self.sid;
	                    return self.do_action(r);
	                });
	            },
	            
	            'click .link2customer': function() {
	                var self = this;

	                this.rpc("/web/action/load", {
	                    action_id: "l10n_mn_dental_management.link2customer_on_dental_schedule",
	                }).then(function(r) {
	                    r.res_id = self.cid;
	                    return self.do_action(r);
	                });
	            },
	            
	            'click .link2appointment': function() {
	                var self = this;

	                this.rpc("/web/action/load", {
	                    action_id: "l10n_mn_dental_management.link2appointment_on_dental_schedule",
	                }).then(function(r) {
	                    r.res_id = self.aid;
	                    return self.do_action(r);
	                });
	            }
	        });
	    },
	});

	WebClient.include({
	    display_schedule_notif: function(notifications) {
	        var self = this;
	        
	        // display each notification & play signal
	        _.each(notifications, function(notif) {
                var notification = new ScheduleNotification(self.notification_manager, notif.title, notif.message, notif.event_id, notif.customer_id, notif.appointment_id);
	            self.notification_manager.display(notification);
	            if (notification){
	            	var audio = new Audio();
	                var ext = audio.canPlayType("audio/ogg; codecs=vorbis") ? ".ogg" : ".mp3";
	                audio.autoplay = true;
	                audio.src = session.url("/l10n_mn_dental_management/static/audio/slow-spring-board-longer-tail" + ext);
	            	audio.play();
	            }
	        });
	    },
	    get_next_schedule_notif: function() {
	        this.rpc("/schedule/notify", {}, {shadow: true})
	            .done(this.display_schedule_notif.bind(this))
	            .fail(function(err, ev) {
	                if(err.code === -32098) {
	                    // Prevent the CrashManager to display an error
	                    // in case of an xhr error not due to a server error
	                    ev.preventDefault();
	                }
	            });
	    },
	    show_application: function() {
	    	// When new schedule notification created this trigger call display function of notification
	        bus.on('notification', this, function (notifications) {
	            _.each(notifications, (function (notification) {
	                if (notification[0][1] === 'schedule.signal') {
	                    this.display_schedule_notif(notification[1]);
	                }
	            }).bind(this));
	        });
	        // Call FATHER function then check is there any notifs & if there, then display it
	        return this._super.apply(this, arguments).then(this.get_next_schedule_notif.bind(this));
	    },
	});

});
