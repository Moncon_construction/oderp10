odoo.define('l10n_mn_dental_management.perio', function (require) {
    "use strict";

    var core = require('web.core');
    var FormView = require('web.FormView');
    var form_common = require('web.form_common');
    var Model = require('web.DataModel');
    var _t = core._t;
    var ajax = require('web.ajax');

    var Perio = form_common.FormWidget.extend(form_common.ReinitializeWidgetMixin, {
        /** Widget-ийг render хийх функц */
        renderElement: function () {
            var self = this;
            this._super();
            var html =
                "<table class='perio'>" +
                "<tr><td>Сүүн шүд</td><td><input type='checkbox' class='milk'></td></tr>" +
                "<tr><td>Байнгийн шүд</td><td><input type='checkbox' class='permanent'></td></tr>" +
                "<tbody class='top long'>" +
                "<tr class='mobility'><td>Хөдөлгөөн</td></tr>" +
                "<tr class='bop1'><td>BoP</td></tr>" +
                "<tr class='pd'><td>PD</td></tr>" +
                "<tr class='bop2'><td>BoP</td></tr>" +
                "<tr class='plaque'><td>Өнгөр</td></tr>" +
                "<tr class='numbering'><td></td></tr>" +
                "</tbody>" +
                "<tbody class='top short'>" +
                "<tr class='plaque'>" +
                "<td class='clear'></td>" +
                "<td class='clear'></td>" +
                "<td class='clear'></td>" +
                "<td>    Өнгөр     </td></tr>" +
                "<tr class='numbering'><td></td><td></td><td></td><td></td></tr>" +
                "</tbody>" +
                "<tbody class='bottom short'>" +
                "<tr class='numbering'><td></td><td></td><td></td><td></td></tr>" +
                "<tr class='plaque'>" +
                "<td class='clear'></td>" +
                "<td class='clear'></td>" +
                "<td class='clear'></td>" +
                "<td>    Өнгөр     </td></tr>" +
                "</tbody>" +
                "<tbody class='bottom long'>" +
                "<tr class='numbering'><td></td></tr>" +
                "<tr class='plaque'><td>Өнгөр</td></tr>" +
                "<tr class='bop2'><td>BoP</td></tr>" +
                "<tr class='pd'><td>PD</td></tr>" +
                "<tr class='bop1'><td>BoP</td></tr>" +
                "<tr class='mobility'><td>Хөдөлгөөн</td></tr>" +
                "</tbody>" +
                "</table>" +
                "<div class='perioCalc'>" +
                "<div class='ps'>Plaque Score: <span>0</span>%</div>" +
                "<div class='bop'>Bleeding on probing: <span>0</span>%</div>" +
                "</div>";

            this.$el.append(html);

            // html зурагдахаас өмнө drawCells дуудагдахаас сэргийлж 300ms-ийн delay өгч байна
            setTimeout(function () {
                self.drawCells();

                setTimeout(function () {
                    // Python-с утга аваад зурах функцийг дуудаж байна
                    ajax.jsonRpc("/perio", 'call', { "app_id": self.getCurrentUrlParameter("id") }).then(function (value) {
                        self.setData(value);
                        self.updateData();
                        self.updateCalcRow();
                    });
                }, 300);

            }, 300);
        },
        getCurrentUrlParameter: function (name) {
            var getUrlParameter = function getUrlParameter(sParam) {
                var sPageURL = decodeURIComponent(window.location.href.substring(window.location.href.lastIndexOf('#')).substring(1)),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            };
            return getUrlParameter(name);
        },
        /** Гол зурж, event өгч байгаа функц */
        drawCells: function () {
            var self = this;
            // давталтанд ашиглагдах утгууд
            var sectorsSelectors = ['.top.long', '.bottom.long', '.top.short', '.bottom.short'];
            var rowBases = [
                [10, 20],
                [40, 30],
                [50, 60],
                [80, 70]
            ];
            var teethCounts = [8, 8, 5, 5];

            // шүднүүдийг зурж байна
            var i, k, selector, teethCount, base;
            for (k = 0; k < 4; k++) {
                selector = sectorsSelectors[k];
                teethCount = teethCounts[k];
                // Зүүн тал
                base = rowBases[k][0];
                for (i = teethCount; i > 0; i--) {
                    if (k < 2) {
                        this.drawColumn(selector, base + i);
                    }
                    this.drawPlaque(selector, base + i, "left");
                    this.drawNumbering(selector, base + i);
                }
                // Баруун тал
                base = rowBases[k][1];
                for (i = 1; i <= teethCount; i++) {
                    if (k < 2) {
                        this.drawColumn(selector, base + i);
                    }
                    this.drawPlaque(selector, base + i, "right");
                    this.drawNumbering(selector, base + i);
                }
            }

            // зурагдсны дараа өнгөрийн болон цус шүүрлийн хувийг тооцно
            //            this.updateCalcRow();

            if ($('.o_form_view .o_form_input').length) { // Edit mode-д байна уу шалгана
                // өнгөрийн Event
                this.$el.find(".perio path").click(function () {
                    if ($(this).attr('class') == 'selected') {
                        $(this).attr("class", "");
                    } else {
                        $(this).attr("class", "selected");
                    }
                    self.updateCalcRow();
                    self.updateData();
                });
                // өнгөрийн Event
                this.$el.find("input[type=checkbox].milk").change(function () {
                    var milk_teeths = ['51', '52', '53', '54', '55', '61', '62', '63', '64', '65', '71', '72', '73', '74', '75', '81', '82', '83', '84', '85']
                    if ($(this).is(":checked")) {
                        for (var number = 0; number < milk_teeths.length; number++) {
                            var perio_table = $(this).parent().parent().parent().parent();
                            var tooth_existen_checkbox = perio_table.find(".numbering td[number=" + milk_teeths[number] + "] input[type=checkbox].tooth_existen");
                            tooth_existen_checkbox.prop("checked", false).change();
                        }
                    } else {
                        for (var number = 0; number < milk_teeths.length; number++) {
                            var perio_table = $(this).parent().parent().parent().parent();
                            var tooth_existen_checkbox = perio_table.find(".numbering td[number=" + milk_teeths[number] + "] input[type=checkbox].tooth_existen");
                            tooth_existen_checkbox.prop("checked", true).change();
                        }
                    }
                    self.updateCalcRow();
                    self.updateData();
                });
                this.$el.find("input[type=checkbox].permanent").change(function () {
                    var permanent_teeths = ['11', '12', '13', '14', '15', '16', '17', '18', '21', '22', '23', '24', '25', '26', '27', '28', '31', '32', '33', '34', '35', '36', '37', '38', '41', '42', '43', '44', '45', '46', '47', '48']
                    if ($(this).is(":checked")) {
                        for (var number = 0; number < permanent_teeths.length; number++) {
                            var perio_table = $(this).parent().parent().parent().parent();
                            var tooth_existen_checkbox = perio_table.find(".numbering td[number=" + permanent_teeths[number] + "] input[type=checkbox].tooth_existen");
                            tooth_existen_checkbox.prop("checked", false).change();
                        }
                    } else {
                        for (var number = 0; number < permanent_teeths.length; number++) {
                            var perio_table = $(this).parent().parent().parent().parent();
                            var tooth_existen_checkbox = perio_table.find(".numbering td[number=" + permanent_teeths[number] + "] input[type=checkbox].tooth_existen");
                            tooth_existen_checkbox.prop("checked", true).change();
                        }
                    }
                    self.updateCalcRow();
                    self.updateData();
                });
                // цус шүүрлийн event
                this.$el.find(".perio input[type=checkbox].tooth_bop").change(function () {
                    self.updateCalcRow();
                    self.updateData();
                });
                // шүд байгаа эсэхийг сонсох event
                this.$el.find(".perio input[type=checkbox].tooth_existen").change(function () {
                    var number = $(this).parent().attr('number');
                    var perio_table = $(this).parent().parent().parent().parent();
                    var plaque = perio_table.find(".plaque td[number=" + number + "]");
                    var bop1 = perio_table.find(".bop1 td[number=" + number + "]");
                    var bop2 = perio_table.find(".bop2 td[number=" + number + "]");
                    var pd = perio_table.find(".pd td[number=" + number + "]");
                    var mobility = perio_table.find(".mobility td[number=" + number + "]");
                    var milk_teeth = perio_table.find("input[type=checkbox].milk");
                    var permanent_teeth = perio_table.find("input[type=checkbox].permanent");
                    var milk_teeths = ['51', '52', '53', '54', '55', '61', '62', '63', '64', '65', '71', '72', '73', '74', '75', '81', '82', '83', '84', '85'];
                    if ($(this).is(":checked")) {
                        plaque.addClass("greyed_out");
                        bop1.addClass("greyed_out");
                        bop2.addClass("greyed_out");
                        pd.addClass("greyed_out");
                        mobility.addClass("greyed_out");
                    } else {
                        if (milk_teeths.includes(number)) {
                            milk_teeth.prop("checked", true);
                        } else {
                            permanent_teeth.prop("checked", true);
                        }
                        plaque.removeClass("greyed_out");
                        bop1.removeClass("greyed_out");
                        bop2.removeClass("greyed_out");
                        pd.removeClass("greyed_out");
                        mobility.removeClass("greyed_out");
                    }

                    self.updateCalcRow();
                    self.updateData();
                });
                // бусад text input-н event
                this.$el.find(".perio input[type=text]").change(function () {
                    self.updateData();
                });
            }
            else { // Үгүй бол input-уудыг disabled болгоно
                this.$el.find("input").prop('disabled', true);
            }
        },
        /** Шүдний ихэнх талбаруудыг зурна */
        drawColumn: function (selector, number) {
            this.$el.find(selector + ' .la').append('<td number="' + number + '"><input type="text" maxlength="1"></input></td>');
            this.$el.find(selector + ' .mobility').append('<td number="' + number + '"><input type="text" maxlength="1"></input></td>');
            this.$el.find(selector + ' .bop1').append('<td number="' + number + '"><input type="checkbox" class="tooth_bop"></td>');
            this.$el.find(selector + ' .pd').append('<td number="' + number + '">' +
                '<table style="width:52px">' +
                '<tr>' +
                '<td><input type="text" maxlength="1"></input></td>' +
                '<td><input type="text" maxlength="1"></input></td>' +
                '<td><input type="text" maxlength="1"></input></td>' +
                '</tr>' +
                '<tr>' +
                '<td><input type="text" maxlength="1"></input></td>' +
                '<td><input type="text" maxlength="1"></input></td>' +
                '<td><input type="text" maxlength="1"></input></td>' +
                '</tr>' +
                '</table>' +
                '</td>');
            this.$el.find(selector + ' .bop2').append('<td number="' + number + '"><input type="checkbox" class="tooth_bop"></td>');
        },
        /** Өнгөрийн талбар зурах функц */
        drawPlaque: function (selector, number, className) {
            this.$el.find(selector + ' .plaque').append('<td class="' + className + '" number="' + number + '">' +
                '<svg width="52" height="40">' +
                '<path d="M0 0h52L26 20z" face="b" />' +
                '<path d="M52 0v40L26 20z" face="m" />' +
                '<path d="M52 40H0l26-20z" face="p" />' +
                '<path d="M0 0v40l26-20z" face="d" />' +
                '</svg>' +
                '</td>');
        },
        /** Шүдний дугаар, тухайн шүд байгаа эсэхийг тэмдэглэх чекийн хамт зурах функц */
        drawNumbering: function (selector, number) {
            this.$el.find(selector + ' .numbering').append('<td number="' + number + '">' + number + '<input type="checkbox" id="' + number + '" class="tooth_existen" number="' + number + '"><label for = "' + number + '"></label></td>');
        },
        /** PD хүснэгтийн утгийг авах функц */
        getPdData: function (table) {
            var data = {};
            this.$el.find(table).find('tr').each(function (i) { // мөрийн дугаар i
                $(this).find('td').each(function (j) { // баганын дугаар j
                    data[(i * 3 + j + 1)] = $(this).find('input').val();
                });
            });
            return data;
        },
        /** Өнгөрийн утгийг авах функц */
        getPlaqueData: function (svg, isBottom) {
            var data = [];
            // Сонгогдсон шүднүүдээр давтна.
            this.$el.find(svg).find("path.selected").each(function () {
                var value = $(this).attr('face');
                // хэрэв доод эгнээний шүд бол P утгийг L утгаар солино
                if (isBottom && value == 'p') {
                    value = "l";
                }
                data.push(value);
            });
            return data;
        },
        /** Шүдний талбаруудаас json data гаргаж авах функц */
        getData: function () {
            var self = this;
            var data = {};
            var number, val;

            this.$el.find('.perio .mobility input').each(function () {
                number = $(this).parent().attr('number');
                val = $(this).val();
                if (!data[number]) {
                    data[number] = {};
                }
                data[number].mobility = val;
            });

            this.$el.find('.perio .bop1 input.tooth_bop').each(function () {
                number = $(this).parent().attr('number');
                val = $(this).is(':checked');
                data[number].bop1 = val;
            });
            this.$el.find('.perio input[type=checkbox].milk').each(function () {
                val = $(this).is(':checked');
                data['milk'] = val;
            });
            this.$el.find('.perio input[type=checkbox].permanent').each(function () {
                val = $(this).is(':checked');
                data['permanent'] = val;
            });
            this.$el.find('.perio .pd table').each(function () {
                number = $(this).parent().attr('number');
                val = self.getPdData(this);
                data[number].pd = val;
            });

            this.$el.find('.perio .bop2 input.tooth_bop').each(function () {
                number = $(this).parent().attr('number');
                val = $(this).is(':checked');
                data[number].bop2 = val;
            });

            this.$el.find('.numbering input.tooth_existen').each(function () {
                number = $(this).parent().attr('number');
                val = $(this).is(':checked');
                if (!data[number]) {
                    data[number] = {};
                }
                data[number].greyed_out = val;
            });

            this.$el.find('.perio .plaque svg').each(function () {
                number = $(this).parent().attr('number');
                val = self.getPlaqueData(this, false);
                if (!data[number]) {
                    data[number] = {};
                }
                data[number].plaque = val;
            });

            return data;
        },
        /** өгөгдсөн датагаар шүднүүдийн утгуудыг дүүргэх функц */
        setData: function (data) {
            var data = JSON.parse(data)
            var i;
            for (var property in data) {
                if (property == 'milk') {
                    this.$el.find("input[type=checkbox].milk").prop('checked', data.milk)
                } else if (property == 'permanent') {
                    this.$el.find("input[type=checkbox].permanent").prop('checked', data.permanent)
                } else {
                    if (data.hasOwnProperty(property)) {
                        var colData = data[property];
                        if (this.$el.find(".la td[number=" + property + "] input").length)
                            this.$el.find(".la td[number=" + property + "] input").val(colData.la);
                        if (this.$el.find(".mobility td[number=" + property + "] input").length)
                            this.$el.find(".mobility td[number=" + property + "] input").val(colData.mobility);
                        if (this.$el.find(".bop1 td[number=" + property + "] input.tooth_bop").length)
                            this.$el.find(".bop1 td[number=" + property + "] input.tooth_bop").prop('checked', colData.bop1);
                        if (this.$el.find(".bop2 td[number=" + property + "] input.tooth_bop").length)
                            this.$el.find(".bop2 td[number=" + property + "] input.tooth_bop").prop('checked', colData.bop2);
                        if (this.$el.find(".numbering td[number=" + property + "] input.tooth_existen").length) {
                            this.$el.find(".numbering td[number=" + property + "] input.tooth_existen").prop('checked', colData.greyed_out);
                            if (colData.greyed_out) {
                                this.$el.find(".plaque td[number=" + property + "]").addClass("greyed_out");
                                this.$el.find(".bop1 td[number=" + property + "]").addClass("greyed_out");
                                this.$el.find(".bop2 td[number=" + property + "]").addClass("greyed_out");
                                this.$el.find(".pd td[number=" + property + "]").addClass("greyed_out");
                                this.$el.find(".mobility td[number=" + property + "]").addClass("greyed_out");
                            }
                        }
                        if (this.$el.find(".pd td[number=" + property + "]").length)
                            for (i = 1; i <= 6; i++) {
                                // getPdData-ийн хийж байгаа үйлдлийг эсрэгээр нь хийж байна
                                this.$el.find(".pd td[number=" + property + "] " +
                                    "tr:eq(" + (Math.floor((i - 1) / 3)) + ") " +
                                    "td:eq(" + ((i - 1) % 3) + ") input").val(colData.pd[i]);
                            }

                        if (this.$el.find(".plaque td[number=" + property + "]").length)
                            for (i = 0; i < colData.plaque.length; i++) {
                                if (colData.plaque[i] == 'l') {
                                    colData.plaque[i] = 'p';
                                }
                                this.$el.find(".plaque td[number=" + property + "] path[face=" + colData.plaque[i] + "]")
                                    .attr("class", "selected");
                            }
                    }
                }
            }
        },
        /** Өнгөрийн болон цус шүүрлийн хувийг тооцох функц */
        updateCalcRow: function () {
            var BoPCountCalc = 0;
            var PSCountCalc = 0;
            var BoPCountText = '0';
            var PSCountText = '0';
            var greyed_BoPCount = this.$el.find('td.greyed_out input[type=checkbox].tooth_bop').length;
            var greyed_PSCount = this.$el.find('td.left.greyed_out path').length + this.$el.find('td.right.greyed_out path').length;
            var total_net_BoPCount = 64 - greyed_BoPCount;
            var total_net_PSCount = 208 - greyed_PSCount;

            this.BopCount = this.$el.find('.perio input[type=checkbox]:checked.tooth_bop').length - this.$el.find('td.greyed_out input[type=checkbox]:checked.tooth_bop').length;
            this.PSCount = this.$el.find('.perio path.selected').length - (this.$el.find('td.left.greyed_out path.selected').length + this.$el.find('td.right.greyed_out path.selected').length);

            if (this.BopCount < total_net_BoPCount) {
                BoPCountCalc = (this.BopCount / total_net_BoPCount * 100).toFixed(1);
                BoPCountText = '(' + this.BopCount + '/' + total_net_BoPCount + '*' + 100 + ')=' + BoPCountCalc;
            }
            if (this.PSCount < total_net_PSCount) {
                PSCountCalc = (this.PSCount / total_net_PSCount * 100).toFixed(1);
                PSCountText = '(' + this.PSCount + '/' + total_net_PSCount + '*' + 100 + ')=' + PSCountCalc;
            }

            $(".perioCalc .ps span").text(PSCountText);
            $(".perioCalc .bop span").text(BoPCountText);
        },
        /** Global perioData утгийг шинчлэх функц */
        updateData: function () {
            window.perioData = this.getData();
        }
    });

    // Widget-ийг зарлаж байна
    core.form_custom_registry.add('perio', Perio);

    FormView.include({
        /** Хадгалах дарахад ажиллах функц */
        save: function () {
            // Бүх цонх дээр дуудагдах тул dental.appointment-ийг тусгайлан сонгох хэрэгтэй
            if (this.model == 'dental.appointment') {
                var dataset = this.dataset;
                var app_id = dataset.ids[dataset.index];
                new Model('dental.periodont.plaque').call('save_js', [app_id, [window.perioData]]).then(function (responded) { });
            }
            return this._super();
        }
    });
});