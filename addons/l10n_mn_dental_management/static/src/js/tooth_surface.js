odoo.define('l10n_mn_dental_management.tooth_surface', function (require) {
    "use strict";

    var core = require('web.core');
    var FormView = require('web.FormView');
    var form_common = require('web.form_common');
    var Model = require('web.DataModel');
    var _t = core._t;
    var ajax = require('web.ajax');
    var Tooth = form_common.FormWidget.extend(form_common.ReinitializeWidgetMixin, {
        /** Widget-ийг render хийх функц */
        renderElement: function () {
            var self = this;
            this._super();
            this.$el.append("<div class='toothSurfaceContainer' style='height:auto;'></div>");

            // toothSurfaceContainer зурагдахаас өмнө drawGraph дуудагдахаас сэргийлж 300ms-ийн delay өгч байна
            setTimeout(function () {
                self.drawGraph();

                setTimeout(function () {
                    // Python-с утга аваад зурах функцийг дуудаж байна
                    ajax.jsonRpc("/dental", 'call', { "app_id": self.getCurrentUrlParameter("id") }).then(function (value) {
                        self.setData(value);
                    });

                }, 300);
                self.drawSetInspectioButton();
                self.bindActionNotExist();
                self.bindActionHealthy();
            }, 300);
        },
        getCurrentUrlParameter: function (name) {
            var getUrlParameter = function getUrlParameter(sParam) {
                var sPageURL = decodeURIComponent(window.location.href.substring(window.location.href.lastIndexOf('#')).substring(1)),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            };
            return getUrlParameter(name);
        },

        /** Гол зурж, event өгч байгаа функц */
        drawGraph: function () {
            //зурагдаж буй үндсэн талбар
            var svg = d3.select(".toothSurfaceContainer").append("svg")
                .attr("width", 800)
                .attr("height", 400);

            // шүднүүдийн үндсэн 4 мөр
            var longTopRow = svg.append("g").attr('class', 'long top row');
            var shortTopRow = svg.append("g").attr('class', 'short top row');
            var shortBottomRow = svg.append("g").attr('class', 'short bottom row');
            var longBottomRow = svg.append("g").attr('class', 'long bottom row');
            var extraRow = svg.append("g").attr('class', 'extra');

            // бүх мөрүүдэд зүүн баруун гэсэн дэд талбарууд нэмж байна
            svg.selectAll('.row').append('g')
                .attr('class', 'left');
            svg.selectAll('.row').append('g')
                .attr('class', 'right');

            // давталтанд ашиглагдах утгууд
            var containers = [longTopRow, shortTopRow, shortBottomRow, longBottomRow],
                sizes = [8, 5, 5, 8],
                bases = [10, 20, 50, 60, 80, 70, 40, 30];

            // шүднүүдийг зурж байна
            var container, selector, i, base;
            for (var j = 0; j < 4; j++) {
                container = containers[j];
                // зүүн тал
                selector = '.left';
                base = bases[j * 2];
                for (i = 0; i < 3; i++) {
                    this.add4Face(container, selector, i, base);
                }
                for (i = 3; i < sizes[j]; i++) {
                    this.add5Face(container, selector, i, base);
                }

                // баруун тал
                selector = '.right';
                base = bases[j * 2 + 1];
                for (i = 0; i < 3; i++) {
                    this.add4Face(container, selector, i, base);
                }
                for (i = 3; i < sizes[j]; i++) {
                    this.add5Face(container, selector, i, base);
                }
            }

            extraRow.append('g').attr('class', 'middle');
            for (i = 0; i < 4; i++) {
                this.add5Face(extraRow, '.middle', i, 'S');
            }

            if ($('.o_form_view .o_form_input').length) { // Edit mode-д байна уу шалгана
                $(".toothSurfaceContainer svg").attr("height", 600);

                var self = this;
                //click event
                this.$el.find(".toothSurfaceContainer path").click(function () {
                    var obj = {};
                    var number = $(this).parent().attr('number');
                    var face = $(this).attr('face');
                    var grandParentClass = $(this).parents().eq(2).attr('class');
                    // хэрэв доод эгнээний шүд бол P утгийг L утгаар солино
                    if (grandParentClass.indexOf('bottom') !== -1 && face == 'P') {
                        face = 'L';
                    }
                    // хэрэв илүү шүд бол P утгийг P/L утгаар солино
                    if (grandParentClass.indexOf('extra') !== -1 && face == 'P') {
                        face = 'P/L';
                    }
                    if (obj[number] === undefined) { //шүдний дата үүсээгүй бол үүсгэнэ
                        obj[number] = [face];
                    } else {
                        obj[number].push(face);
                    }
                    self.do_action({
                        type: 'ir.actions.act_window',
                        res_model: 'dental.register.diagnosis',
                        view_type: 'form',
                        view_mode: 'form',
                        views: [[false, 'form']],
                        target: 'new',
                        context: {
                            'surfaces': obj,
                            'single_surface': true,
                            'active_id': self.getCurrentUrlParameter("id"),
                        },
                    });
                });

                /* Дугаараар сонгох */
                this.$el.find(".toothSurfaceContainer text.number").click(function () {
                    var parent = $(this).parent();
                    var parentClass = parent.attr("class");
                    var fullCount = 0;
                    if (parentClass.indexOf('4face') >= 0) {
                        fullCount = 4;
                    } else if (parentClass.indexOf('5face') >= 0) {
                        fullCount = 5;
                    }

                    if (fullCount == parent.find('path.selected').length) {
                        parent.find('path').attr("class", "");
                    } else {
                        parent.find('path').attr("class", "selected");
                    }
                });

                /* Байхгүй гэж сонгох */
                this.$el.find(".toothSurfaceContainer text.not_exist").click(function () {
                    var parent = $(this).parent();
                    if (parent.find('path.not_exist').length > 0) {
                        parent.find('path').attr("class", "");
                    }
                    else {
                        parent.find('path').attr("class", "not_exist");
                    }
                });

                /* Эрүүл эсэхийг сонгох */
                this.$el.find(".toothSurfaceContainer text.healthy").click(function () {
                    var parent = $(this).parent();
                    if (parent.find('path.selected').length > 0) {
                        parent.find('path.selected').attr("class", "healthy");
                    }
                    else {
                        parent.find('path').attr("class", "healthy");
                    }
                });
            }
        },
        drawSetInspectioButton: function () {
            self = this;
            if ($('.o_form_view .o_form_input').length) { // Edit mode-д байна уу шалгана
                this.$el.append("<button class='setInspection'>Онош оруулах</button>");
                this.$el.find('.setInspection').click(function () {
                    var data = self.getSelectedData();
                    self.do_action({
                        type: 'ir.actions.act_window',
                        res_model: 'dental.register.diagnosis',
                        view_type: 'form',
                        view_mode: 'form',
                        views: [[false, 'form']],
                        target: 'new',
                        context: {
                            'surfaces': data,
                            'active_id': self.getCurrentUrlParameter("id"),
                        },
                    });
                });
            }
        },

        bindActionNotExist: function () {
            self = this;
            if ($('.o_form_view .o_form_input').length) { // Edit mode-д байна уу шалгана
                this.$el.find('text.not_exist').each(function () {
                    $(this).on("click", function () {
                        var number = $(this).parent().attr('number');
                        var app_id = self.getCurrentUrlParameter("id");
                        var DentalInspectionNotExist = new Model('dental.inspection.not.exist');
                        DentalInspectionNotExist.call('toggle_not_exist', [app_id, number]).then(function () {
                            if ($(this).parent().find('path').attr('class') == 'not_exist') {
                                $(this).attr("class", "");
                            } else {
                                $(this).attr("class", "not_exist");
                            }
                        });
                    });
                });
            }
        },

        bindActionHealthy: function () {
            self = this;
            if ($('.o_form_view .o_form_input').length) { // Edit mode-д байна уу шалгана
                this.$el.find('text.healthy').each(function () {
                    $(this).on("click", function () {
                        var tooth = $(this).parent();
                        var number = $(tooth).attr('number');
                        var app_id = self.getCurrentUrlParameter("id");
                        var DentalInspection = new Model('dental.inspection');
                        DentalInspection.call('toggle_healthy', [app_id, number]).then(function (result) {
                            if (result) {
                                $(tooth).find('path').attr("class", "healthy");
                            } else {
                                $(tooth).find('path').attr("class", "");
                            }
                        });
                    });
                });
            }
        },

        /** өгөгдсөн датагаар шүднүүдийг будах функц
         * JSON-ий key-үүдээр давтан шүднүүдийг будна
         */
        setData: function (data) {
            var surfaces = JSON.parse(data)
            var surfaceNames = ["B", "D", "M", "P", "L", "O"];
            for (var i = 11; i < 86; i++) {
                for (var surfaceIndex in surfaceNames) {
                    var surfaceName = surfaceNames[surfaceIndex];
                    var itemName = i + '-' + surfaceName;
                    var surfaceItem = surfaces[itemName];
                    if (surfaceItem != undefined) {
                        var tooth = this.$el.find(".tooth[number=" + i + "]");
                        //доод эгнээний шүд L гэсэн утгатай байдгийг P-ээр сольж байгаа
                        if (surfaceName == 'L') {
                            surfaceName = 'P';
                        }
                        if (surfaceItem.not_exist) {
                            tooth.find("path[face=" + surfaceName + "]").attr("class", "not_exist");
                        }
                        else {
                            if (surfaceItem.is_healthy) {
                                tooth.find("path[face=" + surfaceName + "]").attr("class", "healthy");
                            }
                            else if (surfaceItem.is_decay) {
                                tooth.find("path[face=" + surfaceName + "]").attr("class", "decay");
                            }
                            else {
                                tooth.find("path[face=" + surfaceName + "]").attr("class", "problem");
                            }
                        }
                    }

                }
            }
            var surfaceNames = ["B", "D", "M", "P/L", "O"];
            for (var i = 1; i < 5; i++) {
                for (var surfaceIndex in surfaceNames) {
                    var surfaceName = surfaceNames[surfaceIndex];
                    var itemName = 'S' + i + '-' + surfaceName;
                    var surfaceItem = surfaces[itemName];
                    if (surfaceItem != undefined) {
                        var tooth = this.$el.find(".tooth[number=" + 'S' + i + "]");
                        if (surfaceName == 'P/L') {
                            surfaceName = 'P';
                        }
                        if (surfaceItem.not_exist) {
                            tooth.find("path[face=" + surfaceName + "]").attr("class", "not_exist");
                        }
                        else {
                            if (surfaceItem.is_healthy) {
                                tooth.find("path[face=" + surfaceName + "]").attr("class", "healthy");
                            }
                            else if (surfaceItem.is_decay) {
                                tooth.find("path[face=" + surfaceName + "]").attr("class", "decay");
                            }
                            else {
                                tooth.find("path[face=" + surfaceName + "]").attr("class", "problem");
                            }
                        }
                    }

                }
            }
        },
        /** Сонгогдсон шүдний гадаргуугаас json data гаргаж авах функц (Шар өнгөтэй шүднүүд)*/
        getSelectedData: function () {
            var obj = {};
            this.$el.find("path.selected").each(function () {
                var number = $(this).parent().attr('number');
                var face = $(this).attr('face');
                var grandParentClass = $(this).parents().eq(2).attr('class');
                // хэрэв доод эгнээний шүд бол P утгийг L утгаар солино
                if (grandParentClass.indexOf('bottom') !== -1 && face == 'P') {
                    face = 'L';
                }
                // хэрэв илүү шүд бол P утгийг P/L утгаар солино
                if (grandParentClass.indexOf('extra') !== -1 && face == 'P') {
                    face = 'P/L';
                }
                if (obj[number] === undefined) { //шүдний дата үүсээгүй бол үүсгэнэ
                    obj[number] = [face];
                } else {
                    obj[number].push(face);
                }
            });
            return obj;
        },
        /** үүдэн шүд зурах функц D3js */
        add4Face: function (container, selector, i, base) {
            var number = base + (i + 1);
            var t = container.select(selector).append('g')
                .attr('class', '4face tooth')
                .attr('transform', 'translate(' + (i * 50) + ')')
                .attr('number', (base + i + 1));
            t.append("path")
                .attr('d', "M0 0l18 25h14L50 0z")
                .attr('face', 'B');
            t.append("path")
                .attr('d', "M0 0l18 25L0 50z")
                .attr('face', 'M');
            t.append("path")
                .attr('d', "M50 0L32 25l18 25z")
                .attr('face', 'D');
            t.append("path")
                .attr('d', "M0 50l18-25h14l18 25z")
                .attr('face', 'P');

            this.addNumber(t, container, selector, number);
            this.addExist(t, container, selector);
            this.addHealthy(t, container, selector);
        },
        /** Араа зурах функц D3js */
        add5Face: function (container, selector, i, base) {
            var number = base + (i + 1);
            var t = container.select(selector).append('g')
                .attr('class', '5face tooth')
                .attr('transform', 'translate(' + (i * 50) + ')')
                .attr('number', number);
            t.append("path")
                .attr('d', "M0 0l15 15h20L50 0z")
                .attr('face', 'B');
            t.append("path")
                .attr('d', "M0 0l15 15v20L0 50z")
                .attr('face', 'M');
            t.append("path")
                .attr('d', "M50 0L35 15v20l15 15z")
                .attr('face', 'D');
            t.append("path")
                .attr('d', "M0 50l15-15h20l15 15z")
                .attr('face', 'P');
            t.append("path")
                .attr('d', "M15 15v20h20V15z")
                .attr('face', 'O');

            this.addNumber(t, container, selector, number);
            this.addExist(t, container, selector);
            this.addHealthy(t, container, selector);

        },
        /** Шүдний дугаар зурах функц D3js */
        addNumber: function (t, container, selector, number) {
            var dx = (selector == '.left') ? (-25) : (25);
            var dy = container[0][0].classList[1] == 'bottom' ? -55 : 66;
            t.append("text")
                .attr("dx", dx + "px")
                .attr("dy", dy + "px")
                .attr('class', 'number')
                .style("text-anchor", "middle")
                .text(number);
        },
        /** Байгаа эсэхийг зурах функц */
        addExist: function (t, container, selector) {
            var dx = (selector == '.left') ? (-25) : (25);
            var dy = container[0][0].classList[1] == 'bottom' ? -70 : 81;
            t.append("text")
                .attr("dx", dx + "px")
                .attr("dy", dy + "px")
                .attr('class', 'not_exist')
                .style("text-anchor", "middle")
                .text("байхгүй");
        },
        /** Эрүүл эсэхийг зурах функц */
        addHealthy: function (t, container, selector) {
            var dx = (selector == '.left') ? (-25) : (25);
            var dy = container[0][0].classList[1] == 'bottom' ? -85 : 96;
            t.append("text")
                .attr("dx", dx + "px")
                .attr("dy", dy + "px")
                .attr('class', 'healthy')
                .style("text-anchor", "middle")
                .text("эрүүл");
        }
    });

    // Widget-ийг зарлаж байна
    core.form_custom_registry.add('tooth_surface', Tooth);

    FormView.include({
        /** Хадгалах дарахад ажиллах функц */
        save: function () {
            // Бүх цонх дээр дуудагдах тул medical.appointment-ийг тусгайлан сонгох хэрэгтэй
            if (this.model == 'dental.appointment') {
                //new Model('dental.appointment').call('save_js', [window.tooth_surface]).then(function (responded) {});
            }
            return this._super();
        }
    });
});