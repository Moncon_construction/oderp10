# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.


{
    "name": "Mongolian Dental Management with Reconciliation",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "category": "Mongolian Modules",
    'description': """
    """,
    "website": "http://asterisk-tech.mn",
    'depends': [
        'l10n_mn_account_reconciliation',
        'l10n_mn_dental_management',
    ],
    "data": [
        'security/ir.model.access.csv',
    ],
    'installable': True,
    'auto_install': True,
}
