# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/,
# http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
{
    'name': "Mongolian discussion",
    'version': '1.0',
    'depends': ['mail', 'l10n_mn_hr'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
    Mongolian Discussion
    """,
    'data': [
        'views/auto_cron_channel_get.xml',
        'views/chat_assets.xml',
        'views/discuss_event.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False,
    'qweb': [
        'static/src/xml/client_action.xml',
    ],
}
