# -*- coding: utf-8 -*-
from odoo import api, fields, models


class MailChannel(models.Model):
    _inherit = "mail.channel"

    @api.multi
    def channel_info(self, extra_info = False):
        """ Get the informations header for the current channels
            :returns a list of channels values
            :rtype : list(dict)
        """
        channel_infos = []
        partner_channels = self.env['mail.channel.partner']
        # find the channel partner state, if logged user
        if self.env.user and self.env.user.partner_id:
            partner_channels = self.env['mail.channel.partner'].sudo().search([('partner_id', '=', self.env.user.sudo().partner_id.id), ('channel_id', 'in', self.sudo().ids)])
        # for each channel, build the information header and include the logged partner information
        for channel in self:
            info = {
                'id': channel.id,
                'name': channel.name,
                'uuid': channel.uuid,
                'state': 'open',
                'is_minimized': False,
                'channel_type': channel.channel_type,
                'public': channel.public,
                'mass_mailing': channel.email_send,
            }
            if extra_info:
                info['info'] = extra_info
            # add the partner for 'direct mesage' channel
            # START OF CHANGE
            if channel.channel_type == 'chat':
                info['direct_partner'] = (channel.sudo()
                                          .with_context(active_test=False)
                                          .channel_partner_ids
                                          .filtered(lambda p: p.id != self.env.user.partner_id.id)
                                          .read(['id', 'chat_name', 'emp_company_id', 'emp_company_name', 'department_name', 'department_id','im_status']))
                if info['direct_partner']:
                    info['direct_partner'][0].update({'name': info['direct_partner'][0]['chat_name']})
            # END OF CHANGE
            # add user session state, if available and if user is logged
            if partner_channels.ids:
                partner_channel = partner_channels.filtered(lambda c: channel.id == c.channel_id.id)
                if len(partner_channel) >= 1:
                    partner_channel = partner_channel[0]
                    info['state'] = partner_channel.fold_state or 'open'
                    info['is_minimized'] = partner_channel.is_minimized
                    info['seen_message_id'] = partner_channel.seen_message_id.id
                # add needaction and unread counter, since the user is logged
                info['message_needaction_counter'] = channel.message_needaction_counter
                info['message_unread_counter'] = channel.message_unread_counter
            channel_infos.append(info)
        return channel_infos

    @api.model
    def channel_get_partners(self):
        """ Search available partners then pass args to core function. So first clear, unpin mail partners, direct chat of partners"""
        users = self.env['hr.employee'].search([('active', '=', True)]).mapped('user_id').filtered(lambda r: r.active == True)
        if users:
            partners_to = users.mapped('partner_id').filtered(lambda r: r.active == True)
            if partners_to:
                partners_to = partners_to.ids
            for partner in partners_to:
                self.channel_get_by_partner(partner, partners_to)

    @api.model
    def channel_get_by_partner(self, partner2, partners_list, pin=True):
        """ Get the canonical private channel between some partners, create it if needed.
            To reuse an old channel (conversation), this one must be private, and contains
            only the given partners.
            :param partners_to : list of res.partner ids to add to the conversation
            :param pin : True if getting the channel should pin it for the current user
            :returns a channel header, or False if the users_to was False
            :rtype : dict
        """
        ### Start changes ###
        channels = self.env['mail.channel']
        for partner in partners_list:
            if partner == partner2:
                continue
            
            # Partner of passed user and available partners add to list then set channel one by one 
            partners_to = [partner]
            partners_to.append(partner2)
            ### End changes ###
            # determine type according to the number of partner in the channel
            self.env.cr.execute("""
                SELECT P.channel_id as channel_id
                FROM mail_channel C, mail_channel_partner P
                WHERE P.channel_id = C.id
                    AND C.public LIKE 'private'
                    AND P.partner_id IN %s
                    AND channel_type LIKE 'chat'
                GROUP BY P.channel_id
                HAVING array_agg(P.partner_id ORDER BY P.partner_id) = %s
            """, (tuple(partners_to), sorted(list(partners_to)),))
            result = self.env.cr.dictfetchall()
            if result:
                # get the existing channel between the given partners
                channel = self.browse(result[0].get('channel_id'))
                # pin up the channel for the current partner
                if pin:
                    self.env['mail.channel.partner'].sudo().search([('partner_id', '=', partner2), ('channel_id', '=', channel.sudo().id)]).write({'is_pinned': True})
            else:
                # create a new one
                channel = self.create({
                    'channel_partner_ids': [(4, partner_id) for partner_id in partners_to],
                    'public': 'private',
                    'channel_type': 'chat',
                    'email_send': False,
                    'name': ', '.join(self.env['res.partner'].sudo().browse(partners_to).mapped('name')),
                })
                # broadcast the channel header to the other partner (not me)
                channel._broadcast(partners_to)
            channels = channels | channel
        ### Start changes ###
        channel_info = channels.channel_info()
        return channel_info[0] if partners_list and channel_info else False

    @api.model
    def channel_get(self, partners_list, pin=True):
        """ Get the canonical private channel between some partners, create it if needed.
            To reuse an old channel (conversation), this one must be private, and contains
            only the given partners.
            :param partners_to : list of res.partner ids to add to the conversation
            :param pin : True if getting the channel should pin it for the current user
            :returns a channel header, or False if the users_to was False
            :rtype : dict
        """
        ### Start changes ###
        channels = self.env['mail.channel']
        for partner in partners_list:
            # Partner of current user and available partners add to list then set channel one by one 
            partners_to = [partner]
            partners_to.append(self.env.user.partner_id.id)
            ### End changes ###
            # determine type according to the number of partner in the channel
            self.env.cr.execute("""
                SELECT P.channel_id as channel_id
                FROM mail_channel C, mail_channel_partner P
                WHERE P.channel_id = C.id
                    AND C.public LIKE 'private'
                    AND P.partner_id IN %s
                    AND channel_type LIKE 'chat'
                GROUP BY P.channel_id
                HAVING array_agg(P.partner_id ORDER BY P.partner_id) = %s
            """, (tuple(partners_to), sorted(list(partners_to)),))
            result = self.env.cr.dictfetchall()
            if result:
                # get the existing channel between the given partners
                channel = self.browse(result[0].get('channel_id'))
                # pin up the channel for the current partner
                if pin:
                    self.env['mail.channel.partner'].search([('partner_id', '=', self.env.user.partner_id.id), ('channel_id', '=', channel.id)]).write({'is_pinned': True})
            else:
                # create a new one
                channel = self.create({
                    'channel_partner_ids': [(4, partner_id) for partner_id in partners_to],
                    'public': 'private',
                    'channel_type': 'chat',
                    'email_send': False,
                    'name': ', '.join(self.env['res.partner'].sudo().browse(partners_to).mapped('name')),
                })
                # broadcast the channel header to the other partner (not me)
                channel._broadcast(partners_to)
            channels = channels | channel
        ### Start changes ###
        # It must be return one channel then worked
        channel_info = channels.channel_info()
        return channel_info[0] if partners_list and channel_info else False
        ### End changes ###
