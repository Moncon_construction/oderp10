# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import AccessError

class Partner(models.Model):
    _inherit = "res.partner"

    chat_name = fields.Char(compute='_compute_chat_name', string='Chat name')
    department_name = fields.Char(compute='_compute_department_name', string='Department name')
    department_id = fields.Integer(compute='_compute_department_id', string='Department name')
    emp_company_id = fields.Integer(compute='_compute_department_company_id', string='Department Company name')
    emp_company_name = fields.Char(compute='_compute_department_company_name', string='Department Company name')

    @api.multi
    def _compute_chat_name(self):
        for partner in self:
            chat_name = partner.name
            user_obj = self.env['res.users'].search([('partner_id', '=', partner.id)], limit=1)
            if user_obj:
                employee = self.env['hr.employee'].search([('user_id', '=', user_obj.id)], limit=1)
                if employee and employee.job_id:
                    chat_name += u' - %s'%(employee.job_id.name)
            partner.chat_name = chat_name

    @api.multi
    def _compute_partner_emp_company_id(self):
        for partner in self:
            partner.emp_company_id = -1
            user_obj = self.env['res.users'].search([('partner_id', '=', partner.id)], limit=1)
            if user_obj:
                employee = self.env['hr.employee'].search([('user_id', '=', user_obj.id)], limit=1)
                if employee and employee.department_id and employee.department_id.company_id:
                    partner.emp_company_id = employee.department_id.company_id
        
    @api.multi
    def _compute_department_company_id(self):
        for partner in self:
            partner.emp_company_id = -1
            user_obj = self.env['res.users'].search([('partner_id', '=', partner.id)], limit=1)
            if user_obj:
                employee = self.env['hr.employee'].search([('user_id', '=', user_obj.id)], limit=1)
                if employee and employee.department_id and employee.department_id.company_id:
                    partner.emp_company_id = employee.department_id.company_id
                elif employee:
                    partner.emp_company_id = employee.company_id
        
    @api.multi
    def _compute_department_company_name(self):
        for partner in self:
            partner.company_name = ''
            user_obj = self.env['res.users'].search([('partner_id', '=', partner.id)], limit=1)
            if user_obj:
                employee = self.env['hr.employee'].search([('user_id', '=', user_obj.id)], limit=1)
                if employee and employee.department_id and employee.department_id.company_id:
                    partner.emp_company_name = employee.department_id.company_id.partner_id.name if employee.department_id.company_id.partner_id else employee.department_id.company_id.name
                elif employee:
                    partner.emp_company_name = employee.company_id.partner_id.name if employee.company_id.partner_id else employee.company_id.name
                    
    @api.multi
    def _compute_department_name(self):
        for partner in self:
            partner.department_name = ''
            user_obj = self.env['res.users'].search([('partner_id', '=', partner.id)], limit=1)
            if user_obj:
                employee = self.env['hr.employee'].search([('user_id', '=', user_obj.id)], limit=1)
                if employee and employee.department_id:
                    partner.department_name = employee.department_id.name

    @api.multi
    def _compute_department_id(self):
        for partner in self:
            partner.department_id = -1
            user_obj = self.env['res.users'].search([('partner_id', '=', partner.id)], limit=1)
            if user_obj:
                employee = self.env['hr.employee'].search([('user_id', '=', user_obj.id)], limit=1)
                if employee and employee.department_id:
                    partner.department_id = employee.department_id

    @api.model
    def get_static_mention_suggestions(self):
        """ Extend the mail's static mention suggestions by adding the employees. """
        suggestions = []

        try:
            employee_group = self.env.ref('base.group_user')
            hr_suggestions = []

            for user in employee_group.users:
                user_name = user.name
                employee = self.env['hr.employee'].search([('user_id', '=', user.id)], limit=1)
                if employee and employee.job_id:
                    user_name += u' - %s'%(employee.job_id.name)
                hr_suggestions.append({'id': user.partner_id.id, 'name': user_name, 'email': user.email})

            suggestions.append(hr_suggestions)
            return suggestions
        except AccessError:
            return suggestions


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    @api.multi
    def archive(self):
        mail_channel_partner = self.env['mail.channel.partner']
        res = super(HrEmployee, self).archive()
        for emp in self:
            if emp.user_id:
                # Ажилтан идэвхгүй болгох үед сувгаас хасдаг болгов.
                mail_channel_partners = mail_channel_partner.search([('partner_id', '=', emp.user_id.partner_id.id)])
                for partner in mail_channel_partners:
                    partner.unlink()
        return res