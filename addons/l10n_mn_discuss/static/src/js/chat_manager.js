odoo.define('l10n_mn_chat.chat_manager', function (require) {
    "use strict";

    var chat_manager = require('mail.chat_manager');

    chat_manager.make_channel = (function(old_make_channel) {
        function extended_make_channel(data,options) {
            var channel = old_make_channel(data,options);

            if (_.size(data.direct_partner) > 0) {
            	channel.emp_company_id = data.direct_partner[0].emp_company_id;
                channel.emp_company_name = data.direct_partner[0].emp_company_name;
                channel.department_id = data.direct_partner[0].department_id;
                channel.department_name = data.direct_partner[0].department_name;
            }
            return channel;
        }

        return extended_make_channel;
    })(chat_manager.make_channel);
});