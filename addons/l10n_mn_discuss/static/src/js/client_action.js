odoo.define('l10n_mn_chat.chat_client_action', function (require) {
    "use strict";

    require('mail.chat_client_action');
    var chat_manager = require('mail.chat_manager');
    var core = require('web.core');

    var _t = core._t;

    core.action_registry.get('mail.chat.instant_messaging').include({
        render_sidebar: function () {
            var self = this;
            
            var channels = chat_manager.get_channels();
            var direct_channels = [];
            var companies = [];
            var companies_states;
            var department_states;

            if(self.check_local_storage()) {
            	var comp_item = localStorage.getItem("companies");
            	var dep_item = localStorage.getItem("departments");
                if(comp_item == null) {
                    companies_states = {};
                } else {
                    companies_states = JSON.parse(comp_item);
                }
                if(dep_item == null) {
                	department_states = {};
                } else {
                	department_states = JSON.parse(dep_item);
                }
            }
            
//        	Sort channels by its name 
            channels = channels.sort(self.channel_sort);
            
//          First create companies list. This list contains distinct companies' dictionary values.
            var added_company_ids = [];
            for (var i = 0; i < channels.length; i++) {
            	var channel = channels[i];
            	if (channel.type == 'dm') {
	            	if (!(added_company_ids.includes(channel.emp_company_id))){
	            		added_company_ids.push(channel.emp_company_id);
	            		companies.push({
    	                	"company_id": channel.emp_company_id,
    	                    "company_name": channel.emp_company_name,
    	                    "unread_counter": 0,
    	                    "state": companies_states[channel.emp_company_id],
    	                    "departments": [],
    	                    "dep_counter": 1,
    	                    "comp_channel_counter": 1
    	                });
	            	}else{
	            		for (var l = 0; l < companies.length; l++) {
	            			if (companies[l]['company_id'] == channel.emp_company_id){
	            				companies[l]['comp_channel_counter']++;
	            			}
	            		}
	            	}
            	}
            }
            
//          Second create department list. This list contains distinct channels values.
            for (var i = 0; i < companies.length; i++) {
            	var company_id = companies[i]['company_id'];
            	var added_dep_ids = [];
            	for (var j = 0; j < channels.length; j++) {
                	var channel = channels[j];
                	if (channel.type == 'dm') {
                		var emp_company_id = channel.emp_company_id;
		            	if ((company_id == emp_company_id)) {
		            		if (!(added_dep_ids.includes(channel.department_id))){
		            			added_dep_ids.push(channel.department_id);
			            		companies[i]['departments'].push({
			            			"department_id": channel.department_id,
		    	                    "department_name": channel.department_name,
		    	                    "unread_counter": 0,
		    	                    "state": department_states[channel.department_id],
		    	                    "channels": [],
		    	                    "channel_counter": 1
			            		});
		            		}else{
		            			for (var l = 0; l < companies.length; l++) {
			            			if (companies[l]['company_id'] == channel.emp_company_id){
			            				for (var m = 0; m < companies[l]['departments'].length; m++) {
			            					if (companies[l]['departments'][m]['department_id'] == channel.department_id){
			            						companies[l]['departments'][m]['channel_counter']++;
			    	            			}
			            				}
			            			}
			            		}
		            		}
		            	}
                	}
            	}
            }
            
//          Third create channel list. This list contains detailed information of itself.
            for (var i = 0; i < companies.length; i++) {
            	var company_id = companies[i]['company_id'];
//            	First of third: sort departments by its name
            	companies[i]['departments'] = companies[i]['departments'].sort(self.department_sort);
            	companies[i]['dep_counter'] = companies[i]['departments'].length;
            	for (var j = 0; j < companies[i]['departments'].length; j++) {
            		var department_id = companies[i]['departments'][j]['department_id'];
            		var added_channel_ids = [];
            		for (var x = 0; x < channels.length; x++) {
            			var channel = channels[x];
                    	if (channel.type == 'dm') {
                    		var emp_company_id = channel.emp_company_id;
                    		var dep_id = channel.department_id;
    		            	if (company_id == emp_company_id && department_id == dep_id && !(added_channel_ids.includes(channel.id))) {
    		            		added_channel_ids.push(channel.id);
    		            		companies[i]['departments'][j]['channels'].push(channel);
    		            		companies[i]['unread_counter'] += channel.unread_counter ? 1 : 0;
    		            		companies[i]['departments'][j]['unread_counter'] += channel.unread_counter ? 1 : 0;
    		            	}
                    	}
            		}
            	}
            }
            
//        	Sort companies by its name 
            companies = companies.sort(self.company_sort);
            
            var $sidebar = this._render_sidebar({
                active_channel_id: this.channel ? this.channel.id: undefined,
                channels: channels,
                company_channels: companies,
                needaction_counter: chat_manager.get_needaction_counter(),
                starred_counter: chat_manager.get_starred_counter(),
            });
            this.$(".o_mail_chat_sidebar").html($sidebar.contents());
    
            this.$('.o_mail_add_channel[data-type=public]').find("input").autocomplete({
                source: function(request, response) {
                    self.last_search_val = _.escape(request.term);
                    self.do_search_channel(self.last_search_val).done(function(result){
                        result.push({
                            'label':  _.str.sprintf('<strong>'+_t("Create %s")+'</strong>', '<em>"#'+self.last_search_val+'"</em>'),
                            'value': '_create',
                        });
                        response(result);
                    });
                },
                select: function(event, ui) {
                    if (self.last_search_val) {
                        if (ui.item.value === '_create') {
                            chat_manager.create_channel(self.last_search_val, "public");
                        } else {
                            chat_manager.join_channel(ui.item.id);
                        }
                    }
                },
                focus: function(event) {
                    event.preventDefault();
                },
                html: true,
            });
    
            this.$('.o_mail_add_channel[data-type=dm]').find("input").autocomplete({
                source: function(request, response) {
                    self.last_search_val = _.escape(request.term);
                    chat_manager.search_partner(self.last_search_val, 10).done(response);
                },
                select: function(event, ui) {
                    var partner_id = ui.item.id;
                    var dm = chat_manager.get_dm_from_partner_id(partner_id);
                    if (dm) {
                        self.set_channel(dm);
                    } else {
                        chat_manager.create_channel(partner_id, "dm");
                    }
                    // clear the input
                    $(this).val('');
                    return false;
                },
                focus: function(event) {
                    event.preventDefault();
                },
            });
    
            this.$('.o_mail_add_channel[data-type=private]').find("input").on('keyup', this, function (event) {
                var name = _.escape($(event.target).val());
                if(event.which === $.ui.keyCode.ENTER && name) {
                    chat_manager.create_channel(name, "private");
                }
            });
            
            this.$('.oe_menu_toggler_company').on('click', this, function() {
                var current_comp_id;
                if($(this).hasClass("oe_menu_opened")) {
                    $(this).removeClass("oe_menu_opened");
                    $(this).parent().find(".o_department_item").addClass("o_hidden");
                    $(this).parent().find(".o_company_deps").addClass("o_hidden");
                    current_comp_id = $(this).attr("company-id");
                    self.set_company_state(current_comp_id, 0);
                } else {
                    $(this).addClass("oe_menu_opened");
                    $(this).parent().find(".o_department_item").removeClass("o_hidden");
                    $(this).parent().find(".o_company_deps").removeClass("o_hidden");
                    current_comp_id = $(this).attr("company-id");
                    self.set_company_state(current_comp_id, 1);
                }
            });
            
            this.$('.oe_menu_toggler').on('click', this, function() {
                var current_dep_id;
                if($(this).hasClass("oe_menu_opened")) {
                    $(this).removeClass("oe_menu_opened");
                    $(this).parent().find(".o_mail_chat_channel_item").addClass("o_hidden");
                    current_dep_id = $(this).attr("department-id");
                    self.set_dep_state(current_dep_id, 0);
                } else {
                    $(this).addClass("oe_menu_opened");
                    $(this).parent().find(".o_mail_chat_channel_item").removeClass("o_hidden");
                    current_dep_id = $(this).attr("department-id");
                    self.set_dep_state(current_dep_id, 1);
                }
            });
        },
        check_local_storage: function() {
            if (typeof(window.localStorage) !== "undefined") {
                return true;
            }
            return false;
        },
        set_company_state: function(id, state) {
            if(this.check_local_storage()) {
                var item = localStorage.getItem("companies");
                if(item == null) {
                    item = {};
                } else {
                    item = JSON.parse(item);
                }
                item[id] = state;
                localStorage.setItem("companies", JSON.stringify(item));
            }
        },
        set_dep_state: function(id, state) {
            if(this.check_local_storage()) {
                var item = localStorage.getItem("departments");
                if(item == null) {
                    item = {};
                } else {
                    item = JSON.parse(item);
                }
                item[id] = state;
                localStorage.setItem("departments", JSON.stringify(item));
            }
        },
        channel_sort: function(a, b){
        	var nameA = a.name.toString().toLowerCase();
        	var nameB = b.name.toString().toLowerCase();
        	if (nameA < nameB) //sort string ascending
                return -1 
            if (nameA > nameB)
                return 1
            return 0 //default return value (no sorting)
        },
        company_sort: function(a, b){
        	var nameA = a.company_name.toString().toLowerCase();
        	var nameB = b.company_name.toString().toLowerCase();
        	if (nameA < nameB) //sort string ascending
                return -1 
            if (nameA > nameB)
                return 1
            return 0 //default return value (no sorting)
        },
        department_sort: function(a, b){
        	var nameA = a.department_name.toString().toLowerCase();
        	var nameB = b.department_name.toString().toLowerCase();
        	if (nameA < nameB) //sort string ascending
                return -1 
            if (nameA > nameB)
                return 1
            return 0 //default return value (no sorting)
        }
    });
});