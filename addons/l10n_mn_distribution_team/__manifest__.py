# -*- coding: utf-8 -*-
{
    'name': 'Mongolian Distribution Teams',
    'version': '1.0',
    'author': 'Asterisk Technologies LLC',
    'category': 'Mongolian Modules',
    'summary': 'Distribution Team',
    'description': """
Using this application you can manage Distribution Team  with CRM and/or Sales 
=======================================================================
 """,
    'website': 'http://asterisk-tech.mn',
    'depends': ['base_setup',
                'sales_team',
                'l10n_mn_sale',
                'l10n_mn_stock_account',
                'l10n_mn_sales_report',
                ],
    'data': [
            'views/res_config_views.xml',
            'security/distribution_team_security.xml',
            'security/ir.model.access.csv',
            'views/sale_order_views.xml',
            'views/account_invoice_views.xml',
            'views/distribution_section_views.xml',
            'views/res_partner_views.xml',
            'views/stock_picking_views.xml',
            'wizard/sales_report_view.xml'
             ],
    'demo': [],
    'installable': True,
    'auto_install': False,
}