# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class AccountInvoice(models.Model):
    _inherit = "account.invoice"
    
    distribution_section_id = fields.Many2one('distribution.section', 'Distribution Team')