# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class DistributionSection(models.Model):
    _name = "distribution.section"
    _inherit = ['mail.thread']
    _description = "Distribution Teams"
    
    name = fields.Char('Distribution Team', size=64, required=True, translate=True)
    code = fields.Char('Code', size=8)
    limit = fields.Integer('Limit')
    active = fields.Boolean('Active', help="If the active field is set to "\
                    "false, it will allow you to hide the distribution team without removing it.", default=True)
    user_id = fields.Many2one('res.users', 'Team Leader')
    member_ids = fields.Many2many('res.users', 'distribution_member_rel', 'section_id', 'member_id', 'Team Members')
    reply_to = fields.Char('Reply-To', size=64, help="The email address put in the 'Reply-To' of all emails sent by Odoo about cases in this distribution team")
    parent_id = fields.Many2one('distribution.section', 'Parent Team')
    child_ids = fields.One2many('distribution.section', 'parent_id', 'Child Teams')
    note = fields.Text('Description')
    color = fields.Integer('Color Index')
    count_quotation = fields.Integer(compute='_compute_distribution_team_count')
    count_sale_order = fields.Integer(compute='_compute_distribution_team_count2')
    count_account_invoice = fields.Integer(compute='_compute_distribution_team_count3')
            
    _sql_constraints = [
        ('code_uniq', 'unique (code)', 'The code of the sales team must be unique !')
    ]
    
    @api.multi
    def _compute_distribution_team_count(self):
        data = self.env['sale.order'].read_group([('state', 'in', ('draft', 'sent', 'cancel')), ('distribution_section_id', 'in', self.ids)],
                ['distribution_section_id'], ['distribution_section_id'])
        count_data = dict((item['distribution_section_id'][0], item['distribution_section_id_count']) for item in data)
        for order in self:
            order.count_quotation = count_data.get(order.id, 0)
                
    @api.multi
    def _compute_distribution_team_count2(self):
        data = self.env['sale.order'].read_group([('state', 'in', ('sale', 'done')), ('distribution_section_id', 'in', self.ids)],
                ['distribution_section_id'], ['distribution_section_id'])
        count_data = dict((item['distribution_section_id'][0], item['distribution_section_id_count']) for item in data)
        for order in self:
            order.count_sale_order = count_data.get(order.id, 0)
    
    @api.multi
    def _compute_distribution_team_count3(self):
        data = self.env['account.invoice'].read_group([('state', 'not in', ('draft', 'cancel')), ('distribution_section_id', 'in', self.ids)],
                ['distribution_section_id'], ['distribution_section_id'])
        count_data = dict((item['distribution_section_id'][0], item['distribution_section_id_count']) for item in data)
        for order in self:
            order.count_account_invoice = count_data.get(order.id, 0)