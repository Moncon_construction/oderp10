# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class ResCompany(models.Model):
    _inherit = 'res.company'

    group_multi_distributionteams = fields.Boolean("Organize Distribution activities into multiple Distribution Teams")
