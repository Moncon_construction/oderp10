# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class SaleConfigSettings(models.TransientModel):
    _inherit = 'sale.config.settings'

    group_multi_distributionteams = fields.Boolean(string="Organize Distribution activities into multiple Distribution Teams", implied_group='l10n_mn_distribution_team.group_multi_distributionteams')
