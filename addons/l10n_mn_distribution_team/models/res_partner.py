# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class ResPartner(models.Model):
    _inherit = 'res.partner'
    
    distribution_section_id = fields.Many2one('distribution.section', 'Distribution Team')
