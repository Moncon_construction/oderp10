# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class SaleOrder(models.Model):
    _inherit = "sale.order"
    
    distribution_section_id = fields.Many2one('distribution.section', 'Distribution Team')
    
    @api.onchange('partner_id')
    def onchange_partner_id(self):
        res = super(SaleOrder, self).onchange_partner_id()
        #New code start: Харилцагчийн түгээлтийн багыг борлуулалтын захиалга дээр авна
        if self.partner_id.distribution_section_id:
            self.distribution_section_id = self.partner_id.distribution_section_id.id
        return res
    
    """Үүссэн агуулахын хөдөлгөөн дээр борлуулалтын захиалгын түгээлтийн багыг хөтөлнө"""
    @api.multi
    def action_confirm(self):
        res = super(SaleOrder, self).action_confirm()
        picking_ids = self.env['stock.picking'].browse(self.picking_ids.ids)
        #New code start: 
        for picking in picking_ids:
            picking.distribution_section_id = self.distribution_section_id.id
        return res
    
    @api.multi
    def _prepare_invoice(self):
        res = super(SaleOrder, self)._prepare_invoice()
        res.update({'distribution_section_id' : self.distribution_section_id.id})
        return res
