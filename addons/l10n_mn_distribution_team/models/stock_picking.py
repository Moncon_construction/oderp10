# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class StockPicking(models.Model):
    _inherit = "stock.picking"

    distribution_section_id = fields.Many2one('distribution.section', 'Distribution Team')
        