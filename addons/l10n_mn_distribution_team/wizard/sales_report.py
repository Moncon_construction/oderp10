# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
import xlsxwriter
from odoo import api, fields, models, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from odoo.exceptions import UserError
import pytz
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

class ReportSalesReport(models.TransientModel):
    """
        Борлуулалтын дэлгэрэнгүй тайлан
    """

    _inherit = 'report.sales'

    stage_one = fields.Selection(selection_add=[('distribution', 'Distribution Team')])
    stage_two = fields.Selection(selection_add=[('distribution', 'Distribution Team')])
    stage_three = fields.Selection(selection_add=[('distribution', 'Distribution Team')])
    distribution_team_ids = fields.Many2many('distribution.section', string='Distribution Team')
    
#     #Тайлангийн бүлэглэлтийн шатыг шалгахад түгээлтийн багийг нэмэхээр дарж бичив
    @api.multi
    def _stage_check(self, state, check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, check_distribution_section_ids, 
                    warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum):  
     
        if state == 'stage_one':
            del check_warehouse_ids[:]
            del check_location_ids[:]
            del check_cat_ids[:]
            del check_brand_ids[:]
            del check_salesman_ids[:]
            del check_salesteam_ids[:]
            del check_customer_ids[:]
            del check_supplier_ids[:]
            del check_distribution_section_ids[:]
            warehouse_sum = self._fill_zero(warehouse_sum)
            location_sum = self._fill_zero(location_sum)
            cat_sum = self._fill_zero(cat_sum)
            brand_sum = self._fill_zero(brand_sum)
            salesman_sum = self._fill_zero(salesman_sum)
            salesteam_sum = self._fill_zero(salesteam_sum)
            customer_sum = self._fill_zero(customer_sum)
            supplier_sum = self._fill_zero(supplier_sum)
            distribution_section_sum = self._fill_zero(distribution_section_sum)
        elif state == 'stage_two':
            check_warehouse_ids = [] if self.stage_one != 'warehouse' else check_warehouse_ids
            check_location_ids = [] if self.stage_one != 'location' else check_location_ids
            check_cat_ids = [] if self.stage_one != 'categ' else check_cat_ids
            check_brand_ids = [] if self.stage_one != 'brand' else check_brand_ids
            check_salesman_ids = [] if self.stage_one != 'salesman' else check_salesman_ids
            check_salesteam_ids = [] if self.stage_one != 'salesteam' else check_salesteam_ids
            check_customer_ids = [] if self.stage_one != 'customer' else check_customer_ids
            check_supplier_ids = [] if self.stage_one != 'supplier' else check_supplier_ids
            check_distribution_section_ids = [] if self.stage_one != 'distribution' else check_distribution_section_ids
            warehouse_sum = self._fill_zero(warehouse_sum) if self.stage_one != 'warehouse' else warehouse_sum
            location_sum = self._fill_zero(location_sum) if self.stage_one != 'location' else location_sum
            cat_sum = self._fill_zero(cat_sum) if self.stage_one != 'categ' else cat_sum
            brand_sum = self._fill_zero(brand_sum) if self.stage_one != 'brand' else brand_sum
            salesman_sum = self._fill_zero(salesman_sum) if self.stage_one != 'salesman' else salesman_sum
            salesteam_sum = self._fill_zero(salesteam_sum) if self.stage_one != 'salesteam' else salesteam_sum
            customer_sum = self._fill_zero(customer_sum) if self.stage_one != 'customer' else customer_sum
            distribution_section_sum = self._fill_zero(distribution_section_sum) if self.stage_one != 'distribution' else distribution_section_sum
        else:
            check_warehouse_ids = [] if self.stage_one != 'warehouse' or self.stage_two != 'warehouse' else check_warehouse_ids
            check_location_ids = [] if self.stage_one != 'location' or self.stage_two != 'location' else check_location_ids
            check_cat_ids = [] if self.stage_one != 'categ' or self.stage_two != 'categ' else check_cat_ids
            check_brand_ids = [] if self.stage_one != 'brand' or self.stage_two != 'brand' else check_brand_ids
            check_salesman_ids = [] if self.stage_one != 'salesman' or self.stage_two != 'salesman' else check_salesman_ids
            check_salesteam_ids = [] if self.stage_one != 'salesteam' else check_salesteam_ids
            check_customer_ids = [] if self.stage_one != 'customer' else check_customer_ids
            check_supplier_ids = [] if self.stage_one != 'supplier' else check_supplier_ids
            check_distribution_section_ids = [] if self.stage_one != 'distribution' else check_distribution_section_ids
            warehouse_sum = self._fill_zero(warehouse_sum) if self.stage_one != 'warehouse' and self.stage_two != 'warehouse' else warehouse_sum
            location_sum = self._fill_zero(location_sum) if self.stage_one != 'location' and self.stage_two != 'location' else location_sum
            cat_sum = self._fill_zero(cat_sum) if self.stage_one != 'categ' and self.stage_two != 'categ' else cat_sum
            brand_sum = self._fill_zero(brand_sum) if self.stage_one != 'brand' and self.stage_two != 'brand' else brand_sum
            salesman_sum = self._fill_zero(salesman_sum) if self.stage_one != 'salesman' and self.stage_two != 'salesman' else salesman_sum
            salesteam_sum = self._fill_zero(salesteam_sum) if self.stage_one != 'salesteam' and self.stage_two != 'salesteam' else salesteam_sum
            customer_sum = self._fill_zero(customer_sum) if self.stage_one != 'customer' and self.stage_two != 'customer' else customer_sum
            supplier_sum = self._fill_zero(supplier_sum) if self.stage_one != 'supplier' and self.stage_two != 'supplier' else supplier_sum
            distribution_section_sum = self._fill_zero(distribution_section_sum) if self.stage_one != 'distribution' and self.stage_two != 'distribution' else distribution_section_sum
        return check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, check_distribution_section_ids, \
                    warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum
            
    """Бүлэглэлтийг тооцож зурахад түгээлтийн багийг нэмэхээр дарж бичив"""
    def stage_check(self, state, stage_name, record,
                    check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, check_distribution_section_ids, 
                    warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum, 
                    sheet, lot, format_content_text_color, format_sub_text_float,
                    last_ware_rowx, last_location_rowx, last_cat_rowx, last_brand_rowx, last_salesman_rowx, last_salesteam_rowx, last_customer_rowx, last_supplier_rowx, 
                    last_distribution_section_rowx, rowx, sub_cat, sale_tax_pay):
        if stage_name == 'warehouse':
            if 'warehouse_id' in record and record['warehouse_id'] not in check_warehouse_ids:
                check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids,  check_distribution_section_ids, \
                    warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum, \
                    = self._stage_check(state, check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, check_distribution_section_ids, 
                    warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum)
                name = self.env['stock.warehouse'].search([('id', '=', record['warehouse_id'])]).name
                self.get_sub_header(sheet, rowx, _('Warehouse ') + name, record.get('lot_name', False) if lot != 0 else
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, format_content_text_color, format_sub_text_float, True, sale_tax_pay)
                last_ware_rowx = rowx
                rowx += 1
                sub_cat += 1
        if stage_name == 'location':
            if not self.invis_location:
                if record['location_id'] not in check_location_ids:
                    check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, check_distribution_section_ids,  \
                    warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum, \
                    = self._stage_check(state, check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, check_distribution_section_ids, 
                    warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum)
                    name = self.env['stock.location'].search([('id', '=', record['location_id'])]).name
                    self.get_sub_header(sheet, rowx, _('Location ') + name, record.get('lot_name', False) if lot != 0 else
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, format_content_text_color, format_sub_text_float, True, sale_tax_pay)
                    last_location_rowx = rowx
                    rowx += 1
                    sub_cat += 1
        if stage_name == 'categ':
            if record['cat_id'] not in check_cat_ids:
                check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids,  check_distribution_section_ids, \
                    warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum, \
                    = self._stage_check(state, check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, check_distribution_section_ids, 
                    warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum)
                name = self.env['product.category'].search([('id', '=', record['cat_id'])]).name or 'Тодорхойгүй'
                self.get_sub_header(sheet, rowx, _('Category ') + name, record.get('lot_name', False) if lot != 0 else
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, format_content_text_color, format_sub_text_float, True, sale_tax_pay)
                last_cat_rowx = rowx
                rowx += 1
                sub_cat += 1
        if stage_name == 'brand':
            if record['brand'] not in check_brand_ids:
                check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids,  check_distribution_section_ids, \
                    warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum, \
                    = self._stage_check(state, check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, check_distribution_section_ids, 
                    warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum)
                name = self.env['product.brand'].search([('id', '=', record['brand'])]).brand_name or 'Тодорхойгүй'
                self.get_sub_header(sheet, rowx, _('Brand ') + name, record.get('lot_name', False) if lot != 0 else
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, format_content_text_color, format_sub_text_float, True, sale_tax_pay)
                last_brand_rowx = rowx
                rowx += 1
                sub_cat += 1
        if stage_name == 'salesman':
            if record['salesman_id'] not in check_salesman_ids:
                check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids,  check_distribution_section_ids, \
                    warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum \
                    = self._stage_check(state, check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, check_distribution_section_ids, 
                    warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum)
                name = self.env['res.users'].search([('id', '=', record['salesman_id'])]).name or 'Тодорхойгүй'
                self.get_sub_header(sheet, rowx, _('Salesman ') + name, record.get('lot_name', False) if lot != 0 else
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, format_content_text_color, format_sub_text_float, True, sale_tax_pay)
                last_salesman_rowx = rowx
                rowx += 1
                sub_cat += 1
        if stage_name == 'salesteam':
            if record['salesteam_id'] not in check_salesteam_ids:
                check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, check_distribution_section_ids,  \
                    warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum, \
                    = self._stage_check(state, check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, check_distribution_section_ids, 
                    warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum)
                if record['salesteam_id'] == -1:
                    name = 'Тодорхойгүй'
                else:
                    name = self.env['crm.team'].search([('id', '=', record['salesteam_id'])]).name or 'Тодорхойгүй'
                self.get_sub_header(sheet, rowx, _('Salesteam ') + name, record.get('lot_name', False) if lot != 0 else
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, format_content_text_color, format_sub_text_float, True, sale_tax_pay)
                last_salesteam_rowx = rowx
                rowx += 1
                sub_cat += 1
        if stage_name == 'customer':
            if record['customer_id'] not in check_customer_ids:
                check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, check_distribution_section_ids,  \
                    warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum, \
                    = self._stage_check(state, check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, check_distribution_section_ids, 
                    warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum)
                if record['customer_id'] == -1:
                    name = 'Тодорхойгүй'
                else:
                    name = self.env['res.partner'].search([('id', '=', record['customer_id'])]).name or 'Тодорхойгүй'
                self.get_sub_header(sheet, rowx, _('Customer ') + name, record.get('lot_name', False) if lot != 0 else
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, format_content_text_color, format_sub_text_float, True, sale_tax_pay)
                last_customer_rowx = rowx
                rowx += 1
                sub_cat += 1
        if stage_name == 'supplier':
            if record['supplier_id'] not in check_supplier_ids:
                check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, check_distribution_section_ids,  \
                    warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum, \
                    = self._stage_check(state, check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, check_distribution_section_ids, 
                    warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum)
                name = self.env['res.partner'].search([('id', '=', record['supplier_id'])]).name or 'Тодорхойгүй'
                self.get_sub_header(sheet, rowx, _('Supplier ') + name, record.get('lot_name', False) if lot != 0 else
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, format_content_text_color, format_sub_text_float, True, sale_tax_pay)
                last_supplier_rowx = rowx
                rowx += 1
                sub_cat += 1
        if stage_name == 'distribution':
            if record['distribution_section_id'] not in check_distribution_section_ids:
                check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, check_distribution_section_ids,  \
                    warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum, \
                    = self._stage_check(state, check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, check_distribution_section_ids, 
                    warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum)
                name = self.env['distribution.section'].search([('id', '=', record['distribution_section_id'])]).name or 'Тодорхойгүй'
                self.get_sub_header(sheet, rowx, _('Distribution Section ') + name, record.get('lot_name', False) if lot != 0 else
                                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, format_content_text_color, format_sub_text_float, True, sale_tax_pay)
                last_distribution_section_rowx = rowx
                rowx += 1
                sub_cat += 1
         
        return check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids,  check_distribution_section_ids, \
            warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum, \
            last_ware_rowx, last_location_rowx, last_cat_rowx, last_brand_rowx, last_salesman_rowx, last_salesteam_rowx, last_customer_rowx, last_supplier_rowx, last_distribution_section_rowx, \
            rowx, sub_cat
            
    # Тайлангийн дэд бүлгийн нийлбэр түгээлтийн багийг нэмэв
    def get_dict(self):
        warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum = super(ReportSalesReport, self).get_dict()
        distribution_section_sum = {'qty': 0, 'sub_total': 0, 'tax': 0, 'total': 0, 'without_tax_discount': 0, 'tax_discount': 0, 'with_tax_discount': 0,  'rev_qty': 0, 'rev_sub_total': 0,
                        'rev_tax': 0, 'rev_total': 0, 'net_qty': 0, 'net_sub_total': 0, 'net_tax': 0,
                        'net_total': 0, 'net_cost_price': 0,  'unit_price': 0, 'net_profit': 0, 'profit_of_unit': 0, 'percent': 0}
        return warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum
    
    # Тайлангийн дэд бүлгийн нийлбэрийг олоход түгээлтийн багийг нэмэхээр дарж бичив
    def get_sum_dict(self, sheet, record, warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum, sale_qty, price, tax, price_tax, without_tax_discount, tax_discount, with_tax_discount,\
                        rev_qty, rev_price, rev_tax, rev_price_tax, net_qty, net_price, net_tax, net_price_tax, net_cost_price, cost_price_unit, net_profit, profit_of_unit, percent,  last_ware_rowx, \
                        last_location_rowx, last_cat_rowx, last_brand_rowx, last_salesman_rowx, last_salesteam_rowx, last_customer_rowx, last_supplier_rowx, last_distribution_section_rowx, lot, sale_tax_pay, format_title, format_sub_text_float ):
            warehouse_sum = self._fill_sum_value(warehouse_sum, sale_qty, price, tax, price_tax, without_tax_discount, tax_discount, with_tax_discount,
                                                 rev_qty, rev_price, rev_tax, rev_price_tax, net_qty, net_price,
                                                 net_tax, net_price_tax, net_cost_price, cost_price_unit, net_profit, profit_of_unit, percent)
            location_sum = self._fill_sum_value(location_sum, sale_qty, price, tax, price_tax, without_tax_discount, tax_discount, with_tax_discount,
                                                rev_qty, rev_price, rev_tax, rev_price_tax, net_qty, net_price,
                                                net_tax, net_price_tax, net_cost_price,  cost_price_unit, net_profit, profit_of_unit, percent)
            cat_sum = self._fill_sum_value(cat_sum, sale_qty, price, tax, price_tax, without_tax_discount, tax_discount, with_tax_discount,
                                           rev_qty, rev_price, rev_tax, rev_price_tax, net_qty,
                                           net_price, net_tax, net_price_tax, net_cost_price,  cost_price_unit, net_profit, profit_of_unit, percent)
            brand_sum = self._fill_sum_value(brand_sum, sale_qty, price, tax, price_tax, without_tax_discount, tax_discount, with_tax_discount,
                                             rev_qty, rev_price, rev_tax, rev_price_tax, net_qty,
                                             net_price, net_tax, net_price_tax, net_cost_price,  cost_price_unit, net_profit, profit_of_unit, percent)
            salesman_sum = self._fill_sum_value(salesman_sum, sale_qty, price, tax, price_tax, without_tax_discount, tax_discount, with_tax_discount,
                                                rev_qty, rev_price, rev_tax, rev_price_tax, net_qty, net_price,
                                                net_tax, net_price_tax, net_cost_price,  cost_price_unit, net_profit, profit_of_unit, percent)
            salesteam_sum = self._fill_sum_value(salesteam_sum, sale_qty, price, tax, price_tax, without_tax_discount, tax_discount, with_tax_discount,
                                                 rev_qty, rev_price, rev_tax, rev_price_tax, net_qty, net_price,
                                                 net_tax, net_price_tax, net_cost_price,  cost_price_unit, net_profit, profit_of_unit, percent)
            customer_sum = self._fill_sum_value(customer_sum, sale_qty, price, tax, price_tax, without_tax_discount, tax_discount, with_tax_discount,
                                                rev_qty, rev_price, rev_tax, rev_price_tax, net_qty, net_price,
                                                net_tax, net_price_tax, net_cost_price,  cost_price_unit, net_profit, profit_of_unit, percent)
            supplier_sum = self._fill_sum_value(supplier_sum, sale_qty, price, tax, price_tax, without_tax_discount, tax_discount, with_tax_discount,
                                                rev_qty, rev_price, rev_tax, rev_price_tax, net_qty, net_price,
                                                net_tax, net_price_tax, net_cost_price,  cost_price_unit, net_profit, profit_of_unit, percent)
                        
            distribution_section_sum = self._fill_sum_value(distribution_section_sum, sale_qty, price, tax, price_tax, without_tax_discount, tax_discount, with_tax_discount,
                                                    rev_qty, rev_price, rev_tax, rev_price_tax, net_qty, net_price,
                                                    net_tax, net_price_tax, net_cost_price,  cost_price_unit, net_profit, profit_of_unit, percent)

            if last_ware_rowx != -1:
                self.get_sub_header(sheet, last_ware_rowx, '', record['lot_name'] if lot != 0 else '', warehouse_sum['qty'],
                                        warehouse_sum['sub_total'], warehouse_sum['tax'], warehouse_sum['total'], warehouse_sum['without_tax_discount'], warehouse_sum['tax_discount'], warehouse_sum['with_tax_discount'],
                                        warehouse_sum['rev_qty'], warehouse_sum['rev_sub_total'], warehouse_sum['rev_tax'], warehouse_sum['rev_total'],
                                        warehouse_sum['net_qty'], warehouse_sum['net_sub_total'], warehouse_sum['net_tax'], warehouse_sum['net_total'], warehouse_sum['net_cost_price'], 
                                        warehouse_sum['unit_price'], warehouse_sum['net_profit'], warehouse_sum['profit_of_unit'], warehouse_sum['percent'], format_title, format_sub_text_float, False, sale_tax_pay)
            if last_location_rowx != -1:
                self.get_sub_header(sheet, last_location_rowx, '', record['lot_name'] if lot != 0 else '', location_sum['qty'], 
                                        location_sum['sub_total'], location_sum['tax'],location_sum['total'], location_sum['without_tax_discount'], location_sum['tax_discount'], location_sum['with_tax_discount'], 
                                        location_sum['rev_qty'], location_sum['rev_sub_total'], location_sum['rev_tax'], location_sum['rev_total'],
                                        location_sum['net_qty'], location_sum['net_sub_total'], location_sum['net_tax'], location_sum['net_total'], location_sum['net_cost_price'], 
                                        location_sum['unit_price'], location_sum['net_profit'], location_sum['profit_of_unit'], location_sum['percent'], format_title, format_sub_text_float, False, sale_tax_pay)
            if last_cat_rowx != -1:
                self.get_sub_header(sheet, last_cat_rowx, '', record['lot_name'] if lot != 0 else '', cat_sum['qty'],
                                        cat_sum['sub_total'], cat_sum['tax'], cat_sum['total'], cat_sum['without_tax_discount'], cat_sum['tax_discount'], cat_sum['with_tax_discount'],
                                        cat_sum['rev_qty'], cat_sum['rev_sub_total'],cat_sum['rev_tax'], cat_sum['rev_total'], 
                                        cat_sum['net_qty'], cat_sum['net_sub_total'], cat_sum['net_tax'], cat_sum['net_total'], cat_sum['net_cost_price'], 
                                        cat_sum['unit_price'], cat_sum['net_profit'], cat_sum['profit_of_unit'], cat_sum['percent'], format_title, format_sub_text_float, False, sale_tax_pay)
            if last_brand_rowx != -1:
                self.get_sub_header(sheet, last_brand_rowx, '', record['lot_name'] if lot != 0 else '', brand_sum['qty'], 
                                        brand_sum['sub_total'], brand_sum['tax'], brand_sum['total'], brand_sum['without_tax_discount'], brand_sum['tax_discount'], brand_sum['with_tax_discount'],
                                        brand_sum['rev_qty'], brand_sum['rev_sub_total'], brand_sum['rev_tax'], brand_sum['rev_total'], 
                                        brand_sum['net_qty'], brand_sum['net_sub_total'], brand_sum['net_tax'], brand_sum['net_total'], brand_sum['net_cost_price'], 
                                        brand_sum['unit_price'], brand_sum['net_profit'], brand_sum['profit_of_unit'], brand_sum['percent'], format_title, format_sub_text_float, False, sale_tax_pay)

            if last_salesman_rowx != -1:
                self.get_sub_header(sheet, last_salesman_rowx, '', record['lot_name'] if lot != 0 else '', salesman_sum['qty'], 
                                        salesman_sum['sub_total'], salesman_sum['tax'], salesman_sum['total'], salesman_sum['without_tax_discount'], salesman_sum['tax_discount'], salesman_sum['with_tax_discount'],
                                        salesman_sum['rev_qty'], salesman_sum['rev_sub_total'], salesman_sum['rev_tax'], salesman_sum['rev_total'],
                                        salesman_sum['net_qty'], salesman_sum['net_sub_total'], salesman_sum['net_tax'], salesman_sum['net_total'], salesman_sum['net_cost_price'], 
                                        salesman_sum['unit_price'], salesman_sum['net_profit'], salesman_sum['profit_of_unit'], salesman_sum['percent'], format_title, format_sub_text_float, False, sale_tax_pay)
            if last_salesteam_rowx != -1:
                self.get_sub_header(sheet, last_salesteam_rowx, '', record['lot_name'] if lot != 0 else '', salesteam_sum['qty'], 
                                        salesteam_sum['sub_total'], salesteam_sum['tax'], salesteam_sum['total'], salesteam_sum['without_tax_discount'], salesteam_sum['tax_discount'], salesteam_sum['with_tax_discount'],
                                        salesteam_sum['rev_qty'], salesteam_sum['rev_sub_total'], salesteam_sum['rev_tax'], salesteam_sum['rev_total'], 
                                        salesteam_sum['net_qty'], salesteam_sum['net_sub_total'], salesteam_sum['net_tax'], salesteam_sum['net_total'], salesteam_sum['net_cost_price'], 
                                        salesteam_sum['unit_price'], salesteam_sum['net_profit'], salesteam_sum['profit_of_unit'], salesteam_sum['percent'], format_title, format_sub_text_float, False, sale_tax_pay)
            if last_customer_rowx != -1:
                self.get_sub_header(sheet, last_customer_rowx, '', record['lot_name'] if lot != 0 else '', customer_sum['qty'], 
                                        customer_sum['sub_total'], customer_sum['tax'], customer_sum['total'], customer_sum['without_tax_discount'], customer_sum['tax_discount'], customer_sum['with_tax_discount'],
                                        customer_sum['rev_qty'], customer_sum['rev_sub_total'], customer_sum['rev_tax'], customer_sum['rev_total'], 
                                        customer_sum['net_qty'], customer_sum['net_sub_total'], customer_sum['net_tax'], customer_sum['net_total'], customer_sum['net_cost_price'], 
                                        customer_sum['unit_price'], customer_sum['net_profit'], customer_sum['profit_of_unit'], customer_sum['percent'], format_title, format_sub_text_float, False, sale_tax_pay)
            if last_supplier_rowx != -1:
                self.get_sub_header(sheet, last_supplier_rowx, '', record['lot_name'] if lot != 0 else '', supplier_sum['qty'], 
                                        supplier_sum['sub_total'], supplier_sum['tax'], supplier_sum['total'], supplier_sum['without_tax_discount'], supplier_sum['tax_discount'], supplier_sum['with_tax_discount'],
                                        supplier_sum['rev_qty'], supplier_sum['rev_sub_total'], supplier_sum['rev_tax'], supplier_sum['rev_total'], 
                                        supplier_sum['net_qty'], supplier_sum['net_sub_total'], supplier_sum['net_tax'], supplier_sum['net_total'], supplier_sum['net_cost_price'], 
                                        supplier_sum['unit_price'], supplier_sum['net_profit'], supplier_sum['profit_of_unit'], supplier_sum['percent'], format_title, format_sub_text_float, False, sale_tax_pay)
            if last_distribution_section_rowx != -1:
                self.get_sub_header(sheet, last_distribution_section_rowx, '', record['lot_name'] if lot != 0 else '', distribution_section_sum['qty'], 
                                        distribution_section_sum['sub_total'], distribution_section_sum['tax'], distribution_section_sum['total'], distribution_section_sum['without_tax_discount'], distribution_section_sum['tax_discount'], distribution_section_sum['with_tax_discount'],
                                        distribution_section_sum['rev_qty'], distribution_section_sum['rev_sub_total'], distribution_section_sum['rev_tax'], distribution_section_sum['rev_total'], 
                                        distribution_section_sum['net_qty'], distribution_section_sum['net_sub_total'], distribution_section_sum['net_tax'], distribution_section_sum['net_total'], distribution_section_sum['net_cost_price'], 
                                        distribution_section_sum['unit_price'], distribution_section_sum['net_profit'], distribution_section_sum['profit_of_unit'], distribution_section_sum['percent'], format_title, format_sub_text_float, False, sale_tax_pay)
            return sheet
    
    # query - гээр мэдээлэл татахад түгээлтийн багийг бүлэглэх боломжтойгоор нэмэв
    def _get_query(self, select, _from, where, order_by,  select_two, group_by):
        
        distribution_team_ids = []
        # Түгээлтийн баг сонгосон үед
        if self.distribution_team_ids:
            for distribution_team in self.distribution_team_ids:
                distribution_team_ids.append(distribution_team.id)
        
        select += ', so.distribution_section_id as distribution_section_id'
        if self.stage_one == 'distribution':
            order_by += ' order by distribution_section_id'
            select_two += ", sales_order_report.distribution_section_id as distribution_section_id"
            group_by += ", sales_order_report.distribution_section_id"
            if distribution_team_ids:
                where += 'AND so.distribution_section_id in (' + ','.join(map(str, distribution_team_ids)) + ')'
        elif self.stage_two == 'distribution':
            order_by += ', distribution_section_id'
            select_two += ", sales_order_report.distribution_section_id as distribution_section_id"
            group_by += ", sales_order_report.distribution_section_id"
            if distribution_team_ids:
                where += 'AND so.distribution_section_id in (' + ','.join(map(str, distribution_team_ids)) + ')'
        elif self.stage_three == 'distribution':
            order_by += ', distribution_section_id'
            select_two += ", sales_order_report.distribution_section_id as distribution_section_id"
            group_by += ", sales_order_report.distribution_section_id"
            if distribution_team_ids:
                where += 'AND so.distribution_section_id in (' + ','.join(map(str, distribution_team_ids)) + ')'
        res = super(ReportSalesReport, self)._get_query(select, _from, where, order_by,  select_two, group_by)
        return res
    
    # Тайлангийн бүлэглэлтээс хамааран өгөгдлийг эрэмбэлэх хэсэгт түгээлтийн багийг нэмэв
    def _get_append_sub_header(self,record):
        check_distribution_section_ids = []
        if self.stage_one == 'distribution':
            check_distribution_section_ids.append(record['distribution_section_id'])
        elif self.stage_two == 'distribution':
            check_distribution_section_ids.append(record['distribution_section_id'])
        elif self.stage_three == 'distribution':
            check_distribution_section_ids.append(record['distribution_section_id'])
        check_ids, check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids = super(ReportSalesReport, self)._get_append_sub_header(record)
        return check_ids, check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, check_distribution_section_ids
    
    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        # НӨАТ төлдөг эсэх
        sale_tax_pay = False
        self.env.cr.execute("select * from account_config_settings where company_id = %s order by id DESC LIMIT 1" % self.company_id.id)
        account_setting = self.env.cr.dictfetchall()
        if account_setting:
            if not account_setting[0]['default_sale_tax_id']:
                sale_tax_pay = True

        # create name
        report_name = _('Sales Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        
        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_filter_center = book.add_format(ReportExcelCellStyles.format_filter_center)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_title_small = book.add_format(ReportExcelCellStyles.format_title_small)
        format_title_float = book.add_format(ReportExcelCellStyles.format_title_float)
        format_content_center = book.add_format(ReportExcelCellStyles.format_content_center)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_text_color = book.add_format(ReportExcelCellStyles.format_group_number)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_left = book.add_format(ReportExcelCellStyles.format_content_left)
        format_red_text = book.add_format(ReportExcelCellStyles.format_content_float_redcolor)
        format_sub_text_float = book.add_format(ReportExcelCellStyles.format_group_float)
        alp_col_list = ['E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V']
        temp = 1 if self.see_serial else 0

        # Хэрэглэх хувьсагчид
        seq = 1
        rowx = 1
        colx = 0
        lot = 0 if not self.see_serial else 1 if self.lot_ids else 2
        total_tax = total_price_tax = total_qty = total_price = total_without_tax_discount = total_tax_discount = total_with_tax_discount = rev_price = rev_qty = rev_tax = rev_price_tax = 0
        total_rev_qty = total_rev_price = total_rev_tax = total_rev_price_tax = total_purchase_cost_row = total_standard_cost = 0
        net_total_qty = total_price_net = total_tax_net = total_price_tax_net = 0

        check_ids = []
        check_warehouse_ids = []
        check_location_ids = []
        check_cat_ids = []
        check_brand_ids = []
        check_salesman_ids = []
        check_salesteam_ids = []
        check_customer_ids = []
        check_supplier_ids = []
        check_distribution_section_ids = []

        last_ware_rowx = last_location_rowx = last_cat_rowx = last_brand_rowx = last_salesman_rowx = last_salesteam_rowx = last_customer_rowx = last_supplier_rowx = last_distribution_section_rowx = -1
        warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum = self.get_dict()

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(
            filename_prefix=('sales_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(time.strftime('%Y-%m-%d'))
        sheet.set_portrait()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.39, 0.39, 0.39, 0.39)  # 1cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.freeze_panes(9, 4)
        sheet.set_footer('&C&"Times New Roman"&10&P', {'margin': 0.1})

        # compute column
        sheet, colx_number = self.get_sheet(sheet)

        # create name
        sheet.write(rowx, 0, _(u'Company name: %s') % self.company_id.name, format_filter)
        rowx += 2
        if self.report_type == 'sales order':
            report_type = _('Sale Order')
        elif self.report_type == 'invoice':
            report_type = _('Invoice')
        elif self.report_type == 'shipment':
            report_type = _('Shipment')
        elif self.report_type == 'done':
            report_type = _('Done')
        elif self.report_type == 'loan':
            report_type = _('Loan')
            
        sheet = self.get_title(sheet, rowx, colx_number,report_name, format_name)
        rowx += 2
        sheet.write(rowx, 0, _('Duration: %s - %s') % (self.date_from, self.date_to), format_filter)
        rowx += 1
        sheet.write(rowx, 0, _('Report type: %s') % (report_type), format_filter)
        rowx += 1

        # Толгой хэсэг зурах
        sheet = self.get_header(sheet, rowx, format_title, format_title_small, lot, sale_tax_pay, True)
        rowx += 2
        temp = 1 if self.see_serial else 0
        # get data
        records = self.get_data()
        if records:
            # Тайлан өрөлт
            for record in records:
                sub_cat = 0
                if self.group:
                    check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids,  check_distribution_section_ids, \
                        warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum, \
                        last_ware_rowx, last_location_rowx, last_cat_rowx, last_brand_rowx, last_salesman_rowx, last_salesteam_rowx, last_customer_rowx, last_supplier_rowx, last_distribution_section_rowx, \
                        rowx, sub_cat \
                        = self.stage_check('stage_one', self.stage_one, record,
                                            check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, check_distribution_section_ids, 
                                            warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum, 
                                            sheet, lot, format_content_text_color, format_sub_text_float,
                                            last_ware_rowx, last_location_rowx, last_cat_rowx, last_brand_rowx, last_salesman_rowx, last_salesteam_rowx, last_customer_rowx, last_supplier_rowx, 
                                            last_distribution_section_rowx, rowx, sub_cat, sale_tax_pay)
                    if self.stage_two:
                        check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids,  check_distribution_section_ids, \
                            warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum, \
                            last_ware_rowx, last_location_rowx, last_cat_rowx, last_brand_rowx, last_salesman_rowx, last_salesteam_rowx, last_customer_rowx, last_supplier_rowx, last_distribution_section_rowx, \
                            rowx, sub_cat \
                            = self.stage_check('stage_two', self.stage_two, record,
                                            check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, check_distribution_section_ids, 
                                            warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum, 
                                            sheet, lot, format_content_text_color, format_sub_text_float,
                                            last_ware_rowx, last_location_rowx, last_cat_rowx, last_brand_rowx, last_salesman_rowx, last_salesteam_rowx, last_customer_rowx, last_supplier_rowx, 
                                            last_distribution_section_rowx, rowx, sub_cat, sale_tax_pay)
                    if self.stage_three:
                        check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids,  check_distribution_section_ids, \
                            warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum, \
                            last_ware_rowx, last_location_rowx, last_cat_rowx, last_brand_rowx, last_salesman_rowx, last_salesteam_rowx, last_customer_rowx, last_supplier_rowx, last_distribution_section_rowx, \
                            rowx, sub_cat \
                            = self.stage_check('stage_three', self.stage_three, record,
                                            check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, check_distribution_section_ids, 
                                            warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum, 
                                            sheet, lot, format_content_text_color, format_sub_text_float,
                                            last_ware_rowx, last_location_rowx, last_cat_rowx, last_brand_rowx, last_salesman_rowx, last_salesteam_rowx, last_customer_rowx, last_supplier_rowx, 
                                            last_distribution_section_rowx, rowx, sub_cat, sale_tax_pay)
                            
                sheet, sale_qty, sale_price, price, tax, sale_tax, price_tax, sale_price_tax, without_tax_discount, tax_discount, with_tax_discount, \
                        rev_qty, rev_price, rev_tax, rev_price_tax, net_qty,net_price, net_tax, net_price_tax, net_cost_price, cost_price_unit, net_profit, profit_of_unit, percent, total_qty, total_price, total_tax, total_price_tax, total_without_tax_discount, total_tax_discount, total_with_tax_discount, total_rev_qty, total_rev_price, total_rev_tax,total_rev_price_tax, net_total_qty, total_price_net, total_tax_net, total_price_tax_net, total_purchase_cost_row, total_standard_cost = \
                        self.compute_data(sheet, rowx, seq, temp, record, format_content_center, format_content_left, format_content_text, format_content_float, format_red_text, lot, sale_tax_pay, check_ids, sub_cat,total_qty, total_price, total_tax, total_price_tax, total_without_tax_discount, total_tax_discount, total_with_tax_discount, total_rev_qty, total_rev_price, total_rev_tax,total_rev_price_tax, net_total_qty, total_price_net, total_tax_net, total_price_tax_net, total_purchase_cost_row, total_standard_cost, True)

                
                rowx += 1      
                seq += 1
                                    
                sheet = self.get_sum_dict(sheet, record, warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, distribution_section_sum, sale_qty, price, tax, price_tax, total_without_tax_discount, total_tax_discount, total_with_tax_discount,\
                        rev_qty, rev_price, rev_tax, rev_price_tax, net_qty, net_price, net_tax, net_price_tax, net_cost_price, cost_price_unit, net_profit, profit_of_unit, percent,  last_ware_rowx, \
                        last_location_rowx, last_cat_rowx, last_brand_rowx, last_salesman_rowx, last_salesteam_rowx, last_customer_rowx, last_supplier_rowx, last_distribution_section_rowx, lot, sale_tax_pay, format_title, format_sub_text_float )
                
                if self.group:
                    check_ids, check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, check_distribution_section_ids = self._get_append_sub_header(record)

        # Тайлангийн хөл дүнгүүдийг зурах
        sheet = self._get_footer_total_amount(sheet, rowx, format_title, format_title_float, total_qty, total_price, total_tax, total_price_tax, total_without_tax_discount, total_tax_discount, total_with_tax_discount, total_rev_qty, total_rev_price, total_rev_tax,\
                                total_rev_price_tax, net_total_qty, total_price_net, total_tax_net, total_price_tax_net, total_purchase_cost_row, total_standard_cost, lot, sale_tax_pay, True)
        rowx += 3
        # END OF THE REPOR    T
        sheet = self.get_footer(sheet, rowx, colx_number ,_('Made by'), format_filter_center)   
        rowx += 2
        sheet = self.get_footer(sheet, rowx, colx_number ,_('Check by'), format_filter_center)   
        sheet.hide_gridlines(2)

        sheet.set_zoom(75)
        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()