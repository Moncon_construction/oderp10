# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Distribution Team Stock 'Warehouse Management: Waves'",
    'version': '1.0',
    'depends': [
        'l10n_mn_stock_picking_wave',
        'l10n_mn_distribution_team',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Stock Additional Features
    """,
    'data': [
        'views/stock_picking_wave_view.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True,
}