# -*- coding: utf-8 -*-
{
    'name': "Mongolian Document",
    'version': '1.0',
    'depends': ['base', 'l10n_mn_hr'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Document Modules',
    'summary': 'Бичиг баримт',
    'description': """
        Хүлээн авсан бичиг баримт, явуулсан бичиг баримт бүртгэх.
    """,
    'data': [
        'security/document_security.xml',
        'security/ir.model.access.csv',
        'security/ir_rule.xml',
        'views/document_type_view.xml',
        'views/received_document_view.xml',
        'views/send_document_view.xml',
        'views/email_template.xml'
    ],
}
