# -*- coding: utf-8 -*-.

from odoo import api, fields, models, _  # @UnresolvedImport
from datetime import datetime, timedelta
from odoo.exceptions import UserError, ValidationError

import logging
_logger = logging.getLogger('odoo')


class DocumentType(models.Model):
    _name = 'document.type'
    _description = 'Document type'
   
    name = fields.Char('Document type', required=True, size=64)
    '''Баримтын төрлийг явуулсан,
     хүлээн авсан бичиг баримтанд
     ашиглаагүй байвал устгах'''
    @api.multi
    def unlink(self):
        for type in self:
            received_obj = self.env['received.document']
            received_obj_ids = received_obj.search([('document_type','=',type.name)])
            send_obj = self.env['sent.document']
            send_obj_ids = send_obj.search([('document_type','=',type.name)])
            if received_obj_ids or send_obj_ids:
                raise UserError(_('You cannot delete'))
            else:
                return super(DocumentType, self).unlink()

class ReceivedDocument(models.Model):
    _name = 'received.document'
    _description = 'Received document'
    _inherit = ["mail.thread"]
    _order = 'received_date DESC'

    STATE_SELECTION = [
        ('draft', 'Draft'),
        ('sent', 'Sent'),
        ('confirmed', 'Confirmed'),
    ]

    def _default_employee(self):
        return self.env.context.get('default_employee_id') or self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)

    def _default_registered_date(self):
        now = datetime.now()
        month = timedelta()
        return now - month

    name = fields.Char('Document Name', size=255, required=True, states={'draft': [('readonly', False)]}, track_visibility='onchange')
    document_number = fields.Char('Document number', size=64, required=True, states={'draft': [('readonly', False)]}, track_visibility='onchange')
    document_type = fields.Many2one('document.type', 'Document Type', states={'draft': [('readonly', False)]}, track_visibility='onchange')
    received_date = fields.Date('Received Date', required=True, states={'draft': [('readonly', False)]}, track_visibility='onchange')
    registered_date = fields.Date('Registered Date', default=_default_registered_date, required=True, states={'draft': [('readonly', False)]})
    from1 = fields.Many2one('res.partner','From', size=64, required=True, states={'draft': [('readonly', False)]})
    content = fields.Text('Brief Content', required=True, states={'draft': [('readonly', False)]})
    data = fields.Binary('Received Attachment File', states={'draft': [('readonly', False)]})
    pages = fields.Integer('Number of pages', required=True, states={'draft': [('readonly', False)]}, default= 1)
    employee_id = fields.Many2one('hr.employee', string='Employee', index=True, track_visibility='onchange')
    department_id = fields.Many2one('hr.department', 'Department')
    job_id = fields.Many2one('hr.job', 'Job')
    registered_user_id = fields.Many2one('res.users', "Registered User", required=True, states={'draft': [('readonly', False)]}, track_visibility='onchange', default=lambda self: self.env.uid)
    company_id = fields.Many2one('res.company', "Company", states={'draft': [('readonly', False)]}, track_visibility='onchange', default=lambda self: self.env['res.company']._company_default_get())
    decision_date = fields.Date('Decision Date', states={'draft': [('readonly', False)]}, track_visibility='onchange')
    decision = fields.Text('Decision', states={'draft': [('readonly', False)]})
    decision_action = fields.Char('Decision Action', size=196, states={'draft': [('readonly', False)]})
    state = fields.Selection(STATE_SELECTION, 'Status', readonly=True, required=True, states={'draft': [('readonly', False)]}, default='draft')
    has_answer = fields.Boolean('Has answer?')
    send_doc_id = fields.Many2one('sent.document', string='Send doc', track_visibility='onchange')
    file_name = fields.Char('File name')

    @api.multi
    def unlink(self):
        for document in self:
            if document.state not in ('draft'):
                raise UserError(
                    _('You cannot delete an document which is not draft.'))
        return super(ReceivedDocument, self).unlink()

    @api.onchange('employee_id')
    def onchange_employee_id(self):
        ''' Сонгосон ажилтанд харгалзах хэлтэсийг тодорхойлно.
        '''
        if self.employee_id.department_id:
            self.department_id = self.employee_id.department_id.id
        if self.employee_id.job_id:
            self.job_id = self.employee_id.job_id.id

    # Work flow methods..
    @api.multi
    def action_do_draft(self):
        ''' Цуцлагдсан хүсэлтийг ноорог болгоно.
                Ажлын урсгалын trigger -үүдийг цэвэрлэнэ.
        '''
        self.write({'state': 'draft'})
        return True

    @api.multi
    def do_confirm(self):
        ''' Ирсэн бичиг баримтын хүсэлтийг дуусгана.
        # '''
        if self.env.uid != self.employee_id.user_id.id:
            raise ValidationError(_("Only %s employee can sent received document confirmation!") % self.employee_id.name)
        self.write({'state': 'confirmed'})
        return True

    @api.multi
    def do_send(self):
        ''' Ирсэн бичиг баримтын Хариуцагчруу явуулж байна.
        '''
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('l10n_mn_document', 'document_email_template23')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict()
        ctx.update({
            'default_model': 'received.document',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True,
            'status': 'sent',
        })
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }


class MailComposer(models.TransientModel):
    _inherit = 'mail.compose.message'

    @api.multi
    def send_mail_action(self):
        if self._context.get('active_model') == 'received.document':
            self.env['received.document'].search([('id','=',self._context.get('active_id'))]).write({'state': self._context.get('status')})
        return super(MailComposer, self).send_mail_action()

# create an object
#######################################################################
#------------------------ Send Document ------------------------------#
#######################################################################


class SentDocument(models.Model):
    _name = 'sent.document'
    _description = 'Sent Document'
    _order = 'sent_date DESC'
    _inherit = ["mail.thread"]

    def _default_registration_date(self):
        now = datetime.now()
        month = timedelta()
        return now - month

    def _default_sent_date(self):
        now = datetime.now()
        month = timedelta()
        return now - month

    name = fields.Char('Document name', size=255, required=True)
    document_no = fields.Char('Document number', size=64, required=True)
    document_type = fields.Many2one('document.type', 'Document type')
    sender_dep_id = fields.Many2one('hr.department', 'Sender Department')
    receiver = fields.Char('Receiver', size=100, required=True)
    page_num = fields.Integer('Number of pages',  default=1)
    content = fields.Text('Brief content')
    has_reply = fields.Boolean('Has reply?')
    reply_date = fields.Date(' Reply date')
    respondent = fields.Many2one('hr.employee', 'Respondent')
    reply_name = fields.Char('Reply name', size=64, default="- hariu awah")
    has_answer = fields.Boolean('Has answer?')
    received_doc_id = fields.Many2one('received.document', string='received doc')
    sent_date = fields.Date('Sent Date', required=True, default=_default_sent_date)
    registration_date = fields.Date('Registration Date', required=True, default=_default_registration_date)
    registered_user_id = fields.Many2one('res.users', 'Registered User', required=True, default=lambda self: self.env.uid)
    attachment = fields.Binary('Received Attachment File', states={'draft': [('readonly', False)]})
    company_id = fields.Many2one('res.company', "Company", states={'draft': [('readonly', False)]}, track_visibility='onchange', default=lambda self: self.env['res.company']._company_default_get())
    file_name = fields.Char('File name')
    sender = fields.Many2one('hr.employee', 'Registered User', required=True)

    @api.onchange('name')
    def onchange_name(self):
        self.reply_name = self.name

    @api.model
    def create(self, vals):
        creation = super(SentDocument, self).create(vals)
        return creation

    @api.multi
    def write(self, vals):
        update = super(SentDocument, self).write(vals)
        update_vals = {}
        return update
