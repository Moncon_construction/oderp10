# -*- coding: utf-8 -*-
{
    'name': "Mongolian Event",
    'version': '1.0',
    'depends': ['event'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'description': """
        Арга хэмжээ.
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/event_config_settings_views.xml',
        'views/event_views.xml'
    ],
}
