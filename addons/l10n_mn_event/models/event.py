# -*- coding: utf-8 -*-.
from odoo import api, fields, models, _


class EventEvent(models.Model):
    _inherit = 'event.event'

    def _default_employee(self):
        return self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)

    state = fields.Selection([
        ('draft', 'Unconfirmed'), ('cancel', 'Cancelled'),
        ('confirm', 'Confirmed'), ('report', 'Write Report'), ('done', 'Done')],
        string='Status', default='draft', readonly=True, required=True, copy=False)
    room_organization = fields.Selection(
        [('class', 'Class'), ('ushape', 'U-Shape'),
         ('rounds', 'Rounds'), ('crescent', 'Crescent'), ('free', 'Free Style')],
        'Room Organization', required=True, default='class')
    equipment_ids = fields.Many2many('event.equipment', string='Event Equipment')
    material_ids = fields.Many2many('event.material', string='Event material')
    expected_outputs_outcomes = fields.Text('Expected outputs and outcomes')
    other = fields.Text('Name of other organizations, individuals, and their participation:')
    employee_id = fields.Many2one('hr.employee', string="Employee", default=_default_employee)
    job_id = fields.Many2one('hr.job', related='employee_id.job_id', store=True,
                             string='Job Position')
    duration = fields.Float('Duration')
    image_link = fields.Char('Link')
    event_report = fields.Text('Event Report')
    explanation = fields.Text('Explanation')
    result = fields.Text('Result')
    time_review = fields.Text('Time to review')
    next_event = fields.Text('Next Event')
    summury = fields.Text('Summury')

    @api.one
    def button_report(self):
        self.state = 'report'

class EventRegistration(models.Model):
    _inherit = 'event.registration'

    participant_evaluation = fields.Float('Evaluation participant')