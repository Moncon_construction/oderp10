# -*- coding: utf-8 -*-.
from odoo import api, fields, models, _


class EventEquipment(models.Model):
    _name = 'event.equipment'

    equipment_name = fields.Char(string='Equipment Name', required=True)


class EventMaterial(models.Model):
    _name = 'event.material'

    material_name = fields.Char(string='Material Name', required=True)