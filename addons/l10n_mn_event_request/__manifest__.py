# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Event Request",
    'version': '1.0',
    'depends': ['event'],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Event Modules',
    'description': """
         Register event requests and create events.
    """,
    'website': 'http://asterisk-tech.mn',
    'data': [
        'security/ir.model.access.csv',
        'security/ir_rule.xml',
        'views/event_request_view.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}