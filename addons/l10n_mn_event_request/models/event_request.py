# -*- coding: utf-8 -*-

from odoo import _, api, fields, models
from odoo import exceptions

import datetime
from dateutil.relativedelta import relativedelta


class EventRequest(models.Model):
    _name = 'event.request'
    _description = 'Event Request'
    _inherit = ['mail.thread']

    def _default_employee(self):
        employee = self.env['hr.employee'].search([('user_id', '=', self._uid)])
        if len(employee) > 1:
            employee = employee[0]
        if not employee:
            raise exceptions.AccessError(_("Can't find any related employee for your user. Only employee can create event request."))
        return employee.id

    name = fields.Char(string='Event Name', translate=True, readonly=False, states={'confirm': [('readonly', True)]}, track_visibility="onchange")
    event_type_id = fields.Many2one('event.type', string='Category', readonly=False, states={'confirm': [('readonly', True)]}, track_visibility="onchange")
    employee_id = fields.Many2one('hr.employee', string='Request processed', default=_default_employee, readonly=False, states={'confirm': [('readonly', True)]}, track_visibility="onchange")
    job_id = fields.Many2one('hr.job', string='Job', related='employee_id.job_id', readonly=False, states={'confirm': [('readonly', True)]}, track_visibility="onchange")
    user_id = fields.Many2one('res.users', string='Responsible', default=lambda self: self.env.user)
    partner_ids = fields.Many2many('res.partner', string='Partners', readonly=False, states={'confirm': [('readonly', True)]}, track_visibility="onchange")
    duration = fields.Float('Duration', track_visibility="onchange")
    date = fields.Datetime(string='Date', track_visibility='onchange', states={'confirm': [('readonly', True)]})
    location = fields.Char('Venue of the event', track_visibility='onchange')
    grounds = fields.Text('Grounds', help="Please explain in clearly state the reasons, circumstances and grounds for conducting the activity.", track_visibility='onchange')
    purpose = fields.Text('Purpose of the event', track_visibility='onchange')
    description = fields.Text('Description', help="Please explain in detail in the order in which the activities will take place.", track_visibility='onchange')
    result = fields.Text('Result', track_visibility='onchange')
    other_description = fields.Text('Other Description', track_visibility='onchange')
    report = fields.Text('Report', track_visibility='onchange')
    state = fields.Selection(string='Status', selection=[('draft', 'Draft'),
                                                         ('sent', 'Sent'),
                                                         ('confirm', 'Confirm'),
                                                         ('cancel', 'Cancelled')], copy=False, index=True, readonly=True, default='draft', track_visibility='onchange')
    company_id = fields.Many2one('res.company', string='Company', change_default=True, default=lambda self: self.env['res.company']._company_default_get('event.request'), readonly=False, states={'done': [('readonly', True)]})

    @api.multi
    def button_send(self):
        for req in self:
            req.state = 'sent'

    @api.multi
    def button_confirm(self):
        # Батлах товч дарахард арга хэмжээ үүсгэнэ.
        event = self.env['event.event']
        convert = datetime.datetime.strptime(self.date, '%Y-%m-%d %H:%M:%S')
        end_date = convert + relativedelta(hours=+self.duration)
        event.create({'name': self.name,
                              'event_type_id': self.event_type_id.id,
                              'seats_availability': 'unlimited',
                              'state': 'draft',
                              'date_begin': self.date,
                              'date_end': end_date,
                              'request_id': self.id})
        self.state = 'confirm'

    @api.multi
    def unlink(self):
        for req in self:
            if req.state not in ['draft']:
                raise exceptions.AccessError(_("You cannot delete a record which is not draft!"))
        return super(EventRequest, self).unlink()

    @api.multi
    def button_draft(self):
        for req in self:
            req.state = 'draft'

    @api.multi
    def button_cancel(self):
        for req in self:
            req.state = 'cancel'

    @api.multi
    def stat_button_event(self):
        res = self.env['event.event'].search([('request_id', '=', self.id)])
        return {
            'type': 'ir.actions.act_window',
            'name': _('Event'),
            'res_model': 'event.event',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'view_id': False,
            'domain': [('id', 'in', res.ids if res else False)],
        }

class EventEvent(models.Model):
    _inherit = 'event.event'

    request_id = fields.Many2one('event.request', 'Related Event Request')