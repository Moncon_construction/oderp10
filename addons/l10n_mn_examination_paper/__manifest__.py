# -*- coding: utf-8 -*-
{
    'name': "Mongolian Examination Paper",
    'version': '1.0',
    'depends': ['l10n_mn_document'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Examination Paper Modules',
    'summary': 'Бичиг баримт',
    'description': """
        Шинжилгээний бичиг баримт бүртгэх.
    """,
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/research_document_view.xml',
        'views/product_type_view.xml'
    ],
}
