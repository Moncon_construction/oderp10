# -*- coding: utf-8 -*-.
from odoo import api, fields, models, _

class ProductType(models.Model):
    _name = 'product.type'
    
    name = fields.Char('Product Type Name', copy=False,required=True)