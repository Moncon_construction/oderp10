# -*- coding: utf-8 -*-.

from odoo import api, fields, models, _
from datetime import date, datetime
 
class ResearchDocument(models.Model):
    _name = 'research.document'
    _description = 'Research Document'
    _inherit = ['mail.thread']
    
    def is_archive_document(self):
        today = date.today()
        for obj in self:
            if obj.date_end:
                date_time_obj = datetime.strptime(obj.date_end, '%Y-%m-%d %H:%M:%S')
                if today > date_time_obj.date():
                    obj.is_archive = True
                else:
                    obj.is_archive = False
            else:
                obj.is_archive = False  
    
    def _default_employer(self):
        if self.env.user.employee_ids:
            return self.env.user.employee_ids[0]
         
    name = fields.Char('Product Name', copy=False, required=True)
    product_type = fields.Many2one('product.type', 'Product Type', required=True)
    partner_id = fields.Many2one('res.partner', 'Suplier Company', required=True)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('valid', 'Valid'),
        ('archived', 'Archived')], string='States',
        copy=False, default='draft', required = True)
    attach_file = fields.Binary(string="Attach File", required=True)
    file_name = fields.Char('File name')
    user_id = fields.Many2one('res.users', 'User', default=lambda self: self.env.user.id)
    employee_id = fields.Many2one('hr.employee', string='Registered Employer', default=_default_employer)
    job_id = fields.Many2one('hr.job', string='Registered Employer Job')
    date_start = fields.Datetime('Start Date')
    date_end = fields.Datetime('End Date')
    active = fields.Boolean('Active', default=True)
    is_archive = fields.Boolean(compute='is_archive_document')
    
    @api.onchange('employee_id')
    def onchange_employee_id(self):
        for obj in self:
            obj.job_id = obj.employee_id.job_id.id
    
    @api.multi
    def action_done(self):
        self.state = 'valid'
        self.active = True
        
    @api.multi
    def action_archive(self):
        self.state = 'archived'
        self.active = False
        
    @api.multi
    def action_draft(self):
        self.state = 'draft'
        self.active = True
