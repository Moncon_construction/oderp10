# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

{
    'name': "Mongolian Extra Cost Item",
    'version': '1.0',
    'depends': ['l10n_mn_base', 'l10n_mn_account'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
        Extra Cost Additional Features
    """,
    'data': [
            'data/extra_cost_precision_data.xml',
            'security/extra_cost_security.xml',
            'security/ir.model.access.csv',
            'views/extra_cost_item_view.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
