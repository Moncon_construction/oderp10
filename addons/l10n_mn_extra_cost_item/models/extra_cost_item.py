# -*- coding: utf-8 -*-

from odoo import fields, models

SPLIT_METHOD_EC = [
    ('equal', 'Equal'),
    ('by_quantity', 'By Quantity'),
    ('by_subtotal', 'By Line Sub Total'),
    ('by_weight', 'By Weight'),
    ('by_volume', 'By Volume'),
]


class ExtraCostItem(models.Model):
    _name = 'extra.cost.item'
    _description = 'Extra Cost Item'

    name = fields.Char('Name', size=128, required=True)
    currency = fields.Many2one('res.currency', 'Currency', help='Select Currency for Extra Cost Calculation', required=True, default=lambda self: self.env.user.company_id.currency_id)
    non_cost_calc = fields.Boolean('Non Product Cost')
    allocmeth = fields.Selection(SPLIT_METHOD_EC, string='Allocation Method', default='by_subtotal', required=True)
    company = fields.Many2one('res.company', 'Company Name', default=lambda self: self.env.user.company_id)
    detail_note = fields.Text('Note')
    account_id = fields.Many2one('account.account', 'Account', required=True)
