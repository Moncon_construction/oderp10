# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': "Mongolian Farming",
    'version': '1.0',
    'depends': ['l10n_mn_account', 'l10n_mn_stock', 'l10n_mn_product_expense', 'l10n_mn_task', 'l10n_mn_product_kits'],
    'author': "Asterisk Technologies LLC",
    'maintainer': 'chinzorig.o@asterisk-tech.mn',
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
    Mongolian Farming Features
    """,
    'data': [
            'security/security.xml',
            'security/ir.model.access.csv',
            'datas/sequence_data.xml',
            'views/farming_view.xml',
            'views/farming_product.xml',
            'views/farming_period.xml',
            'views/farming_guidance.xml',
            'views/farming_plan.xml',
            'views/farming_section.xml',
            'views/farming_fill_form.xml',
            'views/plant_protection_substance.xml',
            'report/report_farming_guidance.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
