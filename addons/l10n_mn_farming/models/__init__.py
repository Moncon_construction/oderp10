# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

import farming
import farming_product
import farming_period
import farming_guidance
import farming_fill_form
import farming_plan
import plant_protection_substance
