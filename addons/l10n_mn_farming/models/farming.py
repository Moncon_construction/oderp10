# -*- coding: utf-8 -*-.

from odoo import api, fields, models # @UnresolvedImport


class FarmingSquare(models.Model):
    _name = 'farming.square'
    _inherit = ['mail.thread']

    name = fields.Char('Name', required=True, track_visibility='onchange',)
    registration = fields.Char('State registration #')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id,
                                 track_visibility='onchange')
    account_id = fields.Many2one('account.account', string='Account', track_visibility='onchange')
    analytic_account_id = fields.Many2one('account.analytic.account', string='Analytic Account Farming', track_visibility='onchange',)
    standard_size = fields.Float('Standard size', track_visibility='onchange',)
    registered_size = fields.Integer('Registration size')
    section_id = fields.Many2one('farming.section', string='Section')
    square_perimeter = fields.Integer('Perimeter', track_visibility='onchange')
    sector_id = fields.Many2one('res.sector', string='Sector Farming', track_visibility='onchange')


class FarmingSection(models.Model):
    _name = 'farming.section'
    _inherit = ['mail.thread']
    _rec_name = 'section'

    section = fields.Char('Section', required=True, track_visibility='onchange')
    field_ids = fields.One2many('farming.square', 'section_id', string='Farming field')