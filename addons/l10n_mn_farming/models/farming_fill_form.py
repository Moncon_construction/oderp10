# -*- coding: utf-8 -*-.

from odoo import api, fields, models, _  # @UnresolvedImport
from datetime import datetime, timedelta
from odoo.exceptions import UserError, ValidationError
from odoo.addons.l10n_mn_farming.models import farming_plan
from odoo.addons.l10n_mn_web.models.time_helper import *

import logging
_logger = logging.getLogger('odoo')

class FarmingFillFormTemplate(models.Model):
    _name = 'farming.fill.form.template'
    _rec_name = 'task_id'

    task_id = fields.Many2one('task.type', string='Procedure', required=True)
    orders_ids = fields.Many2many('farming.fill.form.template.line', 'farming_fill_template_line_rel', 'farming_from_id', 'farming_fill_template_line_id', 'Fill form')

class FarmingFillFormTemplateLine(models.Model):
    _name = 'farming.fill.form.template.line'
    _rec_name = 'orders'

    orders = fields.Char(string='Orders', required=True)
    description = fields.Text(string='Description')

class FarmingFillForm(models.Model):
    _name = 'farming.fill.form'
    _inherit = ['mail.thread']
    _rec_name = 'square_id'

    date = fields.Datetime('Date')
    square_id = fields.Many2one('farming.square', string='Square', required=True)
    real_size = fields.Float('Field real size', related='square_id.standard_size')
    section_id = fields.Many2one('farming.section', related='square_id.section_id', readonly=True)
    template_id = fields.Many2one('task.type', string='Task type')
    farming_stage = fields.Selection(selection=farming_plan.FARMING_STAGE, string='Farming stage')
    technic_id = fields.Many2one('technic', string='technic', )
    aggregate_id = fields.Many2one('technic', string='Aggregate', )
    start_gps_point = fields.Float(string='GPS start point')
    end_gps_point = fields.Float(string='GPS end point')
    guidance_id = fields.Many2one('farming.guidance', string='Guidance', )
    type_id = fields.Many2one('cultivation.type', string='Type', related='sort_id.type' )
    sort_id = fields.Many2one('cultivation.sort', string='Sort', )
    width = fields.Integer('Perimeter', related='square_id.square_perimeter')
    total_gps_point = fields.Float(string='GPS total point' , compute='_calc_gps_point', store='True')
    state = fields.Selection([
                            ('draft', 'Draft'),
                            ('confirm', 'Confirm'),
                            ('done', 'Done')], string='States', copy=False, default='draft')
    start_mot = fields.Float('Start mot time')
    end_mot = fields.Float('End mot time')
    acc_id = fields.Many2one('hr.employee', string='Accountant', )
    start_time = fields.Datetime('Start time')
    end_time = fields.Datetime('End time')
    idle_time = fields.Float(string='Idle hours')
    total_time = fields.Char(string='Total hours' , readonly='True', compute='_calc_total_time', store='True')
    seed_sowing_ids = fields.One2many('farming.fill.form.product', 'fill_form_id',
                                      string="Seed sowing", domain=[('material_type','=','seed')])
    fuel_consumption_ids = fields.One2many('farming.fuel.consumption', 'task_id', string="Fuel consumption")
    fertilizer_consumption_ids = fields.One2many('farming.fill.form.product', 'fill_form_id',
                                                 string="Fertilizer consumption",
                                                 domain=[('material_type','=','fertilizer')])
    fill_form_technology_ids = fields.One2many('farming.fill.form.technology', 'fill_form_id')
    fill_form_workers_ids = fields.One2many('farming.fill.form.workers', 'fill_form_id', string='Workers')
    technic_line_ids = fields.One2many('farming.technic.line', 'task_id', string='Technic performance')
    fill_form_produced_ids = fields.One2many('farming.fill.form.produced', 'fill_form_id')
    idle_line_ids = fields.One2many('farming.idle.line','task_id', string='Idle lines')
    sector_id = fields.Many2one('res.sector', string='Sector Farming', related='square_id.sector_id', readonly=True)
    crop_usage = fields.Selection(string='Crop usage', selection=[('seed','Seed'),('consume','Consume'),('feed','Livestock feed')])
    crop_norm = fields.Float('Crop norm')
    fertilizer_product_id = fields.Many2one('product.product',string='Fertilizer')
    fertilizer_norm = fields.Float('Fertilizer norm')
    company_id = fields.Many2one('res.company', related='square_id.company_id', readonly=True)
    technician_id = fields.Many2one('hr.employee')
    aggregate_width = fields.Float('Aggregate width /m/')
    bunker_size = fields.Float('Bunker size /l/')
    seeding_depth = fields.Float('Seeding depth')
    seeding_gear = fields.Char('Seeding gear')
    notes = fields.Text('Notes')
    harvest_ids = fields.One2many('farming.fill.form.harvest', 'fill_form_id', string='Combine and bunker registration')
    crop_protection_ids = fields.One2many('farming.protection.solution.charge', 'fill_form_id', string='Crop Protections')


    @api.one
    @api.depends('start_gps_point', 'end_gps_point')
    def _calc_gps_point(self):
        if self.start_gps_point and self.end_gps_point:
            total = self.end_gps_point - self.start_gps_point
            self.total_gps_point = total
        elif not self.start_gps_point and self.end_gps_point:
            self.total_gps_point = self.end_gps_point

    # @api.onchange('guidance_id')
    # def _load_guidance_data(self):
    #     if self.guidance_id.sort_id:
    #         self.sort_id = self.guidance_id.sort_id
    #     if self.guidance_id.farming_operation_id:
    #         self.template_id = self.guidance_id.farming_operation_id
    #     if self.guidance_id.square_id:
    #         self.square_id = self.guidance_id.square_id

    @api.one
    @api.depends('start_time', 'end_time')
    def _calc_total_time(self):
        if not isinstance(self.start_time, bool):
            if not isinstance(self.end_time, bool):
                start_calc = datetime.strptime(self.start_time, '%Y-%m-%d %H:%M:%S')
                end_cal = datetime.strptime(self.end_time, '%Y-%m-%d %H:%M:%S')
                self.total_time = str(end_cal - start_calc).replace("day", "өдөр")

    @api.multi
    @api.onchange('template_id')
    def onchange_template_id(self):
        for obj in self:
            if obj.template_id:
                obj.farming_stage = obj.template_id.farming_stage

    @api.multi
    def load_tasks(self):
        technology_obj = self.env['farming.fill.form.technology'].search([('fill_form_id', '=', self.id)])
        workers_obj = self.env['farming.fill.form.workers'].search([('fill_form_id', '=', self.id)])
        # technic_obj = self.env['farming.fill.form.technic'].search([('fill_form_id', '=', self.id)])
        technology_obj.unlink()
        section_obj = self.env['farming.section.line'].search([('square_id', '=', self.square_id.id)], limit=1)
        task_obj = self.env['farming.fill.form.template'].search([('task_id', '=', self.template_id.id)], limit=1)
        if section_obj:
            section_resources_obj = self.env['farming.section.resources'].search([('section_id', '=', section_obj.section_id.id)], limit=1)
            if section_resources_obj:
                workers_obj.unlink()
                # technic_obj.unlink()
                for workers in section_resources_obj.resources_employee_ids:
                    self.fill_form_workers_ids.create({
                        'employee_id': workers.employee_id.id,
                        'job_id': workers.job_id.id,
                        'fill_form_id': self.id
                        })
                # for technics in section_resources_obj.resources_tools_ids:
                #     self.fill_form_technic_ids.create({
                #         'tools_id': technics.product_id.id,
                #         'unit': technics.unit,
                #         'amount': technics.amount,
                #         'fill_form_id': self.id
                #         })

        for obj in self:
            for orders in task_obj.orders_ids:
                obj.fill_form_technology_ids.create({
                    'fill_form_id': obj.id,
                    'procedure_id': orders.id
                    })

    @api.multi
    def unlink(self):
        for obj in self:
            if obj.state != 'draft':
                raise UserError(_('You can delete only draft guidance'))
        return super(FarmingFillForm, self).unlink()

    def confirm(self):
        self.state = 'confirm'
        self.user_id = self.env.user.id

    def done(self):
        self.state = 'done'

    def action_to_draft(self):
        self.state='draft'

class FarmingFillFormTechnology(models.Model):
    _name = 'farming.fill.form.technology'

    procedure_id = fields.Many2one('farming.fill.form.template.line', string='Procedure', required=True)
    value = fields.Char(string='Value')
    fill_form_id = fields.Many2one('farming.fill.form')
    guidance_id = fields.Many2one('farming.guidance', ondelete='cascade')

class FarmingFuelExpense(models.Model):
    _name = 'farming.fuel.consumption'

    sequence = fields.Integer('Sequence', default=1, required=True,)
    task_id = fields.Many2one('farming.fill.form', ondelete='cascade')
    fuel_product_id = fields.Many2one('product.product', string='Fuel product',
                                      domain=[('farming_product_type','=','fuel')])
    uom_id = fields.Many2one('product.uom', string='UoM')
    gps_start = fields.Float('GPS start')
    gps_stop = fields.Float('GPS stop')
    fill_amount = fields.Float('Fill amount')
    tanker_vehicle_id = fields.Many2one('technic', string='Transport vehicle')
    fill_date = fields.Datetime('Fill date')
    consumed_to_field = fields.Float('Consumed amount to field')
    consumed_in_field = fields.Float('Consumed in field')
    total_consumed = fields.Float('Total amount', compute="_compute_total", store=True)
    total_cost = fields.Float('Total cost', compute="_compute_total", store=True)
    price_unit = fields.Float('Price unit', compute="_compute_total")
    hectare_norm = fields.Float('Hectare norm')
    processed_hectare = fields.Float('Processed hectare', compute="_compute_hectare")
    hectare_norm_perf = fields.Float('Processed hectare performance', compute="_compute_hectare")
    over_under = fields.Float('Over/under', compute="_compute_hectare")

    @api.multi
    @api.depends('consumed_to_field','consumed_in_field','fuel_product_id')
    def _compute_total(self):
        for obj in self:
            _logger.info(obj.fuel_product_id)
            obj.total_consumed = obj.consumed_to_field + obj.consumed_in_field
            obj.price_unit = obj.fuel_product_id.get_warehouse_standard_price(warehouse_id=None)
            obj.total_cost = obj.price_unit * obj.total_consumed
            obj.uom_id = obj.fuel_product_id.uom_id.id

    @api.multi
    @api.depends('gps_start','gps_stop','hectare_norm')
    def _compute_hectare(self):
        for obj in self:
            obj.processed_hectare = obj.gps_stop - obj.gps_start
            obj.hectare_norm_perf = obj.over_under = 0
            if obj.processed_hectare > 0:
                obj.hectare_norm_perf = obj.total_consumed / obj.processed_hectare
                obj.over_under = obj.hectare_norm - obj.hectare_norm_perf

class FarmingFillFormProduct(models.Model):
    _name = 'farming.fill.form.product'
    _description = 'Seed, fertilizer consumption'

    sequence = fields.Integer('Sequence', default=1, required=True,)
    truck_start_balance = fields.Float('Truck starting balance')
    truck_end_balance = fields.Float('Truck ending balance', compute='_compute_total')
    tractor_start_balance = fields.Float('Tractor start balance')
    tractor_end_balance = fields.Float('Tractor ending balance', compute='_compute_total')
    fill_amount = fields.Float('Fill amount')
    truck_id = fields.Many2one('technic', string="Transport truck number")
    fill_date = fields.Datetime('Fill date')
    tractor_consumed = fields.Float('Tractor consumed amount')
    fill_form_id = fields.Many2one('farming.fill.form')
    product_id = fields.Many2one('product.product', string='Product', required=True,
                                 domain=[('farming_product_type','in',('seed','fertilizer','protection'))])
    price_unit = fields.Float('Unit price', compute='_compute_total')
    total_cost = fields.Float('Total cost', compute='_compute_total')
    hectare_norm = fields.Float('Hectare norm', required=True)
    charge_number = fields.Float(string='Charge Number')
    spending_number = fields.Float('Spending Number')
    moto_hour_start = fields.Float('Start moto.hour')
    moto_hour_end = fields.Float('End moto.hour')
    moto_hour = fields.Float('Motor hours', compute="_compute_motor")
    hectare_to_process = fields.Float('Hectare to process', compute='_compute_norm')
    hectare_processed = fields.Float('Hectare processed')
    hectare_norm_perf = fields.Float('Norm performance', compute="_compute_total")
    over_under = fields.Float('Over/under', compute='_compute_total')
    material_type = fields.Selection(selection=[('seed','Seed'),('fertilizer','Fertilizer'),('protection','Crop protection')])

    @api.multi
    @api.depends('fill_amount', 'hectare_norm')
    def _compute_norm(self):
        for obj in self:
            if obj.hectare_norm > 0:
                obj.hectare_to_process = obj.fill_amount / obj.hectare_norm

    @api.multi
    @api.depends('tractor_start_balance', 'fill_amount', 'tractor_consumed', 'truck_start_balance',
                 'hectare_processed', 'hectare_norm')
    def _compute_total(self):
        for obj in self:
            obj.tractor_end_balance = obj.tractor_start_balance + obj.fill_amount - obj.tractor_consumed
            obj.truck_end_balance = obj.truck_start_balance - obj.fill_amount
            obj.price_unit = obj.product_id.get_warehouse_standard_price(warehouse_id=None)
            obj.total_cost = obj.price_unit * obj.tractor_consumed
            if obj.hectare_processed > 0:
                obj.hectare_norm_perf = obj.tractor_consumed / obj.hectare_processed
                obj.over_under = obj.hectare_norm - obj.hectare_norm_perf

    @api.multi
    @api.depends('moto_hour_start', 'moto_hour_end')
    def _compute_motor(self):
        for obj in self:
            obj.moto_hour = obj.moto_hour_end - obj.moto_hour_start

    @api.onchange('product_id')
    def onchange_product_id(self):
        _logger.info("onchange")
        if self.product_id:
            self.material_type = self.product_id.farming_product_type


class FarmingFillFormWorkers(models.Model):
    _name = 'farming.fill.form.workers'

    employee_id = fields.Many2one('hr.employee', string='Employee', required=True)
    job_id = fields.Many2one('hr.job', string='Job', related='employee_id.job_id')
    fill_form_id = fields.Many2one('farming.fill.form')

    @api.onchange('tools_id')
    def _onchange_fill_form_rel(self):
        for obj in self:
            obj.job_id = obj.employee_id.job_id

class FarmingTechnicLine(models.Model):
    _name = 'farming.technic.line'
    _description = 'Farming technic performance'

    line_type = fields.Char(string='Technical result', readonly=True, default=u'Гүйцэтгэсэн хугацаа')
    uom = fields.Selection([('motor_hour', 'Motor hour'), ('hectare', 'Hectare'), ('hour', 'Hour')], 'Unit of measurement')
    motor_hour_start = fields.Float('Motor hour start')
    motor_hour_stop = fields.Float('Motor hour stop')
    motor_idle_time = fields.Float('Motor hour idle')
    motor_hour_result = fields.Float('Motor hour result', compute='_compute_result')
    date_start = fields.Datetime('Date start')
    date_stop = fields.Datetime('Date stop')
    idle_time = fields.Float('Idle hour, extra move')
    hour_result = fields.Float('Net hour', compute='_compute_result')
    hectare_to_process = fields.Float('Hectare to process')
    hectare_processed = fields.Float('Hectare processed')
    performance = fields.Float('Performance', compute='_compute_performance')
    task_id = fields.Many2one('farming.fill.form', ondelete='cascade')

    @api.depends('motor_hour_start','motor_hour_stop','motor_idle_time',
                 'date_start', 'date_stop','idle_time')
    def _compute_result(self):
        for blocktime in self:
            if blocktime.date_start and blocktime.date_stop:
                diff = fields.Datetime.from_string(blocktime.date_stop) \
                                        - fields.Datetime.from_string(blocktime.date_start)
                blocktime.hour_result = diff.total_seconds() / 3600.0 - blocktime.idle_time
            else:
                blocktime.hour_result = 0.0
            if blocktime.motor_hour_start and blocktime.motor_hour_stop:
                blocktime.motor_hour_result = blocktime.motor_hour_stop - blocktime.motor_hour_start \
                                              - blocktime.motor_idle_time
            else:
                blocktime.motor_hour_result = 0.0

    @api.depends('hectare_to_process','hectare_processed')
    def _compute_performance(self):
        for obj in self:
            if obj.hectare_to_process > 0:
                obj.performance = obj.hectare_processed / obj.hectare_to_process
                obj.performance = obj.performance * 100

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            result.append((record.id, "(%s - %s)" % (record.motor_hour_start, record.motor_hour_stop)))
        return result

class FarmingFillFormTechnology(models.Model):
    _name = 'farming.fill.form.produced'

    fill_form_id = fields.Many2one('farming.fill.form')
    product_id = fields.Many2one('product.product', string='Product')
    product_uom = fields.Many2one('product.uom', string='Product Uom')
    quantity = fields.Float('Quantity')

class FarmingIdleLine(models.Model):
    _name = 'farming.idle.line'

    task_id = fields.Many2one('farming.fill.form', ondelete='cascade')
    idle_type = fields.Selection([
                                ('meal','Meal time'),
                                ('repair','Repair time'),
                                ('service', 'Service time'),
                                ('fuel', 'Fuel pending time'),
                                ('engine_idle', 'Idle engine time'),
                                ('transit','Transit time')], string='Idle type')
    time_norm = fields.Float('Idle norm')
    motor_hour_start = fields.Float('Motor hour start')
    motor_hour_stop = fields.Float('Motor hour stop')
    motor_hour_result = fields.Float('Motor hour result', compute='_compute_result')
    gps_start = fields.Datetime('GPS start')
    gps_stop = fields.Datetime('GPS stop')
    gps_result = fields.Float('GPS hour', compute='_compute_result')
    result = fields.Char('Reason, work type')

    @api.depends('motor_hour_start','motor_hour_stop','gps_start','gps_stop')
    def _compute_result(self):
        for obj in self:
            obj.motor_hour_result = obj.motor_hour_stop - obj.motor_hour_start
            if obj.gps_start and obj.gps_stop:
                diff = fields.Datetime.from_string(obj.gps_stop) \
                       - fields.Datetime.from_string(obj.gps_start)
                obj.gps_result = diff.total_seconds() / 3600.0
            else:
                obj.gps_result = 0.0


class FarmingFillFormHarvest(models.Model):
    _name = 'farming.fill.form.harvest'
    _description = 'Combine and bunker registration'

    @api.depends('date_start', 'date_stop')
    def _compute_natural_time(self):
        for obj in self:
            date_start = datetime.strptime(str(get_day_like_display(obj.date_start,self.env.user)), '%Y-%m-%d %H:%M:%S').strftime('%m.%d %H:%M') if obj.date_start else False
            date_stop = datetime.strptime(str(get_day_like_display(obj.date_stop,self.env.user)), '%Y-%m-%d %H:%M:%S').strftime('%m.%d %H:%M') if obj.date_stop else False
            natural_time = "(%s - %s)" % (str(date_start), str(date_stop))
            obj.natural_time = natural_time

    fill_form_id = fields.Many2one('farming.fill.form', ondelete='cascade')
    form_id = fields.Integer(string='Default form id to pass')
    bunker_percent = fields.Float(string='Bunker %')
    motor_hour = fields.Many2one('farming.technic.line', string='Motor hour')
    motor_idle_time = fields.Float(related='motor_hour.motor_idle_time', string='Motor hour idle harvest')
    motor_hour_result = fields.Float(related='motor_hour.motor_hour_result', string ='Motor hour result harvest')
    date_start = fields.Datetime(related='motor_hour.date_start', string='Date start')
    date_stop = fields.Datetime(related='motor_hour.date_stop', string='Date stop')
    natural_time = fields.Char(compute='_compute_natural_time', string='Natural time')
    natural_idle_time = fields.Float(related='motor_hour.idle_time', string='Natural idle time')
    natural_hour_result = fields.Float(related='motor_hour.hour_result', string='Natural net hour')
    shipped_percent = fields.Float(string='Shipped percent')
    chiton_number = fields.Integer(string='Chiton number')
    bunker_moisture_percent = fields.Float(string='Bunker moisture %')
    technic = fields.Many2one('technic', string='Technic license number')
    shipped_datetime = fields.Datetime(string='Shipped hour minute')
    gps_speed = fields.Float(string='GPS speed')

class FarmingProtectionSolutionCharge(models.Model):
    _name = 'farming.protection.solution.charge'
    _description = 'Farming Protection Solution Charge'

    @api.depends('finish_motor_hour','start_motor_hour','idle_hour')
    def _calculate_result(self):
        for obj in self:
            summer = 0
            if obj.finish_motor_hour:
                summer += obj.finish_motor_hour
                if obj.start_motor_hour:
                    summer -= obj.start_motor_hour
                if obj.idle_hour:
                    summer -= obj.idle_hour
            obj.result = summer

    @api.depends('solution_ids')
    def _calculate_norm(self):
        for obj in self:
            summer = 0
            if obj.solution_ids:
                for solution in obj.solution_ids:
                    summer += solution.total
            if obj.hectare_to_ga != 0:
                obj.norm = summer / obj.hectare_to_ga

    @api.depends('solution_ids')
    def _calculate_price(self):
        for obj in self:
            summer = 0
            if obj.solution_ids:
                for solution in obj.solution_ids:
                    summer += solution.total * solution.unit_price
            obj.price = summer

    fill_form_id = fields.Many2one('farming.fill.form', ondelete='cascade')
    first_balance = fields.Float('First Balance')
    solution_ids = fields.One2many('farming.protection.solution', 'charge_id', 'Ingredients', required=False)
    price = fields.Float('Price', compute=_calculate_price)
    size = fields.Float('Size')
    norm = fields.Float('Norm', compute=_calculate_norm)
    start_motor_hour = fields.Float('Start Motor Hour')
    finish_motor_hour = fields.Float('Finish Motor Hour')
    idle_hour = fields.Float('Idle Hour')
    result = fields.Float('Result', compute=_calculate_result)
    hectare_to_ga = fields.Float('Hectare to Ga')
    gps_ga = fields.Float('GPS Ga')

class FarmingProtectionSolution(models.Model):
    _name = 'farming.protection.solution'
    _description = 'Farming Protection Solution'

    @api.depends('norm', 'ga')
    def _calculate_total(self):
        for obj in self:
            summer = 0
            if obj.norm:
                if obj.ga:
                    summer += obj.ga * obj.norm
            obj.total = summer

    charge_id = fields.Many2one('farming.protection.solution.charge', ondelete='cascade')
    product_id = fields.Many2one('product.product', string='Products')
    unit_price = fields.Float('Unit Price', related='product_id.lst_price')
    norm = fields.Float('Solution Norm')
    ga = fields.Float('Ga')
    total = fields.Float('Total', compute=_calculate_total, store=True)
