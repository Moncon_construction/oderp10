# -*- coding: utf-8 -*-.

from odoo import api, fields, models, _  # @UnresolvedImport
from datetime import datetime, timedelta
from odoo.exceptions import UserError, ValidationError

import logging
_logger = logging.getLogger('odoo')


class FarmingGuidance(models.Model):
    _name = 'farming.guidance'
    _inherit = ['mail.thread']
    _order = 'farming_period_id DESC, farming_operation_id DESC'

    def _get_duration(self, start, stop):
        if start and stop:
            diff = fields.Datetime.from_string(stop) - fields.Datetime.from_string(start)
            if diff:
                duration = float(diff.days) * 24 + (float(diff.seconds) / 3600)
                return round(duration, 2)
            return 0.0

    number = fields.Char('Number', readonly=True)
    farming_period_id = fields.Many2one('farming.period', string='Farming period', track_visibility='onchange', required=True)
    farming_operation_id = fields.Many2one('task.type', string='Farming operation', required=True)
    date_start = fields.Datetime('Date start', required=True, default=fields.Datetime.now)
    date_stop = fields.Datetime(string='Date stop', default=fields.Datetime.now)
    allday = fields.Boolean(string='All Day', compute='_compute_allday')
    confirm_user_id = fields.Many2one('res.users', string='Confirm user', readonly=True)
    guidance = fields.Html('Guidance')
    performance = fields.Html(string='Performance Farming')
    norm_ids = fields.One2many('farming.guidance.norm', 'to_guidance' , string = 'Norms')
    state = fields.Selection([
                            ('draft', 'Draft'),
                            ('confirm', 'Confirm'),
                            ('done', 'Done')], string='States', copy=False, default='draft')
    assigned = fields.Many2one('hr.employee', string="Assigned")
    department_id = fields.Many2one('hr.department', related='assigned.department_id', string='Department id', readonly=True)
    end_date = fields.Date(string='End Date Farming')
    sector_id = fields.Many2one('res.sector', string='Sector Farming')
    company_id = fields.Many2one('res.company', string='Company', readonly=True, default=lambda self: self.env.user.company_id)
    technic_ids = fields.One2many('farming.guidance.technic', 'guidance_id')
    technology_ids = fields.One2many('farming.fill.form.technology', 'guidance_id')
    tag_ids = fields.Many2many('farming.guidance.tag', 'farming_guidance_to_tags', 'farming_guidance_id', 'tag_id', string='Tags')


    @api.multi
    @api.depends('date_start', 'date_stop')
    def _compute_allday(self):
        for obj in self:
            duration = self._get_duration(obj.date_start, obj.date_stop)
            if duration == 0.0 or duration % 12 == 0:
                obj.allday = True
            else:
                obj.allday = False


    @api.multi
    def load_tech(self):
        task_obj = self.env['farming.fill.form.template'].search([('task_id', '=', self.farming_operation_id.id)], limit=1)
        for obj in self:
            obj.technology_ids.unlink()
            for orders in task_obj.orders_ids:
                obj.technology_ids.create({
                    'guidance_id': obj.id,
                    'procedure_id': orders.id
                    })


    @api.model
    def create(self, vals):
        return super(FarmingGuidance, self).create(vals)

    @api.multi
    def unlink(self):
        for obj in self:
            if obj.state != 'draft':
                raise UserError(_('You can delete only draft guidance'))
        return super(FarmingGuidance, self).unlink()

    def confirm(self, vals):
        if self.farming_operation_id:
            short_code = self.farming_operation_id.short_code
            if short_code:
                code = short_code
            else:
                code = ''
            ser_num = self.env['ir.sequence'].get('farming.guidance')
            self.number = code + ser_num
            self.write({'state': 'confirm'})

    def done(self):
        if not self.performance:
            raise UserError(_('Please fill performance!'))
        if not self.end_date:
            raise UserError(_('Please fill end date!'))
        self.state = 'done'

    def action_to_draft(self):
        self.end_date = ''
        self.performance = ''
        self.state='draft'



    @api.multi
    def name_get(self):
        result = []
        for record in self:
            name = (record.sector_id.name or "") + ' | ' + (record.farming_operation_id.name or "")
            result.append((record.id, name))
        return result

class FarmingGuidanceNorm(models.Model):
    _name = 'farming.guidance.norm'

    sequence = fields.Integer('Sequence', default=1, required=True,)
    field_id = fields.Many2one('farming.square', track_visibility='onchange', string='Field registration')
    field_real_size = fields.Float('Field real size', readonly=True)
    production_size = fields.Float('Field production size', required=True)
    sort_id = fields.Many2one('cultivation.sort', string='Cultivation sort', required=True)
    type_id = fields.Many2one('cultivation.type', string='Cultivation type', required=True)
    farming_operation_id = fields.Many2one('task.type', string='Farming operation', required=True, related='to_guidance.farming_operation_id')
    product_id = fields.Many2one('product.product', string='Product', required=True, track_visibility='onchange')
    norms = fields.Float(string='Amount per sq.ha', required=True)
    product_quantity = fields.Integer(string='Product quantity', compute='_compute_product_quantity', store='True')
    unit_price = fields.Float('Unit price', compute='_compute_product_quantity', store='True')
    total_price = fields.Float('Total price', compute='_compute_product_quantity', store='True')
    description = fields.Text(string='Description')
    to_guidance = fields.Many2one('farming.guidance', ondelete='cascade')
    width = fields.Integer(string='Width')
    company_id = fields.Many2one('res.company')

    @api.multi
    @api.depends('product_id', 'norms', 'width')
    def _compute_product_quantity(self):
        for obj in self:
            # guid_id = obj.to_guidance.id
            if obj.product_id and obj.field_id:
                total = obj.field_id.square_perimeter * obj.width
                total += obj.production_size
                total *= obj.norms
                obj.product_quantity = total
                unit_cost_id = self.env['product.warehouse.standard.price'].search(
                    [('product_id', '=', obj.product_id.id)], limit=1)
                obj.unit_price = obj.total_price = 0
                for cost in unit_cost_id:
                    obj.unit_price = cost.standard_price
                    obj.total_price = cost.standard_price * obj.product_quantity

    @api.onchange('field_id')
    def onchange_field_id(self):
        for obj in self:
            if obj.field_id:
                _logger.info("onchange field id")
                obj.update({
                    'field_real_size': obj.field_id.standard_size,
                    'production_size': obj.field_id.standard_size,
                    'company_id': obj.field_id.company_id.id
                })

    @api.onchange('type_id')
    def onchange_type(self):
        if self.type_id:
            if self.sort_id.type != self.type_id:
                self.sort_id = False
            return {'domain': {'sort_id': [('type', 'in', self.type_id.ids)]}}
        else:
            return {'domain': {'sort_id': []}}

    @api.onchange('sort_id')
    def onchange_sort(self):
        if self.sort_id:
            self.type_id = self.sort_id.type


class FarmingGuidanceTechnic(models.Model):
    _name = 'farming.guidance.technic'

    guidance_id = fields.Many2one('farming.guidance', ondelete='cascade')
    technic_id = fields.Many2one('technic')
    aggregate_id = fields.Many2one('technic')
    technician_id = fields.Many2one('hr.employee', string='Technician')
    technic_config = fields.Char('Technic configuration')
    aggregate_config = fields.Char('Aggregate configuration')

class FarmingGuidanceTag(models.Model):
    _name = 'farming.guidance.tag'

    name = fields.Char(string='Tag Name', required=True, translate=True)
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id) 