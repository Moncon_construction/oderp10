# -*- coding: utf-8 -*-.

from odoo import api, fields, models, _  # @UnresolvedImport
from datetime import datetime, timedelta
from odoo.exceptions import UserError, ValidationError

import logging
_logger = logging.getLogger('odoo')


class FarmingPeriod(models.Model):
    _name = 'farming.period'
    _inherit = ['mail.thread']

    name = fields.Char('Name', required=True, track_visibility='onchange',)
    start_date = fields.Date(string="Start Date", track_visibility='onchange')
    stop_date = fields.Date(string='Stop Date', track_visibility='onchange')
