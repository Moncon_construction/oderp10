# -*- coding: utf-8 -*-.

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError

FARMING_STAGE = [('non_farming', 'Not farming'),
                ('soil', 'Soil processing'),
                   ('sowing', 'Seed sowing'),
                   ('seed_cleaning', 'Seed cleaning'),
                   ('fertilization', 'Fertilization'),
                   ('crop_protection', 'Crop protection'),
                   ('harvest', 'Harvesting'),
                    ('hay','Hay harvest')]


class FarmingGeneralGuidance(models.Model):
    _name = 'farming.general.guidance'
    _inherit = ['mail.thread']
    _rec_name = 'branch_id'

    branch_id = fields.Many2one('res.sector', string='Branch', readonly=True, related='field_id.sector_id')
    section_id = fields.Many2one('farming.section', readonly=True, related='field_id.section_id')
    field_id = fields.Many2one('farming.square')
    field_size = fields.Integer('Field size')
    field_registration = fields.Char('Field registration', readonly=True, related='field_id.registration')
    period_id = fields.Many2one('farming.period', string='Period', required=True)
    date = fields.Date('Date')
    confirm_user_id = fields.Many2one('res.users', string='Confirmer', readonly=True)
    soil_plan_ids = fields.One2many('farming.plan', 'to_guidance' , string = 'Soil plan', domain=[('farming_stage','=','soil')])
    sowing_plan_ids = fields.One2many('farming.plan', 'to_guidance' , string = 'Sowing plan', domain=[('farming_stage','=','sowing')])
    seed_cleaning_ids = fields.One2many('farming.plan', 'to_guidance' , string = 'Seed cleaning plan', domain=[('farming_stage','=','seed_cleaning')])
    fertilization_plan_ids = fields.One2many('farming.plan', 'to_guidance' , string = 'Fertilization plan', domain=[('farming_stage','=','fertilization')])
    protection_plan_ids = fields.One2many('farming.plan', 'to_guidance' , string = 'Crop protection plan', domain=[('farming_stage','=','crop_protection')])
    harvest_plan_ids = fields.One2many('farming.plan', 'to_guidance' , string = 'Harvest plan', domain=[('farming_stage','=','harvest')])
    company_id = fields.Many2one('res.company', readonly=True, related='field_id.company_id')
    state = fields.Selection([
                            ('draft', 'Draft'),
                            ('confirm', 'Confirm')], string='States', copy=False, default='draft')

    @api.onchange('field_id')
    def onchange_field_id(self):
        if self.field_id:
            self.field_size = self.field_id.standard_size

    @api.multi
    def unlink(self):
        for obj in self:
            if obj.state != 'draft':
                raise UserError(_('You can delete only draft guidance'))
        return super(FarmingGeneralGuidance, self).unlink()

    def confirm(self):
        self.state = 'confirm'
        self.confirm_user_id = self.env.user.id

    def action_to_draft(self):
        self.state= 'draft'


class FarmingPlan(models.Model):
    _name = 'farming.plan'

    # number = fields.Char('Number')
    # square_id = fields.Many2one('farming.square', string='Square', required=True)
    task_type_id = fields.Many2one('task.type', string='Operation',required=True)
    field_size = fields.Integer(string='Size', readonly='True')
    type_id = fields.Many2one('cultivation.type', string='Cultivation type')
    sort_id = fields.Many2one('cultivation.sort', string='Cultivation sort')
    technic_id = fields.Many2one('technic')
    aggregate_id = fields.Many2one('technic')
    product_id = fields.Many2one('product.product')
    norms = fields.Float(string='Amount per sq.ha', required=True)
    product_quantity = fields.Integer(string='Product quantity', compute='_compute_product_quantity', store='True')
    unit_price = fields.Float('Unit price', compute='_compute_product_quantity', store='True')
    total_price = fields.Float('Total price', compute='_compute_product_quantity', store='True')
    start_date = fields.Date('Start date', required=True)
    # end_date = fields.Date('End date')
    to_guidance = fields.Many2one('farming.general.guidance')
    farming_stage = fields.Selection(selection=FARMING_STAGE)

    @api.multi
    @api.depends('product_id', 'norms')
    def _compute_product_quantity(self):
        for obj in self:
            # guid_id = obj.to_guidance.id
            if obj.product_id and obj.to_guidance.field_id:
                # total = obj.field_id.square_perimeter * obj.width
                # total += obj.production_size
                obj.product_quantity = obj.to_guidance.field_size * obj.norms
                unit_cost_id = self.env['product.warehouse.standard.price'].search(
                    [('product_id', '=', obj.product_id.id)], limit=1)
                obj.unit_price = obj.total_price = 0
                for cost in unit_cost_id:
                    obj.unit_price = cost.standard_price
                    obj.total_price = cost.standard_price * obj.product_quantity

    @api.onchange('task_type_id')
    def onchange_task_type(self):
        if self.task_type_id:
            self.farming_stage = self.task_type_id.farming_stage



class TaskType(models.Model):
    _inherit = 'task.type'

    farming_stage = fields.Selection(string='Farming stage', selection=FARMING_STAGE, required=True,
                                     default='non_farming')