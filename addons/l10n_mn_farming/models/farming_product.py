# -*- coding: utf-8 -*-.

from odoo import api, fields, models


class CultivationType(models.Model):
    _name = 'cultivation.type'
    _inherit = ['mail.thread']

    name = fields.Char('Name', required=True)

class CultivationSort(models.Model):
    _name = 'cultivation.sort'
    _inherit = ['mail.thread']

    name = fields.Char('Name', required=True)
    type = fields.Many2one('cultivation.type', string='Cultivation Type')

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    cultivation_type = fields.Many2one('cultivation.type', string='Cultivation Type')
    cultivation_sort = fields.Many2one('cultivation.sort', string='Cultivation Sort')
    farming_product_type = fields.Selection(string='Farming product type', selection=[
                                            ('non_farming', 'Non-farming product'),
                                            ('seed', 'Seed'),
                                            ('protection', 'Crop protection'),
                                            ('fuel', 'Fuel'),
                                            ('fertilizer', 'Fertilizer')], default='non_farming')

    @api.onchange('cultivation_type')
    def onchange_cultivation_type(self):
        return {'domain': {'cultivation_sort': [('type', '=', self.cultivation_type.id)]}}