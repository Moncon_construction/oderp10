# -*- coding: utf-8 -*-.

from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo.exceptions import UserError, ValidationError

import logging
_logger = logging.getLogger('odoo')


class PlantProtectionSubstance(models.Model):
    _name = 'plant.protection.substance'
    _inherit = ['mail.thread']

    substance_id = fields.Many2one('product.product', string='Substance', required=False)
    state = fields.Selection([
                            ('liquid', 'Liquid'),
                            ('emulsion', 'Emulsion'),
                            ('dry', 'Dry')], string='States')
    price = fields.Float(compute="_compute_price", string="Price", store=True)
    plant_substance_type = fields.Selection([
                            ('seed_detergent', 'Seed Detergent'),
                            ('seed_activator', 'Seed Activator'),
                            ('screen', 'Screen'),
                            ('cultivation', 'Cultivation'),
                            ('insect', 'Insect'),
                            ('disease', 'Disease'),
                            ('accelerate_maturation', 'Accelerate maturation')], string='Plant Substance Type')
    package = fields.Float(string="Package")
    storage_date = fields.Float(string="Storage Date")
    general_norm = fields.Float(string="General Norm")
    substance_type = fields.Selection([
                            ('plant_substance', 'Plant protection substance'),
                            ('fertilizer', 'Fertilizer')], string='Substance type')
    composition = fields.Text(string="Composition")
    cultivation_norm_ids = fields.One2many('cultivation.norm', 'plant_cultivation_norm_id', string="Cultivation Norm")
    fertilization_id = fields.Many2one('product.product', string='Fertilization')
    fertilization_type = fields.Selection([
                            ('single', 'Single'),
                            ('mixed', 'Mixed'),
                            ('organic', 'Organic'),
                            ('liquid', 'Liquid')], string="Fertilization Type")
    elements_ids = fields.Many2many('component.elements', string="Component Elements")
    macro = fields.Text(string="Macro")
    micro = fields.Text(string="Micro")
    meso = fields.Text(string="Meso")

    @api.one
    @api.depends('substance_id')
    def _compute_price(self):
        self.price = self.substance_id.standard_price

    @api.model
    def default_get(self, flds):
        result = super(PlantProtectionSubstance, self).default_get(flds)
        if 'substance_type' in self.env.context:
            if self.env.context['substance_type'] == 'plant_substance':
                result['substance_type'] = 'plant_substance'
            else:
                result['substance_type'] = 'fertilizer'
        return result

class CultivationNorm(models.Model):
    _name = 'cultivation.norm'

    plant_cultivation_norm_id = fields.Many2one('plant.protection.substance', string="Cultivation Norm")
    cultivation_id = fields.Many2one('cultivation.type', string="Cultivation Norm")
    norm = fields.Float(string="Norm")