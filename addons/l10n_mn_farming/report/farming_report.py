# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, tools
# from odoo.fields import Datetime as fieldsDatetime


class FarmingReport(models.Model):
    _name = 'farming.report'
    _auto = False
    _order = 'date asc'

    farming_field_id = fields.Many2one('farming.square', 'Farming field', required=True)
    task_id = fields.Many2one('farming.fill.form')
    task_type_id = fields.Many2one('task.type', 'Task type', required=True)
    technic_id = fields.Many2one('technic', string='technic', )
    aggregate_id = fields.Many2one('technic', string='Aggregate', )
    date = fields.Datetime('Operation Date')
    cultivation_type_id = fields.Many2one('cultivation.type', string='Type', required=True)
    cultivation_sort_id = fields.Many2one('cultivation.sort', string='Sort', required=True)
    gps_hectare = fields.Float(string='Total hectare')
    motor_hour = fields.Float(string='Total motor hour')
    accountant_id = fields.Many2one('res.users', string='Accountant', readonly='True')
    # acc_id = fields.Many2one('hr.employee', string='Accountant', )
    idle_time = fields.Float(string='Idle hours')
    total_time = fields.Char(string='Total hours')
    guidance_id = fields.Many2one('farming.guidance', string='Guidance', )
    technician_id = fields.Many2one('hr.employee', string='Technician')
    sector_id = fields.Many2one('res.sector', string='Sector Farming')
    product_id = fields.Many2one('product.product', 'Product', required=True)
    charge_amount = fields.Float('Load amount')
    consumed_amount = fields.Float('Consumed amount')
    product_cost = fields.Float('Cost')
    total_cost = fields.Float('Total cost')


    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self._cr, 'farming_report')
        self._cr.execute("""
        CREATE VIEW farming_report AS (
            SELECT
	consumed_product.id as id,
	task.square_id as farming_field_id,
	task.id as task_id,
	task.template_id as task_type_id,
	task.technic_id as technic_id,
	task.aggregate_id as aggregate_id,
	task.end_time as date,
	csort.type as cultivation_type_id,
	task.sort_id as cultivation_sort_id,
	sum(task.total_gps_point) as gps_hectare,
	sum(task.end_mot - task.start_mot) as motor_hour,
	task.acc_id as accountant_id,
	sum(task.idle_time) as idle_time,
	task.guidance_id as guidance_id,
	task.technician_id as technician_id,
	s.sector_id as sector_id,
	consumed_product.product_id as product_id,
	sum(consumed_product.charge_number) as charge_amount,
	sum(consumed_product.spending_number) as consumed_amount,
	avg(price.standard_price) as product_cost,
	consumed_product.spending_number * avg(price.standard_price) as total_cost
	
FROM farming_fill_form_product AS consumed_product
JOIN farming_fill_form task on consumed_product.fill_form_id = task.id
JOIN farming_square s on task.square_id = s.id
JOIN cultivation_sort csort on task.sort_id = csort.id
JOIN technic ON task.technic_id = technic.id
JOIN technic agg ON task.aggregate_id = agg.id
JOIN product_warehouse_standard_price price on consumed_product.product_id = price.product_id

GROUP BY consumed_product.id, task.id, task.template_id, task.technic_id, task.aggregate_id, task.end_time, task.sort_id, task.technician_id, task.guidance_id, task.acc_id, s.sector_id, consumed_product.product_id, csort.type, task.square_id )""")
