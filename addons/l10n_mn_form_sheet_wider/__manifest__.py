# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Form sheet full width",
    'version': '1.0',
    'depends': ['base'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
    Sets form sheet width as full size
    """,
    "data": [
        "view/fullwidth.xml",
    ],
    "installable": True,
}