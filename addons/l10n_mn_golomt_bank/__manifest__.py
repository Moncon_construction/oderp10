# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Golomt bank integration",
    'version': '1.0',
    'depends': ['l10n_mn_account',
                'sales_team',
                'l10n_mn_sale'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
    Golomt bank integration for account
    """,
    "data": [
        "security/res_groups.xml",
        "views/golomt_config_log.xml",
        "security/ir.model.access.csv",
        "views/golomt_config_settings_view.xml",
        "views/res_bank_view.xml",
        "wizard/vasco_transaction_wizard_view.xml",
        "wizard/golomt_get_statement_view.xml",
        "views/bank_statement_view.xml",
        "views/account_journal.xml",
    ],
    "installable": True,
}