# coding=utf-8
from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError
import xml.etree.ElementTree as ET
from requests import ConnectionError
from datetime import datetime
import requests
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT


def compute_golomt_result(request, response):
    field_values = {'golomt_corporate_gateway_error_response': False,
                    'golomt_corporate_gateway_sent_request': False,
                    'golomt_corporate_gateway_response': False,
                    'golomt_id': False,
                    'golomt_bank_transaction_state': False}
    if request is False or request is None:
        field_values['golomt_bank_transaction_state'] = 'error'
        field_values['golomt_corporate_gateway_error_response'] = str(response)
    else:
        field_values['golomt_corporate_gateway_sent_request'] = ET.tostring(request, encoding='utf8', method='xml')
        if isinstance(response, Exception) or isinstance(response, ConnectionError):
            if not response:
                field_values['golomt_bank_transaction_state'] = 'error'
                field_values['golomt_corporate_gateway_error_response'] = _('Unable to send request ->(Check your connection)')
            else:
                field_values['golomt_bank_transaction_state'] = 'error'
                field_values['golomt_corporate_gateway_error_response'] = _('Unable to send request ->(') + str(response) + ')'
        else:
            field_values['golomt_corporate_gateway_response'] = response
            response_xml = ET.fromstring(response)
            if response_xml.find('Header').find('ResponseHeader').find('Status').text == 'FAILED':
                field_values['golomt_bank_transaction_state'] = 'error'
                error_detail = response_xml.find('Body').find('Error').find('ErrorDetail')
                msg = error_detail.find('ErrorDesc').text
                code = error_detail.find('ErrorCode').text
                field_values['golomt_corporate_gateway_error_response'] = '(' + code + ') ' + msg
            else:
                field_values['golomt_id'] = response_xml.find('Body').find('TrnAddRs').find('TrnIdentifier').find('TrnId').text
                field_values['golomt_bank_transaction_state'] = 'success'
    return field_values


class AccountBankStatement(models.Model):

    _inherit = 'account.bank.statement'

    state = fields.Selection(selection_add=[('paid_with_bank', _('Paid with Bank'))])
    confirmable_with_golomt = fields.Boolean(compute='_compute_confirmable_with_golomt')

    @api.multi
    def _compute_confirmable_with_golomt(self):
        for obj in self:
            if obj.journal_id.bank_id.integrated_bank and obj.journal_id.bank_id.integrated_bank == 'golomt' and obj.journal_id.is_golomt_journal:
                obj.confirmable_with_golomt = True
            else:
                obj.confirmable_with_golomt = False

    @api.multi
    def action_to_paid_with_golomt_corporate_gateway(self, context=False, vasco_code=False):
        lines = self.line_ids.filtered(lambda l: l.golomt_bank_transaction_state not in ('success', 'statement') and l.amount < 0)
        lines.action_to_paid_with_golomt_corporate_gateway(context=context, vasco_code=vasco_code)
        lines = lines.filtered(lambda l: l.golomt_bank_transaction_state != 'success')
        if not lines:
            self.state = 'paid_with_bank'

    @api.multi
    def button_golomt_confirm(self):
        if not self.env.user.company_id.use_vasco_device:
            return self.action_to_paid_with_golomt_corporate_gateway()
        else:
            # context нь frozendict тул засахын тулд self-г дахин оноосон
            self = self.with_context(golomt_model=self._name, golomt_ids=self.ids)
            return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'golomt.vasco.transaction.wizard',
                'view_id': self.env.ref('l10n_mn_golomt_bank.l10n_mn_transaction_wizard_form_view').id,
                'context': self.env.context,
                'flags': {'initial_mode': 'edit'},
                'target': 'new',
            }

    @api.multi
    def button_confirm_bank(self):
        res = super(AccountBankStatement, self).button_confirm_bank()
        for obj in self:
            obj.line_ids.write({'payable_with_golomt': obj.confirmable_with_golomt})
        return res

    @api.multi
    def button_cancel(self):
        for obj in self:
            if obj.line_ids.filtered(lambda l: l.golomt_bank_transaction_state == 'success'):
                raise UserError(_('You cannot cancel statement line if its successfully transferred with golomt.'))
        return super(AccountBankStatement, self).button_cancel()

    @api.multi
    def button_draft(self):
        for obj in self:
            if obj.line_ids.filtered(lambda l: l.golomt_bank_transaction_state == 'success'):
                raise UserError(_('You cannot cancel statement line if its successfully transferred with golomt.'))

    @api.multi
    def unlink(self):
        for obj in self:
            if obj.line_ids.filtered(lambda l: l.golomt_bank_transaction_state == 'success'):
                raise UserError(_('You cannot delete statement if its successfully made transaction using golomt'))
        return super(AccountBankStatement, self).unlink()

    @api.multi
    def golomt_get_statements(self, start_date, end_date, vasco_code=False):
        xml, response = self.env.user.company_id.golomt_statement_confirm(self.mapped('journal_id'), start_date, end_date, vasco_code=vasco_code)
        if xml is False or xml is None:
            raise UserError(_('There was an error while getting statements:\n %s') % str(response))
        else:
            if isinstance(response, Exception) or isinstance(response, ConnectionError):
                if not response:
                    raise UserError(_('Unable to send request ->(Check your connection)'))
                else:
                    raise UserError(_('Unable to send request ->(') + str(response) + ')')
            else:
                response_xml = ET.fromstring(response)
                xml_string = ET.tostring(xml, encoding='utf8', method='xml')
                if response_xml.find('Header').find('ResponseHeader').find('Status').text == 'FAILED':
                    error_detail = response_xml.find('Body').find('Error').find('ErrorDetail')
                    msg = error_detail.find('ErrorDesc').text
                    code = error_detail.find('ErrorCode').text
                    raise UserError('(' + code + ') ' + msg)
                else:
                    entries = response_xml.find('EnqRsp')
                    statement_line = self.env['account.bank.statement.line']
                    for obj in entries.findall('Ntry'):
                        if statement_line.search([('golomt_id', '=', obj.find('NtryRef').text)]):
                            continue
                        entry_date = datetime.strptime(obj.find('TxDt').text, '%m/%d/%Y').date().strftime(DEFAULT_SERVER_DATE_FORMAT)
                        statement_obj = self.filtered(lambda l: l.date == entry_date)
                        vals = {'statement_id': statement_obj.id,
                                'name': obj.find('TxAddInf').text,
                                'date': entry_date,
                                'amount': float(obj.find('Amt').text),
                                'golomt_partner_name': obj.find('CtAcct').text,
                                'golomt_bank_transaction_state': 'statement',
                                'golomt_id': obj.find('NtryRef').text,
                                'golomt_corporate_gateway_response': ET.tostring(obj, encoding='utf8', method='xml'),
                                'golomt_corporate_gateway_sent_request': xml_string,
                                }
                        if obj.find('TxRt').text == '1':
                            vals['amount'] *= -1
                        statement_line.create(vals)
                self.filtered(lambda l: len(l.line_ids) != 0).calculate_after_bank_statements()
                empties = self.filtered(lambda l: len(l.line_ids) == 0)
                empties.unlink()


class AccountBankStatementLine(models.Model):

    _inherit = 'account.bank.statement.line'

    payable_with_golomt = fields.Boolean(string='Payable with golomt')
    golomt_corporate_gateway_error_response = fields.Char('Error message', copy=False)
    golomt_corporate_gateway_sent_request = fields.Char('Request sent to corporate gateway', copy=False)
    golomt_corporate_gateway_response = fields.Char('Response of corporate gateway', copy=False)
    golomt_id = fields.Char('Golomt transaction id', copy=False)
    golomt_bank_transaction_state = fields.Selection([('success', _('Successfully paid')), ('statement', _('Downloaded statement')),
                                                      ('error', _('Failed'))])
    golomt_partner_name = fields.Char(string='Golomt partner name')

    @api.multi
    def action_to_paid_with_golomt_corporate_gateway(self, context=False, vasco_code=False):
        self.env.user.company_id.check_fields()
        if self.filtered(lambda l: l.state != 'confirm' or l.amount > 0):
            raise UserError(_('You can only Confirm with golomt when bank statement is confirmed and amount is lower than 0(cost)'))
        for obj in self.filtered(lambda l: l.golomt_bank_transaction_state not in ('success', 'statement') and l.payable_with_golomt is True):
            if obj.date != datetime.now().date().strftime(DEFAULT_SERVER_DATE_FORMAT):
                obj.write({'golomt_corporate_gateway_response': _("You can confirm only today's statement"),
                           'golomt_bank_transaction_state': 'error'})
                continue
            request, response = obj.company_id.golomt_confirm(obj.journal_id, obj.bank_account_id, obj.amount * -1,
                                                              obj.name, vasco_code=vasco_code, amount_limit=False)
            # Хүсэлт амжилттай болсны дараа буцаах боломжгүй болно. Хүсэлт илгээсний дараа зөвхөн write функц ажиллаж байгаа
            # write функц дээр raise өгөх эсвэл кодын алдаа байгаа бол rollback хийгдэж юм хадгалагдахгүй.
            # Нэг үгээр данснаас чинь мөнгө гарчихсан гэхдээ erp дээр тэгэж харагдахгүй гэсэн үг
            # Тиймээс write функц өөрчилхөөр бол болгоомжтой байцгаагаарай
            result = compute_golomt_result(request, response)
            obj.write(result)

    @api.multi
    def button_golomt_confirm(self):
        if not self.env.user.company_id.use_vasco_device:
            return self.action_to_paid_with_golomt_corporate_gateway()
        else:
            # context нь frozendict тул засахын тулд self-г дахин оноосон
            self = self.with_context(golomt_model=self._name, golomt_ids=self.ids)
            return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'golomt.vasco.transaction.wizard',
                'view_id': self.env.ref('l10n_mn_golomt_bank.l10n_mn_transaction_wizard_form_view').id,
                'context': self.env.context,
                'flags': {'initial_mode': 'edit'},
                'target': 'new',
            }

    @api.multi
    def button_confirm_bank(self):
        res = super(AccountBankStatementLine, self).button_confirm_bank()
        payable = self.mapped('statement_id').confirmable_with_golomt
        self.write({'payable_with_golomt': payable})
        return res

    @api.multi
    def button_cancel_reconciliation(self):
        for obj in self:
            if obj.golomt_bank_transaction_state == 'success':
                raise UserError(_('You cannot cancel statement line if its successfully transferred with golomt.'))
        super(AccountBankStatementLine, self).button_cancel_reconciliation()

    @api.constrains('journal_entry_ids')
    def constraint_bank_statement(self):
        if self.golomt_bank_transaction_state == 'success':
            raise UserError(_('You cannot cancel statement line if its successfully transferred with golomt.'))

    @api.multi
    def unlink(self):
        for obj in self:
            if obj.golomt_bank_transaction_state == 'success':
                raise UserError(_('You cannot delete statement if its successfully made transaction using golomt'))
        return super(AccountBankStatementLine, self).unlink()
