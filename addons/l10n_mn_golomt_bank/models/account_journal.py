# coding=utf-8
from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError
from requests import ConnectionError
import xml.etree.ElementTree as ET
from datetime import datetime
import requests


class AccountJournal(models.Model):
    _inherit = "account.journal"

    vasco_device_number = fields.Char(string='Vasco device number')
    golomt_statement = fields.Boolean(related='company_id.download_bank_statement')
    is_golomt_journal = fields.Boolean(default=False, string='Is golomt journal')
    golomt_bank_balance = fields.Float(string='Bank account balance')
    golomt_bank_balance_date = fields.Date(string='Bank account balance date')

    def get_golomt_balance_button(self):
        self = self.with_context(golomt_model=self._name, golomt_ids=self.ids)
        if not self.company_id.use_vasco_device:
            return self.get_golomt_balance()
        else:
            return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'golomt.vasco.transaction.wizard',
                'view_id': self.env.ref('l10n_mn_golomt_bank.l10n_mn_transaction_wizard_form_view_for_balance').id,
                'context': self.env.context,
                'flags': {'initial_mode': 'edit'},
                'target': 'new',
            }

    @api.one
    def get_golomt_balance(self, context=False, vasco_code=False):
        self.ensure_one()
        xml, response = self.company_id.golomt_balance_confirm(self, vasco_code=vasco_code)
        if xml is False or xml is None:
            raise UserError(_('There was an error while getting statements:\n %s') % str(response))
        else:
            if isinstance(response, Exception) or isinstance(response, ConnectionError):
                if not response:
                    raise UserError(_('Unable to send request ->(Check your connection)'))
                else:
                    raise UserError(_('Unable to send request ->(') + str(response) + ')')
            else:
                response_xml = ET.fromstring(response)
                if response_xml.find('Header').find('ResponseHeader').find('Status').text == 'FAILED':
                    error_detail = response_xml.find('Body').find('Error').find('ErrorDetail')
                    msg = error_detail.find('ErrorDesc').text
                    code = error_detail.find('ErrorCode').text
                    raise UserError('(' + code + ') ' + msg)
                else:
                    enqrsp = response_xml.find('EnqRsp')
                    balance = enqrsp.find('ABal').text.replace(',', '')
                    self.write({'golomt_bank_balance': balance,
                                'golomt_bank_balance_date': enqrsp.find('RptDt').text})

    # OVERRIDE
    def set_bank_account(self, acc_number, bank_id=None):
        """ Create a res.partner.bank and set it as value of the  field bank_account_id
            OVERRIDE: Банкны дансыг тусд нь үүсгэсэн тохиолдолыг шалгахгүйгээр дахин үүсгэх гэж оролдож байсан.
                      Тэр үед дансны дугаар давхардсан гэх constraint зааж байсан.
        """
        self.ensure_one()
        acc = self.env['res.partner.bank'].search([('acc_number', '=', acc_number)], limit=1)
        if acc:
            self.bank_account_id = acc.id
        else:
            self.bank_account_id = self.env['res.partner.bank'].create({
                'acc_number': acc_number,
                'bank_id': bank_id,
                'company_id': self.company_id.id,
                'currency_id': self.currency_id.id,
                'partner_id': self.company_id.partner_id.id,
            }).id

    @api.multi
    def write(self, vals):
        if vals.get('bank_acc_number') and vals['bank_acc_number']:
            acc = self.env['res.partner.bank'].search([('acc_number', '=', vals['bank_acc_number'])], limit=1)
            vals['bank_account_id'] = acc.id if acc else self.env['res.partner.bank'].create({
                'acc_number': vals['bank_acc_number'],
                'bank_id': vals['bank_id'] if vals.get('bank_id') else self.bank_id.id,
                'company_id': self.company_id.id,
                'currency_id': self.currency_id.id,
                'partner_id': self.company_id.partner_id.id,
            }).id
        return super(AccountJournal, self).write(vals)
