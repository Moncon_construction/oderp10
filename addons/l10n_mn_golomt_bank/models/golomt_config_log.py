# coding=utf-8
from odoo import _, api, fields, models


class GolomtConfigLog(models.Model):
    _name = 'golomt.config.log'
    _order = 'create_date DESC'

    ip_address = fields.Char('Ip address')
    changed_fields = fields.Text('Changed fields')
