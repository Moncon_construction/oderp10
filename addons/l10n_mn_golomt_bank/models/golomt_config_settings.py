# -*- coding: utf-8 -*-
from odoo import fields, models, api,_
from odoo.exceptions import UserError
from odoo.http import request


class GolomtConfigSettings(models.TransientModel):
    _name = 'golomt.config.settings'
    _inherit = 'res.config.settings'

    def get_golomt_corporate_gateway_account_id_domain(self):
        return [('company_id', '=', self.env.user.company_id.id)]

    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)

    use_golomt_corporate_gateway = fields.Boolean(related='company_id.use_golomt_corporate_gateway', string='Use golomt corporate gateway')
    golomt_corporate_gateway_account_journals = fields.Many2many('account.journal', related='company_id.golomt_corporate_gateway_account_journals',
                                                                 string='Transaction account journals of Golomt corporate gateway',
                                                                 domain=get_golomt_corporate_gateway_account_id_domain)
    golomt_corporate_gateway_url = fields.Char(related='company_id.golomt_corporate_gateway_url', string='URL for sending request')
    golomt_corporate_gateway_login_id = fields.Char(related='company_id.golomt_corporate_gateway_login_id', string='Login for Golomt corporate gateway')
    golomt_corporate_gateway_anybic = fields.Char(related='company_id.golomt_corporate_gateway_anybic', string='Anybic for Golomt corporate gateway')
    golomt_corporate_gateway_maximum_amount = fields.Float(related='company_id.golomt_corporate_gateway_maximum_amount', string='Maximum amount per transaction')
    use_vasco_device = fields.Boolean(related='company_id.use_vasco_device', string='Use Vasco device', default=True)
    company_name_registered_in_golomt = fields.Char(related='company_id.company_name_registered_in_golomt', string='Registered Company_name')
    connected_bank_ids = fields.Many2many(related='company_id.connected_bank_ids', string='Connected banks')
    limit_placed_on_user = fields.Boolean(related='company_id.limit_placed_on_user', string='Place limit on users')
    limited_user_ids = fields.Many2many(related='company_id.limited_user_ids', string='Users to limit')
    download_bank_statement = fields.Boolean(related='company_id.download_bank_statement', string='Download bank statement')
    bank_statement_download_date = fields.Date(related='company_id.bank_statement_download_date', string='Start downloading bank statement from')

    @api.multi
    def execute(self):
        if self.golomt_corporate_gateway_maximum_amount <= 0:
            raise UserError(_("'Maximum amount per transaction' field must be higher than 0"))
        return super(GolomtConfigSettings, self).execute()

    @api.model
    def create(self, vals):
        log = self.env['golomt.config.log']
        company = self.env.user.company_id
        text = ''
        for obj in vals:
            if vals[obj] != company[obj]:
                if isinstance(company[obj], object) and isinstance(vals[obj], list):
                    ids = [ls[1] for ls in vals[obj]]
                    if ids != company[obj].ids:
                        text += '%s : %s -> %s\n' % (self.fields_get(obj)[obj]['string'], company[obj].ids, ids)
                else:
                    text += '%s : %s -> %s\n' % (self.fields_get(obj)[obj]['string'], company[obj], vals[obj])
            if obj == 'golomt_corporate_gateway_account_journals':
                journal_model = self.env['account.journal']
                ids = [ls[1] for ls in vals[obj]]
                to_add = set(ids) - set(company[obj].ids)
                to_remove = set(company[obj].ids) - set(ids)
                journal_model.browse(to_add).write({'is_golomt_journal': True})
                journal_model.browse(to_remove).write({'is_golomt_journal': False, 'vasco_device_number': False})
        ip_address = request.httprequest.environ['REMOTE_ADDR']
        if text:
            log.create({'ip_address': ip_address,
                        'changed_fields': text})
        return super(GolomtConfigSettings, self).create(vals)



