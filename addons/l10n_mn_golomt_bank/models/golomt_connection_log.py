# -*- coding: utf-8 -*-

from odoo import _, api, fields, models
from odoo.http import request


class GolomtConnectionLog(models.Model):
    _name = 'golomt.connection.log'
    _order = 'create_date DESC'

    ip_address = fields.Char('Ip address')
    request_type = fields.Selection([('statement', _('Bank statement')),
                                     ('transaction', _('Bank transaction')),
                                     ('balance', _('Bank balance'))])
    sent_request = fields.Text('Sent request')
    received_response = fields.Text('received response')

    @api.model
    def create(self, vals):
        vals['ip_address'] = request.httprequest.environ['REMOTE_ADDR']
        return super(GolomtConnectionLog, self).create(vals)

