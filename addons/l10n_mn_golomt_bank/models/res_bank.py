# -*- coding: utf-8 -*-
from odoo import models, fields, _


class ResBank(models.Model):
    _inherit = 'res.bank'

    BICFI_code_for_golomt_bank = fields.Char('BICFI Code for Golomt bank')
    # For the record, this selection field is not my idea. CEO said it is easier to get if we add field that selects a bank in a bank.
    integrated_bank = fields.Selection(selection_add=[('golomt', _('Golomt'))])


class ResPartnerBank(models.Model):
    _inherit = 'res.partner.bank'

    is_tax_account = fields.Boolean(string='Is tax account', default=False)
