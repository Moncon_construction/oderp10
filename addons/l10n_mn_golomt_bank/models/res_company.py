# -*- coding: utf-8 -*-
from datetime import datetime
import xml.etree.ElementTree as ET
from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError
import requests
from dateutil.relativedelta import relativedelta
from requests.auth import HTTPBasicAuth
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, pytz
import time
DEFAULT_GOLOMT_PASSWORD = '603533'


class ResCompany(models.Model):
    _inherit = 'res.company'

    def get_golomt_corporate_gateway_account_id_domain(self):
        return [('company_id', '=', self.env.user.company_id.id)]

    use_golomt_corporate_gateway = fields.Boolean('Use golomt corporate gateway')
    golomt_corporate_gateway_account_journals = fields.Many2many('account.journal', string='Transaction account journals of Golomt corporate gateway',
                                                                 domain=get_golomt_corporate_gateway_account_id_domain)
    golomt_corporate_gateway_url = fields.Char(string='URL for sending request')
    golomt_corporate_gateway_login_id = fields.Char(string='Login for Golomt corporate gateway')
    golomt_corporate_gateway_anybic = fields.Char(string='Anybic for Golomt corporate gateway')
    use_vasco_device = fields.Boolean(string='Use Vasco device', default=True)
    company_name_registered_in_golomt = fields.Char(string='Registered Company name')
    connected_bank_ids = fields.Many2many('res.bank', string='Connected banks')
    limit_placed_on_user = fields.Boolean('Place limit on users')
    limited_user_ids = fields.Many2many('res.users', string='Users to limit')
    golomt_corporate_gateway_maximum_amount = fields.Float(string='Maximum amount per transaction')
    download_bank_statement = fields.Boolean('Download bank statement')
    bank_statement_download_date = fields.Date('Start downloading bank statement from')

    def check_fields(self):
        if not self.currency_id:
            raise UserError(_("'Company currency' is not set on %s.\n Please contact your administrator'") % self.name)
        if not self.use_golomt_corporate_gateway:
            raise UserError(_("'Golomt corporate gateway configuration' is not set on %s.\n Please contact your administrator") % self.name)
        if not self.golomt_corporate_gateway_url:
            raise UserError(_('"URL for sending request" is not set on %s.\n Please contact your administrator') % self.name)
        if not self.golomt_corporate_gateway_login_id:
            raise UserError(_('"Login for Golomt corporate gateway" is not set on %s.\n Please contact your administrator') % self.name)
        if not self.golomt_corporate_gateway_anybic:
            raise UserError(_('"Anybic for Golomt corporate gateway" is not set on %s.\n Please contact your administrator') % self.name)
        if not self.golomt_corporate_gateway_maximum_amount:
            raise UserError(_('"Maximum amount per transaction" is not set on %s.\n Please contact your administrator') % self.name)

    def get_statement_xml(self, journal_id, start_date, end_date, vasco_code=False):
        xml_request = '<?xml version="1.0" encoding="UTF-8"?><Document></Document>'
        root = ET.fromstring(xml_request)
        grphdr = ET.Element('GrpHdr')
        # creating header elements
        tz = pytz.timezone(self.env.user.partner_id.tz) if self.env.user.partner_id.tz else pytz.utc
        now_utc = datetime.now(pytz.timezone('UTC'))
        now_user_zone = now_utc.astimezone(tz).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        timestamp = str(int(time.mktime(datetime.strptime(now_user_zone, DEFAULT_SERVER_DATETIME_FORMAT).timetuple())))
        now_user_zone = now_user_zone.replace(' ', 'T')

        # Trace number(MsgId) is said to be timestamp of now
        msgid = ET.Element('MsgId')
        msgid.text = timestamp
        grphdr.append(msgid)

        # CredDtTm is now
        credttm = ET.Element('CreDtTm')
        credttm.text = now_user_zone
        grphdr.append(credttm)

        txscd = ET.Element('TxsCd')
        # TxsCd is the code of transaction
        txscd.text = '5004'
        grphdr.append(txscd)

        # InitgPty is the company
        initgpty = ET.Element('InitgPty')
        init_id = ET.Element('Id')
        init_og_id = ET.Element('OrgId')
        anybic = ET.Element('AnyBIC')
        anybic.text = self.golomt_corporate_gateway_anybic
        init_og_id.append(anybic)
        init_id.append(init_og_id)
        initgpty.append(init_id)
        grphdr.append(initgpty)

        # Crdtl Credentials
        crdtl = ET.Element('Crdtl')
        lang = ET.Element('Lang')
        lang.text = '0'
        crdtl.append(lang)
        login_id = ET.Element('LoginID')
        pwds = ET.Element('Pwds')
        pwdtype = ET.Element('PwdType')
        pwd = ET.Element('Pwd')
        pwdtype.text = '3'
        pwds.append(pwdtype)
        if self.use_vasco_device and vasco_code:
            login_id.text = journal_id.vasco_device_number
            pwd.text = vasco_code if vasco_code else DEFAULT_GOLOMT_PASSWORD
        else:
            login_id.text = self.golomt_corporate_gateway_login_id
            pwd.text = DEFAULT_GOLOMT_PASSWORD
        pwds.append(pwd)
        role_id = ET.Element('RoleID')
        role_id.text = '1'
        crdtl.append(login_id)
        crdtl.append(role_id)
        crdtl.append(pwds)
        grphdr.append(crdtl)

        enqinf = ET.Element('EnqInf')

        iban = ET.Element('IBAN')
        iban.text = journal_id.bank_acc_number
        enqinf.append(iban)

        ccy = ET.Element('Ccy')
        ccy.text = self.currency_id.name
        enqinf.append(ccy)

        frdt = ET.Element('FrDt')
        frdt.text = start_date
        enqinf.append(frdt)

        todt = ET.Element('ToDt')
        todt.text = end_date
        enqinf.append(todt)

        jrno = ET.Element('JrNo')
        enqinf.append(jrno)

        stnum = ET.Element('StNum')
        stnum.text = '1'
        enqinf.append(stnum)

        endnum = ET.Element('EndNum')
        endnum.text = '200'
        enqinf.append(endnum)

        root.append(grphdr)
        root.append(enqinf)
        return root

    def get_balance_xml(self, journal_id, vasco_code=False):
        xml_request = '<?xml version="1.0" encoding="UTF-8"?><Document></Document>'
        root = ET.fromstring(xml_request)
        grphdr = ET.Element('GrpHdr')
        # creating header elements
        tz = pytz.timezone(self.env.user.partner_id.tz) if self.env.user.partner_id.tz else pytz.utc
        now_utc = datetime.now(pytz.timezone('UTC'))
        now_user_zone = now_utc.astimezone(tz).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        timestamp = str(int(time.mktime(datetime.strptime(now_user_zone, DEFAULT_SERVER_DATETIME_FORMAT).timetuple())))
        now_user_zone = now_user_zone.replace(' ', 'T')

        # Trace number(MsgId) is said to be timestamp of now
        msgid = ET.Element('MsgId')
        msgid.text = timestamp
        grphdr.append(msgid)

        # CredDtTm is now
        credttm = ET.Element('CreDtTm')
        credttm.text = now_user_zone
        grphdr.append(credttm)

        txscd = ET.Element('TxsCd')
        # TxsCd is the code of transaction
        txscd.text = '5003'
        grphdr.append(txscd)

        # InitgPty is the company
        initgpty = ET.Element('InitgPty')
        init_id = ET.Element('Id')
        init_og_id = ET.Element('OrgId')
        anybic = ET.Element('AnyBIC')
        anybic.text = self.golomt_corporate_gateway_anybic
        init_og_id.append(anybic)
        init_id.append(init_og_id)
        initgpty.append(init_id)
        grphdr.append(initgpty)

        # Crdtl Credentials
        crdtl = ET.Element('Crdtl')
        lang = ET.Element('Lang')
        lang.text = '0'
        crdtl.append(lang)
        login_id = ET.Element('LoginID')
        pwds = ET.Element('Pwds')
        pwdtype = ET.Element('PwdType')
        pwd = ET.Element('Pwd')
        pwdtype.text = '3'
        pwds.append(pwdtype)
        if self.use_vasco_device and vasco_code:
            login_id.text = journal_id.vasco_device_number
            pwd.text = vasco_code if vasco_code else DEFAULT_GOLOMT_PASSWORD
        else:
            login_id.text = self.golomt_corporate_gateway_login_id
            pwd.text = DEFAULT_GOLOMT_PASSWORD
        pwds.append(pwd)
        role_id = ET.Element('RoleID')
        role_id.text = '1'
        crdtl.append(login_id)
        crdtl.append(role_id)
        crdtl.append(pwds)
        grphdr.append(crdtl)

        enqinf = ET.Element('EnqInf')

        iban = ET.Element('IBAN')
        iban.text = journal_id.bank_acc_number
        enqinf.append(iban)

        ccy = ET.Element('Ccy')
        ccy.text = self.currency_id.name
        enqinf.append(ccy)

        root.append(grphdr)
        root.append(enqinf)
        return root

    def get_xml(self, journal_id, bank_account_id, mnt_amount, transaction_value, vasco_code=False):
        xml_request = '<?xml version="1.0" encoding="UTF-8"?><Document></Document>'
        root = ET.fromstring(xml_request)
        # creating header
        grphdr = ET.Element('GrpHdr')
        # creating header elements
        tz = pytz.timezone(self.env.user.partner_id.tz) if self.env.user.partner_id.tz else pytz.utc
        now_utc = datetime.now(pytz.timezone('UTC'))
        now_user_zone = now_utc.astimezone(tz).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        timestamp = str(int(time.mktime(datetime.strptime(now_user_zone, DEFAULT_SERVER_DATETIME_FORMAT).timetuple())))
        now_user_zone = now_user_zone.replace(' ', 'T')
        # Trace number(MsgId) is said to be timestamp of now
        msgid = ET.Element('MsgId')
        msgid.text = timestamp
        grphdr.append(msgid)

        # CredDtTm is now
        credttm = ET.Element('CreDtTm')
        credttm.text = now_user_zone
        grphdr.append(credttm)

        txscd = ET.Element('TxsCd')
        if bank_account_id.is_tax_account:
            code = 1008
        elif bank_account_id.bank_id.BICFI_code_for_golomt_bank == 'GMT':
            code = 1001
        else:
            code = 1002
        # TxsCd is the code of transaction
        txscd.text = str(code)
        grphdr.append(txscd)

        # NbOfTxs is number of transaction
        nboftxs = ET.Element('NbOfTxs')
        nboftxs.text = '1'
        grphdr.append(nboftxs)

        # CtrlSum is the total amount of all transaction
        ctrlsum = ET.Element('CtrlSum')
        ctrlsum.text = str(int(mnt_amount))
        grphdr.append(ctrlsum)

        # InitgPty is the company
        initgpty = ET.Element('InitgPty')
        init_id = ET.Element('Id')
        init_og_id = ET.Element('OrgId')
        anybic = ET.Element('AnyBIC')
        anybic.text = self.golomt_corporate_gateway_anybic
        init_og_id.append(anybic)
        init_id.append(init_og_id)
        initgpty.append(init_id)
        grphdr.append(initgpty)

        # Crdtl Credentials
        crdtl = ET.Element('Crdtl')
        lang = ET.Element('Lang')
        lang.text = '0'
        crdtl.append(lang)
        login_id = ET.Element('LoginID')
        pwds = ET.Element('Pwds')
        pwdtype = ET.Element('PwdType')
        pwd = ET.Element('Pwd')
        pwdtype.text = '3'
        pwds.append(pwdtype)
        if self.use_vasco_device and vasco_code:
            login_id.text = journal_id.vasco_device_number
            pwd.text = vasco_code if vasco_code else DEFAULT_GOLOMT_PASSWORD
        else:
            login_id.text = self.golomt_corporate_gateway_login_id
            pwd.text = DEFAULT_GOLOMT_PASSWORD
        pwds.append(pwd)
        role_id = ET.Element('RoleID')
        role_id.text = '1'
        crdtl.append(login_id)
        crdtl.append(role_id)
        crdtl.append(pwds)
        grphdr.append(crdtl)
        # Creating Payment info
        pmtinf = ET.Element('PmtInf')
        pmtinf.append(nboftxs)
        pmtinf.append(ctrlsum)

        fort = ET.Element('ForT')
        fort.text = 'F'
        pmtinf.append(fort)

        # Dbtr Information of debtor
        dbtr = ET.Element('Dbtr')
        nm = ET.Element('Nm')
        nm.text = self.company_name_registered_in_golomt
        dbtr.append(nm)
        pmtinf.append(dbtr)

        # DbtrAcct Account of debtor
        dbtracct = ET.Element('DbtrAcct')
        dbtr_id = ET.Element('Id')
        iban = ET.Element('IBAN')

        iban.text = journal_id.bank_acc_number
        dbtr_id.append(iban)
        dbtracct.append(dbtr_id)

        ccy = ET.Element('Ccy')
        ccy.text = self.currency_id.name
        dbtracct.append(ccy)

        pmtinf.append(dbtracct)

        cdttrftxinf = ET.Element('CdtTrfTxInf')
        cdtrid = ET.Element('CdtrId')
        cdtrid.text = timestamp
        cdttrftxinf.append(cdtrid)

        amt = ET.Element('Amt')
        instdamt = ET.Element('InstdAmt')
        instdamt.text = str(mnt_amount)
        amt.append(instdamt)
        amt.append(ccy)

        cdttrftxinf.append(amt)

        cdtr = ET.Element('Cdtr')
        cdtrnm = ET.Element('Nm')
        cdtrnm.text = bank_account_id.partner_id.name if bank_account_id.partner_id != self.partner_id else self.company_name_registered_in_golomt
        cdtr.append(cdtrnm)
        cdttrftxinf.append(cdtr)

        cdtracct = ET.Element('CdtrAcct')
        cdtracct_id = ET.Element('Id')
        cdtracct_iban = ET.Element('IBAN')
        cdtracct_iban.text = bank_account_id.acc_number
        cdtracct_id.append(cdtracct_iban)
        cdtracct.append(cdtracct_id)
        cdtracct.append(ccy)
        cdttrftxinf.append(cdtracct)

        cdtragt = ET.Element('CdtrAgt')
        cdtragt_fininstnid = ET.Element('FinInstnId')
        cdtragt_bicfi = ET.Element('BICFI')
        cdtragt_bicfi.text = bank_account_id.bank_id.BICFI_code_for_golomt_bank
        cdtragt_fininstnid.append(cdtragt_bicfi)
        cdtragt.append(cdtragt_fininstnid)
        cdttrftxinf.append(cdtragt)

        rmtinf = ET.Element('RmtInf')
        addtlrmtinf = ET.Element('AddtlRmtInf')
        addtlrmtinf.text = transaction_value
        rmtinf.append(addtlrmtinf)
        cdttrftxinf.append(rmtinf)
        pmtinf.append(cdttrftxinf)

        root.append(grphdr)
        root.append(pmtinf)
        return root

    @api.multi
    def golomt_confirm(self, journal_id, bank_account_id, mnt_amount, transaction_value, vasco_code=False, amount_limit=False):
        """
        :param amount_limit: Гүйлгээний дээд хязгаар
        :param vasco_code: Гүйлгээний васко код
        :param journal_id: Компаний дансны журнал(Шилжүүлэх дансны журнал)
        :param bank_account_id: Харилцагчийн дансны (res.partner.bank)
        :param mnt_amount: Мөнгөн дүн
        :param transaction_value: Гүйлгээний утга
        :returns: corporate gateway руу илгээсэн 'request' xml болон хүлээж авсан 'response'
        """
        if not journal_id.bank_acc_number:
            return False, _('"Account Number" of journal: %s is empty.\n Please set it up.') % journal_id.name
        if not bank_account_id:
            return False, _('"Bank account" is empty.\n Please set it up.')
        if not bank_account_id.acc_number:
            return False, _('"Account Number" of Bank account: %s is empty.\n Please set it up.') % bank_account_id.name
        if not bank_account_id.bank_id.BICFI_code_for_golomt_bank:
            return False, _('"BICFI Code for Golomt bank" of Bank: %s is empty.\n please contact your administrator.') % bank_account_id.bank_id.name
        if not amount_limit:
            if not self.limited_user_ids:
                amount_limit = self.golomt_corporate_gateway_maximum_amount
            elif self.env.user in self.limited_user_ids:
                amount_limit = self.env.user.golomt_corporate_gateway_maximum_amount
            else:
                return False, _('User: %s does not have access to golomt statements, please contact your administrator') % self.env.user.name
        if amount_limit < mnt_amount:
            return False, _('Amount of transaction is exceeded, please contact your administrator')
        if mnt_amount <= 0:
            return False, _('"MNT amount" must be higher than 0.')
        if self.use_vasco_device and not journal_id.vasco_device_number:
            return False, _("'Vasco device number' of journal: %s is empty, please contact your administrator") % journal_id.name

        xml = self.get_xml(journal_id, bank_account_id, mnt_amount, transaction_value, vasco_code=vasco_code)
        return self.golomt_send_request(xml, 'transaction')

    def golomt_statement_confirm(self, journal_id, start_date, end_date, vasco_code=False):
        xml = self.get_statement_xml(journal_id, start_date, end_date, vasco_code=vasco_code)
        return self.golomt_send_request(xml, 'statement')

    def golomt_balance_confirm(self, journal_id, vasco_code=False):
        xml = self.get_balance_xml(journal_id, vasco_code=vasco_code)
        return self.golomt_send_request(xml, 'balance')

    def golomt_send_request(self, xml, transaction_type):
        headers = {'Content-Type': 'text/xml'}
        xml_str = ET.tostring(xml, encoding='utf8', method='xml')
        log = self.env['golomt.connection.log']
        try:
            response = requests.post(self.golomt_corporate_gateway_url, data=xml_str,
                                     headers=headers).text
            log_vals = {'request_type': transaction_type,
                        'sent_request': xml_str,
                        'received_response': response
                        }
            log.create(log_vals)
            # cr.commit() функц нь rollback хийхээс өмнө өгөгдлийн сангийн transaction-г commit хийж байгаа үйлдэл юм.
            # Лог гэх утгаараа rollback хийгдсэн ч хадгалах ёстой гэж үзсэн тул commit хийж байгаа.
            # Commit функц нь тухайн transaction-д байгаа бүх зүйлийг commit хийх учир хамаагүй ашиглаж болохгүй бөгөөд
            # Энэ тохиолдолд ямар нэгэн утга өөрчлөөгүй байгаа дээрээ commit хийж байгаа учир боломжтой гэж үзсэн
            self.env.cr.commit()
            return xml, response
        except Exception as ex:
            log_vals = {'request_type': transaction_type,
                        'sent_request': xml_str,
                        'received_response': False
                        }
            log.create(log_vals)
            self.env.cr.commit()
            return xml, ex
