# -*- coding: utf-8 -*-
from odoo import models, fields


class ResUsers(models.Model):
    _inherit = 'res.users'

    golomt_corporate_gateway_maximum_amount = fields.Float(string='Maximum amount per transaction')
