# coding=utf-8
from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError
from datetime import datetime, date, timedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT


class VascoTransactionWizard(models.TransientModel):
    _name = "golomt.get.statement"

    start_date = fields.Date('Start date', required=True)
    end_date = fields.Date('End date', required=True)
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id)
    journal_id = fields.Many2one('account.journal')
    bank_id = fields.Many2one('res.bank', readonly=True, related='journal_id.bank_id')
    account_number = fields.Char('Account number', readonly=True, related='journal_id.bank_acc_number')
    vasco_code_usable = fields.Boolean('Vasco code usable', default=lambda self: self.env.user.company_id.use_vasco_device)
    vasco_code = fields.Char('Vasco code')

    @api.multi
    def download(self):
        conf_date = self.company_id.bank_statement_download_date
        if not conf_date:
            raise UserError(_('Please configure "Start downloading bank statement from" field from golomt configuration'))
        if conf_date > self.start_date:
            raise UserError(_('Start date must be after %s') % conf_date)
        if self.end_date < self.start_date:
            raise UserError(_('Start date must be before end date'))
        start_date = datetime.strptime(self.start_date, DEFAULT_SERVER_DATE_FORMAT)
        end_date = datetime.strptime(self.end_date, DEFAULT_SERVER_DATE_FORMAT)
        if end_date > datetime.now():
            raise UserError(_('You cannot download future statements'))
        statement_model = self.env['account.bank.statement']
        diff = end_date - start_date
        for obj in range(0, diff.days + 1):
            timee = timedelta(days=obj)
            current_date = (start_date + timee).date()
            current_date_str = current_date.strftime(DEFAULT_SERVER_DATE_FORMAT)
            statement_obj = statement_model.search([('date', '=', current_date_str), ('state', '=', 'open'), ('journal_id', '=', self.journal_id.id)], order='create_date DESC', limit=1)
            if statement_obj:
                statement_model = statement_model | statement_obj
            else:
                vals = {'journal_id': self.journal_id.id,
                        'date': current_date_str,
                        'company_id': self.company_id.id,
                        }
                statement_model = statement_model | statement_model.create(vals)
        statement_model.golomt_get_statements(self.start_date, self.end_date, self.vasco_code)
