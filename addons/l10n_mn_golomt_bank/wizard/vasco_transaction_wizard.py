# coding=utf-8
from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError


class VascoTransactionWizard(models.TransientModel):
    _name = "golomt.vasco.transaction.wizard"

    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    vasco_code = fields.Char(string='Vasco code')

    def make_transaction(self):
        trans_model = self.env[self.env.context['golomt_model']]
        trans_obj = trans_model.browse(self.env.context['golomt_ids'])
        return trans_obj.action_to_paid_with_golomt_corporate_gateway(context=self.env.context, vasco_code=self.vasco_code)

    def get_golomt_balance(self):
        trans_model = self.env[self.env.context['golomt_model']]
        trans_obj = trans_model.browse(self.env.context['golomt_ids'])
        return trans_obj.get_golomt_balance(context=self.env.context, vasco_code=self.vasco_code)

