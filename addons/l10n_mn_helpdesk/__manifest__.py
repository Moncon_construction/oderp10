# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Mongolian Helpdesk',
    'version': '1.0',
    'category': 'Helpdesk',
    'sequence': 57,
    'author': "Asterisk Technologies LLC",
    'summary': 'Ticketing, Support, Issues',
    'depends': [
        'helpdesk',
        'project',
    ],
    'description': """
Helpdesk - Ticket Management App
================================
    """,
    'data': [
        'views/helpdesk_views.xml',
    ],
    'application': True,
    'license': 'OEEL-1',
}
