# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _


class HelpdeskTicket(models.Model):
    _inherit = 'helpdesk.ticket'

    def _compute_total_project(self):
        self.project_task_count = len(self.related_tasks)

    project_task_count = fields.Float('Total Project', compute=_compute_total_project)
    related_tasks = fields.One2many('project.task', 'ticket_id', 'Related Tasks')
    ticket_type_id = fields.Many2one('helpdesk.ticket.type', string="Ticket Type", required=True)
    partner_phone = fields.Char(related='partner_id.phone', string='Partner phone')
    partner_mobile = fields.Char(related='partner_id.mobile', string='Partner mobile')

    @api.multi
    def create_new_task(self):
        task_id = self.env['project.task'].create({
             'name': self.name,
             'user_id': self.user_id.id,
             'partner_id': self.partner_id.id,
             'tag_ids': self.tag_ids.ids,
             'date_deadline': self.deadline,
             'ticket_id': self.id
        })
        view_id = self.env.ref('project.view_task_form2').id
        return {
            'name': 'form_name',
            'view_type': 'form',
            'view_mode': 'tree',
            'views': [(view_id, 'form')],
            'res_model': 'project.task',
            'view_id': view_id,
            'type': 'ir.actions.act_window',
            'res_id': task_id.id,
        }

class ProjectTask(models.Model):
    _inherit = 'project.task'

    ticket_id = fields.Many2one('helpdesk.ticket', 'Task')