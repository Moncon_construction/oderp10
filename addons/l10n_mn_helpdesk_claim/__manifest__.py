# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Helpdesk Claim",
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'depends':['helpdesk', 'l10n_mn_crm_claim'],
    'category': 'Mongolian Modules',
    "data": [
        "views/crm_claim_view.xml",
    ],
    "installable": True,
}