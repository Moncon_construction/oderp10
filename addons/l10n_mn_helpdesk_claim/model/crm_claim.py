# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import odoo
from odoo import api, fields, models, tools, _
from odoo.exceptions import AccessError

class CrmClaim(models.Model):
    _inherit = 'crm.claim'

    helpdesk = fields.Many2one('helpdesk.ticket', string='Tickets')


class HelpdeskTicket(models.Model):
    _inherit = 'helpdesk.ticket'

    @api.multi
    def _claim_count(self):
        for obj in self:
            obj.claim_count =  len(self.env['crm.claim'].sudo().search([('helpdesk','=',obj.id)])) or 0

    claim_count = fields.Integer(string='Claim Count', compute=_claim_count)