# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/,
# http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
{
    'name': "Mongolian Human Resource",
    'version': '1.0',
    'depends': [
        'hr',
        'hr_contract',
        'hr_attendance',
        'hr_recruitment',
        'l10n_mn_web',
        'l10n_mn_contacts'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
   Human Resource Additional Features
    """,
    'data': [
        'security/ir.model.access.csv',
        'security/ir_rule.xml',
        'data/cron_check_children_age.xml',
        'data/data.xml',
        'data/driver_license_category_data.xml',
        'data/required_materials_data.xml',
        'data/documentation_data.xml',
        'views/res_users_view.xml',
        'views/hr_employee.xml',
        'views/hr_employee_additional_info.xml',
        'views/hr_employee_approvation_views.xml',
        'views/employee_move_view.xml',
        'views/hr_employee_reclusion.xml',
        "views/worked_project_view.xml",
        "report/hr_employee_cv.xml",
        'wizard/employee_info_change_view.xml',
        "views/age_range_cron.xml",
        'views/res_partner_view.xml',
        'views/hr_employee_actions.xml',
        'views/res_config_view.xml',
        'views/hr_required_materials_view.xml',
        'views/hr_documentation_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
