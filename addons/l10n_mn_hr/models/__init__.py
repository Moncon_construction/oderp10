# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.


import hr_employee_additional_info
import worked_project
import hr_job
import hr_employee
import employee_move
import res_users
import res_partner
import res_company
import res_config
import hr_required_materials
import hr_documentation