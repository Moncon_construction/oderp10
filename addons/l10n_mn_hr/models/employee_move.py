# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/,
# http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
import logging
import time
from odoo import api, fields, models, _  # @UnresolvedImport.
from odoo.exceptions import UserError, ValidationError  # @UnresolvedImport

_logger = logging.getLogger(__name__)


class hr_employee(models.Model):
    _inherit = "hr.employee"

    CREATE_EMPLOYEE_MOVE = {
        'field': ['department_id', 'job_id', 'state_id'],
        # 'type':['one2many','many2many','float']
    }

    state_id = fields.Many2one('hr.employee.status',
                               string='Employee Status')
    employee_move_lines = fields.One2many('employee.move.line',
                                          'employee_id',
                                          string='Employee_move_line')

    @api.multi
    @api.constrains('ssnid', 'company_id')
    def _check_create_employee(self):
        for record in self:
            hr_employee = self.env['hr.employee'].search([('id', '!=', record.id), ('ssnid', '=', record.ssnid), ('company_id', '=', record.company_id.id)])
            if len(hr_employee) > 1:
                raise ValidationError(_("This employee already exists"))

    def write(self, values):
        for employee in self:
            for key in values.keys():
                if key == 'state_id' and self.env['hr.employee.status'].browse(values[key]).type in ('resigned', 'retired'):
                    values['active'] = False
                    if employee.user_id:
                        employee.sudo().user_id.active = False
        return super(hr_employee, self).write(values)

    def create_employee_move(self, pre_value, new_value, pre_value_ref, new_value_ref, move_type):
        for employee in self:
            if pre_value_ref:
                pre_val_ref = str(pre_value_ref._name) + "," + str(pre_value_ref.id)
            else:
                pre_val_ref = None
            if employee[move_type] and pre_value_ref:
                new_value_ref = str(pre_value_ref._name) + "," + str(employee[move_type].id)
            else:
                new_value_ref = None
            move_id = self.env['employee.move.line'].create({  # @UnusedVariable
                'employee_id': employee.id,
                'date': time.strftime("%Y-%m-%d"),
                'pre_value': pre_value,
                'new_value': employee[move_type],
                'type': move_type,
                'state': 'draft',
                'pre_value_ref': pre_val_ref,
                'new_value_ref': new_value_ref
            })


class hr_employee_status(models.Model):
    _name = 'hr.employee.status'
    _description = 'Hr Employee Status'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    name = fields.Char('Name', required=True)
    type = fields.Selection([('basis', 'Basis'),
                             ('trial', 'Trial'),
                             ('contract', 'Contract'),
                             ('resigned', 'Resigned'),
                             ('maternity', 'Maternity'),
                             ('trainee', 'Trainee'),
                             ('retired', 'Retired'),
                             ('annual_leave', 'Annual leave')],
                            string='Main Type', required=True)


class EmployeeMove(models.Model):
    _name = 'employee.move'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _rec_name = 'employee_id'
    _description = 'Employee Move'

    employee_id = fields.Many2one('hr.employee', string='Employee', states={'done': [('readonly', True)]})
    regulation_start_date = fields.Date(required=1, states={'done': [('readonly', True)]}, string='Regulation start date')
    employee_move_lines = fields.One2many('employee.move.line', 'move_id', string='Employee move lines')
    state = fields.Selection([('draft', 'Draft'),
                              ('done', 'Approved')], readonly=True, states={'done': [('readonly', True)]}, string='Status', default='draft')
    company_id = fields.Many2one('res.company', string='Company', states={'done': [('readonly', True)]}, default=lambda self: self.env.user.company_id)
    prev_dep = fields.Many2one('hr.department', string='Previous department')
    prev_job = fields.Many2one('hr.job', string='Previous job')
    prev_status = fields.Many2one('hr.employee.status', string='Previous status')
    new_dep = fields.Many2one('hr.department', string='New department')
    new_job = fields.Many2one('hr.job', string='New job')
    new_status = fields.Many2one('hr.employee.status', string='New status')

    @api.multi
    def write(self, vals):
        """
           Ажилчны шилжилт хөдөлгөөнд хэлтэс, албан тушаалд өөрчлөлт ороход тухайн ажилчны хөдөлмөрийн гэрээн дээр автоматаар өөрчлөгддөг байна
           """
        if 'state' in vals.keys() and vals['state'] == 'done':
            for move in self:
                # Тухайн хөдөлгөөнд харъяалагдах хамгийн сүүлчийн мөрийн утгаар ажилтны гэрээг өөрчлөх
                qry = """
                                SELECT max(id) AS max_id FROM employee_move_line WHERE  move_id = %s and type = 'department_id'
                            """ % move.id
                self._cr.execute(qry)
                last_dep_line_id = self._cr.fetchall()

                qry1 = """
                                SELECT max(id) AS max_id FROM employee_move_line WHERE   move_id = %s and type = 'job_id'
                            """ % move.id
                self._cr.execute(qry1)
                last_job_line_id = self._cr.fetchall()

                contracts = self.env['hr.contract'].search([('employee_id', '=', move.employee_id.id)])

                if last_dep_line_id:
                    last_dep_line_id = last_dep_line_id[0][0] if last_dep_line_id[0] else False
                    if last_dep_line_id:
                        last_dep_line = self.env['employee.move.line'].browse(last_dep_line_id)
                        if contracts and len(contracts) > 0:
                            for contract in contracts:
                                contract.write({'department_id': last_dep_line.new_dep.id})

                if last_job_line_id:
                    last_job_line_id = last_job_line_id[0][0] if last_job_line_id[0] else False
                    if last_job_line_id:
                        last_job_line = self.env['employee.move.line'].browse(last_job_line_id)
                        if contracts and len(contracts) > 0:
                            for contract in contracts:
                                contract.write({'job_id': last_job_line.new_job.id})

        return super(EmployeeMove, self).write(vals)

    @api.multi
    def unlink(self):
        for this in self:
            if this.state not in ('draft'):
                raise UserError(_('You cannot delete an employee move which is not draft.'))
        return super(EmployeeMove, self).unlink()

    @api.onchange('employee_id')
    def onchange_employee(self):
        values = {}
        employee_obj = self.env['hr.employee']
        if self.employee_id:
            self.prev_dep = self.employee_id.department_id.id
            self.prev_job = self.employee_id.job_id.id
            self.prev_status = self.employee_id.state_id
            self.new_dep = self.employee_id.department_id.id
            self.new_job = self.employee_id.job_id.id
            self.new_status = self.employee_id.state_id

    @api.multi
    def approve(self):
        for this in self:
            for line in this.employee_move_lines:
                line.done()
                if line.move_id and line.move_id.employee_id and line.type == 'state_id' and line.new_status and line.new_status.type in ['resigned', 'retired']:
                    line.move_id.employee_id.write({'reclusion_date': line.move_id.regulation_start_date})
        self.write({'state': 'done'})

    @api.multi
    def set_draft(self):
        for this in self:
            for line in this.employee_move_lines:
                line.revert()
                line.write({'state': 'draft'})
        self.write({'state': 'draft'})


class EmployeeMoveLine(models.Model):
    _name = 'employee.move.line'
    _description = 'Employee Move Line'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _rec_name = 'employee_id'

    move_id = fields.Many2one('employee.move', string='Movement', ondelete="cascade")
    prev_dep = fields.Many2one('hr.department', string='Previous department')
    prev_job = fields.Many2one('hr.job', string='Previous job')
    prev_status = fields.Many2one('hr.employee.status', string='Previous status')
    new_dep = fields.Many2one('hr.department', string='New department')
    new_job = fields.Many2one('hr.job', string='New job')
    new_status = fields.Many2one('hr.employee.status', string='New status')
    new_status_type = fields.Selection(related='new_status.type')
    pre_value_ref_dep = fields.Reference(string='Previous Department Reference', selection=[('hr.department', "Department")])
    new_value_ref_dep = fields.Reference(string='New Department Reference', selection=[('hr.department', "Department")])

    pre_value = fields.Char(string='Pre Value')
    employee_id = fields.Many2one('hr.employee', string='Employee')
    type = fields.Selection([('department_id', 'Department'),
                             ('job_id', 'Job'),
                             ('state_id', 'Status')], required=False, string='Type')
    pre_value_ref = fields.Reference(selection=[('hr.department', "Department"),
                                                ('hr.employee.status', "Status"),
                                                ('hr.job', 'Job')], string='Pre Value Reference')
    new_value_ref = fields.Reference(selection=[('hr.department', "Department"),
                                                ('hr.employee.status', "Status"),
                                                ('hr.job', 'Job')], string='New Value Reference')
    new_value = fields.Char(string='New Value')
    description = fields.Text(string='Description')
#     regulation_id = fields.Many2one('hr.regulation', string='Regulation')
    date = fields.Date(required=1, string='Date', default=fields.Date.context_today)
    state = fields.Selection([('draft', 'Draft'), ('done', 'Done')], string='State', default='draft')

    @api.model
    def create(self, vals):
        job_obj = self.env['hr.job']
        dep_obj = self.env['hr.department']
        status_obj = self.env['hr.employee.status']
        employee_obj = self.env['hr.employee'].browse(vals['employee_id'])
        vals['pre_value_ref'] = str('hr.job') + "," + str(employee_obj.job_id.id) if employee_obj.job_id else None
        vals['pre_value_ref_dep'] = str('hr.department') + "," + str(employee_obj.department_id.id) if employee_obj.department_id else None
        vals['prev_job'] = employee_obj.job_id.id if employee_obj.job_id else None
        vals['prev_dep'] = employee_obj.department_id.id if employee_obj.department_id else None
        vals['prev_status'] = employee_obj.state_id.id if employee_obj.state_id else None
        if vals['type'] == 'state_id':
            vals['pre_value'] = status_obj.browse(vals['prev_status']).name
            vals['new_value'] = status_obj.browse(vals['new_status']).name if 'new_status' in vals else None
            vals['new_value_ref'] = str('hr.employee.status') + "," + str(vals['new_status']) if 'new_status' in vals else None
        if vals['type'] == 'department_id':
            vals['new_value_ref'] = str('hr.department') + "," + str(vals['new_dep']) if vals.has_key('new_dep') and vals['new_dep'] else None
            vals['pre_value'] = dep_obj.browse(vals['prev_dep']).name
            vals['new_value'] = dep_obj.browse(vals['new_dep']).name
        if vals['type'] == 'job_id':
            vals['pre_value'] = job_obj.browse(vals['prev_job']).name
            vals['new_value'] = job_obj.browse(vals['new_job']).name if 'new_job' in vals else None
            vals['new_value_ref'] = str('hr.job') + "," + str(vals['new_job']) if 'new_job' in vals else None
        empl_move = super(EmployeeMoveLine, self.with_context(mail_create_nolog=True)).create(vals)
        return empl_move

    @api.multi
    def write(self, values):
        dep = job = None
        for x in self:
            if 'new_dep' in values:
                dep_id = self.env['hr.department'].search([('id', '=', values['new_dep'])])
                dep = self.env['hr.department'].browse(dep_id)
                values['new_value_ref_dep'] = str('hr.department') + "," + str(values['new_dep'])
            else:
                dep_id = self.env['hr.department'].search([('id', '=', x.new_dep.id)])
                dep = self.env['hr.department'].browse(dep_id)
            if 'new_job' in values:
                job_id = self.env['hr.job'].search([('id', '=', values['new_job'])])
                job = self.env['hr.job'].browse(job_id)
                values['new_value_ref'] = str('hr.job') + "," + str(values['new_job'])
            else:
                job_id = self.env['hr.job'].search([('id', '=', x.new_job.id)])
                job = self.env['hr.job'].browse(job_id)

        return super(EmployeeMoveLine, self).write(values)

    @api.multi
    def unlink(self):
        for this in self:
            if this.state not in ('draft'):
                raise UserError(_('You cannot delete an employee move which is not draft.'))
        return super(EmployeeMoveLine, self).unlink()

    @api.model
    def default_get(self, fields):
        rec = super(EmployeeMoveLine, self).default_get(fields)
        context = dict(self._context or {})

        active_model = context.get('active_model')
        active_ids = context.get('active_ids')

        rec.update({'employee_id': context.get('employee_id', False)})
        rec.update({'prev_dep': context.get('prev_dep', False)})
        rec.update({'prev_job': context.get('prev_job', False)})
        rec.update({'prev_status': context.get('prev_status', False)})
        return rec

    @api.onchange('type')
    def onchange_type(self):
        employee_obj = self.env['hr.employee']
        if self.employee_id.id and self.type:
            employee = employee_obj.browse(self.employee_id.id)
            if employee[self.type]:
                self.pre_value_ref = str(
                    employee[self.type]._name) + "," + str(employee[self.type].id)
                self.new_value_ref = str(
                    employee[self.type]._name) + "," + str(employee[self.type].id)

    @api.multi
    def done(self):
        for move in self:
            employee = move.employee_id
            if (not move.type == 'state_id') and move.prev_dep == move.new_dep and move.prev_job == move.new_job:
                raise ValidationError(_('Previous and new Job Position-Department relation are the same. No employee move is needed.'))
            if move.type == 'department_id':
                employee.sudo().write({'department_id': move.new_dep.id})
            elif move.type == 'job_id':
                employee.sudo().write({'job_id': move.new_job.id})
            elif move.type == 'state_id':
                employee.sudo().write({'state_id': move.new_status.id})
            move.write({'date': move.move_id.regulation_start_date})
        return self.write({'state': 'done'})

    @api.multi
    def revert(self):
        for move in self:
            employee = move.employee_id
            if (not move.type == 'state_id') and move.prev_dep == move.new_dep and move.prev_job == move.new_job:
                raise ValidationError(_('Previous and new Job Position-Department relation are the same. No employee move is needed.'))
            if move.type == 'department_id':
                employee.sudo().write({'department_id': move.prev_dep.id})
            elif move.type == 'job_id':
                employee.sudo().write({'job_id': move.prev_job.id})
            elif move.type == 'state_id':
                employee.sudo().write({'state_id': move.prev_status.id})
            move.write({'date': move.move_id.regulation_start_date})
        return self.write({'state': 'draft'})


class CompanyEmployeeMove(models.Model):
    _name = 'company.employee.move'
    _rec_name = 'employee_id'

    employee_id = fields.Many2one('hr.employee', string='Employee', states={'done': [('readonly', True)]})
    regulation_start_date = fields.Date(required=1, states={'done': [('readonly', True)]}, string='Regulation start date')
    state = fields.Selection([('draft', 'Draft'),
                              ('done', 'Approved')], string='Status', states={'done': [('readonly', True)]}, default='draft')
    prev_company_id = fields.Many2one('res.company', string='Previous Company', states={'done': [('readonly', True)]},)
    prev_dep_id = fields.Many2one('hr.department', string='Previous department', states={'done': [('readonly', True)]},)
    prev_job_id = fields.Many2one('hr.job', string='Previous job', states={'done': [('readonly', True)]},)
    new_dep_id = fields.Many2one('hr.department', string='New department', states={'done': [('readonly', True)]})
    new_job_id = fields.Many2one('hr.job', string='New job', states={'done': [('readonly', True)]})
    new_company_id = fields.Many2one('res.company', string='New Company', states={'done': [('readonly', True)]},)
    date = fields.Date(required=1, string='Date', default=fields.Date.context_today, states={'done': [('readonly', True)]},)

    @api.onchange('employee_id')
    def onchange_employee_id(self):
        if self.employee_id:
            self.prev_company_id = self.employee_id.company_id.id
            self.prev_dep_id = self.employee_id.department_id.id
            self.prev_job_id = self.employee_id.job_id.id

    @api.multi
    def approve(self):
        self.employee_id.active = False
        self.state = 'done'
        return self.env['hr.employee'].create({
            'name': self.employee_id.name,
            'employee_id': self.employee_id.id,
            'state_id': self.employee_id.state_id.id,
            'company_id': self.new_company_id.id,
            'department_id': self.new_dep_id.id,
            'job_id': self.new_job_id.id,
            'active': True,
            'address_id': self.employee_id.address_id.id,
            'mobile_phone': self.employee_id.mobile_phone,
            'work_phone': self.employee_id.work_phone,
            'work_email': self.employee_id.work_email,
            'work_location': self.employee_id.work_location,
            'job_classification_id': self.new_job_id.job_classification.id,
            'skill_level': self.employee_id.skill_level,
            'parent_id': self.employee_id.parent_id.id,
            'coach_id': self.employee_id.coach_id.id,
            'manager': self.employee_id.manager,
            'calendar_id': self.employee_id.calendar_id.id,
            'engagement_in_company': self.regulation_start_date
        })
