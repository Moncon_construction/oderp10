# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/,
# http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

import logging
import time
from datetime import timedelta
from datetime import datetime
from dateutil import parser
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models, _, exceptions  # @UnresolvedImport.
from odoo.exceptions import UserError, ValidationError  # @UnresolvedImport

_logger = logging.getLogger('odoo')
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DATE_FORMAT = "%Y-%m-%d"


class HrEmployee(models.Model):
    _inherit = "hr.employee"

    @api.one
    def _hide_personal_information(self):
        if self.env.user.has_group('hr.group_hr_user') or (self.user_id and self.env.uid == self.user_id.id):
            self.hide_personal_information = False
        else:
            self.hide_personal_information = True

    @api.depends('ssnid')
    def _ssnid_length(self):
        for obj in self:
            if obj.ssnid:
                obj.ssnid_length = len(obj.ssnid)
            else:
                obj.ssnid_length = 0


    @api.depends('sinid')
    def _sinid_length(self):
        for obj in self:
            if obj.sinid:
                obj.sinid_length = len(obj.sinid)
            else:
                obj.sinid_length = 0

    @api.depends('health_insurance_no')
    def _health_insurance_no_length(self):
        for obj in self:
            if obj.health_insurance_no:
                obj.health_insurance_no_len = len(obj.health_insurance_no)
            else:
                obj.health_insurance_no_len = 0

    birthday = fields.Date('Date of Birth')
    annual_leave_days = fields.Integer(compute='compute_leave_days', string='Annual Leave Days', default=15)
    last_name = fields.Char(string='Last name', size=64)
    family_name = fields.Char(string='Family name', size=64)
    health_insurance_no = fields.Char(string='Health insurance number', size=16)
    home_phone = fields.Char(string='Home Phone', size=8)
    is_own_car = fields.Boolean(string='Is Own Car')
    foreign_passport_number = fields.Char(string='Foreign passport', size=16)
    driver_license_number = fields.Char(string='Driver license number', size=16)
    driver_license_category = fields.Many2many('driver.license.category', 'employee_driver_license_rel', 'emp_id', 'license_id', 'Driver license Category')
    driver_license_date = fields.Date('Driver license date')
    number_of_members = fields.Integer(string='Number of members')
    analytic_account_id = fields.Many2one('account.analytic.account', string='Analytic account')
    family_members = fields.One2many('hr.employee.family.line', 'employee_id', string='Family Members')
    education_ids = fields.One2many('hr.education', 'employee_id', string='Employee Education')
    language_ids = fields.One2many('hr.language', 'employee_id', string='Employee Language')
    software_skill_ids = fields.One2many('hr.software.skill', 'employee_id', string='Employee Software Skill')
    arts_sports_talent = fields.One2many('hr.arts.sports.talent', 'employee_id', string='Arts Sports Talent')
    award_ids = fields.One2many('hr.award', 'employee_id', string='Employee Award')
    prize_ids = fields.One2many('hr.prize', 'employee_id', string='Employee Prize')
    benefit_ids = fields.One2many('hr.benefit', 'employee_id', string='Employee Benefit')
    punishment_ids = fields.One2many('hr.punishment', 'employee_id', string='Employee Punishment')
    employment_ids = fields.One2many('hr.employment', 'employee_id', string='Employee Employment')
    employment_approvations = fields.One2many('hr.employment.approvation', 'employee', string='Employee Employment Approvation Information')
    apartment_condition = fields.Many2one('hr.apartment.condition', string='Apartment Condition')
    blood_type = fields.Selection([
        ('first', 'First'),
        ('second', 'Second'),
        ('third', 'Third'),
        ('forth', 'Forth')], string='Blood Type')
    nationality = fields.Many2one('hr.nationality', string='Nationality')
    social_origin = fields.Many2one('hr.social.origin', string='Social Origin')
    military_service = fields.Selection([
        ('yes', "Yes"),
        ('no', "No")], 'Military Service')
    has_disability = fields.Boolean(default=False, string="Has Disability")
    facebook = fields.Char(string='Facebook')
    twitter_account = fields.Char(string='Twitter Acc')
    linkedin_account = fields.Char(string='Linkedin')
    own_mail = fields.Char(string='Own Mail')
    wechat_account = fields.Char(string='WeChat Account')
    height = fields.Integer(string='Height')
    weight = fields.Integer(string='Weight')
    shirt_size = fields.Selection([
        ('2xs', '2XS'),
        ('xs', 'XS'),
        ('s', 'S'),
        ('m', 'M'),
        ('l', 'L'),
        ('xl', 'XL'),
        ('2xl', '2XL'),
        ('3xl', '3XL'),
        ('4xl', '4XL'),
        ('5xl', '5XL')], string='Shirt Size')
    sitting_drums_size = fields.Selection([
        ('2xs', '2XS'),
        ('xs', 'XS'),
        ('s', 'S'),
        ('m', 'M'),
        ('l', 'L'),
        ('xl', 'XL'),
        ('2xl', '2XL'),
        ('3xl', '3XL'),
        ('4xl', '4XL'),
        ('5xl', '5XL')], string='Sitting Drums Size')
    boot_size = fields.Char(string='Boot size', size=8)
    rel_person_name = fields.Char(string='Related Person name', size=64)
    rel_person_phone = fields.Char(string='Related Person Phone', size=64)
    rel_person_email1 = fields.Char('Related Person Email 1 ')
    rel_person_email2 = fields.Char('Related Person Email 2')
    advantage = fields.Char(string='Employee Advantages', size=200)
    disadvantage = fields.Char(string='Employee Disadvantages', size=200)
    purpose_of_three_year = fields.Text(string='Purpose of Three year')
    health_situation = fields.Char(string='Health Situation', size=80)
    engagement_in_company = fields.Date(string='Date of Engagement in company', inverse='onchange_engagement_in_company', store=True)
    years_of_study = fields.Integer(string="Years of study")
    prefix = fields.Char(string="Prefix")
    success = fields.Char(string="Success")
    duration_of_emp_details = fields.Char(compute='_compute_duration', string='Duration of Employment Details')
    duration_of_employment = fields.Float(compute='_compute_duration', string='Duration of Employment', multi=True)
    age = fields.Integer(string='Age', compute='_compute_age', groups='hr.group_hr_user')
    country_state_id = fields.Many2one('res.country.state', string='State')
    gov_start = fields.Date('Gov start')
    gov_end = fields.Date('Gov end')
    job_classification = fields.Many2one('hr.job.classification', related='job_id.job_classification', string='Job classification')
    job_type = fields.Many2one('hr.job.type', 'Job Type')
    skill_level = fields.Many2one('hr.skill.level', string="Skill level")
    place_of_birth_aimag = fields.Char('Place of birth:Aimag')
    place_of_birth_sum = fields.Char('Place of birth:Sum')
    district = fields.Many2one('res.district', string='Province/District')
    sumkhoroo = fields.Many2one('res.khoroo', string='Sum/Khoroo name')
    height = fields.Integer('Height')
    weight = fields.Integer('Weight')
    boot_size = fields.Char('Boot size')
    contract_id = fields.Many2one('hr.contract', string='Contract')
    gov_year = fields.Float(compute='compute_auto_gov_year', string='Worked for Government ', store=True, multi=True)
    gov_paused_years = fields.Float(compute='compute_auto_gov_year', string='Paused working in Government', store=True, multi=True)
    work_skill_ids = fields.One2many('hr.work.skill', 'employee_id', string='Work Skill')
    work_skill_number = fields.Integer(string='Number:')
    cerificate_ids = fields.One2many('hr.certificate', 'employee_id', string='Trainings')
    hide_personal_information = fields.Boolean('Hide Personal Information', compute='_hide_personal_information')
    worked_projects = fields.One2many('worked.project', 'employee_id')
    born_month = fields.Integer(string='Born Month', compute='_compute_age', store=True)
    employment_month = fields.Char(string='Employment Month', compute='_compute_employment_month', store=True)
    check_engagement_date = fields.Boolean(string='Check engamement date', compute='_check_engagement_date', search='_check_engagement_date_search', help="Check last two months engagement")
    children = fields.Integer(string='Number of Children', store=True, compute='_compute_children_number', groups='hr.group_hr_user')
    birth_month_day = fields.Char(string="Birthmonth and birthday", compute='_compute_month_day', store=True)
    check_medic_exam_date = fields.Boolean(string='Medic exam date', compute='medic_exam_check', search='_check_medic_exam_date_search')
    reclusion_date = fields.Date('Reclusion Date')
    ssnid_length = fields.Integer('ssnid length', compute='_ssnid_length', store=True)
    sinid_length = fields.Integer('sinid length', compute='_sinid_length', store=True)
    health_insurance_no_len = fields.Integer('health insurance no length', compute='_health_insurance_no_length', store=True)
    required_materials_ids = fields.One2many('hr.required.materials.line', 'employee_id', string="Required materials")
    documentation_ids = fields.One2many('hr.documentation.line', 'employee_id', string="Documentation")
    is_camp_employee = fields.Boolean(string="Is camp employee", store=True)
    attached_work_ids = fields.Many2many('attached.work.history')

    @api.multi
    @api.constrains('identification_id')
    def _unique_identification_id(self):
        for obj in self:
            if obj.identification_id:
                identifications = self.search([('identification_id', '=', obj.identification_id)])
                if len(identifications) > 1:
                    raise exceptions.ValidationError(_('Identification ID is duplicated: %s') % obj.identification_id)

    @api.multi
    def medic_exam_check(self):
        for employee in self.sudo():
            if employee.medic_exam:
                check_date = datetime.strptime(employee.medic_exam, '%Y-%m-%d') + timedelta(days=320)
                if datetime.today() >= check_date:
                    employee.check_medic_exam_date = True
                else:
                    employee.check_medic_exam_date = False

    @api.multi
    def _check_medic_exam_date_search(self, operator, value):
        recs = self.search([]).filtered(lambda x: x.check_medic_exam_date is True)
        if recs:
            return [('id', 'in', [x.id for x in recs])]

    @api.depends('department_id')
    @api.onchange('department_id')
    def onchange_department(self):
        if self.department_id:
            job_ids = self.env['hr.job'].search([('department_id', '=', self.department_id.id)]).ids
            return {'domain': {'job_id': [('id', '=', job_ids)]}}
        else:
            return {'domain': {'job_id': []}}

    @api.depends('family_members', 'family_members.is_child_check')
    def _compute_children_number(self):
        for emp in self:
            child_number = 0
            current_date = datetime.now()
            for member in emp.family_members:
                if member.age:
                    delta = relativedelta(current_date,
                                    datetime.strptime(member.age, '%Y-%m-%d'))
                    if delta.years < 18 and member.is_child_check:
                        child_number += 1
            emp.children = child_number

    @api.multi
    def cron_check_children_age(self):
        for emp in self.env['hr.employee'].search([]):
            current_date = datetime.now()
            for member in emp.family_members:
                delta = relativedelta(current_date,
                                      datetime.strptime(member.age, '%Y-%m-%d'))
                if delta.years > 18:
                    member.is_child_check = False

    @api.onchange('district')
    def onchange_district(self):
        sumkhoroo_ids = []
        for obj in self:
            if obj.sumkhoroo.district_id != obj.district:
                obj.sumkhoroo = None
            sumkhoroo_ids = self.env['res.khoroo'].search([('district_id', '=', obj.district.id)]).ids

        return {'domain': {'sumkhoroo': [('id', 'in', sumkhoroo_ids)]}}

    @api.onchange('engagement_in_company')
    def onchange_engagement_in_company(self):
        self.compute_auto_gov_year()
        self._compute_duration()

    @api.onchange('company_id')
    def _onchange_company(self):
        # @Override: Ажлын хаяг сонгогдоход ажилтны ажлын утас давхар сонгогдох
        res = super(HrEmployee, self)._onchange_company()
        if self.address_id:
            self._onchange_address()
        
    def get_address(self):
        for obj in self:
            address = ""
            if obj.address_home_id:
                if obj.address_home_id.street:
                    address = address + obj.address_home_id.street
                if obj.address_home_id.street2:
                    address = address + ", " + obj.address_home_id.street2
                if obj.address_home_id.city:
                    address = address + ", " + obj.address_home_id.city
                if obj.address_home_id.state_id:
                    address = address + ", " + obj.address_home_id.state_id.name
                if obj.address_home_id.country_id:
                    address = address + ", " + obj.address_home_id.country_id.name
                if obj.address_home_id.zip:
                    address = address + ", " + obj.address_home_id.zip
            if address:
                return address
            else:
                return False

    def name_get(self):
        res = []
        for record in self:
            output = ""
            name = record.name
            if record.last_name:
                name += ' %s' % (record.last_name,)
            if record.department_id.name:
                for i in record.department_id.name.upper().split():
                    output += i[0]
                    dp_name = output
                name += ' [%s]' % (dp_name,)
            if record.job_id.name:
                name += ' %s' % (record.job_id.name,)
            res.append((record.id, name))
        return res

    @api.depends('birthday')
    def _compute_age(self):
        current_date = datetime.now()
        current_year = current_date.year
        for employee in self:
            employee.age = current_year - parser.parse(employee.sudo().birthday).year if employee.sudo().birthday else 0
            employee.born_month = parser.parse(employee.sudo().birthday).month if employee.sudo().birthday else 0

    @api.depends('birthday')
    def _compute_month_day(self):
        for employee in self:
            employee.birth_month_day = str(parser.parse(employee.sudo().birthday).month) + '-' + str(
                parser.parse(employee.sudo().birthday).day) if employee.sudo().birthday else 0

    @api.depends('engagement_in_company')
    def _compute_employment_month(self):
        for employee in self:
            if employee.engagement_in_company:
                employment_date = datetime.strptime(employee.engagement_in_company, '%Y-%m-%d')
                if employment_date.month in [1, 2, 3, 4, 5, 6, 7, 8, 9]:
                    years_months = str(employment_date.year) + u'-0' + str(employment_date.month)
                else:
                    years_months = str(employment_date.year) + u'-' + str(employment_date.month)
                employee.employment_month = years_months

    @api.multi
    def _check_engagement_date(self):
        check_date = datetime.now() - timedelta(days=61)
        for employee in self:
            if employee.engagement_in_company and datetime.strptime(employee.engagement_in_company, '%Y-%m-%d') >= check_date:
                employee.check_engagement_date = True
            else:
                employee.check_engagement_date = False
    @api.multi
    def _check_engagement_date_search(self, operator, value):
        recs = self.search([]).filtered(lambda x: x.check_engagement_date is True)
        if recs:
            return [('id', 'in', [x.id for x in recs])]

    @api.multi
    def _compute_duration(self):
        current_date = datetime.now()
        for employee in self:
            if employee.engagement_in_company:
                worked_delta = relativedelta()
                for e in employee.employment_ids:
                    if e.in_this_organization and e.resigned_date:
                        if e.resigned_date < employee.engagement_in_company:
                            worked_delta += relativedelta(datetime.strptime(e.resigned_date,
                                                                            '%Y-%m-%d'),
                                                          datetime.strptime(e.entered_date,
                                                                            '%Y-%m-%d'))
                engagement_delta = relativedelta(current_date,
                                                 datetime.strptime(employee.engagement_in_company,
                                                                   '%Y-%m-%d'))  # @IgnorePep8
                delta = engagement_delta + worked_delta
                if delta:
                    employee.duration_of_employment = round(
                        (delta.years * 365 + delta.months * 30 + delta.days +
                         delta.hours / 24) / 365.0, 2)
                deceased = ''
                years_months_days = str(delta.years) + _(u'year ') \
                                    + str(delta.months) + _(u'month ') \
                                    + str(delta.days) + _(u'day') + deceased
                employee.duration_of_emp_details = years_months_days  # (current_date - datetime.strptime(employee.engagement_in_company, '%Y-%m-%d')).days/365.0 if employee.engagement_in_company else 0.0  # @IgnorePep8

    @api.depends('employment_ids.total_worked', 'employment_ids.paid_social_insurance')
    def compute_auto_gov_year(self):
        current_date = datetime.now()
        for employee in self:
            if employee.engagement_in_company:
                total_worked = relativedelta()
                gov_paused_years = relativedelta()
                for n in employee.employment_ids:
                    if n.entered_date and n.resigned_date:
                        if n.paid_social_insurance:
                            total_worked += relativedelta(datetime.strptime(n.resigned_date,
                                                                            '%Y-%m-%d'),
                                                          datetime.strptime(n.entered_date,
                                                                            '%Y-%m-%d'))
                        else:
                            gov_paused_years += relativedelta(datetime.strptime(n.resigned_date,
                                                                                '%Y-%m-%d'),
                                                              datetime.strptime(n.entered_date,
                                                                                '%Y-%m-%d'))
            engagement_delta = relativedelta(current_date,
                                             datetime.strptime(employee.engagement_in_company,
                                                               '%Y-%m-%d'))
            delta = engagement_delta + total_worked
            employee.gov_year = round(
                (delta.years * 365 + delta.months * 30 + delta.days +
                 delta.hours / 24) / 365.0, 2)

    @api.one
    @api.depends('gov_year', 'has_disability', 'age', 'birthday')
    def compute_leave_days(self):
        leave_day = 15
        leave_day_config = self.env['hr.holiday.days.config']

        for employee in self:
            if employee.has_disability or employee.age <= 18:
                leave_day = 20
                
            if employee.job_id:
                if not employee.job_id.condition:
                    conf_id = leave_day_config.search([
                        ('down', '<=', employee.gov_year),
                        ('upper', '>=', employee.gov_year),
                        ('condition_job', '=', 'normal')])
                else:
                    conf_id = leave_day_config.search([
                        ('down', '<=', employee.gov_year),
                        ('upper', '>=', employee.gov_year),
                        ('condition_job', '=', employee.job_id.condition)])
                if conf_id:
                    employee.annual_leave_days = leave_day + conf_id.days
                else:
                    employee.annual_leave_days = leave_day
            else:
                employee.annual_leave_days = 0

    @api.model
    def create(self, data):
        employee = super(HrEmployee, self).create(data)

        # Харилцагч болон хэрэглэгч байхгүй бол харилцагч үүсгэж холбох
        if not employee.address_home_id and not employee.user_id:
            partner = self.env['res.partner'].sudo().create({
                'company_id': self.env.user.company_id.id,
                'name': employee.name_related + '.' + employee.last_name if employee.last_name else employee.name_related,
                'mobile': employee.mobile_phone,
                'email': employee.work_email,
                'phone': employee.work_phone,
                'register': employee.ssnid,
                'notify_email': 'always',
                'employee': True,
                'department': employee.department_id.id if employee.department_id else False,
                'function': employee.job_id.name
            })
            employee.write({'address_home_id': partner.id or False})

        return employee

    @api.multi
    def write(self, vals):
        for emp in self:
            if 'department_id' in vals.keys() and emp.address_home_id:
                department = self.env['hr.department'].browse(vals['department_id'])
                emp.sudo().address_home_id.department = department[0] if department else emp.sudo().address_home_id.department
            if 'job_id' in vals.keys() and emp.address_home_id:
                job_id = self.env['hr.job'].browse(vals['job_id'])
                emp.sudo().address_home_id.function = job_id[0].name if job_id else emp.sudo().address_home_id.function
            if 'active' in vals.keys():
                if vals['active'] is False:
                    # Deactivate related user
                    if emp.user_id:
                        emp.sudo().user_id.active = False
                    # Change partner employee field
                    if emp.address_home_id:
                        emp.sudo().address_home_id.employee = False
                else:
                    # Change partner employee field
                    if emp.address_home_id:
                        emp.sudo().address_home_id.employee = True

            # update related partner
            if vals.get('user_id'):
                vals.update({'address_home_id': self.env['res.users'].browse(vals.get('user_id')).partner_id.id})

            # Update related partner's employee field
            if 'address_home_id' in vals:
                # old partner is exists
                if emp.address_home_id:
                    if self.env['hr.employee'].search_count([('address_home_id', '=', emp.address_home_id.id)]) == 1:
                        emp.sudo().address_home_id.write({'employee': False})
                if vals.get('address_home_id'):
                    self.env['res.partner'].sudo().browse(vals.get('address_home_id')).write({'employee': True})

            # Update fields of related partner
            related_partner_update_dic = {}
            
            if 'ssnid' in vals.keys():
                related_partner_update_dic['register'] = vals['ssnid']
            if 'work_phone' in vals.keys():
                related_partner_update_dic['phone'] = vals['work_phone']
            if 'mobile_phone' in vals.keys():
                related_partner_update_dic['mobile'] = vals['mobile_phone']
            if 'work_email' in vals.keys():
                related_partner_update_dic['email'] = vals['work_email']
                
            if related_partner_update_dic and vals.get('address_home_id', emp.address_home_id):
                self.env['res.partner'].browse(vals.get('address_home_id', emp.address_home_id.id)).write(related_partner_update_dic)
            
            if 'department_id' in vals and vals['department_id']:
                department = self.env['hr.department'].browse(vals['department_id'])
                vals['parent_id'] = department.manager_id.id if department.manager_id  else False
                
        return super(HrEmployee, self).write(vals)

    @api.multi
    def import_material(self):
        self.ensure_one()
        mater = self.env['hr.required.materials'].search([('company_id', '=', self.company_id.id)])
        line_ids = []
        obj = self.env['hr.required.materials.line']
        for line_id in self.required_materials_ids:
            line_ids.append(line_id.required_materials_id.id)
        for mat in mater:
            if mat.id not in line_ids:
                obj.create({'required_materials_id': mat.id, 'employee_id': self.id})

    @api.multi
    def import_documentation(self):
        self.ensure_one()
        mater = self.env['hr.documentation'].search([('company_id', '=', self.company_id.id)])
        lines_ids = []
        ob = self.env['hr.documentation.line']
        for lines_id in self.documentation_ids:
            lines_ids.append(lines_id.documentation_id.id)
        for mat in mater:
            if mat.id not in lines_ids:
                ob.create({'documentation_id': mat.id, 'employee_id': self.id})

    @api.multi
    def create_partner(self):
        ResPartner = self.env['res.partner']
        for employee in self:
            if not employee.address_home_id:
                partner = ResPartner.create({
                    'name': employee.display_name,
                    'email': employee.work_email,
                    'last_name' : employee.last_name,
                    'mobile': employee.mobile_phone,
                    'email': employee.work_email,
                    'phone': employee.work_phone,
                    'register': employee.ssnid
                })
                employee.write({
                    'address_home_id': partner.id
                })

    @api.multi
    def create_user(self):
        for employee in self:
            if employee.work_email:
                if employee.last_name:
                    name = u''.join([employee.name_related, u'.', employee.last_name, ])
                else:
                    raise ValidationError(_("Fill Employee's last name!"))

                # Check user is exist
                exist_user = self.env['res.users'].search([('login', '=', employee.work_email)])
                if exist_user:
                    employee.write({'user_id': exist_user.id})
                else:
                    # check employee partner
                    if employee.address_home_id:
                        created_user = self.env['res.users'].sudo().create({
                            'name': name,
                            'login': employee.work_email,
                            'partner_id': employee.address_home_id.id,
                            'department_id': employee.department_id.id if employee.department_id else False,
                        })
                    else:
                        created_user = self.env['res.users'].sudo().create({
                            'name': name,
                            'login': employee.work_email,
                            'department_id': employee.department_id.id if employee.department_id else False,
                        })
                        employee.sudo().write({'address_home_id': created_user.partner_id.id})
                        created_user.partner_id.sudo().write({'email': employee.work_email})
                    employee.sudo().write({'user_id': created_user.id})
            else:
                raise UserError(_("Fill Employee's work email!"))
        return True

    # TODO: REMOVE ME
    def update_title(self):
        emp_obj = self.env['hr.employee']
        employee_ids = emp_obj.search([('active', '=', True)])
        for employee in employee_ids:
            for employment in employee.employment_ids:
                if employment.job_id:
                    employment.job_title = employment.job_id.name
        return True
    
    @api.multi
    def archive(self):
        if not self.env.user.has_group("hr.group_hr_user"):
            raise UserError(_("You have not right for \"Archive/Unarchive\" employee !!!"))
        self.write({'active': False})

    @api.multi
    def release_archive(self):
        if not self.env.user.has_group("hr.group_hr_user"):
            raise UserError(_("You have not right for \"Archive/Unarchive\" employee !!!"))
        self.write({'active': True})
        for emp in self:
            emp.user_id.write({'active': True})


class HrDepartment(models.Model):
    _inherit = "hr.department"

    analytic_account_id = fields.Many2one('account.analytic.account',
                                          string="Import material"'Analytic account')

    active = fields.Boolean()
    sequence = fields.Integer('Sequence')
    employees_count = fields.Integer(compute='_compute_employees_count', string='Employees')

    @api.multi
    def toggle_active(self):
        for dept in self:
            if dept.active == True:
                emps = self.env['hr.employee'].search([('department_id', '=', self.id)])
                jobs = self.env['hr.job'].search([('department_id', '=', self.id)])
                if emps and len(emps) > 0:
                    raise UserError(_("Cannot deactive this department because there is an employees"))
                if jobs and len(jobs) > 0:
                    raise UserError(_("Cannot deactive this department because there is a job"))

        return super(HrDepartment, self).toggle_active()

    @api.multi
    def button_employees(self):
        return {
            'name': _('Employees'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'hr.employee',
            'type': 'ir.actions.act_window',
            'domain': [('department_id', 'in', self.ids)],
        }

    @api.multi
    def _compute_employees_count(self):
        employee_obj = self.env['hr.employee']
        dep_obj = self.env['hr.department']
        for department in self:
            dep_objs = dep_obj.search([('id', 'child_of', department.id)])
            dep_ids = dep_objs.ids
            employee_ids = employee_obj.search([('department_id', 'in', dep_ids), ('active', '=', True)])
            department.employees_count = len(employee_ids)

    @api.multi
    def write(self, vals):
        for obj in self:
            if 'manager_id' in vals and vals['manager_id']:
                emps = self.env['hr.employee'].search([('department_id', '=', obj.id),('parent_id','=',obj.manager_id.id)])
                for emp in emps:
                    if emp.id == vals['manager_id']:
                        emp.write({'parent_id': False})
                    else: 
                        emp.write({'parent_id': vals['manager_id']})
        return super(HrDepartment, self).write(vals)

class DriverLicenseCategory(models.Model):
    _name = "driver.license.category"
    _description = "Driver license category"

    name = fields.Char('Driver license category')


class AttachedWorkHistory(models.Model):
    _name = 'attached.work.history'
    _description = "Attached Work History"

    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        if self.employee_id:
            self.job_id = self.employee_id.job_id

    @api.multi
    @api.constrains('finish_date')
    def date_constrains(self):
        for rec in self:
            if rec.finish_date:
                if rec.finish_date < rec.start_date:
                    raise ValidationError(_('Finish date must be later than Start date!'))


    employee_id = fields.Many2one('hr.employee', string='Attached Employee')
    job_id = fields.Many2one('hr.job', string='Attached Employee Position', required=True)
    start_date = fields.Date('Started Date', required=True)
    finish_date = fields.Date('Finish Date')



class RequiredMaterialsLine(models.Model):
    _name = "hr.required.materials.line"

    required_materials_id = fields.Many2one('hr.required.materials', string="Required Materials")
    check = fields.Boolean(default=False)
    data_file = fields.Binary(string='File', required=False, attachment=True)
    description = fields.Text(string="Description")
    employee_id = fields.Many2one('hr.employee', string="Employees")
    regist_employee_id = fields.Many2one('hr.employee', string="Regist Employees")
    company_id = fields.Many2one('res.company', default=lambda self: self.env.user.company_id, string="Company")




class DocumentationLine(models.Model):
    _name = "hr.documentation.line"
    
    documentation_id = fields.Many2one('hr.documentation', string="Documentation")
    check = fields.Boolean(default=False)
    data_file = fields.Binary(string='File', required=False, attachment=True)
    description = fields.Text(string="Description")
    employee_id = fields.Many2one('hr.employee', string="Employees")
    regist_employee_id = fields.Many2one('hr.employee', string="Regist Employees")
    company_id = fields.Many2one('res.company', default=lambda self: self.env.user.company_id, string="Company")
