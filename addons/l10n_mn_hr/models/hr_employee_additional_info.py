# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/,
# http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
import logging

from odoo import api, fields, models
from datetime import datetime
import time

_logger = logging.getLogger('odoo')


class hr_employee_family_member(models.Model):
    _name = "hr.employee.family.member"
    _description = "Hr Employee Family Member"

    name = fields.Char('Name', size=128, required=True)
    is_child = fields.Boolean(string='Is child', default=False)


class hr_apartment_condition(models.Model):
    _name = 'hr.apartment.condition'
    _description = 'Apartment Condition'

    name = fields.Char('Name', required=True)


class hr_employee_family_line(models.Model):
    _name = "hr.employee.family.line"
    _description = " Hr Employee Family Lines"

    employee_id = fields.Many2one('hr.employee', 'Employee')
    family_member_id = fields.Many2one('hr.employee.family.member',
                                       string='Family Member', ondelete='restrict')
    is_our_employee = fields.Boolean('Is Our Company Employee', default=False)
    rel_employee = fields.Many2one('hr.employee')
    name = fields.Char('Name', size=128)
    age = fields.Date('Birth year')
    current_organization = fields.Char('Organization', size=64)
    current_job = fields.Char('Job', size=64)
    contact = fields.Char('Contact phone', size=16)
    place_of_birth = fields.Char('Place of Birth', size=16)
    is_relative = fields.Boolean('Is Relative', default=False)
    is_child_check = fields.Boolean('Is Child Check', default=False)
    age_compute = fields.Integer(compute='_compute_age', string=u'нас')

    @api.multi
    def _compute_age(self):
        for obj in self:
            birthday = datetime.strptime(obj.age, '%Y-%m-%d')
            start = datetime.today().year - birthday.year
            obj.age_compute = start
    
    @api.onchange('rel_employee')
    def onchange_rel_employee(self):
        for obj in self:
            obj.name = obj.rel_employee.name
            obj.age = obj.rel_employee.sudo().birthday
            obj.current_job = obj.rel_employee.job_id.name
            obj.current_organization = obj.rel_employee.company_id.name
            obj.contact = obj.rel_employee.mobile_phone


class hr_nationality (models.Model):
    _code = 'hr.code'
    _name = 'hr.nationality'

    code = fields.Char('Code', size=128, required=True)
    name = fields.Char('Nationality', size=128, required=True)


class hr_social_origin (models.Model):
    _name = 'hr.social.origin'

    name = fields.Char('Social Origin', size=128, required=True)


class hr_school (models.Model):
    _name = 'hr.school'
    _rec_name = 'name'

    name = fields.Char('School', size=128, required=True)


class hr_skill_level (models.Model):
    _name = 'hr.skill.level'

    code = fields.Char('Code', size=128, required=True)
    name = fields.Char('Name', size=128, required=True)


class hr_job_type (models.Model):
    _name = "hr.job.type"
    _description = "Job Type"

    name = fields.Char('Job Type', required=True)
    job_id = fields.Many2one('hr.job', string='Job')


class hr_education_level(models.Model):
    _name = "hr.education.level"
    _description = "Level of Education"

    name = fields.Char("Level of Education", required=True, translate=True)


class hr_education (models.Model):
    _name = 'hr.education'
    _description = 'Employee Education'

    school_id = fields.Many2one('hr.school', string='School', required=True)
    education_level = fields.Many2one('hr.education.level', string='Education level')
    education_degree = fields.Many2one('hr.recruitment.degree',
                                       string='Education Degree', required=True)
    entered_date = fields.Date('Entered Date', required=True)
    finished_date = fields.Date('Finished Date')
    profession_name = fields.Char('Profession', size=128, required=True)
    school_location = fields.Char('School location', size=64)
    diploma_number = fields.Char('Diploma Number', size=128)
    diploma_topics = fields.Char('Diploma Topic', size=128, required=True)
    gpa = fields.Float('GPA', required=True)
    employee_id = fields.Many2one('hr.employee', string='Employee')


class hr_language(models.Model):
    _name = 'hr.language'
    _description = 'Employee language'

    language = fields.Many2one('hr.employee.lang', string='Language', required=True)
    listening_skill = fields.Selection([('beginning', 'Beginning'),
                                        ('intermediate', 'Intermediate'),
                                        ('advanced', 'Advanced')],
                                       string='Listening', required=True)
    speaking_skill = fields.Selection([('beginning', 'Beginning'),
                                       ('intermediate', 'Intermediate'),
                                       ('advanced', 'Advanced')],
                                      string='Speaking', required=True)
    reading_skill = fields.Selection([('beginning', 'Beginning'),
                                      ('intermediate', 'Intermediate'),
                                      ('advanced', 'Advanced')],
                                     string='Reading', required=True)
    writing_skill = fields.Selection([('beginning', 'Beginning'),
                                      ('good', 'Intermediate'),
                                      ('advanced', 'Advanced')],
                                     string='Writing', required=True)
    employee_id = fields.Many2one('hr.employee', 'Employee')


class hr_talent_type(models.Model):
    _name = 'hr.talent.type'
    _description = 'Talent Type'

    name = fields.Char(string='Name', required=True)


class hr_arts_sports_talent (models.Model):
    _name = 'hr.arts.sports.talent'
    _description = 'Arts Sports Talent Type'

    name = fields.Many2one('hr.talent.type', string='Talent Type', required=True)
    years_of_study = fields.Integer('Years Of Study')
    prefix = fields.Char('Prefix')
    success = fields.Char('Success')
    employee_id = fields.Many2one('hr.employee', string='Employee')


class hr_award_type (models.Model):
    _name = 'hr.award.type'
    _description = 'Employee Award Type'
    _rec_name = "name"

    name = fields.Char(string='Name', size=64, required=True)
    parent_id = fields.Many2one('hr.award.type', string='Parent Type')


class hr_award (models.Model):
    _name = 'hr.award'
    _description = 'Employee Award'
    _rec_name = "type_id"

    type_id = fields.Many2one('hr.award.type', string='Award Type', required=True)
    date = fields.Date(string='Date', required=True)
    amount = fields.Integer(string='Amount')
    employee_id = fields.Many2one('hr.employee', string='Employee')
    awarding_organization = fields.Char('Awarding Organization')
    active = fields.Boolean('Is Active', default=True)

    @api.multi
    def archive(self):
        for obj in self:
            obj.active = False

    @api.multi
    def release_archive(self):
        for obj in self:
            obj.active = True


class hr_software_skill(models.Model):
    _name = 'hr.software.skill'
    _description = 'Employee software skill'

    name = fields.Many2one('software.technic', string='Name', required=True)
    software_skill = fields.Selection([('low', 'Low'),
                                       ('good', 'Good'),
                                       ('excellent', 'Excellent')],
                                      string='Software skill', required=True)
    employee_id = fields.Many2one('hr.employee', string='Employee')


class software_technic(models.Model):
    _name = 'software.technic'
    _description = 'Software Technic'

    name = fields.Char('Name', required=True)


class hr_driver_license(models.Model):
    _name = "hr.driver.license"

    licence_type = fields.Selection([('B', 'B'),
                                     ('C', 'C'),
                                     ('D', 'D'),
                                     ('E', 'E'),
                                     ('M', 'M')], string='License type')
    employee_id = fields.Many2one('hr.employee', string="Employee")


class hr_job_classification (models.Model):
    _name = "hr.job.classification"
    _description = "Job Classification"

    code = fields.Char('Code', required=True)
    name = fields.Char('Name', required=True)


class hr_disciplinary_measure (models.Model):
    _name = 'hr.disciplinary.measure'
    _description = 'Employee Disciplinary Measures'
    _rec_name = "name"

    name = fields.Char(string='Name', size=128, required=True)
    parent_id = fields.Many2one('hr.disciplinary.measure',
                                string='Parent Measure')


class hr_punishment (models.Model):
    _name = 'hr.punishment'
    _description = 'Employee Punishment'
    _rec_name = "disciplinary_measure_id"

    name = fields.Char(string='Name', size=128)
    reason = fields.Text(string='Reason')
    disciplinary_measure_id = fields.Many2one('hr.disciplinary.measure',
                                              string='Disciplinary Measure', required=True)
    date = fields.Date(string='Date', required=True)
    enddate = fields.Date(string='End Date')
    employee_id = fields.Many2one('hr.employee', string='Employee')
    active = fields.Boolean('Is Active', default=True)

    @api.multi
    def archive(self):
        for obj in self:
            obj.active = False

    @api.multi
    def release_archive(self):
        for obj in self:
            obj.active = True


class hr_benefit_type (models.Model):
    _name = 'hr.benefit.type'
    _description = 'Employee Benefit Type'
    _rec_name = "name"

    name = fields.Char(string='Name', size=128, required=True)
    parent_id = fields.Many2one('hr.benefit.type', string='Benefit Type')

class hr_benefit (models.Model):
    _name = 'hr.benefit'
    _description = 'Employee Benefit'
    _rec_name = "name"

    name = fields.Char(string='Name', size=128, required=True)
    type_id = fields.Many2one('hr.benefit.type', string='Benefit Type', required=True)
    date = fields.Date(string='Date', required=True)
    amount = fields.Integer(string='Amount')
    employee_id = fields.Many2one('hr.employee', string='Employee')
    active = fields.Boolean('Is Active', default=True)

    @api.multi
    def archive(self):
        for obj in self:
            obj.active = False

    @api.multi
    def release_archive(self):
        for obj in self:
            obj.active = True


class hr_prize(models.Model):
    _name = 'hr.prize'
    _description = 'Employee prize'

    name = fields.Char('Name', size=64, required=True)
    date = fields.Date('Date', required=True)
    organization = fields.Char('Organization')
    reason = fields.Char('Reason')
    type = fields.Char('Prize Type')
    employee_id = fields.Many2one('hr.employee', 'Employee')


class hr_employment (models.Model):
    _name = 'hr.employment'
    _description = 'Employee Employment'
    _rec_name = "organization"

    organization = fields.Char('Organization', size=124)
    organization_type = fields.Char('Organization type', size=124)
    job_id = fields.Many2one('hr.job', 'Job title', size=124)
    job_title = fields.Char('Job Title')
    entered_date = fields.Date('Entered Date')
    resigned_date = fields.Date('Resigned Date')
    resigned_reason = fields.Text('Resigned reason')
    working_time_type = fields.Selection([('day', 'Day'),
                                          ('shift', 'Shift')],
                                         string='Time Type')
    working_condition = fields.Selection([('normal', 'Normal'),
                                          ('abnormal', 'Abnormal')],
                                         string='Working Condition')
    insurance_month = fields.Float('Insurance Month')
    wage = fields.Integer('Wage')
    employee_id = fields.Many2one('hr.employee', "Employee")
    responsibility = fields.Char('Responsibility')
    in_this_organization = fields.Boolean('In this organization')
    total_worked = fields.Float('Total Worked')
    experience = fields.Char('Experience')
    contact = fields.Char('Contact')
    paid_social_insurance = fields.Boolean('Paid social insurance')

    @api.onchange('resigned_date', 'total_worked')
    def onchange_resigned_date(self):
        if self.entered_date:
            dateformat = "%Y-%m-%d"
            d1 = datetime.strptime(self.resigned_date, dateformat)
            d2 = datetime.strptime(self.entered_date, dateformat)
            v1 = d1 - d2
            total_days = 0.0
            total_days = float(v1.days)
            self.total_worked = total_days / 365.0


class EmploymentConfirmation(models.Model):
    _name = 'hr.employment.approvation'
    _description = 'Employee Employment Approvation Information'

    name = fields.Char('Name', required=True)
    company_id = fields.Many2one('res.company', default=lambda self: self.env.user.company_id, string='Company', required=True)
    
    organization = fields.Char('Organization', required=True)
    position = fields.Char('Position')
    phone = fields.Char('Phone')
    email = fields.Char('Email')
    relationship = fields.Text('Relationship with Employee')
    
    employee = fields.Many2one('hr.employee', "Employee")
    

class hr_work_skill (models.Model):
    _name = "hr.work.skill"
    _description = "Work Skill"

    name = fields.Char('Work Skill', required=True)
    necessity = fields.Char('Necessity')
    level = fields.Char('Level', required=True)
    description = fields.Char('Description')
    employee_id = fields.Many2one('hr.employee', string="Employee")


class hr_certificate (models.Model):
    _name = "hr.certificate"
    _description = "Employee Certificate"

    employee_id = fields.Many2one('hr.employee', string="Employee")
    code = fields.Char(string='Code')
    classification = fields.Char(string='Traning/Exam name')
    certificate_code = fields.Char(string='Certificate Code')
    start_date = fields.Date(string='Date started')
    end_date = fields.Date(string='Date ended')
    organization = fields.Char(string='Organization')


class HolidayDaysConfigure (models.Model):
    _name = "hr.holiday.days.config"
    _description = "Holiday Days Configure"

    name = fields.Char('Holiday Days Configure')
    condition_job = fields.Selection([('normal', 'Normal'),
                                      ('abnormal', 'Abnormal')],
                                     string='A condition of the job')
    down = fields.Integer(string='Down')
    upper = fields.Integer(string='Upper')
    days = fields.Integer(string="Days")

class HrEmployeeAgeRange(models.Model):
    _name = "hr.employee.age.range"
    _description = "Employee Age Range"

    name = fields.Char(string='Range', size=16)
    lower_age_limit = fields.Integer(string='Lower Age Limit')
    upper_age_limit = fields.Integer(string="Upper Age Limit")


class HrEmployee(models.Model):
    _inherit = "hr.employee"

    age_range = fields.Many2one('hr.employee.age.range', string='Age Range')


    @api.depends('birthday', 'age')
    @api.onchange('birthday', 'age')
    def _compute_age_range(self, all_employees=False):
        age_range_obj = self.env['hr.employee.age.range'].browse(self._context.get('name'))
        if not all_employees:
            if self:
                for employee in self:
                    age_range_id = age_range_obj.search(
                        [('lower_age_limit', '<=', employee.age), ('upper_age_limit', '>=', employee.age)], limit=1)
                    if age_range_id:
                        employee.age_range = age_range_id.id
            else:
                current_date = time.strftime('%Y-%m-%d')
                month = datetime.strptime(current_date, '%Y-%m-%d').month
                day = datetime.strptime(current_date, '%Y-%m-%d').day
                query = ('SELECT id  FROM hr_employee WHERE extract(month from birthday) = %s  and extract(day from birthday) = %s ' % (
                    month, day))
                self._cr.execute(query)
                fetch = map(lambda x: x[0], self._cr.fetchall())
                if fetch:
                    for employee in self.browse(fetch):
                        age_range_id = age_range_obj.search(
                            [('lower_age_limit', '<=', employee.age), ('upper_age_limit', '>=', employee.age)], limit=1)
                        if age_range_id:
                            employee.age_range = age_range_id.id
        else:
            for employee in self.search([]):
                age_range_id = age_range_obj.search(
                    [('lower_age_limit', '<=', employee.age), ('upper_age_limit', '>=', employee.age)], limit=1)
                if age_range_id:
                    employee.age_range = age_range_id.id


    # ажилтны насны ангилалыг тохиргоог автоматаар авдаг крон
    @api.multi
    def cron_age_range(self):
        return self._compute_age_range()

   # бүх ажилтны насны ангилалыг тохиргоог автоматаар авдаг крон
    @api.multi
    def cron_age_range2(self):
        return self.change_age_range_all_employees()

    #ажилтны насны ангилалын тохиргоог бүх ажилтан авах функц
    @api.multi
    def change_age_range_all_employees(self):
        self._compute_age_range(all_employees=True)


class HrEmployeeLang(models.Model):
    _name = "hr.employee.lang"

    name = fields.Char(string='Language', required=True)