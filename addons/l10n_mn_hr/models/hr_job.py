# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api


class HrJob(models.Model):
    _inherit = "hr.job"

    condition = fields.Selection([
        ('normal', 'Normal'),
        ('abnormal', 'Abnormal')], string='A condition of the job')
    job_classification = fields.Many2one('hr.job.classification', string='Job classification')

