# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/,
# http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
import logging

from odoo import api, fields, models
from datetime import datetime
import time

_logger = logging.getLogger('odoo')


class HrRequiredMaterials(models.Model):
    _name = "hr.required.materials"
    _description = "Required Materials"

    name = fields.Char("Required Materials", required=True)
    note = fields.Text("Description")
    company_id = fields.Many2one('res.company', default=lambda self: self.env.user.company_id)