# -*- coding: utf-8 -*-


from odoo import fields, models

class ResCompany(models.Model):
    _inherit = "res.company"

    hr_contract_number = fields.Selection([
            (1, 'One labor agreement is applied to the balance sheet'),
            (2, 'More than one employment agreement may be applied to the balance sheet')
            ], default=1, string="HR Contract Number")
