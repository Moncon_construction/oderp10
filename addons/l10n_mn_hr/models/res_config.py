# -*- coding: utf-8 -*-


from odoo import api, fields, models

class HrConfigSettings(models.TransientModel):
    _name = 'hr.config.settings'
    _inherit = 'res.config.settings'
    _order = "id DESC"

    def _default_hr_contract_number(self):
        return self.env.user.company_id.hr_contract_number

    company_id = fields.Many2one('res.company', string='Company', required=True,default=lambda self: self.env.user.company_id)
    hr_contract_number = fields.Selection([(1, 'One labor agreement is applied to the balance sheet'),
                                          (2, 'More than one employment agreement may be applied to the balance sheet')], 
                                          string="HR Contract Number",related='company_id.hr_contract_number', default=lambda self: self._default_hr_contract_number())
