# -*- coding: utf-8 -*-

import datetime
from odoo import api, fields, models, tools, SUPERUSER_ID, _
from odoo.modules import get_module_resource


class Partner(models.Model):
    _inherit = "res.partner"

    department = fields.Many2one('hr.department', string='Department', readonly=True) # l10n_mn_discuss модульд department_id нэртэй талбар зарлагдсан тул department болгож солив.