from odoo import fields, models


class Users(models.Model):
    _inherit = "res.users"

    allowed_department_ids = fields.Many2many('hr.department', 'res_users_department_rel', 'uid', 'did', string='Allowed Departments')
    department_id = fields.Many2one('hr.department', related='employee_ids.department_id', store=True, string='Department')

    def _compute_last_name(self):
        for obj in self:
            if len(obj.employee_ids) == 1:
                obj.last_name = obj.employee_ids[0].last_name
            else:
                obj.last_name = ""

    last_name = fields.Char(compute='_compute_last_name')
