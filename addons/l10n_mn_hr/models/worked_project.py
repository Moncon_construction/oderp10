# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/,
# http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
import logging

from odoo import api, fields, models
from datetime import datetime


class WorkedProject(models.Model):

    _name = "worked.project"

    employee_id = fields.Many2one("hr.employee")
    project_id = fields.Many2one("project.project")
    position = fields.Char()
    start_date = fields.Date()
    end_date = fields.Date()
    work_and_duties = fields.Text(string="Work and duties performed")
