# -*- coding: utf-8 -*-
from odoo import api, fields, models


class EmployeeInfoChange(models.TransientModel):
    _name = 'employee.info.change'
    _description = 'Wizard for employee info change'

    job_classification = fields.Many2one('hr.job.classification', string='Job classification')
    skill_level = fields.Many2one('hr.skill.level', string="Skill level")
    parent_id = fields.Many2one('hr.employee', string='Manager')
    coach_id = fields.Many2one('hr.employee', string='Coach')

    @api.multi
    def employee_info_change(self):
        context = dict(self._context or {})
        employee_ids = self.env['hr.employee'].browse(context.get('active_ids'))
        for employee in employee_ids:
            if self.job_classification:
                employee.job_classification = self.job_classification
            if self.skill_level:
                employee.skill_level = self.skill_level
            if self.parent_id:
                employee.parent_id = self.parent_id
            if self.coach_id:
                employee.coach_id = self.coach_id
