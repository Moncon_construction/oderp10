# -*- encoding: utf-8 -*-

{
    'name': "Mongolian HR Appraisal",
    'version': '1.0',
    'depends': ['hr', 'calendar', 'survey'],
    'author': 'Asterisk Technologies LLC',
    'website' : 'http://asterisk-tech.mn',
    'category': 'Human Resources',
    'description': """
Тогтмол ажилтны үнэлгээ
==============================================
""",
    "data": [
        'security/ir.model.access.csv',
        'security/hr_appraisal_security.xml',
        'views/hr_appraisal_views.xml',
        'report/hr_appraisal_report_views.xml',
        'views/hr_department_views.xml',
        'data/hr_appraisal_data.xml',
    ],
    "demo": [
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
