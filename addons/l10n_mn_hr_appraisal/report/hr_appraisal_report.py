# -*- coding: utf-8 -*-

from odoo import tools
from odoo import api, models, fields


class HrAppraisalReport(models.Model):
    _name = 'hr.appraisal.report'
    _description = "Appraisal Statistics"

    APPRAISAL_STATES = [
        ('new', 'To Start'),
        ('pending', 'Appraisal Sent'),
        ('done', 'Done'),
        ('cancel', "Cancelled"),
    ]

    create_date = fields.Date(string='Create Date', readonly=True)
    department_id = fields.Many2one('hr.department', string='Department', readonly=True)
    deadline = fields.Date(string="Deadline", readonly=True)
    final_interview = fields.Date(string="Interview", readonly=True)
    employee_id = fields.Many2one('hr.employee', string="Employee", readonly=True)
    state = fields.Selection(APPRAISAL_STATES, 'Status', readonly=True)

    _order = 'create_date desc'

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, 'l10n_mn_hr_appraisal_report')
        self.env.cr.execute("""
            create or replace view l10n_mn_hr_appraisal_report as (
                 select
                     min(a.id) as id,
                     date(a.create_date) as create_date,
                     a.employee_id,
                     e.department_id as department_id,
                     a.start_appraisal_date as deadline,
                     a.date_final_interview as final_interview,
                     a.state
                     from hr_appraisal a
                        left join hr_employee e on (e.id=a.employee_id)
                 GROUP BY
                     a.id,
                     a.create_date,
                     a.state,
                     a.employee_id,
                     a.start_appraisal_date,
                     a.date_final_interview,
                     e.department_id
                )
            """)
