# -*- coding: utf-8 -*-

{
    'name': "Mongolian HR attendances features",
    'version': '1.0',
    'depends': [
        'hr_attendance',
        'l10n_mn_hr',
        'l10n_mn_web'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       - Download attendance logs from ZKTeco time attendance devices
       - Supported Devices
            Firmware Version : Ver 6.21 Nov 19 2008
            Platform : ZEM500
            DeviceName : U580

            Firmware Version : Ver 6.60 Apr 9 2010
            Platform : ZEM510_TFT
            DeviceName : T4-C

            Firmware Version : Ver 6.60 Dec 1 2010
            Platform : ZEM510_TFT
            DeviceName : T4-C

            Firmware Version : Ver 6.60 Mar 18 2011
            Platform : ZEM600_TFT
            DeviceName : iClock260

            Platform         : ZEM560_TFT
            Firmware Version : Ver 6.60 Feb  4 2012
            DeviceName       : (unknown device)

            Firmware Version : Ver 6.60 Oct 29 2012
            Platform : ZEM800_TFT
            DeviceName : iFace402/ID

            Firmware Version : Ver 6.60 Mar 18 2013
            Platform : ZEM560
            DeviceName : MA300

            Firmware Version : Ver 6.60 Dec 27 2014
            Platform : ZEM600_TFT
            DeviceName : iFace800/ID

            Firmware Version : Ver 6.60 Nov 6 2017
            Platform : ZMM220_TFT
            DeviceName : (unknown device)

            Firmware Version : Ver 6.60 Jun 9 2017
            Platform : JZ4725_TFT
            DeviceName : K20

            Firmware Version : Ver 6.60 Aug 23 2014
            Platform : ZEM600_TFT
            DeviceName : VF680

            Firmware Version : Ver 6.70 Feb 16 2017
            Platform : ZLM30_TFT
            DeviceName : RSP10k1

            Firmware Version : Ver 6.60 Jun 16 2015
            Platform : JZ4725_TFT
            DeviceName : iClock260

            Firmware Version : Ver 6.60 Jun 16 2015
            Platform : JZ4725_TFT
            DeviceName : K14

            Firmware Version : Ver 6.60 Jun 5 2015
            Platform : ZMM200_TFT
            DeviceName : iClock3000/ID

            Firmware Version : Ver 6.70 Jul 12 2013
            Platform : ZEM600_TFT
            DeviceName : iClock880-H/ID

            Firmware Version : Ver 6.20 Nov  7 2007
            Platform : ZEM500_TFT
            DeviceName : (unknown device)

            Firmware Version : Ver 6.60 Sep  9 2016
            Platform : ZMM200_TFT
            DeviceName : TX628
    """,
    'data': [
        'security/hr_attendance_security.xml',
        'security/ir.model.access.csv',
        'views/hr_attendance_raw_data_views.xml',
        'views/hr_attendance_device_views.xml',
        'views/res_config_views.xml',
        'views/hr_employee_views.xml',
        'views/hr_only_my_attendance_view.xml',
        'views/hr_attendance_views.xml',
        'wizard/hr_attendance_download.xml',
        'wizard/hr_attendance_report_view.xml',
        'data/auto_download_attendance_cron.xml',
    ],
    'external_dependencies': {
        'python': ['zk'],
    }
}
