# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* l10n_mn_hr_attendance
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 10.0+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-09-04 05:14+0000\n"
"PO-Revision-Date: 2020-09-04 05:14+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:145
#, python-format
msgid "Abs"
msgstr "Тас"

#. module: l10n_mn_hr_attendance
#: model:res.groups,name:l10n_mn_hr_attendance.group_hr_attendance_advanced
msgid "Advanced Configuration"
msgstr "Ахисан Тохиргоо"

#. module: l10n_mn_hr_attendance
#: model:ir.model,name:l10n_mn_hr_attendance.model_hr_attendance
msgid "Attendance"
msgstr "Ирц"

#. module: l10n_mn_hr_attendance
#: model:ir.model,name:l10n_mn_hr_attendance.model_hr_attendance_device
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_raw_data_device
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_employee_attendance_device_device_id
msgid "Attendance Device"
msgstr "Ирцийн төхөөрөмж"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_employee_attendance_device_ids
msgid "Attendance Device IDs"
msgstr "Ирцийн төхөөрөмжийн дугаар"

#. module: l10n_mn_hr_attendance
#: model:ir.actions.act_window,name:l10n_mn_hr_attendance.attendance_raw_data_action
#: model:ir.ui.menu,name:l10n_mn_hr_attendance.menu_attendance_raw_data
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.attendance_raw_data_form_view
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.attendance_raw_data_tree_view
msgid "Attendance Raw Data"
msgstr "Түүхий ирц"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:112
#, python-format
msgid "Attendance code"
msgstr "Ирцийн код"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/models/hr_attendance_raw_data.py:23
#, python-format
msgid "Cannot create raw attendance, but if you have 'Attendance/Advanced Configuration' right, you can import it !!!"
msgstr "Түүхий ирцийг үүсгэх боломжгүй, гэвч хэрвээ танд 'Ирц/Ахисан Тохиргоо' эрх байгаа бол импортлон үүсгэх боломжтой !!!"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,help:l10n_mn_hr_attendance.field_hr_attendance_device_use_udp
msgid "Check this if your device uses UDP port."
msgstr "Хэрвээ таны төхөөрөмж UDP порт ашигладаг бол энийг сонгоно уу."

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_in_time
msgid "Check-in time"
msgstr "Ирсэн цаг"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_out_time
msgid "Check-out time"
msgstr "Гарсан цаг"

#. module: l10n_mn_hr_attendance
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.hr_attendance_download_view_form
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.hr_attendance_report_view_form
msgid "Close"
msgstr "Хаах"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_raw_data_company_id
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_report_company_id
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.attendance_raw_data_search_view
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.view_attendance_search
msgid "Company"
msgstr "Компани"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:97
#, python-format
msgid "Company name"
msgstr "Байгууллагын нэр"

#. module: l10n_mn_hr_attendance
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.hr_attendance_device_view_form
msgid "Connection Information"
msgstr "Холболтын Мэдээлэл"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/models/hr_attendance_device.py:55
#, python-format
msgid "Connection close failed: %s"
msgstr "Холболтыг салгахад алдаа гарлаа: %s"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/models/hr_attendance_device.py:74
#, python-format
msgid "Connection is successful.\n"
"FIRMWARE VERSION: %s\n"
"PLATFORM: %s\n"
"DEVICE NAME: %s\n"
"MAC ADDRESS: %s\n"
"TIME: %s\n"
" USAGE: %s"
msgstr "Холболт амжилттай.\n"
"FIRMWARE VERSION: %s\n"
"PLATFORM: %s\n"
"DEVICE NAME: %s\n"
"MAC ADDRESS: %s\n"
"ЦАГ: %s\n"
" ХЭРЭГЛЭЭ: %s"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/models/hr_attendance_device.py:45
#, python-format
msgid "Connection open failed: %s"
msgstr "Холболтыг үүсгэхэд алдаа гарлаа: %s"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:103
#, python-format
msgid "Create Date"
msgstr "Үүсгэсэн огноо"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_device_create_uid
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_download_create_uid
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_raw_data_create_uid
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_report_create_uid
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_employee_attendance_device_create_uid
msgid "Created by"
msgstr "Үүсгэгч"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_device_create_date
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_download_create_date
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_raw_data_create_date
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_report_create_date
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_employee_attendance_device_create_date
msgid "Created on"
msgstr "Үүсгэсэн огноо"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_raw_data_date
msgid "Date"
msgstr "Огноо"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_download_date_from
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_report_date_from
msgid "Date From"
msgstr "Эхлэх огноо"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_download_date_to
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_report_date_to
msgid "Date To"
msgstr "Дуусах огноо"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:116
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_raw_data_department_id
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.attendance_raw_data_search_view
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.view_attendance_search
#, python-format
msgid "Department"
msgstr "Хэлтэс"

#. module: l10n_mn_hr_attendance
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.attendance_raw_data_search_view
msgid "Device"
msgstr "Ирцийн төхөөрөмж"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/models/hr_employee.py:40
#, python-format
msgid "Device ID of %s device is already exists for employee: %s"
msgstr "%s төхөөрөмж дээрх дугаар %s ажилтны хувьд аль хэдийн орсон байна"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_device_ip
msgid "Device IP"
msgstr "Төхөөрөмжийн IP"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_download_device_ids
msgid "Devices"
msgstr "Төхөөрөмж"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_device_display_name
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_download_display_name
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_raw_data_display_name
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_report_display_name
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_employee_attendance_device_display_name
msgid "Display Name"
msgstr "Дэлгэцийн Нэр"

#. module: l10n_mn_hr_attendance
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.hr_attendance_download_view_form
msgid "Do you want to download ATTENDANCE ?"
msgstr "ИРЦ   --> татах гэж байна уу ?"

#. module: l10n_mn_hr_attendance
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.hr_attendance_download_view_form
msgid "Do you want to download RAW ATTENDANCE ?"
msgstr "ТҮҮХИЙ ИРЦ   --> татах гэж байна уу ?"

#. module: l10n_mn_hr_attendance
#: model:ir.actions.act_window,name:l10n_mn_hr_attendance.hr_attendance_download_action
#: model:ir.ui.menu,name:l10n_mn_hr_attendance.hr_attendance_download_menu
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.hr_attendance_download_view_form
msgid "Download Attendance"
msgstr "Ирц татах"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_device_download_automatically
msgid "Download Automatically"
msgstr "Автоматаар татах"

#. module: l10n_mn_hr_attendance
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.hr_attendance_download_view_form
msgid "Download Raw Attendance"
msgstr "Түүхий ирц татах"

#. module: l10n_mn_hr_attendance
#: model:ir.model,name:l10n_mn_hr_attendance.model_hr_attendance_download
msgid "Download employee attendances."
msgstr "Ажилчдын ирц татах"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:101
#, python-format
msgid "Duration"
msgstr "Тайлант хугацаа"

#. module: l10n_mn_hr_attendance
#: model:ir.model,name:l10n_mn_hr_attendance.model_hr_employee
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_raw_data_employee_id
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_report_employee_ids
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.attendance_raw_data_search_view
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.hr_attendance_report_view_form
msgid "Employee"
msgstr "Ажилтан"

#. module: l10n_mn_hr_attendance
#: model:ir.model,name:l10n_mn_hr_attendance.model_hr_employee_attendance_device
msgid "Employee Attendance Device IDs"
msgstr "Ажилтны төхөөрөмж дээрх дугаарууд"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_employee_attendance_device_employee_device_id
msgid "Employee Device ID"
msgstr "Ажилтны төхөөрөмж дээрх дугаар"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_company_id
msgid "Employee company"
msgstr "Компани"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_employee_attendance_device_employee_id
msgid "Employee id"
msgstr "Ажилтан"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_download_employee_ids
msgid "Employees"
msgstr "Ажилтан"

#. module: l10n_mn_hr_attendance
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.hr_attendance_report_view_form
msgid "Export"
msgstr "Экспорт"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_download.py:118
#, python-format
msgid "Finished creating attendance to the database."
msgstr "Ирц үүсгэж дууслаа."

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_download.py:102
#, python-format
msgid "Finished creating raw attendance to the database."
msgstr "Түүхий ирц үүсгэж дууслаа."

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:62
#, python-format
msgid "Fri"
msgstr "Ба"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:242
#: model:ir.actions.act_window,name:l10n_mn_hr_attendance.hr_attendance_report_action
#: model:ir.ui.menu,name:l10n_mn_hr_attendance.hr_attendance_report_menu
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.hr_attendance_report_view_form
#, python-format
msgid "HR Attendance Detail Report"
msgstr "Ирцийн дэлгэрэнгүй тайлан"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_device_id
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_download_id
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_raw_data_id
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_report_id
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_employee_attendance_device_id
msgid "ID"
msgstr "ID"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_identification_id
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_raw_data_identification_id
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.attendance_raw_data_search_view
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.view_attendance_search
msgid "Identification No"
msgstr "Ялгах дугаар"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/models/hr_attendance_raw_data.py:33
#, python-format
msgid "If you want to delete, you must have 'Attendance/Advanced Configuration' right !!!"
msgstr "Устгахын тулд, 'Ирц/Ахисан Тохиргоо' эрхтэй байх хэрэгтэй !!!"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/models/hr_attendance_raw_data.py:21
#, python-format
msgid "If you want to import, you must have 'Attendance/Advanced Configuration' right !!!"
msgstr "Импорт хийхийн тулд, 'Ирц/Ахисан Тохиргоо' эрхтэй байх хэрэгтэй !!!"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:115
#, python-format
msgid "Job"
msgstr "Албан тушаал"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_device___last_update
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_download___last_update
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_raw_data___last_update
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_report___last_update
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_employee_attendance_device___last_update
msgid "Last Modified on"
msgstr "Сүүлийн засвар хийсэн огноо"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_device_write_uid
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_download_write_uid
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_raw_data_write_uid
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_report_write_uid
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_employee_attendance_device_write_uid
msgid "Last Updated by"
msgstr "Сүүлийн засвар хийсэн"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_device_write_date
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_download_write_date
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_raw_data_write_date
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_report_write_date
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_employee_attendance_device_write_date
msgid "Last Updated on"
msgstr "Сүүлийн засвар хийсэн огноо"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:113
#, python-format
msgid "Last name"
msgstr "Овог"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:54
#, python-format
msgid "Mon"
msgstr "Да"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:128
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:130
#, python-format
msgid "Month"
msgstr "Сар"

#. module: l10n_mn_hr_attendance
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.attendance_raw_data_search_view
msgid "Month /Date/"
msgstr "Сар /Огноо/"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:114
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_device_name
#, python-format
msgid "Name"
msgstr "Нэр"

#. module: l10n_mn_hr_attendance
#: model:ir.actions.act_window,name:l10n_mn_hr_attendance.hr_attendance_only_my_attendances
#: model:ir.ui.menu,name:l10n_mn_hr_attendance.menu_hr_attendance_only_my_attendances
msgid "Only My Attendances"
msgstr "Зөвхөн миний ирцүүд"

#. module: l10n_mn_hr_attendance
#: model:res.groups,comment:l10n_mn_hr_attendance.group_hr_attendance_advanced
msgid "Only this group users can delete/import attendance raw data."
msgstr "Зөвхөн уг группын хэрэглэгчид түүхий ирц устгах/импортлох боломжтой."

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_device_password
msgid "Password"
msgstr "Нууц үг"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_device_port
msgid "Port No"
msgstr "Портын дугаар"

#. module: l10n_mn_hr_attendance
#: model:ir.ui.menu,name:l10n_mn_hr_attendance.menu_hr_attendance_report_root
msgid "Reports"
msgstr "Тайлан"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:64
#, python-format
msgid "Sat"
msgstr "Бя"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_download.py:124
#, python-format
msgid "Started auto download attendance."
msgstr "Ирц автоматаар татаж эхэллэээ."

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_download.py:104
#, python-format
msgid "Started creating attendance to the database."
msgstr "Ирц үүсгэж эхэллээ."

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_download.py:83
#, python-format
msgid "Started creating raw attendance to the database."
msgstr "Түүхий ирц үүсгэж эхэллээ."

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_download.py:137
#, python-format
msgid "Started downloading attendance."
msgstr "Ирц татаж эхэллээ."

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_download.py:204
#, python-format
msgid "Started downloading raw attendance."
msgstr "Түүхий ирц татаж эхэллээ."

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_download.py:59
#, python-format
msgid "Successfully downloaded users from '%s' device."
msgstr "'%s' төхөөрөмжөөс хэрэглэгчдийг амжилттай татлаа."

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:66
#, python-format
msgid "Sun"
msgstr "Ня"

#. module: l10n_mn_hr_attendance
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.hr_attendance_device_view_form
msgid "Test Connection"
msgstr "Холболтыг Тестлэх"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_download.py:128
#, python-format
msgid "There is no devices configured for auto download."
msgstr "Автомат татах тохиргоотой төхөөрөмж байхгүй байна."

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:60
#, python-format
msgid "Thu"
msgstr "Пү"

#. module: l10n_mn_hr_attendance
#: model:ir.actions.act_window,name:l10n_mn_hr_attendance.hr_attendance_device_action
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.hr_attendance_device_view_form
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.hr_attendance_device_view_tree
msgid "Time Attendance Device"
msgstr "Цаг бүртгэлийн төхөөрөмж"

#. module: l10n_mn_hr_attendance
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.hr_attendance_view_config
msgid "Time Attendance Device Configuration"
msgstr "Цаг бүртгэлийн төхөөрөмжийн тохиргоо"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_device_tz
msgid "Timezone"
msgstr "Цагийн бүс"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_total_attendance_hours
msgid "Total attendance(by hour)"
msgstr "Нийт ирц(Цаг)"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:56
#, python-format
msgid "Tue"
msgstr "Мя"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,field_description:l10n_mn_hr_attendance.field_hr_attendance_device_use_udp
msgid "Use UDP port"
msgstr "UDP порт ашиглах"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,help:l10n_mn_hr_attendance.field_hr_employee_attendance_device_employee_device_id
msgid "User ID on time attendance device for this employee."
msgstr "Энэ ажилтны цаг бүртгэлийн төхөөрөмж дээрх хэрэглэгчийн ID."

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_download.py:41
#, python-format
msgid "User timezone: '%s'"
msgstr "Хэрэглэгчийн цагийн бүс: '%s'"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_download.py:40
#, python-format
msgid "Warning !\n"
" The users who is downloading attendances need to set their time zone. Settings go to the User menu!"
msgstr "Анхааруулга !\n"
" Ирц татахын тулд хэрэглэгч цагийн бүсээ тохируулсан байх ёстой. Хэрэглэгчийн цэсэнд тохиргоог хийж болно!"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:58
#, python-format
msgid "Wed"
msgstr "Лх"

#. module: l10n_mn_hr_attendance
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.attendance_raw_data_search_view
msgid "Year /Date/"
msgstr "Жил /Огноо/"

#. module: l10n_mn_hr_attendance
#: model:ir.model.fields,help:l10n_mn_hr_attendance.field_hr_employee_attendance_device_ids
msgid "You can configure employee's ID on time attendance devices."
msgstr "Цаг бүртгэлийн төхөөрөмж дээр ажилтны дугаарыг тохируулна."

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/models/hr_employee.py:30
#: code:addons/l10n_mn_hr_attendance/models/hr_employee.py:49
#, python-format
msgid "You can create only one attendance device ID without choosing device for employee: %s"
msgstr "%s ажилтын хувьд ирцийн төхөөрөмж сонгохгүйгээр зөвхөн ганц дугаарыг бүртгэж болно"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:81
#, python-format
msgid "a.l/hol"
msgstr "э/ам"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:146
#, python-format
msgid "half/att"
msgstr "ирц/дут"

#. module: l10n_mn_hr_attendance
#: model:ir.model,name:l10n_mn_hr_attendance.model_hr_attendance_raw_data
msgid "hr.attendance.raw.data"
msgstr "Түүхий ирц"

#. module: l10n_mn_hr_attendance
#: model:ir.model,name:l10n_mn_hr_attendance.model_hr_attendance_report
msgid "hr.attendance.report"
msgstr "Ирцийн дэлгэрэнгүй тайлан"

#. module: l10n_mn_hr_attendance
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.hr_attendance_download_view_form
#: model:ir.ui.view,arch_db:l10n_mn_hr_attendance.hr_attendance_report_view_form
msgid "or"
msgstr "эсвэл"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:147
#, python-format
msgid "out/work"
msgstr "гад/аж"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:77
#, python-format
msgid "p/hol"
msgstr "ц.тай/чөл"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:83
#, python-format
msgid "s.l/hol"
msgstr "өвч"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:79
#, python-format
msgid "u.p/hol"
msgstr "ц.гүй/чөл"

#. module: l10n_mn_hr_attendance
#: code:addons/l10n_mn_hr_attendance/wizard/hr_attendance_report.py:144
#, python-format
msgid "we/day"
msgstr "ам/өд"
