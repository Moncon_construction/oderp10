# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class HrEmployee(models.Model):
    _inherit = "hr.employee"

    attendance_device_ids = fields.One2many('hr.employee.attendance.device', 'employee_id', 'Attendance Device IDs', help="You can configure employee's ID on time attendance devices.")


class HrEmployeeAttendanceDevice(models.Model):
    _name = "hr.employee.attendance.device"
    _description = 'Employee Attendance Device IDs'

    employee_id = fields.Many2one('hr.employee', required=True, ondelete='cascade')
    employee_device_id = fields.Char('Employee Device ID', help="User ID on time attendance device for this employee.", required=True)
    device_id = fields.Many2one('hr.attendance.device', 'Attendance Device', ondelete='cascade')

    @api.constrains('employee_id', 'employee_device_id', 'device_id')
    def check_fields(self):
        for employee_device in self:
            # check one device id without device
            if not employee_device.device_id:
                other_employee_device_ids_without_device = self.env['hr.employee.attendance.device'].search([
                    ('employee_id', '=', employee_device.employee_id.id),
                    ('id', '!=', employee_device.id)
                ])
                if other_employee_device_ids_without_device:
                    raise ValidationError(_("You can create only one attendance device ID without choosing device for employee: %s") % employee_device.employee_id.name_related)
            # check one device id for a specific device
            if employee_device.device_id:
                other_employee_device_ids_with_device = self.env['hr.employee.attendance.device'].search([
                    ('employee_id', '=', employee_device.employee_id.id),
                    ('device_id', '=', employee_device.device_id.id),
                    ('id', '!=', employee_device.id),
                    ('employee_device_id', '=', employee_device.employee_device_id)
                ])
                if other_employee_device_ids_with_device:
                    raise ValidationError(_("Device ID of %s device is already exists for employee: %s") % (employee_device.device_id.name, employee_device.employee_id.name_related))
                # check exists device id without device
                existing_device_ids_without_device = self.env['hr.employee.attendance.device'].search([
                    ('employee_id', '=', employee_device.employee_id.id),
                    ('device_id', '=', False),
                    ('id', '!=', employee_device.id),
                    ('employee_device_id', '=', employee_device.employee_device_id)
                ])
                if existing_device_ids_without_device:
                    raise ValidationError(_("You can create only one attendance device ID without choosing device for employee: %s") % employee_device.employee_id.name_related)
