# -*- coding: utf-8 -*-
{
    'name': "Mongolian Human Resource Attendance Hour Balance",
    'version': '10.0.1.0',
    'depends': [
        'l10n_mn_hr_hour_balance'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian HR Modules',
    'description': """
        * Уг модулийг суулгаснаар ирц дээр дараах талбарууд нэмэгдэнэ:
            * Ажлын цагийн хуваарь
            * Ажил эхлэх цаг
            * Ажил дуусах цаг
            * Ажиллавал зохих цаг
            * Эрт ирэлт(Минут)
            * Хоцролт(Минут)
            * Эрт таралт(Минут)
            * Илүү цаг
            * Ирцийн төрөл
            * Нийт ажилласан цаг
            * Ирцийн тэмдэглэл
        
        * ЦАГИЙН БАЛАНС ШИНЭЧЛЭХ товчин дээр дарахад балансын мөрийн дараах талбаруудын тооцооллыг ирцээс автоматаар тооцоолно:
            * Ажилласан
                * Ажлын өдөр    
                * Ажлын өдөр /цаг/
                * Баярын өдөр
                * Баярын өдөр /цаг/
                * Амралтын өдөр
                * Амралтын өдөр /цаг/
            * Ажиллаагүй
                * Хоцролт /минут/
                * Хоцролт /цаг/
                * Ирцээрх илүү цаг
    """,
    'data': [
        'data/recompute_attendance_calculations.xml',
        'views/hour_balance_config_settings_view.xml',
        'views/hr_attendance_view.xml',
        'views/hour_balance_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
