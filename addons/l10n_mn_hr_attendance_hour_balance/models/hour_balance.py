# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
from datetime import datetime, timedelta

import pytz
from dateutil import rrule
from dateutil.relativedelta import relativedelta
from lxml import etree

from odoo import _, api, fields, models
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import UserError
from odoo.osv.orm import setup_modifiers
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT

_logger = logging.getLogger(__name__)


class HourBalance(models.Model):
    _inherit = "hour.balance"

    def get_total_overtime_hour(self, employee, date_from, date_to):
        self.ensure_one()
        attendances = self.get_total_attendances(employee, date_from, date_to)
        return sum(att.over_time_hours for att in attendances) if attendances and len(attendances) > 0 else 0

    def get_total_lag_hour_minutes(self, employee, date_from, date_to):
        self.ensure_one()
        attendances = self.get_total_attendances(employee, date_from, date_to)
        total_lag_minutes = 0.0
        total_lag_hours = 0
        for att in attendances: 
            st_date = str(att.check_in)
            end_date = str(att.check_out)

            domain = [('employee_id', '=', att.employee_id.id), ('state', '=', 'accepted'), ('type', '=', 'remove')]
            domain.extend(get_duplicated_day_domain('date_from', 'date_to', st_date, end_date, self.env.user))
            hr_holidays = self.env['hr.holidays'].sudo().search(domain)
            if hr_holidays:
                for hr_holiday in hr_holidays:
                    start_date = datetime.strptime(hr_holiday.date_from, DEFAULT_SERVER_DATETIME_FORMAT)
                    end_date = datetime.strptime(att.check_in, DEFAULT_SERVER_DATETIME_FORMAT)
                    if start_date <= end_date:
                        diff = end_date - start_date
                        hours = (diff.seconds) / 60
                        if hours > 0:
                            if att.total_lag_minutes >= hours:
                                total_lag_minutes += att.total_lag_minutes - hours
                            else:
                                if att.total_lag_minutes - hours >= 0:
                                    total_lag_minutes += att.total_lag_minutes - hours
                                else:
                                    total_lag_minutes += att.total_lag_minutes - att.total_lag_minutes
            else:
                if not att.attendance_type == 'celebration_day':
                    total_lag_minutes += att.total_lag_minutes
        # total_lag_minutes = sum(att.total_lag_minutes for att in attendances) if attendances and len(attendances) > 0 else 0
        total_lag_hours = total_lag_minutes / 60
        return total_lag_minutes, total_lag_hours

    def get_total_attendance(self, employee, date_from, date_to):
        attendances = self.get_total_attendances(employee, date_from, date_to)
        return sum(att.total_attendance_hours for att in attendances) if attendances and len(attendances) > 0 else 0

    def get_worked_days_and_hours(self, employee, date_from, date_to):
        self.ensure_one()
        
        worked_day, worked_hour, weekend_day, weekend_hour = 0, 0, 0, 0
        celeb_day, celeb_hour = 0, 0
        
        attendances = self.get_total_attendances(employee, date_from, date_to)
        if attendances and len(attendances) > 0:
            for att in attendances:

                if att.attendance_type == 'work_day':
                    worked_hour += att.total_worked_hours_of_date
                    if att.check_worked_hours_can_be_day():
                        worked_day += 1
                        
                elif att.attendance_type == 'celebration_day':
                    celeb_hour += att.total_worked_hours_of_date
                    if att.check_worked_hours_can_be_day():
                        celeb_day += 1
                    
                elif att.attendance_type == 'holiday_day':
                    weekend_hour += att.total_worked_hours_of_date
                    if att.check_worked_hours_can_be_day():
                        weekend_day += 1
                        
        return worked_day, worked_hour, weekend_day, weekend_hour, celeb_day, celeb_hour
    
    def get_balance_line_values(self, employee_id, contract_id, check_contract_date=False):
        # @Override: Ирцтэй холбоотой автомат тооцооллуудыг хийх
        self.ensure_one()
        values = super(HourBalance, self).get_balance_line_values(employee_id, contract_id, check_contract_date=check_contract_date)

        date_from, date_to = self.get_balance_dates(contract_id, check_contract_date=check_contract_date)
        if date_from and date_to:
            
            worked_days_and_hours = self.get_worked_days_and_hours(employee_id, date_from, date_to)
            working_days_at_weekend = worked_days_and_hours[2] # Амралтын өдөр ажилласан нийт өдөр
            working_hours_at_weekend = worked_days_and_hours[3] # Амралтын өдөр ажилласан нийт цаг
            celebration_days = worked_days_and_hours[4] # Баярын өдөр ажилласан нийт өдөр /Тухайн өдөр нь давхар амралтын өдөр байж болно, гэхдээ амралтын өдөр ажилласан тоо/цаг-т давхардаж тоологдохгүй/
            celebration_hours = worked_days_and_hours[5] # Баярын өдөр ажилласан нийт цаг /Тухайн өдөр нь давхар амралтын өдөр байж болно, гэхдээ амралтын өдөр ажилласан тоо/цаг-т давхардаж тоологдохгүй/
            if contract_id.hour_balance_calculate_type == 'is_salary_complete':# Хөдөлмөрийн гэрээн дээр
                balance_reg_hours = self.calc_reg_day(date_from, date_to, employee_id, contract_id, is_balance=True)
                unworkable_attendance_hours = working_hours_at_weekend + celebration_hours
                unworkable_attendance_days = working_days_at_weekend + celebration_days 
                worked_day = max(0, balance_reg_hours['working_day'] - unworkable_attendance_days)
                worked_hour = max(0, balance_reg_hours['working_hour'] - unworkable_attendance_hours)
            else:
                worked_day = worked_days_and_hours[0] # Ажлын өдөр ажилласан нийт өдөр
                worked_hour = worked_days_and_hours[1] # Ажлын өдөр ажилласан нийт цаг
                
            overtime_hour = self.get_total_overtime_hour(employee_id, date_from, date_to)
            lag_minute, lag_hour = self.get_total_lag_hour_minutes(employee_id, date_from, date_to) # Хоцролт цагаар
        else:
            worked_day, worked_hour = 0, 0
            working_days_at_weekend, working_hours_at_weekend = 0, 0
            celebration_days, celebration_hours = 0, 0
            overtime, lag_minute, lag_hour = 0, 0, 0
        values['worked_day'] = worked_day
        values['worked_hour'] = worked_hour
        values['working_days_at_weekend'] = working_days_at_weekend if working_days_at_weekend else 0
        values['working_hours_at_weekend'] = working_hours_at_weekend if working_hours_at_weekend else 0
        values['celebration_days'] = celebration_days if celebration_days else 0
        values['celebration_hours'] = celebration_hours if celebration_hours else 0
        values['overtime'] = overtime_hour if overtime_hour > 0 else 0
        values['lag_minute'] = lag_minute
        values['lag_hour'] = lag_hour
            
        return values
    
    
class HourBalanceLine(models.Model):
    
    _inherit = 'hour.balance.line'
    
    def set_values_from_line(self):
        super(HourBalanceLine, self).set_values_from_line()
        for obj in self:
            worked_day, worked_hour = 0, 0
            working_days_at_weekend, working_hours_at_weekend = 0, 0
            celebration_days, celebration_hours = 0, 0
            overtime, lag_minute, lag_hour = 0, 0, 0
            
            for line_id in obj.line_ids:
                worked_day += line_id.worked_day
                worked_hour += line_id.worked_hour
                working_days_at_weekend += line_id.working_days_at_weekend
                working_hours_at_weekend += line_id.working_hours_at_weekend
                celebration_days += line_id.celebration_days
                celebration_hours += line_id.celebration_hours
                overtime += line_id.overtime
                lag_minute += line_id.lag_minute
                lag_hour += line_id.lag_hour
            
            obj.worked_day = worked_day
            obj.worked_hour = worked_hour
            obj.working_days_at_weekend = working_days_at_weekend
            obj.working_hours_at_weekend = working_hours_at_weekend
            obj.celebration_days = celebration_days
            obj.celebration_hours = celebration_hours
            obj.overtime = overtime
            obj.lag_minute = lag_minute
            obj.lag_hour = lag_hour
