# -*- coding: utf-8 -*-

from odoo import fields, models


class HourBalanceConfigSettings(models.TransientModel):
    _inherit = 'hour.balance.config.settings'

    attendance_day_calculation_type = fields.Selection(related='company_id.attendance_day_calculation_type', string='Attendance Day Calculation Type')
    min_hours_for_calculate_worked_hour = fields.Float(related='company_id.min_hours_for_calculate_worked_hour', string='Minimum Hours for Calculate Worked Hour')