# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from dateutil import rrule
from dateutil.relativedelta import relativedelta
import logging
import pytz

from odoo import _, api, fields, models
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT


_logger = logging.getLogger(__name__)


class HrAttendance(models.Model):
    _inherit = "hr.attendance"

    work_start_hour = fields.Float(string='Work start hour', compute='compute_workable_hours', store=True, readonly=True)
    work_end_hour = fields.Float(string='Work end hour', compute='compute_workable_hours', store=True, readonly=True)
    work_start_time = fields.Datetime(string='Work start Time', compute='compute_workable_hours', store=True, readonly=True)
    work_end_time = fields.Datetime(string='Work end Time', compute='compute_workable_hours', store=True, readonly=True)
    
    total_lag_minutes = fields.Float(string='Total Lag(by minute)', compute='compute_total_lag_minutes', store=True, readonly=True)
    early_leave_minutes = fields.Float(string='Early leave(by minute)', compute='compute_early_leave_minutes', store=True, readonly=True)
    work_hour_of_date = fields.Float(string='Work hour of date', compute='compute_work_hour_of_date', store=True, readonly=True)
    over_time_hours = fields.Float(string='Overtime Hour', compute='compute_over_time_hours', store=True, readonly=True)
    minutes_before_work_hour = fields.Float(string='Early arrival(by minute)', compute='compute_minutes_before_work_hour', store=True, readonly=True)
    
    total_worked_hours_of_date = fields.Float(string='Total Worked Hour', compute='compute_total_worked_hours_of_date', store=True, readonly=True, help='Calculate From Workable Hours')

    note = fields.Char(string='Attendance Note')

    attendance_type = fields.Selection([
        ('celebration_day', 'Celebration Day'),
        ('work_day', 'Work Day'),
        ('holiday_day', 'Holiday Day')
    ], string='Attendance Type', default='work_day', compute='compute_attendance_type', store=True, readonly=True)

    contract_id = fields.Many2one('hr.contract', compute='compute_contract_id', store=True, readonly=True, string="Contract")
    working_hours = fields.Many2one('resource.calendar', compute='compute_resource_calendar', store=True, readonly=True, string="Work Hours")
    
    @api.depends('employee_id')
    def compute_contract_id(self):
        for obj in self:
            obj.contract_id = obj.employee_id.sudo().contract_id
            
    @api.depends('contract_id')
    def compute_resource_calendar(self):
        for obj in self:
            obj.working_hours = obj.contract_id.sudo().working_hours
            
    def check_worked_hours_can_be_day(self):
        self.ensure_one()
        if self.employee_id.company_id.attendance_day_calculation_type == 'total_attendance_calculation':
            return True
        elif self.employee_id.company_id.attendance_day_calculation_type == 'standard_calculation' and self.total_worked_hours_of_date >= self.employee_id.company_id.min_hours_for_calculate_worked_hour:
            return True
        return True

    @api.depends('work_start_time', 'work_end_time', 'check_in', 'working_hours')
    def compute_work_hour_of_date(self):
        for obj in self:
            work_hour_of_date = 0
            if obj.work_start_time and obj.work_end_time and not obj.check_day_is_holiday(get_day_like_display(obj.check_in, self.env.user)):
                end_date = get_day_by_user_timezone(obj.work_end_time, self.env.user)
                start_date = get_day_by_user_timezone(obj.work_start_time, self.env.user)
                work_hour_of_date = obj.working_hours.get_working_hours_of_date(start_dt=start_date, end_dt=end_date)
            obj.work_hour_of_date = work_hour_of_date    
            
    @api.depends('check_in', 'working_hours')
    def compute_workable_hours(self):
        for obj in self:
            work_start_hour, work_end_hour = 0, 0 
            work_start_time, work_end_time = False, False
            
            if obj.working_hours:
                check_in_day = get_day_like_display(obj.check_in, self.env.user)
                attends = obj.working_hours.attendance_ids.filtered(lambda att: att.dayofweek == str(check_in_day.weekday()))

                # Ажил эхлэх болон дуусах цагийг тооцох
                if attends:
                    work_start_hour = attends.sorted(key='hour_from')[0].hour_from
                    work_end_hour = attends.sorted(key='hour_to', reverse=True)[0].hour_to

                    if not obj.check_day_is_holiday(check_in_day):
                        if work_start_hour:
                            work_start_time = get_day_to_display_timezone_from_floattime(obj.check_in, work_start_hour, self.env.user)
                        if work_end_hour:
                            work_end_time = get_day_to_display_timezone_from_floattime(obj.check_in, work_end_hour, self.env.user)
                        
            obj.work_start_hour = work_start_hour
            obj.work_end_hour = work_end_hour
            obj.work_start_time = work_start_time
            obj.work_end_time = work_end_time


    @api.depends('check_in', 'work_start_time', 'employee_id')
    def compute_minutes_before_work_hour(self):
        for obj in self:
            minutes_before_work_hour = 0
    
            if obj.check_in and obj.work_start_time and obj.check_in < obj.work_start_time and not obj.check_day_is_holiday(get_day_like_display(obj.check_in, self.env.user)):
                gross_minutes = get_difference_btwn_2date(obj.check_in, obj.work_start_time, {'diff_type': 'minute'})
                paid_holiday, unpaid_holiday, annual_leave, sick_leave = obj.get_duplicated_holiday_hours(obj.check_in, obj.work_start_time, obj.employee_id)
                holiday_minutes = (paid_holiday + unpaid_holiday + annual_leave + sick_leave) * 60
                minutes_before_work_hour = gross_minutes - holiday_minutes if (gross_minutes - holiday_minutes) > 0 else 0
    
            obj.minutes_before_work_hour = minutes_before_work_hour
            
    @api.depends('check_in', 'work_start_time', 'employee_id')
    def compute_total_lag_minutes(self):
        for obj in self:
            total_lag_minutes = 0
    
            if obj.check_in and obj.work_start_time and obj.check_in > obj.work_start_time and not obj.check_day_is_holiday(get_day_like_display(obj.check_in, self.env.user)):
                gross_minutes = get_difference_btwn_2date(obj.work_start_time, obj.check_in, {'diff_type': 'minute'})
                paid_holiday, unpaid_holiday, annual_leave, sick_leave = obj.get_duplicated_holiday_hours(obj.work_start_time, obj.check_in, obj.employee_id)
                holiday_minutes = (paid_holiday + unpaid_holiday + annual_leave + sick_leave) * 60
                total_lag_minutes = gross_minutes - holiday_minutes if (gross_minutes - holiday_minutes) > 0 else 0
    
            obj.total_lag_minutes = total_lag_minutes

    @api.depends('check_out', 'work_end_time', 'employee_id')
    def compute_early_leave_minutes(self):
        for obj in self:
            early_leave_minutes = 0
    
            if obj.check_out and obj.work_end_time and obj.check_out < obj.work_end_time and not obj.check_day_is_holiday(get_day_like_display(obj.check_in, self.env.user)):
                gross_minutes = get_difference_btwn_2date(obj.check_out, obj.work_end_time, {'diff_type': 'minute'})
                paid_holiday, unpaid_holiday, annual_leave, sick_leave = obj.get_duplicated_holiday_hours(obj.check_out, obj.work_end_time, obj.employee_id)
                holiday_minutes = (paid_holiday + unpaid_holiday + annual_leave + sick_leave) * 60
                early_leave_minutes = gross_minutes - holiday_minutes if (gross_minutes - holiday_minutes) > 0 else 0
    
            obj.early_leave_minutes = early_leave_minutes
            
    @api.depends('check_out', 'work_end_time', 'employee_id')
    def compute_over_time_hours(self):
        for obj in self:
            over_time_hours = 0
    
            if obj.check_out and obj.work_end_time and obj.check_out > obj.work_end_time and not obj.check_day_is_holiday(get_day_like_display(obj.check_in, self.env.user)):
                gross_hours = get_difference_btwn_2date(obj.work_end_time, obj.check_out, {})
                paid_holiday, unpaid_holiday, annual_leave, sick_leave = obj.get_duplicated_holiday_hours(obj.work_end_time, obj.check_out, obj.employee_id)
                holiday_hours = paid_holiday + unpaid_holiday + annual_leave + sick_leave
                over_time_hours = (gross_hours - holiday_hours) if (gross_hours - holiday_hours) > 0 else 0
    
            obj.over_time_hours = over_time_hours
            
    @api.depends('check_in', 'check_out', 'total_attendance_hours', 'minutes_before_work_hour', 'over_time_hours', 'work_hour_of_date')
    def compute_total_worked_hours_of_date(self):
        for obj in self:
            # Амралтын өдөр ажилласан болон, ажлын өдөр ажилласан ч тухайн ажиллах ёстой өдөр нь хуваарь тааруулаагүй бол ирцийн цагаар нийт ажилласан цаг тооцно.
            is_holiday = obj.check_day_is_holiday(get_day_like_display(obj.check_in, self.env.user))
            if is_holiday or (not is_holiday and not obj.work_hour_of_date):
                obj.total_worked_hours_of_date = obj.total_attendance_hours
            else:
                lunch = obj.get_lunch_time()
                total_worked_hour = obj.total_attendance_hours - (obj.minutes_before_work_hour / 60) - obj.over_time_hours - lunch
                total_worked_hour = total_worked_hour if total_worked_hour > 0 else 0
                obj.total_worked_hours_of_date = total_worked_hour if total_worked_hour <= obj.work_hour_of_date else obj.work_hour_of_date

    @api.depends('check_in', 'company_id')
    def compute_attendance_type(self):
        # Ирцийн өдөр нь БАЯРЫН ӨДӨР/АМРАЛТЫН ӨДӨР/АЖЛЫН ӨДӨР эсэхийг ялгаж онооно.
        for obj in self:
            if obj.check_in:
                check_in = get_day_like_display(obj.check_in, self.env.user).date()
                celeb_days_on_conf = self.env['hr.holidays.configuration'].search([('company_id', '=', obj.company_id.id), ('date', '=', check_in)])
                if celeb_days_on_conf and len(celeb_days_on_conf) > 0:
                    obj.attendance_type = 'celebration_day'
                elif obj.check_day_is_holiday(check_in):
                    obj.attendance_type = 'holiday_day'
                else:
                    obj.attendance_type = 'work_day'
            else:
                obj.attendance_type = 'work_day'

    def get_lunch_time(self):
        self.ensure_one()
        if not self.contract_id.working_hours or not self.work_start_time or not self.work_end_time or not self.check_out:
            return 0

        check_in = change_date_to_user_tz(self.check_in, self.env.user)
        check_out = change_date_to_user_tz(self.check_out, self.env.user)
        total_attendance_hours = (check_out - check_in).total_seconds() / 3600

        resource_calendar = self.working_hours
        st_date = get_day_like_display(self.check_in, self.env.user)
        end_date = get_day_like_display(self.check_out, self.env.user)
        atts = resource_calendar.get_attendances_for_weekday(st_date)

        att_hours_without_lunch = 0
        for att in atts:
            day = datetime.combine(st_date, datetime.min.time())
            day_from = get_day_like_display_from_floattime(day, att.hour_from, self.env.user)
            day_to = get_day_like_display_from_floattime(day, att.hour_to, self.env.user)

            diff = get_difference_btwn_2date_intervals(day_from, day_to, st_date, end_date)
            att_hours_without_lunch += (diff if diff > 0 else 0)

        lunch_hours = total_attendance_hours - att_hours_without_lunch - self.minutes_before_work_hour / 60 - self.over_time_hours
        return (lunch_hours if lunch_hours > 0 else 0)

    def get_linked_holidays(self):
        result = []
        for obj in self:
            if not (obj.work_start_time and obj.work_end_time):
                continue

            st_date = obj.work_start_time
            end_date = obj.work_end_time

            domain = [('employee_id', '=', obj.employee_id.id), ('type', '=', 'remove'), ('state', '=', 'validate')]
            domain.extend(get_duplicated_day_domain('date_from', 'date_to', st_date, end_date, self.env.user))

            # Search for the employee's approved holidays between the given dates
            holidays = self.env['hr.holidays'].search(domain)
            for holiday in holidays:
                result.append(holiday)
        return result

    def get_duplicated_holiday_hours(self, st_date, end_date, employee_id):
        holidays_ids = self.get_linked_holidays()
        paid_holiday = unpaid_holiday = annual_leave = sick_leave = 0

        for holiday in holidays_ids:
            hol_st_date = get_day_by_user_timezone(holiday.date_from, self.env.user).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            hol_end_date = get_day_by_user_timezone(holiday.date_to, self.env.user).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            from_date = get_day_by_user_timezone(st_date, self.env.user).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            to_date = get_day_by_user_timezone(end_date, self.env.user).strftime(DEFAULT_SERVER_DATETIME_FORMAT)

            diff = get_difference_btwn_2date_intervals(hol_st_date, hol_end_date, from_date, to_date)

            if diff > 0:
                if holiday.holiday_status_id.balance_type == 'paid':
                    paid_holiday += diff
                elif holiday.holiday_status_id.balance_type == 'unpaid':
                    unpaid_holiday += diff
                elif holiday.holiday_status_id.balance_type == 'annual_leave':
                    annual_leave += diff
                elif holiday.holiday_status_id.balance_type == 'sick_leave':
                    sick_leave += diff

        return paid_holiday, unpaid_holiday, annual_leave, sick_leave

    def get_holiday_hours(self, st_date, end_date, employee_id):
        holidays_ids = self.get_linked_holidays()
        paid_holiday = unpaid_holiday = annual_leave = sick_leave = rehabilitated_holiday = 0

        for holiday in holidays_ids:
            hol_st_date = get_day_by_user_timezone(holiday.date_from, self.env.user).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            hol_end_date = get_day_by_user_timezone(holiday.date_to, self.env.user).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            from_date = get_day_by_user_timezone(st_date, self.env.user).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            to_date = get_day_by_user_timezone(end_date, self.env.user).strftime(DEFAULT_SERVER_DATETIME_FORMAT)

            diff = abs(get_difference_btwn_2date_intervals(hol_st_date, hol_end_date, from_date, to_date))

            if diff > self.work_hour_of_date:
                diff = self.work_hour_of_date
            if diff > 0:
                if holiday.holiday_status_id.balance_type == 'paid' and not holiday.holiday_status_id.is_nohon_amrakh:
                    paid_holiday += diff
                if holiday.holiday_status_id.balance_type == 'paid' and holiday.holiday_status_id.is_nohon_amrakh:
                    rehabilitated_holiday += diff
                elif holiday.holiday_status_id.balance_type == 'unpaid':
                    unpaid_holiday += diff
                elif holiday.holiday_status_id.balance_type == 'annual_leave':
                    annual_leave += diff
                elif holiday.holiday_status_id.balance_type == 'sick_leave':
                    sick_leave += diff

        return paid_holiday, unpaid_holiday, annual_leave, sick_leave, rehabilitated_holiday

    def recompute_calculations(self):
        self.compute_contract_id()
        self.compute_check_in_time()
        self.compute_check_out_time()
        
    @api.model
    def recompute_attendance_calculations(self, date_from, date_to):
        """
            Ирцийн тооцооллуудыг нөхөж ажиллуулах функц
        """
        count = 0
        date_from = get_display_day_to_user_day(date_from, self.env.user)
        date_to = get_display_day_to_user_day(date_to, self.env.user)
        domain = [('check_in', '!=', False), ('check_out', '!=', False)]
        domain.extend(get_duplicated_day_domain('check_in', 'check_out', date_from, date_to, self.env.user))
        atts = self.env['hr.attendance'].search(domain)
        if atts and len(atts) > 0:
            for att in atts:
                old_sheet_state = att.sheet_id.state if att.sheet_id else False
                if att.sheet_id:
                    att.sheet_id.state = 'draft'
                    
                att.recompute_calculations()

                if att.sheet_id:
                    att.sheet_id.state = old_sheet_state

                count += 1

        _logger.info("\n###########'%s' attendance calculated successfully.###########\n" %str(count))
        