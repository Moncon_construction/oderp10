# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class ResCompany(models.Model):
    _inherit = 'res.company'

    attendance_day_calculation_type = fields.Selection([
                        ('standard_calculation', 'Standard Calculation'),
                        ('total_attendance_calculation', 'Total Attendance Calculation')
                        ], string='Attendance Day Calculation Type', default='standard_calculation')
    min_hours_for_calculate_worked_hour = fields.Float(default=0.1, string='Minimum Hours for Calculate Worked Hour') # 10 мин