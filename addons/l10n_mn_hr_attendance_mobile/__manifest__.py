# -*- coding: utf-8 -*-

{
    'name': "Mongolian HR attendances Mobile",
    'version': '1.0',
    'depends': [
        'base',
        'base_geoengine',
        'base_geolocalize',
        'l10n_mn_hr_attendance_hour_balance'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/hr_employee_views.xml',
        'views/hr_attendance_views.xml',
        'views/hr_attendance_location.xml',
        'views/geo_attendance_view.xml',
        'report/attendance_location_templates.xml'
    ]
}
