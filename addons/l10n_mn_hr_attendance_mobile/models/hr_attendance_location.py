from odoo import models, fields, api, _
import logging
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.addons.l10n_mn_web.models.time_helper import *
from datetime import datetime, timedelta
from math import sin, cos, sqrt, atan2, radians
import pytz
import json
from pyproj import Proj, transform

_logger = logging.getLogger('odoo')

class HrAttendanceLocation(models.Model):
    _name = "hr.attendance.location"

    name = fields.Char('Name', required=True)
    lat = fields.Float('latitude', digits=(16, 5))
    lng = fields.Float('longitude', digits=(16, 5))
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id,
                                 required=True)
    location_qr = fields.Char('QR',  store=True, readonly=1)
    check_radius = fields.Selection([(5, '5 meter'),
                                     (10, '10 meter'),
                                     (50, '50 meter'),
                                     (100, '100 meter'),
                                     ], string='Calculation range', default=5, required=True)

    @api.model
    def create(self, vals):
        res = super(HrAttendanceLocation, self).create(vals)
        qr_data = '{\n'
        qr_data += '"id": ' + format(res.id) + ',\n'
        qr_data += '"company_id": ' + format(res.company_id.id)
        qr_data += '\n}'
        vals['location_qr'] = qr_data
        if 'geo_point' in vals.keys() and vals['geo_point']:
            lat, lon = self.geopoint_to_latlong(vals)
            vals['lat'] = lat
            vals['lng'] = lon
        super(HrAttendanceLocation, res).write(vals)
        return res

    @api.multi
    def write(self, vals):
        if 'geo_point' in vals.keys() and vals['geo_point']:
            lat, lon = self.geopoint_to_latlong(vals)
            vals['lat'] = lat
            vals['lng'] = lon
        res = super(HrAttendanceLocation, self).write(vals)
        return res

    def geopoint_to_latlong(self, vals):
        coordinates_dict = json.loads(vals['geo_point'])
        x = coordinates_dict['coordinates'][0]
        y = coordinates_dict['coordinates'][1]
        inProj = Proj(init='epsg:3857')
        outProj = Proj(init='epsg:4326')
        lon, lat = transform(inProj, outProj, x, y)
        return lat, lon

class HrEmployee(models.Model):
    _inherit = "hr.employee"

    imei_code = fields.Char('Mobile IMEI code', size=15)

    @api.multi
    def default_registered_employee_id(self):
        if self.env.user.employee_ids:
            employee = self.env.user.employee_ids[0]
            return {'id': employee.id, 'name': employee.name, 'display_name': employee.display_name}
        else:
            return False
class HrAttendance(models.Model):
    _inherit = "hr.attendance"

    in_location = fields.Many2one('hr.attendance.location', 'In location', readonly=1, ondelete='restrict')
    out_location = fields.Many2one('hr.attendance.location', 'Out location', readonly=1, ondelete='restrict')

    @api.multi
    def get_now_attendance_mobile(self, emp_id):
        if self.env.user.sudo().tz:
            user_time_zone = pytz.timezone(self.env.user.sudo().tz)
        else:
            return 'Please set your time zone!'.encode('utf8')
        utc = pytz.UTC

        employee = self.env['hr.employee'].browse(emp_id)

        from_dt = datetime.combine(get_day_like_display(datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                                                        self.env.user), datetime.min.time())
        to_dt = datetime.combine(get_day_like_display(datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                                                      self.env.user) + timedelta(days=1), datetime.min.time())

        from_dt_str = user_time_zone.localize(from_dt).astimezone(utc).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        to_dt_str = user_time_zone.localize(to_dt).astimezone(utc).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        attendance_objects = self.env['hr.attendance'].search(
            [('employee_id', '=', employee.id), ('check_in', '>=', from_dt_str), ('check_in', '<', to_dt_str)],
            order='check_in')
        if len(attendance_objects) > 0:
            dict = {'id': attendance_objects[0].id}
        else:
            dict = {'id': 0}
        return dict

    @api.multi
    def create_attendance_mobile(self, imei, attendance_id, location_id, lat, lng):

        employee_obj = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)], limit=1)
        location_obj = self.env['hr.attendance.location'].search([('id', '=', location_id)])
        distance = self.calculate_distance(location_obj, lat, lng)
        if (distance * 1000) < int(location_obj.check_radius):
            if not employee_obj.imei_code:
                return {'imei': False}
            if employee_obj.imei_code == imei:
                now_time = datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                if attendance_id == 0:
                    res = self.env['hr.attendance'].sudo().create({
                        'employee_id': employee_obj.id,
                        'check_in': now_time,
                        'check_out': (datetime.now() + timedelta(seconds=1)).strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                        'in_location': location_id,
                        'company_id': employee_obj.company_id.id,
                    })
                    res.recompute_calculations()

                    return {'success': True, 'location_name': location_obj.name, 'check_in': str(
                        get_day_like_display(datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                                                      self.env.user))}
                else:
                    attendance = self.env['hr.attendance'].search([('id', '=', attendance_id)])
                    if attendance:
                        attendance.write({
                            'check_out': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                            'out_location': location_id
                        })
                    attendance.recompute_calculations()

                    return {'success': True, 'location_name': location_obj.name, 'check_out': str(
                        get_day_like_display(datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT), self.env.user))}
            else:
                return {'imei': 'different'}
        else:
            return {'range': (distance * 1000)}
        return ''

    def calculate_distance(self, location_obj, lat, lng):

        R = 6373.0
        lat1 = radians(location_obj.lat)
        lng1 = radians(location_obj.lng)
        lat2 = radians(lat)
        lng2 = radians(lng)

        dlng = lng2 - lng1
        dlat = lat2 - lat1

        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlng / 2) ** 2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))

        return R * c

    def get_attendance_list_mobile(self, emp_id, syncDataLimit, local_attendance):
        """
            Утаснаас ирцийн мэдээлэл татах функц
        """
        if self.env.user.sudo().tz:
            user_time_zone = pytz.timezone(self.env.user.sudo().tz)
        else:
            return 'Please set your time zone!'.encode('utf8')
        utc = pytz.UTC
        insert_attendance = []
        employee = self.env['hr.employee'].browse(emp_id)
        time_format = '{0:02.0f}:{1:02.0f}'
        self.env.cr.execute("select name as calendar_name from resource_calendar ")
        calendars = self.env.cr.dictfetchall()
        calendar_name = ''
        if len(calendars) > 0:
            calendar_name = calendars[0]['calendar_name']

        from_dt = datetime.combine(
            get_day_like_display(datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT), self.env.user) - timedelta(
                days=syncDataLimit), datetime.min.time())
        from_dt_str = user_time_zone.localize(from_dt).astimezone(utc).strftime(DEFAULT_SERVER_DATETIME_FORMAT)

        attendance_objects = self.env['hr.attendance'].search(
            [('employee_id', '=', employee.id), ('check_in', '>=', from_dt_str)],
            order='check_in')

        for attendance in attendance_objects:
            insert_attendance.append({'id': attendance.id,
                                   'write_date': attendance.write_date,
                                   'employee_id': emp_id,
                                   'work_start_time': attendance.work_start_time,
                                   'work_end_time': attendance.work_end_time,
                                   'work_hour_of_date': attendance.work_hour_of_date,
                                   'check_in_str': str(get_day_like_display(attendance.check_in, self.env.user))[11:19],
                                   'check_out_str': str(get_day_like_display(attendance.check_out, self.env.user))[
                                                    11:19],
                                   'check_in': attendance.check_in,
                                   'check_out': attendance.check_out,
                                   'total_attendance_hours': attendance.total_attendance_hours,
                                   'total_worked_hours_of_date': attendance.total_worked_hours_of_date or '',
                                   'minutes_before_work_hour': attendance.minutes_before_work_hour,
                                   'total_lag_minutes': attendance.total_lag_minutes,
                                   'early_leave_minutes': attendance.early_leave_minutes,
                                   'total_attendance_hours_str': time_format.format(
                                       *divmod(float(attendance.total_attendance_hours) * 60, 60)),
                                   'total_worked_hours_of_date_str': time_format.format(
                                       *divmod(float(attendance.total_worked_hours_of_date or '0') * 60, 60)),
                                   'minutes_before_work_hour_str': time_format.format(
                                       *divmod(float(attendance.minutes_before_work_hour) * 60, 60)),
                                   'total_lag_minutes_str': time_format.format(
                                       *divmod(float(attendance.total_lag_minutes) * 60, 60)),
                                   'early_leave_minutes_str': time_format.format(
                                       *divmod(float(attendance.early_leave_minutes) * 60, 60)),
                                   'calendar_name': calendar_name,
                                   'employee_name': employee.name})
        dict = {'insert': insert_attendance}
        return dict
