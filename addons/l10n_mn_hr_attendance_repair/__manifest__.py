# -*- coding: utf-8 -*-
# Part of OdErp10. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Hr Attendance Repair",
    'version': '10.0.1.0',
    'depends': [
        'hr_attendance',
        'l10n_mn_workflow_config'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
         Human resource attendance repair
    """,
    'data': [
        'security/hr_attendance_repair_security.xml',
        'security/ir.model.access.csv',
        'views/hr_attendance_repair_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
