# -*- coding: utf-8 -*-

from datetime import datetime
from dateutil.relativedelta import relativedelta
import pytz

from odoo import _, api, fields, models, exceptions
from odoo.exceptions import UserError
from odoo.http import request
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.addons.l10n_mn_web.models.time_helper import *


class HrAttendanceRepair(models.Model):
    _name = 'hr.attendance.repair'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    STATE_SELECTION = [
        ('draft', 'New'),
        ('send', 'Sent request'),
        ('approved', 'Approved'),
        ('canceled', 'Request canceled'),
        ('accepted', 'Registered'),
    ]

    def _default_employee(self):
        employee = self.env['hr.employee'].search([('user_id', '=', self._uid)])
        if len(employee) > 1:
            employee = employee[0]
        return employee.id

    def name_get(self):
        res = []
        for record in self:
            name = '%s, [%s]' % (record.employee_id.name, record.datetime)
            res.append((record.id, name))
        return res

    state = fields.Selection(STATE_SELECTION, default='draft', help="Status of the attendance repair")
    employee_id = fields.Many2one('hr.employee', 'Employee', default=_default_employee, required=True)
    datetime = fields.Date(string="Date", default=fields.Datetime.now, required=True)
    job_id = fields.Many2one('hr.job', 'Job', readonly=1)
    department_id = fields.Many2one('hr.department', 'Department', readonly=1)
    company_id = fields.Many2one('res.company', 'Company', readonly=True)
    line = fields.One2many('hr.attendance.repair.line', 'attendance_repair', 'Lines')

    show_approve_button = fields.Boolean(string='Show Approve Button?', compute='_show_approve_button')
    check_sequence = fields.Integer(string='Workflow Step', copy=False, default=0)
    check_users = fields.Many2many('res.users', 'hr_attendance_repair_check_user_rel', 'attendance_repair_id', 'user_id', 'Checkers', readonly=True, copy=False)
    workflow_id = fields.Many2one('workflow.config', 'Workflow', domain=[('model_id.model', '=', 'hr.attendance.repair')], copy=False)
    workflow_history_ids = fields.One2many(
        'workflow.history', 'attendance_repair_id', string='History', readonly=True)

    @api.onchange('employee_id')
    def onchange_employee(self):
        self.job_id = self.employee_id.job_id
        self.department_id = self.employee_id.department_id
        self.company_id = self.employee_id.company_id

    @api.multi
    def _show_approve_button(self):
        res = {}
        history_obj = self.env['workflow.history']
        for attendance_repair in self:
            history = history_obj.search([('attendance_repair_id', '=', attendance_repair.id), (
                'line_sequence', '=', attendance_repair.check_sequence)], limit=1, order='sent_date DESC')
            if history:
                attendance_repair.show_approve_button = (
                    attendance_repair.state == 'send' and self.env.user.id in map(lambda x: x.id, history.user_ids))
            else:
                attendance_repair.show_approve_button = False
        return res

    @api.model
    def create(self, vals):
        creation = super(HrAttendanceRepair, self).create(vals)
        creation.job_id = creation.employee_id.job_id
        creation.department_id = creation.employee_id.department_id
        creation.company_id = creation.employee_id.company_id
        employee = self.env['hr.employee'].search([('id', '=', vals['employee_id'])])
        workflow_id = self.env['workflow.config'].get_workflow('employee', 'hr.attendance.repair', employee.id, None)
        workflow = None
        if workflow_id:
            workflow = workflow_id
        if not workflow:
            raise UserError(_('There is no workflow defined!'))
        creation.workflow_id = workflow
        self.write({'state': 'draft'})
        return creation

    @api.multi
    def write(self, values):
        for line in self:
            if values.get('employee_id'):
                emp = self.env['hr.employee'].search([('id', '=', values.get('employee_id'))])
                if emp:
                    for emp_id in emp:
                        values.update({'job_id': emp_id.job_id.id,
                                       'department_id': emp_id.department_id.id,
                                       'company_id': emp_id.company_id.id})
            return super(HrAttendanceRepair, line).write(values)

    @api.multi
    def send(self):
        for object in self:
            if object.workflow_id:
                workflow_obj = self.env['workflow.config']
                success, current_sequence = workflow_obj.send('workflow.history', 'attendance_repair_id', object, self.create_uid.id)
                is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(object.workflow_id.id, object, self.env.user.id, object.check_sequence + 1, 'next')
                if is_next:
                    object.check_users = [(6, 0, next_user_ids)]
                if success:
                    object.check_sequence = current_sequence
                    object.ensure_one()
                    self.write({'state': 'send'})
                    manager_id = self.employee_id.parent_id
                    if manager_id:
                        object._send_mail(manager_id, object)
            else:
                raise UserError(_("You Don't configure a Workflow for Attendance Repair!"))

    @api.multi
    def approve(self):
        user = self.env['res.users'].search([('company_id', '=', self.env.user.company_id.id)])
        for object in self:
            for u in user:
                if u.has_group('hr.group_hr_manager'):
                    manager_id = self.env['hr.employee'].search([('user_id', '=', u.id)])
                    for manager in manager_id:
                        object._send_mail(manager, object)

            object.write({'state': 'approved'})

    @api.multi
    def cancel(self):
        for object in self:
            for line in object.line:
                if line.type_of_attendance == 'in':
                    attendance_ids = self.env['hr.attendance'].search([('employee_id', '=', object.employee_id.id), ('check_in', '=', line.date_of_attendance)])
                    for attendance_id in attendance_ids.filtered(lambda x: x.check_out):
                        attendance_id.write({'check_in': datetime.strptime(attendance_id.check_out, DEFAULT_SERVER_DATETIME_FORMAT) + relativedelta(seconds=-1)})
                    attendance_ids.filtered(lambda x: not x.check_out).unlink()
                else:
                    attendance_ids = self.env['hr.attendance'].search([('employee_id', '=', object.employee_id.id), ('check_out', '=', line.date_of_attendance)])
                    for attendance_id in attendance_ids.filtered(lambda x: x.check_in):
                        if attendance_id.check_in:
                            attendance_id.write({'check_out': datetime.strptime(attendance_id.check_in, DEFAULT_SERVER_DATETIME_FORMAT) + relativedelta(seconds=1)})
                    attendance_ids.filtered(lambda x: not x.check_in).unlink()
            object.write({'state': 'canceled'})
            object._send_mail(object.employee_id, object)

    @api.multi
    def accept(self):
        for obj in self:
            for line in obj.line:
                check = False
                attend_id = ''
                attendance = obj.env['hr.attendance'].search([('employee_id', '=', obj.employee_id.id)])
                if attendance:
                    for att in attendance:
                        # Шинэ зүйл эхлэх гэж байна
                        get_date_in = datetime.strptime(att.check_in, "%Y-%m-%d %H:%M:%S").date() if att.check_in else ''
                        get_date_out = datetime.strptime(att.check_out, "%Y-%m-%d %H:%M:%S").date() if att.check_out else ''
                        fix_date = datetime.strptime(line.date_of_attendance, "%Y-%m-%d %H:%M:%S").date()
                        if get_date_in == fix_date or get_date_out == fix_date:
                            check = True
                            attend_id = att
                if check:
                    if line.type_of_attendance == 'in':
                        attend_id.write({'check_in': line.date_of_attendance})
                        obj.write({'state': 'accepted'})
                        obj._send_mail(obj.employee_id, obj)
                    else:
                        attend_id.write({'check_out': line.date_of_attendance})
                        obj.write({'state': 'accepted'})
                        obj._send_mail(obj.employee_id, obj)
                else:
                    if line.type_of_attendance == 'in':
                        self.env['hr.attendance'].create({'employee_id': obj.employee_id.id, 'check_in': line.date_of_attendance, 'is_attendance_repair': True })
                        obj.write({'state': 'accepted'})
                        obj._send_mail(obj.employee_id, obj)
                    else:
                        self.env['hr.attendance'].create({'employee_id': obj.employee_id.id, 'check_out': line.date_of_attendance, 'is_attendance_repair': True })
                        obj.write({'state': 'accepted'})
                        obj._send_mail(obj.employee_id, obj)
            if self.workflow_id:
                success, sub_success, current_sequence = self.env['workflow.config'].approve('workflow.history', 'attendance_repair_id', self,
                                                                                             self.env.user.id)

    def _send_mail(self, user, object):
        emails = []
        manager_id = user
        email = manager_id.work_email
        # TODO Илгээгдсэн и-мейлүүдийг шалгаж татахаар хийнэ
        mail_send = []
        object_name = str('hr.attendance.repair')
        db_name = request.session['db']
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        outgoing_email = self.env['ir.mail_server'].sudo().search([])
        current_url = unicode(base_url) + '/web?db=' + unicode(db_name) + '#id=' + str(
            object.id) + '&view_type=form&model=' + object_name
        if not outgoing_email:
            raise UserError(_(
                'There is no configuration for outgoing mail server. Please contact system administrator.'))
        else:
            if manager_id.user_id not in mail_send:
                outgoing_email = self.env['ir.mail_server'].sudo().search(
                    [('id', '=', outgoing_email[0].id)])
                vals = {
                    'state': 'outgoing',
                    'subject': 'Ирц нөхөн засах хүсэлт',
                    'body_html': '<p>Сайн байна уу, <br/></p>'
                                 '<p> ' + str(
                                     manager_id.name) + ' таньд ирц нөхөн засах талаар имэйл ирсэн байна' + '  <br/></p>' + '<b><a href="' + str(current_url) + '" target="_blank">Холбоос</a></b>' + '<p>Баярлалаа,<br/> -- <br/>ERP Автомат Имэйл </p>',
                    'email_to': email,
                    'email_from': outgoing_email.smtp_user
                }
                emails.append(self.env['mail.mail'].create(vals))

        if emails:
            for email in emails:
                email.send()


class HrAttendanceRepairLine(models.Model):
    _name = 'hr.attendance.repair.line'

    attendance_repair = fields.Many2one('hr.attendance.repair', 'Attendance Repair', invisible=True)
    date_of_attendance = fields.Datetime('Date of attendance', default=fields.Datetime.now, required=True)
    type_of_attendance = fields.Selection([('in', 'In'), ('out', 'Out')], 'Type of Attendance', default='in', required=True)
    reason_of_attendance = fields.Many2one('hr.attendance.repair.reason', 'Reason', required=True)
    comment = fields.Text('Comment', required=True)
    check_in_attendance_tmp = fields.Char('Check in attendance', readonly=True)
    check_out_attendance_tmp = fields.Char('Check out attendance', readonly=True)
    check_in_attendance = fields.Char('Check in attendance')
    check_out_attendance = fields.Char('Check out attendance')

    @api.onchange('type_of_attendance')
    def _onchange_type_of_attendance(self):
        if not self.attendance_repair.employee_id.calendar_id:
            raise UserError(_('%s has no calendar!') % self.attendance_repair.employee_id.display_name)
        else:
            if self.type_of_attendance == 'in':
                time = self.attendance_repair.employee_id.calendar_id.attendance_ids[0].hour_from
                for att in self.attendance_repair.employee_id.calendar_id.attendance_ids:
                    time = (att.hour_from if time > att.hour_from else time)
                self.date_of_attendance = pytz.timezone('Asia/Ulaanbaatar').localize(datetime.strptime(self.date_of_attendance, DEFAULT_SERVER_DATETIME_FORMAT).replace(second=0, hour=int(time), minute=int((time * 100) % 100)), is_dst=None).astimezone(pytz.UTC)
            if self.type_of_attendance == 'out':
                time = self.attendance_repair.employee_id.calendar_id.attendance_ids[0].hour_to
                for att in self.attendance_repair.employee_id.calendar_id.attendance_ids:
                    time = (att.hour_to if time < att.hour_to else time)
                self.date_of_attendance = pytz.timezone('Asia/Ulaanbaatar').localize(datetime.strptime(self.date_of_attendance, DEFAULT_SERVER_DATETIME_FORMAT).replace(second=0, hour=int(time), minute=int((time * 100) % 100)), is_dst=None).astimezone(pytz.UTC)

    @api.onchange('date_of_attendance')
    def _onchange_date_of_attendance(self):
        if self.date_of_attendance:
            tz = get_user_timezone(self.env.user)
            date_time_obj = pytz.utc.localize(datetime.strptime(self.date_of_attendance, '%Y-%m-%d %H:%M:%S')).astimezone(tz)
            dt = str(date_time_obj.date())
            attendance = self.env['hr.attendance'].search([('employee_id','=',self.attendance_repair.employee_id.id)])
            if attendance:
                check_in = False
                check_out = False
                for at in attendance:
                    if at.check_in:
                        check_in_date = str(pytz.utc.localize(datetime.strptime(at.check_in, '%Y-%m-%d %H:%M:%S')).astimezone(tz).date())
                        if check_in_date == dt:
                            check_in = str(pytz.utc.localize(datetime.strptime(at.check_in, '%Y-%m-%d %H:%M:%S')).astimezone(tz).strftime('%Y-%m-%d %H:%M:%S'))
                    if at.check_out:
                        check_out_date = str(
                            pytz.utc.localize(datetime.strptime(at.check_out, '%Y-%m-%d %H:%M:%S')).astimezone(tz).date())
                        if check_out_date == dt:
                            check_out = str(pytz.utc.localize(datetime.strptime(at.check_out, '%Y-%m-%d %H:%M:%S')).astimezone(tz).strftime('%Y-%m-%d %H:%M:%S'))
                self.check_in_attendance_tmp = self.check_in_attendance = str(check_in)
                if check_in:
                    self.check_in_attendance_tmp = self.check_in_attendance = str(check_in)
                else:
                    self.check_in_attendance_tmp = self.check_in_attendance = _('No attendance')

                if check_out:
                    self.check_out_attendance_tmp = self.check_out_attendance = str(check_out)
                else:
                    self.check_out_attendance_tmp = self.check_out_attendance = _('No attendance')

    @api.model
    def create(self, vals):
        if 'check_in_attendance' in vals and vals.get('check_in_attendance'):
            vals.update({'check_in_attendance_tmp': vals.get('check_in_attendance')})
        if 'check_out_attendance' in vals and vals.get('check_out_attendance'):
            vals.update({'check_out_attendance_tmp': vals.get('check_out_attendance')})
        return super(HrAttendanceRepairLine, self).create(vals)

    @api.multi
    def write(self, vals):
        if 'check_in_attendance' in vals and vals.get('check_in_attendance'):
            vals.update({'check_in_attendance_tmp': vals.get('check_in_attendance')})
        if 'check_out_attendance' in vals and vals.get('check_out_attendance'):
            vals.update({'check_out_attendance_tmp': vals.get('check_out_attendance')})
        return super(HrAttendanceRepairLine, self).write(vals)


class HrAttendanceRepairReason(models.Model):
    _name = 'hr.attendance.repair.reason'

    name = fields.Text('Reason')


class WorkflowHistory(models.Model):
    _inherit = 'workflow.history'

    attendance_repair_id = fields.Many2one('hr.attendance.repair', 'Attendance repair')
