# -*- coding: utf-8 -*-
# Part of OdErp10. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Hr Benefit",
    'version': '10.0.1.0',
    'depends': ['l10n_mn_hr','l10n_mn_hr_regulation'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
         Human resource benefit
    """,
    'data': [
        'security/ir.model.access.csv',
        'security/ir_rule.xml',
        'views/hr_benefit_views.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
