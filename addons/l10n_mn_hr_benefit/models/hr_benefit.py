# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/,
# http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
import logging

from odoo import api, fields, models,  _
from odoo.exceptions import UserError ,ValidationError
from datetime import date, datetime, timedelta
import time

_logger = logging.getLogger('odoo')


class hr_benefit_type (models.Model):
    _inherit = 'hr.benefit.type'

    is_limited = fields.Boolean('Is limited' ,default=False)


class hr_benefit_distribution (models.Model):
    _name = 'hr.benefit.distribution'

    start_date = fields.Date(string='Date started')
    end_date = fields.Date(string='Date ended')
    name = fields.Char(string='Name', size=128, required=True,)
    type_id = fields.Many2one('hr.benefit.type', string='Benefit Type', required=True)
    date = fields.Date(string='Date',default=fields.Date.context_today, required=True)
    amount = fields.Integer(string='Amount', required=True)
    employee_id = fields.Many2one('hr.employee', string='Employee' ,required=True)
    regulation_id = fields.Many2one('hr.regulation','Regulation name')
    is_limited_id = fields.Boolean(related='type_id.is_limited', invisible=True)
    benefit_id = fields.Many2one('hr.benefit', string='Hr benefit')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirm', 'Confirmed')],
        string='State', readonly=True, default="draft")

    def action_to_confirm(self):
        return self.write({'state': 'confirm'})

    @api.multi
    @api.depends('name')
    def name_get(self):
        result = []
        for benefit_distribution in self:
            benefits = self.env['hr.benefit'].search([('type_id', '=', benefit_distribution.type_id.id),('employee_id', '=', benefit_distribution.employee_id.id)])
            amount = sum([obj.amount for obj in benefits])
            name = (benefit_distribution.type_id.name or '') + " " + ' - ' + str(benefit_distribution.amount - amount)
            total = (str(benefit_distribution.amount))
            result.append((benefit_distribution.id, '(%s үлдэж буй , %s нийт)' % (name, total)))
        return result




class hr_benefit (models.Model):
    _inherit = 'hr.benefit'

    hr_benefit_distribution_id = fields.Many2one('hr.benefit.distribution', string='Benefit distribution' ,required=True)

    @api.model
    def create(self, vals):
        if 'hr_benefit_distribution_id' and 'amount' in vals.keys():
            benefits = self.env['hr.benefit'].search([('type_id', '=', vals['type_id'])])
            amount = sum([obj.amount for obj in benefits])
            distribution = self.env['hr.benefit.distribution'].browse(vals['hr_benefit_distribution_id'])
            if distribution and distribution.amount - amount - vals['amount'] < 0:
                raise UserError(_(u"The amount of your registered benefit exceeds the amount allocated."))
            if distribution.end_date < vals['date']:
                raise UserError(
                    _(u"Please check your date of expiration if you do not have a valid allowance!!."))
            if distribution.start_date > vals['date']:
                raise UserError(
                    _(u"Please check your date of expiration if you do not have a valid allowance!!."))
            if distribution.state == 'draft':
                raise UserError(
                    _(u"Please check your benefit distribution state !!."))
        ins = super(hr_benefit, self).create(vals)
        return ins


    @api.multi
    def write(self, vals):
        for obj in self:
            benefits = self.env['hr.benefit'].search([('type_id', '=', obj.type_id.id)])
            amount = sum([obj.amount for obj in benefits])
            if 'amount' in vals.keys() and obj.hr_benefit_distribution_id.amount - vals['amount'] < 0:
                raise UserError(_(u"The amount of your registered benefit exceeds the amount allocated."))
            if 'date' in vals.keys() and obj.hr_benefit_distribution_id.end_date < vals['date']:
                raise UserError(
                    _(u"Please check your date of expiration if you do not have a valid allowance!!."))
            if 'date' in vals.keys() and obj.hr_benefit_distribution_id.start_date > vals['date']:
                raise UserError(
                    _(u"Please check your date of expiration if you do not have a valid allowance!!."))
        ins = super(hr_benefit, self).write(vals)
        return ins


    @api.depends('employee_id')
    @api.onchange('employee_id')
    def onchange_distribution(self):
        for benefit in self:
            distributions = self.env['hr.benefit.distribution'].search([('employee_id', '=', benefit.employee_id.id)])
            if distributions and len(distributions) > 0:
                benefit.hr_benefit_distribution_id = distributions[0].id
                benefit.type_id = distributions[0].type_id.id
                benefit.regulation_id = distributions[0].regulation_id.id
            else:
                benefit.hr_benefit_distribution_id = False
                benefit.type_id = False
                benefit.regulation_id = False

    @api.depends('hr_benefit_distribution_id')
    @api.onchange('hr_benefit_distribution_id')
    def onchange_account_transaction(self):
        for benefit in self:
            distributions = self.env['hr.benefit.distribution'].search([('id', '=', benefit.hr_benefit_distribution_id.id)])
            if distributions and len(distributions) > 0:
                benefit.type_id = distributions[0].type_id.id

