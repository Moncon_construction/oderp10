# -*- coding: utf-8 -*-
{
    'name': "Mongolian HR Business Trip",
    'version': '1.0',
    'depends': ['l10n_mn_hr_hour_balance_timesheet',
                'l10n_mn_cash_expense'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Human Resources Modules',
    'description': """
       Human Resource Additional Features Business Trip
    """,
    'data': [
        'security/ir.model.access.csv',
        'security/business_trip_security.xml',
        'views/report_business_trip.xml',
        'views/business_trip_view.xml',
        'views/business_trip_template.xml',
        'views/business_trip_timesheet_view.xml',
        'views/hr_employee_views.xml',
        'views/cash_expense_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
