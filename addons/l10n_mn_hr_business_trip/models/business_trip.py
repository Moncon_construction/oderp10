# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError
from datetime import datetime, date

class BusinessTripTeamLine(models.Model):
    _name = "business.trip.team.line"
    _description = "Business Trip Team"

    trip_id = fields.Many2one('business.trip', string = "Business Trip")
    employee_id = fields.Many2one('hr.employee', string="Employee")
    job_id = fields.Many2one('hr.job', string="Job")
    
    @api.onchange('employee_id')
    def onchange_employee(self):
        if self.employee_id.job_id:
            self.job_id = self.employee_id.job_id
  
  
class BusinessTripCost(models.Model):
    _name = "business.trip.costs"
    _description = "Business Trip Cost"
 
    trip = fields.Many2one('business.trip')
    cost_item = fields.Many2one('business.trip.cost.item','Cost item')
    is_cash_expense = fields.Boolean('Is Cash Expense', default=True)
    measuring_unit = fields.Many2one('product.uom',string = 'Measuring unit')
    unit_price = fields.Float(related ="cost_item.budjed" ,string='Unit price', readonly = 1)
    quantity = fields.Integer(string = 'Quantity')
    sub_total = fields.Float(string = 'Sub total', compute ="_compute_sub_total", store = True)
    description = fields.Char(string = 'Description')
     
    @api.depends('unit_price', 'quantity')
    def _compute_sub_total(self):
        for obj in self:
            obj.sub_total = obj.unit_price*obj.quantity

class BusinessTripCostItem(models.Model):
    _name = "business.trip.cost.item"
    _description = "Business Trip Cost Things"
     
    budjed = fields.Float(string ='Budjed',required = True)
    name = fields.Char(string ='Name', required = True) 

class BusinessTrip(models.Model):
    _name = "business.trip"
    _inherit = "mail.thread" 
    _description = "Business Trip"

    def _default_employee(self):
        return self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)

    @api.depends('trip_costs')
    def _compute_total_price(self):
        for obj in self:
            obj.total_price = 0
            for cost in obj.trip_costs:
                obj.total_price += cost.sub_total

    @api.multi
    def _show_approve_button(self):
        res = {}
        history_obj = self.env['workflow.history']
        for trip in self:
            history = history_obj.search([('business_trip_id', '=', trip.id), (
                'line_sequence', '=', trip.check_sequence)], limit=1, order='sent_date DESC')
            if history:
                trip.show_approve_button = (
                    trip.state == 'send' and self.env.user.id in map(lambda x: x.id, history.user_ids))
            else:
                trip.show_approve_button = False
        return res
    
    @api.multi
    def _cash_expense_count(self):
        for trip in self:
            trip.cash_expense_count = len(self.env['hr.expense.cash'].sudo().search([('business_trip_id','=',trip.id)])) or 0

    @api.multi
    def _task_count(self):
        for trip in self:
            trip.task_count = len(trip.task_ids)

    cash_expense_count = fields.Integer(string='Cash Expense Count', compute=_cash_expense_count)
    task_count = fields.Integer(string='Task Count', compute=_task_count)
    name = fields.Char(string="Trip name", index=True)
    employee_id = fields.Many2one('hr.employee', string="Employee", default=_default_employee)
    trip_type = fields.Selection([
        ('internal', 'Internal'),
        ('external', 'External'), ], string="Trip type", track_visibility="onchange", default="internal")
    start_date = fields.Datetime(string="Start date")
    end_date = fields.Datetime(string="End date")
    total_days = fields.Float(string="Total days", store=True)
    total_nights = fields.Float(string="Total nights", store=True)
    location_from = fields.Many2one('res.country.state', string="Location from")
    location_to = fields.Many2one('res.country.state', string="Location to")
    trip_team_line = fields.One2many('business.trip.team.line', 'trip_id', string="Trip Team Lines", copy=True)
    purpose = fields.Char(string="Purpose")
    reason = fields.Text(string="Reason")
    state = fields.Selection([
        ('draft', 'Draft'),
        ('send', 'Send'),
        ('confirm', 'Confirmed'), 
        ('cancel','Canceled'),
        ('done','Done'),],
        string='State', track_visibility='onchange', readonly=True, default="draft")
    role = fields.Text(string='Role')
    result = fields.Text(string='Result')
    other_notes = fields.Text(string='Other Notes')
    trip_costs = fields.One2many('business.trip.costs','trip')
    total_price = fields.Float(store=True, compute='_compute_total_price')
    
#     workflow fields
    workflow_id = fields.Many2one('workflow.config', 'Workflow', domain=[('model_id.model','=','business.trip')], copy=False)
    check_sequence = fields.Integer(string='Workflow Step', default=0)
    check_users = fields.Many2many('res.users', 'business_trip_check_user_rel', 'business_trip_id', 'user_id', 'Checkers', readonly=True, copy=False)
    show_approve_button = fields.Boolean(string='Show Approve Button?', compute='_show_approve_button')
    step_is_final = fields.Boolean(string='Final Step', default=False)
    task_ids = fields.Many2many('project.task','project_task_business_trip_rel', 'business_trip_id', 'task_id', string='Tasks')

    @api.multi
    def unlink(self):
        for invoice in self:
            if invoice.state not in ('draft', 'cancel'):
                raise UserError(
                    _('You cannot delete an trip which is not draft or cancelled.'))
        return super(BusinessTrip, self).unlink()

    @api.multi
    def action_view_cash_expense(self):
        action = self.env.ref('l10n_mn_cash_expense.action_hr_expense_cash_my_all')
        result = action.read()[0]
        result['context'] = {'default_business_trip_id': self.id}
        return result
    
    @api.multi
    def action_view_tasks(self):
        action = self.env.ref('project.action_view_task')
        result = action.read()[0]
        result['domain'] = [('id','in',self.task_ids.ids)]
        return result

    @api.multi
    def action_sent(self):
        for obj in self:
            if obj.workflow_id:
                workflow_obj = self.env['workflow.config']
                success, current_sequence = workflow_obj.send('workflow.history', 'business_trip_id', obj, self.env.user.id)
                is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(obj.workflow_id.id, obj, self.env.user.id, obj.check_sequence + 1, 'next')
                if is_next:
                    obj.check_users = [(6, 0, next_user_ids)]
                if success:
                    obj.check_sequence = current_sequence
                    obj.ensure_one()
                    obj.state = 'send'
        return True

    @api.multi
    def action_confirm(self):
        self.ensure_one()
        workflow_obj = self.env['workflow.config']
        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve(
                'workflow.history', 'business_trip_id', self, self.env.user.id)
            is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(self.workflow_id.id, self, self.env.user.id, self.check_sequence + 1, 'next')
            if is_next:
                self.check_users = [(6, 0, next_user_ids)]
            if success:
                if sub_success:
                    self.create_cash_expense(self)
                    self.state = 'confirm'
                    self.step_is_final = True
                else:
                    self.check_sequence = current_sequence
                    self.show_approve_button = True
            return True

    @api.multi
    def action_cancel(self):
        self.state = 'cancel'

    def action_done(self):
        if not self.result:
            raise UserError(_("The result can not be empty."))
        if self.create_uid != self.env.user:
            raise UserError(_('Only created user can do this action.\n Created user: %s') % self.create_uid.name)
        return self.write({'state': 'done'})

    @api.multi
    def do_draft(self):
        for obj in self:
            obj.state = 'draft'
            if obj.workflow_history_ids:
                obj.workflow_history_ids.unlink()
                obj.check_sequence = 0
    
    @api.multi
    def calculate_diff_days(self, start, end):
        different_days = 0.00
        if (datetime.strptime(end, '%Y-%m-%d %H:%M:%S') < datetime.strptime(start, '%Y-%m-%d %H:%M:%S')):
                raise ValidationError(_('Incorrect date of start and date of date you selected again !!!'))
        different_days = abs((datetime.strptime(end, '%Y-%m-%d %H:%M:%S') - datetime.strptime(start, '%Y-%m-%d %H:%M:%S')).days)
        return different_days

    @api.multi
    def compute_date(self, values):
        start = end = ''
        if 'start_date' in values and 'end_date' in values:
            start = values['start_date']
            end = values['end_date']
        elif 'start_date' in values:
            start = values['start_date']
            end = self.end_date
        elif 'end_date' in values:
            start = self.start_date
            end = values['end_date']
        else: 
            return values
        days = self.calculate_diff_days(start, end)
        values['total_days'] = days + 1
        values['total_nights'] = days
        return values

    @api.multi
    def write(self,values):
        values = self.compute_date(values)
        return super(BusinessTrip, self).write(values)

    def create_cash_expense(self, trip):
        vals = {'name': trip.name,
                'workflow_id': self.env['workflow.config'].get_workflow('department', 'hr.expense.cash', employee_id=None, department_id=trip.employee_id.department_id.id),
                'department_id': trip.employee_id.department_id.id,
                'business_trip_id': trip.id,
                'company_id':trip.employee_id.company_id.id,
                'employee_id': trip.employee_id.id
                }
        cash_expense_id = self.env['hr.expense.cash'].create(vals)
        for line in trip.trip_costs:
            if line.is_cash_expense:
                vals = {
                    'expense_id': cash_expense_id.id,
                    'name': line.cost_item.name,
                    'qty': line.quantity,
                    'uom_id':line.measuring_unit.id,
                    'unit_amount': line.unit_price,
                    }
                cash_expense_line_id = self.env['hr.expense.cash.line'].create(vals)

    @api.model
    def create(self, vals):
        if 'employee_id' in vals.keys():
            employee = self.env['hr.employee'].browse(vals['employee_id'])
            workflow_id = self.env['workflow.config'].get_workflow('department', 'business.trip', None, employee.department_id.id)
            if not workflow_id:
                raise ValidationError(_("There is no workflow defined at '%s' department.") % employee.department_id.name or "")
            vals['workflow_id'] = workflow_id
        values = self.compute_date(vals)
        
        busstrip_id = super(BusinessTrip,self).create(vals)
        return busstrip_id

    @api.multi
    def copy(self, default=None):
        if not default:
            default = {}
        default.update({
            'state': 'draft',
            'name': '',
            'check_sequence': 0,
            'step_is_final': False
        })
        return super(BusinessTrip, self).copy(default)
    
class MailComposer(models.TransientModel):
    _inherit = 'mail.compose.message'
    
    @api.multi
    def send_mail_action(self):
        if self._context.get('active_model') == 'business.trip':
            self.env['business.trip'].search([('id','=',self._context.get('active_id'))]).write({'state': self._context.get('status')})      
        return super(MailComposer, self).send_mail_action()
    
class WorkflowHistory(models.Model):
    _inherit = 'workflow.history'
    
    business_trip_id = fields.Many2one('business.trip', 'Business trip')

class InheritHrExpenseCash(models.Model):
    _inherit = "business.trip"

    workflow_history_ids = fields.One2many(
        'workflow.history', 'business_trip_id', string='History', readonly=True)
    
class HrExpenseCash(models.Model):
    _inherit = "hr.expense.cash"
    
    business_trip_id = fields.Many2one('business.trip', 'Business trip')
    