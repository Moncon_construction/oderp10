# -*- coding: utf-8 -*-
from odoo import api, fields, models
from datetime import datetime
from dateutil import rrule
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import pytz
from dateutil import rrule
from dateutil.relativedelta import relativedelta

class BusinessTripBalanceLine(models.Model):
    _name = 'business.trip.balance.line'
    _description = "Time balance line of Business trip"
    
    @api.multi
    def default_date(self, date):
        for line in self:
            if line.trip_id:
                if date == 'start_date':
                    return line.trip_id.start_date
                elif date == 'end_date':
                    return line.trip_id.end_date
                return false
    
    trip_id = fields.Many2one('business.trip',ondelete='cascade',string ='Business Trip')
    employee_id = fields.Many2one('hr.employee',ondelete='restrict',string ='Employee')
    start_date = fields.Date('Start date', default=lambda self: self.default_date('start_date'))
    end_date = fields.Date('End date', default=lambda self: self.default_date('end_date'))

class BusinessTrip(models.Model):
    _inherit = 'business.trip'

    balance_lines = fields.One2many('business.trip.balance.line','trip_id','Balance Lines')

class HRTimesheetSheet(models.Model):
    _inherit = "hr_timesheet_sheet.sheet"

    @api.multi
    def _calc_bustrip(self):
#         Цагийн хуудсанд нийт томилолтын цаг талбар оруулж, тухайн талбарын цагийг ажилтны гэрээний дагуу бодож олов
        res = {}
        calendar_obj=self.env.get('resource.calendar')
        hours = 0
        business_trip_lines = self.env['business.trip.balance.line'].search(['&',('employee_id','=',self.employee_id.id),'|','&',('start_date','<=',self.date_to),('start_date','>=',self.date_from),
                                                                            '&',
                                                ('end_date','<=',self.date_to),('end_date','>=',self.date_from)])
        employee = self.employee_id
        for line in business_trip_lines:
            if employee.contract_id  and employee.contract_id.working_hours:
                calendar = employee.contract_id.working_hours
                start_dt  = datetime.strptime(self.date_from if line.start_date <= self.date_from else line.start_date, '%Y-%m-%d' )
                end_dt  = datetime.strptime( self.date_to if line.end_date >= self.date_to else line.end_date, '%Y-%m-%d' )
                leaves = calendar.get_leave_intervals(resource_id=calendar.id, start_datetime=start_dt, end_datetime=end_dt)
                for day in rrule.rrule(rrule.DAILY, dtstart=start_dt,
                                       until=(end_dt).replace(hour=0, minute=0, second=0),
                                       byweekday=calendar.get_weekdays()):
                    day_start_dt = day.replace(hour=0, minute=0, second=0)
                    day_end_dt = day.replace(hour=23, minute=59, second=59)
                    hours += calendar.get_working_hours_of_date(start_dt=day_start_dt, end_dt=day_end_dt,
                          compute_leaves=True, resource_id= calendar.id,
                          default_interval=None)
        self.total_bustrip_hours  = hours 
        
    total_bustrip_hours  = fields.Integer(compute='_calc_bustrip',  string='Total Business Trip Hour') 
    
        
