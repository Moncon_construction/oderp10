import logging
 
from odoo import api, fields, models
from cookielib import domain_match
 
_logger = logging.getLogger(__name__)
 
 
class HrConfiguration(models.TransientModel):
    _inherit = 'hr.recruitment.config.settings'
    
    group_approve_budget = fields.Selection([
        (0, 'Set the not approve budget'),
        (1, 'Set the approve budget')
        ], "Budget", implied_group='l10n_mn_hr_business_trip.group_approve_budget')
          
    group_business_trip = fields.Selection([
        (0, 'Set the not approve business trip'),
        (1, 'Set the approve business trip')
        ], "Trip", implied_group='l10n_mn_hr_business_trip.group_business_trip')
    

                
    
