# -*- coding: utf-8 -*-

import logging
from odoo import api, fields, models, _

class HrEmployee(models.Model):
    _inherit = "hr.employee"

    @api.multi
    def stat_button_trip(self):
        #   ухаалаг даруул дээр дарахад дуудагдах функц
        trip = []
        for line in self:
            res = self.env['business.trip.team.line'].search([('employee_id', '=', line.id)])
            for line in res:
                trip.append(line.trip_id.id)
            return {
                'type': 'ir.actions.act_window',
                'name': _('Business Trip'),
                'res_model': 'business.trip',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'view_id': False,
                'domain': [('id', 'in', trip)],
            }