# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Human Resource Contract",
    'version': '10.0.1.0',
    'depends': [
        'hr_contract',
        'l10n_mn_hr',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian HR Modules',
    'description': """
         HR contract
    """,
    'data': [
        'security/ir.model.access.csv',
        'security/hr_contract_security.xml',
        'wizard/hr_contract_info_change_view.xml',
        'views/hr_recruitment_config_settings_view.xml',
        'views/hr_employee_view.xml',
        'views/hr_contract_views.xml',
        'views/hr_contract_cron_view.xml',
        'views/email_template_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
