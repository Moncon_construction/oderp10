# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime
from dateutil.relativedelta import relativedelta
import logging
import time

from docx.opc import part

from odoo import api, models, fields, _, exceptions
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import ValidationError
from odoo.http import request


_logger = logging.getLogger(__name__)


class HrContract(models.Model):
    _inherit = 'hr.contract'
    wage = fields.Float('Wage', digits=(16, 2), required=True, help="Basic Salary of the employee", track_visibility='onchange')
    working_hours = fields.Many2one('resource.calendar', string='Working Schedule')
    trial_date_start = fields.Date('Trial Start Date',  track_visibility='onchange')
    trial_date_end = fields.Date('Trial End Date', track_visibility='onchange')
    date_start = fields.Date('Start Date', required=True, default=fields.Date.today, track_visibility='onchange')
    date_end = fields.Date('End Date', track_visibility='onchange')
    contract_user = fields.Many2one('res.users','Contract User', default=lambda self: self.env.user, track_visibility='onchange')
    active = fields.Boolean('Active', track_visibility='onchange', default=True)
    contract_code = fields.Char('Contract code')
    hour_salary = fields.Float('Hour Norm Salary')
    contract_emp_dep = fields.Many2one('hr.department', string="Department")

    def _get_default_type(self):
        if self.employee_id and self.employee_id.state_id:
            if self.employee_id.state_id.contract_type:
                return self.employee_id.state_id.contract_type
        else:
            return self.env['hr.contract.type'].search([], limit=1)   
       
    payroll_by_attendance = fields.Boolean('Payroll By Attendance', track_visibility='onchange')
    is_salary_complete = fields.Boolean('Is Salary Complete',  track_visibility='onchange')
    type_id = fields.Many2one('hr.contract.type', string="Contract Type", old_name="type_id", required=True, default=_get_default_type)
    hour_balance_calculate_type = fields.Selection([
                                                    ('payroll_by_attendance', 'Payroll By Attendance'),
                                                    ('is_salary_complete', 'Is Salary Complete'),
                                                ], string='Hour balance calculate type', copy=False )
    company_id = fields.Many2one('res.company', string='Company', readonly=True, default=lambda self: self.env.user.company_id)

    @api.model
    def create(self, vals):
        company = self.env.user.company_id
        if company.hr_contract_number == 2:
            contracts = self.env['hr.contract'].search([('employee_id', '=', vals['employee_id']), ('employee_id.company_id', '=', company.id), ('active', '=', 't')])
            for contract in contracts:
                if not contract.date_end:
                    raise ValidationError(_("Please enter the expiration end date of the previous contract"))
                if contract.date_end > vals['date_start']:
                    raise ValidationError(_("Please check start date"))
        if company.is_contract_sequence_check:
            contract_code_type_obj = self.env['hr.contract.type'].search([('id', '=', vals['type_id'])])
            if contract_code_type_obj:
                sequence = contract_code_type_obj.hr_contract_code_sequence
                code = sequence.next_by_id()
                vals.update({'contract_code': code})
        return super(HrContract, self).create(vals)
    
    @api.onchange('employee_id')
    def set_contract_type(self):
        for obj in self:
            if obj.employee_id and obj.employee_id.state_id:
                if obj.employee_id.state_id.contract_type:
                    obj.type_id = obj.employee_id.state_id.contract_type
                    
    @api.onchange('contract_user')
    def _onchange_contract_user(self):
        if self.contract_user:
            self.contract_emp_dep = self.contract_user.department_id        
            
    @api.multi
    @api.constrains('employee_id')
    def _check_create_employee(self):
        for record in self:
            if record.employee_id.company_id.hr_contract_number == 1:
                hr_employee = self.env['hr.contract'].search([('employee_id', '=', record.employee_id.id), ('employee_id.company_id', '=', record.employee_id.company_id.id), ('active', '=', 't')])
                if len(hr_employee) > 1:
                    raise ValidationError(_("No more than one active contract!"))

    @api.multi
    def _cron_deadline(self):
        current_date = time.strftime('%Y/%m/%d')
        contract = self.search([('date_end', '=', current_date)])
        model_obj = self.env['ir.model.data']
        hr_manager_group = model_obj.get_object('hr', 'group_hr_manager')
        res_user = self.env['res.users'].search(
            [('groups_id', 'in', hr_manager_group.id)])
        if contract:
            index = 0
            detailed_info = []
            for contract_obj in contract:
                index += 1
                detailed_info.append({
                    'base_url': self.env['ir.config_parameter'].get_param( 'web.base.url'),
                    'db_name': self.env.cr.dbname,
                    'id': contract_obj.id,
                    'action_id': self.env['ir.model.data'].get_object_reference('hr_contract', 'action_hr_contract')[1],
                    'model': 'hr.contract',
                    'name': contract_obj.name,
                    'index': index,
                    'emp_name': contract_obj.employee_id.name,
                    'comp_name': contract_obj.employee_id.company_id.partner_id.name,
                    'contract': contract_obj.name,
                    'job': contract_obj.job_id.name
                })
             
            template_id = self.env['ir.model.data'].get_object_reference('l10n_mn_hr_contract', 'contract_deadline_template')[1]
            template = self.env['mail.template'].sudo().browse(template_id)
            if res_user:
                for user in res_user:
                    template.with_context({'detailed_info': detailed_info,
                                           'end_date': current_date,
                                           'menu': u"Ажилтан/Гэрээнүүд"}).send_mail(user.id, force_send=True)

    def get_contract_grouped_data(self, contract_type, type, grouped_data, partner_ids, contract_ids):
        if not contract_ids:
            return grouped_data, partner_ids
        
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        db_name = request.session['db']
        td_style = "style='border: 1px solid #dddddd; padding: 4px;'"
        if type == 'before_month':
            last_time = _("1 month")
        elif type == 'before_2weeks':
            last_time = _("2 weeks")
        else:
            last_time = _("by today")
            
        for contract_id in contract_ids:
            if not contract_id.message_follower_ids:
                continue

            contract_url = unicode(base_url) + '/web?db=' + unicode(db_name) + '#id=' + str(contract_id.id) + '&view_type=form&model=' + 'hr.contract'
            contract_link = "<a href='%s' target='_blank'>%s</a>" % (contract_url, _("Get into Hr Contract"))
            emp_url = unicode(base_url) + '/web?db=' + unicode(db_name) + '#id=' + str(contract_id.employee_id.id) + '&view_type=form&model=' + 'hr.employee'
            emp_link = "<a href='%s' target='_blank'>%s</a>" % (emp_url, contract_id.employee_id.name)
            
            # Дагагчдаар бүлэглэн гэрээний dictionary бэлдэх
            for partner_id in contract_id.message_follower_ids.mapped('partner_id'):
                tr_line = """<tr><td %s>%s</td><td %s>%s</td><td %s>%s</td><td %s>%s</td><td %s>%s</td><td %s>%s</td><td %s>%s</td><td %s>%s</td></tr>
                """ % (td_style, contract_id.employee_id.department_id.name, td_style, contract_id.employee_id.ssnid, td_style, contract_id.employee_id.last_name, 
                       td_style, emp_link, td_style, contract_link, td_style, contract_id.trial_date_start if contract_type == 'trial' else contract_id.date_start, 
                       td_style, contract_id.trial_date_end if contract_type == 'trial' else contract_id.date_end, td_style, last_time)
                if partner_id in grouped_data.keys() and contract_type in grouped_data[partner_id].keys() and type in grouped_data[partner_id][contract_type].keys():      
                    grouped_data[partner_id][contract_type][type].append(tr_line)
                else:
                    if not partner_id in grouped_data.keys():
                        grouped_data[partner_id] = {}
                    if partner_id in grouped_data.keys() and not contract_type in grouped_data[partner_id].keys():
                        grouped_data[partner_id][contract_type] = {}
                    if partner_id in grouped_data.keys() and contract_type in grouped_data[partner_id].keys() and not type in grouped_data[partner_id][contract_type].keys():
                        grouped_data[partner_id][contract_type][type] = []
                    grouped_data[partner_id][contract_type][type] = [tr_line]
                    
                    if partner_id not in partner_ids:
                        partner_ids.append(partner_id)
        
        return grouped_data, partner_ids
        
    def get_grouped_table(self, type, trows):
        # Багцалсан гэрээгээр table үүсгэж буцаах
        body = ""
        contract_type = _("labor") if type == 'labor' else _("trial")
        start_date = _("Labor Contract Start Date") if type == 'labor' else _("Trial Contract Start Date")
        end_date = _("Labor Contract End Date") if type == 'labor' else _("Trial Contract End Date")
        tr_style = "style='border: 1px solid #dddddd; padding: 4px;'"
        subject_style = "display:inline; font-weight: bold;"
        
        body += "<h3><p style='%s'>%s</p> <p style='%s color: red;'>%s</p><p style='%s'> %s.</p></h3>" % (subject_style, _("Belown employees"), subject_style, contract_type, subject_style, _("contracts are expired"))
        body += """
            <table style="border-collapse: collapse; padding-bottom: 10px;">
                <tr style="border: 1px solid grey;"><th %s>%s</th><th %s>%s</th><th %s>%s</th><th %s>%s</th><th %s>%s</th><th %s>%s</th><th %s>%s</th><th %s>%s</th></tr>
        """ % (tr_style, _("Department"), tr_style, _("Register No"), tr_style, _("Last Name"), tr_style, _("First Name"), tr_style, _("Contract"), tr_style, start_date, tr_style, end_date, tr_style, _("Lasts Date"))
        for row in trows:
            body += row
        body += "</table>"
        
        return body
                        
    @api.multi
    def _cron_labor_contract(self):
        # Ажилтны туршилтын/хөдөлмөрийн гэрээний дуусах хугацааг шалгаж дагагчдад мэйл илгээх крон.

        # Гарах мэйл сервэр авах
        outgoing_email = self.env['ir.mail_server'].sudo().search([], order='sequence asc', limit=1)
        if not outgoing_email:
            return
            
        HrContract = self.env['hr.contract']
        current_date = datetime.strptime(str(get_day_like_display(str(datetime.now()), self.env.user))[:10], '%Y-%m-%d')
        time_intervals = ['before_month', 'before_2weeks', 'exactly_tday']
        contract_types = ['trial', 'labor']
        grouped_data = {}
        partner_ids = []
        
        # Харгалзах дагагч бүрээр хугацаа дуусч буй гэрээний жагсаалт бүхий dictionary бэлдэх
        for contract_type in contract_types:
            for time_interval in time_intervals:
                start_date = 'trial_date_start' if contract_type == 'trial' else 'date_start'
                end_date = 'trial_date_end' if contract_type == 'trial' else 'date_end'
                domain = [('active', '=', True), (start_date, '<=', current_date)]
                if time_interval == 'before_month':
                    domain.append((end_date, '=', current_date+relativedelta(months=+1)))
                elif time_interval == 'before_2weeks':
                    domain.append((end_date, '=', current_date+relativedelta(weeks=+2)))
                else:
                    domain.append((end_date, '=', current_date))
                    
                grouped_data, partner_ids = self.get_contract_grouped_data(contract_type, time_interval, grouped_data, partner_ids, HrContract.search(domain))
        
        if grouped_data and partner_ids:
            for partner_id in partner_ids:
                # Дагагчид холбоотой бүх гэрээг туршилт/үндсэн гэрээний алин болохоос хамаарч багцлан хүснэгт бэлдэх
                body = ""
                for contract_type in contract_types:
                    if contract_type in grouped_data[partner_id].keys():
                        table_rows = []
                        for time_interval in time_intervals:   
                            if grouped_data.get(partner_id).get(contract_type).get(time_interval):
                                table_rows.extend(grouped_data[partner_id][contract_type][time_interval])
                        body += self.get_grouped_table(contract_type, table_rows)
                                
                # Бэлдсэн хүснэгтээр дагагчид өдөрт нэг л мэйл явуулах
                self.env['mail.mail'].create({
                    'subject': _("[ERP] Employees' contract expired notification"),
                    'body_html': """<p style="padding-bottom: 20px">Сайн байна уу, %s</p>
                                    %s
                                    <p style="padding-top: 40px">--<br>%s <br>%s </>
                                """ % (partner_id.name, body, _("ERP Auto Email"), _("You are received email because of you exists in followers list of contracts above.")),
                    'email_to': partner_id.email,
                    'email_from': outgoing_email.smtp_user,
                }).send()
                
            
class Employee(models.Model):

    _inherit = "hr.employee"

    home_work_space = fields.Char(string='Home-Work Dist.', groups='hr.group_hr_user')

class InheritedHrContractType(models.Model):
    _inherit = 'hr.contract.type'

    hr_contract_code_sequence = fields.Many2one('ir.sequence', store=True, string="Hr contract Sequence")
    code = fields.Char('code')

    @api.model
    def create(self, vals):
        company = self.env.user.company_id
        if company.is_contract_sequence_check:
            code = self.env["ir.sequence"].next_by_code("hr.contract.code.sequence")
            code_sz = 3
            #       Ижил ангилалын кодтой гэрээ байгаа эсэхийг шалгаж байна
            if 'code' in vals:
                if vals['code'] is not False:
                    code = vals["code"]

            if 'code' in vals.keys() and vals['code'] is not False:
                if vals['code_size'] is not False:
                    code_sz = vals["code_size"]

            #       Ангилалын дарааллыг үүсгэж product_code_sequence талбарт холбож өгч байна.
            seq_obj = self.env['ir.sequence']
            seq_vals = {
                u'padding': code_sz,
                u'code': code,
                u'name': (u'HR Contract(%s)' % vals['name']),
                u'implementation': u'standard',
                u'company_id': 1,
                u'use_date_range': False,
                u'number_increment': 1,
                u'prefix': False,
                u'date_range_ids': [],
                u'number_next_actual': 0,
                u'active': True,
                u'suffix': False
            }
            seq_id = seq_obj.sudo().create(seq_vals)
            vals.update({'hr_contract_code_sequence': seq_id.id, 'code': code})
        return super(InheritedHrContractType, self).create(vals)

class WorkingHoursChangeHistory(models.Model):
    _name = "working.hours.change.history"

    contract_id = fields.Many2one('hr.contract', string='Contract')
    before_working_hours_id = fields.Many2one('resource.calendar', string='Before Working Hours')
    working_hours_id = fields.Many2one('resource.calendar', string='Working Hours')
    edit_date = fields.Date(string='Edit date')
    date = fields.Datetime(string='Date')
    user_id = fields.Many2one('res.users', string='User')
    
class Contract(models.Model):
    _inherit = 'hr.contract'
    
    working_hours_change_history_ids = fields.One2many('working.hours.change.history', 'contract_id',string="Working Hours History", copy=False)


