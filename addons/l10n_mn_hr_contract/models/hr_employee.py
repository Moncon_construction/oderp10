# -*- coding: utf-8 -*-
import logging
import time
from odoo import api, fields, models, _  # @UnresolvedImport.

class HrEmployeeStatus(models.Model):
    _inherit = 'hr.employee.status'
    
    contract_type = fields.Many2one('hr.contract.type', string="Contract Type")
    
    @api.multi
    def write(self, vals):
        for obj in self:
            if 'contract_type' in vals.keys():
                chosen_type = self.env['hr.contract.type'].browse(vals['contract_type'])
                if chosen_type and len(chosen_type) > 0:
                    chosen_type = chosen_type[0]
                emps = self.env['hr.employee'].search([('state_id', '=', obj.id)])
                if chosen_type and emps and len(emps) > 0:
                    for emp in emps:
                        contracts = self.env['hr.contract'].search([('employee_id', '=', emp.id)])
                        if contracts and len(contracts) > 0:
                            for contract in contracts:
                                contract.type_id = chosen_type
            
        return super(HrEmployeeStatus, self).write(vals)
    
class HrEmployee(models.Model):
    _inherit = 'hr.employee'
    
    @api.multi
    def write(self, vals):
        for obj in self:
            if 'state_id' in vals.keys():
                contracts = self.env['hr.contract'].search([('employee_id', '=', obj.id)])
                emp_state = self.env['hr.employee.status'].browse(vals['state_id'])
                if emp_state and contracts and len(contracts) > 0:
                    if len(emp_state) > 0:
                        emp_state = emp_state[0]
                    if emp_state.contract_type:
                        for contract in contracts:
                            contract.type_id = emp_state.contract_type
            
        return super(HrEmployee, self).write(vals)
