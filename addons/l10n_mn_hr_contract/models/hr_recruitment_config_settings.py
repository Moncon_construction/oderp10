import logging
 
from odoo import api, fields, models
 
_logger = logging.getLogger(__name__)
 
 
class HrConfiguration(models.TransientModel):
    _inherit = 'hr.recruitment.config.settings'

    company_id = fields.Many2one('res.company', string='Company', readonly=True, default=lambda self: self.env.user.company_id)
    is_contract_sequence_check = fields.Boolean(related='company_id.is_contract_sequence_check', string='Is contract sequence check')