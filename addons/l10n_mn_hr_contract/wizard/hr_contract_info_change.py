# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################
from odoo import api, fields, models, _
import time
import pytz
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import ValidationError  # @UnresolvedImport


class HRContractInfoChange(models.TransientModel):
    _name = 'hr.contract.info.change'
    _description = 'Contract Information Change Wizard'

    working_hours = fields.Many2one('resource.calendar', string='Working Hour')
    date = fields.Date(string='Date')

    @api.multi
    def change_info(self):
        context = dict(self._context or {})
        contract_ids = self.env['hr.contract'].browse(context.get('active_ids'))
        user_time_zone = pytz.UTC
        if self.env.user.partner_id.tz:
            user_time_zone = pytz.timezone(self.env.user.partner_id.tz)
        else:
            raise ValidationError(_('Warning !\n Please. You set your time zone. Settings go to the User menu!'))
        for contract in contract_ids:
            if self.working_hours:
                datetime_from = user_time_zone.localize(datetime.strptime(self.date + ' 00:00:00', DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(pytz.utc).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                attendances = self.env['hr.attendance'].search([('employee_id', '=', contract.employee_id.id), ('check_in', '>=', datetime_from)])
                date = time.strftime('%Y%m%d_%H%M')
                history = self.env['working.hours.change.history'].create({
                                'contract_id':contract.id,
                                'before_working_hours_id': contract.working_hours.id,
                                'working_hours_id': self.working_hours.id,
                                'edit_date':self.date,
                                'date':date,
                                'user_id':self.env.user.id
                        })
                contract.working_hours = self.working_hours.id
                for att in attendances:
                    att.write({'contract_id':contract.id,
                               'working_hours':self.working_hours.id
                               })
        return True
