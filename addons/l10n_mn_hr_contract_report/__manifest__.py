# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Human Resource Contract Report",
    'version': '10.0.1.0',
    'depends': [
        'l10n_mn_hr_contract',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian HR Modules',
    'description': """
         HR contract report
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/hr_contract_type_views.xml',
        'wizard/print_template.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
