# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import base64
from StringIO import StringIO
from docx import Document
import zipfile
import os

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class HrContractTypeFields(models.Model):
    _name = 'hr.contract.type.fields'

    name = fields.Char(string='name', required=True)
    field_id = fields.Many2one('ir.model.fields', string='Field', required=True,
                               domain="[('model_id.model','in',['hr.contract'])]")
    string_path = fields.Char('First String Path')
    last_path = fields.Char('Last String Path')
    category_id = fields.Many2one('hr.contract.type', string='Category')


class InheritedHrContractType(models.Model):
    _inherit = 'hr.contract.type'

    attachment_id = fields.Many2one('ir.attachment', string='Template file',
                                    domain="[('mimetype','=','application/vnd.openxmlformats-officedocument.wordprocessingml.document')]")
    field_ids = fields.One2many('hr.contract.type.fields', 'category_id', string='Fields')


class HrContractTemplateDocument(models.TransientModel):
    _name='hr.contract.template.document'

    @api.multi
    def export_docx(self):
        context = self._context
        report_type = ''
        active_id = context and context.get('active_ids', [])
        hr_contract_id = self.env['hr.contract'].browse(active_id)
        for exp in hr_contract_id:
            if exp.type_id.attachment_id.datas:
                report, template_name = self.create_doc_source(hr_contract_id)
                out = base64.encodestring(report)
                report_type = '.pdf'

                excel_id = self.env['oderp.report.excel.output'].create({
                    'filedata': out,
                    'filename': template_name + report_type,
                })
                return {
                    'name': 'Export Report',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'oderp.report.excel.output',
                    'res_id': excel_id.id,
                    'view_id': False,
                    'context': self._context,
                    'type': 'ir.actions.act_window',
                    'target': 'new',
                    'nodestroy': True,
                }
            else:
                raise UserError(_('There is no attachment files in contract types!'))

    def create_doc_source(self ,hr_contract_id):
        tmp_folder_name = '/tmp/docx_to_pdf/'
        self._delete_temp_folder(tmp_folder_name)
        self._create_temp_folder(tmp_folder_name)
        convert_path, template_name = self.convert_docx_from_base(tmp_folder_name,hr_contract_id)
        report = self._get_convert_file(convert_path )
        return report ,template_name

    @api.multi
    def convert_docx_from_base(self, tmp_folder_name, hr_contract_id):
        docx_template_name = 'template_1.docx'
        template_path = tmp_folder_name + docx_template_name
        name = hr_contract_id.type_id.attachment_id.name
        convert_path = tmp_folder_name + 'template.docx'
        doc = base64.decodestring(hr_contract_id.type_id.attachment_id.datas)
        data = StringIO(doc)
        data = Document(data)
        data.save(template_path)

        replaceText = {}
        for field in hr_contract_id.type_id.field_ids:
            if field.field_id.model_id.model == 'hr.contract':
                desc = False
                for con in hr_contract_id:
                    if field.field_id.name in con:
                        if field.field_id.ttype == 'many2one':
                            if field.string_path:
                                model_id = self.env['ir.model'].search([('model', '=', field.field_id.relation)])
                                f_id = self.env['ir.model.fields'].search(
                                    [('name', '=', field.string_path), ('model_id', '=', model_id.id)])
                                if f_id:
                                    if f_id.ttype == 'many2one':
                                        if field.last_path:
                                            model_id2 = self.env['ir.model'].search([('model', '=', f_id.relation)])
                                            f_id2 = self.env['ir.model.fields'].search(
                                                [('name', '=', field.last_path), ('model_id', '=', model_id2.id)])
                                            if f_id2:
                                                if f_id2.ttype == 'many2one':
                                                    desc = con[field.field_id.name][field.string_path][field.last_path].name
                                                else:
                                                    desc = con[field.field_id.name][field.string_path][field.last_path]
                                            else:
                                                raise UserError(
                                                    _('The %s field is not set correctly!') % field.last_path)
                                        else:
                                            desc = con[field.field_id.name][field.string_path].name
                                    else:
                                        desc = con[field.field_id.name][field.string_path]
                                else:
                                    raise UserError(_('The %s field is not set correctly!') % field.string_path)
                            else:
                                desc = con[field.field_id.name].name
                        else:
                            desc = con[field.field_id.name]
                        strss = desc.encode(encoding='UTF-8') if type(desc) in (str, unicode) else desc
                        replaceText.update({field.name.encode(encoding='UTF-8'): strss})
        self.create_doc(template_path, name, tmp_folder_name, convert_path, replaceText)

        if hr_contract_id.type_id:
            self._convert_docx_to_pdf(convert_path, tmp_folder_name)
            get_path = tmp_folder_name + 'template.pdf'
            return get_path, name
        return convert_path, name

    @api.multi
    def create_doc(self, template_file, name, tmp_folder_name, out_file, replaceText):
        templateDocx = zipfile.ZipFile(template_file)
        newDocx = zipfile.ZipFile(out_file, "w")
        for file in templateDocx.filelist:
            content = templateDocx.read(file)
            for key in replaceText.keys():
                content = content.replace(str(key), str(replaceText[key]))
            newDocx.writestr(file.filename, content)
        templateDocx.close()
        newDocx.close()
        return newDocx

    def _get_convert_file(self, convert_path):
        input_stream = open(convert_path, 'r')
        try:
            report = input_stream.read()
        finally:
            input_stream.close()

        return report

    def _convert_docx_to_pdf(self, tmp_folder_name, convert_docx_file_name):
        cmd = "soffice --headless --convert-to pdf --outdir " + convert_docx_file_name + " " + tmp_folder_name
        os.system(cmd)

    def _create_temp_folder(self, tmp_folder_name):
        if not os.path.exists(tmp_folder_name):
            os.makedirs(tmp_folder_name)

    def _delete_temp_folder(self, tmp_folder_name):
        cmd = 'rm -rf ' + tmp_folder_name
        os.system(cmd)
        if os.path.exists(tmp_folder_name):
            os.rmdir(tmp_folder_name)