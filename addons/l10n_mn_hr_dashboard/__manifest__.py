# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian HR Dashboard",
    'version': '1.0',
    'depends': [
        'l10n_mn_hr', 'highcharts_graph_widget',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
    Mongolian Human Resource Dashboard
    """,
    'data': [
        'views/employee_salary_dashboard_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
