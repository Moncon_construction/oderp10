# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
{
    'name': "Mongolian HR Direct Schedule",
    'version': "1.0",
    'depends': ['l10n_mn_hr'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': "Mongolian Modules",
    'description': """
HR Management
""",
    'data': [
        'security/ir.model.access.csv',
        'views/hr_direct_schedule_view.xml',
        'views/state_change.xml',
        'views/hr_job_views.xml',
    ],
    'installable': True,
    'auto_install': False
}