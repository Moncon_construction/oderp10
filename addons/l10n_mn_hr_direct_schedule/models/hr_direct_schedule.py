# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.exceptions import ValidationError


class HrDirectSheet(models.Model):
    _name = "hr.direct.sheet"
    _description = "New Employee Direct Sheet"
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    def _this_user(self):
        # Тухайн нэвтэрсэн ажилтан мөн эсэхийг шалгаж байна.
        res = {}
        show = False
        for csheet in self:
            if csheet.user_to_fill == self:
                show = True
                for line in csheet.direct_lines:
                    if csheet.user_to_fill.id == line.clearance_user_id.id:
                        line.can_fill = True
                    else:
                        line.can_fill = False
            elif csheet.user_to_fill != self:
                show = False
        res[csheet.id] = show
        return res

    name = fields.Char(string='Direct schedule', defaults='Direct schedule')
    accepted_user = fields.Many2one('res.users', string='Accepted User')
    employee_id = fields.Many2one('hr.employee', string='Employee')
    department_id = fields.Many2one('hr.department', string='Department')
    state = fields.Selection([('draft', 'Draft'),
                              ('waiting', 'Waiting'),
                              ('done', 'Done'),
                              ('cancel', 'Cancel'),
                              ], track_visibility='onchange', string='State', default='draft')
    history = fields.One2many('direct.history.lines',
                              'parent_id', string='History', readonly=True)
    direct_lines = fields.One2many(
        'hr.direct.lines', 'parent_id', string='Direct lines')
    job_id = fields.Many2one('hr.job', string='Job')
    this_user = fields.Many2one(
        'res.users', string='This user', default=lambda self: self.env.user)
    this_user_edit = fields.Boolean(
        compute=_this_user, string='Current user can execute', default=False, method=True)
    ch = fields.Boolean(string='Prevous was in waiting state', default=False)
    to_fill = fields.Integer(string='row to fill')
    user_to_fill = fields.Many2one(
        'res.users', string='User to fill', readonly=True, default=lambda self: self.env.user)
    date = fields.Datetime.now()
    create_date = fields.Datetime(string='Create date')
    create_uid = fields.Many2one(
        'res.users', string='Create employee', readonly=True)

    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        self.job_id = self.employee_id.job_id
        self.department_id = self.employee_id.department_id

    @api.multi
    def button_send(self):
        # Чиглүүлэх хөтөлбөрийн мэдээллийг бөглөөд илгээх ба танилцах зүйл хавтас хоосон бол анхааруулга өгнө.
        self._fields['name'].translate = True
        lines_obj = self.env['direct.history.lines']
        for sheet in self:
            if not sheet.direct_lines:
                raise ValidationError(
                    _('Warning! Clearance lines are empty!'))

            sheet.state = 'waiting'
            sheet.ch = True
            for lin in sheet.direct_lines:
                if not lin.estimate:
                    sheet.user_to_fill = lin.clearance_user_id
                    self.message_subscribe_users(
                        [sheet.id], [lin.clearance_user_id.id])
            break
            line = {'date_done': fields.Datetime.now(
            ), 'clearance_state': 'sent', 'parent_id': sheet.id}
            lines_obj.create(line)
            sheet.this_user = None
        return True

    @api.multi
    def btn_send_conf(self):
        # Чиглүүлэх хөтөлбөр батлах товч. Зөвхөн чиглүүлэх хөтөлбөрийг үүсгэсэн ажилтан л батлаад илгээх товчийг дарж болно.
        lines_obj = self.env['direct.history.lines']
        have_empty = False
        for sheet in self:
            sheet.this_user = self._uid
            if sheet.this_user != sheet.user_to_fill:
                sheet.this_user = None
                raise ValidationError(
                    _('Warning!Current User can not access function.'))

            if sheet.this_user == sheet.user_to_fill:
                if not sheet.direct_lines[sheet.to_fill].estimate:
                    raise ValidationError(
                        _('Warning! Please fill the estimate field!'))
                for lin in sheet.direct_lines:
                    if not lin.estimate:
                        have_empty = True
                if have_empty:
                    sheet.state = 'waiting'
                    self.message_post(body='Waiting')
                    line = {'date_done': fields.Datetime.now(
                    ), 'clearance_state': 'done', 'parent_id': sheet.id}
                    lines_obj.create(line)

                    self.message_subscribe_users(
                        [sheet.id], [sheet.direct_lines[sheet.to_fill + 1].clearance_user_id])

                    sheet.user_to_fill = sheet.direct_lines[
                        sheet.to_fill + 1].clearance_user_id
                    sheet.to_fill += 1
                    sheet.this_user = None

                if not have_empty:
                    sheet.state = 'done'

                    sheet.ch = False
                    line = {'date_done': fields.datetime.now(
                    ), 'clearance_state': sheet.state, 'parent_id': sheet.id}
                    lines_obj.create(line)
                    sheet.this_user = None

        return True

    @api.multi
    def btn_cancel(self):
        # Чиглүүлэх хөтөлбөрийг цуцлах товч
        lines_obj = self.env['direct.history.lines']
        for sheet in self:
            sheet.state = 'cancel'
            sheet.ch = False

            line = {'date_done': fields.datetime.now(
            ), 'clearance_state': sheet.state, 'parent_id': sheet.id}
            lines_obj.create(line)

        return True

    @api.multi
    def btn_draft(self):
        # Чиглүүлэх хөтөлбөрийг ноорог болгох товч
        lines_obj = self.env['direct.history.lines']
        for sheet in self:
            sheet.state = 'draft'
            sheet.ch = False

            line = {'date_done': fields.datetime.now(
            ), 'clearance_state': sheet.state, 'parent_id': sheet.id}
            lines_obj.create(line)
        return True


class HrDirectLines(models.Model):
    _name = 'hr.direct.lines'
    _description = 'Direct Sheet Lines'
    _order = "order asc, write_date desc"
    
    parent_id = fields.Many2one('hr.direct.sheet', string='Parent')
    order = fields.Integer(string='Order', default=1)
    clearance_description = fields.Char(string='Description')
    clearance_user_id = fields.Many2one(
        'res.users', string='Employee', default=lambda self: self.env.uid)
    estimate = fields.Boolean(string='Estimate')


class DirectHistoryLines(models.Model):
    _name = 'direct.history.lines'
    _description = 'History Lines'
    _rec_name = 'user_id'

    parent_id = fields.Many2one('hr.direct.sheet', string='Parent')
    user_id = fields.Many2one(
        'res.users', string='Employee', default=lambda self: self.env.uid)
    date_done = fields.Datetime(string='Date')
    clearance_state = fields.Selection([('draft', 'Draft'),
                                        ('waiting', 'Waiting'),
                                        ('done', 'Done'),
                                        ('cancel', 'Cancel')], string='State')
