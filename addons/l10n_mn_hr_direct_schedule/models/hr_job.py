# -*- coding: utf-8 -*-
from odoo import models, fields, api

class HrJobDirectSchedule(models.Model):
    _name = "hr.job.direct.schedule"
    _description = 'Direct Schedules for the job'
    _order = "order asc, write_date desc"
    
    order = fields.Integer(string='Order', default=1)
    job = fields.Many2one('hr.job', string='Job', required=True)
    description = fields.Char(string='Description', required=True)
    
    @api.multi
    def name_get(self):
        res = []
        for obj in self:
            name = obj.description + " [" + obj.job.name + "]"
            res.append((obj.id, name))
        return res
    
class HrDirectLines(models.Model):
    _inherit = 'hr.direct.lines'
    
    job_direct_schedule = fields.Many2one('hr.job.direct.schedule', string='Job Direct Schedule')
    
class HrJob(models.Model):
    _inherit = "hr.job"

    job_direct_schedules = fields.One2many('hr.job.direct.schedule', 'job', string='Direct Sheet')

class HrDirectSheet(models.Model):
    _inherit = "hr.direct.sheet"

    @api.multi
    def write(self, values):
        # One2many - руу onchange-с объект нэмэхэд тухайн one2many дахь many2one объектын утгыг авч чадахгүй байсан тул давхар write дотор нь нэмэж өгөв.
        if 'job_id' in values.keys():
            new_direct_lines = []
            registered_job_direct_schedule_ids = []
            job_direct_schedules = self.env['hr.job'].browse(values['job_id']).job_direct_schedules
            
            # Өмнөх чиглүүлэх хөтөлбөрийн мөрүүдээс албан тушаалаас үүссэн мөрүүдийг цэвэрлэж байна.
            for direct_line in self.direct_lines:
                if direct_line.job_direct_schedule and not direct_line.estimate:
                    direct_line.unlink()
                elif direct_line.job_direct_schedule:
                    registered_job_direct_schedule_ids.append(direct_line.job_direct_schedule.id)
                
            # Сонгогдсон албан тушаал дахь чиглүүлэх хөтөлбөрүүдийг нэмэж values-д дамжуулж байна.
            if job_direct_schedules and len(job_direct_schedules) > 0:
                for job_direct_schedule in job_direct_schedules:
                    # Тухайн ажил/мэргэжилд харъяалагдах чиглүүлэх хөтөлбөрийг аль хэдийн нэмсэн бол дахин нэмэх шаардлагагүй.
                    if job_direct_schedule.id not in registered_job_direct_schedule_ids:
                        vals = {
                            'parent_id': self.id,
                            'clearance_description': job_direct_schedule.description,
                            'clearance_user_id': self.employee_id.id if self.employee_id else False,
                            'estimate': False,
                            'order': job_direct_schedule.order or 1,
                            'job_direct_schedule': job_direct_schedule.id,
                        }
                        new_direct_lines.append([0, False, vals])
                values['direct_lines'] = new_direct_lines
                
        return super(HrDirectSheet, self).write(values)
        
    @api.onchange('job_id')
    def _add_direct_lines(self):
        # One2many - руу onchange-с объект нэмэх.
        old_direct_lines = []
        registered_job_direct_schedule_ids = []
        job_direct_schedules = self.job_id.job_direct_schedules
         
        # Тухайн чиглүүлэх хөтөлбөрийг шууд update хийх тул өмнөх чиглүүлэх хөтөлбөрийн мөрүүдийг авч байна.
        for direct_line in self.direct_lines:
            # Тухайн чиглүүлэх хөтөлбөрийн мөр албан тушаалаас үүссэн ч хийгдээгүй бол авахгүй. Өөр албан тушаал сонгож байгаа учир.
            if not direct_line.job_direct_schedule or (direct_line.job_direct_schedule and direct_line.estimate):
                old_direct_lines.append((0, 0, {
                    'parent_id': direct_line.parent_id,
                    'clearance_description': direct_line.clearance_description,
                    'clearance_user_id': direct_line.clearance_user_id,
                    'estimate': direct_line.estimate,
                    'order': direct_line.order
                }))
                
                if direct_line.job_direct_schedule:
                    registered_job_direct_schedule_ids.append(direct_line.job_direct_schedule.id)
          
        # Сонгогдсон албан тушаал дахь чиглүүлэх хөтөлбөрүүдийг авч байна.
        if job_direct_schedules and len(job_direct_schedules) > 0:
            new_direct_lines = []
            for job_direct_schedule in job_direct_schedules:
                # Хэрвээ тухайн албан тушаалын чиглүүлэх хөтөлбөр аль хэдийн бүртгэгдсэн бол дахин нэмэх шаардлагагүй.
                if job_direct_schedule.id not in registered_job_direct_schedule_ids:
                    vals = {
                        'parent_id': self._origin.id,
                        'clearance_description': job_direct_schedule.description,
                        'clearance_user_id': self.employee_id.id if self.employee_id else False,
                        'estimate': False,
                        'order': job_direct_schedule.order or 1,
                        'job_direct_schedule': job_direct_schedule.id,
                    }
                    new_direct_lines.append((0, 0, vals))
                  
            # Чиглүүлэх хөтөлбөрийн мөрүүдийн утгыг шинэчилж байна.
            if len(new_direct_lines) > 0:
                self.update({
                    'direct_lines': new_direct_lines + old_direct_lines,
                })
                    
