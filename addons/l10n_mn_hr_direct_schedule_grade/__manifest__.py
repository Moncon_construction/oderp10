# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
{
    'name': "Mongolian HR Direct Schedule Grade",
    'version': "1.0",
    'depends': ['l10n_mn_hr_direct_schedule'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': "Mongolian Modules",
    'description': """
HR Management
""",
    'data': [
        'views/hr_direct_schedule_grade_view.xml',
    ],
    'installable': True,
    'auto_install': False
}
