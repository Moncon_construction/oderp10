# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.exceptions import ValidationError, UserError

class HrDirectLines(models.Model):
    _inherit = 'hr.direct.lines'

    clearance_percent = fields.Integer(string='Percent')
    clearance_conclusion = fields.Selection([
        ('a', 'A'),
        ('b', 'B'),
        ('c', 'C'),
        ('d', 'D'),
        ('f', 'F')], string='Conclusion', compute='_estimate_clearance_conclusion',)

    @api.constrains('clearance_percent')
    def _check_clearance_percent(self):
        for line in self:
            if line.clearance_percent > 100 or line.clearance_percent < 0:
                raise ValidationError(_('Error ! Percent must be between 0 to 100!!!.'))

    @api.depends('clearance_percent')
    @api.multi
    def _estimate_clearance_conclusion(self):
        for sheet in self:
            if 0 <= sheet.clearance_percent <= 100:
                if sheet.clearance_percent >= 90:
                    sheet.clearance_conclusion = 'a'
                elif sheet.clearance_percent >= 80:
                    sheet.clearance_conclusion = 'b'
                elif sheet.clearance_percent >= 70:
                    sheet.clearance_conclusion = 'c'
                elif sheet.clearance_percent >= 60:
                    sheet.clearance_conclusion = 'd'
                elif sheet.clearance_percent >= 1:
                    sheet.clearance_conclusion = 'f'
            else:
                return False
