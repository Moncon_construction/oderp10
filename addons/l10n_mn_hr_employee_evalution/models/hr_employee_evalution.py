# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class HrEvalutionIndication(models.Model):
    _name = "hr.employee.evalution.indication"

    name = fields.Char(string='Name', required= True)
    indication_type = fields.Selection([('simple', 'Simple'),
                                     ('parent', 'Parent'),
                                     ('template', 'Template'),], string='Type')
    evalution_employee_id = fields.Many2one('res.users', string='Employee')
    max_point = fields.Float(string='Max Point', default = 1)
    min_point = fields.Float(string='Min Point', default = 0)
    parent_id =  fields.Many2one('hr.employee.evalution.indication', string='Parent Indication')

    @api.onchange('indication_type')
    def _onchange_indication_type(self):

        if self.indication_type == 'simple':

            evalutions = self.env['hr.employee.evalution.indication'].search([('indication_type', '=', 'parent')]).ids
            return {'domain': {'parent_id': [('id', 'in', evalutions)]}}

        if self.indication_type == 'parent':
            evalutions = self.env['hr.employee.evalution.indication'].search([('indication_type', '=', 'template')]).ids
            return {'domain': {'parent_id': [('id', 'in', evalutions)]}}




class HrEvalution(models.Model):
    _name = "hr.employee.evalution"

    name = fields.Char(string='Name', required= True)
    evalution_template_id = fields.Many2one('hr.employee.evalution.indication', string='Template', required= True)
    department_id = fields.Many2one('hr.department', string='Department', required=True)
    date = fields.Date('Date', default=fields.Date.context_today)
    state = fields.Selection([('draft', 'Draft'),
                              ('sent', 'Sent'),
                              ('evaluated', 'Evaluated')], string='State', default='draft', compute = 'compute_group_state')
    evalution_line_ids = fields.One2many('hr.employee.evalution.line', 'evalution_id', string="Evalution Line")

    @api.multi
    def compute_group_state(self):
        for obj in self:
            state = []
            for line in obj.evalution_line_ids:
                state.append(line.state)
            if 'sent' not in state:
                if 'draft' not in state:
                    if 'evaluated' not in state:
                        obj.state = 'draft'
                    else:
                        obj.state = 'evaluated'
                else:
                    obj.state = 'draft'
            else:
                obj.state = 'sent'

    @api.multi
    def sent(self):
        indication_line_ids = {}
        for line in self.evalution_line_ids:
            line.state = 'sent'
            for group_line in line.evalution_group_line_ids:
                group_line.state = 'sent'
                for indication_line in group_line.indication_line_ids:
                    indication_line.state = 'sent'
                    if indication_line.template_id not in indication_line_ids:
                        indication_line_ids[indication_line.template_id] = {'employee_ids': [line.employee_id.id], 'indication_line': [indication_line.id]}
                    else:
                        indication_line_ids[indication_line.template_id]['employee_ids'].append(line.employee_id.id)
                        indication_line_ids[indication_line.template_id]['indication_line'].append(indication_line.id)
        for key, value  in indication_line_ids.items():
            vals = {
                'tem_parent_parent_id': key.parent_id.parent_id.id,
                'tem_parent_id': key.parent_id.id,
                'template_id': key.id,
                'max_point': key.max_point,
                'min_point': key.min_point,

            }
            evaluting_id = self.env['hr.employee.evaluting'].sudo().create(vals)

            for index in range(0, len(value['employee_ids'])):
                self.env['hr.employee.evaluting.line'].sudo().create({
                    'evaluting_id': evaluting_id.id,
                    'employee_id': value['employee_ids'][index],
                    'indication_line_id': value['indication_line'][index],
                    'state': 'sent',
                })

        self.state = 'sent'
    @api.multi
    def calculate(self):
        employees = self.env['hr.employee'].search([('department_id', '=', self.department_id.id)])
        for employee_id in employees:
            vals = {
                'template_id': self.evalution_template_id.id,
                'employee_id': employee_id.id,
                'evalution_id': self.id,
            }
            self.env['hr.employee.evalution.line'].create(vals)

    @api.multi
    def unlink(self):
        for obj in self:
            if obj.state != 'draft':
                raise UserError(_('You can delete only draft evalution'))
        return super(HrEvalution, self).unlink()


class HrEvalutionLine(models.Model):
    _name = "hr.employee.evalution.line"

    evalution_id = fields.Many2one('hr.employee.evalution', string='Evalution')
    employee_id = fields.Many2one('hr.employee', string='Employee')
    template_id = fields.Many2one('hr.employee.evalution.indication', string='Template', required= True)
    state = fields.Selection([('draft', 'Draft'),
                              ('sent', 'Sent'),
                              ('evaluated', 'Evaluated')], string='State', default='draft', compute = 'compute_group_state')
    point = fields.Float('Point', compute='_compute_point')
    evalution_group_line_ids = fields.One2many('hr.employee.evalution.group', 'evalution_line_id')

    @api.multi
    def _compute_point(self):
        for obj in self:
            average = 0
            for group_line in obj.evalution_group_line_ids:
                average += group_line.point
            obj.point = average/len(obj.evalution_group_line_ids) if len(obj.evalution_group_line_ids) else 0
    @api.multi
    def compute_group_state(self):
        for obj in self:
            state = []
            for line in obj.evalution_group_line_ids:
                state.append(line.state)
            if 'sent' not in state:
                if 'draft' not in state:
                    if 'evaluated' not in state:
                        obj.state = 'draft'
                    else:
                        obj.state = 'evaluated'
                else:
                    obj.state = 'draft'
            else:
                obj.state = 'sent'


class HrEvalutiongroup(models.Model):
    _name = "hr.employee.evalution.group"

    evalution_line_id = fields.Many2one('hr.employee.evalution.line')
    template_id = fields.Many2one('hr.employee.evalution.indication', string='Template', required=True)
    state = fields.Selection([('draft', 'Draft'),
                              ('sent', 'Sent'),
                              ('evaluated', 'Evaluated')], string='State', default='draft', compute = 'compute_group_state')
    point = fields.Float('Point', compute = 'compute_group_point')
    indication_line_ids = fields.One2many('hr.employee.indication.line', 'group_line_id')

    @api.multi
    def compute_group_point(self):
        for obj in self:
            point = 0.00
            for line in obj.indication_line_ids:
                point += line.evalution_point
            obj.point = point

    @api.multi
    def compute_group_state(self):
        for obj in self:
            state = []
            for line in obj.indication_line_ids:
                state.append(line.state)
            if 'sent' not in state:
                if 'draft' not in state:
                    if 'evaluated' not in state:
                        obj.state = 'draft'
                    else:
                        obj.state = 'evaluated'
                else:
                    obj.state = 'draft'
            else:
                obj.state = 'sent'




class HrIndicationLine(models.Model):
    _name = "hr.employee.indication.line"

    group_line_id = fields.Many2one('hr.employee.evalution.group')
    evaluting_id = fields.Many2one('hr.employee.evaluting')
    template_id = fields.Many2one('hr.employee.evalution.indication', string='Template', required=True)
    min_point = fields.Float(string='Min Point')
    max_point = fields.Float(string='Max Point')
    evalution_point = fields.Float(string='Evalution Point')
    state = fields.Selection([('draft', 'Draft'),
                              ('sent', 'Sent'),
                              ('evaluated', 'Evaluated')], string='State', default='draft')

    @api.onchange('template_id')
    def onchange_template_id(self):
        self.min_point = self.template_id.min_point
        self.max_point = self.template_id.max_point




class HrEmployeeEvaluting(models.Model):
    _name = "hr.employee.evaluting"
    _description = "Indication"

    tem_parent_parent_id = fields.Many2one('hr.employee.evalution.indication', string='Template')
    tem_parent_id = fields.Many2one('hr.employee.evalution.indication', string='Parent Template')
    template_id = fields.Many2one('hr.employee.evalution.indication', string='Indication')
    min_point = fields.Float(string='Min Point')
    max_point = fields.Float(string='Max Point')
    employee_lines = fields.One2many('hr.employee.evaluting.line', 'evaluting_id')
    indication_line_id = fields.Many2one('hr.employee.indication.line')

    @api.multi
    def name_get(self):
        result = []
        for tem in self:
            result.append((tem.id, tem.template_id.name))
        return result


class HrEmployeeEvalutingLine(models.Model):
    _name = "hr.employee.evaluting.line"

    evaluting_id = fields.Many2one('hr.employee.evaluting')
    employee_id = fields.Many2one('hr.employee', string='Employee')
    evalution_point = fields.Float(string='Evalution Point')
    indication_line_id = fields.Many2one('hr.employee.indication.line')
    state = fields.Selection([('sent', 'Sent'),
                              ('evaluated', 'Evaluated')], string='State', default='draft')


    @api.multi
    @api.onchange('evalution_point')
    def onchange_evalution_point(self):
        for obj in self:
            if obj.evalution_point < obj.evaluting_id.min_point or obj.evalution_point > obj.evaluting_id.max_point:
                raise UserError(_('Check your evalution_point'))

    @api.multi
    def to_evalution(self):
        for obj in self:
            if obj.evalution_point < obj.evaluting_id.min_point or obj.evalution_point > obj.evaluting_id.max_point:
                raise UserError(_('Check your evalution_point'))

            obj.indication_line_id.state = 'evaluated'
            obj.indication_line_id.evalution_point = obj.evalution_point
            obj.state = 'evaluated'
