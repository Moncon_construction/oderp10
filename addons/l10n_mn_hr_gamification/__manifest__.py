# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/,
# http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
{
    'name': "Mongolian Human Resource Gamification",
    'version': '1.0',
    'depends': [
        'hr_gamification'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
   Human Resource Gamification Additional Features
    """,
    'data': [
        'views/hr_gamification.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}
