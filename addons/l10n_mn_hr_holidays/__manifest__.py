# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/,
# http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
{
    'name': "Mongolian HR Holidays",
    'version': '1.0',
    'depends': [
        'hr_holidays'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
   Human Resource Holidays Features
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/hr_holidays_views.xml',
        'views/hr_views.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}
