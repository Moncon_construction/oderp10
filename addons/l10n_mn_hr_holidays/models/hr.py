# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class Employee(models.Model):
    _inherit = "hr.employee"

    left_leaves_count = fields.Integer('Number of Left Leaves', compute='_compute_left_leaves_count')

    @api.multi
    def _compute_left_leaves_count(self):
        self.env.cr.execute("""
                    SELECT row_number() OVER () AS id, a.employee_id as employee_id, a.department_id as department_id, a.holiday_status_id as holiday_status_id, 
            a.job_id as job_id, SUM(a.allocated_time)-(SUM(a.taken_time)-SUM(a.allocated_time)) as left_time, SUM(a.allocated_time) as allocated_time 
            FROM (SELECT h.id as id, h.type, e.id as employee_id, e.department_id as department_id, e.job_id as job_id, s.id as holiday_status_id,
             CASE WHEN h.type='add' AND h.state='validate' 
             THEN sum(COALESCE(h.number_of_hours_temp,0)) WHEN h.type='remove' 
             THEN sum(COALESCE(h.number_of_hours_temp,0)+(h.number_of_days_temp)*COALESCE(c.work_hours_day, 8)) ELSE 0 END AS taken_time, 
             CASE WHEN h.type='add' AND h.state='validate' 
             THEN sum(COALESCE(h.number_of_hours_temp,0)) ELSE 0 END AS allocated_time 
             FROM hr_holidays h LEFT JOIN hr_holidays_status s on h.holiday_status_id = s.id 
             LEFT JOIN hr_employee e on h.employee_id=e.id 
             LEFT JOIN resource_resource r on e.resource_id=r.id 
             LEFT JOIN res_users u on r.user_id=u.id 
             LEFT JOIN resource_calendar c ON r.calendar_id = c.id 
             WHERE s.limit = 'f' and u.active='t' and h.state='validate' and e.id = %s   
             GROUP BY h.id, e.id,s.id,s.name, h.type, h.state) AS a GROUP BY a.employee_id, a.department_id, a.holiday_status_id, a.job_id 
             HAVING SUM(a.allocated_time)-(SUM(a.taken_time)-SUM(a.allocated_time)) >0 
             AND SUM(a.allocated_time)>=SUM(a.allocated_time)-(SUM(a.taken_time)-SUM(a.allocated_time))
                """ % self.id)
        results = self.env.cr.dictfetchall()
        if results:
            self.left_leaves_count = len(results)
        else:
            self.left_leaves_count = 0
