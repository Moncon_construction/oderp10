# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo import tools


class HrHolidaysLeft(models.Model):
    _name = "hr.holidays.left"
    _description = 'HR Holidays Left'
    _auto = False

    employee_id = fields.Many2one('hr.employee', string='Employee', readonly=True)
    job_id = fields.Many2one('hr.job', related='employee_id.job_id', string='Job', readonly=True, store=True)
    department_id = fields.Many2one('hr.department', related='employee_id.department_id', string='Department',
                                    readonly=True, store=True)
    holiday_status_id = fields.Many2one("hr.holidays.status", string="Leave Type", readonly=True)
    allocated_time = fields.Float(string='Allocated Time', readonly=True)
    left_time = fields.Float(string='Left Time', readonly=True)

    def init(self):
        tools.sql.drop_view_if_exists(self.env.cr, 'hr_holidays_left')
        self.env.cr.execute("""
            CREATE or REPLACE view hr_holidays_left as 
            (SELECT row_number() OVER () AS id, a.employee_id as employee_id, a.department_id as department_id, a.holiday_status_id as holiday_status_id, 
            a.job_id as job_id, SUM(a.allocated_time)-(SUM(a.taken_time)-SUM(a.allocated_time)) as left_time, SUM(a.allocated_time) as allocated_time 
            FROM (SELECT h.id as id, h.type, e.id as employee_id, e.department_id as department_id, e.job_id as job_id, s.id as holiday_status_id,
             CASE WHEN h.type='add' AND h.state='validate' 
             THEN sum(COALESCE(h.number_of_hours_temp,0)) WHEN h.type='remove' AND EXISTS (SELECT 1 FROM hr_contract ct WHERE ct.employee_id = e.id AND ct.active='t')
             THEN sum(COALESCE(h.number_of_hours_temp,0)) ELSE sum(COALESCE(h.number_of_hours_temp,0)+(h.number_of_days_temp)*COALESCE(c.work_hours_day, 8)) END AS taken_time, 
             CASE WHEN h.type='add' AND h.state='validate' 
             THEN sum(COALESCE(h.number_of_hours_temp,0)) ELSE 0 END AS allocated_time 
             FROM hr_holidays h LEFT JOIN hr_holidays_status s on h.holiday_status_id = s.id 
             LEFT JOIN hr_employee e on h.employee_id=e.id 
             LEFT JOIN resource_resource r on e.resource_id=r.id 
             LEFT JOIN res_users u on r.user_id=u.id 
             LEFT JOIN resource_calendar c ON r.calendar_id = c.id 
             WHERE s.limit = 'f' and u.active='t' and h.state='validate'  
             GROUP BY h.id, e.id,s.id,s.name, h.type, h.state) AS a GROUP BY a.employee_id, a.department_id, a.holiday_status_id, a.job_id 
             HAVING SUM(a.allocated_time)-(SUM(a.taken_time)-SUM(a.allocated_time)) >0 
             AND SUM(a.allocated_time)>=SUM(a.allocated_time)-(SUM(a.taken_time)-SUM(a.allocated_time)));
             
             CREATE OR REPLACE RULE update_hr_holidays_left AS
              ON UPDATE TO hr_holidays_left 
              DO INSTEAD
              UPDATE hr_employee SET job_id = new.job_id 
              WHERE hr_employee.id = old.id;
        """)

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            name = "%(name)s (%(count)s)" % {
                'name': record.holiday_status_id.name,
                'count': _('%g remaining out of %g') % (
                 record.left_time or 0.0, record.allocated_time or 0.0)
            }
            res.append((record.id, name))
        return res







