# -*- coding: utf-8 -*-

{
    'name': "Mongolian HR - Hour balance",
    'version': '1.0',
    'depends': [
        'l10n_mn_hr_attendance',
        'hr_holidays',
        'hr_timesheet_sheet',
        'hr_timesheet_attendance',
        'l10n_mn_hr_contract',
        'l10n_mn_report',
        'l10n_mn_account_period',
        'mail',
        'l10n_mn_workflow_config',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
      Employee Hour balance
    """,
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'wizard/hour_balance_import_view.xml',
        'wizard/hour_balance_print_view.xml',
        'views/res_company_view.xml',
        'views/hour_balance_view.xml',
        'views/hour_balance_config_settings_view.xml',
        'views/hr_holidays_view.xml',
        'views/hr_holidays_configuration_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
