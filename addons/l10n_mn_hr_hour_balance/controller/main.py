# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo.addons.mail.controllers.main import MailController
from odoo import _

from odoo import http
from odoo.exceptions import RedirectWarning


class HrHolidaysControllerOderp(http.Controller):
    @http.route('/hr_holidays/validate_holiday', type='http', auth='user', methods=['GET'])
    def hr_holidays_validate(self, res_id, token):
        record = http.request.env['hr.holidays'].sudo().browse(int(res_id))
        if record and record.validate_token == token and record.is_token_valid:
            try:
                record.action_approve()
                record.is_token_valid = False
            except Exception:
                return MailController._redirect_to_messaging()
        else:
            return http.redirect_with_hash("/web")
        return MailController._redirect_to_record('hr.holidays', int(res_id))

    @http.route('/hr_holidays/refuse_holiday', type='http', auth='user', methods=['GET'])
    def hr_holidays_refuse(self, res_id, token):
        record = http.request.env['hr.holidays'].sudo().browse(int(res_id))
        if record and record.validate_token == token and record.is_token_valid:
            try:
                record.action_refuse()
                record.is_token_valid = False
            except Exception:
                return MailController._redirect_to_messaging()
        else:
            return http.redirect_with_hash("/web")
        return MailController._redirect_to_record('hr.holidays', int(res_id))
