# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/,
# http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import res_company
import hr_employee
import hour_balance_config_settings
import hour_balance
import hr_holidays
import hr_holidays_configuration
import workflow_config
import account_period
import hr_attendance