# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class AccountPeriod(models.Model):
    _inherit = 'account.period'

    @api.multi
    def unlink(self):
        for period in self:
            balance_ids = self.env['hour.balance'].search([('period_id', '=', period.id)])
            if balance_ids:
                raise UserError(_('You cannot delete a period related to an hour balance.'))
        res = super(AccountPeriod, self).unlink()
        return res