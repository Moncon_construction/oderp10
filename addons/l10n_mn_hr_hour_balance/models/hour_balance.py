# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime, timedelta
from dateutil import rrule
from dateutil.relativedelta import relativedelta
import logging
from lxml import etree
import pytz

from odoo import _, api, fields, models
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import UserError
from odoo.osv.orm import setup_modifiers
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT


_logger = logging.getLogger(__name__)

STATE_SELECTION = [
    ('draft', 'Draft'),
    ('timesheet_generated', 'Timesheet Generated'),
    ('sent_to_checker', 'Sent to Checker'),
    ('sent', 'Sent'),
    ('done', 'Done'),
    ('cancel', 'Refused'),
]

SALARY_TYPE = [
    ('advance_salary', 'Advance Salary'),
    ('last_salary', 'Last Salary')
]


class HourBalance(models.Model):
    _name = "hour.balance"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = "Hour balance"
    
    @api.multi
    @api.depends('company_id')
    def calc_show_many_contract(self):
        for balance in self:
            if balance.company_id.hr_contract_number == 2:
                balance.show_many_contract = True
            else:
                balance.show_many_contract = False   

    @api.depends('department_id', 'period_id', 'salary_type')
    def compute_balance_name(self):
        for record in self:
            name = ''
            name_change = False
            if record.department_id:
                name += _("%s departments's ") % record.department_id.name
                name_change = True
            if record.period_id:
                name += _("%s month's ") % record.period_id.name
                name_change = True
            if record.salary_type:
                name += _("%s hour balance") % dict(self._fields['salary_type']._description_selection(self.env)).get(record.salary_type)
                name_change = True
            if name_change:
                record.name = name

    name = fields.Char(string='Name', size=150, compute='compute_balance_name', default='compute_balance_name', store=True)
    period_id = fields.Many2one('account.period', string="Period", domain=[('state', 'not in', ['done'])])
    company_id = fields.Many2one('res.company', string='Company', states={'draft': [('readonly', False)], 'cancel': [('readonly', False)]}, default=lambda self: self.env.user.company_id)
    department_id = fields.Many2one('hr.department', 'Department')
    user_id = fields.Many2one('res.users', 'Users')
    advance_salary_id = fields.Many2one('hour.balance', 'Advance Salary', domain=[('salary_type', '=', ['advance_salary'])])
    salary_type = fields.Selection(SALARY_TYPE, string='Salary type', required=True, default='advance_salary')
    no_advance_balance = fields.Boolean('No Advance Balance')
    date_from = fields.Date(states={'draft': [('readonly', False)], 'cancel': [('readonly', False)]}, required=True, string="Date from")
    date_to = fields.Date(states={'draft': [('readonly', False)], 'cancel': [('readonly', False)]}, required=True, string="Date to")
    balance_date_from = fields.Date(states={'draft': [('readonly', False)], 'cancel': [('readonly', False)]}, required=True, string="Balance date from")
    balance_date_to = fields.Date(states={'draft': [('readonly', False)], 'cancel': [('readonly', False)]}, required=True, string="Balance date to")
    state = fields.Selection(STATE_SELECTION, string='Status', default='draft', index=True, track_visibility='always', required=True)
    balance_line_ids = fields.One2many('hour.balance.line', 'balance_id', string='Employee hour balance')
    show_many_contract = fields.Boolean(string='Show many contract', compute='calc_show_many_contract')
    
    @api.onchange('salary_type')
    def onchange_salary_type(self):
        if self.salary_type == 'advance_salary':
            self.no_advance_balance = False

    @api.onchange('advance_salary_id')
    def onchange_advance_salary(self):
        balance = None
        if self.advance_salary_id:
            balance = self.env['hour.balance'].browse(self.advance_salary_id.id)
            if balance:
                if balance.department_id:
                    self.department_id = balance.department_id.id
                self.period_id = balance.period_id
                self.date_from = balance.date_from
                self.date_to = balance.date_to
                self.balance_date_from = datetime.strptime(balance.balance_date_to, '%Y-%m-%d') + timedelta(days=1)
                self.balance_date_to = balance.date_to

    @api.onchange('period_id')
    def onchange_period_id(self):
        if self.period_id:
            period = self.env['account.period'].search([('id','=',self.period_id.id)])
            self.date_from = period.date_start
            self.date_to = period.date_stop      
     
    @api.multi
    def send(self):
        for hour_balance in self:
            if hour_balance.state == 'timesheet_generated':
                for line in hour_balance.balance_line_ids:
                    line.write({'state': 'sent'})

                self.write({'state': 'sent_to_checker'})
            else:
                for line in hour_balance.balance_line_ids:
                    line.write({'state': 'sent'})
                self.write({'state': 'sent'})

    @api.multi
    def approve(self):
        for hour_balance in self:
            for line in hour_balance.balance_line_ids:
                line.write({'state': 'done'})

        self.write({'state': 'done'})

    @api.multi
    def cancel(self):
        for hour_balance in self:
            for line in hour_balance.balance_line_ids:
                line.write({'state': 'cancel'})

        self.write({'state': 'cancel'})

    @api.multi
    def set_to_draft(self):
        for hour_balance in self:
            for line in hour_balance.balance_line_ids:
                line.write({'state': 'draft'})

        self.write({'state': 'draft'})

    def is_module_installed(self, module_name):
        self._cr.execute("SELECT id FROM ir_module_module WHERE name = '%s' AND state IN ('installed', 'to upgrade')" % module_name)
        results = self._cr.dictfetchall()

        if results and len(results) > 0:
            return True
        else:
            return False
        
    def can_calc_reg_day(self, employee_id, contract_id):
        if self.salary_type == 'last_salary' and employee_id.state_id and employee_id.state_id.type == 'maternity':
            return False
        return True
        
    @api.multi
    def calc_reg_day(self, date_from, date_end, employee, contract, is_balance=False):  # @UnusedVariable
        self.ensure_one()

        working_day = 0
        working_hour = 0
        leaves = []

        start_dt = get_day_by_user_timezone(date_from, self.env.user)
        end_dt = get_day_by_user_timezone(date_end, self.env.user)
        
        if contract.working_hours:
            leaves = contract.working_hours.get_leave_intervals(resource_id=contract.working_hours.id, start_datetime=start_dt, end_datetime=end_dt)
        else:
            raise UserError(_("Working hours does not configured for %s") % employee.name)
        
        int = 0
        for day in rrule.rrule(rrule.DAILY, dtstart=start_dt,
                               until=(end_dt).replace(hour=0, minute=0, second=0),
                               byweekday=contract.working_hours.get_weekdays()):
            day_start_dt = day.replace(hour=0, minute=0, second=0)
            day_end_dt = day.replace(hour=23, minute=59, second=59)
                
            hours = contract.working_hours.get_working_hours_of_date(start_dt=day_start_dt, end_dt=day_end_dt,
                                                                                 compute_leaves=True, resource_id=contract.working_hours.id,
                                                                                 default_interval=None)

            if hours > 0:
                working_day += 1
                working_hour += hours
                
        return {
            'working_day': working_day,
            'working_hour': working_hour,
            'employee_leave': len(leaves)
        }

    @api.model
    def get_total_attendances(self, employee, date_from, date_to):
        st_date = date_from
        end_date = date_to
        
        domain = [
            ('check_in', '!=', False), ('check_out', '!=', False),
            ('employee_id', '=', employee.sudo().id)
        ]
        domain.extend(get_duplicated_day_domain('check_in', 'check_out', st_date, end_date, self.env.user))
        
        attendances = self.env['hr.attendance'].search(domain)
        return attendances
    
    def get_linked_objects(self, object, employee, st_date, end_date):
        if object != 'hr.holidays':
            return self.env['object'].sudo().search([])
        
        domain = [('employee_id', '=', employee.id), ('type', '=', 'remove'), ('state', '=', 'accepted')]
        if 'balance_type' in self._context.keys():
            domain.append(('holiday_status_id.balance_type', '=', self._context['balance_type']))
            domain.append(('holiday_status_id.is_nohon_amrakh', '=', self._context['is_nohon_amrakh']))
        domain.extend(get_duplicated_day_domain('date_from', 'date_to', st_date, end_date, self.env.user))
        
        return self.env[object].sudo().search(domain)
    
    def get_linked_object_time_intervals(self, object, employee, st_date, end_date):
        if object != 'hr.holidays':
            return []
        
        time_intervals = []
        st_date = str(get_display_day_to_user_day(st_date, self.env.user))
        end_date = str(get_display_day_to_user_day(end_date, self.env.user))
        linked_objects = self.get_linked_objects(object, employee, st_date, end_date)
        for obj in linked_objects:
            start_date = datetime.strptime(obj.date_from, DEFAULT_SERVER_DATETIME_FORMAT)
            end_date = datetime.strptime(obj.date_to, DEFAULT_SERVER_DATETIME_FORMAT)
            time_intervals.append((start_date, end_date))
        
        return time_intervals
    
    def get_total_hours_by_object(self, object, employee, contract, date_from, date_to):
        self.ensure_one()
        if not contract.working_hours:
            raise UserError(_("Working hours does not configured for %s") % employee.name)
        
        hours = 0
        st_date = str(get_display_day_to_user_day(date_from, self.env.user))
        end_date = str(get_display_day_to_user_day(date_to, self.env.user))
        time_intervals = self.get_linked_object_time_intervals(object, employee, st_date, end_date)
        
        if time_intervals:
            for time_interval in time_intervals:
                time_interval_hours = contract.working_hours.get_working_hours(start_dt=time_interval[0], end_dt=time_interval[1], compute_leaves=True, resource_id=None, default_interval=None)
                if time_interval_hours > 0:
                    hours += time_interval_hours     
                    
        return hours

    def get_total_attendance_hours_days(self, employee, date_from, date_to):
        self.ensure_one()
        total_hours, total_days = 0, 0
        atts = self.get_total_attendances(employee, date_from, date_to)
        if atts and len(atts) > 0:
            for attendance in atts:
                total_hours += attendance.total_attendance_hours
                if attendance.check_worked_hours_can_be_day():
                    total_days += 1

        return total_hours, total_days
    
    def get_available_employees(self):
        employee_obj = self.env['hr.employee'].sudo()
        employees = []
        
        for obj in self:
            # 'Урьдчилгаа цалин' төрөлтэй цагийн баланс үед 'Жирэмсэн' болон 'Ажлаас гарсан' төлөвтэй ажилчдыг НЭМЭХГҮЙ байх
            ignored_states = ['resigned', 'maternity'] if obj.salary_type == 'advance_salary' else ['resigned']
            ignored_statuses = self.env['hr.employee.status'].sudo().search([('type', 'in', ignored_states)])
            
            # Update of balance sheet information
            domain = [('active', '=', True), ('company_id', '=', obj.company_id.id)]
            if ignored_statuses and len(ignored_statuses) > 0:
                domain.append(('state_id', 'not in', ignored_statuses.sudo().ids))
            if obj.department_id:
                domain.append(('department_id', 'child_of', [obj.department_id.id]))
            
            employees.extend(employee_obj.search(domain))
            
        return employees
    
    def can_insert_balance_line(self, employee_id, contract_id, worked_hour):
        can_insert = False
            
        # Ажилласан цаг 0-с их үед цагийн балансын мөр үүсгэх
        if worked_hour > 0: 
            can_insert = True
        # Сүүл цалин бодож байгаа үед 'Жирэмсэн' төлөвтэй ажилчдын мөрийг ажилласан цагаас үл хамааран үүсгэх
        elif self.salary_type == 'last_salary' and employee_id.state_id and employee_id.state_id.type == 'maternity':
            can_insert = True
        elif contract_id.hour_balance_calculate_type == 'is_salary_complete':
            can_insert = True

        return can_insert
    
    def get_balance_dates(self, contract_id, check_contract_date=False, is_balance=True):
        self.ensure_one()
        
        if is_balance:
            date_from = self.balance_date_from
            date_to = self.balance_date_to
        else:
            date_from = self.date_from
            date_to = self.date_to
        
        # Гэрээний үргэлжлэх хугацаанд тухайн балансын огноо хамаарч байгаа эсэхийг тооцоолно
        if check_contract_date:
            contract_date_from = contract_id.date_start or contract_id.trial_date_start
            if contract_date_from:
                contract_date_to = contract_id.date_end if contract_id.date_start else contract_id.trial_date_end
                if not contract_date_to:
                    contract_date_to = date_to if date_to > contract_date_from else contract_date_from
                date_from, date_to = get_duplication_interval(contract_date_from, contract_date_to, date_from, date_to)
        
        if date_from and date_to:
            date_from = datetime.strptime(date_from, DEFAULT_SERVER_DATE_FORMAT).replace(hour=0, minute=0, second=0).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            date_to = datetime.strptime(date_to, DEFAULT_SERVER_DATE_FORMAT).replace(hour=23, minute=59, second=59).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        
        return date_from, date_to
    
    def get_active_hours(self, values):
        self.ensure_one()
        active_hours = values.get('worked_hour', 0) + values.get('working_hours_at_weekend', 0) + values.get('attendance_hour', 0)
        active_hours += (values.get('paid_holiday', 0) + values.get('celebration_hours', 0))
        return active_hours

    def get_hour_rehabilitated_add_holiday(self, employee_id,contract, date_from, date_to ):
        hours = 0
        st_date = str(get_display_day_to_user_day(date_from, self.env.user))
        end_date = str(get_display_day_to_user_day(date_to, self.env.user))

        domain = [('employee_id', '=', employee_id.id), ('type', '=', 'add'), ('state', '=', 'accepted')]
        domain.append(('holiday_status_id.balance_type', '=', 'paid'))
        domain.append(('holiday_status_id.is_nohon_amrakh', '=', True))
        domain.extend(get_duplicated_day_domain('add_date_from', 'add_date_to', st_date, end_date, self.env.user))

        hr_holidays = self.env['hr.holidays'].sudo().search(domain)

        if hr_holidays:
            for time_interval in hr_holidays:
                if time_interval.number_of_hours_temp > 0:
                    hours += time_interval.number_of_hours_temp
        return hours

    def get_balance_line_values(self, employee_id, contract_id, check_contract_date=False):
        self.ensure_one()
        
        date_from, date_to = self.get_balance_dates(contract_id, check_contract_date=check_contract_date)
        period_date_from, period_date_to = self.get_balance_dates(contract_id, check_contract_date=check_contract_date, is_balance=False)
        if not (date_from and date_to) or not (period_date_from and period_date_to):
            return {}
        
        reg_hours = self.calc_reg_day(period_date_from, period_date_to, employee_id, contract_id)
        balance_reg_hours = self.calc_reg_day(date_from, date_to, employee_id, contract_id, is_balance=True)

        if self.can_calc_reg_day(employee_id, contract_id):
            reg_day = reg_hours['working_day']
            reg_hour = reg_hours['working_hour']
            balance_reg_day = balance_reg_hours['working_day']
            balance_reg_hour = balance_reg_hours['working_hour']
        else:
            reg_day, reg_hour, balance_reg_day, balance_reg_hour = 0, 0, 0, 0
        
        if date_from and date_to:
            attendance_hour, total_attendance_days = self.get_total_attendance_hours_days(employee_id, date_from, date_to) # Нийт ирцийн цаг/өдөр - Амралт нтр тооцохгүй. Ирсэн огноо болон гарсан огнооноос цайны цагийг хассан хугацаа байна
            
            paid_holiday = self.with_context({'balance_type': 'paid', 'is_nohon_amrakh': False}).get_total_hours_by_object('hr.holidays', employee_id, contract_id, date_from, date_to)
            unpaid_holiday = self.with_context({'balance_type': 'unpaid', 'is_nohon_amrakh': False}).get_total_hours_by_object('hr.holidays', employee_id, contract_id, date_from, date_to)
            annual_leave = self.with_context({'balance_type': 'annual_leave', 'is_nohon_amrakh': False}).get_total_hours_by_object('hr.holidays', employee_id, contract_id, date_from, date_to)
            sick_leave_holiday = self.with_context({'balance_type': 'sick_leave', 'is_nohon_amrakh': False}).get_total_hours_by_object('hr.holidays', employee_id, contract_id, date_from, date_to)
            rehabilitated_holiday = self.with_context({'balance_type': 'paid', 'is_nohon_amrakh': True}).get_total_hours_by_object('hr.holidays', employee_id, contract_id, date_from, date_to)
            rehabilitated_add_holiday = self.get_hour_rehabilitated_add_holiday(employee_id, contract_id, date_from, date_to)
        else:
            attendance_hour, total_attendance_days = 0, 0
            paid_holiday, unpaid_holiday, annual_leave, sick_leave_holiday, rehabilitated_holiday, rehabilitated_add_holiday = 0, 0, 0, 0, 0
            
        values = {
            'origin': 'by_attendance',
            'employee_id': employee_id.id,
            'contract_id': contract_id.sudo().id,
            'employee_reg_number': employee_id.ssnid,
            'employee_last_name': employee_id.last_name,
            'department_id': self.department_id.id,
            'balance_id': self.id,
            'salary_type': self.salary_type,
            'reg_day': reg_day if reg_day > 0 else 0,
            'reg_hour': reg_hour if reg_hour > 0 else 0,
            'balance_reg_day': balance_reg_day if balance_reg_day > 0 else 0,
            'balance_reg_hour': balance_reg_hour if balance_reg_hour > 0 else 0,
            'attendance_hour': attendance_hour,
            'total_attendance_days': total_attendance_days,
            'paid_holiday': paid_holiday,
            'unpaid_holiday': unpaid_holiday,
            'annual_leave': annual_leave,
            'sick_leave': sick_leave_holiday,
            'rehabilitated_holiday': rehabilitated_holiday,
            'rehabilitated_add_holiday': rehabilitated_add_holiday,
        }
        
        return values
    
    def update_hour_balance_lines(self):
        self.ensure_one()
        hour_balance_line_obj = self.env['hour.balance.line']
        hour_balance_line_line_obj = self.env['hour.balance.line.line']

        # Идэвхтэй гэрээгүй ажилтан бүхий мөрүүдийг цуглуулах & Аль хэдийн мөр нь үүссэн ажилтнуудыг цуглуулах
        existen_employees = []
        deletable = []
        for hour_balance_line in hour_balance_line_obj.search([('balance_id', '=', self.id)]):
            if not hour_balance_line.employee_id or (not hour_balance_line.employee_id.active) or not hour_balance_line.employee_id.contract_id or (hour_balance_line.employee_id.contract_id and not hour_balance_line.employee_id.contract_id.active): # hour_balance_line.employee_id.contract_id.state != 'open' шалгалтыг түр авав. Хэрэгтэй бол нэмэх
                deletable.append(str(hour_balance_line.id))
            elif hour_balance_line.employee_id not in existen_employees:
                existen_employees.append(hour_balance_line.employee_id)
              
        # Мөрт харгалзах ажилтанд идэвхтэй гэрээ бхгүй бол цагийн балансаас мөрүүдийг устгах  
        if deletable:
            ids = ", ".join(deletable)
            self._cr.execute("""
                DELETE FROM hour_balance_line_line WHERE hour_balance_line_id in (%s);
                DELETE FROM hour_balance_line WHERE id in (%s);
            """ % (ids, ids))

        # Тухайн ажилтны хөдөлмөрийн гэрээнүүдээс яг одоо мөрдөгдөж байгаа гэрээний дагуу цагийн балансын мөрийг шинэчилж | үүсгэж байна.
        if self.company_id.hr_contract_number == 1:
            for employee in self.get_available_employees():
                contract = employee.contract_id
                if contract and contract.active:
                    values = self.get_balance_line_values(employee, contract)
                    if self.can_insert_balance_line(employee, contract, self.get_active_hours(values)):
                        if employee not in existen_employees:
                            hour_balance_line_obj.create(values)
                        else:
                            existen_lines = hour_balance_line_obj.search([('balance_id', '=', self.id), ('employee_id', '=', employee.id),('origin', '=', 'by_attendance')])
                            for line in existen_lines:
                                line.write(values)
        else:
            # Хэрвээ нэгээс олон гэрээгээр баланс тооцох бол эхлээд бүх ажилтны гэрээнд давхцал байгаа эсэхийг бөөнөөр нь эхлээд шалгах
            employee_ids = self.env['hr.employee']
            for emp in self.get_available_employees():
                employee_ids |= emp
            if employee_ids:
                employee_ids.check_contract_duplication()
                
            # Тухайн ажилтны балансын хугацаа дахь бүх идэвхитэй хөдөлмөрийн гэрээнүүдийн дагуу цагийн балансын цагийг бодож байна.
            updatable_balance_line_ids = self.env['hour.balance.line']
            deletable_line_lines = []
            for employee in self.get_available_employees():
                employee_reg_number = employee.ssnid
                employee_last_name = employee.last_name
                query = """SELECT id 
                              FROM hr_contract 
                              WHERE active='t' and  employee_id = %s and (date_start BETWEEN %s AND %s or date_start <= %s)"""
                self._cr.execute(query, (employee.id, self.balance_date_from, self.balance_date_to, self.balance_date_to))
                active_contracts = map(lambda x: x[0], self._cr.fetchall())
                balance_line = False
                if employee not in existen_employees:
                    balance_line = hour_balance_line_obj.create({
                        'origin': 'by_attendance',
                        'employee_id': employee.id,
                        'employee_reg_number': employee_reg_number,
                        'employee_last_name': employee_last_name,
                        'department_id': self.department_id.id,
                        'balance_id': self.id,
                    })
                else:
                    existen_lines = hour_balance_line_obj.search([('balance_id', '=', self.id), ('employee_id', '=', employee.id), ('origin', '=', 'by_attendance')])
                    for line in existen_lines:
                        line.write({
                            'employee_reg_number': employee_reg_number,
                            'employee_last_name': employee_last_name,
                            'department_id': self.department_id.id,
                        })
                        if line.line_ids:
                            deletable_line_lines.extend(line.mapped('line_ids').mapped('id'))
                        balance_line = line
                     
                if balance_line:   
                    for contract_id in active_contracts:
                        contract = self.env['hr.contract'].search([('id', '=', contract_id)])
                        values = self.get_balance_line_values(employee, contract, check_contract_date=True)
                        if self.can_insert_balance_line(employee, contract, self.get_active_hours(values)):
                            values['hour_balance_line_id'] = balance_line.id
                            values.pop('origin', None)
                            new_line = hour_balance_line_line_obj.create(values)
                            if balance_line not in updatable_balance_line_ids:
                                updatable_balance_line_ids |= balance_line
                
            if deletable_line_lines:
                self._cr.execute("""
                    DELETE FROM hour_balance_line_line WHERE id IN (%s)
                """ % ", ".join(str(id) for id in deletable_line_lines))
                    
            #Бүх гэрээнүүдийн нийт цагуудыг үндсэн цагийн балансын мөр лүү шинэчилж байна.
            updatable_balance_line_ids.set_values_from_line()

        return True
    

class HourBalanceLine(models.Model):
    _name = "hour.balance.line"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = "Hour balance line"
         
    @api.one
    @api.depends('annual_leave', 'sick_leave', 'unpaid_holiday', 'missed_hour', 'unworked_leaved_hour')
    def calc_total_unworked_hour(self):
        self.total_unworked_hour = self.annual_leave + self.sick_leave + self.unpaid_holiday + self.missed_hour + self.unworked_leaved_hour
    
    company_id = fields.Many2one(related='balance_id.company_id', string='Company', store=True, readonly=True, states={'draft': [('readonly', False)], 'cancel': [('readonly', False)]}, default=lambda self: self.env.user.company_id)
    balance_id = fields.Many2one('hour.balance', string='Hour balance', ondelete='cascade', index=True)
    employee_id = fields.Many2one('hr.employee', string="Employee First Name", required=True)
    contract_id = fields.Many2one('hr.contract', string='Contract', ondelete='set null', index=True)
    employee_number = fields.Integer(string="№")
    employee_reg_number = fields.Char(string="Employee Register Number")
    employee_last_name = fields.Char(string="Employee Last Name")
    identification_id = fields.Char(related='employee_id.identification_id', string='Identification Number')
    period_id = fields.Many2one(related='balance_id.period_id', string="Period", store=True)
    salary_type = fields.Selection(related='balance_id.salary_type', string='Salary type', store=True)
    department_id = fields.Many2one('hr.department', string="Department")
    
    balance_reg_day = fields.Float(string='Balance regular /days/')
    balance_reg_hour = fields.Float(string='Balance regular /hours/')
    reg_day = fields.Float(string='Regular /days/')
    reg_hour = fields.Float(string='Regular /hours/', digits=(4, 2))
    
    timesheet_hour = fields.Float(string='Timesheet /hours/', digits=(4, 2))
    
    total_attendance_days = fields.Float(string='Total Attendance /days/')
    attendance_hour = fields.Float(string='Total Attendance /hours/')
    
    worked_day = fields.Float(string='Working day /days/')
    worked_hour = fields.Float(string='Working day /hours/', digits=(4, 2))
    celebration_days = fields.Float(string='Celebration day /days/')
    celebration_hours = fields.Float(string='Celebration day /hours/')
    working_days_at_weekend = fields.Float(string='Weekend /days/')
    working_hours_at_weekend = fields.Float(string='Weekend /hours/')
    total_worked_day = fields.Float(string='Total worked /days/', readonly=True, compute='compute_total_worked_day')
    total_worked_hour = fields.Float(string='Total worked /hours/', readonly=True, compute='compute_total_worked_hour', digits=(4, 2))
    
    business_trip_hour = fields.Float(string='Business trip /hours/', digits=(4, 2))
    out_working_hour = fields.Float(string='Out working /hours/', digits=(4, 2))
    total_training_hours = fields.Float(string='Training /hours/', digits=(4, 2)) 
    
    overtime = fields.Float(string='Overtime by Attendance', digits=(4, 2))
    confirmed_overtime = fields.Float(string='Confirmed Overtime', digits=(4,2))
    overtime_holiday = fields.Float(string='Overtime holiday', digits=(4, 2))
    confirmed_overtime_holiday = fields.Float(string='Confirmed Overtime Holiday', digits=(4,2))
    
    paid_holiday = fields.Float(string='Paid holiday /hours/', digits=(4, 2))
    annual_leave = fields.Float(string='Annual leave /hours/')
    sick_leave = fields.Float(string='Sick leave /hours/', digits=(4, 2))
    unpaid_holiday = fields.Float(string='Unpaid holiday /hours/', digits=(4, 2))
    
    delay_hour = fields.Float(string='Delay time /hours/', digits=(4, 2))
    missed_hour = fields.Float(string='Missed hour /hours/', compute='compute_missed_hour', store=True, readonly=False, digits=(4, 2))
    unworked_leaved_hour = fields.Float(string='Un worked/Leave work /hours/', digits=(4, 2))
    lag_hour = fields.Float(string='Lag /hours/', digits=(4, 2))
    lag_minute = fields.Float(string='Lag /minutes/', digits=(4, 2))
    total_unworked_hour = fields.Float(string='Total unworked /hours/', compute='calc_total_unworked_hour', digits=(4, 2))
    
    description = fields.Char(string='Description')
    state = fields.Selection(STATE_SELECTION, string='Status', default='draft', index=True, track_visibility='always', required=True)
    line_ids = fields.One2many('hour.balance.line.line','hour_balance_line_id', string='Lines')
    show_many_contract = fields.Boolean(related='balance_id.show_many_contract',string='Show many contract')
    origin = fields.Selection([
                                ('create_by_hand', 'Create by Hand'),
                                ('by_attendance', 'Create by Attendance'),
                                ('from_import_file', 'From Import File'),
                            ], string='Origin', default='create_by_hand', track_visibility='always')

    @api.multi
    def unlink(self):
        balance_ids = []
        for balance_line in self:
            if balance_line.balance_id.state in ('done', 'sent_to_checker', 'sent', 'cancel'):
                raise UserError(_('You can only delete hour balance in draft state. You can delete the hour balance after drafting it.'))
            balance_ids.append(balance_line.balance_id.id)
        res = super(HourBalanceLine, self).unlink()
        self._reorder(balance_ids)
        return res

    @api.model
    @api.multi
    def _reorder(self, balance_ids):
        for balance in balance_ids:
            sequence = 1
            for line in self.env['hour.balance'].browse(balance).balance_line_ids:
                line.employee_number = sequence
                sequence += 1

    @api.model
    def create(self, vals):
        res = super(HourBalanceLine, self).create(vals)
        if 'employee_number' not in vals:
            self._cr.execute("SELECT max(employee_number) FROM hour_balance_line WHERE balance_id = %s" % res.balance_id.id)
            result = self._cr.fetchall()
            if result:
                employee_number = result[0][0]
                if not employee_number:
                    res.employee_number = 1
                else:
                    res.employee_number = employee_number + 1
        return res
    
    def compute_total_worked_day(self):
        for obj in self:
            obj.total_worked_day = obj.worked_day + obj.celebration_days + obj.working_days_at_weekend
            
    def compute_total_worked_hour(self):
        for obj in self:
            obj.total_worked_hour = obj.worked_hour + obj.celebration_hours + obj.working_hours_at_weekend
            
    @api.depends('balance_reg_hour', 'total_worked_hour', 'paid_holiday', 'unpaid_holiday', 'annual_leave', 'sick_leave', 'out_working_hour', 'business_trip_hour', 'total_training_hours', 'delay_hour', 'unworked_leaved_hour', 'line_ids.missed_hour')
    def compute_missed_hour(self):
        # ТАСЛАЛТ = БАЛАНСЫН АЖИЛЛАВАЛ ЗОХИХ /ЦАГ/ - ТОМИЛОЛТ – ГАДУУР АЖИЛ – СУРГАЛТ /ЦАГ/ - ЦАЛИНТАЙ ЧӨЛӨӨ /ЦАГ/ - ЭЭЛЖИЙН АМРАЛТ  /ЦАГ/ - ӨВЧНИЙ ЧӨЛӨӨ  /ЦАГ/ - ЦАЛИНГҮЙ ЧӨЛӨӨ  /ЦАГ/ - НИЙТ АЖИЛЛАСАН ЦАГ - Сул зогсолт - Ажиллаагүй болон ажлаас гарсан өдөр
        for obj in self:
            if obj.company_id.hr_contract_number == 1:
                holiday_hours = obj.paid_holiday + obj.unpaid_holiday + obj.annual_leave + obj.sick_leave + obj.rehabilitated_holiday
                missed_hour = obj.reg_hour - obj.worked_hour - obj.business_trip_hour - obj.delay_hour - holiday_hours - obj.lag_hour
                obj.missed_hour = max(missed_hour, 0)
            else:
                obj.missed_hour = sum(obj.mapped('line_ids').mapped('missed_hour'))
            
    def set_values_from_line(self):
        for obj in self:
            reg_day, reg_hour = 0, 0
            balance_reg_day, balance_reg_hour = 0, 0
            attendance_hour, total_attendance_days = 0, 0
            paid_holiday, unpaid_holiday, annual_leave, sick_leave = 0, 0, 0, 0
            
            for line_id in obj.line_ids:
                reg_day += line_id.reg_day
                reg_hour += line_id.reg_hour
                balance_reg_day += line_id.balance_reg_day
                balance_reg_hour += line_id.balance_reg_hour
                attendance_hour += line_id.attendance_hour
                total_attendance_days += line_id.total_attendance_days
                paid_holiday += line_id.paid_holiday
                unpaid_holiday += line_id.unpaid_holiday
                annual_leave += line_id.annual_leave
                sick_leave += line_id.sick_leave
            
            obj.reg_day = reg_day
            obj.reg_hour = reg_hour
            obj.balance_reg_day = balance_reg_day
            obj.balance_reg_hour = balance_reg_hour
            obj.attendance_hour = attendance_hour
            obj.total_attendance_days = total_attendance_days
            obj.paid_holiday = paid_holiday
            obj.unpaid_holiday = unpaid_holiday
            obj.annual_leave = annual_leave
            obj.sick_leave = sick_leave
            

class HourBalanceLineLine(models.Model):
    _name = "hour.balance.line.line"
    
    def compute_total_worked_day(self):
        for obj in self:
            obj.total_worked_day = obj.worked_day + obj.celebration_days + obj.working_days_at_weekend
    
    def compute_total_worked_hour(self):
        for obj in self:
            obj.total_worked_hour = obj.worked_hour + obj.celebration_hours + obj.working_hours_at_weekend
    
    @api.depends('annual_leave', 'sick_leave', 'unpaid_holiday', 'missed_hour', 'unworked_leaved_hour')
    def calc_total_unworked_hour(self):
        for obj in self:
            obj.total_unworked_hour = obj.annual_leave + obj.sick_leave + obj.unpaid_holiday + obj.missed_hour + obj.unworked_leaved_hour

    hour_balance_line_id = fields.Many2one('hour.balance.line', string='Hour Balance Line',ondelete='cascade')
    contract_id = fields.Many2one('hr.contract', string='Contract')
    company_id = fields.Many2one(related='balance_id.company_id', string='Company', store=True, readonly=True, states={'draft': [('readonly', False)], 'cancel': [('readonly', False)]}, default=lambda self: self.env.user.company_id)
    balance_id = fields.Many2one('hour.balance', string='Hour balance', ondelete='cascade', index=True)
    employee_id = fields.Many2one('hr.employee', string="Employee First Name")
    employee_number = fields.Integer(string="№")
    employee_reg_number = fields.Char(string="Employee Register Number")
    employee_last_name = fields.Char(string="Employee Last Name")
    identification_id = fields.Char(related='employee_id.identification_id', string='Identification Number')
    period_id = fields.Many2one(related='balance_id.period_id', string="Period", store=True)
    salary_type = fields.Selection(related='balance_id.salary_type', string='Salary type', store=True)
    department_id = fields.Many2one('hr.department', string="Department")
    balance_reg_day = fields.Float(string='Balance regular /days/')
    balance_reg_hour = fields.Float(string='Balance regular /hours/')
    reg_day = fields.Float(string='Regular /days/')
    reg_hour = fields.Float(string='Regular /hours/', digits=(4, 2))
    timesheet_hour = fields.Float(string='Timesheet /hours/', digits=(4, 2))
    total_attendance_days = fields.Float(string='Total Attendance /days/')
    attendance_hour = fields.Float(string='Total Attendance /hours/')
    worked_day = fields.Float(string='Working day /days/')
    worked_hour = fields.Float(string='Working day /hours/', digits=(4, 2))
    celebration_days = fields.Float(string='Celebration day /days/')
    celebration_hours = fields.Float(string='Celebration day /hours/')
    working_days_at_weekend = fields.Float(string='Weekend /days/')
    working_hours_at_weekend = fields.Float(string='Weekend /hours/')
    total_worked_day = fields.Float(string='Total worked /days/', readonly=True, compute='compute_total_worked_day')
    total_worked_hour = fields.Float(string='Total worked /hours/', readonly=True, compute='compute_total_worked_hour', digits=(4, 2))
    business_trip_hour = fields.Float(string='Business trip /hours/', digits=(4, 2))
    out_working_hour = fields.Float(string='Out working /hours/', digits=(4, 2))
    total_training_hours = fields.Float(string='Training /hours/', digits=(4, 2)) 
    overtime = fields.Float(string='Overtime by Attendance', digits=(4, 2))
    confirmed_overtime = fields.Float(string='Confirmed Overtime', digits=(4,2))
    overtime_holiday = fields.Float(string='Overtime holiday', digits=(4, 2))
    confirmed_overtime_holiday = fields.Float(string='Confirmed Overtime Holiday', digits=(4,2))
    paid_holiday = fields.Float(string='Paid holiday /hours/', digits=(4, 2))
    annual_leave = fields.Float(string='Annual leave /hours/')
    sick_leave = fields.Float(string='Sick leave /hours/', digits=(4, 2))
    unpaid_holiday = fields.Float(string='Unpaid holiday /hours/', digits=(4, 2))
    delay_hour = fields.Float(string='Delay time /hours/', digits=(4, 2))
    missed_hour = fields.Float(string='Missed hour /hours/', compute="compute_missed_hour", store=True, readonly=True, digits=(4, 2))
    unworked_leaved_hour = fields.Float(string='Un worked/Leave work /hours/', digits=(4, 2))
    lag_hour = fields.Float(string='Lag /hours/', digits=(4, 2))
    lag_minute = fields.Float(string='Lag /minutes/', digits=(4, 2))
    total_unworked_hour = fields.Float(string='Total unworked /hours/', compute='calc_total_unworked_hour', digits=(4, 2))
    description = fields.Char(string='Description')
    state = fields.Selection(STATE_SELECTION, string='Status', default='draft', index=True, track_visibility='always', required=True)
    
    @api.depends('balance_reg_hour', 'total_worked_hour', 'paid_holiday', 'unpaid_holiday', 'annual_leave', 'sick_leave', 'out_working_hour', 'business_trip_hour', 'total_training_hours', 'delay_hour', 'unworked_leaved_hour')
    def compute_missed_hour(self):
        # ТАСЛАЛТ = БАЛАНСЫН АЖИЛЛАВАЛ ЗОХИХ /ЦАГ/ - ТОМИЛОЛТ – ГАДУУР АЖИЛ – СУРГАЛТ /ЦАГ/ - ЦАЛИНТАЙ ЧӨЛӨӨ /ЦАГ/ - ЭЭЛЖИЙН АМРАЛТ  /ЦАГ/ - ӨВЧНИЙ ЧӨЛӨӨ  /ЦАГ/ - ЦАЛИНГҮЙ ЧӨЛӨӨ  /ЦАГ/ - НИЙТ АЖИЛЛАСАН ЦАГ - Сул зогсолт - Ажиллаагүй болон ажлаас гарсан өдөр
        for obj in self:
            holiday_hours = obj.paid_holiday + obj.unpaid_holiday + obj.annual_leave + obj.sick_leave
            missed_hour = obj.balance_reg_hour - obj.total_worked_hour - holiday_hours - obj.out_working_hour - obj.business_trip_hour - obj.total_training_hours - obj.delay_hour - obj.unworked_leaved_hour
            obj.missed_hour = max(missed_hour, 0) 
