# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/,
# http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class HourBalanceConfigSettings(models.TransientModel):
    _name = 'hour.balance.config.settings'
    _inherit = 'res.config.settings'

    company_id = fields.Many2one('res.company', string='Company', required=True,
        default=lambda self: self.env.user.company_id)
    confirm_overtime = fields.Boolean(string='Confirmed Overtime', related='company_id.confirm_overtime', help="Bank accounts as printed in the footer of each printed document", default=lambda x: x.confirm_overtime)
    confirm_overtime_holiday = fields.Boolean(string='Confirmed Overtime Holiday', related='company_id.confirm_overtime_holiday', default=lambda x: x.confirm_overtime_holiday)

    module_l10n_mn_hr_overtime = fields.Boolean(string="Use approval of overtime", related="company_id.module_l10n_mn_hr_overtime" ,  help="Install the module that allows hr overtime.", default=False)
