# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class HrAttendance(models.Model):
    _inherit = "hr.attendance"

    def check_worked_hours_can_be_day(self):
        self.ensure_one()
        return True