# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from odoo.exceptions import UserError


class HrEmployee(models.Model):
    _inherit = "hr.employee"
    
    @api.multi
    def check_contract_duplication(self):
        # Гэрээний хугацаа давхцсан ажилтнуудын мэдээллийг олж raise өгнө.
        errors = []
        for employee in self:
            contract_names = []
            qry = """
                SELECT a.contract_code
                FROM hr_contract AS a
                JOIN hr_contract AS b ON a.employee_id = b.employee_id
                WHERE a.id != b.id AND a.active = 't' AND b.active = 't' AND a.employee_id = %s
                AND daterange(a.date_start, COALESCE(a.date_end, now()::date), '[]') && daterange(b.date_start, COALESCE(b.date_end, now()::date), '[]')
            """ % employee.id
            self._cr.execute(qry)
            fetch = map(lambda x: x[0], self._cr.fetchall())
            if len(fetch) > 1:
                for contract_name in fetch:
                    contract_names.append(str(contract_name))
            if contract_names:
                last_name = "%s." % (employee.last_name[0].upper() if employee.last_name else "")
                error_value = "%s%s /%s/" % (last_name, employee.name, employee.ssnid)
                error_value += ":\t\t\t%s" % ", ".join(contract_name for contract_name in contract_names)
                errors.append(error_value)
        if errors:
            raise UserError(_('The contracts are in overlap.%s')  % ("\n\n%s" % ",\n".join(error for error in errors)))
        
          