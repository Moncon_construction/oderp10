# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################
from odoo import api, fields, models, _
import logging
from odoo.exceptions import Warning
from odoo import exceptions
from odoo.exceptions import UserError, ValidationError
from datetime import timedelta, datetime
from odoo.addons.l10n_mn_web.models.time_helper import *
from dateutil import rrule
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

_logger = logging.getLogger(__name__)


class HRHolidaysStatus(models.Model):
    _inherit = "hr.holidays.status"

    balance_type = fields.Selection([
        ('paid', 'Paid'),
        ('unpaid', 'Unpaid'),
        ('annual_leave', 'Annual Leave'),
        ('sick_leave', 'Sick Leave')], string='Balance Type', default='unpaid')


class HRHolidays(models.Model):
    _inherit = "hr.holidays"

    number_of_hours_temp = fields.Float('Allocation', readonly=True, copy=False,
                                        states={'draft': [('readonly', False)], 'confirm': [('readonly', False)]})
    validate_token = fields.Char()
    is_token_valid = fields.Boolean()
    state = fields.Selection([
        ('draft', 'To Submit'),
        ('cancel', 'Cancelled'),
        ('confirm', 'To Approve'),
        ('refuse', 'Refused'),
        ('validate1', 'Second Approval'),
        ('validate', 'Approved')
    ], string='Status', readonly=True, track_visibility='onchange', copy=False, default='draft',
        help="The status is set to 'To Submit', when a holiday request is created." +
             "\nThe status is 'To Approve', when holiday request is confirmed by user." +
             "\nThe status is 'Refused', when holiday request is refused by manager." +
             "\nThe status is 'Approved', when holiday request is approved by manager.")
    workflow_id = fields.Many2one('workflow.config', 'Workflow')
    check_sequence = fields.Integer('Workflow Step', default=0)
    history_lines = fields.One2many('hr.holidays.workflow.history', 'history', 'Workflow History')
    is_validator = fields.Boolean(compute='_compute_is_validator')
    is_workflow_history = fields.Boolean(compute='_is_workflow_history')
    gov_year = fields.Float(string='Worked for Government ', related ="employee_id.gov_year", readonly=True)
    annual_leave_days = fields.Integer(string='Annual Leave Days', related="employee_id.annual_leave_days", readonly=True)
    is_annual_leave = fields.Boolean('Is annual leave')

    @api.onchange('holiday_status_id', 'employee_id')
    def onchange_holiday_status_id(self):
        for obj in self:
            if obj.holiday_status_id.balance_type == 'annual_leave':
                obj.is_annual_leave =True
            else:
                obj.is_annual_leave = False

    @api.multi
    def _is_workflow_history(self):
        for obj in self:
            is_workflow = False
            user_id = self._uid
            for line in obj.history_lines:
                if user_id == line.user_id.id:
                    is_workflow = True
            obj.is_workflow_history = is_workflow

    @api.depends('check_sequence')
    def _compute_is_validator(self):
        for rec in self:
            history_obj = self.env['hr.holidays.workflow.history']
            validators = history_obj.search([('history', '=', rec.id), ('line_sequence', '=', rec.check_sequence)],
                                            limit=1, order='sent_date DESC').user_ids
            if self.env.user in validators:
                rec.is_validator = True
            else:
                rec.is_validator = False

    def _get_number_of_days(self, date_from, date_to, employee_id):
        if self.employee_id.contract_id.working_hours:
            day_count = 0
            
            for day in rrule.rrule(rrule.DAILY, dtstart=get_day_like_display(date_from, self.env.user),
                                   until=get_day_like_display(date_to, self.env.user),
                                   byweekday=self.employee_id.contract_id.working_hours.get_weekdays()):
                day_count += 1
            return day_count
        else:
            super(HRHolidays, self)._get_number_of_days(date_from, date_to, employee_id)
            """ Returns a float equals to the timedelta between two dates given as string."""
            from_dt = fields.Datetime.from_string(date_from)
            to_dt = fields.Datetime.from_string(date_to)
            time_delta = to_dt - from_dt
            day_count = 0
            if self.env.user.company_id.overtime_holiday:
                days = {'mon': 0, 'tue': 1, 'wed': 2, 'thu': 3, 'fri': 4, 'sat': 5, 'sun': 6}
                delta_day = timedelta(days=1)
                while from_dt <= to_dt:
                    if from_dt.weekday() in (days['sat'], days['sun']):
                        day_count += 1
                    else:
                        holidays = self.env['hr.holidays.configuration'].search([('date', '=', from_dt)])
                        if holidays:
                            day_count += 1
                    from_dt += delta_day

            return time_delta.days - day_count

    # Цагын зөрүүг өгөх функц
    def _get_number_of_hours(self, date_from, date_to):
        total_hours = 0.0
        if self.employee_id.contract_id.working_hours:
            st_date = get_day_by_user_timezone(date_from, self.env.user)
            end_date = get_day_by_user_timezone(date_to, self.env.user)
            for day in rrule.rrule(rrule.DAILY, dtstart=get_day_like_display(date_from, self.env.user),
                                   until=get_day_like_display(date_to, self.env.user),
                                   byweekday=self.employee_id.contract_id.working_hours.get_weekdays()):
                day_start_dt = day.replace(hour=0, minute=0, second=0)
                day_end_dt = day.replace(hour=23, minute=59, second=59)
                if day.date() == st_date.date():
                    start_date = st_date
                else:
                    start_date = day_start_dt
                if day.date() == end_date.date():
                    finish = end_date
                else:
                    finish = day_end_dt
                resource_calendar = self.employee_id.contract_id.working_hours
                hours = resource_calendar.get_working_hours_of_date(start_dt=start_date, end_dt=finish,
                                                                    compute_leaves=True, resource_id=None,
                                                                    default_interval=None)
                total_hours += hours
        else:
            from_dt = fields.Datetime.from_string(date_from)
            to_dt = fields.Datetime.from_string(date_to)
            time_delta = to_dt - from_dt
            total_hours = float(time_delta.seconds) / 3600
        return total_hours

    @api.onchange('date_from')
    def _onchange_date_from(self):
        super(HRHolidays, self)._onchange_date_from()
        date_from = self.date_from
        date_to = self.date_to

        # Compute and update the number of days
        if (date_to and date_from) and (date_from <= date_to):
            self.number_of_days_temp = self._get_number_of_days(date_from, date_to, self.employee_id.id)
            self.number_of_hours_temp = self._get_number_of_hours(date_from, date_to)
        else:
            self.number_of_days_temp = 0
            self.number_of_hours_temp = 0

    @api.onchange('date_to')
    def _onchange_date_to(self):
        super(HRHolidays, self)._onchange_date_to()
        """ Update the number_of_days. """
        date_from = self.date_from
        date_to = self.date_to

        # Compute and update the number of days
        if (date_to and date_from) and (date_from <= date_to):
            self.number_of_days_temp = self._get_number_of_days(date_from, date_to, self.employee_id.id)
            self.number_of_hours_temp = self._get_number_of_hours(date_from, date_to)
        # else:
        #     self.number_of_days_temp = 0
        #     self.number_of_hours_temp = 0

    """ Амралт, чөлөө хуваарилах хүсэл цаг солигдоход өдрийг тооцох"""
    @api.onchange('number_of_hours_temp')
    def _onchange_number_of_hours_temp(self):
        if self.type == 'add':
            work_hours_day = 8
            if self.employee_id.calendar_id and self.employee_id.calendar_id.work_hours_day > 0:
                work_hours_day = self.employee_id.calendar_id.work_hours_day
            self.number_of_days_temp = self.number_of_hours_temp / work_hours_day

    @api.multi
    def name_get(self):
        super(HRHolidays, self).name_get()
        res = []
        for leave in self:
            res.append((leave.id, _("%s's %s : %.2f day(s)  %.2f hour(s)") % (
            leave.employee_id.name or leave.category_id.name, leave.holiday_status_id.name, leave.number_of_days_temp,
            leave.number_of_hours_temp)))
        return res

    @api.model
    def create(self, vals):
        creation = super(HRHolidays, self).create(vals)
        if vals['holiday_type'] == 'employee':
            employee = self.env['hr.employee'].search([('id', '=', vals['employee_id'])])
        else:
            employee = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)])
        workflow_id = self.env['workflow.config'].get_workflow('employee', 'hr.holidays', employee.id, self.department_id)
        workflow = None
        if workflow_id:
            workflow = workflow_id
        if not workflow:
            raise exceptions.Warning('There is no workflow defined!')
        creation.workflow_id = workflow
        # Амралт чөлөөний товчооны өдөр талбар хадгалагддаг болгов.
        date_from = vals.get('date_from')
        date_to = vals.get('date_to')
        if (date_to and date_from) and (date_from <= date_to):
            creation.number_of_days_temp = self._get_number_of_days(date_from, date_to, employee.id)
        return creation

    @api.multi
    def write(self, vals):
        #Амралт чөлөөний товчооны өдөр талбар хадгалагддаг болгов.
        date_from = vals['date_from'] if 'date_from' in vals else self.date_from
        date_to = vals['date_to'] if 'date_to' in vals else self.date_to
        if self.holiday_type == 'employee':
            employee = self.env['hr.employee'].search([('id', '=', self.employee_id.id)])
        else:
            employee = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)])
        if (date_to and date_from) and (date_from <= date_to):
            vals['number_of_days_temp'] = self._get_number_of_days(date_from, date_to, employee.id)
        return super(HRHolidays, self).write(vals)

    @api.multi
    def action_send(self):
        if self.filtered(lambda holiday: holiday.state != 'draft'):
            raise UserError(_('Leave request must be in Draft state ("To Submit") in order to confirm it.'))
        self.write({'state': 'confirm'})
        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve(
                'hr.holidays.workflow.history', 'history', self, self.employee_id.id)
            if success:
                if sub_success:
                    self.state = 'confirm'
                else:
                    self.check_sequence = current_sequence
        return True

    @api.multi
    def action_approve(self):
        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve(
                'hr.holidays.workflow.history', 'history', self, self.env.user.id)
            if success:
                if sub_success:
                    self.action_validate()
                    self.state = 'validate'
                else:
                    self.check_sequence = current_sequence

    @api.multi
    def action_refuse(self):
        if self.workflow_id:
            success = self.env['workflow.config'].reject('hr.holidays.workflow.history', 'history', self, self.env.user.id)
            if success:
                self.state = 'refuse'
        manager = self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)
        for holiday in self:
            holiday.write({'state': 'refuse', 'manager_id2': manager.id})
            # Delete the meeting
            if holiday.meeting_id:
                holiday.meeting_id.unlink()
            # If a category that created several holidays, cancel all related
            holiday.linked_request_ids.action_refuse()
        self._remove_resource_leave()
        return True


class ExpenseWorkflowHistory(models.Model):
    _name = 'hr.holidays.workflow.history'
    _order = 'history, sent_date'

    STATE_SELECTION = [('waiting', 'Waiting'),
                       ('confirmed', 'Confirmed'),
                       ('approved', 'Approved'),
                       ('return', 'Return'),
                       ('rejected', 'Rejected')]
    history = fields.Many2one('hr.holidays', 'Expense', readonly=True, ondelete='cascade')
    line_sequence = fields.Integer('Workflow Step')
    name = fields.Char('Verification Step')
    user_ids = fields.Many2many('res.users', 'res_users_holidays_workflow_history_ref', 'history_id', 'user_id', 'Validators')
    sent_date = fields.Datetime('Sent date')
    user_id = fields.Many2one('res.users', 'Validator')
    action_date = fields.Datetime('Action date', readonly=True)
    action = fields.Selection(STATE_SELECTION, 'Action', readonly=True)



class HolidaysType(models.Model):

    _inherit = "hr.holidays.status"

    @api.multi
    def get_days(self, employee_id):
        # need to use `dict` constructor to create a dict per id
        result = dict((id, dict(max_leaves=0, leaves_taken=0, remaining_leaves=0, virtual_remaining_leaves=0)) for id in self.ids)

        holidays = self.env['hr.holidays'].search([
            ('employee_id', '=', employee_id),
            ('state', 'in', ['confirm', 'validate1', 'validate']),
            ('holiday_status_id', 'in', self.ids)
        ])

        for holiday in holidays:
            status_dict = result[holiday.holiday_status_id.id]
            if holiday.type == 'add':
                if holiday.state == 'validate':
                    # note: add only validated allocation even for the virtual
                    # count; otherwise pending then refused allocation allow
                    # the employee to create more leaves than possible
                    status_dict['virtual_remaining_leaves'] += holiday.number_of_hours_temp
                    status_dict['max_leaves'] += holiday.number_of_hours_temp
                    status_dict['remaining_leaves'] += holiday.number_of_hours_temp
            elif holiday.type == 'remove':  # number of days is negative
                hr_contract = self.env['hr.contract'].search([('employee_id', '=', employee_id), (
                    'employee_id.company_id', '=', self.env.user.company_id.id), ('active', '=', 't')])
                if hr_contract:
                    status_dict['virtual_remaining_leaves'] -= holiday.number_of_hours_temp
                    if holiday.state == 'validate':
                        status_dict['leaves_taken'] += holiday.number_of_hours_temp
                        status_dict['remaining_leaves'] -= holiday.number_of_hours_temp
                else:
                    status_dict['virtual_remaining_leaves'] -= holiday.number_of_hours_temp + (
                                holiday.number_of_days_temp * holiday.employee_id.calendar_id.work_hours_day)
                    if holiday.state == 'validate':
                        status_dict['leaves_taken'] += holiday.number_of_hours_temp + (holiday.number_of_days_temp * holiday.employee_id.calendar_id.work_hours_day)
                        status_dict['remaining_leaves'] -= holiday.number_of_hours_temp + (
                                    holiday.number_of_days_temp * holiday.employee_id.calendar_id.work_hours_day)
        return result