# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class HRHolidaysConfiguration(models.Model):

    _name = "hr.holidays.configuration"
    _desription = "Hr holidays configuration"

    name = fields.Char(string='Name', size=150, required=True)
    company_id = fields.Many2one('res.company', default=lambda self: self.env.user.company_id, string='Company')
    date = fields.Date(string='Date', required=True)


class HRHolidaysConfigSettings(models.TransientModel):
    _name = 'hr.holidays.config.settings'
    _inherit = 'res.config.settings'

    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    day_monday = fields.Boolean(string='Monday', related='company_id.day_monday')
    day_tuesday = fields.Boolean(string='Tuesday', related='company_id.day_tuesday')
    day_wednesday = fields.Boolean(string='Wednesday', related='company_id.day_wednesday')
    day_thursday = fields.Boolean(string='Thursday', related='company_id.day_thursday')
    day_friday = fields.Boolean(string='Friday', related='company_id.day_friday')
    day_saturday = fields.Boolean(string='Saturday', related='company_id.day_saturday')
    day_sunday = fields.Boolean(string='Sunday', related='company_id.day_sunday')
