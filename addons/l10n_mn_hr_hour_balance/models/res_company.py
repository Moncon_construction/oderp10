# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

from odoo import api, fields, models, _
from odoo.exceptions import UserError
import logging
import time

_logger = logging.getLogger(__name__)

class ResCompany(models.Model):
    _inherit = 'res.company'

    calculate_lag_by_limit = fields.Boolean(string='Insert lag limit')
    lag_limit = fields.Float(string='Lag Limit')
    confirm_overtime = fields.Boolean("Confirmed Overtime", default=False)
    confirm_overtime_holiday = fields.Boolean("Confirmed Overtime Holiday", default=False)
    overtime_holiday = fields.Boolean("Overtime Holiday", default=False)
    module_l10n_mn_hr_overtime = fields.Boolean(string="Hour Overtime", default=False)
    day_monday = fields.Boolean(string='Monday', default=False)
    day_tuesday = fields.Boolean(string='Tuesday', default=False)
    day_wednesday = fields.Boolean(string='Wednesday', default=False)
    day_thursday = fields.Boolean(string='Thursday', default=False)
    day_friday = fields.Boolean(string='Friday', default=False)
    day_saturday = fields.Boolean(string='Saturday', default=False)
    day_sunday = fields.Boolean(string='Sunday', default=False)
