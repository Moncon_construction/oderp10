# -*- coding: utf-8 -*-
import hashlib
import hmac

from odoo import api, fields, models, _

from odoo.http import request


class WorkflowConfig(models.Model):
    _inherit = 'workflow.config'

    def get_email_body(self, model_object, url, obj=None):
        if model_object._name == 'hr.holidays':
            base_link = self.env['ir.config_parameter'].get_param('web.base.url')
            params = {
                'model': model_object._name,
                'res_id': model_object.id
            }
            url = "%s/web#id=%s&view_type=form&model=%s" % (base_link, model_object.id, model_object._name)
            valid_token = request.env['mail.thread']._generate_notification_token(base_link, params)
            model_object.write({
                "validate_token": valid_token,
                "is_token_valid": True
            })
            validate_url = "%s/hr_holidays/validate_holiday?token=%s&res_id=%s" % (base_link, valid_token, model_object.id)
            refuse_url = "%s/hr_holidays/refuse_holiday?token=%s&res_id=%s" % (base_link, valid_token, model_object.id)
            return u"""
                    <p>Сайн байна уу, </br></p><p>Танд батлах %s ирсэн байна.</br></p>
                    <p>Ажилтан %s нь "%s" шалтгаанаар "%s" төрлийн чөлөө хүссэн байна. </p>
                    <p>Дэлгэрэнгүй мэдээллийг харахыг хүсвэл "Харах" товчийг дарна уу.</br></p>
                    <p><a href="%s" target="_blank" style="background-color: #9E588B; margin-top: 10px; padding: 10px; text-decoration: none; color: #fff; border-radius: 5px; font-size: 16px;">Харах</a> |
                     <a href="%s" target="_blank" style="background-color: #9E588B; margin-top: 10px; padding: 10px; text-decoration: none; color: #fff; border-radius: 5px; font-size: 16px;">Зөвшөөрөх</a> |
                     <a href="%s" target="_blank" style="background-color: #9E588B; margin-top: 10px; padding: 10px; text-decoration: none; color: #fff; border-radius: 5px; font-size: 16px;">Татгалзах</a>
                    <p>Od ERP Автомат Имэйл </p>
                """ % (obj if obj else model_object.name, model_object.employee_id.name, model_object.name, model_object.holiday_status_id.name, url, validate_url, refuse_url)
        return super(WorkflowConfig, self).get_email_body(model_object, url, obj)

    def get_email_subject(self, model_object):
        if model_object._name == 'hr.holidays':
            return _('Leave: %s' % model_object.display_name)
        return super(WorkflowConfig, self).get_email_subject(model_object)