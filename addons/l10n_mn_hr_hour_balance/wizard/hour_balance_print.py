# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models
from odoo.addons.l10n_mn_web.models.time_helper import *
from xlsxwriter.utility import xl_rowcol_to_cell
import xlsxwriter
from io import BytesIO
import base64
import time
from datetime import datetime, timedelta

class HourBalancePrint(models.TransientModel):
    _name = "hour.balance.print"
    _description = "Hour Balance Print"
    

    balance_id = fields.Many2one('hour.balance', string="Hour balance", default=lambda self: self._context.get('active_id',False))
            
    @api.multi
    def export(self):
        for obj in self:
            context = dict(self._context or {})
            sheetname_1 = 'Balance'
            
            now = time.strftime('%Y-%m-%d')
            output = BytesIO()
            
            workbook = xlsxwriter.Workbook(output)
            worksheet = workbook.add_worksheet(sheetname_1)
            is_training = False
            module = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_hr_hour_balance_training')])
            if module:
                if module.state == 'installed':
                    is_training = True
            
            title = workbook.add_format({
                'bold': 1,
                'border': 0,
                'align': 'left',
                'valign': 'vcenter',
                'font_size': 9,
                'font_name': 'Calibri',
                })
            sub_title = workbook.add_format({
                'bold': 1,
                'border': 0,
                'align': 'center',
                'valign': 'vcenter',
                'font_size': 14,
                'font_name': 'Calibri',
                })
            sub_title1 = workbook.add_format({
                'bold': 1,
                'border': 1,
                'align': 'center',
                'valign': 'vcenter',
                'font_size': 9,
                'bg_color': '#99ccff',
                'font_name': 'Calibri',
                'text_wrap':1,
                })
            sub_title_left = workbook.add_format({
                'bold': 1,
                'border': 1,
                'align': 'left',
                'valign': 'vcenter',
                'font_size': 9,
                'bg_color': '#b9cde5',
                'font_name': 'Calibri',
                'text_wrap':1,
                })
            header = workbook.add_format({
                'border': 1,
                'bold': 1,
                'align': 'center',
                'valign': 'vcenter',
                'text_wrap': 'on',
                'font_size':8,
                'font_name': 'Times New Roman',
                })
            cell_format_left = workbook.add_format({
                'border': 1,
                'align': 'left',
                'valign': 'vcenter',
                'font_size':9,
                'font_name': 'Times New Roman',
                'text_wrap':1,
                })
            cell_format_right = workbook.add_format({
                'border': 1,
                'align': 'right',
                'valign': 'vcenter',
                'font_size':9,
                'font_name': 'Times New Roman',
                'text_wrap':1,
                'num_format': '0.00'
                })
            cell_format_center = workbook.add_format({
                'border': 1,
                'align': 'center',
                'valign': 'vcenter',
                'font_size':9,
                'font_name': 'Times New Roman',
                'text_wrap':1,
                })
            cell_format_footer = workbook.add_format({
                'align': 'left',
                'valign': 'vcenter',
                'font_size':9,
                'font_name': 'Times New Roman',
                })

            row = 0
            col = 0
            colh = 0
            worksheet.set_column('A:A', 4)
            worksheet.set_column('B:B', 17)
            worksheet.set_column('C:C', 12)
            worksheet.set_column('D:D', 17)
            worksheet.set_column('E:E', 6)
            worksheet.set_column('F:F', 6)
            worksheet.set_column('G:G', 6)
            worksheet.set_column('I:I', 8)
            if is_training == True or obj.balance_id.company_id.confirm_overtime:
                worksheet.set_column('H:AD', 6)
                worksheet.set_column('AE:AE', 15) 
            elif is_training == True and obj.balance_id.company_id.confirm_overtime:
                worksheet.set_column('H:AE', 6)
                worksheet.set_column('AF:AF', 15) 
            else:
                worksheet.set_column('H:AC', 6)
                worksheet.set_column('AD:AD', 15)
                
            if colh == 1:
                worksheet.set_column('H:AD', 6)
                worksheet.set_column('AE:AE', 15)
            if colh == 2:
                if is_training == True:
                    worksheet.set_column('H:AF', 6)
                    worksheet.set_column('AG:AG', 15)
                else:
                    worksheet.set_column('H:AE', 6)
                    worksheet.set_column('AF:AF', 15)
            worksheet.set_row(1, 41)
            worksheet.set_row(2, 25)
            worksheet.set_row(3, 25)
            worksheet.set_row(4, 20)
            worksheet.set_row(5, 20)
            
            # title
            worksheet.write(row, col+1, u'%s' % (obj.balance_id.company_id.name), title)
            worksheet.write(row, colh  , u'Огноо: %s' % (now), title)
            row += 1
            worksheet.merge_range(row, col, row, col+colh+24, u'(%s)-Цагийн баланс' % (obj.balance_id.name), sub_title)
            row += 1
            
            # table header
            worksheet.merge_range(row, col, row+2, col, u'Д/д', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row+2, col, u'Нэр', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row+2, col, u'Регистр', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row+2, col, u'Албан тушаал', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row+1, col+1, u'Балансын ажиллавал зохих өдөр', sub_title1)
            worksheet.write(row+2, col, u'Өдөр', sub_title1)
            col += 1
            worksheet.write(row+2, col, u'Цаг', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row+2, col, u'Цагийн хуудас /цаг/', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row+2, col, u'Нийт ирц /цаг/', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row, col+colh+8, u'Ажилласан', sub_title1)
            worksheet.merge_range(row+1, col, row+1, col+1, u'Ажлын өдөр ажилласан цаг', sub_title1)
            worksheet.write(row+2, col, u'Өдөр', sub_title1)
            col += 1
            worksheet.write(row+2, col, u'Цаг', sub_title1)
            col += 1
            
            worksheet.merge_range(row+1, col, row+1, col+1, u'Баярын өдөр ажилласан цаг', sub_title1)
            worksheet.write(row+2, col, u'Өдөр', sub_title1)
            col += 1
            worksheet.write(row+2, col, u'Цаг', sub_title1)
            col += 1
            
            worksheet.merge_range(row+1, col, row+1, col+1, u'Амралтын өдөр ажилласан цаг', sub_title1)
            worksheet.write(row+2, col, u'Өдөр', sub_title1)
            col += 1
            worksheet.write(row+2, col, u'Цаг', sub_title1)
            col += 1
            
            worksheet.merge_range(row+1, col, row+1, col+1, u'Нийт ажилласан цаг', sub_title1)
            worksheet.write(row+2, col, u'Өдөр', sub_title1)
            col += 1
            worksheet.write(row+2, col, u'Цаг', sub_title1)
            col += 1
            worksheet.merge_range(row+1, col, row+2, col, u'Гадуур ажил', sub_title1)
            col +=1 
            if is_training == True:
                worksheet.merge_range(row, col, row+2, col, u'Сургалт', sub_title1)
                col +=1 
            worksheet.merge_range(row, col, row+2, col, u'Томилолт', sub_title1)
            col +=1 
            worksheet.merge_range(row, col, row+2, col, u'Илүү цаг', sub_title1)
            col += 1
            if obj.balance_id.company_id.confirm_overtime:
                worksheet.merge_range(row, col, row+2, col, u'Батлагдсан илүү цаг', sub_title1)
                col +=1 
            worksheet.merge_range(row, col, row, col+9, u'Ажиллаагүй', sub_title1)
            worksheet.merge_range(row+1, col, row+2, col, u'Цалинтай чөлөө', sub_title1)
            col +=1
            worksheet.merge_range(row + 1, col, row + 2, col, u'Нөхөн амраасан цаг', sub_title1)
            col += 1
            worksheet.merge_range(row+1, col, row+2, col, u'Ээлжийн амралт', sub_title1)
            col +=1 
            worksheet.merge_range(row+1, col, row+2, col, u'Өвчний чөлөө', sub_title1)
            col +=1 
            worksheet.merge_range(row+1, col, row+2, col, u'Цалингүй чөлөө', sub_title1)
            col +=1 
            worksheet.merge_range(row+1, col, row+2, col, u'Сул зогсолт', sub_title1)
            col +=1 
            worksheet.merge_range(row+1, col, row+2, col, u'Таслалт', sub_title1)
            col +=1 
            worksheet.merge_range(row+1, col, row+2, col, u' Ажиллаагүй болон ажлаас гарсан өдөр', sub_title1)
            col +=1 
            worksheet.merge_range(row+1, col, row+2, col, u'Хоцролт /Цаг/', sub_title1)
            col +=1 
            worksheet.merge_range(row+1, col, row+2, col, u'Хоцролт /Минут/', sub_title1)
            col +=1 
            worksheet.merge_range(row+1, col, row+2, col, u'Нийт ажлаагүй цаг', sub_title1)
            col +=1 
            worksheet.merge_range(row, col, row+2, col, u'Тайлбар', sub_title1)
            row += 3
            
            balance = obj.balance_id
            department_ids = []
            for line in balance.balance_line_ids:
                if line.department_id.id not in department_ids:
                    department_ids.append(line.department_id.id)
                
            number = 1
            department_ids.sort()
            # column
            
            if is_training == True:
                colh +=1               
            if obj.balance_id.company_id.confirm_overtime:
                colh +=1 
            for department_id in department_ids:
                col = 0
                department = self.env['hr.department'].browse(department_id)
                worksheet.merge_range(row, col, row, col+colh+29, department.name, sub_title_left)
                row += 1
                for line in balance.balance_line_ids:
                    if department_id == line.department_id.id:
                        col = 0
                        worksheet.write(row, col, number, cell_format_center)
                        col +=1 
                        fullname = ""
                        if line.employee_id.name:
                            fullname += line.employee_id.name
                        if line.employee_id.last_name:
                            if len(line.employee_id.last_name) > 3:
                                fullname += "."+line.employee_id.last_name[:3]
                            else:
                                fullname += "."+line.employee_id.last_name
                        register = ""
                        if line.employee_id.ssnid:
                            register = line.employee_id.ssnid
                        job = ""
                        if line.employee_id.job_id.name:
                            job = line.employee_id.job_id.name
                        worksheet.write(row, col, fullname, cell_format_left)
                        col +=1 
                        worksheet.write(row, col, register, cell_format_left)
                        col +=1 
                        worksheet.write(row, col, job, cell_format_left)
                        col +=1 
                        worksheet.write(row, col, line.balance_reg_day, cell_format_right)
                        col +=1 
                        worksheet.write(row, col, float_to_hours_minutes(line.balance_reg_hour), cell_format_right)
                        col +=1 
                        worksheet.write(row, col, float_to_hours_minutes(line.timesheet_hour), cell_format_right)
                        col +=1 
                        worksheet.write(row, col, float_to_hours_minutes(line.attendance_hour), cell_format_right)
                        col +=1 
                        
                        worksheet.write(row, col, line.worked_day, cell_format_right)
                        col +=1 
                        worksheet.write(row, col, float_to_hours_minutes(line.worked_hour), cell_format_right)
                        col +=1 
                        
                        worksheet.write(row, col, line.celebration_days, cell_format_right)
                        col +=1 
                        worksheet.write(row, col, float_to_hours_minutes(line.celebration_hours), cell_format_right)
                        col +=1 
                        
                        worksheet.write(row, col, line.working_days_at_weekend, cell_format_right)
                        col +=1 
                        worksheet.write(row, col, float_to_hours_minutes(line.working_hours_at_weekend), cell_format_right)
                        col +=1 
                        
                        worksheet.write(row, col, line.total_worked_day, cell_format_right)
                        col +=1 
                        worksheet.write(row, col, float_to_hours_minutes(line.total_worked_hour), cell_format_right)
                        col +=1 
                        
                        worksheet.write(row, col, float_to_hours_minutes(line.out_working_hour), cell_format_right)
                        col +=1                         
                        if is_training == True:
                            worksheet.write(row, col, float_to_hours_minutes(line.total_training_hours), cell_format_right)
                            col +=1 
                        worksheet.write(row, col, float_to_hours_minutes(line.business_trip_hour), cell_format_right)
                        col +=1 
                        
                        worksheet.write(row, col, float_to_hours_minutes(line.overtime), cell_format_right)
                        col +=1 
                        # if obj.balance_id.company_id.confirmed_overtime:
                        #     worksheet.write(row, col, float_to_hours_minutes(line.confirmed_overtime), cell_format_right)
                        #     col +=1
                        
                        worksheet.write(row, col, float_to_hours_minutes(line.paid_holiday), cell_format_right)
                        col +=1
                        worksheet.write(row, col, float_to_hours_minutes(line.rehabilitated_holiday), cell_format_right)
                        col += 1
                        worksheet.write(row, col, float_to_hours_minutes(line.annual_leave), cell_format_right)
                        col +=1 
                        worksheet.write(row, col, float_to_hours_minutes(line.sick_leave), cell_format_right)
                        col +=1 
                        worksheet.write(row, col, float_to_hours_minutes(line.unpaid_holiday), cell_format_right)
                        col +=1 
                        worksheet.write(row, col, float_to_hours_minutes(line.delay_hour), cell_format_right)
                        col +=1 
                        worksheet.write(row, col, float_to_hours_minutes(line.missed_hour), cell_format_right)
                        col +=1 
                        worksheet.write(row, col, float_to_hours_minutes(line.unworked_leaved_hour), cell_format_right)
                        col +=1 
                        worksheet.write(row, col, float_to_hours_minutes(line.lag_hour), cell_format_right)
                        col +=1 
                        worksheet.write(row, col, float_to_hours_minutes(line.lag_minute), cell_format_right)
                        col +=1
                        worksheet.write(row, col, float_to_hours_minutes(line.total_unworked_hour), cell_format_right)
                        col +=1 
                        if line.description:
                            worksheet.write(row, col, line.description, cell_format_left)
                        else:
                            worksheet.write(row, col, u'', cell_format_left)
                        row += 1
                        number += 1
                
            row += 1
            worksheet.write(row+1, 1  , u'Хэвлэсэн: ........................................', cell_format_footer)
            row += 1    
            worksheet.write(row+1, 1  , u'Хүлээж авсан: ....................................', cell_format_footer)
            
            workbook.close()
            out = base64.encodestring(output.getvalue())
            file_name = u'Цагийн баланс_%s' % now

            excel_id = self.env['oderp.report.excel.output'].create({'filedata': out, 'filename': file_name+'.xlsx'})
            
            return {
                'name': 'Export Result',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'oderp.report.excel.output',
                'res_id': excel_id.id,
                'view_id': False,
                'context': self._context,
                'type': 'ir.actions.act_window',
                'target': 'new',
                'nodestroy': True,
            }
