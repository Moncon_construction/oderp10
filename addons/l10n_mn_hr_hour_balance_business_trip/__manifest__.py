# -*- coding: utf-8 -*-
{
    'name': "Mongolian HR Hour Balance Calculation with Business Trip",
    'version': '1.0',
    'depends': ['l10n_mn_hr_hour_balance',
                'l10n_mn_hr_business_trip'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Human Resources Modules',
    'description': """
       Employee Hour Balance Calculation with Business Trip
    """,
    'data': [
        'views/hour_balance_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}
