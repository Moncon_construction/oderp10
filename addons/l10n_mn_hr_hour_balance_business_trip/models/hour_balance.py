# -*- coding: utf-8 -*-
import logging
from datetime import datetime

import pytz
from dateutil import rrule
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT

_logger = logging.getLogger(__name__)


class HourBalance(models.Model):
    _inherit = "hour.balance"

    def get_linked_objects(self, object, employee, st_date, end_date):
        if object != 'business.trip.balance.line':
            return super(HourBalance, self).get_linked_objects(object, employee, st_date, end_date)
        
        domain = [('employee_id', '=', employee.sudo().id), ('trip_id.state', 'in', ['confirm', 'done'])]
        domain.extend(get_duplicated_day_domain('start_date', 'end_date', st_date, end_date, self.env.user))
        return self.env[object].sudo().search(domain)
        
    def get_linked_object_time_intervals(self, object, employee, st_date, end_date):
        if object != 'business.trip.balance.line':
            return super(HourBalance, self).get_linked_object_time_intervals(object, employee, st_date, end_date)
        
        time_intervals = []
        linked_objects = self.get_linked_objects(object, employee, st_date, end_date)
        for obj in linked_objects:
            start_date = get_display_day_to_user_day(datetime.strptime(obj.start_date + " 00:00:00", DEFAULT_SERVER_DATETIME_FORMAT), self.env.user)
            end_date = get_display_day_to_user_day(datetime.strptime(obj.end_date + " 23:59:59", DEFAULT_SERVER_DATETIME_FORMAT), self.env.user)
            time_intervals.append((start_date, end_date))
        
        return time_intervals
    
    def get_active_hours(self, values):
        self.ensure_one()
        active_hours = super(HourBalance, self).get_active_hours(values)
        active_hours += values.get('bus_trip_hours', 0)
        return active_hours
    
    def get_balance_line_values(self, employee_id, contract_id, check_contract_date=False):
        # @Override: Томилолтын цагийг тооцоолох
        self.ensure_one()
        values = super(HourBalance, self).get_balance_line_values(employee_id, contract_id, check_contract_date=check_contract_date)
        
        date_from, date_to = self.get_balance_dates(contract_id, check_contract_date=check_contract_date)
        if date_from and date_to:
            bus_trip_hours = self.get_total_hours_by_object('business.trip.balance.line', employee_id, contract_id, date_from, date_to)
            values['business_trip_hour'] = max(bus_trip_hours, 0)
        
        return values
    
    
class HourBalanceLine(models.Model):
    
    _inherit = 'hour.balance.line'
    
    def set_values_from_line(self):
        super(HourBalanceLine, self).set_values_from_line()
        for obj in self:
            obj.business_trip_hour = sum(obj.mapped('line_ids').mapped('business_trip_hour'))
            
    
