# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Mongolian Hour Balance Calculation with Calendar',
    'version': '1.0',
    'author': 'Asterisk Technologies LLC',
    'website' : 'http://asterisk-tech.mn',
    'depends': [
        'l10n_mn_hr_attendance_hour_balance', 
        'l10n_mn_calendar',
        'hr_timesheet_sheet',
        'hr_timesheet_attendance'
    ],
    'category': 'Mongolian Modules',
    'description': """
      Employee Hour balance calculation with Calendar
    """,
    'data': [
        'views/calendar.xml',
        'views/hour_balance_config_settings_view.xml',
        'views/hour_balance_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': True,
}
