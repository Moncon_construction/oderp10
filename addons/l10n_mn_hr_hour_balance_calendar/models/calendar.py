# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import UserError, ValidationError

class CalendarEvent(models.Model):
    _inherit = 'calendar.event'

    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.user_id.company_id or self.env.user.company_id)
    require_cnfrmtn_on_outwork = fields.Boolean(related='company_id.require_cnfrmtn_on_outwork', string='Require Confirmation on Outside work', readonly=True)

    @api.multi
    def confirm(self):
        self.write({'state': 'open'})
        
    @api.multi
    def reject(self):
        self.write({'state': 'draft'})
        
    @api.multi
    def write(self, vals):
        # Уулзалтын цагаарх хоцролт/эрт таралтын цагуудыг шинэчлэх
        partners = self.partner_ids
        state = vals['state'] if 'state' in vals.keys() else self.state
        was_outside_work = self.is_outside_work
        is_outside_work = vals['is_outside_work'] if 'is_outside_work' in vals.keys() else self.is_outside_work
        company_id = vals['company_id'] if 'company_id' in vals.keys() else self.company_id
        
        if state == 'open' and is_outside_work and company_id.require_cnfrmtn_on_outwork and (vals.get('start', False) or vals.get('stop', False) or vals.get('partner_ids', False)):
            raise UserError(_("You cannot change confirmed event's information !!!"))
        
        res = super(CalendarEvent, self).write(vals)
        
        if ('partner_ids' in vals.keys() or 'state' in vals.keys() or 'start' in vals.keys() or 'stop' in vals.keys()) and (is_outside_work or was_outside_work):
            partners |= self.partner_ids
            domain = [('employee_id.user_id.partner_id', 'in', partners.ids)]
            domain.extend(get_duplicated_day_domain('work_start_time', 'work_end_time', self.start, self.stop, self.env.user))
            checkable_attendances = self.env['hr.attendance'].sudo().search(domain).sudo()
            checkable_attendances.compute_total_lag_minutes()
            checkable_attendances.compute_early_leave_minutes()
        
        return res
    
    @api.multi
    def unlink(self):
        if self.filtered(lambda x: x.is_outside_work and x.company_id.require_cnfrmtn_on_outwork and x.state == 'open'):
            raise UserError(_("You cannot delete confirmed event !!!"))
        
        # Уулзалтын цагаарх хоцролт/эрт таралтын цагуудыг шинэчлэх
        checkable_attendances = self.env['hr.attendance'].sudo()
        for obj in self.filtered(lambda x: x.state == 'open' and x.is_outside_work and x.partner_ids):
            domain = [('employee_id.user_id.partner_id', 'in', obj.partner_ids.ids)]
            domain.extend(get_duplicated_day_domain('work_start_time', 'work_end_time', obj.start, obj.stop, self.env.user))
            checkable_attendances |= self.env['hr.attendance'].sudo().search(domain).sudo()
            
        res = super(CalendarEvent, self).unlink()
        
        if checkable_attendances:
            checkable_attendances.compute_total_lag_minutes()
            checkable_attendances.compute_early_leave_minutes()
        
        return res