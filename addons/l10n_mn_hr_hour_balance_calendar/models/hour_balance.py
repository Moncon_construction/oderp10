# -*- coding: utf-8 -*-
import logging
from datetime import datetime, timedelta

import pytz
from dateutil import rrule
from dateutil.relativedelta import relativedelta

from odoo import _, api, fields, models
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT

_logger = logging.getLogger(__name__)


class HourBalance(models.Model):
    _inherit = "hour.balance"

    def get_linked_objects(self, object, employee, st_date, end_date):
        if object != 'calendar.event':
            return super(HourBalance, self).get_linked_objects(object, employee, st_date, end_date)
        
        results = []

        partner_id = employee.address_home_id
        if employee.user_id and employee.user_id.sudo().partner_id and not partner_id:
            partner_id = employee.user_id.sudo().partner_id
        if partner_id:
            domain = [('company_id', '=', employee.company_id.id), ('is_outside_work', '=', True), ('partner_ids', 'in', partner_id.id)]
            domain.extend(get_duplicated_day_domain('start', 'stop', st_date, end_date, self.env.user))
            if employee.company_id.require_cnfrmtn_on_outwork:
                domain.append(('state', '=', 'open'))
            results = self.env['calendar.event'].sudo().search(domain)

        return results

    def get_linked_object_time_intervals(self, object, employee, st_date, end_date):
        if object != 'calendar.event':
            return super(HourBalance, self).get_linked_object_time_intervals(object, employee, st_date, end_date)
        
        time_intervals = []
        linked_objects = self.get_linked_objects(object, employee, st_date, end_date)
        for obj in linked_objects:
            start_date = datetime.strptime(obj.start, DEFAULT_SERVER_DATETIME_FORMAT)
            end_date = datetime.strptime(obj.stop, DEFAULT_SERVER_DATETIME_FORMAT)
            time_intervals.append((start_date, end_date))
        
        return time_intervals
    
    def get_active_hours(self, values):
        self.ensure_one()
        active_hours = super(HourBalance, self).get_active_hours(values)
        active_hours += values.get('out_working_hour', 0)
        return active_hours
    
    def get_balance_line_values(self, employee_id, contract_id, check_contract_date=False):
        # @Override: Гадуур уулзалтын цаг тооцоолох
        self.ensure_one()
        values = super(HourBalance, self).get_balance_line_values(employee_id, contract_id, check_contract_date=check_contract_date)
        
        date_from, date_to = self.get_balance_dates(contract_id, check_contract_date=check_contract_date)
        if date_from and date_to:
            out_working_hour = self.get_total_hours_by_object('calendar.event', employee_id, contract_id, date_from, date_to)
            values['out_working_hour'] = max(out_working_hour, 0)
        
        return values
    
    
class HourBalanceLine(models.Model):
    
    _inherit = 'hour.balance.line'
    
    def set_values_from_line(self):
        super(HourBalanceLine, self).set_values_from_line()
        for obj in self:
            obj.out_working_hour = sum(obj.mapped('line_ids').mapped('out_working_hour'))
