# -*- coding: utf-8 -*-
from odoo import fields, models

class HourBalanceConfigSettings(models.TransientModel):
    _inherit = 'hour.balance.config.settings'

    require_cnfrmtn_on_outwork = fields.Boolean(string='Require Confirmation on Outside work', related='company_id.require_cnfrmtn_on_outwork', placeholder='When this checked, out work calculation of hour balance only calculate from confirmed calendar meetings.')