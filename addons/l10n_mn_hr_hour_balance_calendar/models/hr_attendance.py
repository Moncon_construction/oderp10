# -*- coding: utf-8 -*-
from datetime import datetime, timedelta

import pytz
from dateutil import rrule
from dateutil.relativedelta import relativedelta

from odoo import _, api, fields, models
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT


class HrAttendance(models.Model):
    _inherit = "hr.attendance"
    
    def get_linked_out_working(self):
        # Ажиллах ёстой цагаар давхцуулж үүсгэсэн гадуур уулзалтуудыг шүүж авах
        result = []
        for obj in self:
            if not (obj.work_start_time and obj.work_end_time):
                continue
            
            if self.employee_id.sudo().user_id and self.employee_id.sudo().user_id.sudo().partner_id:
                domain = [('company_id', '=', self.company_id.id), ('is_outside_work', '=', True), ('partner_ids', 'in', [self.employee_id.sudo().user_id.sudo().partner_id.id])]
                domain.extend(get_duplicated_day_domain('start', 'stop', obj.work_start_time, obj.work_end_time, self.env.user))
                if obj.company_id.require_cnfrmtn_on_outwork:
                    domain.append(('state', '=', 'open'))
                
                result.extend(self.env['calendar.event'].sudo().search(domain))
            
        return result
                    
    def get_duplicated_calendar_hours(self, st_date, end_date, employee):
        # st_date, end_date цагуудын хооронд давхцаж буй цаг бүхий гадуур уулзалтуудын давхцсан ЦАГуудыг олох
        calendar_event_ids = self.get_linked_out_working()
        calendar_hours = 0

        for calendar_event in calendar_event_ids:
            calendar_st_date = get_day_by_user_timezone(calendar_event.start, self.env.user).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            calendar_end_date = get_day_by_user_timezone(calendar_event.stop, self.env.user).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            from_date = get_day_by_user_timezone(st_date, self.env.user).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            to_date = get_day_by_user_timezone(end_date, self.env.user).strftime(DEFAULT_SERVER_DATETIME_FORMAT)

            diff = get_difference_btwn_2date_intervals(calendar_st_date, calendar_end_date, from_date, to_date)
            calendar_hours += diff if diff > 0 else calendar_hours

        return calendar_hours
    
    @api.depends('check_out', 'work_end_time', 'employee_id')
    def compute_early_leave_minutes(self):
        # Батлагдсан гадуур ажлыг эрт тарсан минутаас хасаж тооцдог болгов
        super(HrAttendance, self).compute_early_leave_minutes()
        
        for obj in self.filtered(lambda x: x.early_leave_minutes > 0):
            if obj.company_id.require_cnfrmtn_on_outwork:
                early_leave_minutes = obj.early_leave_minutes
                calendar_minutes_during_work_time = obj.get_duplicated_calendar_hours(obj.check_out, obj.work_end_time, obj.employee_id) * 60
                obj.early_leave_minutes = (early_leave_minutes - calendar_minutes_during_work_time) if (early_leave_minutes - calendar_minutes_during_work_time) > 0 else 0
                
    @api.depends('check_in', 'work_start_time', 'employee_id')
    def compute_total_lag_minutes(self):
        # Батлагдсан гадуур ажлыг хоцролтоос хасаж тооцдог болгов
        super(HrAttendance, self).compute_total_lag_minutes()
        
        for obj in self.filtered(lambda x: x.total_lag_minutes > 0):
            if obj.work_start_time and obj.check_in and obj.employee_id and obj.employee_id.company_id.require_cnfrmtn_on_outwork:
                total_lag_minutes = obj.total_lag_minutes
                calendar_minutes_during_work_time = obj.get_duplicated_calendar_hours(obj.work_start_time, obj.check_in, obj.employee_id) * 60
                obj.total_lag_minutes = (total_lag_minutes - calendar_minutes_during_work_time) if (total_lag_minutes - calendar_minutes_during_work_time) > 0 else 0

    