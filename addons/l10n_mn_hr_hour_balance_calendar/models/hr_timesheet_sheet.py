# -*- coding: utf-8 -*-
import logging
from datetime import date
from datetime import datetime
from datetime import timedelta
from dateutil import rrule

import pytz

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

_logger = logging.getLogger(__name__)

class HRTimesheetSheet(models.Model):
    _inherit = "hr_timesheet_sheet.sheet"

    @api.one
    def _calculate_out_working(self):
        """
            Календараас гадуур ажлыг тооцохдоо:
                Хэрвээ тухайн компани дээр ГАДУУР АЖЛЫГ БАТАЛГААЖУУЛАХЫГ ШААРДАХ чек нь чеклэгдсэн байвал БАТАЛГААЖСАН төлөвтэй гадуур ажлуудаас тооцдог болгов.
        """
        out_working = 0.0
        calendar_obj = self.env['calendar.event']
 
        start_dt = datetime.strptime(self.date_from, '%Y-%m-%d')
        end_dt = datetime.strptime(self.date_to, '%Y-%m-%d')
 
        if self.employee_id:
            if self.employee_id.user_id:
                if self.employee_id.user_id.partner_id:
                    domain = [('company_id', '=', self.company_id.id), ('is_outside_work', '=', True), ('partner_ids', 'in', self.employee_id.user_id.partner_id.id)]
                    if self.company_id.require_cnfrmtn_on_outwork:
                        domain.append(('state', '=', 'open'))
                    calendar_ids = calendar_obj.search(domain)
                    for item in calendar_ids:
                        if self.date_from <= item.start and item.start <= self.date_to:
                            out_working += item.duration

        self.total_out_working = out_working


