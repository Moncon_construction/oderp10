# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError
import logging
import time

_logger = logging.getLogger(__name__)

class ResCompany(models.Model):
    _inherit = 'res.company'

    require_cnfrmtn_on_outwork = fields.Boolean(string='Require Confirmation on Outside work', default=False)
