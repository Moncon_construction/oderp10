# -*- coding: utf-8 -*-

{
    'name': "Mongolian HR - Hour balance Calculation with Overtime",
    'version': '1.0',
    'depends': [
        'l10n_mn_hr_hour_balance', 'l10n_mn_hr_overtime'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
      Linker Module for: Employee Hour balance calculation with Overtime Request
    """,
    'data': [
        'views/hour_balance_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}
