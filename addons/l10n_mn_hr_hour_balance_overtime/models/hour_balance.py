# -*- coding: utf-8 -*-
from datetime import datetime
from dateutil import rrule
import logging

from odoo import api, fields, models
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT

_logger = logging.getLogger(__name__)


class HourBalance(models.Model):
    _inherit = "hour.balance"
    
    def get_confirmed_overtime(self, employee, date_from, date_to):
        self.ensure_one()

        st_date = str(get_display_day_to_user_day(date_from, self.env.user))
        end_date = str(get_display_day_to_user_day(date_to, self.env.user))
        reqsts = self.env['hr.overtime.request.line'].search([('overtime_request_id.overtime_type', '=', 'overtime_normal'), ('overtime_request_id.state', '=', 'done'), ('employee_id', '=', employee.id), ('date', '>=', st_date), ('date', '<=', end_date)])
        reqsts_on_holiday = self.env['hr.overtime.request.line'].search([('overtime_request_id.overtime_type', '=', 'overtime_holiday'), ('overtime_request_id.state', '=', 'done'), ('employee_id', '=', employee.id), ('date', '>=', st_date), ('date', '<=', end_date)])
        normal_rqsts = sum(req.overtime for req in reqsts) if (reqsts and len(reqsts) > 0) else 0
        holiday_rqsts = sum(req.overtime for req in reqsts_on_holiday) if (reqsts_on_holiday and len(reqsts_on_holiday) > 0) else 0
            
        return normal_rqsts, holiday_rqsts
    
    def get_balance_line_values(self, employee_id, contract_id, check_contract_date=False):
        # @Override: Илүү цагуудыг тооцоолох
        self.ensure_one()
        values = super(HourBalance, self).get_balance_line_values(employee_id, contract_id, check_contract_date=check_contract_date)
        
        date_from, date_to = self.get_balance_dates(contract_id, check_contract_date=check_contract_date)
        if date_from and date_to:
            overtime_hours = self.get_confirmed_overtime(employee_id, date_from, date_to)
            confirmed_overtime = overtime_hours[0]
            confirmed_overtime_holiday = overtime_hours[1]
            values['confirmed_overtime'] = max(confirmed_overtime, 0)
            values['confirmed_overtime_holiday'] = max(confirmed_overtime_holiday, 0)
            
        return values
    

class HourBalanceLine(models.Model):
    _inherit = "hour.balance.line"
    
    confirm_overtime = fields.Boolean(related='company_id.confirm_overtime', readonly=True, string='Confirmed Overtime')
    confirm_overtime_holiday = fields.Boolean(related='company_id.confirm_overtime_holiday', readonly=True, string='Confirmed Overtime Holiday')
    
    def set_values_from_line(self):
        super(HourBalanceLine, self).set_values_from_line()
        for obj in self:
            confirm_overtime, confirm_overtime_holiday = 0, 0
            for line_id in obj.line_ids:
                confirm_overtime += line_id.confirm_overtime
                confirm_overtime_holiday += line_id.confirm_overtime_holiday
            obj.confirm_overtime = confirm_overtime
            obj.confirm_overtime_holiday = confirm_overtime_holiday
    
    
class HourBalanceLineLine(models.Model):
    _inherit = "hour.balance.line.line"
    
    confirm_overtime = fields.Boolean(related='company_id.confirm_overtime', readonly=True, string='Confirmed Overtime')
    confirm_overtime_holiday = fields.Boolean(related='company_id.confirm_overtime_holiday', readonly=True, string='Confirmed Overtime Holiday')


