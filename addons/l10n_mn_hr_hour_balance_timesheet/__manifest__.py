# -*- coding: utf-8 -*-

{
    'name': "Mongolian HR - Hour balance timesheet",
    'version': '1.0',
    'depends': [
        'l10n_mn_hr_hour_balance',
        'hr_timesheet_sheet',
        'l10n_mn_hr_attendance_hour_balance'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
      Employee Hour balance
    """,
    'data': [
        'views/hour_balance_view.xml',
        'views/hr_timesheet_sheet_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}
