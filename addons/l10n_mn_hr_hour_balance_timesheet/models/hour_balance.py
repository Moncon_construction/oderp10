# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
from lxml import etree

from datetime import datetime
from datetime import timedelta
from dateutil import rrule

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.osv.orm import setup_modifiers
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT

_logger = logging.getLogger(__name__)

class HourBalance(models.Model):
    _inherit = "hour.balance"
    
    timesheet_line_ids = fields.One2many('hr_timesheet_sheet.sheet', 'balance_id', string='Employee Timesheet')
    
    @api.multi
    def delete_timesheet(self):
        timesheet_obj = self.env['hr_timesheet_sheet.sheet']
        for hour_balance in self:
            timesheet_ids = timesheet_obj.search([('balance_id', '=', hour_balance.id)])
            if timesheet_ids:
                for timesheet in timesheet_ids:
                    timesheet.action_timesheet_draft()
                    self.env.cr.execute('''DELETE FROM hr_timesheet_sheet_sheet WHERE id = %s''' % timesheet.id)

            for line in hour_balance.balance_line_ids:
                line.write({'state': 'draft'})

            hour_balance.write({'state': 'draft'})

        return True
    
    def get_timesheets(self, employee):
        self.ensure_one()
        timesheets = []
        timesheet_obj = self.env['hr_timesheet_sheet.sheet']
        if self.salary_type == 'advance_salary':
            timesheets = timesheet_obj.search(['&', ('state', '=', 'done'), '&', ('balance_id', '=', self.id), ('employee_id', '=', employee)])
        elif self.no_advance_balance:
            timesheets = timesheet_obj.search(['&', ('state', '=', 'done'), '&', ('balance_id', '=', self.id), ('employee_id', '=', employee)])
        else:
            timesheets = timesheet_obj.search(['&', ('state', '=', 'done'), '&', ('balance_id', 'in', [self.advance_salary_id.id, self.id]), ('employee_id', '=', employee)])
            
        return timesheets
    
    def get_timesheet_hour(self, employee, date_from, date_to):
        # 'Цагийн хуудсын цаг' буцаах функц /total_timesheet - д ашиглах/
        self.ensure_one()
        analytic_lines = []
        total_amount = 0
        analytic_line_obj = self.env['account.analytic.line']
        
        timesheets = self.get_timesheets(employee.sudo().id)
        for timesheet in timesheets:
            analytic_lines = analytic_line_obj.search([('sheet_id', '=', timesheet.id), ('date', '>=', date_from), ('date', '<=', date_to)])
            for line in analytic_lines:
                total_amount += line.unit_amount
        return total_amount


    @api.multi
    def generate_timesheet(self):
        employee_obj = self.env['hr.employee']
        timesheet_obj = self.env['hr_timesheet_sheet.sheet']
        contract_obj = self.env['hr.contract']
        for balance in self:
            con_employee_ids = []

            # Ирцээр цалин бодох эсвэл цагийн хуудас автоматаар үүсгэх гэсэн сонголттой бол цагийн хуудас үүсгэнэ.
            con_employee_ids = [con.employee_id.id for con in contract_obj.search(['|', ('hour_balance_calculate_type', '=', 'payroll_by_attendance'), ('hour_balance_calculate_type', '=', 'is_salary_complete')])]
            employees = []
            
            # 'Урьдчилгаа цалин' төрөлтэй цагийн баланс үед 'Жирэмсэн' болон 'Ажлаас гарсан' төлөвтэй ажилчдад ҮҮСГЭХГҮЙ байх
            ignored_states = ['resigned', 'maternity'] if balance.salary_type == 'advance_salary' else ['resigned']
            ignored_statuses = self.env['hr.employee.status'].search([('type', 'in', ignored_states)])
            if ignored_statuses and len(ignored_statuses) > 0:
                if balance.department_id:
                    employees = employee_obj.search([('active', '=', True), ('state_id', 'not in', ignored_statuses.ids), ('id', 'in', con_employee_ids), ('department_id', 'child_of', [balance.department_id.id])])
                else:
                    employees = employee_obj.search([('active', '=', True), ('state_id', 'not in', ignored_statuses.ids), ('id', 'in', con_employee_ids)])
            else:
                if balance.department_id:
                    employees = employee_obj.search([('active', '=', True), ('id', 'in', con_employee_ids), ('department_id', 'child_of', [balance.department_id.id])])
                else:
                    employees = employee_obj.search([('active', '=', True), ('id', 'in', con_employee_ids)])

            date_from = balance.balance_date_from
            date_to = balance.balance_date_to

            for employee in employees:
                if employee.user_id:
                    overlapping_timesheets = timesheet_obj.search([('employee_id', '=', employee.id), '|',
                                                                   '&', ('date_from', '>=', date_from),
                                                                   ('date_from', '<=', date_to),
                                                                   '&', ('date_to', '>=', date_from),
                                                                   ('date_to', '<=', date_to)])
                    if overlapping_timesheets:
                        raise UserError(_("The %s employee can not 2 timesheets that overlap") % employee.name)

                    timesheet = timesheet_obj.create({
                        'employee_id': employee.id,
                        'date_from': date_from,
                        'date_to': date_to,
                        'user_id': employee.user_id.id,
                        'balance_id': balance.id,
                    })

                    timesheet.action_timesheet_confirm()
                    timesheet.action_timesheet_done()
                else:
                    raise UserError(_("No user found for employee %s" % employee.name))

        return self.write({'state': 'timesheet_generated'})

    def get_active_hours(self, values):
        self.ensure_one()
        active_hours = super(HourBalance, self).get_active_hours(values)
        active_hours += values.get('timesheet_hour', 0)
        return active_hours
    
    def get_balance_line_values(self, employee_id, contract_id, check_contract_date=False):
        # @Override: Цагийн хуудсын цаг тооцоолох
        self.ensure_one()
        values = super(HourBalance, self).get_balance_line_values(employee_id, contract_id, check_contract_date=check_contract_date)
        
        date_from, date_to = self.get_balance_dates(contract_id, check_contract_date=check_contract_date)
        if date_from and date_to:
            timesheet_hour = self.get_timesheet_hour(employee_id, date_from, date_to)
            values['timesheet_hour'] = max(timesheet_hour, 0)
        
        return values
    

class HourBalanceLine(models.Model):
    _inherit = "hour.balance.line"
    
    def set_values_from_line(self):
        super(HourBalanceLine, self).set_values_from_line()
        for obj in self:
            obj.timesheet_hour = sum(obj.mapped('line_ids').mapped('timesheet_hour'))
    

class ResourceCalendar(models.Model):
    _inherit = "resource.calendar"

    work_hours_day = fields.Float('To work hours a day', default=8.0)
