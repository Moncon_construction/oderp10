# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
from lxml import etree

from datetime import datetime
from datetime import timedelta
from dateutil import rrule

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.osv.orm import setup_modifiers

_logger = logging.getLogger(__name__)

class HrContract(models.Model):
    _inherit = "hr.contract"

    hour_balance_calculate_type = fields.Selection([
                                                    ('payroll_by_attendance', 'Payroll By Attendance'),
                                                    ('is_salary_complete', 'Is Salary Complete'),
                                                    ('payroll_by_roster', 'Payroll By Roster'),
                                                    ('payroll_by_timesheet', 'Payroll By timesheet'),
                                                ], string='Hour balance calculate type', copy=False )
