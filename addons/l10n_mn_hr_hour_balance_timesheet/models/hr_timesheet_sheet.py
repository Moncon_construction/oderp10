# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.


import logging
from datetime import date
from datetime import datetime
from datetime import timedelta
from dateutil import rrule

import pytz

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

_logger = logging.getLogger(__name__)


class HRTimesheetSheet(models.Model):
    _inherit = "hr_timesheet_sheet.sheet"

    balance_id = fields.Many2one('hour.balance', string='Hour balance', ondelete='restrict')
    is_user = fields.Boolean(compute='_is_user', string='Is user', default=False)
    
    def _is_user(self):
        if self.user_id.id == self.env.user.id and self.state == 'confirm':
            self.is_user = True
        else:
            self.is_user = False

    @api.multi
    def refuse_multiple_timesheet(self):
        self.action_timesheet_draft()

    @api.multi
    def action_timesheet_confirm_to_draft(self):
        self.write({'state': 'draft'})
        
    @api.multi
    def get_all_holiday_hours(self, st_date, end_date, employee_id, limit):
        holiday_hours = 0
        holiday_obj = self.env['hr.holidays']

        # Search for the employee's approved holidays between the given dates
        holidays = holiday_obj.search([('employee_id', '=', employee_id), ('state', '=', 'validate'), ('type', '=', 'remove'),
                                       '|',
                                       '&', ('date_from', '<=', st_date),
                                       ('date_to', '>=', end_date),
                                       '|',
                                       '&', ('date_from', '>=', st_date),
                                       ('date_from', '<=', end_date),
                                       '&', ('date_to', '>=', st_date),
                                       ('date_to', '<=', end_date),
                                       ])

        list_ids = []
        for item in holidays:
            list_ids.append(item.id)

        if holidays:
            until = (datetime.strptime(end_date, '%Y-%m-%d %H:%M:%S') + timedelta(days=0))
            for day in rrule.rrule(rrule.DAILY, dtstart=datetime.strptime(st_date, '%Y-%m-%d %H:%M:%S'),
                                   until=until, byweekday=holidays[0].employee_id.contract_id.working_hours.get_weekdays()):
                day_start_dt = day.replace(hour=0, minute=0, second=0)
                day_start_str = day_start_dt.strftime('%Y-%m-%d')
                holidays_ids = list_ids
                day_holidays_ids = holiday_obj.search([('id', 'in', holidays_ids), ('date_from', '<=', day_start_str), ('date_to', '>=', day_start_str)])

                for holiday in day_holidays_ids:
                    start_date = day_start_dt
                    if start_date.date() == datetime.strptime(st_date, '%Y-%m-%d %H:%M:%S').date() or start_date.date() == datetime.strptime(holiday.date_from, '%Y-%m-%d %H:%M:%S').date():
                        if datetime.strptime(holiday.date_from, '%Y-%m-%d %H:%M:%S') > datetime.strptime(st_date, '%Y-%m-%d %H:%M:%S'):
                            start_date = datetime.strptime(holiday.date_from, '%Y-%m-%d %H:%M:%S')
                        else:
                            start_date = datetime.strptime(st_date, '%Y-%m-%d %H:%M:%S')

                    day_end_dt = start_date.replace(hour=23, minute=59, second=59)
                    if day_end_dt.date() == datetime.strptime(end_date, '%Y-%m-%d %H:%M:%S').date() or day_end_dt.date() == datetime.strptime(holiday.date_to, '%Y-%m-%d %H:%M:%S').date():
                        if datetime.strptime(holiday.date_to, '%Y-%m-%d %H:%M:%S') < datetime.strptime(end_date, '%Y-%m-%d %H:%M:%S'):
                            day_end_dt = datetime.strptime(holiday.date_to, '%Y-%m-%d %H:%M:%S')
                        else:
                            day_end_dt = datetime.strptime(end_date, '%Y-%m-%d %H:%M:%S')

                    resource_calendar = holiday.employee_id.contract_id.working_hours
                    hours = resource_calendar.get_working_hours_of_date(start_dt=start_date, end_dt=day_end_dt,
                                                                        compute_leaves=True, resource_id=None, default_interval=None)

                    holiday_hours += hours

        return holiday_hours

    @api.multi
    def get_holiday_hours(self, st_date, end_date, employee_id):
        _logger.info("get_holiday_hours() FUNCTION CALLED :: %s " % employee_id)
        _logger.info("START DATE :: %s " % st_date)
        _logger.info("END DATE :: %s " % end_date)

        # by default timezone = UTC
        user_time_zone = pytz.UTC
        if self.env.user.partner_id.tz:
            # change the timezone to the timezone of the user
            user_time_zone = pytz.timezone(self.env.user.partner_id.tz)
        else:
            raise ValidationError(_('Warning !\n Please. You set your time zone. Settings go to the User menu!'))

        _logger.info('USER:: % s' % self.env.user)
        _logger.info('USER TIME ZONE:: % s' % user_time_zone)

        st_date = user_time_zone.localize(datetime.strptime(st_date, DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(pytz.utc).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        end_date = user_time_zone.localize(datetime.strptime(end_date, DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(pytz.utc).strftime(DEFAULT_SERVER_DATETIME_FORMAT)

        _logger.info("START DATE /TIME ZONE CONVERTED/:: %s " % st_date)
        _logger.info("END DATE  /TIME ZONE CONVERTED/:: %s " % end_date)

        holiday_obj = self.env['hr.holidays']
        paid_holiday = unpaid_holiday = annual_leave = sick_leave = 0

        # Search for the employee's approved holidays between the given dates
        holidays_ids = holiday_obj.search([('employee_id', '=', employee_id.id), ('state', '=', 'validate'), ('type', '=', 'remove'),
                                           '|',
                                           '&', ('date_from', '<=', st_date),
                                           ('date_to', '>=', end_date),
                                           '|',
                                           '&', ('date_from', '>=', st_date),
                                           ('date_from', '<=', end_date),
                                           '&', ('date_to', '>=', st_date),
                                           ('date_to', '<=', end_date), ])
        _logger.info("Holiday IDS:: %s" % holidays_ids)

        list_ids = []
        for item in holidays_ids:
            list_ids.append(item.id)

        if holidays_ids:
            holidays = holidays_ids
            if holidays:
                until = (datetime.strptime(end_date, '%Y-%m-%d %H:%M:%S') + timedelta(days=1))
                for day in rrule.rrule(rrule.DAILY, dtstart=datetime.strptime(st_date, '%Y-%m-%d %H:%M:%S'),
                                       until=until, byweekday=holidays[0].employee_id.contract_id.working_hours.get_weekdays()):
                    _logger.info("DAY:: %s" % day)
                    day_start_dt = day.replace(hour=0, minute=0, second=0)
                    day_start_str = day_start_dt.strftime('%Y-%m-%d')
                    holidays_ids = list_ids

                    day_holidays_ids = holiday_obj.search([('id', 'in', holidays_ids), ('date_from', '<=', day_start_str), ('date_to', '>=', day_start_str)])

                    for holiday in day_holidays_ids:
                        start_date = day_start_dt
                        if start_date.date() == datetime.strptime(st_date, '%Y-%m-%d %H:%M:%S').date() or start_date.date() == datetime.strptime(holiday.date_from, '%Y-%m-%d %H:%M:%S').date():
                            if datetime.strptime(holiday.date_from, '%Y-%m-%d %H:%M:%S') > datetime.strptime(st_date, '%Y-%m-%d %H:%M:%S'):
                                start_date = datetime.strptime(holiday.date_from, '%Y-%m-%d %H:%M:%S')
                            else:
                                start_date = datetime.strptime(st_date, '%Y-%m-%d %H:%M:%S')
                        day_end_dt = start_date.replace(hour=23, minute=59, second=59)
                        if day_end_dt.date() == datetime.strptime(end_date, '%Y-%m-%d %H:%M:%S').date() or day_end_dt.date() == datetime.strptime(holiday.date_to, '%Y-%m-%d %H:%M:%S').date():
                            if datetime.strptime(holiday.date_to, '%Y-%m-%d %H:%M:%S') < datetime.strptime(end_date, '%Y-%m-%d %H:%M:%S'):
                                day_end_dt = datetime.strptime(holiday.date_to, '%Y-%m-%d %H:%M:%S')
                            else:
                                day_end_dt = datetime.strptime(end_date, '%Y-%m-%d %H:%M:%S')

                        resource_calendar = holiday.employee_id.contract_id.working_hours
                        hours = resource_calendar.get_working_hours_of_date(start_dt=start_date, end_dt=day_end_dt,
                                                                            compute_leaves=True, resource_id=None, default_interval=None)

                        _logger.info("DAY HOLIDAY IDS:: %s - Hours: %s" % (day_holidays_ids, hours))

                        if hours > 0:
                            if holiday.holiday_status_id.balance_type == 'paid':
                                paid_holiday += hours
                            elif holiday.holiday_status_id.balance_type == 'unpaid':
                                unpaid_holiday += hours
                            elif holiday.holiday_status_id.balance_type == 'annual_leave':
                                annual_leave += hours
                            elif holiday.holiday_status_id.balance_type == 'sick_leave':
                                sick_leave += hours

                        else:
                            pass

        return paid_holiday, unpaid_holiday, annual_leave, sick_leave

    @api.one
    def _calculate_holidays(self):
        date_from = datetime.strptime(self.date_from, '%Y-%m-%d').replace(hour=0, minute=0, second=0).strftime('%Y-%m-%d %H:%M:%S')
        date_to = datetime.strptime(self.date_to, '%Y-%m-%d').replace(hour=23, minute=59, second=59).strftime('%Y-%m-%d %H:%M:%S')
        paid_holiday, unpaid_holiday, annual_leave, sick_leave = self.get_holiday_hours(date_from, date_to, self.employee_id)

        self.total_paid_holidays = paid_holiday
        self.total_unpaid_holidays = unpaid_holiday
        self.total_annual_leaves = annual_leave
        self.total_sick_leave = sick_leave

    @api.one
    def _calc_working_time(self):
        _logger.info("_calc_working_time() FUNCTION CALLED :: %s" % (self.employee_id.id))
        attendance_hour = lag_hour = worked_day = 0.0
        attendances_obj = self.env['hr.attendance']
        start_interval = end_interval = None
        company_id = self.env.user.company_id

        user_tz = pytz.utc if not self.env.user.partner_id.tz else pytz.timezone(self.env.user.partner_id.tz)

        attendances = attendances_obj.search([('sheet_id', '=', self.id)])
        _logger.info("LEN:: %s" % len(attendances))
        if attendances:
            date_from = None
            date_to = None

            for attendance in attendances:
                if attendance.check_in and attendance.check_out:
                    date_from = datetime.strptime(attendance.check_in, '%Y-%m-%d %H:%M:%S')
                    date_to = datetime.strptime(attendance.check_out, '%Y-%m-%d %H:%M:%S')
                if date_to and date_from:
                    if ((pytz.utc.localize(date_from)).astimezone(user_tz)).date() == ((pytz.utc.localize(date_to)).astimezone(user_tz)).date():
                        if date_from.date() != date_to.date():
                            date_from = ((pytz.utc.localize(date_from).astimezone(user_tz)).replace(tzinfo=None)).replace(day=date_to.day, hour=0, minute=0, second=0)
                        resource_calendar = self.employee_id.contract_id.working_hours
                        att_hour = resource_calendar.get_working_hours_of_date(start_dt=date_from, end_dt=date_to, compute_leaves=True, resource_id=None, default_interval=None)

                        _logger.info("DATE FROM: %s DATE TO: %s ATT HOUR:: %s" % (date_from, date_to, att_hour))

                        if att_hour > 0:
                            worked_day += 1

                        attendance_hour += att_hour

                        # working hour intervals
                        intervals = resource_calendar.get_working_intervals_of_day(date_from.replace(hour=0, minute=0, second=0), date_to.replace(hour=23, minute=59, second=59),
                                                                                   leaves=None, compute_leaves=True, resource_id=None, default_interval=None)

                        # _logger.info("Intervals:: %s" % intervals)
                        if intervals:
                            start_interval = intervals[0][0]
                            end_interval = intervals[len(intervals) - 1][1]
                            date = date_from
                            if company_id.calculate_lag_by_limit:
                                date_start_max = start_interval + timedelta(minutes=company_id.lag_limit)
                                if date <= end_interval and date >= start_interval and date <= date_start_max:
                                    holiday_hours = 0
                                    hours = resource_calendar.get_working_hours_of_date(start_dt=start_interval, end_dt=date_from,
                                                                                        compute_leaves=True, resource_id=None, default_interval=None)

                                    lag_hour += hours
                            else:
                                _logger.info("Date:: %s - Start interval: %s" % (date, start_interval))
                                if date >= start_interval:
                                    holiday_hours = 0
                                    holiday_hours += self.get_all_holiday_hours(start_interval.strftime('%Y-%m-%d %H:%M:%S'), date.strftime('%Y-%m-%d %H:%M:%S'), self.employee_id.id, 'unpaid')

                                    hours = resource_calendar.get_working_hours_of_date(start_dt=start_interval, end_dt=date_from,
                                                                                        compute_leaves=True, resource_id=None, default_interval=None)

                                    lag_hour += (hours - holiday_hours)
                                    print "HOURS:: %s ---- HOLIDAY HOURS::%s ---- LAG HOUR:: %s" % (hours, holiday_hours, lag_hour)

        _logger.info("LAG HOUR:: %s" % lag_hour)
        _logger.info("ATT HOUR:: %s" % attendance_hour)
        self.total_worked_days = worked_day
        self.total_working_time = attendance_hour
        self.total_lag_hour = lag_hour

    @api.one
    def _calculate_holiday_overtime(self):
        holiday_overtime = 0.0
        attendance_obj = self.pool.get('hr.attendance')

        start_dt = datetime.strptime(self.date_from, '%Y-%m-%d')
        end_dt = datetime.strptime(self.date_to, '%Y-%m-%d')

        # Search for the employee's leaves between the given dates
        leaves = self.employee_id.contract_id.working_hours.get_leave_intervals(resource_id=self.employee_id.contract_id.working_hours.id, start_datetime=start_dt, end_datetime=end_dt)
        for leave in leaves:
            attendances_ids = attendance_obj.search(self.env.cr, self.env.user.id, [('name', '>=', leave[0].strftime('%Y-%m-%d %H:%M:%S')), ('name', '<=', leave[1].strftime('%Y-%m-%d %H:%M:%S')), ('sheet_id', '=', self.id)], order='name ASC')
            if attendances_ids:
                attendances = attendance_obj.browse(self.env.cr, self.env.user.id, attendances_ids)
                pre_attendance = None
                first_attendance = attendances[0]
                for attendance in attendances:
                    if attendance.check_out and attendance.id != first_attendance.id:
                        if pre_attendance:
                            date_to = datetime.strptime(attendance.name, '%Y-%m-%d %H:%M:%S')
                            date_from = datetime.strptime(pre_attendance.name, '%Y-%m-%d %H:%M:%S')
                            if date_to and date_from:
                                att_hour = self.employee_id.contract_id.working_hours.get_working_hours_of_date(start_dt=date_from, end_dt=date_to,
                                                                                                                compute_leaves=True, resource_id=None, default_interval=None)
                                holiday_overtime += att_hour
                    else:
                        pre_attendance = attendance

        self.total_holiday_overtime = holiday_overtime

    @api.one
    def _calculate_out_working(self):
        out_working = 0.0
#         calendar_obj = self.env['calendar.event']
# 
#         start_dt = datetime.strptime(self.date_from, '%Y-%m-%d')
#         end_dt = datetime.strptime(self.date_to, '%Y-%m-%d')
# 
#         if self.employee_id:
#             if self.employee_id.user_id:
#                 if self.employee_id.user_id.partner_id:
#                     calendar_ids = calendar_obj.search([('partner_ids', 'in', self.employee_id.user_id.partner_id.id)])
#                     for item in calendar_ids:
#                         if item.is_outside_work:
#                             if self.date_from <= item.start and item.start <= self.date_to:
#                                 print item.start, '----' ,item.duration
#                                 # dur = item.duration
#                                 out_working += item.duration

        self.total_out_working = out_working


    # TODO: Get holiday overtime from resource calendar
    # total_holiday_overtime = fields.Float(compute='_calculate_holiday_overtime',string='Holiday Overtime')
    total_annual_leaves = fields.Float(compute='_calculate_holidays', string='Total Annual Leaves')
    total_paid_holidays = fields.Float(compute='_calculate_holidays', string='Total Paid holidays')
    total_unpaid_holidays = fields.Float(compute='_calculate_holidays', string='Total Unpaid holidays')
    total_working_time = fields.Float(compute='_calc_working_time', string='Total Working Time')
    total_worked_days = fields.Float(compute='_calc_working_time', string='Total Worked Days')
    total_lag_hour = fields.Float(compute='_calc_working_time', string='Total Lag Hours')
    total_sick_leave = fields.Float(compute='_calculate_holidays', string='Total Sick Leave')
    total_out_working = fields.Float(compute='_calculate_out_working', string='Total Out Working')

    @api.onchange('balance_id')
    def onchange_balance_id(self):
        if self.balance_id:
            if not (self.date_from >= self.balance_id.balance_date_from and self.date_from <= self.balance_id.balance_date_to) or self.date_from > self.date_to:
                self.date_from = self.balance_id.balance_date_from

            if not (self.date_to >= self.balance_id.balance_date_from and self.date_to <= self.balance_id.balance_date_to) or self.date_from > self.date_to:
                self.date_to = self.balance_id.balance_date_to
                
    @api.multi
    def action_timesheet_confirm(self):
        # @Override: Цагийн хуудас үүсэхэд холбогдох ажилтныг message_subscribe хийдэг кодыг дарав.
        self.write({'state': 'confirm'})
        return True

