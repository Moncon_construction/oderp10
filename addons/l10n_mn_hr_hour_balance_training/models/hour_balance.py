# -*- coding: utf-8 -*-
from datetime import datetime
from dateutil import rrule
import logging

from odoo import api, fields, models
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT

_logger = logging.getLogger(__name__)


class HRTraining(models.Model):
    _inherit = "hr.training"

    is_outside_training = fields.Boolean(default=False, string='Is outside Training')
    
class HourBalance(models.Model):
    _inherit = "hour.balance"
    
    def get_linked_objects(self, object, employee, st_date, end_date):
        if object != 'hr.training.line':
            return super(HourBalance, self).get_linked_objects(object, employee, st_date, end_date)
        
        domain = [('employee_id', '=', employee.sudo().id), ('training_id.state', '=', 'implemented'), ('attendance', '=', True), ('training_id.is_outside_training', '=', True)]
        domain.extend(get_duplicated_day_domain('date_start', 'date_end', st_date, end_date, self.env.user))
        
        return self.env[object].sudo().search(domain)
   
    def get_linked_object_time_intervals(self, object, employee, st_date, end_date):
        if object != 'hr.training.line':
            return super(HourBalance, self).get_linked_object_time_intervals(object, employee, st_date, end_date)
        
        time_intervals = []
        linked_objects = self.get_linked_objects(object, employee, st_date, end_date)
        for obj in linked_objects:
            start_date = datetime.strptime(obj.date_start, DEFAULT_SERVER_DATETIME_FORMAT)
            end_date = datetime.strptime(obj.date_end, DEFAULT_SERVER_DATETIME_FORMAT)
            time_intervals.append((start_date, end_date))
        
        return time_intervals
    
    def get_active_hours(self, values):
        self.ensure_one()
        active_hours = super(HourBalance, self).get_active_hours(values)
        active_hours += values.get('total_training_hours', 0)
        return active_hours
    
    def get_balance_line_values(self, employee_id, contract_id, check_contract_date=False):
        # @Override: Сургалтын цаг тооцоолох
        self.ensure_one()
        values = super(HourBalance, self).get_balance_line_values(employee_id, contract_id, check_contract_date=check_contract_date)
        
        date_from, date_to = self.get_balance_dates(contract_id, check_contract_date=check_contract_date)
        if date_from and date_to:
            training_hours = self.get_total_hours_by_object('hr.training.line', employee_id, contract_id, date_from, date_to)
            values['total_training_hours'] = max(training_hours, 0)
            
        return values
    
    
class HourBalanceLine(models.Model):
    _inherit = "hour.balance.line"
    
    def set_values_from_line(self):
        super(HourBalanceLine, self).set_values_from_line()
        for obj in self:
            obj.total_training_hours = sum(obj.mapped('line_ids').mapped('total_training_hours'))
            
