# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian HR Innovation",
    'version': '1.0',
    'depends': ['l10n_mn_hr','l10n_mn_workflow_config'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
   Human Resource Innovation
    """,
    'data': [
        'security/security_view.xml',
        'views/hr_employee_innovation_view.xml',
        'security/ir.model.access.csv',
        'views/hr_employee_views.xml',
        'wizard/action_create_task_innovation.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}