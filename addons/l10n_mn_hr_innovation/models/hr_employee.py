# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class HrEmployee(models.Model):
    _inherit = "hr.employee"

    @api.multi
    def stat_button_innovation(self):
        #   ухаалаг даруул дээр дарахад дуудагдах функц
        res = []
        for line in self:
            res = self.env['hr.employee.innovation'].search([('employee_id', '=', line.id)])
            return {
                'type': 'ir.actions.act_window',
                'name': _('Employee Innovation'),
                'res_model': 'hr.employee.innovation',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'view_id': False,
                'domain': [('id', 'in', res.ids)],
            }