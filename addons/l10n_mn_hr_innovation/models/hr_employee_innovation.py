# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import time
import datetime

from odoo import api, exceptions, fields, models, _
from odoo.exceptions import UserError

STATE_SELECTION = [('draft', 'Draft'),
                   ('waiting_approve', 'Waiting Approve'),
                   ('approved', 'Approved'),
                   ('rejected', 'rejected'),
                   ('cancel', 'Cancel')]

class InnovationStage(models.Model):
    _name = 'innovation.stage'
    _description = "Innovation Stage"
    _order = 'sequence'

    name = fields.Char('Stage Name', translate=True, required=True)
    sequence = fields.Integer(help="Used to order the innovation stages", default=1)
    fold = fields.Boolean('Folded by Default')

class HrInnovationType(models.Model):
    _name = 'hr.innovation.type'
    _description = "Hr Innovation Type"

    name = fields.Char('Name', required=True)

class HrEmployeeInnovation(models.Model):
    _name = 'hr.employee.innovation'
    _inherit = ['mail.thread']

    def _get_default_stage_id(self):
        return self.env['innovation.stage'].search([('name','!=',False)],limit=1)

    @api.model
    def _read_group_stage_ids(self, stages, domain, order):
        search_domain = [('id', 'in', stages.ids)]
        stage_ids = stages._search(search_domain,order=order)
        return stages.browse(stage_ids)

    @api.multi
    def _task_count(self):
        for obj in self:
            obj.task_count = len(self.env['project.task'].sudo().search([('innovation_id','=',obj.id)])) or 0
        return True

    @api.multi
    @api.depends('liked_employee_ids')
    def _compute_is_liked(self):
        for inn in self:
            liked_employees = self.env['innovation.liked.employee'].search([('employee_id','=',self._uid),('innovation_id','=',inn.id)]).ids
            if liked_employees == []:
                inn.is_liked = False
            else:
                inn.is_liked = True
                
    @api.depends('check_sequence')
    def _compute_is_validator(self):
        for rec in self:
            history_obj = self.env['hr.employee.innovation.workflow.history']
            validators = history_obj.search([('innovation_id', '=', rec.id), ('line_sequence', '=', rec.check_sequence)], limit=1, order='sent_date DESC').user_ids
            if self.env.user in validators:
                rec.is_validator = True
            else:
                rec.is_validator = False

    @api.multi
    def _compute_is_creator(self):
        for rec in self:
            if rec.create_uid == self.env.user:
                rec.is_creator = True
            else:
                rec.is_creator = False
        
    name = fields.Char('name', required=True, track_visibility='onchange')
    employee_id = fields.Many2one('hr.employee', 'Employee', required=True, track_visibility='onchange')
    date = fields.Date('Date', track_visibility='onchange', required=True, default=fields.Date.context_today)
    color = fields.Integer(string='Color Index')
    open = fields.Boolean(string='Active', track_visibility='onchange', default=True)
    description = fields.Text('Description', track_visibility='onchange')
    result = fields.Text('Result', track_visibility='onchange')
    task_ids = fields.One2many('project.task', 'innovation_id', 'Tasks')
    task_count = fields.Integer(compute='_task_count' ,string = 'Task Count')
    liked_employee_ids = fields.One2many('innovation.liked.employee', 'innovation_id', 'liked Employees')
    is_liked = fields.Boolean('Is Liked', compute='_compute_is_liked')
    type_id = fields.Many2one('hr.innovation.type', 'Type', track_visibility='onchange')
    history_line_ids = fields.One2many('hr.employee.innovation.workflow.history', 'innovation_id', 'Workflow History', copy=False)    
    workflow_id = fields.Many2one('workflow.config', 'Workflow', copy=False)
    check_sequence = fields.Integer('Workflow Step', default=0, copy=False)
    is_validator = fields.Boolean(compute='_compute_is_validator')
    is_creator = fields.Boolean(compute='_compute_is_creator')
    state = fields.Selection(STATE_SELECTION, 'State', default = 'draft', track_visibility='onchange')

    @api.model
    def default_get(self, fields):
        result = super(HrEmployeeInnovation, self).default_get(fields)
        employee_obj = self.env['hr.employee']
        employee = employee_obj.search([('user_id','=',self._uid)])
        result.update({
                       'employee_id': employee.id
                       })
        return result
    
    @api.model
    def create(self, vals):
        creation = super(HrEmployeeInnovation, self).create(vals)
        employee = self.env['hr.employee'].search([('id', '=', vals['employee_id'])])
        workflow_id = self.env['workflow.config'].get_workflow('employee', 'hr.employee.innovation', employee.id, None)
        if not workflow_id:
            raise exceptions.Warning(_('There is no workflow defined!'))
        creation.workflow_id = workflow_id
        return creation

    @api.multi
    def create_task(self):
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'create.innovation.task',
            'context': self._context,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
        }
        
    @api.multi
    def action_like(self):
        liked_employee_obj = self.env['innovation.liked.employee']
        employee = self.env['hr.employee'].search([('user_id','=',self._uid)])
        liked_employee= liked_employee_obj.search([('employee_id','=',self._uid),('innovation_id','=',self.id)])
        if not liked_employee:
            vals = {
                    'innovation_id': self.id,
                    'name': employee.name_related + '.' + employee.last_name if employee.last_name else employee.name_related,
                    'employee_id': self._uid,
                    }
            liked_employee_obj.create(vals)
        return True
    
    @api.multi
    def action_not_liked(self):
        liked_employee_obj = self.env['innovation.liked.employee']
        liked_employee= liked_employee_obj.search([('employee_id','=',self._uid),('innovation_id','=',self.id)])
        if liked_employee:
            for lkemp in liked_employee:
                lkemp.unlink()
        return True
    
    @api.multi
    def draft(self):
        #Ажилтны санаачлагыг ноороглох
        self.ensure_one()
        self.check_sequence = 0
        self.state = 'draft'

    @api.multi
    def send(self):
        #Ажилтны санаачлагыг илгээх, Ажлын урсгалд тохируулсан ажилчдад илгээгдэнэ.
        if self.workflow_id:
            success, current_sequence = self.env['workflow.config'].send('hr.employee.innovation.workflow.history', 'innovation_id', self, self.create_uid.id)
            if success:
                self.check_sequence = current_sequence
                self.state = 'waiting_approve'

    @api.one
    def approve(self):
        #Ажилтны санаачлагыг батлах
        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve('hr.employee.innovation.workflow.history', 'innovation_id', self,
                                                                                         self.env.user.id)
            if success:
                if sub_success:
                    self.state = 'approved'
                else:
                    self.check_sequence = current_sequence

    @api.multi
    def refuse(self):
        #Ажилтны санаачлагыг татгалзах
        if self.workflow_id:
            success = self.env['workflow.config'].reject('hr.employee.innovation.workflow.history', 'innovation_id', self, self.env.user.id)
            if success:
                self.state = 'rejected'

    @api.multi
    def previous(self):
        #Ажилтны санаачлагыг буцаах
        if self.workflow_id:
            success, current_sequence = self.env['workflow.config'].action_return('hr.employee.innovation.workflow.history', 'innovation_id', self, self.env.user.id)
            if success:
                self.check_sequence = current_sequence
                
    @api.multi
    def cancel(self):
        #Ажилтны санаачлагыг цуцлах
        self.state = 'cancel'
        
    @api.multi
    def unlink(self):
        for inn in self:
            if inn.state != 'draft':
                raise UserError(_('Employee Innovation delete only draft state!'))
            super(HrEmployeeInnovation, inn).unlink()

class InnovationLikedEmployee(models.Model):
    _name ='innovation.liked.employee'

    innovation_id  = fields.Many2one('hr.employee.innovation', string = 'Innovation')
    name = fields.Char('Name')
    employee_id = fields.Many2one('res.users', 'User')

class CreateInnovationTask(models.TransientModel):
    _name ='create.innovation.task'

    innovation_id   = fields.Many2one('hr.employee.innovation', string = 'Innovation')
    name = fields.Char('Name', required=True)
    end_date = fields.Date('End Date', required=True)
    user_id = fields.Many2one('res.users', 'User' , required=True, default=lambda self: self.env.user)

    @api.model
    def default_get(self, fields):
        result = super(CreateInnovationTask, self).default_get(fields)
        if self._context.get('active_id'):
            innovation_obj = self.env['hr.employee.innovation']
            innovation = innovation_obj.browse(self._context['active_id'])
        result.update({
                       'innovation_id': innovation.id
                       })
        return result

    @api.multi
    def create_button(self):
        task_obj = self.env['project.task']
        vals = {
                'name': self.name,
                'user_id': self.user_id.id,
                'date_deadline': self.end_date,
                'innovation_id': self.innovation_id.id
                }
        task = task_obj.create(vals)

class InheritedProjectTask(models.Model):
    _inherit = 'project.task'

    innovation_id = fields.Many2one('hr.employee.innovation', 'Innovation')
    
class HrEmployeeInnovationWorkflowHistory(models.Model):
    _name = 'hr.employee.innovation.workflow.history'
    _order = 'innovation_id, sent_date'
    """Ажилтны санаачлагын ажлын урсгалын түүх"""
    
    innovation_id = fields.Many2one('hr.employee.innovation', 'Innovation', readonly=True, ondelete='cascade')
    line_sequence = fields.Integer('Workflow Step')
    name = fields.Char('Verification Step', readonly=True)
    user_ids = fields.Many2many('res.users', 'res_users_hr_innovation_workflow_history_ref', 'history_id', 'user_id', 'Validators')
    sent_date = fields.Datetime('Sent date', required=True, readonly=True)
    user_id = fields.Many2one('res.users', 'Validator', readonly=True)
    action_date = fields.Datetime('Action date', readonly=True)
    action = fields.Selection(STATE_SELECTION, 'Action', readonly=True)