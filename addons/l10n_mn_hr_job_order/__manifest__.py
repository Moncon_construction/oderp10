# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 77228080, 976 + 88005462, 976 + 94100149
#
##############################################################################
{
	'name': " Mongolian HR Job Order",
	'version': "1.0",
	'depends': ['l10n_mn_hr'],
	'author': "Asterisk Technologies LLC",
	'website': 'http://asterisk-tech.mn',
	'category': "Mongolian Modules",
	'description': """
				Ажлын байрны захиалга бүргэж, сонгон шалгаруулалтыг эхлүүлнэ
	""",
	'data': [
		'security/ir.model.access.csv',
		'views/hr_job_order_view.xml',
		'data/send_order_to_receiver.xml',
	],
	'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}