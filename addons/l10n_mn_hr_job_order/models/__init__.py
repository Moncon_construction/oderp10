# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 77228080, 976 + 88005462, 976 + 94100149
#
##############################################################################
import hr_job_order