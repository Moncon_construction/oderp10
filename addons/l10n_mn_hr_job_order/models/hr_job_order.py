# -*- coding: utf-8 -*-
# Part of OdERP. See LICENSE file for full copyright and licensing details.
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/,
# http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError


class HrJobOrder(models.Model):
    _name = "hr.job.order"
    _description = "Job Order"
    _inherit = ['mail.thread','ir.needaction_mixin']

    @api.multi
    def _get_order_receiver_domain(self):
        user_ids = self.get_hr_group_user_ids()
        if user_ids and len(user_ids) > 0:
            return [('id', 'in', user_ids)]
            
        return [('id', 'in', [])]
    
    @api.multi
    def _default_registered_employee_id(self):
        if self.env.user.employee_ids:
            return self.env.user.employee_ids[0]

    def _compute_hr_applicants(self):
        if self.line_ids:
            count = self.env['hr.applicant'].search([('hr_job_order_id', '=', self.line_ids.mapped('order_id').id)])
            self.hr_applicant_count = len(count)

    name = fields.Char('Name', required=True, states={'confirmed':[('readonly',True)]})
    order_date = fields.Date('Order date', required=True, states={'confirmed':[('readonly',True)]}, default=fields.Date.context_today)
    registered_employee_id = fields.Many2one('hr.employee', string='Registered Employee', required=True, states={'confirmed':[('readonly',True)]}, default=_default_registered_employee_id)
    department_id = fields.Many2one('hr.department', string='Department', required=True)
    description = fields.Text('Description', size=128, states={'confirmed':[('readonly',True)]})
    line_ids = fields.One2many('hr.job.order.line', 'order_id', string='Order Lines', states={'confirmed':[('readonly',True)]})
    state = fields.Selection([
                    ('draft', 'Draft'),
                    ('sent', 'Sent'),
                    ('received', 'Received'),
                    ('supplied', 'HR Supplied'),
                    ('done', 'Done'),
                    ], track_visibility='onchange',string='State', default = 'draft')
    company_id = fields.Many2one('res.company','Company', states={'confirmed':[('readonly',True)]}, default=lambda self: self.env['res.company']._company_default_get())
    order_receivers = fields.Many2many('res.users', 'job_order_to_user', 'job_id', 'user_id', string='Order Receiver', domain=_get_order_receiver_domain, track_visibility='on_change')
    hr_applicant_count = fields.Integer("Application count", compute='_compute_hr_applicants')
    job_responsibilities = fields.Text('Job Responsibilities', size=128, states={'confirmed':[('readonly',True)]})
    other_required_information = fields.Text('Other Required Information', size=128, states={'confirmed':[('readonly',True)]})

    def action_hr_applicant(self):
        for obj in self:
            return {
                'type': 'ir.actions.act_window',
                'name': _('Applications for Job order'),
                'res_model': 'hr.applicant',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'view_id': False,
                'domain': [('hr_job_order_id', '=', self.line_ids.mapped('order_id').id)],
                'context': self._context
            }

    @api.onchange('registered_employee_id')
    def onchange_registered_employee_id(self):
        if self.registered_employee_id and self.registered_employee_id.department_id:
            self.department_id = self.registered_employee_id.department_id.id
    
    def get_hr_group_user_ids(self):
        user_ids = []
        self._cr.execute("""
                        SELECT uid FROM res_groups_users_rel WHERE gid IN
                                (SELECT res_id FROM ir_model_data WHERE module='hr_recruitment' AND name='group_hr_recruitment_manager')
                        """)
        results = self._cr.dictfetchall()
        for result in results:
            user_ids.append(result['uid'])
        
        return user_ids
    
    @api.multi
    def action_to_receive(self):
        return self.write({'state': 'received'})

    @api.multi
    def action_to_send(self):
        """ Open a window to compose an email, with the email_template_to_receiver template
            message loaded by default
        """
        self.ensure_one()
        
        if not self.order_receivers:
            raise UserError(_("Please define order receiver on this order..."))
        
        template = self.env.ref('l10n_mn_hr_job_order.email_template_to_receiver', False)
        compose_form = self.env.ref('mail.email_compose_message_wizard_form', False)
        
        creator = ''
        if self.registered_employee_id.last_name:
            creator += self.registered_employee_id.last_name + ' ' 
        creator += self.registered_employee_id.name if self.registered_employee_id.name else ''
        creator += ' [' + self.registered_employee_id.job_id.name + ']' if self.registered_employee_id.job_id else ''
        
        partners = [receiver.partner_id.id for receiver in self.order_receivers]
        
        ctx = dict(
            default_model='hr.job.order',
            default_res_id=self.id,
            default_use_template=bool(template),
            default_template_id=template and template.id or False,
            default_composition_mode='comment',
            mark_invoice_as_sent=True,
            department_name=self.department_id.name or '' if self.department_id else '',
            creator=creator,
            base_url=self.env['ir.config_parameter'].get_param( 'web.base.url'),
            action_id=self.env['ir.model.data'].get_object_reference('l10n_mn_hr_job_order', 'action_hr_job_order')[1],
            id=self.id,
            db_name=self.env.cr.dbname,
            partners=str(partners).replace('[','').replace(']','') if len(partners) > 0 else '',
        ) 
        
        mail_invite = self.env['mail.wizard.invite'].with_context({
            'default_res_model': 'hr.job.order',
            'default_res_id': self.id
        }).sudo(self.env.uid).create({
            'partner_ids': [(4, receiver.partner_id.id) for receiver in self.order_receivers],
            'send_mail': True})
        mail_invite.add_followers()
            
        self.write({'state':'sent'})
        
        return {
            'name': _('Compose Email to Order Receiver'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': ctx,
        }

    @api.multi
    def action_to_draft(self):
        return self.write({'state':'draft'})

    def action_to_supplied(self):
        return self.write({'state': 'supplied'})

    def action_to_done(self):
        if self.create_uid == self.env.user:
            return self.write({'state': 'done'})
        else:
            raise UserError(_('Only created user can do this action.\n Created user: %s') % self.create_uid.name)

    @api.multi
    def unlink(self):
        for document in self:
            if document.state not in ('draft'):
                raise UserError(
                    _('You cannot delete an job order which is not draft.'))
        return super(HrJobOrder, self).unlink()

class HrJobOrderLine(models.Model):
    _name = "hr.job.order.line"

    job_id = fields.Many2one('hr.job', 'Job position', required=True)
    number = fields.Integer('Number')
    date_of_completion = fields.Date('Date of completion')
    requirement =  fields.Binary('Job requirement')
    order_id = fields.Many2one('hr.job.order', 'Job Order', required=True)
    choice = fields.Selection([('inside', 'Inside'),
                               ('outside', 'Outside')], string="Inside / Outside", default='inside')
    age = fields.Char(string='Age')
    gender = fields.Selection([('male', 'Male'),
                               ('female', 'Female')], string='Gender', default='male')
    is_rep_person = fields.Boolean(string="Is rep person")


class HrApplicant(models.Model):
    _inherit = 'hr.applicant'


    hr_job_order_id = fields.Many2one('hr.job.order', 'Job Order')

    @api.model
    def default_get(self, fields):
        result = super(HrApplicant, self).default_get(fields)
        if self._context.get('active_id') and self._context.get('active_model') == 'hr.job.order':
            hr_job_order = self.env['hr.job.order'].browse(self._context['active_id'])
            result.update({
                'hr_job_order_id': hr_job_order.id if hr_job_order else False
            })
        return result if result else False
