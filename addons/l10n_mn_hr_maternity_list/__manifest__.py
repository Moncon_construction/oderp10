# -*- coding: utf-8 -*-
{
	"name" : "Mongolian HR Maternity List",
	"version" : "1.0",
	"author" : "Asterisk Technologies LLC",
	"category" : "Human Resource",
	"description" : """
Жирэмсний болон амаржсаны эмнэлгийн хуудас
""",
	'depends': ['l10n_mn_hr', 'hr_holidays'],
	"data" : [
		'security/ir.model.access.csv',
		'views/maternity_list_view.xml',
	],
	'application': True,
	"installable": True,
}
