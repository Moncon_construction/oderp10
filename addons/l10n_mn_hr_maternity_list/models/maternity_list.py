# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
import time

class HrHolidays(models.Model):
    _inherit = "hr.holidays"
    
    maternity_list_id = fields.Many2one('hr.maternity.list', 'Maternity List', states={'confirmed':[('readonly',True)]})

class HrMaternityList(models.Model):
    _name = "hr.maternity.list"
    _description = "Employee Maternity List"
    _inherit = ['mail.thread','ir.needaction_mixin']
    
    name = fields.Char('Name', required=True, states={'confirmed':[('readonly',True)]})
    employee_id = fields.Many2one('hr.employee', string='Employee', required=True, states={'confirmed':[('readonly',True)]})
    job_id = fields.Many2one('hr.job', string='Job', related='employee_id.job_id', store=True, readonly=True, related_sudo=False)
    department_id = fields.Many2one('hr.department', string='Department', related='employee_id.department_id', store=True, readonly=True, related_sudo=False)
    list_date = fields.Date('List date', required=True, states={'confirmed':[('readonly',True)]}, default=lambda *a:time.strftime('%Y-%m-%d'))
    type =  fields.Selection([('pregnancy','Pregnancy'),('postpartum','Postpartum')],'Type', required=True, states={'confirmed':[('readonly',True)]})
    hospital = fields.Char('Hospital', size=128, required=True, states={'confirmed':[('readonly',True)]})
    leave_ids = fields.One2many('hr.holidays', 'maternity_list_id', string='Maternity Leave', states={'confirmed':[('readonly',True)]})
    description = fields.Text('Description', size=128, states={'confirmed':[('readonly',True)]})
    state = fields.Selection([
                    ('draft','Draft'),
                    ('confirmed','Confirmed'),
                    ], track_visibility='onchange', string='State', default='draft')
    company_id = fields.Many2one('res.company', 'Company', states={'confirmed':[('readonly',True)]}, default=lambda self: self.env.user.company_id)

    @api.multi
    def action_to_confirm(self):
        self.ensure_one()
        self.state = 'confirmed'
    
    @api.multi
    def action_to_draft(self):
        self.ensure_one()
        self.state = 'draft'
    