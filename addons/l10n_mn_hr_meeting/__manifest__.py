# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian HR Meetings",
    'version': '10.0.1.0',
    'depends': ['l10n_mn_hr', 'project', 'calendar', 'mail'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
         Plan to meetings
    """,
    'data': [
        'views/meeting_view.xml',
        'data/meeting_data.xml',
        'security/ir.model.access.csv',
        'security/meeting_security.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
