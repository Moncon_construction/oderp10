# -*- coding: utf-8 -*-
import babel.dates
from datetime import datetime, timedelta
import logging
import pytz
import time
import uuid
from odoo import api, fields, models, tools, _
from odoo.tools.translate import _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError
from lxml import etree

_logger = logging.getLogger(__name__)


class MailComposer(models.TransientModel):
    _inherit = 'mail.compose.message'

    @api.multi
    def send_mail_action(self):
        if self._context.get('active_model') == 'hr.meeting':
            conference = self.env['hr.meeting'].search([('id', '=', self._context.get('default_res_id'))])
            status = self._context.get('status')
            if status == 'to_announce':
                conference.action_create()
            elif status == 'draft':
                conference.cancel_conference()
            else:
                conference.create_done(conference)
        return super(MailComposer, self).send_mail_action()


class Task(models.Model):
    _inherit = "project.task"

    meeting_id = fields.Many2one('hr.meeting', 'Conference')

    @api.model
    def fields_view_get(self, view_id=None, view_type='tree', toolbar=False, submenu=False):
        res = super(Task, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar,
                                                         submenu=submenu)

        root = etree.fromstring(res['arch'])

        if self._context.get('task_create'):
            root.set('create', 'false',)
            root.set('edit', 'false', )
        else:
            root.set('create', 'true')
            root.set('edit', 'true', )
        res['arch'] = etree.tostring(root)
        return res


class HrMeetingAttendee(models.Model):
    _name = 'hr.meeting.attendee'
    _description = 'Attendee information'

    @staticmethod
    def _default_access_token():
        return uuid.uuid4().hex

    STATE_SELECTION = [
        ('notApproved', 'Not approved'),
        ('needsAction', 'Needs Action'),
        ('tentative', 'Uncertain'),
        ('declined', 'Declined'),
        ('accepted', 'Accepted'),
    ]

    state = fields.Selection(STATE_SELECTION, string='Status', readonly=True, default='notApproved',
                             help="Status of the attendee's participation")
    common_name = fields.Char('Common name', compute='_compute_common_name', store=True)
    partner_id = fields.Many2one('res.partner', 'Contact', readonly="True")
    email = fields.Char('Email', help="Email of Invited Person")
    availability = fields.Selection([('free', 'Free'),
                                     ('busy', 'Busy')],
                                    'Free/Busy', readonly="True")
    access_token = fields.Char('Invitation Token', default=_default_access_token)
    event_id = fields.Many2one('hr.meeting', 'Meeting linked', ondelete='cascade')
    meeting_state = fields.Selection(related='event_id.state', string='Meeting status', store=True, readonly=True)

    @api.depends('partner_id', 'partner_id.name', 'email')
    def _compute_common_name(self):
        # Дэлгэцний нэрийг зохиомжлох
        for attendee in self:
            attendee.common_name = attendee.partner_id.name or attendee.email

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        # Харилцагч солигдох үед мэйлийг нь default-р оноох
        self.email = self.partner_id.email

    @api.multi
    def _send_mail_to_attendees(self, template_xmlid, force_send=False):
        # Оролцогчидод урилга илгээх
        res = False
        calendar_view = self.env.ref('l10n_mn_hr_meeting.view_conference_event_calendar')
        invitation_template = self.env.ref(template_xmlid)
        # get ics file for all meetings
        ics_files = self.mapped('event_id').get_ics_file()
        # prepare rendering context for mail template
        colors = {
            'needsAction': 'grey',
            'accepted': 'green',
            'tentative': '#FFFF00',
            'declined': 'red'
        }
        rendering_context = dict(self._context)
        rendering_context.update({
            'color': colors,
            'action_id': self.env['ir.actions.act_window'].search([('view_id', '=', calendar_view.id)], limit=1).id,
            'dbname': self._cr.dbname,
            'base_url': self.env['ir.config_parameter'].get_param('web.base.url', default='http://localhost:8079')
        })
        invitation_template = invitation_template.with_context(rendering_context)
        # Хавсралттай нь хамт мэйл илгээх
        mails_to_send = self.env['mail.mail']
        for attendee in self:
            if attendee.email or attendee.partner_id.email:
                ics_file = ics_files.get(attendee.event_id.id)
                mail_id = invitation_template.send_mail(attendee.id)
                vals = {}
                if ics_file:
                    vals['attachment_ids'] = [(0, 0, {'name': 'invitation.ics',
                                                      'datas_fname': 'invitation.ics',
                                                      'datas': str(ics_file).encode('base64')})]
                vals['model'] = None  # We don't want to have the mail in the tchatter while in queue!
                vals['res_id'] = False
                current_mail = self.env['mail.mail'].browse(mail_id)
                current_mail.mail_message_id.write(vals)
                mails_to_send |= current_mail

        if force_send and mails_to_send:
            res = mails_to_send.send()
        return res

    @api.model
    def create(self, values):
        # Мэйл болон нэр оноогдоогүй байвал оноож оролцогчидыг үүсгэх
        if not values.get("email") and values.get("common_name"):
            common_nameval = values.get("common_name").split(':')
            email = filter(lambda x: x.__contains__('@'), common_nameval)  # TODO JEM : should be refactored
            values['email'] = email and email[0] or ''
            values['common_name'] = values.get("common_name")
        return super(HrMeetingAttendee, self).create(values)

    @api.multi
    def check_permission(self, ):
        # Тухайн оролцогч л өөрийн ирцийг бөглөх эрхийг шалгах
        user = self.env['res.users'].search([('id', '=', self.env.uid)])
        for attendee in self:
            if attendee.partner_id.id == user.partner_id.id or attendee.email == user.email:
                return True
            else:
                raise UserError(
                    _('Permission denied! Only %s partner do it.') % attendee.partner_id.name or attendee.email)
        return False

    @api.multi
    def do_tentative(self):
        # Оролцогч нь тодорхойгүй төлөвтэй бол дарагдахад
        if self.check_permission():
            return self.write({'state': 'tentative'})

    @api.multi
    def do_accept(self):
        # Оролцогч нь зөвшөөрсөн үед дарагдах
        if self.check_permission():
            result = self.write({'state': 'accepted'})
            for attendee in self:
                attendee.event_id.message_post(body=_("%s has accepted invitation") % attendee.common_name,
                                               subtype="l10n_mn_hr_meeting.subtype_invitation")
            return result

    @api.multi
    def do_decline(self):
        # Оролцогч нь цуцлах үед дарагдахад
        if self.check_permission():
            res = self.write({'state': 'declined'})
            for attendee in self:
                attendee.event_id.message_post(body=_("%s has declined invitation") % attendee.common_name,
                                               subtype="l10n_mn_hr_meeting.subtype_invitation")
            return res


class HrMeetingTask(models.Model):
    _name = 'hr.meeting.task'
    _description = 'Meeting Task'

    name = fields.Char(string='Task Title')
    task_id = fields.Many2one('project.task', string='Task')
    project_id = fields.Many2one('project.project', string='Project')
    user_id = fields.Many2one('res.users', string='Assigned to')
    check_user_id = fields.Many2one('res.users', string='Check user')
    end_date = fields.Date(string='Ending Date')
    meeting_id = fields.Many2one('hr.meeting', string='Meeting')

    @api.onchange('user_id')
    def onchange_user(self):
        # Хянагчид default-р гүйцэтгэгч хүнийг оноох
        if self.user_id:
            self.check_user_id = self.user_id


class HrMeeting(models.Model):
    _name = 'hr.meeting'
    _description = 'Conference'
    _inherit = "mail.thread"

    @api.model
    def _default_partners(self):
        # Холбоотой харилцагчид default-р нэвтэрсэн хэрэглэгчийн харилцагчийг оноох
        partners = self.env.user.partner_id
        active_id = self._context.get('active_id')
        if self._context.get('active_model') == 'res.partner' and active_id:
            if active_id not in partners.ids:
                partners |= self.env['res.partner'].browse(active_id)
        return partners
   
    @api.multi
    def _find_my_attendee(self):
        # Хурлын тухай мэдээлэл илгээгдсэн хэрэглэгчийн холбогдсон эхний хэрэглэгчийг харуулна
        self.ensure_one()
        for attendee in self.attendee_ids:
            if self.env.user.partner_id == attendee.partner_id:
                return attendee
        return False

    @api.model
    def _get_date_formats(self):
        # Одоогийн огноо болон цагийн форматыг context-н хэлийн хамт (format_date, format_time)-д буцаах
        lang = self._context.get("lang")
        lang_params = {}
        if lang:
            record_lang = self.env['res.lang'].search([("code", "=", lang)], limit=1)
            lang_params = {
                'date_format': record_lang.date_format,
                'time_format': record_lang.time_format
            }
        # formats will be used for str{f,p}time() which do not support unicode in Python 2, coerce to str
        format_date = lang_params.get("date_format", '%B-%d-%Y').encode('utf-8')
        format_time = lang_params.get("time_format", '%I-%M %p').encode('utf-8')
        return format_date, format_time

    def _get_duration(self, start, stop):
        # Өгөгдсөн 2огнооны хоорондох үргэлжлэх хугацааг олох
        if start and stop:
            diff = fields.Datetime.from_string(stop) - fields.Datetime.from_string(start)
            if diff:
                duration = float(diff.days) * 24 + (float(diff.seconds) / 3600)
                return round(duration, 2)
            return 0.0

    current_user = fields.Many2one('res.users', 'Current user', default=lambda self: self.env.user)
    name = fields.Char(string='Conference name', required=True)
    user_id = fields.Many2one('res.users', 'Owner', states={'done': [('readonly', True)]},
                              default=lambda self: self.env.user)
    start = fields.Datetime(string='Start', required=True,
                            help="Start date of an event, without time for full days events")
    stop = fields.Datetime(string='Stop', required=True,
                           help="Stop date of an event, without time for full days events")
    location = fields.Char('Location', help="Location of Event")
    duration = fields.Float('Duration')
    state = fields.Selection([('draft', 'Draft'),
                              ('to_announce', 'Announced'),
                              ('in_progress', 'To Do'),
                              ('done', 'Done')],
                             string='Status', default='draft', required=True)
    prot_note = fields.Text('Protocol Note')
    prot_dec = fields.Text('Protocol Decision')
    res_user_id = fields.Many2one('res.users', 'Respondent', required=True, states={'done': [('readonly', True)]})

    allday = fields.Boolean(string='All Day')
    description = fields.Text(string='Description')

    start_date = fields.Date('Start Date', compute='_compute_dates', inverse='_inverse_dates', store=True,
                             track_visibility='onchange')
    start_datetime = fields.Datetime('Start DateTime', compute='_compute_dates', inverse='_inverse_dates', store=True,
                                     track_visibility='onchange')
    stop_date = fields.Date('End Date', compute='_compute_dates', inverse='_inverse_dates', store=True,
                            track_visibility='onchange')
    stop_datetime = fields.Datetime('End Datetime', compute='_compute_dates', inverse='_inverse_dates', store=True,
                                    track_visibility='onchange')

    is_attendee = fields.Boolean('Attendee', compute='_compute_attendee')
    attendee_status = fields.Selection(HrMeetingAttendee.STATE_SELECTION, string='Attendee Status',
                                       compute='_compute_attendee')
    #     display_time = fields.Char('Event Time', compute='_compute_display_time')
    #     display_start = fields.Char('Date', compute='_compute_display_start', store=True)
    color_partner_id = fields.Integer("Color index of creator", compute='_compute_color_partner', store=False)
    active = fields.Boolean('Active', default=True,
                            help="If the active field is set to false, it will allow you to hide the event alarm information without removing it.")
    task_ids = fields.One2many('hr.meeting.task', 'meeting_id', string='Tasks Of Meeting', ondelete='cascade')
    partner_ids = fields.Many2many('res.partner', 'hr_meeting_res_partner_rel', 'meeting_id', 'partner_id',
                                   string='Attendees', default=_default_partners)
    attendee_ids = fields.One2many('hr.meeting.attendee', 'event_id', 'Participant', ondelete='cascade')
    calendar_id = fields.Many2one('calendar.event', 'Event')

    @api.onchange('prot_note','prot_dec','task_ids')
    def _onchange_prot_note_dec_task_ids(self):
        if self.env.user != self.user_id and self.env.user != self.res_user_id:
            raise UserError(_('Permission denied! Only %s and %s users write protocol!') % (
                self.user_id.name, self.res_user_id.name))

    @api.multi
    def _compute_attendee(self):
        for meeting in self:
            attendee = meeting._find_my_attendee()
            meeting.is_attendee = bool(attendee)
            meeting.attendee_status = attendee.state if attendee else 'needsAction'

    @api.multi
    @api.depends('allday', 'start', 'stop')
    def _compute_dates(self):
        """ Adapt the value of start_date(time)/stop_date(time) according to start/stop fields and allday. Also, compute
            the duration for not allday meeting ; otherwise the duration is set to zero, since the meeting last all the day.
        """
        for meeting in self:
            if meeting.allday:
                meeting.start_date = meeting.start
                meeting.start_datetime = False
                meeting.stop_date = meeting.stop
                meeting.stop_datetime = False
                meeting.duration = 0.0
            else:
                meeting.start_date = False
                meeting.start_datetime = meeting.start
                meeting.stop_date = False
                meeting.stop_datetime = meeting.stop
                meeting.duration = self._get_duration(meeting.start, meeting.stop)

    @api.multi
    def get_display_time_tz(self, tz=False):
        """ get the display_time of the meeting, forcing the timezone. This method is called from email template, to not use sudo(). """
        self.ensure_one()
        if self.calendar_id:
            return self.calendar_id.get_display_time_tz(tz)

    @api.multi
    def _inverse_dates(self):
        for meeting in self:
            if meeting.allday:
                tz = pytz.timezone(self.env.user.tz) if self.env.user.tz else pytz.utc
                enddate = fields.Datetime.from_string(meeting.stop_date)
                enddate = tz.localize(enddate)
                enddate = enddate.replace(hour=18)
                enddate = enddate.astimezone(pytz.utc)
                meeting.stop = fields.Datetime.to_string(enddate)
                startdate = fields.Datetime.from_string(meeting.start_date)
                startdate = tz.localize(startdate)  # Add "+hh:mm" timezone
                startdate = startdate.replace(hour=8)  # Set 8 AM in localtime
                startdate = startdate.astimezone(pytz.utc)  # Convert to UTC
                meeting.start = fields.Datetime.to_string(startdate)
            else:
                meeting.start = meeting.start_datetime
                meeting.stop = meeting.stop_datetime

    @api.multi
    def _compute_color_partner(self):
        for meeting in self:
            meeting.color_partner_id = meeting.user_id.partner_id.id

    @api.constrains('start_datetime', 'stop_datetime', 'start_date', 'stop_date')
    def _check_closing_date(self):
        for meeting in self:
            if meeting.start_datetime and meeting.stop_datetime and meeting.stop_datetime < meeting.start_datetime:
                raise ValidationError(_('Ending datetime cannot be set before starting datetime.'))
            if meeting.start_date and meeting.stop_date and meeting.stop_date < meeting.start_date:
                raise ValidationError(_('Ending date cannot be set before starting date.'))

    @api.onchange('start_datetime', 'duration')
    def _onchange_duration(self):
        if self.start_datetime:
            start = fields.Datetime.from_string(self.start_datetime)
            self.start = self.start_datetime
            self.stop = fields.Datetime.to_string(start + timedelta(hours=self.duration))

    @api.multi
    def get_interval(self, interval, tz=None):
        # Template дэх формат болон зарим огноог тогтоож unicode форматаар буцаах
        self.ensure_one()
        date = fields.Datetime.from_string(self.start)
        result = unicode
        if tz:
            timezone = pytz.timezone(tz or 'UTC')
            date = date.replace(tzinfo=pytz.timezone('UTC')).astimezone(timezone)

        if interval == 'day':
            # Day number (1-31)
            result = unicode(date.day)

        elif interval == 'month':
            # Localized month name and year
            result = babel.dates.format_date(date=date, format='MMMM y', locale=self._context.get('lang') or 'en_US')

        elif interval == 'dayname':
            # Localized day name
            result = babel.dates.format_date(date=date, format='EEEE', locale=self._context.get('lang') or 'en_US')

        elif interval == 'time':
            # Localized time
            dummy, format_time = self._get_date_formats()
            result = tools.ustr(date.strftime(format_time + " %Z"))
        return result

    @api.multi
    def action_to_do(self):
        # Хийгдэж байгаа товчийг дарахад дуудагдах функц ба эзэмшигч болон хариуцагч 2 л эрхтэй
        if self.env.uid == self.res_user_id.id or self.env.uid == self.user_id.id:
            self.write({'state': 'in_progress'})
        else:
            raise UserError(
                _('Permission denied! Only %s and %s users can do it.') % (self.res_user_id.name, self.user_id.name))

    @api.one
    def create_done(self, conference):
        # Хурлыг хийгдсэн тухай оролцогчидод мэйлээр мэдэгдэхийг зөвшөөрөх үед дуудагдах ба хурлын төлвийг өөрчилж, үүссэх даалгавруудыг project.task-д бүртгэнэ
        conference.write({'state': 'done'})
        if conference.task_ids:
            task_line = self.env['project.task']
            for task in conference.task_ids:
                task_id = task_line.create({
                    'name': task.name,
                    'project_id': task.project_id.id if task.project_id else False,
                    'user_id': task.user_id.id,
                    'date_deadline': task.end_date,
                    'description': self.prot_dec,
                    'date_start': self.start_datetime,
                    'meeting_id': self.id})
                task.write({'task_id': task_id.id})

    @api.multi
    def action_done(self):
        # Хийгдсэн товчийн дээр дарахад энэ тухай оролцогчидод мэдэгдэл мэйл илгээх цонх гаргах, зөвхөн эзэмшигч болон хариуцагч эрхтэй
        if self.env.uid == self.res_user_id.id or self.env.uid == self.user_id.id:
            self.ensure_one()
            ir_model_data = self.env['ir.model.data']
            try:
                template_id = \
                    ir_model_data.get_object_reference('l10n_mn_hr_meeting',
                                                       'email_template_sent_to_done_from_meeting')[1]

            except ValueError:
                template_id = False
            ctx = dict()
            ctx.update({
                'default_model': 'hr.meeting',
                'default_res_id': self.ids[0],
                'default_use_template': bool(template_id),
                'default_template_id': template_id,
                'default_composition_mode': 'comment',
                'attendees': str(self.partner_ids.ids).replace('[', '').replace(']', ''),
            })
            return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form,tree',
                'res_model': 'mail.compose.message',
                'target': 'new',
                'context': ctx,
            }
        else:
            raise UserError(
                _('Permission denied! Only %s and %s can done it.') % (self.res_user_id.name, self.user_id.name))

    @api.multi
    def cancel_conference(self):
        #         Хурлыг цуцлах тухай оролцогч нарт мэдэгдэхийг баталсан үед дуудагдах функц, оролцогчидийн болон хурлын төлвийг өөрчилж, уулзалтанд бүртгэгдсэн хурлыг устгана.
        if self.calendar_id:
            self.calendar_id.unlink()
        for attendee_id in self.attendee_ids:
            attendee = self.env['hr.meeting.attendee'].search([('id', '=', attendee_id.id)])
            values = {'state': 'notApproved', }
            attendee.update(values)
        self.write({'state': 'draft'})

    @api.multi
    def action_cancel(self):
        # Хурлыг цуцлах товч дараглахад энэ тухай оролцогчидод мэдэгдэх мэйл илгээх цонх гарч ирэх зөюхөн эзэмшигч болон хариуцагч цуцлах эрхтэй
        if self.env.uid == self.res_user_id.id or self.env.uid == self.user_id.id:
            self.ensure_one()
            ir_model_data = self.env['ir.model.data']
            try:
                template_id = ir_model_data.get_object_reference('l10n_mn_hr_meeting',
                                                                 'email_template_sent_to_canceled_from_meeting')[1]

            except ValueError:
                template_id = False
            ctx = dict()
            ctx.update({
                'default_model': 'hr.meeting',
                'default_res_id': self.ids[0],
                'default_use_template': bool(template_id),
                'default_template_id': template_id,
                'default_composition_mode': 'comment',
                'attendees': str(self.partner_ids.ids).replace('[', '').replace(']', ''),
                'user': self.env['res.users'].search([('id', '=', self.env.uid)]).name,
                'status': 'draft',
            })
            return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form,tree',
                'res_model': 'mail.compose.message',
                'target': 'new',
                'context': ctx,
            }
        else:
            raise UserError(_('Permission denied! Only %s and %s users can cancel it.') % (
                self.res_user_id.name, self.user_id.name))

    @api.multi
    def action_create(self):
        # Илгээх товч дарвал төлвийг өөрчилж уулзалтуудад бүртгэнэ
        current_user = self.env.user
        for attendee_id in self.attendee_ids:
            values = {'state': 'needsAction', }
            # current user don't have to accept his own meeting
            if attendee_id.partner_id == self.env.user.partner_id:
                values = {'state': 'accepted', }
            attendee_id.update(values)
        if self.attendee_ids:
            to_notify = self.attendee_ids.filtered(lambda a: a.email != current_user.email)
            to_notify._send_mail_to_attendees('l10n_mn_hr_meeting.conference_template_meeting_invitation')
        calendar_id = self.env['calendar.event'].create({
            'name': self.name,
            'start': self.start,
            'stop': self.stop,
            'allday': self.allday,
            'active': self.active,
            'location': self.location,
            'description': self.description,
            'partner_ids': [(6, 0, self.partner_ids.ids)],
        })
        self.write({'state': 'to_announce',
                    'calendar_id': calendar_id.id})

    @api.multi
    def action_announce(self):
        # Ноорог төлөвтэй хурлыг зарлах товчин дээр дарж энэ тухай оролцогчидод зарлах мэйл илгээх цүнх гарч ирэх зөвхөн эзэмшигч болон хариуцагч зарлах эрхтэй
        if self.env.uid == self.res_user_id.id or self.env.uid == self.user_id.id:
            self.ensure_one()
            ir_model_data = self.env['ir.model.data']
            try:
                template_id = ir_model_data.get_object_reference('l10n_mn_hr_meeting',
                                                                 'email_template_sent_to_partners_from_meeting')[1]
            except ValueError:
                template_id = False
            ctx = dict()
            ctx.update({
                'default_model': 'hr.meeting',
                'default_res_id': self.ids[0],
                'default_use_template': bool(template_id),
                'default_template_id': template_id,
                'default_composition_mode': 'comment',
                'partners': str(self.partner_ids.ids).replace('[', '').replace(']', ''),
                'status': 'to_announce'
            })
            return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form,tree',
                'res_model': 'mail.compose.message',
                'target': 'new',
                'context': ctx,
            }
        else:
            raise UserError(_('Permission denied! Only %s and %s users can cancel it.') % (
                self.res_user_id.name, self.user_id.name))

    @api.multi
    def get_ics_file(self):
        #         Оролцогчидод мэйл илгээх үед холбоотой файлыг ачааллах
        result = {}

        def ics_datetime(idate, allday=False):
            if idate:
                if allday:
                    return fields.Date.from_string(idate)
                else:
                    return fields.Datetime.from_string(idate).replace(tzinfo=pytz.timezone('UTC'))
            return False

        try:
            # FIXME: why isn't this in CalDAV?
            import vobject
        except ImportError:
            _logger.warning(
                "The `vobject` Python module is not installed, so iCal file generation is unavailable. Use 'pip install vobject' to install it")
            return result

        for meeting in self:
            cal = vobject.iCalendar()
            event = cal.add('vevent')

            if not meeting.start or not meeting.stop:
                raise UserError(_("First you have to specify the date of the invitation."))
            event.add('created').value = ics_datetime(time.strftime(DEFAULT_SERVER_DATETIME_FORMAT))
            event.add('dtstart').value = ics_datetime(meeting.start, meeting.allday)
            event.add('dtend').value = ics_datetime(meeting.stop, meeting.allday)
            event.add('summary').value = meeting.name
            if meeting.description:
                event.add('description').value = meeting.description
            if meeting.location:
                event.add('location').value = meeting.location
            # if meeting.rrule:
            #   event.add('rrule').value = meeting.rrule

            for attendee in meeting.attendee_ids:
                attendee_add = event.add('attendee')
                attendee_add.value = 'MAILTO:' + (attendee.email or '')
            result[meeting.id] = cal.serialize()
        return result

    @api.multi
    def create_attendees(self):
        # Оролцогчидийн хүснэгтийг үүсгэх, сонгосон харилцагч нараас
        current_user = self.env.user
        result = {}
        for meeting in self:
            already_meeting_partners = meeting.attendee_ids.mapped('partner_id')
            meeting_attendees = self.env['hr.meeting.attendee']
            meeting_partners = self.env['res.partner']
            for partner in meeting.partner_ids.filtered(lambda prtnr: prtnr not in already_meeting_partners):
                values = {
                    'partner_id': partner.id,
                    'email': partner.email,
                    'event_id': meeting.id,
                }
                attendee = self.env['hr.meeting.attendee'].create(values)
                meeting_attendees |= attendee
                meeting_partners |= partner
            if meeting_partners:
                meeting.message_subscribe(partner_ids=meeting_partners.ids)
            # We remove old attendees who are not in partner_ids now.
            all_partners = meeting.partner_ids
            all_partner_attendees = meeting.attendee_ids.mapped('partner_id')
            old_attendees = meeting.attendee_ids
            partners_to_remove = all_partner_attendees + meeting_partners - all_partners

            attendees_to_remove = self.env["hr.meeting.attendee"]
            if partners_to_remove:
                attendees_to_remove = self.env["hr.meeting.attendee"].search(
                    [('partner_id', 'in', partners_to_remove.ids), ('event_id', '=', meeting.id)])
                attendees_to_remove.unlink()

            result[meeting.id] = {
                'new_attendees': meeting_attendees,
                'old_attendees': old_attendees,
                'removed_attendees': attendees_to_remove,
                'removed_partners': partners_to_remove
            }
        return result

    @api.multi
    def write(self, values):
        # compute duration, only if start and stop are modified
        if 'duration' not in values and 'start' in values and 'stop' in values:
            values['duration'] = self._get_duration(values['start'], values['stop'])
        for meeting in self:
            attendee_to_email = None
            super(HrMeeting, meeting).write(values)
            attendees_create = False
            if values.get('partner_ids', False):
                attendees_create = meeting.with_context(
                    dont_notify=True).create_attendees()  # to prevent multiple notify_next_alarm
            if (values.get('start_date') or values.get('start_datetime')) and values.get('active', True):
                for current_meeting in meeting:
                    if attendees_create:
                        attendees_create = attendees_create[current_meeting.id]
                        attendee_to_email = attendees_create['old_attendees'] - attendees_create['removed_attendees']
                    else:
                        attendee_to_email = current_meeting.attendee_ids
                    #                     Хурлын огноо өөрчлагдсөн бол мэйлээр мэдэгдэх
            if attendee_to_email:
                attendee_to_email._send_mail_to_attendees('l10n_mn_hr_meeting.conference_template_meeting_changedate')
        return True

    @api.model
    def create(self, values):
        if not values.get('user_id', False):  # Else bug with quick_create when we are filter on an other user
            values['user_id'] = self.env.user.id
        #         Үргэлжлэх хугацааг олох
        if not values.get('duration', False):
            values['duration'] = self._get_duration(values['start'], values['stop'])
        meeting = super(HrMeeting, self).create(values)

        meeting.with_context(dont_notify=True).create_attendees()
        return meeting

    @api.multi
    def unlink(self):
        # Хурал устах үед уулзалтанд бүртгэгдсэн хурал болон, хурлаас үүссэн даалгаврыг устгах
        for meeting in self:
            if meeting.state != 'draft':
                raise ValidationError(_("Cannot delete validated conference. First cancel conference!"))
            meeting.task_ids.sudo().unlink()
            meeting.calendar_id.sudo().unlink()
        res = super(HrMeeting, self).unlink()
        return res

    @api.multi
    def copy(self, default=None):
        self.ensure_one()
        default = default or {}
        return super(HrMeeting, self).copy(default)

