# -*- coding: utf-8 -*-
{
	"name" : "Mongolian Organization Structure",
	"version" : "1.0",
	"author" : "Asterisk Technologies LLC",
	"category" : "Human Resource",
	"description" : """
Байгууллагын жилийн орон тоог төлөвлөнө, гүйцэтгэлийг хянана
""",
	"website" : "http://asterisk-tech.mn",
	'depends': ['l10n_mn_hr', 'l10n_mn_hr_regulation', 'l10n_mn_account_period'],
	"data" : [
		'security/ir.model.access.csv',
		'views/hr_org_structure_view.xml',
		'views/hr_department_view.xml',
	],
	'application': True,
	"active": False,
	"installable": True,
}
