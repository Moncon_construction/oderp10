# -*- coding: utf-8 -*-
import time
from odoo import api, fields, models, _
from odoo.exceptions import except_orm

class HrOrgStructureLine(models.Model):
    _name = "hr.org.structure.position.line"
    _description = "Organization Structure Position Line"

    @api.multi
    def _current_position_count(self):
        for emp in self:
            employee_ids = self.env['hr.employee'].search_count([('department_id', '=', emp.job_id.department_id.id), ('job_id', '=', emp.job_id.id)])
            emp.current_position_number = employee_ids

    job_id = fields.Many2one('hr.job', string='Job', required=True)
    position_number = fields.Integer('Position number', required=True)
    current_position_number = fields.Integer(compute='_current_position_count', string='Current position count')

class HrOrgStructureDepLine(models.Model):
    _name = "hr.org.structure.dep.line"
    _description = "Organization Structure Department Line"
    
    department_id = fields.Many2one('hr.department', string='Department', required=True)
    regulation_id = fields.Many2one('hr.regulation', string='Regulation')
    
class HrOrgStructurePositionLine(models.Model):
    _inherit = "hr.org.structure.position.line"
    
    dep_line_id = fields.Many2one('hr.org.structure.dep.line', string='Department')
    
class HrOrgStructure(models.Model):
    _name = "hr.org.structure"
    _description = "Organization Structure"

    name = fields.Char('Name', required=True, states={'confirmed': [('readonly', True)]})
    fiscalyear_id = fields.Many2one('account.fiscalyear', string='Fiscal Year', 
                                    required=True, states={'confirmed': [('readonly', True)]})
    confirm_date = fields.Date('Confirmed Date', required=True, 
                               states={'confirmed': [('readonly', True)]}, 
                               default=lambda *a: time.strftime('%Y-%m-%d'))
    state = fields.Selection([
                            ('draft', 'Draft'),
                            ('confirmed', 'Confirmed')
                            ], track_visibility='onchange', string='State', default='draft')
    company_id = fields.Many2one('res.company', 'Company', states={'confirmed': [('readonly', True)]}, 
                                 default=lambda self: self.env['res.company']._company_default_get())
    description = fields.Text('Description')
    parent_id = fields.Many2one('hr.org.structure', 'Modification of')
    regulation_id = fields.Many2one('hr.regulation', string='Regulation', 
                                    states={'confirmed': [('readonly', True)]})

    @api.onchange('regulation_id')
    def onchange_regulation(self):
        if self.regulation_id:
            self.name = self.regulation_id.name
            
    @api.multi
    def action_to_confirm(self):
        return self.write({'state': 'confirmed'})
    
    @api.multi
    def action_to_draft(self):
        return self.write({'state': 'draft'})

class HrOrgStructureDepLine1(models.Model):
    _inherit = "hr.org.structure.dep.line"
    
    @api.multi
    def _compute_count(self):
        for dep_line in self:
            sub_count = 0
            for struct in dep_line.position_line_ids:
                sub_count += struct.position_number
            dep_line.total_number = sub_count

    @api.multi    
    def _current_position_count(self):
        for line in self:
            curr_count = 0
            for pos in line.position_line_ids:
                curr_count += pos.current_position_number
            line.current_total_number = curr_count
    
    struct_id = fields.Many2one('hr.org.structure', string='Organization Structure')
    position_line_ids = fields.One2many('hr.org.structure.position.line', 'dep_line_id', string='Positions')
    total_number = fields.Integer(compute='_compute_count', string='Position count')
    current_total_number = fields.Integer(compute='_current_position_count', string='Current position count')

class HrOrgStructure1(models.Model):
    _inherit = "hr.org.structure"
    
    line_ids = fields.One2many('hr.org.structure.dep.line', 'struct_id', string='Lines', states={'confirmed': [('readonly', True)]})
            
    @api.onchange('regulation_id')
    def onchange_regulation_id(self):
        if self.regulation_id:
            for line in self.line_ids:
                line.regulation_id = self.regulation_id.id

    @api.multi
    def preview_structure(self):
        dep_obj = self.env['hr.department']
        dep_dic = {}
        deps = dep_obj.search([])
        deps.write({'planned_employees_draft': 0})
        for struct in self:
            for dep_line in struct.line_ids:
                dep_position_num = 0
                for job_line in dep_line.position_line_ids:
                    dep_position_num += job_line.position_number
                dep_line.department_id.write({'planned_employees_draft': dep_position_num})
                dep_dic[dep_line.department_id.id] = dep_position_num

        for dep in deps:
            dep_draft_planned_emp_num = 0
            child_dep_ids = dep_obj.search([('id', 'child_of', dep.id)])
            child_parent_dep_ids = dep_obj.search([('id', 'child_of', dep.id), ('active', '=', True), ('child_ids', '!=', False)])
            if child_parent_dep_ids:
                for child_parent_id in child_parent_dep_ids:
                    dep_draft_planned_emp_num += dep_dic[child_parent_id.id] if child_parent_id.id in dep_dic.keys() else 0

            if child_dep_ids:
                for child_dep in child_dep_ids:
                    dep_draft_planned_emp_num += child_dep.planned_employees_draft
                dep.write({'planned_employees_draft': dep_draft_planned_emp_num})
    
        result = self.env['ir.actions.act_window'].for_xml_id('l10n_mn_hr_org_structure', 'department_parent_preview_action')
        return result
             
    @api.multi
    def create_modify(self):
        for struct in self:
            struct_vals = {
                            'name': struct.name,
                            'fiscalyear_id': struct.fiscalyear_id.id,
                            'parent_id': struct.id,
                            'state': 'draft'
                           }
            struct_id = self.env['hr.org.structure'].create(struct_vals)
            for line in struct.line_ids:
                dep_vals = {
                            'struct_id': struct_id.id,
                            'department_id': line.department_id.id,
                            'regulation_id': line.regulation_id.id
                            }
                dep_line_id = self.env['hr.org.structure.dep.line'].create(dep_vals)
                for position_line in line.position_line_ids:
                    position_vals = {
                                    'dep_line_id': dep_line_id.id,
                                    'job_id': position_line.job_id.id,
                                    'position_number': position_line.position_number
                                    }
                    self.env['hr.org.structure.position.line'].create(position_vals)

        mod_obj = self.env['ir.model.data']
        res = mod_obj.get_object_reference('l10n_mn_hr_org_structure', 'hr_org_structure_form')
        res_id = res and res[1] or False
        return {
            'name': _('Organization Structure'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': [res_id],
            'res_model': 'hr.org.structure',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            'res_id': struct_id.id or False,
        }

class HrDepartment(models.Model):
    _inherit = "hr.department"

    @api.multi
    def _count_employee(self):
        employee_obj = self.env['hr.employee']
        dep_obj = self.env['hr.department']
        status_obj = self.env['hr.employee.status']
        struct_obj = self.env['hr.org.structure']
        dep_planned_emps = {}

        fiscalyear_id = self.env['account.fiscalyear'].search([('date_start', '<=', time.strftime('%Y-%m-%d')), ('date_stop', '>=', time.strftime('%Y-%m-%d'))], limit=1)
        if not fiscalyear_id:
            raise except_orm(_('Warning'), _("Please insert fiscal year %s") % time.strftime('%Y'))

        org_struct_ids = struct_obj.search([('fiscalyear_id', '=', fiscalyear_id.id), ('state', '=', 'confirmed')])
        for struct in org_struct_ids:
            for line in struct.line_ids:
                dep_planned_emps[line.department_id.id] = line.total_number

        for department in self:
            dep_objs = dep_obj.search([('id', 'child_of', department.id)])
            dep_ids = dep_objs.ids

            planned_emp_number = 0
            for dep in dep_ids:
                planned_emp_number += dep_planned_emps[dep] if dep in dep_planned_emps.keys() else 0

            basic_employee_ids = []
            status_ids = status_obj.search([('type', '=', 'basis')])
            if status_ids:
                basic_employee_ids = employee_obj.search([('department_id', 'in', dep_ids), ('active', '=', True), ('state_id', '=', status_ids.ids[0])])

            trial_employee_ids = []
            status_ids = status_obj.search([('type', '=', 'trial')])
            if status_ids:
                trial_employee_ids = employee_obj.search([('department_id', 'in', dep_ids), ('active', '=', True), ('state_id', '=', status_ids.ids[0])])

            trainee_employee_ids = []
            status_ids = status_obj.search([('type', '=', 'trainee')])
            if status_ids:
                trainee_employee_ids = employee_obj.search([('department_id', 'in', dep_ids), ('active', '=', True), ('state_id', '=', status_ids.ids[0])])

            maternity_emp_ids = []
            status_ids = status_obj.search([('type', '=', 'maternity')])
            if status_ids:
                maternity_emp_ids = employee_obj.search([('department_id', 'in', dep_ids), ('active', '=', True), ('state_id', '=', status_ids.ids[0])])

            leave_emp_ids = []
            status_ids = status_obj.search([('type', '=', 'annual_leave')])
            if status_ids:
                leave_emp_ids = employee_obj.search([('department_id', 'in', dep_ids), ('active', '=', True), ('state_id', '=', status_ids.ids[0])])

            contract_emp_ids = []
            status_ids = status_obj.search([('type', '=', 'contract')])
            if status_ids:
                contract_emp_ids = employee_obj.search([('department_id', 'in', dep_ids), ('active', '=', True), ('state_id', '=', status_ids.ids[0])])

            registered_emp_number = len(basic_employee_ids) + len(trial_employee_ids) + len(trainee_employee_ids) + len(maternity_emp_ids) + len(leave_emp_ids)
            
            department.planned_employees = planned_emp_number
            department.registered_employees = registered_emp_number
            working_employee_count = len(basic_employee_ids) + len(trial_employee_ids) + len(trainee_employee_ids) + len(contract_emp_ids)
            department.working_employee_count = working_employee_count
            department.vacancies = planned_emp_number - working_employee_count if planned_emp_number - working_employee_count > 0 else 0
            department.basic_employees = len(basic_employee_ids)
            department.trial_employees = len(trial_employee_ids)
            department.trainee_employees = len(trainee_employee_ids)
            department.maternity_employees = len(maternity_emp_ids)
            department.annual_leave_employees = len(leave_emp_ids)
            department.contract_workers = len(contract_emp_ids)

    planned_employees = fields.Integer(compute='_count_employee', string='Planned Number of Employees')
    vacancies = fields.Integer(compute='_count_employee', string='Vacancies')
    registered_employees = fields.Integer(compute='_count_employee', string='Number of Registered Employees')
    working_employee_count = fields.Integer(compute='_count_employee', string='Number of Working Employees')
    basic_employees = fields.Integer(compute='_count_employee', string='Main Employees')
    trial_employees = fields.Integer(compute='_count_employee', string='Trial Employees')
    trainee_employees = fields.Integer(compute='_count_employee', string='Trainee Employees')
    maternity_employees = fields.Integer(compute='_count_employee', string='Maternity Employees')
    annual_leave_employees = fields.Integer(compute='_count_employee', string='Employees with Annual Leave')
    contract_workers = fields.Integer(compute='_count_employee', string='Contract Workers')
    planned_employees_draft = fields.Integer('Contract Workers Draft')
