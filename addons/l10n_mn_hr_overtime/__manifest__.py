# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian HR Overtime",
    'version': '10.0.1.0',
    'depends': ['l10n_mn_hr_hour_balance','l10n_mn_workflow_config'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
         Plan to overtime requests
    """,
    'data': [
        'security/ir.model.access.csv',
        'security/hr_overtime_security.xml',
        'views/hr_overtime_request_view.xml',
        'views/hr_overtime_request_line_views.xml',
        'views/hr_overtime_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
