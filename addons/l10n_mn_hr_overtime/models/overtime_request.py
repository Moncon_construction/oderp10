# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from lxml import etree
from datetime import datetime
import pytz

STATE_SELECTION = [
    ('draft', _('Draft')),
    ('to_approve', _('To Approve')),
    ('done', _('Done'))
]

class HrOvertimeRequest(models.Model):
    _name = "hr.overtime.request"
    _description = "Hour Overtime"
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    @api.constrains("line_ids")
    def _check_create_employee(self):
        for record in self:
            if not record.line_ids:
                raise ValidationError(_("Line is empty! Please add lines"))

    @api.multi
    def _show_approve_button(self):
        history_obj = self.env['workflow.history']
        for order in self:
            history = history_obj.search([('overtime_request_id', '=', order.id), (
                'line_sequence', '=', order.check_sequence)], limit=1, order='sent_date DESC')
            if history:
                order.show_approve_button = (
                    order.state == 'to_approve' and self.env.user.id in map(lambda x: x.id, history.user_ids))
            else:
                order.show_approve_button = False

    def name_get(self):
        res = []
        for record in self:
            name = '%s, [%s]' % (record.description, record.create_uid.name)
            res.append((record.id, name))
        return res

    def _default_employee(self):
        return self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)

    @api.multi
    def get_default_workflow(self):
        employee = self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)
        workflow_id = self.env['workflow.config'].get_workflow('employee', 'hr.overtime.request', employee.id,
                                                               None)
        return workflow_id

    workflow_id = fields.Many2one('workflow.config', string='Workflow', required=True, default=get_default_workflow)
    check_sequence = fields.Integer(string='Workflow Step', default=0)
    check_users = fields.Many2many('res.users', 'hr_overtime_request_check_user_rel', 'order_id', 'user_id', 'Checkers', readonly=True, copy=False)
    show_approve_button = fields.Boolean(string='Show Approve Button?', compute='_show_approve_button')
    description = fields.Char(string="Description", required=True)
    department_id = fields.Many2one('hr.department', string='Department', default=lambda self: self.env.user.department_id, readonly='1')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    create_uid = fields.Many2one('res.users', string='Created by', readonly=True, default=lambda self: self.env.uid)
    employee_id = fields.Many2one('hr.employee', 'Employee', default=_default_employee)
    state = fields.Selection(STATE_SELECTION, string='Status', copy=False, index=True, readonly=True, default="draft")
    overtime_type = fields.Selection([('overtime_holiday', 'Overtime on Holiday'), ('overtime_normal', 'Normal Overtime')], string='Overtime type', required=True, default='overtime_normal')
    workflow_history_ids = fields.One2many('workflow.history', 'overtime_request_id', string='Request Overtime', readonly=True)
    line_ids = fields.One2many('hr.overtime.request.line', 'overtime_request_id', string='Request Lines', readonly=True, states={'draft': [('readonly', False)]}, copy=False)

    @api.multi
    def unlink(self):
        for a in self:
            if a.state != 'draft':
                raise UserError(_('You can only draft record delete!'))
        return super(HrOvertimeRequest, self).unlink()

    @api.multi
    def action_send(self):
        for obj in self:
            if obj.workflow_id:
                workflow_obj = self.env['workflow.config']
                success, current_sequence = workflow_obj.send('workflow.history', 'overtime_request_id', obj, self.env.user.id)
                is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(obj.workflow_id.id, obj, self.env.user.id, obj.check_sequence + 1, 'next')
                if is_next:
                    obj.check_users = [(6, 0, next_user_ids)]
                if success:
                    obj.check_sequence = current_sequence
                    obj.ensure_one()
                    obj.state = 'to_approve'

    @api.multi
    def action_review(self):
        self.ensure_one()
        status_obj = False
        workflow_obj = self.env['workflow.config']
        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve(
                'workflow.history', 'overtime_request_id', self, self.env.user.id)
            is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(self.workflow_id.id, self, self.env.user.id, self.check_sequence + 1, 'next')
            if is_next:
                self.check_users = [(6, 0, next_user_ids)]
            if success:
                if sub_success:
                    self.state = 'done'
                    self.step_is_final = True
                    self.check_users = [(6, 0, [])]
                else:
                    self.check_sequence = current_sequence
                    self.show_approve_button = True
        return True

    @api.multi
    def action_refuse(self):
        for obj in self:
            return obj.write({'state': 'draft', 'check_sequence':0.0})


class WorkflowHistory(models.Model):
    _inherit = 'workflow.history'

    overtime_request_id = fields.Many2one('hr.overtime.request', 'Overtime request')

class HrOvertimeRequestLine(models.Model):
    _name = "hr.overtime.request.line"
    _description = "Employee Overtime"


    @api.multi
    def compute_attendance_overtime(self):
        # Ирц дээрх илүү цаг харуулах функц
        local = pytz.timezone(self.env.user.tz)
        for line in self:

            date_time_obj = pytz.utc.localize(datetime.strptime(line.date, '%Y-%m-%d %H:%M:%S')).astimezone(local)
            date = date_time_obj.date()
            attendance = self.env['hr.attendance'].search([('employee_id', '=', line.employee_id.id),
                                                           ('work_start_time', '>=', str(date) + ' 00:00:00'),
                                                           ('work_start_time', '<=', str(date) + ' 23:59:59')])
            line.attendance_overtime = attendance.over_time_hours if attendance else 0

    date = fields.Datetime(string='Date', required=True)
    employee_id = fields.Many2one('hr.employee',  string='Employee', required=True)
    overtime = fields.Float("Overtime", required=True)
    additional_note = fields.Char(string="Description", size=64)
    overtime_request_id = fields.Many2one('hr.overtime.request', string='Overtime request', ondelete="cascade")
    attendance_overtime = fields.Float('Attendance Overtime', compute='compute_attendance_overtime')

    department_id = fields.Many2one('hr.department', related='overtime_request_id.department_id', store=True, search='_search_department_id', string='Department', default=lambda self: self.overtime_request_id.department_id)
    company_id = fields.Many2one('res.company', related='overtime_request_id.company_id', store=True, search='_search_company_id', string='Company', default=lambda self: self.overtime_request_id.company_id)
    overtime_state = fields.Selection(related='overtime_request_id.state', store=True, search='_search_parent_state', string='State', default=lambda self: self.overtime_request_id.state)

    @api.model
    def fields_view_get(self, view_id=None, view_type='tree', toolbar=False, submenu=False):

        res = super(HrOvertimeRequestLine,self).fields_view_get(view_id, view_type, toolbar=toolbar, submenu=submenu)
        root = etree.fromstring(res['arch'])

        if self._context.get('readonly_object'):
            root.set('create', 'false')
            root.set('import', 'false')
            root.set('edit', 'false')
            root.set('delete', 'false')
        else:
            root.set('create', 'true')
            root.set('import', 'true')
            root.set('edit', 'true')
            root.set('delete', 'true')
        res['arch'] = etree.tostring(root)

        return res

    @api.multi
    def _search_department_id(self, operator, value):
        res = []
        if value:
            res = self.env['hr.overtime.request.line'].search([('overtime_request_id.department_id.name', 'ilike', value)])
        return [('id', operator, res)]

    @api.multi
    def _search_company_id(self, operator, value):
        res = []
        if value:
            res = self.env['hr.overtime.request.line'].search([('overtime_request_id.company_id.name', 'ilike', value)])
        return [('id', operator, res)]

    @api.multi
    def _search_parent_state(self, operator, value):
        res = []
        if value:
            res = self.env['hr.overtime.request.line'].search([('overtime_request_id.state', '=', value)])
        return [('id', operator, res)]


