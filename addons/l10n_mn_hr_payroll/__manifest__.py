# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian HR Payroll",
    'version': '1.0',
    'depends': [
        'l10n_mn_base',
        'hr_payroll',
        'hr_payroll_account',
        'l10n_mn_account_period',
        'l10n_mn_hr_hour_balance'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
   Human Resource Payroll
    """,
    'data': [
        'security/hr_payroll_security.xml',
        'security/ir.model.access.csv',
        'views/hr_view.xml',
        'views/hr_payroll_view.xml',
        'views/hr_payroll_input_view.xml',
        'views/hr_contract_view.xml',
        'views/ndsh_config_view.xml',
        'views/res_country_view.xml',
        'security/hr_payroll_security.xml',
        'security/ir.model.access.csv',
        'data/hr_occupation_data.xml',
        'data/l10n_mn_hr_payroll_initial_data.xml',
        'data/res_country_data.xml',
        'report/hr_payroll_cart_view.xml',
        'report/hr_payslip_maternity_report_view.xml',
        'report/hr_payslip_report_view.xml',
        'report/hr_payroll_hcta_report_view.xml',
        'report/hr_payslip_ndsh_report_view.xml',
        'report/hr_payslip_hhoat_report_view.xml',
        'report/hr_salary_history_report_view.xml',
        'views/hr_employee_view.xml',
        'views/account_bank_statement_import_payslip_run_view.xml',
        'views/account_bank_statement_view.xml',
        'views/account_bank_statement_import_payslip_move_view.xml',
        'views/account_bank_statement_line_view.xml',
        'views/hr_holidays.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
