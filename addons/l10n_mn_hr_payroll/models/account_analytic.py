# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _

class AccountAnalyticLine(models.Model):
    _inherit = "account.analytic.line"
    
    payslip_run_id = fields.Many2one('hr.payslip.run', string='Payslip Run')
