# -*- coding: utf-8 -*-
from odoo import models, fields, _, api


class AccountBankStatementImportPayslipMove(models.TransientModel):

    _name = "account.bank.statement.import.payslip.move"
    _description = "Import payslip move"

    @api.multi
    def _get_domain_setter(self):
        context = self._context
        domain = []
        if 'domain_aml' in context and context['domain_aml']:
            return [('id', 'in', context['domain_aml'])]

    account_move_line_ids = fields.Many2many('account.move.line', 'account_bank_statement_import_payslip_account_move', 'import_id', 'account_move_line_id', string='Payslip move', domain=_get_domain_setter)
    journal_ids = fields.Many2many('account.journal', string='Journal')

    @api.multi
    def import_payslip_move(self):
        AccountBankStatementLine = self.env['account.bank.statement.line']
        context = self._context
        account_bank_statement = self.env[context['active_model']].browse(context['active_id'])
        payroll_config = self.env['hr.payroll.config'].search([('company_id', '=', self.env.user.company_id.id)], limit=1)
        for line in self.account_move_line_ids:
            salary_import_id = payroll_config.salary_import_ids.filtered(lambda l: l.account_id.id == line.account_id.id)
            AccountBankStatementLine.create({
                'statement_id': account_bank_statement.id,
                'partner_id': line.partner_id.id,
                'date': fields.Date.today(),
                'name': salary_import_id.name or 'Salary imported line',
                'account_id': line.account_id.id,
                'cashflow_id': salary_import_id.cashflow_id.id,
                'amount': -line.credit if line.credit else -line.debit,
                'payslip_run_id': context['aml_run'][str(line.id)],
                'import_line_id': line.id
            })
