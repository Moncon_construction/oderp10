# -*- coding: utf-8 -*-
from odoo import models, fields, _, api
from odoo.exceptions import UserError


class AccountBankStatementImportPayslipRun(models.TransientModel):

    _name = "account.bank.statement.import.payslip.run"
    _description = "Import payslip run"

    payslip_run_ids = fields.Many2many('hr.payslip.run', 'payslip_run_relation', 'import_id', 'payslip_run_id', string='Payslip run')

    @api.multi
    def import_payslip_run(self):
        account_move_line_ids = []
        payroll_config = self.env['hr.payroll.config'].search([('company_id', '=', self.env.user.company_id.id)], limit=1)
        payroll_config_account_ids = []
        context = dict(self._context or {})
        context['aml_run'] = {}
        if payroll_config:
            for i in payroll_config.salary_import_ids:
                payroll_config_account_ids.append(i.account_id.id)
            for run in self.payslip_run_ids:
                if run.move_id:
                    for line in run.move_id.line_ids.filtered(lambda l: l.credit and l.partner_id and l.account_id.user_type_id.type == 'payable' and l.move_id.state == 'posted' and l.account_id.id in payroll_config_account_ids and not l.full_reconcile_id):
                        account_move_line_ids.append(line.id)
                        context['aml_run'][str(line.id)] = run.id
        if not account_move_line_ids:
            raise UserError(_(u'There is no salary rule category which hase must be posted.\n2. Account type must be payable.\n3. Account move line must have a partner.\n4. Account move lines\' accounts must match the accounts of the payroll configuration.'))
        view = self.env.ref('l10n_mn_hr_payroll.view_account_bank_statement_import_payslip_move_form')
        context['domain_aml'] = account_move_line_ids
        return {
            'name': _('Account moves'),
            'context': context,
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'account.bank.statement.import.payslip.move',
            'views': [(view.id, 'form')],
            'type': 'ir.actions.act_window',
            'target': 'new',
        }