from odoo import models, fields


class AccountBankStatementLine(models.Model):
    _inherit = 'account.bank.statement.line'

    payslip_run_id = fields.Many2one('hr.payslip.run')