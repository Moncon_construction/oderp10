# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _

class AccountMove(models.Model):
    _inherit = "account.move"

    is_salary = fields.Boolean(string='Is Salary', default=False)