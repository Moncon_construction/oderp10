# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _

class HrDepartment(models.Model):
    _inherit = "hr.department"

    expense_type = fields.Selection([('general','General'),('sales','Sales')], string='Expense Type', required=True, default='general')

class HrOccupation(models.Model):
    '''Мэргэжилийн ангилал
    '''
    _name = "hr.occupation"

    code = fields.Char(string='Occupation Code',required=True)
    name = fields.Char(string='Occupation Name',required=True)

class HrJob(models.Model):
    _inherit = "hr.job"

    occupation_id = fields.Many2one('hr.occupation', string='Occupation')
