# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class AdditionalSalary(models.Model):
    '''Нэмэгдэл цалин
    '''
    _name = "additional.salary"
    _description = 'Additional salary'

    name = fields.Char(string='Name', size=256, required=True)
    job_ids = fields.Many2many('hr.job', 'hr_job_additional_salary_rel', 'employee_id', 'add_salary_id', string="Jobs")
    is_constant = fields.Boolean('Whether constant', default=True)
    computing_type = fields.Selection([('money', 'Money'), ('percent', 'Percent')], string='Computing Type', default='money')
    add_type = fields.Selection([('raise', 'Raise'), ('deductions', 'Deductions'), ('rebate', 'Rebate')], string='Type')
    code = fields.Many2one('hr.salary.rule.category', string='Salary rule', domain=[('select_in_inputs', '=', True)])
    amount = fields.Integer(string='Amount', default=0)


class Raises(models.Model):
    '''Цалингийн Нэмэгдэл
    '''
    _name = "raises"
    _description = 'Professional level raise'
    
    @api.multi
    @api.depends('name')
    def _compute_raise(self):
        for rais in self:
            if rais.name.is_constant:
                rais.raise_size = rais.name.amount
            else:
                rais.raise_size = 0.0

    @api.multi
    def _get_repeatedly(self):
        for rebate in self:
            count = 0
            reabte_ids = self.env['hr.payslip.input'].search([('raise_id', '=', rebate.id), ('is_count', '=', True)])
            count = len(reabte_ids)
            rebate.repeatedly_count = count

    contract_id = fields.Many2one('hr.contract', string='Contracts')
    name = fields.Many2one('additional.salary', string='Raise type', domain=[('add_type', '=', 'raise')], required=True)
    is_constant = fields.Boolean(related='name.is_constant', string="Whether constant", readonly=True, store=True)
    type = fields.Selection(related='name.computing_type', string="Type", readonly=True, store=True)
    raise_size = fields.Float(string='Raise size', compute='_compute_raise', method=True, store=True)
    period_id = fields.Many2one('account.period', string="Period", required=True)
    repeatedly = fields.Integer(string='Repeatedly')
    repeatedly_count = fields.Integer(string='Repeatedly Count', compute='_get_repeatedly', method=True, size=8)


class Deductions(models.Model):
    '''Суутгалууд
    '''
    _name = "deductions"
    _description = 'Deductions'

    @api.multi
    @api.depends('name')
    def _compute_deduct(self):
        for deduct in self:
            if deduct.name.is_constant:
                deduct.deduction_size = deduct.name.amount
            else:
                deduct.deduction_size = 0.0

    @api.multi
    def _get_repeatedly(self):
        for rebate in self:
            count = 0
            reabte_ids = self.env['hr.payslip.input'].search([('deduction_id', '=', rebate.id), ('is_count', '=', True)])
            count = len(reabte_ids)
            rebate.repeatedly_count = count

    name = fields.Many2one('additional.salary', string='Deductions type', domain=[('add_type', '=', 'deductions')], required=True)
    is_constant = fields.Boolean(related='name.is_constant', string="Whether constant", readonly=True, store=True)
    type = fields.Selection(related='name.computing_type', string="Type", readonly=True, store=True)
    deduction_size = fields.Float(string='Deduction size', compute='_compute_deduct', method=True, store=True)
    contract_id = fields.Many2one('hr.contract', string='Contracts')
    period_id = fields.Many2one('account.period', string="Period", required=True)
    repeatedly = fields.Integer(string='Repeatedly')
    repeatedly_count = fields.Integer(string='Repeatedly Count', compute='_get_repeatedly', method=True, size=8)


class Rebates(models.Model):
    '''Хөнгөлөлт
    '''
    _name = "rebates"
    _description = 'Rebates'

    @api.multi
    @api.depends('name')
    def _compute_rebate(self):
        for rebate in self:
            if rebate.name.is_constant:
                rebate.rebate_size = rebate.name.amount
            else:
                rebate.rebate_size = 0.0

    @api.multi
    def _get_repeatedly(self):
        for rebate in self:
            count = 0
            reabte_ids = self.env['hr.payslip.input'].search([('rebate_id', '=', rebate.id), ('is_count', '=', True)])
            count = len(reabte_ids)
            rebate.repeatedly_count = count

    name = fields.Many2one('additional.salary', string='Rebate type', required=True)
    is_constant = fields.Boolean(related='name.is_constant', string="Whether constant", readonly=True, store=True)
    type = fields.Selection(related='name.computing_type', string="Type", readonly=True, store=True)
    rebate_size = fields.Float(string='Rebate size', compute='_compute_rebate', method=True, store=True)
    contract_id = fields.Many2one('hr.contract', string='Contracts')
    period_id = fields.Many2one('account.period', string="Period", required=True)
    repeatedly = fields.Integer(string='Repeatedly')
    repeatedly_count = fields.Integer(string='Repeatedly Count', compute='_get_repeatedly', method=True, size=8)


class Contract(models.Model):
    _inherit = 'hr.contract'

    def _get_default_structure(self):
        structure_obj = self.env['hr.payroll.structure']
        return structure_obj.search([('code', '=', 'LAST')], limit=1)

    def _get_default_advance_structure(self):
        structure_obj = self.env['hr.payroll.structure']
        return structure_obj.search([('code', '=', 'ADVANCE')], limit=1)

    identification_id = fields.Char(related='employee_id.ssnid', string="Identification Number")
    is_esti_adv_days = fields.Boolean(string='Whether estimate advance pay day')
    idle = fields.Float(string='Idle', track_visibility='onchange')
    active = fields.Boolean(default=True)
    level_pro_raise_ids = fields.One2many('raises', 'contract_id', string='Professional level raise', required=False)
    deduction_ids = fields.One2many('deductions', 'contract_id', string='Deductions', required=False)
    rebate_ids = fields.One2many('rebates', 'contract_id', string='Rebate', required=False)
    struct_id = fields.Many2one('hr.payroll.structure', string='Salary Structure', default=_get_default_structure, track_visibility='onchange')
    advance_structure_id = fields.Many2one('hr.payroll.structure', string='Advance payroll structure',
                                           default=_get_default_advance_structure, track_visibility='onchange')
    advance_rates = fields.Float(string='Advance rates', track_visibility='onchange')
    trial_rates = fields.Float(string='Trial rates', track_visibility='onchange')
    bank_account_id = fields.Many2one('res.partner.bank',  string='Bank Account Number',
                                      related='employee_id.bank_account_id', readonly=True)
    ndsh_type = fields.Many2one('hr.ndsh.config', string='Ndsh type',  track_visibility='onchange')
    is_add_food = fields.Boolean(string='Is add Food',  track_visibility='onchange')
    is_add_vehicle = fields.Boolean(string='Is add Vehicle',  track_visibility='onchange')
    is_risk_foundation = fields.Boolean(string='Is risk foundation')
    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic Account',
                                          domain="[('type','=','normal')]")
    schedule_pay = fields.Selection(selection_add=[(('hourly'), _('Hourly')),
                                                   (('daily'), _('Daily'))],)
    overtime_salary_coefficient = fields.Float(string='Overtime Salary Coefficient', default=1.5, required=True)

    @api.constrains('overtime_salary_coefficient')
    def _constrains_overtime_salary_coefficient(self):
        for obj in self:
            if obj.overtime_salary_coefficient < 0:
                raise ValidationError(_('Overtime salary coefficient should be positive value!'))

    @api.multi
    def button_call_payslip(self):
        #        ухаалаг даруул дээр дарахад дуудагдах функц
        for obj in self:
            return {
                'type': 'ir.actions.act_window',
                'name': 'HR payslip',
                'res_model': 'hr.payslip',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'view_id': False,
                'domain': [('contract_id', '=', obj.id)],
            }

    @api.onchange('employee_id')
    def onchange_employee(self):
        res = super(Contract, self).set_contract_type()
        print res
        if self.employee_id and self.employee_id.has_disability:
            ndsh = self.env['hr.ndsh.config'].search([('code','=','22')],limit=1)
            self.ndsh_type = ndsh.id
        else:
            self.ndsh_type = False




