# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import time
from datetime import datetime, timedelta

import babel

from odoo import _, api, exceptions, fields, models, tools
from odoo.exceptions import UserError, ValidationError


class HRPayrollConfig(models.Model):
    '''Цалингийн тохиргоо
    '''
    _name = "hr.payroll.config"
    _description = "HR Payroll Config"
    _rec_name = "company_id"

    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    minimum_salary = fields.Float(string='Minimum salary', required=True, default=0)
    expense_line_by_analytic = fields.Boolean(string='Expense line by Analytic')
    fill_by_categories = fields.Boolean(string='Fill by Categories')
    seprate_account = fields.Many2many('account.account', 'account_config_payroll_rel', 'config_id', 'account_id', 'Seprate Accounts', copy=False)


class HrPayslipDeductions(models.Model):
    '''Цалингийн нэмэгдэлүүд
    '''
    _name = "hr.payslip.deductions"

    config_id = fields.Many2one('hr.payroll.config', string='Config ID')
    salary_type = fields.Selection([('last_salary', 'Last salary'),
                                    ('advance_salary', 'Advance salary')], string='Salary type', required=True)
    category_id = fields.Many2one('hr.salary.rule.category', string='Category', required=True)
    account_id = fields.Many2one('account.account', string='Account', required=True)


class HrPayslipPartner(models.Model):
    '''Цалингийн тохиргооны харилцагчид
    '''
    _name = "hr.payslip.partner"

    config_id = fields.Many2one('hr.payroll.config', string='Config ID')
    partner_id = fields.Many2one('res.partner', string='Partner', required=True)
    account_id = fields.Many2one('account.account', string='Account', required=True)


class HrPayslipRunImport(models.Model):
    '''Цалингийн хуудсын багц импортлох
    '''
    _name = "hr.payslip.run.import"

    config_id = fields.Many2one('hr.payroll.config', string='Config ID')
    account_id = fields.Many2one('account.account', string='Account', required=True)
    name = fields.Char('Name', required=True)
    cashflow_id = fields.Many2one('account.cashflow.type', 'Cashflow Type', required=True)


class HrPayslipAdditional(models.Model):
    '''Цалингийн суутгалууд
    '''
    _name = "hr.payslip.additional"

    config_id = fields.Many2one('hr.payroll.config', string='Config ID')
    salary_type = fields.Selection([('last_salary', 'Last salary'), ('advance_salary', 'Advance salary')], string='Salary type', required=True)
    category_id = fields.Many2one('hr.salary.rule.category', string='Category', required=True)
    model_id = fields.Many2one('ir.model', string='Model', required=True)
    field_id = fields.Many2one('ir.model.fields', string='Field', required=True)


class HrPayslipVacationLine(models.Model):
    '''Амралтын мөнгө бодолтын задаргаа
    '''
    _name = 'hr.payslip.vacation.line'
    _description = 'Hr Payslip Vacation Line'
    _order = 'period_id DESC'

    payslip_id = fields.Many2one('hr.payslip', string='Hr Payslip', required=True, ondelete='cascade')
    period_id = fields.Many2one('account.period', string="Period", required=True)
    is_vacation = fields.Boolean(string="Is Vacation")
    amount = fields.Float(string='Amount', required=True)
    worked_day = fields.Float(string='Worked Day', required=True)


class HrPayslipVacation(models.Model):
    '''Амралт
        (background)
    '''
    _name = 'hr.payslip.vacation'
    _description = 'Hr payslip Vacation'

    employee_id = fields.Many2one('hr.employee', string='Employee', required=True)
    period_id = fields.Many2one('account.period', string='Period', required=True)
    work_day = fields.Float(string='Work Day', required=True)
    worked_day = fields.Float(string='Worked Day', required=True)
    wage = fields.Float(string='Wage', required=True)
    gross = fields.Float(string='Gross', required=True)
    is_vacation = fields.Boolean(string='Is Vacation')
    vacation_salary = fields.Float(string='Vacation Salary')
    additional = fields.Float(string='Additional')
    additional_food_vehicle = fields.Float(string='Additional food vehicle')
    outage = fields.Float(string='Outage')
    total = fields.Float(string='Total')
    amount = fields.Float(string='Amount')
    ndsh = fields.Float(string='NDSH')
    bdndsh = fields.Float(string='BDNDSH')
    hhoat = fields.Float(string='HHOAT')
    hhoat_h = fields.Float(string='HHOAT_H')
    hhoat_food_vehicle = fields.Float(string='HHOAT food vehicle')


class HourBalance(models.Model):
    _inherit = "hour.balance"

    def can_insert_balance_line(self, employee_id, contract_id, worked_hour):
        self.ensure_one()
        can_insert = super(HourBalance, self).can_insert_balance_line(employee_id, contract_id, worked_hour)
        # create time sheets for employees with auto call ndsh config
        if not can_insert and contract_id.ndsh_type and contract_id.ndsh_type.auto_call_on_hour_balance:
            can_insert = True
        return can_insert

    def can_calc_reg_day(self, employee_id, contract_id):
        can_calc = super(HourBalance, self).can_calc_reg_day(employee_id, contract_id)
        if can_calc and contract_id.ndsh_type and contract_id.ndsh_type.auto_call_on_hour_balance:
            can_calc = False
        return can_calc


class HourBalanceLine(models.Model):
    _inherit = "hour.balance.line"
    payslip_id = fields.Many2one('hr.payslip', string='Payslip')


class HrPayslip(models.Model):
    '''Цалингийн хуудас
    '''
    _inherit = 'hr.payslip'

    advance_salary_id = fields.Many2one('hr.payslip', string='Advance Salary', domain=[('salary_type', '=', 'advance_salary')])
    vacation_id = fields.Many2one('hr.payslip', string='Vacation')
    salary_type = fields.Selection([('last_salary', 'Last salary'),
                                    ('advance_salary', 'Advance salary'),
                                    ('vacation', 'Vacation'),
                                    ('maternity', 'Maternity leave'),
                                    ('hchta', 'HCHTA Leave')], string='Salary type')
    period_id = fields.Many2one('account.period', string='Period')
    vacation_lines = fields.One2many('hr.payslip.vacation.line', 'payslip_id', string='Hr Payslip Vacation Line', readonly=True, states={'draft': [('readonly', False)]})
    is_esti_adv_days = fields.Boolean(string='Whether estimate advance pay day')
    is_advance_vacation = fields.Boolean(string='Is advance vacation')
    net_wage = fields.Float(string='Net Wage')
    vacation_day = fields.Float(string='Vacation Day')
    hour_balance_lines = fields.One2many('hour.balance.line', 'payslip_id', string='Hour Balance Line')
    is_office_employee = fields.Boolean(string='Is office employee')
    department_id = fields.Many2one('hr.department', string='Department')
    list_day = fields.Float(string='List Day')
    from_company = fields.Float(string='From Company')
    hchta_precent = fields.Float(string='Percent')
    create_employee = fields.Many2one('hr.employee', string='Accountant')
    freeze_salary = fields.Boolean(string='Freeze salary')
    describtion = fields.Text(string='Describtion')
    hhoat_h = fields.Boolean(string='Ххоат тооцох эсэх')
    previous_salary_id = fields.Many2one('hr.payslip', string='Өмнөх Сарын цалин', domain=[('salary_type', '=', 'last_salary'), ('employee_id', '=', 'employee_id.id')])
    skip_hour_balance = fields.Boolean(string='Цагийн баланс харгалзахгүй')
    from_company = fields.Float(string='From Company')
    nd_number = fields.Char(string='ND number')
    start_date = fields.Date(string='Start Date')
    end_date = fields.Date(string='End Date')
    ndsh_type = fields.Many2one('hr.ndsh.config', string='Ndsh Config')
    payroll_config = fields.Many2one(compute='_get_payroll_config', comodel_name='hr.payroll.config', string='Payroll Config')
    total_salary = fields.Float(string='Total Salary')
    bonus_salary = fields.Boolean(string='Bonus salary')
    skill_level = fields.Selection([('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5')], string='Skill Level')
    maternity_salary_id = fields.Many2one('hr.payslip', string='Maternity Salary', domain=[('salary_type', '=', 'maternity')])
    hchta_id = fields.Many2one('hr.payslip', string='HCHTA', domain=[('salary_type', '=', 'hchta')])

    """ Ажилтан бүрийн гэрээн дээр нэмэгдэл, суутгал, хөнгөлөлтийг өөр өөрөөр тохируулсаны дараа цалинг тооцоолоход
        нэгэн ижил хэмжээгээр татагдаж байгаа учраас тооцоолох функцийг нь дахин тодорхойлж тухайн өөрчлөлтийг цалинд авах
        тооцооллыг хийж байгаа юм."""

    def _get_hr_salary_line_insert(self, line, date_time, user_id, slip_id, condition, company_id):
        return '''insert into hr_payslip_line (create_date, create_uid,write_date,write_uid,code, sequence,rate,appears_on_payslip,condition_range,
                                                                amount_fix,employee_id,contract_id,amount_percentage,condition_range_min,
                                                                condition_select,amount_percentage_base,amount_select,condition_range_max,
                                                                salary_rule_id,name,amount,category_id,quantity,slip_id,
                                                                condition_python,active,company_id)
                                                                 values('%s',%s,'%s',%s,'%s', %s, %s,%s,'%s',%s,%s,%s,%s,%s,'%s','%s',
                                                                 '%s',%s,%s,'%s',%s,%s,%s,%s,'%s',%s,%s) RETURNING id;''' % (date_time, user_id, date_time, user_id,
                                                                                                                             line[2]['code'], line[2]['sequence'], line[2]['rate'],
                                                                                                                             line[2]['appears_on_payslip'],
                                                                                                                             line[2]['condition_range'],
                                                                                                                             line[2]['amount_fix'], line[2]['employee_id'], line[2]['contract_id'],
                                                                                                                             line[2]['amount_percentage'], line[2]['condition_range_min'],
                                                                                                                             line[2]['condition_select'], line[2]['amount_percentage_base'],
                                                                                                                             line[2]['amount_select'], line[2]['condition_range_max'],
                                                                                                                             line[2]['salary_rule_id'],
                                                                                                                             line[2]['name'],
                                                                                                                             line[2]['amount'],
                                                                                                                             line[2]['category_id'],
                                                                                                                             line[2]['quantity'],
                                                                                                                             slip_id, condition, True, company_id)

    @api.multi
    def compute_sheet(self):
        context = self._context or {}
        payslip_line_obj = self.env['hr.payslip.line']
        for obj in self:
            number = obj.number or self.env['ir.sequence'].next_by_code('salary.slip')
            contract_ids = obj.contract_id.ids or self.get_contract(obj.employee_id, obj.date_from, obj.date_to)
            if 'compute_all' not in context:
                obj.line_ids.unlink()
            lines = [(0, 0, line) for line in self.get_payslip_lines(contract_ids, obj.id)]

            '''Өмнө нь obj.write({'line_ids': line}) гэсэн байсныг дараах байдлаар query болгов.
            Энэ нь илүү хурдан цалингийн хуудасны мөрүүдийг үүсгэж байгаа юм.'''

            for line in lines:
                # condition_python талбар нь дотроо '' утга бүхий текст агуулсан байсан бөгөөд энэ нь insert
                # query хийхэд алдаа болж байсан тул replace хийв.
                condition = str(line[2]['condition_python']).replace("'", "")
                company_id = self.env['res.company']._company_default_get()
                user_id = self.env.user.id
                date_time = datetime.now()
                self.env.cr.execute(obj._get_hr_salary_line_insert(line, date_time, user_id, obj.id, condition, company_id.id))
                line_id = self.env.cr.fetchone()[0]
                payslip_line = payslip_line_obj.browse(line_id)
                total = float(payslip_line.quantity) * payslip_line.amount * payslip_line.rate / 100
                payslip_line.write({'amount_python_compute': line[2]['amount_python_compute'],
                                    'register_id': line[2]['register_id'], 'total': total})
            obj.write({'number': number})
            total_raises = {}
            total_deductions = {}
            total_rebates = {}

            # нэмэгдлүүдийг ажилтан болгонд өөрчлөхөд тухайн утгууд нь raises - т нэмэгддэг бөгөөд contract_id - аар нь дамжуулж хайна
            for salary_raise in obj.contract_id.level_pro_raise_ids:
                # нэмэгдлүүдийг төрлөөр нь хайлт хийгээд гэрээнд тогтмол биш гэж сонгосон байвал raises дэх утгуудыг онооно.
                for category in obj.details_by_salary_rule_category:
                    code = salary_raise.name.code.code
                    if category.code == code:
                        if code not in total_raises:
                            total_raises[code] = 0
                        if not salary_raise.name.is_constant:
                            # нэг төрлийн хэд хэдэн нэмэгдлүүд орж ирж болох учраас total_raises[code] гэж dict үүсгэж төрөл төрлөөр нь нийлбэрийг нь олж байна
                            total_raises[code] += salary_raise.raise_size
                        else:
                            total_raises[code] += salary_raise.name.amount
                        category.amount = total_raises[code]
                        break

            # суутгалуудыг ажилтан болгонд өөрчлөхөд тухайн утгууд нь deductions -т нэмэгддэг бөгөөд contract_id - аар нь дамжуулж хайна
            for salary_deduction in obj.contract_id.deduction_ids:
                # суутгалуудыг төрлөөр нь хайлт хийгээд гэрээнд тогтмол биш гэж сонгосон байвал deductions дэх утгуудыг онооно.
                for category in obj.details_by_salary_rule_category:
                    code = salary_deduction.name.code.code
                    if category.code == code:
                        if code not in total_deductions:
                            total_deductions[code] = 0
                        if not salary_deduction.name.is_constant:
                            total_deductions[code] += salary_deduction.deduction_size
                        else:
                            total_deductions[code] += salary_deduction.name.amount
                        category.amount = total_deductions[code]
                        break

            # хөнгөлөлтүүдийг ажилтан болгонд өөрчлөхөд тухайн утгууд нь rebates -т нэмэгддэг бөгөөд contract_id - аар нь дамжуулж хайна
            for salary_rebate in obj.contract_id.rebate_ids:
                # хөнгөлөлтүүдийг төрлөөр нь хайлт хийгээд гэрээнд тогтмол биш гэж сонгосон байвал rebates дэх утгуудыг онооно.
                for category in obj.details_by_salary_rule_category:
                    code = salary_rebate.name.code.code
                    if category.code == code:
                        if code not in total_rebates:
                            total_rebates[code] = 0
                        if not salary_rebate.name.is_constant:
                            total_rebates[code] += salary_rebate.rebate_size
                        else:
                            total_rebates[code] += salary_rebate.name.amount
                        category.amount = total_rebates[code]
                        break

    @api.depends('company_id')
    def _get_payroll_config(self):
        for line in self:
            config = self.env['hr.payroll.config'].search([('company_id', '=', line.company_id.id)], limit=1)
            if config:
                line.payroll_config = config.id

    @api.onchange('start_date', 'end_date')
    def onchange_start_date(self):
        if self.start_date and self.end_date:
            d1 = datetime.strptime(str(self.start_date), "%Y-%m-%d")
            d2 = datetime.strptime(str(self.end_date), "%Y-%m-%d")
            day = str((d2 - d1).days + 1)
            self.list_day = day

    @api.onchange('period_id')
    def onchange_period_id(self):
        if self.period_id:
            self.date_from = self.period_id.date_start
            self.date_to = self.period_id.date_stop
            ttyme = datetime.fromtimestamp(time.mktime(time.strptime(self.date_from, "%Y-%m-%d")))
            locale = self.env.context.get('lang', 'en_US')
            self.name = _('Salary Slip of %s for %s') % (self.employee_id.name, tools.ustr(babel.dates.format_date(date=ttyme, format='MMMM-y', locale=locale)))

    @api.multi
    def onchange_employee_id(self, date_from, date_to, salary_type="", employee_id=False, contract_id=False):
        # defaults
        res = {
            'value': {
                'line_ids': [],
                # delete old input lines
                'input_line_ids': map(lambda x: (2, x,), self.input_line_ids.ids),
                # delete old worked days lines
                'worked_days_line_ids': map(lambda x: (2, x,), self.worked_days_line_ids.ids),
                # 'details_by_salary_head':[], TODO put me back
                'name': '',
                'contract_id': False,
                'struct_id': False,
                'vacation_lines': [],
                'details_by_salary_rule_category': []
            }
        }

        if (not employee_id) or (not date_from) or (not date_to):
            return res
        ttyme = datetime.fromtimestamp(time.mktime(time.strptime(date_from, "%Y-%m-%d")))
        employee = self.env['hr.employee'].browse(employee_id)

        stype = ''
        if salary_type == 'last_salary':
            stype = 'Last salary'
        elif salary_type == 'advance_salary':
            stype = 'Advance salary'
        elif salary_type == 'vacation':
            stype = 'Vacation salary'
        elif salary_type == 'hchta':
            stype = 'Hchta salary'
        elif salary_type == 'maternity':
            stype = 'Maternity salary'

        locale = self.env.context.get('lang', 'en_US')
        res['value'].update({
            'name': _('%s: Salary Slip of %s for %s') % (stype, employee.name, tools.ustr(babel.dates.format_date(date=ttyme, format='MMMM-y', locale=locale))),
            'company_id': employee.company_id.id,
            'department_id': employee.department_id.id,
        })

        if not self.env.context.get('contract'):
            # fill with the first contract of the employee
            contract_ids = self.get_contract(employee, date_from, date_to)
        else:
            if contract_id:
                # set the list of contract for which the input have to be filled
                contract_ids = [contract_id]
            else:
                # if we don't give the contract, then the input to fill should be for all current contracts of the employee
                contract_ids = self.get_contract(employee, date_from, date_to)

        if not contract_ids:
            return res
        contract = self.env['hr.contract'].browse(contract_ids[0])
        res['value'].update({
            'contract_id': contract.id,
            'ndsh_type': contract.ndsh_type.id if contract.ndsh_type else ""
        })
        return res

    @api.onchange('employee_id', 'date_from', 'date_to')
    def onchange_employee(self):

        if (not self.employee_id) or (not self.date_from) or (not self.date_to):
            return

        employee = self.employee_id
        date_from = self.date_from
        date_to = self.date_to

        ttyme = datetime.fromtimestamp(time.mktime(time.strptime(date_from, "%Y-%m-%d")))
        locale = self.env.context.get('lang', 'en_US')
        stype = ''
        if self.salary_type == 'last_salary':
            stype = 'Last salary'
        elif self.salary_type == 'advance_salary':
            stype = 'Advance salary'
        elif self.salary_type == 'vacation':
            stype = 'Vacation salary'
        elif self.salary_type == 'hchta':
            stype = 'Hchta salary'
        elif self.salary_type == 'maternity':
            stype = 'Maternity salary'

        self.name = _('%s: Salary Slip of %s for %s') % (stype, employee.name, tools.ustr(babel.dates.format_date(date=ttyme, format='MMMM-y', locale=locale)))
        self.company_id = employee.company_id
        self.department_id = employee.department_id

        if not self.env.context.get('contract') or not self.contract_id:
            contract_ids = self.get_contract(employee, date_from, date_to)
            if not contract_ids:
                self.contract_id = False
                return
            self.contract_id = self.env['hr.contract'].browse(contract_ids[0])

        if not self.contract_id.struct_id:
            return
        if self.contract_id.ndsh_type:
            self.ndsh_type = self.contract_id.ndsh_type
        else:
            raise UserError(_("Please Configure ndsh type in contract"))
        self.input_line_ids = []
        return

    @api.multi
    def onchange_contract_id(self, contract_id, date_from, period, salary_type):
        res = {'value': {}}
        input_line_ids = self.get_contract_inputs(self.env['hr.contract'].browse(contract_id), date_from, period, salary_type)
        res['value'].update({
            'input_line_ids': input_line_ids
        })
        return res

    @api.onchange('contract_id')
    def onchange_contract(self):
        res = {'value': {}}
        if self.contract_id:
            input_line_ids = self.get_contract_inputs(self.contract_id, self.date_from, self.period_id, self.salary_type)
            res['value'].update({
                'input_line_ids': input_line_ids
            })
        return res

    @api.multi
    def get_contract_inputs(self, contract, date_from, period, salary_type):
        res = []
        categ_ids = []
        if contract:
            for pro_raise in contract.level_pro_raise_ids:
                '''Хөдөлмөрийн гэрээ нь дээр тохуулсан Цалингийн Нэмэгдэлын авна
                '''
                inputs = {}
                start_date = datetime.strptime(str(pro_raise.period_id.date_start) + ' 00:00:00', '%Y-%m-%d %H:%M:%S')
                slip_date = datetime.strptime(str(date_from) + ' 00:00:01', '%Y-%m-%d %H:%M:%S')
                if slip_date >= start_date:
                    if pro_raise.repeatedly == 0:
                        inputs = {
                            'name': pro_raise.name.name,
                            'code': pro_raise.name.code.code,
                            # contract.wage дамжуулж байсныг болиулж contract дамжуулав
                            'amount': self.get_addition_amount(pro_raise.name.id, contract),
                            'category_id': pro_raise.name.code.id,
                            'contract_id': contract.id,
                            'raise_id': pro_raise.id,
                            'is_count': False,
                        }
                        res += [inputs]
                        categ_ids.append(pro_raise.name.code.id)
                    else:
                        if pro_raise.repeatedly > pro_raise.repeatedly_count:
                            inputs = {
                                'name': pro_raise.name.name,
                                'code': pro_raise.name.code.code,
                                # contract.wage дамжуулж байсныг болиулж contract дамжуулав
                                'amount': self.get_addition_amount(pro_raise.name.id, contract),
                                'category_id': pro_raise.name.code.id,
                                'contract_id': contract.id,
                                'raise_id': pro_raise.id,
                                'is_count': False,
                            }
                            res += [inputs]
                            categ_ids.append(pro_raise.name.code.id)
            for pro_deduction in contract.deduction_ids:
                '''Хөдөлмөрийн гэрээ нь дээр тохуулсан Цалингийн Суутгалууд авна
                '''
                inputs = {}
                start_date = datetime.strptime(str(pro_deduction.period_id.date_start) + ' 00:00:00', '%Y-%m-%d %H:%M:%S')
                slip_date = datetime.strptime(str(date_from) + ' 00:00:01', '%Y-%m-%d %H:%M:%S')
                if slip_date >= start_date:
                    if pro_deduction.repeatedly == 0:
                        inputs = {
                            'name': pro_deduction.name.name,
                            'code': pro_deduction.name.code.code,
                            # contract.wage дамжуулж байсныг болиулж contract дамжуулав
                            'amount': self.get_addition_amount(pro_deduction.name.id, contract),
                            'category_id': pro_deduction.name.code.id,
                            'contract_id': contract.id,
                            'deduction_id': pro_deduction.id,
                            'is_count': False,
                        }
                        res += [inputs]
                        categ_ids.append(pro_deduction.name.code.id)
                    else:
                        if pro_deduction.repeatedly > pro_deduction.repeatedly_count:
                            inputs = {
                                'name': pro_deduction.name.name,
                                'code': pro_deduction.name.code.code,
                                # contract.wage дамжуулж байсныг болиулж contract дамжуулав
                                'amount': self.get_addition_amount(pro_deduction.name.id, contract),
                                'category_id': pro_deduction.name.code.id,
                                'contract_id': contract.id,
                                'deduction_id': pro_deduction.id,
                                'is_count': False,
                            }
                            res += [inputs]
                        categ_ids.append(pro_deduction.name.code.id)
            for pro_rebate in contract.rebate_ids:
                '''Хөдөлмөрийн гэрээ нь дээр тохуулсан Хөнгөлөлт авна
                '''
                inputs = {}
                start_date = datetime.strptime(str(pro_rebate.period_id.date_start) + ' 00:00:00', '%Y-%m-%d %H:%M:%S')
                slip_date = datetime.strptime(str(date_from) + ' 00:00:01', '%Y-%m-%d %H:%M:%S')
                if slip_date >= start_date:
                    if pro_rebate.repeatedly == 0:
                        inputs = {
                            'name': pro_rebate.name.name,
                            'code': pro_rebate.name.code.code,
                            # contract.wage дамжуулж байсныг болиулж contract дамжуулав
                            'amount': self.get_addition_amount(pro_rebate.name.id, contract),
                            'category_id': pro_rebate.name.code.id,
                            'contract_id': contract.id,
                            'rebate_id': pro_rebate.id,
                            'is_count': False,
                        }
                        res += [inputs]
                        categ_ids.append(pro_rebate.name.code.id)
                    else:
                        if pro_rebate.repeatedly > pro_rebate.repeatedly_count:
                            inputs = {
                                'name': pro_rebate.name.name,
                                'code': pro_rebate.name.code.code,
                                # contract.wage дамжуулж байсныг болиулж contract дамжуулав
                                'amount': self.get_addition_amount(pro_rebate.name.id, contract),
                                'category_id': pro_rebate.name.code.id,
                                'contract_id': contract.id,
                                'rebate_id': pro_rebate.id,
                                'is_count': False,
                            }
                            res += [inputs]
                            categ_ids.append(pro_rebate.name.code.id)

            account_id = False
            partner_id = False
            category_id = False
            company_id = False
            balance = 0
            model_id = False
            company_id = contract.employee_id.department_id.company_id.id
            partner_id = contract.employee_id.address_home_id.id
            conf_ids = self.env['hr.payroll.config'].search([('company_id', '=', company_id)])
            for conf in conf_ids:
                for ded_line in conf.salary_deductions:
                    '''Цалингийн тохиргоо нь дээрх суутгалыг авах
                    '''
                    balance = 0
                    if salary_type == ded_line.salary_type:
                        account_id = ded_line.account_id.id
                        category_id = ded_line.category_id
                    if account_id and partner_id:
                        self.env.cr.execute("select l.partner_id, l.account_id, coalesce(sum(l.debit-l.credit), 0) "
                                            "from account_move_line l "
                                            "join account_move m on l.move_id = m.id "
                                            "join account_account a on a.id = l.account_id "
                                            "where l.partner_id is not null and l.partner_id=%s and a.id=%s"
                                            "group by l.partner_id, l.account_id " % (partner_id, account_id))
                        for partner_id, account_id, amount in self.env.cr.fetchall():
                            balance = amount
                        if balance > 0:
                            inputs = {
                                'name': category_id.name,
                                'code': category_id.code,
                                'amount': balance,
                                'category_id': category_id.id,
                                'contract_id': contract.id,
                                'rebate_id': False,
                                'is_count': False,
                            }

                            res += [inputs]
                            categ_ids.append(category_id.id)
                for add_line in conf.salary_additionals:
                    '''Цалингийн тохиргоо нь дээрх нэмэгдлийг авах
                    '''
                    balance = 0
                    if salary_type == add_line.salary_type:
                        model_id = add_line.model_id
                        field_id = add_line.field_id
                        category_id = add_line.category_id
                    if category_id and model_id:
                        additional_ids = self.env['hr.payslip'].search([('employee_id', '=', contract.employee_id.id), ('period_id', '=', period.id),
                                                                        ('state', '=', 'approved')])
                        if additional_ids:
                            for add in additional_ids:
                                inputs = {
                                    'name': category_id.name,
                                    'code': category_id.code,
                                    'amount': add[field_id.name],
                                    'category_id': category_id.id,
                                    'contract_id': contract.id,
                                    'rebate_id': False,
                                    'is_count': False,
                                }
                                res += [inputs]
                                categ_ids.append(category_id.id)
                if conf.fill_by_categories:
                    '''Цалингийн тохиргоо нь дээрх Цалингийн дүрмийн ангилалууд үнэн бол
                    '''
                    new_categ_ids = self.env['hr.salary.rule.category'].search([('select_in_inputs', '=', True),
                                                                                ('id', 'not in', categ_ids),
                                                                                ('company_id', '=', company_id)])
                    for categ in new_categ_ids:
                        inputs = {
                            'name': categ.name,
                            'code': categ.code,
                            'amount': 0,
                            'category_id': categ.id,
                            'contract_id': contract.id,
                            'rebate_id': False,
                            'is_count': False,
                        }
                        res += [inputs]
        return res

    @api.multi
    def get_addition_amount(self, additional_id, contract):
        amount = 0
        add_ids = self.env['additional.salary'].search([('id', '=', additional_id)])
        for add in add_ids:
            # Хэрвээ additional_salary дахь "тогтмол эсэх" нь үнэн, худлыг нь шалгаад ҮНЭН байх юм бол additional_salary -н утгыг авна.
            if add.is_constant:
                if add.computing_type == 'money':
                    amount = add.amount
                elif add.computing_type == 'percent':
                    amount = contract.wage / 100 * add.amount
            # Худал байвал гэрээнд өөрөөр тохируулсан утгыг авна. Эдгээр нь raises, deductions, rebates -т хадгалагддаг.
            else:
                if add.add_type == 'raise':

                    raise_obj = self.env['raises'].search([('name', '=', additional_id), ('contract_id', '=', contract.id)])
                    if add.computing_type == 'money':
                        amount = raise_obj.raise_size
                    elif add.computing_type == 'percent':
                        amount = contract.wage / 100 * raise_obj.raise_size
                    # суутгал
                elif add.add_type == 'deductions':
                    deduction_obj = self.env['deductions'].search([('name', '=', additional_id), ('contract_id', '=', contract.id)])
                    # for obj in deduction_obj:
                    if add.computing_type == 'money':
                        amount = deduction_obj.deduction_size
                    elif add.computing_type == 'percent':
                        amount = contract.wage / 100 * deduction_obj.deduction_size
                    # хөнгөлөлт
                elif add.add_type == 'rebate':
                    rebate_obj = self.env['rebates'].search([('name', '=', additional_id), ('contract_id', '=', contract.id)])
                    # for obj in rebate_obj:
                    if add.computing_type == 'money':
                        amount = rebate_obj.rebate_size
                    elif add.computing_type == 'percent':
                        amount = contract.wage / 100 * rebate_obj.rebate_size

        return amount

    @api.multi
    def onchange_type_id(self, contract_id, employee, period, salary_type):
        '''Цалин бодолтын төрөл сонгох үед
        '''
        res = {}
        res['value'] = {'worked_days_line_ids': ''}
        if contract_id:
            contract = self.env['hr.contract'].browse(contract_id)
            if salary_type == 'last_salary':
                # СҮҮЛ ЦАЛИН
                # Гэрээ нь дээр тохируулсан Цалин бодолтын загварыг авах
                res['value'].update({'struct_id': contract.struct_id.id})

                # Урьдчилгаа Цалинг олох
                advance_salary_id = self._get_advance_salary(employee.id, period.id)
                res['value'].update({'advance_salary_id': advance_salary_id})

                is_advance_vacation = False
                vacation_id = self._get_vacation(employee.id, period.id, is_advance_vacation)
                res['value'].update({'vacation_id': vacation_id})

                # Өмнөх сарын цалингийн хуудасыг олох
                previous_salary_id = self._get_previous_salary(employee.id, period)
                res['value'].update({'previous_salary_id': previous_salary_id})

                # Жирэмсэний амралт олох
                maternity_id = self._get_maternity_salary(employee.id, period.id)
                res['value'].update({'maternity_salary_id': maternity_id})

                # ХЧТА олох
                hchta_id = self._get_hchta_id(employee.id, period.id)
                res['value'].update({'hchta_id': hchta_id})

                # Цагийн баланс олох
                hour_balance_line_id = self._get_hour_balance(employee.id, salary_type, period)
                res['value'].update({'hour_balance_lines': hour_balance_line_id})
                if self.bonus_salary:
                    # Ээлжийн амралт бодолтын мөр олох
                    input_vacation_lines = self.get_vacation_lines(employee.id, period, 12, False)
                    res['value'].update({
                        'vacation_lines': input_vacation_lines,
                        'worked_days_line_ids': [],
                        'input_line_ids': [],
                    })
            elif salary_type == 'advance_salary':
                # УРЬДЧИЛГАА ЦАЛИН
                res['value'].update({'struct_id': contract.advance_structure_id.id, 'advance_salary_id': ''})

                # Цагийн баланс олох
                hour_balance_line_id = self._get_hour_balance(employee.id, salary_type, period)
                res['value'].update({'hour_balance_lines': hour_balance_line_id})

                # Ээлжийн амраллтай эсэхийг олох
                is_advance_vacation = True
                vacation_id = self._get_vacation(employee.id, period.id, is_advance_vacation)
                res['value'].update({'vacation_id': vacation_id})

                # Жирэмсэний амралт олох
                maternity_id = self._get_maternity_salary(employee.id, period.id)
                res['value'].update({'maternity_salary_id': maternity_id})
                # ХЧТА олох

                hchta_id = self._get_hchta_id(employee.id, period.id)
                res['value'].update({'hchta_id': hchta_id})

                # Урьдчилгааг гүйцэтгэлээр тооцох эсэх
                is_esti_adv_days = self.get_is_esti_adv_days(contract)
                res['value'].update({
                    'is_esti_adv_days': is_esti_adv_days
                })
            elif salary_type == 'vacation':
                # АМРАЛТЫН ЦАЛИН
                struct = self.env['hr.payroll.structure'].search([('salary_type', '=', salary_type)], limit=1)
                if struct:
                    res['value'].update({'struct_id': struct.id})
                # Ээлжийн амралт бодолтын мөр олох
                input_vacation_lines = self.get_vacation_lines(employee.id, period, 12, True)
                res['value'].update({
                    'vacation_lines': input_vacation_lines, 'worked_days_line_ids': [], 'input_line_ids': [],
                })
            elif salary_type == 'maternity':
                # Жирэмсний амралт
                struct = self.env['hr.payroll.structure'].search([('salary_type', '=', salary_type)], limit=1)
                if struct:
                    res['value'].update({'struct_id': struct.id})
                # Жирэмсний амралт бодолтын мөр олох
                input_vacation_lines = self.get_vacation_lines(employee.id, period, 13, False)
                res['value'].update({
                    'vacation_lines': input_vacation_lines, 'worked_days_line_ids': [], 'input_line_ids': [],
                })
            elif salary_type == 'hchta':
                struct = self.env['hr.payroll.structure'].search([('salary_type', '=', salary_type)], limit=1)
                if struct:
                    res['value'].update({'struct_id': struct.id})

                input_vacation_lines = self.get_vacation_lines(employee.id, period, 4, False)
                res['value'].update({
                    'vacation_lines': input_vacation_lines, 'worked_days_line_ids': [], 'input_line_ids': [],
                })
            else:
                res['value'].update({'struct_id': False})
        else:
            res['value'].update({'struct_id': False})
        return res

    @api.onchange('salary_type')
    def onchange_type(self):
        '''Цалин бодолтын төрөл сонгох үед
        '''
        employee = self.employee_id
        date_from = self.date_from
        ttyme = datetime.fromtimestamp(time.mktime(time.strptime(date_from, "%Y-%m-%d")))
        locale = self.env.context.get('lang', 'en_US')
        stype = ''
        if self.salary_type == 'last_salary':
            stype = 'Last salary'
        elif self.salary_type == 'advance_salary':
            stype = 'Advance salary'
        elif self.salary_type == 'vacation':
            stype = 'Vacation salary'
        elif self.salary_type == 'hchta':
            stype = 'Hchta salary'
        elif self.salary_type == 'maternity':
            stype = 'Maternity salary'

        self.name = _('%s: Salary Slip of %s for %s') % (stype, employee.name, tools.ustr(babel.dates.format_date(date=ttyme, format='MMMM-y', locale=locale)))

        res = {}
        res['value'] = {'worked_days_line_ids': ''}
        if self.contract_id:
            if self.salary_type == 'last_salary':
                # СҮҮЛ ЦАЛИН
                # Гэрээ нь дээр тохируулсан Цалин бодолтын загварыг авах
                self.struct_id = self.contract_id.struct_id

                # Урьдчилгаа Цалинг олох
                advance_salary_id = self._get_advance_salary(self.employee_id.id, self.period_id.id)
                self.advance_salary_id = advance_salary_id

                # Ээлжийн амраллтай эсэхийг олох
                is_advance_vacation = False
                vacation_id = self._get_vacation(self.employee_id.id, self.period_id.id, is_advance_vacation)
                self.vacation_id = vacation_id

                # Өмнөх сарын цалингийн хуудасыг олох
                previous_salary_id = self._get_previous_salary(self.employee_id.id, self.period_id)
                self.previous_salary_id = previous_salary_id

                # Жирэмсэний амралт олох
                maternity_id = self._get_maternity_salary(employee.id, self.period_id.id)
                self.maternity_salary_id = maternity_id

                # ХЧТА олох
                hchta_id = self._get_hchta_id(employee.id, self.period_id.id)
                self.hchta_id = hchta_id

                # Цагийн баланс олох
                hour_balance_line_id = self._get_hour_balance(self.employee_id.id, self.salary_type, self.period_id)
                self.hour_balance_lines = hour_balance_line_id
                #                 hour_balance_line_id = self._get_hour_balance(self.employee_id.id, self.salary_type, self.period_id)
                #                 if hour_balance_line_id:
                #                     hour_balance_line_id.write({'payslip_id':self._origin.id})
                #                     self.hour_balance_lines = hour_balance_line_id.ids
                #   hour_balance_line_id.write({'payslip_id':self.id})
                if self.bonus_salary:
                    # Ээлжийн амралт бодолтын мөр олох
                    input_vacation_lines = self.get_vacation_lines(self.employee_id.id, self.period_id, 12, False)
                    self.vacation_lines = input_vacation_lines
                    self.worked_days_line_ids = []
                    self.input_line_ids = []
            elif self.salary_type == 'advance_salary':
                # УРЬДЧИЛГАА ЦАЛИН
                self.struct_id = self.contract_id.advance_structure_id.id
                self.advance_salary_id = False

                # Цагийн баланс олох
                hour_balance_line_id = self._get_hour_balance(self.employee_id.id, self.salary_type, self.period_id)
                self.hour_balance_lines = hour_balance_line_id
                #                 hour_balance_line_id = self._get_hour_balance(self.employee_id.id, self.salary_type, self.period_id)
                #                 if hour_balance_line_id:
                #                     hour_balance_line_id.write({'payslip_id':self._origin.id})
                #                     self.hour_balance_lines = hour_balance_line_id.ids
                # hour_balance_line_id.write({'payslip_id':self.id})

                # Ээлжийн амраллтай эсэхийг олох
                is_advance_vacation = True
                vacation_id = self._get_vacation(self.employee_id.id, self.period_id.id, is_advance_vacation)
                self.vacation_id = vacation_id

                # Урьдчилгааг гүйцэтгэлээр тооцох эсэх
                is_esti_adv_days = self.get_is_esti_adv_days(self.contract_id)
                self.is_esti_adv_days = is_esti_adv_days

            elif self.salary_type == 'vacation':
                # АМРАЛТЫН ЦАЛИН
                struct = self.env['hr.payroll.structure'].search([('salary_type', '=', self.salary_type)], limit=1)
                if struct:
                    self.struct_id = struct.id

                # Ээлжийн амралт бодолтын мөр олох
                input_vacation_lines = self.get_vacation_lines(self.employee_id.id, self.period_id, 12, True)
                self.vacation_lines = input_vacation_lines
                self.worked_days_line_ids = []
                self.input_line_ids = []

            elif self.salary_type == 'maternity':
                # Жирэмсний амралт
                struct = self.env['hr.payroll.structure'].search([('salary_type', '=', self.salary_type)], limit=1)
                if struct:
                    self.struct = struct.id
                # Жирэмсний амралт бодолтын мөр олох
                input_vacation_lines = self.get_vacation_lines(self.employee_id.id, self.period_id, 13, False)
                self.vacation_lines = input_vacation_lines
                self.worked_days_line_ids = []
                self.input_line_ids = []

            elif self.salary_type == 'hchta':
                struct = self.env['hr.payroll.structure'].search([('salary_type', '=', self.salary_type)], limit=1)
                if struct:
                    self.struct_id = struct.id

                input_vacation_lines = self.get_vacation_lines(self.employee_id.id, self.period_id, 4, False)
                self.vacation_lines = input_vacation_lines
                self.worked_days_line_ids = []
                self.input_line_ids = []
            else:
                self.struct_id = False
        else:
            self.struct_id = False

    @api.multi
    def _get_advance_salary(self, employee_id, period_id):
        '''Урьдчилгаа цалингийн хуудас олох
        '''
        advance_salary_id = self.env['hr.payslip'].search([
            ('employee_id', '=', employee_id),
            ('period_id', '=', period_id),
            ('salary_type', '=', 'advance_salary'),
            ('state', '=', 'done')], limit=1)
        if advance_salary_id:
            return advance_salary_id.id
        else:
            return False

    @api.multi
    def _get_vacation(self, employee_id, period_id, is_advance_vacation):
        '''Ээлжийн амраллтай бол ээжлийн амралт төрөлтэй цалингийн хуудас олох
        '''
        vacation_id = False
        if period_id:
            if is_advance_vacation:
                vacation_id = self.env['hr.payslip'].search([
                    ('employee_id', '=', employee_id),
                    ('period_id', '=', period_id),
                    ('salary_type', '=', 'vacation'),
                    ('is_advance_vacation', '=', is_advance_vacation),
                    ('state', '=', 'done')], limit=1)
            else:
                vacation_id = self.env['hr.payslip'].search([
                    ('employee_id', '=', employee_id),
                    ('period_id', '=', period_id),
                    ('salary_type', '=', 'vacation'),
                    ('state', '=', 'done')], limit=1)
        if vacation_id:
            return vacation_id.id
        else:
            return False

    @api.multi
    def _get_maternity_salary(self, employee_id, period_id):
        '''Жирэмсэний цалингийн хуудас олох
        '''
        maternity_salary_id = self.env['hr.payslip'].search([
            ('employee_id', '=', employee_id),
            ('period_id', '=', period_id),
            ('salary_type', '=', 'maternity'),
            ('state', '=', 'done')], limit=1)
        if maternity_salary_id:
            return maternity_salary_id.id
        else:
            return False

    @api.multi
    def _get_hchta_id(self, employee_id, period_id):
        '''ХЧТА цалингийн хуудас олох
        '''
        hchta_id = self.env['hr.payslip'].search([
            ('employee_id', '=', employee_id),
            ('period_id', '=', period_id),
            ('salary_type', '=', 'hchta'),
            ('state', '=', 'done')], limit=1)
        if hchta_id:
            return hchta_id.id
        else:
            return False

    @api.multi
    def _get_previous_salary(self, employee_id, period):
        '''Өмнөх сарын цалингийн хуудасыг олох
        '''
        previous_period = self._get_previous_month(period)
        if previous_period:
            previous_obj = self.env['hr.payslip'].search([('employee_id', '=', employee_id),
                                                          ('period_id', '=', previous_period['period_id']),
                                                          ('state', '=', 'done')], limit=1)
            if previous_obj:
                return previous_obj.id
        return False

    @api.multi
    def _get_previous_month(self, start_period):
        '''өмнөх мөчлөгийг тооцож олох
        '''
        ret_date = False
        ret_id = False
        date_start = start_period.date_start
        if date_start:
            lastMonth = datetime.strptime(date_start, '%Y-%m-%d') - timedelta(days=1)
            prev_period = self.env['account.period'].search([('date_stop', '=', lastMonth), ('company_id', '=', self.env.user.company_id.id)], limit=1)
            if prev_period:
                ret_id = prev_period.id
                ret_date = prev_period.date_start
        res = {'period_id': ret_id, 'date_start': ret_date}
        return res

    @api.multi
    def _get_hour_balance(self, employee_id, salary_type, period):
        '''Цагийн баланс олох
        '''
        hour_balance_ids = False
        hour_balance = self.env['hour.balance'].search([
            ('company_id', '=', self.env.user.company_id.id),
            ('salary_type', '=', salary_type),
            ('date_from', '=', period.date_start),
            ('date_to', '<=', period.date_stop),
            ('date_to', '>=', period.date_start),
            ('state', '=', 'done')])
        if hour_balance:
            hour_balance_ids = self.env['hour.balance.line'].search([
                ('employee_id', '=', employee_id),
                ('salary_type', '=', salary_type),
                ('balance_id', 'in', hour_balance.ids)], limit=1)
            if hour_balance_ids:
                hour_balance_ids = hour_balance_ids.ids
            return hour_balance_ids

    @api.multi
    def get_vacation_lines(self, employee_id, period, count, is_limit):
        res = []
        payslip_vacation_obj = self.env['hr.payslip.vacation']
        x = 0
        get_period_id = period
        i = 1
        temp = {}
        while (x < count):
            temp = self._get_previous_month(get_period_id)
            get_period_id = self.env['account.period'].browse(temp['period_id'])
            pv_obj = payslip_vacation_obj.search([('employee_id', '=', employee_id), ('period_id', '=', get_period_id.id)])
            inputs = {}
            config = self.env['hr.payroll.config'].search([('company_id', '=', self.env.user.company_id.id)], limit=1)
            for pv_line in pv_obj:
                inputs = {
                    'period_id': pv_line.period_id.id,
                    'amount': pv_line.amount - pv_line.additional_food_vehicle,
                    'is_vacation': pv_line.is_vacation,
                    'worked_day': pv_line.worked_day,
                }
                if self.salary_type in ('maternity', 'hchta'):
                    if pv_line.period_id.date_start < '2016-12-2':
                        inputs['amount'] = pv_line.amount
                        if inputs['amount'] > 1920000:
                            inputs['amount'] = 1920000
                    else:
                        inputs['amount'] = pv_line.amount
                        if inputs['amount'] > config.minimum_salary * 10:
                            inputs['amount'] = config.minimum_salary * 10
                    start_date = datetime.strptime(pv_line.period_id.date_start, '%Y-%m-%d')
                    end_date = datetime.strptime(pv_line.period_id.date_stop, '%Y-%m-%d')
                    day = 0
                    while start_date <= end_date:
                        if start_date.weekday() not in (5, 6):
                            day += 1
                        start_date += timedelta(days=1)
                    inputs['worked_day'] = day
                x += 1
            i += 1
            if is_limit:
                if 'is_vacation' in inputs:
                    if inputs['is_vacation']:
                        break
            if i > 30:
                break
            if inputs:
                res += [inputs]
        return res

    @api.multi
    def get_is_esti_adv_days(self, contract):
        if contract:
            return contract.is_esti_adv_days
        else:
            return False

    @api.model
    def get_payslip_lines(self, contract_ids, payslip_id):
        '''Тооцоолох товч дарах үед цалингийн тооцоололыг бодно
        '''

        def _sum_salary_rule_category(localdict, category, amount):
            if category.parent_id:
                localdict = _sum_salary_rule_category(localdict, category.parent_id, amount)
            if category.code in localdict['categories'].dict:
                amount += localdict['categories'].dict[category.code]
            localdict['categories'].dict[category.code] = amount
            return localdict

        class BrowsableObject(object):
            def __init__(self, employee_id, dict, env):  # @ReservedAssignment
                self.employee_id = employee_id
                self.dict = dict
                self.env = env

            def __getattr__(self, attr):
                return attr in self.dict and self.dict.__getitem__(attr) or 0.0

        class InputLine(BrowsableObject):
            """a class that will be used into the python code, mainly for usability purposes"""

            def sum(self, code, from_date, to_date=None):
                if to_date is None:
                    to_date = fields.Date.today()
                self.env.cr.execute("""
                    SELECT sum(amount) as sum
                    FROM hr_payslip as hp, hr_payslip_input as pi
                    WHERE hp.employee_id = %s AND hp.state = 'done'
                    AND hp.date_from >= %s AND hp.date_to <= %s AND hp.id = pi.payslip_id AND pi.code = %s""",
                                    (self.employee_id, from_date, to_date, code))
                return self.env.cr.fetchone()[0] or 0.0

        class WorkedDays(BrowsableObject):
            """a class that will be used into the python code, mainly for usability purposes"""

            def _sum(self, code, from_date, to_date=None):
                if to_date is None:
                    to_date = fields.Date.today()
                self.env.cr.execute("""
                    SELECT sum(number_of_days) as number_of_days, sum(number_of_hours) as number_of_hours
                    FROM hr_payslip as hp, hr_payslip_worked_days as pi
                    WHERE hp.employee_id = %s AND hp.state = 'done'
                    AND hp.date_from >= %s AND hp.date_to <= %s AND hp.id = pi.payslip_id AND pi.code = %s""",
                                    (self.employee_id, from_date, to_date, code))
                return self.env.cr.fetchone()

            def sum(self, code, from_date, to_date=None):
                res = self._sum(code, from_date, to_date)
                return res and res[0] or 0.0

            def sum_hours(self, code, from_date, to_date=None):
                res = self._sum(code, from_date, to_date)
                return res and res[1] or 0.0

        class Payslips(BrowsableObject):
            """a class that will be used into the python code, mainly for usability purposes"""

            def sum(self, code, from_date, to_date=None):
                if to_date is None:
                    to_date = fields.Date.today()
                self.env.cr.execute("""SELECT sum(case when hp.credit_note = False then (pl.total) else (-pl.total) end)
                            FROM hr_payslip as hp, hr_payslip_line as pl
                            WHERE hp.employee_id = %s AND hp.state = 'done'
                            AND hp.date_from >= %s AND hp.date_to <= %s AND hp.id = pl.slip_id AND pl.code = %s""",
                                    (self.employee_id, from_date, to_date, code))
                res = self.env.cr.fetchone()
                return res and res[0] or 0.0

        # we keep a dict with the result because a value can be overwritten by another rule with the same code
        result_dict = {}
        rules = {}
        blacklist = []
        payslip_obj = self.env['hr.payslip']
        payslip = payslip_obj.browse(payslip_id)
        worked_days = {}
        for worked_days_line in payslip.worked_days_line_ids:
            worked_days[worked_days_line.code] = worked_days_line
        inputs = {}
        for input_line in payslip.input_line_ids:
            inputs[input_line.code] = input_line

        categories_obj = BrowsableObject(payslip.employee_id.id, {}, self.env)

        input_obj = InputLine(payslip.employee_id.id, inputs, self.env)
        worked_days_obj = WorkedDays(payslip.employee_id.id, worked_days, self.env)
        payslip_obj = Payslips(payslip.employee_id.id, payslip, self.env)
        rules_obj = BrowsableObject(payslip.employee_id.id, rules, self.env)

        baselocaldict = {'categories': categories_obj, 'rules': rules_obj, 'payslip': payslip_obj, 'worked_days': worked_days_obj, 'inputs': input_obj}
        contracts = self.env['hr.contract'].browse(contract_ids)
        structure_ids = contracts.get_all_structures()

        # get the ids of the structures on the contracts and their parent id as well
        # ------start--------  ENE HOOROND STRUCTURE CONTRACTAS STRUCTURE SONGODOG BAISNIIG BOLIULJ PAYSLIP DEEREES SONGODOG BOLGOSON by Javhaa ---
        if payslip.struct_id:
            structure_ids = payslip.struct_id.id
        else:
            raise UserError((u'Цалин бодолтын загвар сонгогдоогүй байна!'))
        # get the rules of the structure and thier children
        rule_ids = self.env['hr.payroll.structure'].browse(structure_ids).get_all_rules()
        # ------end--------  ENE HOOROND STRUCTURE CONTRACTAS STRUCTURE SONGODOG BAISNIIG BOLIULJ PAYSLIP DEEREES SONGODOG BOLGOSON by Javhaa ---

        # run the rules by sequence
        sorted_rule_ids = [id for id, sequence in sorted(rule_ids, key=lambda x: x[1])]  # @ReservedAssignment @UnusedVariable
        sorted_rules = self.env['hr.salary.rule'].browse(sorted_rule_ids)

        for contract in contracts:
            employee = contract.employee_id
            localdict = dict(baselocaldict, employee=employee, contract=contract)
            for rule in sorted_rules:
                key = rule.code + '-' + str(contract.id)
                localdict['result'] = None
                localdict['result_qty'] = 1.0
                localdict['result_rate'] = 100
                # check if the rule can be applied
                if rule.satisfy_condition(localdict) and rule.id not in blacklist:
                    # compute the amount of the rule
                    amount, qty, rate = rule.compute_rule(localdict)
                    # check if there is already a rule computed with that code
                    previous_amount = rule.code in localdict and localdict[rule.code] or 0.0
                    # set/overwrite the amount computed for this rule in the localdict
                    tot_rule = amount * qty * rate / 100.0
                    localdict[rule.code] = tot_rule
                    rules[rule.code] = rule
                    # sum the amount for its salary category
                    localdict = _sum_salary_rule_category(localdict, rule.category_id, tot_rule - previous_amount)
                    # create/overwrite the rule in the temporary results
                    result_dict[key] = {
                        'salary_rule_id': rule.id,
                        'contract_id': contract.id,
                        'name': rule.name,
                        'code': rule.code,
                        'category_id': rule.category_id.id,
                        'sequence': rule.sequence,
                        'appears_on_payslip': rule.appears_on_payslip,
                        'condition_select': rule.condition_select,
                        'condition_python': rule.condition_python,
                        'condition_range': rule.condition_range,
                        'condition_range_min': rule.condition_range_min,
                        'condition_range_max': rule.condition_range_max,
                        'amount_select': rule.amount_select,
                        'amount_fix': rule.amount_fix,
                        'amount_python_compute': rule.amount_python_compute,
                        'amount_percentage': rule.amount_percentage,
                        'amount_percentage_base': rule.amount_percentage_base,
                        'register_id': rule.register_id.id,
                        'amount': amount,
                        'employee_id': contract.employee_id.id,
                        'quantity': qty,
                        'rate': rate,
                    }
                else:
                    blacklist += [id for id, seq in rule._recursive_search_of_rules()]  # @ReservedAssignment @UnusedVariable
        return [value for code, value in result_dict.items()]  # @UnusedVariable

    @api.multi
    def send_email(self):
        for slip in self:
            if slip.salary_type == 'advance_salary' or slip.salary_type == 'last_salary':
                if slip.employee_id.work_email:
                    if slip.salary_type == 'advance_salary':
                        stype = u'урьдчилгаа'
                    else:
                        stype = u'сүүл'
                    outgoing_email_ids = self.env['ir.mail_server'].search([])
                    if not outgoing_email_ids:
                        raise exceptions.ValidationError(_('There is no configuration for outgoing mail server. Please contact system administrator.'))
                    else:
                        outgoing_email = outgoing_email_ids[0]
                        you = slip.employee_id.work_email
                        email_ids = []
                        salary_header = u'<table style="width:400px;" border="1"><tr><th align="left">Огноо</th><td align="right">%s-сар</td></tr><tr><th align="left">Ажилтаны код</th><td align="right">%s</td></tr><tr><th align="left">Нэр</th><td align="right">%s</td></tr><tr><th align="left">Овог</th><td align="right">%s</td></tr></table></br>' % (
                            slip.period_id.name, slip.employee_id.identification_id, slip.employee_id.name_related, slip.employee_id.last_name)
                        salary_body = u'<table style="width:400px;" border="1">'
                        for line in slip.line_ids:
                            if line.salary_rule_id.is_email:
                                salary_body += u'<tr><th align="left">' + u'%s' % line.salary_rule_id.name + \
                                               u'</th><td align="right">' + \
                                               u'%s' % line.amount + u'</td></tr>'
                        salary_body += u'</table>'
                        vals = {
                            'state': 'outgoing',
                            'subject': u'Танд %s Сарын %s цалингийн мэдээлэл ирлээ.' % (slip.period_id.name, stype),
                            'body_html': u'<p>Сайн байна уу </br></p><p>Танд %s Сарын %s цалингийн мэдээлэл ирлээ.</p> %s <br> %s <br> <p> Баярлалаа,</p><pre>--<br>ERP Автомат Имэйл </pre>' % (slip.period_id.name, stype, salary_header, salary_body),
                            'email_to': you,
                            'email_from': outgoing_email.smtp_user,
                        }
                        email_ids.append(self.env['mail.mail'].create(vals))
                        if email_ids != []:
                            self.env['mail.mail'].send(email_ids)
        return True


class PaySlipRunWorkflowHistory(models.Model):
    _name = 'hr.payslip.run.workflow.history'
    _order = 'payslip_id, sent_date'

    STATE_SELECTION = [('verify', 'Waiting'),
                       ('approved', 'Approved'),
                       ('done', 'Done'),
                       ('cancel', 'Rejected'),
                       ('rejected', 'Returned')]
    payslip_id = fields.Many2one('hr.payslip.run', 'Payslip Run', readonly=True, ondelete='cascade')
    line_sequence = fields.Integer('Workflow Step')
    name = fields.Char('Verification Step', readonly=True)
    user_ids = fields.Many2many('res.users', 'res_users_payslip_run_workflow_history_ref', 'history_id', 'user_id', 'Validators')
    sent_date = fields.Datetime('Sent date', required=True, readonly=True)
    user_id = fields.Many2one('res.users', 'Validator', readonly=True)
    action_date = fields.Datetime('Action date', readonly=True)
    action = fields.Selection(STATE_SELECTION, 'Action', readonly=True)

# process_sheet


class HrPayslipRun(models.Model):
    _inherit = ['hr.payslip.run', 'mail.thread']
    _name = 'hr.payslip.run'
    _order = 'create_date DESC'

    @api.model
    def default_get(self, fields):
        period_id = False
        employee_id = False
        now = time.strftime('%Y-%m-%d')
        res = super(HrPayslipRun, self).default_get(fields)

        company_id = self.env.user.company_id.id
        if company_id:
            period = self.env['account.period'].search([('date_start', '<=', now), ('date_stop', '>=', now), ('company_id', '=', company_id)], limit=1)
            if period:
                period_id = period.id
        emp_objs = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)])
        for emp in emp_objs:
            employee_id = emp.id
        res.update({'salary_type': 'last_salary',
                    'state': 'draft',
                    'compute_date': now,
                    'period_id': period_id,
                    'company_id': company_id,
                    'create_employee': employee_id,
                    })
        return res

    @api.multi
    def _total_amount(self):
        for slip_run in self:
            total = 0.0
            for slip in slip_run.slip_ids:
                for line in slip.line_ids:
                    if line.code == 'TOTALGROSS':
                        total += line.amount
            slip_run.total_amount = total

    @api.multi
    def _total_amount_vacation(self):
        for slip_run in self:
            total = 0.0
            for slip in slip_run.slip_ids:
                for line in slip.line_ids:
                    if line.code == 'VACATION_GROSS':
                        total += line.amount
            slip_run.total_amount_vacation = total

    @api.multi
    def _compute_unreconciled_amount(self):
        ResPartner = self.env['res.partner']
        for record in self:
            amount = 0
            partner_ids = []
            if record.move_id:
                for line in record.move_id.line_ids.filtered(lambda l: l.partner_id and l.account_id.user_type_id.type == 'payable' and l.move_id.state == 'posted'):
                    partner_ids.append(line.partner_id.id)
                for partner_id in set(partner_ids):
                    amount += ResPartner.browse(partner_id).debit
            record.unreconciled_amount = amount

    @api.multi
    def _get_period(self):
        company_id = False
        period_id = False
        now = time.strftime('%Y-%m-%d')
        company_id = self.env.user.company_id.id
        if company_id:
            period = self.env['account.period'].search([('date_start', '<=', now), ('date_stop', '>=', now), ('company_id', '=', company_id)], limit=1)
            if period:
                period_id = period.id
        return period_id

    @api.depends('check_sequence')
    def _compute_is_validator(self):
        for rec in self:
            history_obj = self.env['hr.payslip.run.workflow.history']
            validators = history_obj.search([('payslip_id', '=', rec.id), ('line_sequence', '=', rec.check_sequence)], limit=1, order='sent_date DESC').user_ids
            if self.env.user in validators:
                rec.is_validator = True
            else:
                rec.is_validator = False

    @api.multi
    def _compute_is_creator(self):
        for rec in self:
            if rec.create_uid == self.env.user:
                rec.is_creator = True
            else:
                rec.is_creator = False

    salary_type = fields.Selection([('last_salary', 'Last salary'),
                                    ('advance_salary', 'Advance salary'),
                                    ('vacation', 'Vacation'),
                                    ('maternity', 'Maternity leave'),
                                    ('hchta', 'HCHTA Leave')], string='Salary type', states={'draft': [('readonly', False)]}, track_visibility='onchange')
    state = fields.Selection([('draft', 'Draft'),
                              ('verify', 'Waiting'),
                              ('done', 'Done'),
                              ('cancel', 'Rejected'), ], string='Status', index=True, readonly=True, states={'draft': [('readonly', False)]}, track_visibility='onchange')
    period_id = fields.Many2one('account.period', string='Period', index=True, invisible=False, required=True, domain=[('state', 'not in', ['done'])], default=_get_period, track_visibility='onchange')
    number = fields.Char(string='Reference', size=64, required=False, readonly=True, states={'draft': [('readonly', False)]})
    move_id = fields.Many2one('account.move', string='Account Move', readonly=True)
    create_employee = fields.Many2one('hr.employee', string='Accountant', track_visibility='onchange')
    compute_date = fields.Date(string='Salary compute date', required=True, default=fields.Date.context_today, track_visibility='onchange')
    total_amount = fields.Float(string='Total Amount', compute='_total_amount')
    total_amount_vacation = fields.Float(string='Total Amount Vacation', compute='_total_amount_vacation')
    department_id = fields.Many2one('hr.department', string='Department', track_visibility='onchange')
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id, track_visibility='onchange')
    bonus_salary = fields.Boolean(string='Bonus salary')
    journal_id = fields.Many2one(domain=[('payroll', '=', True)], track_visibility='onchange')
    unreconciled_amount = fields.Float('Unreconciled amount', compute='_compute_unreconciled_amount')
    slip_sum_ids = fields.One2many('hr.payslip.sum', 'payslip_run_id', string='Payslips Sum', readonly=True)
    name = fields.Char(required=True, index=True, states={'draft': [('readonly', False)]}, track_visibility='onchange')
    workflow_id = fields.Many2one('workflow.config', 'Workflow', copy=False)
    history_line_ids = fields.One2many('hr.payslip.run.workflow.history', 'payslip_id', 'Workflow History', copy=False)
    check_sequence = fields.Integer('Workflow Step', default=0, copy=False)
    is_validator = fields.Boolean(compute='_compute_is_validator')
    is_creator = fields.Boolean(compute='_compute_is_creator')

    @api.model
    def create(self, vals):
        creation = super(HrPayslipRun, self).create(vals)
        department = self.env['hr.department'].search([('id', '=', vals['department_id'])])
        workflow_id = self.env['workflow.config'].get_workflow('department', 'hr.payslip.run', vals['create_employee'], department.id)
        """    Ажлын урсгалыг ажлын урсгалын бүртгэлээс хайгаад олдохгүй бол компанид тохируулсан ажлын урсгалыг авна.
        """
        workflow = ''
        if workflow_id:
            workflow = workflow_id
        elif creation.env.user.company_id.workflow:
            workflow = self.env.user.company_id.workflow.id
        if not workflow:
            raise exceptions.Warning(_('There is no workflow defined!'))
        creation.workflow_id = workflow
        return creation

    @api.onchange('department_id')
    def onchange_department_id(self):
        if self.department_id:
            workflow_id = self.env['workflow.config'].get_workflow('department', 'hr.payslip.run', self.create_employee.id, self.department_id.id)
            """    Ажлын урсгалыг ажлын урсгалын бүртгэлээс хайгаад олдохгүй бол компанид тохируулсан ажлын урсгалыг авна.
            """
            workflow = ''
            if workflow_id:
                workflow = workflow_id
            elif self.company_id.workflow:
                workflow = self.company_id.workflow.id
            if not workflow:
                raise exceptions.Warning(_('There is no workflow defined!'))
            self.workflow_id = workflow

    @api.multi
    def send_payslip_run(self):
        if self.workflow_id:
            success, current_sequence = self.env['workflow.config'].send('hr.payslip.run.workflow.history',
                                                                         'payslip_id', self, self.create_uid.id)
            if success:
                self.check_sequence = current_sequence
                for run in self:
                    for slip in run.slip_ids:
                        slip.write({'state': 'verify'})
                    run.state = 'verify'

    @api.onchange('department_id', 'salary_type')
    def set_payroll_name(self):
        for obj in self:
            name, name_change = '', False
            if obj.department_id and obj.department_id.name:
                name += _("%s department's ") % obj.department_id.name
                name_change = True
            if obj.period_id and obj.period_id.name:
                name += _("%s month's ") % obj.period_id.name
                name_change = True
            if obj.salary_type:
                name += _("%s's ") % dict(obj._fields['salary_type']._description_selection(obj.env)).get(obj.salary_type)
                name_change = True
            if name_change:
                name += _("salary slip run")
                obj.name = name

    @api.onchange('period_id')
    def onchange_period(self):
        if self.period_id:
            self.date_start = self.period_id.date_start
            self.date_end = self.period_id.date_stop
            self.set_payroll_name()

    @api.one
    @api.constrains('compute_date', 'period_id')
    def _check_compute_date(self):
        if self.period_id.date_start > self.compute_date or self.compute_date > self.period_id.date_stop:
            raise ValidationError(_('Compute date must be in period'))

    @api.onchange('compute_date')
    def onchange_compute_date(self):
        date = self.compute_date
        if self.period_id.date_start > self.compute_date or self.compute_date > self.period_id.date_stop:
            period = self.env['account.period'].search([('date_start', '<=', date), ('date_stop', '>=', date)])
            if period:
                self.period_id = period.id

    @api.multi
    def cancel_payslip_run(self):
        '''Цалингийн хуудасны багцыг цуцлах
        '''
        for run in self:
            if run.move_id:
                raise UserError((u'Та холбоотой ажил гүйлгээ байгаа тул цалингийн хуудсын багцыг цуцлах боломжгүй байна!'))
            if run.workflow_id:
                success = self.env['workflow.config'].reject('hr.payslip.run.workflow.history', 'payslip_id', self, self.env.user.id)
                if success:
                    analytic_lines = self.env['account.analytic.line'].search([('payslip_run_id', '=', run.id)])
                    if analytic_lines:
                        for analytic_line in analytic_lines:
                            analytic_line.unlink()
                    for slip in run.slip_ids:
                        if slip.salary_type == 'last_salary':
                            vacations = self.env['hr.payslip.vacation'].search([('employee_id', '=', slip.employee_id.id),
                                                                                ('period_id', '=', slip.period_id.id)])
                            if vacations:
                                for vacation in vacations:
                                    vacation.unlink()
                            for pro_input in slip.input_line_ids:
                                if pro_input.raise_id:
                                    pro_input.write({'is_count': False})
                                if pro_input.deduction_id:
                                    pro_input.write({'is_count': False})
                                if pro_input.rebate_id:
                                    pro_input.write({'is_count': False})
                        slip.write({'paid': False, 'state': 'cancel'})
                    run.write({'state': 'cancel'})

    @api.multi
    def previous(self):
        if self.workflow_id:
            success, current_sequence = self.env['workflow.config'].action_return('hr.payslip.run.workflow.history',
                                                                                  'payslip_id', self, self.env.user.id)
            if success:
                self.check_sequence = current_sequence

    @api.multi
    def draft_payslip_run(self):
        self.ensure_one()
        self.check_sequence = 0
        for run in self:
            for slip in run.slip_ids:
                slip.write({'state': 'draft'})
        return self.write({'state': 'draft'})

    @api.multi
    def unlink(self):
        for payslip in self:
            if payslip.state not in ['draft']:
                raise UserError(_('You cannot delete a payslip run which is not draft or cancelled!'))
            for line in payslip.slip_ids:
                line.unlink()
        return super(HrPayslipRun, self).unlink()

    @api.multi
    def approve_payslip_run(self):
        '''Цалингийн хуудасны багцыг Нягтлан бодогч батлах
        '''
        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve('hr.payslip.run.workflow.history', 'payslip_id', self,
                                                                                         self.env.user.id)
            if success:
                if sub_success:
                    move_id = False
                    move = False
                    if self.salary_type == 'last_salary':
                        # Сүүл цалин төрөлтэй дараахи функцыг дуудна
                        move = self.approve_last_salary()
                    elif self.salary_type == 'advance_salary':
                        # Урьдчилгаа цалинг тооцохын тулд дараахи функцыг дуудна
                        move = self.approve_advance_salary()
                    elif self.salary_type == 'maternity':
                        # Жирэмсний амралт
                        move = self.approve_maternity_salary()
                    elif self.salary_type == 'hchta':
                        # ХЧТА тэтгэмж
                        move = self.approve_hchta()
                    else:
                        for slip in self.slip_ids:
                            slip.write({'paid': True, 'state': 'done'})
                    if move:
                        move_id = move.id
                    self.write({'state': 'done', 'move_id': move_id})
                else:
                    self.check_sequence = current_sequence

    def _prepare_analytic_account(self, qry_result):
        self.ensure_one()
        return qry_result.get('analytic_account', False)

    def _get_advance_last_slry_credit_qry_with_acc_not_in(self, acc_ids, create_move_in_advance=False):
        # УРЬДЧИЛГАА/СҮҮЛ төрөлтэй цалингийн КТ талын бичилтийн өгөгдлийг авах query
        # Цалингийн бодолтын мөрүүдийг Кредит талдаа дансаар нь бүлэглэж үүсгэнэ, Цалин өглөг, ХХОАТ өглөг, НДШ өглөг гэх мэт
        self.ensure_one()
        advance_where = " AND sr.create_move_in_advance = TRUE" if create_move_in_advance else ""
        return '''
                SELECT
                    sr.account_credit AS account,
                    SUM(pl.total) AS total,
                    MIN(sr.transaction_description) AS dscrtpn
                FROM
                    hr_payslip_line pl,
                    hr_salary_rule sr
                WHERE
                    pl.slip_id in (
                        SELECT
                            id
                        FROM
                            hr_payslip
                        WHERE
                            payslip_run_id = %s)
                    AND pl.salary_rule_id = sr.id
                    AND pl.total <> 0
                    AND sr.account_credit <> 0
                    AND sr.account_credit NOT IN %s %s
                GROUP BY
                    sr.account_credit
                ORDER BY
                    sr.account_credit''' % (self.id, acc_ids, advance_where)

    def _get_advance_last_slry_credit_qry_with_acc_in(self, acc_ids, create_move_in_advance=False):
        # УРЬДЧИЛГАА/СҮҮЛ төрөлтэй цалингийн КТ талын бичилтийн өгөгдлийг авах query
        # Цалингийн бодолтын мөрүүдийг Кредит талдаа тусдаа тусдаа бичилт үүсгэнэ, Ажилчдаас авах авлага гэх мэт
        self.ensure_one()
        advance_where = " AND sr.create_move_in_advance = TRUE" if create_move_in_advance else ""

        return '''
                SELECT
                    ps.employee_id AS emp,
                    sr.account_credit AS account,
                    pl.total AS total,
                    sr.transaction_description AS dscrtpn,
                    sr.id as hsr_id
                FROM
                    hr_payslip_line pl,
                    hr_salary_rule sr,
                    hr_payslip ps
                WHERE
                    pl.salary_rule_id = sr.id
                    AND pl.total <> 0
                    AND sr.account_credit <> 0
                    AND ps.payslip_run_id = %s
                    AND sr.account_credit IN %s
                    AND pl.slip_id = ps.id %s
                ORDER BY
                    ps.employee_id ASC,
                    sr.account_credit ASC''' % (self.id, acc_ids, advance_where)

    def _get_advance_last_slry_debit_qry_with_acc_not_in_without_expense(self, acc_ids):
        # УРЬДЧИЛГАА/СҮҮЛ төрөлтэй цалингийн ДТ талын бичилтийн өгөгдлийг авах query
        # Цалингийн бодолтын мөрүүдийг Дебит талдаа дансаар нь бүлэглэж үүсгэнэ, Цалин өглөг, Цалин зардал, НДШ зардал гэх мэт
        self.ensure_one()
        return '''
                SELECT sr.account_debit AS account, SUM(pl.total) AS total, aa.name, MIN(sr.transaction_description) AS dscrtpn
                FROM hr_payslip_line pl, hr_salary_rule sr, account_account aa, account_account_type at
                WHERE pl.slip_id in (SELECT id FROM hr_payslip WHERE payslip_run_id = %s) AND aa.id = sr.account_debit AND aa.user_type_id = at.id AND at.type <> 'expense'
                AND pl.salary_rule_id = sr.id AND pl.total <> 0 AND sr.account_debit <> 0 AND sr.account_debit NOT IN %s
                GROUP BY sr.account_debit, aa.name
                ORDER BY sr.account_debit''' % (self.id, acc_ids)

    def _get_advance_last_slry_debit_qry_with_acc_not_in(self, acc_ids, create_move_in_advance=False):
        # УРЬДЧИЛГАА/СҮҮЛ төрөлтэй цалингийн ДТ талын бичилтийн өгөгдлийг авах query
        self.ensure_one()
        advance_where = " AND sr.create_move_in_advance = TRUE" if create_move_in_advance else ""

        return '''
                SELECT
                    sr.account_debit AS account,
                    SUM(pl.total) AS total,
                    MIN(sr.transaction_description) AS dscrtpn
                FROM
                    hr_payslip_line pl,
                    hr_salary_rule sr
                WHERE
                    pl.slip_id in (
                        SELECT
                            id
                        FROM
                            hr_payslip
                        WHERE
                            payslip_run_id = %s)
                    AND pl.salary_rule_id = sr.id
                    AND pl.total <> 0
                    AND sr.account_debit <> 0
                    AND sr.account_debit NOT IN %s %s
                GROUP BY sr.account_debit
                ORDER BY sr.account_debit''' % (self.id, acc_ids, advance_where)

    def _get_advance_last_slry_debit_qry_with_acc_in(self, acc_ids, create_move_in_advance=False):
        # УРЬДЧИЛГАА/СҮҮЛ төрөлтэй цалингийн КТ талын бичилтийн өгөгдлийг авах query
        # Цалингийн бодолтын мөрүүдийг Дебит талдаа тусдаа тусдаа бичилт үүсгэнэ,
        self.ensure_one()
        advance_where = " AND sr.create_move_in_advance = TRUE" if create_move_in_advance else ""

        return '''
                SELECT
                    ps.employee_id AS emp,
                    sr.account_debit AS account,
                    pl.total AS total,
                    sr.transaction_description AS dscrtpn,
                    sr.id as hsr_id
                FROM
                    hr_payslip_line pl,
                    hr_salary_rule sr,
                    hr_payslip ps
                WHERE
                    pl.salary_rule_id = sr.id
                    AND pl.total <> 0
                    AND sr.account_debit <> 0
                    AND ps.payslip_run_id = %s
                    AND sr.account_debit IN %s
                    AND pl.slip_id = ps.id %s
                ORDER BY
                    ps.employee_id ASC,
                    sr.account_debit ASC''' % (self.id, acc_ids, advance_where)

    def _get_last_slry_debit_qry_by_each_emp(self, acc_ids):
        # СҮҮЛ цалингийн 'expense' төрөлтэй данс бүхий ДТ талын бичилтүүдэд шинжилгээний данс олгохын тулд өгөгдлийг дараахаар бэлдэнэ.
        self.ensure_one()

        return '''
                SELECT he.id AS emp, sr.account_debit AS account, SUM(pl.total) AS total, pl.id, MIN(sr.transaction_description) AS dscrtpn
                FROM hr_payslip_line pl, hr_salary_rule sr, account_account aa, account_account_type at, hr_payslip ps, hr_employee he
                WHERE pl.slip_id IN (SELECT id FROM hr_payslip WHERE payslip_run_id = %s) AND aa.id = sr.account_debit AND aa.user_type_id = at.id
                AND at.type = 'expense' AND pl.salary_rule_id = sr.id AND pl.total <> 0 AND sr.account_debit <> 0
                AND sr.account_debit NOT IN %s AND pl.slip_id = ps.id AND he.id = ps.employee_id
                GROUP BY pl.id, sr.account_debit, he.id
        ''' % (self.id, acc_ids)

    def _get_maternity_hchta_slry_credit_qry(self):
        # ЖАТ/ХЧТАТ төрөлтэй цалингийн КТ талын бичилтийн өгөгдлийг авах query
        # Цалингийн бодолтын мөрүүдийг Кредит талдаа тусдаа тусдаа бичилт үүсгэнэ, Ажилчдаас авах авлага гэх мэт
        self.ensure_one()
        return '''
                SELECT ps.employee_id AS emp, sr.account_credit AS account, SUM(pl.total) AS total, MIN(sr.transaction_description) AS dscrtpn
                FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip ps
                WHERE pl.salary_rule_id = sr.id AND pl.total <> 0 AND sr.account_credit <> 0 AND ps.payslip_run_id = %s AND pl.slip_id = ps.id
                GROUP BY ps.employee_id, sr.account_credit
                ORDER BY ps.employee_id ASC, sr.account_credit ASC''' % (self.id)

    def _get_maternity_slry_debit_qry_without_expense(self):
        # ЖАТ/ХЧТАТ төрөлтэй цалингийн ДТ талын бичилтийн өгөгдлийг авах query
        # Цалингийн бодолтын мөрүүдийг Дебит талдаа дансаар нь бүлэглэж үүсгэнэ, Цалин өглөг, Цалин зардал, НДШ зардал гэх мэт
        self.ensure_one()
        return '''
                SELECT sr.account_debit AS account, SUM(pl.total) AS total, MIN(sr.transaction_description) AS dscrtpn
                FROM hr_payslip_line pl, hr_salary_rule sr, account_account aa, account_account_type at
                WHERE pl.slip_id in (SELECT id FROM hr_payslip WHERE payslip_run_id = %s) AND aa.id = sr.account_debit AND aa.user_type_id = at.id AND at.type <> 'expense'
                AND pl.salary_rule_id = sr.id AND pl.total <> 0 AND sr.account_debit <> 0
                GROUP BY sr.account_debit
                ORDER BY sr.account_debit''' % (self.id)

    def _get_hchta_slry_credit_qry(self, acc_ids):
        # ХЧТАТ төрөлтэй цалингийн КТ талын бичилтийн өгөгдлийг авах query
        self.ensure_one()
        return '''
                SELECT ps.employee_id AS emp, sr.account_credit AS account, SUM(pl.total) AS total, MIN(sr.transaction_description) AS dscrtpn
                FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip ps
                WHERE pl.salary_rule_id = sr.id AND pl.total <> 0 AND sr.account_credit <> 0 AND ps.payslip_run_id = %s AND sr.account_credit in %s AND pl.slip_id = ps.id
                GROUP BY ps.employee_id, sr.account_credit
                ORDER BY ps.employee_id ASC, sr.account_credit ASC''' % (self.id, acc_ids)

    def _get_hchta_slry_debit_qry(self, acc_ids):
        # ХЧТАТ төрөлтэй цалингийн ДТ талын бичилтийн өгөгдлийг авах query
        self.ensure_one()
        return '''
                SELECT sr.account_debit AS account, SUM(pl.total) AS total, MIN(sr.transaction_description) AS dscrtpn
                FROM hr_payslip_line pl, hr_salary_rule sr
                WHERE pl.slip_id in (SELECT id FROM hr_payslip WHERE payslip_run_id = %s) AND sr.account_debit NOT IN %s
                AND pl.salary_rule_id = sr.id AND pl.total <> 0 AND sr.account_debit <> 0
                GROUP BY sr.account_debit
                ORDER BY sr.account_debit''' % (self.id, acc_ids)

    def _get_hchta_slry_debit_qry_without_expense(self, acc_ids):
        # ХЧТАТ төрөлтэй цалингийн ДТ талын бичилтийн өгөгдлийг авах query
        # Цалингийн бодолтын мөрүүдийг Дебит талдаа дансаар нь бүлэглэж үүсгэнэ, Цалин өглөг, Цалин зардал, НДШ зардал гэх мэт
        self.ensure_one()
        return '''
                SELECT sr.account_debit AS account, SUM(pl.total) AS total, MIN(sr.transaction_description) AS dscrtpn
                FROM hr_payslip_line pl, hr_salary_rule sr, account_account aa, account_account_type at
                WHERE pl.slip_id in (SELECT id FROM hr_payslip WHERE payslip_run_id = %s) AND aa.id = sr.account_debit AND aa.user_type_id = at.id AND at.type <> 'expense'
                AND pl.salary_rule_id = sr.id AND pl.total <> 0 AND sr.account_debit <> 0 AND sr.account_debit NOT IN %s
                GROUP BY sr.account_debit
                ORDER BY sr.account_debit''' % (self.id, acc_ids)

    @api.multi
    def approve_last_salary(self):
        '''Сүүл цалин төрөлтэй багцыг нягтлан бодогч батлах үед
        '''
        cr = self.env.cr
        move_pool = self.env['account.move']
        config_pool = self.env['hr.payroll.config']
        employee_pool = self.env['hr.employee']
        move_id = False
        for run in self:
            line_ids = []
            is_expense_by_analytic = False
            name = _('Payslip Run of %s') % (run.name)
            move = {
                'narration': name,
                'date': run.compute_date,
                'ref': run.number,
                'journal_id': run.journal_id.id,
                'is_salary': True,
            }
            acc_ids = []
            parts = {}
            conf_objs = config_pool.search([('company_id', '=', run.company_id.id)], limit=1)
            for conf in conf_objs:
                is_expense_by_analytic = conf.expense_line_by_analytic
                # Цалингийн тохиргоо нь дээр байгаа бичилт салгаж үүсэх дансуудыг авна
                for acc in conf.seprate_account:
                    acc_ids.append(acc.id)
                # Цалингийн тохиргоо нь дээр байгаа Харилцагчийн мэдээллийг авна
                for part in conf.salary_partners:
                    parts.update({part.account_id.id: part.partner_id.id})
            acc_ids = str(map(str, acc_ids))
            acc_ids = acc_ids.replace('[', '(').replace(']', ')')
            if not acc_ids or acc_ids == '()':
                raise UserError((u'Компанийн цалингийн тохиргоо нь дээр данс тохируулаагүй байна!'))

            # Зардал дээр шинжилгээний данс шаардах
            if is_expense_by_analytic:
                # Цалингийн бодолтын мөрүүдийг Кредит талдаа дансаар нь бүлэглэж үүсгэнэ, Цалин өглөг, ХХОАТ өглөг, НДШ өглөг гэх мэт
                cr.execute(run._get_advance_last_slry_credit_qry_with_acc_not_in(str(acc_ids)))
                credit_records = cr.dictfetchall()
                for credit in credit_records:
                    line_ids.append(run._get_account_move_line_vals(credit, name, parts.get(credit['account'])))
                # Цалингийн бодолтын мөрүүдийг Кредит талдаа тусдаа тусдаа бичилт үүсгэнэ, Ажилчдаас авах авлага гэх мэт
                cr.execute(run._get_advance_last_slry_credit_qry_with_acc_in(acc_ids))
                credit_records_sep = cr.dictfetchall()
                for credit in credit_records_sep:
                    employee = employee_pool.browse(credit['emp'])
                    if not employee.address_home_id:
                        raise UserError(_('Employee %s has no partner check address_home_id') % employee.name_related)
                    line_ids.append(run._get_account_move_line_vals(credit, name, employee.address_home_id.id, employee=employee))
                # Цалингийн бодолтын мөрүүдийг Дебит талдаа дансаар нь бүлэглэж үүсгэнэ, Цалин өглөг, Цалин зардал, НДШ зардал гэх мэт
                cr.execute(run._get_advance_last_slry_debit_qry_with_acc_not_in_without_expense(acc_ids))
                debit_records = cr.dictfetchall()
                for debit in debit_records:
                    line_ids.append(run._get_account_move_line_vals(debit, name, parts.get(debit['account']), is_debit=True))
                # Цалингийн бодолтын мөрүүдийг Дебит талдаа тусдаа тусдаа бичилт үүсгэнэ,
                cr.execute(run._get_advance_last_slry_debit_qry_with_acc_in(acc_ids))
                debit_records_sep = cr.dictfetchall()
                for debit in debit_records_sep:
                    employee = employee_pool.browse(debit['emp'])
                    if not employee.address_home_id:
                        raise UserError(_('Employee %s has no partner check address_home_id') % employee.name_related)
                    line_ids.append(run._get_account_move_line_vals(debit, name, employee.address_home_id.id, employee=employee, is_debit=True))

                # СҮҮЛ цалингийн 'expense' төрөлтэй данс бүхий ДТ талын бичилтүүдэд шинжилгээний данс олгох
                cr.execute(run._get_last_slry_debit_qry_by_each_emp(acc_ids))
                debit_expense_sep = cr.dictfetchall()
                move_dict = {}
                _count = 1
                for expense in debit_expense_sep:
                    analytic_acc_id = run._prepare_analytic_account(expense)
                    if not analytic_acc_id:
                        employee_objs = employee_pool.search([('id', '=', expense['emp'])])
                        for employee in employee_objs:
                            if employee.contract_id:
                                if employee.contract_id.analytic_account_id:
                                    analytic_acc_id = employee.contract_id.analytic_account_id.id
                                else:
                                    if employee.department_id:
                                        if employee.department_id.analytic_account_id:
                                            analytic_acc_id = employee.department_id.analytic_account_id.id
                                    else:
                                        raise UserError(_("Please configure department on '%s.%s' employee !!!") % (employee.name_related, employee.last_name))
                            else:
                                raise UserError(_("Please configure contract on '%s.%s' employee !!!") % (employee.name_related, employee.last_name))
                    if analytic_acc_id:
                        key = str(analytic_acc_id) + ':' + str(expense['account']) + ':' + str(expense['dscrtpn'])
                        if key in move_dict:
                            move_dict[key] += expense['total']
                        else:
                            move_dict.update(
                                {key: expense['total']}
                            )
                        _count += 1
                    else:
                        raise UserError((u'Ажилтан %s овогтой %s-ын хөдөлмөрийн гэрээ эсвэл хэтлэс дээр шинжилгээний данс сонгогдоогүй байна!' % (employee_objs.name_related, employee_objs.last_name)))
                for move1 in move_dict:
                    accs = move1.split(':')
                    debit_line = (0, 0, {
                        'name': accs[2] + ' ' + run.period_id.name if accs[2] else name,
                        'date': run.compute_date,
                        'partner_id': parts.get(accs[1]),
                        'account_id': int(accs[1]),
                        'journal_id': run.journal_id.id,
                        'debit': move_dict[move1],
                        'credit': 0,
                        'analytic_account_id': int(accs[0]),
                    })
                    line_ids.append(debit_line)
                line_ids = self._group_account_move_line_vals(line_ids)
                move.update({'line_ids': line_ids})
                move_id = move_pool.create(move)
                for slip in run.slip_ids:
                    vacation_line = {}
                    vacation_line['employee_id'] = slip.employee_id.id
                    vacation_line['period_id'] = slip.period_id.id
                    gross_vacation = 0
                    gross = 0
                    worked_day = 0
                    work_day = 0
                    wage = 0
                    addall = 0
                    addfoodvehicle = 0
                    additional = 0
                    ndsh = 0
                    hhoat = 0
                    bdndsh = 0
                    hhoat_h = 0
                    outageday = 0
                    hhoat_food_vehicle = 0
                    for line in slip.line_ids:
                        if line.salary_rule_id.history_config == 'vacation_salary':
                            gross_vacation += line.amount
                        if line.salary_rule_id.history_config == 'gross':
                            gross += line.amount
                        if line.salary_rule_id.history_config == 'worked_day':
                            worked_day += line.amount
                        if line.salary_rule_id.history_config == 'work_day':
                            work_day += line.amount
                        if line.salary_rule_id.history_config == 'wage':
                            wage += line.amount
                        if line.salary_rule_id.history_config == 'addall':
                            addall += line.amount
                        if line.salary_rule_id.history_config == 'addfood':
                            addfoodvehicle += line.amount
                        if line.salary_rule_id.history_config == 'addvehicle':
                            addfoodvehicle += line.amount
                        if line.salary_rule_id.history_config == 'outage':
                            outageday += line.amount
                        if line.salary_rule_id.history_config == 'ndsh':
                            ndsh += line.amount
                        if line.salary_rule_id.history_config == 'bdndsh':
                            bdndsh += line.amount
                        if line.salary_rule_id.history_config == 'hhoat':
                            hhoat += line.amount
                        if line.salary_rule_id.history_config == 'hhoat_h':
                            hhoat_h += line.amount
                        if line.salary_rule_id.history_config == 'hhoat_food_vehicle':
                            hhoat_food_vehicle += line.amount
                    additional = addall - addfoodvehicle
                    vacation_line['additional'] = additional
                    vacation_line['worked_day'] = worked_day
                    vacation_line['work_day'] = work_day
                    vacation_line['wage'] = wage
                    if gross_vacation > 0:
                        vacation_line['is_vacation'] = True
                    else:
                        vacation_line['is_vacation'] = False
                    vacation_line['amount'] = gross - gross_vacation
                    vacation_line['gross'] = gross - gross_vacation - additional - outageday
                    vacation_line['vacation_salary'] = gross_vacation
                    vacation_line['total'] = gross
                    vacation_line['outage'] = outageday
                    vacation_line['ndsh'] = ndsh
                    vacation_line['bdndsh'] = bdndsh
                    vacation_line['hhoat'] = hhoat
                    vacation_line['hhoat_h'] = hhoat_h
                    vacation_line['hhoat_food_vehicle'] = hhoat_food_vehicle
                    vacation_line['additional_food_vehicle'] = addfoodvehicle
                    self.env['hr.payslip.vacation'].create(vacation_line)
                    for pro_input in slip.input_line_ids:
                        if pro_input.raise_id:
                            pro_input.write({'is_count': True})
                        if pro_input.deduction_id:
                            pro_input.write({'is_count': True})
                        if pro_input.rebate_id:
                            pro_input.write({'is_count': True})
                    slip.write({'paid': True, 'state': 'done'})
            else:
                cr.execute(run._get_advance_last_slry_credit_qry_with_acc_not_in(acc_ids))
                credit_records = cr.dictfetchall()
                for credit in credit_records:
                    line_ids.append(run._get_account_move_line_vals(credit, name, parts.get(credit['account'])))
                cr.execute(run._get_advance_last_slry_credit_qry_with_acc_in(acc_ids))
                credit_records_sep = cr.dictfetchall()
                for credit in credit_records_sep:
                    employee = employee_pool.browse(credit['emp'])
                    if not employee.address_home_id:
                        raise UserError(_('Employee %s has no partner check address_home_id') % employee.name_related)
                    line_ids.append(run._get_account_move_line_vals(credit, name, employee.address_home_id.id, employee=employee))
                cr.execute(run._get_advance_last_slry_debit_qry_with_acc_not_in(acc_ids))
                debit_records = cr.dictfetchall()
                for debit in debit_records:
                    line_ids.append(run._get_account_move_line_vals(debit, name, parts.get(debit['account']), is_debit=True))
                cr.execute(run._get_advance_last_slry_debit_qry_with_acc_in(acc_ids))
                debit_records_sep = cr.dictfetchall()
                for debit in debit_records_sep:
                    employee = employee_pool.browse(debit['emp'])
                    if not employee.address_home_id:
                        raise UserError(_('Employee %s has no partner check address_home_id') % employee.name_related)
                    line_ids.append(run._get_account_move_line_vals(debit, name, employee.address_home_id.id, employee=employee, is_debit=True))
                move.update({'line_ids': line_ids})
                move_id = move_pool.create(move)
                acc_data = {}
                for acc_move in move_id:
                    for acc_line in acc_move.line_ids:
                        if acc_line.account_id.user_type_id.type == 'expense':
                            acc_data[acc_line.account_id.id] = acc_line.id
                for slip in run.slip_ids:
                    vacation_line = {}
                    if slip.salary_type == 'last_salary':
                        vacation_line['employee_id'] = slip.employee_id.id
                        vacation_line['period_id'] = slip.period_id.id
                        gross_vacation = 0
                        gross = 0
                        worked_day = 0
                        work_day = 0
                        wage = 0
                        addall = 0
                        addfoodvehicle = 0
                        additional = 0
                        ndsh = 0
                        hhoat = 0
                        bdndsh = 0
                        hhoat_h = 0
                        outageday = 0
                        hhoat_food_vehicle = 0

                        for line in slip.line_ids:
                            if line.code == 'GROSSVACATION':
                                gross_vacation = line.amount
                            if line.code == 'TOTALGROSS':
                                gross = line.amount
                            if line.code == 'WORKEDHOUR':
                                worked_day = line.amount / 8
                            if line.code == 'WORKHOUR':
                                work_day = line.amount / 8
                            if line.code == 'BASIC':
                                wage = line.amount
                            if line.code == 'ADDALL':
                                addall = line.amount
                            if line.code == 'ADDFOOD':
                                addfoodvehicle = line.amount
                            if line.code == 'ADDVEHICLE':
                                addfoodvehicle += line.amount
                            if line.code == 'OUTAGEDAY':
                                outageday = line.amount
                            if line.code == 'NDSH':
                                ndsh = line.amount
                            if line.code == 'BDNDSH':
                                bdndsh = line.amount
                            if line.code == 'HHOAT':
                                hhoat = line.amount
                            if line.code == 'HHOAT_H':
                                hhoat_h = line.amount
                            if line.code == 'HAOATFOOD':
                                hhoat_food_vehicle = line.amount

                        additional = addall - addfoodvehicle
                        vacation_line['additional'] = additional
                        vacation_line['worked_day'] = worked_day
                        vacation_line['work_day'] = work_day
                        vacation_line['wage'] = wage
                        if gross_vacation > 0:
                            vacation_line['is_vacation'] = True
                        else:
                            vacation_line['is_vacation'] = False
                        vacation_line['amount'] = gross - gross_vacation
                        vacation_line['gross'] = gross - gross_vacation - additional - outageday
                        vacation_line['vacation_salary'] = gross_vacation
                        vacation_line['total'] = gross
                        vacation_line['outage'] = outageday
                        vacation_line['ndsh'] = ndsh
                        vacation_line['bdndsh'] = bdndsh
                        vacation_line['hhoat'] = hhoat
                        vacation_line['hhoat_h'] = hhoat_h
                        vacation_line['hhoat_food_vehicle'] = hhoat_food_vehicle
                        vacation_line['additional_food_vehicle'] = addfoodvehicle
                        self.env['hr.payslip.vacation'].create(vacation_line)
                        for pro_input in slip.input_line_ids:
                            if pro_input.raise_id:
                                pro_input.write({'is_count': True})
                            if pro_input.deduction_id:
                                pro_input.write({'is_count': True})
                            if pro_input.rebate_id:
                                pro_input.write({'is_count': True})
                        slip.write({'paid': True, 'state': 'done'})
        return move_id

    @api.multi
    def approve_advance_salary(self):
        cr = self.env.cr
        move_pool = self.env['account.move']
        config_pool = self.env['hr.payroll.config']
        employee_pool = self.env['hr.employee']
        move_id = False
        for run in self:
            line_ids = []
            # timenow = run.compute_date
            name = _('Payslip Run of %s') % (run.name)
            move = {
                'narration': name,
                'date': run.compute_date,
                'ref': run.number,
                'journal_id': run.journal_id.id,
                'is_salary': True,
            }
            acc_ids = []
            parts = {}
            conf_ids = config_pool.search([('company_id', '=', self.env.user.company_id.id)])
            for conf in conf_ids:
                for acc in conf.seprate_account:
                    acc_ids.append(acc.id)
                for part in conf.salary_partners:
                    parts.update({part.account_id.id: part.partner_id.id})
            acc_ids = str(map(str, acc_ids))
            acc_ids = acc_ids.replace('[', '(').replace(']', ')')
            if not acc_ids:
                raise UserError((u'Компанийн цалингийн тохиргоо нь дээр данс тохируулаагүй байна!'))
            ######################################################################################################
            cr.execute(run._get_advance_last_slry_credit_qry_with_acc_not_in(acc_ids, create_move_in_advance=True))
            credit_records = cr.dictfetchall()
            for credit in credit_records:
                line_ids.append(run._get_account_move_line_vals(credit, name, parts.get(credit['account'])))
            ######################################################################################################
            cr.execute(run._get_advance_last_slry_credit_qry_with_acc_in(acc_ids, create_move_in_advance=True))
            credit_records_sep = cr.dictfetchall()
            for credit in credit_records_sep:
                employee = employee_pool.browse(credit['emp'])
                if not employee.address_home_id:
                    raise UserError(_('Employee %s has no partner check address_home_id') % employee.name_related)
                line_ids.append(run._get_account_move_line_vals(credit, name, employee.address_home_id.id, employee=employee))
            ######################################################################################################
            cr.execute(run._get_advance_last_slry_debit_qry_with_acc_not_in(acc_ids, create_move_in_advance=True))
            debit_records = cr.dictfetchall()
            for debit in debit_records:
                line_ids.append(run._get_account_move_line_vals(debit, name, parts.get(debit['account']), is_debit=True))
            ######################################################################################################
            cr.execute(run._get_advance_last_slry_debit_qry_with_acc_in(acc_ids, create_move_in_advance=True))
            debit_records_sep = cr.dictfetchall()
            for debit in debit_records_sep:
                employee = employee_pool.browse(debit['emp'])
                if not employee.address_home_id:
                    raise UserError(_('Employee %s has no partner check address_home_id') % employee.name_related)
                line_ids.append(run._get_account_move_line_vals(debit, name, employee.address_home_id.id, employee=employee, is_debit=True))
            line_ids = self._group_account_move_line_vals(line_ids)
            move.update({'line_ids': line_ids})
            move_id = move_pool.create(move)
            for slip in run.slip_ids:
                for pro_input in slip.input_line_ids:
                    if pro_input.raise_id:
                        pro_input.write({'is_count': True})
                    if pro_input.deduction_id:
                        pro_input.write({'is_count': True})
                    if pro_input.rebate_id:
                        pro_input.write({'is_count': True})
                slip.write({'paid': True, 'state': 'done'})
        return move_id

    @api.multi
    def _group_account_move_line_vals(self, line_ids):
        self.ensure_one()
        return line_ids

    @api.multi
    def _get_account_move_line_vals(self, dict, name, partner, is_debit=False, employee=False):
        self.ensure_one()
        return (0, 0, {
            'name': dict['dscrtpn'] + ' ' + self.period_id.name if dict['dscrtpn'] else name,
            'date': self.compute_date,
            'partner_id': partner,
            'account_id': dict['account'],
            'journal_id': self.journal_id.id,
            'debit': dict['total'] if is_debit else 0,
            'credit': 0 if is_debit else dict['total'],
            'analytic_account_id': self._prepare_analytic_account(dict),
        })

    @api.multi
    def approve_maternity_salary(self):
        cr = self.env.cr
        move_pool = self.env['account.move']
        config_pool = self.env['hr.payroll.config']
        employee_pool = self.env['hr.employee']
        move_id = False
        for run in self:
            line_ids = []
            acc_ids = []
            parts = {}
            # timenow = run.compute_date
            name = _('Payslip Run of %s') % (run.name)
            move = {
                'narration': name,
                'date': run.compute_date,
                'ref': run.number,
                'journal_id': run.journal_id.id,
                'is_salary': True,
            }
            conf_ids = config_pool.search([('company_id', '=', self.env.user.company_id.id)])
            for conf in conf_ids:
                for acc in conf.seprate_account:
                    acc_ids.append(acc.id)
                for part in conf.salary_partners:
                    parts.update({part.account_id.id: part.partner_id.id})
            acc_ids = str(map(str, acc_ids))
            acc_ids = acc_ids.replace('[', '(').replace(']', ')')
            if not acc_ids:
                raise UserError((u'Компанийн цалингийн тохиргоо нь дээр данс тохируулаагүй байна!'))
            # Цалингийн бодолтын мөрүүдийг Кредит талдаа тусдаа тусдаа бичилт үүсгэнэ, Ажилчдаас авах авлага гэх мэт
            cr.execute(run._get_maternity_hchta_slry_credit_qry())
            credit_records_sep = cr.dictfetchall()
            for credit in credit_records_sep:
                employee = employee_pool.browse(credit['emp'])
                if not employee.address_home_id:
                    raise UserError(_('Employee %s has no partner check address_home_id') % employee.name_related)
                credit_line = (0, 0, {
                    'name': credit['dscrtpn'] + ' ' + run.period_id.name if credit['dscrtpn'] else name,
                    'date': run.compute_date,
                    'partner_id': employee.address_home_id.id,
                    'account_id': credit['account'],
                    'journal_id': run.journal_id.id,
                    'debit': 0,
                    'credit': credit['total'],
                    'analytic_account_id': run._prepare_analytic_account(credit),
                })
                line_ids.append(credit_line)
            # Цалингийн бодолтын мөрүүдийг Дебит талдаа дансаар нь бүлэглэж үүсгэнэ, Цалин өглөг, Цалин зардал, НДШ зардал гэх мэт
            cr.execute(run._get_maternity_slry_debit_qry_without_expense())
            debit_records = cr.dictfetchall()
            for debit in debit_records:
                debit_line = (0, 0, {
                    'name': debit['dscrtpn'] + ' ' + run.period_id.name if debit['dscrtpn'] else name,
                    'date': run.compute_date,
                    'partner_id': parts.get(debit['account']),
                    'account_id': debit['account'],
                    'journal_id': run.journal_id.id,
                    'debit': debit['total'],
                    'credit': 0,
                    'analytic_account_id': run._prepare_analytic_account(debit),
                })
                line_ids.append(debit_line)
            move.update({'line_ids': line_ids})
            move_id = move_pool.create(move)
            for slip in run.slip_ids:
                slip.write({'paid': True, 'state': 'done'})
        return move_id

    @api.multi
    def approve_hchta(self):
        cr = self.env.cr
        move_pool = self.env['account.move']
        config_pool = self.env['hr.payroll.config']
        employee_pool = self.env['hr.employee']
        move_obj = False
        for run in self:
            line_ids = []
            acc_ids = []
            parts = {}
            # timenow = run.compute_date
            name = _('Payslip Run of %s') % (run.name)
            move = {
                'narration': name,
                'date': run.compute_date,
                'ref': run.number,
                'journal_id': run.journal_id.id,
                'is_salary': True,
            }
            is_expense_by_analytic = False
            conf_ids = config_pool.search([('company_id', '=', self.env.user.company_id.id)], limit=1)
            for conf in conf_ids:
                is_expense_by_analytic = conf.expense_line_by_analytic
                for acc in conf.seprate_account:
                    acc_ids.append(acc.id)
                for part in conf.salary_partners:
                    parts.update({part.account_id.id: part.partner_id.id})
            acc_ids = str(map(str, acc_ids))
            acc_ids = acc_ids.replace('[', '(').replace(']', ')')
            if not acc_ids:
                raise UserError((u'Компанийн цалингийн тохиргоо нь дээр данс тохируулаагүй байна!'))
            if is_expense_by_analytic:
                # Цалингийн бодолтын мөрүүдийг Кредит талдаа тусдаа тусдаа бичилт үүсгэнэ, Ажилчдаас авах авлага гэх мэт
                cr.execute(run._get_maternity_hchta_slry_credit_qry())
                credit_records_sep = cr.dictfetchall()
                for credit in credit_records_sep:
                    employee = employee_pool.browse(credit['emp'])
                    if not employee.address_home_id:
                        raise UserError(_('Employee %s has no partner check address_home_id') % employee.name_related)
                    credit_line = (0, 0, {
                        'name': credit['dscrtpn'] + ' ' + run.period_id.name if credit['dscrtpn'] else name,
                        'date': run.compute_date,
                        'partner_id': employee.address_home_id.id,
                        'account_id': credit['account'],
                        'journal_id': run.journal_id.id,
                        'debit': 0,
                        'credit': credit['total'],
                        'analytic_account_id': run._prepare_analytic_account(credit),
                    })
                    line_ids.append(credit_line)
                # Цалингийн бодолтын мөрүүдийг Дебит талдаа дансаар нь бүлэглэж үүсгэнэ, Цалин өглөг, Цалин зардал, НДШ зардал гэх мэт
                cr.execute(run._get_hchta_slry_debit_qry_without_expense(acc_ids))
                debit_records = cr.dictfetchall()
                for debit in debit_records:
                    debit_line = (0, 0, {
                        'name': debit['dscrtpn'] + ' ' + run.period_id.name if debit['dscrtpn'] else name,
                        'date': run.compute_date,
                        'partner_id': parts.get(debit['account']),
                        'account_id': debit['account'],
                        'journal_id': run.journal_id.id,
                        'debit': debit['total'],
                        'credit': 0,
                        'analytic_account_id': run._prepare_analytic_account(debit),
                    })
                    line_ids.append(debit_line)
                cr.execute(run._get_last_slry_debit_qry_by_each_emp(acc_ids))
                debit_expense_sep = cr.dictfetchall()
                move_dict = {}
                _count = 1
                analytic_acc_id = False
                for expense in debit_expense_sep:
                    analytic_acc_id = run._prepare_analytic_account(expense)
                    if not analytic_acc_id:
                        employee_ids = employee_pool.search([('id', '=', expense['emp'])])
                        for employee in employee_ids:
                            if employee.contract_id:
                                if employee.contract_id.analytic_account_id:
                                    analytic_acc_id = employee.contract_id.analytic_account_id.id
                                else:
                                    if employee.department_id:
                                        if employee.department_id.analytic_account_id:
                                            analytic_acc_id = employee.department_id.analytic_account_id.id
                                    else:
                                        raise UserError(_("Please configure department on '%s.%s' employee !!!") % (employee.name_related, employee.last_name))
                            else:
                                raise UserError(_("Please configure contract on '%s.%s' employee !!!") % (employee.name_related, employee.last_name))
                    if analytic_acc_id:
                        key = str(analytic_acc_id) + ':' + str(expense['account']) + ':' + str(expense['dscrtpn'])
                        if key in move_dict:
                            move_dict[key] += expense['total']
                        else:
                            move_dict.update(
                                {key: expense['total']}
                            )
                        _count += 1
                    else:
                        raise UserError(_(u'Ажилтан %s овогтой %s-ын хөдөлмөрийн гэрээ эсвэл хэтлэс дээр шинжилгээний данс сонгогдоогүй байна!' % (employee_ids.name_related, employee_ids.last_name)))
                for move1 in move_dict:
                    accs = move1.split(':')
                    debit_line = (0, 0, {
                        'name': accs[2] + ' ' + run.period_id.name if accs[2] else name,
                        'date': run.compute_date,
                        'partner_id': parts.get(accs[1]),
                        'account_id': int(accs[1]),
                        'journal_id': run.journal_id.id,
                        'debit': move_dict[move1],
                        'credit': 0,
                        'analytic_account_id': int(accs[0]),
                    })
                    line_ids.append(debit_line)
                move.update({'line_ids': line_ids})
                move_obj = move_pool.create(move)
            else:
                cr.execute(run._get_hchta_slry_credit_qry(acc_ids))
                credit_records_sep = cr.dictfetchall()
                for credit in credit_records_sep:
                    employee = employee_pool.browse(credit['emp'])
                    if not employee.address_home_id:
                        raise UserError(_('Employee %s has no partner check address_home_id') % employee.name_related)
                    credit_line = (0, 0, {
                        'name': credit['dscrtpn'] + ' ' + run.period_id.name if credit['dscrtpn'] else name,
                        'date': run.compute_date,
                        'partner_id': employee.address_home_id.id,
                        'account_id': credit['account'],
                        'journal_id': run.journal_id.id,
                        'debit': 0,
                        'credit': credit['total'],
                        'analytic_account_id': run._prepare_analytic_account(credit),
                    })
                    line_ids.append(credit_line)
                cr.execute(run._get_hchta_slry_debit_qry(acc_ids))
                debit_records = cr.dictfetchall()
                for debit in debit_records:
                    debit_line = (0, 0, {
                        'name': debit['dscrtpn'] + ' ' + run.period_id.name if debit['dscrtpn'] else name,
                        'date': run.compute_date,
                        'partner_id': parts.get(debit['account']),
                        'account_id': debit['account'],
                        'journal_id': run.journal_id.id,
                        'debit': debit['total'],
                        'credit': 0,
                        'analytic_account_id': run._prepare_analytic_account(debit),
                    })
                    line_ids.append(debit_line)
                move.update({'line_ids': line_ids})
                move_obj = move_pool.create(move)
                acc_data = {}
                for acc_line in move_obj.line_ids:
                    if acc_line.account_id.user_type_id.type == 'expense':
                        acc_data[acc_line.account_id.id] = acc_line.id
                for slip in run.slip_ids:
                    if slip.employee_id.contract_id.analytic_account_id:
                        for line in slip.line_ids:
                            if line.salary_rule_id.account_debit and line.salary_rule_id.account_debit.user_type_id.type == 'expense' and line.total > 0:
                                account_analytic_line = {
                                    'name': line.salary_rule_id.name,  #
                                    'amount': -1 * line.total,  #
                                    'user_id': self.env.user.id,
                                    'date': run.compute_date,  #
                                    'company_id': 1,
                                    'account_id': slip.employee_id.contract_id.analytic_account_id.id,  #
                                    'general_account_id': line.salary_rule_id.account_debit.id,  #
                                    'payslip_run_id': run.id,
                                    'ref': slip.number,
                                }
                                self.env['account.analytic.line'].create(account_analytic_line)
                        slip.write({'paid': True, 'state': 'done'})
                    else:
                        raise UserError(_(u'Ажилтан ' + str(slip.employee_id.name_related) + u' ' + str(slip.employee_id.last_name) + u' дээр шинжилгээний данс сонгогдоогүй байна!'))
            for slip in run.slip_ids:
                slip.write({'paid': True, 'state': 'done'})
        return move_obj

    @api.multi
    def compute_all(self):
        ids = []
        sum_ids = []
        slip_ids = []
        for run in self:
            for slip in run.slip_ids:
                if slip.id not in slip_ids:
                    slip_ids.append(slip.id)
                for line in slip.line_ids:
                    ids.append(line.id)
            for slip_sum in run.slip_sum_ids:
                sum_ids.append(slip_sum.id)
        if ids:
            self._cr.execute('DELETE FROM hr_payslip_line where id in (' + str(ids)[1:-1] + ')')
        if sum_ids:
            self._cr.execute('DELETE FROM hr_payslip_sum where id in (%s)' % ','.join(str(a) for a in sum_ids))

        self.compute_all_sheet(slip_ids)
        return True

    @api.multi
    def compute_all_sheet(self, p_ids):
        for slip in self.env['hr.payslip'].browse(p_ids):
            slip.compute_sheet()
        if p_ids:
            self.env.cr.execute("""select m.sid as sid, m.run_id as rid, m.name as name, m.code as code, m.seq as seq, sum(m.amount) as amount from (select p.id as pid, p.struct_id as sid, p.payslip_run_id as run_id, l.id as lid, l.code as code, l.name as name, l.salary_rule_id as rule_id, l.sequence as seq, l.amount as amount
                            from hr_payslip p left join hr_payslip_line l on p.id=l.slip_id where p.id in (%s) ) as m
                            group by m.sid,m.run_id, m.name, m.code, m.seq """ % ','.join(str(a) for a in p_ids))
            output_lines = self.env.cr.dictfetchall()

            for output in output_lines:
                self.env.cr.execute("""insert into hr_payslip_sum (create_uid,create_date,write_uid,write_date,
                                payslip_run_id, struct_id,code, name, total, sequence) values (%s,'%s',%s,'%s',%s,%s,'%s','%s',%s, %s) """
                                    % (self.env.user.id, datetime.now(), self.env.user.id, datetime.now(),
                                       output['rid'], output['sid'], output['code'], output['name'],
                                       output['amount'], output['seq']))

    @api.multi
    def send_mass_email(self):
        for run in self:
            if run.salary_type == 'advance_salary' or run.salary_type == 'last_salary':
                for slip in run.slip_ids:
                    if slip.employee_id.work_email:
                        if slip.salary_type == 'advance_salary':
                            stype = u'урьдчилгаа'
                        else:
                            stype = u'сүүл'
                        outgoing_email_ids = self.env['ir.mail_server'].search([])
                        if not outgoing_email_ids:
                            raise exceptions.ValidationError(_('There is no configuration for outgoing mail server. Please contact system administrator.'))
                        else:
                            outgoing_email = outgoing_email_ids[0]
                            you = slip.employee_id.work_email
                            email_ids = []
                            salary_header = u'<table style="width:400px;" border="1"><tr><th align="left">Огноо</th><td align="right">%s-сар</td></tr><tr><th align="left">Ажилтаны код</th><td align="right">%s</td></tr><tr><th align="left">Нэр</th><td align="right">%s</td></tr><tr><th align="left">Овог</th><td align="right">%s</td></tr></table></br>' % (
                                slip.period_id.name, slip.employee_id.identification_id, slip.employee_id.name_related, slip.employee_id.last_name)
                            salary_body = u'<table style="width:400px;" border="1">'
                            for line in slip.line_ids:
                                if line.salary_rule_id.is_email:
                                    salary_body += u'<tr><th align="left">' + u'%s' % line.salary_rule_id.name + \
                                                   u'</th><td align="right">' + \
                                                   u'%s' % line.amount + u'</td></tr>'
                            salary_body += u'</table>'
                            vals = {
                                'state': 'outgoing',
                                'subject': u'Танд %s Сарын %s цалингийн мэдээлэл ирлээ.' % (slip.period_id.name, stype),
                                'body_html': u'<p>Сайн байна уу </br></p><p>Танд %s Сарын %s цалингийн мэдээлэл ирлээ.</p> %s <br> %s <br> <p> Баярлалаа,</p><pre>--<br>ERP Автомат Имэйл </pre>' % (
                                    slip.period_id.name, stype, salary_header, salary_body),
                                'email_to': you,
                                'email_from': outgoing_email.smtp_user,
                            }
                            email_ids.append(
                                self.env['mail.mail'].create(vals))
                            if email_ids != []:
                                self.env['mail.mail'].send(email_ids)
        return True


class HrPayslipRunWizard(models.TransientModel):
    _name = 'hr.payslip.run.wizard'

# цалингийн хуудсыг бөөнөөр нь хасах
    @api.multi
    def delete_slip_ids(self):
        slip_ids = self.env['hr.payslip.run'].browse(self.env.context.get('active_id')).slip_ids
        for slip in slip_ids:
            slip.unlink()
        slip_sum_ids = self.env['hr.payslip.run'].browse(self.env.context.get('active_id')).slip_sum_ids
        if slip_sum_ids:
            for sum_id in slip_sum_ids:
                sum_id.unlink()


class HrPayslipInput(models.Model):
    '''Цалингийн хуудасны оролтууд
    '''
    _inherit = 'hr.payslip.input'
    _order = 'id'

    category_id = fields.Many2one('hr.salary.rule.category', string='Rule Category', required=True)
    contract_id = fields.Many2one('hr.contract', string='Contract', required=False)
    raise_id = fields.Integer(string="Raise")
    deduction_id = fields.Integer(string="Deduction")
    rebate_id = fields.Integer(string="Rebate")
    is_count = fields.Boolean(string='Is Count')

    @api.onchange('category_id')
    def onchange_contract(self):
        if self.category_id:
            if self.payslip_id.contract_id:
                self.code = self.category_id.code
                self.contract_id = self.payslip_id.contract_id.id
                self.name = self.category_id.name


class HrSalaryRuleCategory(models.Model):
    '''Цалингийн дүрмийн ангилал
    '''
    _inherit = 'hr.salary.rule.category'
    _order = "sequence"

    sequence = fields.Integer(string='Sequence')
    select_in_inputs = fields.Boolean(string='Select in Inputs')
    salary_type = fields.Selection([('last_salary', 'Last salary'),
                                    ('advance_salary', 'Advance salary'),
                                    ('vacation', 'Vacation')], string='Salary type')


class HrPayrollStructure(models.Model):
    '''Цалин бодолтын загвар
    '''
    _inherit = 'hr.payroll.structure'

    salary_type = fields.Selection([('last_salary', 'Last salary'),
                                    ('advance_salary', 'Advance salary'),
                                    ('vacation', 'Vacation'),
                                    ('maternity', 'Maternity leave'),
                                    ('hchta', 'HCHTA Leave')], string='Salary type')
    # receivable_account = fields.Many2one('account.account', string='Receivable Account')
    # receivable_category = fields.Many2one('hr.salary.rule.category', string='Receivable Category')


class HrSalaryRule(models.Model):
    '''Цалингийн дүрэм
    '''
    _inherit = 'hr.salary.rule'
    _order = 'sequence'

    show_in_report = fields.Boolean(string='Show in Report', default=True)
    is_email = fields.Boolean(string='Send in Email', default=True)
    ndsh_report_type = fields.Selection([('undsen', 'Undsen'),
                                         ('shagnalt', 'Shagnalt'),
                                         ('busad', 'Busad'),
                                         ('hool', 'Hool'),
                                         ('tulee', 'Tulee'),
                                         ('niit', 'Niit')], string='Ndsh report Type')
    hhoat_report_type = fields.Selection([('income_7_1_1', '7_1_1'),
                                          ('income_7_1_2', '7_1_2'),
                                          ('income_7_1_3', '7_1_3'),
                                          ('income_7_1_4', '7_1_4'),
                                          ('income_7_1_5', '7_1_5'),
                                          ('income_7_1_6', '7_1_6'),
                                          ('income_7_1_7', '7_1_7'),
                                          ('ndsh1', 'NDSH1'),
                                          ('ndsh2', 'NDSH2'),
                                          ('hhoat_discount', 'HHOAT discount'),
                                          ('2020_rep', '2020.04.01-2020.12.31')], string='Hhoat report Type')
    create_move_in_advance = fields.Boolean(string='Create move in advance')
    column_width = fields.Integer(string='Column width', default=12)
    internal_code = fields.Char(string='Internal code')
    history_config = fields.Selection([
        ('addall', 'All additional'),
        ('worked_day', 'Worked day'),
        ('work_day', 'Work day'),
        ('wage', 'Wage'),
        ('gross', 'Gross'),
        ('vacation_salary', 'Vacation salary'),
        ('addfood', 'Add food'),
        ('addvehicle', 'Add vehicle'),
        ('outage', 'Outage'),
        ('ndsh', 'Ndsh'),
        ('bdndsh', 'Bdndsh'),
        ('hhoat', 'Hhoat'),
        ('hhoat_h', 'Hhoat_h'),
        ('hhoat_food_vehicle', 'Hhoat food vehicle')], string='History type')
    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic Account',
                                          domain="[('type','=','normal')]")
    transaction_description = fields.Char('Transaction Description')
    social_insurance_type = fields.Selection([
        ('salary_income', 'Salary Income'),
        ('insurance_company', 'Insurance of company'),
        ('insurance_employee', 'Insurance of employee')], string='Social Insurance Type')


class InheritHRPayrollConfig(models.Model):
    '''Цалингийн тохиргоо
    '''
    _inherit = "hr.payroll.config"

    salary_deductions = fields.One2many('hr.payslip.deductions', 'config_id', string='Salary Deductions')
    salary_additionals = fields.One2many('hr.payslip.additional', 'config_id', string='Salary Additional')
    salary_partners = fields.One2many('hr.payslip.partner', 'config_id', string='Salary Partner')
    salary_import_ids = fields.One2many('hr.payslip.run.import', 'config_id', string='Payslip run import')


class HrEmployee(models.Model):
    _inherit = "hr.employee"

    def _hide_payslip(self):
        for obj in self:
            if self.env.user.has_group('hr.group_hr_user'):
                obj.hide_payslip = False
                return True
            elif self.user_id:
                obj.hide_payslip = False if self.env.uid == self.user_id.id else True
                return True
            else:
                obj.hide_payslip = True
                return True

    hide_payslip = fields.Boolean('Hide Payslip', compute='_hide_payslip')
    payslip_count = fields.Integer(compute='_compute_payslip_count', string='Payslips', groups="")
    disability_percent = fields.Float('Disability percent')

    @api.onchange('disability_percent')
    def onchange_disability_percent(self):
        for record in self:
            if record.disability_percent > 100:
                record.disability_percent = 100
            elif record.disability_percent < 0:
                record.disability_percent = 0


class HrPayslipSum(models.Model):
    _name = 'hr.payslip.sum'
    _description = 'Pay Slip Sum'

    struct_id = fields.Many2one('hr.payroll.structure', string='Structure',
                                readonly=True)
    payslip_run_id = fields.Many2one('hr.payslip.run', string='Payslip Batches', readonly=True,
                                     copy=False, ondelete='cascade')
    name = fields.Char(string='Name')
    code = fields.Char(string='Code')
    total = fields.Float(string='Total')
    sequence = fields.Integer(string='Sequence')
    rule_id = fields.Many2one('hr.salary.rule', string='Rule')
