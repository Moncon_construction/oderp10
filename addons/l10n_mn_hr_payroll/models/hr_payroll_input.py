# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError
from datetime import datetime, timedelta
import logging
import base64
import xlrd
from tempfile import NamedTemporaryFile
from datetime import datetime
_logger = logging.getLogger(__name__)


class HrPayrollInputImport(models.TransientModel):
    '''Нэмэгдэл суутгал импорт хийх wizard
    '''
    _name = 'hr.payroll.input.import'

    import_type = fields.Selection([('by_register', 'Register'),
                                      ('by_identification', 'Identification')], default='by_register', string='Import Type', required=1)

    @api.multi
    def import_phone_expense_xls(self):
        payroll_input = self.env['hr.payroll.input'].browse(self.env.context['active_ids'])
        try:
            fileobj = NamedTemporaryFile('w+')
            fileobj.write(base64.decodestring(self.data))
            fileobj.seek(0)
            book = xlrd.open_workbook(fileobj.name)
        except ValueError:
            raise UserError(_('Error loading data file. Please try again!'))
        sheet = book.sheet_by_index(0)
        ncols = sheet.ncols
        nrows = sheet.nrows
        error_message = ''
        if self.import_type == 'by_register':
            for row in range(1, nrows):
                row = sheet.row(row)
                if not row[2].value:
                    error_message += _('There is no register on row %s.\n') % (row + 1)
                else:
                    employee = self.env['hr.employee'].search([('ssnid', '=', str(row[2].value))])
                    if not employee:
                        error_message += _('There is no employee with register \'%s\'.\n') % row[2].value
        else:
            for row in range(1, nrows):
                row = sheet.row(row)
                if not row[3].value:
                    error_message += _('There is no identification on row %s.\n') % (row + 1)
                else:
                    employee = self.env['hr.employee'].search([('identification_id', '=', str(row[3].value))])
                    if not employee:
                        error_message += _('There is no employee with identification \'%s\'.\n') % row[3].value
        rule_categories = []
        rule_categories_duplicated = []
        for coli in range(4, ncols):
            rule_category = self.env['hr.salary.rule.category'].search([('code', '=', sheet.row(0)[coli].value), ('select_in_inputs', '=', True)])
            if len(rule_category) > 1:
                error_message += _('There are %s input salary rule category with the same code \'%s\'.\n') % (len(rule_category), rule_category[0].code)
            elif not rule_category:
                error_message += _('There is no salary rule category which has a code \'%s\'.\n') % sheet.row(0)[coli].value
            else:
                if rule_category in rule_categories and rule_category not in rule_categories_duplicated:
                    error_message += _('File has columns with same code \'%s\'.\n') % rule_category.code
                    rule_categories_duplicated.append(rule_category)
                rule_categories.append(rule_category)
        if error_message:
            raise UserError(error_message)
        else:
            for rowi in range(1, nrows):
                row = sheet.row(rowi)
                if self.import_type == 'by_register':
                    importing_type = u"%s" % row[2].value
                else:
                    importing_type = u"%s" % row[3].value
                total_amount = sum(float(row[coli].value) for coli in range(4, ncols) if row[coli].value)
                if self.import_type == 'by_register':
                    employee = self.env['hr.employee'].search([('ssnid', '=', importing_type)], limit=1)
                else:
                    employee = self.env['hr.employee'].search([('identification_id', '=', importing_type)], limit=1)
                input_line = payroll_input.input_lines.filtered(lambda l: l.employee_id.id == employee.id)
                if input_line:
                    input_line.write({'amount': total_amount})
                else:
                    input_line = self.env['hr.payroll.input.line'].create({
                        'input_id': payroll_input.id,
                        'employee_id': employee.id,
                        'amount': total_amount
                    })
                for coli in range(3, ncols):
                    rule_category = self.env['hr.salary.rule.category'].search([('code', '=', sheet.row(0)[coli].value), ('select_in_inputs', '=', True)], limit=1)
                    if rule_category and row[coli].value:
                        existing_line = input_line.employee_input_lines.filtered(lambda l: l.rule_category_id.id == rule_category.id)
                        if existing_line:
                            existing_line.sudo().write({
                                'amount': row[coli].value
                            })
                        else:
                            input_line.sudo().write({
                                'employee_input_lines': [(0, 0, {
                                    'input_id': input_line.id,
                                    'rule_category_id': rule_category.id,
                                    'amount': row[coli].value
                                })]
                            })
            for coli in range(3, ncols):
                rule_category = self.env['hr.salary.rule.category'].search([('code', '=', sheet.row(0)[coli].value), ('select_in_inputs', '=', True)], limit=1)
                if rule_category:
                    total_amount = sum(float(sheet.row(rowi)[coli].value) for rowi in range(1, nrows) if sheet.row(rowi)[coli].value)
                    rule_input = payroll_input.rule_inputs.filtered(lambda i: i.rule_category_id.id == rule_category.id)
                    if rule_input:
                        rule_input.write({
                            'amount': total_amount
                        })
                    else:
                        rule_input = self.env['rule.input'].create({
                            'input_id': payroll_input.id,
                            'rule_category_id': rule_category.id,
                            'amount': total_amount
                        })
                    for rowi in range(1, nrows):
                        if self.import_type == 'by_register':
                            employee = self.env['hr.employee'].search([('ssnid', '=', str(sheet.row(rowi)[2].value))], limit=1)
                        else:
                            employee = self.env['hr.employee'].search([('identification_id', '=', str(sheet.row(rowi)[3].value))],
                                                                      limit=1)
                        if employee and sheet.row(rowi)[coli].value:
                            existing_rule_input = rule_input.lines.filtered(lambda l: l.employee_id.id == employee.id)
                            if existing_rule_input:
                                existing_rule_input.sudo().write({
                                    'amount': float(sheet.row(rowi)[coli].value)
                                })
                            else:
                                rule_input.sudo().write({
                                    'lines': [(0, 0, {
                                        'employee_id': employee.id,
                                        'amount': float(sheet.row(rowi)[coli].value)})]
                                })
            return True

    year_id = fields.Many2one('account.fiscalyear', string='Year', required=False)
    period_id = fields.Many2one('account.period', string='Period', required=False, domain="[('fiscalyear_id','=',year_id)]")
    data = fields.Binary(string='Plan Data File', required=True)

class HrPayrollInputCategory(models.Model):
    '''Нэмэгдэл суутгал импортлох төрлийн бүртгэл
        Бүх компанийн хэмжээн дунд нь бүртгэл Хийж ашиглана
    '''
    _name = 'hr.payroll.input.category'
    _description = 'Payroll Input Category'

    code = fields.Char(string='Code', size=16, required=True)
    name = fields.Char(string='Name', size=128, required=True)

class HrPayrollInput(models.Model):
    '''Нэмэгдэл суутгал бүртгэх
    '''
    _name = 'hr.payroll.input'
    _description = 'Hr Payroll Input'

    @api.multi
    def _get_fiscalyear(self):
        company_id = False
        employee_obj = self.pool.get('hr.employee')
        employee_ids = employee_obj.search(cr, uid, [('user_id','=',uid)])
        if employee_ids:
            for employee in employee_obj.browse(cr, uid, employee_ids, context=context):
                company_id = employee.company_id.id
        
        now = time.strftime('%Y-%m-%d')
        fiscalyears = self.pool.get('account.fiscalyear').search(cr, uid, [('company_id', '=', company_id), ('date_start', '<', now), ('date_stop', '>', now)], limit=1, context=context)
        return fiscalyears and fiscalyears[0] or False

    def _get_company(self):
        company_id = False
        employee_obj = self.pool.get('hr.employee')
        employee_ids = employee_obj.search(cr, uid, [('user_id','=',uid)])
        if employee_ids:
            for employee in employee_obj.browse(cr, uid, employee_ids, context=context):
                company_id = employee.company_id.id
        return company_id or False

    @api.one
    def _hide_button_button(self):
        if self.state == 'approved' and self.run_id.state =='done' or self.state == 'draft':
            self.hide_button = True
        else:
            self.hide_button = False

    period_id = fields.Many2one('account.period', string='Period', index=True,  domain="[('fiscalyear_id','=',year_id)]", required=True)
    year_id = fields.Many2one('account.fiscalyear', string='Year', required=False)
    run_id = fields.Many2one('hr.payslip.run', string='Payslip run', required=False)
    state = fields.Selection([('draft','Draft'),('approved','Approved')], string='State', default='draft')
    name = fields.Char(string='Name', size=512)
    description = fields.Text(string='Description')
    department_id = fields.Many2one('hr.department', string='Department')
    date = fields.Date(string='Inovice Date', default=fields.Date.context_today, readonly=True)
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id, readonly=True) #
    hide_button = fields.Boolean(compute = '_hide_button_button',string='Hide button')
    
    @api.multi
    def unlink(self):
        for input in self:
            if input.state not in ['draft']:
                raise UserError(_('You cannot delete a record which is not draft!'))
        return super(HrPayrollInput, self).unlink()
    
    @api.onchange('department_id')
    def onchange_department_id(self):
        if self.department_id and self.period_id:
            run = self.env['hr.payslip.run'].search([('department_id', '=', self.department_id.id), ('period_id', '=', self.period_id.id)], limit=1)
            if run:
                self.run_id = run
    

    @api.multi
    def to_approve(self):
        '''Батлах үед харгалзах цалингийн хуудсан дээр бичигдэнэ.
        '''
        values = {}
        move_values = {}
        invoice_lines = []
        move_lines = []
        company_id = False
        invoice_id = False
        credit = 0
        for record in self:
            if record.run_id and record.run_id.id:
                for employee in record.input_lines:
                    for payslip in record.run_id.slip_ids:
                        if payslip.employee_id.id == employee.employee_id.id:
                            for line in employee.employee_input_lines:
                                existing_input_line = payslip.input_line_ids.filtered(lambda l: l.category_id.id == line.rule_category_id.id)
                                if existing_input_line:
                                    existing_input_line.write({'amount': line.amount})
                                else:
                                    self.env['hr.payslip.input'].create({
                                        'payslip_id': payslip.id,
                                        'name': line.rule_category_id.name,
                                        'category_id': line.rule_category_id.id,
                                        'code': line.rule_category_id.code,
                                        'amount': line.amount
                                    })
        return super(HrPayrollInput, self).write({"state": "approved"})

    @api.multi
    def to_draft(self):
        for input in self:
            return  input.write({'state':'draft'})

    @api.multi
    def write(self, vals):
        if 'input_lines' in vals:
            for x in vals['input_lines']:
                if x[0] == 1:
                    if 'employee_input_lines' in x[2]:
                        for y in x[2]['employee_input_lines']:
                            rule_category_id = self.env['employee.input.lines'].browse(y[1]).rule_category_id.id
                            employee_id = self.env['hr.payroll.input.line'].browse(x[1]).employee_id.id
                            if y[0] == 0:
                                existing_rule_input = self.rule_inputs.filtered(lambda l: l.rule_category_id.id == y[2]['rule_category_id'])
                                if existing_rule_input:
                                    existing_rule_input.lines.create({
                                        'employee_id': employee_id,
                                        'rule_input_id': existing_rule_input.id,
                                        'amount': y[2]['amount']
                                    })
                                else:
                                    created_rule_input = self.rule_inputs.create({
                                        'input_id': self.id,
                                        'rule_category_id': y[2]['rule_category_id']
                                    })
                                    created_rule_input.lines.create({
                                        'employee_id': employee_id,
                                        'rule_input_id': created_rule_input.id,
                                        'amount': y[2]['amount']
                                    })
                            if y[0] == 1:
                                if 'amount' in y[2]:
                                    self.rule_inputs.filtered(lambda l: l.rule_category_id.id == rule_category_id).mapped('lines').filtered(lambda l: l.employee_id.id == employee_id).write({'amount': y[2]['amount']})
                                if 'rule_category_id' in y[2]:
                                    amount = self.rule_inputs.filtered(lambda l: l.rule_category_id.id == rule_category_id).mapped('lines').filtered(lambda l: l.employee_id.id == employee_id).amount
                                    self.rule_inputs.filtered(lambda l: l.rule_category_id.id == rule_category_id).mapped('lines').filtered(lambda l: l.employee_id.id == employee_id).unlink()
                                    existing_rule_input = self.rule_inputs.filtered(lambda l: l.rule_category_id.id == y[2]['rule_category_id'])
                                    if existing_rule_input:
                                        existing_rule_input.lines.create({
                                            'input_id': existing_rule_input.id,
                                            'employee_id': employee_id,
                                            'amount': amount
                                        })
                                    else:
                                        created_rule_input = self.env['rule.input'].create({
                                            'input_id': self.id,
                                            'rule_category_id': y[2]['rule_category_id']
                                        })
                                        haha = created_rule_input.lines.create({
                                            'rule_input_id': created_rule_input.id,
                                            'employee_id': employee_id,
                                            'amount': amount
                                        })
                            if y[0] == 2:
                                self.rule_inputs.filtered(lambda l: l.rule_category_id.id == rule_category_id).mapped('lines').filtered(lambda l: l.employee_id.id == employee_id).unlink()
                    if 'employee_id' in x[2]:
                        self.rule_inputs.mapped('lines').filtered(lambda l: l.employee_id.id == self.env['hr.payroll.input.line'].browse(x[1]).employee_id.id).write({'employee_id': x[2]['employee_id']})
                if x[0] == 2:
                    employee_id = self.env['hr.payroll.input.line'].browse(x[1]).employee_id.id
                    self.env['rule.input.lines'].browse([line.id for rule_input in self.rule_inputs for line in rule_input.lines if line.employee_id.id == employee_id]).unlink()
                if x[0] == 0:
                    employee_id_to_create = x[2]['employee_id']
                    if 'employee_input_lines' in x[2]:
                        for rule_input in x[2]['employee_input_lines']:
                            existing_rule_inputs = self.rule_inputs.filtered(lambda l: l.rule_category_id.id == rule_input[2]['rule_category_id'])
                            if existing_rule_inputs:
                                existing_rule_inputs.lines.create({
                                    'rule_input_id': existing_rule_inputs.id,
                                    'employee_id': employee_id_to_create,
                                    'amount': rule_input[2]['amount']
                                })
                            else:
                                created_rule_input = self.rule_inputs.create({
                                    'rule_category_id': rule_input[2]['rule_category_id'],
                                    'input_id': self.id
                                })
                                created_rule_input.lines.create({
                                    'rule_input_id': created_rule_input.id,
                                    'employee_id': employee_id_to_create,
                                    'amount': rule_input[2]['amount']
                                })
                self.rule_inputs.filtered(lambda l: len(l.lines) == 0).unlink()
        if 'rule_inputs' in vals:
            for x in vals['rule_inputs']:
                if x[0] == 2:
                    rule_category_id = self.env['rule.input'].browse(x[1]).rule_category_id.id
                    self.env['employee.input.lines'].browse([line.id for input_line in self.input_lines for line in input_line.employee_input_lines if line.rule_category_id.id == rule_category_id]).unlink()
                if x[0] == 1:
                    for y in x[2]['lines']:
                        rule_category_id = self.env['rule.input'].browse(x[1]).rule_category_id.id
                        employee_id = self.env['rule.input.lines'].browse(y[1]).employee_id.id
                        if y[0] == 2:
                            self.input_lines.filtered(lambda l: l.employee_id.id == employee_id).mapped('employee_input_lines').filtered(lambda l: l.rule_category_id.id == rule_category_id).unlink()
                        if y[0] == 1:
                            self.input_lines.filtered(lambda l: l.employee_id.id == employee_id).mapped('employee_input_lines').filtered(lambda l: l.rule_category_id.id == rule_category_id).write(y[2])
                self.input_lines.filtered(lambda l: len(l.employee_input_lines) == 0).unlink()
        return super(HrPayrollInput, self).write(vals)


class HrPayrollInputLine(models.Model):
    _name = 'hr.payroll.input.line'
    _description = 'Payroll input line'

    @api.multi
    def _compute_amount(self):
        for record in self:
            record.amount = sum(line.amount for line in record.employee_input_lines)

    input_id = fields.Many2one('hr.payroll.input', string='Payroll input')
    employee_id = fields.Many2one('hr.employee', string='Employee')
    department_id = fields.Many2one(related='employee_id.department_id', string='Department', readonly=True)
    job_id = fields.Many2one(related='employee_id.job_id', string='Job', readonly=True)
    identification_id = fields.Char(related='employee_id.ssnid', string='Code', readonly=True)
    amount = fields.Float(string='Amount', digits=(3, 2), compute=_compute_amount)
    employee_input_lines = fields.One2many('employee.input.lines', 'input_id')


class InheritHrPayrollInput(models.Model):
    _inherit = 'hr.payroll.input'
    input_lines = fields.One2many('hr.payroll.input.line', 'input_id', string='Employees')
    rule_inputs = fields.One2many('rule.input', 'input_id', string='Employees')


class RuleInput(models.Model):
    _name = 'rule.input'

    @api.multi
    def _compute_amount(self):
        for record in self:
            record.amount = sum(line.amount for line in record.lines)

    input_id = fields.Many2one('hr.payroll.input', string='Payroll inputs')
    rule_category_id = fields.Many2one('hr.salary.rule.category', string='Rule category')
    rule_category_code = fields.Char(related='rule_category_id.code')
    amount = fields.Float('Amount', compute=_compute_amount)
    lines = fields.One2many('rule.input.lines', 'rule_input_id')


class RuleInputLines(models.Model):
    _name = 'rule.input.lines'

    rule_input_id = fields.Many2one('rule.input')
    employee_id = fields.Many2one('hr.employee')
    amount = fields.Float('Amount')


class EmployeeInputLines(models.Model):
    _name = 'employee.input.lines'

    input_id = fields.Many2one('hr.payroll.input.line', string='Payroll inputs')
    rule_category_id = fields.Many2one('hr.salary.rule.category', string='Deduction Type', domain=[('select_in_inputs', '=', True)])
    rule_category_code = fields.Char(related='rule_category_id.code')
    amount = fields.Float('Amount')
