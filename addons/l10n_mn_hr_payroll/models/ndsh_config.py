# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class HRNdishConfig(models.Model):
    _name = "hr.ndsh.config"
    _description = "NDSH Config"

    @api.multi
    def _total_ao(self):
        for config in self:
            total = 0
            total += config.tetgever_ao
            total += config.tetgemj_ao
            total += config.emd_ao
            total += config.ajilguidel_ao
            total += config.uo_ao
            config.total_ao = total

    @api.multi
    def _total_d(self):
        for config in self:
            total = 0
            total += config.tetgever_d
            total += config.tetgemj_d
            total += config.emd_d
            total += config.ajilguidel_d
            total += config.uo_d
            config.total_d = total

    @api.multi
    def _total(self):
        for config in self:
            total = 0
            total += config.tetgever_ao
            total += config.tetgemj_ao
            total += config.emd_ao
            total += config.ajilguidel_ao
            total += config.uo_ao
            total += config.tetgever_d
            total += config.tetgemj_d
            total += config.emd_d
            total += config.ajilguidel_d
            total += config.uo_d
            config.total = total

    code = fields.Char(string='ND Code')
    name = fields.Char(string='ND Type')
    compute_salary = fields.Selection([('salary', 'Salary'),
                                       ('minimum', 'Minimum'),
                                       ('olgovor', 'Olgovor'),
                                       ('juram', 'Rule'),
                                       ('average', 'Average')], string='Compute Salary')
    tetgever_ao = fields.Float(string='Tetgever AO')
    tetgever_d = fields.Float(string='Tetgever D')
    tetgemj_ao = fields.Float(string='Tetgemj AO')
    tetgemj_d = fields.Float(string='Tetgemj D')
    emd_ao = fields.Float(string='Emd AO')
    emd_d = fields.Float(string='Emd D')
    ajilguidel_ao = fields.Float(string='ajilguidel AO')
    ajilguidel_d = fields.Float(string='ajilguidel D')
    uo_ao = fields.Float(string='Uildverleliin osol AO')
    uo_d = fields.Float(string='Uildverleliin osol D')

    total_ao = fields.Float(string='Total AO', compute='_total_ao')
    total_d = fields.Float(string='Total D', compute='_total_d')
    total = fields.Float(string='Total', compute='_total')

    hhoat = fields.Float(string='Hhoat')
    hhoat_h = fields.Float(string='Hhoat h')
    auto_call_on_hour_balance = fields.Boolean(string='Is hour balance writable')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env['res.company']._company_default_get('hr.ndsh.config'), index=1)

    def init(self):
        companies = self.env['res.company'].search([])
        for company in companies:
            ndsh_config = self.env['hr.ndsh.config'].search([('company_id', '=', company.id)], limit=1)
            if not ndsh_config:
                self._cr.execute(
                    'INSERT INTO hr_ndsh_config(code, name, compute_salary, tetgever_ao, tetgever_d, tetgemj_ao, tetgemj_d, emd_ao, emd_d, ajilguidel_ao, ajilguidel_d, uo_ao, uo_d, hhoat, hhoat_h, company_id) '
                    'VALUES'
                    '(01, \'Энгийн ажилтан\', \'salary\', 7, 7, 0.80, 0.80, 2, 2, 0.20, 0.20, 1, 0, 10, 7000, %s), '
                    '(06, \'Хүүхдээ асарч буй эх\', \'minimum\', 7, 0, 0.80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, %s), '
                    '(08, \'Хугацаат цэргийн албан хаагч\', \'minimum\', 7, 0, 0.80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, %s), '
                    '(11, \'Цэрэг цагдаагийн ажилтан\', \'salary\', NULL, NULL, NULL, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, %s), '
                    '(14, \'Сул зогсогч олговортой\', \'olgovor\', 7, 7, 0.80, 0.80, 2, 2, 0.20, 0.20, 1, 0, NULL, NULL, %s), '
                    '(17, \'Жирэмсэн амаржсаны чөлөөлт эх\', \'minimum\', 7, 0, 0.80, 0, 2, 0, 0.20, 0, 1, 0, NULL, NULL, %s), '
                    '(19, \'Онцгой нөхцлөөр /Тусгай гэрээгээр/ 11-13 хувь төлөгч\', \'salary\', 7, 0, 0.80, 0, 2, 0, 0.20, 0, 1, 0, NULL, NULL, %s), '
                    '(20, \'Ажил олгогчийн захиалгаар суралцагч\', \'minimum\', 7, 0, 0.80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, %s), '
                    '(21, \'1 сараас дээш хугацаагаар ХЧАлдсан даатгуулагч\', \'minimum\', 7, NULL, 0.80, NULL, 2, NULL, 0.20, NULL, 1, NULL, NULL, NULL, %s), '
                    '(22, \'Тэтгэвэр тогтоолгосон ажиллагч\', \'salary\', 7, 7, 0.80, 0.80, NULL, NULL, NULL, NULL, 1, NULL, NULL, 7000, %s), '
                    '(23, \'Гадаадын иргэн Сертификатгүй\', \'salary\', 7, 7, 0.80, 0.80, 2, 2, 0.20, 0.20, 1, NULL, NULL, NULL, %s), '
                    '(24, \'Гадаадын иргэн Сертификаттай /Солонгос/\', \'salary\', NULL, NULL, 0.80, 0.80, 2, 2, 0.20, 0.20, 1, NULL, NULL, NULL, %s), '
                    '(25, \'7 сараас дээш улирлын чанартай байгууллагад ХГ-ээр ажиллагч /ажил зогссон үед/\', \'minimum\', 7, NULL, 0.80, NULL, 2, NULL, 0.20, NULL, 1, 0, NULL, NULL, %s), '
                    '(26, \'Хөдөлмөрийн гэрээгээр гадаад улсад ажиллаж буй МУ-ын иргэн /солонгос//\', \'juram\', 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, %s), '
                    '(28, \'Улсын хил дээр алба хааж буй гаалийн улсын байцаагчийн гэр бүл\', \'minimum\', 7, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, %s), '
                    '(29, \'Улсын хил дээр алба хааж буй офицер, ахлагчийн гэр бүл\', \'minimum\', 7, NULL, 0.80, NULL, 2, NULL, 0.20, NULL, 1, 0, NULL, NULL, %s), '
                    '(30, \'Дипломат төлөөлөгчийн газар ажиллагчийн гэр бүл\', \'avarage\', 7, 7, 0.80, 0.80, 2, 2, 0.20, 0.20, 1, 0, NULL, NULL, %s), '
                    '(31, \'Ажиллаагүй үедээ дайчлагдсан даатгуулагч\', \'minimum\', 7, NULL, 0.80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, %s), '
                    '(32, \'Хууль бусаар ажлаас зайлуулагдан мөрдөгдсөн, хилс хэргээр хоригдсон\', \'avarage\', 7, NULL, 0.80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, %s), '
                    '(33, \'Сул зогсогч олговоргүй\', \'minimum\', 7, NULL, 0.80, NULL, 2, NULL, 0.20, NULL, 1, 0, NULL, NULL, %s), '
                    '(34, \'Тэтгэвэр тогтоолгосон ажиллагч сул зогссон\', \'minimum\', 7, NULL, 0.80, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, %s), '
                    '(35, \'Цэрэг цагдаагийн ажилтан сул зогссон\', \'minimum\', NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, %s), '
                    '(37, \'Лам санваартан /хуулийн 4.2.1-ээс бусад/\', \'minimum\', 7, 7, 0.80, 0.80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, %s), '
                    '(38, \'Тэтгэвэр тогтоолгосон ажиллагч /ЖА чөлөөтэй/\', \'minimum\', 7, NULL, 0.80, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, %s), '
                    '(39, \'Тэтгэвэр тогтоолгосон ажиллагч /1 сараас дээш ХЧТАлдсан/\', \'minimum\', 7, NULL, 0.80, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, %s), '
                    '(40, \'Үндсэн ажлаас гадуур ажил гүйцэтгэгч /15А дэвтэртэй/\', \'salary\', 7, 7, 0.80, 0.80, 2, 2, 0.20, 0.20, 1, NULL, NULL, NULL, %s)' %
                    (company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id))


class Company(models.Model):
    _inherit = 'res.company'

    @api.model
    def create(self, vals):
        company = super(Company, self).create(vals)
        self._cr.execute(
            'INSERT INTO hr_ndsh_config(code, name, compute_salary, tetgever_ao, tetgever_d, tetgemj_ao, tetgemj_d, emd_ao, emd_d, ajilguidel_ao, ajilguidel_d, uo_ao, uo_d, hhoat, hhoat_h, company_id) '
            'VALUES'
            '(01, \'Энгийн ажилтан\', \'salary\', 7, 7, 0.80, 0.80, 2, 2, 0.20, 0.20, 1, 0, 10, 7000, %s), '
            '(06, \'Хүүхдээ асарч буй эх\', \'minimum\', 7, 0, 0.80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, %s), '
            '(08, \'Хугацаат цэргийн албан хаагч\', \'minimum\', 7, 0, 0.80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, %s), '
            '(11, \'Цэрэг цагдаагийн ажилтан\', \'salary\', NULL, NULL, NULL, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, %s), '
            '(14, \'Сул зогсогч олговортой\', \'olgovor\', 7, 7, 0.80, 0.80, 2, 2, 0.20, 0.20, 1, 0, NULL, NULL, %s), '
            '(17, \'Жирэмсэн амаржсаны чөлөөлт эх\', \'minimum\', 7, 0, 0.80, 0, 2, 0, 0.20, 0, 1, 0, NULL, NULL, %s), '
            '(19, \'Онцгой нөхцлөөр /Тусгай гэрээгээр/ 11-13 хувь төлөгч\', \'salary\', 7, 0, 0.80, 0, 2, 0, 0.20, 0, 1, 0, NULL, NULL, %s), '
            '(20, \'Ажил олгогчийн захиалгаар суралцагч\', \'minimum\', 7, 0, 0.80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, %s), '
            '(21, \'1 сараас дээш хугацаагаар ХЧАлдсан даатгуулагч\', \'minimum\', 7, NULL, 0.80, NULL, 2, NULL, 0.20, NULL, 1, NULL, NULL, NULL, %s), '
            '(22, \'Тэтгэвэр тогтоолгосон ажиллагч\', \'salary\', 7, 7, 0.80, 0.80, NULL, NULL, NULL, NULL, 1, NULL, NULL, 7000, %s), '
            '(23, \'Гадаадын иргэн Сертификатгүй\', \'salary\', 7, 7, 0.80, 0.80, 2, 2, 0.20, 0.20, 1, NULL, NULL, NULL, %s), '
            '(24, \'Гадаадын иргэн Сертификаттай /Солонгос/\', \'salary\', NULL, NULL, 0.80, 0.80, 2, 2, 0.20, 0.20, 1, NULL, NULL, NULL, %s), '
            '(25, \'7 сараас дээш улирлын чанартай байгууллагад ХГ-ээр ажиллагч /ажил зогссон үед/\', \'minimum\', 7, NULL, 0.80, NULL, 2, NULL, 0.20, NULL, 1, 0, NULL, NULL, %s), '
            '(26, \'Хөдөлмөрийн гэрээгээр гадаад улсад ажиллаж буй МУ-ын иргэн /солонгос//\', \'juram\', 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, %s), '
            '(28, \'Улсын хил дээр алба хааж буй гаалийн улсын байцаагчийн гэр бүл\', \'minimum\', 7, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, %s), '
            '(29, \'Улсын хил дээр алба хааж буй офицер, ахлагчийн гэр бүл\', \'minimum\', 7, NULL, 0.80, NULL, 2, NULL, 0.20, NULL, 1, 0, NULL, NULL, %s), '
            '(30, \'Дипломат төлөөлөгчийн газар ажиллагчийн гэр бүл\', \'avarage\', 7, 7, 0.80, 0.80, 2, 2, 0.20, 0.20, 1, 0, NULL, NULL, %s), '
            '(31, \'Ажиллаагүй үедээ дайчлагдсан даатгуулагч\', \'minimum\', 7, NULL, 0.80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, %s), '
            '(32, \'Хууль бусаар ажлаас зайлуулагдан мөрдөгдсөн, хилс хэргээр хоригдсон\', \'avarage\', 7, NULL, 0.80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, %s), '
            '(33, \'Сул зогсогч олговоргүй\', \'minimum\', 7, NULL, 0.80, NULL, 2, NULL, 0.20, NULL, 1, 0, NULL, NULL, %s), '
            '(34, \'Тэтгэвэр тогтоолгосон ажиллагч сул зогссон\', \'minimum\', 7, NULL, 0.80, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, %s), '
            '(35, \'Цэрэг цагдаагийн ажилтан сул зогссон\', \'minimum\', NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, %s), '
            '(37, \'Лам санваартан /хуулийн 4.2.1-ээс бусад/\', \'minimum\', 7, 7, 0.80, 0.80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, %s), '
            '(38, \'Тэтгэвэр тогтоолгосон ажиллагч /ЖА чөлөөтэй/\', \'minimum\', 7, NULL, 0.80, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, %s), '
            '(39, \'Тэтгэвэр тогтоолгосон ажиллагч /1 сараас дээш ХЧТАлдсан/\', \'minimum\', 7, NULL, 0.80, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, %s), '
            '(40, \'Үндсэн ажлаас гадуур ажил гүйцэтгэгч /15А дэвтэртэй/\', \'salary\', 7, 7, 0.80, 0.80, 2, 2, 0.20, 0.20, 1, NULL, NULL, NULL, %s)' %
            (company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id, company.id))
        return company
