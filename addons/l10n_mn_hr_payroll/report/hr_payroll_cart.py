# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError
import time
from datetime import date, timedelta, datetime
from io import BytesIO
import xlsxwriter
import base64


class HrPayslipCart(models.TransientModel):
    '''Цалингийн карт
    '''
    _name = "hr.payslip.cart"
    _description = "Hr Payslip cart"

    period_id = fields.Many2one('account.period', string='Period', required=True)  # default=_get_period
    employees = fields.Many2many('hr.employee', 'cart_payslip_employee_rel', 'report_id', 'emp_id', string='Employees')
    bank_id = fields.Many2one('res.bank', string='Bank')
    salary_type = fields.Selection([('last_salary', 'Last salary'),
                                    ('advance_salary', 'Advance salary')], string='Salary type', default='last_salary', states={'draft': [('readonly', False)]})
    payslip_run = fields.Many2one('hr.payslip.run', string='Hr Payslip Run')

    # @api.multi
    # def _get_period(self):
    #     company_id = False
    #     period_id = False
    #     now = time.strftime('%Y-%m-%d')
    #     company_id = self.env.user.company_id.id
    #     if company_id:
    #         period = self.env['account.period'].search([('date_start','<=',now),('date_stop','>=',now),('company_id','=',company_id)], limit=1)
    #         if period:
    #             period_id = period.id
    #     return period_id
    def get_first_letter_of_sign(self, sign, company=False):
        company = self.env.user.company_id if not company else company

        first_name = self.get_emp_name_from_sign(sign, company)[0]
        last_name_first_letter = self.get_emp_name_from_sign(sign, company)[1][:1] if len(
            self.get_emp_name_from_sign(sign, company)[1]) > 0 else ''

        return [first_name, last_name_first_letter]

    def get_emp_name_from_sign(self, sign, company=False):
        self.ensure_one()
        sign_user_id = False
        company = self.env.user.company_id if not company else company

        if sign == 'first' and company.first_sign:
            sign_user_id = company.first_sign.sudo().id
        elif sign == 'second' and company.second_sign_cart:
            sign_user_id = company.second_sign_cart.sudo().id
        elif sign == 'general_acc' and company.genaral_accountant_signature:
            sign_user_id = company.genaral_accountant_signature.sudo().id

        if sign_user_id:
            sign_employee = self.env['hr.employee'].search(
                [('company_id', '=', company.sudo().id), ('resource_id.user_id', '=', sign_user_id)], limit=1)
            return [sign_employee.sudo().name or '', sign_employee.sudo().last_name or '']

        return ['', '']

    @api.multi
    def export_cart(self):
        cr = self._cr
        ids = self.ids
        context = self._context or {}
        for template in self:

            output = BytesIO()
            workbook = xlsxwriter.Workbook(output)
            if template.salary_type == 'last_salary':
                stype = u'Сүүл цалин'
            else:
                stype = u'Урьдчилгаа цалин'

            file_name = 'payroll_%s_%s' % (template.period_id.name, stype) + '.xlsx'

            footer = workbook.add_format({'bold': 1, 'num_format': '#,###.##'})
            footer.set_font_size(8)
            footer.set_font_name('tahoma')
            footer.set_align('right')
            footer.set_border(style=1)
            footer.set_bg_color('#E6E6E6')

            # CELL styles тодорхойлж байна
            h1 = workbook.add_format({'bold': 1})
            h1.set_font_size(8)
            h1.set_font_name('tahoma')
            h1.set_align('left')
            h1.set_align('vcenter')

            theader = workbook.add_format()
            theader.set_font_size(8)
            theader.set_text_wrap()
            theader.set_align('center')
            theader.set_align('vcenter')
            theader.set_border(style=1)
            theader.set_bg_color('#E6E6E6')
            theader.set_font_name('tahoma')
            theader.set_align('vjustify')

            content_right = workbook.add_format({'num_format': '#,###.##'})
            content_right.set_text_wrap()
            content_right.set_border(style=1)
            content_right.set_font_size(8)
            content_right.set_align('right')
            content_right.set_font_name('tahoma')

            content_left = workbook.add_format()
            content_left.set_text_wrap()
            content_left.set_border(style=1)
            content_left.set_font_size(8)
            content_left.set_align('left')
            content_left.set_font_name('tahoma')

            content_center = workbook.add_format()
            content_center.set_text_wrap()
            content_center.set_border(style=1)
            content_center.set_font_size(8)
            content_center.set_align('center')
            content_center.set_font_name('tahoma')

            right = workbook.add_format()
            right.set_font_size(8)
            right.set_align('right')
            right.set_font_name('tahoma')

            right_bold = workbook.add_format({'bold': 1})
            right_bold.set_font_size(8)
            right_bold.set_align('right')
            right_bold.set_font_name('tahoma')

            left = workbook.add_format()
            left.set_font_size(8)
            left.set_align('left')
            left.set_font_name('tahoma')

            h2 = workbook.add_format({'bold': 1, 'italic': 1, 'underline': 1})
            h2.set_font_size(8)
            h2.set_align('left')
            h2.set_font_name('tahoma')

            h3 = workbook.add_format({'bold': 1})
            h3.set_text_wrap()
            h3.set_font_size(8)
            h3.set_align('center')
            h3.set_bottom()
            h3.set_font_name('tahoma')

            blank_line = workbook.add_format()
            blank_line.set_top()

            action_required = workbook.add_format()
            action_required.set_text_wrap()
            action_required.set_font_size(8)
            action_required.set_align('left')
            action_required.set_bg_color('#FE2E2E')
            action_required.set_font_name('tahoma')

            no_action_required = workbook.add_format()
            no_action_required.set_text_wrap()
            no_action_required.set_font_size(8)
            no_action_required.set_align('left')
            no_action_required.set_bg_color('#2EFE2E')
            no_action_required.set_font_name('tahoma')

            monitor_compartment = workbook.add_format()
            monitor_compartment.set_text_wrap()
            monitor_compartment.set_font_size(8)
            monitor_compartment.set_align('left')
            monitor_compartment.set_bg_color('#FFFF00')
            monitor_compartment.set_font_name('tahoma')

            # Ерөнхий мэдээллийн sheet нэмэх
            worksheet_general = workbook.add_worksheet(u'Цалингийн Карт')

            # Column өргөн
            # worksheet_general.set_column('A:A', 2)
            # worksheet_general.set_column('B:B', 5)
            # worksheet_general.set_column('C:C', 16)
            # worksheet_general.set_column('D:D', 20)
            # worksheet_general.set_column('E:E', 16)
            # worksheet_general.set_column('F:F', 4)
            # worksheet_general.set_column('G:G', 5)
            # worksheet_general.set_column('H:H', 16)
            # worksheet_general.set_column('I:I', 16)
            columns = {'1': 'A', '2': 'B', '3': 'C', '4': 'D', '5': 'E', '6': 'F', '7': 'G', '8': 'H', '9': 'I', '10': 'J', '11': 'K', '12': 'L', '13': 'M',
                       '14': 'N', '15': 'O', '16': 'P', '17': 'Q', '18': 'R', '19': 'S', '20': 'T', '21': 'U', '22': 'V', '23': 'W', '24': 'X', '25': 'Y', '26': 'Z',
                       '27': 'AA', '28': 'AB', '29': 'AC', '30': 'AD', '31': 'AE', '32': 'AF', '33': 'AG', '34': 'AH', '35': 'AI', '36': 'AJ', '37': 'AK', '38': 'AL', '39': 'AM',
                       '40': 'AN', '41': 'AO', '42': 'AP', '43': 'AQ', '44': 'AR', '45': 'AS', '46': 'AT', '47': 'AU', '48': 'AV', '49': 'AW', '50': 'AX', '51': 'AY', '52': 'AZ',
                       '53': 'BA', '54': 'BB', '55': 'BC', '56': 'BD', '57': 'BE', '58': 'BF', '59': 'BG', '60': 'BH', '61': 'BI', '62': 'BJ', '63': 'BK', '64': 'BL', '65': 'BM',
                       '66': 'BN', '67': 'BO', '68': 'BP', '69': 'BQ', '70': 'BR', '71': 'BS', '72': 'BT', '73': 'BU', '74': 'BV', '75': 'BW', '76': 'BX', '77': 'BY', '78': 'BZ',
                       '79': 'CA', '80': 'CB', '81': 'CC', '82': 'CD', '83': 'CE', '84': 'CF', '85': 'CG', '86': 'CH', '87': 'CI', '88': 'CJ', '89': 'CK', '90': 'CL', '91': 'CM',
                       '92': 'CN', '93': 'CO', '94': 'CP', '95': 'CQ', '96': 'CR', '97': 'CS', '98': 'CT', '99': 'CU', '100': 'CV', '101': 'CW', '102': 'CX', '103': 'CY', '104': 'CZ', }

            irow = 0
            icol = 1

            if not template.payslip_run:
                company = False
                for user in self.env['res.users'].browse(self._uid):
                    company = user.company_id
                    if company:
                        if template.salary_type == 'last_salary':
                            stype = u'Сүүл цалин'
                        else:
                            stype = u'Урьдчилгаа цалин'
                        worksheet_general.write(irow, icol, u"%s %s %s" % (company.name, template.period_id.name, stype), h1)
                        worksheet_general.set_row(irow, 40)
                        irow += 2
                        if not company.second_sign_cart:
                            raise UserError(_("Компаний нэгдүгээр цалингийн гарын үсэг тодорхойгүй байна"))
                        lname = ''
                        if company.first_sign.last_name:
                            lname = company.first_sign.last_name[:1]
                        worksheet_general.write(irow, 2, u'Баталсан: ........................ %s.%s' % (lname, company.first_sign.name), h1)
                        irow += 1
                        if template.bank_id:
                            bank_name = ''
                            bank_account = ''
                            for bank in company.bank_account_lines:
                                if bank.bank_id.id == template.bank_id.id:
                                    bank_name = bank.bank_id.name
                                    bank_account = bank.bank_account_id.acc_number
                            worksheet_general.write(irow + 1, icol, u"%s Данс: %s" % (bank_name, bank_account), left)
                        worksheet_general.write(irow + 1, icol + 3, u"", right)
                        irow += 1
            else:
                worksheet_general.write(irow, icol + 1, u"%s %s" % (template.payslip_run.company_id.name, template.payslip_run.name), h1)
                worksheet_general.set_row(irow, 40)
                irow += 2
                worksheet_general.write(irow, 2, u'Баталсан: ........................ %s.%s' % (self.get_emp_name_from_sign('first', template.payslip_run.company_id)[1], self.get_emp_name_from_sign('first', template.payslip_run.company_id)[0]), h1)
                irow += 1
                if template.bank_id:
                    bank_name = ''
                    bank_account = ''
                    for bank in template.payslip_run.company_id.bank_account_lines:
                        if bank.bank_id.id == template.bank_id.id:
                            bank_name = bank.bank_id.name
                            bank_account = bank.bank_account_id.acc_number
                    worksheet_general.write(irow + 1, icol, u"%s Данс: %s" % (bank_name, bank_account), left)
                worksheet_general.write(irow + 1, icol + 3, u"%s" % (template.payslip_run.compute_date), right)
                irow += 1

            payroll_dict = {}
            payroll_dict['number'] = {'name': u'Д/д', 'code': 'DD', 'col': 4, 'icol': 0}
            payroll_dict['code'] = {'name': u'Код', 'code': 'CODE', 'col': 10, 'icol': 1}
            payroll_dict['employee_lname'] = {'name': u'Ажилтны овог', 'code': 'EMPLOYEE_LNAME', 'col': 20, 'icol': 2}
            payroll_dict['employee'] = {'name': u'Ажилтны нэр', 'code': 'EMPLOYEE', 'col': 20, 'icol': 3}
            payroll_dict['net'] = {'name': u'Гарт олгох', 'code': 'employee', 'col': 12, 'icol': 4}
            payroll_dict['account'] = {'name': u'Дансны дугаар', 'code': 'ACCOUNT', 'col': 20, 'icol': 5}
            irow = irow + 2
            row = irow + 1
            icol = 0
            worksheet_general.set_row(irow, 40)
            worksheet_general.write(irow, icol, payroll_dict['number']['name'], theader)
            worksheet_general.set_column(icol, icol, payroll_dict['number']['col'])
            worksheet_general.write(irow, icol + 1, payroll_dict['code']['name'], theader)
            worksheet_general.set_column(icol + 1, icol + 1, payroll_dict['code']['col'])
            worksheet_general.write(irow, icol + 2, payroll_dict['employee_lname']['name'], theader)
            worksheet_general.set_column(icol + 2, icol + 2, payroll_dict['employee']['col'])
            worksheet_general.write(irow, icol + 3, payroll_dict['employee']['name'], theader)
            worksheet_general.set_column(icol + 3, icol + 3, payroll_dict['employee']['col'])
            worksheet_general.write(irow, icol + 4, payroll_dict['net']['name'], theader)
            worksheet_general.set_column(icol + 4, icol + 4, payroll_dict['net']['col'])
            worksheet_general.write(irow, icol + 5, payroll_dict['account']['name'], theader)
            worksheet_general.set_column(icol + 5, icol + 5, payroll_dict['account']['col'])
            row = irow + 1
            icol = 0
            payslip_run = ()
            if not template.payslip_run:
                emp_ids = []
                count_emp = 0
                count_not_null = 0
                if template.employees:
                    for emp in template.employees:
                        emp_ids.append(emp.id)

                    empids = str(emp_ids)
                    empids = empids.replace("[", "(")
                    empids = empids.replace("]", ")")
                    cr.execute('''SELECT id FROM hr_employee WHERE id IN ''' + empids + ''' ORDER BY name_related ASC, identification_id ASC''')
                    employees = cr.fetchall()
                    emp_ids = []
                    for employee in employees:
                        emp_ids.append(employee[0])
                    hr_emp_ids = str(emp_ids)
                    hr_emp_ids = hr_emp_ids.replace("[", "(")
                    hr_emp_ids = hr_emp_ids.replace("]", ")")
                    cr.execute('''SELECT id FROM hr_employee WHERE id IN ''' + hr_emp_ids + ''' ORDER BY name_related ASC,last_name ASC, identification_id ASC''')
                    emps = cr.fetchall()
                    employees = []

                    for employee in emps:
                        employees.append(employee[0])
                    for emp in self.env['hr.employee'].browse(employees):
                        slips = self.env['hr.payslip'].search([('period_id', '=', template.period_id.id), ('salary_type', '=', template.salary_type), ('employee_id', '=', emp.id), ('freeze_salary', '=', False)])
                        icol = 2
                        for slip in slips:
                            if template.bank_id:
                                if slip.employee_id.contract_id.bank_account_id.bank_id.id == template.bank_id.id:
                                    worksheet_general.write(row, payroll_dict['number']['icol'], row - 6, content_right)
                                    worksheet_general.write(row, payroll_dict['code']['icol'], '%s' % (slip.employee_id.ssnid or slip.employee_id.identification_id), content_right)
                                    worksheet_general.write(row, payroll_dict['employee_lname']['icol'], '%s' % (slip.employee_id.last_name or ''), content_left)
                                    worksheet_general.write(row, payroll_dict['employee']['icol'], '%s' % (slip.employee_id.name), content_left)
                                    worksheet_general.write(row, payroll_dict['account']['icol'], '%s' % (slip.employee_id.contract_id.bank_account_id.acc_number), content_right)
                                    for lines in slip.line_ids:
                                        if lines.code == 'NET':
                                            worksheet_general.write(row, payroll_dict['net']['icol'], lines.amount, content_right)
                                            if lines.amount > 0:
                                                count_not_null += 1
                                    count_emp += 1
                                    row += 1
                            else:
                                worksheet_general.write(row, payroll_dict['number']['icol'], row - 6, content_right)
                                worksheet_general.write(row, payroll_dict['code']['icol'], '%s' % (slip.employee_id.ssnid or slip.employee_id.identification_id), content_right)
                                worksheet_general.write(row, payroll_dict['employee_lname']['icol'], '%s' % (slip.employee_id.last_name or ''), content_left)
                                worksheet_general.write(row, payroll_dict['employee']['icol'], '%s' % (slip.employee_id.name), content_left)
                                worksheet_general.write(row, payroll_dict['account']['icol'], (slip.employee_id.contract_id.bank_account_id.acc_number or ' '), content_right)
                                for lines in slip.line_ids:
                                    if lines.code == 'NET':
                                        worksheet_general.write(row, payroll_dict['net']['icol'], lines.amount, content_right)
                                        if lines.amount > 0:
                                            count_not_null += 1
                                count_emp += 1
                                row += 1
                else:
                    slips = self.env['hr.payslip'].search([('period_id', '=', template.period_id.id), ('salary_type', '=', template.salary_type), ('freeze_salary', '=', False)])
                    icol = 2
                    for slip in slips:
                        worksheet_general.write(row, payroll_dict['number']['icol'], row - 6, content_right)
                        worksheet_general.write(row, payroll_dict['code']['icol'], '%s' % (slip.employee_id.ssnid or slip.employee_id.identification_id), content_right)
                        worksheet_general.write(row, payroll_dict['employee_lname']['icol'], '%s' % (slip.employee_id.last_name or ''), content_left)
                        worksheet_general.write(row, payroll_dict['employee']['icol'], '%s' % (slip.employee_id.name), content_left)
                        worksheet_general.write(row, payroll_dict['account']['icol'], '%s' % (slip.employee_id.contract_id.bank_account_id.acc_number or ' '), content_right)
                        for lines in slip.line_ids:
                            if lines.code == 'NET':
                                worksheet_general.write(row, payroll_dict['net']['icol'], lines.amount, content_right)
                                if lines.amount > 0:
                                    count_not_null += 1
                        count_emp += 1
                        row += 1
                        
                worksheet_general.write(row, 0, '', footer)
                worksheet_general.write(row, 1, '', footer)
                worksheet_general.write(row, 2, u'Цалин олгох: ' + str(count_not_null), footer)
                worksheet_general.write(row, 3, u'Нийт дүн', footer)
                worksheet_general.write_formula(row, 4, '{=SUM(E8:E' + str(count_emp + 7) + ')}', footer)
                worksheet_general.write(row, 5, '', footer)
                if not company.second_sign_cart:
                    raise UserError(_("Компаний хоёрдугаар гарын үсэг тодорхойгүй байна"))
                lname = ''
                if company.genaral_accountant_signature.last_name:
                    lname = company.genaral_accountant_signature.last_name[:1]
                worksheet_general.write(row + 2, 2, u'Ерөнхий нягтлан бодогч: ........................ %s.%s' % (lname, company.genaral_accountant_signature.name), h1)
                user_emp = self.env['hr.employee'].search([('user_id', '=', self._uid)])
                lname = ''
                if user_emp.last_name:
                    lname = user_emp.last_name[:1]
                worksheet_general.write(row + 4, 2, u'Цалин бодсон: ........................ %s.%s' % (lname, user_emp.name), h1)

            else:
                icol = 2
                count_emp = 0
                count_not_null = 0
                for slip in template.payslip_run.slip_ids:
                    if not slip.freeze_salary:
                        if template.bank_id:
                            if slip.employee_id.contract_id.bank_account_id.bank_id.id == template.bank_id.id:
                                worksheet_general.write(row, payroll_dict['number']['icol'], row - 6, content_right)
                                worksheet_general.write(row, payroll_dict['code']['icol'], '%s' % (slip.employee_id.ssnid or slip.employee_id.identification_id), content_right)
                                worksheet_general.write(row, payroll_dict['employee_lname']['icol'], '%s' % (slip.employee_id.last_name or ''), content_left)
                                worksheet_general.write(row, payroll_dict['employee']['icol'], '%s' % (slip.employee_id.name), content_left)
                                worksheet_general.write(row, payroll_dict['account']['icol'], '%s' % (slip.employee_id.contract_id.bank_account_id.acc_number), content_right)
                                for lines in slip.line_ids:
                                    if lines.code == 'NET':
                                        worksheet_general.write(row, payroll_dict['net']['icol'], lines.amount, content_right)
                                        if lines.amount > 0:
                                            count_not_null += 1
                                        count_emp += 1
                                row += 1
                        else:
                            if not slip.employee_id.contract_id.bank_account_id:
                                worksheet_general.write(row, payroll_dict['number']['icol'], row - 6, content_right)
                                worksheet_general.write(row, payroll_dict['code']['icol'], '%s' % (slip.employee_id.ssnid or slip.employee_id.identification_id), content_right)
                                worksheet_general.write(row, payroll_dict['employee_lname']['icol'], '%s' % (slip.employee_id.last_name or ''), content_left)
                                worksheet_general.write(row, payroll_dict['employee']['icol'], '%s' % (slip.employee_id.name), content_left)
                                worksheet_general.write(row, payroll_dict['account']['icol'], '%s' % _('No account'), content_right)
                                for lines in slip.line_ids:
                                    if lines.code == 'NET':
                                        worksheet_general.write(row, payroll_dict['net']['icol'], lines.amount, content_right)
                                        if lines.amount > 0:
                                            count_not_null += 1
                                        count_emp += 1
                                row += 1

                worksheet_general.write(row, 0, '', footer)
                worksheet_general.write(row, 1, '', footer)
                worksheet_general.write(row, 2, u'Цалин олгох: ' + str(count_not_null), footer)
                worksheet_general.write(row, 3, u'Нийт дүн', footer)
                worksheet_general.write_formula(row, 4, '{=SUM(E8:E' + str(count_emp + 7) + ')}', footer)
                worksheet_general.write(row, 5, '', footer)
                if not template.payslip_run.company_id.second_sign_cart:
                    raise UserError(_("Компаний хоёрдугаар гарын үсэг тодорхойгүй байна"))
                # lname = ''
                # if template.payslip_run.company_id.second_sign_cart.last_name:
                #     lname = template.payslip_run.company_id.second_sign_cart.last_name[:1]
                # worksheet_general.write(row + 2, 2, u'Ерөнхий нягтлан бодогчссс: ........................ %s.%s' % (lname, template.payslip_run.create_employee.name), h1)
                if template.payslip_run.company_id.second_sign_cart:
                    if self.get_emp_name_from_sign('general_acc', template.payslip_run.company_id)[1]:
                        if self.get_emp_name_from_sign('general_acc', template.payslip_run.company_id)[0]:
                            worksheet_general.write(row + 2, 2, u'Ерөнхий нягтлан бодогч ........................ %s.%s' % (self.get_first_letter_of_sign('general_acc', template.payslip_run.company_id)[1], self.get_emp_name_from_sign('general_acc', template.payslip_run.company_id)[0]), h1)
                lname = ''
                if template.payslip_run.create_employee.last_name:
                    lname = template.payslip_run.create_employee.last_name[:1]
                worksheet_general.write(row + 4, 2, u'Цалин бодсон: ........................ %s.%s' % (lname, template.payslip_run.create_employee.name), h1)
            workbook.close()

            out = base64.encodestring(output.getvalue())
            excel_id = self.env['oderp.report.excel.output'].create({'filedata': out, 'filename': file_name})

            return {
                'name': 'Export Result',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'oderp.report.excel.output',
                'res_id': excel_id.id,
                'view_id': False,
                'context': context,
                'type': 'ir.actions.act_window',
                'target': 'new',
                'nodestroy': True,
            }
