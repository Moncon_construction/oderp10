# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from io import BytesIO
import base64
import xlsxwriter
from odoo import api, fields, models

class HrPayslipHctaReport(models.TransientModel):
    '''ХЧТА тайлан
    '''
    _name = "hr.payslip.hcta.report"
    _description = "Hr Payslip Hcta Report"

    period_id = fields.Many2one('account.period', string='Period', index=True, invisible=False, required=True)
    employee_ids = fields.Many2many('hr.employee', 'report_payslip_employee_rel', 'report_id', 'emp_id', string='Employees')
    payslip_run = fields.Many2one('hr.payslip.run', 'Hr Payslip Run', required=True)

    @api.multi
    def export_report(self):
        for template in self:

            output = BytesIO()
            workbook = xlsxwriter.Workbook(output)
            stype = u'хөдөлмөрийн чадвар түр алдсаны тэтгэмж'
            file_name = 'payroll_%s_%s' % (template.period_id.name, stype) + '.xlsx'

            # CELL styles тодорхойлж байна
            h1 = workbook.add_format({'bold': 1})
            h1.set_font_size(14)
            h1.set_font_name('Times New Roman')
            h1.set_align('center')
            h1.set_align('vcenter')
            h1.set_text_wrap()

            footer = workbook.add_format({'bold': 1,'num_format': '#,###.##'})
            footer.set_font_size(12)
            footer.set_font_name('Times New Roman')
            footer.set_align('right')
            footer.set_border(style=1)
            footer.set_bg_color('#83caff')

            theader = workbook.add_format()
            theader.set_font_size(12)
            theader.set_text_wrap()
            theader.set_align('center')
            theader.set_align('vcenter')
            theader.set_border(style=1)
            theader.set_font_name('Times New Roman')
            theader.set_bg_color('#83caff')

            theader1 = workbook.add_format()
            theader1.set_font_size(12)
            theader1.set_text_wrap()
            theader1.set_align('center')
            theader1.set_align('vcenter')
            theader1.set_border(style=1)
            theader1.set_font_name('Times New Roman')
            theader1.set_bg_color('#cce7fc')

            theader_rotate = workbook.add_format()
            theader_rotate.set_font_size(12)
            theader_rotate.set_text_wrap()
            theader_rotate.set_align('center')
            theader_rotate.set_align('vcenter')
            theader_rotate.set_border(style=1)
            theader_rotate.set_font_name('Times New Roman')
            theader_rotate.set_rotation(90)
            theader_rotate.set_bg_color('#83caff')

            content_right = workbook.add_format({'num_format': '#,###.##'})
            content_right.set_text_wrap()
            content_right.set_border(style=1)
            content_right.set_font_size(10)
            content_right.set_align('right')
            content_right.set_font_name('Times New Roman')

            content_left = workbook.add_format()
            content_left.set_text_wrap()
            content_left.set_border(style=1)
            content_left.set_font_size(10)
            content_left.set_align('left')
            content_left.set_font_name('Times New Roman')

            content_left_noborder = workbook.add_format()
            content_left_noborder.set_text_wrap()
            content_left_noborder.set_font_size(12)
            content_left_noborder.set_align('left')
            content_left_noborder.set_font_name('Times New Roman')

            left_bold = workbook.add_format({'bold': 1})
            left_bold.set_font_size(12)
            left_bold.set_align('left')
            left_bold.set_font_name('Times New Roman')

            # Ерөнхий мэдээллийн sheet нэмэх
            worksheet_general = workbook.add_worksheet(u'Цалингийн Хүснэгт')
            worksheet_general.set_landscape()
            worksheet_general.set_paper(9)  # A4
            worksheet_general.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
            worksheet_general.fit_to_pages(1, 0)
            worksheet_general.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})

            columns = {'1':'A', '2':'B','3':'C','4':'D','5':'E','6':'F','7':'G','8':'H','9':'I','10':'J','11':'K','12':'L','13':'M',
                '14':'N', '15':'O','16':'P','17':'Q','18':'R','19':'S','20':'T','21':'U','22':'V','23':'W','24':'X','25':'Y','26':'Z',
                '27':'AA', '28':'AB','29':'AC','30':'AD','31':'AE','32':'AF','33':'AG','34':'AH','35':'AI','36':'AJ','37':'AK','38':'AL','39':'AM',
                '40':'AN', '41':'AO','42':'AP','43':'AQ','44':'AR','45':'AS','46':'AT','47':'AU','48':'AV','49':'AW','50':'AX','51':'AY','52':'AZ',
                '53':'BA', '54':'BB','55':'BC','56':'BD','57':'BE','58':'BF','59':'BG','60':'BH','61':'BI','62':'BJ','63':'BK','64':'BL','65':'BM',
                '66':'BN', '67':'BO','68':'BP','69':'BQ','70':'BR','71':'BS','72':'BT','73':'BU','74':'BV','75':'BW','76':'BX','77':'BY','78':'BZ',
                '79':'CA', '80':'CB','81':'CC','82':'CD','83':'CE','84':'CF','85':'CG','86':'CH','87':'CI','88':'CJ','89':'CK','90':'CL','91':'CM',
                '92':'CN', '93':'CO','94':'CP','95':'CQ','96':'CR','97':'CS','98':'CT','99':'CU','100':'CV','101':'CW','102':'CX','103':'CY','104':'CZ',}

            irow = 0
            icol = 1
            worksheet_general.merge_range('C1:O2', u"%s oны %s-р сар / улирал/ -д хөдөлмөрийн чадвар түр  алдсаны тэтгэмж олгосон жагсаалт " % (template.payslip_run.company_id.name.upper(),template.period_id.name.upper()), h1)
            irow += 1
            worksheet_general.write(irow, icol+18,u'НД-10',left_bold)
            irow += 2
            if template.payslip_run:
                icol = 1
                count = 1
                worksheet_general.set_row(irow, 70)
                worksheet_general.set_row(irow+1, 107)
                worksheet_general.merge_range('A' + str(irow+1) + ':A' + str(irow+2), u'№', theader)
                worksheet_general.set_column(icol-1,icol-1, 3)

                worksheet_general.merge_range('B' + str(irow+1) + ':B' + str(irow+2), u'Даатгуулагчийн нэр, овог', theader)
                worksheet_general.set_column(icol,icol, 18)

                worksheet_general.merge_range('C' + str(irow+1) + ':C' + str(irow+2), u'Нийгмийн даатгалын дэвтрийн дугаар', theader_rotate)
                worksheet_general.set_column(icol+1,icol+1, 10)

                worksheet_general.merge_range('D' + str(irow+1) + ':F' + str(irow+1), u'Эмнэлгийн хуудас', theader)

                worksheet_general.write(irow+1,icol+2, u'Дугаар', theader_rotate)
                worksheet_general.set_column(icol+2,icol+2, 8)

                worksheet_general.write(irow+1,icol+3, u'Эхэлсэн он, сар, өдөр', theader_rotate)
                worksheet_general.set_column(icol+3,icol+3, 8)

                worksheet_general.write(irow+1,icol+4, u'Дууссан он, сар, өдөр', theader_rotate)
                worksheet_general.set_column(icol+4,icol+4, 8)

                worksheet_general.merge_range('G' + str(irow+1) + ':G' + str(irow+2), u'Шимтгэл төлсөн нийт жил', theader_rotate)
                worksheet_general.set_column(icol+5, icol+5, 7)

                worksheet_general.merge_range('H' + str(irow+1) + ':H' + str(irow+2), u'Сүүлийн бүтэн ажилласан 3 сарын хөдөлмөрийн дундаж хөлс', theader_rotate)
                worksheet_general.set_column(icol+6,icol+6, 10)

                worksheet_general.merge_range('I' + str(irow+1) + ':K' + str(irow+1), u'Тэтгэмж бодох', theader)
                worksheet_general.write(irow+1,icol+7, u'Ажилласан хоног', theader_rotate)
                worksheet_general.set_column(icol+7,icol+7, 6)
                worksheet_general.write(irow+1,icol+8, u'Ажлын нэг өдөрт ногдох хөдөлмөрийн хөлс', theader_rotate)
                worksheet_general.set_column(icol+8,icol+8, 10)
                worksheet_general.write(irow+1,icol+9, u'Хувь', theader_rotate)
                worksheet_general.set_column(icol+9,icol+9, 6)
                worksheet_general.merge_range('L' + str(irow+1) + ':L' + str(irow+2), u'Ажлын нэг өдөрт ногдох тэтгэмж (7x8)', theader_rotate)
                worksheet_general.set_column(icol+10,icol+10, 8)
                worksheet_general.merge_range('M' + str(irow+1) + ':O' + str(irow+1), u'ХЧТА-ны тэтгэмж олгох хоног', theader)
                worksheet_general.write(irow+1,icol+11, u'Бүгд', theader_rotate)
                worksheet_general.set_column(icol+10,icol+10, 5)
                worksheet_general.write(irow+1,icol+12, u'Ажил олгогчоос', theader_rotate)
                worksheet_general.set_column(icol+11,icol+11, 5)
                worksheet_general.write(irow+1,icol+13, u'Тэтгэмжийн даатгалын сангаас', theader_rotate)
                worksheet_general.set_column(icol+13,icol+13, 5)
                worksheet_general.merge_range('P' + str(irow+1) + ':R' + str(irow+1), u'Олгох тэтгэмж үүнээс', theader)
                worksheet_general.write(irow+1,icol+14, u'Бүгд (9x10)', theader_rotate)
                worksheet_general.set_column(icol+14,icol+14, 10)
                worksheet_general.write(irow+1,icol+15, u'Ажил олгогчоос', theader_rotate)
                worksheet_general.set_column(icol+15,icol+15, 10)
                worksheet_general.write(irow+1,icol+16, u'Тэтгэмжийн даатгалын сангаас', theader_rotate)
                worksheet_general.set_column(icol+16,icol+16, 10)
                worksheet_general.merge_range('S' + str(irow+1) + ':T' + str(irow+2), u'Нийгмийн даатгалын байгууллагын шалгалтаар илэрсэн зөрчлийн товч утга', theader)
                worksheet_general.set_column(icol+17,icol+17, 10)

                irow += 2
                worksheet_general.write(irow,icol-1,'',theader1)
                worksheet_general.write(irow,icol,u'А',theader1)
                worksheet_general.write(irow,icol+1,'1',theader1)
                worksheet_general.write(irow,icol+2,'2',theader1)
                worksheet_general.write(irow,icol+3,'3',theader1)
                worksheet_general.write(irow,icol+4,'4',theader1)
                worksheet_general.write(irow,icol+5,'5',theader1)
                worksheet_general.write(irow,icol+6,'6',theader1)
                worksheet_general.write(irow,icol+7,'7',theader1)
                worksheet_general.write(irow,icol+8,'8',theader1)
                worksheet_general.write(irow,icol+9,'9',theader1)
                worksheet_general.write(irow,icol+10,'10',theader1)
                worksheet_general.write(irow,icol+11,'11',theader1)
                worksheet_general.write(irow,icol+12,'12',theader1)
                worksheet_general.write(irow,icol+13,'13',theader1)
                worksheet_general.write(irow,icol+14,'14',theader1)
                worksheet_general.write(irow,icol+15,'15',theader1)
                worksheet_general.write(irow,icol+16,'16',theader1)
                worksheet_general.merge_range(irow,icol+17,irow,icol+18,'17',theader1)

                irow += 1
                for slip in template.payslip_run.slip_ids:
                    worksheet_general.write(irow, icol-1, count,content_right)
                    worksheet_general.write(irow, icol, '%s %s' % (slip.employee_id.name, slip.employee_id.last_name),content_left)
                    worksheet_general.write(irow, icol+1, slip.employee_id.sinid,content_left)
                    worksheet_general.write(irow, icol+2, slip.nd_number,content_left)
                    worksheet_general.write(irow, icol+3, slip.start_date,content_left)
                    worksheet_general.write(irow, icol+4, slip.end_date,content_left)
                    worksheet_general.write(irow, icol+5, slip.employee_id.gov_year,content_left)
                    percent = 0
                    day = 0
                    average_salary = 0
                    hchta_all_day = 0
                    for line in slip.line_ids:
                        if line.code == 'HCHTA_AMOUNT':
                            worksheet_general.write(irow, icol+6,line.amount ,content_right)
                        if line.code == 'HCHTA_ALL_DAY':
                            worksheet_general.write(irow, icol+7,line.amount ,content_right)
                            hchta_all_day = line.amount
                        if line.code == 'HCHTA_AVERAGE_SALARY':
                            worksheet_general.write(irow, icol+8, line.amount, content_right)
                            average_salary = line.amount
                        if line.code == 'HCHTA_PERCENT':
                            worksheet_general.write(irow, icol+9,line.amount ,content_right)
                            percent = line.amount
                        if line.code == 'HCHTA_NET':
                            worksheet_general.write(irow, icol+14, line.amount, content_right)
                        if line.code == 'HCHTA_COMPANY':
                            worksheet_general.write(irow, icol+15,line.amount ,content_right)
                        if line.code == 'HCHTA_ND':
                            worksheet_general.write(irow, icol+16, line.amount, content_right)
                    oneday_salary = average_salary/100 * percent
                    worksheet_general.write(irow, icol+10, oneday_salary, content_right)
                    worksheet_general.write(irow, icol+11, slip.list_day ,content_right)
                    worksheet_general.write(irow, icol+12, slip.from_company, content_right)
                    pay_day = slip.list_day-slip.from_company
                    worksheet_general.write(irow, icol+13, pay_day,content_right)
                    worksheet_general.merge_range(irow, icol+17, irow, icol+18,'' ,content_right)
                    count += 1
                    irow +=1
                worksheet_general.write(irow, icol-1,'' ,footer)
                worksheet_general.write(irow, icol,u'НИЙТ' ,footer)
                worksheet_general.write(irow, icol+1,'' ,footer)
                worksheet_general.write(irow, icol+2,'' ,footer)
                worksheet_general.write(irow, icol+3,'' ,footer)
                worksheet_general.write(irow, icol+4,'' ,footer)
                worksheet_general.write_formula(irow, icol+5, '{=SUM(G6:G' +str(irow)+')}',footer)
                worksheet_general.write_formula(irow, icol+6, '{=SUM(H6:H' +str(irow)+')}',footer)
                worksheet_general.write_formula(irow, icol+7, '{=SUM(I6:I' +str(irow)+')}',footer)
                worksheet_general.write_formula(irow, icol+8,'{=SUM(J6:J' +str(irow)+')}' ,footer)
                worksheet_general.write_formula(irow, icol+9,'{=SUM(K6:K' +str(irow)+')}' ,footer)
                worksheet_general.write_formula(irow, icol+10, '{=SUM(L6:L' +str(irow)+')}',footer)
                worksheet_general.write_formula(irow, icol+11, '{=SUM(M6:M' +str(irow)+')}',footer)
                worksheet_general.write_formula(irow, icol+12, '{=SUM(N6:N' +str(irow)+')}',footer)
                worksheet_general.write_formula(irow, icol+13, '{=SUM(O6:O' +str(irow)+')}',footer)
                worksheet_general.write_formula(irow, icol+14, '{=SUM(P6:P' +str(irow)+')}',footer)
                worksheet_general.write_formula(irow, icol+15, '{=SUM(Q6:Q' +str(irow)+')}',footer)
                worksheet_general.write_formula(irow, icol+16,'{=SUM(R6:R' +str(irow)+')}',footer)
                worksheet_general.merge_range(irow, icol+17,irow, icol+18,'' ,footer)

                irow += 3
                worksheet_general.merge_range('B' + str(irow) + ':C' + str(irow), u'Дарга:...............................', content_left_noborder)
                worksheet_general.merge_range('B' + str(irow+1) + ':C' + str(irow+1), u'Ня-бо:..............................' , content_left_noborder)
                worksheet_general.merge_range('B' + str(irow+2) + ':C' + str(irow+2), u'20.... Оны ... Сарын ......- ны өдөр' , content_left_noborder)
                worksheet_general.merge_range('L' + str(irow) + ':Q' + str(irow), u'Шалгах хүлээж авсан Нийгмийн даатгалын хэлтэс' , content_left_noborder)
                worksheet_general.merge_range('L' + str(irow+1) + ':Q' + str(irow+1), u'/тасаг/-ийн байцаагч:.............................' , content_left_noborder)               
                worksheet_general.merge_range('L' + str(irow+2) + ':Q' + str(irow+2), u'20.... Оны ... Сарын ......- ны өдөр' , content_left_noborder)
                count += 1
                irow +=4

            workbook.close()
            out = base64.encodestring(output.getvalue())
            excel_id = self.env['oderp.report.excel.output'].create({'filedata': out, 'filename': file_name})

            return {
                'name': 'Export Result',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'oderp.report.excel.output',
                'res_id': excel_id.id,
                'view_id': False,
                'context': self._context,
                'type': 'ir.actions.act_window',
                'target': 'new',
                'nodestroy': True,
            }