# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from io import BytesIO
import base64
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
import time
from datetime import date, datetime, timedelta

from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.modules import get_module_resource


class HrPayslipHhoatReport(models.TransientModel):
    _name = "hr.payslip.hhoat.report"
    _description = "Hr Payslip Hhoat Report"

    start_period_id = fields.Many2one('account.period', string='Start Period', index=True, invisible=False,
                                      required=True)
    end_period_id = fields.Many2one('account.period', string='End Period', index=True, invisible=False, required=True)
    employee_ids = fields.Many2many('hr.employee', 'report_hhoat_payslip_employee_rel', 'report_id', 'emp_id',
                                    string='Employees')
    department_ids = fields.Many2many('hr.department', 'report_hhoat_payslip_department_rel', 'report_id', 'dep_id',
                                      string='Departments')
    payslip_run_ids = fields.Many2many('hr.payslip.run', 'report_hhoat_payslip_payslip_run_rel', 'report_id',
                                       'payslip_run', string='Payslip run')

    @api.onchange('start_period_id', 'end_period_id')
    def _compute_start_to_end_period(self):
        if self.start_period_id and self.end_period_id:
            if self.start_period_id.date_start > self.end_period_id.date_start:
                raise UserError(_("Start period and end period is not right"))
            end_period = self.end_period_id.id
            date_list = []
            while self.start_period_id.id != end_period:
                dateobj = self._get_previous_period(end_period)
                end_period = dateobj['per_id']
                date_list.append(end_period)
            date_list.append(self.end_period_id.id)
            account_periods = self.env['account.period'].search([('id', 'in', date_list)])
            return {
                'domain': {
                    'payslip_run_ids': [('period_id', 'in', account_periods.ids)]
                }
            }

    @api.multi
    def _get_previous_period(self, now_period_id):
        ret_pre_id = False
        user_ids = self.env['res.users'].search([('id', '=', self.env.uid)])
        company_id = user_ids.company_id.id
        period_ids = self.env['account.period'].search([('id', '=', now_period_id)])
        for period_id in period_ids:
            if period_id.date_start:
                start_date = period_id.date_start
        last_day = datetime.strptime(start_date, '%Y-%m-%d').date() - timedelta(days=1)
        pre_ids = period_ids.search(
            [('date_stop', '=', datetime.strftime(last_day, "%Y-%m-%d")), ('company_id', '=', company_id)])
        for pre_id in pre_ids:
            ret_pre_id = pre_id.id
        res = {'per_id': ret_pre_id}
        return res

    # ТТ 11-1 татварын тайлан
    @api.multi
    def export_report_tt_11_1(self):
        for template in self:
            output = BytesIO()
            workbook = xlsxwriter.Workbook(output)

            file_name = u'ТТ-11_1-н Тайлан %s сараас %s' % (
                template.start_period_id.code, template.end_period_id.code) + '.xlsx'
            # CELL styles тодорхойлж байна
            theader = workbook.add_format()
            theader.set_font_size(12)
            theader.set_align('right')
            theader.set_align('vright')
            theader.set_font_name('Times New Roman')

            theader1 = workbook.add_format()
            theader1.set_font_size(12)
            theader1.set_align('left')
            theader1.set_align('vleft')
            theader1.set_font_name('Times New Roman')

            theader2 = workbook.add_format({'bold': 1})
            theader2.set_top()

            theader3 = workbook.add_format({'bold': 1})
            theader3.set_font_size(14)
            theader3.set_align('center')
            theader3.set_align('vcenter')
            theader3.set_font_name('Times New Roman')

            format_content_float = workbook.add_format(ReportExcelCellStyles.format_content_float)

            theader4 = workbook.add_format()
            theader4.set_font_size(12)
            theader4.set_text_wrap()
            theader4.set_align('center')
            theader4.set_align('vcenter')
            theader4.set_font_name('Times New Roman')
            theader4.set_border(style=1)
            theader4.set_bg_color('#83caff')

            theader5 = workbook.add_format()
            theader5.set_font_size(12)
            theader5.set_align('left')
            theader5.set_align('vleft')
            theader5.set_font_name('Times New Roman')
            theader5.set_underline()

            footer = workbook.add_format({'bold': 1, 'num_format': '#,###.##'})
            footer.set_font_size(12)
            footer.set_font_name('Times New Roman')
            footer.set_align('right')
            footer.set_border(style=1)
            footer.set_bg_color('#83caff')

            content_right = workbook.add_format({'num_format': '#,###.##'})
            content_right.set_text_wrap()
            content_right.set_border(style=1)
            content_right.set_font_size(10)
            content_right.set_align('right')
            content_right.set_font_name('Times New Roman')

            content_left = workbook.add_format()
            content_left.set_text_wrap()
            content_left.set_border(style=1)
            content_left.set_font_size(10)
            content_left.set_align('left')
            content_left.set_font_name('Times New Roman')

            # Ерөнхий мэдээллийн sheet нэмэх
            worksheet_general = workbook.add_worksheet(u'ХХОАТ-н тайлан')
            worksheet_general.set_column('A:A', 10)
            worksheet_general.set_column('B:B', 15)
            worksheet_general.set_column('C:C', 20)
            worksheet_general.set_column('D:D', 20)
            worksheet_general.set_column('E:E', 10)
            worksheet_general.set_column('F:F', 10)
            worksheet_general.set_column('G:G', 10)
            worksheet_general.set_column('H:H', 10)
            worksheet_general.set_column('I:I', 10)
            worksheet_general.set_column('J:J', 10)
            worksheet_general.set_column('K:K', 10)
            worksheet_general.set_column('L:L', 15)
            worksheet_general.set_column('M:M', 10)
            worksheet_general.set_column('N:N', 10)
            worksheet_general.set_column('O:O', 15)
            worksheet_general.set_column('P:P', 10)
            worksheet_general.set_column('Q:Q', 10)
            worksheet_general.set_column('R:R', 15)
            worksheet_general.set_column('S:S', 10)
            worksheet_general.set_column('T:T', 10)

            worksheet_general.set_row(3, 50)
            worksheet_general.set_row(16, 38.5)
            worksheet_general.set_row(17, 39)
            end_period = template.end_period_id.id
            date_list = []
            date_list.append(end_period)
            i = 0
            while template.start_period_id.id != end_period:
                dateobj = self._get_previous_period(end_period)
                end_period = dateobj['per_id']
                date_list.append(end_period)
                i += 1
            irow = 0
            icol = 0
            worksheet_general.merge_range(irow, icol, irow + 3, icol, u'ТТД', theader4)
            worksheet_general.merge_range(irow, icol + 1, irow + 3, icol + 1, u'Регистрийн дугаар', theader4)
            worksheet_general.merge_range(irow, icol + 2, irow + 3, icol + 2, u'Овог', theader4)
            worksheet_general.merge_range(irow, icol + 3, irow + 3, icol + 3, u'Нэр', theader4)
            worksheet_general.merge_range(irow, icol + 4, irow + 3, icol + 4, u'Орлого (Хуулийн 7.1.1)', theader4)
            worksheet_general.merge_range(irow, icol + 5, irow + 3, icol + 5, u'Орлого (Хуулийн 7.1.2)', theader4)
            worksheet_general.merge_range(irow, icol + 6, irow + 3, icol + 6, u'Орлого (Хуулийн 7.1.3)', theader4)
            worksheet_general.merge_range(irow, icol + 7, irow + 3, icol + 7, u'Орлого (Хуулийн 7.1.4)', theader4)
            worksheet_general.merge_range(irow, icol + 8, irow + 3, icol + 8, u'Орлого (Хуулийн 7.1.5)', theader4)
            worksheet_general.merge_range(irow, icol + 9, irow + 3, icol + 9, u'Орлого (Хуулийн 7.1.6)', theader4)
            worksheet_general.merge_range(irow, icol + 10, irow + 3, icol + 10, u'Орлого (Хуулийн 7.1.7)', theader4)
            worksheet_general.merge_range(irow, icol + 11, irow + 3, icol + 11, u'Дүн', theader4)
            worksheet_general.merge_range(irow, icol + 12, irow + 3, icol + 12,
                                          u'ЭМД болон НДШ-ийн дүн (7.1.6-д заасан орлогод)', theader4)
            worksheet_general.merge_range(irow, icol + 13, irow + 3, icol + 13,
                                          u'ЭМД болон НДШ-ийн дүн (бусад орлогод)', theader4)
            worksheet_general.merge_range(irow, icol + 14, irow + 3, icol + 14, u'Татвар ногдуулах орлого', theader4)
            worksheet_general.merge_range(irow, icol + 15, irow + 3, icol + 15,
                                          u'Ногдуулсан албан татвар(7.1.6-д заасан орлогод)', theader4)
            worksheet_general.merge_range(irow, icol + 16, irow + 3, icol + 16,
                                          u'Ногдуулсан албан татвар(бусад орлогод)', theader4)
            worksheet_general.merge_range(irow, icol + 17, irow + 3, icol + 17,
                                          u'Орлого хүлээн авсан сарын тоо (ажилласан сар)', theader4)
            worksheet_general.merge_range(irow, icol + 18, irow + 3, icol + 18, u'Хөнгөлөх татвар', theader4)
            worksheet_general.merge_range(irow, icol + 19, irow + 3, icol + 19, u'Суутгасан албан татвар', theader4)
            if not template.employee_ids and not template.department_ids and not template.payslip_run_ids:
                irow += 3
                num = 0
                count = 1
                count_period = 0
                self._cr.execute('''SELECT id FROM hr_employee ORDER BY name_related ASC, identification_id ASC''')
                employees = self._cr.fetchall()
                emp_ids = []
                for employee in employees:
                    emp_ids.append(employee[0])
                employe_ids = self.env['hr.employee'].search([('id', 'in', emp_ids)])
                for emp in employe_ids:
                    k = 0
                    in_7_1_1 = 0
                    in_7_1_2 = 0
                    in_7_1_3 = 0
                    in_7_1_4 = 0
                    in_7_1_5 = 0
                    in_7_1_6 = 0
                    in_7_1_7 = 0
                    ndsh_1 = 0
                    ndsh_2 = 0
                    hhoat_dis = 0
                    hhoat1 = 0
                    tax_free = 0
                    for da in date_list:
                        slip_ids = self.env['hr.payslip'].search(
                            [('salary_type', '=', 'last_salary'), ('period_id', '=', da),
                             ('employee_id', '=', emp.id)])
                        if slip_ids:
                            count_period = len(slip_ids)
                        for slip in slip_ids:
                            k += 1
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_1' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_1 = self._cr.fetchall()
                            for val in income_7_1_1:
                                in_7_1_1 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_2' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_2 = self._cr.fetchall()
                            for val in income_7_1_2:
                                in_7_1_2 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_3' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_3 = self._cr.fetchall()
                            for val in income_7_1_3:
                                in_7_1_3 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_4' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_4 = self._cr.fetchall()
                            for val in income_7_1_4:
                                in_7_1_4 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_5' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_5 = self._cr.fetchall()
                            for val in income_7_1_5:
                                in_7_1_5 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_6' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_6 = self._cr.fetchall()
                            for val in income_7_1_6:
                                in_7_1_6 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_7' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_7 = self._cr.fetchall()
                            for val in income_7_1_7:
                                in_7_1_7 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='ndsh1' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            ndsh1 = self._cr.fetchall()
                            for val in ndsh1:
                                ndsh_1 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='ndsh2' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            ndsh2 = self._cr.fetchall()
                            for val in ndsh2:
                                ndsh_2 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='hhoat_discount' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            hhoat_discount = self._cr.fetchall()
                            for val in hhoat_discount:
                                hhoat_dis += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='hhoat' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            hhoat = self._cr.fetchall()
                            for val in hhoat:
                                hhoat1 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_on_tax' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_on_tax = self._cr.fetchall()
                            for val in income_on_tax:
                                tax_free += val[1]

                    irow += 1
                    worksheet_general.write(irow, icol, 0, content_left)
                    if emp.ssnid:
                        worksheet_general.write(irow, icol + 1, emp.ssnid.upper(), content_left)
                    if emp.last_name:
                        worksheet_general.write(irow, icol + 2, emp.last_name.upper(), content_left)
                    if emp.name_related:
                        worksheet_general.write(irow, icol + 3, emp.name_related.upper(), content_left)
                    worksheet_general.write(irow, icol + 4, in_7_1_1, format_content_float)
                    worksheet_general.write(irow, icol + 5, in_7_1_2, format_content_float)
                    worksheet_general.write(irow, icol + 6, in_7_1_3, format_content_float)
                    worksheet_general.write(irow, icol + 7, in_7_1_4, format_content_float)
                    worksheet_general.write(irow, icol + 8, in_7_1_5, format_content_float)
                    worksheet_general.write(irow, icol + 9, in_7_1_6, format_content_float)
                    worksheet_general.write(irow, icol + 10, in_7_1_7, format_content_float)
                    worksheet_general.write(irow, icol + 11, '{=SUM(' + xl_rowcol_to_cell(irow, icol + 4) + ':'
                                            + xl_rowcol_to_cell(irow, icol + 10) + ')}',
                                            format_content_float)
                    worksheet_general.write(irow, icol + 12, ndsh_2, format_content_float)
                    worksheet_general.write(irow, icol + 13, ndsh_1, format_content_float)
                    worksheet_general.write(irow, icol + 14, '=' + xl_rowcol_to_cell(irow, icol + 11) + '-'
                                            + xl_rowcol_to_cell(irow, icol + 12) + '-' + xl_rowcol_to_cell(
                        irow, icol + 13), format_content_float)
                    worksheet_general.write(irow, icol + 15, '=(' + xl_rowcol_to_cell(irow, icol + 9) + '-'
                                            + xl_rowcol_to_cell(irow, icol + 12) + ')*0.1',
                                            format_content_float)
                    worksheet_general.write(irow, icol + 16, '=((' + xl_rowcol_to_cell(irow, icol + 11) + '-'
                                            + xl_rowcol_to_cell(irow, icol + 9) + ')' + '-' + xl_rowcol_to_cell(
                        irow, icol + 13) + ')*0.1', format_content_float)
                    worksheet_general.write(irow, icol + 17, count_period, content_right)
                    worksheet_general.write(irow, icol + 18, hhoat_dis, format_content_float)
                    worksheet_general.write(irow, icol + 19, '=' + xl_rowcol_to_cell(irow, icol + 15) + '+'
                                            + xl_rowcol_to_cell(irow, icol + 16) + '-' + xl_rowcol_to_cell(
                        irow, icol + 18), format_content_float)
                    count += 1
                    count_period = 0
            elif template.employee_ids:
                irow += 3
                count = 1
                emp_ids = []
                for emp in template.employee_ids:
                    emp_ids.append(emp.id)
                empids = str(emp_ids)
                empids = empids.replace("[", "(")
                empids = empids.replace("]", ")")
                self._cr.execute(
                    '''SELECT id FROM hr_employee WHERE id IN ''' + empids + ''' ORDER BY name_related ASC, identification_id ASC''')
                employees = self._cr.fetchall()
                emp_ids1 = []
                for employee in employees:
                    emp_ids1.append(employee[0])
                icol = 0
                count_period = 0
                for emp in self.env['hr.employee'].browse(emp_ids1):
                    k = 0
                    in_7_1_1 = 0
                    in_7_1_2 = 0
                    in_7_1_3 = 0
                    in_7_1_4 = 0
                    in_7_1_5 = 0
                    in_7_1_6 = 0
                    in_7_1_7 = 0
                    ndsh_1 = 0
                    ndsh_2 = 0
                    hhoat_dis = 0
                    hhoat2 = 0
                    tax_free = 0
                    for da in date_list:
                        slip_ids = self.env['hr.payslip'].search(
                            [('salary_type', '=', 'last_salary'), ('period_id', '=', da),
                             ('employee_id', '=', emp.id)])
                        if slip_ids:
                            count_period = len(slip_ids)
                        for slip in slip_ids:
                            k += 1
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_1' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_1 = self._cr.fetchall()
                            for val in income_7_1_1:
                                in_7_1_1 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_2' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_2 = self._cr.fetchall()
                            for val in income_7_1_2:
                                in_7_1_2 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_3' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_3 = self._cr.fetchall()
                            for val in income_7_1_3:
                                in_7_1_3 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_4' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_4 = self._cr.fetchall()
                            for val in income_7_1_4:
                                in_7_1_4 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_5' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_5 = self._cr.fetchall()
                            for val in income_7_1_5:
                                in_7_1_5 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_6' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_6 = self._cr.fetchall()
                            for val in income_7_1_6:
                                in_7_1_6 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_7' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_7 = self._cr.fetchall()
                            for val in income_7_1_7:
                                in_7_1_7 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='ndsh1' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            ndsh1 = self._cr.fetchall()
                            for val in ndsh1:
                                ndsh_1 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='ndsh2' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            ndsh2 = self._cr.fetchall()
                            for val in ndsh2:
                                ndsh_2 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='hhoat_discount' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            hhoat_discount = self._cr.fetchall()
                            for val in hhoat_discount:
                                hhoat_dis += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='hhoat' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            hhoat = self._cr.fetchall()
                            for val in hhoat:
                                hhoat2 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_on_tax' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_on_tax = self._cr.fetchall()
                            for val in income_on_tax:
                                tax_free += val[1]

                    irow += 1
                    worksheet_general.write(irow, icol, 0, content_left)
                    if emp.ssnid:
                        worksheet_general.write(irow, icol + 1, emp.ssnid.upper(), content_left)
                    if emp.last_name:
                        worksheet_general.write(irow, icol + 2, emp.last_name.upper(), content_left)
                    if emp.name_related:
                        worksheet_general.write(irow, icol + 3, emp.name_related.upper(), content_left)
                    worksheet_general.write(irow, icol + 4, in_7_1_1, format_content_float)
                    worksheet_general.write(irow, icol + 5, in_7_1_2, format_content_float)
                    worksheet_general.write(irow, icol + 6, in_7_1_3, format_content_float)
                    worksheet_general.write(irow, icol + 7, in_7_1_4, format_content_float)
                    worksheet_general.write(irow, icol + 8, in_7_1_5, format_content_float)
                    worksheet_general.write(irow, icol + 9, in_7_1_6, format_content_float)
                    worksheet_general.write(irow, icol + 10, in_7_1_7, format_content_float)
                    worksheet_general.write(irow, icol + 11, '{=SUM(' + xl_rowcol_to_cell(irow, icol + 4) + ':'
                                            + xl_rowcol_to_cell(irow, icol + 10) + ')}',
                                            format_content_float)
                    worksheet_general.write(irow, icol + 12, ndsh_2, format_content_float)
                    worksheet_general.write(irow, icol + 13, ndsh_1, format_content_float)
                    worksheet_general.write(irow, icol + 14, '=' + xl_rowcol_to_cell(irow, icol + 11) + '-'
                                            + xl_rowcol_to_cell(irow, icol + 12) + '-' + xl_rowcol_to_cell(
                        irow, icol + 13), format_content_float)
                    worksheet_general.write(irow, icol + 15, '=(' + xl_rowcol_to_cell(irow, icol + 9) + '-'
                                            + xl_rowcol_to_cell(irow, icol + 12) + ')*0.1',
                                            format_content_float)
                    worksheet_general.write(irow, icol + 16, '=((' + xl_rowcol_to_cell(irow, icol + 11) + '-'
                                            + xl_rowcol_to_cell(irow, icol + 9) + ')' + '-' + xl_rowcol_to_cell(
                        irow, icol + 13) + ')*0.1', format_content_float)
                    worksheet_general.write(irow, icol + 17, count_period, content_right)
                    worksheet_general.write(irow, icol + 18, hhoat_dis, format_content_float)
                    worksheet_general.write(irow, icol + 19, '=' + xl_rowcol_to_cell(irow, icol + 15) + '+'
                                            + xl_rowcol_to_cell(irow, icol + 16) + '-' + xl_rowcol_to_cell(
                        irow, icol + 18), format_content_float)
                    count += 1
                    count_period = 0
                if k > 0:
                    irow += 1
            elif template.payslip_run_ids:
                irow += 3
                count = 1
                num = 0
                run_ids = []
                for run in template.payslip_run_ids:
                    run_ids.append(run.id)
                run_ids = str(run_ids)
                run_ids = run_ids.replace("[", "(")
                run_ids = run_ids.replace("]", ")")
                self._cr.execute('''
                                    SELECT pslp_line.employee_id  FROM hr_payslip_line pslp_line 
                                        LEFT JOIN hr_payslip pslp ON pslp.id=pslp_line.slip_id  
                                        LEFT JOIN hr_payslip_run prun ON prun.id=pslp.payslip_run_id 
                                        LEFT JOIN hr_employee hr_emp ON hr_emp.id=pslp.employee_id 
                                        WHERE prun.id IN ''' + run_ids + ''' group by pslp_line.employee_id''')

                employees = self._cr.fetchall()
                emp_ids1 = []
                k = 0
                count_period = 0
                for employee in employees:
                    for da in date_list:
                        slip_ids = self.env['hr.payslip'].search(
                            [('salary_type', '=', 'last_salary'), ('period_id', '=', da),
                             ('employee_id', '=', employee)])
                        if slip_ids:
                            count_period = len(slip_ids)
                    k += 1
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='income_7_1_1' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + ''' ''')
                    income_7_1_1 = self._cr.fetchall()
                    in_7_1_1 = income_7_1_1[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='income_7_1_2' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + ''' ''')
                    income_7_1_2 = self._cr.fetchall()
                    in_7_1_2 = income_7_1_2[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='income_7_1_3' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + ''' ''')
                    income_7_1_3 = self._cr.fetchall()
                    in_7_1_3 = income_7_1_3[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='income_7_1_4' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + ''' ''')
                    income_7_1_4 = self._cr.fetchall()
                    in_7_1_4 = income_7_1_4[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='income_7_1_5' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + ''' ''')
                    income_7_1_5 = self._cr.fetchall()
                    in_7_1_5 = income_7_1_5[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='income_7_1_6' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + ''' ''')
                    income_7_1_6 = self._cr.fetchall()
                    in_7_1_6 = income_7_1_6[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='income_7_1_7' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + ''' ''')
                    income_7_1_7 = self._cr.fetchall()
                    in_7_1_7 = income_7_1_7[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr,  hr_payslip pslp WHERE sr.hhoat_report_type='ndsh1' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + '''''')
                    ndsh_1 = self._cr.fetchall()
                    ndsh_1_1 = ndsh_1[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='ndsh2' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + '''''')
                    ndsh_2 = self._cr.fetchall()
                    ndsh_2_2 = ndsh_2[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='hhoat_discount' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + '''''')
                    hhoat_dis = self._cr.fetchall()
                    hhoat_disc = hhoat_dis[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='hhoat' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + '''''')
                    hhoat = self._cr.fetchall()
                    hhoat1 = hhoat[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='income_on_tax' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + '''''')
                    income_on_tax = self._cr.fetchall()
                    income_on_tax = income_on_tax[0][0]

                    emp = self.env['hr.employee'].browse(employee[0])

                    irow += 1
                    worksheet_general.write(irow, icol, 0, content_left)
                    if emp.ssnid:
                        worksheet_general.write(irow, icol + 1, emp.ssnid.upper(), content_left)
                    if emp.last_name:
                        worksheet_general.write(irow, icol + 2, emp.last_name.upper(), content_left)
                    if emp.name_related:
                        worksheet_general.write(irow, icol + 3, emp.name_related.upper(), content_left)
                    worksheet_general.write(irow, icol + 4, in_7_1_1, format_content_float)
                    worksheet_general.write(irow, icol + 5, in_7_1_2, format_content_float)
                    worksheet_general.write(irow, icol + 6, in_7_1_3, format_content_float)
                    worksheet_general.write(irow, icol + 7, in_7_1_4, format_content_float)
                    worksheet_general.write(irow, icol + 8, in_7_1_5, format_content_float)
                    worksheet_general.write(irow, icol + 9, in_7_1_6, format_content_float)
                    worksheet_general.write(irow, icol + 10, in_7_1_7, format_content_float)
                    worksheet_general.write(irow, icol + 11, '{=SUM(' + xl_rowcol_to_cell(irow, icol + 4) + ':'
                                            + xl_rowcol_to_cell(irow, icol + 10) + ')}',
                                            format_content_float)
                    worksheet_general.write(irow, icol + 12, ndsh_2_2, format_content_float)
                    worksheet_general.write(irow, icol + 13, ndsh_1_1, format_content_float)
                    worksheet_general.write(irow, icol + 14, '=' + xl_rowcol_to_cell(irow, icol + 11) + '-'
                                            + xl_rowcol_to_cell(irow, icol + 12) + '-' + xl_rowcol_to_cell(
                        irow, icol + 13), format_content_float)
                    worksheet_general.write(irow, icol + 15, '=(' + xl_rowcol_to_cell(irow, icol + 9) + '-'
                                            + xl_rowcol_to_cell(irow, icol + 12) + ')*0.1',
                                            format_content_float)
                    worksheet_general.write(irow, icol + 16, '=((' + xl_rowcol_to_cell(irow, icol + 11) + '-'
                                            + xl_rowcol_to_cell(irow, icol + 9) + ')' + '-' + xl_rowcol_to_cell(
                        irow, icol + 13) + ')*0.1', format_content_float)
                    worksheet_general.write(irow, icol + 17, count_period, content_right)
                    worksheet_general.write(irow, icol + 18, hhoat_disc, format_content_float)
                    worksheet_general.write(irow, icol + 19, '=' + xl_rowcol_to_cell(irow, icol + 15) + '+'
                                            + xl_rowcol_to_cell(irow, icol + 16) + '-' + xl_rowcol_to_cell(
                        irow, icol + 18), format_content_float)
                    count_period = 0
                    count += 1
                if k > 0:
                    irow += 1
            workbook.close()
            out = base64.encodestring(output.getvalue())
            excel_id = self.env['oderp.report.excel.output'].create({'filedata': out, 'filename': file_name})
            return {
                'name': 'Export Result',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'oderp.report.excel.output',
                'res_id': excel_id.id,
                'view_id': False,
                'context': self._context,
                'type': 'ir.actions.act_window',
                'target': 'new',
                'nodestroy': True,
            }

    # ТТ 11-1-2020 татварын тайлан
    @api.multi
    def export_report_tt_11_1_2020(self):
        for template in self:
            if not template.employee_ids and not template.payslip_run_ids:
                raise UserError(_('You must choose at least one employee or payslip run '))
            output = BytesIO()
            workbook = xlsxwriter.Workbook(output)

            file_name = u'ТТ-11-1-2020-н Тайлан %s сараас %s' % (
                template.start_period_id.code, template.end_period_id.code) + '.xlsx'
            # CELL styles тодорхойлж байна
            theader = workbook.add_format()
            theader.set_font_size(12)
            theader.set_align('right')
            theader.set_align('vright')
            theader.set_font_name('Times New Roman')

            theader1 = workbook.add_format()
            theader1.set_font_size(12)
            theader1.set_align('left')
            theader1.set_align('vleft')
            theader1.set_font_name('Times New Roman')

            theader2 = workbook.add_format({'bold': 1})
            theader2.set_top()

            theader3 = workbook.add_format({'bold': 1})
            theader3.set_font_size(14)
            theader3.set_align('center')
            theader3.set_align('vcenter')
            theader3.set_font_name('Times New Roman')

            format_content_float = workbook.add_format(ReportExcelCellStyles.format_content_float)

            theader4 = workbook.add_format()
            theader4.set_font_size(12)
            theader4.set_text_wrap()
            theader4.set_align('center')
            theader4.set_align('vcenter')
            theader4.set_font_name('Times New Roman')
            theader4.set_border(style=1)
            theader4.set_bg_color('#83caff')

            theader5 = workbook.add_format()
            theader5.set_font_size(12)
            theader5.set_align('left')
            theader5.set_align('vleft')
            theader5.set_font_name('Times New Roman')
            theader5.set_underline()

            footer = workbook.add_format({'bold': 1, 'num_format': '#,###.##'})
            footer.set_font_size(12)
            footer.set_font_name('Times New Roman')
            footer.set_align('right')
            footer.set_border(style=1)
            footer.set_bg_color('#83caff')

            content_right = workbook.add_format({'num_format': '#,###.##'})
            content_right.set_text_wrap()
            content_right.set_border(style=1)
            content_right.set_font_size(10)
            content_right.set_align('right')
            content_right.set_font_name('Times New Roman')

            content_left = workbook.add_format()
            content_left.set_text_wrap()
            content_left.set_border(style=1)
            content_left.set_font_size(10)
            content_left.set_align('left')
            content_left.set_font_name('Times New Roman')

            columns = {'1': 'A', '2': 'B', '3': 'C', '4': 'D', '5': 'E', '6': 'F', '7': 'G', '8': 'H', '9': 'I',
                       '10': 'J', '11': 'K', '12': 'L', '13': 'M',
                       '14': 'N', '15': 'O', '16': 'P', '17': 'Q', '18': 'R', '19': 'S', '20': 'T', '21': 'U',
                       '22': 'V', '23': 'W', '24': 'X', '25': 'Y', '26': 'Z',
                       '27': 'AA', '28': 'AB', '29': 'AC', '30': 'AD', '31': 'AE', '32': 'AF', '33': 'AG',
                       '34': 'AH', '35': 'AI', '36': 'AJ', '37': 'AK', '38': 'AL', '39': 'AM',
                       '40': 'AN', '41': 'AO', '42': 'AP', '43': 'AQ', '44': 'AR', '45': 'AS', '46': 'AT',
                       '47': 'AU', '48': 'AV', '49': 'AW', '50': 'AX', '51': 'AY', '52': 'AZ',
                       '53': 'BA', '54': 'BB', '55': 'BC', '56': 'BD', '57': 'BE', '58': 'BF', '59': 'BG',
                       '60': 'BH', '61': 'BI', '62': 'BJ', '63': 'BK', '64': 'BL', '65': 'BM',
                       '66': 'BN', '67': 'BO', '68': 'BP', '69': 'BQ', '70': 'BR', '71': 'BS', '72': 'BT',
                       '73': 'BU', '74': 'BV', '75': 'BW', '76': 'BX', '77': 'BY', '78': 'BZ',
                       '79': 'CA', '80': 'CB', '81': 'CC', '82': 'CD', '83': 'CE', '84': 'CF', '85': 'CG',
                       '86': 'CH', '87': 'CI', '88': 'CJ', '89': 'CK', '90': 'CL', '91': 'CM',
                       '92': 'CN', '93': 'CO', '94': 'CP', '95': 'CQ', '96': 'CR', '97': 'CS', '98': 'CT',
                       '99': 'CU', '100': 'CV', '101': 'CW', '102': 'CX', '103': 'CY', '104': 'CZ', }

            # Ерөнхий мэдээллийн sheet нэмэх
            worksheet_general = workbook.add_worksheet(u'ХХОАТ-н тайлан')
            worksheet_general.set_column('A:A', 10)
            worksheet_general.set_column('B:B', 15)
            worksheet_general.set_column('C:C', 20)
            worksheet_general.set_column('D:D', 20)
            worksheet_general.set_column('E:E', 10)
            worksheet_general.set_column('F:F', 10)
            worksheet_general.set_column('G:G', 10)
            worksheet_general.set_column('H:H', 10)
            worksheet_general.set_column('I:I', 10)
            worksheet_general.set_column('J:J', 10)
            worksheet_general.set_column('K:K', 10)
            worksheet_general.set_column('L:L', 15)
            worksheet_general.set_column('M:M', 10)
            worksheet_general.set_column('N:N', 10)
            worksheet_general.set_column('O:O', 15)
            worksheet_general.set_column('P:P', 10)
            worksheet_general.set_column('Q:Q', 10)
            worksheet_general.set_column('R:R', 15)
            worksheet_general.set_column('S:S', 10)
            worksheet_general.set_column('T:T', 10)
            worksheet_general.set_column('U:U', 10)

            worksheet_general.set_row(3, 50)
            worksheet_general.set_row(16, 38.5)
            worksheet_general.set_row(17, 39)
            struct_id = 0
            max_count = 0
            irow = 0
            end_period = template.end_period_id.id
            dateobj = {}
            date_list = []
            date_list.append(end_period)
            i = 0
            while template.start_period_id.id != end_period:
                dateobj = self._get_previous_period(end_period)
                end_period = dateobj['per_id']
                date_list.append(end_period)
                i += 1
            irow = 0
            icol = 0
            worksheet_general.merge_range(irow, icol, irow + 3, icol, u'ТТД', theader4)
            worksheet_general.merge_range(irow, icol + 1, irow + 3, icol + 1, u'Регистрийн дугаар', theader4)
            worksheet_general.merge_range(irow, icol + 2, irow + 3, icol + 2, u'Овог', theader4)
            worksheet_general.merge_range(irow, icol + 3, irow + 3, icol + 3, u'Нэр', theader4)
            worksheet_general.merge_range(irow, icol + 4, irow + 3, icol + 4, u'Орлого (Хуулийн 7.1.1)', theader4)
            worksheet_general.merge_range(irow, icol + 5, irow + 3, icol + 5, u'Орлого (Хуулийн 7.1.2)', theader4)
            worksheet_general.merge_range(irow, icol + 6, irow + 3, icol + 6, u'Орлого (Хуулийн 7.1.3)', theader4)
            worksheet_general.merge_range(irow, icol + 7, irow + 3, icol + 7, u'Орлого (Хуулийн 7.1.4)', theader4)
            worksheet_general.merge_range(irow, icol + 8, irow + 3, icol + 8, u'Орлого (Хуулийн 7.1.5)', theader4)
            worksheet_general.merge_range(irow, icol + 9, irow + 3, icol + 9, u'Орлого (Хуулийн 7.1.6)', theader4)
            worksheet_general.merge_range(irow, icol + 10, irow + 3, icol + 10, u'Орлого (Хуулийн 7.1.7)', theader4)
            worksheet_general.merge_range(irow, icol + 11, irow + 3, icol + 11, u'Дүн', theader4)
            worksheet_general.merge_range(irow, icol + 12, irow + 3, icol + 12,
                                          u'ЭМД болон НДШ-ийн дүн (7.1.6-д заасан орлогод)', theader4)
            worksheet_general.merge_range(irow, icol + 13, irow + 3, icol + 13,
                                          u'ЭМД болон НДШ-ийн дүн (бусад орлогод)', theader4)
            worksheet_general.merge_range(irow, icol + 14, irow + 3, icol + 14, u'Татвар ногдуулах орлого', theader4)
            worksheet_general.merge_range(irow, icol + 15, irow + 3, icol + 15,
                                          u'Ногдуулсан албан татвар(7.1.6-д заасан орлогод)', theader4)
            worksheet_general.merge_range(irow, icol + 16, irow + 3, icol + 16,
                                          u'Ногдуулсан албан татвар(бусад орлогод)', theader4)
            worksheet_general.merge_range(irow, icol + 17, irow + 3, icol + 17,
                                          u'Орлого хүлээн авсан сарын тоо (ажилласан сар)', theader4)
            worksheet_general.merge_range(irow, icol + 18, irow + 3, icol + 18, u'Хөнгөлөх татвар', theader4)
            worksheet_general.merge_range(irow, icol + 19, irow + 3, icol + 19, u'Суутгасан албан татвар', theader4)
            worksheet_general.merge_range(irow, icol + 20, irow + 3, icol + 20,
                                          u'Албан татвараас чөлөөлөх /2020.04.01-2020.12.31/ хугацаанд', theader4)
            if not template.employee_ids and not template.department_ids and not template.payslip_run_ids:
                irow += 3
                num = 0
                count = 1
                self._cr.execute('''SELECT id FROM hr_employee ORDER BY name_related ASC, identification_id ASC''')
                employees = self._cr.fetchall()
                emp_ids = []
                for employee in employees:
                    emp_ids.append(employee[0])
                employe_ids = self.env['hr.employee'].search([('id', 'in', emp_ids)])
                count_period = 0
                for emp in employe_ids:
                    k = 0
                    in_7_1_1 = 0
                    in_7_1_2 = 0
                    in_7_1_3 = 0
                    in_7_1_4 = 0
                    in_7_1_5 = 0
                    in_7_1_6 = 0
                    in_7_1_7 = 0
                    ndsh_1 = 0
                    ndsh_2 = 0
                    hhoat_dis = 0
                    total = 0
                    hhoat1 = 0
                    ndsh_em = 0
                    hhoat_cal = 0
                    tax_free = 0
                    re_2020 = 0
                    for da in date_list:
                        slip_ids = self.env['hr.payslip'].search(
                            [('salary_type', '=', 'last_salary'), ('period_id', '=', da),
                             ('employee_id', '=', emp.id)])
                        if slip_ids:
                            count_period = len(slip_ids)
                        for slip in slip_ids:
                            k += 1
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_1' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_1 = self._cr.fetchall()
                            for val in income_7_1_1:
                                in_7_1_1 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_2' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_2 = self._cr.fetchall()
                            for val in income_7_1_2:
                                in_7_1_2 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_3' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_3 = self._cr.fetchall()
                            for val in income_7_1_3:
                                in_7_1_3 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_4' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_4 = self._cr.fetchall()
                            for val in income_7_1_4:
                                in_7_1_4 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_5' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_5 = self._cr.fetchall()
                            for val in income_7_1_5:
                                in_7_1_5 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_6' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_6 = self._cr.fetchall()
                            for val in income_7_1_6:
                                in_7_1_6 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_7' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_7 = self._cr.fetchall()
                            for val in income_7_1_7:
                                in_7_1_7 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='ndsh1' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            ndsh1 = self._cr.fetchall()
                            for val in ndsh1:
                                ndsh_1 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='ndsh2' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            ndsh2 = self._cr.fetchall()
                            for val in ndsh2:
                                ndsh_2 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='hhoat_discount' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            hhoat_discount = self._cr.fetchall()
                            for val in hhoat_discount:
                                hhoat_dis += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='hhoat' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            hhoat = self._cr.fetchall()
                            for val in hhoat:
                                hhoat1 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_on_tax' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_on_tax = self._cr.fetchall()
                            for val in income_on_tax:
                                tax_free += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='2020_rep' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            rep_2020 = self._cr.fetchall()
                            for val in rep_2020:
                                re_2020 += val[1]

                    total = in_7_1_1 + in_7_1_2 + in_7_1_3 + in_7_1_4 + in_7_1_5 + in_7_1_6 + in_7_1_7
                    ndsh_emd = total - ndsh_1 - ndsh_2
                    nogduulsan_alban_tatvar_7_1_6 = (in_7_1_6 - ndsh_1) * 0.1
                    nogduulsan_alban_tatvar_busad = ((total - in_7_1_6) - ndsh_2) * 0.1
                    irow += 1
                    worksheet_general.write(irow, icol, 0, content_left)
                    if emp.ssnid:
                        worksheet_general.write(irow, icol + 1, emp.ssnid.upper(), content_left)
                    if emp.last_name:
                        worksheet_general.write(irow, icol + 2, emp.last_name.upper(), content_left)
                    if emp.name_related:
                        worksheet_general.write(irow, icol + 3, emp.name_related.upper(), content_left)
                    worksheet_general.write(irow, icol + 4, in_7_1_1, format_content_float)
                    worksheet_general.write(irow, icol + 5, in_7_1_2, format_content_float)
                    worksheet_general.write(irow, icol + 6, in_7_1_3, format_content_float)
                    worksheet_general.write(irow, icol + 7, in_7_1_4, format_content_float)
                    worksheet_general.write(irow, icol + 8, in_7_1_5, format_content_float)
                    worksheet_general.write(irow, icol + 9, in_7_1_6, format_content_float)
                    worksheet_general.write(irow, icol + 10, in_7_1_7, format_content_float)
                    worksheet_general.write(irow, icol + 11, '{=SUM(' + xl_rowcol_to_cell(irow, icol + 4) + ':'
                                            + xl_rowcol_to_cell(irow, icol + 10) + ')}',
                                            format_content_float)
                    worksheet_general.write(irow, icol + 12, ndsh_2, format_content_float)
                    worksheet_general.write(irow, icol + 13, ndsh_1, format_content_float)
                    worksheet_general.write(irow, icol + 14, '=' + xl_rowcol_to_cell(irow, icol + 11) + '-'
                                            + xl_rowcol_to_cell(irow, icol + 12) + '-' + xl_rowcol_to_cell(
                        irow, icol + 13), format_content_float)
                    worksheet_general.write(irow, icol + 15, '=(' + xl_rowcol_to_cell(irow, icol + 9) + '-'
                                            + xl_rowcol_to_cell(irow, icol + 12) + ')*0.1',
                                            format_content_float)
                    worksheet_general.write(irow, icol + 16, '=((' + xl_rowcol_to_cell(irow, icol + 11) + '-'
                                            + xl_rowcol_to_cell(irow, icol + 9) + ')' + '-' + xl_rowcol_to_cell(
                        irow, icol + 13) + ')*0.1', format_content_float)
                    worksheet_general.write(irow, icol + 17, count_period, content_right)
                    worksheet_general.write(irow, icol + 18, hhoat_dis, format_content_float)
                    worksheet_general.write(irow, icol + 19, '=' + xl_rowcol_to_cell(irow, icol + 15) + '+'
                                            + xl_rowcol_to_cell(irow, icol + 16) + '-' + xl_rowcol_to_cell(
                        irow, icol + 18), format_content_float)
                    worksheet_general.write(irow, icol + 20, re_2020, format_content_float)
                    count += 1
                    count_period = 0
            elif template.employee_ids:
                irow += 3
                count = 1
                emp_ids = []
                for emp in template.employee_ids:
                    emp_ids.append(emp.id)
                empids = str(emp_ids)
                empids = empids.replace("[", "(")
                empids = empids.replace("]", ")")
                self._cr.execute(
                    '''SELECT id FROM hr_employee WHERE id IN ''' + empids + ''' ORDER BY name_related ASC, identification_id ASC''')
                employees = self._cr.fetchall()
                emp_ids1 = []
                for employee in employees:
                    emp_ids1.append(employee[0])
                icol = 0
                count_period = 0
                for emp in self.env['hr.employee'].browse(emp_ids1):
                    k = 0
                    in_7_1_1 = 0
                    in_7_1_2 = 0
                    in_7_1_3 = 0
                    in_7_1_4 = 0
                    in_7_1_5 = 0
                    in_7_1_6 = 0
                    in_7_1_7 = 0
                    ndsh_1 = 0
                    ndsh_2 = 0
                    hhoat_dis = 0
                    total = 0
                    hhoat2 = 0
                    ndsh_em = 0
                    hhoat_cal = 0
                    tax_free = 0
                    re_2020 = 0
                    for da in date_list:
                        slip_ids = self.env['hr.payslip'].search(
                            [('salary_type', '=', 'last_salary'), ('period_id', '=', da),
                             ('employee_id', '=', emp.id)])
                        if slip_ids:
                            count_period = len(slip_ids)
                        for slip in slip_ids:
                            k += 1
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_1' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_1 = self._cr.fetchall()
                            for val in income_7_1_1:
                                in_7_1_1 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_2' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_2 = self._cr.fetchall()
                            for val in income_7_1_2:
                                in_7_1_2 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_3' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_3 = self._cr.fetchall()
                            for val in income_7_1_3:
                                in_7_1_3 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_4' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_4 = self._cr.fetchall()
                            for val in income_7_1_4:
                                in_7_1_4 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_5' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_5 = self._cr.fetchall()
                            for val in income_7_1_5:
                                in_7_1_5 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_6' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_6 = self._cr.fetchall()
                            for val in income_7_1_6:
                                in_7_1_6 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_7' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_7 = self._cr.fetchall()
                            for val in income_7_1_7:
                                in_7_1_7 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='ndsh1' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            ndsh1 = self._cr.fetchall()
                            for val in ndsh1:
                                ndsh_1 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='ndsh2' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            ndsh2 = self._cr.fetchall()
                            for val in ndsh2:
                                ndsh_2 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='hhoat_discount' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            hhoat_discount = self._cr.fetchall()
                            for val in hhoat_discount:
                                hhoat_dis += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='hhoat' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            hhoat = self._cr.fetchall()
                            for val in hhoat:
                                hhoat2 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_on_tax' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_on_tax = self._cr.fetchall()
                            for val in income_on_tax:
                                tax_free += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='2020_rep' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            rep_2020 = self._cr.fetchall()
                            for val in rep_2020:
                                re_2020 += val[1]

                    total = in_7_1_1 + in_7_1_2 + in_7_1_3 + in_7_1_4 + in_7_1_5 + in_7_1_6 + in_7_1_7
                    ndsh_emd = total - ndsh_1 - ndsh_2
                    nogduulsan_alban_tatvar_7_1_6 = (in_7_1_6 - ndsh_1) * 0.1
                    nogduulsan_alban_tatvar_busad = ((total - in_7_1_6) - ndsh_2) * 0.1
                    irow += 1
                    worksheet_general.write(irow, icol, 0, content_left)
                    if emp.ssnid:
                        worksheet_general.write(irow, icol + 1, emp.ssnid.upper(), content_left)
                    if emp.last_name:
                        worksheet_general.write(irow, icol + 2, emp.last_name.upper(), content_left)
                    if emp.name_related:
                        worksheet_general.write(irow, icol + 3, emp.name_related.upper(), content_left)
                    worksheet_general.write(irow, icol + 4, in_7_1_1, format_content_float)
                    worksheet_general.write(irow, icol + 5, in_7_1_2, format_content_float)
                    worksheet_general.write(irow, icol + 6, in_7_1_3, format_content_float)
                    worksheet_general.write(irow, icol + 7, in_7_1_4, format_content_float)
                    worksheet_general.write(irow, icol + 8, in_7_1_5, format_content_float)
                    worksheet_general.write(irow, icol + 9, in_7_1_6, format_content_float)
                    worksheet_general.write(irow, icol + 10, in_7_1_7, format_content_float)
                    worksheet_general.write(irow, icol + 11, '{=SUM(' + xl_rowcol_to_cell(irow, icol + 4) + ':'
                                            + xl_rowcol_to_cell(irow, icol + 10) + ')}',
                                            format_content_float)
                    worksheet_general.write(irow, icol + 12, ndsh_2, format_content_float)
                    worksheet_general.write(irow, icol + 13, ndsh_1, format_content_float)
                    worksheet_general.write(irow, icol + 14, '=' + xl_rowcol_to_cell(irow, icol + 11) + '-'
                                            + xl_rowcol_to_cell(irow, icol + 12) + '-' + xl_rowcol_to_cell(
                        irow, icol + 13), format_content_float)
                    worksheet_general.write(irow, icol + 15, '=(' + xl_rowcol_to_cell(irow, icol + 9) + '-'
                                            + xl_rowcol_to_cell(irow, icol + 12) + ')*0.1',
                                            format_content_float)
                    worksheet_general.write(irow, icol + 16, '=((' + xl_rowcol_to_cell(irow, icol + 11) + '-'
                                            + xl_rowcol_to_cell(irow, icol + 9) + ')' + '-' + xl_rowcol_to_cell(
                        irow, icol + 13) + ')*0.1', format_content_float)
                    worksheet_general.write(irow, icol + 17, count_period, content_right)
                    worksheet_general.write(irow, icol + 18, hhoat_dis, format_content_float)
                    worksheet_general.write(irow, icol + 19, '=' + xl_rowcol_to_cell(irow, icol + 15) + '+'
                                            + xl_rowcol_to_cell(irow, icol + 16) + '-' + xl_rowcol_to_cell(
                        irow, icol + 18), format_content_float)
                    worksheet_general.write(irow, icol + 20, re_2020, format_content_float)
                    count += 1
                    count_period = 0
                if k > 0:
                    irow += 1
            elif template.payslip_run_ids:
                irow += 3
                count = 1
                num = 0
                run_ids = []
                for run in template.payslip_run_ids:
                    run_ids.append(run.id)
                run_ids = str(run_ids)
                run_ids = run_ids.replace("[", "(")
                run_ids = run_ids.replace("]", ")")
                self._cr.execute('''
                                    SELECT pslp_line.employee_id  FROM hr_payslip_line pslp_line 
                                        LEFT JOIN hr_payslip pslp ON pslp.id=pslp_line.slip_id  
                                        LEFT JOIN hr_payslip_run prun ON prun.id=pslp.payslip_run_id
                                        LEFT JOIN hr_employee hr_emp ON hr_emp.id = pslp_line.employee_id
                                        WHERE prun.id IN ''' + run_ids + ''' group by pslp_line.employee_id''')
                employees = self._cr.fetchall()
                emp_ids1 = []
                k = 0
                count_period = 0
                for employee in employees:
                    for da in date_list:
                        slip_ids = self.env['hr.payslip'].search(
                            [('salary_type', '=', 'last_salary'), ('period_id', '=', da),
                             ('employee_id', '=', employee)])
                        if slip_ids:
                            count_period = len(slip_ids)
                    k += 1
                    k = 0
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='income_7_1_1' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + ''' ''')
                    income_7_1_1 = self._cr.fetchall()
                    in_7_1_1 = income_7_1_1[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='income_7_1_2' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + ''' ''')
                    income_7_1_2 = self._cr.fetchall()
                    in_7_1_2 = income_7_1_2[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='income_7_1_3' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + ''' ''')
                    income_7_1_3 = self._cr.fetchall()
                    in_7_1_3 = income_7_1_3[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='income_7_1_4' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + ''' ''')
                    income_7_1_4 = self._cr.fetchall()
                    in_7_1_4 = income_7_1_4[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='income_7_1_5' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + ''' ''')
                    income_7_1_5 = self._cr.fetchall()
                    in_7_1_5 = income_7_1_5[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='income_7_1_6' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + ''' ''')
                    income_7_1_6 = self._cr.fetchall()
                    in_7_1_6 = income_7_1_6[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='income_7_1_7' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + ''' ''')
                    income_7_1_7 = self._cr.fetchall()
                    in_7_1_7 = income_7_1_7[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr,  hr_payslip pslp WHERE sr.hhoat_report_type='ndsh1' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + '''''')
                    ndsh_1 = self._cr.fetchall()
                    ndsh_1_to = ndsh_1[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='ndsh2' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + '''''')
                    ndsh_2 = self._cr.fetchall()
                    ndsh_2_to = ndsh_2[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='hhoat_discount' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + '''''')
                    hhoat_dis = self._cr.fetchall()
                    hhoat_dis2 = hhoat_dis[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='hhoat' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + '''''')
                    hhoat = self._cr.fetchall()
                    hhoat4 = hhoat[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='income_on_tax' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + '''''')
                    income_on_tax = self._cr.fetchall()
                    income_on_tax = income_on_tax[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='2020_rep' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + ''' ''')
                    rep_2020 = self._cr.fetchall()
                    report_2020 = rep_2020[0][0]

                    emp = self.env['hr.employee'].browse(employee[0])

                    irow += 1
                    worksheet_general.write(irow, icol, 0, content_left)
                    if emp.ssnid:
                        worksheet_general.write(irow, icol + 1, emp.ssnid.upper(), content_left)
                    if emp.last_name:
                        worksheet_general.write(irow, icol + 2, emp.last_name.upper(), content_left)
                    if emp.name_related:
                        worksheet_general.write(irow, icol + 3, emp.name_related.upper(), content_left)
                    worksheet_general.write(irow, icol + 4, in_7_1_1, format_content_float)
                    worksheet_general.write(irow, icol + 5, in_7_1_2, format_content_float)
                    worksheet_general.write(irow, icol + 6, in_7_1_3, format_content_float)
                    worksheet_general.write(irow, icol + 7, in_7_1_4, format_content_float)
                    worksheet_general.write(irow, icol + 8, in_7_1_5, format_content_float)
                    worksheet_general.write(irow, icol + 9, in_7_1_6, format_content_float)
                    worksheet_general.write(irow, icol + 10, in_7_1_7, format_content_float)
                    worksheet_general.write(irow, icol + 11, '{=SUM(' + xl_rowcol_to_cell(irow, icol + 4) + ':'
                                            + xl_rowcol_to_cell(irow, icol + 10) + ')}',
                                            format_content_float)
                    worksheet_general.write(irow, icol + 12, ndsh_2_to, format_content_float)
                    worksheet_general.write(irow, icol + 13, ndsh_1_to, format_content_float)
                    worksheet_general.write(irow, icol + 14, '=' + xl_rowcol_to_cell(irow, icol + 11) + '-'
                                            + xl_rowcol_to_cell(irow, icol + 12) + '-' + xl_rowcol_to_cell(
                        irow, icol + 13), format_content_float)
                    worksheet_general.write(irow, icol + 15, '=(' + xl_rowcol_to_cell(irow, icol + 9) + '-'
                                            + xl_rowcol_to_cell(irow, icol + 12) + ')*0.1',
                                            format_content_float)
                    worksheet_general.write(irow, icol + 16, '=((' + xl_rowcol_to_cell(irow, icol + 11) + '-'
                                            + xl_rowcol_to_cell(irow, icol + 9) + ')' + '-' + xl_rowcol_to_cell(
                        irow, icol + 13) + ')*0.1', format_content_float)
                    worksheet_general.write(irow, icol + 17, count_period, content_right)
                    worksheet_general.write(irow, icol + 18, hhoat_dis2, format_content_float)
                    worksheet_general.write(irow, icol + 19, '=' + xl_rowcol_to_cell(irow, icol + 15) + '+'
                                            + xl_rowcol_to_cell(irow, icol + 16) + '-' + xl_rowcol_to_cell(
                        irow, icol + 18), format_content_float)
                    worksheet_general.write(irow, icol + 20, report_2020, format_content_float)
                    count_period = 0
                    count += 1
                if k > 0:
                    irow += 1
            workbook.close()
            out = base64.encodestring(output.getvalue())
            excel_id = self.env['oderp.report.excel.output'].create({'filedata': out, 'filename': file_name})
            return {
                'name': 'Export Result',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'oderp.report.excel.output',
                'res_id': excel_id.id,
                'view_id': False,
                'context': self._context,
                'type': 'ir.actions.act_window',
                'target': 'new',
                'nodestroy': True,
            }

    # ТТ 11-2 татварын тайлан
    @api.multi
    def export_report_tt_11_2(self):
        for template in self:
            if not template.employee_ids and not template.payslip_run_ids:
                raise UserError(_('You must choose at least one employee or payslip run '))
            output = BytesIO()
            workbook = xlsxwriter.Workbook(output)

            file_name = u'ТТ-11-1-2020-н Тайлан %s сараас %s' % (
                template.start_period_id.code, template.end_period_id.code) + '.xlsx'
            # CELL styles тодорхойлж байна
            theader = workbook.add_format()
            theader.set_font_size(12)
            theader.set_align('right')
            theader.set_align('vright')
            theader.set_font_name('Times New Roman')

            theader1 = workbook.add_format()
            theader1.set_font_size(12)
            theader1.set_align('left')
            theader1.set_align('vleft')
            theader1.set_font_name('Times New Roman')

            theader2 = workbook.add_format({'bold': 1})
            theader2.set_top()

            theader3 = workbook.add_format({'bold': 1})
            theader3.set_font_size(14)
            theader3.set_align('center')
            theader3.set_align('vcenter')
            theader3.set_font_name('Times New Roman')

            theader4 = workbook.add_format()
            theader4.set_font_size(12)
            theader4.set_text_wrap()
            theader4.set_align('center')
            theader4.set_align('vcenter')
            theader4.set_font_name('Times New Roman')
            theader4.set_border(style=1)
            theader4.set_bg_color('#83caff')

            format_content_float = workbook.add_format(ReportExcelCellStyles.format_content_float)

            theader5 = workbook.add_format()
            theader5.set_font_size(12)
            theader5.set_align('left')
            theader5.set_align('vleft')
            theader5.set_font_name('Times New Roman')
            theader5.set_underline()

            footer = workbook.add_format({'bold': 1, 'num_format': '#,###.##'})
            footer.set_font_size(12)
            footer.set_font_name('Times New Roman')
            footer.set_align('right')
            footer.set_border(style=1)
            footer.set_bg_color('#83caff')

            content_right = workbook.add_format({'num_format': '#,###.##'})
            content_right.set_text_wrap()
            content_right.set_border(style=1)
            content_right.set_font_size(10)
            content_right.set_align('right')
            content_right.set_font_name('Times New Roman')

            content_left = workbook.add_format()
            content_left.set_text_wrap()
            content_left.set_border(style=1)
            content_left.set_font_size(10)
            content_left.set_align('left')
            content_left.set_font_name('Times New Roman')

            columns = {'1': 'A', '2': 'B', '3': 'C', '4': 'D', '5': 'E', '6': 'F', '7': 'G', '8': 'H', '9': 'I',
                       '10': 'J', '11': 'K', '12': 'L', '13': 'M',
                       '14': 'N', '15': 'O', '16': 'P', '17': 'Q', '18': 'R', '19': 'S', '20': 'T', '21': 'U',
                       '22': 'V', '23': 'W', '24': 'X', '25': 'Y', '26': 'Z',
                       '27': 'AA', '28': 'AB', '29': 'AC', '30': 'AD', '31': 'AE', '32': 'AF', '33': 'AG',
                       '34': 'AH', '35': 'AI', '36': 'AJ', '37': 'AK', '38': 'AL', '39': 'AM',
                       '40': 'AN', '41': 'AO', '42': 'AP', '43': 'AQ', '44': 'AR', '45': 'AS', '46': 'AT',
                       '47': 'AU', '48': 'AV', '49': 'AW', '50': 'AX', '51': 'AY', '52': 'AZ',
                       '53': 'BA', '54': 'BB', '55': 'BC', '56': 'BD', '57': 'BE', '58': 'BF', '59': 'BG',
                       '60': 'BH', '61': 'BI', '62': 'BJ', '63': 'BK', '64': 'BL', '65': 'BM',
                       '66': 'BN', '67': 'BO', '68': 'BP', '69': 'BQ', '70': 'BR', '71': 'BS', '72': 'BT',
                       '73': 'BU', '74': 'BV', '75': 'BW', '76': 'BX', '77': 'BY', '78': 'BZ',
                       '79': 'CA', '80': 'CB', '81': 'CC', '82': 'CD', '83': 'CE', '84': 'CF', '85': 'CG',
                       '86': 'CH', '87': 'CI', '88': 'CJ', '89': 'CK', '90': 'CL', '91': 'CM',
                       '92': 'CN', '93': 'CO', '94': 'CP', '95': 'CQ', '96': 'CR', '97': 'CS', '98': 'CT',
                       '99': 'CU', '100': 'CV', '101': 'CW', '102': 'CX', '103': 'CY', '104': 'CZ', }

            # Ерөнхий мэдээллийн sheet нэмэх
            worksheet_general = workbook.add_worksheet(u'ХХОАТ-н тайлан')
            worksheet_general.set_column('A:A', 10)
            worksheet_general.set_column('B:B', 15)
            worksheet_general.set_column('C:C', 20)
            worksheet_general.set_column('D:D', 20)
            worksheet_general.set_column('E:E', 10)
            worksheet_general.set_column('F:F', 10)
            worksheet_general.set_column('G:G', 15)
            worksheet_general.set_column('H:H', 15)

            worksheet_general.set_row(3, 50)
            worksheet_general.set_row(16, 38.5)
            worksheet_general.set_row(17, 39)
            struct_id = 0
            max_count = 0
            irow = 0
            end_period = template.end_period_id.id
            dateobj = {}
            date_list = []
            date_list.append(end_period)
            i = 0
            while template.start_period_id.id != end_period:
                dateobj = self._get_previous_period(end_period)
                end_period = dateobj['per_id']
                date_list.append(end_period)
                i += 1
            irow = 0
            icol = 0
            worksheet_general.merge_range(irow, icol, irow + 3, icol, u'ТТД', theader4)
            worksheet_general.merge_range(irow, icol + 1, irow + 3, icol + 1, u'Регистрийн дугаар', theader4)
            worksheet_general.merge_range(irow, icol + 2, irow + 3, icol + 2, u'Овог', theader4)
            worksheet_general.merge_range(irow, icol + 3, irow + 3, icol + 3, u'Нэр', theader4)
            worksheet_general.merge_range(irow, icol + 4, irow + 3, icol + 4, u'Орлогын төрөл', theader4)
            worksheet_general.merge_range(irow, icol + 5, irow + 3, icol + 5, u'Орлого олгосон огноо', theader4)
            worksheet_general.merge_range(irow, icol + 6, irow + 3, icol + 6, u'Орлого', theader4)
            worksheet_general.merge_range(irow, icol + 7, irow + 3, icol + 7, u'Суутгуулсан ЭМ-ийн болон НДШ', theader4)

            if not template.employee_ids and not template.department_ids and not template.payslip_run_ids:
                irow += 3
                num = 0
                count = 1
                self._cr.execute('''SELECT id FROM hr_employee ORDER BY name_related ASC, identification_id ASC''')
                employees = self._cr.fetchall()
                emp_ids = []
                for employee in employees:
                    emp_ids.append(employee[0])
                employe_ids = self.env['hr.employee'].search([('id', 'in', emp_ids)])
                for emp in employe_ids:
                    k = 0
                    in_7_1_1 = 0
                    in_7_1_2 = 0
                    in_7_1_3 = 0
                    in_7_1_4 = 0
                    in_7_1_5 = 0
                    in_7_1_6 = 0
                    in_7_1_7 = 0
                    ndsh_1 = 0
                    ndsh_2 = 0
                    for da in date_list:
                        slip_ids = self.env['hr.payslip'].search(
                            [('salary_type', '=', 'last_salary'), ('period_id', '=', da),
                             ('employee_id', '=', emp.id)])
                        for slip in slip_ids:

                            k += 1
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_1' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_1 = self._cr.fetchall()
                            for val in income_7_1_1:
                                in_7_1_1 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_2' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_2 = self._cr.fetchall()
                            for val in income_7_1_2:
                                in_7_1_2 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_3' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_3 = self._cr.fetchall()
                            for val in income_7_1_3:
                                in_7_1_3 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_4' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_4 = self._cr.fetchall()
                            for val in income_7_1_4:
                                in_7_1_4 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_5' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_5 = self._cr.fetchall()
                            for val in income_7_1_5:
                                in_7_1_5 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_6' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_6 = self._cr.fetchall()
                            for val in income_7_1_6:
                                in_7_1_6 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_7' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_7 = self._cr.fetchall()
                            for val in income_7_1_7:
                                in_7_1_7 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='ndsh1' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            ndsh1 = self._cr.fetchall()
                            for val in ndsh1:
                                ndsh_1 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='ndsh2' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            ndsh2 = self._cr.fetchall()
                            for val in ndsh2:
                                ndsh_2 += val[1]

                    total = in_7_1_1 + in_7_1_2 + in_7_1_3 + in_7_1_4 + in_7_1_5 + in_7_1_6 + in_7_1_7
                    total_ndsh = ndsh_1 + ndsh_2
                    emp = self.env['hr.employee'].browse(employee[0])
                    irow += 1
                    worksheet_general.write(irow, icol, 0, content_left)
                    if emp.ssnid:
                        worksheet_general.write(irow, icol + 1, emp.ssnid.upper(), content_left)
                    if emp.last_name:
                        worksheet_general.write(irow, icol + 2, emp.last_name.upper(), content_left)
                    if emp.name_related:
                        worksheet_general.write(irow, icol + 3, emp.name_related.upper(), content_left)
                    worksheet_general.write(irow, icol + 4, 1, content_left)
                    worksheet_general.write(irow, icol + 5, self.end_period_id.date_stop, format_content_float)
                    worksheet_general.write(irow, icol + 6, total, format_content_float)
                    worksheet_general.write(irow, icol + 7, total_ndsh, format_content_float)
                    count += 1
            elif template.employee_ids:
                irow += 3
                count = 1
                emp_ids = []
                for emp in template.employee_ids:
                    emp_ids.append(emp.id)
                empids = str(emp_ids)
                empids = empids.replace("[", "(")
                empids = empids.replace("]", ")")
                self._cr.execute(
                    '''SELECT id FROM hr_employee WHERE has_disability = True and id IN ''' + empids + ''' ORDER BY name_related ASC, identification_id ASC''')
                employees = self._cr.fetchall()
                emp_ids1 = []
                for employee in employees:
                    emp_ids1.append(employee[0])
                icol = 0
                k3 = 0
                for emp in self.env['hr.employee'].browse(emp_ids1):
                    in_7_1_1 = 0
                    in_7_1_2 = 0
                    in_7_1_3 = 0
                    in_7_1_4 = 0
                    in_7_1_5 = 0
                    in_7_1_6 = 0
                    in_7_1_7 = 0
                    ndsh_1 = 0
                    ndsh_2 = 0
                    for da in date_list:
                        slip_ids = self.env['hr.payslip'].search(
                            [('salary_type', '=', 'last_salary'), ('period_id', '=', da),
                             ('employee_id', '=', emp.id)])
                        for slip in slip_ids:

                            k3 += 1
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_1' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_1 = self._cr.fetchall()
                            for val in income_7_1_1:
                                in_7_1_1 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_2' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_2 = self._cr.fetchall()
                            for val in income_7_1_2:
                                in_7_1_2 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_3' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_3 = self._cr.fetchall()
                            for val in income_7_1_3:
                                in_7_1_3 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_4' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_4 = self._cr.fetchall()
                            for val in income_7_1_4:
                                in_7_1_4 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_5' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_5 = self._cr.fetchall()
                            for val in income_7_1_5:
                                in_7_1_5 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_6' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_6 = self._cr.fetchall()
                            for val in income_7_1_6:
                                in_7_1_6 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='income_7_1_7' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            income_7_1_7 = self._cr.fetchall()
                            for val in income_7_1_7:
                                in_7_1_7 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='ndsh1' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            ndsh1 = self._cr.fetchall()
                            for val in ndsh1:
                                ndsh_1 += val[1]
                            self._cr.execute(
                                '''SELECT sr.hhoat_report_type as hhoat_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.hhoat_report_type='ndsh2' AND pl.slip_id=''' + str(
                                    slip.id) + ''' AND pl.salary_rule_id = sr.id''')
                            ndsh2 = self._cr.fetchall()
                            for val in ndsh2:
                                ndsh_2 += val[1]

                    total = in_7_1_1 + in_7_1_2 + in_7_1_3 + in_7_1_4 + in_7_1_5 + in_7_1_6 + in_7_1_7
                    total_ndsh = ndsh_1 + ndsh_2
                    irow += 1
                    worksheet_general.write(irow, icol, 0, content_left)
                    if emp.ssnid:
                        worksheet_general.write(irow, icol + 1, emp.ssnid.upper(), content_left)
                    if emp.last_name:
                        worksheet_general.write(irow, icol + 2, emp.last_name.upper(), content_left)
                    if emp.name_related:
                        worksheet_general.write(irow, icol + 3, emp.name_related.upper(), content_left)
                    worksheet_general.write(irow, icol + 4, 1, content_left)
                    worksheet_general.write(irow, icol + 5, self.end_period_id.date_stop, format_content_float)
                    worksheet_general.write(irow, icol + 6, total, format_content_float)
                    worksheet_general.write(irow, icol + 7, total_ndsh, format_content_float)

                    count += 1
                if k3 > 0:
                    irow += 1
            elif template.payslip_run_ids:
                irow += 3
                count = 1
                num = 0
                run_ids = []
                for run in template.payslip_run_ids:
                    run_ids.append(run.id)
                run_ids = str(run_ids)
                run_ids = run_ids.replace("[", "(")
                run_ids = run_ids.replace("]", ")")
                self._cr.execute('''
                                    SELECT pslp_line.employee_id  FROM hr_payslip_line pslp_line 
                                        LEFT JOIN hr_payslip pslp ON pslp.id=pslp_line.slip_id  
                                        LEFT JOIN hr_payslip_run prun ON prun.id=pslp.payslip_run_id 
                                        LEFT JOIN hr_employee hr_emp ON hr_emp.id=pslp.employee_id 
                                        WHERE hr_emp.has_disability is True and prun.id IN ''' + run_ids + ''' group by pslp_line.employee_id''')
                employees = self._cr.fetchall()
                emp_ids1 = []
                k = 0
                for employee in employees:
                    k += 1
                    k = 0
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='income_7_1_1' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + ''' ''')
                    income_7_1_1 = self._cr.fetchall()
                    in_7_1_1 = income_7_1_1[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='income_7_1_2' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + ''' ''')
                    income_7_1_2 = self._cr.fetchall()
                    in_7_1_2 = income_7_1_2[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='income_7_1_3' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + ''' ''')
                    income_7_1_3 = self._cr.fetchall()
                    in_7_1_3 = income_7_1_3[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='income_7_1_4' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + ''' ''')
                    income_7_1_4 = self._cr.fetchall()
                    in_7_1_4 = income_7_1_4[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='income_7_1_5' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + ''' ''')
                    income_7_1_5 = self._cr.fetchall()
                    in_7_1_5 = income_7_1_5[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='income_7_1_6' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + ''' ''')
                    income_7_1_6 = self._cr.fetchall()
                    in_7_1_6 = income_7_1_6[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='income_7_1_7' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + ''' ''')
                    income_7_1_7 = self._cr.fetchall()
                    in_7_1_7 = income_7_1_7[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr,  hr_payslip pslp WHERE sr.hhoat_report_type='ndsh1' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + '''''')
                    ndsh_1 = self._cr.fetchall()
                    ndsh_1_1 = ndsh_1[0][0]
                    self._cr.execute(
                        '''SELECT coalesce(sum(total),0) FROM hr_payslip_line pl, hr_salary_rule sr, hr_payslip pslp WHERE sr.hhoat_report_type='ndsh2' AND pslp.payslip_run_id IN ''' + run_ids + ''' AND pl.salary_rule_id = sr.id AND pslp.id=pl.slip_id AND pl.employee_id =''' + str(
                            employee[0]) + '''''')
                    ndsh_2 = self._cr.fetchall()
                    ndsh_2_2 = ndsh_2[0][0]

                    total = in_7_1_1 + in_7_1_2 + in_7_1_3 + in_7_1_4 + in_7_1_5 + in_7_1_6 + in_7_1_7
                    total_ndsh = ndsh_1_1 + ndsh_2_2

                    emp = self.env['hr.employee'].browse(employee[0])
                    irow += 1
                    worksheet_general.write(irow, icol, 0, content_left)
                    if emp.ssnid:
                        worksheet_general.write(irow, icol + 1, emp.ssnid.upper(), content_left)
                    if emp.last_name:
                        worksheet_general.write(irow, icol + 2, emp.last_name.upper(), content_left)
                    if emp.name_related:
                        worksheet_general.write(irow, icol + 3, emp.name_related.upper(), content_left)
                    worksheet_general.write(irow, icol + 4, 1, content_left)
                    worksheet_general.write(irow, icol + 5, self.end_period_id.date_stop, content_right)
                    worksheet_general.write(irow, icol + 6, total, format_content_float)
                    worksheet_general.write(irow, icol + 7, total_ndsh, format_content_float)
                    count += 1
                if k > 0:
                    irow += 1

            workbook.close()
            out = base64.encodestring(output.getvalue())
            excel_id = self.env['oderp.report.excel.output'].create({'filedata': out, 'filename': file_name})
            return {
                'name': 'Export Result',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'oderp.report.excel.output',
                'res_id': excel_id.id,
                'view_id': False,
                'context': self._context,
                'type': 'ir.actions.act_window',
                'target': 'new',
                'nodestroy': True,
            }
