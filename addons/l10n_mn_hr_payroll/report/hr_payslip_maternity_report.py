# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from io import BytesIO
import base64
import xlsxwriter
from odoo import api, fields, models, _

class HrPayslipMaternityReport(models.TransientModel):
    '''Бусад тайлан
    '''
    _name = "hr.payslip.maternity.report"
    _description = "Hr Payslip Maternity Report"

    period_id = fields.Many2one('account.period', string='Period', index=True, invisible=False, required=True)
    employee_ids = fields.Many2many('hr.employee', 'report_payslip_employee_rel', 'report_id', 'emp_id', string='Employees')
    payslip_run = fields.Many2one('hr.payslip.run','Hr Payslip Run')

    @api.multi
    def export_report(self):
        for template in self:

            output = BytesIO()
            workbook = xlsxwriter.Workbook(output)
            stype = u'Жирэмсэн амаржсаны тэтгэмж'
            file_name = 'payroll_%s_%s' % (template.period_id.name, stype) + '.xlsx'

            # CELL styles тодорхойлж байна
            h1 = workbook.add_format({'bold': 1})
            h1.set_font_size(14)
            h1.set_font_name('Times New Roman')
            h1.set_align('center')
            h1.set_align('vcenter')
            h1.set_text_wrap()

            h2 = workbook.add_format({'bold': 1})
            h2.set_font_size(8)
            h2.set_font_name('Times New Roman')
            h2.set_border(style=1)
            h2.set_align('center')
            h2.set_align('vcenter')
            h2.set_text_wrap()

            h3 = workbook.add_format()
            h3.set_font_size(12)
            h3.set_font_name('Times New Roman')
            h3.set_align('left')

            footer = workbook.add_format({'bold': 1,'num_format': '#,###.##'})
            footer.set_font_size(10)
            footer.set_font_name('Times New Roman')
            footer.set_align('right')
            footer.set_border(style=1)
            footer.set_bg_color('#83caff')

            theader = workbook.add_format()
            theader.set_font_size(10)
            theader.set_text_wrap()
            theader.set_align('center')
            theader.set_align('vcenter')
            theader.set_border(style=1)
            theader.set_font_name('Times New Roman')
            theader.set_bg_color('#83caff')

            theader_rotate = workbook.add_format()
            theader_rotate.set_font_size(10)
            theader_rotate.set_text_wrap()
            theader_rotate.set_align('center')
            theader_rotate.set_align('vcenter')
            theader_rotate.set_border(style=1)
            theader_rotate.set_font_name('Times New Roman')
            theader_rotate.set_rotation(90)
            theader_rotate.set_bg_color('#83caff')

            content_right = workbook.add_format({'num_format': '#,###.##'})
            content_right.set_text_wrap()
            content_right.set_border(style=1)
            content_right.set_font_size(10)
            content_right.set_align('right')
            content_right.set_font_name('Times New Roman')

            content_left = workbook.add_format()
            content_left.set_text_wrap()
            content_left.set_border(style=1)
            content_left.set_font_size(10)
            content_left.set_align('left')
            content_left.set_font_name('Times New Roman')

            content_left_noborder = workbook.add_format()
            content_left_noborder.set_text_wrap()
            content_left_noborder.set_font_size(12)
            content_left_noborder.set_align('left')
            content_left_noborder.set_font_name('Times New Roman')

            # Ерөнхий мэдээллийн sheet нэмэх
            worksheet_general = workbook.add_worksheet(u'Цалингийн Хүснэгт')
            worksheet_general.set_landscape()
            worksheet_general.set_paper(9)  # A4
            worksheet_general.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
            worksheet_general.fit_to_pages(1, 0)
            worksheet_general.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})

            columns = {'1':'A', '2':'B','3':'C','4':'D','5':'E','6':'F','7':'G','8':'H','9':'I','10':'J','11':'K','12':'L','13':'M',
                '14':'N', '15':'O','16':'P','17':'Q','18':'R','19':'S','20':'T','21':'U','22':'V','23':'W','24':'X','25':'Y','26':'Z',
                '27':'AA', '28':'AB','29':'AC','30':'AD','31':'AE','32':'AF','33':'AG','34':'AH','35':'AI','36':'AJ','37':'AK','38':'AL','39':'AM',
                '40':'AN', '41':'AO','42':'AP','43':'AQ','44':'AR','45':'AS','46':'AT','47':'AU','48':'AV','49':'AW','50':'AX','51':'AY','52':'AZ',
                '53':'BA', '54':'BB','55':'BC','56':'BD','57':'BE','58':'BF','59':'BG','60':'BH','61':'BI','62':'BJ','63':'BK','64':'BL','65':'BM',
                '66':'BN', '67':'BO','68':'BP','69':'BQ','70':'BR','71':'BS','72':'BT','73':'BU','74':'BV','75':'BW','76':'BX','77':'BY','78':'BZ',
                '79':'CA', '80':'CB','81':'CC','82':'CD','83':'CE','84':'CF','85':'CG','86':'CH','87':'CI','88':'CJ','89':'CK','90':'CL','91':'CM',
                '92':'CN', '93':'CO','94':'CP','95':'CQ','96':'CR','97':'CS','98':'CT','99':'CU','100':'CV','101':'CW','102':'CX','103':'CY','104':'CZ',}
            worksheet_general.set_column('A:A', 2)
            worksheet_general.set_column('B:B', 15)
            worksheet_general.set_column('C:C', 7)
            struct_id = 0
            max_count = 0
            irow = 0
            icol = 1
            worksheet_general.merge_range('C1:O2', u"%s-д АЖИЛЛАЖ БУЙ ЭХ-д %s-р САРЫН ТЭТГЭМЖ ОЛГОСОН ЖАГСААЛТ" % (template.payslip_run.company_id.name,template.period_id.name), h1)
            irow += 1
            worksheet_general.write(irow+1, icol+16, u'НДМ№10а',h3)
            irow += 2
            if template.payslip_run:
                icol = 1
                count = 1
                worksheet_general.set_row(irow, 70)
                worksheet_general.set_row(irow+1, 107)
                worksheet_general.merge_range('A' + str(irow+1) + ':A' + str(irow+2), u'№', theader)
                worksheet_general.merge_range('B' + str(irow+1) + ':B' + str(irow+2), u'Даатгуулагчдын нэр, Овог', theader)
                worksheet_general.merge_range('C' + str(irow+1) + ':C' + str(irow+2), u'Регистрийн дугаар', theader_rotate)
                worksheet_general.set_column(icol+1, icol+1, 10)
                worksheet_general.merge_range('D' + str(irow+1) + ':D' + str(irow+2), u'Нийгмийн даатгалын дэвтрийн дугаар', theader_rotate)
                worksheet_general.set_column(icol + 2, icol + 2, 10)
                worksheet_general.merge_range('E' + str(irow+1) + ':G' + str(irow+1), u'Эмнэлгийн хуудас', theader)
                worksheet_general.write(irow+1,icol+3, u'Дугаар', theader_rotate)
                worksheet_general.write(irow+1,icol+4, u'Эхэлсэн он, сар, өдөр', theader_rotate)
                worksheet_general.write(irow+1,icol+5, u'Дууссан он, сар, өдөр', theader_rotate)

                worksheet_general.merge_range('H' + str(irow+1) + ':H' + str(irow+2), u'Шимтгэл төлсөн нийт жил', theader_rotate)
                worksheet_general.merge_range('I' + str(irow+1) + ':I' + str(irow+2), u'Үүнээс сүүлийн 6 сард тасралтгүй төлсөн', theader_rotate)
                worksheet_general.merge_range('J' + str(irow+1) + ':J' + str(irow+2), u'Сүүлийн бүтэн ажилласан 12 сарын хөдөлмөрийн хөлс орлогын дундаж', theader)
                worksheet_general.set_column(icol + 8, icol + 8, 10)
                worksheet_general.merge_range('K' + str(irow+1) + ':L' + str(irow+1), u'Тэтгэмж бодох', theader)
                worksheet_general.write(irow+1,icol+9, u'Ажлын нэг өдөрт ноогдох хөдөлмөрийн хөлс', theader)
                worksheet_general.write(irow+1,icol+10, u'Хувь', theader)
                worksheet_general.merge_range('M' + str(irow+1) + ':M' + str(irow+2), u'Ажлын нэг өдөрт ноогдох тэтгэмж/9*10/', theader)
                worksheet_general.set_column(icol + 11, icol + 11, 10)
                worksheet_general.merge_range('N' + str(irow+1) + ':N' + str(irow+2), u'Тэтгэмж бодох ажлын өдөр', theader)
                worksheet_general.merge_range('O' + str(irow+1) + ':O' + str(irow+2), u'Олгохоор тооцсон тэтгэмжийн дүн', theader)
                worksheet_general.merge_range('P' + str(irow+1) + ':P' + str(irow+2), u'Нийгмийн даатгалын байгууллагаас шалгаж шилжүүлсэн тэтгэмжийн дүн', theader)
                worksheet_general.set_column(icol + 14, icol + 14, 10)
                worksheet_general.merge_range('Q' + str(irow+1) + ':R' + str(irow+2), u'Тайлбар', theader)
                worksheet_general.write(irow+2,icol-1, u'а', theader)
                worksheet_general.write(irow+2,icol, u'б', theader)
                worksheet_general.write(irow+2,icol+1, '1', theader)
                worksheet_general.write(irow+2,icol+2, '2', theader)
                worksheet_general.write(irow+2,icol+3, '3', theader)
                worksheet_general.write(irow+2,icol+4, '4', theader)
                worksheet_general.write(irow+2,icol+5, '5', theader)
                worksheet_general.write(irow+2,icol+6, '6', theader)
                worksheet_general.write(irow+2,icol+7, '7', theader)
                worksheet_general.write(irow+2,icol+8, '8', theader)
                worksheet_general.write(irow+2,icol+9, '9', theader)
                worksheet_general.write(irow+2,icol+10, '10', theader)
                worksheet_general.write(irow+2,icol+11, '11', theader)
                worksheet_general.write(irow+2,icol+12, '12', theader)
                worksheet_general.write(irow+2,icol+13, '13', theader)
                worksheet_general.write(irow+2,icol+14, '14', theader)
                worksheet_general.merge_range(irow+2,icol+15, irow+2, icol+16, '16', theader)
                irow += 3

                for slip in template.payslip_run.slip_ids:
                    worksheet_general.write(irow, icol-1, count,content_right)
                    worksheet_general.write(irow, icol, '%s %s' % (slip.employee_id.name, slip.employee_id.last_name or ''),content_left)
                    worksheet_general.write(irow, icol+1, slip.employee_id.ssnid,content_left)
                    worksheet_general.write(irow, icol+2, slip.employee_id.sinid ,content_left)
                    worksheet_general.write(irow, icol+3, slip.nd_number ,content_left)
                    worksheet_general.write(irow, icol+4, slip.start_date ,content_left)
                    worksheet_general.write(irow, icol+5, slip.end_date ,content_left)
                    worksheet_general.write(irow, icol+6, slip.employee_id.gov_year,content_left)
                    worksheet_general.write(irow, icol+7, '' ,content_left)
                    average_salary = 0
                    average_day = 0
                    for line in slip.line_ids:
                        if line.code == 'MATERNITY_AVERAGE_SALARY':
                            worksheet_general.write(irow, icol + 8, line.amount, content_right)
                            maternity_amount = line.amount
                        if line.code == 'MATERNITY_AVERAGE_DAY':
                            work_day = maternity_amount / line.amount
                            worksheet_general.write(irow, icol + 9, work_day, content_right)
                        if line.code == 'MATERNITY_PERCENT':
                            worksheet_general.write(irow, icol + 10, str(int(line.amount))+u'%', content_right)
                            worksheet_general.write(irow, icol + 11, work_day, content_right)
                        if line.code == 'MATERNITY_DAY':
                            worksheet_general.write(irow, icol + 12, line.amount, content_right)
                        if line.code == 'MATERNITY_NET':
                            worksheet_general.write(irow, icol + 13, line.amount, content_right)
                        worksheet_general.write(irow, icol + 14, '', content_right)
                        worksheet_general.merge_range(irow, icol + 15, irow, icol + 16, '', content_right)
                    count += 1
                    irow += 1
                worksheet_general.write(irow, icol-1,'' ,footer)
                worksheet_general.write(irow, icol,u'Нийт' ,footer)
                worksheet_general.write(irow, icol+1,'' ,footer)
                worksheet_general.write(irow, icol+2,'' ,footer)
                worksheet_general.write(irow, icol+3,'' ,footer)
                worksheet_general.write(irow, icol+4,'' ,footer)
                worksheet_general.write(irow, icol+5,'' ,footer)
                worksheet_general.write(irow, icol+6,'' ,footer)
                worksheet_general.write(irow, icol+7,'' ,footer)
                worksheet_general.write_formula(irow, icol + 8, '{=SUM(J6:J' + str(irow) + ')}', footer)
                worksheet_general.write_formula(irow, icol + 9, '{=SUM(K6:K' + str(irow) + ')}', footer)
                worksheet_general.write(irow, icol + 10, '', footer)
                worksheet_general.write_formula(irow, icol + 11, '{=SUM(M6:M' + str(irow) + ')}', footer)
                worksheet_general.write_formula(irow, icol + 12, '{=SUM(N6:N' + str(irow) + ')}', footer)
                worksheet_general.write_formula(irow, icol + 13, '{=SUM(O6:O' + str(irow) + ')}', footer)
                worksheet_general.write_formula(irow, icol + 14, '{=SUM(P6:P' + str(irow) + ')}', footer)
                worksheet_general.merge_range(irow, icol + 15, irow, icol + 16, '', footer)

                irow += 3


                worksheet_general.merge_range(irow,icol+1,irow,icol+4, u'Дарга:..........................%s '% (template.payslip_run.company_id.executive_signature.name or '/...................../'), content_left_noborder)
                worksheet_general.merge_range(irow,icol+8,irow, icol+16, u'Шалгаж хүлээн авсан:...............-н НДХ-н байцаагч:.........................../........................' , content_left_noborder)
                irow += 1
                worksheet_general.merge_range(irow,icol+1,irow,icol+4,u'Нябо:..............................%s '% (template.payslip_run.company_id.genaral_accountant_signature.name or '/...................../'), content_left_noborder)
                worksheet_general.merge_range(irow,icol+8,irow,icol+16, u'20......оны ......р сарын ....... өдөр' , content_left_noborder)
                irow +=2
            workbook.close()
            out = base64.encodestring(output.getvalue())
            excel_id = self.env['oderp.report.excel.output'].create({'filedata': out, 'filename': file_name})

            return {
                'name': 'Export Result',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'oderp.report.excel.output',
                'res_id': excel_id.id,
                'view_id': False,
                'context': self._context,
                'type': 'ir.actions.act_window',
                'target': 'new',
                'nodestroy': True,
            }