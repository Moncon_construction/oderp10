# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from io import BytesIO
import base64
import xlsxwriter
from datetime import date,datetime,timedelta
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError

class HrPayslipNdshReport(models.TransientModel):
    _name = "hr.payslip.ndsh.report"
    _description = "Hr Payslip Ndsh Report"

    start_period_id = fields.Many2one('account.period', string='Start Period', index=True, invisible=False, required=True)
    end_period_id = fields.Many2one('account.period', string='End Period', index=True, invisible=False, required=True)
    employee_ids = fields.Many2many('hr.employee', 'report_ndsh_payslip_employee_rel', 'report_id', 'emp_id', string='Employees')
    payslip_run_ids = fields.Many2many('hr.payslip.run', 'report_ndsh_payslip_payslip_run_rel', 'report_id', 'payslip_run', string='Payslip run')

    @api.onchange('start_period_id', 'end_period_id')
    def _compute_start_to_end_period(self):
        if self.start_period_id and self.end_period_id:
            if self.start_period_id.date_start > self.end_period_id.date_start:
                raise UserError(_("Start period and end period is not right"))
            end_period = self.end_period_id.id
            date_list = []
            while self.start_period_id.id != end_period:
                dateobj = self._get_previous_period(end_period)
                end_period = dateobj['per_id']
                date_list.append(end_period)
            date_list.append(self.end_period_id.id)
            account_periods = self.env['account.period'].search([('id', 'in', date_list)])
            return {
                'domain': {
                    'payslip_run_ids': [('period_id', 'in', account_periods.ids)]
                }
            }


    @api.multi
    def _get_previous_period(self, now_period_id):
        ret_pre_id = False
        start_date = ''
        user_ids = self.env['res.users'].search([('id','=',self.env.uid)])
        company_id = user_ids.company_id.id
        period_ids = self.env['account.period'].search([('id','=',now_period_id)])
        for period_id in period_ids:
            start_date = period_id.date_start
        last_day = datetime.strptime(start_date,"%Y-%m-%d").date() - timedelta(days=1)
        pre_ids = self.env['account.period'].search([('date_stop','=',last_day.strftime("%Y-%m-%d")),('company_id','=',company_id)])
        for pre_id in pre_ids:
            ret_pre_id = pre_id.id
        res = {'per_id': ret_pre_id}
        return res

    def _check_prove(self, value, checker):
        if sum(c.isalnum() for c in value) == checker:
            return True
        else:
            return False


    @api.multi
    def export_report(self):
        for template in self:
            if not template.employee_ids and not template.payslip_run_ids:
                raise UserError(_('You must choose at least one employee or payslip run '))
            output = BytesIO()
            workbook = xlsxwriter.Workbook(output)

            file_name = u'Нийгмийн даатгалын Тайлан %s сараас %s' % (template.start_period_id.code,template.end_period_id.code) + '.xlsx'

            # CELL styles тодорхойлж байна
            theader = workbook.add_format({'bold': 1})
            theader.set_font_size(12)
            theader.set_text_wrap()
            theader.set_align('center')
            theader.set_align('vcenter')
            theader.set_border(style=1)
            theader.set_bg_color('#a2ccfb')
            theader.set_font_name('Times New Roman')

            content_right = workbook.add_format({'bold': 1, 'num_format': '#,###.##'})
            content_right.set_text_wrap()
            content_right.set_border(style=1)
            content_right.set_font_size(10)
            content_right.set_align('right')
            content_right.set_font_name('Times New Roman')

            content_left = workbook.add_format({'bold': 1})
            content_left.set_text_wrap()
            content_left.set_border(style=1)
            content_left.set_font_size(10)
            content_left.set_align('left')
            content_left.set_font_name('Times New Roman')

            footer = workbook.add_format({'bold': 1,'num_format': '#,###.##'})
            footer.set_font_size(12)
            footer.set_font_name('Times New Roman')
            footer.set_align('right')
            footer.set_border(style=1)
            footer.set_bg_color('#a2ccfb')

            # Ерөнхий мэдээллийн sheet нэмэх
            worksheet_general = workbook.add_worksheet('Sheet1')

            columns = {'1':'A', '2':'B','3':'C','4':'D','5':'E','6':'F','7':'G','8':'H','9':'I','10':'J','11':'K','12':'L','13':'M',
                '14':'N', '15':'O','16':'P','17':'Q','18':'R','19':'S','20':'T','21':'U','22':'V','23':'W','24':'X','25':'Y','26':'Z',
                '27':'AA', '28':'AB','29':'AC','30':'AD','31':'AE','32':'AF','33':'AG','34':'AH','35':'AI','36':'AJ','37':'AK','38':'AL','39':'AM',
                '40':'AN', '41':'AO','42':'AP','43':'AQ','44':'AR','45':'AS','46':'AT','47':'AU','48':'AV','49':'AW','50':'AX','51':'AY','52':'AZ',
                '53':'BA', '54':'BB','55':'BC','56':'BD','57':'BE','58':'BF','59':'BG','60':'BH','61':'BI','62':'BJ','63':'BK','64':'BL','65':'BM',
                '66':'BN', '67':'BO','68':'BP','69':'BQ','70':'BR','71':'BS','72':'BT','73':'BU','74':'BV','75':'BW','76':'BX','77':'BY','78':'BZ',
                '79':'CA', '80':'CB','81':'CC','82':'CD','83':'CE','84':'CF','85':'CG','86':'CH','87':'CI','88':'CJ','89':'CK','90':'CL','91':'CM',
                '92':'CN', '93':'CO','94':'CP','95':'CQ','96':'CR','97':'CS','98':'CT','99':'CU','100':'CV','101':'CW','102':'CX','103':'CY','104':'CZ',}
            struct_id = 0
            max_count = 0
            end_period = template.end_period_id.id
            dateobj = {}
            date_list = []
            date_list.append(end_period)
            i = 0
            while template.start_period_id.id != end_period:
                dateobj = self._get_previous_period(end_period)
                end_period = dateobj['per_id']
                date_list.append(end_period)
                i+=1
            irow = 0
            icol = 1
            worksheet_general.set_row(irow, 30)
            worksheet_general.write( irow, icol-1, 'ovog', theader)
            worksheet_general.set_column(icol-1,icol-1, 15)
            worksheet_general.write( irow, icol, 'name', theader)
            worksheet_general.set_column(icol,icol, 15)
            worksheet_general.write( irow, icol+1, 'register', theader)
            worksheet_general.set_column(icol+1,icol+1, 10)
            worksheet_general.write( irow, icol+2, 'nd_dugaar', theader)
            worksheet_general.set_column(icol+2,icol+2, 10)
            worksheet_general.write( irow, icol+3, 'emd_dugaar', theader)
            worksheet_general.set_column(icol+3,icol+3, 10)
            worksheet_general.write( irow, icol+4, 'undsen_nemegdel_tsalin', theader)
            worksheet_general.set_column(icol+4,icol+4, 10)
            worksheet_general.write( irow, icol+5, 'shagnalt_tsalin', theader)
            worksheet_general.set_column(icol+5,icol+5, 10)
            worksheet_general.write( irow, icol+6, 'busad_nemegdel_tsalin', theader)
            worksheet_general.set_column(icol+6,icol+6, 10)
            worksheet_general.write( irow, icol+7, 'hool_unaa', theader)
            worksheet_general.set_column(icol+7,icol+7, 10)
            worksheet_general.write( irow, icol+8, 'tulee_nuurs', theader)
            worksheet_general.set_column(icol+8,icol+8, 10)
            worksheet_general.write( irow, icol+9, 'niit', theader)
            worksheet_general.set_column(icol+9,icol+9, 10)
            worksheet_general.write( irow, icol+10, 'daat_turul', theader)
            worksheet_general.set_column(icol+10,icol+10, 6)
            worksheet_general.write( irow, icol+11, 'hyazgaar', theader)
            worksheet_general.set_column(icol+11,icol+11, 6)
            worksheet_general.write( irow, icol+12, 'countryid', theader)
            worksheet_general.set_column(icol+12,icol+12, 6)
            worksheet_general.write( irow, icol+13, 'occ_code', theader)
            worksheet_general.set_column(icol+13,icol+13, 6)
            worksheet_general.write( irow, icol+14, 'loc_code', theader)
            worksheet_general.set_column(icol+14,icol+14, 6)
            worksheet_general.write( irow, icol+15, 'cellphone', theader)
            worksheet_general.set_column(icol+15,icol+15, 8)
            worksheet_general.write( irow, icol+16, 'email', theader)
            worksheet_general.set_column(icol+16,icol+16, 15)
            if not template.employee_ids:
                irow += 1
                emp_ids = []
                for obj in template.payslip_run_ids:
                    for employee in obj.slip_ids:
                        emp_ids.append(employee.employee_id.id)
                employe_ids = self.env['hr.employee'].search([('id','in',emp_ids)])
                icol = 0
                for emp in employe_ids:
                    min_salary = self.env['hr.payroll.config'].search([('company_id','=',emp.company_id.id)], limit=1)
                    k = 0
                    wage = 0
                    bonus = 0
                    other_add = 0
                    food_car = 0
                    fuel = 0
                    total = 0
                    for da in date_list:
                        slip_ids = self.env['hr.payslip'].search([('salary_type','=','last_salary'),('period_id','=', da),('employee_id','=', emp.id)])
                        for slip in slip_ids:
                            k+=1
                            self._cr.execute('''SELECT sr.ndsh_report_type as ndsh_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.ndsh_report_type='undsen' AND pl.slip_id='''+str(slip.id)+''' AND pl.salary_rule_id = sr.id''')
                            undsen = self._cr.fetchall()
                            for val in undsen:
                                wage += val[1]
                            self._cr.execute('''SELECT sr.ndsh_report_type as ndsh_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.ndsh_report_type='shagnalt' AND pl.slip_id='''+str(slip.id)+''' AND pl.salary_rule_id = sr.id''')
                            shagnalt = self._cr.fetchall()
                            for val in shagnalt:
                                bonus += val[1]
                            self._cr.execute('''SELECT sr.ndsh_report_type as ndsh_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.ndsh_report_type='busad' AND pl.slip_id='''+str(slip.id)+''' AND pl.salary_rule_id = sr.id''')
                            busad = self._cr.fetchall()
                            for val in busad:
                                other_add += val[1]
                            self._cr.execute('''SELECT sr.ndsh_report_type as ndsh_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.ndsh_report_type='hool' AND pl.slip_id='''+str(slip.id)+''' AND pl.salary_rule_id = sr.id''')
                            hool = self._cr.fetchall()
                            for val in hool:
                                food_car += val[1]
                            self._cr.execute('''SELECT sr.ndsh_report_type as ndsh_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.ndsh_report_type='tulee' AND pl.slip_id='''+str(slip.id)+''' AND pl.salary_rule_id = sr.id''')
                            tulee = self._cr.fetchall()
                            for val in tulee:
                                fuel += val[1]
                            self._cr.execute('''SELECT sr.ndsh_report_type as ndsh_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.ndsh_report_type='niit' AND pl.slip_id='''+str(slip.id)+''' AND pl.salary_rule_id = sr.id''')
                            niit = self._cr.fetchall()
                            for val in niit:
                                total += val[1]

                    worksheet_general.write(irow, icol, emp.last_name.upper() if emp.last_name else '', content_left)
                    worksheet_general.write(irow, icol+1, emp.name_related.upper(), content_left)
                    check_value = template._check_prove(emp.ssnid if emp.ssnid else '', 10)
                    if not check_value:
                        raise ValidationError (_('%s employers ssnid is wrong') % emp.name)
                    worksheet_general.write(irow, icol+2, emp.ssnid , content_left)
                    check_value = template._check_prove(emp.sinid if emp.sinid else '', 7)
                    if not check_value:
                        raise ValidationError(_('%s employers sinid is wrong') % emp.name)
                    worksheet_general.write(irow, icol+3, emp.sinid , content_left)
                    check_value = template._check_prove(emp.health_insurance_no if emp.health_insurance_no else '', 8)
                    if not check_value:
                        raise ValidationError(_('%s employers health insurance is wrong')%emp.name)
                    worksheet_general.write(irow, icol+4, emp.health_insurance_no , content_left)
                    worksheet_general.write(irow, icol+5, wage , content_right)
                    worksheet_general.write(irow, icol+6, bonus , content_right)
                    worksheet_general.write(irow, icol+7, other_add, content_right)
                    worksheet_general.write(irow, icol+8, food_car , content_right)
                    worksheet_general.write(irow, icol+9, fuel , content_right)
                    worksheet_general.write(irow, icol+10, total , content_right)
                    worksheet_general.write(irow, icol+11, emp.contract_id.ndsh_type.code if emp.contract_id.ndsh_type else "", content_right)
                    worksheet_general.write(irow, icol+12, 1 if total >= min_salary.minimum_salary * 10 else 0 , content_right)
                    worksheet_general.write(irow, icol+13, emp.country_id.nd_code , content_right)
                    worksheet_general.write(irow, icol+14, emp.job_id.occupation_id.code , content_left)
                    worksheet_general.write(irow, icol+15, emp.company_id.sum_khoroo_id.code , content_left)
                    if emp.work_phone:
                        worksheet_general.write(irow, icol+16, emp.work_phone , content_left)
                    elif emp.mobile_phone:
                        worksheet_general.write(irow, icol+16, emp.mobile_phone , content_left)
                    else:
                        worksheet_general.write(irow, icol+16, emp.address_home_id.mobile , content_left)
                    if emp.work_email:
                        worksheet_general.write(irow, icol+17, emp.work_email , content_left)
                    else:
                        worksheet_general.write(irow, icol+17, emp.address_home_id.email , content_left)
                    if k > 0:
                        irow += 1
                worksheet_general.write(irow, icol, '',footer)
                worksheet_general.write(irow, icol+1, '',footer)
                worksheet_general.write(irow, icol+2, '',footer)
                worksheet_general.write(irow, icol+3, '',footer)
                worksheet_general.write(irow, icol+4, u'Нийт дүн',footer)
                worksheet_general.write_formula(irow, icol+5, '{=SUM(' + columns[str(icol+5+1)]+ '2:' + columns[str(icol+5+1)] +str(irow)+')}',footer)
                worksheet_general.write_formula(irow, icol+6, '{=SUM(' + columns[str(icol+6+1)]+ '2:' + columns[str(icol+6+1)] +str(irow)+')}',footer)
                worksheet_general.write_formula(irow, icol+7, '{=SUM(' + columns[str(icol+7+1)]+ '2:' + columns[str(icol+7+1)] +str(irow)+')}',footer)
                worksheet_general.write_formula(irow, icol+8, '{=SUM(' + columns[str(icol+8+1)]+ '2:' + columns[str(icol+8+1)] +str(irow)+')}',footer)
                worksheet_general.write_formula(irow, icol+9, '{=SUM(' + columns[str(icol+9+1)]+ '2:' + columns[str(icol+9+1)] +str(irow)+')}',footer)
                worksheet_general.write_formula(irow, icol+10, '{=SUM(' + columns[str(icol+10+1)]+ '2:' + columns[str(icol+10+1)] +str(irow)+')}',footer)
                worksheet_general.write(irow, icol+11, '',footer)
                worksheet_general.write(irow, icol+12, '',footer)
                worksheet_general.write(irow, icol+13, '',footer)
                worksheet_general.write(irow, icol+14, '',footer)
                worksheet_general.write(irow, icol+15, '',footer)
                worksheet_general.write(irow, icol+16, '',footer)
                worksheet_general.write(irow, icol+17, '',footer)
            else:
                irow += 1
                emp_ids = []
                employees= []
                if template.payslip_run_ids:
                    for obj in template.payslip_run_ids:
                        for employee in obj.slip_ids:
                            employees.append(employee.employee_id.id)
                    for emp in template.employee_ids:
                        if emp.id in employees:
                            emp_ids.append(emp.id)
                else:
                    for emp in template.employee_ids:
                        emp_ids.append(emp.id)

                empids = str(emp_ids)
                empids = empids.replace("[", "(")
                empids = empids.replace("]", ")")
                self._cr.execute('''SELECT id FROM hr_employee WHERE id IN '''+ empids +''' ORDER BY name_related ASC, identification_id ASC''')
                employees = self._cr.fetchall()
                emp_ids = []
                for employee in employees:
                    emp_ids.append(employee[0])
                employe_ids = self.env['hr.employee'].search([('id','in',emp_ids)])
                icol = 0
                for emp in employe_ids:
                    hr_payroll_config = self.env['hr.payroll.config'].search([('company_id', '=', emp.company_id.id)], limit=1)
                    k = 0
                    wage = 0
                    bonus = 0
                    other_add = 0
                    food_car = 0
                    fuel = 0
                    total = 0
                    for da in date_list:
                        slip_ids = self.env['hr.payslip'].search([('salary_type','=','last_salary'),('period_id','=', da),('employee_id','=', emp.id)])
                        for slip in slip_ids:
                            k+=1
                            self._cr.execute('''SELECT sr.ndsh_report_type as ndsh_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.ndsh_report_type='undsen' AND pl.slip_id='''+str(slip.id)+''' AND pl.salary_rule_id = sr.id''')
                            undsen = self._cr.fetchall()
                            for val in undsen:
                                wage += val[1]
                            self._cr.execute('''SELECT sr.ndsh_report_type as ndsh_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.ndsh_report_type='shagnalt' AND pl.slip_id='''+str(slip.id)+''' AND pl.salary_rule_id = sr.id''')
                            shagnalt = self._cr.fetchall()
                            for val in shagnalt:
                                bonus += val[1]
                            self._cr.execute('''SELECT sr.ndsh_report_type as ndsh_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.ndsh_report_type='busad' AND pl.slip_id='''+str(slip.id)+''' AND pl.salary_rule_id = sr.id''')
                            busad = self._cr.fetchall()
                            for val in busad:
                                other_add += val[1]
                            self._cr.execute('''SELECT sr.ndsh_report_type as ndsh_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.ndsh_report_type='hool' AND pl.slip_id='''+str(slip.id)+''' AND pl.salary_rule_id = sr.id''')
                            hool = self._cr.fetchall()
                            for val in hool:
                                food_car += val[1]
                            self._cr.execute('''SELECT sr.ndsh_report_type as ndsh_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.ndsh_report_type='tulee' AND pl.slip_id='''+str(slip.id)+''' AND pl.salary_rule_id = sr.id''')
                            tulee = self._cr.fetchall()
                            for val in tulee:
                                fuel += val[1]
                            self._cr.execute('''SELECT sr.ndsh_report_type as ndsh_report_type, pl.total as total FROM hr_payslip_line pl, hr_salary_rule sr WHERE sr.ndsh_report_type='niit' AND pl.slip_id='''+str(slip.id)+''' AND pl.salary_rule_id = sr.id''')
                            niit = self._cr.fetchall()
                            for val in niit:
                                total += val[1]

                    worksheet_general.write(irow, icol, emp.last_name.upper() if emp.last_name else '', content_left)
                    worksheet_general.write(irow, icol+1, emp.name_related.upper(), content_left)
                    check_value = template._check_prove(emp.ssnid if emp.ssnid else '', 10)
                    if not check_value:
                        raise ValidationError (_('%s employers ssnid is wrong') % emp.name)
                    worksheet_general.write(irow, icol+2, emp.ssnid , content_left)
                    check_value = template._check_prove(emp.sinid if emp.sinid else '', 7)
                    if not check_value:
                        raise ValidationError(_('%s employers sinid is wrong') % emp.name)
                    worksheet_general.write(irow, icol+3, emp.sinid, content_left)
                    check_value = template._check_prove(emp.health_insurance_no if emp.health_insurance_no else '', 8)
                    if not check_value:
                        raise ValidationError(_('%s employers health insurance is wrong') % emp.name)
                    worksheet_general.write(irow, icol+4, emp.health_insurance_no , content_left)
                    worksheet_general.write(irow, icol+5, wage , content_right)
                    worksheet_general.write(irow, icol+6, bonus , content_right)
                    worksheet_general.write(irow, icol+7, other_add, content_right)
                    worksheet_general.write(irow, icol+8, food_car , content_right)
                    worksheet_general.write(irow, icol+9, fuel , content_right)
                    worksheet_general.write(irow, icol+9, fuel , content_right)
                    worksheet_general.write(irow, icol+10, total , content_right)
                    worksheet_general.write(irow, icol+11, emp.contract_id.ndsh_type.code if emp.contract_id.ndsh_type else "", content_right)
                    # Хязгаарын утга
                    worksheet_general.write(irow, icol+12, 1 if total >= hr_payroll_config.minimum_salary * 10 else 0, content_right)
                    worksheet_general.write(irow, icol+13, emp.country_id.nd_code, content_right)
                    worksheet_general.write(irow, icol+14, emp.job_id.occupation_id.code , content_left)
                    worksheet_general.write(irow, icol+15, emp.company_id.sum_khoroo_id.code , content_left)
                    if emp.work_phone:
                        worksheet_general.write(irow, icol+16, emp.work_phone , content_left)
                    elif emp.mobile_phone:
                        worksheet_general.write(irow, icol+16, emp.mobile_phone , content_left)
                    else:
                        worksheet_general.write(irow, icol+16, emp.address_home_id.mobile , content_left)
                    if emp.work_email:
                        worksheet_general.write(irow, icol+17, emp.work_email , content_left)
                    else:
                        worksheet_general.write(irow, icol+17, emp.address_home_id.email , content_left)
                    if k > 0:
                        irow += 1
                worksheet_general.write(irow, icol, '',footer)
                worksheet_general.write(irow, icol+1, '',footer)
                worksheet_general.write(irow, icol+2, '',footer)
                worksheet_general.write(irow, icol+3, '',footer)
                worksheet_general.write(irow, icol+4, u'Нийт дүн',footer)
                worksheet_general.write_formula(irow, icol+5, '{=SUM(' + columns[str(icol+5+1)]+ '2:' + columns[str(icol+5+1)] +str(irow)+')}',footer)
                worksheet_general.write_formula(irow, icol+6, '{=SUM(' + columns[str(icol+6+1)]+ '2:' + columns[str(icol+6+1)] +str(irow)+')}',footer)
                worksheet_general.write_formula(irow, icol+7, '{=SUM(' + columns[str(icol+7+1)]+ '2:' + columns[str(icol+7+1)] +str(irow)+')}',footer)
                worksheet_general.write_formula(irow, icol+8, '{=SUM(' + columns[str(icol+8+1)]+ '2:' + columns[str(icol+8+1)] +str(irow)+')}',footer)
                worksheet_general.write_formula(irow, icol+9, '{=SUM(' + columns[str(icol+9+1)]+ '2:' + columns[str(icol+9+1)] +str(irow)+')}',footer)
                worksheet_general.write_formula(irow, icol+10, '{=SUM(' + columns[str(icol+10+1)]+ '2:' + columns[str(icol+10+1)] +str(irow)+')}',footer)
                worksheet_general.write(irow, icol+11, '',footer)
                worksheet_general.write(irow, icol+12, '',footer)
                worksheet_general.write(irow, icol+13, '',footer)
                worksheet_general.write(irow, icol+14, '',footer)
                worksheet_general.write(irow, icol+15, '',footer)
                worksheet_general.write(irow, icol+16, '',footer)
                worksheet_general.write(irow, icol+17, '',footer)
            workbook.close()
            out = base64.encodestring(output.getvalue())
            excel_id = self.env['oderp.report.excel.output'].create({'filedata': out, 'filename': file_name})
            return {
                'name': 'Export Result',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'oderp.report.excel.output',
                'res_id': excel_id.id,
                'view_id': False,
                'context': self._context,
                'type': 'ir.actions.act_window',
                'target': 'new',
                'nodestroy': True,
            }