# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from io import BytesIO
import base64
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from odoo import api, fields, models, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class HrPayslipReport(models.TransientModel):

    _inherit = 'oderp.report.html.output'
    _name = "hr.payslip.report"
    _description = "Hr Payslip Report"

    period_id = fields.Many2one('account.period', string='Period', index=True, invisible=False, required=True)
    one_sheet = fields.Boolean('Print in One sheet', default=True)
    group_by_department = fields.Boolean('Group by department', default=True)
    employee_ids = fields.Many2many('hr.employee', 'report_hr_payslip_employee_rel', 'report_id', 'emp_id', string='Employees')
    category_ids = fields.Many2many('hr.salary.rule.category', 'report_payslip_category_rel', 'report_id', 'categ_id', string='Categories')
    salary_type = fields.Selection([('last_salary','Last salary'),
                                    ('advance_salary','Advance salary'),
                                    ('vacation','Vacation')], string='Salary type', default='last_salary', states={'draft': [('readonly', False)]})
    payslip_run = fields.Many2one('hr.payslip.run', string='Hr Payslip Run')

    def get_emp_name_from_sign(self, sign, company=False):
        self.ensure_one()
        sign_user_id = False
        company = self.env.user.company_id if not company else company 
        
        if sign == 'first' and company.first_sign:
            sign_user_id = company.first_sign.sudo().id
        elif sign == 'second' and company.second_sign_cart:
            sign_user_id = company.second_sign_cart.sudo().id
        elif sign == 'general_acc' and company.genaral_accountant_signature:
            sign_user_id = company.genaral_accountant_signature.sudo().id
            
        if sign_user_id:
            sign_employee = self.env['hr.employee'].search([('company_id', '=', company.sudo().id), ('resource_id.user_id', '=', sign_user_id)], limit=1)
            return [sign_employee.sudo().name or '', sign_employee.sudo().last_name or '']
            
        return ['', '']
    
    def get_first_letter_of_sign(self, sign, company=False):
        company = self.env.user.company_id if not company else company 
        
        first_name = self.get_emp_name_from_sign(sign, company)[0]
        last_name_first_letter = self.get_emp_name_from_sign(sign, company)[1][:1] if len(self.get_emp_name_from_sign(sign, company)[1]) > 0 else ''
        
        return [first_name, last_name_first_letter]

    def export_report(self):
        for template in self:
            context = dict(self._context or {})
            output = BytesIO()
            workbook = xlsxwriter.Workbook(output)
            file_name = 'payroll_%s' % (template.period_id.name)

            # CELL styles тодорхойлж байна
            h1 = workbook.add_format(ReportExcelCellStyles.format_name)
            h2 = workbook.add_format(ReportExcelCellStyles.format_date)
            h3 = workbook.add_format(ReportExcelCellStyles.format_filter)
            footer = workbook.add_format(ReportExcelCellStyles.format_footer_float)
            subfooter = workbook.add_format(ReportExcelCellStyles.format_group_float)
            theader = workbook.add_format(ReportExcelCellStyles.format_title_small)
            format_name = workbook.add_format(ReportExcelCellStyles.format_name)
            content_right = workbook.add_format(ReportExcelCellStyles.format_content_right_float)
            content_left = workbook.add_format(ReportExcelCellStyles.format_content_left_float)
            content_number = workbook.add_format(ReportExcelCellStyles.format_content_number)
            content_center = workbook.add_format(ReportExcelCellStyles.format_content_center_float)
            format_left = workbook.add_format(ReportExcelCellStyles.format_content_left_float)

            # Багануудын толгой тодорхойлох
            columns = {'1': 'A', '2': 'B', '3': 'C', '4': 'D', '5': 'E', '6': 'F', '7': 'G', '8': 'H', '9': 'I', '10': 'J', '11': 'K', '12': 'L', '13': 'M',
                       '14': 'N', '15': 'O', '16': 'P', '17': 'Q', '18': 'R', '19': 'S', '20': 'T', '21': 'U', '22': 'V', '23': 'W', '24': 'X', '25': 'Y', '26': 'Z',
                       '27': 'AA', '28': 'AB', '29': 'AC', '30': 'AD', '31': 'AE', '32': 'AF', '33': 'AG', '34': 'AH', '35': 'AI', '36': 'AJ', '37': 'AK', '38': 'AL', '39': 'AM',
                       '40': 'AN', '41': 'AO', '42': 'AP', '43': 'AQ', '44': 'AR', '45': 'AS', '46': 'AT', '47': 'AU', '48': 'AV', '49': 'AW', '50': 'AX', '51': 'AY', '52': 'AZ',
                       '53': 'BA', '54': 'BB', '55': 'BC', '56': 'BD', '57': 'BE', '58': 'BF', '59': 'BG', '60': 'BH', '61': 'BI', '62': 'BJ', '63': 'BK', '64': 'BL', '65': 'BM',
                       '66': 'BN', '67': 'BO', '68': 'BP', '69': 'BQ', '70': 'BR', '71': 'BS', '72': 'BT', '73': 'BU', '74': 'BV', '75': 'BW', '76': 'BX', '77': 'BY', '78': 'BZ',
                       '79': 'CA', '80': 'CB', '81': 'CC', '82': 'CD', '83': 'CE', '84': 'CF', '85': 'CG', '86': 'CH', '87': 'CI', '88': 'CJ', '89': 'CK', '90': 'CL', '91': 'CM',
                       '92': 'CN', '93': 'CO', '94': 'CP', '95': 'CQ', '96': 'CR', '97': 'CS', '98': 'CT', '99': 'CU', '100': 'CV', '101': 'CW', '102': 'CX', '103': 'CY', '104': 'CZ',}

            struct_id = 0
            max_count = 0
            salary_list = {}
            # Сүүл цалин, Урьдчилсан цалинг сонгосон үед
            if template.salary_type in ('last_salary', 'advance_salary'):
                if template.one_sheet:  # Нэг хуудасанд хэвлэхийг сонгосон үед
                    col_num = 0
                    irow = 0
                    icol = 1
                    worksheets = workbook.add_worksheet(u'Цалингийн хүснэгт')
                    worksheets.set_landscape()
                    worksheets.set_paper(9)  # A4
                    worksheets.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
                    worksheets.fit_to_pages(1, 0)
                    categories = []
                    if template.category_ids: #Нэмэгдэл суутгал буюу төрөл сонгосон үед
                        for cat in template.category_ids:
                            categories.append((cat.id, cat.name, cat.code)) 
                    else: #Нэмэгдэл суутгал сонгоогүй бол:
                        self._cr.execute('''SELECT id, name, code FROM hr_salary_rule_category
                                WHERE salary_type='%s'
                                ORDER BY sequence ASC, create_date ASC''' % template.salary_type)
                        categories = self._cr.fetchall()
                    salary_list = {'max_count' :  len(categories), 'data' : {}, 'worksheet' : worksheets, 'footer' : {}}
                    irow += 2
                    salary_list['worksheet'].write(irow, icol, u"Огноо: %s" % (template.period_id.name), h2)
                    irow += 1
                    row=irow+1
                    icol = 0
                    salary_list['data']['number'] = {'name': u'№', 'code':'DD','col':4,'icol':0}
                    salary_list['data']['employee_code'] = {'name': u'Код', 'code':'CODE','col':10,'icol':1}
                    salary_list['data']['employee_last'] = {'name': u'Овог', 'code': 'LAST_NAME', 'col': 12, 'icol': 2}
                    salary_list['data']['employee_first'] = {'name': u'Нэр', 'code':'FIRST_NAME','col':12,'icol':3}
                    salary_list['data']['work_position'] = {'name': u'Албан тушаал', 'code':'WORK_POSITION','col':22,'icol':4}
                    salary_list['worksheet'].set_row(irow, 40)
                    salary_list['worksheet'].write(irow, icol, salary_list['data']['number']['name'], theader)
                    salary_list['worksheet'].set_column(icol,icol, salary_list['data']['number']['col'])
                    salary_list['worksheet'].write(irow, icol+1,  salary_list['data']['employee_code']['name'], theader)
                    salary_list['worksheet'].set_column(icol+1,icol+1, salary_list['data']['employee_code']['col'])
                    salary_list['worksheet'].write(irow, icol+2,  salary_list['data']['employee_last']['name'], theader)
                    salary_list['worksheet'].set_column(icol+2,icol+2, salary_list['data']['employee_last']['col'])
                    salary_list['worksheet'].write(irow, icol+3,  salary_list['data']['employee_first']['name'], theader)
                    salary_list['worksheet'].set_column(icol+3,icol+3, salary_list['data']['employee_first']['col'])
                    salary_list['worksheet'].write(irow, icol+4,  salary_list['data']['work_position']['name'], theader)
                    salary_list['worksheet'].set_column(icol+4,icol+4, salary_list['data']['work_position']['col'])
                    salary_columns = []
                    icol = 5

                    for category in categories:
                        salary_list['data'][category[2]] = {'name': category[1], 'code':category[2],'col':20,'icol':icol}
                        salary_list['worksheet'].write(irow, icol, salary_list['data'][category[2]]['name'], theader)
                        salary_list['worksheet'].set_column(icol,icol, salary_list['data'][category[2]]['col'])
                        salary_list['footer'][category[2]] = 0
                        salary_columns.append(category[2])
                        icol += 1
                    salary_list['data']['ndcode'] = {'name': u'НД-н код', 'code': 'NDCODE', 'col': 10, 'icol': icol}
                    salary_list['worksheet'].write(irow, icol, salary_list['data']['ndcode']['name'], theader)
                    salary_list['worksheet'].set_column(icol, icol, salary_list['data']['ndcode']['col'])
                    icol += 1
                    salary_list['data']['bank_name'] = {'name': u'БАНКНЫ НЭР', 'code': 'BANK_NAME', 'col': 10, 'icol': icol}
                    salary_list['worksheet'].write(irow, icol, salary_list['data']['bank_name']['name'],
                                                   theader)
                    salary_list['worksheet'].set_column(icol, icol, salary_list['data']['bank_name']['col'])
                    icol += 1
                    salary_list['data']['bank_account'] = {'name': u'БАНКНЫ ДАНС', 'code': 'BANK_ACCOUNT', 'col': 10, 'icol': icol}
                    salary_list['worksheet'].write(irow, icol, salary_list['data']['bank_account']['name'],
                                                   theader)
                    salary_list['worksheet'].set_column(icol, icol, salary_list['data']['bank_account']['col'])
                    icol += 1

                    checklist = {}
                    count_emp = 0
                    emp_ids =[]
                    department_ids = []
                    number = 1
                    where = ''
                    if template.payslip_run: #Цалингийн хуудсын багц сонгосон үед
                        if template.group_by_department: #Хэлтэсээр бүлэглэх
                            temp_ids =[]
                            if template.employee_ids:
                                for emp in template.employee_ids:
                                    temp_ids.append((emp.id))
                                    where = " AND hp.employee_id in "+'(' + ','.join(map(str, temp_ids)) + ')'
                            salary_list['worksheet'].write(0, 9, u"%s %s" % (template.payslip_run.company_id.name.upper(), template.payslip_run.name.upper()), h1)
                            salary_list['worksheet'].set_row(1, 40)
                            salary_list['worksheet'].write(1, 2, u'Баталсан: ........................ %s.%s' % (self.get_emp_name_from_sign('first', template.payslip_run.company_id)[1], self.get_emp_name_from_sign('first', template.payslip_run.company_id)[0]), h3)
                            self._cr.execute("""
                                SELECT
                                    hd.id
                                FROM
                                    hr_payslip hp,
                                    hr_employee he,
                                    hr_department hd
                                WHERE
                                    hp.payslip_run_id = %s
                                    """ + where + """
                                    AND hp.employee_id = he.id
                                    AND he.department_id = hd.id
                                GROUP BY
                                    hd.id
                                ORDER BY
                                    hd.id ASC""", ([template.payslip_run.id]))
                            departments = self._cr.fetchall()
                            for department in departments:
                                department_ids.append(department[0])
                            departments = self.env['hr.department'].search([('id','in', tuple(department_ids))], order = 'sequence')
                            for dep in departments:
                                temp_ids =[]
                                emp_ids =[]
                                self._cr.execute("""select hp.employee_id,hd.id 
                                    from hr_payslip hp, hr_employee he, hr_department hd 
                                    where hp.payslip_run_id=%s  """+where+""" and hp.employee_id=he.id and he.department_id=hd.id and hd.id=%s
                                    order by hd.id asc""",(template.payslip_run.id, dep.id))
                                
                                employees = self._cr.fetchall()
                                for employee in employees:
                                    emp_ids.append(employee[0])
                                dep_header = {}
                                for key in salary_columns:
                                    dep_header[key] = 0
                                deprow = row
                                row+=1
                                    
                                slip_ids = self.env['hr.payslip'].search([('period_id','=', template.period_id.id),('salary_type','=', template.salary_type),('employee_id','in', emp_ids),('payslip_run_id','=', template.payslip_run.id)],order = 'name')
                                icol = 5
                                for slip in slip_ids:
                                    salary_list['worksheet'].write(row, salary_list['data']['number']['icol'], number,content_number)
                                    salary_list['worksheet'].write(row, salary_list['data']['employee_code']['icol'], slip.employee_id.ssnid , content_left)
                                    salary_list['worksheet'].write(row, salary_list['data']['employee_last']['icol'],slip.employee_id.last_name or '', content_left)
                                    salary_list['worksheet'].write(row, salary_list['data']['employee_first']['icol'], slip.employee_id.name , content_left)
                                    salary_list['worksheet'].write(row, salary_list['data']['work_position']['icol'], slip.employee_id.job_id.name or '' , content_left)
                                    i=0
                                    for i in range(0, salary_list['max_count']):
                                        salary_list['worksheet'].write(row, i+5, '',content_right)
                                        i+=1
                                    for line in slip.line_ids:
                                        if line.code in salary_list['data'].keys():
                                            salary_list['worksheet'].write(row, salary_list['data'][line.code]['icol'], line.amount,content_right)
                                            col_num = salary_list['data'][line.code]['icol']
                                            salary_list['footer'][line.code] += line.amount
                                            dep_header[line.code] += line.amount
                                            if line.salary_rule_id.show_in_report:
                                                checklist[line.code] = False
                                            else:
                                                checklist[line.code] = True
                                        icol += 1
                                        if line.code == 'NET':
                                            if line.amount >0:
                                                count_emp +=1
                                        icol += 1
                                    salary_list['worksheet'].write(row, salary_list['data']['ndcode']['icol'],
                                                                   slip.employee_id.contract_id.ndsh_type.code or '-' if slip.employee_id.contract_id else '-',
                                                                   content_center)
                                    salary_list['worksheet'].write(row,salary_list['data']['bank_name']['icol'],
                                                                   slip.employee_id.contract_id.bank_account_id.bank_id.name or '-' if slip.employee_id.contract_id and slip.employee_id.contract_id.bank_account_id else '-',
                                                                   content_center)
                                    salary_list['worksheet'].write(row,salary_list['data']['bank_account']['icol'],
                                                                   slip.employee_id.contract_id.bank_account_id.acc_number or '-' if slip.employee_id.contract_id and slip.employee_id.contract_id.bank_account_id else '-',
                                                                   content_center)
                                    salary_list['worksheet'].set_row(row, None, None, {'level': 1})
                                    row+=1
                                    number += 1
                                salary_list['worksheet'].merge_range('A%s:E%s' % (str(deprow+1),str(deprow+1)), u'%s' % dep.name, subfooter)
                                i = 5
                                for key in salary_columns:
                                    salary_list['worksheet'].write(deprow, i, dep_header[key],subfooter)
                                    i += 1

                                salary_list['worksheet'].write(deprow, i, '', subfooter)
                                i += 1
                                salary_list['worksheet'].write(deprow, i, '', subfooter)
                                i += 1
                                salary_list['worksheet'].write(deprow, i, '', subfooter)
                            temp_ids = []
                            emp_ids = []
                            self._cr.execute("""
                                select
                                    hp.employee_id
                                    --, hd.id
                                from
                                    hr_payslip hp,
                                    hr_employee he
                                where
                                    hp.payslip_run_id = """ + str(template.payslip_run.id) + """
                                    """ + where + """
                                    and hp.employee_id = he.id
                                    and he.department_id is null""")
                            
                            employees = self._cr.fetchall()
                            for employee in employees:
                                emp_ids.append(employee[0])
                            dep_header = {}
                            for key in salary_columns:
                                dep_header[key] = 0
                            deprow = row
                            row+=1
                                
                            slip_ids = self.env['hr.payslip'].search([('period_id','=', template.period_id.id),('salary_type','=', template.salary_type),('employee_id','in', emp_ids),('payslip_run_id','=', template.payslip_run.id)],order = 'name')
                            icol = 5
                            for slip in slip_ids:
                                salary_list['worksheet'].write(row, salary_list['data']['number']['icol'], number,content_number)
                                salary_list['worksheet'].write(row, salary_list['data']['employee_code']['icol'], slip.employee_id.ssnid , content_left)
                                salary_list['worksheet'].write(row, salary_list['data']['employee_last']['icol'],slip.employee_id.last_name or '', content_left)
                                salary_list['worksheet'].write(row, salary_list['data']['employee_first']['icol'], slip.employee_id.name , content_left)
                                salary_list['worksheet'].write(row, salary_list['data']['work_position']['icol'], slip.employee_id.job_id.name or '' , content_left)
                                i=0
                                for i in range(0, salary_list['max_count']):
                                    salary_list['worksheet'].write(row, i+5, '',content_right)
                                    i+=1
                                for line in slip.line_ids:
                                    if line.code in salary_list['data'].keys():
                                        salary_list['worksheet'].write(row, salary_list['data'][line.code]['icol'], line.amount,content_right)
                                        col_num = salary_list['data'][line.code]['icol']
                                        salary_list['footer'][line.code] += line.amount
                                        dep_header[line.code] += line.amount
                                        if line.salary_rule_id.show_in_report:
                                            checklist[line.code] = False
                                        else:
                                            checklist[line.code] = True
                                    icol += 1
                                    if line.code == 'NET':
                                        if line.amount >0:
                                            count_emp +=1
                                    icol += 1
                                salary_list['worksheet'].write(row, salary_list['data']['ndcode']['icol'],
                                                               slip.employee_id.contract_id.ndsh_type.code or '-' if slip.employee_id.contract_id else '-',
                                                               content_center)
                                salary_list['worksheet'].write(row, salary_list['data']['bank_name']['icol'],
                                                               slip.employee_id.contract_id.bank_account_id.bank_id.name or '-' if slip.employee_id.contract_id and slip.employee_id.contract_id.bank_account_id else '-',
                                                               content_center)
                                salary_list['worksheet'].write(row, salary_list['data']['bank_account']['icol'],
                                                               slip.employee_id.contract_id.bank_account_id.acc_number or '-' if slip.employee_id.contract_id and slip.employee_id.contract_id.bank_account_id else '-',
                                                               content_center)
                                salary_list['worksheet'].set_row(row, None, None, {'level': 1})
                                row+=1
                                number += 1
                            row -= 1
                            if slip_ids:
                                row +=1
                                salary_list['worksheet'].merge_range('A%s:E%s' % (str(deprow+1),str(deprow+1)), u'%s' % _('Not defined'), subfooter)
                                i = 5
                                for key in salary_columns:
                                    salary_list['worksheet'].write(deprow, i, dep_header[key],subfooter)
                                    i += 1
                                salary_list['worksheet'].write(deprow, i, '', subfooter)
                                i += 1
                                salary_list['worksheet'].write(deprow, i, '', subfooter)
                                i += 1
                                salary_list['worksheet'].write(deprow, i, '', subfooter)

                        else: #Хэлтэсээр бүлэглээгүй үед
                            salary_list['worksheet'].write(0, 9, u"%s %s" % (template.payslip_run.company_id.name.upper(), template.payslip_run.name.upper()), h1)
                            salary_list['worksheet'].set_row(1, 40)
                            salary_list['worksheet'].write(1, 2, u'Баталсан: ........................ %s.%s' % (self.get_emp_name_from_sign('first', template.payslip_run.company_id)[1], self.get_emp_name_from_sign('first', template.payslip_run.company_id)[0]),h1)
                            slip_ids = self.env['hr.payslip'].search([('payslip_run_id','=', template.payslip_run.id)],order = 'name')
                            icol = 5
                            for slip in slip_ids:
                                salary_list['worksheet'].write(row, salary_list['data']['number']['icol'], number,content_number)
                                salary_list['worksheet'].write(row, salary_list['data']['employee_code']['icol'], slip.employee_id.ssnid , content_left)
                                salary_list['worksheet'].write(row, salary_list['data']['employee_last']['icol'], slip.employee_id.last_name or '', content_left)
                                salary_list['worksheet'].write(row, salary_list['data']['employee_first']['icol'], slip.employee_id.name , content_left)
                                salary_list['worksheet'].write(row, salary_list['data']['work_position']['icol'], slip.employee_id.job_id.name or '' , content_left)
                                i=0
                                for i in range(0, salary_list['max_count']):
                                    salary_list['worksheet'].write(row, i+5, '',content_right)
                                    i+=1
                                for line in slip.line_ids:
                                    if line.code in salary_list['data'].keys():
                                        salary_list['worksheet'].write(row, salary_list['data'][line.code]['icol'], line.amount,content_right)
                                        col_num = salary_list['data'][line.code]['icol']
                                        salary_list['footer'][line.code] += line.amount
                                        if line.salary_rule_id.show_in_report:
                                            checklist[line.code] = False
                                        else:
                                            checklist[line.code] = True
                                    icol += 1
                                    if line.code == 'NET':
                                        if line.amount >0:
                                            count_emp +=1
                                    icol += 1
                                salary_list['worksheet'].write(row, salary_list['data']['ndcode']['icol'],
                                                               slip.employee_id.contract_id.ndsh_type.code or '-' if slip.employee_id.contract_id else '-',
                                                               content_center)
                                salary_list['worksheet'].write(row,salary_list['data']['bank_name']['icol'],
                                                               slip.employee_id.contract_id.bank_account_id.bank_id.name or '-' if slip.employee_id.contract_id and slip.employee_id.contract_id.bank_account_id else '-',
                                                               content_center)
                                salary_list['worksheet'].write(row,salary_list['data']['bank_account']['icol'],
                                                               slip.employee_id.contract_id.bank_account_id.acc_number or '-' if slip.employee_id.contract_id and slip.employee_id.contract_id.bank_account_id else '-',
                                                               content_center)
                                salary_list['worksheet'].set_row(row, None, None, {'level': 1})
                                row+=1
                                number += 1
                    elif template.employee_ids:  
                        company = False
                        if template.salary_type == 'advance_salary':
                            name = u'Урьдчилгаа цалин'
                        if template.salary_type == 'last_salary':
                            name = u'Сүүл цалин'
                        for user in self.env['res.users'].browse(self.env.uid):
                            company = user.company_id
                        if company :     
                            tem_emp_ids = []
                            for emp in template.employee_ids:
                                tem_emp_ids.append((emp.id))
                            if template.group_by_department:
                                salary_list['worksheet'].write(0, 2, u"%s %s %s" % (company.name,template.period_id.name,name), h1)
                                salary_list['worksheet'].set_row(1, 40)
                                salary_list['worksheet'].write(1, 2, u'Батласан: ........................ %s.%s' % (self.get_first_letter_of_sign('first', company)[1], self.get_emp_name_from_sign('first', company)[0]),h1)
                                self._cr.execute('''select hd.id
                                    from hr_payslip hp, hr_employee he, hr_department hd 
                                    where hp.salary_type = '%s'  and hp.employee_id=he.id and he.department_id=hd.id and hp.company_id=%s and he.id in %s group by hd.id
                                    order by hd.id asc''' % (template.salary_type, company.id, tuple(tem_emp_ids)))
                                departments = self._cr.fetchall()
                                
                                for department in departments:
                                    department_ids.append(department[0])
                                departments = self.env['hr.department'].search([('id','in', tuple(department_ids))],order = 'sequence')
                                for dep in departments:
                                    emp_ids =[]
                                    self._cr.execute('''select hp.employee_id,hd.id 
                                        from hr_payslip hp, hr_employee he, hr_department hd 
                                        where hp.salary_type = '%s' and hp.period_id = '%s' and hp.employee_id=he.id and he.department_id=hd.id and hd.id=%s and hp.employee_id in %s 
                                        order by hd.id asc''' % (template.salary_type,template.period_id.id,dep.id,tuple(tem_emp_ids)))
                                    employees = self._cr.fetchall()
                                    for employee in employees:
                                        emp_ids.append(employee[0])
                                    dep_header = {}
                                    for key in salary_columns:
                                        dep_header[key] = 0
                                    deprow = row
                                    row+=1
                                    slip_ids = self.env['hr.payslip'].search([('period_id','=', template.period_id.id),('salary_type','=', template.salary_type),('employee_id','in',emp_ids )], order='name')
                                    icol = 6
                                    for slip in slip_ids:                                        
                                        salary_list['worksheet'].write(row, salary_list['data']['number']['icol'], number,content_number)
                                        salary_list['worksheet'].write(row, salary_list['data']['employee_code']['icol'], slip.employee_id.ssnid , content_left)
                                        salary_list['worksheet'].write(row, salary_list['data']['employee_last']['icol'], slip.employee_id.last_name or '', content_left)
                                        salary_list['worksheet'].write(row, salary_list['data']['employee_first']['icol'], slip.employee_id.name , content_left)
                                        salary_list['worksheet'].write(row, salary_list['data']['work_position']['icol'], slip.employee_id.job_id.name or '', content_left)
                                        i=0
                                        
                                        for i in range(0, salary_list['max_count']):
                                            salary_list['worksheet'].write(row, i+5, '',content_right)
                                            i+=1
                                        for line in slip.line_ids:
                                            if line.code in salary_list['data'].keys():
                                                salary_list['worksheet'].write(row, salary_list['data'][line.code]['icol'], line.amount,content_right)
                                                col_num = salary_list['data'][line.code]['icol']
                                                salary_list['footer'][line.code] += line.amount
                                                dep_header[line.code] += line.amount
                                                if line.salary_rule_id.show_in_report:
                                                    checklist[line.code] = False
                                                else:
                                                    checklist[line.code] = True
                                            icol += 1
                                            if line.code == 'NET':
                                                if line.amount >0:
                                                    count_emp +=1
                                            icol += 1
                                        salary_list['worksheet'].write(row, salary_list['data']['ndcode']['icol'],
                                                                       slip.employee_id.contract_id.ndsh_type.code or '-' if slip.employee_id.contract_id else '-',
                                                                       content_center)
                                        salary_list['worksheet'].write(row, salary_list['data']['bank_name']['icol'],
                                                                       slip.employee_id.contract_id.bank_account_id.bank_id.name or '-' if slip.employee_id.contract_id and slip.employee_id.contract_id.bank_account_id else '-',
                                                                       content_center)
                                        salary_list['worksheet'].write(row, salary_list['data']['bank_account']['icol'],
                                                                       slip.employee_id.contract_id.bank_account_id.acc_number or '-' if slip.employee_id.contract_id and slip.employee_id.contract_id.bank_account_id else '-',
                                                                       content_center)
                                        salary_list['worksheet'].set_row(row, None, None, {'level': 1})
                                        row+=1
                                        number += 1
                                    salary_list['worksheet'].merge_range('A%s:F%s' % (str(deprow+1),str(deprow+1)), u'%s' % dep.name, subfooter)
                                    i = 5
                                    for key in salary_columns:
                                        salary_list['worksheet'].write(deprow, i, dep_header[key],subfooter)
                                        i += 1
                                    salary_list['worksheet'].write(deprow, i, '', subfooter)
                                    i += 1
                                    salary_list['worksheet'].write(deprow, i, '', subfooter)
                                    i += 1
                                    salary_list['worksheet'].write(deprow, i, '', subfooter)
                            else:
                                salary_list['worksheet'].write(0, 2, u"%s %s %s" % (company.name,template.period_id.name,name), h1)
                                salary_list['worksheet'].set_row(1, 40)
                                salary_list['worksheet'].write(1, 2, u'Батласан: ........................ %s.%s' % (self.get_first_letter_of_sign('first', company)[1], self.get_emp_name_from_sign('first', company)[0]),h1)
                                slip_ids = self.env['hr.payslip'].search([('period_id','=', template.period_id.id),('salary_type','=', template.salary_type),('employee_id','in',tem_emp_ids)], order='name')
                                icol = 5
                                for slip in slip_ids:
                                    salary_list['worksheet'].write(row, salary_list['data']['number']['icol'], number,content_number)
                                    salary_list['worksheet'].write(row, salary_list['data']['employee_code']['icol'], slip.employee_id.ssnid , content_left)
                                    salary_list['worksheet'].write(row, salary_list['data']['employee_last']['icol'], slip.employee_id.last_name or '', content_left)
                                    salary_list['worksheet'].write(row, salary_list['data']['employee_first']['icol'], slip.employee_id.name , content_left)
                                    salary_list['worksheet'].write(row, salary_list['data']['work_position']['icol'], slip.employee_id.job_id.name or '', content_left)
                                    i=0
                                    for i in range(0, salary_list['max_count']):
                                        salary_list['worksheet'].write(row, i+6, '',content_right)
                                        i+=1
                                    for line in slip.line_ids:
                                        if line.code in salary_list['data'].keys():
                                            salary_list['worksheet'].write(row, salary_list['data'][line.code]['icol'], line.amount,content_right)
                                            col_num = salary_list['data'][line.code]['icol']
                                            salary_list['footer'][line.code] += line.amount
                                            if line.salary_rule_id.show_in_report:
                                                checklist[line.code] = False
                                            else:
                                                checklist[line.code] = True
                                        icol += 1
                                        if line.code == 'NET':
                                            if line.amount >0:
                                                count_emp +=1
                                        icol += 1
                                    salary_list['worksheet'].write(row, salary_list['data']['ndcode']['icol'],
                                                                   slip.employee_id.contract_id.ndsh_type.code or '-' if slip.employee_id.contract_id else '-',
                                                                   content_center)
                                    salary_list['worksheet'].write(row, salary_list['data']['bank_name']['icol'],
                                                                   slip.employee_id.contract_id.bank_account_id.bank_id.name or '-' if slip.employee_id.contract_id and slip.employee_id.contract_id.bank_account_id else '-',
                                                                   content_center)
                                    salary_list['worksheet'].write(row, salary_list['data']['bank_account']['icol'],
                                                                   slip.employee_id.contract_id.bank_account_id.acc_number or '-' if slip.employee_id.contract_id and slip.employee_id.contract_id.bank_account_id else '-',
                                                                   content_center)
                                    salary_list['worksheet'].set_row(row, None, None, {'level': 1})
                                    row+=1
                                    number += 1
                    
                    else:
                        company = False
                        for user in self.env['res.users'].browse(self.env.uid):
                            company = user.company_id
                        if company :
                            number = 1
                            if template.salary_type == 'advance_salary':
                                name = u'Урьдчилгаа цалин'
                            if template.salary_type == 'last_salary':
                                name = u'Сүүл цалин'
                            salary_list['worksheet'].write(0, 2, u"%s %s %s" % (company.name.upper(),template.period_id.name.upper(),name.upper()), h1)
                            salary_list['worksheet'].set_row(1, 40)
                            if template.group_by_department: #Цалингын төрөлд хамаарагдах ажилтан хэлтэстэй бол
                                self._cr.execute('''select hd.id
                                    from hr_payslip hp, hr_employee he, hr_department hd 
                                    where hp.salary_type='%s' and hp.employee_id=he.id and he.department_id=hd.id 
                                    and hp.company_id=%s group by hd.id
                                    order by hd.id asc''' % (template.salary_type, company.id))
                                departments = self._cr.fetchall()
                                for department in departments:
                                    department_ids.append(department[0])
                                departments = self.env['hr.department'].search([('id','in', department_ids)],order = 'sequence')
                                for dep in departments:
                                    dep_header = {}
                                    for key in salary_columns:
                                        dep_header[key] = 0
                                    deprow = row
                                    row+=1
                                    slip_ids = self.env['hr.payslip'].search([('period_id','=', template.period_id.id),('salary_type','=', template.salary_type),('department_id','=',dep.id)], order = 'name')
                                    icol = 5
                                    for slip in slip_ids:
                                        salary_list['worksheet'].write(row, salary_list['data']['number']['icol'], number,content_number)
                                        salary_list['worksheet'].write(row, salary_list['data']['employee_code']['icol'], slip.employee_id.ssnid , content_left)
                                        salary_list['worksheet'].write(row, salary_list['data']['employee_last']['icol'], slip.employee_id.last_name or '', content_left)
                                        salary_list['worksheet'].write(row, salary_list['data']['employee_first']['icol'], slip.employee_id.name , content_left)
                                        salary_list['worksheet'].write(row, salary_list['data']['work_position']['icol'], slip.employee_id.job_id.name or '', content_left)
                                        i=0
                                        for i in range(0, salary_list['max_count']):
                                            salary_list['worksheet'].write(row, i+5, '',content_right)
                                            i+=1
                                        for line in slip.line_ids:
                                            if line.code in salary_list['data'].keys():
                                                salary_list['worksheet'].write(row, salary_list['data'][line.code]['icol'], line.amount,content_right)
                                                col_num = salary_list['data'][line.code]['icol']
                                                salary_list['footer'][line.code] += line.amount
                                                dep_header[line.code] += line.amount
                                                if line.salary_rule_id.show_in_report:
                                                    checklist[line.code] = False
                                                else:
                                                    checklist[line.code] = True
                                            icol += 1
                                            if line.code == 'NET':
                                                if line.amount >0:
                                                    count_emp +=1
                                            icol += 1
                                        salary_list['worksheet'].write(row, salary_list['data']['ndcode']['icol'],
                                                                       slip.employee_id.contract_id.ndsh_type.code or '-' if slip.employee_id.contract_id else '-',
                                                                       content_center)
                                        salary_list['worksheet'].write(row,salary_list['data']['bank_name']['icol'],
                                                                       slip.employee_id.contract_id.bank_account_id.bank_id.name or '-' if slip.employee_id.contract_id and slip.employee_id.contract_id.bank_account_id else '-',
                                                                       content_center)
                                        salary_list['worksheet'].write(row,salary_list['data']['bank_account']['icol'],
                                                                       slip.employee_id.contract_id.bank_account_id.acc_number or '-' if slip.employee_id.contract_id and slip.employee_id.contract_id.bank_account_id else '-',
                                                                       content_center)
                                        salary_list['worksheet'].set_row(row, None, None, {'level': 1})
                                        row+=1
                                        number += 1
                                    salary_list['worksheet'].merge_range('A%s:E%s' % (str(deprow+1),str(deprow+1)), u'%s' % dep.name, subfooter)
                                    i = 5
                                    for key in salary_columns:
                                        salary_list['worksheet'].write(deprow, i, dep_header[key],subfooter)
                                        i += 1
                                    salary_list['worksheet'].write(deprow, i, '', subfooter)
                                    i += 1
                                    salary_list['worksheet'].write(deprow, i, '', subfooter)
                                    i += 1
                                    salary_list['worksheet'].write(deprow, i, '', subfooter)
                            else: #Цалингын төрөлд хамаарагдах ажилтан хэлтэсгүй бол
                                self._cr.execute('''select employee_id from hr_payslip where salary_type='%s' ''' % (template.salary_type))
                                employees = self._cr.fetchall()
                                for employee in employees:
                                    emp_ids.append(employee[0])
                                hr_emp_ids = []
                                hr_emp_ids = str(emp_ids)
                                hr_emp_ids = hr_emp_ids.replace("[", "(")
                                hr_emp_ids = hr_emp_ids.replace("]", ")")
                                self._cr.execute('''SELECT id FROM hr_employee WHERE id IN '''+ hr_emp_ids +''' ORDER BY name_related ASC,last_name ASC, ssnid ASC''')
                                emps = self._cr.fetchall()
                                employee_ids = []
                                
                                for employee in emps: 
                                    employee_ids.append(employee[0])
                                
                                for emp in self.env['hr.employee'].browse(employee_ids):
                                    slip_ids = self.env['hr.payslip'].search([('period_id','=', template.period_id.id),('salary_type','=', template.salary_type),('employee_id','=', emp.id)])
                                    icol = 5
                                    for slip in slip_ids:
                                        salary_list['worksheet'].write(row, salary_list['data']['number']['icol'], number ,content_number)
                                        salary_list['worksheet'].write(row, salary_list['data']['employee_code']['icol'], slip.employee_id.ssnid , content_left)
                                        salary_list['worksheet'].write(row, salary_list['data']['employee_last']['icol'], slip.employee_id.last_name or '', content_left)
                                        salary_list['worksheet'].write(row, salary_list['data']['employee_first']['icol'], slip.employee_id.name , content_left)
                                        salary_list['worksheet'].write(row, salary_list['data']['work_position']['icol'], slip.employee_id.job_id.name or '', content_left)
                                        i=0
                                        for i in range(0, salary_list['max_count']):
                                            salary_list['worksheet'].write(row, i+5, '',content_right)
                                            i+=1
                                        for line in slip.line_ids:
                                            if line.code in salary_list['data'].keys():
                                                salary_list['worksheet'].write(row, salary_list['data'][line.code]['icol'], line.amount,content_right)
                                                col_num = salary_list['data'][line.code]['icol']
                                                salary_list['footer'][line.code] += line.amount
                                                if line.salary_rule_id.show_in_report:
                                                    checklist[line.code] = False
                                                else:
                                                    checklist[line.code] = True
                                            icol += 1
                                            if line.code == 'NET':
                                                if line.amount >0:
                                                    count_emp +=1
                                            icol += 1
                                        salary_list['worksheet'].write(row, salary_list['data']['ndcode']['icol'],
                                                                       slip.employee_id.contract_id.ndsh_type.code or '-' if slip.employee_id.contract_id else '-', content_center)
                                        salary_list['worksheet'].write(row,
                                                                       salary_list['data']['bank_name']['icol'],
                                                                       slip.employee_id.contract_id.bank_account_id.bank_id.name or '-' if slip.employee_id.contract_id and slip.employee_id.contract_id.bank_account_id else '-', content_center)
                                        salary_list['worksheet'].write(row,
                                                                       salary_list['data']['bank_account']['icol'],
                                                                       slip.employee_id.contract_id.bank_account_id.acc_number or '-' if slip.employee_id.contract_id and slip.employee_id.contract_id.bank_account_id else '-', content_center)
                                        salary_list['worksheet'].set_row(row, None, None, {'level': 1})
                                        row += 1
                                        number += 1
                    #Хэлтэс нь байхгүй үед дараах талбаруудаар дүүргэгдэнэ.
                    salary_list['worksheet'].write(row, 0, '',footer)
                    salary_list['worksheet'].write(row, 1, '',footer)
                    salary_list['worksheet'].write(row, 2, u'Цалин олгох: ' + str(count_emp),footer)
                    salary_list['worksheet'].write(row, 3, '',footer)
                    salary_list['worksheet'].write(row, 4, u'Нийт дүн',footer)
                    i = 5
                    for key in salary_columns:
                        salary_list['worksheet'].write(row, i, salary_list['footer'][key],footer)
                        if checklist:
                            if checklist.has_key(key):
                                if checklist[key]:
                                    salary_list['worksheet'].set_column(columns[str(i+1)]+':'+columns[str(i+1)], None, None, {'hidden': True})
                                else:
                                    if salary_list['footer'][key] == 0: 
                                        salary_list['worksheet'].set_column(columns[str(i+1)]+':'+columns[str(i+1)], None, None, {'hidden': True})
                        i += 1
                    salary_list['worksheet'].write(row, i, '', footer)
                    salary_list['worksheet'].write(row, i+1, '', footer)
                    salary_list['worksheet'].write(row, i+2, '', footer)
                    if template.payslip_run.company_id.second_sign_cart: 
                        if self.get_emp_name_from_sign('general_acc', template.payslip_run.company_id)[1]:
                            if self.get_emp_name_from_sign('general_acc', template.payslip_run.company_id)[0]:
                                salary_list['worksheet'].write(row+2, 2, u'Ерөнхий нягтлан бодогч: ........................ %s.%s'% (self.get_first_letter_of_sign('general_acc', template.payslip_run.company_id)[1], self.get_emp_name_from_sign('general_acc', template.payslip_run.company_id)[0]),h3)
                    if template.payslip_run.create_employee:
                        if template.payslip_run.create_employee.last_name:
                            if template.payslip_run.create_employee.name:
                                salary_list['worksheet'].write(row+4, 2, u'Цалин бодсон: ........................ %s.%s'% ( template.payslip_run.create_employee.last_name[:1], template.payslip_run.create_employee.name),h3)
                else: #Нэг хуудасанд хэвлэгдэхийг сонгоогүй үед
                    salary_list  = {}
                    structure_ids = self.env['hr.payroll.structure'].search([('salary_type','=',template.salary_type)])
                    struct_count = 0
                    for struct in structure_ids:
                        struct_count += 1
                        col_num = 0
                        irow = 0
                        icol = 1
                        name = u'Цалингийн хүснэгт' + str(struct_count)
                        struct_ids = self.env['hr.payslip'].search([('struct_id','=',struct.id),('salary_type','=',template.salary_type),('period_id','=',template.period_id.id)])
                        if struct_ids: #Дээрх нөхцөл биелэсэн бол
                            worksheets = workbook.add_worksheet(name)
                            worksheets.set_landscape()
                            worksheets.set_paper(9)  # A4
                            worksheets.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
                            worksheets.fit_to_pages(1, 0)
                            salary_list[struct.id] = {'max_count' :  len(struct.rule_ids), 'data' : {}, 'worksheet' : worksheets, 'footer' : {}}
                            irow += 2
                            salary_list[struct.id]['worksheet'].write(irow, icol, u"Огноо: %s" % (template.period_id.name), h2)
                            irow += 1
                            row=irow+1
                            icol = 0
                            salary_list[struct.id]['data']['number'] = {'name': u'№', 'code':'DD','col':4,'icol':0}
                            salary_list[struct.id]['data']['employee_code'] = {'name': u'Код', 'code':'CODE','col':6,'icol':1}
                            salary_list[struct.id]['data']['employee_last'] = {'name': u'Овог', 'code': 'LAST_NAME','col': 12, 'icol': 2}
                            salary_list[struct.id]['data']['employee_first'] = {'name': u'Нэр', 'code':'FIRST_NAME','col':12,'icol':3}
                            salary_list[struct.id]['data']['work_position'] = {'name': u'Албан тушаал', 'code':'WORK_POSITION','col':22,'icol':4}
                            salary_list[struct.id]['worksheet'].set_row(irow, 40)
                            salary_list[struct.id]['worksheet'].write(irow, icol, salary_list[struct.id]['data']['number']['name'], theader)
                            salary_list[struct.id]['worksheet'].set_column(icol,icol, salary_list[struct.id]['data']['number']['col'])
                            salary_list[struct.id]['worksheet'].write(irow, icol+1,  salary_list[struct.id]['data']['employee_code']['name'], theader)
                            salary_list[struct.id]['worksheet'].set_column(icol+1,icol+1, salary_list[struct.id]['data']['employee_code']['col'])
                            salary_list[struct.id]['worksheet'].write(irow, icol+2,  salary_list[struct.id]['data']['employee_last']['name'], theader)
                            salary_list[struct.id]['worksheet'].set_column(icol+2,icol+2, salary_list[struct.id]['data']['employee_last']['col'])
                            salary_list[struct.id]['worksheet'].write(irow, icol+3,  salary_list[struct.id]['data']['employee_first']['name'], theader)
                            salary_list[struct.id]['worksheet'].set_column(icol+3,icol+3, salary_list[struct.id]['data']['employee_first']['col'])
                            salary_list[struct.id]['worksheet'].write(irow, icol+4,  salary_list[struct.id]['data']['work_position']['name'], theader)
                            salary_list[struct.id]['worksheet'].set_column(icol+4,icol+4, salary_list[struct.id]['data']['work_position']['col'])
                            icol = 5
                            salary_columns = []
                            for slip_rule in struct.rule_ids:
                                salary_list[struct.id]['data'][slip_rule.code] = {'name': slip_rule.name, 'code':slip_rule.code,'col':slip_rule.column_width,'icol':icol}
                                salary_list[struct.id]['worksheet'].write(irow, icol, salary_list[struct.id]['data'][slip_rule.code]['name'], theader)
                                salary_list[struct.id]['worksheet'].set_column(icol,icol, salary_list[struct.id]['data'][slip_rule.code]['col'])
                                salary_list[struct.id]['footer'][slip_rule.code] = 0;

                                salary_columns.append(slip_rule.code)
                                icol += 1
                            salary_list[struct.id]['data']['ndcode'] = {'name': u'НД-н код', 'code': 'NDCODE', 'col': 20,
                                                             'icol': icol}
                            salary_list[struct.id]['worksheet'].write(irow, icol, salary_list[struct.id]['data']['ndcode']['name'], theader)
                            salary_list[struct.id]['worksheet'].set_column(icol, icol, salary_list[struct.id]['data']['ndcode']['col'])
                            icol += 1
                            salary_list[struct.id]['data']['bank_name'] = {'name': u'БАНКНЫ НЭР', 'code': 'BANK_NAME', 'col': 20,
                                                                'icol': icol}
                            salary_list[struct.id]['worksheet'].write(irow, icol, salary_list[struct.id]['data']['bank_name']['name'],
                                                           theader)
                            salary_list[struct.id]['worksheet'].set_column(icol, icol, salary_list[struct.id]['data']['bank_name']['col'])
                            icol += 1
                            salary_list[struct.id]['data']['bank_account'] = {'name': u'БАНКНЫ ДАНС', 'code': 'BANK_ACCOUNT',
                                                                   'col': 20, 'icol': icol}
                            salary_list[struct.id]['worksheet'].write(irow, icol, salary_list[struct.id]['data']['bank_account']['name'],
                                                           theader)
                            salary_list[struct.id]['worksheet'].set_column(icol, icol, salary_list[struct.id]['data']['bank_account']['col'])
                            icol += 1
                            checklist = {}
                            count_emp = 0
                            emp_ids =[]
                            department_ids = []
                            number = 1
                            if template.payslip_run: #Цалингийн хуудсын багц сонгосон үед
                                if template.group_by_department: #Бүлэглэхийг сонгосон.
                                    salary_list[struct.id]['worksheet'].write(0, 9, u"%s %s" % (template.payslip_run.company_id.name.upper(), template.payslip_run.name.upper()), h1)
                                    salary_list[struct.id]['worksheet'].set_row(1, 40)
                                    salary_list[struct.id]['worksheet'].write(1, 2, u'Баталсан: ........................ %s.%s' % (self.get_emp_name_from_sign('first', template.payslip_run.company_id)[1], self.get_emp_name_from_sign('first', template.payslip_run.company_id)[0]),h1)
                                    self._cr.execute('''select hd.id
                    from hr_payslip hp, hr_employee he, hr_department hd 
                    where hp.struct_id=%s and hp.payslip_run_id=%s and hp.employee_id=he.id and he.department_id=hd.id group by hd.id
                    order by hd.id asc''' % (struct.id,template.payslip_run.id))
                                    departments = self._cr.fetchall()
                                    for department in departments:
                                        department_ids.append(department[0])
                                    deps = []
                                    if department_ids:
                                        depids = str(department_ids)
                                        depids = depids.replace("[", "(")
                                        depids = depids.replace("]", ")")
                                        self._cr.execute('''SELECT id FROM hr_department WHERE id IN '''+ depids + ''' ORDER BY sequence ASC''')
                                        deps = self._cr.fetchall()
                                    department_ids = []
                                    for department in deps:
                                        department_ids.append(department[0])
                                    employee_ids = self.env['hr.department'].search([('id','in',department_ids)])
                                    for dep in self.env['hr.department'].browse(department_ids):
                                        emp_ids =[]
                                        self._cr.execute('''select hp.employee_id,hd.id 
                    from hr_payslip hp, hr_employee he, hr_department hd 
                    where hp.struct_id=%s and hp.payslip_run_id=%s and hp.employee_id=he.id and he.department_id=hd.id and hd.id=%s
                    order by hd.id asc''' % (struct.id,template.payslip_run.id, dep.id))
                                        employees = self._cr.fetchall()
                                        for employee in employees:
                                            emp_ids.append(employee[0])
                                        employees = []
                                        if emp_ids:
                                            empids = str(emp_ids)
                                            empids = empids.replace("[", "(")
                                            empids = empids.replace("]", ")")
                                            self._cr.execute('''SELECT id FROM hr_employee WHERE id IN '''+ empids +''' ORDER BY name_related ASC,last_name ASC, ssnid ASC''')
                                            employees = self._cr.fetchall()
                                        emp_ids = []
                                        for employee in employees:
                                            emp_ids.append(employee[0])                                       
                                        hr_emp_ids = []
                                        hr_emp_ids = str(emp_ids)
                                        hr_emp_ids = hr_emp_ids.replace("[", "(")
                                        hr_emp_ids = hr_emp_ids.replace("]", ")")
                                        self._cr.execute('''SELECT id FROM hr_employee WHERE id IN '''+ hr_emp_ids +''' ORDER BY name_related ASC,last_name ASC, ssnid ASC''')
                                        emps = self._cr.fetchall()
                                        employee_ids = []
                                        
                                        for employee in emps: 
                                            employee_ids.append(employee[0])
                                        
                                        dep_header = {}
                                        for key in salary_columns:
                                            dep_header[key] = 0
                                        deprow = row
                                        row+=1
                                        for emp in self.env['hr.employee'].browse(employee_ids):
                                            slip_ids = self.env['hr.payslip'].search([('period_id','=', template.period_id.id),('salary_type','=', template.salary_type),('employee_id','=', emp.id)])
                                            icol = 5
                                            for slip in slip_ids:
                                                salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['number']['icol'], number,content_number)
                                                salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['employee_code']['icol'], slip.employee_id.ssnid , content_left)
                                                salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['employee_last']['icol'], slip.employee_id.last_name or '',content_left)
                                                salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['employee_first']['icol'], slip.employee_id.name , content_left)
                                                salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['work_position']['icol'], slip.employee_id.job_id.name or '', content_left)
                                                i=0
                                                for i in range(0, salary_list[struct.id]['max_count']):
                                                    salary_list[struct.id]['worksheet'].write(row, i+5, '',content_right)
                                                    i+=1
                                                for line in slip.line_ids:
                                                    if line.code in salary_list[struct.id]['data'].keys():
                                                        salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data'][line.code]['icol'], line.amount,content_right)
                                                        col_num = salary_list[struct.id]['data'][line.code]['icol']
                                                        salary_list[struct.id]['footer'][line.code] += line.amount
                                                        dep_header[line.code] += line.amount
                                                        if line.salary_rule_id.show_in_report:
                                                            checklist[line.code] = False
                                                        else:
                                                            checklist[line.code] = True
                                                    icol += 1
                                                    if line.code == 'NET':
                                                        if line.amount >0:
                                                            count_emp +=1
                                                    icol += 1
                                                salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id][
                                                    'data']['ndcode']['icol'],slip.employee_id.contract_id.ndsh_type.code or '-' if slip.employee_id.contract_id else '-',
                                                                                          content_center)
                                                salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id][
                                                    'data']['bank_name']['icol'],slip.employee_id.contract_id.bank_account_id.bank_id.name or '-' if slip.employee_id.contract_id and slip.employee_id.contract_id.bank_account_id else '-',
                                                                                          content_center)
                                                salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id][
                                                    'data']['bank_account']['icol'],slip.employee_id.contract_id.bank_account_id.acc_number or '-' if slip.employee_id.contract_id and slip.employee_id.contract_id.bank_account_id else '-',
                                                                                          content_center)
                                                salary_list[struct.id]['worksheet'].set_row(row, None, None, {'level': 1})
                                                row+=1
                                                number += 1
                                        salary_list[struct.id]['worksheet'].merge_range('A%s:E%s' % (str(deprow+1),str(deprow+1)), u'%s' % dep.name, subfooter)
                                        i = 5
                                        for key in salary_columns:
                                            salary_list[struct.id]['worksheet'].write(deprow, i, dep_header[key],subfooter)
                                            i += 1
                                        salary_list[struct.id]['worksheet'].write(deprow, i, '', subfooter)
                                        i += 1
                                        salary_list[struct.id]['worksheet'].write(deprow, i, '', subfooter)
                                        i += 1
                                        salary_list[struct.id]['worksheet'].write(deprow, i, '', subfooter)
                                    temp_ids = []
                                    emp_ids = []
                                    self._cr.execute('''select hd.id
                    from hr_payslip hp, hr_employee he, hr_department hd 
                    where hp.struct_id=%s and hp.payslip_run_id=%s and hp.employee_id=he.id and he.department_id is null''' % (struct.id,template.payslip_run.id))
                                    employees = self._cr.fetchall()
                                    for employee in employees:
                                        emp_ids.append(employee[0])
                                    dep_header = {}
                                    for key in salary_columns:
                                        dep_header[key] = 0
                                    deprow = row
                                    row+=1
                                        
                                    slip_ids = self.env['hr.payslip'].search([('period_id','=', template.period_id.id),('salary_type','=', template.salary_type),('employee_id','in', emp_ids),('payslip_run_id','=', template.payslip_run.id)],order = 'name')
                                    icol = 5
                                    for slip in slip_ids:
                                        salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['number']['icol'], number,content_number)
                                        salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['employee_code']['icol'], slip.employee_id.ssnid , content_left)
                                        salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['employee_last']['icol'],slip.employee_id.last_name or '', content_left)
                                        salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['employee_first']['icol'], slip.employee_id.name , content_left)
                                        salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['work_position']['icol'], slip.employee_id.job_id.name or '' , content_left)
                                        i=0
                                        for i in range(0, salary_list[struct.id]['max_count']):
                                            salary_list[struct.id]['worksheet'].write(row, i+5, '',content_right)
                                            i+=1
                                        for line in slip.line_ids:
                                            if line.code in salary_list[struct.id]['data'].keys():
                                                salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data'][line.code]['icol'], line.amount,content_right)
                                                col_num = salary_list[struct.id]['data'][line.code]['icol']
                                                salary_list[struct.id]['footer'][line.code] += line.amount
                                                dep_header[line.code] += line.amount
                                                if line.salary_rule_id.show_in_report:
                                                    checklist[line.code] = False
                                                else:
                                                    checklist[line.code] = True
                                            icol += 1
                                            if line.code == 'NET':
                                                if line.amount >0:
                                                    count_emp +=1
                                            icol += 1
                                        salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id][
                                            'data']['ndcode']['icol'], slip.employee_id.contract_id.ndsh_type.code or '-' if slip.employee_id.contract_id else '-',
                                                                                  content_center)
                                        salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id][
                                            'data']['bank_name']['icol'],slip.employee_id.contract_id.bank_account_id.bank_id.name or '-' if slip.employee_id.contract_id and slip.employee_id.contract_id.bank_account_id else '-',
                                                                                  content_center)
                                        salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id][
                                            'data']['bank_account']['icol'],slip.employee_id.contract_id.bank_account_id.acc_number or '-' if slip.employee_id.contract_id and slip.employee_id.contract_id.bank_account_id else '-',
                                                                                  content_center)
                                        salary_list[struct.id]['worksheet'].set_row(row, None, None, {'level': 1})
                                        row+=1
                                        number += 1
                                    row -= 1
                                    if slip_ids:
                                        row +=1
                                        salary_list[struct.id]['worksheet'].merge_range('A%s:E%s' % (str(deprow+1),str(deprow+1)), u'%s' % _('Not defined'), subfooter)
                                        i = 5
                                        for key in salary_columns:
                                            salary_list[struct.id]['worksheet'].write(deprow, i, dep_header[key],subfooter)
                                            i += 1
                                        salary_list[struct.id]['worksheet'].write(deprow, i, '', subfooter)
                                        i += 1
                                        salary_list[struct.id]['worksheet'].write(deprow, i, '', subfooter)
                                        i += 1
                                        salary_list[struct.id]['worksheet'].write(deprow, i, '', subfooter)

                                else: #Бүлэглэхийг сонгоогүй
                                    salary_list[struct.id]['worksheet'].write(0, 9, u"%s %s" % (template.payslip_run.company_id.name.upper(), template.payslip_run.name.upper()), h1)
                                    salary_list[struct.id]['worksheet'].set_row(1, 40)
                                    salary_list[struct.id]['worksheet'].write(1, 2, u'Баталсан: ........................ %s.%s' % (self.get_emp_name_from_sign('first', template.payslip_run.company_id)[1], self.get_emp_name_from_sign('first', template.payslip_run.company_id)[0]),h1)
                                    cr = self._cr
                                    self._cr.execute('''select employee_id from hr_payslip where struct_id=%s and payslip_run_id=%s''' % (struct.id,template.payslip_run.id))
                                    employees = self._cr.fetchall()
                                    for employee in employees:
                                        emp_ids.append(employee[0])
                                    slip_ids = self.env['hr.payslip'].search([('period_id','=', template.period_id.id),('salary_type','=', template.salary_type),('employee_id','in', tuple(emp_ids))])
                                    icol = 5
                                    for slip in slip_ids:
                                        salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['number']['icol'], number,content_number)
                                        salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['employee_code']['icol'], slip.employee_id.ssnid , content_left)
                                        salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['employee_last']['icol'], slip.employee_id.last_name or '', content_left)
                                        salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['employee_first']['icol'], slip.employee_id.name , content_left)
                                        salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['work_position']['icol'], slip.employee_id.job_id.name or '', content_left)
                                        i=0
                                        for i in range(0, salary_list[struct.id]['max_count']):
                                            salary_list[struct.id]['worksheet'].write(row, i+5, '',content_right)
                                            i+=1
                                        for line in slip.line_ids:
                                            if line.code in salary_list[struct.id]['data'].keys():
                                                salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data'][line.code]['icol'], line.amount,content_right)
                                                col_num = salary_list[struct.id]['data'][line.code]['icol']
                                                salary_list[struct.id]['footer'][line.code] += line.amount
                                                if line.salary_rule_id.show_in_report:
                                                    checklist[line.code] = False
                                                else:
                                                    checklist[line.code] = True
                                            icol += 1
                                            if line.code == 'NET':
                                                if line.amount >0:
                                                    count_emp +=1
                                            icol += 1
                                        salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['ndcode']['icol'], slip.employee_id.contract_id.ndsh_type.code or '-' if slip.employee_id.contract_id else '-',
                                                                                  content_center)
                                        salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['bank_name']['icol'],slip.employee_id.contract_id.bank_account_id.bank_id.name or '-' if slip.employee_id.contract_id and slip.employee_id.contract_id.bank_account_id else '-',
                                                                                  content_center)
                                        salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['bank_account']['icol'],slip.employee_id.contract_id.bank_account_id.acc_number or '-' if slip.employee_id.contract_id and slip.employee_id.contract_id.bank_account_id else '-',
                                                                                  content_center)
                                        salary_list[struct.id]['worksheet'].set_row(row, None, None, {'level': 1})
                                        row+=1
                                        number += 1
                                salary_list[struct.id]['worksheet'].write(row, 0, '',footer)
                                salary_list[struct.id]['worksheet'].write(row, 1, '',footer)
                                salary_list[struct.id]['worksheet'].write(row, 2, u'Цалин олгох: ' + str(count_emp),footer)
                                salary_list[struct.id]['worksheet'].write(row, 3, '',footer)
                                salary_list[struct.id]['worksheet'].write(row, 4, u'Нийт дүн',footer)

                                i = 5
                                for key in salary_columns:
                                    salary_list[struct.id]['worksheet'].write(row, i, salary_list[struct.id]['footer'][key],footer)
                                    if checklist:
                                        if checklist.has_key(key):
                                            if checklist[key]:
                                                salary_list[struct.id]['worksheet'].set_column(columns[str(i+1)]+':'+columns[str(i+1)], None, None, {'hidden': True})
                                            else:
                                                if salary_list[struct.id]['footer'][key] == 0: 
                                                    salary_list[struct.id]['worksheet'].set_column(columns[str(i+1)]+':'+columns[str(i+1)], None, None, {'hidden': True})
                                    i += 1
                                salary_list[struct.id]['worksheet'].write(row, i, '', footer)
                                salary_list[struct.id]['worksheet'].write(row, i + 1, '', footer)
                                salary_list[struct.id]['worksheet'].write(row, i + 2, '', footer)
                                if template.payslip_run.company_id.second_sign_cart: 
                                    if self.get_emp_name_from_sign('general_acc', template.payslip_run.company_id)[1]:
                                        if self.get_emp_name_from_sign('general_acc', template.payslip_run.company_id)[0]:
                                            salary_list[struct.id]['worksheet'].write(row+2, 2, u'Ерөнхий нягтлан бодогч ........................ %s.%s'% ( self.get_first_letter_of_sign('general_acc', template.payslip_run.company_id)[1], self.get_emp_name_from_sign('general_acc', template.payslip_run.company_id)[0]),h3)
                                if template.payslip_run.create_employee:
                                    if template.payslip_run.create_employee.last_name:
                                        if template.payslip_run.create_employee.name:
                                            salary_list[struct.id]['worksheet'].write(row+4, 2, u'Цалин бодсон: ........................ %s.%s'% ( template.payslip_run.create_employee.last_name[:1], template.payslip_run.create_employee.name),h3)
                            else: #Цалингийн хуудсын багц сонгоогүй үед
                                company = False
                                for user in self.env['res.users'].browse(self.env.uid):
                                    company = user.company_id
                                if company :
                                    number = 1
                                    if template.salary_type == 'advance_salary':
                                        name = u'Урьдчилгаа цалин'
                                    if template.salary_type == 'last_salary':
                                        name = u'Сүүл цалин'
                                    salary_list[struct.id]['worksheet'].write(0, 2, u"%s %s %s" % (company.name.upper(),template.period_id.name.upper(),name), h1)
                                    salary_list[struct.id]['worksheet'].set_row(1, 40)
                                    salary_list[struct.id]['worksheet'].write(1, 2, u'Баталсан: ........................ %s.%s' % (self.get_first_letter_of_sign('first', company)[1], self.get_emp_name_from_sign('first', company)[0]),h1)
                                    cr = self._cr
                                    if template.group_by_department:
                                        self._cr.execute('''select hd.id
                    from hr_payslip hp, hr_employee he, hr_department hd 
                    where hp.struct_id=%s and hp.salary_type='%s' and hp.employee_id=he.id and he.department_id=hd.id and hp.company_id=%s group by hd.id
                    order by hd.id asc''' % (struct.id,template.salary_type, company.id))
                                        departments = self._cr.fetchall()
                                        for department in departments:
                                            department_ids.append(department[0])
                                        deps = []
                                        if department_ids:
                                            depids = str(department_ids)
                                            depids = depids.replace("[", "(")
                                            depids = depids.replace("]", ")")
                                            cr = self._cr
                                            self._cr.execute('''SELECT id FROM hr_department WHERE id IN '''+ depids + ''' ORDER BY sequence ASC''')
                                            deps = self._cr.fetchall()
                                        department_ids = []
                                        for department in deps:
                                            department_ids.append(department[0])
                                        employee_ids = self.env['hr.department'].search([('id','in',department_ids)], order='sequence')
                                        for dep in employee_ids:
                                            emp_ids =[]
                                            cr = self._cr
                                            self._cr.execute('''select hp.employee_id,hd.id 
                    from hr_payslip hp, hr_employee he, hr_department hd 
                    where hp.struct_id=%s and hp.salary_type='%s' and hp.employee_id=he.id and he.department_id=hd.id and hd.id=%s
                    order by hd.id asc''' % (struct.id,template.salary_type, dep.id))
                                            employees = self._cr.fetchall()
                                            for employee in employees:
                                                emp_ids.append(employee[0])
                                            employees = []
                                            if emp_ids:
                                                empids = str(emp_ids)
                                                empids = empids.replace("[", "(")
                                                empids = empids.replace("]", ")")
                                                self._cr.execute('''SELECT id FROM hr_employee WHERE id IN '''+ empids +''' ORDER BY name_related ASC,last_name ASC, ssnid ASC''')
                                                employees = self._cr.fetchall()
                                            emp_ids = []
                                            for employee in employees:
                                                emp_ids.append(employee[0])
                                            hr_emp_ids = []
                                            hr_emp_ids = str(emp_ids)
                                            hr_emp_ids = hr_emp_ids.replace("[", "(")
                                            hr_emp_ids = hr_emp_ids.replace("]", ")")
                                            self._cr.execute('''SELECT id FROM hr_employee WHERE id IN '''+ hr_emp_ids +''' ORDER BY name_related ASC,last_name ASC, ssnid ASC''')
                                            emps = self._cr.fetchall()
                                            employee_ids = []
                                            
                                            for employee in emps: 
                                                employee_ids.append(employee[0])
                                            
                                            dep_header = {}
                                            for key in salary_columns:
                                                dep_header[key] = 0
                                            deprow = row
                                            row+=1
                                            for emp in self.env['hr.employee'].browse(employee_ids):

                                                slip_ids = self.env['hr.payslip'].search([('period_id','=', template.period_id.id),('salary_type','=', template.salary_type),('employee_id','=', emp.id)])
                                                icol = 5
                                                for slip in slip_ids:
                                                    salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['number']['icol'], number,content_number)
                                                    salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['employee_code']['icol'], slip.employee_id.ssnid , content_left)
                                                    salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['employee_last']['icol'],slip.employee_id.last_name or '',content_left)
                                                    salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['employee_first']['icol'], slip.employee_id.name , content_left)
                                                    salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['work_position']['icol'], slip.employee_id.job_id.name or '', content_left)
                                                    i=0
                                                    for i in range(0, salary_list[struct.id]['max_count']):
                                                        salary_list[struct.id]['worksheet'].write(row, i+5, '',content_right)
                                                        i+=1
                                                    for line in slip.line_ids:
                                                        if line.code in salary_list[struct.id]['data'].keys():
                                                            salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data'][line.code]['icol'], line.amount,content_right)
                                                            col_num = salary_list[struct.id]['data'][line.code]['icol']
                                                            salary_list[struct.id]['footer'][line.code] += line.amount
                                                            dep_header[line.code] += line.amount
                                                            if line.salary_rule_id.show_in_report:
                                                                checklist[line.code] = False
                                                            else:
                                                                checklist[line.code] = True
                                                        icol += 1
                                                        if line.code == 'NET':
                                                            if line.amount >0:
                                                                count_emp +=1
                                                        icol += 1
                                                    salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['ndcode']['icol'],
                                                                                              slip.employee_id.contract_id.ndsh_type.code or '-' if slip.employee_id.contract_id else '-',
                                                                                              content_center)
                                                    salary_list[struct.id]['worksheet'].write(row,salary_list[struct.id]['data']['bank_name']['icol'],
                                                                                              slip.employee_id.contract_id.bank_account_id.bank_id.name or '-' if slip.employee_id.contract_id and slip.employee_id.contract_id.bank_account_id else '-',
                                                                                              content_center)
                                                    salary_list[struct.id]['worksheet'].write(row,salary_list[struct.id]['data']['bank_account']['icol'],
                                                                                              slip.employee_id.contract_id.bank_account_id.acc_number or '-' if slip.employee_id.contract_id and slip.employee_id.contract_id.bank_account_id else '-',
                                                                                              content_center)
                                                    salary_list[struct.id]['worksheet'].set_row(row, None, None, {'level': 1})
                                                    row+=1
                                                    number += 1
                                            salary_list[struct.id]['worksheet'].merge_range('A%s:E%s' % (str(deprow+1),str(deprow+1)), u'%s' % dep.name, subfooter)
                                            i = 5
                                            for key in salary_columns:
                                                salary_list[struct.id]['worksheet'].write(deprow, i, dep_header[key],subfooter)
                                                i += 1
                                            salary_list[struct.id]['worksheet'].write(deprow, i, '', subfooter)
                                            i += 1
                                            salary_list[struct.id]['worksheet'].write(deprow, i, '', subfooter)
                                            i += 1
                                            salary_list[struct.id]['worksheet'].write(deprow, i, '', subfooter)
                                    else:
                                        self._cr.execute('''select employee_id from hr_payslip where struct_id=%s and salary_type='%s' ''' % (struct.id, template.salary_type))
                                        employees = self._cr.fetchall()
                                        for employee in employees:
                                            emp_ids.append(employee[0])
                                        employees = []
                                        if emp_ids:
                                            empids = str(emp_ids)
                                            empids = empids.replace("[", "(")
                                            empids = empids.replace("]", ")")
                                            self._cr.execute('''SELECT id FROM hr_employee WHERE id IN '''+ empids +''' ORDER BY name_related ASC,last_name ASC, ssnid ASC''')
                                            employees = self._cr.fetchall()
                                        emp_ids = []
                                        for employee in employees:
                                            emp_ids.append(employee[0])
                                        hr_emp_ids = []
                                        hr_emp_ids = str(emp_ids)
                                        hr_emp_ids = hr_emp_ids.replace("[", "(")
                                        hr_emp_ids = hr_emp_ids.replace("]", ")")
                                        self._cr.execute('''SELECT id FROM hr_employee WHERE id IN '''+ hr_emp_ids +''' ORDER BY name_related ASC,last_name ASC, ssnid ASC''')
                                        emps = self._cr.fetchall()
                                        employee_ids = []
                                        
                                        for employee in emps: 
                                            employee_ids.append(employee[0])
                                        
                                        for emp in self.env['hr.employee'].browse(employee_ids):
                                            slip_ids = self.env['hr.payslip'].search([('period_id','=', template.period_id.id),('salary_type','=', template.salary_type),('employee_id','=', emp.id)])
                                            icol = 5
                                            for slip in slip_ids:
                                                salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['number']['icol'], number ,content_number)
                                                salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['employee_code']['icol'], slip.employee_id.ssnid , content_left)
                                                salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['employee_last']['icol'], slip.employee_id.last_name or '',content_left)
                                                salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['employee_first']['icol'], slip.employee_id.name , content_left)
                                                salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data']['work_position']['icol'], slip.employee_id.job_id.name , content_left)
                                                i=0
                                                for i in range(0, salary_list[struct.id]['max_count']):
                                                    salary_list[struct.id]['worksheet'].write(row, i+5, '',content_right)
                                                    i+=1
                                                for line in slip.line_ids:
                                                    if line.code in salary_list[struct.id]['data'].keys():
                                                        salary_list[struct.id]['worksheet'].write(row, salary_list[struct.id]['data'][line.code]['icol'], line.amount,content_right)
                                                        col_num = salary_list[struct.id]['data'][line.code]['icol']
                                                        salary_list[struct.id]['footer'][line.code] += line.amount
                                                        if line.salary_rule_id.show_in_report:
                                                            checklist[line.code] = False
                                                        else:
                                                            checklist[line.code] = True
                                                    icol += 1
                                                    if line.code == 'NET':
                                                        if line.amount >0:
                                                            count_emp +=1
                                                    icol += 1
                                                salary_list[struct.id]['worksheet'].write(row,salary_list[struct.id]['data']['ndcode']['icol'],
                                                                                          slip.employee_id.contract_id.ndsh_type.code or '-' if slip.employee_id.contract_id else '-',
                                                                               content_center)
                                                salary_list[struct.id]['worksheet'].write(row,salary_list[struct.id]['data']['bank_name']['icol'],
                                                                               slip.employee_id.contract_id.bank_account_id.bank_id.name or '-' if slip.employee_id.contract_id and slip.employee_id.contract_id.bank_account_id else '-',
                                                                               content_center)
                                                salary_list[struct.id]['worksheet'].write(row,salary_list[struct.id]['data']['bank_account'][
                                                                                   'icol'],slip.employee_id.contract_id.bank_account_id.acc_number or '-' if slip.employee_id.contract_id and slip.employee_id.contract_id.bank_account_id else '-',
                                                                               content_center)
                                                salary_list[struct.id]['worksheet'].set_row(row, None, None, {'level': 1})
                                                row+=1
                                                number += 1
                                    salary_list[struct.id]['worksheet'].write(row, 0, '',footer)
                                    salary_list[struct.id]['worksheet'].write(row, 1, '',footer)
                                    salary_list[struct.id]['worksheet'].write(row, 2, u'Цалин олгох: ' + str(count_emp),footer)
                                    salary_list[struct.id]['worksheet'].write(row, 3, '',footer)
                                    salary_list[struct.id]['worksheet'].write(row, 4, u'Нийт дүн',footer)

                                    i = 5
                                    for key in salary_columns:
                                        salary_list[struct.id]['worksheet'].write(row, i, salary_list[struct.id]['footer'][key],footer)
                                        if checklist:
                                            if checklist.has_key(key):
                                                if checklist[key]:
                                                    salary_list[struct.id]['worksheet'].set_column(columns[str(i+1)]+':'+columns[str(i+1)], None, None, {'hidden': True})
                                                else:
                                                    if salary_list[struct.id]['footer'][key] == 0: 
                                                        salary_list[struct.id]['worksheet'].set_column(columns[str(i+1)]+':'+columns[str(i+1)], None, None, {'hidden': True})
                                        i += 1
                                    salary_list[struct.id]['worksheet'].write(row, i, '', footer)
                                    salary_list[struct.id]['worksheet'].write(row, i + 1, '', footer)
                                    salary_list[struct.id]['worksheet'].write(row, i + 2, '', footer)
                                    if company.second_sign_cart:
                                        if company.second_sign_cart.last_name:
                                            if self.get_emp_name_from_sign('general_acc', company)[1]:
                                                salary_list[struct.id]['worksheet'].write(row+2, 2, u'Ерөнхий нягтлан бодогч: ........................ %s.%s'% ( self.get_first_letter_of_sign('general_acc', company)[1], self.get_emp_name_from_sign('general_acc', company)[0]),h1)                                           
            if template.salary_type == 'vacation': #Ээлжмйн амралт сонгосон үед
                worksheet_general = workbook.add_worksheet(u'Цалингийн Хүснэгт')
                worksheet_general.set_landscape()
                worksheet_general.set_paper(9)  # A4
                worksheet_general.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
                worksheet_general.fit_to_pages(1, 0)
                icol = 1
                irow = 4
                if not template.payslip_run: #Цалингийн хуудасын багц сонгоогүй
                    for user in self.env['res.users'].browse(self.env.uid):
                        company = user.company_id
                    worksheet_general.write(irow - 4, icol-1, u'Компани: %s' %company.name, format_left)
                    worksheet_general.write(irow - 2, icol + 3, u'Амралтын мөнгө олгох хүснэгт', format_name)

                    if template.employee_ids:
                        for emp in template.employee_ids:
                            slip_ids = self.env['hr.payslip'].search([('period_id','=', template.period_id.id),('salary_type','=', template.salary_type),('employee_id','=', emp.id)])
                            for slip in slip_ids:
                                worksheet_general.write(irow, icol-1, u'Овог, нэр: %s.%s' %(slip.employee_id.last_name or '', slip.employee_id.name),format_left)
                                irow +=1
                                worksheet_general.write(irow, icol - 1, u'Албан тушаал: %s' % slip.employee_id.job_id.name or '', format_left)
                                irow +=1
                                worksheet_general.set_column(icol - 1, icol - 1, 3)
                                worksheet_general.write(irow, icol - 1, u'Регистр: %s' % slip.employee_id.ssnid,format_left)
                                worksheet_general.set_column(icol, icol, 12)
                                worksheet_general.set_column(icol + 1, icol + 1, 15)
                                irow += 1
                                worksheet_general.set_row(irow, 30)
                                worksheet_general.write(irow, icol-1, u'№',theader)
                                worksheet_general.write(irow, icol, u'Огноо',theader)
                                worksheet_general.write(irow, icol+1, u'Цалин',theader)
                                worksheet_general.write(irow, icol+2, u'Ажилласан хоног',theader)
                                worksheet_general.set_column(icol+2,icol+2, 10)
                                worksheet_general.write(irow, icol+3, u'Нэг өдрийн цалин',theader)
                                worksheet_general.set_column(icol+3,icol+3, 10)
                                worksheet_general.write(irow, icol+4, u'Амрах хоног',theader)
                                worksheet_general.set_column(icol+4,icol+4, 6)
                                worksheet_general.write(irow, icol+5, u'Амралтын цалин',theader)
                                worksheet_general.set_column(icol+5,icol+5, 12)
                                worksheet_general.write(irow, icol+6, u'НДШ, ХХОАТ',theader)
                                worksheet_general.set_column(icol+6,icol+6, 10)
                                worksheet_general.write(irow, icol+7, u'Гарт олгох',theader)
                                worksheet_general.set_column(icol+7,icol+7, 12)
                                irow += 1
                                i = 1
                                for vac_line in slip.vacation_lines:
                                    worksheet_general.write(irow, icol-1,str(i) ,content_right)
                                    worksheet_general.write(irow, icol, u"%s-%s" % (vac_line.year.code,vac_line.month),content_center)
                                    worksheet_general.write(irow, icol+1, vac_line.amount ,content_right)
                                    worksheet_general.write(irow, icol+2, vac_line.worked_day ,content_right)
                                    i +=1
                                    irow += 1
                                worksheet_general.write(irow, icol-1,'' ,footer)
                                worksheet_general.write(irow, icol,'' ,footer)
                                gross = 0
                                net = 0 
                                for line in slip.line_ids:
                                    if line.code == 'VACATION_AMOUNT':
                                        worksheet_general.write(irow, icol+1,line.amount ,footer)
                                    if line.code == 'VACATION_WORKED_DAY':
                                        worksheet_general.write(irow, icol+2,line.amount ,footer)
                                    if line.code == 'VACATION_SALARY':
                                        worksheet_general.write(irow, icol+3,line.amount ,footer)
                                    if line.code == 'VACATION_DAY':
                                        worksheet_general.write(irow, icol+4,line.amount ,footer)
                                    if line.code == 'VACATION_GROSS':
                                        gross = line.amount
                                        worksheet_general.write(irow, icol+5,line.amount ,footer)
                                    if line.code == 'VACATION_NDSH_HHOAT':
                                        net = line.amount
                                        worksheet_general.write(irow, icol+6,line.amount ,footer)
                                    if line.code == 'VACATION_NET':
                                        net = line.amount
                                        worksheet_general.write(irow, icol+7,line.amount ,footer)
                                irow += 2
                                row = irow
                                worksheet_general.write(row + 1, 3,
                                                        u'Ерөнхий нягтлан бодогч: ........................ %s.%s' % (
                                                            self.get_emp_name_from_sign('general_acc', template.payslip_run.company_id)[1],
                                                            self.get_emp_name_from_sign('general_acc', template.payslip_run.company_id)[0]),
                                                        format_left)
                                worksheet_general.write(row + 3, 3, u'Цалин бодсон: ........................ %s.%s' % (
                                    template.payslip_run.create_employee.last_name,
                                    template.payslip_run.create_employee.name),
                                                        format_left)
                                worksheet_general.write(irow, icol + 7, gross - net, footer)
                                irow += 11
                else: #Цалингийн хуудасын багцтай бол
                    for slip in template.payslip_run.slip_ids:
                        for user in self.env['res.users'].browse(self.env.uid):
                            company = user.company_id
                        worksheet_general.write(irow - 4, icol-1, u'Компани: %s'%company.name, format_left)
                        worksheet_general.write(irow - 2, icol + 3, u'Амралтын мөнгө олгох хүснэгт', format_name)
                        worksheet_general.write(irow, icol-1, u'Овог, нэр: %s.%s' %(slip.employee_id.last_name or '', slip.employee_id.name),format_left)
                        irow +=1
                        worksheet_general.write(irow, icol-1, u'Албан тушаал: %s' %slip.employee_id.job_id.name or '', format_left)
                        irow +=1
                        worksheet_general.set_column(icol - 1, icol - 1, 3)
                        worksheet_general.write(irow, icol-1, u'Регистр: %s' %slip.employee_id.ssnid, format_left)
                        worksheet_general.set_column(icol, icol, 12)
                        worksheet_general.set_column(icol + 1, icol + 1, 15)
                        irow += 1
                        worksheet_general.set_row(irow, 30)
                        worksheet_general.write(irow, icol-1, u'№',theader)
                        worksheet_general.write(irow, icol, u'Огноо',theader)
                        worksheet_general.write(irow, icol+1, u'Цалин',theader)
                        worksheet_general.write(irow, icol+2, u'Ажилласан хоног',theader)
                        worksheet_general.set_column(icol+2,icol+2, 10)
                        worksheet_general.write(irow, icol+3, u'Нэг өдрийн цалин',theader)
                        worksheet_general.set_column(icol+3,icol+3, 10)
                        worksheet_general.write(irow, icol+4, u'Амрах хоног',theader)
                        worksheet_general.set_column(icol+4,icol+4, 6)
                        worksheet_general.write(irow, icol+5, u'Амралтын цалин',theader)
                        worksheet_general.set_column(icol+5,icol+5, 12)
                        worksheet_general.write(irow, icol+6, u'НДШ, ХХОАТ',theader)
                        worksheet_general.set_column(icol+6,icol+6, 10)
                        worksheet_general.write(irow, icol+7, u'Гарт олгох',theader)
                        worksheet_general.set_column(icol+7,icol+7, 12)
                        irow += 1
                        i = 1
                        for vac_line in slip.vacation_lines:
                            worksheet_general.write(irow, icol-1,str(i) ,content_right)
                            worksheet_general.write(irow, icol, u"%s" % (vac_line.period_id.code),content_center)
                            worksheet_general.write(irow, icol+1, vac_line.amount ,content_right)
                            worksheet_general.write(irow, icol+2, vac_line.worked_day ,content_right)
                            i +=1
                            irow += 1
                        worksheet_general.write(irow, icol-1,'' ,footer)
                        worksheet_general.write(irow, icol,'' ,footer)
                        gross = 0
                        net = 0 
                        for line in slip.line_ids:
                            if line.code == 'VACATION_AMOUNT':
                                worksheet_general.write(irow, icol+1,line.amount ,footer)
                            if line.code == 'VACATION_WORKED_DAY':
                                worksheet_general.write(irow, icol+2,line.amount ,footer)
                            if line.code == 'VACATION_SALARY':
                                worksheet_general.write(irow, icol+3,line.amount ,footer)
                            if line.code == 'VACATION_DAY':
                                worksheet_general.write(irow, icol+4,line.amount ,footer)
                            if line.code == 'VACATION_GROSS':
                                gross = line.amount
                                worksheet_general.write(irow, icol+5,line.amount ,footer)
                            if line.code == 'VACATION_NET':
                                net = line.amount
                                worksheet_general.write(irow, icol+7,line.amount ,footer)
                        irow += 2
                        row = irow
                        worksheet_general.write(row + 1, 3,
                                                u'Ерөнхий нягтлан бодогч: ........................ %s.%s' % (
                                                self.get_emp_name_from_sign('general_acc', template.payslip_run.company_id)[1],
                                                self.get_emp_name_from_sign('general_acc', template.payslip_run.company_id)[0]), format_left)
                        worksheet_general.write(row + 3, 3, u'Цалин бодсон: ........................ %s.%s' % (
                        template.payslip_run.create_employee.last_name, template.payslip_run.create_employee.name),
                                                format_left)
                        worksheet_general.write(irow-2, icol + 6, gross - net, footer)
                        irow += 11
            workbook.close()

            out = base64.encodestring(output.getvalue())
            excel_id = self.env['oderp.report.excel.output'].create({'filedata': out, 'filename': file_name+'.xlsx'})

            return excel_id.export_report()