# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from io import BytesIO
import base64
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from xlrd import sheet
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
import time
from datetime import datetime, date


class HrSalaryHistoryReport(models.TransientModel):
    """
        Цалингийн түүх тайлан
    """
    
    _name = "hr.salary.history.report"
    _description = "Hr Salary History Report"

    SALARY_TYPE = [
        ('last_salary', 'Last salary'),
        ('advance_salary', 'Advance salary')
    ]
    
    company = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    salary_type = fields.Selection(SALARY_TYPE, string='Salary type', default='last_salary', required=True)
    
    start_period = fields.Many2one('account.period', string='Start Period', required=True)
    end_period = fields.Many2one('account.period', string='End Period', required=True)
    group_by_department = fields.Boolean('Group by department', default=True)
    show_period_detail = fields.Boolean('Show Period Detail', default=True)
    
    payslip_runs = fields.Many2many('hr.payslip.run', 'sal_hist_report_pslip_run_rel', 'report_id', 'pslip_run_id', string='Hr Payslip Run')
    employees = fields.Many2many('hr.employee', 'sal_hist_report_emp_rel', 'report_id', 'emp_id', string='Employees')
    sal_rule_categs = fields.Many2many('hr.salary.rule.category', 'sal_hist_report_sal_rule_rel', 'report_id', 'rule_categ_id', string='Salary Rule Categories')

    @api.onchange("company")
    def _get_domains(self):
        domain = {}
        
        domain['payslip_run'] = [('company_id', '=', self.company.id), ('state', '=', 'done')]
        domain['start_period'] = [('company_id', '=', self.company.id)]
        domain['end_period'] = [('company_id', '=', self.company.id)]
        
        domain['employees'] = [('company_id', '=', self.company.id)]
        domain['sal_rule_categs'] = [('company_id', '=', self.company.id)]
        
        return {'domain': domain}
    
    @api.onchange("start_period", "end_period", "salary_type")
    def _get_payslip_runs_domain(self):
        domain = {}
        
        domain['payslip_runs'] = [('company_id', '=', self.company.id),
                                  ('salary_type', '=', self.salary_type),
                                  ('state', '=', 'done'),
                                  ('period_id.date_start', '>=', self.start_period.date_start), ('period_id.date_start', '<=', self.end_period.date_start)]
        
        return {'domain': domain}
    
    @api.onchange("salary_type", "company", "payslip_runs", "employees", "start_period", "end_period")
    def _get_sal_rule_categs_domain(self):
        domain = {}
        domain['sal_rule_categs'] = [('id', 'in', self.get_rule_cat_ids())]
        return {'domain': domain}
    
    @api.onchange('start_period', 'end_period')
    def check_periods(self):
        for obj in self:
            if obj.start_period and obj.end_period:
                if obj.start_period.date_start > obj.end_period.date_start:
                    raise UserError(_(u"End Period must be older than Start Period!!!"))
    
    def get_rule_cat_ids(self):
        employee_qry = " emp.id IN (" + str(self.employees.ids)[1:len(str(self.employees.ids)) - 1] + ") " if str(self.employees.ids) != "[]" else ""
            
        payslip_run_qry = "AND (hp.payslip_run_id IN (" + str(self.payslip_runs.ids)[1:len(str(self.payslip_runs.ids)) - 1] + ") AND prun.state = 'done') "  if str(self.payslip_runs.ids) != "[]" else ""
        if employee_qry and payslip_run_qry:
            payslip_run_qry = "AND ( %s OR (hp.payslip_run_id IN (" % employee_qry + str(self.payslip_runs.ids)[1:len(str(self.payslip_runs.ids)) - 1] + ") AND prun.state = 'done') ) "  if str(self.payslip_runs.ids) != "[]" else ""
        elif employee_qry and not payslip_run_qry:
            payslip_run_qry = " AND %s" % employee_qry
            
        period_qry = " AND (ap.date_start >= '%s') AND (ap.date_start <= '%s')" % (self.start_period.date_start, self.end_period.date_start) if self.start_period and self.end_period else ""
        
        qry = """
            SELECT rule_ctg.id FROM hr_payslip_line hpl
            LEFT JOIN hr_payslip hp ON hp.id = hpl.slip_id
            LEFT JOIN hr_employee emp ON emp.id = hp.employee_id
            LEFT JOIN account_period ap ON ap.id = hp.period_id
            LEFT JOIN hr_payslip_run prun ON prun.id = hp.payslip_run_id
            LEFT JOIN hr_salary_rule_category rule_ctg ON rule_ctg.id = hpl.category_id
            WHERE hp.company_id = %s
                %s
                AND LOWER(hp.salary_type) = '%s'
                AND LOWER(rule_ctg.salary_type) = '%s'
                %s
                GROUP BY rule_ctg.id
        """ % (self.company.id, period_qry, self.salary_type.lower(), self.salary_type.lower(), payslip_run_qry)
        
        self._cr.execute(qry)
        results = self._cr.dictfetchall()
        
        if results:
            return [result['id'] for result in results]
        else:
            return []
        
    def  get_xsl_column_name(self, index):
        alphabet = {'0': 'A', '1':'B', '2':'C', '3':'D', '4':'E',
                    '5': 'F', '6':'G', '7':'H', '8':'I', '9':'J',
                    '10':'K', '11':'L', '12':'M', '13':'N', '14':'O',
                    '15':'P', '16':'Q', '17':'R', '18':'S', '19':'T',
                    '20':'U', '21':'V', '22':'W', '23':'X', '24':'Y', '25':'Z'}
        
        if index <= 25:
            return (alphabet[str(index)] + ":" + alphabet[str(index)])
        else:
            return (alphabet[str(index / 26 - 1)] + alphabet[str(index % 26)] + ":" + alphabet[str(index / 26 - 1)] + alphabet[str(index % 26)])
    
    def get_column_name_for_calculate(self, index):
        column_name = self.get_xsl_column_name(index).split(':')[0]
        return column_name
    
    def build_footer(self, sheet, title_id, rowx, format_text_left_bold, format_text_left):
        fname = ""
        lname = ""
        title = ""
        if title_id == 1:
            title = _(u'General accountant:')
            lname = self.company.genaral_accountant_signature.last_name[:1] if self.company.genaral_accountant_signature and self.company.genaral_accountant_signature.last_name and len(self.company.genaral_accountant_signature) > 0 else lname
            fname = self.company.genaral_accountant_signature.name if self.company.genaral_accountant_signature and self.company.genaral_accountant_signature.name else fname
        elif title_id == 2:
            title = _(u'Calculated By:')
            lname = self.env.user.last_name[:1] if self.env.user.last_name and len(self.env.user.last_name) > 0 else lname
            fname = self.env.user.name if self.env.user.name else fname
        elif title_id == 3:
            title = _(u'Approved By:')
            lname = self.company.first_sign.last_name[:1] if self.company.first_sign and self.company.first_sign.last_name and len(self.company.first_sign) > 0 else lname
            fname = self.company.first_sign.name if self.company.first_sign and self.company.first_sign.name else fname
        rowx += 2        
        sheet.merge_range(rowx, 0, rowx, 2, title, format_text_left_bold)
        if lname:
            sheet.merge_range(rowx, 3, rowx, 5, u"................................................... /%s.%s/" % (lname, fname), format_text_left)
        elif fname:
            sheet.merge_range(rowx, 3, rowx, 5, u"................................................... /%s/" % (fname), format_text_left)
        else:
            sheet.merge_range(rowx, 3, rowx, 5, u"................................................... ", format_text_left)
        
        return sheet, rowx
        
    def _get_lines(self):
        rule_ids = self.get_rule_cat_ids()
        sal_rule_categ_qry = ""
        if self.sal_rule_categs:
            sal_rule_categ_qry = " AND srule_categ.id IN (" + str(self.sal_rule_categs.ids)[1:len(str(self.sal_rule_categs.ids)) - 1] + ") " if str(self.sal_rule_categs.ids) != "[]" else ""
        else:
            if rule_ids and len(rule_ids) > 0:
                sal_rule_categ_qry = " AND srule_categ.id IN (%s)" % (", ".join(str(rule_id) for rule_id in rule_ids))
            
        period_qry = """ AND (period.date_start >= '%s') 
                         AND (period.date_start <= '%s')
                     """ % (self.start_period.date_start, self.end_period.date_start)
        
        employee_qry = " emp.id IN (" + str(self.employees.ids)[1:len(str(self.employees.ids)) - 1] + ") " if str(self.employees.ids) != "[]" else ""
            
        payslip_run_qry = "AND (hp.payslip_run_id IN (" + str(self.payslip_runs.ids)[1:len(str(self.payslip_runs.ids)) - 1] + ") AND prun.state = 'done') "  if str(self.payslip_runs.ids) != "[]" else ""
        if employee_qry and payslip_run_qry:
            payslip_run_qry = "AND ( %s OR (hp.payslip_run_id IN (" % employee_qry + str(self.payslip_runs.ids)[1:len(str(self.payslip_runs.ids)) - 1] + ") AND prun.state = 'done') ) "  if str(self.payslip_runs.ids) != "[]" else ""
        elif employee_qry and not payslip_run_qry:
            payslip_run_qry = " AND %s" % employee_qry
            
        order_by_dep = ""
        if self.group_by_department:
            order_by_dep = "dep.name, dep.id,"
            
        qry = """
            SELECT  dep.id as dep_id, dep.name as dep_name, 
                emp.id as emp_id, emp.ssnid as emp_register, lvl.name as emp_lvl, 
                emp.name_related as emp_name, emp.last_name as emp_lname, job.name as emp_job, 
                period.id as period_id, period.name as period_name,
                srule_categ.id as srule_id, srule_categ.name as srule_name, table1.hpl_total as hpl_total
            
            FROM
            (
                SELECT dep.id as dep_id,
                    emp.id as emp_id,
                    period.id as period_id,
                    srule_categ.id as srule_categ_id, SUM(hpl.total) as hpl_total
                FROM hr_payslip hp 
                LEFT JOIN hr_employee emp ON emp.id = hp.employee_id
                LEFT JOIN hr_department dep ON dep.id = hp.department_id
                LEFT JOIN account_period period ON period.id = hp.period_id
                LEFT JOIN hr_payslip_line hpl ON hpl.slip_id = hp.id
                LEFT JOIN hr_salary_rule srule ON srule.id = hpl.salary_rule_id
                LEFT JOIN hr_salary_rule_category srule_categ ON srule_categ.id = srule.category_id
                LEFT JOIN hr_payslip_run prun ON prun.id = hp.payslip_run_id
                WHERE hp.company_id = %s
                    %s 
                    %s
                    %s
                    AND LOWER(hp.salary_type) = '%s'
                    AND LOWER(srule_categ.salary_type) = '%s'
                GROUP BY dep.id, emp.id, period.id, srule_categ.id
            ) table1 
            LEFT JOIN hr_employee emp ON emp.id = table1.emp_id
            LEFT JOIN hr_department dep ON dep.id = table1.dep_id
            LEFT JOIN hr_skill_level lvl ON lvl.id = emp.skill_level
            LEFT JOIN hr_job job ON job.id = emp.job_id
            LEFT JOIN account_period period ON period.id = table1.period_id
            LEFT JOIN hr_salary_rule_category srule_categ ON srule_categ.id = table1.srule_categ_id
            ORDER BY %s emp.name_related, emp.ssnid, emp.last_name, emp.id, period.date_start, period.date_stop, period.id, srule_categ.sequence
        """ % (self.company.id,
              sal_rule_categ_qry, period_qry, payslip_run_qry, self.salary_type.lower(), self.salary_type.lower(), order_by_dep)
        
        self._cr.execute(qry)
        results = self._cr.dictfetchall()
        
        return results if results else False
    
    def get_sum_formula_from_list(self, colx, rows):
        formula = "="
        if list:
            for i in range(len(rows)):
                formula += ("+" + self.get_column_name_for_calculate(colx) + str(rows[i] + 1))
        return formula
    
    def fill_rule_ids(self, sheet, rowx, static_col_count, dynamic_col_indexes_dict, dynamic_col_ids, writen_rule_ids, format_number_right_bordered):
        # Үлдсэн sal_rule_categs-г 0 утгаар дүүргэх
        for rule_id in dynamic_col_ids:      
            if rule_id not in writen_rule_ids:
                sheet.write(rowx, static_col_count + dynamic_col_indexes_dict[str(rule_id)], 0, format_number_right_bordered)
        return sheet
        
    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        
        # create name
        report_name = _(u"Salary History Report")
        file_name = "%s_%s.xlsx" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('sales_history_report'), form_title=file_name).create({})
        
        # create formats
        format_text_left = {
            'font_name': 'Times New Roman',
            'font_size': 10,
            'align': 'left',
        }
        
        format_text_right = format_text_left.copy()
        format_text_right['align'] = 'right'
        
        format_text_right_bold = format_text_right.copy()
        format_text_right_bold['bold'] = True
        
        format_text_left_bold = format_text_left.copy()
        format_text_left_bold['bold'] = True
        
        format_text_center_header = format_text_left_bold.copy()
        format_text_center_header['font_size'] = '12'
        format_text_center_header['align'] = 'center'
        
        format_text_left_bordered = format_text_left.copy()
        format_text_left_bordered['border'] = 1
        
        format_text_left_bordered_red = format_text_left_bordered.copy()
        format_text_left_bordered_red['color'] = '#ed1c24'
        
        format_number_right_bordered = format_text_left_bordered.copy()
        format_number_right_bordered['num_format'] = '_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)'
        format_number_right_bordered['align'] = 'right'
        
        format_dd_right_bordered = format_number_right_bordered.copy()
        format_dd_right_bordered['num_format'] = '#,##0_);(#,##0)'
        
        format_dd_right_bordered_colored = format_dd_right_bordered.copy()
        format_dd_right_bordered_colored['bg_color'] = '#ccffff'
        
        format_column_header = format_text_left_bordered.copy()
        format_column_header['bold'] = True
        format_column_header['bg_color'] = '#99ccff'
        format_column_header['text_wrap'] = 1
        format_column_header['align'] = 'center'
        
        format_column_header_red = format_column_header.copy()
        format_column_header_red['color'] = '#ed1c24'
        
        format_column_footer_text_right = format_column_header.copy()
        format_column_footer_text_right['align'] = 'right'
        
        format_column_footer_number = format_column_header.copy()
        format_column_footer_number['num_format'] = '_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)'
        format_column_footer_number['align'] = 'right'
        format_column_footer_number['font_size'] = '11'
        
        format_column_subh_text = format_column_header.copy()
        format_column_subh_text['bg_color'] = '#ccffff'
        format_column_subh_text['align'] = 'right'
        
        format_column_subh_number = format_column_subh_text.copy()
        format_column_subh_number['num_format'] = '_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)'
        format_column_subh_number['align'] = 'right'
        format_column_subh_number['font_size'] = '11'
        
        format_column_subh_dd_right_bordered_colored = format_dd_right_bordered_colored.copy()
        format_column_subh_dd_right_bordered_colored['bold'] = True
        format_column_subh_dd_right_bordered_colored['font_size'] = '10'
        format_column_subh_dd_right_bordered_colored['bg_color'] = '#ffe5ca'
        
        format_column_subh_subh_text_left = format_column_header.copy()
        format_column_subh_subh_text_left['bg_color'] = '#ffe5ca'
        format_column_subh_subh_text_left['align'] = 'left'
        format_column_subh_subh_text_left['text_wrap'] = 0
        
        format_column_subh_subh_text_center = format_column_subh_subh_text_left.copy()
        format_column_subh_subh_text_center['align'] = 'center'
        format_column_subh_subh_text_center['color'] = '#ed1c24'
        
        format_column_subh_subh_number_right = format_number_right_bordered.copy()
        format_column_subh_subh_number_right['bg_color'] = '#ffe5ca'
        format_column_subh_number['font_size'] = '10'
        
        format_text_left = book.add_format(format_text_left)
        format_text_center_header = book.add_format(format_text_center_header)
        format_text_right = book.add_format(format_text_right)
        format_text_right_bold = book.add_format(format_text_right_bold)
        format_text_left_bordered = book.add_format(format_text_left_bordered)
        format_text_left_bold = book.add_format(format_text_left_bold)
        format_number_right_bordered = book.add_format(format_number_right_bordered)
        format_dd_right_bordered = book.add_format(format_dd_right_bordered)
        format_dd_right_bordered_colored = book.add_format(format_dd_right_bordered_colored)
        format_column_header = book.add_format(format_column_header)
        format_column_header.set_align('vcenter')
        format_column_header_red = book.add_format(format_column_header_red)
        format_column_header_red.set_align('vcenter')
        format_column_footer_text_right = book.add_format(format_column_footer_text_right)
        format_column_footer_number = book.add_format(format_column_footer_number)
        format_column_subh_text = book.add_format(format_column_subh_text)
        format_column_subh_number = book.add_format(format_column_subh_number)
        format_column_subh_dd_right_bordered_colored = book.add_format(format_column_subh_dd_right_bordered_colored)
        format_column_subh_subh_text_left = book.add_format(format_column_subh_subh_text_left)
        format_column_subh_subh_text_center = book.add_format(format_column_subh_subh_text_center)
        format_text_left_bordered_red = book.add_format(format_text_left_bordered_red)
        format_column_subh_subh_number_right = book.add_format(format_column_subh_subh_number_right)
        
        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_portrait()
        sheet.set_zoom(100)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        
        if not self.sal_rule_categs:
            rule_ids = self.get_rule_cat_ids()
            if rule_ids and len(rule_ids) > 0:
                self.sal_rule_categs = self.env['hr.salary.rule.category'].search([('id', 'in', rule_ids)])
        
        dynamic_col_count = len(self.sal_rule_categs) if self.sal_rule_categs else 0
        dynamic_col_names = [rule.name for rule in self.sal_rule_categs]
        dynamic_col_ids = [rule.id for rule in self.sal_rule_categs]
        dynamic_col_indexes_dict = {}
        
        for i in range(dynamic_col_count):
            dynamic_col_indexes_dict[str(dynamic_col_ids[i])] = i
            
        static_col_count = 7
        
        coly = 0
        # build static columns
        #    №    РД    Зэрэг    Нэр    Овог    Албан тушаал    Он/Сар
        sheet.set_column(self.get_xsl_column_name(coly), 3)
        sheet.set_column(self.get_xsl_column_name(coly + 1), 10)
        sheet.set_column(self.get_xsl_column_name(coly + 2), 6)
        sheet.set_column(self.get_xsl_column_name(coly + 3), 12)
        sheet.set_column(self.get_xsl_column_name(coly + 4), 12)
        sheet.set_column(self.get_xsl_column_name(coly + 5), 25)
        sheet.set_column(self.get_xsl_column_name(coly + 6), 10)
        
        # build dynamic columns
        for i in range(dynamic_col_count):
            sheet.set_column(self.get_xsl_column_name(coly + static_col_count + i), 10)
        
        # build xslx header 
        rowx = 1
        sl_type_in_header = ""
        if self.salary_type == 'last_salary':
            sl_type_in_header = u'СҮҮЛ ЦАЛИН' if self.env.user.lang == 'mn_MN' else u"LAST SALARY"
        elif self.salary_type == 'advance_salary':
            sl_type_in_header = u'УРЬДЧИЛГАА ЦАЛИН' if self.env.user.lang == 'mn_MN' else u"ADVANCED SALARY"
            
        sheet.merge_range(rowx, 0, rowx, static_col_count - 1, _(u"%s's %s PAYROLL TABLE (%s - %s)") % (self.company.name, sl_type_in_header, self.start_period.name, self.end_period.name), format_text_center_header)
        sheet.merge_range(rowx + 2, 0, rowx + 2, 2, _(u'Report Duration:'), format_text_left_bold)
        sheet.merge_range(rowx + 2, 3, rowx + 2, 4, u'%s ~ %s' % (self.start_period.name, self.end_period.name), format_text_left) 
        sheet.write(rowx + 2, static_col_count + dynamic_col_count - 2, _(u'Printed Date:'), format_text_right_bold)
        sheet.write(rowx + 2, static_col_count + dynamic_col_count - 1, u'%s' % datetime.now().strftime('%Y-%m-%d'), format_text_right) 

        # build column headers
        rowx += 3
        coly = 0
        sheet.merge_range(rowx, coly, rowx + 2, coly, _(u'№'), format_column_header)
        sheet.merge_range(rowx, coly + 1, rowx + 2, coly + 1, _(u'Register Number'), format_column_header)
        sheet.merge_range(rowx, coly + 2, rowx + 2, coly + 2, _(u'Level'), format_column_header)
        sheet.merge_range(rowx, coly + 3, rowx + 2, coly + 3, _(u'First Name'), format_column_header)
        sheet.merge_range(rowx, coly + 4, rowx + 2, coly + 4, _(u'Last Name'), format_column_header)
        sheet.merge_range(rowx, coly + 5, rowx + 2, coly + 5, _(u'Job Position'), format_column_header)
        sheet.merge_range(rowx, coly + 6, rowx + 2, coly + 6, _(u'Date/Time'), format_column_header_red)
        coly += static_col_count
        
        for i in range(dynamic_col_count):
            sheet.merge_range(rowx, coly + i, rowx + 2, coly + i, dynamic_col_names[i], format_column_header)
            
        results = self._get_lines()
        
        if results:
            all_emp_ids_for_count = []
            all_emp_count = 0
            
            writen_dep_ids = []
            writen_emp_ids = []
            writen_period_ids = []
            writen_rule_ids = []
            
            rowx += 2
            
            dep_count = 0
            emp_count = 0 
            
            level1_rows = []
            level2_rows = []
            level3_rows = []
            
            last_emp_rowx = -1
            last_emp_period_rowx = -1
            last_emp_period_rowxs = []
            last_dep_rowx = -1
            last_dep_emp_rowxs = [] 
            all_emp_rowxs = []
            all_dep_rowxs = []
            
            for result in results:
                if self.group_by_department and result['dep_id'] not in writen_dep_ids:
                    # Хэлтэсийн багануудад томъёо оруулах
                    if last_dep_rowx != -1:
                        for i in range(len(dynamic_col_ids)):
                            sheet.write(last_dep_rowx, static_col_count + i, self.get_sum_formula_from_list(static_col_count + i, last_dep_emp_rowxs), format_column_subh_number)
                        
                    # Үлдсэн sal_rule_categs-г 0 утгаар дүүргэх
                    if last_emp_period_rowx != -1:
                        sheet = self.fill_rule_ids(sheet, rowx, static_col_count, dynamic_col_indexes_dict, dynamic_col_ids, writen_rule_ids, format_number_right_bordered)
                            
                    # start /one row/ -------- build department's row
                    coly = 0
                    rowx += 1
                    dep_count += 1
                    last_dep_emp_rowxs = []
                    writen_rule_ids = []
                    writen_emp_ids = []
                    
                    sheet.write(rowx, coly, dep_count, format_dd_right_bordered_colored)
                    sheet.merge_range(rowx, coly + 1, rowx, coly + static_col_count - 2, result['dep_name'] if result['dep_name'] else '', format_column_subh_text)
                    sheet.write(rowx, coly + static_col_count - 1, '', format_column_subh_text)
                    
                    last_dep_rowx = rowx
                    all_dep_rowxs.append(rowx)
                    level1_rows.append(rowx)
                    coly += static_col_count
                    
                    writen_dep_ids.append(result['dep_id'] if result['dep_id'] else -1)
                
                    # end /one row/ -------- build department's row
                    
                if result['emp_id'] not in all_emp_ids_for_count:
                    all_emp_ids_for_count.append(result['emp_id'])
                    all_emp_count += 1
                    
                if result['emp_id'] not in writen_emp_ids:
                    # Үлдсэн sal_rule_categs-г 0 утгаар дүүргэх
                    if len(writen_period_ids) > 0 and not self.group_by_department:
                        sheet = self.fill_rule_ids(sheet, rowx, static_col_count, dynamic_col_indexes_dict, dynamic_col_ids, writen_rule_ids, format_number_right_bordered)
                                
                    if last_emp_rowx != -1:
                        sheet.write(last_emp_rowx, 6, _(u"Total %s") % len(writen_period_ids), format_column_subh_subh_text_center)
                        for i in range(len(dynamic_col_ids)):
                            sheet.write(last_emp_rowx, static_col_count + i, self.get_sum_formula_from_list(static_col_count + i, last_emp_period_rowxs), format_column_subh_subh_number_right)
                        
                    # build employee's row
                    coly = 0
                    rowx += 1
                    writen_period_ids = []
                    writen_rule_ids = []
                    last_emp_period_rowxs = []
                    emp_count += 1
                    
                    sheet.write(rowx, coly, emp_count, format_column_subh_dd_right_bordered_colored)
                    sheet.write(rowx, coly + 1, result['emp_register'] if result['emp_register'] else '', format_column_subh_subh_text_left)
                    sheet.write(rowx, coly + 2, result['emp_lvl'] if result['emp_lvl'] else '', format_column_subh_subh_text_left)
                    sheet.write(rowx, coly + 3, result['emp_name'] if result['emp_name'] else '', format_column_subh_subh_text_left)
                    sheet.write(rowx, coly + 4, result['emp_lname'] if result['emp_lname'] else '', format_column_subh_subh_text_left)
                    sheet.write(rowx, coly + 5, result['emp_job'] if result['emp_job'] else '', format_column_subh_subh_text_left)
                    
                    last_emp_rowx = rowx
                    last_dep_emp_rowxs.append(rowx)
                    all_emp_rowxs.append(rowx)
                    level2_rows.append(rowx)
                    
                    coly += static_col_count
                    
                    writen_emp_ids.append(result['emp_id'] if result['emp_id'] else -1)
                    
                if result['period_id'] not in writen_period_ids:
                    # Үлдсэн sal_rule_categs-г 0 утгаар дүүргэх
                    if len(writen_period_ids) > 0:
                        sheet = self.fill_rule_ids(sheet, rowx, static_col_count, dynamic_col_indexes_dict, dynamic_col_ids, writen_rule_ids, format_number_right_bordered)
                                    
                    # build period's row
                    coly = 0
                    rowx += 1
                    writen_rule_ids = []
                    
                    for i in range(static_col_count - 1):
                        sheet.write(rowx, coly + i, '', format_text_left_bordered)
                    coly += (static_col_count - 1)
                    sheet.write(rowx, coly, result['period_name'] if result['period_name'] else '', format_text_left_bordered_red)   
                    
                    level3_rows.append(rowx)
                    last_emp_period_rowxs.append(rowx)
                    last_emp_period_rowx = rowx
                    coly += 1
                
                    if result['srule_id']:
                        sheet.write(rowx, coly + dynamic_col_indexes_dict[str(result['srule_id'])], result['hpl_total'] if result['hpl_total'] else 0, format_number_right_bordered)
                    writen_rule_ids.append(result['srule_id'] or -1)
                    
                    writen_period_ids.append(result['period_id'] or -1)
                else:
                    if result['srule_id']:
                        sheet.write(rowx, coly + dynamic_col_indexes_dict[str(result['srule_id'])], result['hpl_total'] if result['hpl_total'] else 0, format_number_right_bordered)
                    writen_rule_ids.append(result['srule_id'] or -1)
                    
                # Нийлбэрийн томъёо бүхий баганууд нь нийлбэрүүдийнхээ дээр байрласан тул хамгийн сүүлийн мөрийн хувьд нийлбэрийг тооцож чадахгүй. 
                # Тиймээс доорхи шалгалтаар хамгийн сүүлийн мөрүүдэд харгалзах тухайн ажилтны/хэлтэсийн нийт цалингийн томъёоллын дүнг тооцно.
                if results[-1] == result:
                    if last_dep_rowx != -1:
                        for i in range(len(dynamic_col_ids)):
                            sheet.write(last_dep_rowx, static_col_count + i, self.get_sum_formula_from_list(static_col_count + i, last_dep_emp_rowxs), format_column_subh_number)
                            
                    if last_emp_rowx != -1:
                        sheet.write(last_emp_rowx, 6, _(u"Total %s") % len(writen_period_ids), format_column_subh_subh_text_center)
                        for i in range(len(dynamic_col_ids)):
                            sheet.write(last_emp_rowx, static_col_count + i, self.get_sum_formula_from_list(static_col_count + i, last_emp_period_rowxs), format_column_subh_subh_number_right)
                            
                    # Үлдсэн sal_rule_categs-г 0 утгаар дүүргэх
                    if len(writen_period_ids) > 0:
                        sheet = self.fill_rule_ids(sheet, rowx, static_col_count, dynamic_col_indexes_dict, dynamic_col_ids, writen_rule_ids, format_number_right_bordered)
                          
            # set group & collapse for rows
            for i in range(len(level1_rows)):
                sheet.set_row(level1_rows[i], None, None, {'level': 1})   
            for i in range(len(level2_rows)):
                sheet.set_row(level2_rows[i], None, None, {'level': 2})   
            for i in range(len(level3_rows)):
                sheet.set_row(level3_rows[i], None, None, {'level': 3, 'hidden': not self.show_period_detail, 'collapsed': self.show_period_detail})   
                  
            # build table footer
            rowx += 1
            coly = 0
            sheet.merge_range(rowx, coly, rowx, coly + 3, _(u"Count of employee: %s") % all_emp_count, format_column_footer_text_right)
            sheet.write(rowx, coly + 4, '', format_column_footer_text_right)
            sheet.write(rowx, coly + 5, _(u'Totals'), format_column_footer_text_right)
            sheet.write(rowx, coly + 6, '', format_column_footer_text_right)
            for i in range(len(dynamic_col_ids)):   
                if self.group_by_department:   
                    sheet.write(rowx, static_col_count + i, self.get_sum_formula_from_list(static_col_count + i, all_dep_rowxs), format_column_footer_number)
                else:   
                    sheet.write(rowx, static_col_count + i, self.get_sum_formula_from_list(static_col_count + i, all_emp_rowxs), format_column_footer_number)
                     
            # build xslx footer
            sheet, rowx = self.build_footer(sheet, 1, rowx, format_text_left_bold, format_text_left)
            sheet, rowx = self.build_footer(sheet, 2, rowx, format_text_left_bold, format_text_left)
            # Батласан гэсэн мөр нэмэх бол доорхи комментыг авах
#             sheet, rowx = self.build_footer(sheet, 3, rowx, format_text_left_bold, format_text_left)
              
        book.close()
        
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report() 
        
        
