# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models, _
from odoo.exceptions import UserError
import time
from datetime import datetime, timedelta


class HrPayslipEmployees(models.TransientModel):
    _inherit = 'hr.payslip.employees'

    @api.multi
    def compute_sheet(self):
        payslips = self.env['hr.payslip']
        period_pool = self.env['account.period']
        [data] = self.read()
        active_id = self.env.context.get('active_id')
        if active_id:
            [run_data] = self.env['hr.payslip.run'].browse(active_id).read(['date_start', 'date_end', 'credit_note', 'period_id',
                                                                            'bonus_salary', 'create_employee', 'journal_id', 'salary_type', 'credit_note'])

        from_date = run_data.get('date_start')
        to_date = run_data.get('date_end')
        period_id = run_data.get('period_id', False)
        if period_id:
            period_id = period_id[0]
        bonus_salary = run_data.get('bonus_salary', False)
        period = False
        if period_id:
            period = period_pool.search([('id', '=', period_id)])
            if period:
                from_date = period.date_start
                to_date = period.date_stop
        create_employee = run_data.get('create_employee', False)
        if create_employee:
            create_employee = create_employee[0]

        if period_id:
            credit_note = run_data.get('credit_note', False)
            salary_type = run_data.get('salary_type', False)
            journal_id = run_data.get('journal_id', False)
            if journal_id:
                journal_id = journal_id[0]
            if not data['employee_ids']:
                raise UserError(_("You must select employee(s) to generate payslip(s)."))
            check = self.check_payslip_employee_warnings(data['employee_ids'], from_date, to_date, salary_type, period)
            if check:
                for employee in self.env['hr.employee'].browse(data['employee_ids']):
                    employee_name = employee.name
                    if employee.last_name:
                        employee_name += ' %s' % (employee.last_name,)
                    if employee.job_id.name:
                        employee_name += ' %s' % (employee.job_id.name,)
                    slip_data = self.env['hr.payslip'].onchange_employee_id(from_date, to_date, salary_type=salary_type, employee_id=employee.id, contract_id=False)
                    slip_data_contract = self.env['hr.payslip'].onchange_contract_id(slip_data['value'].get('contract_id', False), from_date, period, salary_type)
                    slip_data_type = self.env['hr.payslip'].onchange_type_id(slip_data['value'].get('contract_id', False), employee, period, salary_type)
                    res = {
                        'employee_id': employee.id,
                        'name': slip_data['value'].get('name', False),
                        'department_id': slip_data['value'].get('department_id'),
                        'contract_id': slip_data['value'].get('contract_id', False),
                        'ndsh_type': slip_data['value'].get('ndsh_type'),
                        'struct_id': slip_data_type['value'].get('struct_id', False),
                        'payslip_run_id': self.env.context.get('active_id', False),
                        'input_line_ids': [(0, 0, x) for x in slip_data_contract['value'].get('input_line_ids', [])],
                        'worked_days_line_ids': [(0, 0, x) for x in slip_data_type['value'].get('worked_days_line_ids', [])],
                        'date_from': from_date,
                        'date_to': to_date,
                        'period_id': period_id,
                        'credit_note': credit_note,
                        'journal_id': journal_id,
                        'advance_salary_id': slip_data_type['value'].get('advance_salary_id', False),
                        'vacation_id': slip_data_type['value'].get('vacation_id', False),
                        'salary_type': salary_type,
                        'is_esti_adv_days': slip_data_type['value'].get('is_esti_adv_days'),
                        'create_employee': create_employee,
                        'previous_salary_id': slip_data_type['value'].get('previous_salary_id', False),
                        'bonus_salary': bonus_salary,
                        'hchta_id': slip_data_type['value'].get('hchta_id', False),
                        'maternity_salary_id': slip_data_type['value'].get('maternity_salary_id', False),
                    }
                    if salary_type == 'vacation':
                        res['vacation_day'] = employee.annual_leave_days
                    if salary_type in ('last_salary', 'advance_salary'):
                        res['hour_balance_lines'] = [(4, x) for x in slip_data_type['value'].get('hour_balance_lines', [])]
                        if bonus_salary:
                            res['vacation_lines'] = [(0, 0, x) for x in slip_data_type['value'].get('vacation_lines', [])]
                    if salary_type in ('vacation', 'maternity', 'hchta'):
                        res['vacation_lines'] = [(0, 0, x) for x in slip_data_type['value'].get('vacation_lines', [])]
                    payslips += self.env['hr.payslip'].create(res)

            # "Цалин -> Цалингийн хуудсын багцууд" цэсний дэлгэцэнд Цалингийн тооцооллын нийт дүн таб шинээр нэмэгдсэн бөгөөд
            # тухайн таб дахь утгууд нь нийт цалингийн хуудсуудыг бодогдож дууссаны дараа үр дүнг нь нэгтгэх тул "payslips.compute_sheet()" гэсэн
            # кодыг коммент болгож дараах функцыг дуудаж ажиллуулав.
            self.compute_all_sheet(payslips.ids, active_id)
            # payslips.compute_sheet()

        else:
            raise UserError(_("Period not selected in Payslip. You must choose period first."))

        return {'type': 'ir.actions.act_window_close'}

    @api.multi
    def compute_all_sheet(self, p_ids, run_id):
        sum_ids = []
        slip_ids = []
        for slip in self.env['hr.payslip'].browse(p_ids):
            slip.compute_sheet()

        for run in self.env['hr.payslip.run'].browse(run_id):
            for slip in run.slip_ids:
                if slip.id not in slip_ids:
                    slip_ids.append(slip.id)
            for slip_sum in run.slip_sum_ids:
                sum_ids.append(slip_sum.id)
        if sum_ids:
            self._cr.execute('DELETE FROM hr_payslip_sum where id in (%s)' % ','.join(str(a) for a in sum_ids))

        # Цалингийн тооцооллын нийт дүн талбар дахь утгуудыг авч, бичиж байна.
        self.env.cr.execute("""select m.sid as sid, m.run_id as rid, m.name as name, m.code as code, m.seq as seq, sum(m.amount) as amount, m.rule_id as rule from (select p.id as pid, p.struct_id as sid, p.payslip_run_id as run_id, l.id as lid, l.code as code, l.name as name, l.salary_rule_id as rule_id, l.sequence as seq, l.amount as amount
                    from hr_payslip p left join hr_payslip_line l on p.id=l.slip_id where p.id in (%s) ) as m
                    group by m.sid,m.run_id, m.name, m.code, m.seq, m.rule_id""" % ','.join(str(a) for a in slip_ids))
        output_lines = self.env.cr.dictfetchall()

        for output in output_lines:
            self.env.cr.execute("""insert into hr_payslip_sum (create_uid,create_date,write_uid,write_date,
                        payslip_run_id, struct_id,code, name, total, sequence, rule_id) values (%s,'%s',%s,'%s',%s,%s,'%s','%s',%s, %s, %s) """
                                % (self.env.user.id, datetime.now(), self.env.user.id, datetime.now(),
                                   output['rid'], output['sid'], output['code'], output['name'],
                                   output['amount'], output['seq'], output['rule']))
        return True

    def check_payslip_employee_warnings(self, emp_ids,from_date, to_date, salary_type, period):
        hour_balance_messages = []
        contract_messages = []
        for employee in self.env['hr.employee'].browse(emp_ids):
            employee_name = employee.name
            if employee.last_name:
                employee_name += ' %s' % (employee.last_name,)
            if employee.job_id.name:
                employee_name += ' %s' % (employee.job_id.name,)
            # Цагийн баланс олох
            hour_balance_line_id = self.env['hr.payslip']._get_hour_balance(employee.id, salary_type, period)
            if salary_type in ('last_salary', 'advance_salary'):
                if not hour_balance_line_id:
                    hour_balance_messages.append(_(u"[%s] There is no hour balance line for the employee.\n The  must be hour balance line!") % employee_name)
            slip_data = self.env['hr.payslip'].onchange_employee_id(from_date, to_date, salary_type=salary_type,
                                                                    employee_id=employee.id, contract_id=False)
            if not slip_data['value'].get('contract_id', False):
                contract_messages.append(_(u"[%s] The term of the employee's contract is incorrect.\n In order to be included in the payroll, the contract period must be determined correctly!") % employee_name)
        # Эхлээд цагийн баланс байхгүй бол анхааруулга харуулна.
        if hour_balance_messages:
            message = '\n \n'.join(hour_balance_messages)
            raise UserError(_(message))
        # Гэрээтэй холбоотой анхааруулгыг харуулна.
        if contract_messages:
            message = '\n \n'.join(contract_messages)
            raise UserError(_(message))
        return True
