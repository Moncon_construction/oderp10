# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError
import time
from datetime import datetime, timedelta


class ResCompany(models.Model):
    _inherit = 'res.company'

    call_payroll_company = fields.Boolean(string='Call the payroll company')


class HrPayslipEmployees(models.TransientModel):
    _inherit = 'hr.payslip.employees'

    call_employees = fields.Boolean(string='Call employees')
    
    @api.model
    def create(self, vals):
        # Many2many - н утга /(1, ID, { values }) update the linked record with id = ID (write values on it)/ гэж орж ирч байсныг 
        # /(4, ID) link to existing record with id = ID (adds a relationship)/ гэж засав.
        if 'call_employees' in vals.keys() and vals['call_employees'] and 'employee_ids' in vals.keys() and vals['employee_ids']:
            if isinstance(vals['employee_ids'][0][2], dict):
                vals['employee_ids'] = [(4, emp_val[1]) for emp_val in vals['employee_ids']]
            else:
                vals['employee_ids'] = [(4, emp_id) for emp_id in vals['employee_ids'][0][2]]
 
        return super(HrPayslipEmployees, self).create(vals)

    def get_child_department_ids(self, department):
        # Хүүхэд хэлтсүүдийг нь олж өөртэй нь хамт буцаах
        department_ids = [department.id]
        parent_ids = [department.id]
        
        while parent_ids:
            child_deps = self.env['hr.department'].search([('parent_id', 'in', parent_ids)])
            parent_ids = child_deps.ids if child_deps else False
            department_ids.extend(child_deps.ids)
        
        return department_ids
    
    @api.multi
    def compute_sheet(self):
        payslips = self.env['hr.payslip']
        emp_pool = self.env['hr.employee']
        run_pool = self.env['hr.payslip.run']
        period_pool = self.env['account.period']
        slip_ids = []
        self.onchange_call_employees()
        [data] = self.read()
        active_id = self.env.context.get('active_id')
        if active_id:
            [run_data] = self.env['hr.payslip.run'].browse(active_id).read(['date_start', 'date_end', 'credit_note', 'period_id', \
                                                                            'bonus_salary', 'create_employee', 'journal_id', 'salary_type', 'credit_note'])

        from_date = run_data.get('date_start')
        to_date = run_data.get('date_end')
        period_id = run_data.get('period_id', False)
        if period_id:
            period_id = period_id[0]
        bonus_salary = run_data.get('bonus_salary', False)
        period = False
        if period_id:
            period = period_pool.search([('id', '=', period_id)])
            if period:
                period_name = period.name.split('/')
                from_date = period.date_start
                to_date = period.date_stop
        create_employee = run_data.get('create_employee', False)
        if create_employee:
            create_employee = create_employee[0]

        if period_id:
            credit_note = run_data.get('credit_note', False)
            salary_type = run_data.get('salary_type', False)
            journal_id = run_data.get('journal_id', False)
            if journal_id:
                journal_id = journal_id[0]
            if not data['employee_ids']:
                raise UserError(_("You must select employee(s) to generate payslip(s)."))
            check = self.check_payslip_employee_warnings(data['employee_ids'], from_date, to_date, salary_type, period)
            if check:
                for employee in self.env['hr.employee'].browse(data['employee_ids']):
                    employee_name = employee.name
                    if employee.last_name:
                        employee_name += ' %s' % (employee.last_name,)
                    if employee.job_id.name:
                        employee_name += ' %s' % (employee.job_id.name,)
                    slip_data = self.env['hr.payslip'].onchange_employee_id(from_date, to_date, salary_type, employee.id, contract_id=False)
                    slip_data_contract = self.env['hr.payslip'].onchange_contract_id(slip_data['value'].get('contract_id', False), from_date, period, salary_type)
                    slip_data_type = self.env['hr.payslip'].onchange_type_id(slip_data['value'].get('contract_id', False), employee, period, salary_type)

                    res = {
                        'employee_id': employee.id,
                        'name': slip_data['value'].get('name', False),
                        'department_id': slip_data['value'].get('department_id'),
                        'contract_id': slip_data['value'].get('contract_id', False),
                        'ndsh_type': slip_data['value'].get('ndsh_type'),
                        'struct_id': slip_data_type['value'].get('struct_id', False),
                        'payslip_run_id': self.env.context.get('active_id', False),
                        'input_line_ids': [(0, 0, x) for x in slip_data_contract['value'].get('input_line_ids', [])],
                        'worked_days_line_ids': [(0, 0, x) for x in slip_data_type['value'].get('worked_days_line_ids', [])],
                        'date_from': from_date,
                        'date_to': to_date,
                        'period_id': period_id,
                        'credit_note': credit_note,
                        'journal_id': journal_id,
                        'advance_salary_id': slip_data_type['value'].get('advance_salary_id', False),
                        'vacation_id': slip_data_type['value'].get('vacation_id', False),
                        'salary_type': salary_type,
                        'is_esti_adv_days': slip_data_type['value'].get('is_esti_adv_days'),
                        #   'is_office_employee': slip_data['value'].get('is_office_employee'),
                        'create_employee': create_employee,
                        'previous_salary_id': slip_data_type['value'].get('previous_salary_id', False),
                        'bonus_salary': bonus_salary,
                        'hchta_id':slip_data_type['value'].get('hchta_id', False),
                        'maternity_salary_id':slip_data_type['value'].get('maternity_salary_id', False),
                    }
                    if salary_type == 'vacation':
                        res['vacation_day'] = employee.annual_leave_days
                    if salary_type in ('last_salary', 'advance_salary'):
                        res['hour_balance_lines'] = [(4, x) for x in slip_data_type['value'].get('hour_balance_lines', [])]
                        if bonus_salary:
                            res['vacation_lines'] = [(0, 0, x) for x in slip_data_type['value'].get('vacation_lines', [])]
                    if salary_type in ('vacation', 'maternity', 'hchta'):
                        res['vacation_lines'] = [(0, 0, x) for x in slip_data_type['value'].get('vacation_lines', [])]
                    payslips += self.env['hr.payslip'].create(res)

            # "Цалин -> Цалингийн хуудсын багцууд" цэсний дэлгэцэнд Цалингийн тооцооллын нийт дүн таб шинээр нэмэгдсэн бөгөөд
            # тухайн таб дахь утгууд нь нийт цалингийн хуудсуудыг бодогдож дууссаны дараа үр дүнг нь нэгтгэх тул "payslips.compute_sheet()" гэсэн
            # кодыг коммент болгож дараах функцыг дуудаж ажиллуулав.
            self.compute_all_sheet(payslips.ids, active_id)
            # payslips.compute_sheet()

        else:
            raise UserError(_("Period not selected in Payslip. You must choose period first."))

        return {'type': 'ir.actions.act_window_close'}

    @api.onchange('call_employees')
    def onchange_call_employees(self):
        employee_ids = []
        payslip_id = self.env['hr.payslip.run'].browse(self.env.context.get('active_ids'))
        if self.call_employees:
            slip_employee_ids = []
            for slip_employee_id in payslip_id.slip_ids:
                slip_employee_ids.append(slip_employee_id.employee_id.id)
            if self.env.user.company_id and self.env.user.company_id.call_payroll_company:
                balance_ids = self.env['hour.balance'].search([('period_id', '=', payslip_id.period_id.id),
                                                               ('company_id', '=', payslip_id.company_id.id),
                                                               ('salary_type', '=', payslip_id.salary_type)])
                for balance in balance_ids:
                    for line in balance.balance_line_ids:
                        if line.employee_id.id not in slip_employee_ids:
                            employee_ids.append(line.employee_id.id)
            else:
                balance_ids = self.env['hour.balance'].search([('period_id', '=', payslip_id.period_id.id),
                                                               ('department_id', 'in', self.get_child_department_ids(payslip_id.department_id)),
                                                               ('salary_type', '=', payslip_id.salary_type)])
                for balance in balance_ids:
                    for line in balance.balance_line_ids.filtered(lambda x: x.employee_id and x.employee_id.id not in slip_employee_ids):
                        employee_ids.append(line.employee_id.id)
        self.employee_ids = employee_ids
