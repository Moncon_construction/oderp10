# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian HR Payroll Dashboard",
    'version': '1.0',
    'depends': [
        'l10n_mn_hr_payroll', 'l10n_mn_hr_dashboard',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
    Mongolian HR payroll Dashboard for employees
    """,
    'data': [
        'data/dashboard_data.xml',
        'views/hr_salary_rule_view.xml',
        'views/employee_salary_structure_dashboard.xml',
        'views/employee_salary_pie_dashboard.xml',
        'views/salary_assets_backend.xml',
        'security/ir.model.access.csv',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False,
    'qweb': [
        'static/src/xml/salary_info.xml',
    ],
}
