# -*- coding: utf-8 -*-

from odoo.exceptions import UserError
from odoo import fields, models, api
from odoo import _
import collections


class EmployeeSalaryPieDashboard(models.Model):

    _name = "employee.salary.pie.dashboard"
    _description = "Employee salary pie report"

    def _employee_domain(self):
        if self.env.user.has_group('hr.group_hr_user'):
            return []
        else:
            return [('user_id', '=', self.env.user.id)]

    def _period_domain(self):
        return [('company_id', '=', self.env.user.company_id.id)]

    def _set_default_employee(self):
        return self.env['hr.employee'].search([('user_id', '=', self.env.user.id)], limit=1)

    employee_id = fields.Many2one('hr.employee', string="Employee", domain=_employee_domain, default=_set_default_employee)
    start_period_id = fields.Many2one('account.period', string='Start Period', domain=_period_domain)

    @api.multi
    def compute_graph_data(self):
        categs = []
        series = []

        if not self.employee_id or not self. start_period_id:
            raise UserError(_("Please fill the required fields!"))

        if not self.employee_id.user_id == self.env.user and not self.env.user.has_group('hr.group_hr_user'):
            self.employee_id = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)], limit=1)
            if not self.employee_id:
                raise UserError(_("Please select correct employee"))

        periods = self.env['account.period'].search([('date_start', '=', self.start_period_id.date_start), ('company_id', '=', self.env.user.company_id.id)])
        payslips = self.env['hr.payslip'].sudo().search([('period_id', 'in', periods.ids), ('employee_id', '=', self.employee_id.id),('state', 'in', ('draft','done')),('salary_type','=','last_salary')])
        if not payslips:
            raise UserError(_("There is no payslip to see in pie dashboard in this period"))
        if len(payslips) != 1:
            raise UserError(_("There are more than one payslip in this period"))
        
        rule_total = self.env['hr.salary.rule'].sudo().search([('pie_dashboard', '=', 'total'), ('company_id', '=', self.env.user.company_id.id)], limit=1)
        rule_piece1 = self.env['hr.salary.rule'].sudo().search([('pie_dashboard', '=', 'piece1'), ('company_id', '=', self.env.user.company_id.id)], limit=1)
        rule_piece2 = self.env['hr.salary.rule'].sudo().search([('pie_dashboard', '=', 'piece2'), ('company_id', '=', self.env.user.company_id.id)], limit=1)
        rule_piece3 = self.env['hr.salary.rule'].sudo().search([('pie_dashboard', '=', 'piece3'), ('company_id', '=', self.env.user.company_id.id)], limit=1)
        rule_piece4 = self.env['hr.salary.rule'].sudo().search([('pie_dashboard', '=', 'piece4'), ('company_id', '=', self.env.user.company_id.id)], limit=1)
        if not rule_total:
            raise UserError(_("There is no salary rule to see in pie dashboard"))
        
        if not rule_piece1 and not rule_piece2:
            raise UserError(_("There is no salary rule to see in pie dashboard"))
        
        basic_salary = 0
        add1 = 0
        add2 = 0
        add3 = 0
        add4 = 0
        over_salary = 0

        for payslip in payslips:
            for payslip_line in payslip.line_ids:
                if payslip_line.salary_rule_id.id == rule_total.id:
                    basic_salary += payslip_line.amount
                if rule_piece1 and payslip_line.salary_rule_id.id == rule_piece1.id:
                    add1 += payslip_line.amount
                if rule_piece2 and payslip_line.salary_rule_id.id == rule_piece2.id:
                    add2 += payslip_line.amount
                if rule_piece3 and payslip_line.salary_rule_id.id == rule_piece3.id:
                    add3 += payslip_line.amount
                if rule_piece4 and payslip_line.salary_rule_id.id == rule_piece4.id:
                    add4 += payslip_line.amount
        if add1 > 0:
            over_salary = add1 - basic_salary
        data = []
        if over_salary > 0:
            if add1 and add2:
                data = [
                            [_('Remaining'),   basic_salary - add1],
                            {
                                'name': _('Performance Salary'),
                                'y': add1,
                                'sliced': 'true',
                                'selected': 'true',
                                'color':'yellow',
                            },
                            {
                                'name': _('Additional Salary'),
                                'y': add2,
                                'sliced': 'true',
                                'selected': 'true',
                                'color':'blue',
                            },
                            {
                                'name': _('Add Food'),
                                'y': add3,
                                'sliced': 'true',
                                'selected': 'true',
                                'color': 'orange',
                            },
                            {
                                'name': _('Over Time'),
                                'y': add4,
                                'sliced': 'true',
                                'selected': 'true',
                                'color': 'purple',
                            },
                            {
                                'name': _('Over Salary'),
                                'y': over_salary,
                                'sliced': 'true',
                                'selected': 'true',
                                'color':'green',
                            },
                        ]
            elif add1:
                data = [
                            [_('Remaining'),   basic_salary - add1],
                            {
                                'name': _('Performance Salary'),
                                'y': add1,
                                'sliced': 'true',
                                'selected': 'true',
                                'color':'yellow',
                            },
                            {
                                'name': _('Add Food'),
                                'y': add3,
                                'sliced': 'true',
                                'selected': 'true',
                                'color': 'orange',
                            },
                            {
                                'name': _('Over Time'),
                                'y': add4,
                                'sliced': 'true',
                                'selected': 'true',
                                'color': 'purple',
                            },
                            {
                                'name': _('Over Salary'),
                                'y': over_salary,
                                'sliced': 'true',
                                'selected': 'true',
                                'color':'green',
                            },
                        ]
            elif add2:
                data = [
                            [_('Remaining'),   basic_salary],
                            {
                                'name': _('Additional Salary'),
                                'y': add2,
                                'sliced': 'true',
                                'selected': 'true',
                                'color':'blue',
                            },
                            {
                                'name': _('Add Food'),
                                'y': add3,
                                'sliced': 'true',
                                'selected': 'true',
                                'color': 'orange',
                            },
                            {
                                'name': _('Over Time'),
                                'y': add4,
                                'sliced': 'true',
                                'selected': 'true',
                                'color': 'purple',
                            },
                            {
                                'name': _('Over Salary'),
                                'y': over_salary,
                                'sliced': 'true',
                                'selected': 'true',
                                'color':'green',
                            },
                        ]
        else:
            if add1 and add2:
                data = [
                            [_('Remaining'),   basic_salary - add1],
                            {
                                'name': _('Performance Salary'),
                                'y': add1,
                                'sliced': 'true',
                                'selected': 'true',
                                'color':'yellow',
                            },
                            {
                                'name': _('Additional Salary'),
                                'y': add2,
                                'sliced': 'true',
                                'selected': 'true',
                                'color':'blue',
                            },
                            {
                                'name': _('Add Food'),
                                'y': add3,
                                'sliced': 'true',
                                'selected': 'true',
                                'color': 'orange',
                            },
                            {
                                'name': _('Over Time'),
                                'y': add4,
                                'sliced': 'true',
                                'selected': 'true',
                                'color': 'purple',
                            },
                        ]
            elif add1:
                data = [
                            [_('Remaining'),   basic_salary - add1],
                            {
                                'name': _('Performance Salary'),
                                'y': add1,
                                'sliced': 'true',
                                'selected': 'true',
                                'color':'yellow',
                            },
                        ]
            elif add2:
                data = [
                            [_('Remaining'),   basic_salary],
                            {
                                'name': _('Additional Salary'),
                                'y': add2,
                                'sliced': 'true',
                                'selected': 'true',
                                'color':'blue',
                            },
                        ]
            
        return {
            'chart': {
                'type': 'pie'
            },
            'title': {
                'text': _('Salary Pie of employee :%s') % self.employee_id.name
            },
            'subtitle': {
                'text': ''
            },
            'plotOptions': {
                'pie': {
                    'allowPointSelect': 'true',
                    'cursor': 'pointer',
                    'dataLabels': {
                        'enabled': 'false',
                        'format': '<b>{point.name}</b>: {point.y: #,##.f} ₮',
                    },
                    'showInLegend': 'true'
                }
            },
            'series': [{
                'type': 'pie',
                'name': 'Salary Pie Chart',
                'data': data
            }],
        }

    def delete_used_datas(self):
        query = 'delete from employee_salary_pie_dashboard'
        self._cr.execute(query)
