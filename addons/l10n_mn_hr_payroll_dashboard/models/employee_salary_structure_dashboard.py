# -*- coding: utf-8 -*-

from odoo.exceptions import UserError
from odoo import fields, models, api
from odoo import _
import collections


class EmployeeSalaryDashboard(models.Model):

    _name = "employee.salary.structure.dashboard"
    _description = "Employee salary structure report"

    def _employee_domain(self):
        if self.env.user.has_group('hr.group_hr_user'):
            return []
        else:
            return [('user_id', '=', self.env.user.id)]

    def _period_domain(self):
        return [('company_id', '=', self.env.user.company_id.id)]

    def _set_default_employee(self):
        return self.env['hr.employee'].search([('user_id', '=', self.env.user.id)], limit=1)

    employee_id = fields.Many2one('hr.employee', string="Employee", domain=_employee_domain, default=_set_default_employee)
    start_period_id = fields.Many2one('account.period', string='Start Period', domain=_period_domain)
    end_period_id = fields.Many2one('account.period', string='End Period', domain=_period_domain)

    def show(self):
        self.end_period_id = False
        return

    @api.multi
    def compute_graph_data(self):
        categs = []
        series = []

        if not self.employee_id or not self. start_period_id or not self.end_period_id:
            raise UserError(_("Please fill the required fields!"))

        if self.start_period_id.date_start > self.end_period_id.date_start:
            self.end_period_id = False
            raise UserError(_("Start period and end period is not right"))
        if not self.employee_id.user_id == self.env.user and not self.env.user.has_group('hr.group_hr_user'):
            self.employee_id = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)], limit=1)
            if not self.employee_id:
                raise UserError(_("Please select correct employee"))

        periods = self.env['account.period'].search([('date_start', '>=', self.start_period_id.date_start), ('date_stop', '<=', self.end_period_id.date_stop), ('company_id', '=', self.env.user.company_id.id)])
        payslips = self.env['hr.payslip'].sudo().search([('period_id', 'in', periods.ids), ('employee_id', '=', self.employee_id.id)])
        rules = self.env['hr.salary.rule'].sudo().search([('see_in_employee_dashboard', '=', True)], order = 'sequence asc')
        if not rules:
            raise UserError(_(" There is no salary rule to see in dashboard"))
        for rule in rules:
            series_values = {'name': rule.name}
            arr = []
            for i in range(len(periods)):
                arr.append(0)
            series_values['data'] = arr
            series.append(series_values)
        salary_info = []
        salary_info_dict = {}
        for index, period in enumerate(periods, start=0):
            categs.append(period.name)
            for obj in payslips.filtered(lambda payslip: payslip.period_id.id == period.id):
                lines = obj.line_ids.filtered(lambda l: l.salary_rule_id.id in rules.ids).sorted(key='sequence')
                for line in lines:
                    for ser in series:
                        if ser['name'] == line.salary_rule_id.name:
                            ser['data'][index] += line.amount
                            break
                    if line.salary_rule_id.name in salary_info_dict:
                        salary_info_dict[line.salary_rule_id.name] += line.total
                        for a in salary_info:
                            if line.salary_rule_id.name in a:
                                a[line.salary_rule_id.name] += line.total
                    else:
                        salary_info_dict[line.salary_rule_id.name] = line.total
                        salary_info.append({line.salary_rule_id.name: line.total})
                        
        return {
            'chart': {
                'type': 'line'
            },
            'title': {
                'text': _('Salary breakdown of employee :%s') % self.employee_id.name
            },
            'subtitle': {
                'text': ''
            },
            'xAxis': {
                'categories': categs
            },
            'yAxis': {
                'title': {
                    'text': _('Salary')
                }
            },
            'plotOptions': {
                'line': {
                    'dataLabels': {
                        'enabled': 'true'
                    },
                    'enableMouseTracking': 'false'
                }
            },
            'series': series,
            'salary_info': salary_info
        }

    def delete_used_datas(self):
        query = 'delete from employee_salary_structure_dashboard'
        self._cr.execute(query)
