# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo.exceptions import UserError
from datetime import datetime, timedelta
import time
import babel
from odoo import api, fields, models, tools, _
from odoo import exceptions


class HrPayslipRun(models.Model):
    _inherit = "hr.salary.rule"

    see_in_employee_dashboard = fields.Boolean(string='See in employee dashboard')
    pie_dashboard = fields.Selection([('total', 'Total'), ('piece1', 'Piece1'), ('piece2', 'Piece2'),('piece3', 'Piece3'), ('piece4', 'Piece4')], string='Salary Pie Dashboard')