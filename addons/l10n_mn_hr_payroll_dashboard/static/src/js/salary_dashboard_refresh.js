odoo.define('l10n_mn_hr_payroll_dashboard.salary_dashboard_refresh', function (require) {
    "use strict";
    var core = require('web.core');
    var form_widget = require('web.form_widgets');
    var form_common = require('web.form_common');
    var formats = require('web.formats');
    var Model = require('web.Model');
    var QWeb = core.qweb;
    var _t = core._t;
    var this_ = this;
// ажилтын дашбоард зурах функц
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };
    function refresh_salary_dashboard(){
        if(this_.field_manager.datarecord.id){
            var model = new Model(this_.field_manager.model);
            var model_id = this_.field_manager.datarecord.id
            model.call("compute_graph_data",[model_id]).then(function(data){
                var salary_info_div = $('#salary_info');
                salary_info_div.empty();
                if(data){
                    var salary_infos = data.salary_info;
                    for (var i=0; i< salary_infos.length; i+=1) {
                    	for (var info in salary_infos[i]) {
                            salary_infos[i][info] = numberWithCommas(salary_infos[i][info].toFixed(2));
                    	}
                    }
                    salary_info_div.append($(QWeb.render('EmployeeSalaryInfo',{salarylines:salary_infos})));
                    var highchart = $('#employee_salary_dashboard_graph').highcharts(data);
                }
            });
        }
        else{
            setTimeout(refresh_salary_dashboard, 500); // check again in half second
        }
        return true;
    };
    form_widget.WidgetButton.include({
        on_click:function(node) {
            if(this.node.attrs.id === ('salary_dashboard_show')){
                setTimeout(refresh_salary_dashboard, 1000); // check after a second
                this._super();
                this_ = this;
            }
            else{
                this._super();
            }
        },
    });
});