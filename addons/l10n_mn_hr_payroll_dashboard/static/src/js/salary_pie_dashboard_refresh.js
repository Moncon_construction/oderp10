odoo.define('l10n_mn_hr_payroll_dashboard.salary_pie_dashboard_refresh', function (require) {
    "use strict";
    var core = require('web.core');
    var form_widget = require('web.form_widgets');
    var form_common = require('web.form_common');
    var formats = require('web.formats');
    var Model = require('web.Model');
    var QWeb = core.qweb;
    var _t = core._t;
    var this_ = this;
// ажилтын дашбоард зурах функц
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };
    function refresh_salary_pie_dashboard(){
        if(this_.field_manager.datarecord.id){
            var model = new Model(this_.field_manager.model);
            var model_id = this_.field_manager.datarecord.id
            model.call("compute_graph_data",[model_id]).then(function(data){
                if(data){
                    var highchart = $('#employee_salary_pie_dashboard_graph').highcharts(data);
                }
            });
        }
        else{
            setTimeout(refresh_salary_pie_dashboard, 500); // check again in half second
        }
        return true;
    };
    form_widget.WidgetButton.include({
        on_click:function(node) {
            if(this.node.attrs.id === ('salary_pie_dashboard_show')){
                setTimeout(refresh_salary_pie_dashboard, 1000); // check after a second
                this._super();
                this_ = this;
            }
            else{
                this._super();
            }
        },
    });
});