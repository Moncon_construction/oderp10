# -*- coding: utf-8 -*-
{
    'name': "Mongolian HR Payroll NDSH Report",
    'version': '1.0',
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
        Mongolian Hr Payroll Ndsh Report
    """,
    'depends': ['l10n_mn_hr_payroll'],
    'data': [
        'views/hr_payroll_ndsh_report_view.xml'
    ],
    'installable': True,
    'auto_install': False
}