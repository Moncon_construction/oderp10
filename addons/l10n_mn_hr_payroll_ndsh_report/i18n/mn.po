# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* l10n_mn_hr_payroll_ndsh_report
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 10.0+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-24 00:56+0000\n"
"PO-Revision-Date: 2019-05-24 00:56+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: l10n_mn_hr_payroll_ndsh_report
#: model:ir.ui.view,arch_db:l10n_mn_hr_payroll_ndsh_report.hr_payroll_ndsh_report_view
msgid "Cancel"
msgstr "Цуцлах"

#. module: l10n_mn_hr_payroll_ndsh_report
#: model:ir.model.fields,field_description:l10n_mn_hr_payroll_ndsh_report.field_hr_payroll_ndsh_report_company
msgid "Company"
msgstr "Байгууллагын нэр"

#. module: l10n_mn_hr_payroll_ndsh_report
#: model:ir.model.fields,field_description:l10n_mn_hr_payroll_ndsh_report.field_hr_payroll_ndsh_report_create_uid
msgid "Created by"
msgstr "Үүсгэгч"

#. module: l10n_mn_hr_payroll_ndsh_report
#: model:ir.model.fields,field_description:l10n_mn_hr_payroll_ndsh_report.field_hr_payroll_ndsh_report_create_date
msgid "Created on"
msgstr "Үүсгэсэн"

#. module: l10n_mn_hr_payroll_ndsh_report
#: model:ir.model.fields,field_description:l10n_mn_hr_payroll_ndsh_report.field_hr_payroll_ndsh_report_display_name
msgid "Display Name"
msgstr "Дэлгэцийн Нэр"

#. module: l10n_mn_hr_payroll_ndsh_report
#: model:ir.model.fields,field_description:l10n_mn_hr_payroll_ndsh_report.field_hr_payroll_ndsh_report_employees
#: model:ir.ui.view,arch_db:l10n_mn_hr_payroll_ndsh_report.hr_payroll_ndsh_report_view
msgid "Employees"
msgstr "Ажилтан"

#. module: l10n_mn_hr_payroll_ndsh_report
#: model:ir.model.fields,field_description:l10n_mn_hr_payroll_ndsh_report.field_hr_payroll_ndsh_report_end_period
msgid "End Period"
msgstr "Дуусах мөчлөг"

#. module: l10n_mn_hr_payroll_ndsh_report
#: code:addons/l10n_mn_hr_payroll_ndsh_report/models/hr_payroll_ndsh_report.py:68
#, python-format
msgid "End Period must be older than Start Period!!!"
msgstr "Дуусах мөчлөг нь эхлэх мөчлөгөөс их байх ёстой!!!"

#. module: l10n_mn_hr_payroll_ndsh_report
#: model:ir.model.fields,field_description:l10n_mn_hr_payroll_ndsh_report.field_hr_payroll_ndsh_report_group_by_department
msgid "Group by department"
msgstr "Хэлтэсээр бүлэглэх"

#. module: l10n_mn_hr_payroll_ndsh_report
#: code:addons/l10n_mn_hr_payroll_ndsh_report/models/hr_payroll_ndsh_report.py:271
#, python-format
msgid "Hr Ndsh Report"
msgstr "НДШ ногдуулалт"

#. module: l10n_mn_hr_payroll_ndsh_report
#: model:ir.actions.act_window,name:l10n_mn_hr_payroll_ndsh_report.action_hr_payroll_ndsh_report_menu
#: model:ir.ui.menu,name:l10n_mn_hr_payroll_ndsh_report.hr_payroll_ndsh_report_menu
msgid "Hr Payroll NDSH Report"
msgstr "НДШ ногдуулалт"

#. module: l10n_mn_hr_payroll_ndsh_report
#: model:ir.model,name:l10n_mn_hr_payroll_ndsh_report.model_hr_payroll_ndsh_report
#: model:ir.ui.view,arch_db:l10n_mn_hr_payroll_ndsh_report.hr_payroll_ndsh_report_view
msgid "Hr Payroll Ndsh Report"
msgstr "НДШ ногдуулалт"

#. module: l10n_mn_hr_payroll_ndsh_report
#: model:ir.model.fields,field_description:l10n_mn_hr_payroll_ndsh_report.field_hr_payroll_ndsh_report_payslip_runs
msgid "Hr Payslip Run"
msgstr "Цалингийн хуудсын багцууд"

#. module: l10n_mn_hr_payroll_ndsh_report
#: model:ir.model.fields,field_description:l10n_mn_hr_payroll_ndsh_report.field_hr_payroll_ndsh_report_id
msgid "ID"
msgstr "ID"

#. module: l10n_mn_hr_payroll_ndsh_report
#: model:ir.model.fields,field_description:l10n_mn_hr_payroll_ndsh_report.field_hr_payroll_ndsh_report___last_update
msgid "Last Modified on"
msgstr "Сүүлийн засвар хийсэн огноо"

#. module: l10n_mn_hr_payroll_ndsh_report
#: model:ir.model.fields,field_description:l10n_mn_hr_payroll_ndsh_report.field_hr_payroll_ndsh_report_salary_type
msgid "Last Salary"
msgstr "Сүүл цалин"

#. module: l10n_mn_hr_payroll_ndsh_report
#: model:ir.model.fields,field_description:l10n_mn_hr_payroll_ndsh_report.field_hr_payroll_ndsh_report_write_uid
msgid "Last Updated by"
msgstr "Сүүлийн засвар хийсэн"

#. module: l10n_mn_hr_payroll_ndsh_report
#: model:ir.model.fields,field_description:l10n_mn_hr_payroll_ndsh_report.field_hr_payroll_ndsh_report_write_date
msgid "Last Updated on"
msgstr "Сүүлийн засвар хийсэн огноо"

#. module: l10n_mn_hr_payroll_ndsh_report
#: model:ir.ui.view,arch_db:l10n_mn_hr_payroll_ndsh_report.hr_payroll_ndsh_report_view
msgid "Payslip Runs"
msgstr "Цалингийн хуудсын багцууд"

#. module: l10n_mn_hr_payroll_ndsh_report
#: model:ir.ui.view,arch_db:l10n_mn_hr_payroll_ndsh_report.hr_payroll_ndsh_report_view
msgid "Print"
msgstr "Хэвлэх"

#. module: l10n_mn_hr_payroll_ndsh_report
#: code:addons/l10n_mn_hr_payroll_ndsh_report/models/hr_payroll_ndsh_report.py:416
#, python-format
msgid "Printed Date:"
msgstr "Хэвлэсэн огноо:"

#. module: l10n_mn_hr_payroll_ndsh_report
#: model:ir.model.fields,field_description:l10n_mn_hr_payroll_ndsh_report.field_hr_payroll_ndsh_report_sal_rule_categs
msgid "Salary Rule Categories"
msgstr "Цалингийн дүрмийн ангилалууд"

#. module: l10n_mn_hr_payroll_ndsh_report
#: model:ir.model.fields,field_description:l10n_mn_hr_payroll_ndsh_report.field_hr_payroll_ndsh_report_salary_type
msgid "Salary Type"
msgstr "Цалингийн төрөл"

#. module: l10n_mn_hr_payroll_ndsh_report
#: model:ir.model.fields,field_description:l10n_mn_hr_payroll_ndsh_report.field_hr_payroll_ndsh_report_start_period
msgid "Start Period"
msgstr "Эхлэх мөчлөг"

