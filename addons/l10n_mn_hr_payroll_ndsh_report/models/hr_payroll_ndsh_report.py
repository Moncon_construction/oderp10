# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from io import BytesIO
import base64
import xlsxwriter
from odoo import api, fields, models, _
from odoo.exceptions import UserError
import time
from datetime import datetime, date

class HrPayrollNdshReport(models.TransientModel):
    """
        НДШ ногдуулалтын тайлан
    """

    _name = 'hr.payroll.ndsh.report'
    _description = 'Hr Payroll Ndsh Report'

    SALARY_TYPE = [
        ('last_salary', 'Last salary'),
    ]

    company = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    start_period = fields.Many2one('account.period', string='Start Period', required=True)
    end_period = fields.Many2one('account.period', string='End Period', required=True)
    group_by_department = fields.Boolean('Group by department', default=True)
    payslip_runs = fields.Many2many('hr.payslip.run', 'hr_payroll_ndsh_report_payslip_rel', 'report_id', 'pslip_run_id', string='Hr Payslip Run')
    employees = fields.Many2many('hr.employee', 'hr_payroll_ndsh_report_emp_rel', 'report_id', 'emp_id', string='Employees')
    sal_rule_categs = fields.Many2many('hr.salary.rule.category', 'hr_payroll_ndsh_report_sal_rule_cat_rel', 'report_id', 'rule_categ_id', string='Salary Rule Categories')

    @api.onchange('company')
    def onchange_company(self):
        domain = {}
        domain['payslip_run'] = [('company_id', '=', self.company.id), ('state', '=', 'done')]
        domain['start_period'] = [('company_id', '=', self.company.id)]
        domain['end_period'] = [('company_id', '=', self.company.id)]
        domain['employees'] = [('company_id', '=', self.company.id)]
        return {'domain': domain}

    @api.onchange('start_period', 'end_period')
    def onchange_period(self):
        domain = {}
        domain['payslip_runs'] = [
             ('company_id', '=', self.company.id),
             ('state', '=', 'done'),
             ('period_id.date_start', '>=', self.start_period.date_start),
             ('period_id.date_start', '<=', self.end_period.date_start)]

        return {'domain': domain}

    @api.onchange('company', 'payslip_runs', 'employees', 'start_period', 'end_period')
    def _get_sal_rule_categs_domain(self):
        domain = {}
        domain['sal_rule_categs'] = [('id', 'in', self.get_rule_cat_ids())]
        return {'domain': domain}

    @api.onchange('start_period', 'end_period')
    def check_periods(self):
        for obj in self:
            if obj.start_period and obj.end_period:
                if obj.start_period.date_start > obj.end_period.date_start:
                    raise UserError(_(u"End Period must be older than Start Period!!!"))

    def get_rule_cat_ids(self):
        employee_qry = " emp.id IN (" + str(self.employees.ids)[1:len(str(self.employees.ids)) - 1] + ") " if str(
            self.employees.ids) != "[]" else ""

        payslip_run_qry = "AND (hp.payslip_run_id IN (" + str(self.payslip_runs.ids)[1:len(
            str(self.payslip_runs.ids)) - 1] + ") AND prun.state = 'done') " if str(
            self.payslip_runs.ids) != "[]" else ""

        if employee_qry and payslip_run_qry:
            payslip_run_qry = "AND ( %s OR (hp.payslip_run_id IN (" % employee_qry + str(self.payslip_runs.ids)[1:len(
                str(self.payslip_runs.ids)) - 1] + ") AND prun.state = 'done') ) " if str(
                self.payslip_runs.ids) != "[]" else ""
        elif employee_qry and not payslip_run_qry:
            payslip_run_qry = " AND %s" % employee_qry

        period_qry = " AND (ap.date_start >= '%s') AND (ap.date_start <= '%s')" % (
        self.start_period.date_start, self.end_period.date_start) if self.start_period and self.end_period else ""

        qry = """
            SELECT rule_ctg.id FROM hr_payslip_line hpl
            LEFT JOIN hr_payslip hp ON hp.id = hpl.slip_id
            LEFT JOIN hr_employee emp ON emp.id = hp.employee_id
            LEFT JOIN account_period ap ON ap.id = hp.period_id
            LEFT JOIN hr_payslip_run prun ON prun.id = hp.payslip_run_id
            LEFT JOIN hr_salary_rule_category rule_ctg ON rule_ctg.id = hpl.category_id
            WHERE hp.company_id = %s
                %s
                AND LOWER(hp.salary_type) = '%s'
                AND LOWER(rule_ctg.salary_type) = '%s'
                %s
                GROUP BY rule_ctg.id
        """ % (self.company.id, period_qry, 'last_salary', 'last_salary', payslip_run_qry)

        self._cr.execute(qry)
        results = self._cr.dictfetchall()

        if results:
            return [result['id'] for result in results]
        else:
            return []

    def get_xsl_column_name(self, index):
        alphabet = {'0': 'A', '1': 'B', '2': 'C', '3': 'D', '4': 'E',
                    '5': 'F', '6': 'G', '7': 'H', '8': 'I', '9': 'J',
                    '10': 'K', '11': 'L', '12': 'M', '13': 'N', '14': 'O',
                    '15': 'P', '16': 'Q', '17': 'R', '18': 'S', '19': 'T',
                    '20': 'U', '21': 'V', '22': 'W', '23': 'X', '24': 'Y', '25': 'Z'}

        if index <= 25:
            return (alphabet[str(index)] + ":" + alphabet[str(index)])
        else:
            return (alphabet[str(index / 26 - 1)] + alphabet[str(index % 26)] + ":" + alphabet[str(index / 26 - 1)] +
                    alphabet[str(index % 26)])

    def get_column_name_for_calculate(self, index):
        column_name = self.get_xsl_column_name(index).split(':')[0]
        return column_name

    def get_arithmetic_formula(self, f_coly, f_rowx, s_coly, s_rowx, oprtr):
        f_cell_index = self.get_column_name_for_calculate(f_coly) + str(f_rowx)
        s_cell_index = self.get_column_name_for_calculate(s_coly) + str(s_rowx)
        return f_cell_index + oprtr + s_cell_index

    def _get_lines(self):
        rule_ids = self.get_rule_cat_ids()
        sal_rule_categ_qry = ""
        period_qry = """ AND (period.date_start >= '%s') 
                         AND (period.date_start <= '%s')
                     """ % (self.start_period.date_start, self.end_period.date_start)

        employee_qry = " emp.id IN (" + str(self.employees.ids)[1:len(str(self.employees.ids)) - 1] + ") " if str(
            self.employees.ids) != "[]" else ""

        payslip_run_qry = "AND (hp.payslip_run_id IN (" + str(self.payslip_runs.ids)[1:len(
            str(self.payslip_runs.ids)) - 1] + ") AND prun.state = 'done') " if str(
            self.payslip_runs.ids) != "[]" else ""
        if employee_qry and payslip_run_qry:
            payslip_run_qry = "AND ( %s OR (hp.payslip_run_id IN (" % employee_qry + str(self.payslip_runs.ids)[1:len(
                str(self.payslip_runs.ids)) - 1] + ") AND prun.state = 'done') ) " if str(
                self.payslip_runs.ids) != "[]" else ""
        elif employee_qry and not payslip_run_qry:
            payslip_run_qry = " AND %s" % employee_qry

        order_by_dep = ""
        if self.group_by_department:
            order_by_dep = "dep.name, dep.id,"

        qry = """
                SELECT  dep.id as dep_id, dep.name as dep_name, 
                          emp.id as emp_id, emp.ssnid as emp_register,emp.sinid as emp_sinid, emp.work_email as emp_email, emp.mobile_phone as emp_phone,
                          emp.name_related as emp_name, emp.last_name as emp_lname,
                          period.id as period_id, period.name as period_name, occ.code as occ_code, ndsh.code as ndsh_code,
                          srule.social_insurance_type as social_insurance_type,
                          table1.salary_income_total as salary_income_total,
                          table1.insurance_company_total as insurance_company_total,
                          table1.insurance_employee_total as insurance_employee_total
                      FROM
                      (
                          SELECT hp.id as hp_id, dep.id as dep_id,
                              emp.id as emp_id,
                              period.id as period_id,
                              srule_categ.id as srule_categ_id, srule.id as srule_id,
                              SUM(table2.salary_income_total) as salary_income_total,
                              SUM(table2.insurance_company_total) as insurance_company_total,
                              SUM(table2.insurance_employee_total) as insurance_employee_total
                          FROM
                          (
                          SELECT hp.id as hp_id, 
                              CASE 
                                WHEN srule.social_insurance_type = 'salary_income'
                                    THEN total ELSE 0 END AS salary_income_total,
                              CASE
                                WHEN srule.social_insurance_type = 'insurance_company'
                                    THEN total ELSE 0 END AS insurance_company_total,
                              CASE
                                WHEN srule.social_insurance_type = 'insurance_employee'
                                    THEN total ELSE 0 END AS insurance_employee_total 
                          FROM hr_payslip hp 
                          LEFT JOIN hr_payslip_line hpl ON hpl.slip_id = hp.id
                          LEFT JOIN hr_salary_rule srule ON srule.id = hpl.salary_rule_id
                          LEFT JOIN account_period period ON period.id = hp.period_id
                          ) table2
                          LEFT JOIN hr_payslip hp ON hp.id = table2.hp_id
                          LEFT JOIN hr_employee emp ON emp.id = hp.employee_id
                          LEFT JOIN hr_department dep ON dep.id = hp.department_id
                          LEFT JOIN account_period period ON period.id = hp.period_id
                          LEFT JOIN hr_payslip_line hpl ON hpl.slip_id = hp.id
                          LEFT JOIN hr_salary_rule srule ON srule.id = hpl.salary_rule_id
                          LEFT JOIN hr_salary_rule_category srule_categ ON srule_categ.id = srule.category_id
                          LEFT JOIN hr_payslip_run prun ON prun.id = hp.payslip_run_id
                          WHERE hp.company_id = %s AND hp.state = 'done'
                              %s 
                              %s
                              %s
                              AND LOWER(hp.salary_type) = '%s'
                              AND LOWER(srule_categ.salary_type) = '%s'
                          GROUP BY dep.id, emp.id, period.id, srule_categ.id, srule_id, hp.id
                      ) table1
                      LEFT JOIN hr_employee emp ON emp.id = table1.emp_id
                      LEFT JOIN hr_department dep ON dep.id = table1.dep_id
                      LEFT JOIN hr_skill_level lvl ON lvl.id = emp.skill_level
                      LEFT JOIN hr_job job ON job.id = emp.job_id
                      LEFT JOIN hr_occupation occ ON occ.id = job.occupation_id
                      LEFT JOIN account_period period ON period.id = table1.period_id
                      LEFT JOIN hr_payslip hp ON hp.id = table1.hp_id
                      LEFT JOIN hr_ndsh_config ndsh ON ndsh.id = hp.ndsh_type
                      LEFT JOIN hr_salary_rule srule  ON  srule.id = table1.srule_id
                      LEFT JOIN hr_salary_rule_category srule_categ ON srule_categ.id = table1.srule_categ_id
                      ORDER BY %s emp.name_related, emp.ssnid, emp.last_name, emp.id, period.date_start, period.date_stop, period.id, srule_categ.sequence
        """% (self.company.id,
                         sal_rule_categ_qry, period_qry, payslip_run_qry, 'last_salary',
                         'last_salary',
                         order_by_dep)
        self._cr.execute(qry)
        results = self._cr.dictfetchall()
        return results if results else False

    def get_sum_formula_from_list(self, colx, rows):
        formula = "="
        for i in range(len(rows)):
            formula += ("+" + self.get_column_name_for_calculate(colx) + str(rows[i] + 1))
        return formula

    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _(u"Hr Ndsh Report")
        file_name = "%s_%s.xlsx" % (report_name, time.strftime('%Y%m%d_%H%M'))

        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(
            filename_prefix=('hr_ndsh_report'), form_title=file_name).create({})

        # create formats
        format_text_left = {
            'font_name': 'Times New Roman',
            'font_size': 10,
            'align': 'left',
        }

        format_text_right = format_text_left.copy()
        format_text_right['align'] = 'right'

        format_text_right_bold = format_text_right.copy()
        format_text_right_bold['bold'] = True

        format_text_left_bold = format_text_left.copy()
        format_text_left_bold['bold'] = True

        format_text_center_header = format_text_left_bold.copy()
        format_text_center_header['font_size'] = '12'
        format_text_center_header['align'] = 'center'

        format_text_left_bordered = format_text_left.copy()
        format_text_left_bordered['border'] = 1

        format_text_left_bordered_red = format_text_left_bordered.copy()
        format_text_left_bordered_red['color'] = '#ed1c24'

        format_number_right_bordered = format_text_left_bordered.copy()
        format_number_right_bordered['num_format'] = '_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)'
        format_number_right_bordered['align'] = 'right'

        format_dd_center_bordered = format_number_right_bordered.copy()
        format_dd_center_bordered['align'] = 'center'
        format_dd_center_bordered['num_format'] = '#,##0_);(#,##0)'

        format_dd_right_bordered_colored = format_dd_center_bordered.copy()
        format_dd_right_bordered_colored['bg_color'] = '#ccffff'

        format_column_header = format_text_left_bordered.copy()
        format_column_header['bold'] = True
        format_column_header['bg_color'] = '#99ccff'
        format_column_header['text_wrap'] = 1
        format_column_header['align'] = 'center'

        format_column_header_red = format_column_header.copy()
        format_column_header_red['color'] = '#ed1c24'

        format_column_footer_text_right = format_column_header.copy()
        format_column_footer_text_right['align'] = 'right'

        format_column_footer_number = format_column_header.copy()
        format_column_footer_number['num_format'] = '_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)'
        format_column_footer_number['align'] = 'right'
        format_column_footer_number['font_size'] = '11'

        format_column_subh_text = format_column_header.copy()
        format_column_subh_text['bg_color'] = '#ccffff'
        format_column_subh_text['align'] = 'right'

        format_column_subh_number = format_column_subh_text.copy()
        format_column_subh_number['num_format'] = '_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)'
        format_column_subh_number['align'] = 'right'
        format_column_subh_number['font_size'] = '11'

        format_column_subh_dd_right_bordered_colored = format_dd_right_bordered_colored.copy()
        format_column_subh_dd_right_bordered_colored['bold'] = True
        format_column_subh_dd_right_bordered_colored['font_size'] = '10'
        format_column_subh_dd_right_bordered_colored['bg_color'] = '#ffe5ca'

        format_column_subh_subh_text_left = format_column_header.copy()
        format_column_subh_subh_text_left['bg_color'] = '#ffe5ca'
        format_column_subh_subh_text_left['align'] = 'left'
        format_column_subh_subh_text_left['text_wrap'] = 0

        format_column_subh_subh_text_center = format_column_subh_subh_text_left.copy()
        format_column_subh_subh_text_center['align'] = 'center'
        format_column_subh_subh_text_center['color'] = '#ed1c24'

        format_column_subh_subh_number_right = format_number_right_bordered.copy()
        format_column_subh_subh_number_right['bg_color'] = '#ffe5ca'
        format_column_subh_number['font_size'] = '10'

        format_text_center_header = book.add_format(format_text_center_header)
        format_text_right = book.add_format(format_text_right)
        format_text_right_bold = book.add_format(format_text_right_bold)
        format_text_left_bordered = book.add_format(format_text_left_bordered)
        format_text_left_bold = book.add_format(format_text_left_bold)
        format_number_right_bordered = book.add_format(format_number_right_bordered)
        format_dd_center_bordered = book.add_format(format_dd_center_bordered)
        format_column_header = book.add_format(format_column_header)
        format_column_header.set_align('vcenter')
        format_column_header_red = book.add_format(format_column_header_red)
        format_column_header_red.set_align('vcenter')
        format_column_footer_text_right = book.add_format(format_column_footer_text_right)
        format_column_footer_number = book.add_format(format_column_footer_number)
        format_column_subh_text = book.add_format(format_column_subh_text)
        format_column_subh_number = book.add_format(format_column_subh_number)
        format_column_subh_dd_right_bordered_colored = book.add_format(format_column_subh_dd_right_bordered_colored)
        format_column_subh_subh_text_left = book.add_format(format_column_subh_subh_text_left)
        format_column_subh_subh_number_right = book.add_format(format_column_subh_subh_number_right)

        # create sheet
        sheet = book.add_worksheet(u'НДШ ногдуулалт')
        sheet.set_portrait()
        sheet.set_zoom(100)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})

        dynamic_col_count = 0
        dynamic_col_ids = [rule.id for rule in self.sal_rule_categs]

        for i in range(dynamic_col_count):
            dynamic_col_indexes_dict[str(dynamic_col_ids[i])] = i

        static_col_count = 14

        rowx = 1

        # build dynamic columns
        sheet.set_column(self.get_xsl_column_name(0), 4)
        sheet.set_column(self.get_xsl_column_name(1), 8)
        sheet.set_column(self.get_xsl_column_name(2), 12)
        sheet.set_column(self.get_xsl_column_name(3), 12)
        sheet.set_column(self.get_xsl_column_name(4), 10)
        sheet.set_column(self.get_xsl_column_name(5), 8)
        sheet.set_column(self.get_xsl_column_name(6), 10)
        sheet.set_column(self.get_xsl_column_name(7), 12)
        sheet.set_column(self.get_xsl_column_name(8), 12)
        sheet.set_column(self.get_xsl_column_name(9), 12)
        sheet.set_column(self.get_xsl_column_name(10), 12)
        sheet.set_column(self.get_xsl_column_name(11), 10)
        sheet.set_column(self.get_xsl_column_name(12), 10)
        sheet.set_column(self.get_xsl_column_name(13), 5)
        sheet.set_column(self.get_xsl_column_name(14), 5)
        sheet.set_column(self.get_xsl_column_name(15), 8)

        sheet.set_row(7, 35)

        sheet.merge_range(rowx, 0, rowx, 4, u'Баталсан:..................................../%s/' % self.company.first_sign.name, format_text_left_bold)
        sheet.merge_range(rowx + 1, 5, rowx + 1, 9, u'Нийгмийн даатгалын шимтгэл ногдуулалт', format_text_center_header)
        sheet.write(rowx + 3, static_col_count + dynamic_col_count , _(u'Printed Date:'), format_text_right_bold)
        sheet.write(rowx + 3, static_col_count + dynamic_col_count+1, u'%s' % datetime.now().strftime('%Y-%m-%d'),
                    format_text_right)

        # build column headers
        rowx += 4
        coly = 0
        sheet.merge_range(rowx, coly, rowx + 2, coly, u'№', format_column_header)
        sheet.merge_range(rowx, coly + 1, rowx, coly + 7, u'Даатгуулагчийн', format_column_header)
        sheet.merge_range(rowx, coly + 8, rowx, coly + 10, u'Ноогдуулсан шимтгэл(төгрөг)', format_column_header)
        sheet.merge_range(rowx, coly + 11, rowx, coly + 15, u'Даатгуулагчийн', format_column_header)
        sheet.merge_range(rowx + 1, coly + 1, rowx + 2, coly + 1, u'Он сар', format_column_header)
        sheet.merge_range(rowx + 1, coly + 2, rowx + 2, coly + 2, u'Овог', format_column_header)
        sheet.merge_range(rowx + 1, coly + 3, rowx + 2, coly + 3, u'Нэр', format_column_header)
        sheet.merge_range(rowx + 1, coly + 4, rowx + 2, coly + 4, u'Регистрийн дугаар', format_column_header)
        sheet.merge_range(rowx + 1, coly + 5, rowx + 2, coly + 5, u'НД-ын дэвтрийн дугаар', format_column_header)
        sheet.merge_range(rowx + 1, coly + 6, rowx + 2, coly + 6, u'Даатгуулагчийн төрөл', format_column_header)
        sheet.merge_range(rowx + 1, coly + 7, rowx + 2, coly + 7, u'Хөдөлмөрийн хөлс, түүнтэй адилтгах орлого /төгрөг/',
                          format_column_header)
        sheet.merge_range(rowx + 1, coly + 8, rowx + 2, coly + 8, u'Нийт дүн', format_column_header)
        sheet.merge_range(rowx + 1, coly + 9, rowx + 2, coly + 9, u'Ажил олгогч', format_column_header)
        sheet.merge_range(rowx + 1, coly + 10, rowx + 2, coly + 10, u'Даатгуулагч', format_column_header)
        sheet.merge_range(rowx + 1, coly + 11, rowx + 2, coly + 11, u'Ажил мэргэжлийн ангилал', format_column_header)
        sheet.merge_range(rowx + 1, coly + 12, rowx + 2, coly + 12, u'Утас', format_column_header)
        sheet.merge_range(rowx + 1, coly + 13, rowx + 2, coly + 15, u'И-мэйл хаяг', format_column_header)
        coly += static_col_count

        for i in range(dynamic_col_count):
            sheet.merge_range(rowx, coly + i, rowx + 2, coly + i, dynamic_col_names[i], format_column_header)

        results = self._get_lines()

        if results:
            all_emp_ids_for_count = []
            all_emp_count = 0

            written_dep_ids = []
            written_emp_ids = []
            written_period_ids = []

            rowx += 2

            period_count = 0
            emp_count = 0

            level1_rows = []
            level2_rows = []
            level3_rows = []

            last_emp_rowx = -1
            last_emp_period_rowxs = []
            last_dep_rowx = -1
            last_dep_emp_rowxs = []
            all_emp_rowxs = []
            all_dep_rowxs = []
            all_period_rowxs = []

            for result in results:
                if self.group_by_department and result['dep_id'] not in written_dep_ids:
                    # Хэлтэсийн багануудад томъёо оруулах
                    if last_dep_rowx != -1:
                        for i in range(7, 11, 1):
                            sheet.write(last_dep_rowx, i,
                                        self.get_sum_formula_from_list(i, last_dep_emp_rowxs),
                                        format_column_subh_number)

                    # start /one row/ -------- build department's row
                    coly = 0
                    rowx += 1
                    last_dep_emp_rowxs = []
                    written_emp_ids = []

                    sheet.merge_range(rowx, 0, rowx, 6, result['dep_name'] if result['dep_name'] else '', format_column_subh_text)
                    sheet.merge_range(rowx, 11, rowx, 15, '', format_column_subh_text)

                    last_dep_rowx = rowx
                    all_dep_rowxs.append(rowx)
                    level1_rows.append(rowx)
                    coly += static_col_count

                    written_dep_ids.append(result['dep_id'] if result['dep_id'] else -1)

                    # end /one row/ -------- build department's row

                if result['emp_id'] not in written_emp_ids:
                    if last_emp_rowx != -1:
                        for i in range(7, 11, 1):
                            sheet.write(last_emp_rowx, i, self.get_sum_formula_from_list(i, last_emp_period_rowxs), format_column_subh_subh_number_right)

                    all_emp_ids_for_count.append(result['emp_id'])
                    all_emp_count += 1

                    # build employee's row
                    coly = 0
                    rowx += 1
                    written_period_ids = []
                    last_emp_period_rowxs = []
                    emp_count += 1

                    sheet.write(rowx, 0, emp_count, format_column_subh_dd_right_bordered_colored)
                    sheet.merge_range(rowx, 1, rowx, 6, '', format_column_subh_subh_text_left)
                    sheet.merge_range(rowx, 11, rowx, 15, '', format_column_subh_subh_text_left)

                    last_emp_rowx = rowx
                    last_dep_emp_rowxs.append(rowx)
                    all_emp_rowxs.append(rowx)
                    level2_rows.append(rowx)

                    coly += static_col_count

                    written_emp_ids.append(result['emp_id'] if result['emp_id'] else -1)

                if result['period_id'] not in written_period_ids:
                    coly = 0
                    rowx += 1
                    period_count += 1
                    all_period_rowxs.append(rowx)
                    sheet.write(rowx, coly, period_count, format_dd_center_bordered)
                    sheet.write(rowx, coly + 1, result['period_name'] if result['period_name'] else '',format_text_left_bordered)
                    sheet.write(rowx, coly + 2, result['emp_lname'] if result['emp_lname'] else '', format_text_left_bordered)
                    sheet.write(rowx, coly + 3, result['emp_name'] if result['emp_name'] else '',format_text_left_bordered)
                    sheet.write(rowx, coly + 4, result['emp_register'] if result['emp_register'] else '', format_text_left_bordered)
                    sheet.write(rowx, coly + 5, result['emp_sinid'] if result['emp_sinid'] else '',format_text_left_bordered)
                    sheet.write(rowx, coly + 6, result['ndsh_code'] if result['ndsh_code'] else '',format_dd_center_bordered)
                    sheet.write(rowx, coly + 7, result['salary_income_total'] if result['salary_income_total'] else '', format_number_right_bordered)
                    sheet.write_formula(rowx, coly + 8, self.get_arithmetic_formula(coly + 9, rowx+1, coly + 10, rowx+1, '+'), format_number_right_bordered)
                    sheet.write(rowx, coly + 9, result['insurance_company_total'] if result['insurance_company_total'] else '', format_number_right_bordered)
                    sheet.write(rowx, coly + 10, result['insurance_employee_total'] if result['insurance_employee_total'] else '', format_number_right_bordered)
                    sheet.write(rowx, coly + 11, result['occ_code'] if result['occ_code'] else '',format_dd_center_bordered)
                    sheet.write(rowx, coly + 12, result['emp_phone'] if result['emp_phone'] else '',format_text_left_bordered)
                    sheet.merge_range(rowx, coly + 13, rowx, coly + 15,result['emp_email'] if result['emp_email'] else '', format_text_left_bordered)

                    level3_rows.append(rowx)
                    last_emp_period_rowxs.append(rowx)
                    coly += 1
                    written_period_ids.append(result['period_id'] or -1)

                if results[-1] == result:
                    if last_dep_rowx != -1:
                        for i in range(7, 11, 1):
                            sheet.write(last_dep_rowx, i,
                                        self.get_sum_formula_from_list(i, last_dep_emp_rowxs),
                                        format_column_subh_number)

                    if last_emp_rowx != -1:
                        for i in range(7, 11, 1):
                            sheet.write(last_emp_rowx, i, self.get_sum_formula_from_list(i, last_emp_period_rowxs), format_column_subh_subh_number_right)

            # build table footer
            rowx += 1
            coly = 0
            sheet.write(rowx, coly + 4, '', format_column_footer_text_right)
            sheet.merge_range(rowx, coly, rowx, coly + 6, u'Нийт дүн', format_column_footer_text_right)
            sheet.merge_range(rowx, 11, rowx, 15, '', format_column_footer_text_right)
            for i in range(7, 11, 1):
                if self.group_by_department:
                    sheet.write(rowx, i, self.get_sum_formula_from_list(i, all_dep_rowxs), format_column_footer_number)
                else:
                    sheet.write(rowx, i, self.get_sum_formula_from_list(i, all_emp_rowxs), format_column_footer_number)

            rowx +=3
            sheet.merge_range(rowx, 5, rowx, 10,u'Дарга/Захирал:......................................................................../%s/' % self.company.genaral_accountant_signature.name,format_text_left_bold)
            sheet.merge_range(rowx+1, 5, rowx+1, 10,u'Нягтлан бодогч:..................................................................../%s/' % self.company.accountant_signature.name,format_text_left_bold)

        book.close()

        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report()

