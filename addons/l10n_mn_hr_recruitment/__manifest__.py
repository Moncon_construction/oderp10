# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian HR Recruitment",
    'description': """
       Human Resource Additional Features of Recruitment
    """,
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'version': '1.0',
    'depends': [
        'hr',
        'hr_recruitment',
        'l10n_mn_hr',
        'website_form',
        'website_hr_recruitment',
        'hr_recruitment_survey',
        'l10n_mn_hr_regulation'
    ],
    'data': [
        'security/ir.model.access.csv',
        'data/hr_applicant_data.xml',
        'views/hr_applicant_additional_views.xml',
        'views/hr_cv_settings.xml',
        'views/hr_applicant_view.xml',
        'views/hr_applicant_templates_view.xml',
        'views/hr_job.xml',
    ],
    'installable': True,
}
