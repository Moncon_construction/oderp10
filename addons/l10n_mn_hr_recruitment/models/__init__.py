# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

import hr_applicant
import hr_employee
import hr_employee_additional_info
import hr_cv_settings
import hr_job
import hr_regulation
