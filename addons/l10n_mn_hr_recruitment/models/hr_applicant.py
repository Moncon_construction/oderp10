# -*- coding: utf-8 -*-
from datetime import datetime

from dateutil.relativedelta import relativedelta
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from datetime import date
from dateutil import parser

from odoo.http import request


class HrVerifyWork (models.Model):
    _name = "hr.verify.work"
    _description = "Verify Work Experience Contacts"
    name = fields.Char('Name')
    company = fields.Char('Company')
    job = fields.Char('Job')
    phone = fields.Char('Phone Number')
    employee_id = fields.Many2one('hr.employee', "Employee")


class HrApplicant(models.Model):
    _inherit = ['hr.applicant']

    # Өргөдлийн үегийг default-аар авах
    def _default_stage_id(self):
        stage_num = self.env['hr.recruitment.stage'].search([('sequence', '=', 1)], limit=1)
        if stage_num:
            return stage_num.id
        return False

    @api.depends('health_insurance_no')
    def _health_insurance_no_length(self):
        for obj in self:
            if obj.health_insurance_no:
                obj.health_insurance_no_len = len(obj.health_insurance_no)
            else:
                obj.health_insurance_no_len = 0

    @api.onchange('partner_name', 'job_id')
    def _onchange_name(self):
        for obj in self:
            def_name = ''
            if obj.job_id:
                def_name += _('%s applicant ') % obj.job_id.name
            if obj.partner_name:
                def_name += _('%s ') % obj.partner_name
            obj.name = def_name

    name = fields.Char("Subject / Application Name", required=True, compute='_onchange_name')
    job_id = fields.Many2one('hr.job', required=True)
    partner_name = fields.Char(string='Last Name')
    last_name = fields.Char("Last Name")
    app_summary = fields.One2many('hr.application.summary', 'cv_id', "Application Summary", required=True)
    gender = fields.Selection([('male', 'Male'), ('female', 'Female')], 'Gender')
    date_of_birth = fields.Date("Date of Birth")
    age = fields.Integer("Age")
    place_of_birth = fields.Char("Place of Birth")
    ssnid = fields.Char(string='SSNID')
    major = fields.Char("Major")
    address = fields.Char("Currnet Address")
    prev_stage = fields.Many2one('hr.recruitment.stage', 'Previous Stage')
    emergency_phone = fields.Char("Emergency Phone Number")
    education_lines = fields.One2many('hr.education', 'cv_id', 'Education Lines', required=True)
    decision_employee = fields.Many2one('res.users', 'Decision Maker')
    contract_employee = fields.Many2one('res.users', 'Contract Lawyer')
    language_lines = fields.One2many('hr.language', 'cv_id', 'Language Skills', required=True)
    course_lines = fields.One2many('hr.course', 'cv_id', 'Additional Cource')
    interviewers = fields.One2many('hr.interviewers', 'cv_id', 'Interviewers', required=True)
    software_skill_lines = fields.One2many('hr.software.skill', 'cv_id', 'Software Skills')
    employment_lines = fields.One2many('hr.employment', 'cv_id', 'Employment')
    work_verify_lines = fields.One2many('hr.verify.work', 'cv_id', 'Verify Work Experience Contacts')
    idea1 = fields.Char('1.', help='Useful Work Idea')
    idea2 = fields.Char('2.', help='Useful Work Idea')
    idea3 = fields.Char('3.', help='Useful Work Idea')
    idea4 = fields.Char('4.', help='Useful Work Idea')
    reason_for_applying = fields.Char('Reason for applying', help='Reason for applying')
    family_members_num = fields.Integer('Number of family members')
    family_members_1 = fields.One2many('hr.employee.family.line', 'cv_id', 'Family Members', required=True)
    stage_id = fields.Many2one('hr.recruitment.stage', 'Stage', default=_default_stage_id)
    order_id = fields.Many2one('hr.regulation', 'Order of Employment')
    family_name = fields.Char(string='Family Name')
    award_ids = fields.One2many('hr.award', 'employee_id', string='Employee Award')
    work_skill_ids = fields.One2many('hr.work.skill', 'employee_id', string='Work Skill')
    arts_sports_talent = fields.One2many('hr.arts.sports.talent', 'employee_id', string='Arts Sports Talent')
    employment_ids = fields.One2many('hr.employment', 'employee_id', string='Employee Employment')
    employment_approvations = fields.One2many('hr.employment.approvation', 'employee',
                                              string='Employee Employment Approvation Information')
    country_id = fields.Many2one('res.country', string='Nationality (Country)')
    country_state_id = fields.Many2one('res.country.state', string='State')
    identification_id = fields.Char(string='Identification No', groups='hr.group_hr_user')
    passport_id = fields.Char('Passport No', groups='hr.group_hr_user')
    bank_account_id = fields.Many2one('res.partner.bank', string='Bank Account Number')
    ssnid = fields.Char(string='SSNID')
    health_insurance_no = fields.Char(string='Health insurance number', size=16)
    home_phone = fields.Char(string='Home Phone', size=8)
    is_own_car = fields.Boolean(string='Is Own Car')
    address_home_id = fields.Many2one('res.partner', string='Home Address')
    apartment_condition = fields.Many2one('hr.apartment.condition', string='Apartment Condition')
    sinid = fields.Char('SIN No', help='Social Insurance Number', groups='hr.group_hr_user')
    ssnid_length = fields.Integer('ssnid length', compute='_ssnid_length', store=True)
    sinid_length = fields.Integer('sinid length', compute='_sinid_length', store=True)
    health_insurance_no_len = fields.Integer('health insurance no length', compute='_health_insurance_no_length',
                                             store=True)
    foreign_passport_number = fields.Char(string='Foreign passport', size=16)
    driver_license_date = fields.Date('Driver license date')
    driver_license_number = fields.Char(string='Driver license number', size=16)
    driver_license_category = fields.Many2many('driver.license.category', 'employee_driver_license_rel', 'emp_id',
                                               'license_id', 'Driver license Category')
    other_job = fields.Char(string='Interested Other Job')
    working_year = fields.Selection([('year_1', '1 year'),
                                     ('year_1_3', '1-3 years'),
                                     ('year_3_5', '3-5 years'),
                                     ('year_5_plus', '5 plus')], string='Working Year')
    is_acquaintance = fields.Boolean('Is Acquaintance')
    facebook = fields.Char(string='Facebook')
    twitter_account = fields.Char(string='Twitter Acc')
    linkedin_account = fields.Char(string='Linkedin')
    own_mail = fields.Char(string='Own Mail')
    nationality = fields.Many2one('hr.nationality', string='Nationality')
    social_origin = fields.Many2one('hr.social.origin', string='Social Origin')
    age = fields.Integer(string='Age', compute='_compute_age', groups='hr.group_hr_user')
    age_range = fields.Many2one('hr.employee.age.range', string='Age Range')
    health_situation = fields.Char(string='Health Situation', size=80)
    blood_type = fields.Selection([
        ('first', 'First'),
        ('second', 'Second'),
        ('third', 'Third'),
        ('forth', 'Forth')], string='Blood Type')
    military_service = fields.Selection([
        ('yes', "Yes"),
        ('no', "No")], 'Military Service')
    has_disability = fields.Boolean(default=False, string="Has Disability")
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other')
    ], groups='hr.group_hr_user')
    marital = fields.Selection([
        ('single', 'Single'),
        ('married', 'Married'),
        ('widower', 'Widower'),
        ('divorced', 'Divorced')
    ], string='Marital Status', groups='hr.group_hr_user')
    children = fields.Integer(string='Number of Children', store=True, compute='_compute_children_number',
                              groups='hr.group_hr_user')
    birthday = fields.Date('Date of Birth')
    born_month = fields.Integer(string='Born Month', compute='_compute_age', store=True)
    place_of_birth_aimag = fields.Char('Place of birth:Aimag')
    place_of_birth_sum = fields.Char('Place of birth:Sum')
    district = fields.Many2one('res.district', string='Province/District')
    sumkhoroo = fields.Many2one('res.khoroo', string='Sum/Khoroo name')
    height = fields.Integer('Height')
    weight = fields.Integer('Weight')
    boot_size = fields.Char('Boot size')
    shirt_size = fields.Selection([
        ('2xs', '2XS'),
        ('xs', 'XS'),
        ('s', 'S'),
        ('m', 'M'),
        ('l', 'L'),
        ('xl', 'XL'),
        ('2xl', '2XL'),
        ('3xl', '3XL'),
        ('4xl', '4XL'),
        ('5xl', '5XL')], string='Shirt Size')
    sitting_drums_size = fields.Selection([
        ('2xs', '2XS'),
        ('xs', 'XS'),
        ('s', 'S'),
        ('m', 'M'),
        ('l', 'L'),
        ('xl', 'XL'),
        ('2xl', '2XL'),
        ('3xl', '3XL'),
        ('4xl', '4XL'),
        ('5xl', '5XL')], string='Sitting Drums Size')
    birth_month_day = fields.Char(string="Birthmonth and birthday", compute='_compute_month_day', store=True)
    family_members = fields.One2many('hr.employee.family.line', 'employee_id', string='Family Members')
    salary_expected_min = fields.Float("Expected Minimum Salary", help="Salary Expected by Applicant")
    salary_expected_max = fields.Float("Expected Maximum Salary", help="Salary Expected by Applicant")
    acquaintance_ids = fields.One2many('hr.applicant.acquaintance', 'cv_id', string='Applicant Acquaintance')
    home_address = fields.Char('Home Address')
    is_smoke = fields.Boolean('Is Smoke')
    is_drunk_alcohol = fields.Boolean('Is Drunk Alcohol')
    reason_1 = fields.Char('r1')
    reason_2 = fields.Char('r2')
    reason_3 = fields.Char('r3')
    partner_evaluation_1 = fields.Char('P1')
    partner_evaluation_2 = fields.Char('P2')
    partner_evaluation_3 = fields.Char('P3')
    working_environment = fields.Char('Working Environment')
    working_environment2 = fields.Char('Working Environment 2')
    culture = fields.Char('Culture')
    salary = fields.Char('Salary')
    working_hierarchy1 = fields.Integer('Hierarchy 1')
    working_hierarchy2 = fields.Integer('Hierarchy 2')
    working_hierarchy3 = fields.Integer('Hierarchy 3')
    working_hierarchy4 = fields.Integer('Hierarchy 4')
    working_description1 = fields.Char('Description 1')
    working_description2 = fields.Char('Description 2')
    working_description3 = fields.Char('Description 3')
    working_description4 = fields.Char('Description 4')

    @api.depends('ssnid')
    def _ssnid_length(self):
        for obj in self:
            if obj.ssnid:
                obj.ssnid_length = len(obj.ssnid)
            else:
                obj.ssnid_length = 0

    @api.depends('sinid')
    def _sinid_length(self):
        for obj in self:
            if obj.sinid:
                obj.sinid_length = len(obj.sinid)
            else:
                obj.sinid_length = 0

    @api.depends('birthday')
    def _compute_month_day(self):
        for employee in self:
            employee.birth_month_day = str(parser.parse(employee.sudo().birthday).month) + '-' + str(
                parser.parse(employee.sudo().birthday).day) if employee.sudo().birthday else 0

    @api.depends('birthday')
    def _compute_age(self):
        current_date = datetime.now()
        current_year = current_date.year
        for employee in self:
            employee.age = current_year - parser.parse(employee.sudo().birthday).year if employee.sudo().birthday else 0
            employee.born_month = parser.parse(employee.sudo().birthday).month if employee.sudo().birthday else 0

    @api.depends('family_members', 'family_members.is_child_check')
    def _compute_children_number(self):
        for emp in self:
            child_number = 0
            current_date = datetime.now()
            for member in emp.family_members:
                if member.age:
                    delta = relativedelta(current_date,
                                          datetime.strptime(member.age, '%Y-%m-%d'))
                    if delta.years < 18 and member.is_child_check:
                        child_number += 1
            emp.children = child_number

    @api.depends('birthday')
    def _compute_age(self):
        current_date = datetime.now()
        current_year = current_date.year
        for employee in self:
            employee.age = current_year - parser.parse(employee.sudo().birthday).year if employee.sudo().birthday else 0
            employee.born_month = parser.parse(employee.sudo().birthday).month if employee.sudo().birthday else 0
    #Өргөдлүүп үе шилжих үед insert хийж горилогч болон шийдвэр гаргагчруу имэйл явуулах

    def get_education_lines(self, values):
        invoice_lines = []
        i = 0
        list = []
        today = date.today()
        if values:
            for vals in values:
                x = vals.split('_')
                if x[0] == 'education':
                    list.append(x[1])
            while i <= int(max(list)):
                if 'education_' + str(i) + '_3' in values:
                    degree_obj = self.env['hr.recruitment.degree'].search([('name','=',values['education_' + str(i) + '_3'])], limit=1)
                if 'education_' + str(i) + '_1' in values:
                    school_obj = self.env['hr.school'].search([('name','=',values['education_' + str(i) + '_1'])], limit=1)
                    if school_obj:
                        school_id = school_obj.id
                    else:
                        school_id = self.env['hr.school'].create({'name': values['education_' + str(i) + '_1']}).id
                    line_vals = {'school_id': school_id if school_id else False,
                                 'school_location': values['education_' + str(i) + '_2'] if 'education_' + str(i) + '_2' in values else False,
                                 'education_degree': degree_obj.id if degree_obj else False,
                                 'entered_date': values['education_' + str(i) + '_4'] if 'education_' + str(i) + '_6' in values else False,
                                 'finished_date': values['education_' + str(i) + '_5'] if 'education_' + str(i) + '_6' in values else False,
                                 'profession_name': values['education_' + str(i) + '_6'] if 'education_' + str(i) + '_6' in values else False,
                                 'diploma_number': values['education_' + str(i) + '_7'] if 'education_' + str(i) + '_7' in values else False,
                                 'diploma_topics': values['education_' + str(i) + '_8'] if 'education_' + str(i) + '_8' in values else False,
                                 'gpa': values['education_' + str(i) + '_9'] if 'education_' + str(i) + '_9' in values else False}
                    invoice_lines.append((0, 0, line_vals))
                i += 1
        return invoice_lines

    def get_language_lines(self, values):
        lines = []
        i = 0
        list = []
        if values:
            for vals in values:
                x = vals.split('_')
                if x[0] == 'language':
                    list.append(x[1])
            while i <= int(max(list)):
                if 'language_' + str(i) + '_1' in values:
                    language_obj = self.env['hr.employee.lang'].search([('name','=',values['language_' + str(i) + '_1'])], limit=1)
                if values['language_' + str(i) + '_2'] == u'Анхан':
                    listening_skill = 'beginning'
                if values['language_' + str(i) + '_2'] == u'Дунд':
                    listening_skill = 'intermediate'
                if values['language_' + str(i) + '_2'] == u'Гүнзгий':
                    listening_skill = 'advanced'
                if values['language_' + str(i) + '_3'] == u'Анхан':
                    speaking_skill = 'beginning'
                if values['language_' + str(i) + '_3'] == u'Дунд':
                    speaking_skill = 'intermediate'
                if values['language_' + str(i) + '_3'] == u'Гүнзгий':
                    speaking_skill = 'advanced'
                if values['language_' + str(i) + '_4'] == u'Анхан':
                    reading_skill = 'beginning'
                if values['language_' + str(i) + '_4'] == u'Дунд':
                    reading_skill = 'intermediate'
                if values['language_' + str(i) + '_4'] == u'Гүнзгий':
                    reading_skill = 'advanced'
                if values['language_' + str(i) + '_5'] == u'Анхан':
                    writing_skill = 'beginning'
                if values['language_' + str(i) + '_5'] == u'Дунд':
                    writing_skill = 'intermediate'
                if values['language_' + str(i) + '_5'] == u'Гүнзгий':
                    writing_skill = 'advanced'
                line_vals = {'language': language_obj.id if language_obj else False,
                             'listening_skill': listening_skill if listening_skill else False,
                             'speaking_skill': speaking_skill if speaking_skill else False,
                             'reading_skill': reading_skill if reading_skill else False,
                             'writing_skill': writing_skill if writing_skill else False
                             }
                lines.append((0, 0, line_vals))
                i += 1
        return lines

    def get_course_lines(self, values):
        lines = []
        i = 0
        list = []
        if values:
            for vals in values:
                x = vals.split('_')
                if x[0] == 'course':
                    list.append(x[1])
            if list:
                while i <= int(max(list)):
                    line_vals = {'location':  values['course_' + str(i) + '_1'] if 'course_' + str(i) + '_1' in values else False,
                                 'name': values['course_' + str(i) + '_2'] if 'course_' + str(i) + '_2' in values else False,
                                 'date': values['course_' + str(i) + '_3'] if 'course_' + str(i) + '_3' in values else False,
                                 'subject': values['course_' + str(i) + '_4'] if 'course_' + str(i) + '_4' in values else False,
                                 'duration': values['course_' + str(i) + '_5'] if 'course_' + str(i) + '_5' in values else False
                                 }
                    lines.append((0, 0, line_vals))
                    i += 1
        return lines

    def get_family_lines(self, values):
        lines = []
        i = 0
        list = []
        if values:
            for vals in values:
                x = vals.split('_')
                if x[0] == 'family':
                    list.append(x[1])
            if list:
                while i <= int(max(list)):
                    if 'family_' + str(i) + '_1' in values:
                        family_obj = self.env['hr.employee.family.member'].search([('name', '=', values['family_' + str(i) + '_1'])], limit=1)
                    line_vals = {'family_member_id': family_obj.id if family_obj else False,
                                 'name': values['family_' + str(i) + '_2'] if 'family_' + str(i) + '_2' in values else False,
                                 'date_of_birth': values['family_' + str(i) + '_3'] if 'family_' + str(i) + '_3' in values else False,
                                 'place_of_birth': values['family_' + str(i) + '_4'] if 'family_' + str(i) + '_4' in values else False,
                                 'current_organization': values['family_' + str(i) + '_5'] if 'family_' + str(i) + '_5' in values else False,
                                 'current_job': values['family_' + str(i) + '_6'] if 'family_' + str(i) + '_6' in values else False,
                                 'contact': values['family_' + str(i) + '_7'] if 'family_' + str(i) + '_7' in values else False
                                 }
                    lines.append((0, 0, line_vals))
                    i += 1
        return lines

    def get_software_lines(self, values):
        lines = []
        i = 0
        list = []
        if values:
            for vals in values:
                x = vals.split('_')
                if x[0] == 'software':
                    list.append(x[1])
            if 'software_' + str(i) + '_1' in values:
                software_obj = self.env['software.technic'].search([('name', '=', values['software_' + str(i) + '_1'])], limit=1)
                if software_obj:
                    software_id = software_obj.id
                else:
                    software_id = self.env['software.technic'].create({'name': values['software_' + str(i) + '_1']}).id
                while i <= int(max(list)):
                    if values['language_' + str(i) + '_2'] == u'Анхан':
                        software_skill = 'low'
                    if values['language_' + str(i) + '_2'] == u'Дунд':
                        software_skill = 'good'
                    if values['language_' + str(i) + '_2'] == u'Гүнзгий':
                        software_skill = 'excellent'
                    line_vals = {'name':  software_id if software_id else False,
                                 'software_skill': software_skill if software_skill else False,
                                 }
                    lines.append((0, 0, line_vals))
                    i += 1
        return lines

    def get_employement_lines(self, values):
        lines = []
        i = 0
        list = []
        if values:
            for vals in values:
                x = vals.split('_')
                if x[0] == 'employment':
                    list.append(x[1])
            if list:
                while i <= int(max(list)):
                    line_vals = {'organization': values['employment_' + str(i) + '_1'] if 'employment_' + str(i) + '_1' in values else False,
                                 'job_title': values['employment_' + str(i) + '_2'] if 'employment_' + str(i) + '_2' in values else False,
                                 'entered_date': values['employment_' + str(i) + '_3'] if 'employment_' + str(i) + '_3' in values else False,
                                 'resigned_date': values['employment_' + str(i) + '_4'] if 'employment_' + str(i) + '_4' in values else False,
                                 'resigned_reason': values['employment_' + str(i) + '_5'] if 'employment_' + str(i) + '_5' in values else False,
                                 'wage': values['employment_' + str(i) + '_6'] if 'course_' + str(i) + '_6' in values else False
                                 }
                    lines.append((0, 0, line_vals))
                    i += 1
        return lines

    def get_work_verify_lines(self, values):
        lines = []
        i = 0
        list = []
        if values:
            for vals in values:
                x = vals.split('_')
                if x[0] == 'work':
                    list.append(x[1])
            if list:
                while i <= int(max(list)):
                    line_vals = {'name': values['work_' + str(i) + '_1'] if 'work_' + str(i) + '_1' in values else False,
                                 'job': values['work_' + str(i) + '_2'] if 'work_' + str(i) + '_2' in values else False,
                                 'phone': values['work_' + str(i) + '_3'] if 'work_' + str(i) + '_3' in values else False,
                                 'company': values['work_' + str(i) + '_4'] if 'work_' + str(i) + '_4' in values else False,
                                 }
                    lines.append((0, 0, line_vals))
                    i += 1
        return lines

    def get_ideas(self, values):
        idea1 = None
        idea2 = None
        idea3 = None
        idea4 = None
        if values:
            for vals in values:
                x = vals.split('_')
                if x[0] == 'idea':
                    if x[1] == '1':
                        idea1 = values['idea_1']
                    if x[1] == '2':
                        idea2 = values['idea_2']
                    if x[1] == '3':
                        idea3 = values['idea_3']
                    if x[1] == '4':
                        idea4 = values['idea_4']
        return idea1, idea2, idea3, idea4

    @api.model
    def create(self, vals):
        context = self._context
        if 'job_id' in vals:
            cv_obj = self.env['hr.cv.settings'].search([('job_id','=',vals['job_id'])])
            if 'custom' in context:
                if cv_obj.education_lines_set:
                    edu_lines = self.get_education_lines(context['custom'])
                    vals['education_lines'] = edu_lines
                if cv_obj.language_lines_set:
                    lang_lines = self.get_language_lines(context['custom'])
                    vals['language_lines'] = lang_lines
                if cv_obj.course_lines_set:
                    course_lines = self.get_course_lines(context['custom'])
                    vals['course_lines'] = course_lines
                if cv_obj.software_skill_lines_set:
                    software_lines = self.get_software_lines(context['custom'])
                    vals['software_skill_lines'] = software_lines
                if cv_obj.employment_lines_set:
                    employment_lines = self.get_employement_lines(context['custom'])
                    vals['employment_lines'] = employment_lines
                if cv_obj.work_verify_lines_set:
                    work_verify_lines = self.get_work_verify_lines(context['custom'])
                    vals['work_verify_lines'] = work_verify_lines
                if cv_obj.family_set:
                    family_members_1 = self.get_family_lines(context['custom'])
                    vals['family_members_1'] = family_members_1
                if cv_obj.idea_set:
                    idea1, idea2, idea3, idea4 = self.get_ideas(context['custom'])
                    if idea1:
                        vals['idea1'] = idea1
                    if idea2:
                        vals['idea2'] = idea2
                    if idea3:
                        vals['idea3'] = idea3
                    if idea4:
                        vals['idea4'] = idea4
        return super(HrApplicant, self.with_context(mail_create_nolog=True)).create(vals)

    @api.multi
    def write(self, vals):
        res = True
        if vals.get('user_id'):
            vals['date_open'] = fields.datetime.now()

        if 'stage_id' in vals:
            vals['date_last_stage_update'] = fields.datetime.now()
            for applicant in self:
                vals['last_stage_id'] = applicant.stage_id.id
                res = super(HrApplicant, self).write(vals)
        else:
            res = super(HrApplicant, self).write(vals)

        # if vals.get('job_id'):
        #     for applicant in self:
        #         name = applicant.partner_name if applicant.partner_name else applicant.name
        #         self.env['hr.job'].message_post([vals['job_id']],
        #                                         body=_('New application from %s') % name,
        #                                         subtype="hr_recruitment.mt_job_applicant_new")

        if vals.get('stage_id'):
            stage = self.env['hr.recruitment.stage']
            if stage.template_id:
                compose_id = self.env['mail.compose.message'].create({
                    'model': self._name,
                    'composition_mode': 'mass_mail',
                    'template_id': stage.template_id.id,
                    'post': True,
                    'notify': True})
                values = self.env['mail.compose.message'].onchange_template_id([compose_id], stage.template_id.id, 'mass_mail', self._name, False)['value']
                if values.get('attachment_ids'):
                    values['attachment_ids'] = [(6, 0, values['attachment_ids'])]
                self.env['mail.compose.message'].write([compose_id], values)
                self.env['mail.compose.message'].send_mail([compose_id])
            for application in self:
                if (application.stage_id.name == 'Health Inspection' or application.stage_id.name == 'Accepted') and self._uid != application.decision_employee.id:
                    raise ValidationError('Current user cannot change application stage.')
                if (application.stage_id.name != 'Refused') and (application.stage_id.name != 'Waiting for decision'):
                    application.prev_stage = application.stage_id
                email_ids = []
                vals = {}
                self._uid_user = self.env['res.users']
                if application.stage_id.name == 'Health Inspection' or application.stage_id.name == u'Эрүүл мэндийн үзлэг':
                    if not application.user_id:
                        raise ValidationError('Select the Responsible HR employee.')
                    vals = {'state': 'outgoing',
                            'subject': u'Aнкет.',
                            'body_html': u'<p>Сайн байна уу </br></p><p>Анкет "Эрүүл Мэндийн үзлэг" төлөвт орлоо.</p>  <br>  <br> <p> Баярлалаа,</p><pre>--<br>ERP Автомат Имэйл </pre>',
                            'email_to': application.user_id.partner_id.email,
                            'email_from': self._uid_user.partner_id.email}

                if application.stage_id.name == 'Received' or application.stage_id.name == u'Хүлээн авсан':
                    application.user_id = self._uid
                    vals = {'state': 'outgoing',
                            'subject': u'Aнкет.',
                            'body_html': u'<p>Сайн байна уу </br></p><p>Таны анкетыг хүлээн авлаа.</p>  <br>  <br> <p> Баярлалаа,</p><pre>--<br>ERP Автомат Имэйл </pre>',
                            'email_to': application.email_from,
                            'email_from': self._uid_user.company_id.email}

                if application.stage_id.name == 'Short List' or application.stage_id.name == u'Товч жагсаалт':
                    if not application.user_id:
                        application.user_id = self._uid
                    vals = {'state': 'outgoing',
                            'subject': u'Aнкет.',
                            'body_html': u'<p>Сайн байна уу </br></p><p>Таны анкет шорт лист-д орлоо.</p>  <br>  <br> <p> Баярлалаа,</p><pre>--<br>ERP Автомат Имэйл </pre>',
                            'email_to': application.email_from,
                            'email_from': self._uid_user.company_id.email}

                if application.stage_id.name == 'First Interview' or application.stage_id.name == u'Эхний ярилцлага':
                    if not application.user_id:
                        application.user_id = self._uid
                    vals = {'state': 'outgoing',
                            'subject': u'Aнкет.',
                            'body_html': u'<p>Сайн байна уу </br></p><p>Та анхны ярилцлагад тэцлээ.</p>  <br>  <br> <p> Баярлалаа,</p><pre>--<br>ERP Автомат Имэйл </pre>',
                            'email_to': application.email_from,
                            'email_from': self._uid_user.company_id.email}

                if application.stage_id.name == 'Waiting for decision' or application.stage_id.name == u'Хүлээгдэж байгаа':
                    if not application.user_id:
                        application.user_id = self._uid
                    if not application.decision_employee:
                        raise ValidationError('Select the Decision making Employee.')
                    user = self.env['res.users']
                    base_url = self.env['ir.config_parameter'].get_param('web.base.url')
                    action_id = self.env['ir.model.data'].get_object_reference('hr_recruitment', 'crm_case_categ0_act_job')[1],
                    db_name = request.session
                    vals = {'state': 'outgoing',
                            'subject': u'Aнкет.',
                            'body_html': u'<p>Сайн байна уу </br></p><p>Танд батлах анкет ирлээ.</p> <p> Холбоос:<b><a href="' + unicode(base_url) + u'/web?db=' +
                            unicode(db_name) + u'#id=' + str(application.id) + u'&view_type=form&model=hr.applicant&action=' + str(action_id[0]) + u'">Батлах анкет </a></b> </p> <br>  <br> <p> Баярлалаа,</p><pre>--<br>ERP Автомат Имэйл </pre>',
                            'email_to': application.decision_employee.partner_id.email,
                            'email_from': self._uid_user.company_id.email}

                if application.stage_id.name == 'Refused' or application.stage_id.name == u'Татгалзсан':
                    if application.prev_stage.name == 'Received' or application.stage_id.name == u'Хүлээн авсан':
                        vals = {'state': 'outgoing',
                                'subject': u'Aнкет.',
                                'body_html': u'<p>Сайн байна уу </br></p><p>Таны анкет didt meet our recuirents.</p>  <br>  <br> <p> Баярлалаа,</p><pre>--<br>ERP Автомат Имэйл </pre>',
                                'email_to': application.email_from,
                                'email_from': self._uid_user.company_id.email}

                    if application.prev_stage.name == 'First Interview' or application.stage_id.name == u'Эхний ярилцлага':
                        vals = {'state': 'outgoing',
                                'subject': u'Aнкет.',
                                'body_html': u'<p>Сайн байна уу </br></p><p>Та did not pass first interview.</p>  <br>  <br> <p> Баярлалаа,</p><pre>--<br>ERP Автомат Имэйл </pre>',
                                'email_to': application.email_from,
                                'email_from': self._uid_user.company_id.email}

                    if application.prev_stage.name == 'Second Interview' or application.stage_id.name == u'2 дахь ярилцлага':
                        vals = {'state': 'outgoing',
                                'subject': u'Aнкет.',
                                'body_html': u'<p>Сайн байна уу </br></p><p>Та did not pass second interview.</p>  <br>  <br> <p> Баярлалаа,</p><pre>--<br>ERP Автомат Имэйл </pre>',
                                'email_to': application.email_from,
                                'email_from': self._uid_user.company_id.email}

                    if application.prev_stage.name == 'Health Inspection' or application.stage_id.name == u'Эрүүл мэндийн үзлэг':
                        vals = {'state': 'outgoing',
                                'subject': u'Aнкет.',
                                'body_html': u'<p>Сайн байна уу </br></p><p>Та did not pass Health Inspection.</p>  <br>  <br> <p> Баярлалаа,</p><pre>--<br>ERP Автомат Имэйл </pre>',
                                'email_to': application.email_from,
                                'email_from': self._uid_user.company_id.email}

                if application.stage_id.name == 'Accepted' or application.stage_id.name == u'Зөвшөөрөгдсөн':
                    regulation_type = self.env['hr.regulation.type'].search([('type', '=', 'new_employee')])
                    if not regulation_type:
                        regulation_type = self.env['hr.regulation.type'].create({
                            'type': 'new_employee',
                            'name': 'Order of Employment From Application',
                        })
                    regulation_obj = self.env['hr.regulation']
                    regulation_id = regulation_obj.create({
                        'regulation_type_id': regulation_type.id,
                        'reg_number': str(datetime.now()),
                        'name': str(datetime.now()) + "Application",
                        'company_id': application.company_id.id,
                        'verify_user': application.decision_employee.id,
                        'created_employee_id': self._uid,
                        'applicant_id': application.id,
                    })
                    application.order_id = regulation_id
                    if not application.user_id:
                        raise ValidationError('Select the Resposible HR user.')

                    user = self.env['res.users']
                    base_url = self.env['ir.config_parameter'].get_param('web.base.url')
                    action_id = self.env['ir.model.data'].get_object_reference('hr_recruitment', 'crm_case_categ0_act_job')[1],
                    db_name = request.session
                    vals = {'state': 'outgoing',
                            'subject': u'Aнкет.',
                            'body_html': u'<p>Сайн байна уу </br></p><p>Ажил горилогч тэнцлээ. Ажилд авах тушаал үүссэн. Шинэ ажилтан үүсэгнэ үү.</p> <p> Холбоос:<b><a href="' +
                            unicode(base_url) + u'/web?db=' + unicode(db_name) + u'#id=' + str(application.id) + u'&view_type=form&model=hr.applicant&action=' + str(action_id[0]) +
                            u'">Батлах анкет </a></b> </p> <br>  <br> <p> Баярлалаа,</p><pre> -- <br>ERP Автомат Имэйл </pre>',
                            'email_to': application.user_id.partner_id.email,
                            'email_from': user.partner_id.email}

                if vals:
                    email_ids.append(self.env['mail.mail'].create(vals))
                    if email_ids != []:
                        self.env['mail.mail'].send(email_ids)

        for application in self:
            if application.description:
                self._uid_user = self.env['res.users']
                summary = []
                summary.append((0, 0, {'user_id': self._uid_user,
                                       'summ': application.description}))
                self.env['hr.application.summary'].create({
                    'cv_id': application.id,
                    'user_id': self._uid,
                    'summ': application.description,
                })
                application.description = ""
        return res
    # Өргөдөл үүсгэж ажилтан үүсгэх товч дарагдахад нэмэлт талбаруудыг барьж авч ажилтан үүсгэх, имэйл явуулах

    @api.multi
    def create_employee_from_applicant(self):
        hr_employee = self.env['hr.employee']
        model_data = self.env['ir.model.data']
        act_window = self.env['ir.actions.act_window']
        emp_id = []
        for applicant in self:
            if not applicant.contract_employee:
                raise ValidationError('Select Contract Lawer to create Employment contract.')
            contact_name = False
            if applicant.partner_id:
                contact_name = self.env['res.partner'].search([('id', '=', applicant.partner_id.id)]).name_get()[0][1]
            if applicant.job_id and (applicant.partner_name or contact_name):
                applicant.job_id.write({'no_of_hired_employee': applicant.job_id.no_of_hired_employee + 1})

                school = []
                language = []
                courses = []
                soft = []
                verify_work = []
                family = []
                work = []
                for w in applicant.employment_lines:
                    work.append((0, 0, {'organization': w.organization,
                                        'job_title': w.job_title,
                                        'entered_date': w.entered_date,
                                        'resigned_date': w.resigned_date,
                                        'resigned_reason': w.resigned_reason,
                                        'wage': w.wage,
                                        'responsibility': w.responsibility}))
                for e in applicant.education_lines:
                    school.append((0, 0, {'school_location': e.school_location,
                                          'entered_date': e.entered_date,
                                          'finished_date': e.finished_date,
                                          'profession_name': e.profession_name,
                                          'diploma_topics': e.diploma_topics,
                                          'school_id': e.school_id.id,
                                          'education_degree': e.education_degree.id
                                          }))
                for l in applicant.language_lines:
                    language.append((0, 0, {'language': l.language,
                                            'duration': l.duration,
                                            'speaking_skill': l.speaking_skill,
                                            'reading_skill': l.reading_skill,
                                            'listening_skill': l.listening_skill,
                                            'writing_skill': l.writing_skill,
                                            }))
                for s in applicant.software_skill_lines:
                    soft.append((0, 0, {'name': s.name.id,
                                        'software_skill': s.software_skill}))

                for c in applicant.course_lines:
                    courses.append((0, 0, {'name': c.name,
                                           'subject': c.subject,
                                           'date': c.date,
                                           'duration': c.duration}))

                for f in applicant.family_members_1:
                    family.append((0, 0, {'family_member_id': f.family_member_id.id,
                                          'name': f.name,
                                          'date_of_birth': f.date_of_birth,
                                          'place_of_birth': f.place_of_birth,
                                          'current_job': f.current_job,
                                          'contact': f.contact,
                                          }))
                for v in applicant.work_verify_lines:
                    verify_work.append((0, 0, {'name': v.name,
                                               'company': v.company,
                                               'job': v.job,
                                               'phone': v.phone,
                                               'cv_id': applicant.id}))
                emp_id = hr_employee.create({'name': applicant.partner_name or contact_name,
                                             'last_name': applicant.last_name or False,
                                             'gender': applicant.gender or False,
                                             'birthday': applicant.date_of_birth or False,
                                             'place_of_birth': applicant.place_of_birth or False,
                                             'ssnid': applicant.ssnid or False,
                                             'major': applicant.major or False,
                                             'rel_person_phone': applicant.emergency_phone or False,
                                             'job_id': applicant.job_id.id or False,
                                             'department_id': applicant.department_id.id or False,
                                             'address_id': applicant.company_id and applicant.company_id.partner_id and applicant.company_id.partner_id.id or False,
                                             'work_email': applicant.email_from or False,
                                             'work_phone': applicant.partner_mobile or False})
                employee = self.env['hr.employee'].search([('id', '=', emp_id.id)])
                employee.write({
                    'education_ids': school,
                    'language_ids': language,
                    'software_skill_ids': soft,
                    'course_ids': courses,
                    'verify_work_ids': verify_work,
                    'family_members': family,
                    'employment_ids': work,
                })

                prtnr = self.env['res.partner']
                if applicant.partner_name and applicant.last_name:
                    prtnr.create({'name': applicant.last_name + '' + applicant.partner_name,
                                  'street': applicant.address,
                                  'notify_email': 'none'})
                address_idd = self.env['res.partner'].search([('id', '=', applicant.partner_id.id)]).address_get([prtnr])['contact']
                employee.write({'address_home_id': address_idd or False})
                self.write({'emp_id': emp_id.id})
                self.env['hr.job'].search([('id', '=', applicant.job_id.id)]).message_post(body=_('New Employee %s Hired') % applicant.partner_id.name
                                                                                           if applicant.partner_id.name else applicant.name,
                                                                                           subtype="hr_recruitment.mt_job_applicant_hired")
            else:
                raise ValidationError('You must define an Applied Job and a Contact Name for this applicant.')

        action_id = model_data.get_object_reference('hr', 'open_view_employee_list')
        act = action_id[1]
        dict_act_window = act_window.search([('id', '=', act)])
        if emp_id:
            dict_act_window['res_id'] = emp_id[0]
        dict_act_window['view_mode'] = 'form,tree'

        email_ids = []
        uid_user = self.env['res.users']
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        action_id = self.env['ir.model.data'].get_object_reference('hr', 'open_view_employee_list_my')[1],
        db_name = request.session['db']
        vals = {'state': 'outgoing',
                'subject': u'Aнкет.',
                'body_html': u'<p>Сайн байна уу </br></p><p>Шинэ ажилтантай хөдөлмөрийн гэрээ байгуулах сануулага ирлээ.</p> <p> Холбоос:<b><a href="' +
                unicode(base_url) + u'/web?db=' + unicode(db_name) + u'#id=' + str(emp_id.id) + u'&view_type=form&model=hr.employee&action=' + str(action_id[0]) + u'">Шинэ ажилчин </a></b> </p> <br>  <br> <p> Баярлалаа,</p><pre>--<br>ERP Автомат Имэйл </pre>',
                'email_to': applicant.contract_employee.partner_id.email,
                'email_from': uid_user.partner_id.email}
        if vals:
            email_ids.append(self.env['mail.mail'].create(vals))
            if email_ids != []:
                self.env['mail.mail'].send(email_ids)
        return dict_act_window


class HrVerifyWorkInherit (models.Model):
    _inherit = "hr.verify.work"

    cv_id = fields.Many2one('hr.applicant', 'Application')
    applicant_ssnid = fields.Char('Applicant ssnid')


class HrApplicationSummary (models.Model):
    _name = "hr.application.summary"
    _description = "Application Summary"

    cv_id = fields.Many2one('hr.applicant', 'Application')
    user_id = fields.Many2one('res.users', "User")
    summ = fields.Text("Summary")


class HrCourse (models.Model):
    _name = "hr.course"
    _description = "HR Cources"

    cv_id = fields.Many2one('hr.applicant', 'Application')
    applicant_ssnid = fields.Char('Applicant ssnid')
    location = fields.Char('Location')
    name = fields.Char('Cource Name')
    date = fields.Date('Date')
    subject = fields.Char('Subject')
    duration = fields.Char('Duration')
    employee_id = fields.Many2one('hr.employee', "Employee")

class HrApplicantAcquaintance(models.Model):
    _name = "hr.applicant.acquaintance"

    cv_id = fields.Many2one('hr.applicant', 'Application')
    acquaintance = fields.Char('Acquaintance')
    acquaintance_position = fields.Char('Acquaintance Position')
    acquaintance_name = fields.Char('Acquaintance Name')

class HrEmployment(models.Model):
    _inherit = 'hr.employment'

    organization = fields.Char('Organization', size=124)
    organization_type = fields.Char('Organization type', size=124)
    job_id = fields.Many2one('hr.job', 'Job title', size=124)
    job_title = fields.Char('Job Title')
    entered_date = fields.Date('Entered Date')
    resigned_date = fields.Date('Resigned Date')
    resigned_reason = fields.Text('Resigned reason')
    working_time_type = fields.Selection([('day', 'Day'),
                                          ('shift', 'Shift')],
                                         string='Time Type')
    working_condition = fields.Selection([('normal', 'Normal'),
                                          ('abnormal', 'Abnormal')],
                                         string='Working Condition')
    insurance_month = fields.Float('Insurance Month')
    wage = fields.Integer('Wage')
    employee_id = fields.Many2one('hr.employee', "Employee")
    responsibility = fields.Char('Responsibility')
    in_this_organization = fields.Boolean('In this organization')
    total_worked = fields.Float('Total Worked')
    experience = fields.Char('Experience')
    contact = fields.Char('Contact')
    paid_social_insurance = fields.Boolean('Paid social insurance')
    skill_level = fields.Many2one('hr.skill.level', string="Skill level")
    contract_id = fields.Many2one('hr.contract', string='Contract')
    job_classification = fields.Many2one('hr.job.classification', string='Job classification')