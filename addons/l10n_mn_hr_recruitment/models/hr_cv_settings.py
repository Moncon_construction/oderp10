# -*- coding: utf-8 -*-

from odoo import fields, api, models
from odoo.exceptions import ValidationError


class HrJob (models.Model):
    _inherit = ['hr.job']
    cv_settings_id = fields.Many2one('hr.cv.settings', 'CV settings')


class HrCvSetting (models.Model):
    _name = "hr.cv.settings"
    _description = "CV Settings"
    _inherit = ['mail.thread']

    job_id = fields.Many2one('hr.job', 'Job', required=True)
    last_name_set = fields.Selection([('required', 'Required'), ], 'Last Name')
    salary_expected = fields.Selection([('required', 'Required'), ('have', 'Have'), ], 'Expected Salary')
    availability_set = fields.Selection([('required', 'Required'), ('have', 'Have'), ], 'Availability')
    source_id_set = fields.Selection([('required', 'Required'), ('have', 'Have'), ], 'Source')
    first_name_set = fields.Selection([('required', 'Required'), ], 'First Name')
    gender_set = fields.Selection([('required', 'Required'), ], 'Gender')
    date_of_birth_set = fields.Selection([('required', 'Required'), ('have', 'Have'), ], 'Date of birth')
    age_set = fields.Selection([('required', 'Required'), ('have', 'Have'), ], 'Age')
    place_of_birth_set = fields.Selection([('required', 'Required'), ('have', 'Have'), ], 'Place of birth')
    ssnid_set = fields.Selection([('required', 'Required'), ('have', 'Have'), ], 'SSNID')
    major_set = fields.Selection([('required', 'Required'), ('have', 'Have'), ], 'Major')
    address_set = fields.Selection([('required', 'Required'), ('have', 'Have'), ], 'Address')
    emergency_phone_set = fields.Selection([('required', 'Required'), ('have', 'Have'), ], 'Emergency Phone')
    phone_set = fields.Selection([('required', 'Required'), ('have', 'Have'), ], 'Phone')
    mobile_set = fields.Selection([('required', 'Required'), ('have', 'Have'), ], 'Mobile')
    email_set = fields.Selection([('required', 'Required'), ], 'Email')
    education_lines_set = fields.Selection([('have', 'Have')], 'Education')
    language_lines_set = fields.Selection([('have', 'Have')], 'Language')
    course_lines_set = fields.Selection([('have', 'Have')], 'Courses')
    software_skill_lines_set = fields.Selection([('have', 'Have')], 'Software Skills')
    employment_lines_set = fields.Selection([('have', 'Have')], 'Employment')
    work_verify_lines_set = fields.Selection([('have', 'Have')], 'Work Verify People')
    idea_set = fields.Selection([('have', 'Have')], 'Usefull Work Ideas')
    family_set = fields.Selection([('have', 'Have'), ], 'Family Members')
    reason_for_applying = fields.Selection([('required', 'Required'), ('have', 'Have'), ], 'Reason for Applying')
    #cv тохиргоог ажилтан ажлын байр бүрт үүсгэх, ажлын байрны cv тохиргоо өмнө нь хийгдсэн эсэхийг шалгах

    @api.model
    def create(self, vals):
        obj = self
        if 'job_id' in vals:
            job_id = obj.search([('job_id', '=', vals.get('job_id'))]).id
            if job_id:
                raise ValidationError('CV template for this job already exists.')
            created_settings = super(HrCvSetting, self).create(vals)
            z = self.env['hr.job'].search([('id', '=', vals.get('job_id'))])
            z.write({'cv_settings_id': created_settings.id})
        return created_settings
    #cv тохиргоог ажилтан ажлын байр бүрт үүсгэсний дараа, засах үйлдэл хийгдэхэд ажлын байрны cv тохиргоо өмнө нь хийгдсэн эсэхийг шалгах

    @api.multi
    def write(self, vals):
        obj = self
        if vals:
            job_id = obj.search([('job_id', '=', vals.get('job_id'))]).id
            if job_id:
                raise ValidationError('CV template for this job already exists.')
        return super(HrCvSetting, self).write(vals)
