# -*- coding: utf-8 -*-

from datetime import datetime

from odoo import api, fields, models

class HrEmployee (models.Model):
    _inherit = "hr.employee"
    engagement_in_company = fields.Date(string='Date of Engagement in company', store=True, required=False, default=datetime.now())

    major = fields.Char('Major')
    field_start = fields.Date('Started working in this field')
    field_paused = fields.Float('Paused working in this field')
    field_worked = fields.Float(string='Worked in current field')
    last_vacation = fields.Date('Last Vacation')
    work_paused = fields.Float('Paused working in this Company')
    work_phone_out = fields.Float('Out Work Phone')
    retired_pay = fields.Boolean('Set retired pay')
    army = fields.Boolean('Military service')
    website = fields.Char('Website')
    promotion_date = fields.Date('Date of Promotion')
    family_memb_number = fields.Integer('Number of family members')
    bank_account_num = fields.Integer('Account Number')
    bank_account_name = fields.Char('Account Name')
    emp_education_level = fields.Many2one('hr.education.level', 'Education Level')
    course_ids = fields.One2many('hr.course', 'employee_id', 'Course')
    verify_work_ids = fields.One2many('hr.verify.work', 'employee_id', 'Verify Work Experience Contacts')

    @api.multi
    def name_get(self):
        '''Ажилтны харьяа хэлтэс болон албан тушаалыг авч нэрний ард нэмэх'''
        result = []
        for emp in self:
            name = emp.name
            if emp.last_name:
                name += ' %s' % (emp.last_name,)
                name += ' %s' % (emp.job_id.name if emp.job_id.name else ' ')
            result.append((emp.id, name))
        return result

class HrEmployee_family_line (models.Model):
    _inherit = "hr.employee.family.line"

    cv_id = fields.Many2one('hr.applicant', 'Application')
    applicant_ssnid = fields.Char('Applicant ssnid')
    date_of_birth = fields.Date('Date of Birth')


class HrEmployeeAccountSettings (models.Model):
    _inherit = "res.config.settings"
    _name = "hr.employee.account.settings"

    company_id = fields.Many2one('res.company', string='Company')
