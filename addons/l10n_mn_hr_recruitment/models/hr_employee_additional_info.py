# -*- coding: utf-8 -*-

from odoo import fields, models


class HrWorkSkill (models.Model):
    _inherit = "hr.work.skill"


class HrEducationLevel(models.Model):
    _name = "hr.education.level"
    _description = "Education Level"
    name = fields.Char('Education Level')


class HrCertificate (models.Model):
    _inherit = "hr.certificate"


class HrEmployment (models.Model):
    _inherit = "hr.employment"

    phone_number = fields.Char('Phone number')
    applicant_ssnid = fields.Char('Applicant ssnid')
    cv_id = fields.Many2one('hr.applicant', 'Application')
    total_paused = fields.Float('Total Paused')


class HrPrize (models.Model):
    _inherit = "hr.prize"


class HrEducation (models.Model):
    _inherit = "hr.education"

    cv_id = fields.Many2one('hr.applicant', 'Application')
    applicant_ssnid = fields.Char('Applicant ssnid')
    if_graduated = fields.Boolean('Graduated or not')
    foreign = fields.Boolean('Foreign School')
    diploma_num = fields.Char('Diploma Code')
    thesis = fields.Char('Graduation Thesis Theme')
    education_level = fields.Many2one('hr.education.level', 'Education Level')
    school_id = fields.Many2one('hr.school', string='School', required=False)


class HrSchool (models.Model):
    _inherit = "hr.school"


class HrInterviewers (models.Model):
    _name = "hr.interviewers"
    _description = "Applicant Interviewers"

    cv_id = fields.Many2one('hr.applicant', 'Application')
    name = fields.Many2one('res.users', 'Interviewer')


class HrJobClassification (models.Model):
    _name = "hr.job.classification"
    _description = "Job Classification"

    name = fields.Char('Name')


class HrLanguage (models.Model):
    _inherit = 'hr.language'

    cv_id = fields.Many2one('hr.applicant', 'Application')
    applicant_ssnid = fields.Char('Applicant ssnid')
    duration = fields.Char('Duration')


class HrSoftwareSkill (models.Model):
    _inherit = "hr.software.skill"

    cv_id = fields.Many2one('hr.applicant', 'Application')
    applicant_ssnid = fields.Char('Applicant ssnid')
