# -*- coding: utf-8 -*-

from odoo import api, fields, models, exceptions, _
from odoo.exceptions import UserError


class HrJob (models.Model):
    _inherit = "hr.job"

    @api.multi
    def _count_employee(self):
        res = {}
        employee_id = []
        self = self.env['hr.employee']
        obj = self.env['hr.job']
        for job in obj:
                employee_id = self.search([('job_id', '=', job.id)])
                res[job.id] = len(employee_id)
        return res

    count_employee = fields.Integer(compute=_count_employee, string="Number of Employees")
    no_of_employee = fields.Integer(compute=_count_employee)
    active = fields.Boolean('Active', default=True,
                            help="If unchecked, it will allow you to hide the job without removing it.")

    @api.model
    def view_employee(self):
        mod_obj = self.env['ir.model.data']
        dummy, action_id = tuple(mod_obj.get_object_reference('hr', 'open_view_employee_list_my'))
        action = self.env['ir.actions.act_window'].read(action_id)
        employee_ids = []
        self = self.env['hr.employee']
        obj = self

        for job in obj:
            employee_id = self.search([('job_id', '=', job.id)])
            if employee_id != []:
                for e in employee_id:
                    employee_ids.append(e)
        action['context'] = {}
        if len(employee_ids) > 1 or len(employee_ids) == 0:
            action['domain'] = "[('id','in',[" + ','.join(map(str, employee_ids)) + "])]"
        else:
            res = mod_obj.get_object_reference('hr', 'view_employee_form')
            action['views'] = [(res and res[1] or False, 'form')]
            action['res_id'] = employee_ids and employee_ids[0] or False
        return action

    @api.multi
    def write(self, vals):
        emp_obj = self.env['hr.employee']
        employee_ids = emp_obj.search([('active', '=', True)])
        if 'active' in vals:
            for obj in self:
                if obj.active:
                    for employee in employee_ids:
                        if employee.job_id.id == obj.id:
                            raise UserError(
                                _(u'Энэ ажлын байранд харъяалагдах идэвхтэй ажилтан байгаа тул идэвхгүй болгох боломжгүй.'))
                        else:
                            vals['active'] = False
                            vals['website_published'] = False
                else:
                    vals['active'] = True
        return super(HrJob, self).write(vals)


class HrJobType (models.Model):
    _name = "hr.job.type"
    _description = "Job Type"

    name = fields.Char('Job Type')
    job_id = fields.Many2one('hr.job', 'Job')
