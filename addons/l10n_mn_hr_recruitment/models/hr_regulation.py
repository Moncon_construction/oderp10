# -*- coding: utf-8 -*-

from odoo import fields, models


class HrRegulation(models.Model):
    _inherit = 'hr.regulation'

    applicant_id = fields.Many2one('hr.applicant', 'Application')
    verify_user = fields.Many2one('res.users', 'Decision Maker')


class HrRegulationType(models.Model):
    _inherit = 'hr.regulation.type'

    applicant_id = fields.Many2one('hr.applicant', 'Application')
