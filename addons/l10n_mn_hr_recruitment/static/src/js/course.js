$(document).ready(function () {
    var course_counter = 1;

    $("#addrow-course").on("click", function () {
        var course_newRow = $("<tr>");
        var course_cols = "";

        course_cols += '<td><input type="text" class="form-control" name="course_' + course_counter + '_1"/></td>';
        course_cols += '<td><input type="text" class="form-control" name="course_' + course_counter + '_2"/></td>';
        course_cols += '<td><input type="date" class="form-control" name="course_' + course_counter + '_3"/></td>';
        course_cols += '<td><input type="text" class="form-control" name="course_' + course_counter + '_4"/></td>';
        course_cols += '<td><input type="text" class="form-control" name="course_' + course_counter + '_5"/></td>';
        course_cols += '<td><input type="button" class="ibtnDel-language btn btn-md btn-danger "  value="Устгах"></td>';

        course_newRow.append(course_cols);
        $("table.order-list-course").append(course_newRow);
        course_counter++;
    });



    $("table.order-list-course").on("click", ".ibtnDel-course", function (event) {
        $(this).closest("tr").remove();
        course_counter -= 1
    });


});