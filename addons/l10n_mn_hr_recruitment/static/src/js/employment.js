$(document).ready(function () {
    var employment_counter = 1;

    $("#addrow-employment").on("click", function () {
        var employment_newRow = $("<tr>");
        var employment_cols = "";

        employment_cols += '<td><input type="text" class="form-control" name="employment_' + employment_counter + '_1"/></td>';
        employment_cols += '<td><input type="text" class="form-control" name="employment_' + employment_counter + '_2"/></td>';
        employment_cols += '<td><input type="date" class="form-control" name="employment_' + employment_counter + '_3"/></td>';
        employment_cols += '<td><input type="date" class="form-control" name="employment_' + employment_counter + '_4"/></td>';
        employment_cols += '<td><input type="text" class="form-control" name="employment_' + employment_counter + '_5"/></td>';
        employment_cols += '<td><input type="text" class="form-control" name="employment_' + employment_counter + '_6"/></td>';
        employment_cols += '<td><input type="button" class="ibtnDel-employment btn btn-md btn-danger "  value="Устгах"></td>';

        employment_newRow.append(employment_cols);
        $("table.order-list-employment").append(employment_newRow);
        employment_counter++;
    });



    $("table.order-list-employment").on("click", ".ibtnDel-employment", function (event) {
        $(this).closest("tr").remove();
        employment_counter -= 1
    });


});