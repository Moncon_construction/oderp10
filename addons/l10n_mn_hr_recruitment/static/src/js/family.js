$(document).ready(function () {
    var family_counter = 1;

    $("#addrow-family").on("click", function () {
        var family_newRow = $("<tr>");
        var family_cols = "";
        var inner_html = document.getElementById("family_lines").innerHTML

        family_cols += '<td><select name="family_' + family_counter + '_1">' + inner_html + '</option></t></select></td>';
        family_cols += '<td><input type="text" class="form-control" name="family_' + family_counter + '_2"/></td>';
        family_cols += '<td><input type="date" class="form-control" name="family_' + family_counter + '_3"/></td>';
        family_cols += '<td><input type="text" class="form-control" name="family_' + family_counter + '_4"/></td>';
        family_cols += '<td><input type="text" class="form-control" name="family_' + family_counter + '_5"/></td>';
        family_cols += '<td><input type="text" class="form-control" name="family_' + family_counter + '_6"/></td>';
        family_cols += '<td><input type="text" class="form-control" name="family_' + family_counter + '_7"/></td>';
        family_cols += '<td><input type="button" class="ibtnDel-family btn btn-md btn-danger "  value="Устгах"></td>';

        family_newRow.append(family_cols);
        $("table.order-list-family").append(family_newRow);
        family_counter++;
    });



    $("table.order-list-family").on("click", ".ibtnDel-family", function (event) {
        $(this).closest("tr").remove();
        family_counter -= 1
    });


});