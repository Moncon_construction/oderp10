$(document).ready(function () {
    var language_counter = 1;

    $("#addrow-language").on("click", function () {
        var language_newRow = $("<tr>");
        var language_cols = "";
        var inner_html = document.getElementById("language_line_ids").innerHTML

        language_cols += '<td><select name="language_' + language_counter + '_1">' + inner_html + '</option></t></select></td>';
        language_cols += '<td><select name="language_' + language_counter + '_2"><option><t>Анхан</t></option><option><t>Дунд</t></option><option><t>Гүнзгий</t></option></select></td>';
        language_cols += '<td><select name="language_' + language_counter + '_3"><option><t>Анхан</t></option><option><t>Дунд</t></option><option><t>Гүнзгий</t></option></select></td>';
        language_cols += '<td><select name="language_' + language_counter + '_4"><option><t>Анхан</t></option><option><t>Дунд</t></option><option><t>Гүнзгий</t></option></select></td>';
        language_cols += '<td><select name="language_' + language_counter + '_5"><option><t>Анхан</t></option><option><t>Дунд</t></option><option><t>Гүнзгий</t></option></select></td>';
        language_cols += '<td><input type="button" class="ibtnDel-language btn btn-md btn-danger "  value="Delete"></td>';

        language_newRow.append(language_cols);
        $("table.order-list-language").append(language_newRow);
        language_counter++;
    });



    $("table.order-list-language").on("click", ".ibtnDel-language", function (event) {
        $(this).closest("tr").remove();
        language_counter -= 1
    });


});