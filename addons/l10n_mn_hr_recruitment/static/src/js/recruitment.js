$(document).ready(function () {
    var counter = 1;

    $("#addrow-education").on("click", function () {
        var newRow = $("<tr>");
        var cols = "";
        var inner_html = document.getElementById("education_line_ids").innerHTML
        var inner_html2 = document.getElementById("school_line_ids").innerHTML

        cols += '<td><select name="education_' + counter + '_1">' + inner_html2 + '</option></t></select></td>';;
        cols += '<td><input type="text" class="form-control" name="education_' + counter + '_2" required=""/></td>';
        cols += '<td><select name="education_' + counter + '_3">' + inner_html + '</option></t></select></td>';
        cols += '<td><input type="date" class="form-control" name="education_' + counter + '_4" required=""/></td>';
        cols += '<td><input type="date" class="form-control" name="education_' + counter + '_5" required=""/></td>';
        cols += '<td><input type="text" class="form-control" name="education_' + counter + '_6" required=""/></td>';
        cols += '<td><input type="text" class="form-control" name="education_' + counter + '_7" required=""/></td>';
        cols += '<td><input type="text" class="form-control" name="education_' + counter + '_8" required=""/></td>';
        cols += '<td><input type="text" class="form-control" name="education_' + counter + '_9" required=""/></td>';

        cols += '<td><input type="button" class="ibtnDel-education btn btn-md btn-danger "  value="Устгах"></td>';
        newRow.append(cols);
        $("table.order-list-education").append(newRow);
        counter++;
    });

    $("table.order-list-education").on("click", ".ibtnDel-education", function (event) {
        $(this).closest("tr").remove();
        counter -= 1
    });
});