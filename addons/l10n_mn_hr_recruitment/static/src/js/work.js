$(document).ready(function () {
    var work_counter = 1;

    $("#addrow-work").on("click", function () {
        var work_newRow = $("<tr>");
        var work_cols = "";

        work_cols += '<td><input type="text" class="form-control" name="work_' + work_counter + '_1"/></td>';
        work_cols += '<td><input type="text" class="form-control" name="work_' + work_counter + '_2"/></td>';
        work_cols += '<td><input type="text" class="form-control" name="work_' + work_counter + '_3"/></td>';
        work_cols += '<td><input type="text" class="form-control" name="work_' + work_counter + '_4"/></td>';
        work_cols += '<td><input type="button" class="ibtnDel-workverify btn btn-md btn-danger "  value="Устгах"></td>';

        work_newRow.append(work_cols);
        $("table.order-list-work").append(work_newRow);
        work_counter++;
    });



    $("table.order-list-work").on("click", ".ibtnDel-work", function (event) {
        $(this).closest("tr").remove();
        work_counter -= 1
    });


});