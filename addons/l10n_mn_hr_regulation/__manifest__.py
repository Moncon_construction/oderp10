# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
	'name' : "Mongolian HR Regulation",
	'version' : "1.0",
	'depends': ['hr','base','l10n_mn_document'],
	'author' : "Asterisk Technologies LLC",
	'website' : 'http://asterisk-tech.mn',
	'category' : "Mongolian Modules",
	'description' : """

Тушаал, тогтоолын бүртгэлийн функциональ

Тушаал бүртгэхэд ашиглагдана. Хүний нөөцийн менежер батлагдсан тушаалуудыг бүртгэж, баталгаажуулах бөгөөд тухайн тушаалыг
харах шаардлагатай хүмүүсийг  дагагчаар нэмнэ. 
Хүний нөөцийн ажилтан цэсэн дэх цалин, зардал таб дахь сахилга шийтгэл дээр тушаал сонгож болно.

""",
	'data': [
		'data/import_old_regulation_employees_cron.xml',
	    'email_templates/email_template.xml',
	    'security/regulation_security.xml',
	    'security/ir.model.access.csv',
	    'security/regulation_rule.xml',
	    'views/sequence_view.xml',
		'views/hr_regulation_view.xml',
		'views/hr_view.xml',
		'views/hr_employee_views.xml',
	    'views/menu_view.xml',
    ],
    'demo': [
    ],
}