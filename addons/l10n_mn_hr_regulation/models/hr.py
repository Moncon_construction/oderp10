# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/,
# http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

from odoo import api, fields, models, _

class HrBenefit(models.Model):
    _inherit = 'hr.benefit'

    regulation_id = fields.Many2one('hr.regulation', string='Regulation')


class HrAward(models.Model):
    _inherit = 'hr.award'

    regulation_id = fields.Many2one('hr.regulation', string='Regulation')

class HrPunishment(models.Model):
    _inherit = 'hr.punishment'

    regulation_id = fields.Many2one('hr.regulation', string='Regulation')

class HrContract(models.Model):
    _inherit = 'hr.contract'

    regulation_id = fields.Many2one('hr.regulation', string='Regulation')

