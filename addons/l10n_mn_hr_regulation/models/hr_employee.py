# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import ValidationError, UserError
from odoo.tools.translate import _

class HrEmployee(models.Model):
    _inherit = "hr.employee"
    
    regulation_count = fields.Integer(compute="_regulation_count", string='Linked Regulation Count')
    
    def _regulation_count(self):
        for obj in self:
            linked_regulations = self.env['hr.regulation'].search([('employee_ids', '=', obj.id)])
            obj.regulation_count = len(linked_regulations) if linked_regulations else 0