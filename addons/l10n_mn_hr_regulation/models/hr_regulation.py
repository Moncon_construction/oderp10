# -*- coding: utf-8 -*-
import itertools
import psycopg2
import base64
from StringIO import StringIO
from docx import Document
import zipfile
import os

import odoo.addons.decimal_precision as dp

from odoo import api, fields, models
from odoo.exceptions import ValidationError, UserError
from odoo.tools.translate import _
from datetime import date, datetime

class RegulationTypeFields(models.Model):
    _name = 'regulation.type.fields'
    
    name = fields.Char(string='name', required=True)
    field_id = fields.Many2one('ir.model.fields', string='Field', required=True, domain = "[('model_id.model','in',['hr.regulation'])]")
    string_path = fields.Char('First String Path')
    last_path = fields.Char('Last String Path')
    type_id = fields.Many2one('hr.regulation.type', string='Category')

class HrRegulationType(models.Model):
    _name = 'hr.regulation.type'

    name = fields.Char('Regulation Name', required=True)
    type = fields.Char('Regulation Type')
    category = fields.Selection(
        [('a', u'А'), ('b', u'Б')], 'Category', default='b')
    attachment_id = fields.Many2one('ir.attachment', string='Template file', domain="[('mimetype','=','application/vnd.openxmlformats-officedocument.wordprocessingml.document')]")
    field_ids = fields.One2many('regulation.type.fields', 'type_id', string='Fields')

class HrRegulationEmployee(models.Model):
    _name = 'hr.regulation.employee'
    
    employee = fields.Many2one('hr.employee', string='Employee', required=True)
    start_date = fields.Date('Start Date of Implementation', default=fields.Date.context_today)
    end_date = fields.Date('End Date of Implementation', default=fields.Date.context_today)
    regulation = fields.Many2one('hr.regulation', string='Linked Regulation')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id)

class HrRegulation(models.Model):
    _name = 'hr.regulation'
    _inherit = 'mail.thread'
    _order = 'reg_date DESC'

    @api.multi
    def information(self):
        info = _('KEEP THE CONFIDENTIALITY OF THE DOCUMENT INFORMATION !!!')
        return info

    name = fields.Char('Name', required=True, states={
        'confirmed': [('readonly', True)]})
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id)
    reg_number = fields.Char('Number')
    regulation_type_id = fields.Many2one('hr.regulation.type', 'Regulation type', states={
                                         'confirmed': [('readonly', True)]})
    reg_date = fields.Date('Regulation verified date', default=fields.Date.context_today, required=True, states={
                           'confirmed': [('readonly', True)]})
    general_start_date = fields.Date('General Start Date', default=fields.Date.context_today)
    general_end_date = fields.Date('General End Date')
    created_employee_id = fields.Many2one(
        'res.users', 'Created Employee', default=lambda self: self.env.uid, readonly=True)
    employee_ids = fields.Many2many('hr.employee', 'regulation_emp_rel', 'reg_id', 'emp_id', 'Hr Employee')
    employees = fields.One2many(
        'hr.regulation.employee', 'regulation', 'Employee')
    old_employees = fields.Many2many(
        'hr.employee', 'regulation_employee_rel', 'regulation_id', 'employee_id', 'Multiple Employee')
    employee_id = fields.Many2one('hr.employee', 'Employee', states={
                                  'confirmed': [('readonly', True)]})
    employees_name = fields.Char('Employees Name')
    notification = fields.Char(
        'Notification', default=information, readonly=True)
    state = fields.Selection([('draft', 'Draft'), ('confirmed', 'Confirmed')],
                             readonly=True, default='draft', copy=False, string="State")
    description = fields.Html('Description', states={
        'confirmed': [('readonly', True)]})
    active = fields.Boolean('Is Active', default= True)
    type_is_a = fields.Boolean('Is Type A')
    binary_attachment = fields.Binary('File')
    file_name = fields.Char('File Name')
    
    @api.multi
    def write(self, vals):
        if vals.get('employee_ids'):
            name = ''
            for emp in vals.get('employee_ids'):
                if len(emp) == 3:
                    for employee in self.env['hr.employee'].browse(emp[2]):
                        name += '%s, ' % employee.name
            vals['employees_name'] = name
        return super(HrRegulation, self).write(vals)

    @api.model
    def create(self, vals):
        if vals.get('employee_ids'):
            name = ''
            for emp in vals.get('employee_ids'):
                if len(emp) == 3:
                    for employee in self.env['hr.employee'].browse(emp[2]):
                        name += '%s, ' % employee.name
            vals['employees_name'] = name
            
        new_reg = super(HrRegulation, self).create(vals)
        return new_reg

    @api.onchange('general_start_date')
    def set_employees_start_date(self):
        for obj in self:
            if obj.general_start_date:
                for emp in obj.employees:
                    emp.start_date = obj.general_start_date
            
# Тушаал дээрхи ажилтан заавал хэрэглэгчтэй холбогдсон байх албагүй, логикийн алдаатай тул доорхи кодыг болиулав.
    @api.multi
    def send_notification(self):
        sel_user_ids = {}
        if self.employees:
            for line in self.employees:
                if line.employee.user_id:
                    sel_user_ids[line.employee.user_id.id] = line.employee.user_id.partner_id.email

        self.ensure_one()
        template = self.env.ref('l10n_mn_hr_regulation.regulation_email_template_mn', False)
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        dbname = self.env.cr.dbname
        action_id = self.env.ref('l10n_mn_hr_regulation.action_hr_regulation_at_document').id
        for user_id, partner in sel_user_ids.items():
            ctx = {
                'base_url': base_url,
                'db_name': dbname,
                'id': self.id,
                'action_id': action_id,
                'mails': partner,
                'name': self.name,
                'reg_date': self.reg_date
            }
            self.env['mail.template'].sudo().browse(template.id).with_context(ctx).send_mail(user_id, force_send=True)

    @api.one
    def action_to_confirm(self):
        self.send_notification()
        if self.regulation_type_id.category == 'b':
            self.write({'reg_number': self.env['ir.sequence'].next_by_code('hr.regulation')})
        return self.write({'state': 'confirmed'})

    @api.one
    def action_to_draft(self):
        return self.write({'state': 'draft'})

    @api.multi
    def unlink(self):
        for obj in self:
            if obj.state not in ('draft'):
                raise UserError(
                    _('You cannot delete an regulation which is not draft.'))

        return super(HrRegulation, self).unlink()

    @api.multi
    def archive(self):
        for obj in self:
            obj.active = False
    
    @api.multi
    def release_archive(self):
        for obj in self:
            obj.active = True
        
    def _import_old_regulation_employees(self):
        all_regulations = self.env['hr.regulation'].search([])
        for obj in all_regulations:
            # Өмнөх employees = fields.One2many('hr.regulation.employee', 'regulation', 'Employee') -с үүсгэх
            self._cr.execute(
                """
                    SELECT employee_id AS id FROM regulation_employee_rel WHERE regulation_id = %s
                """ %obj.id
            )
             
            existen_emps_from_m2m = self._cr.dictfetchall() 
            if existen_emps_from_m2m and len(existen_emps_from_m2m) > 0:
                for emp_id in existen_emps_from_m2m:
                    if 'id' in emp_id.keys() and emp_id['id']:
                        self.env['hr.regulation.employee'].create({
                            'employee': emp_id['id'],
                            'start_date': obj.reg_date or obj.general_start_date,
                            'end_date': obj.reg_date,
                            'regulation': obj.id,
                            'company_id': obj.company_id.id if obj.company_id else False
                        })
             
            # Өмнөх employee_id = fields.Many2one('hr.employee', 'Employee', states={'confirmed': [('readonly', True)]}) -с үүсгэх
            existen_emp_from_m2o = self._cr.execute(
                """
                    SELECT employee_id AS id FROM hr_regulation WHERE id = %s
                """ %obj.id
            )
            existen_emp_from_m2o = self._cr.dictfetchall() 
            if existen_emp_from_m2o and len(existen_emp_from_m2o) > 0:
                emp_id = existen_emp_from_m2o[0]
                if 'id' in emp_id.keys() and emp_id['id']:
                    self.env['hr.regulation.employee'].create({
                        'employee': existen_emp_from_m2o[0]['id'],
                        'start_date': obj.reg_date or obj.general_start_date,
                        'end_date': obj.reg_date,
                        'regulation': obj.id,
                        'company_id': obj.company_id.id if obj.company_id else False
                    })       

        # set cron unactive & delete 
        self._cr.execute(
            """
            UPDATE ir_cron SET active = False WHERE name = 'Import OLD Regulation Employees';
            DELETE FROM ir_cron WHERE name = 'Import OLD Regulation Employees';
            """
        )
        
    @api.multi
    def action_print(self):
        context = self._context     
        report_type='.docx'
        for regulation in self:
            for exp in regulation.employees:
                if regulation.regulation_type_id.attachment_id.datas:
                    report ,template_name= self.create_doc_source(regulation)
                    out = base64.encodestring(report)
                    
    
                    excel_id = self.env['oderp.report.excel.output'].create( {
                    'filedata':out,
                    'filename':template_name + report_type,
                    })
                    return {
                            'name': 'Export Report',
                            'view_type':'form',
                            'view_mode':'form',
                            'res_model':'oderp.report.excel.output',
                            'res_id':excel_id.id,
                            'view_id':False,
                            'context':self._context,
                            'type': 'ir.actions.act_window',
                            'target':'new',
                            'nodestroy': True,
                            }
                else:
                    raise UserError(_('There is no attachment files!'))

    def create_doc_source(self ,regulation_id):
        tmp_folder_name = '/tmp/docx_to_pdf/'
        self._delete_temp_folder(tmp_folder_name)
        self._create_temp_folder(tmp_folder_name)
        convert_path, template_name = self.convert_docx_from_base(tmp_folder_name,regulation_id)
        report = self._get_convert_file(convert_path )
        return report ,template_name

    @api.multi
    def convert_docx_from_base(self,tmp_folder_name,regulation_id):
        docx_template_name = 'template_1.docx'
        template_path = tmp_folder_name + docx_template_name
        name = regulation_id.regulation_type_id.attachment_id.name
        convert_path = tmp_folder_name + 'template.docx'
        doc= base64.decodestring(regulation_id.regulation_type_id.attachment_id.datas)
        data=StringIO(doc)
        data = Document(data)
        data.save(template_path)

        replaceText = {}
        for field in regulation_id.regulation_type_id.field_ids:
            if field.field_id.model_id.model=='hr.regulation':
                desc = False
                for con in regulation_id:
                    if field.field_id.name in con:
                        if field.field_id.ttype=='many2one':
                            if field.string_path:
                                model_id = self.env['ir.model'].search([('model','=',field.field_id.relation)])
                                f_id = self.env['ir.model.fields'].search([('name','=',field.string_path),('model_id','=',model_id.id)])
                                if f_id.ttype=='many2one':
                                    if field.last_path:
                                        model_id2 = self.env['ir.model'].search([('model','=',f_id.relation)])
                                        f_id2 = self.env['ir.model.fields'].search([('name','=',field.last_path),('model_id','=',model_id2.id)])
                                        if f_id2.ttype=='many2one':
                                            desc = con[field.field_id.name][field.string_path][field.last_path].name if con[field.field_id.name][field.string_path][field.last_path].name else '         '
                                        else:
                                            desc = con[field.field_id.name][field.string_path][field.last_path] if con[field.field_id.name][field.string_path][field.last_path] else '         '
                                    else:
                                        desc = con[field.field_id.name][field.string_path].name if con[field.field_id.name][field.string_path].name else '         '
                                else:
                                    desc = con[field.field_id.name][field.string_path] if con[field.field_id.name][field.string_path] else '         '
                            else:
                                desc = con[field.field_id.name].name if con[field.field_id.name].name else '         '
                        elif field.field_id.ttype=='one2many' and field.field_id.relation == 'hr.regulation.employee':
                            if field.string_path:
                                model_id = self.env['ir.model'].search([('model','=','hr.regulation.employee')])
                                f_id = self.env['ir.model.fields'].search([('name','=',field.string_path),('model_id','=',model_id.id)])
                                if f_id.ttype=='many2one':
                                    if field.last_path:
                                        model_id2 = self.env['ir.model'].search([('model','=',f_id.relation)])
                                        f_id2 = self.env['ir.model.fields'].search([('name','=',field.last_path),('model_id','=',model_id2.id)])
                                        if f_id2.ttype=='many2one':
                                            desc = con[field.field_id.name][field.string_path][field.last_path].name if con[field.field_id.name][field.string_path][field.last_path].name else '         '
                                        else:
                                            desc = con[field.field_id.name][field.string_path][field.last_path] if con[field.field_id.name][field.string_path][field.last_path] else '         '
                                    else:
                                        desc = con[field.field_id.name][field.string_path].name if con[field.field_id.name][field.string_path].name else '         '
                                else:
                                    desc = con[field.field_id.name][field.string_path] if con[field.field_id.name][field.string_path] else '         '
                            else:
                                desc = con[field.field_id.name].name if con[field.field_id.name].name else '         '
                        else:
                            if field.name == '[date]':
                                desc = str(datetime.strptime(con[field.field_id.name], "%Y-%m-%d").year) + ' оны ' + str(datetime.strptime(con[field.field_id.name], "%Y-%m-%d").month) + ' сарын ' + str(datetime.strptime(con[field.field_id.name], "%Y-%m-%d").day) + '-ны өдөр'
                            else:
                                desc = con[field.field_id.name]
                        strss = desc.encode(encoding='UTF-8') if type(desc) in (str, unicode) else desc
                        replaceText.update({field.name.encode(encoding='UTF-8') : strss })
        self.create_doc(template_path,name,tmp_folder_name, convert_path,replaceText)
        return convert_path , name

    @api.multi
    def create_doc(self,template_file,name, tmp_folder_name,out_file, replaceText):
        templateDocx = zipfile.ZipFile(template_file)
        newDocx = zipfile.ZipFile(out_file, "w")
        for file in templateDocx.filelist:
            content = templateDocx.read(file)
            for key in replaceText.keys():
                content = content.replace(str(key), str(replaceText[key]))
            newDocx.writestr(file.filename, content)
        templateDocx.close()
        newDocx.close()
        return newDocx

    def _get_convert_file(self, convert_path):
        input_stream = open(convert_path, 'r')
        try:
            report = input_stream.read()
        finally:
            input_stream.close()

        return report

    def _convert_docx_to_pdf(self, tmp_folder_name,convert_docx_file_name):
            cmd = "soffice --headless --convert-to pdf --outdir " +convert_docx_file_name +" "+ tmp_folder_name 
            os.system(cmd)

    def _create_temp_folder(self, tmp_folder_name):
        if not os.path.exists(tmp_folder_name):
            os.makedirs(tmp_folder_name)
            
    def _delete_temp_folder(self, tmp_folder_name):
        cmd = 'rm -rf ' + tmp_folder_name
        os.system(cmd)
        if os.path.exists(tmp_folder_name):
            os.rmdir(tmp_folder_name)
    
class EmployeeMove(models.Model):
    _inherit = 'employee.move'

    regulation_id = fields.Many2one('hr.regulation', 'Regulation')

class EmployeeContract(models.Model):
    _inherit = 'hr.contract'

    regulation_id = fields.Many2one('hr.regulation', 'Regulation')
