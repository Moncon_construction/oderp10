# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
	'name' : "Mongolian HR Regulation Task",
	'version' : "1.0",
	'depends': ['project','l10n_mn_hr_regulation'],
	'author' : "Asterisk Technologies LLC",
	'website' : 'http://asterisk-tech.mn',
	'category' : "Mongolian Modules",
	'description' : """

Тушаал, тогтоолын бүртгэлд төсөлийн даалгавар үүсгэх функциональ

Тушаал бүртгээд төсөлийн даалгавар ашиглагдана.

""",
	'data': [
	    'security/ir.model.access.csv',
		'views/project_task_view.xml',
		'views/hr_regulation_view.xml'
    ],
    'demo': [
    ],
}
