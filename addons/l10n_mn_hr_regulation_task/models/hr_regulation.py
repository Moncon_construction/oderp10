# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.exceptions import ValidationError, UserError
from odoo.tools.translate import _


class HrRegulation(models.Model):
    _inherit = 'hr.regulation'

    role_line_ids = fields.One2many(
        'hr.regulation.line', 'regulation_id', string='Role')

    task_ids = fields.One2many('project.task', 'partner_id', string='Task name')
    task_count = fields.Integer(compute='_compute_task_count', string='# Tasks')

    @api.multi
    def _compute_task_count(self):
        for contract in self:
            contract.task_count = len(self.env['project.task'].sudo().search([('regulation_id','=',self.id)])) or 0
        self.task_count = contract.task_count

    
    @api.one
    def action_to_confirm(self):
        reg = super(HrRegulation, self).action_to_confirm()
        employee_obj = self.env['hr.employee']
        task_obj = self.env['project.task']
    
        for line in self.role_line_ids:
            wr_vals = {}
            if not line.task_id:
                vals = {'name': line.rolename,
                        'user_id': line.respondent.user_id.id,
                        'date_deadline': line.date,
                        'description': self.description,
                        'regulation_id': self.id}

                task_id = task_obj.create(vals)
        
                line.task_id = task_id.id
            else:
                task = task_obj.search([('id', '=', line.task_id.id)])
                if line.rolename != task.name:
                    wr_vals['name'] = line.rolename
                if line.respondent.user_id.id != task.user_id:
                    wr_vals['user_id'] = line.respondent.user_id.id
                if line.date != task.date_deadline:
                    wr_vals['date_deadline'] = line.date
                if self.description != task.description:
                    wr_vals['description'] = self.description
                if wr_vals:
                    task.write(wr_vals)
        return reg

        
class HrRegulationLine(models.Model):
    _name = 'hr.regulation.line'
    task_id = fields.Many2one('project.task')
    rolename = fields.Char('Role task', required=True)
    respondent = fields.Many2one('hr.employee', string='Respondent', required = True)
    date = fields.Date('Date', default=fields.Date.context_today)
    regulation_id = fields.Many2one('hr.regulation')

