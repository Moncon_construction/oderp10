# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.exceptions import ValidationError, UserError
from odoo.tools.translate import _

class InheritedProjecTask(models.Model):
    _inherit = 'project.task'

    regulation_id = fields.Many2one('hr.regulation', 'Regulation name', readonly=True)
