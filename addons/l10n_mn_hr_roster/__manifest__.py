# -*- coding: utf-8 -*-

# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': "Mongolian HR Roster",
    'version': "1.0",
    'depends': [
        'l10n_mn_hr_attendance_hour_balance'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'description': """

Ростерийн бүртэл

""",
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/menu.xml',
        'views/hr_shift_views.xml',
        'views/hr_shift_lunch_time_views.xml',
        'wizard/roster_import_view.xml',
        'views/roster_views.xml',
        'views/hour_balance_view.xml',
        'views/hr_attendance_view.xml',
        'wizard/hour_balance_import_view.xml',
        'wizard/change_shift_views.xml',
    ],
}
