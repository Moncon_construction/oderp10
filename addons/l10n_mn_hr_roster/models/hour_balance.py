# -*- coding: utf-8 -*-

# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime, timedelta

import pytz
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models, _
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as dsdf
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as dsdtf


class HourBalance(models.Model):
    _inherit = 'hour.balance'

    @api.multi
    def calc_reg_day(self, date_from, date_end, employee, contract, is_balance=False):
        """
            Ээлжийн ажилтны ажиллавал зохих өдөр/цаг буцаах функц
        """
        res = {}
        reg_day = 0
        leaves = []
        rosters = self.env['hr.roster.line'].search([
#             ('roster_id.state', '=', 'confirm'),
            ('roster_id.balance_id', 'in', [self.advance_salary_id.id, self.id]),
            ('employee_id', '=', employee.id)])
        if contract.hour_balance_calculate_type == 'payroll_by_roster':
            reg_objs = self.env['hr.roster.employee.line'].search([('line_id', 'in', rosters.ids),
                                                                   ('date', '>=', date_from),
                                                                   ('date', '<=', date_end)])
            reg_hour = 0.0
            for r_obj in reg_objs:
                if r_obj.hr_shift_id:
                    reg_hour += r_obj.hr_shift_id.shift_hour
                    reg_day += 1 * (r_obj.hr_shift_id.shift_day or 1)
                    
            if employee.contract_id.working_hours:
                start_dt = datetime.strptime(date_from[:10], dsdf)
                end_dt = datetime.strptime(date_end[:10], dsdf)
                leaves = employee.contract_id.working_hours.get_leave_intervals(resource_id=employee.contract_id.working_hours.id, start_datetime=start_dt, end_datetime=end_dt)
            
            res.update({'working_day': reg_day})
            res.update({'working_hour': reg_hour})
            res.update({'employee_leave': len(leaves)})
        
        else:
            res = super(HourBalance, self).calc_reg_day(date_from, date_end, employee, contract, is_balance=is_balance)
        
        return res

    def get_total_attendance_hours_days(self, employee, date_from, date_to):
        self.ensure_one()
        if not employee.contract_id.hour_balance_calculate_type == 'payroll_by_roster':
            return super(HourBalance, self).get_total_attendance_hours_days(employee, date_from, date_to)
        is_night = False
        total_hours, total_days = 0, 0 
        
        atts = self.get_total_attendances(employee,date_from, date_to)
        if atts and len(atts) > 0:
            for attendance in self.get_total_attendances(employee,date_from, date_to):
                shift_day = attendance.roster_emp_line.hr_shift_id.shift_day if (attendance.roster_emp_line and attendance.roster_emp_line.hr_shift_id and attendance.roster_emp_line.hr_shift_id.shift_day > 0) else 1
                total_hours += attendance.total_attendance_hours
                if attendance.check_worked_hours_can_be_day():
                    total_days += 1 * shift_day
            
        return total_hours, total_days
    
    def get_linked_objects(self, object, employee, st_date, end_date):
        if object != 'hr.roster.employee.line':
            return super(HourBalance, self).get_linked_objects(object, employee, st_date, end_date)
        
        balance_ids = [balance.id for balance in self]
        for balance in self:
            if balance.advance_salary_id:
                balance_ids.append(balance.advance_salary_id.id)
        roster = self.env['hr.roster.line'].search([('employee_id', '=', employee.id), ('roster_id.balance_id', 'in', balance_ids)])
        domain = [('line_id', 'in', roster.ids)]
        domain.extend(get_duplicated_day_domain('from_time', 'to_time', st_date, end_date, self.env.user))
                
        return self.env[object].sudo().search(domain)
    
    def get_linked_object_time_intervals(self, object, employee, st_date, end_date):
        if object != 'hr.roster.employee.line':
            return super(HourBalance, self).get_linked_object_time_intervals(object, employee, st_date, end_date)
        
        time_intervals = []
        st_date = str(get_display_day_to_user_day(st_date, self.env.user))
        end_date = str(get_display_day_to_user_day(end_date, self.env.user))
        linked_objects = self.get_linked_objects(object, employee, st_date, end_date)
        for obj in linked_objects:
            time_intervals.extend(obj.get_working_hours_intervals(by_user_timezone=True))
        
        return time_intervals
    
    def get_total_hours_by_object(self, object, employee, contract, date_from, date_to):
        self.ensure_one()
        if not contract.hour_balance_calculate_type == 'payroll_by_roster':
            return super(HourBalance, self).get_total_hours_by_object(object, employee, contract, date_from, date_to)
         
        st_date = str(get_display_day_to_user_day(date_from, self.env.user))
        end_date = str(get_display_day_to_user_day(date_to, self.env.user))
        time_intervals = self.get_linked_object_time_intervals(object, employee, st_date, end_date)
        roster_time_intervals = self.get_linked_object_time_intervals('hr.roster.employee.line', employee, st_date, end_date)
        return get_duplicated_hours_between_intervals(time_intervals, roster_time_intervals)
    
    def get_balance_line_values(self, employee_id, contract_id, check_contract_date=False):
        # @Override: Шөнөөр ажилласан өдөр/цагийг тооцоолох
        self.ensure_one()
        values = super(HourBalance, self).get_balance_line_values(employee_id, contract_id, check_contract_date=check_contract_date)
        
        if contract_id.hour_balance_calculate_type == 'payroll_by_roster':
            date_from, date_to = self.get_balance_dates(contract_id, check_contract_date=check_contract_date)
            if date_from and date_to:
                night_worked_hour, night_worked_day = 0, 0
                attendances = self.get_total_attendances(employee_id, date_from, date_to)
                if attendances and len(attendances) > 0:
                    for att in attendances:
                        if att.roster_emp_line.hr_shift_id.is_night:
                            night_worked_hour += att.total_worked_hours_of_date
                            night_worked_day += 1
                
                values['worked_night_shift_hour'] = max(night_worked_hour, 0)
                values['worked_night_shift_day'] = max(night_worked_day, 0)
                
        return values


class HourBalanceLine(models.Model):
    _inherit = 'hour.balance.line'
    
    worked_night_shift_hour = fields.Float(string='Worked night shift hour', digits=(4,2))
    worked_night_shift_day = fields.Float(string='Worked night shift day', digits=(4,2))
    
    def compute_total_worked_day(self):
        for obj in self:
            obj.total_worked_day = obj.worked_day + obj.celebration_days + obj.working_days_at_weekend + obj.worked_night_shift_day
            
    def compute_total_worked_hour(self):
        for obj in self:
            obj.total_worked_hour = obj.worked_hour + obj.celebration_hours + obj.working_hours_at_weekend + obj.worked_night_shift_hour
    
    def set_values_from_line(self):
        super(HourBalanceLine, self).set_values_from_line()
        for obj in self:
            worked_night_shift_hour, worked_night_shift_day = 0, 0
            for line_id in obj.line_ids:
                worked_night_shift_hour += line_id.worked_night_shift_hour
                worked_night_shift_day += line_id.worked_night_shift_day
            obj.worked_night_shift_hour = worked_night_shift_hour
            obj.worked_night_shift_day = worked_night_shift_day
            
            
class HourBalanceLineLine(models.Model):
    _inherit = 'hour.balance.line.line'
    
    worked_night_shift_hour = fields.Float(string='Worked night shift hour', digits=(4,2))
    worked_night_shift_day = fields.Float(string='Worked night shift day', digits=(4,2))
