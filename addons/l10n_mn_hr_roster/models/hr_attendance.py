# -*- coding: utf-8 -*-
import pytz
from odoo import models, fields, api, _
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError
from dateutil import rrule
from dateutil.relativedelta import relativedelta
from odoo.addons.l10n_mn_web.models.time_helper import *

class HrAttendance(models.Model):
    _inherit = "hr.attendance"
    
    roster_emp_line = fields.Many2one('hr.roster.employee.line', string='Linked Roster Line', compute='compute_roster_emp_line', store=True, readonly=True, ondelete='set null')
    attendance_type = fields.Selection(selection_add=[(('night'), _('Night Shift'))], translate=True)
    attendance_calculation_type = fields.Selection([
        ('like_roster_employee', 'Like Roster Employee'),
        ('like_office_employee', 'Like Office Employee')
    ], string='Attendance Calculation Type', default='like_office_employee', compute='compute_attendance_calculation_type', store=True, readonly=True)
    
    @api.depends('check_in', 'company_id')
    def compute_attendance_type(self):
        res = super(HrAttendance, self).compute_attendance_type()
        for obj in self:
            if obj.roster_emp_line and obj.roster_emp_line.hr_shift_id and obj.roster_emp_line.hr_shift_id.is_night:
                obj.attendance_type = 'night'
        return res
        
    def _get_attendance_calculation_type(self):
        self.ensure_one()
        if self.employee_id:
            if self.employee_id.contract_id:
                if self.employee_id.contract_id.sudo().hour_balance_calculate_type == 'payroll_by_roster':
                    return 'like_roster_employee'
                else:
                    return 'like_office_employee'
            else:
                return False
        else:
            return False

    
    @api.constrains('check_in', 'check_out', 'employee_id')
    def _check_validity(self):
        # @Override: Ээлжийн ажилтны хувьд core-н тухайн өдөртөө ирц давхардахгүй үүсэх ёстой шалгалт нь тохирохгүй байсан тул override хийв.
        super(HrAttendance, self.filtered(lambda x: x._get_attendance_calculation_type() != 'like_roster_employee'))._check_validity()
                    
    def get_lunch_time(self):
        self.ensure_one()
        if (self.employee_id.contract_id and not self.employee_id.contract_id.sudo().hour_balance_calculate_type == 'payroll_by_roster') or not self.work_start_time:
            return super(HrAttendance, self).get_lunch_time()
        
        start_date = get_day_like_display(self.work_start_time or self.check_in, self.env.user)
        roster_emp_line = self.roster_emp_line
        if not roster_emp_line:
            rosters = self.env['hr.roster.line'].search([('employee_id', '=', self.employee_id.id)])
            roster_emp_line = self.env['hr.roster.employee.line'].search([('line_id', 'in', rosters.ids), ('date', '=', start_date.date())], limit=1)
        lunch_hour = 0.0

        if roster_emp_line and (self.check_in or self.check_out):
            check_in = datetime.strptime(self.check_out[:19], DEFAULT_SERVER_DATETIME_FORMAT) + relativedelta(minutes=-1) if not self.check_in else self.check_in
            check_out = datetime.strptime(self.check_in[:19], DEFAULT_SERVER_DATETIME_FORMAT) + relativedelta(minutes=1) if not self.check_out else self.check_out
            check_in = get_day_like_display(check_in, self.env.user)
            check_out = get_day_like_display(check_out, self.env.user)
            lunch_hour = roster_emp_line.with_context({'date_interval': [(check_in, check_out)]}).get_lunch_hours()

        return lunch_hour
    
    @api.depends('employee_id', 'contract_id', 'check_in')
    def compute_roster_emp_line(self):
        for obj in self:
            reg_obj = False
            if obj.employee_id.contract_id and obj.employee_id.contract_id.sudo().hour_balance_calculate_type == 'payroll_by_roster':
                check_in = get_day_like_display(obj.check_in, self.env.user).date()
                reg_obj = self.env['hr.roster.employee.line'].search([('line_id.employee_id', '=', obj.employee_id.id), ('date', '=', check_in)], limit=1)
            obj.roster_emp_line = reg_obj
            
    @api.depends('work_start_time', 'work_end_time', 'check_in', 'working_hours')
    def compute_work_hour_of_date(self):
        roster_atts = self.filtered(lambda x: x.employee_id and x.employee_id.contract_id and x.employee_id.contract_id.sudo().hour_balance_calculate_type == 'payroll_by_roster')
        other_atts = self
        if roster_atts:
            other_atts -= roster_atts
            for obj in roster_atts:
                work_hour_of_date = 0
                if obj.work_start_time and obj.work_end_time and not obj.check_day_is_holiday(get_day_like_display(obj.check_in, self.env.user)):
                    lunch_hours = obj.roster_emp_line.get_lunch_hours() if obj.roster_emp_line else 0
                    work_hour_of_date = get_difference_btwn_2date(obj.work_start_time, obj.work_end_time) - lunch_hours
                obj.work_hour_of_date = work_hour_of_date  
        if other_atts:
            super(HrAttendance, other_atts).compute_work_hour_of_date()
            
    @api.depends('check_in', 'working_hours', 'roster_emp_line')
    def compute_workable_hours(self):
        # Ажил эхлэх болон дуусах цагийн тооцох
        for obj in self:
            if obj.employee_id:
                if obj.employee_id.contract_id and obj.employee_id.contract_id.sudo().hour_balance_calculate_type == 'payroll_by_roster':
                    reg_obj = obj.roster_emp_line
                    
                    obj.work_start_hour = reg_obj.time_from if reg_obj else 0
                    obj.work_end_hour = reg_obj.time_to if reg_obj else 0
                    
                    obj.work_start_time = reg_obj.from_time if reg_obj else False
                    obj.work_end_time = reg_obj.to_time if reg_obj else False
                else:
                    super(HrAttendance, obj).compute_workable_hours()
    
    @api.depends('employee_id')            
    def compute_attendance_calculation_type(self):
        for obj in self:
            if obj.employee_id and obj.employee_id.contract_id and obj.employee_id.contract_id.sudo().hour_balance_calculate_type == 'payroll_by_roster':
                obj.attendance_calculation_type = 'like_roster_employee'
            else:
                obj.attendance_calculation_type = 'like_office_employee'
                
    def check_day_is_holiday(self, date):
        self.ensure_one()
        if not date:
            return True
        if self.employee_id and self.employee_id.contract_id and self.employee_id.contract_id.sudo().hour_balance_calculate_type == 'payroll_by_roster':
            roster = self.env['hr.roster.employee.line'].search([('line_id.employee_id', '=', self.employee_id.id), ('date', '=', date)])
            if roster and len(roster) > 0:
                return False
            return True
        else:
            return super(HrAttendance, self).check_day_is_holiday(date)
        
    def recompute_calculations(self):
        self.compute_attendance_calculation_type()
        super(HrAttendance, self).recompute_calculations()
        
        
