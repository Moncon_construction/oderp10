# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api

class HrContract(models.Model):
    _inherit = 'hr.contract'

    hour_balance_calculate_type = fields.Selection([
                                                    ('payroll_by_attendance', 'Payroll By Attendance'),
                                                    ('is_salary_complete', 'Is Salary Complete'),
                                                    ('payroll_by_roster', 'Payroll By Roster'),
                                                ], string='Hour balance calculate type', copy=False )