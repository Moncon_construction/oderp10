# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class HrShift(models.Model):
    _name = 'hr.shift'
    _description = 'Hr Shift'

    def _compute_shift_hour(self):
        #Ээлжийн ажиллах цагийг тооцоолно.
        for obj in self:
            shift_hour = obj.time_to - obj.time_from
            shift_hour += (24 * obj.shift_day if obj.is_day else 0)
            shift_hour -= sum(obj.mapped('lunch_time_ids').mapped('lasts_hour'))
            obj.shift_hour = shift_hour

    name = fields.Char('Name', required=True)
    company_id = fields.Many2one('res.company', default=lambda self: self.env.user.company_id, ondelete='cascade', string='Company')
    short_name = fields.Char('Short Name', required=True, size=3)
    time_from = fields.Float('Time From', required=True)
    time_to = fields.Float('Time To', required=True)
    color = fields.Integer(string='Color Index')
    is_day = fields.Boolean('Is Day')
    in_date_from = fields.Float('In Date From', required=True)
    in_date_to = fields.Float(related='time_from', string='In Date To', required=True)
    out_date_from = fields.Float(related='time_to', string='Out Date From', required=True)
    out_date_to = fields.Float('Out Date To', required=True)
    shift_hour = fields.Float('Compute Shift Hour', compute=_compute_shift_hour)
    shift_day = fields.Integer('Shift Day', required=True, default=1)
    is_night = fields.Boolean('Is Night Shift')
    active = fields.Boolean('Is Active', default=True)
    lunch_time_ids = fields.One2many('hr.shift.lunch.time', 'shift_id', string='Lunch Time')
    

class HrShiftLunchTime(models.Model):
    _name = 'hr.shift.lunch.time'
    
    name = fields.Char(string='Description')
    shift_id = fields.Many2one('hr.shift', string='Hr Shift', required=True, ondelete='cascade')
    is_day = fields.Boolean(related='shift_id.is_day', store=True, readonly=True, string='Is Day')
    company_id = fields.Many2one('res.company', default=lambda self: self.env.user.company_id, ondelete='cascade', string='Company')
    time_from = fields.Float(string='Time From', required=True)
    time_to = fields.Float(string='Time To', required=False)
    lasts_hour = fields.Float(string='Lasts Hour')
    number_of_day = fields.Integer(default=1, string='Related number of Shift Day', help='Used for calculate lunch time when work through the days')
    
    @api.onchange('number_of_day')
    def check_number_of_day(self):
        if self.filtered(lambda x: x.is_day and x.number_of_day < 1):
            raise ValidationError(_("'Related number of Shift Day' cannot be less than 1, because of it used for identify which work day belongs to this !!!"))
        if self.filtered(lambda x: x.is_day and x.shift_id.shift_day + 1 < x.number_of_day):
            raise ValidationError(_("'Related number of Shift Day' cannot be greater than shift through day !!!"))
        
