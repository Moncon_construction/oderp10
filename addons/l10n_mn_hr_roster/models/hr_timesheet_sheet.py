# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.


import logging
from datetime import datetime, timedelta

from odoo import api, models
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as dsdf
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as dsdtf

_logger = logging.getLogger(__name__)


class HRTimesheetSheet(models.Model):
    _inherit = "hr_timesheet_sheet.sheet"

    @api.one
    def _calc_working_time(self):
        contract_obj = self.env['hr.contract']
        _logger.info("hr_rostrer _calc_working_time() FUNCTION CALLED :: %s" % (self.employee_id.id))
        contracts = contract_obj.search([('state', '=', 'open'), ('employee_id', '=', self.employee_id.id)])
        if contracts.hour_balance_calculate_type == 'payroll_by_roster':
            attendances_obj = self.env['hr.attendance']
            roster_obj = self.env['hr.roster.employee.line']
            attendance_hour = lag_hour = worked_day = 0.0
            company_id = self.env.user.company_id
            attendances = attendances_obj.search([('sheet_id', '=', self.id), ('check_out', '!=', False)])
            if attendances:
                date_from = None
                date_to = None
                for attendance in attendances:
                    date_from = datetime.strptime(attendance.check_in, dsdtf)
                    date_to = datetime.strptime(attendance.check_out, dsdtf)
                    if date_to and date_from and date_to.date() == date_from.date():
                        roster = roster_obj.search([('line_id.employee_id', '=', self.employee_id.id), ('date', '=', date_from.date())], limit=1) # is_work - г ашиглахгүй байгаа тул түр авав
                        if roster:
                            att_hour = roster.get_working_hours_of_date(date_from.time(), date_to.time()) / 3600
                        else:
                            att_hour = 0
                        if att_hour > 4:
                            worked_day += 1
                        attendance_hour += att_hour
                        if roster:
                            strt_date = datetime.strptime(roster.date, dsdf) + timedelta(hours=roster.time_from)
                            holiday_hours = 0
                            date_from += timedelta(hours=8)
                            holiday_hours += self.get_all_holiday_hours(strt_date.strftime(dsdtf), date_from.strftime(dsdtf), self.employee_id.id, 'unpaid')
                            if company_id.calculate_lag_by_limit:
                                strt_date += timedelta(minutes=company_id.lag_limit)
                                lag_time = datetime.strptime(date_from.strftime(dsdtf), dsdtf) - datetime.strptime(strt_date.strftime(dsdtf), dsdtf)
                            else:
                                lag_time = datetime.strptime(date_from.strftime(dsdtf), dsdtf) - datetime.strptime(strt_date.strftime(dsdtf), dsdtf)
                            lag_hour += (lag_time.seconds / 3600.0)

            self.total_worked_days = worked_day
            self.total_working_time = attendance_hour
            self.total_lag_hour = lag_hour
        else:
            return super(HRTimesheetSheet, self)._calc_working_time()
