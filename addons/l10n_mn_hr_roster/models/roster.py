# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

import logging
import time
from datetime import datetime, timedelta

import pytz
from dateutil.relativedelta import relativedelta

from odoo import _, api, fields, models
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.addons.l10n_mn_web.models.time_helper import *

_logger = logging.getLogger(__file__)


class HrRoster(models.Model):
    _name = 'hr.roster'
    _inherit = ['mail.thread']
    _order = 'date_from DESC, create_date DESC'

    def _default_date_from(self):
        return time.strftime('%Y-%m-01')

    def _default_date_to(self):
        return (datetime.today() + relativedelta(months=+1, day=1, days=-1)).strftime('%Y-%m-%d')

    @api.multi
    def _show_approve_button(self):
        res = {}
        history_obj = self.env['workflow.history']
        for roster in self:
            history = history_obj.search([('roster_id', '=', roster.id), (
                'line_sequence', '=', roster.check_sequence)], limit=1, order='sent_date DESC')
            if history:
                roster.show_approve_button = (
                    roster.state == 'send' and self.env.user.id in map(lambda x: x.id, history.user_ids))
            else:
                roster.show_approve_button = False
        return res

    state = fields.Selection([
        ('draft', 'Draft'),
        ('send', 'Send'),
        ('confirm', 'Confirm'),
        ('cancel','Cancelled')
    ], string='Status', index=True, readonly=True, default='draft',
        track_visibility='onchange', copy=False)

    balance_id = fields.Many2one('hour.balance', string='Hour Balance', required=True)
    name = fields.Char(string="Note", required=True, track_visibility='onchange')
    user_id = fields.Many2one('res.users', 'User', required=True, default=lambda self: self.env.uid, track_visibility='onchange')
    department_id = fields.Many2one('hr.department', string='Department', required=True, track_visibility='onchange')
    date_from = fields.Date(string='Date From', default=_default_date_from, required=True, index=True)
    date_to = fields.Date(string='Date To', default=_default_date_to, required=True, index=True)
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    line_ids = fields.One2many('hr.roster.line', 'roster_id', string='Lines', copy=True)
    time_from = fields.Float('Time From', default='9', required=True)
    time_to = fields.Float('Time To', default='18', required=True)
    workflow_id = fields.Many2one('workflow.config', 'Workflow', required=True, domain=[('model_id.model', '=', 'hr.roster')])
    check_sequence = fields.Integer(string='Workflow Step', default=0)
    check_users = fields.Many2many('res.users', 'account_hr_roster_confirm_user_rel', 'roster_id', 'user_id', 'Checkers', readonly=True, copy=False)
    show_approve_button = fields.Boolean(string='Show Approve Button?', compute='_show_approve_button')
    step_is_final = fields.Boolean(string='Final Step', default=False)
    workflow_history_ids = fields.One2many('workflow.history', 'roster_id', string='History', readonly=True)

    @api.multi
    def unlink(self):
        for obj in self:
            for line in obj.line_ids:
                line.roster_employee_line_ids.unlink()
            obj.line_ids.unlink()
        return super(HrRoster, self).unlink()
        
    @api.multi
    def action_send(self):
        for obj in self:
            if obj.workflow_id:
                workflow_obj = self.env['workflow.config']
                success, current_sequence = workflow_obj.send('workflow.history', 'roster_id', obj, self.env.user.id)
                is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(obj.workflow_id.id, obj, self.env.user.id, obj.check_sequence + 1, 'next')
                if is_next:
                    obj.check_users = [(6, 0, next_user_ids)]
                if success:
                    obj.check_sequence = current_sequence
                    obj.ensure_one()
                    obj.state = 'send'
                    self.action_change_state('send')
        return True

    @api.multi
    def action_confirm(self):
        self.ensure_one()
        workflow_obj = self.env['workflow.config']
        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve(
                'workflow.history', 'roster_id', self, self.env.user.id)
            is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(self.workflow_id.id, self, self.env.user.id, self.check_sequence + 1, 'next')
            if is_next:
                self.check_users = [(6, 0, next_user_ids)]
            if success:
                if sub_success:
                    self.state = 'confirm'
                    self.action_change_state('confirm')
                    self.step_is_final = True
                else:
                    self.check_sequence = current_sequence
                    self.show_approve_button = True
        return True

    @api.multi
    def action_draft(self):
        self.write({'state': 'draft'})
        self.action_change_state('draft')
        if self.workflow_history_ids:
            self.workflow_history_ids.unlink()
            self.check_sequence = 0
    @api.multi
    def action_cancel(self):
        self.write({'state': 'cancel'})
        self.action_change_state('cancel')

    @api.multi
    def action_change_state(self, state):
        for obj in self:
            for line in obj.line_ids:
                line.state = state

    def create_line(self, employees):
        self.ensure_one()
        for employee in employees:
            hr_roster_line = self.env['hr.roster.line'].create({
                'roster_id': self.id,
                'employee_id': employee.id,
                'company_id': self.company_id.id,
                'department_id': self.department_id.id,
                'hr_shift_id': self.env['hr.shift'].search([], limit=1).id
            })
            date = datetime.strptime(self.date_from, "%Y-%m-%d")
            to_date = datetime.strptime(self.date_to, "%Y-%m-%d")
            while (date <= to_date):
                self.env['hr.roster.employee.line'].create({
                    'line_id': hr_roster_line.id,
                    'date': date,
                })
                date = date + timedelta(days=1)
        
    @api.multi
    def action_create_line(self):
        # Ээлжийн цагийн хуваарийн мөрийг тухайн ХЭЛТЭС дэх ээлжээр ажиллах гэрээ бүхий ажилтнуудаар үүсгэх 
        for obj in self:
            emp_ids = []
            
            # remove old lines
            if obj.line_ids:
                obj.line_ids.unlink()
                
            # collect available employees
            contracts = obj.env['hr.contract'].search([('hour_balance_calculate_type', '=', 'payroll_by_roster')])
            for contract in contracts:
                if contract.employee_id.department_id == obj.department_id:
                    emp_ids.append(contract.employee_id.id)
                    
            # create roster line for employees
            obj.create_line(obj.env['hr.employee'].search([('id', 'in', emp_ids)]))

    @api.multi
    def action_create_line_all(self):
        # Ээлжийн цагийн хуваарийн мөрийг ээлжээр ажиллах гэрээ бүхий бүх ажилтнуудаар үүсгэх 
        for obj in self:
            emp_ids = []
            
            # remove old lines
            if obj.line_ids:
                obj.line_ids.unlink()
                
            # collect available employees
            contracts = obj.env['hr.contract'].search([('hour_balance_calculate_type', '=', 'payroll_by_roster')])
            for contract in contracts:
                emp_ids.append(contract.employee_id.id)

            # create roster line for employees
            obj.create_line(obj.env['hr.employee'].search([('id', 'in', emp_ids)]))

    @api.multi
    def action_update_department_lines(self):
        # Ээлжийн цагийн хуваарийн мөрийг тухайн ХЭЛТЭС дэх ээлжээр ажиллах гэрээ бүхий ажилтнуудаар шинэчлэх 
        for obj in self:
            emp_ids = []
            
            # get old line employees
            line_employees = [line.employee_id for line in obj.line_ids]
            
            # get new employees who aren't old line
            contracts = obj.env['hr.contract'].search([('hour_balance_calculate_type', '=', 'payroll_by_roster')])
            for contract in contracts:
                if contract.employee_id.department_id == obj.department_id and contract.employee_id not in line_employees:
                    emp_ids.append(contract.employee_id.id)
                    
            # create roster line for employees
            obj.create_line(obj.env['hr.employee'].search([('id', 'in', emp_ids)]))

    @api.multi
    def action_update_all_lines_from_roster_emps(self):
        # Ээлжийн цагийн хуваарийн мөрийг ээлжээр ажиллах гэрээ бүхий бүх ажилтнуудаар шинэчлэх 
        for obj in self:
            emp_ids = []
            
            # get old line employees
            line_employees = [line.employee_id for line in obj.line_ids]
            
            # get new employees who aren't old line
            contracts = obj.env['hr.contract'].search([('hour_balance_calculate_type', '=', 'payroll_by_roster')])
            for contract in contracts:
                if contract.employee_id not in line_employees:
                    emp_ids.append(contract.employee_id.id)
            employee_obj = obj.env['hr.employee'].search([('id', 'in', emp_ids)])

            # create roster line for employees
            obj.create_line(obj.env['hr.employee'].search([('id', 'in', emp_ids)]))

    @api.multi
    def update_roster_date(self):
        dics_for_create = []
        
        for obj in self:
            for line in obj.line_ids:
                
                # get dates from roster line
                registered_dates = [roster_line.date for roster_line in line.roster_employee_line_ids]
                
                # prepare dictionary for create new roster lines from 'update hour' action.
                check_date = line.date_from
                while (check_date <= line.date_to):
                    if check_date not in registered_dates:
                        dics_for_create.append({
                            'date': check_date,
                            'line_id': line.sudo().id,
                            'employee_id': line.employee_id.id,
                            'department_id': line.department_id.id,
                            'company_id': line.company_id.sudo().id,
                            'balance_id': line.roster_id.balance_id.id,
                            'state': line.state
                        })
                    check_date = str(datetime.strptime(str(check_date) + " 00:00:00", DEFAULT_SERVER_DATETIME_FORMAT) + relativedelta(days=+1))[0:10]
       
                # delete rows which aren't existed between new date intervals
                self._cr.execute("""
                    DELETE FROM hr_roster_employee_line 
                    WHERE line_id = %s AND (date < '%s' or date > '%s') RETURNING *
                """ % (line.id, line.date_from, line.date_to))
                
        # create new roster lines from prepared dictionary
        if len(dics_for_create) > 0:
            uid = self.env.uid
            date = fields.Datetime.now()
            insert_qry = "INSERT INTO hr_roster_employee_line (create_uid, create_date, write_uid, write_date, date, line_id, employee_id, department_id, company_id, balance_id, state) VALUES "
            for obj in dics_for_create:
                insert_qry += "(%s, '%s', %s, '%s', '%s', %s, %s, %s, %s, %s, '%s')" %(uid, date, uid, date, obj['date'], obj['line_id'], obj['employee_id'], obj['department_id'], obj['company_id'], obj['balance_id'], obj['state'])
                if obj != dics_for_create[-1]:
                    insert_qry += ", "
                     
            self.env.cr.execute(insert_qry)
            
        
class HrRosterEmployeeLine(models.Model):
    _name = 'hr.roster.employee.line'

    @api.onchange('hr_shift_id')
    def set_hr_shift_id(self):
        for obj in self:
            obj.time_from = obj.hr_shift_id.time_from
            obj.time_to = obj.hr_shift_id.time_to
            if obj.date:
                obj.from_time = datetime.strptime(str(obj.get_start_date()), DEFAULT_SERVER_DATETIME_FORMAT)
                obj.to_time = datetime.strptime(str(obj.get_end_date()), DEFAULT_SERVER_DATETIME_FORMAT)

    def compute_color(self):
        for line in self:
            print 'ongo tsootsno'

    line_id = fields.Many2one('hr.roster.line', string='Line')
    date = fields.Date('Date', required=True)
    time_from = fields.Float('Time From')
    time_to = fields.Float('Time To')
    is_work = fields.Boolean('Is Work')
    hr_shift_id = fields.Many2one('hr.shift', 'Hr Shift')
    from_time = fields.Datetime('Time From')
    to_time = fields.Datetime('Time To')
    day_of_week = fields.Char(string='Day of Week', compute='set_day_of_week', readonly=True)
    color_state = fields.Selection([('blue', 'Blue'), ('red', 'Red'), ('black', 'Black')], string='Color', default='black', compute=compute_color)
    is_checked = fields.Boolean(default=False)
    employee_id = fields.Many2one(related='line_id.employee_id', string='Employee', store=True)
    department_id = fields.Many2one(related='employee_id.department_id', string='Department',store=True)
    company_id = fields.Many2one(related='line_id.company_id', string='Company',store=True)
    balance_id = fields.Many2one(related='line_id.roster_id.balance_id', string='Hour Balance',store=True)
    state = fields.Selection(related='line_id.state', string='State',store=True)

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            result.append((record.id, "%s /%s - %s/" % (record.hr_shift_id.name, get_day_like_display(record.from_time, self.env.user), get_day_like_display(record.to_time, self.env.user))))
        return result
    
    def get_available_check_in_interval(self):
        self.ensure_one()
        from_date, to_date = self.from_time, self.from_time
        
        if self.hr_shift_id and self.hr_shift_id.in_date_from:
            from_date = self.from_time.replace(hour=self.hr_shift_id.in_date_from)
        if self.hr_shift_id and self.hr_shift_id.in_date_to:
            to_date = self.from_time.replace(hour=self.hr_shift_id.in_date_to)
            
        return from_date, to_date
    
    def get_available_check_out_interval(self):
        self.ensure_one()
        from_date, to_date = self.to_time, self.to_time
        
        if self.hr_shift_id and self.hr_shift_id.out_date_from:
            from_date = self.to_time.replace(hour=self.hr_shift_id.out_date_from)
        if self.hr_shift_id and self.hr_shift_id.out_date_to:
            to_date = self.to_time.replace(hour=self.hr_shift_id.out_date_to)
            
        return from_date, to_date
        
    @api.multi
    @api.depends('date')
    def set_day_of_week(self):
        for obj in self:
            
            if not obj.date:
                continue

            if datetime.strptime(obj.date, "%Y-%m-%d").weekday() == 0:
                obj.day_of_week = _("Monday")
            elif datetime.strptime(obj.date, "%Y-%m-%d").weekday() == 1:
                obj.day_of_week = _("Tuesday")
            elif datetime.strptime(obj.date, "%Y-%m-%d").weekday() == 2:
                obj.day_of_week = _("Wednesday")
            elif datetime.strptime(obj.date, "%Y-%m-%d").weekday() == 3:
                obj.day_of_week = _("Thursday")
            elif datetime.strptime(obj.date, "%Y-%m-%d").weekday() == 4:
                obj.day_of_week = _("Friday")
            elif datetime.strptime(obj.date, "%Y-%m-%d").weekday() == 5:
                obj.day_of_week = _("Saturday")
            else:
                obj.day_of_week = _("Sunday")
        
    def toggle_is_checked(self):
        return self.write({'is_checked': not self.is_checked})
    
    @api.multi
    def get_working_hours_of_date(self, time_from=None, time_to=None):
        from_seconds = (time_from.hour * 60 + time_from.minute) * 60 + time_from.second
        to_seconds = (time_to.hour * 60 + time_to.minute) * 60 + time_to.second
        max_seconds = to_seconds if to_seconds >= self.float_to_time(self.time_to) else self.float_to_time(self.time_to)
        min_seconds = from_seconds if from_seconds >= self.float_to_time(self.time_from) else self.float_to_time(self.time_from)
        return max_seconds - min_seconds

    @api.multi
    def float_to_time(self, ftimes):
        ftime = ftimes
        count = 0
        while(ftime >= 1):
            count += 1
            ftime = ftime - 1.0
        return count * 60 + ftime
    
    def get_start_date(self):
        self.ensure_one()
        user_time_zone = pytz.UTC
        if self.env.user.partner_id.tz:
            user_time_zone = pytz.timezone(self.env.user.partner_id.tz)
        return user_time_zone.localize(datetime.strptime(self.date, "%Y-%m-%d") + relativedelta(hours=self.time_from)).astimezone(pytz.utc).strftime(DEFAULT_SERVER_DATETIME_FORMAT)

    def get_end_date(self):
        self.ensure_one()
        user_time_zone = pytz.UTC
        if self.env.user.partner_id.tz:
            user_time_zone = pytz.timezone(self.env.user.partner_id.tz)
        end_date = datetime.strptime(self.date, "%Y-%m-%d") + relativedelta(hours=self.time_to)
        if self.hr_shift_id.is_day:
            end_date += relativedelta(days=self.hr_shift_id.shift_day)
        return user_time_zone.localize(end_date).astimezone(pytz.utc).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
    
    def get_working_hours_intervals(self, by_datetime=False, by_user_timezone=False):
        self.ensure_one()
        working_intervals = []
        
        date_start = get_day_like_display(self.from_time, self.env.user)
        date_end = get_day_like_display(self.to_time, self.env.user)
                    
        if self.hr_shift_id and self.hr_shift_id.lunch_time_ids:
            for lunch_time_id in self.hr_shift_id.lunch_time_ids:
                days = lunch_time_id.number_of_day
                date_from = get_day_like_display_from_floattime(get_day_by_user_timezone(self.date, self.env.user) + (relativedelta(days=(days - 1) if days >= 1 else 0)), lunch_time_id.time_from, self.env.user) 
                date_to = date_from + relativedelta(hours=lunch_time_id.lasts_hour)
                working_intervals.append((date_start, date_from))
                date_start = date_to
                if lunch_time_id == self.hr_shift_id.lunch_time_ids[-1]:
                    working_intervals.append((date_start, date_end))
        
        if not working_intervals:
            working_intervals.append((date_start, date_end))
                    
        by_user_timezoned_intervals = []   
        if by_user_timezone:
            for working_interval in working_intervals:
                by_user_timezoned_intervals.append((get_display_day_to_user_day(working_interval[0], self.env.user), get_display_day_to_user_day(working_interval[1], self.env.user)))
            working_intervals = by_user_timezoned_intervals
            
        working_intervals_by_str = []
        if not by_datetime:
            for working_interval in working_intervals:
                working_intervals_by_str.append((working_interval[0].strftime(DEFAULT_SERVER_DATETIME_FORMAT), working_interval[1].strftime(DEFAULT_SERVER_DATETIME_FORMAT)))
            working_intervals = working_intervals_by_str
             
        return working_intervals
    
    def get_lunch_hours_intervals(self, by_datetime=False):
        self.ensure_one()
        lunch_intervals = []
        if self.hr_shift_id and self.hr_shift_id.lunch_time_ids:
            for lunch_time_id in self.hr_shift_id.lunch_time_ids:
                days = lunch_time_id.number_of_day
                date_from = get_day_like_display_from_floattime(get_day_by_user_timezone(self.date, self.env.user) + (relativedelta(days=(days - 1) if days >= 1 else 0)), lunch_time_id.time_from, self.env.user) 
                date_to = date_from + relativedelta(hours=lunch_time_id.lasts_hour)
                if not by_datetime:
                    date_from = date_from.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                    date_to = date_to.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                lunch_intervals.append((date_from, date_to))
        return lunch_intervals
    
    def get_lunch_hours(self):
        self.ensure_one()
        
        lunch_hour = 0
        lunch_intervals = self.get_lunch_hours_intervals(by_datetime=True)
        date_interval = self._context.get('date_interval', False)
        
        if not date_interval:
            for time_interval in lunch_intervals:
                diff = time_interval[1] - time_interval[0]
                lunch_hour += float(diff.days) * 24 + (float(diff.seconds) / 3600)
        else:
            lunch_hour = get_duplicated_hours_between_intervals(date_interval, lunch_intervals)
            
        return lunch_hour
    

class HrRosterLine(models.Model):
    _name = 'hr.roster.line'

    def _work_day(self):
        for obj in self:
            count = 0
            for line in obj.roster_employee_line_ids:
                if line.hr_shift_id:
                    count += (line.hr_shift_id.shift_day or 1)
            obj.work_day = count

    def _work_hour(self):
        for obj in self:
            work_hour = 0.0
            for line in obj.roster_employee_line_ids.filtered(lambda x: x.hr_shift_id):
                work_hour += line.hr_shift_id.shift_hour
            obj.work_hour = work_hour

    roster_id = fields.Many2one('hr.roster', string='Roster')
    employee_id = fields.Many2one('hr.employee', string='Employee', required=True)
    date_from = fields.Date(string='Date From', related='roster_id.date_from', readonly=True)
    date_to = fields.Date(string='Date To', related='roster_id.date_to', readonly=True)
    company_id = fields.Many2one('res.company', related='roster_id.company_id', string='Company')
    department_id = fields.Many2one('hr.department', related='roster_id.department_id', string='Company')
    roster_employee_line_ids = fields.One2many('hr.roster.employee.line', 'line_id', string='Employee Roster Line', copy=True)
    work_day = fields.Integer(compute=_work_day, string='Work Day')
    work_hour = fields.Float(compute=_work_hour, string='Work Hour')
    hr_shift_id = fields.Many2one('hr.shift', 'Hr Shift', required=True)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('send', 'Open'),
        ('confirm', 'Confirm'),
        ('cancel', 'Cancelled'),
    ], string='Status', index=True, readonly=True, default='draft',
        track_visibility='onchange', copy=False)
    day_of_week = fields.Selection([
                              (0, 'Monday'),
                              (1, 'Tuesday'),
                              (2, 'Wednesday'),
                              (3, 'Thursday'),
                              (4, 'Friday'),
                              (5, 'Saturday'),
                              (6, 'Sunday')],
                             'Week of Day', default=0)
    
    def select_all(self):
        self.roster_employee_line_ids.write({'is_checked': True})
        return True

    def deselect_all(self):
        self.roster_employee_line_ids.write({'is_checked': False})
        return True

    def select_by_day_of_week(self):
        for obj in self:
            for line in obj.roster_employee_line_ids:
                if datetime.strptime(line.date, "%Y-%m-%d").weekday() == obj.day_of_week:
                    line.write({'is_checked': True})
                else:
                    line.write({'is_checked': False})
        
    def select_un_shifted(self):
        for obj in self:
            for line in obj.roster_employee_line_ids:
                if not line.hr_shift_id:
                    line.write({'is_checked': True})
                else:
                    line.write({'is_checked': False})
                    
    @api.multi
    def change_shift(self):
        ids = []
        for obj in self:
            for emp_line in obj.roster_employee_line_ids:
                if emp_line.is_checked:
                    ids.append(emp_line.id)
        if not ids:
            raise UserError(_(u"Please choose line for change roster !!!"))
        else:
            change_wizard = self.env['change.shift.wizard'].create({
                'shift_id': self[0].hr_shift_id.id
            })
            
            values = ""
            for id in ids:
                values += "(%s, %s), " %(change_wizard.id, id)
            values = values[:-2] if values[-2:] == ", " else values
                
            self._cr.execute("""
                INSERT INTO change_shift_wizard_to_employee_line (wizard_id, line_id) 
                VALUES %s
            """ % values)
            return {
                'type': 'ir.actions.act_window',
                'name': _('Change Shift'),
                'res_model': 'change.shift.wizard',
                'view_mode': 'form',
                'target': 'new',
                'res_id': change_wizard.id,
            }
