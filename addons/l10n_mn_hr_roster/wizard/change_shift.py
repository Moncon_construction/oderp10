# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import pytz
    
class ChangeShift(models.TransientModel):
    _name = 'change.shift.wizard'
    
    employee_line_ids = fields.Many2many('hr.roster.employee.line', 'change_shift_wizard_to_employee_line', 'wizard_id', 'line_id', string="Employee Roster Line")
    shift_id = fields.Many2one('hr.shift', string='Hr Shift', required=True, ondelete='cascade')

    @api.one
    def change_shift(self):
        for employee_line in self.employee_line_ids:
            employee_line.write({
                'hr_shift_id': self.shift_id.id,
                'from_time': datetime.strptime(str(self.get_start_date(employee_line.date)), DEFAULT_SERVER_DATETIME_FORMAT),
                'to_time': datetime.strptime(str(self.get_end_date(employee_line.date)), DEFAULT_SERVER_DATETIME_FORMAT),
                'time_from': self.shift_id.time_from,
                'time_to':  self.shift_id.time_to
            })
            
    def get_start_date(self, date):
        user_time_zone = pytz.UTC
        if self.env.user.partner_id.tz:
            user_time_zone = pytz.timezone(self.env.user.partner_id.tz)
        return user_time_zone.localize(datetime.strptime(date, "%Y-%m-%d") + relativedelta(hours=self.shift_id.time_from)).astimezone(pytz.utc).strftime(DEFAULT_SERVER_DATETIME_FORMAT)

    def get_end_date(self, date):
        user_time_zone = pytz.UTC
        if self.env.user.partner_id.tz:
            user_time_zone = pytz.timezone(self.env.user.partner_id.tz)
        end_date = datetime.strptime(date, "%Y-%m-%d") + relativedelta(hours=self.shift_id.time_to)
        if self.shift_id.is_day:
            end_date += relativedelta(days=self.shift_id.shift_day)
        return user_time_zone.localize(end_date).astimezone(pytz.utc).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
