# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import base64, os, xlrd
from datetime import datetime
from datetime import timedelta
import logging
from tempfile import NamedTemporaryFile

from odoo import api, fields, models, _
from odoo.exceptions import UserError


_logger = logging.getLogger(__name__)

class HourBalanceImport(models.TransientModel):
    _inherit = 'hour.balance.import'
    _description = 'Hr Hour Balance Import'

    @api.multi
    def import_hour_balance(self):
        context = dict(self._context or {})

        balance_obj = self.env['hour.balance']
        balance_ids = context['active_ids']

        _logger.info("BALANCE IDS :: %s " % balance_ids)
        _logger.info("CONTEXT :: %s " % context)

        for form in self:
            for data in balance_obj.browse(balance_ids):
                fileobj = NamedTemporaryFile('w+')
                fileobj.write(base64.decodestring(form.data))
                fileobj.seek(0)
            
                if not os.path.isfile(fileobj.name):
                    raise UserError(_("Import failed. Errorreason: The file has syntax errors"))

                book = xlrd.open_workbook(fileobj.name)  
                sheet_general = book.sheet_by_index(0)
                company_id = self.env.user.company_id.id
                nrows = sheet_general.nrows
                rowi = 1
                while rowi < nrows :
                    row = sheet_general.row(rowi)
                
                    identification_no = str(row[0].value).strip()
                    code = str(row[1].value).strip()
                    if form.import_type == 'ssnid':
                        self.env.cr.execute("SELECT emp.id FROM hr_employee emp ,resource_resource rss WHERE emp.resource_id = rss.id AND emp.ssnid like '%s' AND rss.company_id = %s" % (code, company_id))
                        employee_id = self.env.cr.dictfetchall()
                        if not employee_id:
                            raise UserError(_("Employee not found /RN/: %s") % code)
                    elif form.import_type == 'identification_id':
                        self.env.cr.execute("SELECT emp.id FROM hr_employee emp ,resource_resource rss WHERE emp.resource_id = rss.id AND emp.identification_id like '%s' AND rss.company_id = %s" % (identification_no, company_id))
                        employee_id = self.env.cr.dictfetchall()
                        if not employee_id:
                            raise UserError(_("Employee not found /IN/: %s") % identification_no)
                
                    employee = self.env['hr.employee'].search([('id','=',employee_id[0]['id'])])
                    work_hour = 0
                    worked_hour = 0
                    work_day = 0
                    worked_day = 0
                
                    # TODO: Get analytic account
                    if employee:
                        department_id = employee.department_id.id
                        # analytic_account_id = employee.department_id.analytic_account_id.id
                        employee_reg_number = row[1].value
                        employee_last_name = row[2].value
                        work_day = float(row[4].value)
                        work_hour = float(row[5].value)
                        worked_day = float(row[6].value)
                        worked_hour = float(row[7].value)
                        worked_night_shift_day = float(row[8].value)
                        worked_night_shift_hour = float(row[9].value)
                        overtime = float(row[10].value)
                        overtime_holiday = float(row[11].value)
                        attendance_hour = float(row[12].value)
                        bustrip = float(row[13].value)
                        paid_holiday = float(row[14].value)
                        out_working_hour = float(row[15].value)
                        annual_leave = float(row[16].value)
                        sick_leave = float(row[17].value)
                        unpaid_holiday = float(row[18].value)
                        delay_hour = float(row[19].value)
                        missed_hour = float(row[20].value)
                        unworked_leaved_hour = float(row[21].value)
                        total_unworked_hour = float(row[16].value) + float(row[17].value) + float(row[18].value) + float(row[20].value) + float(row[21].value)
                        lag_minute = float(row[23].value)
                        desc = row[24].value
                        balance_reg_day = float(row[4].value)
                        balance_reg_hour = float(row[5].value)

                        _data = {
                            'origin':'from_import_file',
                            'department_id': department_id,
                            # 'analytic_account_id': analytic_account_id,
                            'employee_id' :employee.id,
                            'employee_reg_number' :employee.ssnid,
                            'employee_last_name' :employee.last_name,
                            'balance_id': data.id,
                            'reg_day': work_day,
                            'reg_hour' : work_hour,
                            'balance_reg_day': balance_reg_day,
                            'balance_reg_hour': balance_reg_hour,
                            'worked_day': worked_day,
                            'worked_hour': worked_hour,
                            'overtime': overtime,
                            'overtime_holiday': overtime_holiday,
                            'worked_night_shift_day': worked_night_shift_day,
                            'worked_night_shift_hour': worked_night_shift_hour,
                            'attendance_hour': attendance_hour,
                            'business_trip_hour': bustrip,
                            'paid_holiday': paid_holiday,
                            'out_working_hour': out_working_hour,
                            'annual_leave': annual_leave,
                            'sick_leave': sick_leave,
                            'unpaid_holiday': unpaid_holiday,
                            'delay_hour': delay_hour,
                            'missed_hour': missed_hour,
                            'unworked_leaved_hour': unworked_leaved_hour,
                            'total_unworked_hour': total_unworked_hour,
                            'lag_minute': lag_minute,
                            'description': u'%s' % desc,
                        }
                    
                        self.env['hour.balance.line'].create(_data)
                    
                    rowi += 1
                
        return True