# -*- coding: utf-8 -*-
import base64
from datetime import datetime, timedelta
import logging
from tempfile import NamedTemporaryFile
import threading

import xlrd

from odoo import _, api, fields, models, registry
from odoo.exceptions import UserError, ValidationError


_logger = logging.getLogger(__name__)


class RosterImport(models.TransientModel):
    _name = 'roster.import'
    _description = 'Roster Import Tool'
    '''
        Ээлжийн цагийн хуваарь Excel файлаас импортлох
    '''
    data = fields.Binary(string='Import File', required=True)

    def check_short_name(self, short_name, rowi, wrong_rows, pass_empty = True):
        # Богино нэрээрх алдааг олж wrong_rows-д цуглуулах
        if short_name and float(str(short_name)) and float(short_name).is_integer():
            short_name = str(int(float(short_name)))
        if not pass_empty and not short_name and (rowi + 1) not in wrong_rows['empty_short_name']['rows']:
            wrong_rows['empty_short_name']['rows'].append(rowi + 1)
        elif short_name:
            shift_ids = self.env['hr.shift'].search([('short_name','=', short_name)])
            if not shift_ids and short_name not in wrong_rows['not_found_shift']['wrong_short_names']:
                wrong_rows['not_found_shift']['wrong_short_names'].append(short_name)
            elif len(shift_ids) > 1 and short_name not in wrong_rows['duplicated_shift']['wrong_short_names']:
                wrong_rows['duplicated_shift']['wrong_short_names'].append(short_name)
        return wrong_rows

    def check_register_no(self, register_number, checked_reg_nos, rowi, wrong_rows):
        # Ажилтны РД-рх алдааг олж wrong_rows-д цуглуулах
        if not register_number and (rowi + 1) not in wrong_rows['empty_reg_no']['rows']:
            wrong_rows['empty_reg_no']['rows'].append(rowi + 1)
        elif register_number and not self.env['hr.employee'].search([('ssnid','=', register_number)]) and register_number not in wrong_rows['not_found_employee']['wrong_reg_nos']:
            wrong_rows['not_found_employee']['wrong_reg_nos'].append(register_number)
        if register_number and register_number in checked_reg_nos and register_number not in wrong_rows['duplicated_reg_no']['wrong_reg_nos']:
            wrong_rows['duplicated_reg_no']['wrong_reg_nos'].append(register_number)
        return wrong_rows

    def build_error_message(self, note, has_error, wrong_rows, index1, index2):
        # Алдаа байгаа эсэхийг шалгаж note-д алдааны мессиж-г цуглуулах
        if len(wrong_rows[index1][index2]) > 0:
            has_error = True
            note += "\n\n" + wrong_rows[index1]['error_msg'] + ":\t %s" % ", ".join(["'%s'" %wrong_row for wrong_row in wrong_rows[index1][index2]])
        return note, has_error

    def check_full_error(self, roster, sheet, nrows):
        # Алдаатай мөрүүдийг олж raise өгөх
        if nrows <= 1:
            raise ValidationError(_("File is empty !!!"))

        wrong_rows = {
            'empty_reg_no': {'error_msg': _("'Register Number' is empty at belown rows"), 'rows': []}, # РД багана нь хоосон
            'empty_short_name': {'error_msg': _("'Short Name' is empty at belown rows"), 'rows': []}, # Богино нэр багана нь хоосон
            'not_found_employee': {'error_msg': _("Employee not found with belown register number"), 'wrong_reg_nos': []}, # РД-р ажилтан олдоогүй
            'not_found_shift': {'error_msg': _("HR Shift not found with belown short name"), 'wrong_short_names': []}, # Богино нэрээр ээлж олдоогүй
            'duplicated_reg_no': {'error_msg': _("Please merge belown register numbered employees' line"), 'wrong_reg_nos': []}, # Нэг ажилтны ээлжийг нэг л мөрөөр оруулах
            'duplicated_shift': {'error_msg': _("Duplicated shift found with belown short name"), 'wrong_short_names': []}, # Богино нэр нь давхцсан
        }

        rowi = 1
        columns = sheet.ncols
        checked_reg_nos = []

        while rowi < nrows:
            row = sheet.row(rowi)
            wrong_rows = self.check_register_no(str(row[0].value).strip(), checked_reg_nos, rowi, wrong_rows)
            wrong_rows = self.check_short_name(str(row[3].value).strip(), rowi, wrong_rows, pass_empty=False)
            checked_reg_nos.append(str(row[0].value).strip())

            i = 3
            date = datetime.strptime(roster.date_from, "%Y-%m-%d")
            to_date = datetime.strptime(roster.date_to, "%Y-%m-%d")
            while date <= to_date:
                i += 1
                if i < columns:
                    self.check_short_name(str(row[i].value).strip(), rowi, wrong_rows)
                date = date + timedelta(days=1)

            rowi += 1

        has_error = False
        note = _("Please fix belown errors:")
        note, has_error = self.build_error_message(note, has_error, wrong_rows, 'empty_reg_no', 'rows')
        note, has_error = self.build_error_message(note, has_error, wrong_rows, 'not_found_employee', 'wrong_reg_nos')
        note, has_error = self.build_error_message(note, has_error, wrong_rows, 'duplicated_reg_no', 'wrong_reg_nos')
        note, has_error = self.build_error_message(note, has_error, wrong_rows, 'empty_short_name', 'rows')
        note, has_error = self.build_error_message(note, has_error, wrong_rows, 'not_found_shift', 'wrong_short_names')
        note, has_error = self.build_error_message(note, has_error, wrong_rows, 'duplicated_shift', 'wrong_short_names')

        if has_error:
            raise ValidationError(note)

    def split_list(self, alist, wanted_parts=1):
        # Жагсаалт/alist/-г хүссэн тоогоор/wanted_parts/ дэд жагсаалт болгон хуваарилж буцаана.
        length = len(alist)
        return [alist[i*length // wanted_parts: (i+1)*length // wanted_parts] for i in range(wanted_parts)]

    def _run_roster_process(self, *dic_list):
        # hr.roster.line болон hr.roster.employee.line -н объектүүдийг үүсгэх thread функц
        with api.Environment.manage():
            with registry(self.env.cr.dbname).cursor() as new_cr:

                new_env = api.Environment(new_cr, self.env.uid, self.env.context)
                line_existen = False
                insert_qry = """INSERT INTO hr_roster_employee_line (create_uid, create_date, write_uid, write_date, date, line_id, employee_id, department_id, company_id, balance_id, state, hr_shift_id) VALUES """

                for dic in dic_list:

                    uid, sql_date = dic['uid'], dic['sql_date']
                    date_from, date_to = dic['date_from'], dic['date_to']
                    roster_id, hr_shift_id = dic['roster_id'], dic['hr_shift_id']
                    employee_id, company_id, department_id, balance_id, state = dic['employee_id'], dic['company_id'], dic['department_id'], dic['balance_id'], dic['state']
                    list_of_line_dic = dic['list_of_line_dic']

                    roster_line_id = new_env['hr.roster.line'].create({
                        'roster_id': roster_id,
                        'employee_id': employee_id,
                        'hr_shift_id': hr_shift_id or False,
                        'date_from': date_from,
                        'date_to': date_to,
                    }).id

                    if roster_line_id and len(list_of_line_dic) > 0:
                        for line_dic in list_of_line_dic:
                            insert_qry += """(%s, '%s', %s, '%s', '%s', %s, %s, %s, %s, %s, '%s', %s),""" % (uid, sql_date, uid, sql_date,
                                                                                                            line_dic['date'], roster_line_id, employee_id, department_id,
                                                                                                            company_id, balance_id, state, line_dic['hr_shift_id'] or 'NULL')
                        if not line_existen:
                            line_existen = True

                insert_qry = insert_qry[:-1]
                insert_qry += " RETURNING id"
                if line_existen:
                    new_cr.execute(insert_qry)
                    ids = [result[0] for result in new_cr.fetchall()]
                    new_env['hr.roster.employee.line'].browse(ids).filtered(lambda x: x.hr_shift_id).set_hr_shift_id()

    def run_roster_thread(self, dics_for_create):
        # Импортлох файл дах мөрийг/dics_for_create/ 5 хувааж 5-н thread үүсгэн зэрэг hr.roster.line болон түүнд харгалзах hr.roster.employee.line-н объектүүдийг үүсгэх
        split_dics_for_create = self.split_list(dics_for_create, 5)
        threads = []
        for dic_list in split_dics_for_create:
            dic_calculation = threading.Thread(target=self._run_roster_process, args=dic_list)
            dic_calculation.start()
            threads.append(dic_calculation)

        # Бүх thread ажиллаж дуусахыг хүлээх
        for t in threads:
            t.join()

    def create_roster_dictionary(self, roster, sheet, nrows):
        # sheet-г уншиж sheet-н мөр бүрээр hr.roster.line болон түүнд харгалзах hr.roster.employee.line-н объектүүдийг үүсгэх dictionary бэлдэж/dics_for_create/ буцаана.
        company_id, department_id, balance_id = roster.company_id.id, roster.department_id.id, roster.balance_id.id
        state, date_from, date_to = roster.state, roster.date_from, roster.date_to
        dics_for_create = []
        rowi = 1

        while rowi < nrows:
            row = sheet.row(rowi)

            i = 3
            list_of_line_dic = []
            date = datetime.strptime(roster.date_from, "%Y-%m-%d")
            to_date = datetime.strptime(roster.date_to, "%Y-%m-%d")
            employee = self.env['hr.employee'].search([('ssnid','=', str(row[0].value).strip())])
            shift = self.env['hr.shift'].search([('short_name','=', str(row[3].value).strip())])
            columns = sheet.ncols

            res = {
                'roster_id': roster.id,
                'employee_id': employee.id,
                'hr_shift_id': shift.id,
                'date_from': str(date_from),
                'date_to': str(date_to),
                'company_id': company_id,
                'department_id': department_id,
                'uid': self.env.uid,
                'sql_date': fields.Datetime.now(),
                'balance_id': balance_id,
                'state': state
            }

            while date <= to_date:
                i += 1

                line_shift = False
                if i < columns:
                    line_short_name = str(row[i].value).strip()
                    line_shift = self.env['hr.shift'].search([('short_name', '=', line_short_name)])

                list_of_line_dic.append({
                    'roster_id': roster.id,
                    'date': date,
                    'hr_shift_id': line_shift.id if line_shift else False,
                })

                date = date + timedelta(days=1)

            res.update({'list_of_line_dic': list_of_line_dic})
            dics_for_create.append(res)

            rowi += 1

        return dics_for_create

    def import_data(self):
        obj = self.browse(self.id)
        context = dict(self._context or {})
        try:
            fileobj = NamedTemporaryFile('w+')
            fileobj.write(base64.decodestring(obj.data))
            fileobj.seek(0)
            file_name = fileobj.name
            book = xlrd.open_workbook(file_name)
        except:
            raise UserError(_("Error loading data file.\nPlease try again!"))

        active_ids = context.get('active_ids')
        if not active_ids:
            raise ValidationError(_("Please try again !!!"))

        sheet = book.sheet_by_index(0)
        nrows = sheet.nrows
        roster = self.env['hr.roster'].browse(active_ids[0])

        _logger.info("#########HR ROSTER IMPORT PROCESS '%s'######### STARTED WITH %s LINES." %(roster.name, (nrows - 1)))

        # Файл дахь алдааг бөөнөөр нь шалгах
        _logger.info("#########HR ROSTER IMPORT PROCESS '%s'######### CHECKING FILE ERROR." %roster.name)
        self.check_full_error(roster, sheet, nrows)
        _logger.info("#########HR ROSTER IMPORT PROCESS '%s'######### CHECKING FILE ERROR FINISHED. NO ERROR FOUND" %roster.name)

        # Файлын мөр бүрээр hr.roster.line болон түүнд харгалзах hr.roster.employee.line-н объектүүдийг үүсгэх dictionary бэлдэх
        _logger.info("#########HR ROSTER IMPORT PROCESS '%s'######### PREPARING DICTIONARY" %roster.name)
        dics_for_create = self.create_roster_dictionary(roster, sheet, nrows)
        _logger.info("#########HR ROSTER IMPORT PROCESS '%s'######### PREPARING DICTIONARY FINISHED." %roster.name)

        # Бэлдсэн dictionary-гаа thread функц үүсгэн бааз руу hr.roster.line болон түүнд харгалзах hr.roster.employee.line-н объектүүдийг үүсгэх
        _logger.info("#########HR ROSTER IMPORT PROCESS '%s'######### WRITING TO DB PROCESS USING THREAD." %roster.name)
        self.run_roster_thread(dics_for_create)
        _logger.info("#########HR ROSTER IMPORT PROCESS '%s'######### WRITING TO DB PROCESS FINISHED." %roster.name)

        _logger.info("#########HR ROSTER IMPORT PROCESS '%s'######### SUCCESSFULLY DONE WITH %s LINES."  %(roster.name, (nrows - 1)))

        # Бүх thread ажиллаж дуусмагц визардыг хаах
        return {'type': 'ir.actions.act_window_close'}
