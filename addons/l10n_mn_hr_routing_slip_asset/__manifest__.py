# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

{
    "name" : "Mongolian HR Routing Slip for Asset",
    "version" : "1.0",
    "depends" : ['l10n_mn_hr_routing_slip_for_reclusion', 'l10n_mn_account_asset'],
    "author" : "Asterisk Technologies LLC",
    "description": """ Human Resource Routing Slip for Asset""",
    "website" : "http://asterisk-tech.mn",
    "category" : "Mongolian Modules",
    "data" : [
        'security/ir.model.access.csv',
        'views/hr_routing_slip_for_asset_view.xml',
    ],
    "installable": True,

}
