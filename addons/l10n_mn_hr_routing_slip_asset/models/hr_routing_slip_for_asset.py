# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class HrRoutingSlip(models.Model):
    _inherit = "hr.routing.slip"

    wait_asset_ids = fields.One2many('hr.routing.slip.asset.line', 'routing_id', string='Wait assets')

    # Ажил хүлээлгэн өгсөн ажилтанг сонгоход Хүлээлцэх хөрөнгүүдийг автоматаар дуудна
    @api.onchange('job_given')
    def _onchange_job_given(self):
        for obj in self:
            list = []
            sequence = 0
            if obj.job_given.id:
                asset_ids = self.env['account.asset.asset'].search([('user_id', '=', obj.job_given.id)])
                for asset_id in asset_ids:
                    sequence += 1
                    vals = {
                        'sequence': sequence,
                        'asset_id': asset_id.id,
                        'code': asset_id.code
                    }
                    list.append((0, 0, vals))
        return {'value': {'wait_asset_ids': list}}


class HrRoutingSlipAssetLine(models.Model):
    _name = "hr.routing.slip.asset.line"

    routing_id = fields.Many2one('hr.routing.slip')
    sequence = fields.Integer('Sequence')
    asset_id = fields.Many2one('account.asset.asset')
    code = fields.Char('Code')
