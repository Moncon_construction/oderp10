# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class HrJob(models.Model):
    _inherit = "hr.job"
    
    hr_job_acceptance_acts_ids = fields.One2many('hr.job.acceptance.acts', 'hr_job_id',"Job Acceptance Act")
    routing_slip_master_id = fields.Many2one('routing.slip.master', string='Routing Slip')
    
    @api.model
    def default_get(self, fields):
        res = super(HrJob, self).default_get(fields)
        acceptance_list = []
        acceptance_list.append(
            (0, 0, {
            'hr_job_id' : self.id,
            'is_selected' : False,
            'acceptance_item' : u'Цахим болон хэвлэмэл бичиг баримтууд авах'
        }))
        acceptance_list.append(
            (0, 0, {
            'hr_job_id' : self.id,
            'is_selected' : False,
            'acceptance_item' : u'Компьютерийн болон түүний дагалдах хэрэгсэл орох нууц код үг'
        }))
        acceptance_list.append(
            (0, 0, {
            'hr_job_id' : self.id,
            'is_selected' : False,
            'acceptance_item' : u'Серверийн хандалтын код'
        }))
        acceptance_list.append(
            (0, 0, {
            'hr_job_id' : self.id,
            'is_selected' : False,
            'acceptance_item' : u'Програмын эх код'
        }))
        acceptance_list.append(
            (0, 0, {
            'hr_job_id' : self.id,
            'is_selected' : False,
            'acceptance_item' : u'Емайлын нууц/үг'
        }))
        acceptance_list.append(
            (0, 0, {
            'hr_job_id' : self.id,
            'is_selected' : False,
            'acceptance_item' : u'Нэрийн хуудас, энгэрийн тэмдэг, зүүлт'
        }))
        acceptance_list.append(
            (0, 0, {
            'hr_job_id' : self.id,
            'is_selected' : False,
            'acceptance_item' : u'Ажлын хэрэгцээнд зориулсан хувцас, хэрэглэл'
        }))
        acceptance_list.append(
            (0, 0, {
            'hr_job_id' : self.id,
            'is_selected' : False,
            'acceptance_item' : u'Гар утас, ажлын хэрэгцээнд олгосон  техник хэрэгсэл'
        }))
        acceptance_list.append(
            (0, 0, {
            'hr_job_id' : self.id,
            'is_selected' : False,
            'acceptance_item' : u'Хамтран ажилладаг байгууллагын холбогдох мэдээлэл авах'
        }))
        acceptance_list.append(
            (0, 0, {
            'hr_job_id' : self.id,
            'is_selected' : False,
            'acceptance_item' : u'Хариуцаж байсан эд хөрөнгийг картын дагуу бүрэн бүтэн хүлээн авсан эсэх'
        }))
        acceptance_list.append(
            (0, 0, {
            'hr_job_id' : self.id,
            'is_selected' : False,
            'acceptance_item' : u'Цалингийн өр авлагын тооцоо дууссан эсэх'
        }))
        acceptance_list.append(
            (0, 0, {
            'hr_job_id' : self.id,
            'is_selected' : False,
            'acceptance_item' : u'Түлхүүр, флаш, CD диск'
        }))
        acceptance_list.append(
            (0, 0, {
            'hr_job_id' : self.id,
            'is_selected' : False,
            'acceptance_item' : u'Ямар нэг төлбөр тооцоо байгаа эсэх'
        }))
        acceptance_list.append(
            (0, 0, {
            'hr_job_id' : self.id,
            'is_selected' : False,
            'acceptance_item' : u'Төлбөр тооцооны карт эргүүлэн өгсөн эсэх'
        }))
        res.update({
               'hr_job_acceptance_acts_ids':acceptance_list,
               })
        return res

    
class HrJobAcceptanceActs(models.Model):
    _name = "hr.job.acceptance.acts"
    
    hr_job_id = fields.Many2one('hr.job', 'Job')
    hr_routing_slip = fields.Many2one('hr.routing.slip','Routing Slip')
    acceptance_item = fields.Char("Item's name")
    is_received = fields.Boolean('Is received')
