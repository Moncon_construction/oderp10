# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/,
# http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class HrRoutingSlip(models.Model):
    _name = "hr.routing.slip"
    _description = "Routing Slips"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _rec_name = 'job_given'

    out_date = fields.Date('Date of Out', required=True)
    given_date = fields.Date('Date of Given', required=True)
    job_given = fields.Many2one('hr.employee', 'Job given', domain=['|', ('state_id.type', 'not in', ('resigned', 'retired')), ('state_id', '=', False)], required=True)
    job_taken = fields.Many2one('hr.employee', 'Job taken', domain=['|', ('state_id.type', 'not in', ('resigned', 'retired')), ('state_id', '=', False)],)
    manager = fields.Many2one('hr.employee', 'Review Manager', domain=['|', ('state_id.type', 'not in', ('resigned', 'retired')), ('state_id', '=', False)], required=True)
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    department_id = fields.Many2one('hr.department', 'Department', required=True)
    job_id = fields.Many2one('hr.job', 'Job Title', required=True)
    description = fields.Text('Description')
    decision_ids = fields.One2many('hr.routing.slip.desicion.emp.line', 'decision_id')
    intro_ids = fields.One2many('hr.routing.slip.to.hand.in', 'intro_id')
    contact = fields.Char('Phone Number', required=True)
    show_approve_button = fields.Boolean(string='Show Approve Button?', compute='_show_approve_button')
    check_sequence = fields.Integer(string='Workflow Step', copy=False, default=0)
    workflow_id = fields.Many2one('workflow.config', string='Workflow')
    decision_emp_ids = fields.One2many('hr.routing.slip.desicion.emp.line', 'routing_id', string='Decision Lines')
    workflow_history_ids = fields.One2many('workflow.history', 'workflow_ids', string='History')
    hr_job_acceptance_acts_ids = fields.One2many('hr.job.acceptance.acts', 'hr_routing_slip', "Job Acceptance Act")
    state = fields.Selection([
            ('draft', 'Draft'),
            ('waiting', 'Waiting'),
            ('approved', 'Approved'),
            ('cancelled', 'Cancelled'),
    ], string='Status', index=True, readonly=True, default='draft',
        track_visibility='onchange')
    engagement_in_company = fields.Date(related="job_given.engagement_in_company", string='Date of Engagement in company', store=True, readonly=True)
    annual_leavel_date = fields.Date('Annual leavel date', readonly=True)
    routing_slip_master_id = fields.Many2one('routing.slip.master', string='Routing Slip')
    hr_routing_slip_line_ids = fields.One2many('hr.routing.slip.line.to.hand.in','routing_id', string='Items to Hand In')

    # Зөвхөн ноорог үед устгагдах
    @api.multi
    def unlink(self):
        if self.state != 'draft':
            raise UserError(_('You can only delete an routing slip if the slip is in draft state.'))
        return super(HrRoutingSlip, self).unlink()

    @api.multi
    def button_cancel(self):
        if self.state == 'waiting':
            self.state = 'cancelled'

    @api.multi
    def _show_approve_button(self):
        res = {}
        history_obj = self.env['workflow.history']
        for rout in self:
            history = history_obj.search([('workflow_ids', '=', rout.id), ('line_sequence', '=', rout.check_sequence)], limit=1, order='sent_date DESC')
            if history:
                rout.show_approve_button = (
                    rout.state == 'waiting' and self.env.user.id in map(lambda x: x.id, history.user_ids))
            else:
                rout.show_approve_button = False
        return res

    def is_module_installed(self, module_name):
        self._cr.execute("SELECT id FROM ir_module_module WHERE name = '%s' AND state IN ('installed', 'to upgrade')" % module_name)
        results = self._cr.dictfetchall()

        if results and len(results) > 0:
            return True
        else:
            return False

    # Ажил хүлээлгэн өгсөн ажилтныг барьж авч ажлын урсгалд оноох, ажилтанг сонгоход албан тушаал барьж авах
    @api.onchange('job_given')
    def _onchange_employee_id(self):
        self.department_id = self.job_given.department_id
        if self.job_given.job_id:
            self.job_id = self.job_given.job_id
        else:
            self.job_id = False

        if self.job_given.department_id:
            workflow_id = self.env['workflow.config'].get_workflow(
                'department', 'hr.routing.slip', employee_id=self.job_given.id, department_id=self.job_given.department_id.id)
            if workflow_id:
                self.workflow_id = workflow_id

        if self.job_given.id:
            hr_obj = self.env['hr.job']
            hr_job = hr_obj.search([('id', '=', self.job_given.id)])
            for job in hr_job:
                if self.job_given.id == job.id:
                    self.job_id = job

            if self.is_module_installed('hr_payroll'):
                vacation_hr_payslips = self.env['hr.payslip.run'].search([('salary_type', '=', 'vacation')], order='compute_date asc')
                for vacation_hr_payslip in vacation_hr_payslips:
                    for hr_slip in vacation_hr_payslip.slip_ids:
                        if hr_slip.employee_id.id == self.job_given.id:
                            self.annual_leavel_date = vacation_hr_payslip.compute_date
                        else:
                            self.annual_leavel_date = False

    # Албан тушаал сонгоход албан тушаал дээр
    # сонгогдсон тойрох хуудсыг автоматаар сонгодог болгов
    @api.onchange('job_id')
    def _onchange_job_id(self):
        if self.job_id:
            if self.job_id.routing_slip_master_id:
                self.routing_slip_master_id = self.job_id.routing_slip_master_id
            else:
                raise UserError(_('Routing slip is not set on [%s] job') % self.job_id.name)
        else:
            self.routing_slip_master_id = False

    # Тойрох хуудас талбар өөрчлөгдөхөд тухайн тойрох хуудсанд тохируулсан өгөгдлүүдээр
    # Хүлээлгэн өгөх шаардлагатай бичиг баримт -н талбарууд автоматаар бөглөгдөнө.from
    @api.onchange('routing_slip_master_id')
    def _onchange_routing_slip_master_id(self):
        data = []
        if self.routing_slip_master_id:
            for line in self.routing_slip_master_id.routing_slip_line_ids:
                data.append([0,0,{
                    'stage': line.code,
                    'dep_stage': line.stage,
                    'item_name': line.item_name,
                    'is_approve': False,
                    'note': ''
                }])
            return {'value': {'hr_routing_slip_line_ids': data}}
        else:
            return {'value': {'hr_routing_slip_line_ids': data}}


    # Тойрох хуудсыг ажлын урсгалын дараалалд орсон хэрэглэгчидрүү илгээж бүх хэрэглэгчид батлах хүртэл хүлээгдэж байгаа төлөвт шилжих
    @api.multi
    def submit_routing(self):
        '''Send Routing slip
        '''
        for obj in self:
            if obj.workflow_id:
                sequence = []
                for line in obj.workflow_id.line_ids:
                    if line.sequence not in sequence:
                        sequence.append(line.sequence)
                stages = []
                for slip_line in self.hr_routing_slip_line_ids:
                    if slip_line.stage not in stages:
                        stages.append(slip_line.stage)
                if sequence != stages:
                    raise UserError(_('Workflow sequence does not match stages'))

                workflow_obj = self.env['workflow.config']
                success, current_sequence = workflow_obj.send('workflow.history', 'workflow_ids', obj, self.env.user.id)
                is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(obj.workflow_id.id, obj, self.env.user.id, obj.check_sequence + 1, 'next')
                if is_next:
                    obj.check_users = [(6, 0, next_user_ids)]
                if success:
                    obj.check_sequence = current_sequence
                    obj.ensure_one()
                    obj.state = 'waiting'
                    self._line_state_update('waiting')
            else:
                raise UserError(_('Job given not workflow config'))
        return True

    # Илгээгдсэн тойрох хуудас ажлын урсгалын дагуу батлагдаж хэрвээ бүгд баталсан бол Батлагдсан төлөвт шилжих
    @api.multi
    def validate(self):
        self.ensure_one()
        workflow_obj = self.env['workflow.config']
        emp_obj = self.env['hr.employee']
        val = {}
        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve(
                'workflow.history', 'workflow_ids', self, self.env.user.id)
            is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(self.workflow_id.id, self, self.env.user.id, self.check_sequence + 1, 'next')
            if is_next:
                self.check_users = [(6, 0, next_user_ids)]
            if success:
                if sub_success:
                    self.state = 'approved'
                    self._line_state_update('approved')
                    self.step_is_final = True
                    emp = emp_obj.search([('id', '=', self.job_given.id)])
                    state = self.env['hr.employee.status'].search([('type', '=', 'resigned')], limit=1)
                    if state:
                        emp.write({'state_id': state.id})
                    else:
                        raise UserError(_('Employee resigned status not found'))
                else:
                    self.check_sequence = current_sequence
                    self.validating_user_id = self.env.user.id
                    self.show_approve_button = True
            return True

    # Тойрох хуудас цуцлагдаж Цуцлагдсан төлөвт шилжих
    @api.multi
    def refuse_routing(self, reason):
        self.write({'state': 'cancelled'})
        self._line_state_update('cancelled')
        for sheet in self:
            body = (_("Your Routing slip has been refused."))
            sheet.message_post(body=body)

    # Ноорог төлөвт шилжүүлэх
    @api.multi
    def action_to_draft(self):
        self.write({'state': 'draft', 'check_sequence': 0})
        self._line_state_update('draft')
        return True

    # Ажлын урсгалын дарааллын түүх шинэчлэгдэх
    @api.multi
    def _line_state_update(self, state):
        for obj in self.env['workflow.history']:
            for line in obj.workflow_ids:
                line.write({'state': state})


class HrRoutingSlipDesicionEmpLine(models.Model):
    _name = "hr.routing.slip.desicion.emp.line"

    employee_id = fields.Many2one('res.users', 'Desicion Employees', required=True)
    note = fields.Text('Note')
    decision_id = fields.Many2one('hr.routing.slip')
    routing_id = fields.Many2one('hr.routing.slip')


class WorkflowHistory(models.Model):
    _inherit = 'workflow.history'
    workflow_ids = fields.Many2one('hr.routing.slip', 'Routing Slip')


class HrLeaveIntroduce(models.Model):
    _name = "hr.routing.slip.to.hand.in"

    name = fields.Char('Digital and other files')
    done = fields.Boolean('Done', default=False)
    working = fields.Boolean('Working', default=False)
    printed_papers = fields.Boolean('Printed')
    digital_file = fields.Boolean('Digital File')
    note = fields.Text('Note')
    intro_id = fields.Many2one('hr.routing.slip', 'Intro')
    did = fields.Text("Did", default="+")

    # Хүлээлцэх шаардлагатай бичиг баримт хийгдсэн эсвэл хийгдэж байгаа гэсэн 2 checkbox-н нэгийг л сонгож болох
    @api.onchange('done')
    def is_done(self):
        if self.done:
            self.working = False

    @api.onchange('working')
    def is_working(self):
        if self.working:
            self.done = False


class HrRoutingSlipLineToHandIn(models.Model):
    _name = "hr.routing.slip.line.to.hand.in"

    @api.multi
    def _is_approve(self):
        res = {}
        history_obj = self.env['workflow.history']
        for obj in self:
            history = history_obj.search([('workflow_ids', '=', obj.routing_id.id), ('line_sequence', '=', obj.routing_id.check_sequence)],
                                         limit=1, order='sent_date DESC')
            if history:
                obj.is_approve = (obj.routing_id.check_sequence==obj.stage and obj.routing_id.state == 'waiting' and self.env.user.id in map(lambda x: x.id, history.user_ids))
            else:
                obj.is_approve = False
        return res

    routing_id = fields.Many2one('hr.routing.slip', string='Routing Slip', ondelete='cascade')
    stage = fields.Integer(string='Stage')
    dep_stage = fields.Char(string='Department,Stage')
    item_name = fields.Char(string='Hand In item name')
    is_check = fields.Boolean(string='Is Check')
    note = fields.Text(string='Note')
    is_approve = fields.Boolean(string='Is approve', compute='_is_approve')

