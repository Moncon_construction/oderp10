# -*- coding: utf-8 -*-

import datetime
from odoo import fields, models


class RoutingSlip(models.Model):
    _name = "routing.slip.master"

    name = fields.Char(string='Name', required=True)
    workflow_id = fields.Many2one('workflow.config', string='Routing Slip Workflow',domain=[('model_id.model','=','hr.routing.slip')], required=True)
    routing_slip_line_ids = fields.One2many('routing.slip.line', 'slip_id', string='Routing Slip Line')


class RoutingSlipLine(models.Model):
    _name = "routing.slip.line"

    slip_id = fields.Many2one('routing.slip.master', string='Routing Slip', ondelete='cascade')
    code = fields.Integer(string='Code L', required=True, default=1)
    stage = fields.Char(string='Stage L', required=True)
    item_name = fields.Char(string='Hand In item name L', required=True)

    _sql_constraints = [
        ('code_nonzero', 'check(code > 0)',
         'Code must be greater than zero!')
    ]






