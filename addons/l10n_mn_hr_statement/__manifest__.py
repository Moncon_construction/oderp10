# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian HR Statement",
    'version': '1.0',
    'depends': ['hr', 'l10n_mn_hr', 'l10n_mn_report'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Human Resource Additional Feature for Employee
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/hr_employee_report.xml',
        'views/employee_report.xml',
        'views/hr_employee_print_report_view.xml',
        'views/hr_employee_views.xml',
        'views/statement_type_view.xml',
    ],
    'installable': True,
    'license': 'GPL-3',
}
