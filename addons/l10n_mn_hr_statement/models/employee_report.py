# -*- coding: utf-8 -*-
from odoo import api, models, _

class EmployeeReport(models.AbstractModel):
    _name = 'report.l10n_mn_hr_statement.employee_report'
    @api.model
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        employee_report_obj = self.env['hr.employee.print.report.history']
        employee_report = employee_report_obj.browse(docids)
        report = report_obj._get_report_from_name('l10n_mn_hr_statement.action_report_hr_employee')
        validation = ''
        telephone = ''
        start_date_of_work = ''
        destination = ''
        certification = ''
        body = ''
        employee_wage = self.env['hr.contract'].search([('employee_id', '=',employee_report.employee_id.id )], limit=1).wage
        last_name = employee_report.employee_id.last_name or ''
        name = employee_report.employee_id.name or ''
        department = employee_report.employee_id.department_id.name or ''
        job = employee_report.employee_id.job_id.name or ''

        if employee_report.language == 'mongolia':
            validation = _('This certificate is valid for 14 days')
            telephone = _('Telephone')
            start_date_of_work = _('Working since')
            destination = str(employee_report.delivery_company) + _('-pre')
            certification = _('Certification')
            if employee_report.type == 'salary':
                body = last_name + ' ' + name + _(' in ') + department + _(' department ') + job + _(' job ') + str("{0:,.2f}".format(employee_wage)) + _(' monthly gross salary. ')
            elif employee_report.type == 'job':
                body = last_name + ' ' + name + _(' in ') + department + _(' department ') + job + _(' is working.')

        elif employee_report.language == 'english':
            validation = 'This certificate is valid for 14 days'
            telephone = 'Phone number'
            start_date_of_work = 'Working since '
            destination = 'Attn: ' + str(employee_report.delivery_company)
            certification = 'Certification'
            if employee_report.type == 'salary':
                body = 'This is to certify ' + name + ' ' + last_name + ' is working as "' + job + '" at ' + job + ' department with monthly gross salary of ' + str("{0:,.2f}".format(employee_wage)) + ' MNT. '
            elif employee_report.type == 'job':
                body = 'This is to certify ' + name + ' ' + last_name + ' is working as "' + job + '" at ' + job + ' department. '

        docargs = {
            'doc_ids': docids,
            'doc_model': report.model,
            'docs': employee_report,
            'validation': validation,
            'telephone': telephone,
            'start_date_of_work': start_date_of_work,
            'body': body,
            'destination': destination,
            'certification': certification,
        }
        return report_obj.render('l10n_mn_hr_statement.employee_report', docargs)