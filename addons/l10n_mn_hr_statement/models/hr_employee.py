# -*- coding: utf-8 -*-
##############################################################################
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2017 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved #
##############################################################################
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError 
from datetime import datetime
from odoo.addons.l10n_mn_report.models.report_helper import verbose_numeric, comma_me, convert_curr
import base64
from StringIO import StringIO
from docx import Document
import zipfile
import os

class HrEmployeePrintReportHistory (models.Model):
    _name = "hr.employee.print.report.history"

    # компани default-р авах
    @api.multi
    def _get_default_company(self):
        company_id = self.env['res.users']._get_company()
        if not company_id:
            raise ValidationError('There is no default company for the current user!')
        return company_id

    @api.model
    def _default_partners(self):
        # Холбоотой харилцагчид default-р нэвтэрсэн хэрэглэгчийн харилцагчийг оноох
        partners = self.env.user.partner_id
        active_id = self._context.get('active_id')
        if self._context.get('active_model') == 'res.partner' and active_id:
            if active_id not in partners.ids:
                partners |= self.env['res.partner'].browse(active_id)
        return partners

    @api.onchange('company_id')
    def _onchange_company_id(self):
        # ажилтанг олох
        active_id = self._context.get('active_id')
        active_model = self._context.get('active_model')
        if active_model == 'hr.employee' and active_id:
            self.employee_id = active_id
            employee = self.env[active_model].search([('id', '=', active_id)])
            if employee:
                if employee[0].department_id and employee[0].department_id.manager_id:
                    self.manager = employee[0].department_id.manager_id.id
                elif employee[0].parent_id:
                    self.manager = employee[0].parent_id.id
                else:
                    self.manager = False

    @api.onchange('employee_id')
    def _get_default_manager(self):
        # ажилтны менежерийг автоматаар авах
        if self.employee_id:
            if self.employee_id.department_id and self.employee_id.department_id.manager_id:
                self.manager = self.employee_id.department_id.manager_id.id
            elif self.employee_id.parent_id:
                self.manager = self.employee_id.parent_id.id
            else:
                self.manager = False

    name = fields.Char('Name', readonly=True, translate=True)
    location = fields.Char('Location', translate=True)
    employee_id = fields.Many2one('hr.employee', 'Employee', translate=True, domain=['|', ('state_id.type', 'not in', ('resigned', 'retired')), ('state_id', '=', False)])
    company_id = fields.Many2one('res.company', 'Company', default=_get_default_company, translate=True)
    manager = fields.Many2one('hr.employee', 'Manager', translate=True)
    partner_id = fields.Many2one('res.partner', 'Current partner', readonly=True, default=_default_partners, translate=True)
    date = fields.Date('Date', default=datetime.now(), translate=True)
    print_date = fields.Date('Print Date', default=datetime.now(), translate=True)
    type = fields.Selection([('job', 'Job'), ('salary', 'Salary')], string="Type")
    language = fields.Selection([('english', 'English'), ('mongolia', 'Mongolia')], string="Language",
                                default="mongolia")
    delivery_company = fields.Char("Delivery Company")
    contract_id = fields.Many2one('hr.contract')    
    employee_salary = fields.Float(related='contract_id.wage' ,string='employee salary')
    employee_salary_verbose_amount = fields.Char(string='Verbose Amount', compute='_compute_employee_salary_verbose_amount')
    statement_type = fields.Many2one('hr.contract.report.type', string='Statement Type', required= True)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('approved', 'Approved')
    ], string='Status', index=True, readonly=True, default='draft',
        track_visibility='onchange')

    @api.model
    @api.onchange('employee_id')
    def _get_contract_id(self):
        if self.employee_id:
            self.contract_id = self.employee_id.contract_id.id
    # дараалал үүсгэж тодорхойлолтыг дугаарлах
    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].get('hr.employee.print.report.history')
        if vals.get('employee_id',False):
            vals['contract_id'] = self.env['hr.employee'].browse(vals['employee_id']).contract_id.id
        employee_report = super(HrEmployeePrintReportHistory, self).create(vals)
        return employee_report

    @api.multi
    def write(self, vals):
        if vals.get('employee_id',False):
            vals['contract_id'] = self.env['hr.employee'].browse(vals['employee_id']).contract_id.id
        return super(HrEmployeePrintReportHistory, self).write(vals)

    @api.multi
    def unlink(self):
        for statement in self:
            if statement.state != 'draft':
                raise UserError('You can only delete an employee statement if the statements is in draft state.')
        return super(HrEmployeePrintReportHistory, self).unlink()

    @api.multi
    def validate(self):
        for statement in self:
            if statement.state == 'draft':
                statement.state = 'approved'

    @api.multi
    def set_to_draft(self):
        for statement in self:
            if statement.state == 'approved':
                statement.state = 'draft'

    @api.multi
    def _compute_employee_salary_verbose_amount(self):        
        for obj in self:
            currency_id = self.env.user.company_id.currency_id
            list = verbose_numeric(abs(obj.employee_salary))        
            if currency_id:
                obj.employee_salary_verbose_amount = convert_curr(list, currency_id.id, '')
            else:
                obj.employee_salary_verbose_amount = ''

    @api.multi
    def export_docx(self):
        for obj in self:
            context = obj._context
            for exp in obj:
                if exp.statement_type.attachment_id.datas:
                    report, template_name = self.create_doc_source(obj)
                    out = base64.encodestring(report)
                    report_type = '.docx'

                    excel_id = self.env['oderp.report.excel.output'].create({
                        'filedata': out,
                        'filename': template_name + report_type,
                    })
                    return {
                        'name': 'Export Report',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'oderp.report.excel.output',
                        'res_id': excel_id.id,
                        'view_id': False,
                        'context': self._context,
                        'type': 'ir.actions.act_window',
                        'target': 'new',
                        'nodestroy': True,
                    }
                else:
                    raise UserError(_('There is no attachment files in contract types!'))

    def create_doc_source(self, hr_contract_id):
        tmp_folder_name = '/tmp/docx_to_pdf/'
        self._delete_temp_folder(tmp_folder_name)
        self._create_temp_folder(tmp_folder_name)
        convert_path, template_name = self.convert_docx_from_base(tmp_folder_name, hr_contract_id)
        report = self._get_convert_file(convert_path)
        return report, template_name

    @api.multi
    def convert_docx_from_base(self, tmp_folder_name, hr_contract_id):
        docx_template_name = 'template_1.docx'
        template_path = tmp_folder_name + docx_template_name
        name = hr_contract_id.statement_type.attachment_id.name
        convert_path = tmp_folder_name + 'template.docx'
        doc = base64.decodestring(hr_contract_id.statement_type.attachment_id.datas)
        data = StringIO(doc)
        data = Document(data)
        data.save(template_path)

        replaceText = {}
        for field in hr_contract_id.statement_type.field_ids:
            if field.field_id.model_id.model == 'hr.employee.print.report.history':
                desc = False
                for con in hr_contract_id:

                    if field.field_id.name in con:
                        if field.field_id.ttype == 'many2one':
                            if field.string_path:
                                model_id = self.env['ir.model'].search([('model', '=', field.field_id.relation)])
                                f_id = self.env['ir.model.fields'].search(
                                    [('name', '=', field.string_path), ('model_id', '=', model_id.id)])
                                if f_id:
                                    if f_id.ttype == 'many2one':
                                        if field.last_path:
                                            model_id2 = self.env['ir.model'].search([('model', '=', f_id.relation)])
                                            f_id2 = self.env['ir.model.fields'].search(
                                                [('name', '=', field.last_path), ('model_id', '=', model_id2.id)])
                                            if f_id2:
                                                if f_id2.ttype == 'many2one':
                                                    desc = con[field.field_id.name][field.string_path][
                                                        field.last_path].name
                                                else:
                                                    desc = con[field.field_id.name][field.string_path][field.last_path]
                                            else:
                                                raise UserError(
                                                    _('The %s field is not set correctly!') % field.last_path)
                                        else:
                                            desc = con[field.field_id.name][field.string_path].name
                                    else:
                                        desc = con[field.field_id.name][field.string_path]
                                else:
                                    raise UserError(_('The %s field is not set correctly!') % field.string_path)
                            else:
                                desc = con[field.field_id.name].name
                        else:
                            if field.field_id.name == 'employee_salary':
                                desc = '{:5,.2f}'.format(con[field.field_id.name])
                            else:
                                desc = con[field.field_id.name]
                        strss = desc.encode(encoding='UTF-8') if type(desc) in (str, unicode) else desc
                        replaceText.update({field.name.encode(encoding='UTF-8'): strss})
                       
        self.create_doc(template_path, name, tmp_folder_name, convert_path, replaceText)

        if hr_contract_id.statement_type:
            get_path = tmp_folder_name + 'template.pdf'
            self.create_doc(template_path, name, tmp_folder_name, get_path, replaceText)
            return get_path, name
        return convert_path, name

    @api.multi
    def create_doc(self, template_file, name, tmp_folder_name, out_file, replaceText):
        templateDocx = zipfile.ZipFile(template_file)
        newDocx = zipfile.ZipFile(out_file, "w")
        for file in templateDocx.filelist:
            content = templateDocx.read(file)
            for key in replaceText.keys():
                content = content.replace(str(key), str(replaceText[key]))
            newDocx.writestr(file.filename, content)
        templateDocx.close()
        newDocx.close()
        return newDocx

    def _get_convert_file(self, convert_path):
        input_stream = open(convert_path, 'r')
        try:
            report = input_stream.read()
        finally:
            input_stream.close()

        return report

    def _convert_docx_to_pdf(self, tmp_folder_name, convert_docx_file_name):
        cmd = "soffice --headless --convert-to pdf --outdir " + convert_docx_file_name + " " + tmp_folder_name
        os.system(cmd)
        
        
    def _create_temp_folder(self, tmp_folder_name):
        if not os.path.exists(tmp_folder_name):
            os.makedirs(tmp_folder_name)

    def _delete_temp_folder(self, tmp_folder_name):
        cmd = 'rm -rf ' + tmp_folder_name
        os.system(cmd)
        if os.path.exists(tmp_folder_name):
            os.rmdir(tmp_folder_name)

class HrEmployee(models.Model):
    _inherit = "hr.employee"

    @api.multi
    def stat_button_statement(self):
        #   ухаалаг даруул дээр дарахад дуудагдах функц
        for line in self:
            res = self.env['hr.employee.print.report.history'].search([('employee_id', '=', line.id)])
            return {
                'type': 'ir.actions.act_window',
                'name': _('HR Statement'),
                'res_model': 'hr.employee.print.report.history',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'view_id': False,
                'domain': [('id', 'in', res.ids)],
            }

class HrContractTypeFields(models.Model):
    _name = 'hr.contract.report.type.fields'

    name = fields.Char(string='name', required=True)
    field_id = fields.Many2one('ir.model.fields', string='Field', required=True,
                               domain="[('model_id.model','in',['hr.employee.print.report.history'])]")
    string_path = fields.Char('First String Path')
    last_path = fields.Char('Last String Path')
    category_id = fields.Many2one('hr.employee.print.report.history.type', string='Category')

class HrEmployeePrintReportHistoryType(models.Model):
    _name = "hr.contract.report.type"

    name = fields.Char('name', required=True)
    attachment_id = fields.Many2one('ir.attachment', string='Template file',
                                    domain="[('mimetype','=','application/vnd.openxmlformats-officedocument.wordprocessingml.document')]")
    field_ids = fields.One2many('hr.contract.report.type.fields', 'category_id', string='Fields')