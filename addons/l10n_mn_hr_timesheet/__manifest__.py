# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian HR Timesheet",
    'version': '1.0',
    'depends': ['l10n_mn_hr_hour_balance_timesheet', 'l10n_mn_workflow_config'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Hr Timesheet workflow features
    """,
    'data': [
        'views/hr_timesheet_sheet_view.xml',
    ],
    'installable': True,
    'license': 'GPL-3',
}
