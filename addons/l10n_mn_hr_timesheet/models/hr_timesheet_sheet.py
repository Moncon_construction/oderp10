# -*- coding: utf-8 -*-
##############################################################################
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2020 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved #
##############################################################################
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError


class HrTimesheetSheet(models.Model):
    _inherit = "hr_timesheet_sheet.sheet"

    @api.multi
    def _show_approve_button(self):
        res = {}
        history_obj = self.env['workflow.history']
        for sheet in self:
            history = history_obj.search([('timesheet_sheet_id', '=', sheet.id), (
                'line_sequence', '=', sheet.check_sequence)], limit=1, order='sent_date DESC')
            if history:
                sheet.show_approve_button = (
                    sheet.state == 'confirm' and self.env.user.id in map(lambda x: x.id, history.user_ids))
            else:
                sheet.show_approve_button = False
        return res

    @api.multi
    def get_default_workflow(self):
        employee = self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)
        workflow_id = self.env['workflow.config'].get_workflow('employee', 'hr_timesheet_sheet.sheet', employee.id, None)
        return workflow_id

    workflow_id = fields.Many2one('workflow.config', 'Workflow', domain=[('model_id.model', '=', 'hr_timesheet_sheet.sheet')], default=get_default_workflow, required=True)
    check_sequence = fields.Integer(string='Workflow Step', default=0)
    check_users = fields.Many2many('res.users', 'hr_timesheet_sheet_check_user_rel', 'sheet_id', 'user_id', 'Checkers', readonly=True, copy=False)
    show_approve_button = fields.Boolean(string='Show Approve Button?', compute='_show_approve_button')
    step_is_final = fields.Boolean(string='Final Step', default=False)
    workflow_history_ids = fields.One2many('workflow.history', 'timesheet_sheet_id', string='History', readonly=True)

    @api.multi
    def action_timesheet_confirm(self):
        for sheet in self:
            if sheet.employee_id and sheet.employee_id.parent_id and sheet.employee_id.parent_id.user_id:
                self.message_subscribe_users(user_ids=[sheet.employee_id.parent_id.user_id.id])
            if sheet.workflow_id:
                workflow_obj = self.env['workflow.config']
                success, current_sequence = workflow_obj.send('workflow.history', 'timesheet_sheet_id', sheet, self.env.user.id)
                is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(sheet.workflow_id.id, sheet, self.env.user.id, sheet.check_sequence + 1, 'next')
                if is_next:
                    sheet.check_users = [(6, 0, next_user_ids)]
                if success:
                    sheet.check_sequence = current_sequence
                    sheet.ensure_one()
                    sheet.state = 'confirm'

    @api.multi
    def action_timesheet_draft(self):
        for sheet in self:
            if not self.env.user.has_group('hr_timesheet.group_hr_timesheet_user'):
                raise UserError(_('Only an HR Officer or Manager can refuse timesheets or reset them to draft.'))
            sheet.write({'state': 'draft'})
            if sheet.workflow_history_ids:
                sheet.workflow_history_ids.unlink()
                sheet.check_sequence = 0

    @api.multi
    def action_timesheet_done(self):
        workflow_obj = self.env['workflow.config']
        if not self.env.user.has_group('hr_timesheet.group_hr_timesheet_user'):
            raise UserError(_('Only an HR Officer or Manager can approve timesheets.'))
        if self.filtered(lambda sheet: sheet.state != 'confirm'):
            raise UserError(_("Cannot approve a non-submitted timesheet."))
        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve(
                'workflow.history', 'timesheet_sheet_id', self, self.env.user.id)
            is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(self.workflow_id.id, self, self.env.user.id, self.check_sequence + 1, 'next')
            if is_next:
                self.check_users = [(6, 0, next_user_ids)]
            if success:
                if sub_success:
                    self.state = 'done'
                    self.step_is_final = True
                else:
                    self.check_sequence = current_sequence
                    self.show_approve_button = True
            return True

    @api.multi
    def action_timesheet_confirm_to_draft(self):
        for sheet in self:
            sheet.write({'state': 'draft'})
            if sheet.workflow_history_ids:
                sheet.workflow_history_ids.unlink()
                sheet.check_sequence = 0

    def update_timesheet_project_by_project_task(self):
        # Цагийн хуудсын төслийг даалгавраас шинэчлэнэ.
        for sheet in self:
            for timesheet in sheet.timesheet_ids:
                self._cr.execute("""
                                    UPDATE account_analytic_line  set project_id = %s
                                    WHERE id = %s
                                """, (timesheet.task_id.project_id.id, timesheet.id))

class WorkflowHistory(models.Model):
    _inherit = 'workflow.history'

    timesheet_sheet_id = fields.Many2one('hr_timesheet_sheet.sheet', 'Timesheet Sheet')