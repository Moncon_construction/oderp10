# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian HR Training",
    'version': '10.0.1.0',
    'depends': ['l10n_mn_report', 'calendar', 'l10n_mn_hr', 'l10n_mn_workflow_config'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian HR Modules',
    'description': """
         Plan to training
    """,
    'data': [
        'security/training_security.xml',
        'security/ir.model.access.csv',
        'views/training_view.xml',
        'views/training_template.xml',
        'report/print_participant_list.xml',
        'views/training_plan_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
