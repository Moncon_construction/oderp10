# -*- coding: utf-8 -*-
from odoo import api, models, _
from odoo.exceptions import UserError
from odoo import api, fields, models, _
 
class Employee(models.Model):
    _inherit = "hr.employee"
    
    @api.multi
    def _training_line_count(self):
        for employee in self:
            training_line_ids = self.env['hr.training.line'].search([('employee_id', '=', employee.id)])
            employee.training_line_count = len(training_line_ids)
    
    training_line_count = fields.Integer(string='Training Count', compute=_training_line_count)
     
    @api.multi
    def unlink(self):
        for employee in self:
            training_line_ids = self.env['hr.training.line'].search([('employee_id','=',employee.id)])
            if training_line_ids:
                raise UserError(_("Cannot delete, this employee registered training."))
        super(Employee, self).unlink()

    @api.multi
    def stat_button_training(self):
        # Сургалт хийсэн  ухаалаг даруул дээр дарахад дуудагдах функц
        res = []
        for line in self:
            res = self.env['hr.training'].search([('partner_id', '=', line.address_home_id.id)])
            return {
                'type': 'ir.actions.act_window',
                'name': _('Training'),
                'res_model': 'hr.training',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'view_id': False,
                'domain': [('id', 'in', res.ids)],
            }

    @api.multi
    def stat_button_training_participant(self):
        #  Сургалтанд суусан ухаалаг даруул дээр дарахад дуудагдах функц
        training = []
        for line in self:
            res = self.env['hr.training.line'].search([('employee_id', '=', line.id)])
            return {
                'type': 'ir.actions.act_window',
                'name': _('Training'),
                'res_model': 'hr.training.line',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'view_id': False,
                'domain': [('id', 'in', res.ids)],
            }