# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError

class HRTrainingPlan(models.Model):
    _name = "hr.training.plan"
    _description = "Training Plan"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    
    STATE_SELECTION = [('draft', 'Draft'),
                       ('waiting_approve', 'Waiting for approve'),
                       ('approved', 'Approved'),
                       ('returned', 'Returned'),
                       ('cancel', 'Cancelled')]

    name = fields.Char(string='Training Plan Name', required=True)
    company_id = fields.Many2one('res.company', 'Company', default=lambda self:self.env.user.company_id.id, readonly=1)
    date_start = fields.Date('Date Start', required=True)
    date_end = fields.Date('Date End', required=True)
    training_ids = fields.One2many('hr.training', 'plan_id', 'HR Training')
    workflow_id = fields.Many2one('workflow.config', string='Workflow')
    check_sequence = fields.Integer(string='Workflow Step', copy=False, default=0)
    check_users = fields.Many2many('res.users', 'hr_training_plan_check_user_rel', 'plan_id', 'user_id', 'Checkers', readonly=True, copy=False)
    state = fields.Selection(STATE_SELECTION, default='draft')
    workflow_history_ids = fields.One2many(
        'workflow.history', 'training_plan_id', string='History', readonly=True)
    show_approve_button = fields.Boolean(string='Show Approve Button?', compute='_show_approve_button')
    
    @api.multi
    def _show_approve_button(self):
        res = {}
        history_obj = self.env['workflow.history']
        for plan in self:
            history = history_obj.search([('training_plan_id', '=', plan.id), (
                'line_sequence', '=', plan.check_sequence)], limit=1, order='sent_date DESC')
            if history:
                plan.show_approve_button = (
                    plan.state == 'waiting_approve' and self.env.user.id in map(lambda x: x.id, history.user_ids))
            else:
                plan.show_approve_button = False
        return res
    
    @api.multi
    def submit_trainig_plan(self):
        for obj in self:
            if obj.workflow_id:
                workflow_obj = self.env['workflow.config']
                success, current_sequence = workflow_obj.send('workflow.history',
                                                              'training_plan_id', self, self.create_uid.id)
                if success:
                    obj.check_sequence = current_sequence
                    obj.ensure_one()
                    obj.state = 'waiting_approve'
                    
    @api.multi
    def validate(self):
        self.ensure_one()
        workflow_obj = self.env['workflow.config']
        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve(
                'workflow.history', 'training_plan_id', self, self.env.user.id)
            is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(self.workflow_id.id, self, self.env.user.id, self.check_sequence + 1, 'next')
            if is_next:
                self.check_users = [(6, 0, next_user_ids)]
            if success:
                if sub_success:
                    self.state = 'approved'
                else:
                    self.check_sequence = current_sequence
                    self.validating_user_id = self.env.user.id
            return True
    
    @api.one
    def approve(self):
        # Данс сонгосон тохиолдолд тухайн данс нь Шинжилгээний данс шаардах данс бол Шинжилгээний данс сонгохыг шаардана
        if self.account and self.account.req_analytic_account:
            for line in self.expense_line:
                if line.analytic_account:
                    continue
                else:
                    raise UserError(_('Сонгосон данс шинжилгээний данс шаардах данс тул шинжилгээний дансыг мөрөн '
                                      'дээр сонгож өгнө үү'))

        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve('product.expense.workflow'
                                                                                         '.history', 'history', self,
                                                                                         self.env.user.id)
            if success:
                if sub_success:
                    if not self.check_quanitity(self.expense_line):
                        raise UserError(_('Боломжит тоо хэмжээ хүрэлцэхгүй байна!.'))
                    else:
                        for line in self.expense_line:
                            if not line.quantity:
                                raise UserError(_('Шаардахын мөрүүдын тоо хэмжээ 0 байна'))
                        self.create_out_picking()
                        self.state = 'approved'
                else:
                    self.check_sequence = current_sequence
                    
    @api.multi
    def refuse(self):
        if self.workflow_id:
            success = self.env['workflow.config'].reject('workflow.history', 'training_plan_id', self, self.env.user.id)
            if success:
                self.state = 'cancel'
                
    @api.multi
    def previous(self):
        if self.workflow_id:
            success, current_sequence = self.env['workflow.config'].action_return('workflow.history',
                                                                                  'training_plan_id', self, self.env.user.id)
            if success:
                self.check_sequence = current_sequence
                self.state = 'draft'
        
    @api.multi            
    def set_draft(self):
        self.write({
            'state': 'draft',
            'check_sequence': 0
        })
        
    @api.model
    def create(self, vals):
        creation = super(HRTrainingPlan, self).create(vals)
        workflow_id = self.env['workflow.config'].search([('model_id.model', '=', 'hr.training.plan')], limit = 1)
        if workflow_id:
            creation.workflow_id = workflow_id.id
        else:
            raise ValidationError(_('There is no workflow defined!'))
        return creation
    
    @api.multi
    def unlink(self):
        for plan in self:
            if plan.state in ('waiting_approve','approved'):
                 raise ValidationError(_('You cannot unlink approved or waiting approve training plan!'))
        return super(AccountBankStatementLine, self).unlink()
    
class WorkflowHistory(models.Model):
    _inherit = 'workflow.history'
    
    training_plan_id = fields.Many2one('hr.training.plan', 'HR Training Plan')