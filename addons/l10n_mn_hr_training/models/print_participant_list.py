# -*- coding: utf-8 -*-

from odoo import api, models

class PrintParticipantLis(models.Model):
    _name = "report.l10n_mn_hr_training.print_participant_list"

    @api.model
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        hr_training_obj = self.env['hr.training']
        report = report_obj._get_report_from_name('l10n_mn_hr_training.print_participant_list')
        hr_training_ids = hr_training_obj.browse(docids)
        docargs = {
            'doc_ids': docids,
            'doc_model': report.model,
            'docs': hr_training_ids,
        }
        return report_obj.with_context(self._context).render('l10n_mn_hr_training.print_participant_list', docargs)
