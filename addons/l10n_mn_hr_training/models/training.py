# -*- coding: utf-8 -*-
import odoo.addons.decimal_precision as dp
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError
from datetime import datetime
from odoo.addons.calendar.models.calendar import is_calendar_id
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from datetime import datetime, timedelta


class TrainingType(models.Model):
    _name = "hr.training.type"
    _description = "Training Type"

    name = fields.Char(string='Name', index=True, required=True)
    is_recurrent = fields.Boolean(string='Is Recurrent')
    recurrency = fields.Selection([
        (1, 'Daily'),
        (7, 'Weekly'),
        (30, 'Monthly'),
        (90, 'Quarterly'),
        (180, 'Half yearly'),
        (365, 'Yearly')], string='Recurrency')


class Training(models.Model):
    _name = "hr.training"
    _inherit = "mail.thread"
    _description = "Training"

    def default_employee(self):
        return self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)

    def default_manager(self):
        employee = self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)
        if employee:
            if employee.department_id.manager_id:
                return employee.department_id.manager_id
            else:
                return employee
        return False

    def _get_duration(self, start, stop):
        if start and stop:
            diff = fields.Datetime.from_string(stop) - fields.Datetime.from_string(start)
            if diff:
                duration = float(diff.days) * 24 + (float(diff.seconds) / 3600)
                return round(duration, 2)
            return 0.0

    @api.multi
    def _compute_color_partner(self):
        for meeting in self:
            meeting.color_partner_id = meeting.user_id.partner_id.id

    name = fields.Char(string='Name', index=True)
    employee_id = fields.Many2one('hr.employee', string="Employee", default=default_employee, readonly=True,
                                  ondelete='cascade')

    company_id = fields.Many2one('res.company', string='Company', required=True, index=True,
                                 default=lambda self: self.env.user.company_id,
                                 help="Company related to this training")
    manager_id = fields.Many2one('hr.employee', string="Manager", default=default_manager, readonly=True,
                                 ondelete='restrict')
    partner_id = fields.Many2one('res.partner', string="Teacher")
    teacher = fields.Char(string='Teacher', required=True)
    date_start = fields.Datetime(string='Start date', required=True)
    date_end = fields.Datetime(string='End date', required=True)
    training_type = fields.Many2one('hr.training.type', string='Training type', required=False, ondelete='restrict')
    check_users = fields.Many2many('hr.employee', 'hr_training_employee_rel', 'training_id', 'employee_id',
                                   'Check Users')
    file_id = fields.Binary('Received Attachment File', attachment=True)
    file_name = fields.Char('File name')
    to_include = fields.Many2many('hr.department', 'hr_training_department_rel', 'training_id', 'depart_id',
                                  'Departments')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('sented', 'Sented'),
        ('canceled', 'Canceled'),
        ('done', 'Confirmed'),
        ('implemented', 'Implemented')], default="draft")

    goal = fields.Char(string="Goal", required=True)
    location = fields.Text(string="Location")
    conclusion = fields.Text(string="Conclusion")
    score = fields.Selection([('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5')], string='Score')
    participant_line = fields.One2many('hr.training.line', 'training_id', 'Participant')
    allday = fields.Boolean('All Day', default=False)
    duration = fields.Float('Duration', default=0.0)
    calendar_id = fields.Many2one('calendar.event', 'Event', ondelete='cascade', copy=False)
    user_id = fields.Many2one('res.users', 'Responsible', default=lambda self: self.env.user, ondelete='cascade')
    color_partner_id = fields.Integer("Color index of creator", compute='_compute_color_partner', store=False)
    partner_ids = fields.Many2many('res.partner', 'hr_training_res_partner_rel', string='Partners')
    participant_employee_ids = fields.Many2many('hr.employee', string="Participating Employees",
                                                domain="[('state_id.type','not in',('maternity','resigned', 'retired'))]")
    plan_id = fields.Many2one('hr.training.plan', 'HR Training Plan')

    @api.multi
    def add_employees(self):
        this_object = self.env['hr.training'].search([('id', '=', self._context.get('active_id'))])
        training_line = self.env.get('hr.training.line').search([('training_id', '=', this_object.id)])
        # Оролцогчдын жагсаалтыг авч байна
        training_line_employee_ids = []
        for line_employee in training_line:
            training_line_employee_ids.append(line_employee.employee_id.id)

        emp_id = this_object.participant_employee_ids

        for line in emp_id:
            # Сонгосон ажилчид оролцсон эсэхийг шалгаж байхгүй бол нэмнэ.
            if line.id not in training_line_employee_ids:
                training_line.create({'employee_id': line.id,
                                      'date_start': this_object.date_start,
                                      'teacher': this_object.teacher,
                                      'attendance': True,
                                      'training_id': this_object.id})
                partner_ids = line.user_id.partner_id.ids
                subtype_ids = None
                channel_ids = []
                partner_data = dict((pid, subtype_ids) for pid in partner_ids)
                channel_data = dict((cid, subtype_ids) for cid in channel_ids)
                followers, fixer = self.env['mail.followers']._add_follower_command(self._name, self.ids, partner_data,
                                                                                    channel_data, True)
                self.sudo().write({'message_follower_ids': followers})
            this_object.participant_employee_ids = False

    def add_participant_view(self):
        for obj in self:
            view = obj.env.ref('l10n_mn_hr_training.add_participant_view')
            return {
                'name': _('Add participants'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'hr.training',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new',
                'res_id': self.id
            }

    @api.multi
    def write(self, values):
        return super(Training, self).write(values)

    @api.multi
    def add_participant(self):
        #         Сургалтанд оролцогчидыг нэмэх
        hr_data = self.browse(self.id)
        emp_obj = self.env.get('hr.employee')
        emp_id = emp_obj.search(
            [('department_id', 'in', [line.id for line in hr_data.to_include]), ('active', '=', 'True'),
             ('state_id.type', 'not in', ['maternity', 'resigned', 'retired'])], order='department_id')

        # уулзалтын оролцогчдыг авч байна
        training_line = self.env.get('hr.training.line').search([('training_id', '=', self.id)])
        training_line_employee_ids = []
        for line_employee in training_line:
            training_line_employee_ids.append(line_employee.employee_id.id)

        for line in emp_id:
            # Оролцогчид дунд нэмж буй оролцогч байгаа эсэхийг шалгаж байхгүй тохиолдолд нэмж байна.
            if line.id not in training_line_employee_ids:
                training_line.create({'employee_id': line.id,
                                      'date_start': hr_data.date_start,
                                      'teacher': hr_data.teacher,
                                      'attendance': True,
                                      'training_id': hr_data.id})
                partner_ids = line.user_id.partner_id.ids
                subtype_ids = None
                channel_ids = []
                partner_data = dict((pid, subtype_ids) for pid in partner_ids)
                channel_data = dict((cid, subtype_ids) for cid in channel_ids)
                # fixer ashiglagdahgui ch dangaar ni zarlah ued aldaa garch bn
                followers, fixer = self.env['mail.followers']._add_follower_command(self._name, self.ids, partner_data,
                                                                                    channel_data, True)
                self.sudo().write({'message_follower_ids': followers})

    @api.multi
    def delete_lines(self):
        line_pool = self.env['hr.training.line']
        line_ids = line_pool.search([('training_id', '=', self.id)])
        for l in line_ids:
            l.unlink()
        return True

    @api.multi
    def action_sent(self):
        partner_list = []
        #         Сургалтын батлах ажилтан руу мэйл илгээх цонх гарч ирэх
        if self.env.uid != self.employee_id.user_id.id:
            raise ValidationError(_("Only %s employee can sent training confirmation!" % self.employee_id.name))
        if not self.participant_line:
            raise UserError(_(
                'Training have not participant! Select department then click "Add Participant" button or register "Participant".'))
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('l10n_mn_hr_training', 'email_template_sent_to')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False

        for line in self.participant_line:
            if line.employee_id and line.employee_id.address_home_id:
                partner_list.append(line.employee_id.address_home_id.id)

        ctx = dict()
        ctx.update({
            'default_model': 'hr.training',
            'default_res_id': self.ids[0],
            'default_partner_ids': partner_list,
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True,
            'status': 'sented',
            'status_emp': 'uncertain'
        })
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    @api.multi
    def action_cancel(self):
        #         Менежер цуцлавал сургалтыг үүсгэсэн ажилтан руу тэйл илгээнэ, l10n_mn_hr-н hr_employee-д туслах функц бичсэн, Илгээх товч дарснаар төлвүүд солигдох ёстой
        followers = []
        if self.env.uid != self.manager_id.user_id.id:
            raise ValidationError(_("Only %s manager can cancel it!" % self.manager_id.name))
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('l10n_mn_hr_training', 'email_template_sent_to_canceled')[
                1]
        except ValueError:
            template_id = False
        ctx = dict()
        ctx.update({
            'default_model': 'hr.training',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'sent_to': str(followers).replace('[', '').replace(']', ''),
            'user': self.env['res.users'].search([('id', '=', self.env.uid)], limit=1).name,
            'status': 'canceled',
        })
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form,tree',
            'res_model': 'mail.compose.message',
            'target': 'new',
            'context': ctx,
        }

    @api.multi
    def create_expense_and_event(self, training):
        #         Оролцогчид руу мэйл илгээгдэхэд төлөв батлагдсан болно, бэлэн мөнгөний хүсэлт үүснэ, уулзалт үүснэ
        followers = []
        line_obj = self.env.get('hr.training.line')
        for id in training.ids:
            emps = line_obj.search([('training_id', '=', id)])
            for emp in emps:
                if emp.employee_id.user_id.partner_id:
                    followers.append(emp.employee_id.user_id.partner_id.id)
        partner_ids = self.env['res.partner'].search([('id', 'in', followers)]).ids
        calendar_id = self.env['calendar.event'].create({
            'name': training.name,
            'start': training.date_start,
            'stop': training.date_end,
            'partner_ids': [(6, 0, partner_ids)]})
        self.write({'state': 'done',
                    'duration': training._get_duration(self.date_start, self.date_end),
                    'calendar_id': calendar_id.id,
                    'partner_ids': [(6, 0, partner_ids)]})
        return True

    @api.multi
    def action_done(self):
        #         Сургалтыг батлах үед сургалтанд оролцогчидод мэйл илгээх цонх гарч ирэх
        followers = []
        if self.env.uid != self.manager_id.user_id.id:
            raise ValidationError(_("Only %s manager can confirm it!" % self.manager_id.name))
        calendar_view = self.env.ref('l10n_mn_hr_training.view_training_event_calendar')
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('l10n_mn_hr_training', 'email_template_sent_to_employees')[
                1]
        except ValueError:
            template_id = False
        line_obj = self.env.get('hr.training.line')
        for id in self.ids:
            emps = line_obj.search([('training_id', '=', id)])
            for emp in emps:
                if emp.employee_id.user_id.partner_id:
                    followers.append(emp.employee_id.user_id.partner_id.id)
        ctx = dict()
        ctx.update({
            'default_model': 'hr.training',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'followers': str(followers).replace('[', '').replace(']', ''),
            'status': 'done',
        })
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form,tree',
            'res_model': 'mail.compose.message',
            'target': 'new',
            'context': ctx,
        }

    @api.multi
    def action_implement(self):
        for id in self.ids:
            lines = self.env.get('hr.training.line').search([('training_id', '=', id)])
            for line in lines:
                if line.attendance:
                    self.env['hr.certificate'].sudo().create({
                        'employee_id': line.employee_id.id,
                        'classification': line.training_id.name,
                        'start_date': line.training_id.date_start,
                        'end_date': line.training_id.date_end,
                        'organization': line.training_id.location
                    })
                    line.write({'state': 'involved', })
                else:
                    line.write({'state': 'un_involved', })
        self.write({'state': 'implemented'})

    @api.multi
    def action_print_training_participants(self):
        return self.env['report'].get_action(self.ids, 'l10n_mn_hr_training.print_participant_list', None)

    @api.model
    def create(self, values):
        if (datetime.strptime(values['date_end'], '%Y-%m-%d %H:%M:%S') < datetime.strptime(values['date_start'],
                                                                                           '%Y-%m-%d %H:%M:%S')):
            raise ValidationError(_('Incorrect date of start and date of date you selected again !!!'))
        return super(Training, self).create(values)

    @api.multi
    def unlink(self):
        calendar_obj = self.env['calendar.event']
        records_to_unlink = self.env['calendar.event']
        for training in self:
            if training.state not in ('done','implemented'):
                if training.calendar_id:
                    if training.calendar_id.recurrent_id:
                        training.calendar_id.unlink()
                    else:
                        self._cr.execute('DELETE FROM calendar_event ' \
                                         'WHERE id = %s', (training.calendar_id.id,))
            else:
                raise UserError(_(
                    'You cant delete Done or Implemented training.'))
        return super(Training, self).unlink()


class TrainingLine(models.Model):
    _name = "hr.training.line"
    _description = "Employee training Lines"

    @api.multi
    @api.depends('date_end', 'training_type_id')
    def _compute_training_expiration(self):
        for training_line in self:
            if training_line.training_type_id.is_recurrent and training_line.date_end:
                training_line.training_expiration = datetime.strptime(training_line.date_end[:10],
                                                                      DEFAULT_SERVER_DATE_FORMAT) + timedelta(
                    days=training_line.training_type_id.recurrency)
            else:
                training_line.training_expiration = None

    @api.multi
    def _compute_today(self):
        for obj in self:
            obj.today = datetime.strptime(datetime.today().strftime('%Y-%m-%d'),
                                          DEFAULT_SERVER_DATE_FORMAT) + timedelta(days=30)

    @api.multi
    def _compute_turn_on_alarm(self):
        training_line_obj = self.env['hr.training.line']
        for obj in self:
            obj.turn_on_alarm = True
            retraining = training_line_obj.search([('id', '!=', obj.id),
                                                   ('employee_id', '=', obj.employee_id.id),
                                                   ('training_type_id', '=', obj.training_type_id.id),
                                                   ('date_end', '>=', obj.date_end)])
            if retraining:
                obj.turn_on_alarm = False

    training_id = fields.Many2one('hr.training', string='Training', readonly=True, required=True, ondelete='cascade')
    training_type_id = fields.Many2one(related='training_id.training_type', string='Training Type', store=True)
    employee_id = fields.Many2one('hr.employee', string='Employee name', domain="[('active','=','True')]",
                                  ondelete='restrict')
    employee_name = fields.Char(related="employee_id.name")
    department_id = fields.Many2one(related='employee_id.department_id', string='Department', store=True)
    attendance = fields.Boolean('Attendance', size=128, default=True, store=True)
    state = fields.Selection([('draft', 'Draft'),
                              ('uncertain', 'Uncertain'),
                              ('un_involved', 'Un involved'),
                              ('involved', 'Involved'),
                              ('canceled', 'Canceled'), ],
                             string='States', track_visibility='onchange', readonly=True, index=True, default="draft")
    date_start = fields.Datetime(related='training_id.date_start', string="Start date", store=True)
    date_end = fields.Datetime(related='training_id.date_end', string="End date", store=True)
    training_expiration = fields.Date("Training Expiration", compute='_compute_training_expiration', store=True)
    teacher = fields.Char(related='training_id.teacher', string='Teacher', store=True)
    today = fields.Date('Today', compute='_compute_today')
    turn_on_alarm = fields.Boolean('Turn on Alarm', compute='_compute_turn_on_alarm')

    @api.multi
    def write(self, values):
        for line in self:
            if values.get('attendance'):
                att = values.get('attendance')
                if att:
                    values.update({'state': 'involved'})
                else:
                    values.update({'state': 'un_involved'})
        return super(TrainingLine, self).write(values)

    @api.multi
    def unlink(self):
        for obj in self.training_id.message_follower_ids:
            if obj.partner_id == self.employee_id.address_home_id:
                obj.unlink()
        return super(TrainingLine, self).unlink()


class MailComposer(models.TransientModel):
    _inherit = 'mail.compose.message'

    @api.multi
    def send_mail_action(self):
        #         Мэйл илгээх цонх гарч ирэн илгээх товчин дээр дарахад тохирох үйлдэлүүдийг хийх
        if self._context.get('active_model') == 'hr.training':
            training = self.env['hr.training'].search([('id', '=', self._context.get('active_id'))])
            status = self._context.get('status')
            if status == 'done':
                #           Сургалтанд оролцогчид руу мэйл илгээж, зардалаар бэлэн мөнгөний хүсэлт үүсгэх, уулзалт үүсгэх функц дуудах
                training.create_expense_and_event(training)
            else:
                #                 Сургалтыг батлах ажилтан руу мэйл илгээх, сургалтанд оролцогчидын төлвийг өөрчлөх
                training.write({'state': status})
                emps = self.env['hr.training.line'].search([('training_id', '=', self._context.get('active_id'))])
                for emp in emps:
                    emp.write({'state': self._context.get('status_emp')})
        return super(MailComposer, self).send_mail_action()
