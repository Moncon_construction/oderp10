# -*- coding: utf-8 -*-
{
    "name": "Traninig Contract",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "description": """
      Training Contract
""",
    "website": False,
    "category": "Human Resource",
    "depends": ['base', 'mail', 'l10n_mn_hr_training'],
    'data': [
        'views/training_contract_view.xml',
        'security/ir.model.access.csv',
    ],
    'installable': True
}