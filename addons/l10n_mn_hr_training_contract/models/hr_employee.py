# -*- coding: utf-8 -*-
from odoo import models, fields, api, _

class HrEmployee(models.Model):
    _inherit = "hr.employee"

    def _count_training_contract(self):
        order_ids = self.env['hr.training.contract'].search([('employee_id', '=', self.id)])
        if order_ids:
            self.training_contract_count = len(order_ids)


    training_contract_count = fields.Integer(compute=_count_training_contract, string='Orders')