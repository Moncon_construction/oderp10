# -*- coding: utf-8 -*-
import time
from odoo import models, fields, api, _
from odoo import exceptions


class HrTrainingContractType(models.Model):
    _name = "hr.training.contract.type"
    _description = "Training Contract Type"
    
    name = fields.Char('Name', required=True)

class HrTrainingContract(models.Model):
    _name = "hr.training.contract"
    _description = "Training Contract"
    

    num_tcontract = fields.Char('Name of Contract', states={'confirmed':[('readonly',True)]})
    contract_start = fields.Date('Contract Start Date', required=True, states={'confirmed':[('readonly',True)]}, default=lambda *a: time.strftime('%Y-%m-%d'))
    contract_end = fields.Date('Contract End Date', required=True, states={'confirmed':[('readonly',True)]})
    employee_id = fields.Many2one('hr.employee', string='Employee', required=True, states={'confirmed':[('readonly',True)]})
    type_id = fields.Many2one('hr.training.contract.type',  string='Contract Type', required=True, states={'confirmed':[('readonly',True)]})
    fee = fields.Float('Fee', required=True, states={'confirmed':[('readonly',True)]})
    currency_id = fields.Many2one('res.currency', 'Currency', required=True, states={'confirmed':[('readonly',True)]})
    training_start = fields.Date('Training Start Date', states={'confirmed':[('readonly',True)]})
    training_end = fields.Date('Training End Date',states={'confirmed':[('readonly',True)]})
    duration = fields.Float('Employment Duration',states={'confirmed':[('readonly',True)]})
    duration_type = fields.Selection([
                    ('year','Year'),
                    ('at_least','At least'),
                    ], string='Duration Type')

    description = fields.Char('Description', size=128, states={'confirmed':[('readonly',True)]})
    state = fields.Selection([
                    ('draft','Draft'),
                    ('confirmed','Confirmed'),
                    ], track_visibility='onchange',string='State', default='draft')
    company_id =fields.Many2one('res.company','Company', states={'confirmed':[('readonly',True)]},default=lambda self: self.env.user.company_id)
    major =fields.Char('Major',states={'confirmed':[('readonly',True)]})
    location =fields.Char('Location',states={'confirmed':[('readonly',True)]})


    def action_to_confirm(self):
        return self.write({'state':'confirmed'})
    
    def action_to_draft(self):
        return self.write({'state':'draft'})

    @api.multi
    def unlink(self):
        tcontract = self.read(['state'])
        unlink_ids = []
        for s in tcontract:
            if s['state'] in ['draft']:
                unlink_ids.append(s['id'])
                super(HrTrainingContract, self).unlink_ids.unlink()
            else:
                raise exceptions.except_orm(_('Invalid Action!'),
                                     _('In order to delete a contract of training, you must Draft it first.'))


