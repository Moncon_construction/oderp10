# -*- coding: utf-8 -*-
{
    "name": "Mongolian Hr Wrong Entries",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "description": """
Хүний нөөцийн алдаатай мэдээллийн жагсаалт
""",
    "website": "http://asterisk-tech.mn",
    "depends": [
        'l10n_mn_hr',
        'l10n_mn_hr_recruitment',
        'l10n_mn_base'
    ],
    "data": [
        'security/hr_wrong_entries_security.xml',
        'views/wrong_entries.xml',
    ],
    "active": False,
    "installable": True,
}
