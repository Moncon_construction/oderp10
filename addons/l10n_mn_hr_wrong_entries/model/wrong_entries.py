# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class HrEmployee(models.Model):
    _inherit = "hr.employee"


    @api.multi
    def get_wrong_hr_employee_work_email(self):
        emails = []
        self.env.cr.execute(
            "select work_email from hr_employee group by work_email having count(id)>1 and work_email != '' ")
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            emails = [line[0] for line in fetched]

        tree_id = self.env.ref("hr.view_employee_tree")
        form_id = self.env.ref("hr.view_employee_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Employees with the same email'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'hr.employee',
            'domain': [('work_email', 'in', emails)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'context' : {'search_default_group_work_email' : 1},
            'target': 'current',
        }

    @api.multi
    def get_wrong_hr_employee_bank_account(self):
        accounts = []
        self.env.cr.execute(
            "select bank_account_id from hr_employee group by bank_account_id having count(id)>1  ")
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            accounts = [line[0] for line in fetched]

        tree_id = self.env.ref("hr.view_employee_tree")
        form_id = self.env.ref("hr.view_employee_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Employees with the same bank account'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'hr.employee',
            'domain': [('bank_account_id', 'in', accounts)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'context': {'search_default_group_bank_account_id': 1},
            'target': 'current',
        }

    @api.multi
    def get_wrong_hr_employee_home_address(self):
        address = []
        self.env.cr.execute(
            "select address_home_id from hr_employee group by address_home_id having count(id)>1")
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            address = [line[0] for line in fetched]

        tree_id = self.env.ref("hr.view_employee_tree")
        form_id = self.env.ref("hr.view_employee_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Employees with the same home address'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'hr.employee',
            'domain': [('address_home_id', 'in', address)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'context': {'search_default_group_address_home_id': 1},
            'target': 'current',
        }

    @api.multi
    def get_wrong_hr_employee_user(self):
        users = []
        duplicate = []
        employee = self.env['hr.employee'].search([('user_id', '!=', False)])
        for emp in employee:
            if emp.user_id.id not in duplicate:
                duplicate.append(emp.user_id.id)
            else:
                users.append(emp.user_id.id)
        tree_id = self.env.ref("hr.view_employee_tree")
        form_id = self.env.ref("hr.view_employee_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Employees with the same user'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'hr.employee',
            'domain': [('user_id', 'in', users)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'context': {'search_default_group_user_id': 1},
            'target': 'current',
        }

    @api.multi
    def not_selected_active_contract(self):
        contracts = []
        self.env.cr.execute(
            """
                SELECT id 
                FROM hr_employee 
                WHERE id not in (SELECT distinct(employee_id) FROM hr_contract) 
                and state_id not in (SELECT id FROM hr_employee_status WHERE type in ('resigned', 'retired'))
                UNION 
                SELECT id 
                FROM hr_employee 
                WHERE id not in (SELECT distinct(employee_id) FROM hr_contract) and state_id IS NULL 
                UNION 
                SELECT id 
                FROM hr_employee 
                WHERE id in (SELECT distinct(employee_id) FROM hr_contract WHERE active = 'f') 
                and state_id not in (SELECT id FROM hr_employee_status WHERE type in ('resigned', 'retired'))
            """)
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            contracts = [line[0] for line in fetched]

        tree_id = self.env.ref("hr.view_employee_tree")
        form_id = self.env.ref("hr.view_employee_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Not selected active contracts'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'hr.employee',
            'domain': [('id', 'in', contracts), ('active', '=', True)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'context': {'search_default_group_by_state': 1},
            'target': 'current',
        }

class Contract(models.Model):
    _inherit = "hr.contract"

    @api.multi
    def get_wrong_hr_employee_contracts(self):
        emp = []
        self.env.cr.execute(
            "select employee_id from hr_contract group by employee_id, active having count(id)>1 and active= True")
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            emp = [line[0] for line in fetched]

        return {
            'type': 'ir.actions.act_window',
            'name': _('Employees with one more active contracts'),
            'res_model': 'hr.contract',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'view_id': False,
            'domain': [('employee_id', 'in', emp)],
            'context': {'search_default_group_employee_id': 1},
        }
