# -*- coding: utf-8 -*-
{
    'name': "Mongolian Stock - Inventory Speed UP",
    'version': '1.0',
    'depends': ['l10n_mn_stock_account_cost_for_each_wh'],
    'author': "Asterisk Technologies LLC",
    'description': """
        Компаны агуулахын тохиргоон дахь "Барааны боломжит нөөц тооцох арга" нь "Барааны хөдөлгөөнөөс тооцох" үед уг модуль ажиллана. Доорх бүх сайжруулалтыг уг тохиргоотой үед хийв.
            * Тооллого ТООЛОХ товчин дээр дарахад stock.move-г query ашиглан үүсгэдэг болгов.
            * Тооллого БАТЛАХ товчин дээр дарахад stock.move-с account.move-үүдийг query ашиглан үүсгэдэг болгов.
        "Барааны хөдөлгөөнөөс тооцох" үед уг модуль дахь функцүүдийг бүхэлд нь override хийсэн тул ирээдүйд дараах классууд дээр required талбар нэмсэн бол өөрчлөлтийг уг модульд давхар хийж өгнө үү.
            * stock.move
            * account.move
            * account.move.line
            * account.analytic.line
    """,
    'category': 'Mongolian Modules',
    'data': [],
    'installable': True,
    'application': True
}
