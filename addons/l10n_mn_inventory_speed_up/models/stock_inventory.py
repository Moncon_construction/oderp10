# -*- coding: utf-8 -*-
from datetime import datetime
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_utils
from odoo.addons.l10n_mn_base.models.tools import *
    
    
class StockInventory(models.Model):
    _inherit = "stock.inventory"

    @api.multi
    def action_check(self):
        # Quant тооцох бол
        res = super(StockInventory, self.filtered(lambda x: x.company_id.availability_compute_method != 'stock_move')).action_check()
        
        # Доорх бүх код үлдэгдлээ stock.move-с тооцох бол ажиллаж quant-с хамааралгүйгээр хөдөлгөөн үүсгэнэ.
        inventory_ids = self.filtered(lambda x: x.company_id.availability_compute_method == 'stock_move')
        inventory_ids.filtered(lambda x: x.accounting_date).fix_theoretical_qty()
        inventory_ids.compute_standard_price()
        
        values = []
        uid = self.env.uid
        date = fields.Datetime.now()
        for inventory_id in inventory_ids:
            inventory_id.mapped('move_ids').unlink()
            for line in inventory_id.line_ids:
                if line.product_id.type == 'service':
                    continue
                # compare the checked quantities on inventory lines to the theorical one
                if float_utils.float_compare(line.theoretical_qty, line.product_qty, precision_rounding=line.product_id.uom_id.rounding) == 0:
                    continue
                diff = line.theoretical_qty - line.product_qty
                qty = abs(diff)
                location_id = line.product_id.property_stock_inventory.id if diff < 0 else line.location_id.id
                location_dest_id = line.location_id.id if diff < 0 else line.product_id.property_stock_inventory.id
                sm_values = line._get_move_values(qty, location_id, location_dest_id)
                values.append("('make_to_stock', %s, '%s', %s, '%s', '%s', '%s', %s, %s, %s, %s, %s, '%s', '%s', %s, %s, %s, %s, %s, %s, %s)" % (
                    uid, date, uid, date,
                    sm_values.get('state', 'confirmed') or 'confirmed',
                    get_qry_value(sm_values.get('name', '') or ''),
                    sm_values.get('product_id', 'NULL') or 'NULL',
                    sm_values.get('product_uom', 'NULL') or 'NULL',
                    sm_values.get('product_uom_qty', 0) or 0,
                    sm_values.get('product_uom_qty', 0) or 0,
                    0, # price_unit нь 0 байх ёстой. Орлогын өртөг тохируулагдсаны дараа, тооллого батлагдах үед post_inventory() функцаар засагдана.
                    sm_values.get('date', 'NULL') or 'NULL',
                    sm_values.get('date', 'NULL') or 'NULL',
                    sm_values.get('analytic_account_id', 'NULL') or 'NULL',
                    sm_values.get('company_id', 'NULL') or 'NULL',
                    sm_values.get('inventory_id', 'NULL') or 'NULL',
                    sm_values.get('restrict_lot_id', 'NULL') or 'NULL',
                    sm_values.get('restrict_partner_id', 'NULL') or 'NULL',
                    sm_values.get('location_id', 'NULL') or 'NULL',
                    sm_values.get('location_dest_id', 'NULL') or 'NULL'
                ))
        
        if values:
            qry = """
                INSERT INTO stock_move (procure_method, create_uid, create_date, write_uid, write_date, state,
                name, product_id, product_uom, product_uom_qty, product_qty, price_unit, date, date_expected, analytic_account_id,
                company_id, inventory_id, restrict_lot_id, restrict_partner_id, location_id, location_dest_id) VALUES 
            """
            qry += ", ".join(value for value in values)
            self._cr.execute(qry)
            
        inventory_ids.write({'state':'counted'})
        
        return res
    
    @api.multi
    def post_inventory(self):
        # Quant тооцох бол
        res = False
        by_quant = self.filtered(lambda x: x.company_id.availability_compute_method != 'stock_move')
        if by_quant:
            res = super(StockInventory, by_quant).post_inventory()
        
        # Доорх бүх код үлдэгдлээ stock.move-с тооцох бол ажиллана. Quant-г тэр чигт нь хаясан.
        # Санхүүгийн огноо түгжигдсэн эсэхийг шалгах
        inventory_ids = self.filtered(lambda x: x.company_id.availability_compute_method == 'stock_move')
        for inventory_id in inventory_ids:
            lock_date = inventory_id.company_id.period_lock_date or False if not self.env.user.has_group('account.group_account_manager') else inventory_id.company_id.fiscalyear_lock_date or False
            if lock_date and lock_date > (inventory_id.accounting_date or inventory_id.date):
                raise ValidationError(_(u"Accounting date is locked until '%s' !!!") % inventory_id.company_id.period_lock_date)
            
        # stock.move үүсгэж агуулахын өртөг дундажлана.
        if inventory_ids:
            move_ids = inventory_ids.mapped('move_ids')
            # Орлогын өртөг тохируулахаар орлогын өртөг тохируулагдчихдаг тул доорхиор зарлагын өртгүүдийг олж онооно.
            for move in move_ids:
                if not move.price_unit and move.location_id.usage == 'internal':
                    move.write({'price_unit': move.get_product_standard_price()})
            move_ids.create_account_move_by_qry()
            move_ids.filtered(lambda move: move.state != 'done').write({'state': 'done'})
            inventory_ids.update_warehouse_standard_price()
                    
        return res
