# -*- coding: utf-8 -*-
# Part of Asterisk technologies. See LICENSE file for full copyright and licensing details.
{

    'name': 'Khanbank Integration',
    'version': '1.0',
    'author': 'Asterisk Technologies LLC',
    'category': 'Account',
    'description': """
Khanbank Integration
====================
    - Integration configuration

    - Download transaction

    - Process transaction
""",
    'website': 'http://www.asterisk-tech.mn',
    'depends': [
        'l10n_mn_account'
    ],
    'data': [
        'views/khanbank_config_settings_view.xml',
        'views/res_bank_view.xml',
        'views/res_partner_bank_view.xml',
        'wizard/khanbank_get_transaction_view.xml',
        'views/account_journal_view.xml',
        'views/account_bank_statement_view.xml',
    ],
    'installable': True,
    'auto_install': False,
}
