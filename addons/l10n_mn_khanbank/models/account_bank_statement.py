from odoo import api, fields, models


class AccountBankStatement(models.Model):
    _inherit = 'account.bank.statement'

    khanbank_integrated = fields.Boolean(related='journal_id.bank_id.khanbank_integrated')

    @api.multi
    def button_confirm_bank(self):
        super(AccountBankStatement, self).button_confirm_bank()
        for record in self:
            for line in record.line_ids:
                line.khanbank_confirm()

    @api.multi
    def button_khanbank_get_transaction(self):
        for record in self:
            record.journal_id.khanbank_download(record.date, record.date)
