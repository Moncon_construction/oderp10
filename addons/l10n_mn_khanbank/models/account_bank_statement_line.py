# -*- coding: utf-8 -*-
import base64

import requests

from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError


class AccountBankStatementLine(models.Model):
    _inherit = 'account.bank.statement.line'

    khanbank_journal = fields.Integer('Khanbank journal')

    @api.multi
    def khanbank_confirm(self):
        for record in self:
            journal_id = record.statement_id.journal_id
            company_id = record.env.user.company_id
            currency_id = journal_id.currency_id if journal_id.currency_id else company_id.currency_id
            # Хаанбанктай интеграц хийгдсэн журнал бол
            if record.statement_id.journal_id.bank_id and record.statement_id.journal_id.bank_id.khanbank_integrated:
                bank_id = record.statement_id.journal_id.bank_id
                if record.statement_id.journal_id.bank_acc_number:
                    bank_acc_number = record.statement_id.journal_id.bank_acc_number
                    if company_id.khanbank_check_token():
                        for line in record.filtered(lambda l: l.journal_entry_ids and l.journal_entry_ids.state == 'posted'):
                            if line.amount < 0 and line.bank_account_id and line.bank_account_id.bank_id and not line.khanbank_journal:
                                to_currency = line.bank_account_id.currency_id if line.bank_account_id.currency_id else company_id.currency_id
                                headers = {
                                    'Content-Type': 'application/json',
                                    'Authorization': 'Bearer ' + company_id.khanbank_access_token
                                }
                                if line.bank_account_id.bank_id.khanbank_integrated:
                                    # Банк доторх гүйлгээ
                                    json = {
                                        'fromAccount': bank_acc_number,
                                        'toAccount': line.bank_account_id.acc_number,
                                        'toCurrency': to_currency.name,
                                        'amount': abs(line.amount),
                                        'description': line.name,
                                        'currency': currency_id.name,
                                        'loginName': company_id.khanbank_internetbank_username,
                                        'tranPassword': base64.b64encode(company_id.khanbank_internetbank_password)
                                    }
                                    response = requests.post(company_id.khanbank_url_for_demestic_transaction, headers=headers, json=json)
                                else:
                                    # Банк хоорондын гүйлгээ
                                    if not line.bank_account_id.owner_name:
                                        raise UserError(_('%s bank account has no owner name!') % line.bank_account_id.acc_number)
                                    if not line.bank_account_id.bank_id.code_for_khanbank_integration:
                                        raise UserError(_('%s bank has no code for Khanbank integration!') % line.bank_account_id.bank_id.name)
                                    json = {
                                        'fromAccount': bank_acc_number,
                                        'toAccount': line.bank_account_id.acc_number,
                                        'toCurrency': to_currency.name,
                                        'toAccountName': line.bank_account_id.owner_name,
                                        'toBank': line.bank_account_id.bank_id.code_for_khanbank_integration,
                                        'amount': abs(line.amount),
                                        'description': line.name,
                                        'currency': currency_id.name,
                                        'loginName': company_id.khanbank_internetbank_username,
                                        'tranPassword': base64.b64encode(company_id.khanbank_internetbank_password)
                                    }
                                    response = requests.post(company_id.khanbank_url_for_interbank_transaction, headers=headers, json=json)
                                if response.status_code == 200:
                                    # Амжилттай гүйлгээ хийсэн
                                    line.khanbank_journal = response.json()['journalNo']
                                else:
                                    raise ValidationError(_('The transaction is not successful.\nStatus code: %s\nError message: %s\nError code: %s') % (response.status_code, response.json()['message'], response.json()['code']))
                else:
                    raise UserError(_('The bank on your journal is integrated with Khanbank but does not have an account number. Please set account number on the journal.'))

    @api.multi
    def button_confirm_bank(self):
        super(AccountBankStatementLine, self).button_confirm_bank()
        self.khanbank_confirm()

    @api.multi
    def button_cancel_reconciliation(self):
        for record in self:
            if record.khanbank_journal:
                raise UserError(_('Finished transactions can not be cancelled.'))
        res = super(AccountBankStatementLine, self).button_cancel_reconciliation()

    @api.multi
    def unlink(self):
        for record in self:
            if record.khanbank_journal:
                raise UserError(_('Finished transactions can not be deleted.'))