# -*- coding: utf-8 -*-
import requests

from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError


class AccountJournal(models.Model):
    _inherit = 'account.journal'

    khanbank_integrated = fields.Boolean(related='bank_id.khanbank_integrated')

    @api.multi
    def khanbank_download(self, start_date, end_date):
        AccountBankStatement = self.env['account.bank.statement']
        AccountBankStatementLine = self.env['account.bank.statement.line']
        ResPartnerBank = self.env['res.partner.bank']
        ResCurrency = self.env['res.currency']
        company_id = self.env.user.company_id
        if company_id.khanbank_check_token():
            if not company_id.khanbank_url_to_get_transaction:
                raise UserError(_('Url to get transaction is not set'))
            headers = {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + company_id.khanbank_access_token
            }
            if not self.bank_acc_number:
                raise UserError(_('The bank on your journal is integrated with Khanbank but does not have an account number. Please set account number on the journal.'))
            response = requests.get(company_id.khanbank_url_to_get_transaction + '/' + self.bank_acc_number + '?from=' + start_date.replace('-', '') + '&to=' + end_date.replace('-', ''), headers=headers)
            if response.status_code == 200:
                for transaction in response.json()['transactions']:
                    statement = AccountBankStatement.search([('date', '=', transaction['tranDate']), ('journal_id', '=', self.id), ('company_id', '=', company_id.id)])
                    if not statement:
                        statement = AccountBankStatement.create({
                            'journal_id': self.id,
                            'date': transaction['tranDate'],
                            'company_id': company_id.id,
                        })
                    else:
                        if len(statement) > 1:
                            raise UserError(_('You have created %s statements on %s!') % (len(statement), transaction['tranDate']))
                        else:
                            if statement.state == 'confirm':
                                raise UserError(_('Statement on %s is confirmed. You can only download transactions to an open statement.') % transaction['tranDate'])
                                # <Огноо>-ны өдрийн хуулга батлагдсан байна. Та зөвхөн шинэ төлөвтэй хуулга руу гүйлгээ татаж болно.
                            if not statement.line_ids.filtered(lambda l: l.khanbank_journal == transaction['journal']):
                                res_partner_bank_id = ResPartnerBank.search([('acc_number', '=', transaction['relatedAccount'])])
                                partner_id = res_partner_bank_id.partner_id.id if res_partner_bank_id and res_partner_bank_id.partner_id else False
                                currency_id = ResCurrency.search([('name', '=', response.json()['currency']), ('active', '=', True)])
                                if not currency_id:
                                    raise UserError(_('Your company has no active %s currency!') % response.json()['currency'])
                                elif len(currency_id) > 1:
                                    raise UserError(_('Your company has %s active %s currency!') % (len(currency_id), response.json()['currency']))
                                elif (self.currency_id and currency_id != self.currency_id) or (not self.currency_id and currency_id != company_id.currency_id):
                                    raise UserError(_('Journal and transaction\'s currencies are different, transaction\'s currency is %s') % currency_id.name)
                                else:
                                    AccountBankStatementLine.create({
                                        'date': transaction['tranDate'],
                                        'statement_id': statement.id,
                                        'name': transaction['description'],
                                        'amount': transaction['amount'],
                                        'bank_account_id': res_partner_bank_id and res_partner_bank_id.id or False,
                                        'partner_id': partner_id,
                                        'khanbank_journal': transaction['journal'],
                                        'currency_id': currency_id.id
                                    })
            else:
                raise ValidationError(_('The transaction is not successful.\nStatus code: %s\nError message: %s\nError code: %s') % (response.status_code, response.json()['message'], response.json()['code']))
