# -*- coding: utf-8 -*-

from odoo import fields, models


class KhanbankConfigSettings(models.TransientModel):
    _name = 'khanbank.config.settings'
    _inherit = 'res.config.settings'

    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    khanbank_username = fields.Char('Username', related='company_id.khanbank_username')
    khanbank_password = fields.Char('Password', related='company_id.khanbank_password')
    khanbank_internetbank_username = fields.Char('Internetbank username', related='company_id.khanbank_internetbank_username')
    khanbank_internetbank_password = fields.Char('Internetbank password', related='company_id.khanbank_internetbank_password')
    khanbank_access_token = fields.Char('Access token', related='company_id.khanbank_access_token')
    khanbank_organization_name = fields.Char('Organization name', related='company_id.khanbank_organization_name')
    khanbank_access_token_expires_in = fields.Char('Access token expires in', related='company_id.khanbank_access_token_expires_in')
    khanbank_developer_email = fields.Char('Developer e-mail', related='company_id.khanbank_developer_email')
    khanbank_token_type = fields.Char('Token type', related='company_id.khanbank_token_type')
    khanbank_access_token_datetime = fields.Datetime('Token datetime', related='company_id.khanbank_access_token_datetime')
    khanbank_url_to_get_token = fields.Char('URL to get token', related='company_id.khanbank_url_to_get_token')
    khanbank_url_for_demestic_transaction = fields.Char('URL for domestic transaction', related='company_id.khanbank_url_for_demestic_transaction')
    khanbank_url_for_interbank_transaction = fields.Char('URL for interbank transaction', related='company_id.khanbank_url_for_interbank_transaction')
    khanbank_url_to_get_transaction = fields.Char('URL to get transaction', related='company_id.khanbank_url_to_get_transaction')
