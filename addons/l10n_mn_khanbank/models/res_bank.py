from odoo import models, fields, _


class ResBank(models.Model):
    _inherit = 'res.bank'

    khanbank_integrated = fields.Boolean('Khanbank integrated')
    code_for_khanbank_integration = fields.Char('Code for Khanbank integration')
    integrated_bank = fields.Selection(selection_add=[('khanbank', _('Khaan bank'))])
