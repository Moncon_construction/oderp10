# -*- coding: utf-8 -*-
from datetime import datetime

import requests
from dateutil.relativedelta import relativedelta
from requests.auth import HTTPBasicAuth

from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as DTF


class ResCompany(models.Model):
    _inherit = 'res.company'

    khanbank_username = fields.Char('Username')
    khanbank_password = fields.Char('Password')
    khanbank_internetbank_username = fields.Char('Internetbank username')
    khanbank_internetbank_password = fields.Char('Internetbank password')
    khanbank_access_token = fields.Char('Access token')
    khanbank_organization_name = fields.Char('Organization name')
    khanbank_access_token_expires_in = fields.Char('Access token expires in')
    khanbank_developer_email = fields.Char('Developer e-mail')
    khanbank_token_type = fields.Char('Token type')
    khanbank_access_token_datetime = fields.Datetime('Token datetime')
    khanbank_url_to_get_token = fields.Char('URL to get token')
    khanbank_url_for_demestic_transaction = fields.Char('URL for domestic transaction')
    khanbank_url_for_interbank_transaction = fields.Char('URL for interbank transaction')
    khanbank_url_to_get_transaction = fields.Char('URL to get transaction')

    @api.one
    @api.model
    def khanbank_check_token(self):
        if self.khanbank_username and self.khanbank_password:
            if self.khanbank_access_token and self.khanbank_access_token_datetime and (datetime.now() - datetime.strptime(self.khanbank_access_token_datetime, DTF)).total_seconds() < int(self.khanbank_access_token_expires_in):
                return True
            else:
                self.khanbank_get_token()
                if self.khanbank_access_token and self.khanbank_access_token_datetime and (datetime.now() - datetime.strptime(self.khanbank_access_token_datetime, DTF)).total_seconds() < self.khanbank_access_token_expires_in:
                    return True
        else:
            raise UserError(_('The bank on your journal is integrated with Khanbank but you have not set username and/or password in the configuration.'))

    @api.one
    @api.model
    def khanbank_get_token(self):
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        data = {'grant_type': 'client_credentials'}
        if not self.khanbank_url_to_get_token:
            raise UserError(_('No Khanbank token url provided in the configuration!'))
        response = requests.post(self.khanbank_url_to_get_token, auth=HTTPBasicAuth(self.khanbank_username, self.khanbank_password), params=data, headers=headers)
        if response.status_code == 200:
            self.khanbank_access_token = response.json()['access_token']
            self.khanbank_organization_name = response.json()['organization_name']
            self.khanbank_access_token_expires_in = response.json()['access_token_expires_in']
            self.khanbank_developer_email = response.json()['developer_email']
            self.khanbank_token_type = response.json()['token_type']
            self.khanbank_access_token_datetime = datetime.now()
        else:
            raise ValidationError(_('The bank on your journal is integrated with Khanbank but you could not get token from the bank.\nStatus code: %s\nError message: %s\nError code: %s') % (response.status_code, response.json()['message'], response.json()['code']))
