from odoo import models, fields


class ResPartnerBank(models.Model):
    _inherit = 'res.partner.bank'

    owner_name = fields.Char('Account owner name', required=True)
