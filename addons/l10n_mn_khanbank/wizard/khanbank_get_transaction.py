# -*- coding: utf-8 -*-
import requests

from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError


class KhanbankGetTransaction(models.TransientModel):
    _name = 'khanbank.get.transaction'

    start_date = fields.Date('Start date', required=True)
    end_date = fields.Date('End date', required=True)
    journal_id = fields.Many2one('account.journal')
    bank_id = fields.Many2one('res.bank', readonly=True, related='journal_id.bank_id')
    account_number = fields.Char('Account number', readonly=True, related='journal_id.bank_acc_number')

    @api.multi
    def download(self):
        self.journal_id.khanbank_download(self.start_date, self.end_date)
