# -*- coding: utf-8 -*-
# Part of Asterisk technologies. See LICENSE file for full copyright and licensing details.
{

    'name': 'Mongolian - KPI',
    'version': '1.0',
    'author': 'Asterisk Technologies LLC',
    'category': 'Project',
    'description': """
KPI (Key Performance Indicator)
========================
Хүний нөөцийн үнэлгээг тооцоолно
""",
    'website': 'http://www.asterisk-tech.mn',
    'images': [''],
    'depends': ['hr',
                'l10n_mn_crm_claim',
                'l10n_mn_project_task_norm',
                'l10n_mn_hr_hour_balance',
                'l10n_mn_project_issue',
                'l10n_mn_project_plan',
                'l10n_mn_project',
                'l10n_mn_hr_gamification',
                ],
    'data': [
        'security/data.xml',
        'security/ir.model.access.csv',
        'views/kpi_view.xml',
        'views/crm_claim_view.xml',
        'views/badge_view.xml',
        'views/project_view.xml'
    ],
    'installable': True,
    'auto_install': False,
}
