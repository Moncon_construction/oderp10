# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError

TYPE_SELECTION = [
        ('2','Small (+2)'), #-2
        ('6','Medium (+6)'), #-6
        ('10','Large (+10)') #-10
    ]

class GamificationBadgeUserWizard(models.TransientModel):
    _inherit = 'gamification.badge.user.wizard'
    
    awarded_date = fields.Date(string="Awarded date")
    use_kpi = fields.Boolean('Use KPI?', default=False)
    size_type = fields.Selection(TYPE_SELECTION, 'Type (size)')
     
    @api.multi
    @api.onchange('badge_id')
    def onchange_badge_id(self):
        if self.badge_id:
            self.update({'use_kpi':self.badge_id.use_kpi})

    @api.multi
    def action_grant_badge(self):
        """Wizard action for sending a badge to a chosen employee"""
        if not self.user_id:
            raise UserError(_('You can send badges only to employees linked to a user.'))

        if self.env.uid == self.user_id.id:
            raise UserError(_('You can not send a badge to yourself'))

        values = {
            'user_id': self.user_id.id,
            'sender_id': self.env.uid,
            'badge_id': self.badge_id.id,
            'employee_id': self.employee_id.id,
            'comment': self.comment,
            'use_kpi':self.use_kpi,
            'size_type':self.size_type,
            'awarded_date':self.awarded_date
        }

        return self.env['gamification.badge.user'].create(values)._send_badge()

class GamificationBadge(models.Model):
    _inherit='gamification.badge'

    use_kpi = fields.Boolean('Use KPI?')

class GamificationBadgeUser(models.Model):
    _inherit = 'gamification.badge.user'

    awarded_date = fields.Date(string="Awarded date")
    department_id = fields.Many2one('hr.department', related='employee_id.department_id', store=True, readonly=True)
    job_id = fields.Many2one('hr.job', related='employee_id.job_id', store=True, readonly=True)
    use_kpi = fields.Boolean('Use KPI?', default=False)
    size_type = fields.Selection(TYPE_SELECTION, 'Type (size)')