# -*- coding: utf-8 -*-
from odoo import fields, models


class crm_claim(models.Model):
    _inherit = 'crm.claim'

    TYPE_SELECTION = [
        ('2', 'Small (-2)'),  # -2
        ('6', 'Medium (-6)'),  # -6
        ('10', 'Large (-10)')  # -10
    ]

    size_type = fields.Selection(TYPE_SELECTION, 'Type (size)', default='2', required=True)
