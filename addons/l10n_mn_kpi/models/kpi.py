# -*- coding: utf-8 -*-
from odoo import api, fields, models, _, modules
from odoo.exceptions import UserError, ValidationError


class Kpi(models.Model):
    '''KPI - Key Performance Indicators'''
    _name = 'kpi'
    _description = 'KPI'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    STATE_SELECTION = [
        ('draft', 'Draft'),
        ('sent', 'Sent'),
        ('done', 'Done')
    ]

    TYPE_SELECTION = [
        ('employee', 'Employee'),
        ('manager', 'Project manager')
    ]

    @api.multi
    def _compute_productivity(self):
        for obj in self:
            productivity_percent = 70 
            productivity_percent = productivity_percent / 2 if obj.type == 'manager' else productivity_percent
            
            manager_total, employee_total = 0.0, 0.0
            
            if obj.work_hour:
                employee_total = (obj.planned_task_process_value + obj.not_planned_task_hours / obj.work_hour * 100) * productivity_percent / 100
            
            if obj.type == 'manager' and obj.project_lines and len(obj.project_lines) > 0.0:
                total_performance = sum(project.performance for project in obj.project_lines)
                total_weight = sum(project.weight for project in obj.project_lines)
                manager_total = round(total_performance / total_weight, 2) if total_weight else 0.0

            obj.productivity_total = manager_total + employee_total
                
    @api.multi
    def _compute_attitude(self):
        for obj in self:
            total = 10
            for att in obj.attitude_lines:
                total += att.point
            obj.attitude_total = total

    @api.multi
    def _compute_quality(self):
        for obj in self:
            total = 20
            total_claim = 0
            total_badge = 0
            total_issue = 0
            for claim in obj.crm_claim_ids:
                total_claim += int(claim.size_type)

            for badge in obj.badge_user_ids:
                total_badge += int(badge.size_type)

            for issue in obj.issue_ids:
                total_issue += 3

            if total_claim > 0:
                total_claim = total_claim * (-1)

            if total_issue > 0:
                total_issue = total_issue * (-1)
            obj.quality_total = total + total_claim + total_badge + total_issue

    @api.multi
    def _compute_total(self):
        for obj in self:
            obj.kpi_total = obj.productivity_total + obj.attitude_total + obj.quality_total

    @api.multi
    def _compute_task_planned_hours(self):
        for obj in self:
            hours = 0.0
            if obj.task_ids:
                for task in obj.task_ids:
                    hours += task.planned_hours if task.is_planned else 0
            obj.planned_task_hours = hours

    @api.multi
    def _compute_not_task_planned_hours(self):
        for obj in self:
            hours = 0.0
            if obj.task_ids:
                for task in obj.task_ids:
                    hours += task.planned_hours if not task.is_planned else 0
            obj.not_planned_task_hours = hours

    @api.multi
    def _compute_planned_task_process_value(self):
        for obj in self:
            hours = 0.0
            
            if obj.task_ids:
                for task in obj.task_ids:
                    hours += ((task.progress_value * task.planned_hours) / (100)) if task.is_planned else 0
                    
            obj.planned_task_process_value = round(hours, 2)
            
    @api.multi
    def _compute_task_effective_hours(self):
        for obj in self:
            hours = 0.0
            if obj.task_ids:
                for task in obj.task_ids:
                    hours += task.effective_hours
            obj.task_effective_hours = hours

    @api.multi
    def _compute_productivity_extra_note(self):
        for obj in self:
            productivity_percent = 70
            total_percent = 100
            productivity_percent = productivity_percent / 2 if obj.type == 'manager' else productivity_percent

            employee_total = 0
            if obj.work_hour:
                employee_total = round((obj.planned_task_process_value + obj.not_planned_task_hours / obj.work_hour * total_percent) * productivity_percent / total_percent, 2)
            note = u'Даалгаврын бүтээмж: %s = (%s+%s/%s*%s)*%s/%s' % (employee_total, obj.planned_task_process_value, obj.not_planned_task_hours, obj.work_hour, total_percent,
            productivity_percent, total_percent)
            obj.productivity_extra_note = note

    @api.multi
    def _compute_productivity_project_extra_note(self):
        for obj in self:
            note = ""
            
            if obj.type == 'manager':
                total_weight = 0.0
                total_performance = 0.0
                
                if obj.project_lines and len(obj.project_lines) > 0.0:
                    for project in obj.project_lines:
                        total_performance += project.performance
                        total_weight += project.weight
                
                average_performance = (total_performance / total_weight) if total_weight else 0
                note = u'Төслийн явцын бүтээмж: %s = (%s/%s)' % (round(average_performance, 2), total_performance, total_weight)
            obj.productivity_project_extra_note = note

    name = fields.Char(string='Name', size=256)
    employee_id = fields.Many2one('hr.employee', string='Employee', required=True, readonly=True, index=True, states={'draft': [('readonly', False)]}, track_visibility='always')
    user_id = fields.Many2one('res.users', string='User', required=True)
    period_id = fields.Many2one('account.period', string='Period', required=True, readonly=True, index=True, states={'draft': [('readonly', False)]}, track_visibility='always')
    type = fields.Selection(TYPE_SELECTION, string='KPI Type', readonly=True, required=True, default='employee', states={'draft': [('readonly', False)]}, track_visibility='always')
    work_hour = fields.Float(string='Work Hour', required=True, digits=(16, 2), default=0, readonly=True, states={'draft': [('readonly', False)]}, track_visibility='always')

    task_ids = fields.Many2many(comodel_name='project.task', string='Tasks', readonly=True)  # БҮТЭЭМЖ
    planned_task_hours = fields.Float(compute='_compute_task_planned_hours', string='Norm Task Planned Hours', digits=(16, 2))
    not_planned_task_hours = fields.Float(compute='_compute_not_task_planned_hours', string='Not Norm Task Planned Hours', digits=(16, 2))
    planned_task_process_value = fields.Float(compute='_compute_planned_task_process_value', string='Planned Task Process Value', digits=(16, 2))
    
    task_effective_hours = fields.Float(compute='_compute_task_effective_hours', string='Effective Hours', digits=(16, 2))
    productivity_extra_note = fields.Text(compute='_compute_productivity_extra_note', string='Note')
    productivity_project_extra_note = fields.Text(compute='_compute_productivity_project_extra_note', string='Note')

    crm_claim_ids = fields.Many2many(comodel_name='crm.claim', string='Claims', readonly=True)  # ЧАНАР ГОМДОЛ
    badge_user_ids = fields.Many2many(comodel_name='gamification.badge.user', string='Badges', readonly=True)  # ЧАНАР ЗАХИАЛАГЧИЙН МАГТААЛ
    issue_ids = fields.Many2many(comodel_name='project.issue', string='Issues', readonly=True)  # ЧАНАР АЛДАА

    productivity_total = fields.Float(compute='_compute_productivity', string='Productivity total %', digits=(16, 2))
    attitude_total = fields.Float(compute='_compute_attitude', string='Attitude total %', digits=(16, 2))
    quality_total = fields.Float(compute='_compute_quality', string='Quality total %', digits=(16, 2))

    kpi_total = fields.Float(compute='_compute_total', string='KPI Total %', digits=(16, 2), track_visibility='always')
    state = fields.Selection(STATE_SELECTION, string='State', readonly=True, default='draft', track_visibility='always')

    @api.model
    def create(self, vals):
        name = ' '
        if 'employee_id' in vals.keys() and 'period_id' in vals.keys():
            employee = self.env['hr.employee'].browse(vals['employee_id'])
            period = self.env['account.period'].browse(vals['period_id'])
            name = u'%s-н %s сарын KPI үнэлгээ' % (employee.name, period.name)
        vals.update({'name': name})
        return super(Kpi, self).create(vals)
        
    @api.multi
    def write(self, vals):
        if 'employee_id' in vals.keys() or 'period_id' in vals.keys():
            employee = self.env['hr.employee'].browse(vals['employee_id']) if 'employee_id' in vals.keys() else self.employee_id
            period = self.env['account.period'].browse(vals['period_id']) if 'period_id' in vals.keys() else self.period_id
            if employee and period:
                name = u'%s-н %s сарын KPI үнэлгээ' % (employee.name, period.name)
                vals.update({'name': name})
        return super(Kpi, self).write(vals)
    
    @api.multi
    @api.onchange('employee_id', 'period_id')
    def onchange_employee(self):
        if self.employee_id:
            if not self.employee_id.user_id:
                raise UserError(_('This employee not user!'))
            self.update({'user_id': self.employee_id.user_id.id})

        if self.period_id:
            name = u'%s-н %s сарын KPI үнэлгээ' % (self.employee_id.name, self.period_id.name)
            self.update({'name': name})
            hour_balance_line = self.env['hour.balance.line'].search([('employee_id', '=', self.employee_id.id), ('period_id', '=', self.period_id.id)])
            if hour_balance_line:
                self.update({'work_hour': hour_balance_line.reg_hour})

    @api.multi
    def import_task(self):
        task_obj = self.env['project.task']
        for obj in self:
            unplanned_tasks = task_obj.search([('date_deadline', '<=', obj.period_id.date_stop),
                                     ('date_deadline', '>=', obj.period_id.date_start),
                                     ('stage_id.closed', '=', True),
                                     ('is_planned', '=', False),
                                     ('user_id', '=', obj.user_id.id)])
            
            planned_tasks = task_obj.search([('date_deadline', '<=', obj.period_id.date_stop),
                                     ('date_deadline', '>=', obj.period_id.date_start),
                                     ('is_planned', '=', True),
                                     ('user_id', '=', obj.user_id.id)])
            
            tasks_ids = []
            if unplanned_tasks:
                tasks_ids.extend(unplanned_tasks.ids)
            if planned_tasks:
                tasks_ids.extend(planned_tasks.ids)
                
            obj.write({'task_ids': [(6, 0, tasks_ids)]})
        return True

    @api.multi
    def import_quality_rate(self):
        crm_claim_obj = self.env['crm.claim']
        badge_user_obj = self.env['gamification.badge.user']
        issue_obj = self.env['project.issue']

        for obj in self:
            if obj.user_id and obj.period_id:
                claim_ids = []
                issue_ids = []
                
                claims = crm_claim_obj.search([('user_id', '=', obj.user_id.id), ('date', '<=', obj.period_id.date_stop), ('date', '>=', obj.period_id.date_start)])
                if claims:
                    claim_ids = claims.ids
                    obj.write({'crm_claim_ids': [(6, 0, [claims.ids])]})

                badge_user_ids = []
                badges = badge_user_obj.search([('use_kpi', '=', True), ('employee_id', '=', obj.employee_id.id), ('create_date', '<=', obj.period_id.date_stop), ('create_date', '>=', obj.period_id.date_start)])
                if badges:
                    badge_user_ids = badges.ids

                #issues = issue_obj.search([('issue_made_by', '=', obj.user_id.id), ('create_date', '<=', obj.period_id.date_stop), ('create_date', '>=', obj.period_id.date_start), ('step_type', '=', 'production')])
                #if issues:
                #    issue_ids = issues.ids

                obj.write({'crm_claim_ids': [(6, 0, claim_ids)],
                           'badge_user_ids': [(6, 0, badge_user_ids)],
                           'issue_ids': [(6, 0, issue_ids)]
                           })

        return True

    @api.multi
    def import_project(self):
        project_obj = self.env['project.project']
        kpi_project_obj = self.env['kpi.project']
        project_plan_line_obj = self.env['project.plan.line']
        
        for obj in self:
            process_goals = 0.0
            productivity_percent = 70
            productivity_percent = productivity_percent / 2 if obj.type == 'manager' else productivity_percent
            
            if obj.project_lines:
                self._cr.execute("delete from kpi_project where kpi_id=%s" % (obj.id))
                self._cr.commit()

            projects = project_obj.search([('user_id', '=', obj.user_id.id)])
            for project in projects:
                project_plan_line = project_plan_line_obj.search([('project_id', '=', project.id), ('period_id', '=', obj.period_id.id)], limit=1)
                process_goals = project_plan_line.progress_goal if project_plan_line else 0.0
                progress = project_plan_line.period_progress if project_plan_line else 0.0
                weight = project_plan_line.weight if project_plan_line else 0.0
                kpi_project_id = kpi_project_obj.create({'kpi_id': obj.id, 'project_id': project.id, 'process_goals': process_goals, 'progress': progress, 'weight': weight, 'productivity_percent': productivity_percent})

        return True

    @api.multi
    def send(self):
        for obj in self:
            obj.write({'state': 'sent'})
        return True

    @api.multi
    def confirm(self):
        for obj in self:
            obj.write({'state': 'done'})
        return True

    @api.multi
    def action_cancel(self):
        for obj in self:
            obj.write({'state': 'draft'})
        return True


class AttitudeType(models.Model):
    '''Хандлага төрөл'''
    _name = 'attitude.type'
    _description = 'Attitude Type'

    @api.multi
    def name_get(self):
        result = []
        for obj in self:
            name = u'%s: [%s, %s]' % (obj.name, obj.min_point, obj.max_point)
            result.append((obj.id, name))
        return result

    name = fields.Char('Name', size=128, required=True)
    min_point = fields.Integer('Min Point', required=True, default=0)
    max_point = fields.Integer('Max Point', required=True, default=0)


class Attitude(models.Model):
    '''Хандлага'''
    _name = 'kpi.attitude'
    _description = 'Attitude'

    kpi_id = fields.Many2one('kpi', 'Kpi', readonly=True, ondelete="cascade")
    type_id = fields.Many2one('attitude.type', 'Type', required=True)
    point = fields.Float('Point', required=True, default=0)

    @api.multi
    @api.onchange('type_id')
    def onchange_type(self):
        if self.type_id:
            self.update({'point': self.type_id.min_point})

    @api.multi
    @api.onchange('point')
    def onchange_point(self):
        if self.point and self.type_id:
            if self.point > self.type_id.max_point:
                raise UserError(_('Point is Max!'))

            if self.point < self.type_id.min_point:
                raise UserError(_('Point is Min!'))


class KpiProject(models.Model):
    '''Төслийн гүйцэтгэлийн үнэлгээ'''
    _name = 'kpi.project'
    _description = 'Project Performance'

    kpi_id = fields.Many2one('kpi', 'Kpi', readonly=True, ondelete="cascade")
    project_id = fields.Many2one('project.project', 'Project', readonly=True, required=True)
    progress = fields.Float('Progress %', digits=(16, 2), default=0.0, readonly=True, required=True)
    process_goals = fields.Float('Process Goals %', readonly=True, required=True, digits=(16, 2))
    weight = fields.Float(string='Project Weight', readonly=True)
    productivity_percent = fields.Float(string='Productivity Percent')
    performance = fields.Float(compute='compute_performance')
    
    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        " Overwrite the read_group in order to sum the function field 'performance' and others in group by "
        res = super(KpiProject, self).read_group(domain, fields, groupby, offset=offset, limit=limit, orderby=orderby, lazy=lazy)
        if 'performance' in fields:
            for line in res:
                if '__domain' in line:
                    lines = self.search(line['__domain'])
                    performance = sum(line2.performance for line2 in lines)
                    line['performance'] = str(performance)
        return res
    
    @api.multi
    def compute_performance(self):
        for project in self:
            ttb = (project.progress / project.process_goals * project.productivity_percent) if project.process_goals else 0.0
            ttb = ttb if ttb <= project.productivity_percent else project.productivity_percent
            
            project.performance = ttb * project.weight

class KpiInherit(models.Model):
    _inherit = 'kpi'

    attitude_lines = fields.One2many('kpi.attitude', 'kpi_id', 'Attitude', readonly=True, states={'draft': [('readonly', False)]})
    project_lines = fields.One2many('kpi.project', 'kpi_id', 'Project', readonly=True, states={'draft': [('readonly', False)]})

    @api.multi
    def unlink(self):
        for obj in self:
            if obj.state != 'draft':
                raise UserError(_('You can only delete draft record.'))
        return super(Kpi, self).unlink()
