# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class ProjectTask(models.Model):
    _inherit = "project.task"

    period_id = fields.Many2one('account.period', 'Period', domain=[('state', '=', 'draft')])
    is_planned = fields.Boolean('Is Planned Job', default=False)

    @api.model
    def is_module_installed(self, module_name):
        self._cr.execute("SELECT id FROM ir_module_module WHERE name = '%s' AND state IN ('installed', 'to upgrade')" % module_name)
        results = self._cr.dictfetchall()

        if results and len(results) > 0:
            return True
        else:
            return False
        
    @api.onchange('date_deadline')
    def onchange_date_deadline(self):
        if self.date_deadline:
            period_id = None
            for task in self:
                period = self.env['account.period'].search([('date_start', '<=', task.date_deadline), ('date_stop', '>=', task.date_deadline)], limit=1)
                if period:
                    period_id = period.id
            self.period_id = period_id
    
class ProjectProject(models.Model):
    _inherit = 'project.project'

    plan_budget = fields.Float(string='Plan Budget ₮', digits=(16, 2))
    net_sales = fields.Float(string='Net sales revenue ₮', digits=(16, 2))
    
class ProjectIssue(models.Model):
    _inherit = "project.issue"

    STEP_SELECTION = [
        ('development', 'Development'),
        ('test', 'Test'),
        ('production', 'Production')
    ]

    step_type = fields.Selection(STEP_SELECTION, 'Step / Environment')
