# -*- coding: utf-8 -*-
# Part of Asterisk technologies. See LICENSE file for full copyright and licensing details.
{

    'name': 'Mongolian - KPI calculation with Normed Task',
    'version': '1.0',
    'author': 'Asterisk Technologies LLC',
    'category': 'Project',
    'description': """
KPI (Key Performance Indicator) with Normed Task
========================
Хүний нөөцийн үнэлгээг тооцоолно
""",
    'website': 'http://www.asterisk-tech.mn',
    'depends': [
                'l10n_mn_kpi',
                'l10n_mn_project_task_norm',
                ],
    'data': [
        'views/kpi_view.xml',
    ],
    'installable': True,
    'auto_install': True,
}
