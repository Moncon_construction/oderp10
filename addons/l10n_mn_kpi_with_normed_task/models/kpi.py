# -*- coding: utf-8 -*-
from odoo import api, fields, models, _, modules
from odoo.exceptions import UserError, ValidationError


class Kpi(models.Model):
    '''KPI - Key Performance Indicators'''
    _inherit = 'kpi'

    @api.multi
    def _compute_task_planned_hours(self):
        for obj in self:
            hours = 0.0
            if obj.task_ids:
                for task in obj.task_ids:
                    hours += (task.norm_id.time if task.is_norm else task.planned_hours) if task.is_planned else 0
            obj.planned_task_hours = hours

    @api.multi
    def _compute_not_task_planned_hours(self):
        for obj in self:
            hours = 0.0
            if obj.task_ids:
                for task in obj.task_ids:
                    hours += (task.norm_id.time  if task.is_norm else task.planned_hours) if not task.is_planned else 0
            obj.not_planned_task_hours = hours

    @api.multi
    def _compute_planned_task_process_value(self):
        for obj in self:
            hours = 0.0
            # Жигнэсэн дундажаар тооцоолол хийдэг болов
            total_hours = 0.0
            if obj.task_ids:
                for task in obj.task_ids:
                    hours += (task.progress_value * task.planned_hours_with_norm) if task.is_planned else 0
                    if task.is_planned:
                        total_hours += task.planned_hours_with_norm
            hours = hours / total_hours if total_hours > 0 else 1
            obj.planned_task_process_value = round(hours, 2)