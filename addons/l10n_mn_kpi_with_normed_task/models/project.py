# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class ProjectTask(models.Model):
    _inherit = "project.task"

    planned_hours_with_norm = fields.Float(string='Planned Hours', compute="compute_planned_hours")

    @api.multi
    def compute_planned_hours(self):
        for obj in self:
            obj.planned_hours_with_norm = obj.planned_hours if not obj.is_norm else obj.norm_id.time
        
