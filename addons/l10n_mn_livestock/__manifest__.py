# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Livestock",
    'version': '1.0',
    'depends': [
        'account',
        'stock',
        'l10n_mn_task'
    ],
    'author': "Asterisk Technologies LLC",
    'category': 'Livestock',
    'summary': 'Аж ахуйн зориулалттай мал, амьтны модуль',
    'description': """
    Энэхүү модулийг мал амьтанд суурилсан аж ахуйн үйл ажиллагаа эрхэлдэг байгууллагын биологийн хөрөнгийн бүртгэлийн хэрэгцээнд зориулав.
    """,
    'website': 'http://asterisk-tech.mn',
    'data': [
        'data/livestock_specie_cattle.xml',
        'security/ir_rule.xml',
        'security/ir.model.access.csv',
        'wizard/livestock_animal_info_change_view.xml',
        'data/compute_animal_age_cron.xml',
        'data/task_type_insemination.xml',
        'data/sequence.xml',
        'views/livestock_animal_view.xml',
        'views/livestock_specie.xml',
        'views/livestock_location.xml',
        'views/livestock_specie_breed.xml',
        'views/livestock_insemination_view.xml',
        'views/livestock_transfer.xml',
        'report/livestock_transfer_template.xml',

    ],
    'installable': True,
    'auto_install': False,
    'application': True,
    'icon': '/l10n_mn_livestock/static/description/icon.png',
}
