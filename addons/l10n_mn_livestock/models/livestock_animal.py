# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from datetime import datetime, timedelta
from dateutil import relativedelta, parser
import time


class LivestockAnimal(models.Model):
    _name = 'livestock.animal'
    _inherit = ['mail.thread']
    _description = "Animal"
    _rec_name = "number"
    _order = "number"

    number = fields.Char(string='Number', required=True)
    specie = fields.Many2one('livestock.specie', string="Specie")
    birthday = fields.Date(string='Birthday')
    age = fields.Char(compute='_compute_age', string="Age", store=True)
    animal_age = fields.Char(compute='_compute_animal_age', store=True, string="Age (year)")
    age_int = fields.Integer(compute='_compute_age_int', store=True, default=lambda x: x._compute_age_int(),
                             string="Age")
    gender = fields.Selection([('male', 'Male'),
                               ('female', 'Female')], string='Gender')
    state = fields.Selection([('draft', 'Draft'),
                               ('register', 'Register'),
                               ('closed', 'Closed')], default = 'draft', string='State')
    breed = fields.Many2one('livestock.specie.breed', string='Breed')
    skin_color = fields.Char(string="Skin color")
    father = fields.Many2one('livestock.animal', domain="[('gender','=','male')]", string="Father")
    mother = fields.Many2one('livestock.animal', domain="[('gender','=','female')]", string="Mother")
    respondent = fields.Many2one('res.partner', string='Respondent', required=True)
    current_location = fields.Many2one('livestock.settings.location', string='Current Location', ondelete='set null')
    analytic_account_id = fields.Many2one('account.analytic.account', string='Analytic Account')
    income_date = fields.Date(string="Income Date", required=True)
    income_location = fields.Many2one('livestock.settings.location', string="Income Location", ondelete='set null')
    origin = fields.Selection([('naturally', 'Naturally born'),
                               ('purchased', 'Purchased')], string='Origin')
    livestock_weight = fields.One2many('livestock.weight', 'animal_id', string='Weight')
    weight = fields.Float(compute='_compute_weight', string='Weight')
    tag_ids = fields.Many2many('project.tags', string='Tags')
    active = fields.Boolean(default=True, track_visibility='on_change', string='Active')
    livestock_insemination_ids = fields.One2many('livestock.insemination', 'animal_number', string='Insemination')
    register_number = fields.Char(string="Register Number")
    chip_number = fields.Char(string="Chip Number")
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id.id, required=True)
    gps_number = fields.Char(string="GPS Number")
    birk_number2 = fields.Char(string="Birk 2")
    is_bull = fields.Boolean( string="Bull")
    is_stamped = fields.Boolean( string="Stamped")
    is_castrated = fields.Boolean( string="Castrated")
    note = fields.Text('Note')
    image = fields.Binary('Image', attachment=True)
    images = fields.One2many('animal.image', 'animal', string='Images')
    state_insemination = fields.Char(compute='_compute_state', readonly=True, string="State")
    birth_date = fields.Date('Birth Date', compute='_compute_state', readonly=True)
    transfer_ids = fields.Many2many('livestock.transfer', 'Transfers', compute='_compute_transfers')
    milking_cows = fields.Boolean(string="Milking Cows")
    calf = fields.Boolean(string="Calf of Milking Cows")
    check_cattle = fields.Boolean(default=False)
    transfer_ids = fields.Many2many('livestock.transfer', 'Transfers', compute="_compute_transfers")
    milk_cow = fields.Many2one('livestock.animal', domain="[('gender','=','female')]", string="Milk Cow")
    milk_cow_calf = fields.Many2one('livestock.animal', string="Milk cow calf")

    @api.multi
    def _compute_transfers(self):
        for obj in self:
            transfer_ids = self.env['livestock.transfer'].search([('livestock_animal_ids', '=', obj.id)])
            obj.transfer_ids = transfer_ids.ids

    @api.multi
    def _compute_transfers(self):
        for obj in self:
            transfer_ids = self.env['livestock.transfer'].search([('livestock_animal_ids', '=', obj.id)])
            obj.transfer_ids = transfer_ids.ids

    @api.onchange('specie')
    def onchange_specie(self):
        for line in self:
            if line.specie.specie == 'Үхэр':
                line.check_cattle = True
            else:
                line.check_cattle = False

    @api.multi
    @api.depends('livestock_insemination_ids.state', 'livestock_insemination_ids.birth_date')
    def _compute_state(self):
        for obj in self:
            state_id = obj.livestock_insemination_ids.search([('animal_number', '=', obj.id)], order='repeat_insemination_date DESC', limit=1)
            obj.state_insemination = state_id.state if state_id else ""
            obj.birth_date = state_id.birth_date if state_id else False

    @api.multi
    def _compute_weight(self):
        for obj in self:
            livestock_weight_id = obj.livestock_weight.search([('animal_id', '=', obj.id)], order='date DESC', limit=1)
            obj.weight= livestock_weight_id.weight

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            name = '%s /%s/' % (record.number, record.mother.number if record.mother else '')
            result.append((record.id, name))
        return result

    @api.depends('birthday')
    @api.multi
    def _compute_age(self):
        datetime.now() + timedelta(days=1)
        for rec in self:
            if rec.birthday:
                date = datetime.strptime(rec.birthday, '%Y-%m-%d')
                today = datetime.strptime(time.strftime('%Y-%m-%d'), '%Y-%m-%d')
                date_diff = relativedelta.relativedelta(today, date)
                duration_str = ""
                year = 0
                if date_diff.years:
                    year = date_diff.years * 12
                    duration_str += str(date_diff.years) + u' жил'
                if date_diff.months:
                    duration_str += "\t" + str(date_diff.months) + u' сар'
                if date_diff.days:
                    duration_str += "\t" + str(date_diff.days) + u' өдөр'
                rec.age = duration_str
            else:
                rec.age = ''

    @api.model
    def compute_age_int_cron(self):
        animal_ids = self.env['livestock.animal'].search([])
        animal_ids._compute_age_int()

    @api.multi
    @api.depends('birthday')
    def _compute_age_int(self):
        current_date = datetime.now()
        current_year = current_date.year
        for obj in self:
            obj.age_int = current_year - parser.parse(obj.sudo().birthday).year if obj.sudo().birthday else 0

    @api.multi
    @api.depends('birthday')
    def _compute_animal_age(self):
        datetime.now() + timedelta(days=1)
        for rec in self:
            if rec.birthday:
                date = datetime.strptime(rec.birthday, '%Y-%m-%d')
                today = datetime.strptime(time.strftime('%Y-%m-%d'), '%Y-%m-%d')
                date_diff = relativedelta.relativedelta(today, date)
                duration_str = ""
                if date_diff.years:
                    duration_str += str(date_diff.years) + u' жил'
                else:
                    duration_str = 0
                rec.animal_age = duration_str
            else:
                rec.animal_age = ''

    def register(self):
        self.state = 'register'

    def closed(self):
        self.state = 'closed'

    def action_to_draft(self):
        self.state='draft'

class LivestockWeight(models.Model):

    _name = 'livestock.weight'

    animal_id = fields.Many2one('livestock.animal', string='Animal')
    date = fields.Date(string='Date')
    weight = fields.Float(string='Weight')

class AnimalImage(models.Model):
    _name = 'animal.image'
    _order = 'sequence, id DESC'

    name = fields.Char('Name')
    sequence = fields.Integer('Sequence')
    image = fields.Binary('Image', attachment=True)
    image_small = fields.Binary('Small Image', attachment=True)
    animal = fields.Many2one('livestock.animal', 'Animal')

class LivestockTransfer(models.Model):
    _name = 'livestock.transfer'

    datetime = fields.Datetime(string='Date')
    respondent_id = fields.Many2one('res.partner', string="Respondent")
    received_id = fields.Many2one('res.partner', string="Received")