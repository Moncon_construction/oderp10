# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from datetime import datetime, timedelta
import time
from odoo.exceptions import UserError


class LivestockInsemination(models.Model):
    _name = 'livestock.insemination'
    _inherit = ['mail.thread']
    _description = "animal_number"
    _rec_name = "animal_number"
    _order = "planned_date Desc"

    @api.onchange('animal_number')
    def _default_insemination_check_name(self):
        if self.animal_number.number:
            self.insemination_check_name = str(self.animal_number.number) + '-н хээлтүүлгийн шалгалт'

    animal_number = fields.Many2one('livestock.animal', string="Animal Number", domain="[('gender','=','female')]", required=True)
    insemination_id = fields.Many2one('livestock.animal', string="Sire", domain="[('gender','=','male')]", required=True)
    type = fields.Selection([
        ('artificial', 'Artificial'),
        ('natural', 'Natural')], string='Type')
    state = fields.Selection([
        ('planned', 'Planned'),
        ('executed', 'Executed'),
        ('pregnant', 'Pregnant'),
        ('no_pregnant', 'No Pregnant'),
    ], string='State', default="planned")
    planned_date = fields.Date('Planned date')
    task_id = fields.Many2one('task.task', string="Task")
    veterinarian_id = fields.Many2one('hr.employee', 'Veterinarian')
    execution_date = fields.Date('First Insemination Date')
    repeat_insemination_date = fields.Date('Repeat Insemination Date')
    insemination_check_ids = fields.One2many('livestock.insemination.check', 'insemination_id', string="Insemination check")
    insemination_material_ids = fields.One2many('livestock.material', 'insemination_id', string="Material")
    age = fields.Char(related="animal_number.age", string="Age")
    farmer = fields.Many2one('res.partner', related="animal_number.respondent", string="Farmer")
    gender = fields.Selection(related='animal_number.gender', string="Gender", store=True)
    breed = fields.Many2one('livestock.specie.breed', related='animal_number.breed', string="Breed", store=True)
    insemination_check_name = fields.Char('Insemination Check Name', default=_default_insemination_check_name)
    is_twin = fields.Boolean('Is Twin', related='insemination_check_ids.is_twin_check', readonly=1)
    number_of_babies = fields.Integer(string='Number of Babies')
    babies_smart_button = fields.Integer(string='Babies')
    number_of_expenses = fields.Integer(string='Expenses', readonly=True)

    @api.onchange('animal_number')
    @api.depends('animal_number')
    def onchange_get_gender(self):
        if self.animal_number:
            self.gender = self.animal_number.gender
            self.breed = self.animal_number.breed

    @api.onchange('animal_number', 'state', 'execution_date')
    @api.depends('animal_number', 'state', 'execution_date')
    def _birth_date(self):
        if self.animal_number.specie.birth_time and self.execution_date and self.state == 'pregnant':
            self.birth_date = datetime.strptime(self.execution_date, '%Y-%m-%d') + timedelta(days=self.animal_number.specie.birth_time)
        else:
            self.birth_date = ''

    birth_date = fields.Date('Birth Date', compute=_birth_date, store=True)

    @api.model
    def create(self, vals):
        # call super
        employee_id = self.env['hr.employee'].search([('id', '=', vals['veterinarian_id'])], limit=1)
        animal_id = self.env['livestock.animal'].search([('id', '=', vals['animal_number'])], limit=1)

        type_id = self.env.ref('l10n_mn_livestock.insemination')
        state_id = self.env.ref('l10n_mn_livestock.insemination_project_task_type_planned')

        task_vals = {
            'name': animal_id.number + '-н хээлтүүлэг',
            'task_type_id': type_id.id if type_id else False,
            'user_id': employee_id.user_id.id,
            'planned_hours': 1,
            'date_start': vals['execution_date'],
            'date_deadline': vals['execution_date'],
            'stage_id': state_id.id if state_id else False
        }
        task_id = self.env['task.task'].create(task_vals)
        vals['task_id'] = task_id.id
        res = super(LivestockInsemination, self).create(vals)
        return res

    @api.onchange('animal_number')
    def _onchange_animal_number(self):
        # Амьтанг соливол мөрүүдийн хээлийн шалгалтын нэр сонгогдсон эсэхээс үл хамааран амьтны дугаараар сольдог болгов.
        for obj in self:
            for line in obj.insemination_check_ids:
                line.name = str(obj.animal_number.number) + '-н хээлтүүлгийн шалгалт'

    def execute(self):
        if self.animal_number.specie.insemination_check_days and self.execution_date:
            date = datetime.strptime(self.execution_date,'%Y-%m-%d') + timedelta(days=self.animal_number.specie.insemination_check_days)
        else:
            date = ''

        insemination_check_vals = {
            'insemination_id': self.id,
            'date': date,
            'assigned': False,
            'state': 'planned'
        }
        self.env['livestock.insemination.check'].create(insemination_check_vals)
        self.state = 'executed'
        state_id = self.env.ref('l10n_mn_livestock.insemination_project_task_type_done')

        for material_id in self.insemination_material_ids:
            insemination_material_vals = {
                'task_id': self.task_id.id,
                'date': material_id.date,
                'warehouse_id': material_id.warehouse_id.id,
                'product_id': material_id.product_id.id,
                'product_qty': material_id.quantity,
                'product_uom': material_id.uom_id.id
            }
            self.env['task.task.product'].create(insemination_material_vals)
        self.task_id.write({'stage_id': state_id.id if state_id else False})

    @api.multi
    def action_create_baby_animal(self):
        if self.is_twin is not True:
            self.number_of_babies = 1
            self.babies_smart_button = 1
        else:
            self.babies_smart_button += 1
        for baby in range(self.number_of_babies):
            self.env['livestock.animal'].create({
                'number': 'Шинэ',
                'mother': self.animal_number.id,
                'father': self.insemination_id.id,
                'specie': self.animal_number.specie.id,
                'respondent': self.farmer.id,
                'income_date': time.strftime("%Y-%m-%d"),
                'company_id': self.animal_number.company_id.id,
                'analytic_account_id': self.animal_number.analytic_account_id.id
            })

    @api.multi
    def action_create_expense(self):
        for object in self:
            if object.number_of_babies != 0:
                line_dates = []
                for obj in object.insemination_material_ids:
                    if obj.state == 'draft':
                        if obj.date not in line_dates:
                            line_dates.append(obj.date)
                for date in line_dates:
                    expense_line = object.env['product.expense.line']
                    warehouse_id = object.env.user.allowed_warehouses.ids[0]
                    expense = object.env['product.expense'].create({
                        'date': time.strftime('%Y-%m-%d %H:%M:%S'),
                        'warehouse': warehouse_id,
                        'name': str(object.animal_number.id) + ' ' + 'малын хээлтүүлгэнд',
                        'department': object.veterinarian_id.department_id.id,
                        'stock_picking_type': object.env['stock.warehouse'].search([], limit=1).pick_type_id.id,
                        'employee': object.veterinarian_id.id})
                    object.number_of_expenses += 1
                    for material in object.insemination_material_ids:
                        if material.date == date:
                            expense_line.create(material._expense_line_from_insemination(expense))
                            material.state = 'expensed'
            else:
                raise UserError(
                    u'Эхлээд төллөх малын дугаарыг үүсгэнэ үү!')

    @api.multi
    def insemination_expense(self):
        res = []
        for line in self:
            res = self.env['product.expense']
            return {
                'type': 'ir.actions.act_window',
                'name': _('Insemination Expenses'),
                'res_model': 'product.expense',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'view_id': False,
                'domain': [('name', '=', str(self.animal_number.id) + ' ' + 'малын хээлтүүлгэнд')]
            }

    @api.multi
    def baby_animal(self):
        res = []
        for line in self:
            search_view_ref = self.env.ref('livestock.animal.view_livestock_animal_search_form', False)
            res = self.env['livestock.animal']
            return {
                'type': 'ir.actions.act_window',
                'name': _('Offsprings'),
                'res_model': 'livestock.animal',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'view_id': False,
                'search_view_id': search_view_ref,
                'domain': [('mother', '=', self.animal_number.id)]

            }

    def planned(self):
        self.state = 'planned'

    @api.multi
    def unlink(self):
        self.task_id.unlink()
        super(LivestockInsemination, self).unlink()


class LivestockInseminationCheck(models.Model):
    _name = 'livestock.insemination.check'
    _order = "date DESC"

    @api.multi
    def _default_registered_employee_id(self):
        if self.env.user.employee_ids:
            return self.env.user.employee_ids[0]

    name = fields.Char('Name')
    insemination_id = fields.Many2one('livestock.insemination', string="Insemination", readonly=True)
    date = fields.Date(string="Date", required =True)
    assigned = fields.Many2one('hr.employee', string="Assigned", readonly=True, default=_default_registered_employee_id)
    animal_id = fields.Many2one('livestock.animal', related='insemination_id.animal_number', string="Animal number", readonly=True)
    state = fields.Selection([
        ('planned', 'Planned'),
        ('executed', 'Executed'),
        ('pregnant', 'Pregnant'),
        ('no_pregnant', 'No Pregnant'),
        ('done', 'done'),
    ], string='State', readonly=True, default='executed')
    result = fields.Selection([
        ('pregnant', 'Pregnant'),
        ('no_pregnant', 'No Pregnant'),
    ], string='Result', readonly=True)
    description = fields.Char('Description')
    is_twin_check = fields.Boolean('is Twin')

    def pregnant(self):
        emp_id = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)], limit=1)
        self.assigned = emp_id
        self.state = 'done'
        self.insemination_id.write({'state': 'pregnant'})
        self.result = 'pregnant'

    def no_pregnant(self):
        emp_id = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)], limit=1)
        self.assigned = emp_id
        self.state = 'done'
        self.insemination_id.write({'state': 'no_pregnant'})
        self.result = 'no_pregnant'


class LivestockMaterial(models.Model):
    _name = 'livestock.material'

    insemination_id = fields.Many2one('livestock.insemination', string="Insemination", required=True)
    date = fields.Date(string="Date", required=True)
    warehouse_id = fields.Many2one('stock.warehouse', string='Technic', required=True)
    product_id = fields.Many2one('product.product', string="Product", required=True)
    uom_id = fields.Many2one('product.uom', string="uom", required=True)
    quantity = fields.Float(string='Quantity', required=True)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('expensed', 'Expensed'),
    ], default='draft', string='State')

    def _expense_line_from_insemination(self, expense):
        return {
            'expense': expense.id,
            'product': self.product_id.id,
            'name': self.product_id,
            'quantity': self.quantity
        }
