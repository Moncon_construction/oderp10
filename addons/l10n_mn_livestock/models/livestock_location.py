# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class LivestockSettingsLocation(models.Model):
    _name = 'livestock.settings.location'
    _description = 'Livestock Settings Location'

    name = fields.Char('Name')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id.id)