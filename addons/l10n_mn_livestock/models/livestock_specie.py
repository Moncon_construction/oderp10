# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class LivestockSpecie(models.Model):

    _name = 'livestock.specie'
    _rec_name = "specie"
    _inherit = ['mail.thread']
    _description = "Specie"
    _order = "specie"

    specie = fields.Char(string="Specie", required=True)
    birth_time = fields.Integer('Birth Time /Date/')
    product_id = fields.Many2one('product.product', string='Product')
    breed_ids = fields.One2many('livestock.breed.specie.line', 'specie_id', string='Breed')
    insemination_check_days = fields.Integer('insemination_check_days')

class LivestockBreedSpecie(models.Model):
    _name = 'livestock.breed.specie.line'

    specie_id = fields.Many2one('livestock.specie')
    breed = fields.Many2one('livestock.specie.breed', string="Breed")

