# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class LivestockSpecieBreed(models.Model):

    _name = 'livestock.specie.breed'
    _rec_name = "breed"
    _inherit = ['mail.thread']
    _description = "Breed"
    _order = "breed"

    breed = fields.Char(string="Breed")