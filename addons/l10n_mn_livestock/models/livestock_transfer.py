from odoo import api, fields, models, _
from datetime import datetime, timedelta
from dateutil import relativedelta
import time


class LivestockTransfer(models.Model):
    _name = 'livestock.transfer'
    _inherit = ['mail.thread']
    _description = "Transfer"
    _rec_name = "animal_id"
    _order = "number"

    animal_id = fields.Many2one('livestock.animal', string="Animal")
    number = fields.Char('number', readonly=True)
    respondent_id = fields.Many2one('res.partner', string="Respondent")
    datetime = fields.Datetime(string='Date')
    specie_id = fields.Many2one('livestock.specie', string="Specie")
    received_id = fields.Many2one('res.partner', string="Received")
    stock = fields.Integer(string="Stock", compute="compute_stock", store=True, readonly=True)
    age_id = fields.Many2one('livestock.animal', string='Age')
    gender = fields.Selection([('male', 'Male'),
                               ('female', 'Female')], string='Gender')
    skin_color_id = fields.Many2one('livestock.animal')
    note = fields.Text('Note')
    state = fields.Selection([('draft', 'Draft'),
                              ('register', 'Registered')], default='draft', string='State')
    livestock_animal_ids = fields.Many2many('livestock.animal', string='Animal')
    livestock_transfer_line_ids = fields.One2many('livestock.transfer.line', 'transfer_id', string='Transfer',
                                                  ondelete='cascade')

    @api.model
    def create(self, vals):
        vals['number'] = self.env['ir.sequence'].get('sequence')
        return super(LivestockTransfer, self).create(vals)

    @api.multi
    def unlink(self):
        for obj in self:
            if obj.state == 'draft':
                return super(LivestockTransfer, self).unlink()

    @api.multi
    def _compute_age(self):
        datetime.now() + timedelta(days=1)
        for rec in self:
            if rec.birthday:
                date = datetime.strptime(rec.birthday, '%Y-%m-%d')
                today = datetime.strptime(time.strftime('%Y-%m-%d'), '%Y-%m-%d')
                date_diff = relativedelta.relativedelta(today, date)
                duration_str = ""
                year = 0
                if date_diff.years:
                    year = date_diff.years * 12
                    duration_str += str(date_diff.years) + u' year'
                if date_diff.months:
                    duration_str += "\t" + str(date_diff.months) + u' month'
                if date_diff.days:
                    duration_str += "\t" + str(date_diff.days) + u' day'
                rec.age_id = duration_str
            else:
                rec.age_id = ''

    @api.depends('livestock_animal_ids')
    def compute_stock(self):
        for obj in self:
            obj.stock = len(obj.livestock_animal_ids)

    @api.multi
    def import_animal(self):
        livestock_animal_ids = self.env['livestock.animal'].search(
            [('respondent', 'in', self.mapped('respondent_id').ids)])
        self.livestock_animal_ids = livestock_animal_ids

    @api.multi
    def registering(self):
        for obj in self:
            for line in obj.livestock_animal_ids:
                obj.livestock_transfer_line_ids.create({'datetime': obj.datetime,
                                                        'respondent_id': obj.respondent_id.id,
                                                        'received_id': obj.received_id.id,
                                                        'animal_id': line.id,
                                                        'transfer_id': obj.id})
            obj.livestock_animal_ids.write({'respondent': self.received_id.id})
            obj.state = 'register'


class LivestockTransferLineIds(models.Model):
    _name = 'livestock.transfer.line'

    animal_id = fields.Many2one('livestock.animal', string='Livestock animal')
    transfer_id = fields.Many2one('livestock.transfer', string='Livestock Transfer', ondelete='cascade')
    datetime = fields.Datetime(string='Date', ondelete='cascade')
    gender = fields.Selection([('male', 'Male'),
                               ('female', 'Female')], string='Gender', ondelete='cascade')
    respondent_id = fields.Many2one('res.partner', string="Respondent", ondelete='cascade')
    received_id = fields.Many2one('res.partner', string="Received", ondelete='cascade')
    note = fields.Text('Note', ondelete='cascade')
