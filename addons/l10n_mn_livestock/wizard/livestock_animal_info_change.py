# -*- coding: utf-8 -*-
from odoo import api, fields, models


class LivestockAnimalInfoChange(models.TransientModel):
    _name = 'livestock.animal.info.change'
    _description = 'Wizard for livestock animal info change'

    tag_ids = fields.Many2many('project.tags', string='Tags')
    add_tag = fields.Boolean('Add', help='If checked the selected tags will be added.')
    is_bull = fields.Boolean(string="Bull")
    is_castrated = fields.Boolean(string="Castrated")
    is_stamped = fields.Boolean(string="Stamped")

    @api.multi
    def livestock_animal_info_change(self):
        context = dict(self._context or {})
        animal_ids = self.env['livestock.animal'].browse(context.get('active_ids'))
        for animal in animal_ids:
            if self.is_bull:
                animal.is_bull = self.is_bull
            if self.is_castrated:
                animal.paris_castratedent_id = self.is_castrated
            if self.is_stamped:
                animal.is_stamped = self.is_stamped
            if self.add_tag:
                if self.tag_ids:
                    for tag in self.tag_ids:
                        animal.write({'tag_ids': [(4, tag.id, 0)]})
            else:
                if self.tag_ids:
                    animal.tag_ids = self.tag_ids
