# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Livestock Account",
    'version': '1.0',
    'depends': [
        'l10n_mn_livestock',
        'l10n_mn_account_asset'
    ],
    'author': "Asterisk Technologies LLC",
    'category': 'Livestock',
    'summary': 'Аж ахуйн зориулалттай мал, амьтны модуль',
    'description': """
    Энэхүү модулийг мал амьтанд суурилсан аж ахуйн үйл ажиллагаа эрхэлдэг байгууллагын биологийн хөрөнгийн бүртгэлийн хэрэгцээнд зориулав.
    """,
    'website': 'http://asterisk-tech.mn',
    'data': [
        'views/account_asset.xml',
        'views/livestock_specie.xml',
        'views/res_config_view.xml',
        'views/livestock_animal.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
    'icon': '/l10n_mn_livestock/static/description/icon.png',
}
