# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from odoo.osv import expression
from odoo import exceptions


class AccountAssetCategory(models.Model):
    _inherit = 'account.asset.category'

    is_biological = fields.Boolean('Is Biological')
    account_work_in_progress = fields.Many2one('account.account', string='Incomplete Production Account',
                                               domain=[('internal_type', '=', 'other'), ('deprecated', '=', False)],
                                               oldname='account_income_recognition_id',
                                               help="Account used in the periodical entries, to record a part of the asset as expense.")


class AccountAssetAsset(models.Model):
    _inherit = 'account.asset.asset'

    is_biological = fields.Boolean('Is Biological')
    animal_id = fields.Many2one('livestock.animal', string='Animal')
    active = fields.Boolean(default=True, track_visibility='on_change', string='Active')

    def action_view_validate_account_asset_asset(self):
        if not self.animal_id:
            raise ValidationError('Create and connect animals first!')
        else:
            self.animal_id.state = 'register'
            return {
                'type': 'ir.actions.act_window',
                'name': _('Biological Asset'),
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'account.asset.validate',
                'target': 'new',
            }

    @api.one
    def get_depr_residual(self, date_str):
        if not self.is_biological:
            [res] = super(AccountAssetAsset, self).get_depr_residual(date_str)
            return res
        return self.value_residual

    @api.multi
    def compute_depreciation_board(self):
        if not self.is_biological:
            return super(AccountAssetAsset, self).compute_depreciation_board()