# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class LivestockAnimal(models.Model):
    _inherit = 'livestock.animal'

    current_location = fields.Many2one('account.asset.location', string='Current Location', ondelete='set null')
    income_location = fields.Many2one('account.asset.location', string="Income Location", ondelete='set null')
    biological_asset_id = fields.Many2one('account.asset.asset', string="Biological Asset", ondelete='cascade')
    initial_cost = fields.Float('Value', related='biological_asset_id.value')
    active = fields.Boolean(default=True, track_visibility='on_change', string='Active')

    @api.model
    def create(self, vals):
        res = super(LivestockAnimal, self).create(vals)
        asset_name = str(res.specie.specie) + ' ' + str(res.breed.breed) + ' ' + str(res.number)
        asset_user_id = res.respondent
        if not asset_user_id or not self.env['hr.employee'].search([('address_home_id', '=', asset_user_id.id)]):
            raise UserError(_("%s no connected staff found!! " % res.respondent.name))
        value = {
            'name': asset_name if asset_name else False,
            'category_id': res.specie.category_id.id if res.specie.category_id else False,
            'date': res.income_date if res.income_date else False,
            'purchase_date': res.income_date if res.income_date else False,
            'user_id': self.env['hr.employee'].search([('address_home_id', '=', asset_user_id.id)])[0].id if res.respondent else False,
            'location_id': res.income_location.id if res.income_location else False,
            'value': 0,
            'is_biological': True
        }
        asset = self.env['account.asset.asset'].create(value)
        res.biological_asset_id = asset.id
        analytic_account_vals = {
            'name': res.number,
            'code': res.number,
            'type': 'normal',
            'company_id': res.company_id.id
        }
        analytic_account_id = self.env['account.analytic.account'].create(analytic_account_vals)
        res.analytic_account_id = analytic_account_id.id
        return res

    def register(self):
        if self.biological_asset_id.state != 'open':
            raise UserError(_("Please confirm your relevant biological assets first!"))
        self.state = 'register'

    @api.multi
    def toggle_active(self):
        res = super(LivestockAnimal, self).toggle_active()
        self.mapped('biological_asset_id').toggle_active()
        return res