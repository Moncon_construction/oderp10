# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class LivestockSpecie(models.Model):
    _inherit = 'livestock.specie'

    category_id = fields.Many2one('account.asset.category', 'Asset category', domain="[('is_biological','=',True)]")

