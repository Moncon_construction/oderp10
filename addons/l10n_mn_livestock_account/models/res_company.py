# -*- coding: utf-8 -*-
from odoo import fields, models, _


class ResCompany(models.Model):
    _inherit = "res.company"

    bio_asset_sale_gain_account_id = fields.Many2one('account.account', string="Biological asset sales gain account")
    bio_asset_sale_loss_account_id = fields.Many2one('account.account', string="Biological asset sales loss account")