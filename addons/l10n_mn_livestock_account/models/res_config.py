# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class AccountConfigSettings(models.TransientModel):
    _inherit = 'account.config.settings'

    bio_asset_sale_gain_account_id = fields.Many2one('account.account', related='company_id.bio_asset_sale_gain_account_id')
    bio_asset_sale_loss_account_id = fields.Many2one('account.account', related='company_id.bio_asset_sale_loss_account_id')

    @api.onchange('company_id')
    def onchange_company_id(self):
        if self.company_id:
            company = self.company_id
            self.bio_asset_sale_gain_account_id = company.bio_asset_sale_gain_account_id
            self.bio_asset_sale_loss_account_id = company.bio_asset_sale_loss_account_id
        return super(AccountConfigSettings, self).onchange_company_id()