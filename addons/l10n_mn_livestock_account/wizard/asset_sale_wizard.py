# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from datetime import datetime

class AccountAssetAssetSell(models.TransientModel):
    _inherit = 'account.asset.asset.sell'

    # Хөрөнгө борлуулахад үүсэх журналын бичилтийн утга буцаах функц
    @api.multi
    def _get_account_move_vals(self, line, value_residual):
        if not line.asset_id.is_biological:
            return super(AccountAssetAssetSell, self)._get_account_move_vals(line, value_residual)

        invoice_line_obj = self.env['account.invoice.line']
        asset = line.asset_id
        m_line_vals = [(0, 0, {
            'name': self.description,
            'ref': line.asset_id.name,
            'account_id': asset.account_asset_id.id,
            'debit': (line.value < 0 and -line.value) or 0,
            'credit': (line.value > 0 and line.value) or 0,
            'journal_id': self.journal_id.id,
            'currency_id': asset.currency_id.id,
            'date': self.date,
            'asset_id': asset.id
        }), (0, 0, {
            'name': self.description,
            'ref': asset.name,
            'account_id': asset.category_id.account_depreciation_id.id,
            'debit': (line.value - line.value_residual > 0 and line.value - line.value_residual) or 0,
            'credit': (line.value - line.value_residual < 0 and -line.value - line.value_residual) or 0,
            'journal_id': self.journal_id.id,
            'currency_id': asset.currency_id.id,
            'date': self.date,
            'asset_id': asset.id
        })]
        inv_line = invoice_line_obj.search([('asset_id', '=', asset.id)])
        if inv_line:
            inv_line = inv_line[0]
        m_line_vals.append((0, 0, {
            'name': self.description,
            'ref': asset.name,
            'account_id': self.journal_id.default_credit_account_id.id,
            'debit': (inv_line.price_subtotal > 0 and inv_line.price_subtotal) or 0,
            'credit': (inv_line.price_subtotal < 0 and -inv_line.price_subtotal) or 0,
            'journal_id': self.journal_id.id,
            'currency_id': asset.currency_id.id,
            'date': self.date,
            'asset_id': asset.id
        }))

        debit = credit = 0.0
        account_id = asset.company_id.bio_asset_sale_loss_account_id
        if line.sell_value == line.asset_id.value_residual:
            debit = line.sell_value - inv_line.price_subtotal
        elif line.sell_value > line.asset_id.value_residual:
            credit = inv_line.price_subtotal - line.asset_id.value_residual
            account_id = asset.company_id.bio_asset_sale_gain_account_id
            if credit < 0:
                account_id = asset.company_id.bio_asset_sale_loss_account_id
                debit = - credit
                credit = 0
        else:
            debit = line.asset_id.value_residual - inv_line.price_subtotal
        m_line_vals.append((0, 0, {
            'name': self.description,
            'ref': asset.name,
            'account_id': account_id.id,
            'debit': debit,
            'credit': credit,
            'journal_id': self.journal_id.id,
            'currency_id': asset.currency_id.id,
            'date': self.date,
            'asset_id': asset.id
            }))
        move_vals = {
            'name': asset.journal_id.code + '/' + str(datetime.strptime(self.date, '%Y-%m-%d').year) + '/' + self.description,
            'date': self.date,
            'ref': self.description,
            'journal_id': asset.journal_id.id,
            'line_ids': m_line_vals
        }
        return move_vals
