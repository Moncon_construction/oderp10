# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Mongolian BBSB Loan Base ',
    'version': '1.0',
    'category': 'Localization',
    'author': 'Asterisk Technologies LLC',
    'maintainer': 'nasanochir@asterisk-tech.mn',
    'website': 'http://asterisk-tech.mn',
    'description': """
This is the base module to manage the accounting chart for Mongolian.
    Loan - Account type data
    Loan - Account chart data
    Loan - Account tax data
    Loan - Account cashflow data
    """,
    'depends': ['base', 'account', 'account_accountant'],
    'data': [
        'data/account_financial_report_data.xml',
        'data/account_account_type_data.xml',
        'data/l10n_mn_chart_data.xml',
        'data/account_account_template_data.xml',
        'data/account_chart_template_data.yml',
        'data/account_chart_cashflow_template_data.xml',
        'data/account_cashflow_template_data.xml',
        'data/account_chart_cashflow_template_data.yml',
        'data/account_account_journal_data.xml',
    ],
    'installable': True,
    'auto_install': False,
}
