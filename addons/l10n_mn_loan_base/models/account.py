# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class AccountFinancialReport(models.Model):
    _inherit = "account.financial.report"

    company_id = fields.Many2one('res.company', string='Company', required=True)
    chart_type = fields.Selection([('balance', 'Balance sheet')
                                      , ('profit', 'Income statement')
                                      , ('acc_chart', 'Account chart')
                                      , ('other', 'Other')], string='Chart type', default='other')

    def _get_children_by_order(self):
        '''returns a recordset of all the children computed recursively, and sorted by sequence. Ready for the printing'''
        res = self
        if 'company_id' in self.env.context.keys():
            children = self.search(
                [('parent_id', 'in', self.ids), ('company_id', 'in', self.env.context.get('company_id'))],
                order='sequence ASC')
        else:
            children = self.search([('parent_id', 'in', self.ids)], order='sequence ASC')
        if children:
            for child in children:
                res += child._get_children_by_order()
        return res


class AccountAccountType(models.Model):
    _inherit = "account.account.type"

    @api.multi
    @api.depends('parent_id', 'parent_id.level')
    def _get_level(self):
        for atype in self:
            level = 0
            if atype.parent_id:
                level = atype.parent_id.level + 1
            atype.level = level

    code = fields.Char(string='Code', size=10)
    sequence = fields.Integer(string='Sequence')
    parent_id = fields.Many2one('account.account.type', string='Parent', domain=[('type', '=', 'view')])
    children_ids = fields.One2many('account.account.type', 'parent_id', 'Account Type')
    level = fields.Integer(compute='_get_level', string='Level', store=True)
    type = fields.Selection(selection_add=[(('view'), _('View')),
                                           (('income'), _('Income')),
                                           (('expense'), _('Expense')),
                                           (('liability'), _('Liability'))], string='Type', required=True,
                            translate=True, default='other')
    financial_report_id = fields.Many2one('account.financial.report', string='Account Financial Report', readonly=True)
    company_id = fields.Many2one('res.company', string='Company')
    is_product = fields.Boolean('Is product', store=True)
    is_cash = fields.Boolean('Is Cash', store=True)
    is_credit = fields.Boolean('Is Credit', store=True)

    @api.onchange('parent_id')
    def onchange_parent(self):
        if self.parent_id:
            if self.parent_id.is_product:
                self.is_product = True
            else:
                self.is_product = False
            if self.parent_id.is_cash:
                self.is_cash = True
            else:
                self.is_cash = False
            if self.parent_id.is_credit:
                self.is_credit = True
            else:
                self.is_credit = False

    @api.onchange('is_cash')
    def onchange_cash(self):
        print "onchange_type-->"
        if self.is_credit == True:
            self.is_cash = False
        elif self.is_cash == True:
            self.is_credit = False

    @api.model
    def create(self, vals):
        """Дансны төрөл/бүлэг шинээр үүсгэх үед санхүүгийн тайланруу шинэ бичлэг нэмнэ.
            Санхүүгийн тайлан дээр Дансны төлөвлөгөө (xml_id = account_financial_report_account_chart) өгөгдөл файлаас сууссан байгаа.
            Хэрэв тухайн дансны төрөл нь эцэг төрөлтэй бол эцэг төрөлтэй харгалзах санхүүгийн тайлангийн дэд бичлэг болж үүснэ.
            Эцэггүй төрөл бол үндсэн Дансны төлөвлөгөө гэсэн санхүүгийн тайлангийн дэд бичлэг болж үүснэ.
        """
        root_chart_account_id = False
        root_report = self.env['ir.model.data'].get_object_reference('l10n_mn_loan_base',
                                                                     'loan_account_financial_report_account_chart')
        if root_report:
            root_chart_account_id = root_report[1]
        if not root_chart_account_id:
            raise UserError(_(
                'No Account plan was found on the financial report structure! xml_id: account_financial_report_account_chart'))

        accountType = super(AccountAccountType, self).create(vals)
        if accountType.parent_id and accountType.parent_id.financial_report_id:
            root_chart_account_id = accountType.parent_id.financial_report_id.id
        report_obj = self.env['account.financial.report'].create({'name': accountType.name,
                                                                  'sequence': accountType.sequence,
                                                                  'sign': 1,
                                                                  'type': 'sum',
                                                                  'chart_type': 'acc_chart',
                                                                  'parent_id': root_chart_account_id})
        accountType.write({'financial_report_id': report_obj.id})
        return accountType

    @api.multi
    def write(self, vals):
        """Дансны төрөл/бүлгийн мэдээллийг засах үед харгалзах санхүүгийн тайлангийн мэдээллийг шинэчлэнэ.
            Хэрэв тухайн дансны төрөлтэй харгалзах санхүүгийн тайлангийн бичлэг байхгүй бол шинээр үүсгэнэ.
            Үүсгэх хэсэг дээр тодорхойлсон create функцийн тайлбартай ижил болно.
        """
        root_chart_account_id = False
        root_report = self.env['ir.model.data'].get_object_reference('l10n_mn_loan_base',
                                                                     'loan_account_financial_report_account_chart')
        if root_report:
            root_chart_account_id = root_report[1]
        if not root_chart_account_id:
            raise UserError(_(
                'No Account plan was found on the financial report structure! xml_id: account_financial_report_account_chart'))

        result = super(AccountAccountType, self).write(vals)
        if self.parent_id and self.parent_id.financial_report_id:
            root_chart_account_id = self.parent_id.financial_report_id.id
        if not self.financial_report_id:
            report_obj = self.env['account.financial.report'].create({'name': self.name,
                                                                      'sequence': self.sequence,
                                                                      'sign': 1,
                                                                      'type': 'sum',
                                                                      'chart_type': 'acc_chart',
                                                                      'parent_id': root_chart_account_id})
            self.write({'financial_report_id': report_obj.id})
        else:
            report_obj = self.env['account.financial.report'].browse(self.financial_report_id.id)
            report_obj.write({'name': self.name,
                              'sequence': self.sequence,
                              'sign': 1,
                              'type': 'sum',
                              'chart_type': 'acc_chart',
                              'parent_id': root_chart_account_id
                              })
        if 'is_product' in vals:
            if vals['is_product']:
                if self.children_ids:
                    for child in self.children_ids:
                        child.is_product = True
                        accounts = self.env['account.account'].search([('user_type_id', '=', child.id)])
                        if accounts:
                            for acc in accounts:
                                acc.journal_entries_selectable = True
                else:
                    account_ids = self.env['account.account'].search([('user_type_id', '=', self.id)])
                    if account_ids:
                        for account in account_ids:
                            account.journal_entries_selectable = True

            elif not vals['is_product']:
                if self.children_ids:
                    for child in self.children_ids:
                        child.is_product = False
                        accounts = self.env['account.account'].search([('user_type_id', '=', child.id)])
                        if accounts:
                            for account in accounts:
                                account.journal_entries_selectable = False
                else:
                    account_ids = self.env['account.account'].search([('user_type_id', '=', self.id)])
                    if account_ids:
                        for account in account_ids:
                            account.journal_entries_selectable = False
        if ('is_credit', 'is_cash') in vals.keys():
            if self.children_ids:
                for child in self.children_ids:
                    child.is_cash = True if 'is_cash' in vals else False
                    child.is_credit = True if 'is_credit' in vals else False
        # Банк, бэлэн мөнгө сонголтуудыг зэрэг сонгогдоггүй болгов
        if 'is_credit' in vals:
            if self.is_cash == True and vals['is_credit'] == True:
                raise UserError(_('Please, select one.'))
        if 'is_cash' in vals:
            if self.is_credit == True and vals['is_cash'] == True:
                raise UserError(_('Please, select one.'))
        return result


class AccountAccount(models.Model):
    _inherit = "account.account"

    financial_report_id = fields.Many2one('account.financial.report', string='Account Financial Report', readonly=True)

    @api.model
    def create(self, vals):
        """Данс шинээр үүсгэх үед санхүүгийн тайланруу шинэ бичлэг нэмнэ.
            Тухайн дансны төрөлтэй харгалзах санхүүгийн тайлангийн дэд бичлэг болж үүснэ.
        """
        root_chart_account_id = False
        accountAccount = super(AccountAccount, self).create(vals)
        if accountAccount.user_type_id and accountAccount.user_type_id.financial_report_id:
            root_chart_account_id = accountAccount.user_type_id.financial_report_id.id
        report_obj = self.env['account.financial.report'].create({'name': accountAccount.name,
                                                                  'sequence': accountAccount.user_type_id.sequence,
                                                                  'sign': 1,
                                                                  'type': 'accounts',
                                                                  'chart_type': 'acc_chart',
                                                                  'account_ids': [(6, 0, [accountAccount.id])],
                                                                  'parent_id': root_chart_account_id,
                                                                  'company_id': accountAccount.company_id.id
                                                                  })
        accountAccount.write({'financial_report_id': report_obj.id})
        return accountAccount

    @api.multi
    def write(self, vals):
        """Данс засах үед харгалзах санхүүгийн тайлангийн мэдээллийг шинэчлэнэ.
            Хэрэв тухайн данстай харгалзах санхүүгийн тайлангийн бичлэг байхгүй бол шинээр үүсгэнэ.
            Үүсгэх хэсэг дээр тодорхойлсон create функцийн тайлбартай ижил болно.
        """
        root_chart_account_id = False
        result = super(AccountAccount, self).write(vals)
        if self.user_type_id and self.user_type_id.financial_report_id:
            root_chart_account_id = self.user_type_id.financial_report_id.id
        if not self.financial_report_id:
            report_obj = self.env['account.financial.report'].create({'name': self.name,
                                                                      'sequence': self.user_type_id.sequence,
                                                                      'sign': 1,
                                                                      'chart_type': 'acc_chart',
                                                                      'type': 'accounts',
                                                                      'account_ids': [(6, 0, [self.id])],
                                                                      'parent_id': root_chart_account_id,
                                                                      'company_id': self.company_id.id
                                                                      })
            self.write({'financial_report_id': report_obj.id})
        else:
            report_obj = self.env['account.financial.report'].browse(self.financial_report_id.id)
            report_obj.write({'name': self.name,
                              'sequence': self.user_type_id.sequence,
                              'sign': 1,
                              'type': 'accounts',
                              'chart_type': 'acc_chart',
                              'account_ids': [(6, 0, [self.id])],
                              'parent_id': root_chart_account_id,
                              'company_id': self.company_id.id
                              })
        return result
