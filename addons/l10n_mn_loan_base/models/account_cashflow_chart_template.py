# -*- coding: utf-8 -*-

from odoo import fields, models, api, _

import logging

from odoo.exceptions import AccessError

_logger = logging.getLogger(__name__)


class AccountCashflowTemplate(models.Model):
    _name = "account.cashflow.template"
    _description = "Templates for Account Cashflow Chart"
    _order = "code"

    name = fields.Char(required=True, index=True)
    code = fields.Char(size=64, required=True, index=True)
    c_type = fields.Selection([('view', 'View'), ('normal', 'Normal')], 'Type')
    parent_id = fields.Many2one('account.cashflow.template', 'Parent', domain=[('c_type', '=', 'view')])
    chart_cashflow_template_id = fields.Many2one('account.chart.cashflow.template', string='Chart Template')
    value_of_amount_type = fields.Selection([('positive', 'Positive'), ('negative', 'Negative'), ('mixed', 'Mixed')], required=True)

    @api.multi
    @api.depends('name', 'code')
    def name_get(self):
        res = []
        for record in self:
            name = record.name
            if record.code:
                name = record.code + ' ' + name
            res.append((record.id, name))
        return res


class AccountChartCashflowTemplate(models.Model):
    _name = "account.chart.cashflow.template"
    _description = "Templates for Account Chart"

    name = fields.Char(required=True)
    company_id = fields.Many2one('res.company', string='Company')
    cashflow_ids = fields.One2many('account.cashflow.template', 'chart_cashflow_template_id', string='Cashflow Templates', domain={'invisible':[('report_cashflow_type', '=', 'simple')]})
    parent_id = fields.Many2one('account.chart.cashflow.template', 'Parent')

    @api.multi
    def _install_template(self, company, obj_wizard=None, acc_ref=None):
        self.ensure_one()
        if acc_ref is None:
            acc_ref = {}
        if self.parent_id:
            tmp = self.parent_id._install_template(company, acc_ref=acc_ref)
            acc_ref.update(tmp)
        tmp = self._load_template(company, account_ref=acc_ref)
        acc_ref.update(tmp)
        return acc_ref

    @api.multi
    def _load_template(self, company, account_ref=None):
        if account_ref is None:
            account_ref = {}

        # Generating Accounts from templates.
        account_template_ref = self.generate_account(account_ref, company)
        account_ref.update(account_template_ref)
        return account_ref

    @api.multi
    def generate_account(self, acc_template_ref, company):
        self.ensure_one()
        account_tmpl_obj = self.env['account.cashflow.template']
        acc_template = account_tmpl_obj.search([('chart_cashflow_template_id', '=', self.chart_cashflow_template_id.id)], order='id')
        for account_template in acc_template:
            code_main = account_template.code and len(account_template.code) or 0
            code_acc = account_template.code or ''
            if code_main > 0:
                code_acc = str(code_acc)
            vals = self._get_account_vals(company, account_template, code_acc)
            new_account = self.create_record_with_xmlid(company, account_template, 'account.account', vals)
            acc_template_ref[account_template.id] = new_account
        return acc_template_ref

    def _get_account_vals(self, company, account_template, code_acc, tax_ids=None):
        self.ensure_one()
        for tax in account_template.tax_ids:
            tax_ids.append(tax_template_ref[tax.id])
        val = {
                'name': account_template.name,
                'currency_id': account_template.currency_id and account_template.currency_id.id or False,
                'code': code_acc,
                'user_type_id': account_template.user_type_id and account_template.user_type_id.id or False,
                'reconcile': account_template.reconcile,
                'note': account_template.note,
                'tax_ids': [(6, 0, tax_ids)],
                'company_id': company.id,
                'tag_ids': [(6, 0, [t.id for t in account_template.tag_ids])],
            }
        return val

    @api.multi
    def create_record_with_xmlid(self, company, template, model, vals):
        # Create a record for the given model with the given vals and 
        # also create an entry in ir_model_data to have an xmlid for the newly created record
        # xmlid is the concatenation of company_id and template_xml_id
        ir_model_data = self.env['ir.model.data']
        template_xmlid = ir_model_data.search([('model', '=', template._name), ('res_id', '=', template.id)])
        new_xml_id = str(company.id) + '_' + template_xmlid.name
        return ir_model_data._update(model, template_xmlid.module, vals, xml_id=new_xml_id, store=True, noupdate=True, mode='init', res_id=False)

    @api.one
    def try_loading_for_current_company(self):
        self.ensure_one()
        company = self.env.user.company_id
        if not company.chart_cashflow_template_id:
            wizard = self.env['wizard.multi.charts.cashflows'].create({
                'company_id': company.id,
                'chart_cashflow_template_id': self.id})
            wizard.execute()


class WizardMultiChartsCashflows(models.TransientModel):
    _name = 'wizard.multi.charts.cashflows'
    _inherit = 'res.config'

    company_id = fields.Many2one('res.company', string='Company', required=True)
    chart_cashflow_template_id = fields.Many2one('account.chart.cashflow.template', string='Chart Template')
    
    @api.multi
    def execute(self):
        self.ensure_one()
        if not self.env.user._is_admin():
            raise AccessError(_("Only administrators can change the settings"))
        if self.chart_cashflow_template_id:
            self.company_id.write({'chart_cashflow_template_id': self.chart_cashflow_template_id.id})
            account_tmpl_obj = self.env['account.cashflow.template']
            ir_model_data = self.env['ir.model.data']
            cashflow_obj = self.env['account.cashflow.type']
            acc_template = account_tmpl_obj.search([('chart_cashflow_template_id', '=', self.chart_cashflow_template_id.id)])
            acc_template1 = acc_template.search([('parent_id', '=', False)], order='code')
            acc_template2 = acc_template.search([('parent_id', '!=', False)], order='code')

            for template in acc_template1:
                template_xmlid = ir_model_data.search([('model', '=', template._name), ('res_id', '=', template.id)])
                new_xml_id = str(self.company_id.id) + '_' + template_xmlid.name
                code_acc = template.code or ''
                vals = {
                        'name': template.name,
                        'code': code_acc,
                        'type': template.c_type,
                        'company_id': self.company_id.id,
                        'value_of_amount_type': template.value_of_amount_type
                    }
                ir_model_data._update('account.cashflow.type', template_xmlid.module, vals, xml_id=new_xml_id, store=True, noupdate=True, mode='init', res_id=False)

            for template in acc_template2:
                template_xmlid = ir_model_data.search([('model', '=', template._name), ('res_id', '=', template.id)])
                new_xml_id = str(self.company_id.id) + '_' + template_xmlid.name
                code_acc = template.code or ''
                template_parent = cashflow_obj.search([('code', '=', template.parent_id.code)], limit=1)
                vals = {
                        'name': template.name,
                        'code': code_acc,
                        'type':template.c_type,
                        'company_id': self.company_id.id,
                        'parent_id': template_parent.id,
                        'value_of_amount_type': template.value_of_amount_type
                }
                ir_model_data._update('account.cashflow.type', template_xmlid.module, vals, xml_id=new_xml_id, store=True, noupdate=True, mode='init', res_id=False)
