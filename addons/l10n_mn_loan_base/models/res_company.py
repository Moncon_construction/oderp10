# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class ResCompany(models.Model):
    _inherit = "res.company"

    chart_cashflow_template_id = fields.Many2one('account.chart.cashflow.template', help='The chart template for the company (if any)')
    has_chart_of_cashflows = fields.Boolean(string='Company has a chart of cashflows', default=False)