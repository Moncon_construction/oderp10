# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': "Mongolian Loan Management",
    'version': '1.0',
    'depends': ['analytic', 'hr', 'l10n_mn_account_period', 'l10n_mn_loan_base', 'l10n_mn_contacts_extra',
                'l10n_mn_report'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
        Зээлийн модуль
        Зээлдэгч цэс
        Зээлийн хүсэлт
        Зээлийн гэрээ
        Зээлийн төлөлт
        Бүтээгдэхүүний тохиргоо
        Бүртээгдэхүүн бүртгэл
        Зориулалт бүртгэл
        Хөрөнгийн төрөл
        Төлөлтийн хуваарь
    """,
    'website': 'http://asterisk-tech.mn',
    'data': [
        # Эрхийн тохиргоо
        "security/loan_security.xml",
        "security/ir.model.access.csv",
        # Мастер өгөгдлүүд
        "data/demo_data.xml",
        "data/mail_template_view.xml",
        "data/loan_report_paperformat.xml",
        "data/loan_report_data.xml",
        "data/profit_report_data.xml",
        # Тайлангууд
        "report/loan_schedule_report.xml",
        'report/loan_progress_control_report_view.xml',
        'report/overdue_loan_report_view.xml',
        # Тайлан, тооцооллын визардууд
        "wizard/loan_order_statement.xml",
        "wizard/loan_schedule_report_view.xml",
        "wizard/closed_loan_report_view.xml",
        "wizard/loan_monthly_discount_view.xml",
        "wizard/create_multiple_payments_in_month_view.xml",
        "wizard/create_loan_accrued_interest.xml",
        'wizard/loan_collateral_report_view.xml',
        'wizard/create_refund_loan_accrued_interest.xml',
        # Үндсэн болон тохиргооны харагдацууд
        "views/account_bank_statement_import_loan.xml",
        "views/account_bank_statement_import_payment_loan.xml",
        "views/account_equity_change_report_view.xml",
        "views/sequence_view.xml",
        "views/res_partner_view.xml",
        "views/loan_product_view.xml",
        "views/loan_purpose_view.xml",
        "views/loan_grap_coll_view.xml",
        "views/loan_order_view.xml",
        "views/loan_payment_view.xml",
        "views/loan_category_change_view.xml",
        "views/loan_risk_fund_view.xml",
        # "views/loan_schedule_view.xml",
        "views/loan_history_statement_view.xml",
        # Цэс
        "views/menu_view.xml",
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
