# -*- coding: utf-8 -*-
import account
import account_invoice
import res_partner
import res_company
import loan_product
import loan_collateral
import loan_history_statement
import loan_schedule
import loan_order
import loan_payment
import loan_purpose
import loan_negative_info
import loan_category_change
import loan_risk_fund
import account_bank_statement_import_loan
import account_bank_statement_import_payment_loan
import account_bank_statement