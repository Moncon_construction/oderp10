# -*- encoding: utf-8 -*-

from odoo import fields, models


class AccountMove(models.Model):
    _inherit = "account.move"

    period_id = fields.Many2one('account.period', string='Period')
    loan_id = fields.Many2one('loan.order', string='Loan')
    loan_risk_fund_id = fields.Many2one('loan.risk.fund', string='Loan Risk Fund')
