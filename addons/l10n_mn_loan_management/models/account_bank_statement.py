# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
from datetime import datetime


class AccountBankStatementLine(models.Model):
    _inherit = 'account.bank.statement.line'

    loan_id = fields.Many2one('loan.order', string='Loan Order')
    loan_collected_interest_move_id = fields.Many2one('account.move', string='Loan Collected Interest Account Move')
    paid_loan_schedule_id = fields.Many2one('loan.schedule', 'Paid Loan Schedule')
    is_late_pay = fields.Boolean(string='Is late payment?', default=False)
    is_balance_line = fields.Boolean(string='Is balance line?', default=False)

    # Касс харилцахаас зээлийн олголт болон төлөлт хийж батлах үед тухайн зээлийн төлвийг өөрчлөх
    def process_reconciliation(self, counterpart_aml_dicts=None, payment_aml_rec=None, new_aml_dicts=None):
        for line in self:
            if line.loan_id.state == 'paid':
                line.loan_id.write({'state': 'done'})
                line.loan_id.write({'date_approve': datetime.now()})
            elif line.loan_id.state == 'done':
                if line.is_balance_line:
                    if line.is_late_pay:
                        line.paid_loan_schedule_id.is_paid = 'late_paid'
                    else:
                        line.paid_loan_schedule_id.is_paid = 'paid'
            return super(AccountBankStatementLine, self).process_reconciliation(counterpart_aml_dicts, payment_aml_rec,
                                                                                new_aml_dicts)
