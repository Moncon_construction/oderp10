# -*- encoding: utf-8 -*-
from odoo import fields, models, _


class AccountBankStatementImportLoan(models.TransientModel):
    _name = "account.bank.statement.import.loan"

    import_loan = fields.Char('Name')
    loan_ids = fields.Many2many('loan.order', string='Loan order')

    def import_loan(self):
        statement_line_obj = self.env['account.bank.statement.line']
        loan_category_obj = self.env['loan.category.change.type']
        context = dict(self._context or {})
        statement_id = context.get('statement_id', False)
        if not statement_id:
            return {'type': 'ir.actions.act_window_close'}
        statement = self.env['account.bank.statement'].browse(statement_id)
        for line in self.loan_ids:
            bank_account_id = False
            partner_bank = self.env['res.partner.bank'].search([('partner_id', '=', line.partner_id.id)], limit=1)
            if partner_bank:
                bank_account_id = partner_bank.id

            if line.loan_state:
                if line.loan_state.account_citizen_id:
                    account_id = line.loan_state.account_citizen_id
                    if line.loan_state.account_citizen_id.currency_id:
                        currency_id = line.loan_state.account_citizen_id.currency_id
                else:
                    account_id = False
                    currency_id = False
            # Зээлийн олголтыг касс харилцахын мөрд импортлох
            lending_line_id = statement_line_obj.create({
                'name': line.name,
                'amount': -line.amount,
                'partner_id': line.partner_id.id,
                'bank_account_id': bank_account_id,
                'statement_id': statement.id,
                'ref': line.name,
                # 'amount_currency': line.amount_currency,
                'currency_id': line.product_id.currency_id.id,
                'account_id': account_id.id or False,
                'cashflow_id': account_id.cashflow_account_ids[0].id if account_id.cashflow_account_ids else False,
                'loan_id': line.id})
            # Зээлийн шимтгэлийн импортлох
            if line.loan_fee_amount:
                if line.product_id.loan_fee_account_id:
                    account_id = line.product_id.loan_fee_account_id
                statement_line_obj.create({
                    'name': line.name + _(' : Loan Fee'),
                    'amount': line.loan_fee_amount,
                    'partner_id': line.partner_id.id,
                    'bank_account_id': bank_account_id,
                    'statement_id': statement.id,
                    'ref': line.name + _(' : Loan Fee'),
                    # 'amount_currency': line.amount_currency,
                    'currency_id': line.product_id.currency_id.id,
                    'account_id': account_id.id or False,
                    'cashflow_id': account_id.cashflow_account_ids[0].id if account_id.cashflow_account_ids else False,
                    'loan_id': line.id})
            line.write({'loan_lending_statement_line_id': lending_line_id.id})
            category_id = loan_category_obj.search([('code', '=', 'normal_loan')])
            line.write({'loan_state': category_id.id})
        return {'type': 'ir.actions.act_window_close'}
