# -*- encoding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class AccountBankStatementImportPaymentLoan(models.TransientModel):
    _name = "account.bank.statement.import.payment.loan"

    import_loan = fields.Char('Name')
    loan_ids = fields.Many2many('loan.order', string='Loan order')
    loan_schedule_ids = fields.Many2many('loan.schedule', string='Loan Schedule')
    is_late_pay = fields.Boolean(string='Is late payment?', default=False)

    # Сонгогдсон зээлийн гэрээний эргэн төлөлтийн хуваариас төлөгдөөгүйг татах
    @api.multi
    def get_schedules(self):
        for obj in self:
            for loan_id in obj.loan_ids:
                if loan_id.schedules_ids:
                    unpaid_schedule_ids = loan_id.schedules_ids.filtered(lambda line: line.is_paid == 'un_paid')
                    for schedule in unpaid_schedule_ids:
                        schedule.do_pay = False
                    obj.update({'loan_schedule_ids': [(6, 0, unpaid_schedule_ids.ids)]})
        return {
            "type": "ir.actions.do_nothing",
        }

    # Сонгосон эргэн төлөлтийн үндсэн зээлийн төлбөр болон хуримтлагдсан хүү, үндсэн зээлийн хүүгийн орлогыг
    # касс харилцахын мөрд татах
    def import_loan(self):
        statement_line_obj = self.env['account.bank.statement.line']
        context = dict(self._context or {})
        statement_id = context.get('statement_id', False)
        statement_id = self.env['account.bank.statement'].browse(statement_id)
        for obj in self:
            selected_schedule_ids = obj.loan_schedule_ids.filtered(lambda line: line.do_pay)
            for schedule_id in selected_schedule_ids:
                if schedule_id.loan_payment and schedule_id.accrued_interest and schedule_id.interest_income_on_time:
                    currency_id = False
                    account_id = False
                    if schedule_id.contract_id.loan_state:
                        if schedule_id.contract_id.loan_state.account_citizen_id:
                            account_id = schedule_id.contract_id.loan_state.account_citizen_id
                            if schedule_id.contract_id.loan_state.account_citizen_id.currency_id:
                                currency_id = schedule_id.contract_id.loan_state.account_citizen_id.currency_id
                        else:
                            account_id = False
                            currency_id = False
                    if obj.is_late_pay:
                        if not schedule_id.pay_payment:
                            raise UserError(_('Pay payment is zero!'))
                        # Үндсэн зээлийн төлөлт хоцорч төлөгдөж байгаа бол
                        # зээлийн дүн төлөх дүн талбараас авна
                        statement_line_obj.create({
                            'name': schedule_id.name,
                            'amount': schedule_id.pay_payment,
                            'partner_id': schedule_id.contract_id.partner_id.id,
                            'statement_id': statement_id.id,
                            'ref': schedule_id.contract_id.name,
                            'currency_id': currency_id.id if currency_id else False,
                            'account_id': account_id.id if account_id else False,
                            'cashflow_id': False,
                            'loan_id': schedule_id.contract_id.id,
                            'is_late_pay': True,
                            'is_balance_line': True,
                            'paid_loan_schedule_id': schedule_id.id
                        })
                    else:
                        # Үндсэн зээлийн төлөлт
                        statement_line_obj.create({
                            'name': schedule_id.name,
                            'amount': schedule_id.loan_payment,
                            'partner_id': schedule_id.contract_id.partner_id.id,
                            'statement_id': statement_id.id,
                            'ref': schedule_id.contract_id.name,
                            'currency_id': currency_id.id if currency_id else False,
                            'account_id': account_id.id if account_id else False,
                            'cashflow_id': False,
                            'loan_id': schedule_id.contract_id.id,
                            'is_late_pay': False,
                            'is_balance_line': True,
                            'paid_loan_schedule_id': schedule_id.id
                        })
                    # Хуримтлуулсан хүүгийн авлага
                    if schedule_id.contract_id.loan_state.account_citizen_id:
                        account_id = schedule_id.contract_id.product_id.accrued_loan_interest_receivables_account_id
                    else:
                        account_id = False
                    statement_line_obj.create({
                        'name': schedule_id.name,
                        'amount': schedule_id.accrued_interest,
                        'partner_id': schedule_id.contract_id.partner_id.id,
                        'statement_id': statement_id.id,
                        'ref': schedule_id.contract_id.name,
                        'currency_id': currency_id.id if currency_id else False,
                        'account_id': account_id.id if account_id else False,
                        'cashflow_id': False,
                        'loan_id': schedule_id.contract_id.id,
                        'paid_loan_schedule_id': schedule_id.id
                    })
                    # Үндсэн зээлийн хүүгийн орлого - Хугацаандаа байгаа зээлийн хүүгийн орлого
                    if schedule_id.contract_id.loan_state.account_citizen_id:
                        account_id = schedule_id.contract_id.loan_state.income_citizen_account_id
                    else:
                        account_id = False
                    # Касс харилцахын хуулгын мөр үүсгэх
                    statement_line_obj.create({
                        'name': schedule_id.name,
                        'amount': schedule_id.interest_income_on_time,
                        'partner_id': schedule_id.contract_id.partner_id.id,
                        'statement_id': statement_id.id,
                        'ref': schedule_id.contract_id.name,
                        'currency_id': currency_id.id if currency_id else False,
                        'account_id': account_id.id if account_id else False,
                        'cashflow_id': False,
                        'loan_id': schedule_id.contract_id.id,
                        'paid_loan_schedule_id': schedule_id.id
                    })

                    # Үндсэн зээлийн хүүгийн орлого - Хугацаа хэтэрсэн зээлийн хүүгийн орлого
                    if schedule_id.contract_id.loan_state.overdue_income_citizen_account_id:
                        account_id = schedule_id.contract_id.loan_state.overdue_income_citizen_account_id
                    else:
                        raise UserError(_('Loan Overdue Income account did not selected!'))
                    # Нэмэгдүүлсэн хүүгийн касс харилцахын хуулгын мөр үүсгэх
                    if schedule_id.increased_interest:
                        statement_line_obj.create({
                            'name': schedule_id.name,
                            'amount': schedule_id.increased_interest,
                            'partner_id': schedule_id.contract_id.partner_id.id,
                            'statement_id': statement_id.id,
                            'ref': schedule_id.contract_id.name,
                            'currency_id': currency_id.id if currency_id else False,
                            'account_id': account_id.id if account_id else False,
                            'cashflow_id': False,
                            'loan_id': schedule_id.contract_id.id,
                            'paid_loan_schedule_id': schedule_id.id
                        })

                elif not schedule_id.accrued_interest:
                    raise UserError(_('Loan schedule did not compute accrued interest!'))
                elif not schedule_id.interest_income_on_time:
                    raise UserError(_('Loan schedule did not compute interest income!'))
        return {'type': 'ir.actions.act_window_close'}
