# -*- encoding: utf-8 -*-

from odoo import fields, models, fields


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    is_collect_loan_interest = fields.Boolean('To collect loan interest?', default=False)
