# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
from odoo.exceptions import UserError
from datetime import datetime


class LoanCategoryChangeType(models.Model):
    _name = "loan.category.change.type"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = "Loan cateogry change type"

    name = fields.Char('category change type name', size=64, help='Нэр төрөл')
    code = fields.Char('category change type code', size=64, help='Код')
    sequence = fields.Char('category change type sequence', size=64, help='Дарааллын код')
    # Зээлийн ангилалын дансууд
    account_government_agency_id = fields.Many2one('account.account', 'Government Agency')  # Улсын байгууллага
    account_private_sector_id = fields.Many2one('account.account', 'Private Sector')  # Хувийн сектор
    account_other_financial_institutions_id = fields.Many2one('account.account',
                                                              'Other Financial Institutions')  # Бусад санхүүгийн байгууллага
    account_citizen_id = fields.Many2one('account.account', 'Citizen')  # Иргэн
    account_foreign_resident_id = fields.Many2one('account.account', 'Foreign Resident')  # Гадаадын оршин суугч
    account_non_foreign_resident_id = fields.Many2one('account.account',
                                                      'Not a Foreign Resident')  # Гадаадын оршин суугч бус
    account_other_id = fields.Many2one('account.account', 'Other')  # Бусад

    # Зээлийн ангилалын орлогын дансууд
    # Хугацаандаа байгаа зээлийн орлогын дансууд
    income_government_agency_account_id = fields.Many2one('account.account',
                                                          'Income Government Agency')  # Улсын байгууллага
    income_private_sector_account_id = fields.Many2one('account.account', 'Income Private Sector')  # Хувийн сектор
    income_other_financial_institutions_account_id = fields.Many2one('account.account',
                                                                     'Income Other Financial Institutions')  # Бусад санхүүгийн байгууллага
    income_citizen_account_id = fields.Many2one('account.account', 'Income Citizen')  # Иргэн
    income_foreign_resident_account_id = fields.Many2one('account.account',
                                                         'Income Foreign Resident')  # Гадаадын оршин суугч
    income_non_foreign_resident_account_id = fields.Many2one('account.account',
                                                             'Income Not a Foreign Resident')  # Гадаадын оршин суугч бус
    income_other_account_id = fields.Many2one('account.account', 'Income Other')  # Бусад
    # Хугацаа хэтэрсэн зээлийн оорлогын данс
    overdue_income_government_agency_account_id = fields.Many2one('account.account',
                                                                  'Overdue Income Government Agency')  # Улсын байгууллага
    overdue_income_private_sector_account_id = fields.Many2one('account.account',
                                                               'Overdue Income Private Sector')  # Хувийн сектор
    overdue_income_other_financial_institutions_account_id = fields.Many2one('account.account',
                                                                             'Overdue Income Other Financial Institutions')  # Бусад санхүүгийн байгууллага
    overdue_income_citizen_account_id = fields.Many2one('account.account', 'Overdue Income Citizen')  # Иргэн
    overdue_income_foreign_resident_account_id = fields.Many2one('account.account',
                                                                 'Overdue Income Foreign Resident')  # Гадаадын оршин суугч
    overdue_income_non_foreign_resident_account_id = fields.Many2one('account.account',
                                                                     'Overdue Income Not a Foreign Resident')  # Гадаадын оршин суугч бус
    overdue_income_other_account_id = fields.Many2one('account.account', 'Overdue Income Other')  # Бусад
    # Хүүгийн орлогын буцаалтын данс
    interest_income_refund_account_id = fields.Many2one('account.account', 'Interest Income Refund Account')


class LoanCategoryChange(models.Model):
    _name = "loan.category.change"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = "Loan category changed"
    _order = 'name desc'

    name = fields.Char('Name', size=64)
    loan_order_id = fields.Many2one('loan.order', 'Loan contract number', domain=[('state', '=', 'done')])
    partner_id = fields.Many2one('res.partner', 'Debtor', domain=[('is_debtor', '=', True)])
    date_order = fields.Date('Date', default=datetime.now())
    date_confirm = fields.Date('Date confirm', readonly=True)
    user_id = fields.Many2one('res.users', 'User', default=lambda self: self._uid)
    company_id = fields.Many2one('res.company', string='Company', store=True, readonly=True,
                                 default=lambda self: self.env.user.company_id)
    loan_state_to = fields.Many2one('loan.category.change.type', 'loan state to')
    loan_state = fields.Many2one('loan.category.change.type', 'loan state')
    journal_id = fields.Many2one('account.journal', 'Journal')
    category_change_account_move_id = fields.Many2one('account.move', 'Category Change Account Move')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirmed', 'Confirmed'),
        ('done', 'Done'),
    ], 'State', readonly=True, default='draft')

    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Loan payment number must be unique!'),
    ]

    @api.onchange('loan_order_id')
    def onchange_loan_order(self):
        for obj in self:
            if obj.loan_order_id:
                if obj.loan_order_id.loan_state:
                    obj.loan_state_to = obj.loan_order_id.loan_state.id
                if obj.loan_order_id.date_order:
                    obj.date_order = obj.loan_order_id.date_order
                if obj.loan_order_id.partner_id:
                    obj.partner_id = obj.loan_order_id.partner_id.id

    @api.multi
    def action_to_confirmed(self):
        for obj in self:
            obj.write({'state': 'confirmed',
                       'name': self.env['ir.sequence'].get('loan.category.change'),
                       'date_confirm': datetime.now()})
        return True

    @api.multi
    def action_to_done(self):
        for obj in self:
            obj.write({'state': 'done'})
            obj.create_category_change_account_move()
        return True

    @api.multi
    def action_cancel(self):
        for obj in self:
            obj.write({'state': 'draft'})
            if obj.category_change_account_move_id:
                obj.category_change_account_move_id.button_cancel()
                obj.category_change_account_move_id = False
        return True

    # Зээлийн шилжүүлэх 2 ангилалын хооронд
    # журналын бичилт үүсгэх функ
    @api.multi
    def create_category_change_account_move(self):
        account_move_obj = self.env['account.move']
        for obj in self:
            if not obj.category_change_account_move_id:
                move_line_ids = [(0, 0, {
                    'name': obj.name,
                    'debit': obj.loan_order_id.amount,
                    'credit': 0,
                    'account_id': obj.loan_state.account_citizen_id.id,
                    'amount_currency': 0,
                    'currency_id': False,
                    'date': obj.date_order,
                    'journal_id': obj.journal_id.id,
                    'partner_id': obj.partner_id.id,
                }), (0, 0, {
                    'name': obj.name,
                    'debit': 0,
                    'credit': obj.loan_order_id.amount,
                    'account_id': obj.loan_state_to.account_citizen_id.id,
                    'amount_currency': 0,
                    'currency_id': False,
                    'date': obj.date_order,
                    'journal_id': obj.journal_id.id,
                    'partner_id': obj.partner_id.id,
                })]
                # Шилжүүлэх гэж буй данс Дт
                # Шилжих гэж буй данс Кт
                # Зээлийн ангилал солих ЖБ
                account_move_id = account_move_obj.create({
                    'journal_id': obj.journal_id.id,
                    'date': obj.date_order,
                    'loan_id': obj.loan_order_id.id,
                    'description': _("%s :: %s /Loan Category Change/" % (obj.loan_order_id.name, obj.name)),
                    'line_ids': move_line_ids})
                obj.category_change_account_move_id = account_move_id.id
                obj.loan_order_id.loan_state = obj.loan_state.id
            elif obj.category_change_account_move_id.state == 'posted':
                raise UserError(_('Loan Category Change Account Move Already Posted! Cancel before create'))
