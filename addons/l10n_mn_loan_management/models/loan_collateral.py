# -*- coding: utf-8 -*-
from odoo import models, fields


class LoanCollateral(models.Model):
    _name = "loan.collateral"
    _description = "Collateral"

    name = fields.Char('Description', related='contract_id.name')
    contract_id = fields.Many2one('loan.order', string='Contract number')
    certificate_no = fields.Char(string='Certificate number')
    bvrtgel_no = fields.Char(string='Registration number')
    remark = fields.Char(string='Remark')
    name_type = fields.Many2one('loan.collateral.type', string='Collateral name', domain=[('parent_id', '!=', False)])
    parent_type = fields.Many2one('loan.collateral.type', string='Collateral type', domain=[('parent_id', '=', False)])
    location = fields.Char(string='Location')
    purpose = fields.Char(string='Purpose')#Зориулалт
    count = fields.Integer(string='Quantity')
    currency_id = fields.Many2one('res.currency', string='Currency')
    price = fields.Float(string='Valuation price', digits=(16, 2))
    max_price = fields.Float(string='Max valuation', digits=(16, 2))
    price_date = fields.Date(string='Date of valuation')
    percent = fields.Float(string='Percent', digits=(16, 2))
    company_id = fields.Many2one('res.company', string='Company', readonly=True,
                                 default=lambda self: self.env.user.company_id)
    coll_type = fields.Selection([('type1', 'type1'),  # Автомашин
                                  ('type2', 'type2'),  # Мөнгөн хадгаламж
                                  ('type3', 'Type3'),  # Арилжааны үнэт цаас
                                  ('type4', 'Type4'),  # Орон сууц
                                  ('type5', 'Type5'),  # Үнэт эдлэл
                                  ('type6', 'Type6'),  # Бусад үл хөдлөх хөрөнгө
                                  ('type7', 'type7')],  # Бусад хөдлөх хөрөнгө
                                 string='Collateral Type', default='type1')

    # 1. Автомашин
    state_reg_number = fields.Float('State Registration number')  # Улсын бүртгэлийн дугаар
    owner = fields.Char('Owner')  # өмчлөгч
    vin_number = fields.Float('VIN number')  # Арлын дугаар
    state_number = fields.Float('State number')  # Улсын дугаар
    technic_model = fields.Char('Technic Model')  # Загвар
    mark = fields.Char('Mark')  # Марк
    technic_type = fields.Char('Technic Type')  # Ангилал
    capacity = fields.Float('Capacity')  # Хөдөлгүүрийн багтаамж
    date_of_manufacture = fields.Date('Date Of Manufacture')  # Үлдвэрлэсэн он
    date_of_import = fields.Date('Date Of Import')  # Монголд орж ирсэн
    color_id = fields.Many2one('collateral.color', string='Color')  # Өнгө
    is_driver_insurance = fields.Boolean('Is Driver Insurance', default=False)  # Жолоочийн хариуцлагын даатгалтай эсэх
    is_vehicle_insurance = fields.Boolean('Is Vehicle Insurance', default=False)  # Тээврийн хэрэгслийн даатгалтай эсэх
    col_picture = fields.Binary("Image", attachment=True)  # Зураг
    # 2.Мөнгөн хадгаламж
    account_owner = fields.Char('Account Owner')  # Данс эзэмшигч
    book_number = fields.Char('Book Number')  # Хадгаламжийн дэвтэрийн дугаар
    account_number = fields.Char('Account Number')  # Дансны дугаар
    start_date = fields.Date('Account Start Date')  # Нээсэн огноо
    end_date = fields.Date('Account End Date')  # Хаасан огноо
    account_residual = fields.Float('Account Residual')  # Дансандах хадгаламжийн үлдэгдэл
    account_term = fields.Char('Account Term')  # Хадгаламжийн төрөл
    bank_name = fields.Char('Bank Name')  # Банкны нэр
    interest = fields.Float('Interest')  # Хүү
    # 3.Арилжааны үнэт цаас
    trade_type = fields.Char('Trade Type')  # Үнэт цаасны төрөл
    trade_number = fields.Float('Trade Number')  # Дугаар
    owner_name = fields.Char('Owner Name')  # Эзэмшигчийн нэр
    trade_amount = fields.Float('Trade Amount')  # Үнэт цаасны үнэ
    amount = fields.Float('Amount')  # Худалдах үнэ
    trade_time = fields.Char('Trade Time')  # Хугацаа
    trade_partner = fields.Float('Trade Partner')  # Үнэт цаас гаргагч
    # 4.Орон сууц
    apartment_type = fields.Selection([('self_apartment', 'Self Apartment'),  # Хувийн сууц
                                       ('apartment', 'Apartment'),  # Орон сууц
                                       ('land', 'Land'),  # Газар
                                       ('other', 'Other'),  # Бусад
                                       ('garage', 'Garage'),  # Граж
                                       ('office', 'Office')], default='apartment',
                                      track_visibility='onchange')  # Үйлчилгээний оффис
    operation_year = fields.Date('Operation Year')  # Ашиглалтанд орсон он
    registered_company = fields.Char('Registered Company')  # Бүртгэсэн байгууллагын нэр
    registered_date = fields.Date('Registered Date')  # Бүртгэсэн огноо
    is_insurance = fields.Boolean('Is Insurance')  # Даатгалтай эсэх
    size = fields.Float('Size')  # Хэмжээ
    address = fields.Char('Address')  # Хаяг
    m_rating = fields.Float('1 m Rating')  # 1м.кв үнэлгээ
    total_rating = fields.Float('Total Amount')  # Нийт үнэлгээ
    owner_changed_type = fields.Float('Owner Changed Type')  # Эзэмшигч солигдсон төрөл
    # 5. Үнэт эдлэл
    jewelry_type = fields.Char('Jewelry Type')  # Төрөл
    style = fields.Char('Jewelry Style')  # Загвар
    purpose = fields.Char('Purpose')  # Зориулалт
    brand_name = fields.Char('Brand Name')  # Брендийн нэр
    weight = fields.Float('Weight')  # Жин
    is_trustee = fields.Boolean('Is Trustee', default=False)
    snd = fields.Char('SND')  # Дахин давтагдашгүй дугаар
    trustee_id = fields.Many2one('res.partner', string='Trustee')
    trustee_start_date = fields.Date('Start Date')
    trustee_end_date = fields.Date('End Date')
    is_registered_agencies = fields.Boolean('Is Registered')#ЭХЭУБГазарт бүртгүүлсэн эсэх
    location = fields.Char('Location of Collateral', size=250)#ЭХЭУБГазарт бүртгүүлсэн эсэх

class LoanCollateralType(models.Model):
    _name = "loan.collateral.type"
    _description = "Collateral type"

    name = fields.Char(string='Name', size=30, required=True)
    code = fields.Char(string='Code', size=30, required=True)
    parent_id = fields.Many2one('loan.collateral.type', string='Parent', domain=[('parent_id', '=', False)])
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id,
                                 required=True)
    remarks = fields.Text(string='Description')
    land_mode = fields.Selection([('1', 'Own'),  # Өмчлөх
                                  ('2', 'Ownership'),  # Эзэмших
                                  ('3', 'Use')], default='1', track_visibility='onchange') #Ашиглах
    is_asset = fields.Boolean('Is Asset')


class CollateralColor(models.Model):
    _name = "collateral.color"
    _description = "Collateral color"

    name = fields.Char('Color')
