# -*- coding: utf-8 -*-
from odoo import models, fields


class LoanAccountStatement(models.Model):
    _name = "loan.account.statement"
    _description = 'Loan Account Statement'
    _order = 'changed_date asc'

    contract_id = fields.Many2one('loan.order', 'Loan contract')
    changed_date = fields.Date('Date')
    name = fields.Char('Transaction remark', size=128)
    amount = fields.Float('Disbursement', digits=(16, 2))
    payment = fields.Float('Repayment', digits=(16, 2))
    debit_xx = fields.Float('Debit', digits=(16, 2))
    credit_xx = fields.Float('Credit', digits=(16, 2))
    interest = fields.Float('Interest Charge', digits=(16, 2))
    loss = fields.Float('SurCharge', digits=(16, 2))
    risk = fields.Float('Risk amount', digits=(16, 2))
    company_id = fields.Many2one('res.company', string='Company', readonly=True,
        default=lambda self: self.env.user.company_id, required=True)
    history_id = fields.Many2one('loan.history','History')

class LoanHistory(models.Model):
    _name = "loan.history"
    _description = "Loan history"

    contract_id = fields.Many2one('loan.order', 'Contract number')
    name = fields.Char('Description', size=128, required=True)
    changed_date = fields.Date('Date')
    company_id = fields.Many2one('res.company', string='Company', readonly=True,
        default=lambda self: self.env.user.company_id, required=True)
    lines = fields.One2many('loan.account.statement', 'history_id', 'Lines')
