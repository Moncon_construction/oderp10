# -*- coding: utf-8 -*-
from odoo import models, fields


class NegativeInfo(models.Model):
    _name = "negative.info"
    _description = "Negative Information"

    contract_id = fields.Many2one('loan.order', 'Contract number')
    explanation = fields.Char('Remarks on conflicts', size=256, required=True)
    police_date = fields.Date('Date of claim')
    attorney_date = fields.Date('Date of submitted to procuratorate')
    judge_date = fields.Date('Date of submitted to judge office')
    decided_date = fields.Date('Date of decision')
    total_invoice = fields.Float('Claimed amount', digits=(16, 1))
    currency_id = fields.Many2one('res.currency', 'Currency', required=True)
    payment = fields.Float('Assigned repayment amount', digits=(16, 1))
    state = fields.Boolean('Fulfillment')
    other = fields.Text('Other')
    company_id = fields.Many2one('res.company', string='Company', readonly=True,
        default=lambda self: self.env.user.company_id, required=True)
