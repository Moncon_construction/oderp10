# -*- coding: utf-8 -*-
from calendar import monthrange, calendar
from datetime import datetime, date
from dateutil.relativedelta import relativedelta
from odoo import models, fields, api, _
from odoo.exceptions import UserError


class LoanOrder(models.Model):
    _name = 'loan.order'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    @api.depends('loan_committee_ids')
    @api.multi
    def _count_all(self):
        approve_percent = cancelled_percent = approved = cancelled = 0.0
        committees_len = len(self.loan_committee_ids)
        for users in self.loan_committee_ids:
            if users.yes == True:
                approved += 1
            if users.no == True:
                cancelled += 1
        if committees_len != 0:
            approve_percent = approved * 100 / committees_len
            cancelled_percent = cancelled * 100 / committees_len
        self.approve_percent = approve_percent
        self.cancelled_percent = cancelled_percent

    @api.multi
    @api.depends('loan_committee_ids')
    def _compute_is_checked(self):
        for order in self:
            yes_employees = self.env['loan.committee.users'].search(
                [('user_id', '=', self._uid), ('contract_id', '=', order.id), ('yes', '=', True)]).ids
            no_employees = self.env['loan.committee.users'].search(
                [('user_id', '=', self._uid), ('contract_id', '=', order.id), ('no', '=', True)]).ids
            if yes_employees:
                order.yes = True
            if no_employees:
                order.no = True

    def default_manager(self):
        loan_state_id = self.env['loan.category.change.type'].search([('code', '=', 'normal_loan')], limit=1)
        if loan_state_id:
            return loan_state_id
        return False

    name = fields.Char('Contract number', size=64, required=True, copy=False, default='/')
    product_id = fields.Many2one('product.product.loan', 'Loan product', required=True)
    loan_product_type = fields.Selection(related='product_id.loan_product_type')
    period_type = fields.Selection(related='product_id.period_type')
    min_period = fields.Integer(related='product_id.min_period')
    max_period = fields.Integer(related='product_id.max_period')
    min_amount = fields.Integer(related='product_id.min_amount')
    max_amount = fields.Integer(related='product_id.max_amount')
    min_interest = fields.Float(related='product_id.min_interest')
    max_interest = fields.Float(related='product_id.max_interest')
    min_insterest_by_year = fields.Float(related='product_id.min_insterest_by_year')
    max_insterest_by_year = fields.Float(related='product_id.max_insterest_by_year')
    min_interest_commission_percent = fields.Float(related='product_id.min_interest_commission_percent')
    max_interest_commission_percent = fields.Float(related='product_id.max_interest_commission_percent')
    currency_id = fields.Many2one(related='product_id.currency_id',
                                  default=lambda self: self.env.user.company_id.currency_id)

    period = fields.Integer('Period /Month/', required=True)
    number_of_loans = fields.Integer('Number of loans')
    total_percent = fields.Float('Percent on loan amount')
    installment_date = fields.Date('Installment Date')
    close_date = fields.Date('Expire Date', compute='compute_loan_balance')
    payment_day = fields.Integer('Date of montly repayment')
    has_monthly_discount = fields.Boolean('Has monthly discount', default=False)
    has_multiple_payments_in_month = fields.Boolean('Has multiple payments in month', default=False)
    fixed_payment = fields.Float('Fixed repayment amount')
    loan_balance = fields.Float('Loan balance', digits=(16, 2), size=20, compute='compute_loan_balance')
    total_paid_balance = fields.Float('Total paid balance', digits=(16, 2), size=20, compute='compute_loan_balance')
    total_paid_interest = fields.Float('Total paid interest', digits=(16, 2), size=20, compute='compute_loan_balance')
    total_paid_amount = fields.Float('Total paid amount', digits=(16, 2), size=20, compute='compute_loan_balance')
    amount = fields.Float('Loan amount', digits=(16, 2), size=20, required=True)
    interest_by_year = fields.Float('Loan interest /year/', digits=(16, 2), required=True)
    interest_by_month = fields.Float('Loan interest /month/', digits=(16, 6), required=True)
    interest_commission_percent = fields.Float('Loan commission rate', digits=(12, 2))
    bond_purchased_quantity = fields.Float('Bond Purchased Quantity', digits=(12, 2))
    loan_fee_amount = fields.Float('Loan fee amount', digits=(16, 2), compute='compute_loan_fee')
    unit_price_of_bills = fields.Float('Unit price of bills', digits=(16, 2))
    decision = fields.Text('Decision', size=128)
    late_day = fields.Integer('Beyond days')
    loan_state = fields.Many2one('loan.category.change.type', 'Loan state', required=True, default=default_manager)
    loan_type = fields.Selection([
        ('1', 'Loan'),
        ('2', 'Warranty'),
        ('3', 'Letter of credit'),
        ('4', 'Rent'),
        ('5', 'Financial obligations'),
        ('6', 'Receipt'),
        ('7', 'Line of credit'),
        ('8', 'Line of credit Securities'),
    ], string='Type of Loan', default='1')
    issued_economist_id = fields.Many2one('hr.employee', 'Initially confirmed by')
    current_economist_id = fields.Many2one('hr.employee', 'Current economist')
    loan_purpose_id = fields.Many2one('loan.purpose', 'Purpose', required=True)
    is_custom_period = fields.Boolean('Custom period')
    is_custom_amount = fields.Boolean('Custom amount')
    is_custom_interest = fields.Boolean('Custom interest')
    is_interest = fields.Boolean('Repayment in 1st month?')
    collateral_ids = fields.One2many('loan.collateral', 'contract_id', string='Collaterals',
                                     states={'confirmed': [('readonly', True)],
                                             'payment': [('readonly', True)],
                                             'approved': [('readonly', True)],
                                             'done': [('readonly', True)],
                                             'paid': [('readonly', True)]})
    negative_info_ids = fields.One2many('negative.info', 'contract_id', 'Negative information')
    loan_history_ids = fields.One2many('loan.account.statement', 'contract_id', 'Loan history', readonly=True)
    schedules_ids = fields.One2many('loan.schedule', 'contract_id', 'Graphics')
    schedule_type = fields.Selection([('type1', 'Fixed repayment PMT'),
                                      ('type3', 'Fixed loan repayment'),
                                      ('type5', 'Loan reapyment on last')], 'Graphic type')
    date_closed = fields.Date('closed Date')
    date_order = fields.Datetime('Order Date', default=datetime.now())
    date_approve = fields.Datetime('Date Approved Amount')
    partner_id = fields.Many2one('res.partner', string="Debtor", required=True, domain=[('is_debtor', '=', True)])
    sponsor_id = fields.Many2one('res.partner', string='Sponsor debtor')
    partner_address = fields.Char('Address', size=256)
    state = fields.Selection([
        ('draft', 'Request for Quotation'),
        ('research', 'Research'),
        ('confirmed', 'Waiting Approval'),
        ('approved', 'Approved'),
        ('payment', 'Wait Payment'),
        ('paid', 'Paid'),
        ('done', 'Closed'),
        ('done_order', 'Done'),
        ('cancel', 'Cancelled')
    ], 'States', default='draft')
    validator = fields.Many2one('res.users', 'ValiDated by', readonly=True)
    notes = fields.Text('Other')
    company_id = fields.Many2one('res.company', string='Company', readonly=True,
                                 default=lambda self: self.env.user.company_id, required=True)
    loan_committee_ids = fields.One2many('loan.committee.users', 'contract_id', 'Loan committee')
    approve_percent = fields.Float(string='Approve percent', compute='_count_all', digits=(16, 2))
    cancelled_percent = fields.Float(string='Cancelled percent', compute='_count_all', digits=(16, 2))
    yes = fields.Boolean('Yes', compute='_compute_is_checked')
    no = fields.Boolean('No', compute='_compute_is_checked')
    # Зээлийн олголт хийгдсэн касс харилцахын мөр
    loan_lending_statement_line_id = fields.Many2one('account.bank.statement.line', 'Loan lending statement line')
    # Тухайн зээлтэй холбоотой журналын бичилтүүдийг харуулна
    account_move_count = fields.Integer(compute='_compute_loan_account_move', string='Receptions', default=0)
    account_move_ids = fields.Many2many('account.move', compute='_compute_loan_account_move',
                                        string='Productions',
                                        copy=False)
    # Тухайн зээлтэй холбоотой касс, харилцахын мөрүүдтйш харуулна
    account_bank_statement_line_count = fields.Integer(compute='_compute_loan_account_bnk_stmnt_line',
                                                       string='Receptions', default=0)
    account_bank_statement_line_ids = fields.Many2many('account.bank.statement.line',
                                                       compute='_compute_loan_account_bnk_stmnt_line',
                                                       string='Productions',
                                                       copy=False)
    payment_type_of_amount_for_schedule = fields.Selection([
        ('1', 'By year'),
        ('2', 'For half a year'),
        ('3', 'By season'),
        ('4', 'By month'),
        ('5', 'Proportional'),
        ('6', 'Not equal'),
    ], string='Payment Type of Amount for Loan Schedule')
    payment_type_of_interest_for_schedule = fields.Selection([
        ('1', 'By year'),
        ('2', 'For half a year'),
        ('3', 'By season'),
        ('4', 'By month'),
        ('5', 'Proportional'),
        ('6', 'Not equal'),
    ], string='Payment Type of Interest For Loan Schedule')
    extend_counter = fields.Integer('Extend of Loan Counter')

    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Contract number must be unique!'),
    ]

    # Зээлийн шимтгэлийн дүн тооцоолох
    def compute_loan_fee(self):
        for obj in self:
            if obj.interest_commission_percent:
                obj.loan_fee_amount = obj.amount / 100 * obj.interest_commission_percent
            else:
                obj.loan_fee_amount = 0

    # Зээлийн гэрээн дээр дуусах огноо, үлдэгдэл, нийт төлсөн дүнгүүд тооцоолох функц
    def compute_loan_balance(self):
        for obj in self:
            last_schedule_id = self.env['loan.schedule'].search(
                [('contract_id', '=', obj.id)], order="payment_date desc",
                limit=1)
            if last_schedule_id:
                obj.close_date = last_schedule_id.payment_date

            last_balance_schedule_id = self.env['loan.schedule'].search(
                [('contract_id', '=', obj.id), ('is_paid', '=', 'un_paid')], order="payment_date asc",
                limit=1)
            if last_balance_schedule_id:
                obj.loan_balance = last_balance_schedule_id.balance

            paid_schedule_ids = obj.schedules_ids.filtered(lambda schedule: schedule.is_paid != 'un_paid')
            if paid_schedule_ids:
                total_paid_balance = sum(paid_schedule_ids.mapped('loan_payment'))
                total_paid_interest = sum(paid_schedule_ids.mapped('interest_payment'))
                total_paid_amount = total_paid_interest + total_paid_balance
                obj.total_paid_balance = total_paid_balance
                obj.total_paid_interest = total_paid_interest
                obj.total_paid_amount = total_paid_amount

    # Smart button дээр журналын бичилтын хэд байгаа тоог харуулна
    def _compute_loan_account_move(self):
        for obj in self:
            account_move_obj = self.env['account.move']
            account_move_ids = account_move_obj.search([('loan_id', '=', obj.id)])
            if account_move_ids:
                obj.account_move_ids = account_move_ids

            obj.account_move_count = len(obj.account_move_ids)

    # Smart button дарахад ЖБ-рүү үсрэнэ
    @api.multi
    def view_loan_account_moves(self):
        for obj in self:
            action = self.env.ref('account.action_move_line_form')
            result = action.read()[0]

            # override the context to get rid of the default filtering on account move
            result.pop('id', None)
            result['context'] = {}
            account_move_ids = sum([obj.account_move_ids.ids for obj in self], [])
            if account_move_ids:
                result['domain'] = "[('id','in',[" + ','.join(map(str, account_move_ids)) + "])]"
            return result

    # Smart button дээр касс, харилцахын мөр хэд байгаа тоог харуулна
    def _compute_loan_account_bnk_stmnt_line(self):
        for obj in self:
            account_bank_statement_line_obj = self.env['account.bank.statement.line']
            account_bank_statement_line_ids = account_bank_statement_line_obj.search([('loan_id', '=', obj.id)])
            if account_bank_statement_line_ids:
                obj.account_bank_statement_line_ids = account_bank_statement_line_ids

            obj.account_bank_statement_line_count = len(obj.account_bank_statement_line_ids)

    # Smart button дарахад касс харилцахын мөрлүү үсрэнэ
    @api.multi
    def view_loan_bank_statement_lines(self):
        for obj in self:
            action = self.env.ref('account.action_bank_statement_line')
            result = action.read()[0]

            # override the context to get rid of the default filtering on account move
            result.pop('id', None)
            result['context'] = {}
            account_bank_statement_line_ids = sum([obj.account_bank_statement_line_ids.ids for obj in self], [])
            if account_bank_statement_line_ids:
                result['domain'] = "[('id','in',[" + ','.join(map(str, account_bank_statement_line_ids)) + "])]"
            return result

    @api.onchange('schedule_type', 'interest_by_month', 'amount', 'period')
    def onchange_schedule_type(self):
        val = {}
        if self.schedule_type == 'type1':
            if self.interest_by_month <= 0:
                raise UserError(_('Insert interest!'))
            if self.amount <= 0:
                raise UserError(_('Insert amount!'))
            if self.period <= 0:
                raise UserError(_('Insert period!'))

    @api.onchange('interest_by_year')
    def onchange_method_years(self):
        if self.interest_by_year >= 0:
            self.interest_by_month = float(self.interest_by_year) / 12

    @api.onchange('interest_by_month')
    def onchange_method_months(self):
        if self.interest_by_month >= 0:
            self.interest_by_year = float(self.interest_by_month) * 12

    @api.multi
    @api.onchange('partner_id')
    def onchange_partner_id(self):
        partner = self.env['res.partner'].search([('id', '=', self.partner_id.id)])
        for cat in partner.category_id:
            cat.name
        fre = self.env['loan.order'].search([('state', '!=', 'draft'), ('partner_id', '=', self.partner_id.id)])
        fre = len(fre) + 1
        address = unicode((partner.country_id and partner.country_id.name) or '') + ', ' + unicode(
            (partner.state_id and partner.state_id.name) or '') + ', ' + unicode(partner.city or '') + ', ' + unicode(
            partner.street2 or '')
        return {'value': {'partner_address': address, 'number_of_loans': fre}}

    @api.multi
    @api.onchange('sponsor_id')
    def onchange_sponsor_id(self):
        val = {}
        war = {}
        if self.partner_id:
            if self.partner_id == self.sponsor_id:
                war['message'] = 'Not yourself!'
                war['title'] = 'Warning!'
                val['sponsor_id'] = False
        return {'value': val, 'warning': war}

    @api.multi
    def calc_total_percent(self):
        read_obj = self
        colls = read_obj.collateral_ids
        if colls:
            amount = read_obj.amount
            total_p = 0.0
            for col in colls:
                price = col.price
                per = price * 100 / amount
                self.env['loan.collateral'].search([('id', '=', col.id)]).write({'percent': per})
                total_p += per
            self.search([('id', '=', read_obj['id'])]).write({'total_percent': total_p})
        return True

    @api.onchange('issued_economist_id')
    def onchange_eco(self):
        val = {}
        val['current_economist_id'] = self.issued_economist_id
        return {'value': val}

    @api.onchange('period', 'is_custom_period')
    def setTimeDiff(self):
        war = {}
        val = {}
        if self.is_custom_period is False:
            lpro = self.env['product.product.loan'].browse(self.product_id.id)
            tmin = lpro.min_period
            tmax = lpro.max_period
            if tmin > self.period or self.period > tmax:
                val['period'] = 0
                war['title'] = 'Invalid period\n'
                war['message'] = 'Value must be from %d to %d ' % (tmin, tmax)
                return {'value': val, 'warning': war}
        return {}

    @api.onchange('amount', 'is_custom_amount')
    def setTotalDiff(self):
        war = {}
        val = {}
        if not self.is_custom_amount:
            lpro = self.env['product.product.loan'].search([('id', '=', self.product_id.id)])
            tmin = lpro.min_amount
            tmax = lpro.max_amount
            if tmin > self.amount or self.amount > tmax:
                val['amount'] = 0
                war['title'] = 'Invalid amount\n'
                war['message'] = 'Value must be from %d to %d ' % (tmin, tmax)
                return {'value': val, 'warning': war}
        return {}

    @api.onchange('payment_day')
    def checkPaymentDay(self):
        war = {}
        val = {}
        if self.payment_day > 31:
            val['payment_day'] = 25
            war['title'] = 'Invalid payment day\n'
            war['message'] = 'Value must be from 1 to 30 '
            return {'value': val, 'warning': war}
        return {}

    @api.onchange('interest_by_month', 'is_custom_interest')
    def setRateDiff(self):
        war = {}
        val = {}

        if self.is_custom_interest is False:
            lpro = self.env['product.product.loan'].browse(self.product_id.id)
            tmin = lpro.min_interest
            tmax = lpro.max_interest
            if tmin > self.interest_by_month or self.interest_by_month > tmax:
                val['interest'] = 0
                war['title'] = 'Invalid interest\n'
                war['message'] = 'Value must be from %d to %d ' % (tmin, tmax)
                return {'value': val, 'warning': war}
        return {}

    @api.onchange('period', 'is_custom_period')
    def checkTime(self):
        if self.period or self.is_custom_period:
            war = {}
            val = {}
            if self.product_id != 0:
                if self.is_custom_period == False:
                    pro = self.env['product.product.loan'].browse(self.product_id.id)
                    tmin = pro.min_period
                    tmax = pro.max_period
                    if tmin <= self.period and self.period <= tmax:
                        val['period'] = self.period
                    else:
                        val['period'] = 0
                        war['title'] = 'Invalid period'
                        war['message'] = 'Value must be from %d to %d ' % (tmin, tmax)
                    return {'value': val, 'warning': war}
                else:
                    val['period'] = self.period
                    return {'value': val}
            else:
                val['period'] = 0
                war['title'] = 'Choose loan product!'
                war['message'] = 'Choose loan product!'
                return {'value': val, 'warning': war}

    @api.multi
    def write(self, vals):
        return super(LoanOrder, self).write(vals)

    @api.onchange('amount', 'product_id', 'is_custom_amount')
    def onchange_checkTotal(self):
        war = {}
        val = {}
        if self.product_id:
            if self.is_custom_amount is False:
                pro = self.env['product.product.loan'].browse(self.product_id.id)
                tmin = pro.min_amount
                tmax = pro.max_amount
                if tmin <= self.amount and self.amount <= tmax:
                    self.amount = self.amount

    @api.onchange('interest_by_month', 'is_custom_interest')
    def checkRate(self):
        war = {}
        val = {}
        if self.product_id != 0:
            if self.is_custom_interest is False:
                pro = self.env['product.product.loan'].browse(self.product_id.id)
                rmin = pro.min_interest
                rmax = pro.max_interest
                if rmin <= self.interest_by_month and self.interest_by_month <= rmax:
                    val['interest_by_month'] = self.interest_by_month
                else:
                    val['interest_by_month'] = 0
                    war['title'] = 'Invalid interest!'
                    war['message'] = 'Value must be from %d to %d ' % (rmin, rmax)
                return {'value': val, 'warning': war}
            else:
                val['interest_by_month'] = self.interest_by_month
                return {'value': val}
        else:
            val['interest_by_month'] = 0
            war['title'] = 'Choose loan product!'
            war['message'] = 'Choose loan product!'
            return {'value': val, 'warning': war}

    def _get_month_days(self, year, month):
        return calendar.monthrange(year, month)[1]

    def _calc_interest(self, begin, end, interest_by_month, period, ptype):
        xvv = 0.0
        return xvv

    @api.onchange('has_monthly_discount')
    def onchange_has_discount(self):
        for obj in self:
            if obj.has_monthly_discount:
                obj.has_multiple_payments_in_month = False

    @api.onchange('has_multiple_payments_in_month')
    def onchange_has_multi(self):
        for obj in self:
            if obj.has_multiple_payments_in_month:
                obj.has_monthly_discount = False

    # Зээлийн гэрээний эргэн төлөлтийн хуваарьт бутархайн зөрүү гарвал 
    # үндсэн зээлийн илүү/дутуу бутархайг зээлийн хүү дээр нэмж/хасна
    @api.multi
    def check_total_loan_amount(self):
        for obj in self:
            if obj.schedules_ids:
                total = sum(obj.schedules_ids.mapped('loan_payment'))
                last_balance_schedule_id = self.env['loan.schedule'].search(
                    [('contract_id', '=', obj.id)], order="payment_date desc",
                    limit=1)
                if total > obj.amount:
                    diff = abs(total - obj.amount)
                    last_balance_schedule_id.loan_payment = last_balance_schedule_id.loan_payment - diff
                    last_balance_schedule_id.interest_payment = last_balance_schedule_id.interest_payment + diff
                elif total < obj.amount:
                    diff = abs(total - obj.amount)
                    last_balance_schedule_id.loan_payment = last_balance_schedule_id.loan_payment + diff
                    last_balance_schedule_id.interest_payment = last_balance_schedule_id.interest_payment - diff

    @api.multi
    def create_schedule_from_loan_order(self, prior_id, contract_id, loan_payment, interest_payment, balance,
                                        total_payment, payment_date,
                                        days):
        loan_schedule_obj = self.env['loan.schedule']
        loan_schedule_obj.create({
            'prior_id': prior_id,
            'contract_id': contract_id,
            'interest_payment': interest_payment,
            'loan_payment': loan_payment,
            'balance': balance,
            'total_payment': total_payment,
            'payment_date': payment_date,
            'days': days,
            'is_paid': 'un_paid',
        })

    # Тэнцүү төлөлтийн аргаар эргэн төлөлтийн хуваарь үүсгэх функц
    # гаднаас дуудаж ажиллуулахын тулд тусад нь функцээр тодорхойлов
    @api.multi
    def create_schedule_main_type(self, period=False):
        for obj in self:
            # Үндсэн зээлийн мэдээлэл
            principal = obj.amount
            # Зээлийн сар
            loan_term = int(obj.period)
            if period:
                loan_term = period
            # Зээлийн хүү жилээр
            interest_rate_year = obj.interest_by_year
            # Зээлийн хүү хувь-р
            interest_rate = interest_rate_year / 100
            interest_rate_month = interest_rate_year / 100
            # Зээл олгох огноо
            loan_start_date = obj.installment_date
            year, month, day = map(int, loan_start_date.split('-'))
            # FIX ME: Тухайн жилийн зээлийн эргэн төлөлтийн хүүг
            # өндөр жил эсэхээс хамаарч 366 хоногоор бодох эсэх
            # монголбанкны шийдвэр өөрчлөгдөх эсэхээс хамаарч ашиглах эсэх
            # year_day_range = get_year_day_range(year)
            # Монголбанкны томёогоор үргэлж 365 хоногоор бодох
            year_day_range = 365
            # Зээлийн эргэн төлөлт - Тэнцүү төлөлтийн аргаар сар бүрт тооцоолол
            # Эхний сарын огноо боловсруулалт
            year, month, day = map(int, loan_start_date.split('-'))
            start_date = datetime(year, month, day).strftime('%Y-%m-%d')
            day_of_month = monthrange(year, month)[1]
            # Монголбанкнаас гаргасан - Зээлийн итгэлцүүр тооцооллын томёо
            coefficient = 1 / (1 + day_of_month * interest_rate / year_day_range)
            i = 1
            # Нийт итгэлцүүрийн нийлбэр
            sum_coefficient = coefficient
            while i < loan_term:
                # Дараа сар
                date_after_month = date(year, month, day) + relativedelta(months=+1)
                date_after_month = date_after_month.strftime('%Y-%m-%d')
                year, month, day = map(int, date_after_month.split('-'))
                # Тухайн сарын нийт хоног
                day_of_month = monthrange(year, month)[1]
                # Тухайн сарын итгэлцүүр
                coefficient = coefficient / (1 + day_of_month * interest_rate / year_day_range)
                # Нийт итгэлцүүрийн нийлбэр
                sum_coefficient += coefficient
                i += 1
            sum_coefficient = sum_coefficient
            # Үндсэн зээлийн эхний сарын тооцооллууд
            year, month, day = map(int, loan_start_date.split('-'))
            start_date = datetime(year, month, day).strftime('%Y-%m-%d')
            day_of_month = monthrange(year, month)[1]
            # Сарын төлөлтийн нийт итгэлцүүрээр бодох
            monthly_payment = principal / sum_coefficient
            monthly_payment = monthly_payment
            # Тогмол төлбөрийн дүн талбарт утга оноох
            obj.fixed_payment = monthly_payment
            # Эхний сарын хүүгийн төлөлт тооцоолол
            payment_of_monthly_interest = principal * day_of_month * interest_rate_month / year_day_range
            payment_of_monthly_interest = payment_of_monthly_interest
            # Эхний сарын үндсэн зээлийн төлөлт тооцоолол
            payment_of_monthly_principal = monthly_payment - payment_of_monthly_interest
            payment_of_monthly_principal = payment_of_monthly_principal
            # Тухайн сар дахь хоногийн тоог гаргах
            day_of_month = monthrange(year, month)
            i = 1
            # Эхний сарын зээлийн хуваарь үүсгэх
            obj.create_schedule_from_loan_order(i, obj.id, payment_of_monthly_principal,
                                                payment_of_monthly_interest,
                                                principal, monthly_payment, start_date,
                                                day_of_month[1])
            principal = principal - payment_of_monthly_principal
            # Сард төлөх үндсэн зээлийн төлбөрийн нийт дүнгийн нийлбэр
            sum_payment_of_monthly_principal = payment_of_monthly_principal
            # Сард төлөх зээлийн хүүгийн нийт дүнгийн нийлбэр
            sum_payment_of_monthly_interest = payment_of_monthly_interest
            year, month, day = map(int, loan_start_date.split('-'))
            first_month_day = day
            while i < loan_term:
                # Дараа сар
                date_after_month = date(year, month, day) + relativedelta(months=+1)
                date_after_month = date_after_month.strftime('%Y-%m-%d')
                # Тухайн сарын жил, сар, өдөр авах
                year, month, day = map(int, date_after_month.split('-'))
                # Сарын нийт хоног
                day_of_month = monthrange(year, month)[1]
                # Тухайн сарын сүүлийн хоногт төлөлт үүсгэсэн бол дараагийн саруудын эцсийн өдрүүдийг авах
                if first_month_day > day:
                    date_after_month = date(year, month, day_of_month)
                    date_after_month = date_after_month.strftime('%Y-%m-%d')

                # Тухайн сард төлөх зээлийн хүү тооцоолол
                payment_of_monthly_interest = principal * (day_of_month * interest_rate_month / year_day_range)
                payment_of_monthly_interest = payment_of_monthly_interest
                # Тухайн сарл төлөх үндсэн зээлийн төлөлт тооцоолол
                payment_of_monthly_principal = monthly_payment - payment_of_monthly_interest
                payment_of_monthly_principal = payment_of_monthly_principal
                i += 1
                # Нийт төлөлтийн хуваарь үүссэн төлөлт, хүүгийн нийлбэр бодолт
                sum_payment_of_monthly_interest += payment_of_monthly_interest
                sum_payment_of_monthly_principal += payment_of_monthly_principal
                # Нийт төлсөн үндсэн зээлийн төлбөрийн нийлбэр үндсэн зээлээс их болсон үед
                # зөрүүг үндсэн төлөлтөөс хасч тухайн сарын хүүнд нэмж бодох
                # Тухайн сарын зээлийн хуваарь үүсгэх
                obj.create_schedule_from_loan_order(i, obj.id, payment_of_monthly_principal,
                                                    payment_of_monthly_interest, principal,
                                                    monthly_payment,
                                                    date_after_month,
                                                    day_of_month)
                # Дараа сарын нийт зээлийн дүн гаргах
                principal = principal - payment_of_monthly_principal

    """
    Эргэн төлөлтийн хуваарь үүсгэх функц Монголбанк-с гаргасан стандарт тооцоолол.
    """

    @api.multi
    def calculate_graphics(self):
        # FIX ME: Тухайн жилийн зээлийн эргэн төлөлтийн хүүг
        # өндөр жил эсэхээс хамаарч 366 хоногоор бодох эсэх
        # монголбанкны шийдвэр өөрчлөгдөх эсэхээс хамаарч ашиглах эсэх
        # def get_year_day_range(year):
        #     total_days = 0
        #     month = 1
        #     while month <= 12:
        #         total_days += monthrange(year, month)[1]
        #         month += 1
        #     return total_days
        for obj in self:
            # Үндсэн зээлийн мэдээлэл
            principal = obj.amount
            # Зээлийн сар
            loan_term = int(obj.period)
            # Зээлийн хүү жилээр
            interest_rate_year = obj.interest_by_year
            # Зээлийн хүү хувь-р
            interest_rate = interest_rate_year / 100
            interest_rate_month = interest_rate_year / 100
            # Зээл олгох огноо
            if not obj.installment_date:
                raise UserError(_('Loan start date is empty!'))
            loan_start_date = obj.installment_date
            year, month, day = map(int, loan_start_date.split('-'))
            # FIX ME: Тухайн жилийн зээлийн эргэн төлөлтийн хүүг
            # өндөр жил эсэхээс хамаарч 366 хоногоор бодох эсэх
            # монголбанкны шийдвэр өөрчлөгдөх эсэхээс хамаарч ашиглах эсэх
            # year_day_range = get_year_day_range(year)
            # Монголбанкны томёогоор үргэлж 365 хоногоор бодох
            year_day_range = 365
            # Зээлийн эргэн төлөлт - Тэнцүү төлөлтийн аргаар сар бүрт тооцоолол
            if obj.schedule_type == 'type1':
                # Хэрвээ тухайн сард олон төлөлттэй бол хоног сонгох визард нээгдэнэ
                if obj.has_multiple_payments_in_month:
                    return {
                        'name': _('Loan Schedule - Multiple Payment In Month'),
                        'res_model': 'loan.schedule.multiple.payment',
                        'view_mode': 'form',
                        'view_type': 'form',
                        'type': 'ir.actions.act_window',
                        'context': {'contract_id': self.id},
                        'target': 'new'
                    }
                # Хэрвээ сараар хөнгөлөх бол сар сонгох визард нээгдэнэ
                elif obj.has_monthly_discount:
                    return {
                        'name': _('Loan Schedule - Monthly Discount'),
                        'res_model': 'loan.schedule.discount',
                        'view_mode': 'form',
                        'view_type': 'form',
                        'type': 'ir.actions.act_window',
                        'context': {'contract_id': self.id},
                        'target': 'new'
                    }
                else:
                    obj.create_schedule_main_type()
            # Зээлийн эргэн төлөлт - Үндсэн зээлийн төлөлт тэнцүү зээлийн төлөлтийн арга
            elif obj.schedule_type == 'type3':
                # Эхний сарын огноо боловсруулалт
                year, month, day = map(int, loan_start_date.split('-'))
                start_date = datetime(year, month, day).strftime('%Y-%m-%d')
                day_of_month = monthrange(year, month)[1]
                # Зээлийн эргэн төлөлтийг тэнцүү төлөлтийн аргаар сар бүрт тооцоолол
                # Эхний сарын төлөлт тооцоолол
                obj.fixed_payment = obj.amount / loan_term
                payment_of_day_interest = principal * interest_rate_year / 100 / year_day_range * day_of_month
                day_of_month = monthrange(year, month)
                i = 1
                payment_of_monthly_principal = obj.amount / loan_term
                payment_of_monthly_total = payment_of_monthly_principal + payment_of_day_interest
                obj.create_schedule_from_loan_order(i, obj.id, payment_of_monthly_principal,
                                                    payment_of_day_interest,
                                                    principal, payment_of_monthly_total, start_date,
                                                    day_of_month[1])
                principal = principal - payment_of_monthly_principal
                year, month, day = map(int, loan_start_date.split('-'))
                while i < loan_term:
                    # Дараа сар
                    date_after_month = date(year, month, day) + relativedelta(months=+1)
                    date_after_month = date_after_month.strftime('%Y-%m-%d')
                    year, month, day = map(int, date_after_month.split('-'))
                    # Сарын нийт хоног
                    day_of_month = monthrange(year, month)[1]
                    # Төлөлт тооцоолол
                    payment_of_day_interest = principal * interest_rate_year / 100 / year_day_range * day_of_month
                    i += 1
                    payment_of_monthly_total = payment_of_monthly_principal + payment_of_day_interest
                    # Нийт төлөлтийн хуваарь үүссэн төлөлт, хүүгийн нийлбэр бодолт
                    obj.create_schedule_from_loan_order(i, obj.id, payment_of_monthly_principal,
                                                        payment_of_day_interest, principal,
                                                        payment_of_monthly_total,
                                                        date_after_month,
                                                        day_of_month)
                    principal = principal - payment_of_monthly_principal

            # Зээлийн эргэн төлөлт - Хугацааны эцэст үндсэн зээлийн төлөлтийн арга
            elif obj.schedule_type == 'type5':
                # Эхний сарын огноо боловсруулалт
                year, month, day = map(int, loan_start_date.split('-'))
                start_date = datetime(year, month, day).strftime('%Y-%m-%d')
                day_of_month = monthrange(year, month)[1]
                # Зээлийн эргэн төлөлтийг тэнцүү төлөлтийн аргаар сар бүрт тооцоолол
                # Эхний сарын төлөлт тооцоолол
                obj.fixed_payment = obj.amount
                payment_of_day_interest = principal * interest_rate_year / 100 / year_day_range * day_of_month
                day_of_month = monthrange(year, month)
                i = 1
                obj.create_schedule_from_loan_order(i, obj.id, 0,
                                                    round(payment_of_day_interest, 2),
                                                    round(principal, 2), round(payment_of_day_interest, 2), start_date,
                                                    day_of_month[1])
                year, month, day = map(int, loan_start_date.split('-'))
                while i < loan_term:
                    # Дараа сар
                    date_after_month = date(year, month, day) + relativedelta(months=+1)
                    date_after_month = date_after_month.strftime('%Y-%m-%d')
                    year, month, day = map(int, date_after_month.split('-'))
                    # Сарын нийт хоног
                    day_of_month = monthrange(year, month)[1]
                    # Төлөлт тооцоолол
                    payment_of_day_interest = principal * interest_rate_year / 100 / year_day_range * day_of_month
                    i += 1
                    # Нийт төлөлтийн хуваарь үүссэн төлөлт, хүүгийн нийлбэр бодолт
                    # Сүүлийн сарын төлөлт эсэх
                    if i == loan_term:
                        obj.create_schedule_from_loan_order(i, obj.id, round(principal, 2),
                                                            round(payment_of_day_interest, 2), 0,
                                                            round(payment_of_day_interest + principal, 2),
                                                            date_after_month,
                                                            day_of_month)
                    # Бусад сарын төлөлт
                    else:
                        obj.create_schedule_from_loan_order(i, obj.id, 0,
                                                            round(payment_of_day_interest, 2), round(principal, 2),
                                                            round(payment_of_day_interest, 2),
                                                            date_after_month,
                                                            day_of_month)
                    principal = principal - 0
            # Бутархайн зөрүү гарсан эсэхийг шалгаж тэнцүүлэх
            obj.check_total_loan_amount()

    @api.multi
    def _create_graph(self, cg_id, xvv, md, to_pa, balance, ma_lo, pa_da, pr_id):
        gid = self.env['loan.schedule'].create({
            'contract_id': cg_id,
            'interest_payment': xvv,
            'days': md,
            'total_payment': to_pa,
            'balance': balance,
            'loan_payment': ma_lo,
            'payment_date': pa_da,
            'prior_id': pr_id,
        })
        return gid

    @api.multi
    def reset_graphics(self):
        gids = self.schedules_ids
        for gid in gids:
            gid.unlink()
        return True

    @api.multi
    def print_graphics(self):
        return True

    @api.multi
    def _get_days(self, now, last, m):
        last_year = int(last[:4])
        last_mounth = int(last[5:7]) + m
        last_day = int(last[8:])

        if last_mounth > 12:
            last_mounth = 1
            last_year += 1

        now_year = int(now[:4])
        now_mounth = int(now[5:7])
        now_day = int(now[8:])

        d0 = date(last_year, last_mounth, last_day)
        d1 = date(now_year, now_mounth, now_day)
        delta = d1 - d0
        return delta.days

    @api.model
    def create(self, vals):
        if vals['amount'] <= 0:
            raise UserError(_('Insert loan amount!'))
        if vals['period'] <= 0:
            raise UserError(_('Insert loan period!'))
        if vals['interest_by_month'] < 0:
            raise UserError(_('Insert loan interest!'))
        return super(LoanOrder, self).create(vals)

    @api.multi
    def unlink(self):
        if ['draft', 'cancel'] not in self.read(['state']):
            raise UserError(_('In order to delete a loan order, it must be cancelled first!'))
        return super(LoanOrder, self).unlink()

    @api.multi
    def send_loan_email(self, state, new_state):
        # Төлөв солигдох үед тухайн баримтын дагагчид рүү мэйл явуулах функц
        for line in self:
            data = {
                'name': line.name or '',
                'loan_purpose_id': line.loan_purpose_id.name,
                'partner_name': line.partner_id.name,
                'amount': line.amount or 0,
                'new_state': new_state,
                'state': state,
                'base_url': self.env['ir.config_parameter'].get_param('web.base.url'),
                'action_id': self.env['ir.model.data'].get_object_reference('l10n_mn_loan_management',
                                                                            'loan_request_form_action')[1],
                'id': line.id,
                'db_name': self.env.cr.dbname,
                'sender': self.env.user.name,
            }
            template_id = \
                self.env['ir.model.data'].get_object_reference('l10n_mn_loan_management', 'loan_order_template')[1]
            template = self.env['mail.template'].browse(template_id)
            group_user_ids = []
            for partner in line.message_follower_ids:
                user = self.env['res.users'].search([('partner_id', '=', partner.partner_id.id)]).id
                if user and user not in group_user_ids:
                    group_user_ids.append(user)
            if group_user_ids:
                users = self.env['res.users'].browse(group_user_ids)
                user_emails = []
                if users:
                    for user in users:
                        user_emails.append(user.login)
                        template.with_context(data).send_mail(user.id, force_send=True)
                    email = u'Төлөв: → ' + new_state
                    self.message_post(
                        body=email + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ', '.join(user_emails))
        return True

    @api.multi
    def send_loan_committee_notificatoin_email(self, new_state):
        # Зээлийн хорооны гишүүд рүү мэйл явуулах функц
        model_obj = self.env['ir.model.data']
        for order in self:
            data = {
                'name': order.name or '',
                'partner_name': order.partner_id.name,
                'amount': order.amount or 0,
                'base_url': self.env['ir.config_parameter'].get_param('web.base.url'),
                'action_id': self.env['ir.model.data'].get_object_reference('l10n_mn_loan_management',
                                                                            'loan_request_form_action')[1],
                'id': order.id,
                'db_name': self.env.cr.dbname,
                'sender': self.env.user.name,
            }
            template_id = \
                self.env['ir.model.data'].get_object_reference('l10n_mn_loan_management',
                                                               'loan_committee_notificatoin_email_template')[1]
            template = self.env['mail.template'].browse(template_id)
            res_users = []
            active_user = self.env['res.users'].search([('id', '=', self._uid)])
            notif_groups = model_obj.get_object('l10n_mn_loan_management', 'group_loan_committee')
            res_users = self.env['res.users'].search([('groups_id', 'in', notif_groups.id)])
            if new_state == 'approved':
                state = _('Approved')
            if new_state == 'cancel':
                state = _('Cancelled')
            if res_users:
                user_emails = []
                for user in res_users:
                    user_emails.append(user.login)
                    template.with_context(data).send_mail(user.id, force_send=True)
                email = _('Member of the Credit Committee ') + active_user.name + _(
                    ' "Approval by the Credit Committee"-> ') + state + _('. Totally approved: ') + str(
                    self.approve_percent) + '% ' + _(' Totally cancelled: ') + str(self.cancelled_percent) + '% '
                self.message_post(body=email)
                if self.approve_percent >= 51:
                    self.message_post(
                        body=_('Feedback from approved users ') + str(self.approve_percent) + _(
                            '%reached and approved a loan order.An email was sent to the following users: ') + ', '.join(
                            user_emails))
                if self.cancelled_percent >= 51:
                    self.message_post(
                        body=_('Feedback from cancelled users ') + str(self.cancelled_percent) + _(
                            '%reached and cancelled a loan order.An email was sent to the following users: ') + ', '.join(
                            user_emails))
        return True

    @api.multi
    def action_order_cancel(self):
        self.send_loan_email(self.state, _('Draft'))
        self.write({'state': 'draft'})
        return True

    @api.multi
    def action_confirm_director(self):
        self.send_loan_email(_('Wait Payment'), _('Paid'))
        self.write({'state': 'paid'})
        return True

    @api.multi
    def action_confirm_order(self):
        if not self.loan_committee_ids:
            self.create_loan_committee()
        self.send_loan_email(_('Research'), _('Waiting Approval'))
        self.write({'state': 'confirmed'})
        return True

    @api.multi
    def action_confirm(self):
        for obj in self:
            obj.send_loan_email(_('Approved'), _('Wait Payment'))
            obj.write({'state': 'payment'})
            # Эргэн төлөлтийн хуваарь үүсгэж батлах үед дуусах огноо талбарт
            # эргэн төлөлтийн эцсийн огноогоор утга онооноо
            last_schedule_id = self.env['loan.schedule'].search([('id', 'in', obj.schedules_ids.ids)],
                                                                order="payment_date desc",
                                                                limit=1)
            if last_schedule_id:
                obj.close_date = last_schedule_id.payment_date
        return True

    @api.multi
    def action_research(self):
        if self.amount <= 0:
            raise UserError(_('Insert loan amount!'))
        self.send_loan_email(_('Request for Quotation'), _('Research'))
        self.write({'state': 'research', 'name': self.env['ir.sequence'].get('loan.order')})

    @api.multi
    def action_done(self):
        for obj in self:
            if obj.schedules_ids.filtered(lambda schedule: schedule.is_paid == 'un_paid'):
                raise UserError(_('This loan must be fully paid!'))
            obj.send_loan_email(_('Closed'), _('Done'))
            obj.write({'state': 'done_order'})
        return True

    @api.multi
    def action_approve_order(self):
        if not self.loan_committee_ids:
            self.create_loan_committee()
        self.change_line_state('approved')
        self.send_loan_committee_notificatoin_email('approved')
        if self.approve_percent >= 51:
            self.send_loan_email(_('Waiting Approval'), _('Approved'))
            self.write({'state': 'approved',
                        'validator': self._uid,
                        'date_approve': datetime.now(),
                        })
        return True

    @api.multi
    def action_cancel_order(self):
        if not self.loan_committee_ids:
            self.create_loan_committee()
        self.change_line_state('cancel')
        self.send_loan_committee_notificatoin_email('cancel')
        if self.cancelled_percent >= 51:
            self.send_loan_email(_('Waiting Approval'), _('Cancelled'))
            self.write({'state': 'draft'})
        return True

    @api.multi
    def create_loan_committee(self):
        # Зээлийн хорооны хэрэглэгчдээр хорооны хэрэглэгчийн мөрүүдийг үүсгэнэ
        model_obj = self.env['ir.model.data']
        notif_groups = model_obj.get_object('l10n_mn_loan_management', 'group_loan_committee')
        res_user = self.env['res.users'].search(
            [('groups_id', 'in', notif_groups.id)])
        if res_user:
            for user in res_user:
                loan_committee_users = self.env['loan.committee.users'].create({
                    'contract_id': self.id,
                    'user_id': user.id,
                })
        return True

    @api.multi
    def change_line_state(self, state):
        # Зээлийн хорооны ажилчдын саналыг оруулах функ
        for user in self.loan_committee_ids:
            if user.user_id.id == self._uid:
                if state == 'approved':
                    user.write({'yes': True})
                    if user.no == True:
                        user.write({'no': False})
                if state == 'cancel':
                    user.write({'no': True})
                    if user.yes == True:
                        user.write({'yes': False})
        return True

    @api.multi
    def action_do_cancel(self):
        return True

    @api.multi
    def print_all_statement(self):
        obj = self

    @api.multi
    def copy(self, default):
        if not default:
            default = {}
        default = {'state': 'draft',
                   'name': self.env['ir.sequence'].next_by_code('loan.order')
                   }
        return super(LoanOrder, self).copy(default)

    @api.multi
    def print_schedule(self):
        """ Зээлийн эргэн төлөлтийн хуваарь хэвлэх
        """
        return self.env['report'].get_action(self, 'l10n_mn_loan_management.loan_schedule_report')


class loanCommitteeUsers(models.Model):
    _name = 'loan.committee.users'

    contract_id = fields.Many2one('loan.order', 'Loan Order')
    user_id = fields.Many2one('res.users', 'Users')
    yes = fields.Boolean('Yes')
    no = fields.Boolean('No')
