# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError
from datetime import date, datetime, timedelta


class LoanPayment(models.Model):
    _name = "loan.payment"
    _description = "Loan payment"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _order = 'name desc'

    name = fields.Char('Loan payment number', size=64 , required=True, copy=False, default=lambda self:self.env['ir.sequence'].get('loan.payment'))
    cash_journal_id = fields.Many2one('account.journal', 'Cash journal')
    bank_journal_id = fields.Many2one('account.journal', 'Bank journal')
    payment_type = fields.Selection([
                    ('cash', 'Cash'),
                    ('bank', 'Bank')], 'Payment type')
    loan_order = fields.Many2one('loan.order', 'Loan contract number')
    product_id = fields.Many2one('product.product.loan', 'Loan product')
    re_payment = fields.Float('Diffrence repayment', digits=(16, 2))
    is_accumlate = fields.Boolean('Have a accumlate interest?', default=False)
    accumlated = fields.Float('Accumlated', digits=(16, 2), default=0)
    no_xt_accumlated = fields.Float('No xt accumlated', digits=(16, 2))
    xt_payment = fields.Float('Interest payment', digits=(16, 2))
    xt_payment_move = fields.Float('Interest payment', digits=(16, 2))
    xt_no_payment = fields.Float('Interest payment', digits=(16, 2))
    xxt_payment = fields.Float('Accumlated interest payment', digits=(16, 2))
    loss_payment = fields.Float('SurCharge payment', digits=(16, 2))
    vzt_payment = fields.Float('Loan repayment', digits=(16, 2))
    balance = fields.Float('Loan balance', digits=(16, 2))
    now_payment_amount = fields.Float('Currently repayment amount', digits=(16, 2))
    graphic = fields.Many2one('loan.schedule', 'Graphic')
    grap_pay = fields.Float('Repayment amount')
    is_undue_loss = fields.Boolean('Is surCharged')
    dont_pay_xt_xxt = fields.Boolean("Don't pay interest", default=False)
    dont_pay_interest = fields.Boolean("Don't pay interest", default=False)
    is_loan_balance = fields.Boolean("Is Loan Balance", default=False)
    is_compute = fields.Selection([
        ('incomplete', 'Incomplete'),
        ('additional', 'Additional'),
        ('normal', 'Normal')], 'Is compute', default='normal')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirmed', 'Confirmed'),
        ('waitpayment', 'Waiting payment'),
        ('paid', 'Paid'),
        ('cancel', 'Cancelled')
        ], string='State', default='draft', readonly=True, help="Тусламж")
    date_order = fields.Date('Date', required=True, default=datetime.now())
    create_date = fields.Datetime('Creation Date')
    date_confirm = fields.Date('Confirmed Date')
    test_date = fields.Date('Test s')
    user_id = fields.Many2one('res.users', 'User', default=lambda self: self._uid)
    partner_id = fields.Many2one('res.partner', 'Debtor', domain=[('is_debtor', '=', True)])
    partner_address = fields.Char('Address', size=256)
    company_id = fields.Many2one('res.company', string='Company', readonly=True,
        default=lambda self: self.env.user.company_id, required=True)
    note = fields.Text('Other')
    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Loan payment number must be unique!'),
    ]

    @api.multi
    def copy(self, default):
        if not default:
            default = {}
        default = {
            'state': 'draft',
            'date_confirm': False,
            'name': self.env['ir.sequence'].get('loan.payment'),
        }
        return super(LoanPayment, self).copy(default)

    @api.multi
    def _get_month_days(self, x):
        return {
            1:31, 2:28, 3:31, 4:30, 5:31, 6:30, 7:31, 8:31, 9:30, 10:31, 11:30, 12:31
        }.get(x)

    @api.multi
    def _get_days(self, now, last):
        last_year = int(last[:4])
        last_mounth = int(last[5:7])
        last_day = int(last[8:])
        now_year = int(now[:4])
        now_mounth = int(now[5:7])
        now_day = int(now[8:])

        d0 = date(last_year, last_mounth, last_day)
        d1 = date(now_year, now_mounth, now_day)
        delta = d1 - d0
        return delta.days

    @api.multi
    def _date_inc_dec(self, dddd, d):
        if dddd and d:
            day = int(dddd[8:])
            month = int(dddd[5:7])
            year = int(dddd[:4])
            dd = date(year, month, day) + timedelta(days=d)
            return dd

    @api.multi
    def _get_last_date(self, lid):
        loan = self.env['loan.order'].search([('id', '=', lid.id)])
        xtid = self.env['loan.payment'].search([('state', '=', 'paid'), ('loan_order', '=', lid.id)], order='id')
        xxtid = self.env['loan.xxt.payment'].search([('loan_order', '=', lid.id), ('state', '!=', 'draft')], order='id')
        last_date = ''
        if loan:
            last_date = ''
            if not xtid and not xxtid:
                last_date = loan.date_approve
                last_date = str(self._date_inc_dec(last_date, -1))
            elif xtid and not xxtid:
                xt = self.env['loan.payment'].search([('id', '=', xtid[len(xtid) - 1])])
                last_dates = xt.date_order
                last_date = str(self._date_inc_dec(last_dates, -1))
            elif not xtid and xxtid:
    #            xt = self.env['loan.payment']search([('','',)])(cr,uid,xxtid[-1])
    #            last_dates = xt.graphic.payment_date
    #            last_date = str(self._date_inc_dec(last_dates,-1))
                xxt = self.env['loan.xxt.payment'].search([('id', '=', xxtid[len(xxtid) - 1])])
                last_date = xxt.test_date
            elif xtid and xxtid:
                xt = self.env['loan.payment'].search([('id', '=', xtid[len(xtid) - 1])])
                xxt = self.env['loan.xxt.payment'].search([('id', '=', xxtid[len(xxtid) - 1])])
                if str(self._date_inc_dec(xt.date_order, -1)) <= xxt.test_date:
                    last_date = xxt.test_date
                else:
                    last_date = str(self._date_inc_dec(xt.date_order, -1))
        return last_date

    @api.onchange('loan_order', 'graphic')
    def onchange_loan_order(self):
        if self.loan_order:
            order_obj = self.env['loan.order'].search([('id', '=', self.loan_order.id)])
            self.partner_id = order_obj.partner_id
            self.product_id = order_obj.product_id
            self.balance = order_obj.loan_balance
        if self.graphic:
            self.vzt_payment = self.graphic.loan_payment
            self.xt_payment = self.graphic.interest_payment
            self.grap_pay = self.graphic.total_payment

    @api.multi
    def action_cancel(self):
        self.write({'state': 'cancel'})
        return True

# 
    @api.multi
    def action_to_confirmed(self):
        obj = self
        if obj.xt_payment <= 0 and obj.xxt_payment <= 0 and obj.vzt_payment <= 0:
            raise UserError(_('Please compute all payments'))
        if obj.dont_pay_xt_xxt:
            self.write({'state': 'confirmed',
                        'name': self.env['ir.sequence'].get('loan.payment'),
                        'date_confirm': datetime.now()})
        else:
            self.write({'state': 'confirmed', 'xt_no_payment': obj.xt_payment,
                        'name': self.env['ir.sequence'].get('loan.payment'),
                        'date_confirm': datetime.now()})
        return True

    @api.multi
    def unlink(self):
        loan_orders = self.read(['state'])
        for s in loan_orders:
            if s['state'] not in ['draft', 'cancel']:
                raise UserError(_('Invalid action !'), _('In order to delete a loan order, it must be cancelled first!'))
        return super(LoanPayment, self).unlink()
 
    @api.onchange('partner_id')
    def onchange_partner_id(self):
        partner = self.env['res.partner']
        partner = self.env['res.partner'].search([('id', '=', self.partner_id.id)])
        address = unicode((partner.country_id and partner.country_id.name) or '') + ', ' + unicode((partner.state_id and partner.state_id.name) or '') + ', ' + unicode(partner.city or '') + ', ' + unicode(partner.street2 or '')
        return {'value':{'partner_address': address}}

    """
    Төлөлт хүлээж буй төлөвт ажиллах функц
    Энэ функц зээлийн гэрээнд хамаарах хүчин зүйлсүүдэд үндэслэн төлөлт хийн
    түүний мэдээллээр журналын бичилт үүсгэнэ.
    """

    @api.onchange('now_payment_amount')
    def onchange_partner_id(self):
          if self.now_payment_amount < self.grap_pay:
            raise UserError(u'Таны оруулсан дүн төлөх дүнд хүрэхгүй байна!!!')

    @api.multi
    def action_to_payment(self):
        if self.now_payment_amount >= self.grap_pay:
            self.graphic.write({'is_paid': 'paid'})
            self.write({'state':'paid'})
