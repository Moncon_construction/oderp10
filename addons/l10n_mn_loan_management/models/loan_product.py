# -*- encoding: utf-8 -*-
from odoo import fields, models, api


class ProductProductLoan(models.Model):
    _name = "product.product.loan"
    _description = "Loan product"

    name = fields.Char('name', size=128, required=True)
    supplier_taxes_ids = fields.Many2many('account.tax')
    accrued_interest_journal_id = fields.Many2one('account.journal', string="Accrued interest journal", required=True)
    accrued_loan_interest_receivables_account_id = fields.Many2one('account.account',
                                                                   string="Accrued loan interest receivables account",
                                                                   required=True)
    loan_fee_account_id = fields.Many2one('account.account', string="Revenue from service fees account", required=True)
    p_type = fields.Selection([('view', 'View'), ('normal', 'Normal')], 'Category Type',
                              help="A category of the view type is a virtual category that can be used as the parent of another category to create a hierarchical structure.")
    categ_id = fields.Many2one('product.category', 'Category', required=True)
    sale_ok = fields.Boolean('Can be Sold', help="Specify if the product can be selected in a sales order line.")
    loan_product_type = fields.Selection([
        ('loan_product', 'Loan product'),
        ('interest_payment', 'Interest payment'),
        ('accumulative_interest_payment', 'Accumlated interest payment'),
        ('undue_loss', 'Surcharge payment'),
        ('xxz_product', 'Past due loans'),
        ('xb_product', 'Abnormal loans'),
        ('ez_product', 'Doubted loans'),
        ('mz_product', 'Bad loans'), ], 'Loan product type', required=True)
    period_type = fields.Selection([
        ('mounth', 'Month'),
        ('day', 'Day'), ], 'Period type', required=True)
    min_period = fields.Integer('Min period', required=True)
    max_period = fields.Integer('Max period', required=True)
    min_amount = fields.Integer('Min amount', required=True)
    max_amount = fields.Integer('Max amount', required=True)
    min_interest = fields.Float('Min interest')
    max_interest = fields.Float('Max interest')
    min_insterest_by_year = fields.Float('Min interest of Year', required=True)
    max_insterest_by_year = fields.Float('Max interest of Year', required=True)
    min_interest_commission_percent = fields.Float('Min Loan commission rate', digits=(12, 2), required=True)
    max_interest_commission_percent = fields.Float('Max Loan commission rate', digits=(12, 2), required=True)

    currency_id = fields.Many2one('res.currency', 'Currency')
    company_id = fields.Many2one('res.company', string='Company', readonly=True,
                                 default=lambda self: self.env.user.company_id, required=True)

    origin_of_loan = fields.Selection([
        ('1', 'I applied for a loan'),
        ('2', 'Issued under the credit line'),
        ('3', 'Moved from warranty'),
        ('4', 'Transferred from a letter of credit'),
        ('5', 'Moved from other responsibilities'),
        ('6', 'Other'),
    ], string='Origin Of Loan', required=True)

    @api.onchange('min_insterest_by_year')
    def onchange_min_insterest_by_year(self):
        for obj in self:
            if obj.min_insterest_by_year:
                obj.min_interest = obj.min_insterest_by_year / 12

    @api.onchange('max_insterest_by_year')
    def onchange_max_insterest_by_year(self):
        for obj in self:
            if obj.max_insterest_by_year:
                obj.max_interest = obj.max_insterest_by_year / 12
