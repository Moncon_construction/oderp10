# -*- encoding: utf-8 -*-
from odoo import models, fields


class LoanPurpose(models.Model):
	_name = "loan.purpose"
	_description = "Loan purpose"

	code = fields.Char('Code', size=128, required=True)
	name = fields.Char('Purpose name', size=128, required=True)
	parent_id = fields.Many2one('loan.purpose', 'Parent', domain=[('parent_id', '=', False)])
