# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
from odoo.exceptions import UserError
from datetime import datetime


class LoanRiskFund(models.Model):
    _name = "loan.risk.fund"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = "Loan Risk Fund"
    _order = 'name desc'

    name = fields.Char('Name', size=64, default='/')
    loan_order_ids = fields.Many2many('loan.order', string='Loan Orders', domain=[('state', '=', 'done')])
    date_order = fields.Date('Date', default=datetime.now())
    date_confirm = fields.Date('Date confirm', readonly=True)
    user_id = fields.Many2one('res.users', 'User', default=lambda self: self._uid)
    company_id = fields.Many2one('res.company', string='Company', store=True, readonly=True,
                                 default=lambda self: self.env.user.company_id)
    journal_id = fields.Many2one('account.journal', 'Risk Fund Journal')
    account_risk_expense_id = fields.Many2one('account.account', 'Account Risk Expense')
    # Тухайн эрсдлийн сантай холбоотой журналын бичилтүүдийг харуулна
    account_move_count = fields.Integer(compute='_compute_loan_account_move', string='Receptions', default=0)
    account_move_ids = fields.Many2many('account.move', compute='_compute_loan_account_move',
                                        string='Productions',
                                        copy=False)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirmed', 'Confirmed'),
        ('done', 'Done'),
    ], 'State', readonly=True, default='draft')

    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Loan risk fund number must be unique!'),
    ]

    # Smart button дээр журналын бичилтын хэд байгаа тоог харуулна
    def _compute_loan_account_move(self):
        for obj in self:
            account_move_obj = self.env['account.move']
            account_move_ids = account_move_obj.search([('loan_risk_fund_id', '=', obj.id)])
            if account_move_ids:
                obj.account_move_ids = account_move_ids

            obj.account_move_count = len(obj.account_move_ids)

    # Smart button дарахад ЖБ-рүү үсрэнэ
    @api.multi
    def view_loan_account_moves(self):
        for obj in self:
            action = self.env.ref('account.action_move_line_form')
            result = action.read()[0]

            # override the context to get rid of the default filtering on account move
            result.pop('id', None)
            result['context'] = {}
            account_move_ids = sum([obj.account_move_ids.ids for obj in self], [])
            if account_move_ids:
                result['domain'] = "[('id','in',[" + ','.join(map(str, account_move_ids)) + "])]"
            return result

    @api.multi
    def action_to_confirmed(self):
        for obj in self:
            obj.write({'state': 'confirmed',
                       'name': self.env['ir.sequence'].get('loan.risk.fund'),
                       'date_confirm': datetime.now()})
        return True

    @api.multi
    def action_to_done(self):
        for obj in self:
            obj.write({'state': 'done'})
            obj.create_risk_fund_account_move()
        return True

    @api.multi
    def action_cancel(self):
        for obj in self:
            obj.write({'state': 'draft'})
            if obj.account_move_ids:
                obj.account_move_ids.button_cancel()
                obj.account_move_ids = False
        return True

    # Зээлийн эрсдлийн сан байгуулах
    # журналын бичилт үүсгэх функ
    @api.multi
    def create_risk_fund_account_move(self):
        account_move_obj = self.env['account.move']
        for obj in self:
            account_move_ids = []
            for loan_order_id in obj.loan_order_ids:
                move_line_ids = [(0, 0, {
                    'name': obj.name,
                    'debit': loan_order_id.amount,
                    'credit': 0,
                    'account_id': obj.account_risk_expense_id.id,
                    'amount_currency': 0,
                    'currency_id': False,
                    'date': obj.date_order,
                    'journal_id': obj.journal_id.id,
                    'partner_id': loan_order_id.partner_id.id,
                }), (0, 0, {
                    'name': obj.name,
                    'debit': 0,
                    'credit': loan_order_id.amount,
                    'account_id': obj.journal_id.default_credit_account_id.id,
                    'amount_currency': 0,
                    'currency_id': False,
                    'date': obj.date_order,
                    'journal_id': obj.journal_id.id,
                    'partner_id': loan_order_id.partner_id.id,
                })]
                # Шилжүүлэх гэж буй данс Дт буюу зардлын данс
                # Шилжих гэж буй данс Кт буюу журнал дээрх үндсэн данс
                # Зээлийн эрсдлийн сан байгуулах
                account_move_id = account_move_obj.create({
                    'journal_id': obj.journal_id.id,
                    'date': obj.date_order,
                    'loan_id': loan_order_id.id,
                    'loan_risk_fund_id': obj.id,
                    'description': _("%s :: /Loan Risk Fund/" % obj.name),
                    'line_ids': move_line_ids})
                account_move_ids.append(account_move_id.id)
            obj.account_move_ids = account_move_ids
