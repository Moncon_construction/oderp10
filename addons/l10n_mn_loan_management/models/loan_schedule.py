# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

import calendar
import datetime
from datetime import datetime, date
from dateutil.relativedelta import relativedelta


class LoanSchedule(models.Model):
    _name = "loan.schedule"
    _order = "prior_id"

    name = fields.Char(string='Graphic name',
                       default=lambda self: self.env['ir.sequence'].next_by_code('loan.schedule'))
    contract_id = fields.Many2one('loan.order', 'Contract number')
    interest_payment = fields.Float('Interest', digits=(20, 2))
    pay_payment = fields.Float('Pay Payment', digits=(20, 2))
    total_payment = fields.Float('Total payment', digits=(20, 2), compute='compute_total_payment')
    balance = fields.Float('Balance', digits=(16, 2))
    loan_payment = fields.Float('Loan', digits=(16, 2))
    payment_date = fields.Date('Payment Date')
    interest_payment_date = fields.Date('Interest Payment Date')  # Зээлийн хүү графикийн дагуу төлөгдөх огноо
    date_of_contact = fields.Datetime('Date of Contact')
    loan_quality = fields.Selection([('worst', 'Worst'),
                                     ('bad', 'Bad'),
                                     ('mid', 'Mid'),
                                     ('good', 'Good'),
                                     ('best', 'Best')], string='Loan quality')
    note = fields.Char('Note')
    note_taken = fields.Many2one('hr.employee', string='Not taken by')
    days = fields.Integer('Interest Charged days')
    prior_id = fields.Integer('Prior ID')
    is_paid = fields.Selection([('un_paid', 'Un paid'),
                                ('late_paid', 'Late paid'),
                                ('paid', 'Normal paid')], string='Is paid', default='un_paid')
    remaining_payment = fields.Float('Difference +/-', digits=(16, 2), default=lambda *a: 0)
    company_id = fields.Many2one('res.company', string='Company', readonly=True,
                                 default=lambda self: self.env['res.company']._company_default_get('loan.schedule'),
                                 required=True)
    do_pay = fields.Boolean(string='Do Pay', default=False)
    do_refund_accrued_interest = fields.Float(string='Do Refund Accrued Interest', default=0.0)
    accrued_interest = fields.Float('Accrued Interest')
    interest_income_on_time = fields.Float('Interest Income')
    increased_interest = fields.Float('Increased interest')
    accrued_interest_account_move_id = fields.Many2one('account.move', 'Accrued Interest Account Move')
    refund_accrued_interest_account_move_id = fields.Many2one('account.move', 'Refund Accrued Interest Account Move')
    move_check = fields.Selection([('none', 'None'),
                                   ('draft', 'Draft'),
                                   ('posted', 'Posted')], compute='_get_move_check', string='Linked')

    # Мөр дэх журналын бичилтын төлөв тооцоолол
    @api.multi
    @api.depends('accrued_interest_account_move_id')
    def _get_move_check(self):
        for line in self:
            if line.accrued_interest_account_move_id.state == 'draft':
                line.move_check = 'draft'
            elif line.accrued_interest_account_move_id.state == 'posted':
                line.move_check = 'posted'
            elif not line.accrued_interest_account_move_id:
                line.move_check = 'none'

    # Тухайн зээлийн эргэн төлөлтийн хуваарийн хүүг хуримтлуулж тооцоолох функц
    @api.multi
    def compute_accrued_interest(self):
        for obj in self:
            if obj.payment_date:
                year, month, day = map(int, obj.payment_date.split('-'))
                first_day_of_month = 1
                last_day_of_month = calendar.monthrange(year, month)[1]
                if last_day_of_month:
                    # Зээлийн хүүгийн хуримтлал хийх үед тухайн төлөлтийн огнооноос
                    # сарын сүүлийн өдөр хүртэлх хугацаанд хүү хуримтлуулах
                    accrue_day = last_day_of_month - day
                    if accrue_day > 0:
                        payment_of_day_interest = obj.balance * obj.contract_id.interest_by_year / 100 / 365 * accrue_day
                        if payment_of_day_interest > 0:
                            obj.accrued_interest = payment_of_day_interest
                if first_day_of_month:
                    # Зээлийн хүүгийн хуримтлал хийх үед тухайн төлөлтийн огнооноос
                    # сарын эхний өдөр хүртэлх хугацаа дахь хүүг орлго гэж үзэх
                    interest_day = day
                    if interest_day > 0:
                        payment_of_day_interest = obj.balance * obj.contract_id.interest_by_year / 100 / 365 * interest_day
                        if payment_of_day_interest > 0:
                            obj.interest_income_on_time = payment_of_day_interest

    # Хүү хуримтлуулсан бол ЖБ үүсгэх функц
    @api.multi
    def create_accrued_interest_account_move(self, is_refund=False, refund_interest=False):
        account_move_obj = self.env['account.move']
        for line in self:
            if not line.contract_id.product_id.accrued_loan_interest_receivables_account_id:
                raise UserError(_('Accrued Loan Interest Receivable Account not found!'))
            if not line.contract_id.loan_state:
                raise UserError(_('Loan Order State not found!'))
            if not line.contract_id.loan_state.income_citizen_account_id.id:
                raise UserError(_('Loan Category Income Account not found!'))
            if not line.contract_id.product_id.accrued_interest_journal_id:
                raise UserError(_('Loan Product Accrued Interest Journal not found!'))
            move_line_ids = []
            # Хуримтлагдсан хүүгийн буцаалтын бичилт хийх
            if is_refund and refund_interest:
                if not line.contract_id.loan_state.interest_income_refund_account_id:
                    raise UserError(_('Loan Order Interest Income Refund Account not found!'))
                if line.refund_accrued_interest_account_move_id:
                    if line.refund_accrued_interest_account_move_id.state == 'posted':
                        raise UserError(
                            _(
                                'Loan Schedule Refund of Accrued Account Move already posted! %s' % line.contract_id.name))
                    if line.refund_accrued_interest_account_move_id.state == 'draft':
                        line.refund_accrued_interest_account_move_id.unlink()
                # Авлагын данс данс зээлийн бүтээгдэхүүнээс авна
                move_line_ids.append((0, 0, {
                    'name': line.name,
                    'debit': 0,
                    'credit': refund_interest,
                    'account_id': line.contract_id.product_id.accrued_loan_interest_receivables_account_id.id,
                    'amount_currency': 0,
                    'currency_id': False,
                    'date': line.payment_date,
                    'journal_id': line.contract_id.product_id.accrued_interest_journal_id.id,
                    'partner_id': line.contract_id.partner_id.id,
                }))
                # Орлогын буцаалтын данс зээлийн ангилалаас авна
                move_line_ids.append((0, 0, {
                    'name': line.name,
                    'debit': refund_interest,
                    'credit': 0,
                    'account_id': line.contract_id.loan_state.interest_income_refund_account_id.id,
                    'amount_currency': 0,
                    'currency_id': False,
                    'date': line.payment_date,
                    'journal_id': line.contract_id.product_id.accrued_interest_journal_id.id,
                    'partner_id': line.contract_id.partner_id.id,
                }))
                # Хүү хуримтлалын буцаалт ЖБ
                account_move_id = account_move_obj.create({
                    'journal_id': line.contract_id.product_id.accrued_interest_journal_id.id,
                    'date': line.contract_id.date_order,
                    'loan_id': line.contract_id.id,
                    'description': _("%s /Refund of Accrued Loan Interest/" % line.contract_id.name),
                    'line_ids': move_line_ids})
                line.refund_accrued_interest_account_move_id = account_move_id.id
            # Хуримтлагдсан хүүгийн бичилт хийх
            elif line.accrued_interest:
                if line.accrued_interest_account_move_id:
                    if line.accrued_interest_account_move_id.state == 'posted':
                        raise UserError(
                            _('Loan Schedule Accrued Account Move already posted! %s' % line.contract_id.name))
                    if line.accrued_interest_account_move_id.state == 'draft':
                        line.accrued_interest_account_move_id.unlink()
                # Авлагын данс данс зээлийн бүтээгдэхүүнээс авна
                move_line_ids.append((0, 0, {
                    'name': line.name,
                    'debit': line.accrued_interest,
                    'credit': 0,
                    'account_id': line.contract_id.product_id.accrued_loan_interest_receivables_account_id.id,
                    'amount_currency': 0,
                    'currency_id': False,
                    'date': line.payment_date,
                    'journal_id': line.contract_id.product_id.accrued_interest_journal_id.id,
                    'partner_id': line.contract_id.partner_id.id,
                }))
                # Орлогын данс зээлийн ангилалаас авна
                move_line_ids.append((0, 0, {
                    'name': line.name,
                    'debit': 0,
                    'credit': line.accrued_interest,
                    'account_id': line.contract_id.loan_state.income_citizen_account_id.id,
                    'amount_currency': 0,
                    'currency_id': False,
                    'date': line.payment_date,
                    'journal_id': line.contract_id.product_id.accrued_interest_journal_id.id,
                    'partner_id': line.contract_id.partner_id.id,
                }))
                # Хүү хуримтлалын ЖБ
                account_move_id = account_move_obj.create({
                    'journal_id': line.contract_id.product_id.accrued_interest_journal_id.id,
                    'date': line.contract_id.date_order,
                    'loan_id': line.contract_id.id,
                    'description': _("%s /Accrued Loan Interest/" % line.contract_id.name),
                    'line_ids': move_line_ids})
                line.accrued_interest_account_move_id = account_move_id.id

    # Хүү хуримтлуулсан ЖБ цуцлах
    @api.multi
    def cancel_accrued_interest_account_move(self):
        for obj in self:
            if obj.accrued_interest_account_move_id.state == 'posted':
                obj.accrued_interest_account_move_id.button_cancel()
            return

    # Хүү хуримтлуулсан ЖБ устгах
    @api.multi
    def delete_accrued_interest_account_move(self):
        for obj in self:
            if obj.accrued_interest_account_move_id.state == 'draft':
                obj.accrued_interest_account_move_id.unlink()
            return

    # Нэмэгдүүлсэн хүүгийн тооцоолол
    @api.multi
    def create_increased_interest(self):
        # Хоёр огнооны хоорондох өдрийн тоо олох функц
        def diff_dates(date2, date1):
            return abs(date2 - date1).days

        for obj in self:
            payment_date = obj.payment_date
            if obj.is_paid == 'un_paid' and payment_date < datetime.now().strftime('%Y-%m-%d'):
                year, month, day = map(int, payment_date.split('-'))
                start_date = date(year, month, day)
                year, month, day = map(int, datetime.now().strftime('%Y-%m-%d').split('-'))
                end_date = date(year, month, day)
                # Өмнөх төлөлтөөс дараагийн төлөх хүртэлх зөрүү хоног
                diff = diff_dates(start_date, end_date)
                if diff > 0:
                    obj.increased_interest = obj.balance * obj.contract_id.interest_by_year / 100 / 365 * diff
            else:
                obj.increased_interest = 0

    # Нэмэгдүүлсэн хүүг арилгах
    @api.multi
    def clear_increased_interest(self):
        for obj in self:
            obj.increased_interest = 0

    @api.multi
    def refund_accrued_interest_income_account_move(self):
        for obj in self:
            return

    @api.multi
    def _cron_loan_schedule(self):
        # =======================================================================
        # Зээлийн төлбөр төлөх хуваарийг сануулах крон.
        # =======================================================================
        today = str(datetime.date.today())
        convert = datetime.datetime.strptime(today, '%Y-%m-%d')
        dates = convert + relativedelta(days=+1)
        schedules = self.env['loan.schedule'].search([('payment_date', '=', dates)])
        name = ''
        for schedule in schedules:
            if schedule.contract_id.state == 'done':
                base_url = self.env['ir.config_parameter'].get_param('web.base.url')
                outgoing_email_ids = self.env['ir.mail_server'].sudo().search([])
                if not outgoing_email_ids:
                    raise UserError(
                        _('There is no configuration for outgoing mail server. Please contact system administrator.'))
                else:
                    outgoing_email = self.env['ir.mail_server'].sudo().search([('id', '=', outgoing_email_ids[0].id)])
                    email_ids = 0
                    if schedule.contract_id.partner_id.company_type == 'person':
                        name = str(schedule.contract_id.partner_id.surname or '') + ' овогтой ' + str(
                            schedule.contract_id.partner_id.name)
                    else:
                        name = str(schedule.contract_id.partner_id.name)
                    vals = {
                        'subject': 'Зээлийн төлбөрийн сануулга',
                        'body_html': '<p>Сайн байна уу? Энэ өдрийн мэнд хүргэе </br></pr>'
                                     '<pr><br>' + ' Зээлдэгч ' + name + ' -н Зээлийн хүсэлтийн код ( ' + str(
                            schedule.contract_id.name) + ' ) </br></pre>'
                                                         '<pr><br>  гэрээний дагуу ' + str(
                            schedule.payment_date) + '-нд ' + str(
                            schedule.total_payment) + ' төгрөгийн төлбөр төлөхийг сануулж байна. </br></pr>',
                        'email_to': schedule.contract_id.partner_id.email,
                        'email_from': outgoing_email.smtp_user,
                    }
                    email_ids = self.env['mail.mail'].create(vals)
                    if email_ids != 0:
                        email_ids.send()

    @api.multi
    def compute_total_payment(self):
        for obj in self:
            obj.total_payment = obj.loan_payment + obj.interest_payment

    # Төлөгдсөн эргэн төлөлтийн хуваарь устгаж болохгүй анхааруулга
    @api.multi
    def unlink(self):
        for obj in self:
            if obj.is_paid == 'paid' or obj.is_paid == 'late_paid':
                raise UserError(_('Paid schedule cant be delete'))
            else:
                return super(LoanSchedule, self).unlink()
