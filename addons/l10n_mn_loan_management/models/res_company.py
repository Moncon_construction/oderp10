# -*- encoding: utf-8 -*-

from odoo import fields, models


class ResCompany(models.Model):
 	_inherit = "res.company"
	"""
	Зээлийн төлөлт хийхэд ашиглагдана
	"""
	property_default_analytic_account = fields.Many2one('account.analytic.account', 'Analytic Account')
