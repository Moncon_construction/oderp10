# -*- encoding: utf-8 -*-

from odoo import fields, models, api, _

from datetime import datetime, date
from dateutil import parser

'''
	Loan Debitor
'''


class ResPartnerCategory(models.Model):
    _name = "res.partner.type"

    name = fields.Char('Category Name', required=True, size=50)
    parent_id = fields.Many2one('res.partner.type', 'Parent Category', ondelete='cascade')
    category_type = fields.Selection([
        ('customer_type', 'Customer type'),
        ('business_type', 'Business type')], 'Type')


class ResPartner(models.Model):
    _inherit = "res.partner"

    """
        Харилцагчийн моделийг удамшуулж Зээлдэгч цэсэнд харуулах талбарууд
    """

    @api.multi
    def _compute_collateral_count(self):
        # Барьцаа тоолох функц
        for line in self:
            loan_order = self.env['loan.order'].search([('partner_id', '=', line.id)])
            collateral = self.env['loan.collateral'].search([('contract_id', '=', loan_order.ids)])
            line.collateral_count = len(collateral) or 0

    code = fields.Char('Customer ID', size=16, readonly=True,
                       default=lambda self: self.env['ir.sequence'].get('res.partner'))
    is_trustee = fields.Boolean('Is trustee')  # Итгэлцэл эсэх
    is_debtor = fields.Boolean('Is debtor', default=True)  # Зээлдэгч эсэх
    id_address = fields.Text('Identification Address')
    stamp = fields.Binary('Stamp')  # Тамга
    signature = fields.Binary('Signature')  # Гарын үсэг
    age = fields.Integer('Age', compute='_compute_age')  # Нас
    sex = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female')], 'Gender')  # Хүйс
    education = fields.Selection([
        ('higher', 'Higher Education'),
        ('special_secondary', 'Special secondary Education'),
        ('high_school_education', 'High School Education'),
        ('secondary', 'Secondary Education'),
        ('primary', 'Primary Education')], 'Education', default='higher', required=True)  # Боловсрол
    established_date = fields.Date('Date of Enterprise registration', default=datetime.now())
    category_id = fields.Many2many('res.partner.category', id1='partner_id', id2='category_id',
                                   string='Tags')  # Харилцагчийн ангилал
    collateral_count = fields.Integer(string='# of Collateral', compute='_compute_collateral_count', readonly=True)
    behavior = fields.Char('Behavior')
    capacity = fields.Char('Capacity')
    collateral = fields.Char('Collateral')
    reserves = fields.Char('Reserves')
    situation = fields.Char('Situation')
    repayment_of_loan = fields.Integer('Repayment of loan', compute='count_loan_history')
    loan_history = fields.One2many('loan.order', 'partner_id', 'Loan history')  # Зээлийн түүх
    nationality = fields.Char('Nationality')  # Нийгмийн гарал угсаа
    root = fields.Char('Root')  # Яс үндэс
    family_name = fields.Char('Family name', size=128)  # Ургийн овог
    identification_home_address = fields.Char('Home address of Identification Card', size=128)  # Ургийн овог
    second_phone = fields.Char('2nd Phone')  # Холбогдох утасны дугаар 2
    third_phone = fields.Char('3rd Phone')  # Холбогдох утасны дугаар 3

    @api.depends('birthday')
    def _compute_age(self):
        current_date = datetime.now()
        current_year = current_date.year
        for partner in self:
            partner.age = current_year - parser.parse(partner.sudo().birthday).year if partner.sudo().birthday else 0

    @api.multi
    def count_loan_history(self):
        # Зээлийн түүх тоолуух
        for obj in self:
            loans = self.env['loan.order'].search([('partner_id', '=', obj.id), ('state', '=', 'done')])
            obj.repayment_of_loan = len(loans)

    @api.onchange('birthdate')
    def onchange_calc_age(self):
        # Төрсөн өдрөөс насыг тооцоолох
        if self.birthdate:
            today = date.today()
            born = datetime.strptime(self.birthdate, '%Y-%m-%d')
            ager = today.year - born.year - ((today.month, today.day) < (born.month, born.day))
            self.age = ager

    @api.multi
    def action_view_collateral(self):
        # Ухаалаг даруул дээр дархад холбоотой барьцааг харуулах функц
        coll = []
        for line in self:
            loan_order = self.env['loan.order'].search([('partner_id', '=', line.id)])
            collateral = self.env['loan.collateral'].search([('contract_id', '=', loan_order.ids)])
            for line in collateral:
                coll.append(line.id)
            return {
                'type': 'ir.actions.act_window',
                'name': _('Loan Collateral'),
                'res_model': 'loan.collateral',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'view_id': False,
                'domain': [('id', 'in', coll)],
            }
