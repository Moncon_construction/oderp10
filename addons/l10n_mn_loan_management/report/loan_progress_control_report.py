# -*- encoding: utf-8 -*-
from odoo import fields, models, _
from odoo.exceptions import UserError
import base64
from io import BytesIO
from datetime import datetime, timedelta, date
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as DATE_FORMAT
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class OverdueLoanReport(models.TransientModel):
    _inherit = 'oderp.report.html.output'
    _name = 'progress.control.report'
    _description = 'Progress control report'

    company_id = fields.Many2one('res.company', readonly=True, default=lambda s: s.env.user.company_id.id,
                                 string=_('Company'))
    # transaction_filter = fields.Selection([('posted', _('Posted transactions')), ('all', _('All transactions'))],
    #                                       default='posted', required=True, string=_('Transaction filter'))

    partner_ids = fields.Many2many('res.partner', 'partner_progress_report_rel', 'report_id',
                                   'partner_id', string='Loan partners', domain=[('is_debtor', '=', True)])
    date_start = fields.Date(string=_('Start date'), required=True, default=date.today().replace(day=1))
    date_end = fields.Date(string=_('End date'), required=True, default=date.today())

    _START_COL = 0

    def check_date(self):
        if self.date_start > self.date_end:
            raise UserError(_('End date must be after Start date'))

    def get_header_and_total_col(self):
        """Method to get header and total col
        :returns 'Total column number' -> Нийт баганын тоо
                 'Headers' -> Тайлангийн толгой
                 'Total row of headers' -> Нийт мөрийн тоо
         """
        headers = [{'name': _('Sequence')},
                   {'name': _('Contacted date')},
                   {'name': _('Loan quality')},
                   {'name': _('Loan balance')},
                   {'name': _('amount to be paid')},
                   {'name': _('Note')},
                   {'name': _('Note written')}]
        col = 7
        return col, headers

    def get_body(self):
        if not self.partner_ids:
            loan_orders = self.env['loan.order'].search(['&', ('date_approve', '<=', self.date_end),
                                                        '&', ('date_approve', '>=', self.date_start),
                                                         ('state', '=', 'done')])
        else:
            loan_orders = self.env['loan.order'].search(['&', ('date_approve', '<=', self.date_end),
                                                         '&', ('date_approve', '>=', self.date_start),
                                                         '&', ('partner_id', 'in', self.partner_ids.ids),
                                                         ('state', '=', 'done')])
        return loan_orders

    def export_report(self):
        self.check_date()
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        report_name = _('Progress control report')
        sheet = book.add_worksheet('Progress control report')
        sheet.set_landscape()
        sheet.hide_gridlines(2)
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})

        # Setting up formats
        format_title = book.add_format(ReportExcelCellStyles.format_name)
        format_name = book.add_format(ReportExcelCellStyles.format_filter)
        format_filter_right_dict = ReportExcelCellStyles.format_filter_right
        format_filter_right_dict['num_format'] = '#,##0.00'
        format_name_right = book.add_format(format_filter_right_dict)
        format_header_center = book.add_format(ReportExcelCellStyles.format_title_small)
        format_header_float = book.add_format(ReportExcelCellStyles.format_title_float)
        format_header_float.set_align('left')
        format_filter = ReportExcelCellStyles.format_filter_center
        format_filter['bold'] = False
        format_filter['border'] = 1
        format_filter['text_wrap'] = 1
        format_filter_center = book.add_format(format_filter)
        format_filter['align'] = 'left'
        format_filter_left = book.add_format(format_filter)
        format_filter['num_format'] = '#,##0.00'
        format_filter_float = book.add_format(format_filter)

        # Setting up columns
        sheet.set_default_row(17)
        sheet.set_row(2, 40)
        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 18)
        sheet.set_column('C:C', 18)
        sheet.set_column('D:D', 18)
        sheet.set_column('E:E', 18)
        sheet.set_column('F:F', 18)
        sheet.set_column('G:G', 18)

        rowx = 0
        total_col, headers = self.get_header_and_total_col()
        sheet.merge_range(rowx, self._START_COL, rowx, total_col - 1, _('Company name') + ':' + self.company_id.name,
                          format_name)
        rowx += 1
        sheet.merge_range(rowx, self._START_COL, rowx, total_col - 1, report_name.upper(), format_title)
        rowx += 1
        sheet.merge_range(rowx, self._START_COL, rowx, total_col - 5, _('Duration') + ':' + self.date_start + ":" + self.date_end,
                          format_name)
        sheet.merge_range(rowx, total_col - 4, rowx, total_col - 1, _('Loan economist: ') + self.env.user.partner_id.name,
                          format_name_right)
        rowx += 1
        sheet.merge_range(rowx, self._START_COL, rowx, total_col - 1, _('Create date: ') + datetime.now().date().strftime('%Y-%m-%d'),
                          format_name)
        rowx += 2
        body = self.get_body()
        # drawing body
        self.env.context = {'lang': self.env.user.lang}
        for obj in body:
            sheet.merge_range(rowx, self._START_COL, rowx, total_col - 5, _('Product: ') + obj.product_id.name, format_name)
            rowx += 1
            sheet.merge_range(rowx, self._START_COL, rowx, total_col - 5, _('State: ') + (obj.loan_state.name or ''), format_name)
            rowx += 1
            sheet.merge_range(rowx, self._START_COL, rowx, total_col - 5, _('Debtor №: ') + '', format_name)
            sheet.merge_range(rowx, total_col - 4, rowx, total_col - 1, _('Contract №: ') + obj.name, format_name_right)
            rowx += 1
            sheet.merge_range(rowx, self._START_COL, rowx, total_col - 5, _('Debtor name: ') + obj.partner_id.name, format_name)
            sheet.merge_range(rowx, total_col - 4, rowx, total_col - 1, _('Economist: ') + (obj.current_economist_id.name or ''),
                              format_name_right)
            rowx += 1
            # drawing header
            head_col = self._START_COL
            for header in headers:
                sheet.write(rowx, head_col, header['name'], format_header_center)
                head_col += 1
            rowx += 1
            number = 1
            body_row = rowx
            for schedule in obj.schedules_ids:
                colx = self._START_COL
                sheet.write(rowx, colx, number, format_filter_center)
                colx += 1
                number += 1
                sheet.write(rowx, colx, schedule.date_of_contact or '', format_filter_center)
                colx += 1
                sheet.write(rowx, colx, dict(schedule._fields['loan_quality']._description_selection(self.env)).get(schedule.loan_quality), format_filter_center)
                colx += 1
                sheet.write(rowx, colx, schedule.balance or '', format_filter_float)
                colx += 1
                sheet.write(rowx, colx, schedule.total_payment or '', format_filter_float)
                colx += 1
                sheet.write(rowx, colx, schedule.note or '', format_filter_center)
                colx += 1
                sheet.write(rowx, colx, (schedule.note_taken.name or ''), format_filter_center)
                rowx += 1
            sheet.merge_range(rowx, self._START_COL, rowx, 2, _('Total Amount').upper(), format_header_center)
            sheet.write_formula(rowx, 3, '{=SUM(' + xl_rowcol_to_cell(rowx - 1, 3) + ':' + xl_rowcol_to_cell(body_row, 3) + ')}',
                                format_header_float)
            sheet.write_formula(rowx, 4, '{=SUM(' + xl_rowcol_to_cell(rowx - 1, 4) + ':' + xl_rowcol_to_cell(body_row, 4) + ')}',
                                format_header_float)
            sheet.write(rowx, 5, '', format_header_float)
            sheet.write(rowx, 6, '', format_header_float)
            rowx += 2
        book.close()
        out = base64.encodestring(output.getvalue())
        excel_id = self.env['oderp.report.excel.output'].with_context(filename_prefix=report_name).create(
            {'filedata': out})
        return excel_id.export_report()
