# -*- coding: utf-8 -*-

from datetime import datetime
import time
from odoo import api, models, _
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.addons.l10n_mn_report.models.report_helper import comma_me


class ReportLoanSchedule(models.AbstractModel):
    _name = 'report.l10n_mn_loan_management.loan_schedule_report'

    @api.model
    def render_html(self, docids, data=None):
        context = self._context.copy() or {}
        report_obj = self.env['report']
        loan_obj = self.env['loan.order']
        line_obj = self.env['loan.schedule']
        report = report_obj._get_report_from_name('l10n_mn_loan_management.loan_schedule_report')
        active_ids = docids
        if 'ids' in data and data['ids']:
            active_ids = data['ids']
        loans = loan_obj.browse(active_ids)
        verbose_total_dict = {}
        lines = []
        total_dict = {'avail_qty': 0,
                      'avail_amount': 0.0,
                      'check_qty': 0,
                      'check_amount': 0.0,
                      'less_qty': 0,
                      'less_amount': 0.0,
                      'more_qty': 0,
                      'more_amount': 0.0}
        total_days = 0
        total_loan_payment = 0
        total_interest_payment = 0
        total_payment = 0
        line_dict = {}
        counter = 0
        for loan in loans:
            month_interest = round(loan.interest_by_year/12,2)
            for i in loan.schedules_ids:
                counter += 1
                total_days += i.days
                total_loan_payment += i.loan_payment
                total_interest_payment += i.interest_payment
                total_payment += i.total_payment

        total_dict['total_days'] = comma_me(total_days)
        total_dict['total_loan_payment'] = comma_me(total_loan_payment)
        total_dict['total_interest_payment'] = comma_me(total_interest_payment)
        total_dict['total_payment'] = comma_me(total_payment)
        docargs = {
            'doc_ids': docids,
            'doc_model': report.model,
            'docs': loans,
            'month_interest':month_interest,
            'total_dict': total_dict
            }
        return self.env['report'].render('l10n_mn_loan_management.loan_schedule_report', docargs)
