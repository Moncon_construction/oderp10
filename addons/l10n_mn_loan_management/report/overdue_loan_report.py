# -*- encoding: utf-8 -*-
from odoo import fields, models, _
from odoo.exceptions import UserError
import base64
from io import BytesIO
from datetime import datetime, timedelta, date
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell_fast
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from xlsxwriter.utility import xl_rowcol_to_cell
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT


class OverdueLoanReport(models.TransientModel):
    _inherit = 'oderp.report.html.output'
    _name = 'overdue.loan.report'
    _description = 'Overdue loan report'

    company_id = fields.Many2one('res.company', readonly=True, default=lambda s: s.env.user.company_id.id,
                                 string=_('Company'))

    date_start = fields.Date(string=_('Start date'), required=True, default=date.today().replace(day=1))
    date_end = fields.Date(string=_('End date'), required=True, default=date.today())

    _START_COL = 0

    def check_date(self):
        if self.date_start > self.date_end:
            raise UserError(_('End date must be after Start date'))

    def get_header_and_total_col(self):
        """Method to get header and total col
        :returns 'Total column number' -> Нийт баганын тоо
                 'Headers' -> Тайлангийн толгой
                 'Total row of headers' -> Нийт мөрийн тоо
         """
        headers = [{'name': _('Sequence')},
                   {'name': _('Full name'),
                    'subheader': [{'name': _('Surname')},
                                  {'name': _('Name')}]},
                   {'name': _('Loan interest /year/')},
                   {'name': _('Loan duration'),
                    'subheader': [{'name': _('Start date')},
                                  {'name': _('End date')}]},
                   {'name': _('Loan'),
                    'subheader': [{'name': _('Loan balance')},
                                  {'name': _('Accumulated interest')},
                                  {'name': _('Added interest')},
                                  {'name': _('Total')}]},
                   {'name': _('Loan overdue days'),
                    'subheader': [{'name': _('Normal 0-15')},
                                  {'name': _('Attention required 16-90')},
                                  {'name': _('Abnormal 91-180')},
                                  {'name': _('Doubtful 181-360')},
                                  {'name': _('Bad 361<')}]},
                   {'name': _('Last payment date')},
                   {'name': _('Description')}]
        col = 17
        return col, headers, 2

    def get_body(self):
        loan_schedule = self.env['loan.schedule'].search(['&', ('payment_date', '>=', self.date_start),
                                                          '&', ('payment_date', '<=', self.date_end),
                                                          ('is_paid', '=', 'un_paid')])
        loan_order = loan_schedule.mapped('contract_id').filtered(lambda l: l.state == 'done')
        return loan_order

    def get_footer(self):
        return [_('Total loan contract number, amount'),
                _('Loan type, total value'),
                _('Loan type, total percentage'),
                _('Loan type, accumulated interest'),
                _('Loan type, accumulated interest percentage'),
                _('Loan type, added interest'),
                _('Loan type, added interest percentage'),
                _('Total loan'),
                _('Total loan percentage')], 4

    def export_report(self):
        self.check_date()
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        report_name = _('Overdue loan report')
        sheet = book.add_worksheet('Overdue loan report')
        sheet.set_landscape()
        sheet.hide_gridlines(2)
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})

        # Setting up formats
        format_title = book.add_format(ReportExcelCellStyles.format_name)
        format_name = book.add_format(ReportExcelCellStyles.format_filter)
        format_header = ReportExcelCellStyles.format_title_small
        format_header['align'] = 'center'
        format_header_center = book.add_format(format_header)
        format_header_rotation = book.add_format(format_header)
        format_header_rotation.set_rotation(90)
        format_header['align'] = 'left'
        format_header_left = book.add_format(format_header)
        format_header['align'] = 'right'
        format_header['num_format'] = '#,##0.00'
        format_header_right = book.add_format(format_header)
        format_filter = ReportExcelCellStyles.format_filter_center
        format_filter['bold'] = False
        format_filter['border'] = 1
        format_filter_center = book.add_format(format_filter)
        format_filter['align'] = 'left'
        format_filter_left = book.add_format(format_filter)
        format_filter['align'] = 'right'
        format_filter['num_format'] = '#,##0.00'
        format_filter_float = book.add_format(format_filter)
        format_filter_date = book.add_format(ReportExcelCellStyles.format_content_date)

        # Setting up columns
        sheet.set_default_row(17)
        sheet.set_row(2, 40)
        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 18)
        sheet.set_column('C:C', 18)
        sheet.set_column('D:D', 4)
        sheet.set_column('E:E', 10)
        sheet.set_column('F:F', 10)
        sheet.set_column('G:G', 12)
        sheet.set_column('H:H', 17)
        sheet.set_column('I:I', 17)
        sheet.set_column('J:J', 12)
        sheet.set_column('K:K', 10)
        sheet.set_column('L:L', 10)
        sheet.set_column('M:M', 10)
        sheet.set_column('N:N', 10)
        sheet.set_column('O:O', 10)
        sheet.set_column('P:Q', 10)
        sheet.set_column('Q:Q', 10)
        sheet.set_row(3, 60)
        sheet.set_row(4, 60)

        rowx = 0
        total_col, headers, total_row = self.get_header_and_total_col()
        sheet.merge_range(rowx, self._START_COL, rowx, total_col - 1, _('Company name') + ':' + self.company_id.name,
                          format_name)
        rowx += 1
        sheet.merge_range(rowx, self._START_COL, rowx, total_col - 1, report_name.upper(), format_title)
        rowx += 1
        sheet.merge_range(rowx, self._START_COL, rowx, total_col - 1, _('Duration') + ':' + self.date_start + ":" + self.date_end,
                          format_name)
        rowx += 1
        head_col = self._START_COL
        # drawing header
        for header in headers:
            if not header.get('subheader'):
                alpha = xl_rowcol_to_cell_fast(rowx, head_col)
                if alpha[0] == 'D':
                    sheet.merge_range(rowx, head_col, rowx + total_row-1, head_col, header['name'],
                                      format_header_rotation)
                else:
                    sheet.merge_range(rowx, head_col, rowx + total_row-1, head_col, header['name'],
                                      format_header_center)
                head_col += 1
            else:
                sheet.merge_range(rowx, head_col, rowx, head_col + len(header['subheader']) - 1,
                                  header['name'], format_header_center)
                for sub in header['subheader']:
                    sheet.write(rowx+1, head_col, sub['name'], format_header_center)
                    head_col += 1
        lists = self.get_body()
        rowx += total_row
        number = 1
        rowx_foot_sum = rowx
        # initiating footer data variables
        normal_total_balance = 0.0
        normal_total_acc_interest = 0.0
        normal_total_added_interest = 0.0
        normal_total_amount = 0.0
        att_total_balance = 0.0
        att_total_acc_interest = 0.0
        att_total_added_interest = 0.0
        att_total_amount = 0.0
        not_total_balance = 0.0
        not_total_acc_interest = 0.0
        not_total_added_interest = 0.0
        not_total_amount = 0.0
        doubt_total_balance = 0.0
        doubt_total_acc_interest = 0.0
        doubt_total_added_interest = 0.0
        doubt_total_amount = 0.0
        bad_total_balance = 0.0
        bad_total_acc_interest = 0.0
        bad_total_added_interest = 0.0
        bad_total_amount = 0.0
        # drawing body
        for obj in lists:
            col = self._START_COL
            unpaid_schedules = obj.schedules_ids.filtered(lambda l: self.date_start <= l.payment_date <= self.date_end
                                                          and l.is_paid == 'un_paid')
            unpaid_date = datetime.strptime(min(obj.schedules_ids.mapped('payment_date')), DEFAULT_SERVER_DATE_FORMAT).date()
            unpaid_days = (datetime.strptime(self.date_end, DEFAULT_SERVER_DATE_FORMAT).date() - unpaid_date).days
            # getting footer data
            if unpaid_days <= 15:
                normal_total_acc_interest += sum(unpaid_schedules.mapped('accrued_interest'))
                normal_total_added_interest += sum(unpaid_schedules.mapped('interest_income_on_time'))
                normal_total_balance += obj.schedules_ids.filtered(lambda l: l.is_paid == 'un_paid').sorted('payment_date')[0].balance
            elif unpaid_days <= 90:
                att_total_acc_interest += sum(unpaid_schedules.mapped('accrued_interest'))
                att_total_added_interest += sum(unpaid_schedules.mapped('interest_income_on_time'))
                att_total_balance += obj.schedules_ids.filtered(lambda l: l.is_paid == 'un_paid').sorted('payment_date')[0].balance
            elif unpaid_days <= 180:
                not_total_acc_interest += sum(unpaid_schedules.mapped('accrued_interest'))
                not_total_added_interest += sum(unpaid_schedules.mapped('interest_income_on_time'))
                not_total_balance += obj.schedules_ids.filtered(lambda l: l.is_paid == 'un_paid').sorted('payment_date')[
                    0].balance
            elif unpaid_days <= 360:
                doubt_total_acc_interest += sum(unpaid_schedules.mapped('accrued_interest'))
                doubt_total_added_interest += sum(unpaid_schedules.mapped('interest_income_on_time'))
                doubt_total_balance += obj.schedules_ids.filtered(lambda l: l.is_paid == 'un_paid').sorted('payment_date')[
                    0].balance
            elif unpaid_days > 360:
                bad_total_acc_interest += sum(unpaid_schedules.mapped('accrued_interest'))
                bad_total_added_interest += sum(unpaid_schedules.mapped('interest_income_on_time'))
                bad_total_balance += obj.schedules_ids.filtered(lambda l: l.is_paid == 'un_paid').sorted('payment_date')[
                    0].balance

            sheet.write(rowx, col, number, format_filter_center)
            col += 1
            number += 1
            sheet.write(rowx, col, obj.partner_id.surname or '', format_filter_left)
            col += 1
            sheet.write(rowx, col, obj.partner_id.name, format_filter_left)
            col += 1
            sheet.write(rowx, col, obj.interest_by_year, format_filter_center)
            col += 1
            sheet.write(rowx, col, min(obj.schedules_ids.mapped('payment_date')), format_filter_date)
            col += 1
            sheet.write(rowx, col, max(obj.schedules_ids.mapped('payment_date')), format_filter_date)
            col += 1
            sheet.write(rowx, col, obj.schedules_ids.filtered(lambda l: l.is_paid == 'un_paid').sorted('payment_date')[0].balance, format_filter_float)
            col += 1
            sheet.write(rowx, col, sum(unpaid_schedules.mapped('accrued_interest')), format_filter_float)
            col += 1
            sheet.write(rowx, col, sum(unpaid_schedules.mapped('interest_income_on_time')), format_filter_float)
            col += 1
            sheet.write_formula(rowx, col, '{=SUM(' + xl_rowcol_to_cell(rowx, col - 3) + ':' + xl_rowcol_to_cell(rowx, col - 1) + ')}', format_filter_float)
            col += 1
            sheet.write(rowx, col, unpaid_days if unpaid_days <= 15 else '', format_filter_float)
            col += 1
            sheet.write(rowx, col, unpaid_days if 16 <= unpaid_days <= 90 else '', format_filter_float)
            col += 1
            sheet.write(rowx, col, unpaid_days if 91 <= unpaid_days <= 180 else '', format_filter_float)
            col += 1
            sheet.write(rowx, col, unpaid_days if 181 <= unpaid_days <= 360 else '', format_filter_float)
            col += 1
            sheet.write(rowx, col, unpaid_days if 361 <= unpaid_days else '', format_filter_float)
            col += 1
            last_payment = obj.schedules_ids.filtered(lambda l: l.is_paid in ('late_paid', 'paid')).mapped('payment_date')
            sheet.write(rowx, col, max(last_payment) if last_payment else '', format_filter_center)
            col += 1
            sheet.write(rowx, col, '', format_filter_center)
            col += 1
            rowx += 1
        row_foot = rowx
        normal_total_amount += normal_total_added_interest + normal_total_acc_interest + normal_total_balance
        att_total_amount += att_total_added_interest + att_total_acc_interest + att_total_balance
        doubt_total_amount += doubt_total_added_interest + doubt_total_acc_interest + doubt_total_balance
        not_total_amount += not_total_added_interest + not_total_acc_interest + not_total_balance
        bad_total_amount += bad_total_added_interest + bad_total_acc_interest + bad_total_balance
        footer, footer_col = self.get_footer()
        # END OF EXAMPLE BODY
        for obj in footer:
            sheet.merge_range(rowx, self._START_COL, rowx, self._START_COL + footer_col, obj, format_header_left)
            sheet.write(rowx, self._START_COL + footer_col + 1, '', format_header_left)
            rowx += 1
        # drawing sum of loans
        for obj in range(2, 6):
            sheet.write_formula(row_foot, footer_col + obj, '{=SUM(' + xl_rowcol_to_cell(rowx_foot_sum, footer_col + obj)
                                + ':' + xl_rowcol_to_cell(row_foot - 1, footer_col + obj) + ')}', format_header_right)
        para = _('There are %s partners with %s active contracts') % (len(lists.mapped('partner_id')), len(lists))
        sheet.merge_range(row_foot + 1, footer_col + 2, rowx - 1, footer_col + 5, para, format_header_center)
        footer_col += 6
        # drawing count of overdue days
        for obj in range(0, 5):
            sheet.write_formula(row_foot, footer_col + obj,
                                '{=COUNT(' + xl_rowcol_to_cell(rowx_foot_sum, footer_col + obj)
                                + ':' + xl_rowcol_to_cell(row_foot - 1, footer_col + obj) + ')}', format_header_right)
        sheet.write(row_foot + 1, footer_col, normal_total_balance, format_header_right)
        sheet.write(row_foot + 1, footer_col + 1, att_total_balance, format_header_right)
        sheet.write(row_foot + 1, footer_col + 2, not_total_balance, format_header_right)
        sheet.write(row_foot + 1, footer_col + 3, doubt_total_balance, format_header_right)
        sheet.write(row_foot + 1, footer_col + 4, bad_total_balance, format_header_right)
        sheet.write_formula(row_foot + 2, footer_col, '{=%s/%s*100}' % (xl_rowcol_to_cell(row_foot + 1, footer_col),
                                                                        xl_rowcol_to_cell(row_foot, footer_col - 4)), format_header_right)
        sheet.write_formula(row_foot + 2, footer_col + 1, '{=%s/%s*100}' % (xl_rowcol_to_cell(row_foot + 1, footer_col + 1),
                                                                            xl_rowcol_to_cell(row_foot, footer_col - 4)), format_header_right)
        sheet.write_formula(row_foot + 2, footer_col + 2, '{=%s/%s*100}' % (xl_rowcol_to_cell(row_foot + 1, footer_col + 2),
                                                                            xl_rowcol_to_cell(row_foot, footer_col - 4)), format_header_right)
        sheet.write_formula(row_foot + 2, footer_col + 3, '{=%s/%s*100}' % (xl_rowcol_to_cell(row_foot + 1, footer_col + 3),
                                                                            xl_rowcol_to_cell(row_foot, footer_col - 4)), format_header_right)
        sheet.write_formula(row_foot + 2, footer_col + 4, '{=%s/%s*100}' % (xl_rowcol_to_cell(row_foot + 1, footer_col + 4),
                                                                            xl_rowcol_to_cell(row_foot, footer_col - 4)), format_header_right)
        sheet.write(row_foot + 3, footer_col, normal_total_acc_interest, format_header_right)
        sheet.write(row_foot + 3, footer_col + 1, att_total_acc_interest, format_header_right)
        sheet.write(row_foot + 3, footer_col + 2, not_total_acc_interest, format_header_right)
        sheet.write(row_foot + 3, footer_col + 3, doubt_total_acc_interest, format_header_right)
        sheet.write(row_foot + 3, footer_col + 4, bad_total_acc_interest, format_header_right)
        sheet.write_formula(row_foot + 4, footer_col, '{=%s/%s*100}' % (xl_rowcol_to_cell(row_foot + 3, footer_col),
                                                                        xl_rowcol_to_cell(row_foot, footer_col - 3)), format_header_right)
        sheet.write_formula(row_foot + 4, footer_col + 1, '{=%s/%s*100}' % (xl_rowcol_to_cell(row_foot + 3, footer_col + 1),
                                                                            xl_rowcol_to_cell(row_foot, footer_col - 3)), format_header_right)
        sheet.write_formula(row_foot + 4, footer_col + 2, '{=%s/%s*100}' % (xl_rowcol_to_cell(row_foot + 3, footer_col + 2),
                                                                            xl_rowcol_to_cell(row_foot, footer_col - 3)), format_header_right)
        sheet.write_formula(row_foot + 4, footer_col + 3, '{=%s/%s*100}' % (xl_rowcol_to_cell(row_foot + 3, footer_col + 3),
                                                                            xl_rowcol_to_cell(row_foot, footer_col - 3)), format_header_right)
        sheet.write_formula(row_foot + 4, footer_col + 4, '{=%s/%s*100}' % (xl_rowcol_to_cell(row_foot + 3, footer_col + 4),
                                                                            xl_rowcol_to_cell(row_foot, footer_col - 3)), format_header_right)
        sheet.write(row_foot + 5, footer_col, normal_total_added_interest, format_header_right)
        sheet.write(row_foot + 5, footer_col + 1, att_total_added_interest, format_header_right)
        sheet.write(row_foot + 5, footer_col + 2, not_total_added_interest, format_header_right)
        sheet.write(row_foot + 5, footer_col + 3, doubt_total_added_interest, format_header_right)
        sheet.write(row_foot + 5, footer_col + 4, bad_total_added_interest, format_header_right)
        sheet.write_formula(row_foot + 6, footer_col, '{=%s/%s*100}' % (xl_rowcol_to_cell(row_foot + 5, footer_col),
                                                                        xl_rowcol_to_cell(row_foot, footer_col - 2)), format_header_right)
        sheet.write_formula(row_foot + 6, footer_col + 1, '{=%s/%s*100}' % (xl_rowcol_to_cell(row_foot + 5, footer_col + 1),
                                                                            xl_rowcol_to_cell(row_foot, footer_col - 2)), format_header_right)
        sheet.write_formula(row_foot + 6, footer_col + 2, '{=%s/%s*100}' % (xl_rowcol_to_cell(row_foot + 5, footer_col + 2),
                                                                            xl_rowcol_to_cell(row_foot, footer_col - 2)), format_header_right)
        sheet.write_formula(row_foot + 6, footer_col + 3, '{=%s/%s*100}' % (xl_rowcol_to_cell(row_foot + 5, footer_col + 3),
                                                                            xl_rowcol_to_cell(row_foot, footer_col - 2)), format_header_right)
        sheet.write_formula(row_foot + 6, footer_col + 4, '{=%s/%s*100}' % (xl_rowcol_to_cell(row_foot + 5, footer_col + 4),
                                                                            xl_rowcol_to_cell(row_foot, footer_col - 2)), format_header_right)
        sheet.write(row_foot + 7, footer_col, normal_total_amount, format_header_right)
        sheet.write(row_foot + 7, footer_col + 1, att_total_amount, format_header_right)
        sheet.write(row_foot + 7, footer_col + 2, not_total_amount, format_header_right)
        sheet.write(row_foot + 7, footer_col + 3, doubt_total_amount, format_header_right)
        sheet.write(row_foot + 7, footer_col + 4, bad_total_amount, format_header_right)
        sheet.write_formula(row_foot + 8, footer_col, '{=%s/%s*100}' % (xl_rowcol_to_cell(row_foot + 7, footer_col),
                                                                        xl_rowcol_to_cell(row_foot, footer_col - 1)), format_header_right)
        sheet.write_formula(row_foot + 8, footer_col + 1, '{=%s/%s*100}' % (xl_rowcol_to_cell(row_foot + 7, footer_col + 1),
                                                                            xl_rowcol_to_cell(row_foot, footer_col - 1)), format_header_right)
        sheet.write_formula(row_foot + 8, footer_col + 2, '{=%s/%s*100}' % (xl_rowcol_to_cell(row_foot + 7, footer_col + 2),
                                                                            xl_rowcol_to_cell(row_foot, footer_col - 1)), format_header_right)
        sheet.write_formula(row_foot + 8, footer_col + 3, '{=%s/%s*100}' % (xl_rowcol_to_cell(row_foot + 7, footer_col + 3),
                                                                            xl_rowcol_to_cell(row_foot, footer_col - 1)), format_header_right)
        sheet.write_formula(row_foot + 8, footer_col + 4, '{=%s/%s*100}' % (xl_rowcol_to_cell(row_foot + 7, footer_col + 4),
                                                                            xl_rowcol_to_cell(row_foot, footer_col - 1)), format_header_right)
        for obj in range(0, 9):
            sheet.write(row_foot + obj, footer_col + 5, '', format_header_right)
        sheet.merge_range(row_foot, footer_col + 6, row_foot + 8, footer_col + 6, '', format_header_right)
        book.close()
        out = base64.encodestring(output.getvalue())
        excel_id = self.env['oderp.report.excel.output'].with_context(filename_prefix=report_name).create(
            {'filedata': out})
        return excel_id.export_report()
