# -*- encoding: utf-8 -*-
##############################################################################
from odoo import api, models, _

class AccountEquityChangeReport(models.Model):
    _inherit = "account.equity.change.report"

    @api.multi
    def get_header(self, sheet, rowx, format_title):
        # Тайлангийн хүснэгтийн толгой зурах
        sheet.set_row(rowx, 40)
        sheet.write(rowx, 0, _('№'), format_title)
        sheet.write(rowx, 1, _('Indication'), format_title)
        sheet.write(rowx, 2, _('Treasury Stock'), format_title)
        sheet.write(rowx, 3, _('Additional Paid-in Capital'), format_title)
        sheet.write(rowx, 4, _('Revaluation Addition'), format_title)
        sheet.write(rowx, 5, _('Foreign Currency Conversion Inventory'), format_title)
        sheet.write(rowx, 6, _('Retained Earnings'), format_title)
        sheet.write(rowx, 7, _('Total Amount'), format_title)
        return sheet

    @api.multi
    def get_value(self, sheet, rowx, line_number, format_number,format_text, format_float, line):
        # Тайлангийн мөр зурах
        
        sheet.write(rowx, 0, line_number, format_number)
        sheet.write(rowx, 1, line.name, format_text)
        sheet.write(rowx, 2, line.treasury_stock_amount, format_float)
        sheet.write(rowx, 3, line.additional_capital_amount, format_float)
        sheet.write(rowx, 4, line.revaluation_addition_amount, format_float)
        sheet.write(rowx, 5, line.foreign_currency_conversion_amount, format_float)
        sheet.write(rowx, 6, line.accumulated_profit_amount, format_float)
        sheet.write(rowx, 7, line.total_amount, format_float)
        rowx += 1
        return sheet, rowx