# -*- coding: utf-8 -*-

from io import BytesIO
import base64
import time
import xlsxwriter

from odoo import api, fields, models, _
import pytz
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class ClosedLoanReport(models.TransientModel):
    _name = 'closed.loan.report'
    _description = 'Closed Loan Report'
    
    date_from = fields.Date(string='Date from', default=lambda *a: time.strftime('%Y-%m-01'))
    date_to = fields.Date(string='Date to', default=lambda *a: time.strftime('%Y-%m-%d'))
    partner_id = fields.Many2one('res.partner', string='Partner')
    product_id = fields.Many2one('product.product.loan', string='Product')    
    economist_id = fields.Many2one('hr.employee', string='Economist')    
    loan_product_type = fields.Selection([
                ('loan_product', 'Loan product'),
                ('interest_payment', 'Interest payment'),
                ('accumulative_interest_payment', 'Accumlated interest payment'),
                ('undue_loss', 'Surcharge payment'),
                ('xxz_product', 'Past due loans'),
                ('xb_product', 'Abnormal loans'),
                ('ez_product', 'Doubted loans'),
                ('mz_product', 'Bad loans'), ], 'Loan product type')

    @api.multi
    def export_report(self):
        report_obj = self
        user = self.env['res.users'].search([('id', '=', self._uid)])
        company = user.company_id
        from_date = report_obj.date_from
        to_date = report_obj.date_to
        partner = report_obj.partner_id
        product = report_obj.product_id
        economist = report_obj.economist_id
        loan_product_type = report_obj.loan_product_type

        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Closed Loan Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        # Custom for standart
        format_name = {
            'font_name': 'Times New Roman',
            'font_size': 14,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter'
        }
        # create formats
        format_content_text_footer = {
        'font_name': 'Times New Roman',
        'font_size': 11,
        'align': 'vcenter',
        'valign': 'vcenter',
        }
        format_content_right = {
        'font_name': 'Times New Roman',
        'font_size': 11,
        'align': 'right',
        'valign': 'vcenter',
        'border': 1,
        'num_format': '#,##0.00'
        }
        format_content_left = {
        'font_name': 'Times New Roman',
        'font_size': 11,
        'align': 'left',
        'valign': 'vcenter',
        'border': 1,
        'num_format': '#,##0.00'
        }
        format_group_center_bold = {
        'font_name': 'Times New Roman',
        'font_size': 11,
        'bold': True,
        'align': 'center',
        'valign': 'vcenter',
        'border': 1,
        }
        format_content_right_bold = {
        'font_name': 'Times New Roman',
        'font_size': 11,
        'bold': True,
        'align': 'right',
        'valign': 'vcenter',
        'border': 1,
        'num_format': '#,##0.00'
        }
        format_group_center = {
        'font_name': 'Times New Roman',
        'font_size': 11,
        'align': 'center',
        'valign': 'vcenter',
        'border': 1,
        'num_format': '#,##0.00'
        }
        format_group_center = book.add_format(format_group_center)
        format_name = book.add_format(format_name)
        format_content_text_footer = book.add_format(format_content_text_footer)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_right = book.add_format(format_content_right)
        format_content_left = book.add_format(format_content_left)
        format_content_right_bold = book.add_format(format_content_right_bold)
        format_group_center_bold = book.add_format(format_group_center_bold)  
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('closed_loan_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 0

        # compute column
        colx_number = 3
        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 15)
        sheet.set_column('C:C', 20)
        sheet.set_column('D:D', 20)
        sheet.set_column('E:E', 20)
        sheet.set_column('F:F', 20)
        sheet.set_column('G:G', 10)
        sheet.set_column('H:H', 15)
        sheet.set_column('I:I', 20)
        sheet.set_column('J:J', 20)
        sheet.set_column('K:K', 20)
        sheet.set_column('L:L', 15)
        sheet.set_column('M:M', 15)

        # create name
        sheet.write(rowx, 0, _('Closed Loan Report'), format_content_text_footer)
        sheet.write(rowx, 12, '%s' % (company.name), format_content_text_footer)
        rowx += 2
        sheet.merge_range(rowx, 1, rowx, 12, report_name.upper(), format_name)
        rowx += 2
        # report duration
        sheet.write(rowx + 1, 10, '%s: %s - %s' % (_('Duration'), from_date, to_date), format_content_text_footer)
        rowx += 2

        sheet.merge_range(rowx, 0, rowx + 1, 0, _(u'Д/д'), format_title) 
        sheet.merge_range(rowx, 1, rowx + 1, 1, _(u'Гэрээний дугаар'), format_title)
        sheet.merge_range(rowx, 2, rowx, 4, _(u'Зээлдэгч'), format_title) 
        sheet.write(rowx + 1, 2, _(u'Харилцагчийн дугаар'), format_title)
        sheet.write(rowx + 1, 3, _(u'Овог'), format_title)
        sheet.write(rowx + 1, 4, _(u'Нэр'), format_title)
        sheet.merge_range(rowx, 5, rowx + 1, 5, _(u'Олгосон дүн'), format_title)
        sheet.merge_range(rowx, 6, rowx + 1, 6, _(u'Хүү % (сараар)'), format_title)
        sheet.merge_range(rowx, 7, rowx, 10, _(u'Төлөгдсөн'), format_title)
        sheet.write(rowx + 1, 7, _(u'Огноо'), format_title)
        sheet.write(rowx + 1, 8, _(u'Үндсэн зээл'), format_title)
        sheet.write(rowx + 1, 9, _(u'Хүү'), format_title)
        sheet.write(rowx + 1, 10, _(u'Дүн'), format_title)        
        sheet.merge_range(rowx, 11, rowx + 1, 11, _(u'Ангилал'), format_title)
        sheet.merge_range(rowx, 12, rowx + 1, 12, _(u'Төлөв'), format_title)
        rowx += 2

        total_loan_payment = total_interest_payment = payment_total = 0
        where = ' '
        if partner:
            where += ' AND rp.id = ' + str(partner.id) + ' '
        if product:
            where += ' AND pp.id = ' + str(product.id) + ' '
        if economist:
            where += ' AND he.id = ' + str(economist.id) + ' '
        if loan_product_type:
            where += " AND pp.loan_product_type = '" + str(loan_product_type) +"'"
 
        query = """
            SELECT lo.id AS order_id, pp.id AS product_id, lo.name AS name, lo.amount AS loan_amount, lo.interest_by_month AS interest,
                     lc.name AS loan_state, lo.date_order AS Date, 
                    rp.name AS partner_name, rp.surname AS partner_surname,
                    rp.certificate_number AS certificate_number, 
                        SUM(COALESCE(ls.loan_payment,0)) AS loan_payment,
                        SUM(COALESCE(ls.interest_payment,0)) AS interest_payment, 
                        SUM(COALESCE(ls.total_payment,0)) AS total_payment 
                    FROM loan_order lo 
                    LEFT JOIN loan_schedule ls ON (lo.id = ls.contract_id) 
                    LEFT JOIN res_partner rp ON (lo.partner_id = rp.id) 
                    LEFT JOIN product_product_loan pp ON(lo.product_id = pp.id)
                    LEFT JOIN hr_employee he ON(lo.current_economist_id = he.id)
                    LEFT JOIN loan_category_change_type lc ON(lo.loan_state = lc.id)
                    WHERE lo.date_order >= '%s' and lo.date_order <= '%s' and lo.state = 'done_order' %s 
                    GROUP BY pp.id, lo.id,product_id, lo.name, rp.name, rp.surname, rp.certificate_number,pp.name, lc.name      
                   ORDER BY pp.name
        """ %(from_date, to_date,where)
 
        self._cr.execute(query)
         
        lines = self._cr.dictfetchall()
        product_ids = []
        number = 0
        counter = 1
        if lines:
            for line in lines:
                if line['product_id'] not in product_ids:
                   product_ids.append(line['product_id'])
            for product_id in product_ids:
                sub_loan_payment = sub_interest_payment = sub_total_payment = 0
                for product in self.env['product.product.loan'].search([('id', '=', product_id)]):
                    sheet.merge_range(rowx, 0, rowx, 1,  _(u'Бүтээгдэхүүний нэр:'), format_content_right)
                    sheet.merge_range(rowx, 2, rowx, 12, product.name, format_content_left)
                    rowx += 1
                    
                    for line in lines:
                        loan_code = line['name']
                        certificate_number = line['certificate_number']
                        partner_surname = line['partner_surname']
                        partner_name = line['partner_name']
                        loan_amount = line['loan_amount']
                        interest = line['interest']
                        date = line['date']
                        loan_payment = line['loan_payment']
                        interest_payment = line['interest_payment']
                        total_payment = line['total_payment']
                        loan_state = line['loan_state']
                        if product.id == line['product_id']:
                            number += 1
                            sheet.write(rowx, 0, counter, format_content_number)
                            sheet.write(rowx, 1, loan_code, format_content_left)
                            sheet.write(rowx, 2, certificate_number, format_content_left)
                            sheet.write(rowx, 3, partner_surname, format_content_left)
                            sheet.write(rowx, 4, partner_name, format_content_left)
                            sheet.write(rowx, 5, loan_amount, format_group_center)
                            sheet.write(rowx, 6, interest, format_group_center)
                            sheet.write(rowx, 7, date, format_group_center)
                            sheet.write(rowx, 8, loan_payment, format_content_right)
                            sheet.write(rowx, 9, interest_payment, format_content_right)
                            sheet.write(rowx, 10, total_payment, format_content_right)
                            sheet.write(rowx, 11, loan_state, format_group_center)
                            sheet.write(rowx, 12, _(u'Хаагдсан'), format_group_center)
                    
                            sub_loan_payment += loan_payment
                            sub_interest_payment += interest_payment
                            sub_total_payment += total_payment
                    
                            total_loan_payment += loan_payment
                            total_interest_payment += interest_payment
                            payment_total += total_payment
                            counter += 1
                            rowx += 1
                             
                        product_id = line['product_id']
                    counter = 1
                    sheet.merge_range(rowx, 0, rowx, 7, _(u'Бүтээгдэхүүний дүн:'), format_content_right_bold)
                    sheet.write(rowx, 8, sub_loan_payment, format_content_right_bold)
                    sheet.write(rowx, 9, sub_interest_payment, format_content_right_bold)
                    sheet.write(rowx, 10, sub_total_payment, format_content_right_bold)
                    sheet.merge_range(rowx, 11, rowx, 12, '', format_content_right_bold)
                    rowx += 1
        sheet.merge_range(rowx, 0, rowx, 5, _(u'НИЙТ ТОО'), format_content_right_bold)
        sheet.write(rowx, 6, number, format_group_center_bold)
        sheet.write(rowx, 7, _(u'НИЙТ ДҮН'), format_content_right_bold)
        sheet.write(rowx, 8, total_loan_payment, format_content_right_bold)
        sheet.write(rowx, 9, total_interest_payment, format_content_right_bold)
        sheet.write(rowx, 10, payment_total, format_content_right_bold)
        sheet.merge_range(rowx, 11, rowx, 12, '', format_content_right_bold)
        rowx += 3
        
        sheet.merge_range(rowx, 5, rowx, 8, _(u'Тайлан гаргасан:') + '.....................................' + '/' + self.env.user.name 
                          + '/', format_content_text_footer)
        
        sheet.set_zoom(90)
        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report()
