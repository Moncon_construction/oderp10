# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError

import time


class CreateLoanAccruedInterest(models.TransientModel):
    _name = 'create.loan.accrued.interest'

    debtor_id = fields.Many2one('res.partner', 'Debtor')
    loan_ids = fields.Many2many('loan.order', string='Loan Order')
    date_start = fields.Date('Date start', default=lambda *a: time.strftime('%Y-%m-01'))
    date_stop = fields.Date('Date stop', default=lambda *a: time.strftime('%Y-%m-%d'))

    # Нэг харилцагч сонгож зээлийн
    # тухайн мөчлөгт эргэн төлөлтийн
    # хүүгийн хуримтлал үүсгэх функц
    @api.multi
    def create_accrued_interest(self):
        loan_order_obj = self.env['res.partner']
        for obj in self:
            if obj.loan_ids:
                date_start = obj.date_start
                date_stop = obj.date_stop
                for loan_id in obj.loan_ids:
                    for schedule_id in loan_id.schedules_ids:
                        if date_start <= schedule_id.payment_date <= date_stop:
                            schedule_id.compute_accrued_interest()
                            schedule_id.create_accrued_interest_account_move()
