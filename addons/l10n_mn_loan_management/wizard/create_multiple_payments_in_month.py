# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError

from datetime import datetime, date
from dateutil.relativedelta import relativedelta
from calendar import monthrange


class LoanScheduleMultiplePayment(models.TransientModel):
    _name = 'loan.schedule.multiple.payment'

    contract_id = fields.Many2one('loan.order', 'Loan Order')
    schedule_multiple_payment_line_ids = fields.One2many('loan.schedule.multiple.payment.line',
                                                         'schedule_multpile_payment_id', 'Loan Order')
    schedule_payment_start_date = fields.Date('Schedule payment start date', default=datetime.now())

    @api.model
    def default_get(self, fields):
        res = super(LoanScheduleMultiplePayment, self).default_get(fields)
        active_id = self.env.context.get('active_id')
        loan_id = self.env['loan.order'].browse(active_id)
        if 'contract_id' in self._fields:
            res['contract_id'] = loan_id.id
            lines = []
            i = 1
            while i in range(32):
                lines.append((0, 0, {
                    'day': i,
                    'decision': False,
                    'schedule_multpile_payment_id': self.id,
                }))
                i += 1
            res['schedule_multiple_payment_line_ids'] = lines
        return res

    # Төлөлтийн хоног сонгож төлөлтийн хуваарь бодох функц
    @api.multi
    def import_schedule(self):
        def diff_dates(date2, date1):
            return abs(date2 - date1).days

        for obj in self:
            # Үндсэн зээлийн мэдээлэл
            principal = obj.contract_id.amount
            # Зээлийн сар
            loan_term = int(obj.contract_id.period)
            # Зээлийн хүү жилээр
            interest_rate_year = obj.contract_id.interest_by_year
            # Зээлийн хүү хувь-р
            interest_rate = interest_rate_year / 100
            interest_rate_month = interest_rate_year / 100
            # Зээл олгох огноо
            loan_start_date = obj.schedule_payment_start_date
            year, month, day = map(int, loan_start_date.split('-'))
            # FIX ME: Тухайн жилийн зээлийн эргэн төлөлтийн хүүг
            # өндөр жил эсэхээс хамаарч 366 хоногоор бодох эсэх
            # монголбанкны шийдвэр өөрчлөгдөх эсэхээс хамаарч ашиглах эсэх
            # year_day_range = get_year_day_range(year)
            # Монголбанкны томёогоор үргэлж 365 хоногоор бодох
            year_day_range = 365
            i = 1
            # Визард цонхноос нийт төлөлтийн хоногууд авах
            multiple_days = []
            for line in obj.schedule_multiple_payment_line_ids:
                if line.decision:
                    multiple_days.append(line.day)

            year, month, day = map(int, loan_start_date.split('-'))
            date_after_period = date(year, month, day) + relativedelta(months=+loan_term)
            loan_start_date = date(year, month, day)
            total_days_of_period = diff_dates(date_after_period, loan_start_date)
            # Төлөлт хийхэд сонгогдсон өдрүүдээр огноо үүсгэх
            loan_start_date = obj.schedule_payment_start_date
            multiple_days_calc = []
            total_days_of_scheduled = 0
            previous_date = loan_start_date
            year, month, day = map(int, previous_date.split('-'))
            previous_date = date(year, month, day)
            while total_days_of_scheduled <= total_days_of_period:
                if month < 13:
                    for multiple_day in multiple_days:
                        day_of_month = monthrange(year, month)[1]
                        if day_of_month >= multiple_day:
                            schedule_date = datetime(year, month, multiple_day).strftime('%Y-%m-%d')
                            if schedule_date == loan_start_date:
                                i -= 1
                            elif schedule_date > loan_start_date:
                                year, month, day = map(int, schedule_date.split('-'))
                                schedule_date = date(year, month, day)
                                if previous_date < schedule_date:
                                    diff = diff_dates(schedule_date, previous_date)
                                    total_days_of_scheduled += diff
                                    if total_days_of_scheduled < total_days_of_period:
                                        multiple_days_calc.append({'prior': i,
                                                                   'date': schedule_date.strftime('%Y-%m-%d')})
                                    elif total_days_of_scheduled > total_days_of_period:
                                        total_diff = total_days_of_scheduled - total_days_of_period
                                        schedule_date = schedule_date + relativedelta(days=-total_diff)
                                        multiple_days_calc.append({'prior': i,
                                                                   'date': schedule_date.strftime('%Y-%m-%d')})
                                previous_date = schedule_date
                        elif day_of_month < multiple_day:
                            schedule_date = datetime(year, month, day_of_month).strftime('%Y-%m-%d')
                            if schedule_date == loan_start_date:
                                i -= 1
                            elif schedule_date > loan_start_date:
                                day_of_month = monthrange(year, month)[1]
                                if day_of_month < day:
                                    day = day_of_month
                                schedule_date = date(year, month, day)
                                if previous_date < schedule_date:
                                    diff = diff_dates(schedule_date, previous_date)
                                    total_days_of_scheduled += diff
                                    if total_days_of_scheduled < total_days_of_period:
                                        multiple_days_calc.append({'prior': i,
                                                                   'date': schedule_date.strftime('%Y-%m-%d')})
                                    elif total_days_of_scheduled > total_days_of_period:
                                        total_diff = total_days_of_scheduled - total_days_of_period
                                        schedule_date = schedule_date + relativedelta(days=-total_diff)
                                        multiple_days_calc.append({'prior': i,
                                                                   'date': schedule_date.strftime('%Y-%m-%d')})
                                previous_date = schedule_date
                    i += 1
                    month += 1
                else:
                    year += 1
                    month = 1

            # Эргэн төлөлтийн итгэлцүүрийг төлөлийн хоногуудаар бодох
            year, month, day = map(int, loan_start_date.split('-'))
            start_date = date(year, month, day)
            previous_date = start_date
            sum_diff = 0
            # Монголбанкнаас гаргасан - Зээлийн итгэлцүүр тооцооллын томёо
            coefficient = 1
            sum_coefficient = 0
            for multiple_day in multiple_days_calc:
                year, month, day = map(int, multiple_day['date'].split('-'))
                multiple_date = date(year, month, day)
                if previous_date <= multiple_date:
                    # Өмнөх төлөлтөөс дараагийн төлөх хүртэлх зөрүү хоног
                    diff = diff_dates(multiple_date, previous_date)
                    coefficient = coefficient / (1 + diff * interest_rate / year_day_range)
                    # Нийт итгэлцүүрийн нийлбэр
                    sum_coefficient += coefficient
                    sum_diff += diff
                    previous_date = multiple_date
            sum_coefficient = sum_coefficient

            # Зээлийн эргэн төлөлт - Тэнцүү төлөлтийн аргаар сар бүрт тооцоолол
            monthly_payment = principal / sum_coefficient
            monthly_payment = monthly_payment
            # Тогмол төлбөрийн дүн талбарт утга оноох
            obj.contract_id.fixed_payment = monthly_payment
            # Зээлийн эргэн төлөлтийн хуваарийг сард олон хоног төлөлттэй байдлаар үүсгэж эхэлж байна
            year, month, day = map(int, loan_start_date.split('-'))
            start_date = date(year, month, day)
            previous_date = start_date
            sum_diff = 0
            # Сард төлөх үндсэн зээлийн төлбөрийн нийт дүнгийн нийлбэр
            sum_payment_of_monthly_principal = 0
            # Сард төлөх зээлийн хүүгийн нийт дүнгийн нийлбэр
            sum_payment_of_monthly_interest = 0
            for multiple_day in multiple_days_calc:
                year, month, day = map(int, multiple_day['date'].split('-'))
                multiple_date = date(year, month, day)
                if previous_date <= multiple_date:
                    day_of_month = diff_dates(multiple_date, previous_date)
                    # Нийт итгэлцүүрийн нийлбэр
                    sum_diff += day_of_month
                    previous_date = multiple_date
                    # Тухайн сард төлөх зээлийн хүү тооцоолол
                    payment_of_monthly_interest = principal * (day_of_month * interest_rate_month / year_day_range)
                    payment_of_monthly_interest = payment_of_monthly_interest
                    # Тухайн сарл төлөх үндсэн зээлийн төлөлт тооцоолол
                    payment_of_monthly_principal = monthly_payment - payment_of_monthly_interest
                    payment_of_monthly_principal = payment_of_monthly_principal
                    # Нийт төлөлтийн хуваарь үүссэн төлөлт, хүүгийн нийлбэр бодолт
                    sum_payment_of_monthly_interest += payment_of_monthly_interest
                    sum_payment_of_monthly_principal += payment_of_monthly_principal
                    # Нийт төлсөн үндсэн зээлийн төлбөрийн нийлбэр үндсэн зээлээс их болсон үед
                    # зөрүүг үндсэн төлөлтөөс хасч тухайн сарын хүүнд нэмж бодох
                    # Тухайн сарын зээлийн хуваарь үүсгэх
                    obj.contract_id.create_schedule_from_loan_order(i, obj.contract_id.id, payment_of_monthly_principal,
                                                                    payment_of_monthly_interest, principal,
                                                                    monthly_payment,
                                                                    multiple_date,
                                                                    day_of_month)
                    # Дараа сарын нийт зээлийн дүн гаргах
                    principal = principal - payment_of_monthly_principal


class LoanScheduleMultiplePaymentLine(models.TransientModel):
    _name = 'loan.schedule.multiple.payment.line'

    schedule_multpile_payment_id = fields.Many2one('loan.schedule.multiple.payment', 'Loan Schedule')
    day = fields.Integer('Day')
    decision = fields.Boolean('Has monthly multiple payment')
