# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class CreateRefundLoanAccruedInterest(models.TransientModel):
    _name = 'create.refund.loan.accrued.interest'

    import_loan = fields.Char('Name')
    refund_accrued_interest = fields.Float('Refund Accrued Interest')
    loan_ids = fields.Many2many('loan.order', string='Loan order')
    loan_schedule_ids = fields.Many2many('loan.schedule', string='Loan Schedule')

    # Сонгогдсон зээлийн гэрээний эргэн төлөлтийн хуваариас төлөгдөөгүйг татах
    @api.multi
    def get_schedules(self):
        for obj in self:
            for loan_id in obj.loan_ids:
                if loan_id.schedules_ids:
                    unpaid_schedule_ids = loan_id.schedules_ids.filtered(lambda line: line.accrued_interest)
                    obj.update({'loan_schedule_ids': [(6, 0, unpaid_schedule_ids.ids)]})
        return {
            "type": "ir.actions.do_nothing",
        }

    # Зээлийн гэрээний эргэн төлөлтөөс хуримтлагдсан хүүгийн буцаалт хийх функц
    @api.multi
    def create_refund_accrued_interest(self):
        for obj in self:
            for schedule_id in obj.loan_schedule_ids:
                if schedule_id.do_refund_accrued_interest:
                    schedule_id.create_accrued_interest_account_move(is_refund=True,
                                                                     refund_interest=schedule_id.do_refund_accrued_interest)
