# -*- coding: utf-8 -*-

from io import BytesIO
import base64
import time
from datetime import datetime
import xlsxwriter

from odoo import api, fields, models, _
import pytz
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class LoanCollateralReport(models.TransientModel):
    _name = 'loan.collateral.report'
    _description = 'Loan Collateral Report'
    
    company_id = fields.Many2one('res.company', string=u'Компани', readonly=True, default=lambda self: self.env.user.company_id, required=True)
    date_to = fields.Date(u'Дуусах огноо', default=lambda *a: time.strftime('%Y-%m-%d'))
    date_from = fields.Date(u'Эхлэх огноо', default=lambda *a: time.strftime('%Y-%m-01'))
    partner_id = fields.Many2many('res.partner', string='Debtor')
    coll_type = fields.Selection([('type1', 'Type1'),  # Автомашин
                                  ('type2', 'Type2'),  # Мөнгөн хадгаламж
                                  ('type3', 'Type3'),  # Арилжааны үнэт цаас
                                  ('type4', 'Type4'),  # Орон сууц
                                  ('type5', 'Type5'),  # Үнэт эдлэл
                                  ('type6', 'Type6'),  # Бусад үл хөдлөх хөрөнгө
                                  ('type7', 'Type7')], string='Collateral Type') # Бусад хөдлөх хөрөнгө

    def _solve_single_value(self, list):
        if len(list) == 0:
            list.append(-1)
        if len(list) < 2:
            list.append(-1)
        return list

    @api.multi
    def export_report(self):
    # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Collateral Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        # Custom for standart
        format_name = {
            'font_name': 'Times New Roman',
            'font_size': 14,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter'
        }
        # create formats
        format_content_text_footer = {
            'font_name': 'Times New Roman',
            'font_size': 10,
            'align': 'center',
            'valign': 'vcenter',
        }
        format_group_center = {
            'font_name': 'Times New Roman',
            'font_size': 10,
            'align': 'center',
            'valign': 'vcenter',
            'border': 1,
        }
        format_group_center = book.add_format(format_group_center)
        format_name = book.add_format(format_name)
        format_content_text_footer = book.add_format(format_content_text_footer)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_title_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)

        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('loan_collateral_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 0

        # compute column
        colx_number = 3
        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 15)
        sheet.set_column('C:C', 15)
        sheet.set_column('D:D', 15)
        sheet.set_column('E:E', 15)
        sheet.set_column('F:F', 15)
        sheet.set_column('G:G', 15)
        sheet.set_column('H:H', 15)
        sheet.set_column('I:I', 15)
        sheet.set_column('J:J', 15)
        sheet.set_column('K:K', 15)
        sheet.set_column('L:L', 15)
        sheet.set_column('M:M', 15)
        sheet.set_column('N:N', 15)
        sheet.set_column('O:O', 15)
        sheet.set_column('P:P', 15)
        sheet.set_column('Q:Q', 15)
        sheet.set_column('R:R', 15)
        sheet.set_row(3, 40)

        local = pytz.UTC
        if self.env.user.tz:
            local = pytz.timezone(self.env.user.tz)
        # rowx += 1
        sheet.merge_range(rowx, 1, rowx, 16, report_name, format_name)

        rowx += 1
        # create name
        sheet.merge_range(rowx, 14, rowx, 16, '%s' % (self.company_id.name), format_name)
        # create date
        sheet.merge_range(rowx, 0, rowx, 13, _(u'Duration: %s - %s') % (pytz.utc.localize(datetime.strptime(self.date_from, '%Y-%m-%d')).astimezone(local).strftime('%Y-%m-%d'),
                                                                        pytz.utc.localize(datetime.strptime(self.date_to, '%Y-%m-%d')).astimezone(local).strftime('%Y-%m-%d')), format_filter)

        rowx += 2

        sheet.write(rowx, 0, _('№'), format_title)
        sheet.write(rowx, 1, _('Loan number'), format_title)
        sheet.write(rowx, 2, _('Debtor'), format_title)
        sheet.write(rowx, 3, _('Reg Number'), format_title)
        sheet.write(rowx, 4, _('Cer Number'), format_title)
        sheet.write(rowx, 5, _('Collateral Type'), format_title)
        sheet.write(rowx, 6, _('Collateral name'), format_title)
        sheet.write(rowx, 7, _('Is trustee'), format_title)
        sheet.write(rowx, 8, _('Start Date'), format_title)
        sheet.write(rowx, 9, _('End Date'), format_title)
        sheet.write(rowx, 10, _('Qty'), format_title)
        sheet.write(rowx, 11, _('Price Date'), format_title)
        sheet.write(rowx, 12, _('Price'), format_title)
        sheet.write(rowx, 13, _('Price Currency'), format_title)
        sheet.write(rowx, 14, _('Percent'), format_title)
        sheet.write(rowx, 15, _('Economist'), format_title)
        sheet.write(rowx, 16, _('Other'), format_title)
        rowx += 1

        where = ''
        if len(self.partner_id) > 0:
            partners = self.partner_id.ids
            where += ' AND l.partner_id in (' + ','.join(map(str, partners)) + ') '

        if self.coll_type:
            where += " AND c.coll_type = '%s' " % self.coll_type

        self._cr.execute("SELECT l.name as lname, p.name as pname , c.bvrtgel_no, c.state_reg_number, c.coll_type, c.name_type, c.count, c.price_date, "
                         "c.price, cur.symbol, c.percent, l.current_economist_id, l.loan_balance, c.remark , c.is_trustee, c.trustee_start_date, c.trustee_end_date "
                         "FROM loan_collateral c "
                         "LEFT JOIN loan_order l ON c.contract_id = l.id "
                         "LEFT JOIN res_partner p ON l.partner_id = p.id "
                         "LEFT JOIN res_currency cur ON c.currency = cur.id "
                         "WHERE l.date_order >= %s AND l.date_order <= %s AND l.company_id = %s " + where +
                         "ORDER BY l.date_order",  (self.date_from, self.date_to, self.company_id.id))
        lines = self._cr.dictfetchall()

        total_count = total_price = 0

        if lines:
            counter = 1
            for order in lines:
                if order['coll_type'] == 'type1':
                    coll_type = _('Type1')
                elif order['coll_type'] == 'type2':
                    coll_type = _('Type2')
                elif order['coll_type'] == 'type3':
                    coll_type = _('Type3')
                elif order['coll_type'] == 'type4':
                    coll_type = _('Type4')
                elif order['coll_type'] == 'type5':
                    coll_type = _('Type5')
                elif order['coll_type'] == 'type6':
                    coll_type = _('Type6')
                elif order['coll_type'] == 'type7':
                    coll_type = _('Type7')
                sheet.write(rowx, 0, counter, format_content_number)
                sheet.write(rowx, 1, order['lname'], format_content_text)
                sheet.write(rowx, 2, order['pname'], format_content_text)
                sheet.write(rowx, 3, order['bvrtgel_no'], format_group_center)
                sheet.write(rowx, 4, order['state_reg_number'], format_group_center)
                sheet.write(rowx, 5, coll_type, format_group_center)
                sheet.write(rowx, 6, order['name_type'], format_group_center)
                sheet.write(rowx, 7, _('Yes') if order['is_trustee'] else '', format_group_center)
                sheet.write(rowx, 8, order['trustee_start_date'], format_group_center)
                sheet.write(rowx, 9, order['trustee_end_date'], format_group_center)
                sheet.write(rowx, 10, order['count'], format_group_center)
                sheet.write(rowx, 11, order['price_date'], format_group_center)
                sheet.write(rowx, 12, order['price'], format_content_float)
                sheet.write(rowx, 13, order['symbol'], format_group_center)
                sheet.write(rowx, 14, order['percent'], format_group_center)
                sheet.write(rowx, 15, order['current_economist_id'], format_group_center)
                sheet.write(rowx, 16, order['remark'], format_group_center)
                rowx += 1
                counter += 1
                # total
                total_count += order['count'] if order['count'] else 0
                total_price += order['price'] if order['price'] else 0

        sheet.merge_range(rowx, 0, rowx, 9, _('Total'), format_title)
        sheet.write(rowx, 10, total_count, format_title)
        sheet.write(rowx, 11, '', format_title)
        sheet.write(rowx, 12, total_price, format_title_float)
        sheet.write(rowx, 13, '', format_title)
        sheet.write(rowx, 14, '', format_title)
        sheet.write(rowx, 15, '', format_title)
        sheet.write(rowx, 16, '', format_title)

        rowx += 3
        sheet.merge_range(rowx, 1, rowx, 16, _('Made by:') + '........................' + '/' + self.env.user.name
                          + '/', format_content_text_footer)

        sheet.set_zoom(100)
        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report()