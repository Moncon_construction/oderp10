# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError

from datetime import datetime, date
from dateutil.relativedelta import relativedelta
from calendar import monthrange


class LoanScheduleDiscount(models.TransientModel):
    _name = 'loan.schedule.discount'

    contract_id = fields.Many2one('loan.order', 'Loan Order')
    schedule_discount_line_ids = fields.One2many('loan.schedule.discount.line', 'schedule_discount_id', 'Loan Order')
    loan_period = fields.Integer('Loan period')
    total_discounting_months = fields.Integer('Total discount months')
    total_loan_months = fields.Integer('Total loan months')
    schedule_start_date = fields.Date('Schedule Start Date')

    @api.model
    def default_get(self, fields):
        res = super(LoanScheduleDiscount, self).default_get(fields)
        active_id = self.env.context.get('active_id')
        loan_id = self.env['loan.order'].browse(active_id)
        if 'contract_id' in self._fields:
            res['contract_id'] = loan_id.id
            lines = []
            # Зээлийн сар
            loan_term = int(loan_id.period)
            # Зээл олгох огноо
            loan_start_date = loan_id.installment_date
            date_after_month = loan_start_date
            i = 1
            while i <= loan_term:
                lines.append((0, 0, {
                    'date': date_after_month,
                    'decision': False,
                    'schedule_discount_id': self.id,
                }))
                year, month, day = map(int, date_after_month.split('-'))
                date_after_month = date(year, month, day) + relativedelta(months=+1)
                date_after_month = date_after_month.strftime('%Y-%m-%d')
                i += 1
            res['schedule_discount_line_ids'] = lines
            res['loan_period'] = loan_id.period
        return res

    @api.onchange('total_discounting_months')
    def onchange_total_discounting_months(self):
        for obj in self:
            if obj.total_discounting_months:
                obj.total_loan_months = obj.loan_period - obj.total_discounting_months

    @api.onchange('schedule_discount_line_ids')
    def onchange_schedule_discount_line(self):
        for obj in self:
            if obj.schedule_discount_line_ids:
                total_months = len(obj.schedule_discount_line_ids.filtered(lambda line: line.decision))
                if total_months > obj.total_discounting_months:
                    raise UserError(_('Discount month must be lesser than %s' % obj.total_discounting_months))

    def import_schedule(self):
        for obj in self:
            if obj.schedule_discount_line_ids:
                total_months = len(obj.schedule_discount_line_ids.filtered(lambda line: line.decision))
                if total_months > obj.total_discounting_months:
                    raise UserError(_('Discount month must be lesser than %s' % obj.total_discounting_months))
                elif total_months < obj.total_discounting_months:
                    raise UserError(_('Discount months must be equal to %s' % obj.total_discounting_months))
                elif total_months == 0:
                    raise UserError(_('Discount months cannot be empty !'))
            obj.contract_id.create_schedule_main_type(period=obj.loan_period - obj.total_discounting_months)
            has_discount_months_ids = obj.schedule_discount_line_ids.filtered(lambda line: line.decision)
            if has_discount_months_ids:
                for month_line_id in has_discount_months_ids:
                    discounting_schedule_ids = obj.contract_id.schedules_ids.filtered(
                        lambda line: line.payment_date >= month_line_id.date)
                    principal = 0
                    for schedule_id in discounting_schedule_ids:
                        year, month, day = map(int, schedule_id.payment_date.split('-'))
                        date_after_month = date(year, month, day) + relativedelta(months=+1)
                        if schedule_id.payment_date == month_line_id.date:
                            principal = schedule_id.balance
                        schedule_id.write({'payment_date': date_after_month})
                    # Хөнгөлсөн сарын хуваарийг зөвхөн тухайн хоногийн хүүг бодож үүсгэх
                    year, month, day = map(int, month_line_id.date.split('-'))
                    day_of_month = monthrange(year, month)[1]
                    payment_of_day_interest = obj.contract_id.amount * obj.contract_id.interest_by_year / 100 / 365 * day_of_month
                    obj.contract_id.create_schedule_from_loan_order(schedule_id.prior_id, obj.contract_id.id, 0,
                                                                    payment_of_day_interest,
                                                                    principal, payment_of_day_interest,
                                                                    month_line_id.date,
                                                                    day_of_month)
            # Эргэн төлөлтийн хуваарийн тухайн сарын хоногуудыг тооцоолох
            for schedule_id in obj.contract_id.schedules_ids:
                year, month, day = map(int, schedule_id.payment_date.split('-'))
                day_of_month = monthrange(year, month)[1]
                schedule_id.write({'days': day_of_month})


class LoanScheduleDiscountLine(models.TransientModel):
    _name = 'loan.schedule.discount.line'

    schedule_discount_id = fields.Many2one('loan.schedule.discount', 'Loan Schedule')
    date = fields.Date('Date')
    decision = fields.Boolean('Has monthly discount')
