# -*- coding: utf-8 -*-

from io import BytesIO
import base64
import time, calendar
from datetime import date, datetime, timedelta
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from operator import itemgetter

from odoo import api, fields, models, _

from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class LoanOrderStatement(models.TransientModel):
    _name = 'loan.order.statement'
    _description = 'Loan Order Statement'
    
    company_id = fields.Many2one('res.company', string=u'Компани', readonly=True,
        default=lambda self: self.env.user.company_id, required=True)
    product_id = fields.Many2one('product.product.loan', string=u'Зээлийн бүтээгдэхүүн', domain="[('loan_product_type','in',['loan_product'])]", required=True)
    date_to = fields.Date(u'Дуусах огноо', default=lambda *a: time.strftime('%Y-%m-%d'))
    date_from = fields.Date(u'Эхлэх огноо', default=lambda *a: time.strftime('%Y-%m-01'))
    
    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = (u'Зээл олголтын мэдээ')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        # Custom for standart
        format_name = {
            'font_name': 'Times New Roman',
            'font_size': 14,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter'
        }
        # create formats
        format_content_text_footer = {
        'font_name': 'Times New Roman',
        'font_size': 10,
        'align': 'vcenter',
        'valign': 'vcenter',
        }
        format_content_right = {
        'font_name': 'Times New Roman',
        'font_size': 9,
        'align': 'right',
        'valign': 'vcenter',
        'border': 1,
        'num_format': '#,##0.00'
        }
        format_group_center = {
        'font_name': 'Times New Roman',
        'font_size': 10,
        'align': 'center',
        'valign': 'vcenter',
        'border': 1,
        }
        format_group = {
        'font_name': 'Times New Roman',
        'font_size': 10,
        'bold': True,
        'align': 'center',
        'valign': 'vcenter',
        'border': 1,
        'bg_color': '#CFE7F5',
        'num_format': '#,##0.00'
        }
        format_title = {
        'font_name': 'Times New Roman',
        'font_size': 12,
        'bold': True,
        'align': 'center',
        'valign': 'vcenter',
        'border': 1,
        'text_wrap':1,
        'bg_color': '#f4ee42'
        }
        format_group_center = book.add_format(format_group_center)
        format_name = book.add_format(format_name)
        format_content_text_footer = book.add_format(format_content_text_footer)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(format_title)
        format_group_right = book.add_format(ReportExcelCellStyles.format_group_right)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_group_left = book.add_format(ReportExcelCellStyles.format_group_left)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_center = book.add_format(ReportExcelCellStyles.format_content_center)
        format_group = book.add_format(format_group)

        format_content_right = book.add_format(format_content_right)
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('loan_statement_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 0

        # compute column
        colx_number = 3
        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 15)
        sheet.set_column('C:C', 15)
        sheet.set_column('D:D', 15)
        sheet.set_column('E:E', 8)
        sheet.set_column('F:F', 15)
        sheet.set_column('G:G', 15)
        sheet.set_column('H:H', 15)
        sheet.set_column('I:I', 35)
        sheet.set_column('J:J', 15)

        # create name
        sheet.write(rowx, 0, '%s' % (self.company_id.name), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 1, rowx, 10, report_name.upper(), format_name)
        # create date
        sheet.write(rowx + 1, 0, '%s: %s' % (_(u'Үүсгэсэн: '), time.strftime('%Y-%m-%d')), format_filter)
        rowx += 2

        sheet.merge_range(rowx, 0, rowx + 1, 0, _(u'№'), format_title)  # seq
        sheet.merge_range(rowx, 1, rowx + 1, 1, _(u'Гэрээний дугаар'), format_title)  # Хөрөнгийн дугаар
        sheet.merge_range(rowx, 2, rowx, 3, _(u'Зээлдэгч'), format_title)  # Өртөг
        sheet.write(rowx + 1, 2, _(u'Код'), format_title)  # Худалдан aвалт
        sheet.write(rowx + 1, 3, _(u'Нэр'), format_title)  # Хөдөлгөөнөөр
        sheet.merge_range(rowx, 4, rowx + 1, 4, _(u'Хүү'), format_title)  # Хөрөнгийн дугаар
        sheet.merge_range(rowx, 5, rowx + 1, 5, _(u'Хугацаа'), format_title)  # Хөрөнгийн дугаар

        sheet.merge_range(rowx, 6, rowx, 7, _(u'Олгосон'), format_title)  #
        sheet.write(rowx + 1, 6, _(u'Огноо'), format_title)  # Хаагдсан
        sheet.write(rowx + 1, 7, _(u'Дүн'), format_title)  # Хөдөлгөөнөөр
        sheet.merge_range(rowx, 8, rowx + 1, 8, _(u'Зээлийн зориулалтын дугаар'), format_title)  # Эцсийн
        sheet.merge_range(rowx, 9, rowx + 1, 9, _(u'Зээлийн төлөлтийн төрөл'), format_title)  # Эцсийн
        rowx += 2
        self._cr.execute("SELECT * FROM loan_order WHERE '%s' <=date_order AND date_order <='%s' " % (self.date_from, self.date_to))
        lines = self._cr.dictfetchall()
        res_partner_obj = self.env['res.partner']
        if lines:
            categ_id = False
            counter = 1
            for line in lines:
                partner = res_partner_obj.search([('id', '=', line['partner_id'])])
                purpose = self.env['loan.purpose'].search([('id', '=', line['loan_purpose_id'])])
                sheet.write(rowx, 0, counter, format_content_number)
                sheet.write(rowx, 1, line['name'], format_content_text)
                sheet.write(rowx, 2, partner.code, format_content_text)
                sheet.write(rowx, 3, partner.name, format_group_center)
                sheet.write(rowx, 4, line['interest_by_month'], format_group_center)
                sheet.write(rowx, 5, line['period'], format_group_center)
                sheet.write(rowx, 6, line['date_approve'], format_content_right)
                sheet.write(rowx, 7, line['loan_balance'], format_group_center)
                sheet.write(rowx, 8, purpose.name, format_content_right)
                sheet.write(rowx, 9, line['payment_type'], format_content_right)
                rowx += 1
        rowx += 3
        sheet.merge_range(rowx, 1, rowx, 4, _(u'Тайлан бэлтгэсэн:') + '........................' + '/' + self.env.user.name 
                          + '/', format_content_text_footer)
        rowx += 1
        sheet.merge_range(rowx, 1, rowx, 4, _(u'Тайлан хянасан: ') + '........................' + '/                          /', format_content_text_footer)
        
        sheet.set_zoom(80)
        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report()
