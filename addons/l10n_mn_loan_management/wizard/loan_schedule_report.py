# -*- coding: utf-8 -*-

from io import BytesIO
import base64
import time, calendar
from datetime import date, datetime, timedelta
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from operator import itemgetter

from odoo import api, fields, models, _

from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class LoanOrderStatement(models.TransientModel):
    _name = 'loan.schedule.report'
    _description = 'Loan Schedule Report'
    
    company_id = fields.Many2one('res.company', string=u'Компани', readonly=True,
        default=lambda self: self.env.user.company_id, required=True)
    loan_order_id = fields.Many2one('loan.order', string=u'Зээлийн гэрээ', required=True)
    
    @api.multi
    def export_report(self):
        data = {
            'ids': self.loan_order_id.id,
            'model': 'loan.order',
        }
        result = self.env['report'].get_action(self.loan_order_id.id, 'l10n_mn_loan_management.loan_schedule_report', data=data)
        return result  
