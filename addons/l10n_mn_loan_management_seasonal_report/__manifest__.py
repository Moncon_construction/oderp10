# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': "Mongolian Loan Management seasonal report",
    'version': '1.0',
    'depends': ['l10n_mn_loan_management'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
        ББСБ-н улирлын мэдээ тайлан
    """,
    'website': 'http://asterisk-tech.mn',
    'data': [
        'views/loan_order_seasonal_report_view.xml',
        'security/ir.model.access.csv',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
