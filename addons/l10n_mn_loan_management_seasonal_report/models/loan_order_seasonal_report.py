# -*- encoding: utf-8 -*-
import openpyxl
import time
import base64
from datetime import datetime, timedelta, date
from dateutil import parser
from openpyxl import load_workbook
from openpyxl import Workbook
from openpyxl.styles import Alignment
from openpyxl.styles.borders import Border, Side

from odoo import fields, models, _, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_module_resource


class LoanOrderSeasonalReport(models.Model):
    _name = 'loan.order.seasonal.report'
    _description = 'Loan order seasonal report'

    name = fields.Char('Name', required=True)
    company_id = fields.Many2one('res.company', 'Company', required=True,
                                 default=lambda self: self.env.user.company_id.id)
    start_date = fields.Date('Start date')
    end_date = fields.Date('End date')
    output_file = fields.Binary(attachment=True, string='Output file', readonly=True)
    file_name = fields.Char('File name')
    date_field = fields.Char(default='A7')
    company_name_field = fields.Char(default='A3')
    statistic_1 = fields.One2many('loan.order.seasonal.report.statistic', 'report_id',
                                  'Total partners number /Increased from beginning of year/',
                                  domain=[('line_type', '=', 'statistic_1_1')])
    statistic_2 = fields.One2many('loan.order.seasonal.report.statistic', 'report_id',
                                  'Total loaners number /Increased from beginning of year/',
                                  domain=[('line_type', '=', 'statistic_1_2')])
    statistic_3 = fields.One2many('loan.order.seasonal.report.statistic', 'report_id',
                                  'Loaners age /number/', domain=[('line_type', '=', 'statistic_1_3')])
    statistic_4 = fields.One2many('loan.order.seasonal.report.statistic', 'report_id',
                                  'Loaners education /number/', domain=[('line_type', '=', 'statistic_1_4')])
    statistic_6 = fields.One2many('loan.order.seasonal.report.statistic', 'report_id',
                                  'Number of shareholders', domain=[('line_type', '=', 'statistic_1_6')])
    statistic_7 = fields.One2many('loan.order.seasonal.report.statistic', 'report_id',
                                  'Number of employees', domain=[('line_type', '=', 'statistic_1_7')])
    statistic_8 = fields.One2many('loan.order.seasonal.report.statistic', 'report_id',
                                  'Licensed activities', domain=[('line_type', '=', 'statistic_1_8')])
    statistic_9 = fields.One2many('loan.order.seasonal.report.statistic', 'report_id',
                                  'Does have financial', domain=[('line_type', '=', 'statistic_1_9')])
    statistic_10 = fields.One2many('loan.order.seasonal.report.statistic', 'report_id',
                                   'Location', domain=[('line_type', '=', 'statistic_1_10')])
    statistic_11 = fields.One2many('loan.order.seasonal.report.statistic', 'report_id',
                                   'Number of branches', domain=[('line_type', '=', 'statistic_1_11')])
    statistic_13 = fields.One2many('loan.order.seasonal.report.statistic', 'report_id',
                                   'Is insured with foreign organization',
                                   domain=[('line_type', '=', 'statistic_1_13')])
    statistic_14 = fields.One2many('loan.order.seasonal.report.statistic', 'report_id',
                                   'Is selected to receive a loan through the Microfinance Development Fund',
                                   domain=[('line_type', '=', 'statistic_1_14')])
    statistic_15 = fields.One2many('loan.order.seasonal.report.statistic', 'report_id',
                                   'Total assets of same period of previous year /thousand MNT/',
                                   domain=[('line_type', '=', 'statistic_1_15')])
    statistic_16 = fields.One2many('loan.order.seasonal.report.statistic', 'report_id',
                                   'Self assets of same period of previous year /thousand MNT/',
                                   domain=[('line_type', '=', 'statistic_1_16')])
    statistic_17_1 = fields.One2many('loan.order.seasonal.report.statistic', 'report_id',
                                     'Total transcations delivered to Mongolbank',
                                     domain=[('line_type', '=', 'statistic_1_17_1')])
    statistic_17_2 = fields.One2many('loan.order.seasonal.report.statistic', 'report_id',
                                     'Amount transcations delivered to Mongolbank',
                                     domain=[('line_type', '=', 'statistic_1_17_2')])
    statistic_18 = fields.One2many('loan.order.seasonal.report.statistic', 'report_id',
                                   'Average of last 3 years profit /thousand MNT/',
                                   domain=[('line_type', '=', 'statistic_1_18')])

    # БАЛАНС
    # Санхүү байдлын тайлан
    balance_report_id = fields.Many2one('account.mongolian.balance.sheet')
    balance_line_ids = fields.One2many('account.mongolian.balance.sheet.line', related='balance_report_id.line_ids',
                                       string='Balance Lines')
    # Мөнгөн гүйлгээний тайлан
    cashflow_report_id = fields.Many2one('account.cashflow.report')
    cashflow_line_ids = fields.One2many('account.cashflow.report.line', related='cashflow_report_id.line_ids',
                                        string='Cashflow Lines')
    # Орлогын дэлгэрэнгүй тайлан
    profit_report_id = fields.Many2one('account.profit.report')
    profit_line_ids = fields.One2many('account.profit.report.line', related='profit_report_id.line_ids',
                                      string='Profit Lines')

    # Өмчийн өөрчлөлтийн тайлан
    equity_changes_report_id = fields.Many2one('account.equity.change.report')
    equity_changes_line_ids = fields.One2many('account.equity.change.report.line',
                                              related='equity_changes_report_id.line_ids',
                                              string='Equity Changes Lines')

    @api.multi
    def base_report_compute(self):
        for obj in self:
            if obj.balance_report_id:
                obj.balance_report_id.compute()
            if obj.cashflow_report_id:
                obj.cashflow_report_id.compute()
            if obj.profit_report_id:
                obj.profit_report_id.compute()
            if obj.equity_changes_report_id:
                obj.equity_changes_report_id.compute()

    forex_1 = fields.One2many('loan.order.seasonal.report.forex', 'report_id', 'A. ACTIVE',
                              domain=[('line_type', '=', 'forex_1')])
    forex_2 = fields.One2many('loan.order.seasonal.report.forex', 'report_id', 'B. PASSIVE',
                              domain=[('line_type', '=', 'forex_2')])
    forex_3 = fields.One2many('loan.order.seasonal.report.forex', 'report_id', 'C. EXTRA-BALANCE ACCOUNT BALANCE',
                              domain=[('line_type', '=', 'forex_3')])

    alm_1 = fields.One2many('loan.order.seasonal.report.alm', 'report_id', 'Asset',
                            domain=[('line_type', '=', 'alm_1')])
    alm_2 = fields.One2many('loan.order.seasonal.report.alm', 'report_id', 'Liabilities',
                            domain=[('line_type', '=', 'alm_2')])

    top_forty = fields.One2many('loan.order.seasonal.report.top.forty', 'report_id',
                                'Top 40 loaners of NFBI')
    insider = fields.One2many('loan.order.seasonal.report.insider', 'report_id',
                              'Loans given to shareholders, board members, CEOs and employees of NFBI')
    source = fields.One2many('loan.order.seasonal.report.source', 'report_id',
                             'NBFI projects and programs Other sources')
    securities = fields.One2many('loan.order.seasonal.report.securities', 'report_id',
                                 'Securities concentration report')
    bond = fields.One2many('loan.order.seasonal.report.bond', 'report_id',
                           'Bond report')
    trust = fields.One2many('loan.order.seasonal.report.trust', 'report_id',
                            'Trust service centralization report')
    money_transfer = fields.One2many('loan.order.seasonal.report.money.transfer', 'report_id',
                                     'Electronic payment and money transfer service report')

    off_balance_1 = fields.One2many('loan.order.seasonal.report.off.balance', 'report_id',
                                    '1',
                                    domain=[('line_type', '=', 'off_balance_1')])
    off_balance_2 = fields.One2many('loan.order.seasonal.report.off.balance', 'report_id',
                                    '2',
                                    domain=[('line_type', '=', 'off_balance_2')])
    off_balance_3 = fields.One2many('loan.order.seasonal.report.off.balance', 'report_id',
                                    '3',
                                    domain=[('line_type', '=', 'off_balance_3')])
    ipotec = fields.One2many('loan.order.seasonal.report.ipotec', 'report_id',
                             'LOAN REPORT OF IPOTEC')

    provide_1 = fields.One2many('loan.order.seasonal.report.provide', 'report_id',
                                'Loans given to citizens', domain=[('line_type', '=', 'provide_1')])
    provide_2 = fields.One2many('loan.order.seasonal.report.provide', 'report_id',
                                'Loans given to citizens', domain=[('line_type', '=', 'provide_2')])
    provide_3 = fields.One2many('loan.order.seasonal.report.provide', 'report_id',
                                'By loan term category', domain=[('line_type', '=', 'provide_3')])
    # Зээл ангилалаар
    provide_4_1 = fields.One2many('loan.order.seasonal.report.provide', 'report_id',
                                  'Loans given to citizens', domain=[('line_type', '=', 'provide_4_1')])
    provide_4_2 = fields.One2many('loan.order.seasonal.report.provide', 'report_id',
                                  'Loans given to legal entities', domain=[('line_type', '=', 'provide_4_2')])

    @api.constrains('start_date', 'end_date')
    def check_date(self):
        if self.start_date > self.end_date:
            raise ValidationError(_('End date must be after Start date'))

    @api.multi
    def compute(self):
        res_partner_obj = self.env['res.partner']
        loan_order_obj = self.env['loan.order']
        hr_employee_obj = self.env['hr.employee']
        all_debtor_ids = res_partner_obj.search([('is_debtor', '=', True)])
        all_loan_order_ids = loan_order_obj.search([('partner_id', 'in', all_debtor_ids.ids)])

        for obj in self:
            # Нийт харилцагчдын тоо /оны эхнээс өссөн дүнгээр/
            if obj.statistic_1:
                for line in obj.statistic_1:
                    if line.name == 'Эмэгтэй':
                        female_partner_ids = res_partner_obj.search([('sex', '=', 'female')])
                        line.value = len(female_partner_ids)
                    elif line.name == 'Эрэгтэй':
                        male_partner_ids = res_partner_obj.search([('sex', '=', 'male')])
                        line.value = len(male_partner_ids)
                    elif line.name == 'Хуулийн этгээд':
                        company_partner_ids = res_partner_obj.search([('company_type', '=', 'company')])
                        line.value = len(company_partner_ids)
                    elif line.name == 'Нийт итгэмжлэгчдийн тоо':
                        trustee_partner_ids = res_partner_obj.search([('is_trustee', '=', True)])
                        line.value = len(trustee_partner_ids)
                    elif line.name == 'Нийт өрийн бичиг авсан харилцагчдийн тоо':
                        line.value = 0

            # Зээлдэгчдийн тоо /оны эхнээс өссөн дүнгээр/
            if obj.statistic_2:
                for line in obj.statistic_2:
                    if line.name == 'Хэвийн':
                        line.value = len(all_loan_order_ids.filtered(lambda loan: loan.loan_state.sequence == '01'))
                    if line.name == 'Хугацаа хэтэрсэн':
                        line.value = len(all_loan_order_ids.filtered(lambda loan: loan.loan_state.sequence == '02'))
                    if line.name == 'Хэвийн бус':
                        line.value = len(all_loan_order_ids.filtered(lambda loan: loan.loan_state.sequence == '03'))
                    if line.name == 'Эргэлзээтэй':
                        line.value = len(all_loan_order_ids.filtered(lambda loan: loan.loan_state.sequence == '04'))
                    if line.name == 'Муу':
                        line.value = len(all_loan_order_ids.filtered(lambda loan: loan.loan_state.sequence == '05'))

            # Зээлдэгчдийн насжилт /тоогоор/
            if obj.statistic_3:
                def compute_age(partners):
                    current_date = datetime.now()
                    current_year = current_date.year
                    for partner in partners:
                        return current_year - parser.parse(
                            partner.sudo().birthday).year if partner.sudo().birthday else 0

                for line in obj.statistic_3:
                    if line.cell_value == 'C24':
                        line.value = len(all_debtor_ids.filtered(lambda debtor: 18 <= compute_age(debtor) <= 35))
                    if line.cell_value == 'C25':
                        line.value = len(all_debtor_ids.filtered(lambda debtor: 36 <= compute_age(debtor) <= 45))
                    if line.cell_value == 'C26':
                        line.value = len(all_debtor_ids.filtered(lambda debtor: 46 <= compute_age(debtor) <= 55))
                    if line.cell_value == 'C27':
                        line.value = len(all_debtor_ids.filtered(lambda debtor: compute_age(debtor) > 55))

            # Зээлдэгчдийн боловсрол /тоогоор/
            if obj.statistic_4:
                for line in obj.statistic_4:
                    if line.cell_value == 'C29':
                        line.value = len(all_debtor_ids.search([('education', '=', 'higher')]))
                    if line.cell_value == 'C30':
                        line.value = len(all_debtor_ids.search([('education', '=', 'special_secondary')]))
                    if line.cell_value == 'C31':
                        line.value = len(all_debtor_ids.search([('education', '=', 'high_school_education')]))
                    if line.cell_value == 'C32':
                        line.value = len(all_debtor_ids.search([('education', '=', 'secondary')]))
                    if line.cell_value == 'C33':
                        line.value = len(all_debtor_ids.search([('education', '=', 'primary')]))

            # Хувь нийлүүлэгчдийн тоо
            if obj.statistic_6:
                for line in obj.statistic_6:
                    if line.cell_value == 'C36':
                        line.value = 0
                    if line.cell_value == 'C37':
                        line.value = len(all_debtor_ids.mapped('shareholder_ids').filtered(
                            lambda shareholder: shareholder.sex == 'female'))
                    if line.cell_value == 'C38':
                        line.value = len(all_debtor_ids.mapped('shareholder_ids').filtered(
                            lambda shareholder: shareholder.sex == 'male'))
                    if line.cell_value == 'C39':
                        line.value = len(all_debtor_ids.mapped('shareholder_ids').filtered(
                            lambda shareholder: shareholder.company_type == 'company'))

            # Нийт ажилтнуудын тоо
            if obj.statistic_7:
                for line in obj.statistic_7:
                    if line.cell_value == 'C40':
                        line.value = len(hr_employee_obj.search([]))
                    if line.cell_value == 'C41':
                        line.value = len(
                            hr_employee_obj.search([]).filtered(
                                lambda employee: employee.user_id.partner_id.sex == 'female'))

            # Тусгай зөвшөөрөлтэй үйл ажиллагаа
            if obj.statistic_8:
                for line in obj.statistic_8:
                    if line.cell_value == 'C43':
                        line.value = 1

            # Байршил
            if obj.statistic_10:
                for line in obj.statistic_10:
                    if line.cell_value == 'C57':
                        line.value = self.env.user.company_id.street or ''
                    if line.cell_value == 'C58':
                        line.value = self.env.user.company_id.phone or ''
                    if line.cell_value == 'C59':
                        line.value = self.env.user.company_id.email or ''
                    if line.cell_value == 'C60':
                        line.value = self.env.user.partner_id.phone or ''
            # Байршил
            if obj.statistic_10:
                for line in obj.statistic_10:
                    if line.cell_value == 'C62':
                        line.value = 1
                    if line.cell_value == 'C63':
                        line.value = 0
                    if line.cell_value == 'C64':
                        line.value = 0

    @api.model
    def create(self, vals):
        statistic_1 = []
        statistic_2 = []
        statistic_3 = []
        statistic_4 = []
        statistic_6 = []
        statistic_7 = []
        statistic_8 = []
        statistic_9 = []
        statistic_10 = []
        statistic_11 = []
        statistic_13 = []
        statistic_14 = []
        statistic_15 = []
        statistic_16 = []
        statistic_17_1 = []
        statistic_17_2 = []
        statistic_18 = []

        statistic_1.append((0, 0, {
            'name': u'Эмэгтэй',
            'cell_value': 'C11',
            'line_type': 'statistic_1_1',
        }))
        statistic_1.append((0, 0, {
            'name': u'Эрэгтэй',
            'cell_value': 'C12',
            'line_type': 'statistic_1_1',
        }))
        statistic_1.append((0, 0, {
            'name': u'Хуулийн этгээд',
            'cell_value': 'C13',
            'line_type': 'statistic_1_1',
        }))
        statistic_1.append((0, 0, {
            'name': u'Нийт итгэмжлэгчдийн тоо',
            'cell_value': 'C14',
            'line_type': 'statistic_1_1',
        }))
        statistic_1.append((0, 0, {
            'name': u'Нийт өрийн бичиг авсан харилцагчдийн тоо',
            'cell_value': 'C15',
            'line_type': 'statistic_1_1',
        }))

        statistic_2.append((0, 0, {
            'name': u'Хэвийн',
            'cell_value': 'C17',
            'line_type': 'statistic_1_2',
        }))
        statistic_2.append((0, 0, {
            'name': u'Хугацаа хэтэрсэн',
            'cell_value': 'C18',
            'line_type': 'statistic_1_2',
        }))
        statistic_2.append((0, 0, {
            'name': u'Хэвийн бус',
            'cell_value': 'C20',
            'line_type': 'statistic_1_2',
        }))
        statistic_2.append((0, 0, {
            'name': u'Эргэлзээтэй',
            'cell_value': 'C21',
            'line_type': 'statistic_1_2',
        }))
        statistic_2.append((0, 0, {
            'name': u'Муу',
            'cell_value': 'C22',
            'line_type': 'statistic_1_2',
        }))

        statistic_3.append((0, 0, {
            'name': u'18-35 насны',
            'cell_value': 'C24',
            'line_type': 'statistic_1_3',
        }))
        statistic_3.append((0, 0, {
            'name': u'36-45 насны',
            'cell_value': 'C25',
            'line_type': 'statistic_1_3',
        }))
        statistic_3.append((0, 0, {
            'name': u'46-55 насны',
            'cell_value': 'C26',
            'line_type': 'statistic_1_3',
        }))
        statistic_3.append((0, 0, {
            'name': u'55-аас дээш',
            'cell_value': 'C27',
            'line_type': 'statistic_1_3',
        }))

        statistic_4.append((0, 0, {
            'name': u'Дээд',
            'cell_value': 'C29',
            'line_type': 'statistic_1_4',
        }))
        statistic_4.append((0, 0, {
            'name': u'Тусгай дунд',
            'cell_value': 'C30',
            'line_type': 'statistic_1_4',
        }))
        statistic_4.append((0, 0, {
            'name': u'Бүрэн дунд',
            'cell_value': 'C31',
            'line_type': 'statistic_1_4',
        }))
        statistic_4.append((0, 0, {
            'name': u'Дунд',
            'cell_value': 'C32',
            'line_type': 'statistic_1_4',
        }))
        statistic_4.append((0, 0, {
            'name': u'Бага',
            'cell_value': 'C33',
            'line_type': 'statistic_1_4',
        }))

        statistic_6.append((0, 0, {
            'name': u'Гадаад эзэмшлийн хувь',
            'cell_value': 'C36',
            'line_type': 'statistic_1_6',
        }))
        statistic_6.append((0, 0, {
            'name': u'Эмэгтэй',
            'cell_value': 'C37',
            'line_type': 'statistic_1_6',
        }))
        statistic_6.append((0, 0, {
            'name': u'Эрэгтэй',
            'cell_value': 'C38',
            'line_type': 'statistic_1_6',
        }))
        statistic_6.append((0, 0, {
            'name': u'Хуулийн этгээд',
            'cell_value': 'C39',
            'line_type': 'statistic_1_6',
        }))

        statistic_7.append((0, 0, {
            'name': u'Нийт ажилтнуудын тоо',
            'cell_value': 'C40',
            'line_type': 'statistic_1_7',
        }))
        statistic_7.append((0, 0, {
            'name': u'Эмэгтэй ажилтны тоо',
            'cell_value': 'C41',
            'line_type': 'statistic_1_7',
        }))

        statistic_8.append((0, 0, {
            'name': u'Зээл',
            'cell_value': 'C43',
            'line_type': 'statistic_1_8',
        }))
        statistic_8.append((0, 0, {
            'name': u'Факторингийн үйлчилгээ',
            'cell_value': 'C44',
            'line_type': 'statistic_1_8',
        }))
        statistic_8.append((0, 0, {
            'name': u'Төлбөрийн баталгаа гаргах',
            'cell_value': 'C45',
            'line_type': 'statistic_1_8',
        }))
        statistic_8.append((0, 0, {
            'name': u'Төлбөр тооцооны хэрэгсэл гаргах',
            'cell_value': 'C46',
            'line_type': 'statistic_1_8',
        }))
        statistic_8.append((0, 0, {
            'name': u'Цахим төлбөр тооцоо, мөнгөн гуйвуулгын үйлчилгээ',
            'cell_value': 'C47',
            'line_type': 'statistic_1_8',
        }))
        statistic_8.append((0, 0, {
            'name': u'Гадаад валютын арилжаа',
            'cell_value': 'C48',
            'line_type': 'statistic_1_8',
        }))
        statistic_8.append((0, 0, {
            'name': u'Итгэлцлийн үйлчилгээ',
            'cell_value': 'C49',
            'line_type': 'statistic_1_8',
        }))
        statistic_8.append((0, 0, {
            'name': u'Богино хугацаат санхүүгийн хэрэгсэлд хөрөнгө оруулалт хийх',
            'cell_value': 'C50',
            'line_type': 'statistic_1_8',
        }))
        statistic_8.append((0, 0, {
            'name': u'Хөрөнгө оруулалт, санхүүгийн чиглэлээр зөвлөгөө, мэдээлэл өгөх',
            'cell_value': 'C51',
            'line_type': 'statistic_1_8',
        }))
        statistic_8.append((0, 0, {
            'name': u'Үл хөдлөх эд хөрөнгө барьцаалахтай холбоотой санхүүгийн зуучлалын үйл ажиллагаа',
            'cell_value': 'C52',
            'line_type': 'statistic_1_8',
        }))

        statistic_9.append((0, 0, {
            'name': u'Санхүүгийн программтай эсэх',
            'cell_value': 'C53',
            'line_type': 'statistic_1_9',
            'value': u'Тийм',
        }))
        statistic_9.append((0, 0, {
            'name': u'Программын нэр',
            'cell_value': 'C54',
            'line_type': 'statistic_1_9',
            'value': u'OdERP 10.0',
        }))
        statistic_9.append((0, 0, {
            'name': u'Санхүүгийн программыг нийлүүлсэн компанийн нэр',
            'cell_value': 'C55',
            'line_type': 'statistic_1_9',
            'value': u'Астериск Техноложис ХХК',
        }))
        statistic_9.append((0, 0, {
            'name': u'Зээлийн мэдээллийн сантай холбогдсон эсэх',
            'cell_value': 'C56',
            'line_type': 'statistic_1_9',
            'value': u'Тийм',
        }))

        statistic_10.append((0, 0, {
            'name': u'Байршил',
            'cell_value': 'C57',
            'line_type': 'statistic_1_10',
        }))
        statistic_10.append((0, 0, {
            'name': u'ББСБ-ын утас',
            'cell_value': 'C58',
            'line_type': 'statistic_1_10',
        }))
        statistic_10.append((0, 0, {
            'name': u'ББСБ-ын цахим шуудан',
            'cell_value': 'C59',
            'line_type': 'statistic_1_10',
        }))
        statistic_10.append((0, 0, {
            'name': u'Тайлан оруулсан ажилтны гар утасны дугаар:',
            'cell_value': 'C60',
            'line_type': 'statistic_1_10',
        }))

        statistic_11.append((0, 0, {
            'name': u'-Улаанбаатарт',
            'cell_value': 'C62',
            'line_type': 'statistic_1_11',
        }))
        statistic_11.append((0, 0, {
            'name': u'-Орон нутагт',
            'cell_value': 'C63',
            'line_type': 'statistic_1_11',
        }))
        statistic_11.append((0, 0, {
            'name': u'Төлөөлөгчийн газар',
            'cell_value': 'C64',
            'line_type': 'statistic_1_11',
        }))

        statistic_13.append((0, 0, {
            'name': u'Гадаадын байгууллагад даатгагдсан эсэх',
            'cell_value': 'C65',
            'line_type': 'statistic_1_13',
        }))

        statistic_14.append((0, 0, {
            'name': u'Бичил санхүүгийн хөгжлийн сангаас дамжуулан зээлдүүлэх сонгон шалгаруулалтад шалгарч зээл авсан эсэх',
            'cell_value': 'C66',
            'line_type': 'statistic_1_14',
        }))

        statistic_15.append((0, 0, {
            'name': u'Өмнөх оны мөн үеийн нийт хөрөнгө /мянган төгрөгөөр/',
            'cell_value': 'C67',
            'line_type': 'statistic_1_15',
        }))

        statistic_16.append((0, 0, {
            'name': u'Өмнөх оны мөн үеийн өөрийн хөрөнгө хөрөнгө /мянган төгрөгөөр/',
            'cell_value': 'C68',
            'line_type': 'statistic_1_16',
        }))

        statistic_17_1.append((0, 0, {
            'name': u'20 сая төгрөгөөс дээш бэлэн мөнгөний гүйлгээний тоо',
            'cell_value': 'C70',
            'line_type': 'statistic_1_17_1',
        }))
        statistic_17_1.append((0, 0, {
            'name': u'Сэжигтэй гүйлгээний тоо',
            'cell_value': 'C71',
            'line_type': 'statistic_1_17_1',
        }))

        statistic_17_2.append((0, 0, {
            'name': u'20 сая төгрөгөөс дээш бэлэн мөнгөний гүйлгээний дүн /төгрөгөөр/',
            'cell_value': 'C73',
            'line_type': 'statistic_1_17_2',
        }))
        statistic_17_2.append((0, 0, {
            'name': u'Сэжигтэй гүйлгээний дүн /төгрөгөөр/',
            'cell_value': 'C74',
            'line_type': 'statistic_1_17_2',
        }))

        statistic_18.append((0, 0, {
            'name': u'Сүүлийн 3 жилийн эерэг ашгийн дундаж дүн /мянган төгрөгөөр/',
            'cell_value': 'C75',
            'line_type': 'statistic_1_18',
        }))

        forex_1 = []
        forex_2 = []
        forex_3 = []
        forex_1.append((0, 0, {
            'name': u'Бэлэн валют',
            'cell_value_usd': 'C10',
            'cell_value_eur': 'D10',
            'cell_value_jpy': 'E10',
            'cell_value_chf': 'F10',
            'cell_value_gbp': 'G10',
            'cell_value_cny': 'H10',
            'cell_value_rub': 'I10',
            'cell_value_krw': 'J10',
            'cell_value_other': 'K10',
            'line_type': 'forex_1',
        }))
        forex_1.append((0, 0, {
            'name': u' Банк, санхүүгийн байгуулага дахь гадаад валютын харилцах, хадгаламжийн данс',
            'cell_value_usd': 'C11',
            'cell_value_eur': 'D11',
            'cell_value_jpy': 'E11',
            'cell_value_chf': 'F11',
            'cell_value_gbp': 'G11',
            'cell_value_cny': 'H11',
            'cell_value_rub': 'I11',
            'cell_value_krw': 'J11',
            'cell_value_other': 'K11',
            'line_type': 'forex_1',
        }))
        forex_1.append((0, 0, {
            'name': u'Валютаар олгосон зээлийн өрийн үлдэгдэл',
            'cell_value_usd': 'C12',
            'cell_value_eur': 'D12',
            'cell_value_jpy': 'E12',
            'cell_value_chf': 'F12',
            'cell_value_gbp': 'G12',
            'cell_value_cny': 'H12',
            'cell_value_rub': 'I12',
            'cell_value_krw': 'J12',
            'cell_value_other': 'K12',
            'line_type': 'forex_1',
        }))
        forex_1.append((0, 0, {
            'name': u'Санхүүгийн түрээс /валютаар/',
            'cell_value_usd': 'C13',
            'cell_value_eur': 'D13',
            'cell_value_jpy': 'E13',
            'cell_value_chf': 'F13',
            'cell_value_gbp': 'G13',
            'cell_value_cny': 'H13',
            'cell_value_rub': 'I13',
            'cell_value_krw': 'J13',
            'cell_value_other': 'K13',
            'line_type': 'forex_1',
        }))
        forex_1.append((0, 0, {
            'name': u'Факторингийн авлага /валютаар/',
            'cell_value_usd': 'C14',
            'cell_value_eur': 'D14',
            'cell_value_jpy': 'E14',
            'cell_value_chf': 'F14',
            'cell_value_gbp': 'G14',
            'cell_value_cny': 'H14',
            'cell_value_rub': 'I14',
            'cell_value_krw': 'J14',
            'cell_value_other': 'K14',
            'line_type': 'forex_1',
        }))
        forex_1.append((0, 0, {
            'name': u'Бусад',
            'cell_value_usd': 'C15',
            'cell_value_eur': 'D15',
            'cell_value_jpy': 'E15',
            'cell_value_chf': 'F15',
            'cell_value_gbp': 'G15',
            'cell_value_cny': 'H15',
            'cell_value_rub': 'I15',
            'cell_value_krw': 'J15',
            'cell_value_other': 'K15',
            'line_type': 'forex_1',
        }))

        forex_2.append((0, 0, {
            'name': u'Итгэлцлийн үйлчилгээний өглөг',
            'cell_value_usd': 'C17',
            'cell_value_eur': 'D17',
            'cell_value_jpy': 'E17',
            'cell_value_chf': 'F17',
            'cell_value_gbp': 'G17',
            'cell_value_cny': 'H17',
            'cell_value_rub': 'I17',
            'cell_value_krw': 'J17',
            'cell_value_other': 'K17',
            'line_type': 'forex_2',
        }))
        forex_2.append((0, 0, {
            'name': u' Банк, санхүүгийн байгууллагаас авсан зээл ',
            'cell_value_usd': 'C18',
            'cell_value_eur': 'D18',
            'cell_value_jpy': 'E18',
            'cell_value_chf': 'F18',
            'cell_value_gbp': 'G18',
            'cell_value_cny': 'H18',
            'cell_value_rub': 'I18',
            'cell_value_krw': 'J18',
            'cell_value_other': 'K18',
            'line_type': 'forex_2',
        }))
        forex_2.append((0, 0, {
            'name': u'Төслийн зээлийн санхүүжилт',
            'cell_value_usd': 'C19',
            'cell_value_eur': 'D19',
            'cell_value_jpy': 'E19',
            'cell_value_chf': 'F19',
            'cell_value_gbp': 'G19',
            'cell_value_cny': 'H19',
            'cell_value_rub': 'I19',
            'cell_value_krw': 'J19',
            'cell_value_other': 'K19',
            'line_type': 'forex_2',
        }))
        forex_2.append((0, 0, {
            'name': u'Өрийн бичиг',
            'cell_value_usd': 'C20',
            'cell_value_eur': 'D20',
            'cell_value_jpy': 'E20',
            'cell_value_chf': 'F20',
            'cell_value_gbp': 'G20',
            'cell_value_cny': 'H20',
            'cell_value_rub': 'I20',
            'cell_value_krw': 'J20',
            'cell_value_other': 'K20',
            'line_type': 'forex_2',
        }))
        forex_2.append((0, 0, {
            'name': u' Бусад гадаад эх үүсвэр',
            'cell_value_usd': 'C21',
            'cell_value_eur': 'D21',
            'cell_value_jpy': 'E21',
            'cell_value_chf': 'F21',
            'cell_value_gbp': 'G21',
            'cell_value_cny': 'H21',
            'cell_value_rub': 'I21',
            'cell_value_krw': 'J21',
            'cell_value_other': 'K21',
            'line_type': 'forex_2',
        }))
        forex_2.append((0, 0, {
            'name': u' Бусад өр төлбөр',
            'cell_value_usd': 'C22',
            'cell_value_eur': 'D22',
            'cell_value_jpy': 'E22',
            'cell_value_chf': 'F22',
            'cell_value_gbp': 'G22',
            'cell_value_cny': 'H22',
            'cell_value_rub': 'I22',
            'cell_value_krw': 'J22',
            'cell_value_other': 'K22',
            'line_type': 'forex_2',
        }))

        forex_3.append((0, 0, {
            'name': u'Төлбөрийн  баталгаанууд',
            'cell_value_usd': 'C24',
            'cell_value_eur': 'D24',
            'cell_value_jpy': 'E24',
            'cell_value_chf': 'F24',
            'cell_value_gbp': 'G24',
            'cell_value_cny': 'H24',
            'cell_value_rub': 'I24',
            'cell_value_krw': 'J24',
            'cell_value_other': 'K24',
            'line_type': 'forex_3',
        }))
        forex_3.append((0, 0, {
            'name': u'Бусад хүлээж болзошгүй үүргүүд',
            'cell_value_usd': 'C25',
            'cell_value_eur': 'D25',
            'cell_value_jpy': 'E25',
            'cell_value_chf': 'F25',
            'cell_value_gbp': 'G25',
            'cell_value_cny': 'H25',
            'cell_value_rub': 'I25',
            'cell_value_krw': 'J25',
            'cell_value_other': 'K25',
            'line_type': 'forex_3',
        }))

        alm_1 = []
        alm_2 = []
        alm_1.append((0, 0, {
            'name': u'Бэлэн мөнгө',
            'month_1_cell_value': 'D8',
            'month_1_3_cell_value': 'E8',
            'month_3_6_cell_value': 'F8',
            'month_6_12_cell_value': 'G8',
            'year_1_3_cell_value': 'H8',
            'year_3_5_cell_value': 'I8',
            'year_5_cell_value': 'J8',
            'line_type': 'alm_1',
        }))
        alm_1.append((0, 0, {
            'name': u'Банк, санхүүгийн байгууллагад байршуулсан харилцах',
            'undated_cell_value': 'C9',
            'month_1_cell_value': 'D9',
            'month_1_3_cell_value': 'E9',
            'month_3_6_cell_value': 'F9',
            'month_6_12_cell_value': 'G9',
            'year_1_3_cell_value': 'H9',
            'year_3_5_cell_value': 'I9',
            'year_5_cell_value': 'J9',
            'line_type': 'alm_1',
        }))
        alm_1.append((0, 0, {
            'name': u'Банк, санхүүгийн байгууллагад байршуулсан хадгаламж',
            'undated_cell_value': 'C10',
            'month_1_cell_value': 'D10',
            'month_1_3_cell_value': 'E10',
            'month_3_6_cell_value': 'F10',
            'month_6_12_cell_value': 'G10',
            'year_1_3_cell_value': 'H10',
            'year_3_5_cell_value': 'I10',
            'year_5_cell_value': 'J10',
            'line_type': 'alm_1',
        }))
        alm_1.append((0, 0, {
            'name': u'Зээл',
            'undated_cell_value': 'C11',
            'month_1_cell_value': 'D11',
            'month_1_3_cell_value': 'E11',
            'month_3_6_cell_value': 'F11',
            'month_6_12_cell_value': 'G11',
            'year_1_3_cell_value': 'H11',
            'year_3_5_cell_value': 'I11',
            'year_5_cell_value': 'J11',
            'line_type': 'alm_1',
        }))
        alm_1.append((0, 0, {
            'name': u'Хөрөнгө оруулалт',
            'undated_cell_value': 'C12',
            'month_1_cell_value': 'D12',
            'month_1_3_cell_value': 'E12',
            'month_3_6_cell_value': 'F12',
            'month_6_12_cell_value': 'G12',
            'year_1_3_cell_value': 'H12',
            'year_3_5_cell_value': 'I12',
            'year_5_cell_value': 'J12',
            'line_type': 'alm_1',
        }))
        alm_1.append((0, 0, {
            'name': u'Факторинг ',
            'undated_cell_value': 'C13',
            'month_1_cell_value': 'D13',
            'month_1_3_cell_value': 'E13',
            'month_3_6_cell_value': 'F13',
            'month_6_12_cell_value': 'G13',
            'year_1_3_cell_value': 'H13',
            'year_3_5_cell_value': 'I13',
            'year_5_cell_value': 'J13',
            'line_type': 'alm_1',
        }))
        alm_1.append((0, 0, {
            'name': u'Авлага',
            'undated_cell_value': 'C14',
            'month_1_cell_value': 'D14',
            'month_1_3_cell_value': 'E14',
            'month_3_6_cell_value': 'F14',
            'month_6_12_cell_value': 'G14',
            'year_1_3_cell_value': 'H14',
            'year_3_5_cell_value': 'I14',
            'year_5_cell_value': 'J14',
            'line_type': 'alm_1',
        }))
        alm_1.append((0, 0, {
            'name': u'Бусад актив',
            'undated_cell_value': 'C15',
            'month_1_cell_value': 'D15',
            'month_1_3_cell_value': 'E15',
            'month_3_6_cell_value': 'F15',
            'month_6_12_cell_value': 'G15',
            'year_1_3_cell_value': 'H15',
            'year_3_5_cell_value': 'I15',
            'year_5_cell_value': 'J15',
            'line_type': 'alm_1',
        }))

        alm_2.append((0, 0, {
            'name': u'Итгэлцлийн үйлчилгээний өглөг',
            'undated_cell_value': 'C18',
            'month_1_cell_value': 'D18',
            'month_1_3_cell_value': 'E18',
            'month_3_6_cell_value': 'F18',
            'month_6_12_cell_value': 'G18',
            'year_1_3_cell_value': 'H18',
            'year_3_5_cell_value': 'I18',
            'year_5_cell_value': 'J18',
            'line_type': 'alm_2',
        }))
        alm_2.append((0, 0, {
            'name': u'Банк, санхүүгийн байгууллагаас авсан зээл',
            'undated_cell_value': 'C19',
            'month_1_cell_value': 'D19',
            'month_1_3_cell_value': 'E19',
            'month_3_6_cell_value': 'F19',
            'month_6_12_cell_value': 'G19',
            'year_1_3_cell_value': 'H19',
            'year_3_5_cell_value': 'I19',
            'year_5_cell_value': 'J19',
            'line_type': 'alm_2',
        }))
        alm_2.append((0, 0, {
            'name': u'Бусад эх үүсвэр',
            'undated_cell_value': 'C20',
            'month_1_cell_value': 'D20',
            'month_1_3_cell_value': 'E20',
            'month_3_6_cell_value': 'F20',
            'month_6_12_cell_value': 'G20',
            'year_1_3_cell_value': 'H20',
            'year_3_5_cell_value': 'I20',
            'year_5_cell_value': 'J20',
            'line_type': 'alm_2',
        }))
        alm_2.append((0, 0, {
            'name': u'ББСБ-аас гаргасан өрийн бичиг',
            'undated_cell_value': 'C21',
            'month_1_cell_value': 'D21',
            'month_1_3_cell_value': 'E21',
            'month_3_6_cell_value': 'F21',
            'month_6_12_cell_value': 'G21',
            'year_1_3_cell_value': 'H21',
            'year_3_5_cell_value': 'I21',
            'year_5_cell_value': 'J21',
            'line_type': 'alm_2',
        }))
        alm_2.append((0, 0, {
            'name': u'Төслийн зээлийн санхүүжилт',
            'undated_cell_value': 'C22',
            'month_1_cell_value': 'D22',
            'month_1_3_cell_value': 'E22',
            'month_3_6_cell_value': 'F22',
            'month_6_12_cell_value': 'G22',
            'year_1_3_cell_value': 'H22',
            'year_3_5_cell_value': 'I22',
            'year_5_cell_value': 'J22',
            'line_type': 'alm_2',
        }))
        alm_2.append((0, 0, {
            'name': u'Хоёрдогч өглөг',
            'undated_cell_value': 'C23',
            'month_1_cell_value': 'D23',
            'month_1_3_cell_value': 'E23',
            'month_3_6_cell_value': 'F23',
            'month_6_12_cell_value': 'G23',
            'year_1_3_cell_value': 'H23',
            'year_3_5_cell_value': 'I23',
            'year_5_cell_value': 'J23',
            'line_type': 'alm_2',
        }))

        money_transfer = []
        money_transfer.append((0, 0, {
            'name': u'USD',
        }))
        money_transfer.append((0, 0, {
            'name': u'EUR',
        }))
        money_transfer.append((0, 0, {
            'name': u'JPY',
        }))
        money_transfer.append((0, 0, {
            'name': u'CHF',
        }))
        money_transfer.append((0, 0, {
            'name': u'GBP',
        }))
        money_transfer.append((0, 0, {
            'name': u'CNY',
        }))
        money_transfer.append((0, 0, {
            'name': u'RUB',
        }))
        money_transfer.append((0, 0, {
            'name': u'KRW',
        }))
        money_transfer.append((0, 0, {
            'name': u'Бусад',
        }))

        ipotec = []
        ipotec.append((0, 0, {
            'name': u'Өргөдлийн тоо',
        }))
        ipotec.append((0, 0, {
            'name': u'Өргөдлөөр хүссэн зээлийн нийт дүн',
        }))
        ipotec.append((0, 0, {
            'name': u'Сарын эхний үлдэгдэл',
        }))
        ipotec.append((0, 0, {
            'name': u'Шинээр олгосон зээл',
        }))
        ipotec.append((0, 0, {
            'name': u'Төлөгдсөн зээл',
        }))
        ipotec.append((0, 0, {
            'name': u'Хугацаанаас өмнө төлөгдсөн зээл',
        }))
        ipotec.append((0, 0, {
            'name': u'Дебет',
        }))
        ipotec.append((0, 0, {
            'name': u'Кредит',
        }))
        ipotec.append((0, 0, {
            'name': u'Тайлант сард шинээр нэмэгдсэн',
        }))
        ipotec.append((0, 0, {
            'name': u'Тайлан сарын эцэст',
        }))
        ipotec.append((0, 0, {
            'name': u'Зээлийн хугацаа /сараар/ Дээд',
        }))
        ipotec.append((0, 0, {
            'name': u'Зээлийн хугацаа /сараар/ Доод',
        }))
        ipotec.append((0, 0, {
            'name': u'Тайлант сард шинээр олгосон',
        }))
        ipotec.append((0, 0, {
            'name': u'Тайлант сарын эцэс дэх',
        }))
        ipotec.append((0, 0, {
            'name': u'Тайлант сард олгосон зээлийн жилийн хүү /хувиар/ Дээд',
        }))
        ipotec.append((0, 0, {
            'name': u'Тайлант сард олгосон зээлийн жилийн хүү /хувиар/ Доод',
        }))
        ipotec.append((0, 0, {
            'name': u'Жигнэсэн дундаж',
        }))
        ipotec.append((0, 0, {
            'name': u'Тайлант сард шинээр олгосон зээлийн',
        }))
        ipotec.append((0, 0, {
            'name': u'Тайлант сарын эцэс дэх зээлийн',
        }))
        ipotec.append((0, 0, {
            'name': u'Тайлант сард энэ ангилалд шилжсэн зээл',
        }))
        ipotec.append((0, 0, {
            'name': u'Сарын эцсийн үлдэгдэл',
        }))
        ipotec.append((0, 0, {
            'name': u'Хугацаа хэтэрсэн',
        }))
        ipotec.append((0, 0, {
            'name': u'Хэвийн бус',
        }))
        ipotec.append((0, 0, {
            'name': u'Эргэлзээтэй',
        }))
        ipotec.append((0, 0, {
            'name': u'Mуу',
        }))
        ipotec.append((0, 0, {
            'name': u'Тайлант сард шинээр шилжсэн',
        }))
        ipotec.append((0, 0, {
            'name': u'Тайлант сарын эцсийн үлдэгдэл',
        }))

        provide_1 = []
        provide_2 = []
        provide_3 = []
        provide_4_1 = []
        provide_4_2 = []

        provide_1.append((0, 0, {
            'name': u'1.1 Хөдөө аж аху, ойн аж ахуй, загас барилт, ан агнуур',
            'line_type': 'provide_1',
        }))
        provide_1.append((0, 0, {
            'name': u'1.2 Уул уурхай, олборлолт',
            'line_type': 'provide_1',
        }))
        provide_1.append((0, 0, {
            'name': u'1.3 Боловсруулах үйлдвэрлэл',
            'line_type': 'provide_1',
        }))
        provide_1.append((0, 0, {
            'name': u'1.4 Цахилгаан, хий, уур, агааржуулалтын хангамж',
            'line_type': 'provide_1',
        }))
        provide_1.append((0, 0, {
            'name': u'1.5 Усан хангамж, бохир ус, хог хаягдлын менежмент болон цэвэрлэх үйл ажиллагаа',
            'line_type': 'provide_1',
        }))
        provide_1.append((0, 0, {
            'name': u'1.6 Барилга',
            'line_type': 'provide_1',
        }))
        provide_1.append((0, 0, {
            'name': u'1.7 Бөөний болон жижиглэн худалдаа',
            'line_type': 'provide_1',
        }))
        provide_1.append((0, 0, {
            'name': u'1.8 Машин мотоциклийн засвар үйлчилгээ',
            'line_type': 'provide_1',
        }))
        provide_1.append((0, 0, {
            'name': u'1.9 Тээвэр ба агуулахын үйл ажиллагаа',
            'line_type': 'provide_1',
        }))
        provide_1.append((0, 0, {
            'name': u'1.10 Байр сууц болон хоол хүнсээр үйлчлэх үйл ажиллагаа',
            'line_type': 'provide_1',
        }))
        provide_1.append((0, 0, {
            'name': u'1.11 Мэдээлэл холбоо',
            'line_type': 'provide_1',
        }))
        provide_1.append((0, 0, {
            'name': u'1.12 Санхүүгийн болон даатгалын үйл ажиллагаа',
            'line_type': 'provide_1',
        }))
        provide_1.append((0, 0, {
            'name': u'1.13 Үл хөдлөх хөрөнгийн үйл ажиллагаа',
            'line_type': 'provide_1',
        }))
        provide_1.append((0, 0, {
            'name': u'1.14 Мэргэжлийн% шинжлэх ухаан болон техникийн үйл ажиллагаа',
            'line_type': 'provide_1',
        }))
        provide_1.append((0, 0, {
            'name': u'1.15 Захиргааны болон дэмжлэг үзүүлэх үйл ажиллагаа',
            'line_type': 'provide_1',
        }))
        provide_1.append((0, 0, {
            'name': u'1.16 Төрийн удирдлага ба батлан хамгаалах үйл ажиллагаа, албан журмын нийгмийн хамгаалал',
            'line_type': 'provide_1',
        }))
        provide_1.append((0, 0, {
            'name': u'1.17 Боловсрол',
            'line_type': 'provide_1',
        }))
        provide_1.append((0, 0, {
            'name': u'1.18 Хүний эрүүл мэнд, нийгмийн үйл ажиллагаа',
            'line_type': 'provide_1',
        }))
        provide_1.append((0, 0, {
            'name': u'1.19 Бусад',
            'line_type': 'provide_1',
        }))
        provide_1.append((0, 0, {
            'name': u'Үүнээс: Хэрэглээ, цалин, тэтгэвэр, хадгаламж барьцаалсан зээл',
            'line_type': 'provide_1',
        }))

        provide_2.append((0, 0, {
            'name': u'2.1 Хөдөө аж ахуй, ойн аж ахуй, загас барилт, ан агнуур',
            'line_type': 'provide_2',
        }))
        provide_2.append((0, 0, {
            'name': u'2.2 Уул уурхай, олборлолт',
            'line_type': 'provide_2',
        }))
        provide_2.append((0, 0, {
            'name': u'2.3 Боловсруулах үйлдвэрлэл',
            'line_type': 'provide_2',
        }))
        provide_2.append((0, 0, {
            'name': u'2.4 Цахилгаан, хий, уур, агааржуулалтын хангамж',
            'line_type': 'provide_2',
        }))
        provide_2.append((0, 0, {
            'name': u'2.5 Усан хангамж, бохир ус, хог хаягдлын менежмент болон цэвэрлэх үйл ажиллагаа',
            'line_type': 'provide_2',
        }))
        provide_2.append((0, 0, {
            'name': u'2.6 Барилга',
            'line_type': 'provide_2',
        }))
        provide_2.append((0, 0, {
            'name': u'2.7 Бөөний болон жижиглэн худалдаа',
            'line_type': 'provide_2',
        }))
        provide_2.append((0, 0, {
            'name': u'2.8 Машин мотоциклийн засвар үйлчилгээ',
            'line_type': 'provide_2',
        }))
        provide_2.append((0, 0, {
            'name': u'2.9 Тээвэр ба агуулахын үйл ажиллагаа',
            'line_type': 'provide_2',
        }))
        provide_2.append((0, 0, {
            'name': u'2.10 Байр сууц болон хоол хүнсээр үйлчлэх үйл ажиллагаа',
            'line_type': 'provide_2',
        }))
        provide_2.append((0, 0, {
            'name': u'2.11 Мэдээлэл холбоо',
            'line_type': 'provide_2',
        }))
        provide_2.append((0, 0, {
            'name': u'2.12 Санхүүгийн болон даатгалын үйл ажиллагаа',
            'line_type': 'provide_2',
        }))
        provide_2.append((0, 0, {
            'name': u'2.13 Үл хөдлөх хөрөнгийн үйл ажиллагаа',
            'line_type': 'provide_2',
        }))
        provide_2.append((0, 0, {
            'name': u'2.14 Мэргэжлийн, шинжлэх ухаан болон техникийн үйл ажиллагаа',
            'line_type': 'provide_2',
        }))
        provide_2.append((0, 0, {
            'name': u'2.15 Захиргааны болон дэмжлэг үзүүлэх үйл ажиллагаа',
            'line_type': 'provide_2',
        }))
        provide_2.append((0, 0, {
            'name': u'2.16 Төрийн удирдлага ба батлан хамгаалах үйл ажиллагаа, албан журмын нийгмийн хамгаалал',
            'line_type': 'provide_2',
        }))
        provide_2.append((0, 0, {
            'name': u'2.17 Боловсрол',
            'line_type': 'provide_2',
        }))
        provide_2.append((0, 0, {
            'name': u'2.18 Хүний эрүүл мэнд, нийгмийн үйл ажиллагаа',
            'line_type': 'provide_2',
        }))
        provide_2.append((0, 0, {
            'name': u'2.19 Бусад',
            'line_type': 'provide_2',
        }))
        provide_2.append((0, 0, {
            'name': u'Үүнээс:  Хэрэглээ, цалин, тэтгэвэр,хадгаламж барьцаалсан зээл',
            'line_type': 'provide_2',
        }))

        provide_3.append((0, 0, {
            'line_type': 'provide_3',
            'name': u'1 сар хүртэлх хугацаатай олгосон зээл',
        }))
        provide_3.append((0, 0, {
            'line_type': 'provide_3',
            'name': u'1-3 сарын хугацаатай олгосон зээл',
        }))
        provide_3.append((0, 0, {
            'line_type': 'provide_3',
            'name': u'3-6 сарын хугацаатай олгосон зээл',
        }))
        provide_3.append((0, 0, {
            'line_type': 'provide_3',
            'name': u'6-12 сарын хугацаатай олгосон зээл',
        }))
        provide_3.append((0, 0, {
            'line_type': 'provide_3',
            'name': u'1-2.5 жилийн хугацаатай олгосон зээл',
        }))
        provide_3.append((0, 0, {
            'line_type': 'provide_3',
            'name': u'2.6-5 жилийн хугацаатай олгосон зээл',
        }))
        provide_3.append((0, 0, {
            'line_type': 'provide_3',
            'name': u'5-аас дээш жилийн хугацаатай олгосон зээл',
        }))

        provide_4_1.append((0, 0, {
            'line_type': 'provide_4_1',
            'name': u'Хэрэглээнд зориулсан зээл',
        }))
        provide_4_1.append((0, 0, {
            'line_type': 'provide_4_1',
            'name': u'Бизнесд зориулсан зээл',
        }))
        provide_4_1.append((0, 0, {
            'line_type': 'provide_4_1',
            'name': u'Автомашинд зориулсан зээл',
        }))
        provide_4_1.append((0, 0, {
            'line_type': 'provide_4_1',
            'name': u'Үл хөдлөх эд хөрөнгөд зориулсан зээл',
        }))
        provide_4_1.append((0, 0, {
            'line_type': 'provide_4_1',
            'name': u'Бусад зээл',
        }))

        provide_4_2.append((0, 0, {
            'line_type': 'provide_4_2',
            'name': u'Бизнесийн зээл',
        }))
        provide_4_2.append((0, 0, {
            'line_type': 'provide_4_2',
            'name': u'Автомашины зээл',
        }))
        provide_4_2.append((0, 0, {
            'line_type': 'provide_4_2',
            'name': u'Үл хөдлөх эд хөрөнгийн зээл',
        }))
        provide_4_2.append((0, 0, {
            'line_type': 'provide_4_2',
            'name': u'Бусад зээл',
        }))
        vals.update({
            'statistic_1': statistic_1,
            'statistic_2': statistic_2,
            'statistic_3': statistic_3,
            'statistic_4': statistic_4,
            'statistic_6': statistic_6,
            'statistic_7': statistic_7,
            'statistic_8': statistic_8,
            'statistic_9': statistic_9,
            'statistic_10': statistic_10,
            'statistic_11': statistic_11,
            'statistic_13': statistic_13,
            'statistic_14': statistic_14,
            'statistic_15': statistic_15,
            'statistic_16': statistic_16,
            'statistic_17_1': statistic_17_1,
            'statistic_17_2': statistic_17_2,
            'statistic_18': statistic_18,
            'forex_1': forex_1,
            'forex_2': forex_2,
            'forex_3': forex_3,
            'alm_1': alm_1,
            'alm_2': alm_2,
            'money_transfer': money_transfer,
            'ipotec': ipotec,
            'provide_1': provide_1,
            'provide_2': provide_2,
            'provide_3': provide_3,
            'provide_4_1': provide_4_1,
            'provide_4_2': provide_4_2,
        })
        return super(LoanOrderSeasonalReport, self).create(vals)

    @api.multi
    def action_export(self):
        thin_border = Border(left=Side(style='thin'),
                             right=Side(style='thin'),
                             top=Side(style='thin'),
                             bottom=Side(style='thin'))

        company = self.company_id
        # Анхны хоосон загвар файл авах
        template_file = get_module_resource('l10n_mn_loan_management_seasonal_report', 'static/src/report',
                                            'NBFI_in.xlsx')
        # Анхны хоосон загвар файлд утга нэмж хадгалах файл
        generate_file = get_module_resource('l10n_mn_loan_management_seasonal_report', 'static/src/report',
                                            'NBFI_out.xlsx')
        if template_file:
            # Эксель файлаа нээх
            wb = openpyxl.load_workbook(template_file)
            sheets = wb.active
            # Sheet -үүдээ зарлана
            caution_sheet = wb['!']
            statistic_sheet = wb['Statistic']
            balance_sheet = wb['BALANCE']
            balancesheet_sheet = wb['BALANCESHEET']
            income_sheet = wb['INCOME']
            capital_sheet = wb['Capital']
            liquidity_sheet = wb['Liquidity']
            forex_sheet = wb['Forex']
            provision_sheet = wb['Provision']
            alm_sheet = wb['ALM']
            top_sheet = wb['Top40']
            insider_sheet = wb['Insider']
            source_sheet = wb['Source']
            provide_sheet = wb['Provide']
            ipotec_sheet = wb['Ipotec']
            securities_sheet = wb['Securities']
            bond_sheet = wb['Bond']
            trust_sheet = wb['Trust']
            off_balance_sheet = wb['Off-balance']
            money_transfer_sheet = wb['Money transfer']
            ratio_sheet = wb['Ratio']
            # Сонгосон sheet-н мөрүүдрүү хандах
            balance_rows = balance_sheet.rows
            r = 2
            # Мөр баганаа тохируулж ижил дансны кодтой бол утга онооноо
            for row in balance_sheet.iter_rows(min_row=2, max_col=4, max_row=365, values_only=True):
                if unicode(row[2]) == '1102':
                    balance_sheet.cell(row=r, column=4).value = 1000
                r = r + 1

            for row in balance_sheet.iter_rows(min_row=2, max_col=4, max_row=365, values_only=True):
                if unicode(row[2]) == '1102':
                    print unicode(row[3]), '<<<<< TEMPLATE FILE'
            wb.save(generate_file)
            for row in wb['BALANCE'].iter_rows(min_row=2, max_col=4, max_row=365, values_only=True):
                if unicode(row[2]) == '1102':
                    print unicode(row[3]), '<<<<< GENERATED FILE'
            # Файлын нэр
            file_name = "NBFI_%s.xlsx" % (time.strftime('%Y%m%d_%H%M'),)
            # Анхны хоосон загвар файлд утга нэмж хадгалж байна
            report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix='NBFI',
                                                                                         form_title=file_name).create(
                {})
            image = open(generate_file, 'rb')
            image_read = image.read()
            report_excel_output_obj.filedata = base64.encodestring(image_read)

            return report_excel_output_obj.export_repoert()
