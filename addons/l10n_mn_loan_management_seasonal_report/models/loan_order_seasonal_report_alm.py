# -*- encoding: utf-8 -*-
from odoo import fields, models, _, api
from odoo.exceptions import UserError, ValidationError
from xlsxwriter.utility import xl_cell_to_rowcol


class LoanOrderSeasonalReportAlm(models.Model):
    _name = 'loan.order.seasonal.report.alm'

    def _get_selection(self):
        selection = []
        for obj in range(1, 3):
            selection.append(('alm_' + str(obj), 'alm_' + str(obj)))
        return selection

    line_type = fields.Selection(_get_selection)
    report_id = fields.Many2one('loan.order.seasonal.report', ondelete='cascade', string='Report')
    name = fields.Char('Name')
    undated = fields.Char('Undated')
    undated_cell_value = fields.Char()
    month_1 = fields.Char('Up to 1 month')
    month_1_cell_value = fields.Char()
    month_1_3 = fields.Char('Between 1 to 3 months')
    month_1_3_cell_value = fields.Char()
    month_3_6 = fields.Char('Between 3 to 6 months')
    month_3_6_cell_value = fields.Char()
    month_6_12 = fields.Char('Between 6 to 12 months')
    month_6_12_cell_value = fields.Char()
    year_1_3 = fields.Char('Between 1 to 3 years')
    year_1_3_cell_value = fields.Char()
    year_3_5 = fields.Char('Between 3 to 5 years')
    year_3_5_cell_value = fields.Char()
    year_5 = fields.Char('More than 5 years')
    year_5_cell_value = fields.Char()
