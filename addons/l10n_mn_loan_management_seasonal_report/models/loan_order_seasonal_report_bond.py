# -*- encoding: utf-8 -*-
from odoo import fields, models, _, api
from odoo.exceptions import UserError, ValidationError
from xlsxwriter.utility import xl_cell_to_rowcol


class LoanOrderSeasonalReportBond(models.Model):
    _name = 'loan.order.seasonal.report.bond'

    partner_id = fields.Many2one('res.partner', string='Loaner')
    bond_payment_amount = fields.Float('Bont payment amount')
    interest_rate_month = fields.Float('interest rate /month/')
    date_month = fields.Integer('Date /Month/')
    date_of_issue = fields.Date('Date of issue')
    date_of_payment = fields.Date('Date of payment')
    balance = fields.Float('Balance')
    report_id = fields.Many2one('loan.order.seasonal.report', ondelete='cascade', string='Report')

