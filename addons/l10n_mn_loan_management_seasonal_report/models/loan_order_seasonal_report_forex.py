# -*- encoding: utf-8 -*-
from odoo import fields, models, _, api
from odoo.exceptions import UserError, ValidationError
from xlsxwriter.utility import xl_cell_to_rowcol


class LoanOrderSeasonalReportForex(models.Model):
    _name = 'loan.order.seasonal.report.forex'

    def _get_selection(self):
        selection = []
        for obj in range(1, 4):
            selection.append(('forex_' + str(obj), 'forex_' + str(obj)))
        return selection

    line_type = fields.Selection(_get_selection)
    name = fields.Char(string='Name')
    report_id = fields.Many2one('loan.order.seasonal.report', ondelete='cascade', string='Report')
    value_usd = fields.Char('USD')
    cell_value_usd = fields.Char('Cell value USD')
    value_eur = fields.Char('EUR')
    cell_value_eur = fields.Char('Cell value EUR')
    value_jpy = fields.Char('JPY')
    cell_value_jpy = fields.Char('Cell value JPY')
    value_chf = fields.Char('CHF')
    cell_value_chf = fields.Char('Cell value CHF')
    value_gbp = fields.Char('GBP')
    cell_value_gbp = fields.Char('Cell value GBP')
    value_cny = fields.Char('CNY')
    cell_value_cny = fields.Char('Cell value CNY')
    value_rub = fields.Char('RUB')
    cell_value_rub = fields.Char('Cell value RUB')
    value_krw = fields.Char('KRW')
    cell_value_krw = fields.Char('Cell value KRW')
    value_other = fields.Char('Other')
    cell_value_other = fields.Char('Cell value Other')
