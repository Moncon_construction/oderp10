# -*- encoding: utf-8 -*-
from odoo import fields, models, _, api
from odoo.exceptions import UserError, ValidationError
from xlsxwriter.utility import xl_cell_to_rowcol


class LoanOrderSeasonalReportInsider(models.Model):
    _name = 'loan.order.seasonal.report.insider'

    partner_id = fields.Many2one('res.partner', string='Loaner')
    dependence = fields.Selection([('shareholder', 'Shareholder'),
                                  ('bod_member', 'Bod member'),
                                  ('ceo', 'CEO'),
                                  ('employee', 'Employee'),
                                  ('other', 'Other members with relation'), ], string='Relation of loaner')
    initial_of_assets = fields.Float('Initial of assets')
    interest_rate_month = fields.Float('interest rate /month/')
    purpose = fields.Char('Purpose')
    date_of_issue = fields.Date('Date of issue')
    date_of_payment = fields.Date('Date of payment')
    date_of_payment_extension = fields.Date('Date of payment extension')
    collateral_asset_type = fields.Selection([('vehicle', 'Vehicle'),
                                              ('trading_bond', 'Trading bond'),
                                              ('estate', 'Other real estate'),
                                              ('asset', 'Movable asset'),
                                              ('savings', 'Savings'),
                                              ('housing', 'Housing'),
                                              ('jewelry', 'Jewelry'),
                                              ('none', 'No collateral'), ], string='Type of collateral')
    collateral_asset_amount = fields.Float('Collateral asset amount')
    final_balance_of_assets = fields.Float('Final balance of assets')
    currency_type = fields.Selection([('tugrug', 'Tugrug'),
                                     ('currency', 'Currency')],
                                     string='MNT loan or foreign currency loan')
    type_of_asset = fields.Selection([('normal', 'Normal'),
                                      ('overdue', 'Overdue'),
                                      ('abnormal', 'Abnormal'),
                                      ('doubtful', 'Doubtful'),
                                      ('bad', 'Bad'), ], string='Classification of assets')
    report_id = fields.Many2one('loan.order.seasonal.report', ondelete='cascade', string='Report')

