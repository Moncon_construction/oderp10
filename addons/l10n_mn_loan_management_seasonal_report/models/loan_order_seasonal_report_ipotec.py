# -*- encoding: utf-8 -*-
from odoo import fields, models, _, api
from odoo.exceptions import UserError, ValidationError
from xlsxwriter.utility import xl_cell_to_rowcol


class LoanOrderSeasonalReportIpotec(models.Model):
    _name = 'loan.order.seasonal.report.ipotec'

    name = fields.Char('Name')
    own_expense = fields.Float('Own expense')
    housing_ipotec_funding = fields.Float('Housing ipotec funding')
    ipotec_loan_refunding = fields.Float('Ipotec loan refunding')
    ossk_funding = fields.Float('Ossk funding')
    adb_funding = fields.Float('Adb funding')
    currency_own_expense = fields.Float('Currency own expense')
    currency_source = fields.Float('....Currency source')
    report_id = fields.Many2one('loan.order.seasonal.report', ondelete='cascade', string='Report')
