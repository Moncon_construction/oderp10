# -*- encoding: utf-8 -*-
from odoo import fields, models, _, api
from odoo.exceptions import UserError, ValidationError
from xlsxwriter.utility import xl_cell_to_rowcol


class LoanOrderSeasonalReportMoneyTransfer(models.Model):
    _name = 'loan.order.seasonal.report.money.transfer'

    name = fields.Char('Currency type')
    currency_amount_transferred = fields.Float('Currency amount transferred')
    transaction_number_transferred = fields.Float('Transaction number transferred')
    converted_currency_amount_transferred = fields.Float('Converted currency amount transferred')
    currency_amount_received = fields.Float('Currency amount received')
    transaction_number_received = fields.Float('Transaction number received')
    converted_currency_amount_received = fields.Float('Converted currency amount received')
    report_id = fields.Many2one('loan.order.seasonal.report', ondelete='cascade', string='Report')

