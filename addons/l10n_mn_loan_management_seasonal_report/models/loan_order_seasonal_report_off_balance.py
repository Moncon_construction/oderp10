# -*- encoding: utf-8 -*-
from odoo import fields, models, _, api
from odoo.exceptions import UserError, ValidationError
from xlsxwriter.utility import xl_cell_to_rowcol


class LoanOrderSeasonalReportBalance(models.Model):
    _name = 'loan.order.seasonal.report.off.balance'

    def _get_selection(self):
        selection = []
        for obj in range(1, 4):
            selection.append(('off_balance_' + str(obj), 'off_balance_' + str(obj)))
        return selection

    line_type = fields.Selection(_get_selection)
    account_id = fields.Many2one('account.account', string='Account number')
    business_name = fields.Char('Business name')
    register = fields.Char('Register number')
    state_registration_number = fields.Char('State registration number')
    warranty_purpose = fields.Char('Warranty purpose')
    business_ceo_name = fields.Char('Business ceo name')
    register_num = fields.Char('Register number')
    relation_of_nbfi = fields.Char('Relation to nbfi*')
    date_of_issue = fields.Date('Date of issue')
    date_of_payment = fields.Date('Date of payment')
    date_of_extension = fields.Date('Date of extension')
    payment = fields.Float('Tugrug')
    payment_currency = fields.Float('Currency')
    report_id = fields.Many2one('loan.order.seasonal.report', ondelete='cascade', string='Report')

