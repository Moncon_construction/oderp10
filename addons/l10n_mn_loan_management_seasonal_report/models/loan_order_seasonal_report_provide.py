# -*- encoding: utf-8 -*-
from odoo import fields, models, _, api
from odoo.exceptions import UserError, ValidationError
from xlsxwriter.utility import xl_cell_to_rowcol


class LoanOrderSeasonalReportProvide(models.Model):
    _name = 'loan.order.seasonal.report.provide'

    def _get_selection(self):
        selection = []
        for obj in range(1, 5):
            if obj != 4:
                selection.append(('provide_' + str(obj), 'provide_' + str(obj)))
            else:
                selection.append(('provide_' + str(obj) + '_1', 'provide_' + str(obj) + '_1'))
                selection.append(('provide_' + str(obj) + '_2', 'provide_' + str(obj) + '_2'))
        return selection

    line_type = fields.Selection(_get_selection)
    name = fields.Char('Name')
    report_id = fields.Many2one('loan.order.seasonal.report', ondelete='cascade', string='Report')
    amount_initial = fields.Float('Amount initial')
    quantity_initial = fields.Float('Quantity initial')
    normal_initial = fields.Float('Normal initial')
    overdue_initial = fields.Float('Overdue initial')
    poor_quality_initial = fields.Float('Poor quality initial')
    amount_issued = fields.Float('Amount issued')
    quantity_issued = fields.Float('Quantity issued')
    date_zjdh = fields.Date('Date zjdh')
    amount_paid = fields.Float('Amount paid')
    quantity_paid = fields.Float('Quantity paid')
    currency_loan_difference = fields.Float('Currency loan difference')
    quantity_balance = fields.Float('Quantity balance')
    normal_balance = fields.Float('Normal balance')
    overdue_balance = fields.Float('Overdue balance')
    poor_quality_balance = fields.Float('Poor quality balance')
    top_interest = fields.Float('Top interest')
    bottom_interest = fields.Float('Bottom interest')
