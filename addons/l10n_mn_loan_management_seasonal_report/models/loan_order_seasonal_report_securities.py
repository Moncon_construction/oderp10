# -*- encoding: utf-8 -*-
from odoo import fields, models, _, api
from odoo.exceptions import UserError, ValidationError
from xlsxwriter.utility import xl_cell_to_rowcol


class LoanOrderSeasonalReportSecurities(models.Model):
    _name = 'loan.order.seasonal.report.securities'

    name = fields.Char(string='Type name')
    purchase_amount = fields.Float('Purchase amount')
    date_of_issue = fields.Date('Purchase date')
    sale_date = fields.Date('Sale date')
    period_balance = fields.Float('Period balance')
    amount_shares_of_company = fields.Float(string='Total amount of shares issued by the company')
    report_id = fields.Many2one('loan.order.seasonal.report', ondelete='cascade', string='Report')

