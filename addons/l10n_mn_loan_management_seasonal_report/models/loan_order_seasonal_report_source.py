# -*- encoding: utf-8 -*-
from odoo import fields, models, _, api
from odoo.exceptions import UserError, ValidationError
from xlsxwriter.utility import xl_cell_to_rowcol


class LoanOrderSeasonalReportSource(models.Model):
    _name = 'loan.order.seasonal.report.source'

    partner_id = fields.Many2one('res.partner', string='Loaner')
    initial_of_loan = fields.Float('Initial of loan')
    interest_rate_month = fields.Float('interest rate /month/')
    date_of_issue = fields.Date('Date of receipt')
    date_of_payment = fields.Date('Date of payment')
    date_of_extension = fields.Date('Date of extension')
    type_of_asset = fields.Selection([('normal', 'Normal'),
                                      ('overdue', 'Overdue'),
                                      ('abnormal', 'Abnormal'),
                                      ('doubtful', 'Doubtful'),
                                      ('bad', 'Bad'), ], string='Type of loan')
    collateral_asset = fields.Char('Collateral asset')
    loan_balance = fields.Float('Loan balance')
    overdue_loan_debt = fields.Float('Overdue loan debt')
    report_id = fields.Many2one('loan.order.seasonal.report', ondelete='cascade', string='Report')

