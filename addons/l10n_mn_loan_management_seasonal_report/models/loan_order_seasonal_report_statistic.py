# -*- encoding: utf-8 -*-
from odoo import fields, models, _, api
from odoo.exceptions import UserError, ValidationError
from xlsxwriter.utility import xl_cell_to_rowcol


class LoanOrderSeasonalReportStatistic(models.Model):
    _name = 'loan.order.seasonal.report.statistic'

    def _get_selection(self):
        selection = []
        for obj in range(1, 19):
            if obj == 17:
                selection.append(('statistic_1_' + str(obj) + '_1', 'statistic_1_' + str(obj) + '_1'))
                selection.append(('statistic_1_' + str(obj) + '_2', 'statistic_1_' + str(obj) + '_2'))
            else:
                selection.append(('statistic_1_' + str(obj), 'statistic_1_' + str(obj)))
        return selection

    name = fields.Char('Name', required=True)
    cell_value = fields.Char('Cell value')
    value = fields.Char('Value')
    report_id = fields.Many2one('loan.order.seasonal.report', ondelete='cascade', string='Report')
    line_type = fields.Selection(_get_selection)

    @api.constrains('cell_value')
    def check_cell_value(self):
        try:
            row_col = xl_cell_to_rowcol(self.cell_value.upper())
        except Exception as ex:
            raise ValidationError(_('%s is not excel cell') % self.cell_value)
        return bool(row_col)
