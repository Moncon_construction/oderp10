# -*- encoding: utf-8 -*-
from odoo import fields, models, _, api
from odoo.exceptions import UserError, ValidationError
from xlsxwriter.utility import xl_cell_to_rowcol


class LoanOrderSeasonalReportTrust(models.Model):
    _name = 'loan.order.seasonal.report.trust'

    partner_id = fields.Many2one('res.partner', string='Trustee')
    payment_amount = fields.Float('Payment amount')
    date_year = fields.Integer('Date year')
    interest_rate_year = fields.Float('Interest rate year')
    date_of_issue = fields.Date('Date of issue')
    date_of_payment = fields.Date('Date of payment')
    date_of_extension = fields.Date('Date of extension')
    payment_balance = fields.Float('Payment balance')
    report_id = fields.Many2one('loan.order.seasonal.report', ondelete='cascade', string='Report')

