# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': "Mongolian Loan & Credit Information System Connector",
    'version': '1.0',
    'depends': ['l10n_mn_contacts_extra', 'l10n_mn_loan_management'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
        Монголбанкны Зээлийн мэдээллийн системтэй холбогч модуль
    """,
    'website': 'http://asterisk-tech.mn',
    'data': [
        # Эрхийн тохиргоо
        "security/loan_security.xml",
        "security/ir.model.access.csv",
        # Үндсэн болон тохиргооны харагдацууд
        "data/ir_config_parameter_data.xml",
        "views/sequence_view.xml",
        "views/ir_config_parameter_view.xml",
        "views/res_partner_view.xml",
        "views/loan_schedule_view.xml",
        "views/loan_collateral_view.xml",
        "views/loan_zms_order_view.xml",
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
