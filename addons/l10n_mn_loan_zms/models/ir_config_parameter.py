# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class IrConfigParameter(models.Model):
    _inherit = "ir.config_parameter"

    is_zms = fields.Boolean('Just for ZMS & Loan', default=True)
    user_code = fields.Char(u'Хэрэглэгчийн код')
    company_code = fields.Char(u'Байгууллагын код')
    branch_code = fields.Char(u'Салбарын код')
    user_password = fields.Char(u'Мэдээлэл нийлүүлэх нууц үг')