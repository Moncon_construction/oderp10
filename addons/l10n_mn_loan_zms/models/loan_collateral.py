# -*- coding: utf-8 -*-
from odoo import models, fields


class LoanCollateral(models.Model):
    _inherit = "loan.collateral"

    collateral_identification = fields.Char('Collateral ID', size=30)
    name_of_registered_agencie = fields.Selection([('1', 'EHEUB'),
                                 ('2', 'Other'),
                                 ('3', 'State Center for Defense'),
                                 ('4', 'Ulaanbaatar Railway Authority')], string='Name of the registered authority')
    confirmed_of_registered_agencie = fields.Date(string='Date of Registration / Acceptance')
    confirmed_id_of_registered_agencie = fields.Char(string='Registration / Authorization Number')
    certification_id_of_registered_agencie = fields.Char(string='Certification ID')
    owner_id = fields.Many2one('res.partner', 'Owner')  # Эзэмшигч