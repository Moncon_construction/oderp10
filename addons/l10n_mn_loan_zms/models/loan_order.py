# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError

import hashlib
import requests
from requests.auth import HTTPBasicAuth


class LoanOrder(models.Model):
    _inherit = 'loan.order'

    def add_customer_data(self, send_type):
        ir_config_obj = self.env['ir.config_parameter']
        zms_info_id = ir_config_obj.search([('key', '=', 'loan_zms')])
        if zms_info_id:
            for obj in self:
                # Байгууллага эсэх
                is_organization = 1
                if obj.partner_id.company_type == 'person':
                    customer_prefix = '01'
                else:
                    customer_prefix = '03'
                    is_organization = 0

                # Гадаадын иргэн эсэх
                is_foreigner = 1
                if obj.partner_id.debtor_code == 2:
                    is_foreigner = 0

                # ЗМС-д мэдээлэл нийлүүлэх мэдээлэл XML загварт оруулж байна.
                body_xml = """<?xml version='1.0' encoding='utf-8'?>
                                <customers>
                                     <datapackageno>%s</datapackageno><!-- Зээлийн дугаар -->
                                     <customer action="%s">
                                         <o_c_customer_information action="%s">
                                            <o_c_customercode>%s</o_c_customercode><!-- Зээлдэгчийн код -->
                                            <o_c_bankCode>%s</o_c_bankCode><!-- Зээл олгосон банкны/байгууллагын код -->
                                            <o_c_branchcode>%s</o_c_branchcode><!-- Салбарын код -->
                                            <o_c_isorganization>%s</o_c_isorganization><!-- Байгууллага эсэх -->
                                            <o_c_customername>%s</o_c_customername><!-- Байгууллага, иргэний нэр -->
                                            <c_lastname>%s</c_lastname><!-- Иргэний эцгийн нэр -->
                                            <o_c_isforeign>%s</o_c_isforeign><!-- Монгол улсад бүртгэгдсэн эсэх,Монгол улсын иргэн эсэх -->
                                            <o_c_address>%s</o_c_address><!-- Албан ёсны хаяг -->
                                            <o_c_registerno>%s</o_c_registerno><!-- Байгууллага иргэний регистерийн дугаар/ID дугаар -->
                                         </o_c_customer_information>
                                         <o_c_onus_information>
                                            <o_c_loan_information action="%s">
                                                <o_c_loan_provideLoanSize>%s</o_c_loan_provideLoanSize><!-- Олгосон зээлийн хэмжээ -->
                                                <o_c_loan_balance>%s</o_c_loan_balance><!-- Зээлийн үлдэгдэл -->
                                                <o_c_loan_loanProvenance>%s</o_c_loan_loanProvenance><!-- Зээлийн үүсэл -->
                                                <o_c_loan_starteddate>%s</o_c_loan_starteddate><!-- Олгосон огноо -->
                                                <o_c_loan_expdate>%s</o_c_loan_expdate><!-- Төлөгдөх огноо -->
                                                <o_c_loan_currencycode>%s</o_c_loan_currencycode><!-- Зээл олгосон валютын нэр -->
                                                <o_c_loan_sectorcode>%s</o_c_loan_sectorcode><!-- Зээлийн зориулалт -->
                                                <o_c_loan_interestinperc>%s</o_c_loan_interestinperc><!-- Зээлийн хүүгийн хувь -->
                                                <o_c_loan_commissionperc>%s</o_c_loan_commissionperc><!-- Зээлийн шимтгэлийн хувь -->
                                                <o_c_loan_fee>%s</o_c_loan_fee><!-- Зээлийн хураамжийн хэмжээ -->
                                                <o_c_loan_updatedexpdate>%s</o_c_loan_updatedexpdate><!-- Зээл төлөгдөх шинэчилсэн огноо -->
                                                <o_c_loan_loanclasscode>%s</o_c_loan_loanclasscode><!-- Зээлийн ангилал -->
                                            </o_c_loan_information>
                                        </o_c_onus_information>
                                     </customer>
                                 </customers>""" % (
                    # ***<customers>***
                    str(obj.name),  # datapackageno
                    str(send_type),  # action type
                    str(send_type),  # action type
                    # Харилцагчийн мэдээлэл
                    #   <o_c_customer_information action="add">
                    str(customer_prefix) + str(obj.partner_id.register),  # o_c_customercode
                    str(zms_info_id.company_code),  # o_c_bankCode
                    str(zms_info_id.branch_code),  # o_c_branchcode
                    str(is_organization),  # o_c_isorganization
                    str(obj.partner_id.name),  # o_c_customername
                    str(obj.partner_id.surname),  # c_lastname
                    str(is_foreigner),  # o_c_isforeign
                    str(obj.partner_id.official_address),  # o_c_address
                    str(obj.partner_id.register),  # o_c_registerno
                    #   </o_c_customer_information>
                    # Зээлийн мэдээлэл
                    # *<o_c_loan_information action="add">*
                    str(send_type),  # action type
                    str(obj.amount),  # o_c_loan_provideLoanSize
                    str(obj.loan_balance),  # o_c_loan_balance
                    str(obj.loan_state.sequence),  # o_c_loan_loanProvenance 01
                    str(obj.date_approve),  # o_c_loan_starteddate
                    str(obj.close_date),  # o_c_loan_expdate
                    str(obj.currency_id.name),  # o_c_loan_currencycode
                    str(obj.loan_purpose_id.code),  # o_c_loan_sectorcode
                    str(obj.interest_by_year),  # o_c_loan_interestinperc
                    str(obj.interest_commission_percent),  # o_c_loan_commissionperc
                    str(obj.loan_fee_amount),  # o_c_loan_fee
                    str(obj.close_date),  # o_c_loan_updatedexpdate
                    str(obj.loan_state.sequence),  # o_c_loan_loanclasscode
                )
            return body_xml

    def get_zms_config(self):
        ir_config_obj = self.env['ir.config_parameter']
        zms_info_id = ir_config_obj.search([('key', '=', 'loan_zms')])
        if zms_info_id:
            host = zms_info_id.value
            username = str(zms_info_id.user_code)
            password = hashlib.md5(str(zms_info_id.user_password))
            params = {'grant_type': 'client_credentials'}
            headers = {
                "Content-type": "text/xml; charset=\"UTF-8\"",
                "X-Username": username,
                "X-Signature": str(password.hexdigest())
            }
            return host, username, password, params, headers
        else:
            raise UserError(_('ZMS configuration not found!'))

    @api.one
    @api.model
    def send_data(self, send_type):
        host, username, password, params, headers = self.get_zms_config()
        response = requests.post(host, auth=HTTPBasicAuth(username, str(password)), params=params,
                                 data=self.add_customer_data(send_type),
                                 headers=headers,
                                 verify=False)
        if response.status_code == 200:
            return response
        else:
            raise UserError(response.text)
