# -*- coding: utf-8 -*-
from odoo import models, fields, api

class LoanSchedule(models.Model):
    _inherit = "loan.schedule"

    state = fields.Selection([
            ('1', 'New'),
            ('2', 'Schedule sent'),
            ('3', 'Execution sent'),
            ], string='State')
