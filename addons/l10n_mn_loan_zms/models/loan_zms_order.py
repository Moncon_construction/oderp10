# -*- coding: utf-8 -*-
from datetime import datetime
import requests

from requests.auth import HTTPBasicAuth
from odoo import models, fields, api, _
from odoo.exceptions import UserError


class LoanZMSOrder(models.Model):
    _name = 'loan.zms.order'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = '"Mongol-Bank" Integration Form'

    name = fields.Char('Name', size=64, required=True, copy=False, default='/', track_visibility='onchange')
    sent_date = fields.Datetime('Sent date', track_visibility='onchange', copy=False)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirmed', 'Confirmed'),
        ('sent', 'Sent'),
        ('done', 'Done'),
        ('cancel', 'Cancelled')
    ], 'States', default='draft', track_visibility='onchange')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id, required=True)
    loan_order_ids = fields.Many2many('loan.order', required=True, track_visibility='onchange')
    action = fields.Selection([
        ('add', 'Add'),
        ('update', 'Update')], 'Action', default='add', track_visibility='onchange', required=True)
    response_text = fields.Text('Response', track_visibility='onchange')

    # Монголбанкны ЗМС ажиллаж байгаа эсэхийг
    # Холболт шалгах товч
    def action_check(self):
        host, username, password, params, headers = self.env['loan.order'].get_zms_config()
        try:
            response = requests.get(host, auth=HTTPBasicAuth(username, str(password)), params=params,
                                    headers=headers, verify=False, timeout=3)
        finally:
            if response.status_code == 200:
                raise UserError(_('Connection successfully! %s' % response.status_code))
            else:
                raise UserError(_('Connection failed!'))

    # Зээлдэгч болон Зээлийн мэдээлэл шинээр үүсгэх болон шинэслэх функц
    @api.multi
    def action_send(self):
        for obj in self:
            obj.response_text = ''
            if obj.action == 'update':
                send_type = 'update'
            else:
                send_type = 'add'
            for order_id in obj.loan_order_ids:
                order_response = order_id.send_data(send_type)
                obj.response_text += u'Монголбанкны ЗМС-д зээлдэгч ' + order_id.partner_id.name + \
                                     u'-н зээлийн мэдээллийг нийлүүлэхэд ирсэн хариу: ' + order_response[0].text
        obj.state = 'sent'
        obj.sent_date = datetime.now()

    # Батлах функц
    @api.multi
    def action_confirm(self):
        for obj in self:
            obj.state = 'confirmed'
            obj.name = self.env['ir.sequence'].get('loan.zms.order')

    # Дуусгах функц
    @api.multi
    def action_done(self):
        for obj in self:
            obj.state = 'done'

    # Ноорог болгох
    @api.multi
    def action_draft(self):
        for obj in self:
            obj.state = 'draft'
