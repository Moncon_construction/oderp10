# -*- encoding: utf-8 -*-

from odoo import fields, models, api, _

class ResPartner(models.Model):
    _inherit = "res.partner"

    zms_xml_id = fields.Char('Request ID', size=5)
    debtor_code = fields.Selection([('1', 'Mongolian Citizen'), ('2', 'Foreigner'), ('3', 'Business organization'),
                                    ('4', 'Government organization'), ('5', 'Monastery'), ('6', 'Foundation')],
                                   string='Debtor Code', default='1')
    organization_code = fields.Char('Organization Code', size=10)
    branch_code = fields.Char('Branch Code', size=10)
    official_address = fields.Char('Official address', size=300)
    register = fields.Char('Register', size=16)
    manager_name = fields.Char('Manager Name', size=50)  # Гүйцэтгэх удирдлагын нэр, Гэр бүлийн нэр
    manager_root_name = fields.Char("Manager's Root Name",
                                    size=50)  # Гүйцэтгэх удирдлагын эцгийн нэр, Гэр бүлийн эцгийн нэр
    is_manager_mongolian_citizen = fields.Boolean('Is Manager Mongolian Citizen',
                                                  default=False)  # Гүйцэтгэх удирдлага Монгол улсын иргэн эсэх /0 - Монголын иргэн 1 - Гадаадын иргэн/

    collateral_owner_name = fields.Char('Collateral Owner Name', size=50)
    collateral_owner_father_name = fields.Char('Fathers full name of owner', size=50)
    is_collateral_owner_mongolian_citizen = fields.Boolean('Is Collateral Owner Mongolian Citizen')
