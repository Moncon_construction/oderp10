# -*- coding: utf-8 -*-
{
    'name': "Mongolian Low Value Asset",
    'version': '1.0',
    'depends': ['l10n_mn_product_expense', 'l10n_mn_report', 'mail'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Asset Modules',
    'description': """
    """,
    'data': [
        'security/low_value_asset_security.xml',
        'security/ir.model.access.csv',
        'views/low_value_asset_view.xml',
        'views/product_product_view.xml',
        'views/product_expense_view.xml',
        'views/low_value_asset_type_view.xml',
        'report/report_low_value_asset_move_view.xml',
        'report/low_value_asset_barcode.xml',
        'report/low_value_asset_qr.xml',
        'wizard/low_value_asset_transfer_view.xml',
        'views/low_value_asset_location_view.xml',
        'wizard/low_value_asset_sell_view.xml',
        'wizard/low_value_asset_scrap_confirm_wizard_view.xml',
        'wizard/mail_low_value_asset_wizard_view.xml',
        'report/low_value_asset_cart.xml',
        'report/low_value_asset_aging.xml',
        'data/email_to_low_value_asset_owner_cron.xml',
        'report/low_value_asset_detailed_report_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
