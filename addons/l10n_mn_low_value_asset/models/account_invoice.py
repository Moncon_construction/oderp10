# -*- coding: utf-8 -*-
from odoo import models, fields


class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'

    low_value_asset_id = fields.Many2one('low.value.asset', 'Low value asset')
