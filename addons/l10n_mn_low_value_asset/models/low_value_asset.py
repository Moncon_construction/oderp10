# -*- coding: utf-8 -*-
import urllib

from odoo import models, fields, api, _
from random import choice
from string import digits
from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError


class LowValueAsset(models.Model):
    _name = 'low.value.asset'
    _inherit = ['mail.thread']

    def name_get(self):
        result = []
        for record in self:
            name = '%s [%s]' % (record.product_id.name, record.barcode)
            result.append((record.id, name))
        return result

    def _default_random_barcode(self):
        barcode = None
        while not barcode or self.env['low.value.asset'].search([('barcode', '=', barcode)]) or int(barcode) < 10000000:
            barcode = ''.join(choice(digits) for i in range(8))
        return barcode

    @api.model
    def _compute_ownership_history(self):
        for record in self:
            record_ids = self.env['low.value.asset.history'].search([('low_value_asset_id', '=', record.id)])
            record.ownership_history_count = len(record_ids)

    @api.multi
    @api.depends('date_acquired', 'duration', 'scrapped_date', 'value')
    def _compute_usage_percent(self):
        for record in self:
            if record.date_acquired and record.duration:
                if record.scrapped_date:
                    used_days = (datetime.strptime(record.scrapped_date, '%Y-%m-%d').date() - datetime.strptime(record.date_acquired, '%Y-%m-%d').date()).days + 1
                    to_use_days = (datetime.strptime(record.date_acquired, '%Y-%m-%d').date() + relativedelta(months=record.duration) - datetime.strptime(record.date_acquired, '%Y-%m-%d').date()).days + 1
                    record.usage_percent = used_days * 100 / to_use_days
                    value_residual = record.value - record.value / 100 * record.usage_percent
                    if value_residual < 0:
                        value_residual = 0
                    record.value_residual = value_residual
                else:
                    used_days = (date.today() - datetime.strptime(record.date_acquired, '%Y-%m-%d').date()).days + 1
                    to_use_days = (datetime.strptime(record.date_acquired, '%Y-%m-%d').date() + relativedelta(months=record.duration) - datetime.strptime(record.date_acquired, '%Y-%m-%d').date()).days + 1
                    record.usage_percent = used_days * 100 / to_use_days
                    value_residual = record.value - round(record.value / 100 * used_days * 100 / to_use_days, 2)
                    if value_residual < 0:
                        value_residual = 0
                    record.value_residual = value_residual

    @api.multi
    @api.depends('location_id', 'barcode', 'date_acquired')
    def _compute_qr(self):
        for record in self:
            qr = '{"id": ' + format(record.id) + ',\n'
            qr += '"info": "'
            first = True
            if record.location_id:
                if not first:
                    qr += ', '
                data = format(record.location_id.name.replace('"', ''))
                data = format(data.replace('\'', ''))
                qr += data
                first = False
            if record.date_acquired:
                if not first:
                    qr += ', '
                data = format(record.date_acquired.replace('\'', ''))
                data = format(data.replace('"', ''))
                qr += data
                first = False
            if record.barcode:
                if not first:
                    qr += ', '
                data = format(record.barcode.replace('"', ''))
                data = format(data.replace('\'', ''))
                qr += data
                first = False
            qr += '"}'
            record.qr = qr

    product_id = fields.Many2one('product.product', 'Product', required=True, domain="[('is_low_value_asset','=',True)]")
    date_acquired = fields.Date('Date acquired', required=True)
    duration = fields.Integer('Duration /Month/')
    employee_id = fields.Many2one('hr.employee', 'Owner', required=True)
    type_id = fields.Many2one('low.value.asset.type', 'Type')
    expense_id = fields.Many2one('product.expense', 'Expense_id')
    is_defective = fields.Boolean('Defective asset', copy=False)
    barcode = fields.Char(string="Barcode", help="Barcode used for asset identification.", default=_default_random_barcode, copy=False, readonly=True)
    usage_percent = fields.Float(string='Usage Percent', compute='_compute_usage_percent')
    location_id = fields.Many2one('low.value.asset.location', 'Location')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('using', 'Using'),
        ('scrapped', 'Scrapped'),
        ('sold', 'Sold')], track_visibility='onchange', default='draft')
    scrapped_date = fields.Date('Scrapped date')
    ownership_history_count = fields.Integer('History', compute='_compute_ownership_history')
    qr = fields.Char('QR', compute=_compute_qr, store=True)
    value = fields.Float('Cost', required=True)
    value_residual = fields.Float('Value residual', compute='_compute_usage_percent', readonly=True)
    invoice_count = fields.Integer(compute='_invoice_count', string='Low value asset invoices')

    @api.multi
    def _invoice_count(self):
        for low_value_asset in self:
            inv_ids = []
            inv_lines = self.env['account.invoice.line'].search([('low_value_asset_id', '=', low_value_asset.id)])
            for line in inv_lines:
                if line.invoice_id:
                    inv_ids.append(line.invoice_id.id)
            low_value_asset.invoice_count = self.env['account.invoice'].search_count([('id', 'in', inv_ids)]) or 0

    @api.multi
    def open_invoice(self):
        # БҮТЭЗ борлуулахад үүссэн нэхэмжлэх  рүү орох
        inv_ids = []
        inv_lines = self.env['account.invoice.line'].search([('low_value_asset_id', '=', self.id)])
        for line in inv_lines:
            if line.invoice_id:
                inv_ids.append(line.invoice_id.id)
        return {
            'name': _('Invoice'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.invoice',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', inv_ids)],
        }

    @api.constrains('duration')
    def _check_duration(self):
        if not self.duration:
            raise UserError(_('Duration must be greater than 0.'))

    @api.multi
    def confirm(self):
        self._check_duration()
        low_value_asset_history_obj = self.env['low.value.asset.history'].search([('low_value_asset_id', '=', self.id), ('change_type', '=', 'income')])
        if not low_value_asset_history_obj:
            low_value_asset_history_obj.create({
                'change_type': 'income',
                'new_value': self.employee_id.name,
                'new_owner': self.employee_id.id,
                'changed_date': self.date_acquired,
                'changed_user_id': self.env.user.id,
                'description': _('Income'),
                'low_value_asset_id': self.id
            })
        self.write({'state': 'using'})

    @api.multi
    def scrap(self):
        view = self.env.ref('l10n_mn_low_value_asset.low_value_scrap_scrap_view')
        return {
            'name': _('Scrap'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'low.value.asset.scrap.confirm.wizard',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'target': 'new'
        }

    def _scrap(self, scrap_date=False):
        for obj in self:
            low_value_asset_history_obj = obj.env['low.value.asset.history']
            low_value_asset_history_obj.create({
                'change_type': 'outgo',
                'old_value': obj.employee_id.name,
                'changed_date': scrap_date or datetime.now(),
                'changed_user_id': obj.env.user.id,
                'description': _('Outgo'),
                'low_value_asset_id': obj.id
            })
            obj.write({
                'state': 'scrapped',
                'scrapped_date': scrap_date
            })

    @api.multi
    def draft(self):
#         self.send_email_to_owners_cron() # БҮТЭЗ ноороглоход БҮТЭЗ бүртгэлтэй бүх ажилчдад имэйл илгээгдэж байсан тул комент болгов. Шаардлагатай тохиолдолд сэргээх
        for record in self:
            if record.state == 'sold':
                invoice_line_obj = record.env['account.invoice.line']
                invoice_line = invoice_line_obj.search([('low_value_asset_id', '=', record.id)])
                if invoice_line:
                    raise UserError(_('There is an invoice linket to the asset, you must first delete the invoice.'))
            record.write({
                'state': 'draft',
                'scrapped_date': False
            })

    def get_owners_with_low_value_asset(self):
        """Ашиглаж буй төлөвт БҮТЭЗ-н email-тэй эзэмшигчдийн ID-г жагсаалт хэлбэрээр буцаах"""
        qry = """
            SELECT DISTINCT asset.employee_id as employee_id
            FROM low_value_asset asset
            LEFT JOIN hr_employee employee on employee.id = asset.employee_id
            LEFT JOIN res_partner partner on employee.address_home_id = partner.id
            WHERE asset.state='using' AND partner.email is not null"""
        self._cr.execute(qry)
        return [obj['employee_id'] for obj in self._cr.dictfetchall()]

    @api.multi
    def send_email_to_owners_cron(self, employee_ids=None):
        if not employee_ids:
            employee_ids = self.get_owners_with_low_value_asset()
        outgoing_email = self.env['ir.mail_server'].sudo().search([])
        if not outgoing_email:
            raise UserError(_('There is no configuration for outgoing mail server. Please contact system administrator.'))
        else:
            outgoing_email = self.env['ir.mail_server'].sudo().search([('id', '=', outgoing_email[0].id)])
        assets = self.search([('employee_id', 'in', employee_ids), ('state', '=', 'using')])
        for employee in self.employee_id.browse(employee_ids):
            body_asset = ''
            number = 1
            for asset in assets.filtered(lambda a: a.employee_id.id == employee.id):
                body_asset += u'<tr>' \
                              u'<td style="border: 1px solid black; border-collapse: collapse;">%s</td>' \
                              u'<td style="border: 1px solid black; border-collapse: collapse;">%s</td>' \
                              u'<td style="border: 1px solid black; border-collapse: collapse;">%s</td>' \
                              u'<td style="border: 1px solid black; border-collapse: collapse;">%s</td>' \
                              u'<td style="border: 1px solid black; border-collapse: collapse;"><img src="https://chart.googleapis.com/chart?cht=qr&chl=%s&chs=180x180&choe=UTF-8&chld=L|2" /></td>' \
                              u'</tr>' % (number, asset.name_get()[0][1], asset.date_acquired, asset.location_id.name,
                                          urllib.quote_plus(str(asset.qr)))
                number += 1
            body_html = u'Сайн байна уу,<br/><br/>' \
                        u'%s танд %s-н байдлаар дараах БҮТЭЗ(үүд) бүртгэлтэй байна.<br/><br/>' \
                        u'<table style="border: 1px solid black; border-collapse: collapse;">' \
                        u'<tr>' \
                        u'<th style="border: 1px solid black; border-collapse: collapse;">№</th>' \
                        u'<th style="border: 1px solid black; border-collapse: collapse;">БҮТЭЗ нэр</th>' \
                        u'<th style="border: 1px solid black; border-collapse: collapse;">Ашиглалтанд<br/> орсон огноо</th>' \
                        u'<th style="border: 1px solid black; border-collapse: collapse;">Байршил</th>' \
                        u'<th style="border: 1px solid black; border-collapse: collapse;">Хөрөнгийн код</th>' \
                        u'</tr>' \
                        u'%s' \
                        u'</table>' \
                        u'<br/>' % (employee.name, datetime.now().date(), body_asset)

            values = {
                'subject': u'БҮТЭЗ-ийн мэдээлэл',
                'body_html': body_html,
                'email_from': outgoing_email.smtp_user,
                'reply_to': outgoing_email.smtp_user,
                'state': 'outgoing',
                'email_to': employee.address_home_id.email,
            }
            email_obj = self.env['mail.mail'].create(values)
            if email_obj:
                email_obj.send()
