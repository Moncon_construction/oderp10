# -*- coding: utf-8 -*-
from odoo import models, fields


class LowValueAsset(models.Model):
    _name = 'low.value.asset.history'
    _description = "Low value asset history"
    _order = 'changed_date DESC'

    changed_date = fields.Datetime('History date')
    description = fields.Text('Description')
    changed_user_id = fields.Many2one('res.users', 'Changed user')
    low_value_asset_id = fields.Many2one('low.value.asset', 'Low value asset', ondelete="cascade")
    change_type = fields.Selection([
        ('employee', 'Employee'),
        ('type', 'Type'),
        ('location', 'Location'),
        ('income', 'Income'),
        ('outgo', 'Outgo')])
    old_value = fields.Char('Old value')
    new_value = fields.Char('New value')
    old_location = fields.Many2one('low.value.asset.location', 'Old location')
    new_location = fields.Many2one('low.value.asset.location', 'New location')
    old_owner = fields.Many2one('hr.employee', 'Old owner')
    new_owner = fields.Many2one('hr.employee', 'new owner')
    old_type = fields.Many2one('low.value.asset.type', 'Old type')
    new_type = fields.Many2one('low.value.asset.type', 'New type')


