# -*- coding: utf-8 -*-
from odoo import models, fields


class LowValueAssetLocation(models.Model):
    _name = 'low.value.asset.location'

    name = fields.Char('Name', required=True)
