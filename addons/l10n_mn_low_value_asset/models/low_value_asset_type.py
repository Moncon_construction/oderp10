# -*- coding: utf-8 -*-
from odoo import models, fields


class LowValueAssetType(models.Model):
    _name = 'low.value.asset.type'

    name = fields.Char('Name')
