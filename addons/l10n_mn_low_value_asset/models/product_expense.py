# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class ProductExpenseLine(models.Model):
    _inherit = 'product.expense.line'

    is_product_low_level_asset = fields.Boolean(related='product.is_low_value_asset')
    is_low_value_asset = fields.Boolean('Is low value asset')

    @api.onchange('product')
    def change_is_low_value_asset(self):
        for obj in self:
            obj.is_low_value_asset = False


class ProductExpense(models.Model):
    _inherit = 'product.expense'

    def send(self):
        error_message = ''
        res = super(ProductExpense, self).send()
        for line in self.expense_line:
            if line.available_qty < line.quantity:
                if line.available_qty == 0:
                    error_message += _('There is no %s in stock.\n') % line.product.name
                else:
                    error_message += _('Available quantity for %s is %s.\n') % (line.product.name, line.available_qty)
        if error_message:
            raise ValidationError(error_message)
        return res
