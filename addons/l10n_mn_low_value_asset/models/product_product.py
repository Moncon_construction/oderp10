# -*- coding: utf-8 -*-
from odoo import models, fields, _


class ProductProduct(models.Model):
    _inherit = 'product.product'

    is_low_value_asset = fields.Boolean(_('Is low value asset'))
