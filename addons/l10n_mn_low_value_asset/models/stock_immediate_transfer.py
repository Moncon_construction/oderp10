# -*- coding: utf-8 -*-
from odoo import models, api, _
from datetime import datetime
from odoo.exceptions import UserError


class StockImmediateTransfer(models.TransientModel):
    _inherit = 'stock.immediate.transfer'

    @api.one
    def process(self):
        res = super(StockImmediateTransfer, self).process()
        low_value_asset_obj = self.env['low.value.asset']
        employee_id = None
        if self.pick_id and self.pick_id.expense:
            is_cost_for_each_wh = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_stock_account_cost_for_each_wh')], limit=1)
            if is_cost_for_each_wh.state == 'installed':
                for line in self.pick_id.expense.expense_line:
                    if line.is_low_value_asset:
                        query = '''
                                SELECT
                                    standard_price as cost
                                FROM
                                    product_warehouse_standard_price
                                where
                                    warehouse_id = ''' + str(self.pick_id.expense.warehouse.id) + '''
                                    AND product_id = ''' + str(line.product.id) + '''
                                    AND company_id = ''' + str(self.env.user.company_id.id)
                        self.env.cr.execute(query)
                        standard_price = self.env.cr.fetchall()
                        if not standard_price:
                            raise UserError(_('The product has no cost on %s') % self.pick_id.expense.warehouse.name)
                        quantity = line.quantity
                        while quantity > 0:
                            low_value_asset_obj.create({
                                'product_id': line.product.id,
                                'date_acquired': datetime.now(),
                                'value': standard_price[0][0] or 0,
                                'expense_id': self.pick_id.expense.id,
                                'employee_id': self.pick_id.expense.employee.id})
                            quantity -= 1
            else:
                for line in self.pick_id.expense.expense_line:
                    if line.is_low_value_asset:
                        quantity = line.quantity
                        while (quantity > 0):
                            low_value_asset_obj.create({
                                'product_id': line.product.id,
                                'date_acquired': datetime.now(),
                                'value': line.product.standard_price,
                                'expense_id': self.pick_id.expense.id,
                                'employee_id': self.pick_id.expense.employee.id})
                            quantity -= 1
        return res
