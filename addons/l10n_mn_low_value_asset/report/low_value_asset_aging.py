# -*- coding: utf-8 -*-
import base64
from io import BytesIO
from odoo import models, fields, api, _
from datetime import datetime, date
import xlsxwriter
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DATE_FORMAT
from odoo.addons.l10n_mn_low_value_asset.tools.msc_report_tools import list_to_str_tuple
from dateutil.relativedelta import relativedelta


class LowValueAssetAging(models.TransientModel):
    _inherit = 'oderp.report.html.output'
    _name = "low.value.asset.aging"
    _description = "Low value asset aging"

    date = fields.Date("Date", required=True, default=lambda *a: datetime.now())
    product_ids = fields.Many2many("product.product", string="Product")
    low_value_asset_type_ids = fields.Many2many("low.value.asset.type", string="Low value asset type")
    filter_type = fields.Selection([
        ('product', 'By product'),
        ('type', 'By type')], string="Filter type")

    date_type = fields.Selection([
        ('day', 'By day'),
        ('month', 'By month'),
        ('year', 'By year')], string="Date type", default='day')

    @api.onchange('filter_type')
    def onchange_filter_type(self):
        if self.filter_type == 'type':
            self.product_ids = False
        else:
            self.low_value_asset_type_ids = False

    def get_header_and_total_col(self):
        """Толгой мэдээллийг тооцооллох method"""
        header = [u'Ангилал', u'Эзэмшигчийн нэр', u'Хангамжийн материалын нэр', u'Худалдан авсан огноо']
        col = 4
        if self.date_type == 'day':
            header.extend([u'Насжилт/хоног/'])
        elif self.date_type == 'month':
            header.extend([u'Насжилт/сар/'])
        elif self.date_type == 'year':
            header.extend([u'Насжилт/жил/'])
        col += 1

        header.extend([u'Тоо', u'Нэгж өртөг', u'Нийт өртөг'])
        col += 3
        return col, header

    def get_data(self):
        where = "WHERE asset.state = 'using'"
        if self.filter_type == "product":
            where += " AND asset.product_id in %s" % list_to_str_tuple(self.product_ids.ids)
        elif self.filter_type == 'type':
            where += " AND asset.type_id in %s" % list_to_str_tuple(self.low_value_asset_type_ids.ids)
        qry = """
            SELECT 
                resource.name AS owner, asset.product_id AS p_id, asset.date_acquired AS ac_date, count(asset.id) AS qty, 
                asset.value AS cost, sum(asset.value) AS total_cost, type.name AS t_name
            FROM low_value_asset asset
            LEFT JOIN hr_employee AS employee ON asset.employee_id = employee.id
            LEFT JOIN resource_resource AS resource ON resource.id= employee.resource_id
            LEFT JOIN product_product AS product ON asset.product_id = product.id
            LEFT JOIN low_value_asset_type AS type ON asset.type_id = type.id
            %s AND asset.date_acquired < '%s'
            GROUP BY owner, p_id, ac_date, cost, t_name
            ORDER BY t_name, p_id
        """ % (where, self.date)
        self._cr.execute(qry)
        return self._cr.dictfetchall()

    def export_report(self):

        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        # форматуудыг зарлаж байна
        format_name = book.add_format({
            'font_name': 'Times New Roman',
            'text_wrap': 1,
            'font_size': 13,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter'
        })
        format_filter_center = book.add_format({
            'font_name': 'Times New Roman',
            'text_wrap': 1,
            'font_size': 11,
            'bold': False,
            'align': 'center',
            'valign': 'vcenter',
            'border': 1,
        })
        format_filter_float = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'text_wrap': 1,
            'bold': False,
            'align': 'left',
            'valign': 'vcenter',
            'num_format': '#,##0.00',
            'border': 1,
        })

        format_filter_right = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'text_wrap': 1,
            'bold': True,
            'align': 'right',
            'valign': 'vcenter'
        })
        format_filter_left = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'bold': False,
            'align': 'left',
            'text_wrap': 1,
            'valign': 'vcenter'
        })
        format_date = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'bold': False,
            'align': 'right',
            'text_wrap': 1,
            'valign': 'vcenter'
        })
        format_title = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter',
            'border': 1,
            'text_wrap': 1,
            'bg_color': '#cccccc',
        })
        format_title_left = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'bold': True,
            'align': 'left',
            'valign': 'vcenter',
            'border': 1,
            'text_wrap': 1,
            'bg_color': '#cccccc',
            'num_format': '#,##0.00',
        })

        report_name = _('Aging of the supply materials')
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 0
        total_col, headers = self.get_header_and_total_col()
        # баганы тохиргоог оруулж байна
        sheet.hide_gridlines(2)
        sheet.set_default_row(17)
        sheet.set_column('A:A', 20)
        sheet.set_column('B:B', 25)
        sheet.set_column('C:C', 30)
        sheet.set_column('D:D', 20)
        sheet.set_column('E:E', 20)
        sheet.set_column('F:F', 10)
        sheet.set_column('G:G', 10)
        sheet.set_column('H:H', 20)
        sheet.merge_range(rowx, 0, rowx, total_col - 1, report_name.upper(), format_name)
        rowx += 1
        sheet.merge_range(rowx, total_col - 3, rowx, total_col - 1, self.env.user.company_id.name, format_filter_right)
        rowx += 1
        sheet.merge_range(rowx, total_col - 3, rowx, total_col - 1, self.date, format_date)
        rowx += 2
        # Толгой мэдээллийг зурж байна
        head_col = 0
        sheet.set_row(rowx, 30)
        for header in headers:
            sheet.write(rowx, head_col, header, format_title)
            head_col += 1
        rowx += 1
        data = self.get_data()
        total_cost = float()
        product = self.env['product.product']
        for line in data:
            total_cost += line['total_cost']
            colx = 0
            sheet.write(rowx, colx, line['t_name'], format_filter_center)
            colx += 1
            sheet.write(rowx, colx, line['owner'], format_filter_center)
            colx += 1
            sheet.write(rowx, colx, product.browse(line['p_id']).name, format_filter_center)
            colx += 1
            sheet.write(rowx, colx, line['ac_date'], format_filter_center)
            colx += 1
            diff_in_day = datetime.strptime(self.date, DATE_FORMAT) - datetime.strptime(line['ac_date'], DATE_FORMAT)
            diff_of_date = relativedelta(datetime.strptime(self.date, DATE_FORMAT), datetime.strptime(line['ac_date'], DATE_FORMAT))
            if self.date_type == 'day':
                sheet.write(rowx, colx, diff_in_day.days, format_filter_center)
            elif self.date_type == 'month':
                sheet.write(rowx, colx, diff_of_date.months + diff_of_date.years * 12, format_filter_center)
            elif self.date_type == 'year':
                sheet.write(rowx, colx, diff_of_date.years, format_filter_center)
            colx += 1
            sheet.write(rowx, colx, line['qty'], format_filter_center)
            colx += 1
            sheet.write(rowx, colx, line['cost'], format_filter_float)
            colx += 1
            sheet.write(rowx, colx, line['total_cost'], format_filter_float)
            rowx += 1
        rowx += 2

        book.close()
        out = base64.encodestring(output.getvalue())
        excel_id = self.env['oderp.report.excel.output'].with_context(filename_prefix=report_name).create(
            {'filedata': out})
        return excel_id.export_report()
