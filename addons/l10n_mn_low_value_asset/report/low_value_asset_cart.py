# -*- coding: utf-8 -*-
import base64
from io import BytesIO
from odoo import models, fields, api, _
from datetime import datetime, timedelta
import xlsxwriter
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as DATE_FORMAT
from odoo.addons.l10n_mn_low_value_asset.tools.msc_report_tools import list_to_str_tuple
from odoo.exceptions import UserError


class LowValueAssetCart(models.TransientModel):
    _inherit = 'oderp.report.html.output'
    _name = "low.value.asset.cart"
    _description = "Low value asset cart"

    date = fields.Datetime("Date", required=True, default=lambda *a: datetime.now().replace(hour=15, minute=59, second=59))
    employee_id = fields.Many2many("hr.employee", string="Employee")
    product_id = fields.Many2many("product.product", string="Product")
    low_value_asset_type_id = fields.Many2one("low.value.asset.type", string="Low value asset type")
    filter_type = fields.Selection([
        ('employee', 'By employee'),
        ('type', 'By type'),
        ('product', 'By product')], string="Filter")

    @api.onchange('filter_type')
    def onchange_filter_type(self):
        self.change_other_fields(self.filter_type)

    """change_other_fields нь төрөл солих үед солигдсон төрөлд хамааралгүй
    талбаруудын утгыг False болгодог функц"""

    def change_other_fields(self, filter_type):
        if filter_type == 'employee':
            self.product_id = False
            self.low_value_asset_type_id = False
        elif filter_type == 'type':
            self.product_id = False
        elif filter_type == 'product':
            self.low_value_asset_type_id = False
            self.employee_id = False

    def get_header_and_total_col(self):
        """Толгой мэдээллийг тооцооллох method"""
        col = 0
        header = []
        if self.filter_type == 'employee':
            col += 2
            header.extend([{'name': _('Type name')}, {'name': _('Product name')}])
        elif self.filter_type == 'product':
            col += 2
            header.extend([{'name': _('Type name')}, {'name': _('Owner name')}])
        elif self.filter_type == 'type':
            col += 2
            header.extend([{'name': _('Owner name')}, {'name': _('Product name')}])
        header.extend([{'name': _('Date'),
                        'subheader': [_('Bought date'), _('Transferred date')]
                        }, {'name': _('Quantity')}, {'name': _('Cost')}, {'name': _('Total cost')}])
        col += 5
        if self.filter_type == 'product':
            header.append({'name': _('Signature')})
            col += 1
        return col, header

    def get_data(self, obj):
        where = ""
        if self.filter_type == 'product':
            where += " WHERE asset.product_id in %s" % list_to_str_tuple(obj.ids)
        elif self.filter_type == 'employee':
            where += " WHERE employee.id in %s" % list_to_str_tuple(obj.ids)
        elif self.filter_type == 'type':
            where += " WHERE employee.id in %s AND asset.type_id = %s" % (list_to_str_tuple(obj.ids), self.low_value_asset_type_id.id)
        qry = """
            SELECT 
                cast(history.changed_date as date) as t_date, 
                resource.name as emp_name, 
                type.name as tname, 
                product.id as pid, 
                count(asset.id) as qty, 
                asset.value as cost, 
                sum(asset.value) as total_cost, 
                min_date.c_date
            FROM low_value_asset asset 
            LEFT JOIN (select * from low_value_asset_history where id in (select max(history.id) as history_id
                    from low_value_asset asset 
                    left join low_value_asset_history history on asset.id = history.low_value_asset_id 
                    left join product_product product on asset.product_id = product.id 
                    where history.changed_date < '%s'
                    group by product.id, asset.id)) as history
                    on history.low_value_asset_id = asset.id
            LEFT JOIN hr_employee as employee on history.new_owner = employee.id
            LEFT JOIN resource_resource as resource on resource.id= employee.resource_id
            LEFT JOIN product_product AS product on asset.product_id = product.id
            LEFT JOIN low_value_asset_type AS type on asset.type_id = type.id
            LEFT JOIN (SELECT low_value_asset_id as asset_id, CAST(MIN(changed_date) as date) as c_date from low_value_asset_history GROUP BY asset_id) AS min_date on asset.id = min_date.asset_id
            %s AND history.changed_date < '%s' AND asset.state = 'using'
            GROUP BY emp_name, tname, pid, cost, t_date, min_date.c_date""" % (self.date, where, self.date)
        self._cr.execute(qry)
        return self._cr.dictfetchall()

    def export_report(self):

        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        # форматуудыг зарлаж байна
        format_name = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 13,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter'
        })
        format_filter_center = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'bold': False,
            'align': 'center',
            'valign': 'vcenter',
            'border': 1,
        })
        format_filter_float = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'bold': False,
            'align': 'left',
            'valign': 'vcenter',
            'num_format': '#,##0.00',
            'border': 1,
        })

        format_filter_right = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'bold': True,
            'align': 'right',
            'valign': 'vcenter'
        })
        format_filter_left = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'bold': False,
            'align': 'left',
            'valign': 'vcenter'
        })
        format_date = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'bold': False,
            'align': 'right',
            'valign': 'vcenter'
        })
        format_title = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter',
            'border': 1,
            'text_wrap': 1,
            'bg_color': '#cccccc',
        })
        format_title_left = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'bold': True,
            'align': 'left',
            'valign': 'vcenter',
            'border': 1,
            'text_wrap': 1,
            'bg_color': '#cccccc',
            'num_format': '#,##0.00',
        })

        report_name = _('Cart of the supply materials')
        loop_obj = self.employee_id or self.product_id
        is_empty = True
        sequence = 0
        for obj in loop_obj:
            data = self.get_data(obj)
            if data:
                is_empty = False
                sheet = book.add_worksheet('%s: %s' % (sequence, obj.name))
                sequence += 1
                sheet.set_landscape()
                sheet.set_paper(9)  # A4
                sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
                sheet.fit_to_pages(1, 0)
                sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
                rowx = 0
                total_col, headers = self.get_header_and_total_col()
                # баганы тохиргоог оруулж байна
                sheet.hide_gridlines(2)
                sheet.set_default_row(17)
                sheet.set_column('A:A', 20)
                sheet.set_column('B:B', 25)
                sheet.set_column('C:C', 20)
                sheet.set_column('D:D', 20)
                sheet.set_column('E:E', 10)
                sheet.set_column('F:F', 10)
                sheet.set_column('G:G', 10)
                sheet.set_column('H:H', 20)
                sheet.merge_range(rowx, 0, rowx, total_col - 1, report_name.upper(), format_name)
                rowx += 1
                sheet.merge_range(rowx, total_col - 3, rowx, total_col - 1, self.env.user.company_id.name, format_filter_right)
                if self.filter_type == "product":
                    sheet.merge_range(rowx, 0, rowx, 2, _("Product: %s") % obj.name, format_filter_left)
                elif self.filter_type == "type":
                    sheet.merge_range(rowx, 0, rowx, 2, _("Type: %s") % self.low_value_asset_type_id.name, format_filter_left)
                elif self.filter_type == "employee":
                    sheet.merge_range(rowx, 0, rowx, 2, _("Employee: %s") % obj.name, format_filter_left)
                rowx += 1
                sheet.merge_range(rowx, total_col - 2, rowx, total_col - 1, (datetime.strptime(self.date, DATE_FORMAT) + timedelta(hours=8)).strftime(DATE_FORMAT), format_date)
                rowx += 2
                # Толгой мэдээллийг зурж байна
                head_col = 0
                for header in headers:
                    if not header.get('subheader'):
                        sheet.merge_range(rowx, head_col, rowx + 1, head_col, header['name'], format_title)
                        head_col += 1
                    else:
                        sheet.merge_range(rowx, head_col, rowx, head_col + len(header['subheader']) - 1, header['name'], format_title)
                        index = 0
                        for col in range(head_col, head_col + len(header['subheader'])):
                            sheet.write(rowx + 1, col, header['subheader'][index], format_title)
                            index += 1
                            head_col += 1
                rowx += 2
                total_cost = float()
                product = self.env['product.product']
                for line in data:
                    total_cost += line['total_cost']
                    colx = 0
                    if self.filter_type == 'type':
                        sheet.write(rowx, colx, line['emp_name'], format_filter_center)
                    else:
                        sheet.write(rowx, colx, line['tname'], format_filter_center)
                    colx += 1
                    if self.filter_type == 'product':
                        sheet.write(rowx, colx, line['emp_name'], format_filter_center)
                    else:
                        sheet.write(rowx, colx, product.browse(line['pid']).name, format_filter_center)
                    colx += 1
                    sheet.write(rowx, colx, line['c_date'], format_filter_center)
                    colx += 1
                    sheet.write(rowx, colx, line['t_date'], format_filter_center)
                    colx += 1
                    sheet.write(rowx, colx, line['qty'], format_filter_center)
                    colx += 1
                    sheet.write(rowx, colx, line['cost'], format_filter_float)
                    colx += 1
                    sheet.write(rowx, colx, line['total_cost'], format_filter_float)
                    colx += 1
                    if self.filter_type == 'product':
                        sheet.write(rowx, colx, '', format_filter_center)
                    rowx += 1
                    for col in range(0, total_col-2):
                        sheet.write(rowx, col, '', format_title_left)
                    sheet.write(rowx, total_col-2, _('Total amount:'), format_title_left)
                    sheet.write(rowx, total_col-1,  total_cost, format_title_left)
                rowx += 2
                if self.filter_type == 'product':
                    sheet.merge_range(rowx, 2, rowx, 3,
                                      _('Managed accountant: .................................................'),
                                      format_filter_left)
                else:
                    sheet.merge_range(rowx, 2, rowx, 3,
                                      _('Employee signature: .................................................'),
                                      format_filter_left)
        if is_empty:
            raise UserError(_("Could not find anything"))
        book.close()
        out = base64.encodestring(output.getvalue())
        excel_id = self.env['oderp.report.excel.output'].with_context(filename_prefix=report_name).create(
            {'filedata': out})
        return excel_id.export_report()
