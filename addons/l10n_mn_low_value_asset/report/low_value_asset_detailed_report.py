# -*- coding: utf-8 -*-
import base64
from io import BytesIO
from dateutil.relativedelta import relativedelta
from odoo import models, fields, api, _
from datetime import datetime
import time
import xlsxwriter
from odoo.addons.l10n_mn_low_value_asset.tools.msc_report_tools import list_to_str_tuple
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DATE_FORMAT
from xlsxwriter.utility import xl_rowcol_to_cell_fast



class LowValueAssetCart(models.TransientModel):
    _inherit = 'oderp.report.html.output'
    _name = "low.value.asset.detailed.report"
    _description = "Low value asset detailed report"

    date_from = fields.Date("Start Date", required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    date_to = fields.Date("End Date", required=True, default=lambda *a: time.strftime(DATE_FORMAT))
    product_ids = fields.Many2many("product.product", string="Products")
    type_ids = fields.Many2many("low.value.asset.type", string="Low value asset types")

    def get_header_and_total_col(self):
        """Толгой мэдээллийг тооцооллох method"""
        col = 25
        header = [{'name': u'Ангилал'}, {'name': u'Хангамж материалын нэр'}, {'name': u'Дахин давтагдашгүй дугаар'},
                  {'name': u'Эзэмшигч'}, {'name': u'Эхний үлдэгдэл',
                                          'subheader': [u'Эхэлсэн огноо', u'Тоо ширхэг', u'Анхны өртөг',
                                                        u'Хуримт элэгдэл', u'C-1 үлдэгдэл']},
                  {'name': u'Орлого', 'subheader': [u'Тоо ширхэг', u'нэгж өртөг', u'Нийт өртөг']},
                  {'name': u'Зарлага',
                   'subheader': [u'Харилцагч', u'Огноо', u'Тоо ширхэг', u' Худалдсан үнэ /НӨАТ-гүй/', u'Анхны өртөг', u'Зарлагдах үеийн элэгдэл', u'Олз',
                                 u'Гарз']}, {'name': u'Тайлант хугацааны элэгдэл'},
                  {'name': u'Эцсийн үлдэгдэл',
                   'subheader': [u'Тоо ширхэг', u'Анхны өртөг', u'Нийт хуримтлагдсан элэгдэл', u'C2-үлдэгдэл']}, ]
        return col, header

    def get_income(self):
        where = "WHERE "
        if self.product_ids:
            where += " product.id in %s AND " % list_to_str_tuple(self.product_ids.ids)
        if self.type_ids:
            where += " type.id in %s AND " % list_to_str_tuple(self.type_ids.ids)

        qry = """
            SELECT asset.id as asset_id, type.name as t_name, product.id as p_id, asset.barcode as barcode, resource.name as emp_name, COUNT(asset.id) as qty,
                asset.value as cost, SUM(asset.value) as total_cost, asset.state as state, asset.date_acquired as ac_date, asset.duration as duration, asset.scrapped_date as scrapped_date
            FROM low_value_asset asset
            LEFT JOIN product_product product on product.id = asset.product_id
            LEFT JOIN hr_employee employee on asset.employee_id = employee.id
            LEFT JOIN resource_resource resource on resource.id = employee.resource_id
            LEFT JOIN low_value_asset_type type on type.id = asset.type_id
            %s CAST(asset.date_acquired AS date) BETWEEN '%s' AND '%s' AND asset.state != 'draft'
            GROUP BY t_name, p_id, asset.barcode, emp_name, asset_id, asset.state, ac_date, scrapped_date
        """ % (where, self.date_from, self.date_to)
        self._cr.execute(qry)
        return self._cr.dictfetchall()

    def get_initial(self):
        where = "WHERE "
        if self.product_ids:
            where += " product.id in %s AND " % list_to_str_tuple(self.product_ids.ids)
        if self.type_ids:
            where += " type.id in %s AND " % list_to_str_tuple(self.type_ids.ids)

        # Эхний үлдэгдэл авах
        qry = """
            SELECT asset.id as asset_id, asset.product_id AS p_id, asset.barcode AS barcode, resource.name AS emp_name, CAST(asset.date_acquired AS date) AS ac_date, count(asset.id) AS qty,
                asset.value AS cost, type.name AS t_name, asset.state as state, asset.duration as duration, asset.scrapped_date as scrapped_date 
            FROM low_value_asset asset
            LEFT JOIN low_value_asset_type AS type ON asset.type_id = type.id
            LEFT JOIN product_product AS product ON asset.product_id = product.id
            LEFT JOIN hr_employee employee ON asset.employee_id = employee.id
            LEFT JOIN resource_resource resource ON resource.id = employee.resource_id
            %s CAST(asset.date_acquired AS date) < '%s' AND asset.state != 'draft' AND asset.id NOT IN 
                (SELECT low_value_asset_id from low_value_asset_history where change_type = 'outgo' and changed_date < '%s')
            GROUP BY asset.id, p_id, asset.barcode, emp_name, ac_date, cost, t_name, duration, scrapped_date
        """ % (where, self.date_from, self.date_from)
        self._cr.execute(qry)
        return self._cr.dictfetchall()

    def export_report(self):

        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        # форматуудыг зарлаж байна
        format_name = book.add_format({
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'font_size': 13,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter'
        })
        format_filter_center = book.add_format({
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'font_size': 11,
            'bold': False,
            'align': 'center',
            'valign': 'vcenter',
            'border': 1,
        })
        format_filter_float = book.add_format({
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'font_size': 11,
            'bold': False,
            'align': 'left',
            'valign': 'vcenter',
            'num_format': '#,##0.00',
            'border': 1,
        })

        format_filter_right = book.add_format({
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'font_size': 11,
            'bold': True,
            'align': 'right',
            'valign': 'vcenter'
        })
        format_date = book.add_format({
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'font_size': 11,
            'bold': False,
            'align': 'left',
            'valign': 'vcenter'
        })
        format_title = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'bold': True,
            'align': 'centers',
            'valign': 'vcenter',
            'border': 1,
            'text_wrap': 1,
            'bg_color': '#cccccc',
            'num_format': '#,##0.00',
        })
        format_title_float = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'bold': True,
            'align': 'left',
            'valign': 'vcenter',
            'border': 1,
            'text_wrap': 1,
            'bg_color': '#cccccc',
            'num_format': '#,##0.00',
        })
        report_name = u'Хангамжийн материалын дэлгэрэнгүй тайлан'
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 0
        total_col, headers = self.get_header_and_total_col()
        # баганы тохиргоог оруулж байна
        sheet.hide_gridlines(2)
        sheet.set_default_row(17)
        sheet.set_column('A:A', 20)
        sheet.set_column('B:B', 15)
        sheet.set_column('C:C', 17)
        sheet.set_column('D:D', 15)
        sheet.set_column('E:E', 15)
        sheet.set_column('F:F', 11)
        sheet.set_column('G:G', 11)
        sheet.set_column('H:H', 12)
        sheet.set_column('I:I', 15)
        sheet.set_column('J:J', 11)
        sheet.set_column('K:K', 11)
        sheet.set_column('L:L', 12)
        sheet.set_column('M:M', 15)
        sheet.set_column('N:N', 12)
        sheet.set_column('O:O', 12)
        sheet.set_column('P:P', 15)
        sheet.set_column('Q:Q', 12)
        sheet.set_column('R:R', 15)
        sheet.set_column('S:S', 12)
        sheet.set_column('T:T', 12)
        sheet.set_column('U:U', 15)
        sheet.set_column('V:V', 12)
        sheet.set_column('W:W', 12)
        sheet.set_column('X:X', 25)
        sheet.set_column('Y:Y', 12)
        sheet.merge_range(rowx, 0, rowx, total_col - 1, report_name.upper(), format_name)
        rowx += 1
        sheet.merge_range(rowx, total_col - 3, rowx, total_col - 1, self.env.user.company_id.name, format_filter_right)
        rowx += 1
        sheet.merge_range(rowx, 0, rowx, 1, u'Огноо: %s - %s' % (self.date_from, self.date_to), format_date)
        rowx += 2
        # Толгой мэдээллийг зурж байна
        head_col = 0
        sheet.set_row(rowx+1, 33)
        for header in headers:
            if not header.get('subheader'):
                sheet.merge_range(rowx, head_col, rowx + 1, head_col, header['name'], format_title)
                head_col += 1
            else:
                sheet.merge_range(rowx, head_col, rowx, head_col + len(header['subheader']) - 1, header['name'],
                                  format_title)
                index = 0
                for col in range(head_col, head_col + len(header['subheader'])):
                    sheet.write(rowx + 1, col, header['subheader'][index], format_title)
                    index += 1
                    head_col += 1
        rowx += 2
        initial = self.get_initial()
        product = self.env['product.product'].browse(list(set([line['p_id'] for line in initial])))
        sum_row = rowx
        sum_cols = ['F', 'G', 'H', 'I', 'J', 'K', 'L', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y']
        for line in initial:
            colx = 0
            sheet.write(rowx, colx, line['t_name'], format_filter_center)
            colx += 1
            sheet.write(rowx, colx, product.filtered(lambda p: p.id == line['p_id']).name, format_filter_center)
            colx += 1
            sheet.write(rowx, colx, line['barcode'], format_filter_center)
            colx += 1
            sheet.write(rowx, colx, line['emp_name'], format_filter_center)
            colx += 1
            sheet.write(rowx, colx, line['ac_date'], format_filter_center)
            colx += 1
            sheet.write(rowx, colx, line['qty'], format_filter_float)
            colx += 1
            sheet.write(rowx, colx, line['cost'], format_filter_float)
            colx += 1
            left_cost = -self.get_left_cost(line['ac_date'], self.date_from, line['cost'], line['duration'])
            sheet.write(rowx, colx, left_cost, format_filter_float)
            colx += 1
            sheet.write(rowx, colx, float(float(line['cost']) + left_cost), format_filter_float)
            colx += 1
            sheet.write(rowx, colx, "", format_filter_float)
            colx += 1
            sheet.write(rowx, colx, "", format_filter_float)
            colx += 1
            sheet.write(rowx, colx, "", format_filter_float)
            colx += 1
            if line['state'] != 'using':
                invoice_line = self.env['account.invoice.line'].search([('low_value_asset_id', '=', line['asset_id'])], limit=1, order='create_date desc')
                if invoice_line:
                    sheet.write(rowx, colx, invoice_line.partner_id.name, format_filter_float)
                    colx += 1
                    sheet.write(rowx, colx, invoice_line.invoice_id.date_due, format_filter_float)
                    colx += 1
                    sheet.write(rowx, colx, line['qty'], format_filter_float)
                    colx += 1
                    sheet.write(rowx, colx, -invoice_line.price_unit, format_filter_float)
                    colx += 1
                    sheet.write(rowx, colx, line['cost'], format_filter_float)
                    colx += 1
                    left_cost = self.get_left_cost(line['ac_date'], invoice_line.invoice_id.date_due, line['cost'], line['duration'])
                    sheet.write(rowx, colx, -left_cost, format_filter_float)
                    colx += 1
                    gain_loss = line['cost'] - invoice_line.price_unit - left_cost
                    if gain_loss < 0:
                        sheet.write(rowx, colx, gain_loss, format_filter_float)
                        colx += 1
                        sheet.write(rowx, colx, 0, format_filter_float)
                        colx += 1
                    else:
                        sheet.write(rowx, colx, 0, format_filter_float)
                        colx += 1
                        sheet.write(rowx, colx, gain_loss, format_filter_float)
                        colx += 1
                else:
                    sheet.write(rowx, colx, "", format_filter_float)
                    colx += 1
                    sheet.write(rowx, colx, line['scrapped_date'], format_filter_float)
                    colx += 1
                    sheet.write(rowx, colx, line['qty'], format_filter_float)
                    colx += 1
                    sheet.write(rowx, colx, 0, format_filter_float)
                    colx += 1
                    sheet.write(rowx, colx, line['cost'], format_filter_float)
                    colx += 1
                    sheet.write(rowx, colx, -line['cost'], format_filter_float)
                    colx += 1
                    sheet.write(rowx, colx, 0, format_filter_float)
                    colx += 1
                    sheet.write(rowx, colx, line['cost'], format_filter_float)
                    colx += 1
                sheet.write(rowx, colx, "", format_filter_float)
            else:
                sheet.write(rowx, colx, "", format_filter_center)
                colx += 1
                sheet.write(rowx, colx, "", format_filter_center)
                colx += 1
                sheet.write(rowx, colx, "", format_filter_center)
                colx += 1
                sheet.write(rowx, colx, "", format_filter_center)
                colx += 1
                sheet.write(rowx, colx, "", format_filter_center)
                colx += 1
                sheet.write(rowx, colx, "", format_filter_center)
                colx += 1
                sheet.write(rowx, colx, "", format_filter_center)
                colx += 1
                sheet.write(rowx, colx, "", format_filter_center)
                colx += 1
                deprication_of_report = self.get_left_cost(self.date_from, self.date_to, line['cost'], line['duration'])
                sheet.write(rowx, colx, deprication_of_report, format_filter_float)
            colx += 1
            if line['state'] != 'using':
                sheet.write(rowx, colx, "", format_filter_float)
                colx += 1
                sheet.write(rowx, colx, "", format_filter_float)
                colx += 1
                sheet.write(rowx, colx, "", format_filter_float)
                colx += 1
                sheet.write(rowx, colx, "", format_filter_float)
                colx += 1
            else:
                sheet.write(rowx, colx, line["qty"], format_filter_float)
                colx += 1
                sheet.write(rowx, colx, line["cost"], format_filter_float)
                colx += 1
                total_dep = self.get_left_cost(line['ac_date'], self.date_to, line['cost'], line['duration'])
                sheet.write(rowx, colx, total_dep, format_filter_float)
                colx += 1
                sheet.write(rowx, colx, line['cost'] - total_dep, format_filter_float)
                colx += 1
            rowx += 1
        income = self.get_income()
        product = self.env['product.product'].browse(list(set([line['p_id'] for line in income])))
        for line in income:
            colx = 0
            sheet.write(rowx, colx, line['t_name'], format_filter_center)
            colx += 1
            sheet.write(rowx, colx, product.filtered(lambda p: p.id == line['p_id']).name, format_filter_center)
            colx += 1
            sheet.write(rowx, colx, line['barcode'], format_filter_center)
            colx += 1
            sheet.write(rowx, colx, line['emp_name'], format_filter_center)
            colx += 1
            sheet.write(rowx, colx, "", format_filter_float)
            colx += 1
            sheet.write(rowx, colx, "", format_filter_float)
            colx += 1
            sheet.write(rowx, colx, "", format_filter_float)
            colx += 1
            sheet.write(rowx, colx, "", format_filter_float)
            colx += 1
            sheet.write(rowx, colx, "", format_filter_float)
            colx += 1
            sheet.write(rowx, colx, line['qty'], format_filter_float)
            colx += 1
            sheet.write(rowx, colx, line['cost'], format_filter_float)
            colx += 1
            sheet.write(rowx, colx, line['total_cost'], format_filter_float)
            colx += 1
            if line['state'] != 'using':
                invoice_line = self.env['account.invoice.line'].search([('low_value_asset_id', '=', line['asset_id'])], limit=1, order='create_date desc')
                if invoice_line:
                    sheet.write(rowx, colx, invoice_line.partner_id.name, format_filter_float)
                    colx += 1
                    sheet.write(rowx, colx, invoice_line.invoice_id.date_due, format_filter_float)
                    colx += 1
                    sheet.write(rowx, colx, line['qty'], format_filter_float)
                    colx += 1
                    sheet.write(rowx, colx, -invoice_line.price_unit, format_filter_float)
                    colx += 1
                    sheet.write(rowx, colx, line['cost'], format_filter_float)
                    colx += 1
                    left_cost = self.get_left_cost(line['ac_date'], invoice_line.invoice_id.date_due, line['cost'], line['duration'])
                    sheet.write(rowx, colx, -left_cost, format_filter_float)
                    colx += 1
                    gain_loss = line['cost'] - invoice_line.price_unit - left_cost
                    if gain_loss < 0:
                        sheet.write(rowx, colx, gain_loss, format_filter_float)
                        colx += 1
                        sheet.write(rowx, colx, 0, format_filter_float)
                        colx += 1
                    else:
                        sheet.write(rowx, colx, 0, format_filter_float)
                        colx += 1
                        sheet.write(rowx, colx, gain_loss, format_filter_float)
                        colx += 1
                else:
                    sheet.write(rowx, colx, "", format_filter_float)
                    colx += 1
                    sheet.write(rowx, colx, line['scrapped_date'], format_filter_float)
                    colx += 1
                    sheet.write(rowx, colx, line['qty'], format_filter_float)
                    colx += 1
                    sheet.write(rowx, colx, 0, format_filter_float)
                    colx += 1
                    sheet.write(rowx, colx, line['cost'], format_filter_float)
                    colx += 1
                    sheet.write(rowx, colx, -line['cost'], format_filter_float)
                    colx += 1
                    sheet.write(rowx, colx, 0, format_filter_float)
                    colx += 1
                    sheet.write(rowx, colx, line['cost'], format_filter_float)
                    colx += 1
                sheet.write(rowx, colx, "", format_filter_float)
            else:
                sheet.write(rowx, colx, "", format_filter_center)
                colx += 1
                sheet.write(rowx, colx, "", format_filter_center)
                colx += 1
                sheet.write(rowx, colx, "", format_filter_center)
                colx += 1
                sheet.write(rowx, colx, "", format_filter_center)
                colx += 1
                sheet.write(rowx, colx, "", format_filter_center)
                colx += 1
                sheet.write(rowx, colx, "", format_filter_center)
                colx += 1
                sheet.write(rowx, colx, "", format_filter_center)
                colx += 1
                sheet.write(rowx, colx, "", format_filter_center)
                colx += 1
                deprication_of_report = self.get_left_cost(line['ac_date'], self.date_to, line['cost'], line['duration'])
                sheet.write(rowx, colx, deprication_of_report, format_filter_float)
            colx += 1
            if line['state'] != 'using':
                sheet.write(rowx, colx, "", format_filter_float)
                colx += 1
                sheet.write(rowx, colx, "", format_filter_float)
                colx += 1
                sheet.write(rowx, colx, "", format_filter_float)
                colx += 1
                sheet.write(rowx, colx, "", format_filter_float)
                colx += 1
            else:
                sheet.write(rowx, colx, line["qty"], format_filter_float)
                colx += 1
                sheet.write(rowx, colx, line["cost"], format_filter_float)
                colx += 1
                total_dep = self.get_left_cost(line['ac_date'], self.date_to, line['cost'], line['duration'])
                sheet.write(rowx, colx, total_dep, format_filter_float)
                colx += 1
                sheet.write(rowx, colx, line['cost'] - total_dep, format_filter_float)
                colx += 1
            rowx += 1
        sheet.merge_range(rowx, 0, rowx, 3, u'Нийт', format_title)
        for col in range(4, total_col):
            alpha = xl_rowcol_to_cell_fast(rowx, col)[0]
            if alpha in sum_cols:
                sheet.write_formula(rowx, col, '{=SUM(%s%s:%s%s)}' % (alpha, sum_row + 1, alpha, rowx), format_title_float)
            else:
                sheet.write(rowx, col, "", format_title)
        rowx += 1
        book.close()
        out = base64.encodestring(output.getvalue())
        excel_id = self.env['oderp.report.excel.output'].with_context(filename_prefix=report_name).create(
            {'filedata': out})
        return excel_id.export_report()

    def get_left_cost(self, acquired_date, some_date, initial_cost, duration):
        used_days = (datetime.strptime(some_date, DATE_FORMAT).date() - datetime.strptime(acquired_date, DATE_FORMAT).date()).days + 1
        to_use_days = (datetime.strptime(acquired_date, DATE_FORMAT).date() + relativedelta(months=duration) - datetime.strptime(acquired_date, DATE_FORMAT).date()).days + 1
        return round(initial_cost / 100 * used_days * 100 / to_use_days, 2)
