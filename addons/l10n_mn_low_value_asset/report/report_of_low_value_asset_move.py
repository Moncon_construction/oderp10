# -*- coding: utf-8 -*-
import base64
from io import BytesIO
from odoo import models, fields, api, _
import time
import xlsxwriter
from odoo.addons.l10n_mn_low_value_asset.tools.msc_report_tools import list_to_str_tuple
from xlsxwriter.utility import xl_rowcol_to_cell_fast


class ReportLowValueAssetMove(models.TransientModel):
    _inherit = 'oderp.report.html.output'
    _name = "report.low.value.asset.move"
    _description = "Report of Low value asset move"

    date_from = fields.Date("Start Date", required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    date_to = fields.Date("End Date", required=True, default=lambda *a: time.strftime('%Y-%m-%d'))

    change_type = fields.Selection([
        ('employee', 'Employee'),
        ('type', 'Type'),
        ('location', 'Location')], default='employee', string="Change type")
    filter_type = fields.Selection([
        ('product', 'By product'),
        ('type', 'By type')], default='product', string="Filter type")

    product_ids = fields.Many2many('product.product', string='Products')
    type_id = fields.Many2many('low.value.asset.type', 'low_value_asset_type_move_report_rel', string='Types')

    old_location = fields.Many2many('low.value.asset.location', 'old_location_asset_location_rel',
                                    string='Old location')
    new_location = fields.Many2many('low.value.asset.location', 'new_location_asset_location_rel',
                                    string='New location')
    old_owner = fields.Many2many('hr.employee', 'old_owner_hr_employee_rel', string='Old owner')
    new_owner = fields.Many2many('hr.employee', 'new_owner_hr_employee_rel', string='New owner')
    old_type = fields.Many2many('low.value.asset.type', 'old_type_asset_type_rel', string='Old type')
    new_type = fields.Many2many('low.value.asset.type', 'new_type_asset_type_rel', string='New type')
    move_filter = fields.Selection([
        ('income', 'Income'),
        ('outgo', 'Outgo'),
        ('employee', 'Transfer')], string="Move type filter")

    @api.onchange('change_type')
    def onchange_change_type(self):
        self.change_other_fields(self.change_type)

    """onchange_filter_type, change_other_fields нь төрөл солих үед солигдсон төрөлд хамааралгүй
    талбаруудын утгыг False болгодог функц"""

    def change_other_fields(self, change_type):
        if change_type == 'employee':
            self.old_location = False
            self.new_location = False
            self.old_type = False
            self.new_type = False
        elif change_type == 'type':
            self.old_location = False
            self.new_location = False
            self.old_owner = False
            self.new_owner = False
        elif change_type == 'location':
            self.old_owner = False
            self.new_owner = False
            self.old_type = False
            self.new_type = False
        else:
            self.old_location = False
            self.new_location = False
            self.old_owner = False
            self.new_owner = False
            self.old_type = False
            self.new_type = False

    @api.onchange('filter_type')
    def onchange_filter_type(self):
        if self.filter_type == 'type':
            self.product_ids = False
        else:
            self.type_id = False

    def get_header_and_total_col(self):
        """Толгой мэдээллийг тооцооллох method"""
        col = 3
        header = [_('№'), _('Type name'), _('description')]
        if self.change_type == 'employee':
            header.extend([_('Old owner'), _('New Owner/Buyer')])
            col += 2
        elif self.change_type == 'type':
            header.extend([_('Old type'), _('New type')])
            col += 2
        elif self.change_type == 'location':
            header.extend([_('Old location'), _('New location')])
            col += 2
        header.extend([_('Change date'), _('Quantity'), _('Cost (₮)'), _('Total cost (₮)')])
        col += 4
        return col, header

    def get_data(self):
        if self.filter_type == 'product':
            if self.product_ids:
                where = " WHERE asset.product_id in %s" % list_to_str_tuple(self.product_ids.ids)
            else:
                where = " WHERE asset.product_id in (SELECT id FROM product_product where is_low_value_asset='t')"
        else:
            if self.type_id:
                where = " WHERE asset.type_id in %s" % list_to_str_tuple(self.type_id.ids)
            else:
                where = " WHERE asset.type_id in (select id from low_value_asset_type)"
        change_type = ""
        if self.change_type == 'employee':
            if self.move_filter:
                change_type += " AND history.change_type = '%s'" % self.move_filter
            else:
                change_type += " AND history.change_type in ('employee', 'income', 'outgo')"
            if self.old_owner:
                change_type += " AND history.old_owner in %s" % list_to_str_tuple(self.old_owner.ids)
            if self.new_owner:
                change_type += "  AND history.new_owner in %s" % list_to_str_tuple(self.new_owner.ids)
        elif self.change_type == 'type':
            change_type += " AND history.change_type = 'type'"
            if self.old_type:
                change_type += " AND history.old_type in %s" % list_to_str_tuple(self.old_type.ids)
            if self.new_type:
                change_type += " AND history.new_type in %s" % list_to_str_tuple(self.new_type.ids)
        elif self.change_type == 'location':
            change_type += " AND history.change_type = 'location'"
            if self.old_location:
                change_type += " AND history.old_location in %s" % list_to_str_tuple(self.old_location.ids)
            if self.new_location:
                change_type += " AND history.new_location in %s" % list_to_str_tuple(self.new_location.ids)
        if self.change_type != 'employee':
            change_type += " AND history.change_type not in ('income', 'outgo')"
        qry = """SELECT type.name as tname, asset.product_id AS pid, COUNT(history.id) AS quantity,  
                history.change_type AS hdes, CAST(history.changed_date as date) as changed_date,  history.old_value, history.new_value, asset.value as value,
                SUM(asset.value) as total_value
                FROM low_value_asset_history history
                LEFT JOIN low_value_asset AS asset ON  history.low_value_asset_id = asset.id
                LEFT JOIN product_product AS p ON asset.product_id = p.id
                LEFT JOIN low_value_asset_type as type ON asset.type_id = type.id
                %s %s AND history.changed_date BETWEEN '%s' AND '%s' AND asset.state != 'draft'
                GROUP BY asset.product_id, history.change_type, changed_date, history.old_value, history.new_value, type.name, asset.value
                ORDER BY history.changed_date ASC""" % (where, change_type, self.date_from + " 00:00:00", self.date_to + " 23:59:59")
        self._cr.execute(qry)
        return self._cr.dictfetchall()

    @api.multi
    def export_report(self):
        products = self.env['product.product']
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        # форматуудыг зарлаж байна
        format_name = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 13,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter'
        })
        format_filter_center = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'bold': False,
            'align': 'center',
            'valign': 'vcenter',
            'border': 1,
        })
        format_filter_float = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'bold': False,
            'align': 'left',
            'valign': 'vcenter',
            'num_format': '#,##0.00',
            'border': 1,
        })

        format_filter_right = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'bold': True,
            'align': 'right',
            'valign': 'vcenter'
        })
        format_filter_left = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'bold': False,
            'align': 'left',
            'valign': 'vcenter'
        })
        format_date = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'bold': False,
            'align': 'right',
            'valign': 'vcenter'
        })
        format_title = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter',
            'border': 1,
            'text_wrap': 1,
            'bg_color': '#cccccc',
        })
        format_title_left = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'bold': True,
            'align': 'left',
            'valign': 'vcenter',
            'border': 1,
            'text_wrap': 1,
            'bg_color': '#cccccc',
            'num_format': '#,##0.00',
        })

        # Барааны формат
        format_header_left = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'bold': True,
            'align': 'left',
            'valign': 'vcenter',
            'border': 1,
            'text_wrap': 1,
            'bg_color': '#dddddd',
        })
        format_header_center = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter',
            'border': 1,
            'text_wrap': 1,
            'bg_color': '#dddddd',
        })

        report_name = _('Report of the supply materials')
        sheet = book.add_worksheet(report_name)
        # Sheet тохиргоог оруулж байна
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 0
        total_col, headers = self.get_header_and_total_col()
        # баганы тохиргоог оруулж байна
        sheet.hide_gridlines(2)
        sheet.set_default_row(17)
        sheet.set_column('A:A', 4)
        sheet.set_column('B:B', 30)
        sheet.set_column('C:C', 20)
        sheet.set_column('D:D', 20)
        sheet.set_column('E:E', 30)
        sheet.set_column('F:F', 20)
        sheet.set_column('G:G', 10)
        sheet.set_column('H:H', 12)
        sheet.set_column('I:I', 12)
        sheet.set_column('J:J', 15)
        sheet.merge_range(rowx, 0, rowx, total_col - 1, report_name.upper(), format_name)
        rowx += 1
        sheet.merge_range(rowx, total_col - 3, rowx, total_col - 1, self.env.user.company_id.name, format_filter_right)
        rowx += 1
        sheet.merge_range(rowx, total_col - 3, rowx, total_col - 1, _('%s to %s') % (self.date_from, self.date_to), format_date)
        rowx += 2
        head_col = 0
        data = self.get_data()
        product_ids = list(ids['pid'] for ids in data)
        product_ids = list(dict.fromkeys(product_ids))
        # products хувьсагчид зөвхөн БҮТЭЗ-тэй бараанууд байна
        products = products.browse(product_ids)
        # Толгой мэдээллийг зурж байгаа хэсэг
        sheet.set_row(rowx, 30)
        for header in headers:
            sheet.write(rowx, head_col, header, format_title)
            head_col += 1
        rowx += 1
        sequence = 1
        sum_row = rowx
        for product in products:
            subsequence = 1
            sheet.write(rowx, 0, sequence, format_header_center)
            sheet.merge_range(rowx, 1, rowx, total_col - 1, _('Product: %s') % product.name, format_header_left)
            rowx += 1
            for history in [d for d in data if d['pid'] == product.id]:
                if history['hdes'] == 'income':
                    history['hdes'] = u'Орлого'
                elif history['hdes'] == 'outgo':
                    history['hdes'] = u'Зарлага'
                    history['quantity'] *= (-1)
                    history['total_value'] *= (-1)
                else:
                    history['hdes'] = u'Шилжүүлэг'
                    colx = 0
                    sheet.write(rowx, colx, '%s.%s' % (sequence, subsequence), format_filter_center)
                    colx += 1
                    sheet.write(rowx, colx, history['tname'], format_filter_center)
                    colx += 1
                    sheet.write(rowx, colx, history['hdes'], format_filter_center)
                    colx += 1
                    sheet.write(rowx, colx, history['old_value'], format_filter_center)
                    colx += 1
                    sheet.write(rowx, colx, history['new_value'], format_filter_center)
                    colx += 1
                    sheet.write(rowx, colx, history['changed_date'].split('.')[0], format_filter_center)
                    colx += 1
                    sheet.write(rowx, colx, history['quantity'], format_filter_center)
                    colx += 1
                    sheet.write(rowx, colx, history['value'], format_filter_float)
                    colx += 1
                    sheet.write(rowx, colx, history['total_value'], format_filter_float)
                    rowx += 1
                    subsequence += 1
                    history['quantity'] *= (-1)
                    history['total_value'] *= (-1)
                colx = 0
                sheet.write(rowx, colx, '%s.%s' % (sequence, subsequence), format_filter_center)
                colx += 1
                sheet.write(rowx, colx, history['tname'], format_filter_center)
                colx += 1
                sheet.write(rowx, colx, history['hdes'], format_filter_center)
                colx += 1
                sheet.write(rowx, colx, history['old_value'], format_filter_center)
                colx += 1
                sheet.write(rowx, colx, history['new_value'], format_filter_center)
                colx += 1
                sheet.write(rowx, colx, history['changed_date'].split('.')[0], format_filter_center)
                colx += 1
                sheet.write(rowx, colx, history['quantity'], format_filter_center)
                colx += 1
                sheet.write(rowx, colx, history['value'], format_filter_float)
                colx += 1
                sheet.write(rowx, colx, history['total_value'], format_filter_float)
                rowx += 1
                subsequence += 1
            sequence += 1
        if self.change_type == 'employee':
            sheet.write(rowx, 0, '', format_filter_center)
            for col in range(1, total_col - 2):
                sheet.write(rowx, col, '', format_title_left)
            sheet.write(rowx, total_col-2, _('Total amount:'), format_title_left)
            alpha = xl_rowcol_to_cell_fast(rowx, total_col-1)[0]
            sheet.write_formula(rowx, total_col-1, '{=SUM(%s%s:%s%s)}' % (alpha, sum_row + 1, alpha, rowx), format_title_left)

            rowx += 1
        rowx += 1
        sheet.merge_range(rowx, 2, rowx, 3, _('Managed accountant: .................................................'), format_filter_left)
        book.close()
        out = base64.encodestring(output.getvalue())
        excel_id = self.env['oderp.report.excel.output'].with_context(filename_prefix=report_name).create(
            {'filedata': out})
        return excel_id.export_report()
