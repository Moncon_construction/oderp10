# -*- coding: utf-8 -*-
from datetime import date, datetime

import pytz
from odoo import models, fields, api, _
from odoo.exceptions import UserError
import time
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT


class ScrapConfirmWizard(models.TransientModel):
    _name = 'low.value.asset.scrap.confirm.wizard'

    low_value_asset = fields.Many2one('low.value.asset', string="Low value asset",
                                      default=lambda self: self.env['low.value.asset'].browse(self._context.get('active_id')))
    transfer_date = fields.Datetime('Date', default=lambda *a: time.strftime(DEFAULT_SERVER_DATETIME_FORMAT))

    def scrap(self):
        if self.low_value_asset.state != 'using':
            raise UserError(_("You can only scrap when low value asset's state is in 'Using'"))
        self.low_value_asset._scrap(scrap_date=self.transfer_date)

    @api.constrains('transfer_date')
    def transfer_date_constraint(self):
        tz = pytz.timezone(self.env.user.tz or pytz.utc)
        history_obj = self.env['low.value.asset.history'].search([('low_value_asset_id', '=', self.low_value_asset.id)], order='changed_date desc', limit=1)
        last_history_date = datetime.strptime(history_obj.changed_date, DEFAULT_SERVER_DATETIME_FORMAT)
        if last_history_date >= datetime.strptime(self.transfer_date, DEFAULT_SERVER_DATETIME_FORMAT):
            raise ValidationError(_("%s low value asset's scrap date must be after last transfer date\n which is %s") %
                                  (self.low_value_asset.name_get()[0][1], last_history_date.replace(tzinfo=pytz.utc).astimezone(tz).replace(tzinfo=None)))