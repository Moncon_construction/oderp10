# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from datetime import datetime, date
from odoo.exceptions import UserError


class LowValueAssetSell(models.TransientModel):
    _name = 'low.value.asset.sell'

    date = fields.Date('Sell date', required=True)
    description = fields.Text('Description', required=True)
    partner_id = fields.Many2one('res.partner', 'Partner', required=True)
    journal_id = fields.Many2one('account.journal', 'Journal', required=True, domain=[('type', '=', 'sale')])
    account_id = fields.Many2one('account.account', required=True)
    line_ids = fields.One2many('low.value.asset.sell.line', 'low_value_asset_sell_id', 'Low value assets')

    @api.multi
    def sell(self):
        if not self.journal_id.default_credit_account_id:
            raise UserError(_('The journal has no credit account!'))
        assets_not_sold = self.line_ids.filtered(lambda move: move.low_value_asset_id.state != 'using')
        if assets_not_sold:
            raise UserError(_("You can only sell when low value asset's state is in 'Using'"
                              "\n If you are mass selling, please uncheck any low value asset that the state is not 'Using'"))

        line_vals = []
        product_ids = {}
        for line in self.line_ids:
            product_ids[line.low_value_asset_id.product_id.id] = (line.sell_value)
            line_vals.append((0, 0, {
                'product_id': line.low_value_asset_id.product_id.id,
                'name': '%s - %s' % (self.description, line.low_value_asset_id.product_id.name),
                'account_id': self.journal_id.default_credit_account_id.id,
                'quantity': 1,
                'price_unit': line.sell_value,
                'invoice_line_tax_ids': [[6, 0, [line.tax_id.id]]] if line.tax_id else False,
                'low_value_asset_id': line.low_value_asset_id.id,
            }))
            low_value_asset_history_obj = self.env['low.value.asset.history']
            low_value_asset_history_obj.create({
                'change_type': 'outgo',
                'old_value': line.low_value_asset_id.employee_id.name,
                'new_value': self.partner_id.name,
                'changed_date': datetime.now(),
                'changed_user_id': line.low_value_asset_id.env.user.id,
                'description': _('Outgo'),
                'low_value_asset_id': line.low_value_asset_id.id
            })
            line.low_value_asset_id.write({
                'state': 'sold',
                'scrapped_date': date.today()
            })
        invoice_vals = {
            'partner_id': self.partner_id.id,
            'date_invoice': self.date,
            'user_id': self.env.uid,
            'company_id': self.env.user.company_id.id,
            'currency_id': self.env.user.company_id.currency_id.id,
            'account_id': self.account_id.id,
            'journal_id': self.journal_id.id,
            'invoice_line_ids': line_vals
        }
        invoice_id = self.env['account.invoice'].create(invoice_vals)
        # Нэхэмжлэхийг баталж холбоотой журналын бичилт үүсэх
        invoice_id.action_invoice_open()

    @api.model
    def default_get(self, default_fields):
        '''БҮТЭЗ-үүдийг олноор нь сонгон боловсруулах үед мөр бүрт мэдээллийг оруулах '''
        res = super(LowValueAssetSell, self).default_get(default_fields)
        context = dict(self._context or {})
        lines = []
        if context.get('active_model') == 'low.value.asset' and context.get('active_ids'):
            low_value_assets = self.env[context.get('active_model')].browse(context.get('active_ids'))
            for low_value_asset in low_value_assets:
                lines.append((0, False, {
                    'low_value_asset_id': low_value_asset.id,
                    'value': low_value_asset.value,
                    'value_residual': low_value_asset.value_residual,
                    'sell_value': low_value_asset.value_residual,
                    'low_value_asset_sell_id': self.id}))
        res.update({'line_ids': lines})
        return res

    @api.onchange('partner_id')
    def _onchange_partner(self):
        if self.partner_id.property_account_receivable_id:
            self.account_id = self.partner_id.property_account_receivable_id.id


class LowValueAssetSellLine(models.TransientModel):
    _name = 'low.value.asset.sell.line'

    low_value_asset_id = fields.Many2one('low.value.asset', 'Low value asset', readonly=True)
    value = fields.Float('Value', readonly=True)
    value_residual = fields.Float('Value residual', readonly=True)
    sell_value = fields.Float('Sell value')
    tax_id = fields.Many2one('account.tax', domain=[('type_tax_use', '=', 'sale')])
    low_value_asset_sell_id = fields.Many2one('low.value.asset.sell')
