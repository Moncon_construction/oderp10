# -*- coding: utf-8 -*-
import pytz
from odoo import models, fields, _, api
from datetime import datetime
import time
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT

class LowValueAsset(models.TransientModel):
    _name = 'low.value.asset.transfer'

    def action_transfer(self):
        low_value_asset_obj = self.env['low.value.asset']
        low_value_asset_history_obj = self.env['low.value.asset.history']
        low_value_assets = low_value_asset_obj.browse(self.env.context['active_ids'])

        if low_value_assets.filtered(lambda asset: asset.state != 'using'):
            raise UserError(_("You can only transfer when low value asset's state is in 'Using'"
                              "\n If you are mass transferring, please uncheck any low value asset that the state is not 'Using'"))
        for low_value_asset in low_value_assets:
            if low_value_asset.state != 'scrapped':
                if self.change_type == 'employee':
                    low_value_asset_history_obj.create({
                        'change_type': self.change_type,
                        'old_value': low_value_asset.employee_id.name,
                        'new_value': self.employee_id.name,
                        'changed_date': self.transfer_date or datetime.now(),
                        'changed_user_id': self.env.user.id,
                        'description': self.description,
                        'low_value_asset_id': low_value_asset.id,
                        'old_owner': low_value_asset.employee_id.id,
                        'new_owner': self.employee_id.id,
                    })
                    low_value_asset.write({
                        'employee_id': self.employee_id.id
                    })
                if self.change_type == 'type':
                    low_value_asset_history_obj.create({
                        'change_type': self.change_type,
                        'old_value': low_value_asset.type_id.name,
                        'new_value': self.type_id.name,
                        'changed_date': self.transfer_date or datetime.now(),
                        'changed_user_id': self.env.user.id,
                        'description': self.description,
                        'low_value_asset_id': low_value_asset.id,
                        'old_type': low_value_asset.type_id.id,
                        'new_type': self.type_id.id,
                    })
                    low_value_asset.write({
                        'type_id': self.type_id.id
                    })
                if self.change_type == 'location':
                    low_value_asset_history_obj.create({
                        'change_type': self.change_type,
                        'old_value': low_value_asset.location_id.name,
                        'new_value': self.location_id.name,
                        'changed_date': self.transfer_date or datetime.now(),
                        'changed_user_id': self.env.user.id,
                        'description': self.description,
                        'low_value_asset_id': low_value_asset.id,
                        'old_location': low_value_asset.location_id.id,
                        'new_location': self.location_id.id
                    })
                    low_value_asset.write({
                        'location_id': self.location_id.id
                    })

    change_type = fields.Selection([
        ('employee', 'Employee'),
        ('type', 'Type'),
        ('location', 'Location')], required=True, string="Change type")
    employee_id = fields.Many2one('hr.employee', 'Owner')
    type_id = fields.Many2one('low.value.asset.type', 'Type')
    location_id = fields.Many2one('low.value.asset.location', 'Location')
    description = fields.Text('Description', required=True)
    transfer_date = fields.Datetime('Date', default=lambda *a: time.strftime(DEFAULT_SERVER_DATETIME_FORMAT))

    @api.constrains('transfer_date')
    def transfer_date_constraint(self):
        if self.transfer_date:
            tz = pytz.timezone(self.env.user.tz or pytz.utc)
            low_value_assets = self.env['low.value.asset'].browse(self.env.context['active_ids'])
            error_message = _("Low value asset's transfer date must be after last transfer date.\nAssets:")
            error = False
            assets_error = ""
            max_date = datetime.min
            for obj in low_value_assets:
                history_obj = self.env['low.value.asset.history'].search([('low_value_asset_id', '=', obj.id)], order='changed_date desc', limit=1)
                if history_obj:
                    last_history_date = datetime.strptime(history_obj.changed_date, DEFAULT_SERVER_DATETIME_FORMAT)
                    max_date = last_history_date if max_date < last_history_date else max_date
                    if last_history_date >= datetime.strptime(self.transfer_date, DEFAULT_SERVER_DATETIME_FORMAT):
                        error = True
                        assets_error += "\n %s" % obj.name_get()[0][1]
            if error:
                raise ValidationError(error_message + _('\n Latest date: %s ') % max_date.replace(tzinfo=pytz.utc).astimezone(tz).replace(tzinfo=None) + assets_error)
