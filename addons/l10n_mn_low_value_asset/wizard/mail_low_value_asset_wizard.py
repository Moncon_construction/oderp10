# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools
from datetime import datetime
import urllib


class MailWizardAsset(models.TransientModel):
    """ Wizard to send mail to the owner of the asset. """
    _name = 'mail.wizard.low.value.asset'
    _description = 'Send mail to the owner of low value assets'

    def get_domain_employee(self):
        employee_ids = self.env['low.value.asset'].get_owners_with_low_value_asset()
        return [('id', 'in', employee_ids)]

    employee_ids = fields.Many2many('hr.employee', string='Recipients', domain=get_domain_employee)

    def send(self):
        self.env['low.value.asset'].send_email_to_owners_cron(employee_ids=self.employee_ids.ids)
