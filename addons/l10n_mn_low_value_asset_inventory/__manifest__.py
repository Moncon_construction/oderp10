# -*- coding: utf-8 -*-
{
    'name': "Mongolian Low Value Asset Inventory",
    'version': '1.0',
    'depends': ['l10n_mn_low_value_asset'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Asset Modules',
    'description': """
    Inventory of Low value assets
    """,
    'data': [
        'wizard/inventory_import_wizard_view.xml',
        'views/low_value_asset_inventory.xml',
        'security/ir.model.access.csv',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
