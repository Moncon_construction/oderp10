# -*- coding: utf-8 -*-
import base64
from datetime import datetime, date
from io import BytesIO

from odoo import models, fields, api, _
from odoo.exceptions import UserError
import xlsxwriter


class LowValueAssetInventory(models.Model):
    _name = "low.value.asset.inventory"
    _description = "Inventory of low value asset"

    name = fields.Char(string='Name', required=True)
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    inventory_date = fields.Date(string='Inventory Date', default=datetime.now().date())
    type = fields.Selection([('location', 'Location'), ('owner', 'Owner')])
    owner_id = fields.Many2one('hr.employee', 'Owner')
    location_id = fields.Many2one('low.value.asset.location', 'Location')
    inventory_line = fields.One2many('low.value.asset.inventory.line', 'inventory_id', 'Inventory Lines',
                                     domain=[('asset_inventory_type', '=', 'have')],
                                     states={'done': [('readonly', True)]})
    inventory_loss = fields.One2many('low.value.asset.inventory.line', 'inventory_id', 'Inventory loss',
                                     domain=[('asset_inventory_type', '=', 'loss')],
                                     states={'done': [('readonly', True)]})
    inventory_not_registered = fields.One2many('low.value.asset.inventory.line', 'inventory_id',
                                               'Inventory not registered',
                                               domain=[('asset_inventory_type', '=', 'not_registered')],
                                               states={'done': [('readonly', True)]})
    state = fields.Selection(
        [('draft', 'Draft'), ('process', 'Processing'), ('import', 'Imported'), ('confirm', 'Confirmed'),
         ('done', 'Done')], default='draft')

    def cancel(self):
        self.inventory_line.unlink()
        self.inventory_loss.unlink()
        self.inventory_not_registered.unlink()
        self.state = 'draft'

    def begin(self):
        qry = """
            SELECT DISTINCT low_value_asset_id FROM low_value_asset_history history
            WHERE history.change_type = 'outgo' AND CAST(changed_date as date) <= '%s'
        """ % self.inventory_date
        self._cr.execute(qry)
        ids = self._cr.fetchall()
        assets = self.env['low.value.asset'].search([('date_acquired', '<', self.inventory_date), ('state', '!=', 'draft'), ('id', 'not in', ids)])
        if self.type == 'location':
            assets = assets.filtered(lambda a: a.location_id.id == self.location_id.id)
        elif self.type == 'owner':
            assets = assets.filtered(lambda a: a.employee_id.id == self.owner_id.id)
        for asset in assets:
            self.inventory_line.create({'asset_id': asset.id, 'inventory_id': self.id})
        self.state = 'process'

    def confirm(self):
        self.inventory_line.filtered(lambda l: not l.is_have).write({'asset_inventory_type': 'loss'})
        self.state = 'confirm'

    def done(self):
        self.inventory_line.filtered(lambda l: not l.is_have).write({'asset_inventory_type': 'loss'})
        self.inventory_loss.filtered(lambda l: l.is_have).write({'asset_inventory_type': 'have'})
        self.state = 'done'

    def unlink(self):
        for this in self:
            if this.state == 'done':
                raise UserError(_('You cannot delete an inventory that is done.'))
        return super(LowValueAssetInventory, self).unlink()

    def export_report(self):
        report_obj = self.env['low.value.asset.inventory.report'].create({'inventory_id': self.id})
        return report_obj.export_report()


class LowValueAssetInventoryLine(models.Model):
    _name = "low.value.asset.inventory.line"
    _description = "Inventory of low value asset lines"

    asset_id = fields.Many2one('low.value.asset', 'Low value asset ')
    asset_code = fields.Char('Code')
    owner_id = fields.Many2one('hr.employee', 'Owner')
    location_id = fields.Many2one('low.value.asset.location', 'Location')
    is_have = fields.Boolean('Is have')
    inventory_id = fields.Many2one('low.value.asset.inventory', ondelete='cascade')
    asset_inventory_type = fields.Selection([('not_registered', 'Not registered'),
                                             ('loss', 'Loss'),
                                             ('have', 'Have')], default='have')
    comment = fields.Char(string='Comment')

    @api.model
    def create(self, vals):
        if vals.get('asset_id'):
            asset = self.env['low.value.asset'].browse(vals['asset_id'])
            vals['asset_code'] = asset.barcode
            vals['owner_id'] = asset.employee_id.id
            vals['location_id'] = asset.location_id.id
        return super(LowValueAssetInventoryLine, self).create(vals)
