# -*- coding: utf-8 -*-
import base64

import xlrd
from odoo import models, fields, api, _


class LowValueAssetInventoryImportWizard(models.TransientModel):
    _name = "low.value.asset.inventory.import.wizard"
    _description = "Inventory of low value asset import wizard"

    data = fields.Binary(string='Import File', required=True)
    file_name = fields.Char()
    inventory_id = fields.Many2one('low.value.asset.inventory', 'Asset Inventory', default=lambda self: self._context.get('active_id'))

    @api.one
    def import_data(self):
        book = xlrd.open_workbook(file_contents=base64.decodestring(self.data))
        sheet = book.sheet_by_index(0)
        data = list(set([sheet.cell_value(r, 0) for r in range(1, sheet.nrows)]))
        for obj in data:
            if obj:
                barcode = str(obj)
                if '.' in barcode:
                    barcode = barcode.split('.')[0]
                line = self.inventory_id.inventory_line.filtered(lambda l: l.asset_code == barcode)
                if line:
                    line.is_have = True
                else:
                    self.inventory_id.inventory_not_registered.create({'asset_inventory_type': 'not_registered', 'inventory_id': self.inventory_id.id, 'asset_code': barcode})
        self.inventory_id.state = 'import'

