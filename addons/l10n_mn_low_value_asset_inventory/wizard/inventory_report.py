# -*- coding: utf-8 -*-
import base64
from io import BytesIO
from odoo import models, fields, api, _
from datetime import datetime, timedelta
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell_fast


class LowValueAssetInventoryReport(models.TransientModel):
    _inherit = 'oderp.report.excel.output'
    _name = "low.value.asset.inventory.report"

    inventory_id = fields.Many2one('low.value.asset.inventory')

    def get_header_and_total_col(self):
        return 9, [_('Employee name'), _('Asset type'), _('Asset name'), _('Code'), _('Acquired date'), _('Quantity'), _('Is Have'), _('Difference'), _('Comment')]

    def export_report(self):
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        # форматуудыг зарлаж байна
        format_name = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 11,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter'
        })
        format_filter_center = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 10,
            'bold': False,
            'align': 'center',
            'valign': 'vcenter',
        })
        format_filter_left = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 10,
            'bold': False,
            'align': 'left',
            'valign': 'vcenter',
            'border': 1,
        })
        format_filter_float = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 10,
            'bold': False,
            'align': 'right',
            'valign': 'vcenter',
            'num_format': '#,##0.00',
            'border': 1,
        })

        format_date = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 10,
            'bold': False,
            'align': 'right',
            'valign': 'vcenter'
        })
        format_title = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 10,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter',
            'border': 1,
            'text_wrap': 1,
            'bg_color': '#e7e6e6',
        })
        format_title_float = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 10,
            'bold': True,
            'align': 'left',
            'valign': 'vcenter',
            'border': 1,
            'text_wrap': 1,
            'bg_color': '#e7e6e6',
            'num_format': '#,##0.00',
        })

        report_name = _('Low value asset inventory report')
        sheet = book.add_worksheet('')
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 0
        total_col, headers = self.get_header_and_total_col()
        # баганы тохиргоог оруулж байна
        sheet.hide_gridlines(2)
        sheet.set_default_row(17)
        sheet.set_row(2, 40)
        sheet.set_column('A:A', 18)
        sheet.set_column('B:B', 18)
        sheet.set_column('C:C', 18)
        sheet.set_column('D:D', 15)
        sheet.set_column('E:E', 10)
        sheet.set_column('F:F', 10)
        sheet.set_column('G:G', 10)
        sheet.set_column('H:H', 10)
        sheet.set_column('I:I', 20)
        sheet.merge_range(rowx, 0, rowx, total_col - 1, report_name.upper(), format_name)
        rowx += 1
        sheet.merge_range(rowx, total_col - 2, rowx, total_col - 1, _('Date: %s') % self.inventory_id.inventory_date, format_date)
        rowx += 1
        col = 0
        lines = self.inventory_id.inventory_line | self.inventory_id.inventory_loss
        for header in headers:
            sheet.write(rowx, col, header, format_title)
            col += 1
        col = 0
        rowx += 1
        line_start_row = rowx
        for line in lines:
            sheet.write(rowx, col, line.owner_id.name, format_filter_left)
            col += 1
            sheet.write(rowx, col, line.asset_id.type_id.name, format_filter_left)
            col += 1
            sheet.write(rowx, col, line.asset_id.product_id.name, format_filter_left)
            col += 1
            sheet.write(rowx, col, line.asset_id.barcode, format_filter_left)
            col += 1
            sheet.write(rowx, col, line.asset_id.date_acquired, format_filter_left)
            col += 1
            sheet.write(rowx, col, 1, format_filter_float)
            col += 1
            sheet.write(rowx, col, 1 if line.is_have else 0, format_filter_float)
            col += 1
            sheet.write(rowx, col, 1 if not line.is_have else 0, format_filter_float)
            col += 1
            sheet.write(rowx, col, line.comment or '', format_filter_left)
            col += 1
            rowx += 1
            col = 0
        sheet.write(rowx, col, _('Total'), format_title)
        col += 1
        sheet.write_blank(rowx, col, None, format_title)
        col += 1
        sheet.write_blank(rowx, col, None, format_title)
        col += 1
        sheet.write_blank(rowx, col, None, format_title)
        col += 1
        sheet.write_blank(rowx, col, None, format_title)
        col += 1
        alpha = xl_rowcol_to_cell_fast(rowx, col)[0]
        sheet.write_formula(rowx, col, '{=SUM(%s%s:%s%s)}' % (alpha, line_start_row + 1, alpha, rowx), format_title_float)
        col += 1
        alpha = xl_rowcol_to_cell_fast(rowx, col)[0]
        sheet.write_formula(rowx, col, '{=SUM(%s%s:%s%s)}' % (alpha, line_start_row + 1, alpha, rowx), format_title_float)
        col += 1
        alpha = xl_rowcol_to_cell_fast(rowx, col)[0]
        sheet.write_formula(rowx, col, '{=SUM(%s%s:%s%s)}' % (alpha, line_start_row + 1, alpha, rowx), format_title_float)
        col += 1
        sheet.write_blank(rowx, col, None, format_title)
        rowx += 2
        sheet.merge_range(rowx, 2, rowx, 5, _('Signature of issued accountant:........................................./                                      /'), format_filter_center)

        book.close()
        out = base64.encodestring(output.getvalue())
        report_name = report_name + ': ' + self.inventory_id.inventory_date
        excel_id = self.env['oderp.report.excel.output'].with_context(filename_prefix=report_name).create(
            {'filedata': out})
        return excel_id.export_report()
