# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2014-today Asterisk Technologies LLC (<http://www.asterisk-tech.mn>)
#
##############################################################################

from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo.http import request
import pytz

class mining_dashboard(models.TransientModel):
    _name = 'mining.dashboard'
    _description = 'Mining dashboard'

    @api.multi
    def _get_start_day(self):
        date_time = datetime.today().replace(day=1)
        return date_time.strftime('%Y-%m-%d')

    @api.multi
    def _get_project_ids(self):
        proj_ids = self.env['project.project'].search(
            [('favorite_user_ids', 'in', self.env.uid), ('project_type', '=', 'True')])
        return proj_ids

    @api.multi
    def _get_product_ids(self):
        self.env.cr.execute("""
        select pp.id from product_product pp left join product_template pt on pt.id=pp.product_tmpl_id 
        where pt.is_mining_product = 't' 
        """)
        lines = self.env.cr.dictfetchall()
        prod_ids = []
        for line in lines:
            prod_ids.append(line['id'])
        return prod_ids

    @api.depends('start_date', 'end_date', 'project_ids', 'product_ids', 'measurement', 'group_by')
    @api.multi
    def _get_performance(self):
        if self.project_ids and self.product_ids and self.measurement and self.group_by:
            datas = self.get_data(self.start_date, self.end_date, self.project_ids.ids, self.product_ids.ids, self.measurement, self.group_by)
            total_plan = total_disp = 0
            for data in datas:
                total_plan += data[1] or 0
                total_disp += data[3] or 0
            perf = 0.0
            if total_plan != 0:
                perf = total_disp / total_plan * 100
            else:
                perf = 0
            self.performance_percent = perf

    start_date = fields.Date(strint='Start date', required=True, default=_get_start_day)
    end_date = fields.Date(string='End date', required=True, default= datetime.now().strftime('%Y-%m-%d'))
    project_ids = fields.Many2many('project.project', 'mining_dashboard_project_rel', 'report_id', 'project_id',
                                        string='Project', default=_get_project_ids)
    product_ids = fields.Many2many('product.product', 'mining_dashboard_product_rel','report_id','product_id',
                                   string='Product', default=_get_product_ids)
    group_by = fields.Selection([('by_day', 'By day'),
                                 ('by_month', 'By month'),
                                 ('by_year', 'By year')], string='Group by', default='by_month')
    measurement = fields.Selection([('tn', 'Tn'), ('m3', 'm3')], string='Measure unit', default='tn')
    performance_percent = fields.Float(compute=_get_performance, string='Performance percent',digits=0)

    @api.multi
    def name_get(self):
        res = []
        for obj in self:
            name = _("Mining dashboard")
            res.append((obj.id, name))
        return res

    @api.multi
    def get_data(self, start_date, end_date, project_ids, product_ids, measurement, group_by):
        self._cr.execute(
            "SELECT * FROM (SELECT COALESCE(sur.date,dis.date,plan.date) as date,ROUND(COALESCE(plan)::decimal,2) AS plan, "
            "ROUND(COALESCE(meas_m3)::decimal,2) AS meas_m3, "
            "ROUND(COALESCE(sum_prod)::decimal,2) AS sum_prod, "
            "ROUND(COALESCE(sum_real)::decimal,2) AS sum_real FROM "
            "(SELECT mp.date, CASE WHEN %s = 'tn' THEN sum(mpl.forecast_tn) ELSE sum(mpl.forecast_m3) END as plan "
            "FROM mining_plan mp left join mining_plan_line mpl on (mpl.plan_id=mp.id) "
            "WHERE mp.project_id in %s AND mpl.product_id in %s AND mp.state='approved' group by mp.date) as plan FULL JOIN  "
            "(SELECT mde.date, CASE WHEN %s = 'tn' THEN sum(mpel.sum_tn) ELSE sum(mpel.sum_m3) END as sum_prod,"
            "CASE WHEN %s = 'tn' THEN sum(mpel.real_tonn) ELSE 0 END as sum_real "
            "FROM mining_daily_entry mde "
            "left join mining_production_entry_line mpel on (mpel.production_id=mde.id) "
            "WHERE mpel.is_production='t' and mde.project_id in %s AND mpel.product_id in %s AND mde.state != 'draft' group by mde.date) as dis "
            "ON (plan.date=dis.date) FULL JOIN (SELECT msm.date, sum(msml.amount_by_measurement) as meas_m3 FROM  "
            "mining_surveyor_measurement msm left join mining_surveyor_measurement_line msml on (msml.mining_surveyor_measurement_id=msm.id) "
            "WHERE msm.project_id in %s AND msml.product_id in %s AND msm.state='approved' group by msm.date) as sur ON (sur.date=dis.date) "
            ") as plan_actual where date>=%s and date<=%s order by date "
            ,
            (str(measurement), tuple(project_ids), tuple(product_ids), str(measurement), str(measurement),
             tuple(project_ids), tuple(product_ids), tuple(project_ids), tuple(product_ids), start_date, end_date))

        str_cr = self._cr.fetchall()
        return str_cr

    @api.model
    def get_plan_actual(self, start_date, end_date, project_ids, product_ids, measurement, group_by):
        plan_actual = []
        if project_ids and start_date and end_date:
            str_cr = self.get_data(start_date, end_date, project_ids, product_ids, measurement, group_by)
            monthDict = {1: _('January'), 2: _('February'), 3: _('March'), 4: _('April'), 5: _('May'), 6: _('June'),
                         7: _('July'), 8: _('August'), 9: _('September'), 10: _('October'), 11: _('November'), 12: _('December')}

            months = []
            years = []
            for item in str_cr:
                if datetime.strptime(item[0],'%Y-%m-%d').month not in months:
                    months.append(datetime.strptime(item[0],'%Y-%m-%d').month)
                if datetime.strptime(item[0],'%Y-%m-%d').year not in years:
                    years.append(datetime.strptime(item[0],'%Y-%m-%d').year)
            if group_by == 'by_day':
                for item in str_cr:
                    data = {}
                    data['Date'] = monthDict[datetime.strptime(item[0], '%Y-%m-%d').month] + ' ' + str(
                        datetime.strptime(item[0], '%Y-%m-%d').day)

                    data['Plan'] = item[1] or 0
                    data['Meas'] = item[2] or 0
                    data['Disp'] = item[3] or 0
                    data['Perf'] = item[4] or 0
                    plan_actual.append(data)
            elif group_by == 'by_month':
                for month in months:
                    data = {}
                    plan = meas = disp = perf = 0
                    for item in str_cr:
                        if datetime.strptime(item[0], '%Y-%m-%d').month == month:
                            plan += item[1] or 0
                            meas += item[2] or 0
                            disp += item[3] or 0
                            perf += item[4] or 0
                    data['Date'] = monthDict[month]
                    data['Plan'] = plan
                    data['Meas'] = meas
                    data['Disp'] = disp
                    data['Perf'] = perf
                    plan_actual.append(data)
            else:
                for year in years:
                    data = {}
                    plan = meas = disp = perf = 0
                    for item in str_cr:
                        if datetime.strptime(item[0], '%Y-%m-%d').year == year:
                            plan += item[1] or 0
                            meas += item[2] or 0
                            disp += item[3] or 0
                            perf += item[4] or 0
                    data['Date'] = year
                    data['Plan'] = plan
                    data['Meas'] = meas
                    data['Disp'] = disp
                    data['Perf'] = perf
                    plan_actual.append(data)

        return {'plan_actual': plan_actual}
