# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016-today Asterisk Technologies LLC (<http://www.asterisk-tech.mn>)

#
##############################################################################

from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo import exceptions
from odoo.exceptions import ValidationError, UserError
from odoo.addons.l10n_mn_web.models.time_helper import *


class technic_usage(models.Model):
    _inherit = 'technic.usage'

    is_mining = fields.Boolean(string='Updated by Mining', default=False)
    mining_daily_id = fields.Many2one('mining.daily.entry', string='Mining Daily Entry')


class technic_usage_history(models.Model):
    _inherit = 'technic.usage.history'

    is_mining = fields.Boolean(string='Updated by Mining', default=False)
    mining_daily_id = fields.Many2one('mining.daily.entry', string='Mining Daily Entry')


class mining_daily_entry(models.Model):
    _name = 'mining.daily.entry'
    _discription = 'The Daily Entry'
    _inherit = ["mail.thread"]
    STATE_SELECTION = [
        ('draft', 'Draft'),
        ('approved', 'Approved'),
    ]

    # Тооцоолох
    @api.depends('production_line_ids')
    @api.multi
    def _sum_all(self):
        for obj in self:
            sum_m3 = 0.0
            sum_tn = 0
            sum_mineral_m3 = 0.0
            sum_car = 0
            car = []
            for item in obj.production_line_ids:
                if item.is_production:
                    sum_m3 += item.sum_m3
                    sum_tn += item.sum_tn
                if item.product_id.mining_product_type == 'mineral':
                    sum_mineral_m3 += item.sum_m3
                    if item.dump_id not in car:
                        car.append(item.dump_id)
                        sum_car += 1
            obj.sum_soil_m3 = sum_m3
            obj.sum_coal_tn = sum_tn
            obj.sum_mineral_m3 = sum_mineral_m3
            obj.sum_car = sum_car

    # Нэр оноох Өдөр+Ээлж
    @api.depends('date', 'shift')
    @api.multi
    def _set_name(self):
        for obj in self:
            obj.name = str(obj.date) + '-' + obj.shift

    # Өдөрөөр салгах
    @api.depends('date')
    @api.multi
    def _set_date(self):
        for obj in self:
            date_object = datetime.strptime(obj.date, '%Y-%m-%d')
            obj.year = date_object.year
            obj.month = date_object.month
            obj.day = date_object.day

    # нийлбэр олох
    @api.depends('production_line_ids')
    @api.multi
    def _sum_res_count(self):
        for obj in self:
            sum_res = 0
            sum_m3 = 0
            sum_tn = 0
            for item in obj.production_line_ids:
                sum_res += item.res_count
                sum_m3 += item.res_count * item.body_capacity_m3
                sum_tn += item.res_count * item.body_capacity_tn
            obj.sum_res_count = sum_res
            obj.sum_production_m3 = sum_m3
            obj.sum_production_tn = sum_tn

    @api.multi
    def _get_project_ids(self):
        proj_ids = self.env['project.project'].search(
            [('favorite_user_ids', 'in', self.env.uid), ('project_type', '=', True)])
        if not proj_ids:
            raise exceptions.except_orm(_(True),
                                        _(
                                            u'Please adjust the mining project and the product category on the project!!!'))
        return proj_ids[0]

    name = fields.Char(compute='_set_name', string='Name', readonly=True, store=True)
    date = fields.Date(string='Date', required=True, default=(datetime.now()).strftime('%Y-%m-%d'),
                       states={'approved': [('readonly', True)]},
                       track_visibility='onchange')
    project_id = fields.Many2one('project.project', string='Project', default=_get_project_ids, required=True,
                                 help='The Project',
                                 states={'approved': [('readonly', True)]}, track_visibility='onchange')
    shift = fields.Selection([('day', 'Day'), ('night', 'Night')], string='Shift', default='day', required=True,
                             states={'approved': [('readonly', True)]}, track_visibility='onchange')
    user_id = fields.Many2one('res.users', string='Registration', required=True, readonly=True,
                              default=lambda self: self.env.user.id)
    production_line_ids = fields.One2many('mining.production.entry.line', 'production_id', string='Production Line',
                                          states={'approved': [('readonly', True)]})
    state = fields.Selection(STATE_SELECTION, string='State', readonly=True, track_visibility='onchange',
                             default='draft')
    master_id = fields.Many2one('res.users', string='Master', required=True,
                                track_visibility='onchange')
    notes = fields.Text(string='Terms and Conditions', required=True, states={'approved': [('readonly', True)]})
    year = fields.Integer(compute='_set_date', string='Year', readonly=True, store=True)
    month = fields.Integer(compute='_set_date', string='Month', readonly=True, store=True)
    day = fields.Integer(compute='_set_date', string='Day', readonly=True, store=True)
    sum_soil_m3 = fields.Float(compute='_sum_all', string='Total Product m3',
                               store=True, help="The total amount", track_visibility='onchange')
    sum_coal_tn = fields.Float(compute='_sum_all', string='Total coal tn',
                               store=True, help="The total amount", track_visibility='onchange')
    sum_mineral_m3 = fields.Float(compute='_sum_all', string='Total mineral m3',
                                  store=True, help="The total amount")
    sum_car = fields.Integer(compute='_sum_all', string='Total car with mineral',
                             store=True, help="The total amount")
    motohour_line = fields.One2many('mining.motohour.entry.line', 'motohour_id',
                                    states={'approved': [('readonly', True)]})
    sum_res_count = fields.Integer(compute='_sum_res_count', string='Sum Res', store=True)
    sum_production_m3 = fields.Integer(compute='_sum_res_count', string='Sum Production m3', store=True)
    sum_production_tn = fields.Integer(compute='_sum_res_count', string='Sum Production tn', store=True)
    sum_motohour_time = fields.Float(string='Sum Motohour',
                                     store=True)
    sum_repair_time = fields.Float(string='Sum Repair',
                                   store=True)
    sum_production_time = fields.Float(string='Sum Production',
                                       store=True)
    sum_work_time = fields.Float(string='Sum Worked Time', store=True)
    sum_break_time = fields.Float(string='Sum Normative Break',
                                  store=True)

    _order = 'date desc, shift asc'
    _sql_constraints = [
        ('name_uniq', 'UNIQUE(date, master_id, project_id, shift)', 'Date and Shift must be unique')
    ]

    # Бүх техникийг импортлох
    @api.multi
    def import_technic(self):
        for obj in self:
            ready_technic_ids = self.env['technic'].search([('state', '=', 'ready')])
            for technic in ready_technic_ids:
                line_id = self.env['mining.motohour.entry.line'].search([('motohour_id','=', obj.id),('technic_id','=',technic.id)])
                if not line_id:
                    data = {
                            'technic_id': technic.id,
                            'motohour_id': obj.id,
                            'last_km': technic.last_km,
                            'first_odometer_value': technic.last_motohour
                    }
                    self.env['mining.motohour.entry.line'].create(data)
            for line in obj.motohour_line:
                line.action_get_first_values()

    @api.multi
    def unlink_technic(self):
        for obj in self:
            obj.motohour_line.unlink()

    @api.multi
    def button_dummy(self):
        return True

    # Батлах
    @api.multi
    def action_to_approved(self):
        for obj in self:
            for line in obj.motohour_line:
                if line.technic_id:
                    motohour_usage_uom_id = self.env['usage.uom'].search([('is_motohour','=',True)], limit=1)
                    motohour_norm_id = self.env['usage.uom.line'].search([('technic_norm_id', '=', line.technic_id.technic_norm_id.id),('usage_uom_id','=',motohour_usage_uom_id.id)], limit=1)
                    if motohour_norm_id and line.diff_odometer_value > 0:
                        vals = {
                            'technic_id': line.technic_id.id,
                            'product_uom_id': motohour_norm_id.product_uom_id.id,
                            'usage_value': line.diff_odometer_value,
                            'is_mining': True,
                            'usage_uom_id': motohour_norm_id.usage_uom_id.id,
                            'mining_daily_id': obj.id,
                        }
                        self.env['technic.usage'].create(vals)
                    km_usage_uom_id = self.env['usage.uom'].search([('is_kilometer', '=', True)], limit=1)
                    km_norm_id = self.env['usage.uom.line'].search(
                        [('technic_norm_id', '=', line.technic_id.technic_norm_id.id),
                         ('usage_uom_id', '=', km_usage_uom_id.id)], limit=1)
                    if km_norm_id  and line.last_km > 0:
                        vals = {
                            'technic_id': line.technic_id.id,
                            'product_uom_id': km_norm_id.product_uom_id.id,
                            'usage_value': line.last_km,
                            'is_mining': True,
                            'usage_uom_id': km_norm_id.usage_uom_id.id,
                            'mining_daily_id': obj.id,
                        }
                        self.env['technic.usage'].create(vals)

            obj.write({'state': 'approved'})
        return True

    # Цуцлах
    @api.multi
    def action_to_draft(self):
        for obj in self:
            self.env['task.task'].search([('mining_id', '=', obj.id)]).unlink()
            self.env['technic.usage'].search([('mining_daily_id', '=', obj.id)]).unlink()
            obj.state = 'draft'

    #     Устгах
    @api.multi
    def unlink(self):
        for obj in self:
            if not obj.state == 'draft':
                raise UserError(_('Delete only draft in state'))
            for line in obj.motohour_line:
                line.unlink()
        return super(mining_daily_entry, self).unlink()


class mining_production_entry_line(models.Model):
    _name = 'mining.production.entry.line'
    _description = 'Production Entry Line'

    # тэвшний багтаамж өөрчлөгдөх
    @api.onchange('product_id')
    def on_change_material(self):
        material_obj = self.env['product.product'].browse(self.product_id.id)
        product_type = material_obj.mining_product_type
        if material_obj.mining_product_type == 'mineral':
            product_type = False
        value = {
            'the_from': False,
            'the_for': False,
            'from_pile_id': False,
            'from_location': False,
            'for_pile_id': False,
            'for_location': False,
            'coal_layer': False,
            'is_stone': False,
            'is_sulfur': False,
            'domain_material_type': product_type
        }
        self.write(value)
        return {
            'value': value,
        }

    @api.onchange('the_from')
    def on_change_from(self):
        value = {'from_pile_id': False, 'from_location': False}
        self.write(value)
        return {'value': value}

    @api.onchange('the_for')
    def on_change_for(self):
        value = {'for_pile_id': False, 'for_location': False}
        self.write(value)
        return {'value': value}

    # Тооцоолж хадгалах
    @api.depends('dump_id', 'product_id', 'production_id', 'res_count')
    @api.multi
    def _sum_all(self):
        for obj in self:
            sum_m3 = 0.0
            sum_tn = 0.0
            capacity_m3 = 0.0
            capacity_tn = 0.0
            technic = self.env['technic'].browse(obj.dump_id.id)
            capacity = self.env['mining.technic.configure'].search([('technic_norm_id', '=',
                                                                     technic.technic_norm_id.id)
                                                                       , ('product_id', '=',
                                                                          obj.product_id.id)
                                                                       , ('project_id', '=',
                                                                          obj.production_id.project_id.id)])
            if capacity:
                capacity_m3 = capacity[0].body_capacity_m3
                capacity_tn = capacity[0].body_capacity_tn

            sum_m3 = obj.res_count * capacity_m3
            sum_tn = obj.res_count * capacity_tn

            obj.sum_m3 = sum_m3
            obj.sum_tn = sum_tn
            obj.body_capacity_m3 = capacity_m3
            obj.body_capacity_tn = capacity_tn

    # Огноо авах
    @api.multi
    @api.depends('production_id.date')
    def _set_date(self):
        for obj in self:
            obj.date = obj.production_id.date

    @api.multi
    @api.depends('job_qty_tn', 'land_avg_km', 'sum_tn', 'is_solo_technic')
    def _total_freight_tn_km(self):
        for obj in self:
            if obj.is_solo_technic:
                obj.total_freight_tn_km = obj.job_qty_tn * obj.land_avg_km
            else:
                obj.total_freight_tn_km = obj.sum_tn * obj.land_avg_km

    is_solo_technic = fields.Boolean('Solo Technic')
    production_id = fields.Many2one('mining.daily.entry', string='Production ID', required=True, ondelete='cascade')
    date = fields.Date(compute='_set_date', string='Date', store=True)
    shift = fields.Selection(related='production_id.shift', string='Shift')
    dump_id = fields.Many2one('technic', string='Dump', domain=[('technic_norm_id.technic_type_id.is_auto', '=', 'True')])
    dump_driver_id = fields.Many2one('hr.employee', string="Dump driver")
    dump_driver_workhour = fields.Float('Dump driver workhour')
    product_id = fields.Many2one('product.product', string='Material', help='The Product', required=True)
    domain_material_type = fields.Char(string='Domain Material', size=100)
    the_from = fields.Selection([('location', 'Location'), ('pile', 'Pile')], string='From')
    from_pile_id = fields.Many2one('mining.pile', string='Pile')
    from_location = fields.Many2one('mining.location', string='Location')
    the_for = fields.Selection([('location', 'Location'), ('pile', 'Pile')], string='For')
    for_pile_id = fields.Many2one('mining.pile', string='Pile')
    for_location = fields.Many2one('mining.location', string='Location')
    level = fields.Integer(string='Level', default=1000)
    level_to = fields.Integer(string='Level', default=1000)
    res_count = fields.Integer(string='Res count', default=1)
    excavator_id = fields.Many2one('technic', string='Mechanism', domain=[('technic_norm_id.technic_type_id.is_mechanism', '=', 'True')])
    excavator_driver_id = fields.Many2one('hr.employee', string="Excavator driver")
    excavator_driver_workhour = fields.Float('Excavator driver workhour')
    body_capacity_m3 = fields.Float(compute='_sum_all', string='Capacity m3', store=True)
    body_capacity_tn = fields.Float(compute='_sum_all', string='Capacity tn', store=True)
    sum_m3 = fields.Float(compute='_sum_all', string='Sum m3', store=True)
    sum_tn = fields.Float(compute='_sum_all', string='Sum tn', store=True)
    is_production = fields.Boolean(string='Is Production', default=True)
    is_stone = fields.Boolean(string='Is Stone', readonly=True)
    is_sulfur = fields.Boolean(string='Is Sulfur', readonly=True)
    coal_layer = fields.Selection(
        [('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'), ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'),
         ('10', '10')], 'Coal Layer', readonly=True)
    job_qty_tn = fields.Float(string='Job qty tn')
    job_qty_m3 = fields.Float(string='Job qty m3')
    land_avg_km = fields.Float(string='Land avg km')
    total_freight_tn_km = fields.Float(compute='_total_freight_tn_km', string='Total freight tn/km', store=True)


class mining_motohour_entry_line(models.Model):
    _name = 'mining.motohour.entry.line'
    _description = 'Mining Motohour Entry Line'

    # техникийн төрөл оноох
    @api.depends('technic_id')
    @api.multi
    def _set_type_name(self):
        for obj in self:
            obj_tech = self.env['technic'].browse(obj.technic_id.id)
            obj.technic_type = obj_tech.technic_type_id.name
            obj.technic_name = obj_tech.name

    # Ажилласан цаг
    @api.depends('motohour_cause_line', 'first_odometer_value')
    @api.multi
    def _set_operator_odometer(self):
        for obj in self:
            last_odometer_value = 0
            for cause in obj.motohour_cause_line:
                if cause.start_motohour > last_odometer_value:
                    last_odometer_value = cause.start_motohour
            obj.last_odometer_value = last_odometer_value
            if obj.motohour_cause_line:
                obj.operator_names = obj.motohour_cause_line[0].operator_id.name
            else:
                obj.operator_names = ''
            diff = last_odometer_value - obj.first_odometer_value
            if diff >= 0.0:
                obj.diff_odometer_value = diff
            else:
                obj.diff_odometer_value = 0.0

    @api.depends('first_km', 'motohour_cause_line')
    @api.multi
    def _get_last_km(self):
        for obj in self:
            last_km = 0.0
            for cause in obj.motohour_cause_line:
                if cause.start_km > last_km:
                    last_km = cause.start_km
            diff = last_km - obj.first_km
            if diff >= 0:
                obj.last_km = diff
            else:
                obj.last_km = 0.0

    # Төсөл болон огноо авах
    @api.depends('motohour_id')
    @api.multi
    def _set_date_project(self):
        for obj in self:
            obj.date = obj.motohour_id.date
            obj.project_id = obj.motohour_id.project_id.id

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            name = '%s' % record.technic_name
            result.append((record.id, name))
        return result

    motohour_id = fields.Many2one('mining.daily.entry', string='Motohour ID', required=True, ondelete='cascade')
    technic_id = fields.Many2one('technic', string='Technic', required=True, readonly=True)
    technic_name = fields.Char(compute='_set_type_name', string='Technic Name', store=True)
    technic_type = fields.Char(compute='_set_type_name', string='Type', store=True)
    operator_names = fields.Char(compute='_set_operator_odometer', string='Operators', readonly=True, store=True)
    first_odometer_value = fields.Float(sring='Start shit odometer', states={'approved': [('readonly', True)]})
    last_odometer_value = fields.Float(compute='_set_operator_odometer', string='End shit odometer', readonly=True,
                                       store=True)
    diff_odometer_value = fields.Float(compute='_set_operator_odometer', string='Diff Odometer', store=True)
    motohour_cause_line = fields.One2many('mining.motohour.entry.cause.line', 'motohour_cause_id',
                                          states={'approved': [('readonly', True)]})
    work_diff_time = fields.Float(string='Sum Time', digits=(16, 2),
                                  readonly=True, store=True)
    motohour_time = fields.Float(string='Motohour Time', digits=(16, 2),
                                 readonly=True, store=True)
    repair_time = fields.Float(string='Repair Time', digits=(16, 2),
                               readonly=True, store=True)
    work_time = fields.Float(string='Worked Time', digits=(16, 2),
                             readonly=True, store=True)
    production_time = fields.Float(string='Production Time', digits=(16, 2),
                                   readonly=True, store=True)
    norm_break_time = fields.Float(string='Norm Break Time', digits=(16, 2),
                                   readonly=True, store=True)
    state = fields.Selection(related='motohour_id.state', string='state')
    shift = fields.Selection(related='motohour_id.shift', string='Shift')
    motohour_date = fields.Date(related='motohour_id.date', string='Date')
    date = fields.Date(compute='_set_date_project', string='Date',
                       store=True)
    project_id = fields.Integer(compute='_set_date_project', string='Project',
                                store=True)
    domain_operator = fields.Many2one('res.users', string='Domain operator')
    operator_line = fields.One2many('mining.motohour.entry.operator.line', 'motohour_cause_id',
                                    states={'approved': [('readonly', True)]})
    is_medium_technic = fields.Boolean(string='Is Medium Technic')
    first_km = fields.Float(string='First Km', states={'approved': [('readonly', True)]})
    last_km = fields.Float(string='Last Km', compute='_get_last_km', store=True)

    _order = 'technic_name asc, technic_type desc, date desc'
    # _sql_constraints = [
    #     ('last_km_cons', 'CHECK(last_km >= 0)', 'Error ! Last Km is  >= 0')
    # ]

    @api.multi
    def action_get_first_values(self):
        for obj in self:
            obj.first_odometer_value = sum(moto.usage_value for moto in obj.technic_id.technic_usage_ids if moto.usage_uom_id.is_motohour)
            obj.first_km = sum(km.usage_value for km in obj.technic_id.technic_usage_ids if km.usage_uom_id.is_kilometer)

    @api.multi
    def write(self, vals):
        res = super(mining_motohour_entry_line, self).write(vals)
        work_diff_time = 0.0
        motohour_time = 0.0
        norm_break_time = 0.0
        work_time = 0.0
        production_time = 0.0
        repair_time = 0.0
        for cause_line in self.motohour_cause_line:
            cause_line.check_time()
            cause_line.time_setting()

        for item in self.motohour_cause_line:
            if item.cause_id.calc_production:
                production_time += item.diff_time
            if item.cause_id.cause_type.type == 'smu':
                motohour_time += item.diff_time
            if item.cause_id.cause_type.type == 'smu':
                work_time += item.diff_time
            if item.cause_id.is_repair:
                repair_time += item.diff_time
            if item.cause_id.is_norm_break:
                norm_break_time += item.diff_time
            work_diff_time += item.diff_time

        for obj in self:
            line = self.env['mining.motohour.entry.line'].browse(obj.id)
            self.env.cr.execute(
                "UPDATE mining_motohour_entry_line SET work_diff_time = %s, motohour_time = %s, "
                "work_time = %s, production_time = %s, repair_time = %s,norm_break_time= %s   WHERE id = %s" % (
                    work_diff_time, motohour_time, work_time, production_time, repair_time, norm_break_time, line.id))

            sum_motohour_time = 0.0
            sum_norm_break_time = 0.0
            sum_work_diff_time = 0.0
            sum_production_time = 0.0
            sum_repair_time = 0.0

            for motohour_line in line.motohour_id.motohour_line:
                for cause_line in motohour_line.motohour_cause_line:
                    if cause_line.cause_id.calc_production:
                        sum_production_time += cause_line.diff_time
                    if cause_line.cause_id.cause_type.type == 'smu':
                        sum_motohour_time += cause_line.diff_time
                    if cause_line.cause_id.is_repair:
                        sum_repair_time += cause_line.diff_time
                    if cause_line.cause_id.is_norm_break:
                        sum_norm_break_time += cause_line.diff_time
                    sum_work_diff_time += cause_line.diff_time
            self.env.cr.execute(
                "UPDATE mining_daily_entry SET sum_motohour_time = %s, sum_repair_time = %s, "
                "sum_production_time = %s, sum_work_time = %s, sum_break_time = %s   WHERE id = %s" % (
                    sum_work_diff_time, sum_repair_time, sum_production_time, sum_motohour_time, sum_norm_break_time,
                    line.motohour_id.id))

        return res

    @api.multi
    def unlink(self):
        for obj in self:
            if obj.motohour_cause_line:
                for line in obj.motohour_cause_line:
                    line.unlink()
            if obj.operator_line:
                for oline in obj.operator_line:
                    oline.unlink()
        return super(mining_motohour_entry_line, self).unlink()


class mining_motohour_entry_operator_line(models.Model):
    _name = 'mining.motohour.entry.operator.line'
    _description = 'Mining Motohour Opertator Entr Line'

    # Ажилласан цаг
    @api.depends('motohour_cause_id.operator_line')
    @api.multi
    def _set_time(self):
        for item in self:
            f_value = 0.0
            o_time = 0.0
            first = item.motohour_cause_id.first_odometer_value
            last = item.last_odometer_value
            for line in item:
                if first < line.last_odometer_value < last:
                    first = line.last_odometer_value

            f_value = first
            o_time = last - first
            item.o_motohour_time = o_time
            item.o_production_time = 0.0
            item.first_odometer_value = f_value

    motohour_cause_id = fields.Many2one('mining.motohour.entry.line', string='Cause ID', required=True, ondelete='cascade')
    operator_id = fields.Many2one('hr.employee', string='Operator')
    last_odometer_value = fields.Float(string='Last Odometer')
    first_odometer_value = fields.Float(compute='_set_time', string='First Odometer Value',
                                        readonly=True,
                                        store=True)
    o_production_time = fields.Float(compute='_set_time', string='Production Time',
                                     readonly=True,
                                     store=True)
    o_motohour_time = fields.Float(compute='_set_time', string='Motohour Time',
                                   readonly=True,
                                   store=True)
    last_killometer_value = fields.Float(string='Last km')

    _order = 'last_odometer_value asc'
    _sql_constraints = [
        ('motohour_time', 'CHECK(o_motohour_time >= 0)', 'Error ! Motohour Time is ever >= 0'),
    ]


class mining_motohour_entry_cause_line(models.Model):
    _name = 'mining.motohour.entry.cause.line'
    _description = 'Mining Motohour Cause Line'

    @api.depends('start_time', 'motohour_cause_id')
    @api.multi
    def _set_start_time(self):
        for o in self:
            time = o.start_time
            if o.motohour_cause_id.shift == 'night':
                time += 24
            o.r_start_time = time
            
    @api.depends('finish_motohour', 'start_motohour')
    @api.multi
    def _compute_performance_motohour_km(self):
        for obj in self:
            if obj.finish_motohour > 0:
                obj.performance_motohour = obj.finish_motohour - obj.start_motohour
            else:
                obj.performance_motohour = 0
            if obj.finish_km > 0:
                obj.performance_km = obj.finish_km - obj.start_km
            else:
                obj.performance_km = 0

    @api.onchange('cause_id')
    def on_change_cause(self):
        cause_obj = self.env['mining.motohours.cause'].browse(self.cause_id.id)
        value = {}
        if cause_obj:
            value = {
                'is_repair': cause_obj.is_repair,
                'work_order_id': False
            }
        return {
            'value': value,
        }

    # Тухайн мотоцагийн шалтгааны нэрийг харуулах.
    @api.onchange('cause_id', 'cause_name')
    def onchange_name(self):
        res = {}
        name = ''
        if self.cause_id:
            cause_ids = self.env['mining.motohours.cause'].search([('id', '=', self.cause_id.id)])
            for cause in cause_ids:
                name = cause.cause_name
                res.update({'cause_name': name})
            return {'value': res}

    # Эхлэх цаг авах
    @api.multi
    def _get_start_time(self):
        now_time = get_day_like_display(datetime.now(), self.env.user)
        return now_time.hour + float(now_time.minute) / 60

    # Өмнөх мөр
    @api.multi
    def _default_previous_cause_line_id(self):
        default_motohour_cause_id = self._context.get('default_motohour_cause_id', [])
        if default_motohour_cause_id:
            motohour_cause_obj = self.env['mining.motohour.entry.line'].browse(default_motohour_cause_id)
            previous_cause_line_id = self.env['mining.motohour.entry.cause.line'].search([('motohour_cause_id', '=', default_motohour_cause_id)], order='start_motohour desc', limit=1)
            if not previous_cause_line_id:
                previous_entry_lines = self.env['mining.motohour.entry.line'].search([('id', '!=', default_motohour_cause_id),('motohour_id.state', '=', 'approved'), ('technic_id', '=', motohour_cause_obj.technic_id.id)], order='date desc', limit=1)
                previous_cause_line_id = self.env['mining.motohour.entry.cause.line'].search([('motohour_cause_id', '=', previous_entry_lines.id)], order='start_motohour, start_km desc', limit=1)
                return previous_cause_line_id.id
            else:
                return previous_cause_line_id.id

    @api.depends('r_start_time')
    def _compute_sequence(self):
        for obj in self:
            schedule = self.env['mining.schedule'].search([('project_id','=',obj.motohour_cause_id.project_id),
                                                   ('shift','=', obj.motohour_cause_id.shift),('active','=',True)])
            start_times = []
            greater_start_times = []
            lower_start_times = []
            for item in obj.motohour_cause_id.motohour_cause_line:
                if obj.motohour_cause_id.shift == 'day':
                    start_times.append(item.r_start_time)
                else:
                    greater_start_times.append(item.r_start_time) if schedule and 24 <= item.r_start_time <= schedule[0].end_time + 24 else lower_start_times.append(item.r_start_time)

            start_times.sort()
            greater_start_times.sort()
            lower_start_times.sort()
            if obj.motohour_cause_id.shift == 'day' and start_times:
                seq = 0
                for time in start_times:
                    seq += 1
                    if time == obj.r_start_time:
                        obj.sequence = seq

            else:
                seq = 0
                if lower_start_times:
                    for time in lower_start_times:
                        seq += 1
                        if time == obj.r_start_time:
                            obj.sequence = seq
                if greater_start_times:
                    for time in greater_start_times:
                        seq += 1
                        if time == obj.r_start_time:
                            obj.sequence = seq

    company_id = fields.Many2one('res.company', string='Company', required=True,
        default=lambda self: self.env.user.company_id)
    previous_cause_line_id = fields.Many2one('mining.motohour.entry.cause.line', string='Previous Cause Line', default=_default_previous_cause_line_id)
    motohour_cause_id = fields.Many2one('mining.motohour.entry.line', string='Cause ID', required=True, ondelete='cascade')
    cause_id = fields.Many2one('mining.motohours.cause', string='Cause', required=True)
    cause_name = fields.Char(related='cause_id.cause_name', string='Cause Name')
    is_medium_technic = fields.Boolean(related='motohour_cause_id.is_medium_technic', string='Is Medium Technic')
    start_time = fields.Float(string='Start Time', digits=(16, 2), default=_get_start_time)
    r_start_time = fields.Float(compute='_set_start_time', digits=(16, 2), string='Real Start Time',
                                readonly=True, store=True)
    diff_time = fields.Float(string='Diff Time', digits=(16, 2), readonly=True)
    description = fields.Char(string='Description', size=150)
    location_id = fields.Many2one('mining.location', string='Location')
    color = fields.Selection(related='cause_id.color', string='Color')
    is_repair = fields.Boolean(string='Is Repair')
    percentage = fields.Integer(string='Percentage', readonly=True, invisible=True)
    product_id = fields.Many2one('product.product', string='Product', domain=[('is_mining_product', '=', True)])
    operator_id = fields.Many2one('res.users', string='Operator')
    carrier_id = fields.Many2one('technic', 'Carrier', domain=[('technic_type_id.is_mechanism', '=', True)])
    carrier_operator_id = fields.Many2one('res.users', string='Carrier Operator')
    start_motohour = fields.Float(string='Start Motohour')
    start_km = fields.Float(string='Start KM', default=0)
    finish_motohour = fields.Float(string='Start Motohour', default=0)
    finish_km = fields.Float(string='Start KM', default=0)
    performance_motohour = fields.Float(string='Performance Motohour', compute='_compute_performance_motohour_km', store=True)
    performance_km = fields.Float(string='Performance KM', compute='_compute_performance_motohour_km', store=True)
    sequence = fields.Integer(compute=_compute_sequence, string='Sequence', store=True)

    _order = 'sequence asc'
    _sql_constraints = [
        ('percentage', 'CHECK(percentage >= 0)', 'Error ! 0 <= percentage is ever <= 100'),
    ]
    
    @api.onchange('previous_cause_line_id')
    def onchange_previous_cause_line_id(self):
        self.operator_id = self.previous_cause_line_id.operator_id.id
        if self.company_id.automate_check_motohour_error and not self.previous_cause_line_id.cause_id.cause_type.type == 'smu':
            self.start_motohour = self.previous_cause_line_id.start_motohour
        else:
            self.start_motohour = 0
    
    @api.model
    def create(self, vals):
        sup_id = super(mining_motohour_entry_cause_line, self).create(vals)
        obj = self.browse([sup_id.id])[0]
        r_start_time = self._get_time(self.motohour_cause_id.shift, vals['start_time'])
        self.env.cr.execute(
            "UPDATE mining_motohour_entry_cause_line SET r_start_time = %s WHERE id = %s" % (
                r_start_time, sup_id.id))
        obj.check_time()
        obj.time_setting()
        obj.check_other()
        return sup_id

    @api.multi
    def write(self, vals):
        if 'motohour_cause_id' not in vals:
            super(mining_motohour_entry_cause_line, self).write(vals)
            self.check_time()
            self.time_setting()
            self.check_other()
        return True

    @api.multi
    def unlink(self):
        obj = self.browse(self.ids[0])
        line_id = obj.id
        sup_id = super(mining_motohour_entry_cause_line, self).unlink()

        if line_id != obj.id:
            self.browse(line_id).time_setting()
        return sup_id

    @api.multi
    def _get_time(self, shift, time):
        if shift == 'night':
            time += 24.0
        return time
    
    # Хугацаа шалгах
    @api.multi
    def check_other(self):
        for obj in self:
            total_hour = 0
            if obj.previous_cause_line_id:
                if obj.previous_cause_line_id.start_km > obj.start_km and obj.company_id.right_motohour:
                    raise exceptions.except_orm(_('Warning'), _('Please register right start km.'))
            if obj.start_motohour and obj.previous_cause_line_id:
                obj.previous_cause_line_id.finish_motohour = obj.start_motohour
            if obj.start_km and obj.previous_cause_line_id:
                obj.previous_cause_line_id.finish_km = obj.start_km

    # Хугацаа шалгах
    @api.multi
    def check_time(self):
        st_time = float(self.start_time)
        hours = int(st_time)
        minutes = int((st_time - hours) * 60)
        project = self.env['project.project'].browse(self.motohour_cause_id.project_id)
        mining_schedule = self.env['mining.schedule'].search(
            [('project_id', '=', project.id), ('shift', '=', self.motohour_cause_id.shift),
             ('active', '=', True)])

        if mining_schedule:
            if mining_schedule[0].validity_date_beginning <= self.motohour_cause_id.motohour_date <= mining_schedule[
                0].validity_date_ending:
                if mining_schedule[0].shift == 'day':
                    if self.motohour_cause_id.shift == 'day' and (
                            mining_schedule[0].start_time > st_time or mining_schedule[0].end_time < st_time):
                        raise exceptions.except_orm(_('Wrong time'), _(' Time must be between %s and %s ') % (
                            '{0:02.0f}:{1:02.0f}'.format(*divmod(float(mining_schedule[0].start_time) * 60, 60)),
                            '{0:02.0f}:{1:02.0f}'.format(*divmod(float(mining_schedule[0].end_time) * 60, 60))))
                else:
                    if mining_schedule[0].end_time > st_time: st_time += 24
                    if self.motohour_cause_id.shift == 'night' and (
                            mining_schedule[0].start_time > st_time or mining_schedule[0].end_time + 24 < st_time):
                        raise exceptions.except_orm(_('Wrong time'), _(' Time must be between %s and %s ') % (
                            '{0:02.0f}:{1:02.0f}'.format(*divmod(float(mining_schedule[0].start_time) * 60, 60)),
                            '{0:02.0f}:{1:02.0f}'.format(*divmod(float(mining_schedule[0].end_time) * 60, 60))))
            else:
                raise exceptions.except_orm(_('Warning'), _(
                    'There is no mining schedule for the created motohour registration date.'))
        else:
            raise exceptions.except_orm(_('Warning'), _(
                'The mining schedule has not been adjusted for the selected project or shift.'))
        # Night shalgah
        m_date = self.motohour_cause_id.motohour_date
        if hours > 24:
            raise exceptions.except_orm(_('Wrong time'), _(' Wrong time inserted!!!'))
        if hours == 24:
            hours = '00'
            m_date = (datetime.strptime(m_date, '%Y-%m-%d') + timedelta(days=1)).strftime('%Y-%m-%d')
 
        form_date = datetime.strptime(
            m_date + ' ' + str(hours) + ':' + str(minutes) + ':00.00',
            '%Y-%m-%d %H:%M:%S.%f')
        form_date = form_date.replace()
        now_time = get_day_like_display(datetime.now(), self.env.user)
#         if form_date >= now_time:
#             raise exceptions.except_orm(_('Wrong time'), _(' Time is not Time > (Now Time) '))
        for item in self.motohour_cause_id.motohour_cause_line:
            if item.start_time == self.start_time and self.id != item.id:
                raise exceptions.except_orm(_('Wrong time'), _(' Time is UNIQUE'))
        return True

    # Цаг тохируулах
    @api.multi
    def time_setting(self):
        i = 0
        if self.motohour_cause_id:
            lines = self.motohour_cause_id.motohour_cause_line
            project = self.env['project.project'].browse(self.motohour_cause_id.project_id)
            mining_schedule = self.env['mining.schedule'].search(
                [('project_id', '=', project.id), ('shift', '=', self.motohour_cause_id.shift),
                 ('active', '=', True)])
            if mining_schedule:
                if mining_schedule[0].validity_date_beginning <= self.motohour_cause_id.motohour_date <= \
                        mining_schedule[0].validity_date_ending:
                    if mining_schedule[0].shift == 'day':
                        new_lines = []
                        for line in lines:
                            if line.start_time <= mining_schedule[0].end_time:
                                new_lines.append({
                                    'id': line.id,
                                    'time': line.start_time + 24
                                })
                            else:
                                new_lines.append({
                                    'id': line.id,
                                    'time': line.start_time
                                })
                        ids = []
                        for l in sorted(new_lines, key=lambda x: x['time']):
                            ids.append(l.get('id'))
                        for item in self.env['mining.motohour.entry.cause.line'].browse(ids):
                            i += 1
                            obj = self.env['mining.motohour.entry.cause.line'].browse(item.id)
                            total_hour = 0
                            if i < len(lines):
                                diff_time = abs(sorted(new_lines, key=lambda x: x['time'])[i - 1].get('time') -
                                                sorted(new_lines, key=lambda x: x['time'])[i].get('time'))
                                if obj.previous_cause_line_id and obj.company_id.automate_check_motohour_error:
                                    if obj.previous_cause_line_id.cause_id.cause_type.type == 'smu':
                                        total_hour = diff_time * 30 / 100
                                        if obj.previous_cause_line_id.start_motohour + total_hour < obj.start_motohour or obj.previous_cause_line_id.start_motohour - total_hour > obj.start_motohour:
                                            raise exceptions.except_orm(_('Warning'), _('Please register right motohour.'))
                                self.env.cr.execute(
                                    "UPDATE mining_motohour_entry_cause_line SET diff_time = %s WHERE id = %s" % (
                                        diff_time, obj.id))
                            else:
                                e_time = mining_schedule[0].end_time
                                diff_time = 24 + e_time - sorted(new_lines, key=lambda x: x['time'])[i - 1].get('time')
                                if obj.previous_cause_line_id and obj.company_id.automate_check_motohour_error:
                                    if obj.previous_cause_line_id.cause_id.cause_type.type == 'smu':
                                        total_hour = diff_time * 30 / 100
                                        if obj.previous_cause_line_id.start_motohour + total_hour < obj.start_motohour or obj.previous_cause_line_id.start_motohour - total_hour > obj.start_motohour:
                                            raise exceptions.except_orm(_('Warning'), _('Please register right motohour.'))
                                self.env.cr.execute(
                                    "UPDATE mining_motohour_entry_cause_line SET diff_time = %s WHERE id = %s" % (
                                        diff_time, obj.id))
                    else:
                        new_lines = []
                        for line in lines:
                            if line.start_time <= mining_schedule[0].end_time:
                                new_lines.append({
                                    'id': line.id,
                                    'time': line.start_time + 24
                                })
                            else:
                                new_lines.append({
                                    'id': line.id,
                                    'time': line.start_time
                                })
                        ids = []
                        for l in sorted(new_lines, key=lambda x: x['time']):
                            ids.append(l.get('id'))
                        for item in self.env['mining.motohour.entry.cause.line'].browse(ids):
                            i += 1
                            obj = self.env['mining.motohour.entry.cause.line'].browse(item.id)
                            if i < len(lines):
                                diff_time = abs(sorted(new_lines, key=lambda x: x['time'])[i - 1].get('time') -
                                                sorted(new_lines, key=lambda x: x['time'])[i].get('time'))
                                self.env.cr.execute(
                                    "UPDATE mining_motohour_entry_cause_line SET diff_time = %s WHERE id = %s" % (
                                        diff_time, obj.id))
                            else:
                                e_time = mining_schedule[0].end_time
                                diff_time = 24 + e_time - sorted(new_lines, key=lambda x: x['time'])[i - 1].get('time')
                                self.env.cr.execute(
                                    "UPDATE mining_motohour_entry_cause_line SET diff_time = %s WHERE id = %s" % (
                                        diff_time, obj.id))
                else:
                    raise exceptions.except_orm(_('Warning'), _(
                        'There is no mining schedule for the created motohour registration date.'))
            else:
                raise exceptions.except_orm(_('Warning'), _(
                    'The mining schedule has not been adjusted for the selected project or shift.'))
        return True


class MiningSchedule(models.Model):
    _name = 'mining.schedule'
    _description = 'Mining Schedule'

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            name = '%s - %s (%s - %s)' % (record.validity_date_beginning, record.validity_date_ending,
                                          '{0:02.0f}:{1:02.0f}'.format(*divmod(float(record.start_time) * 60, 60)),
                                          '{0:02.0f}:{1:02.0f}'.format(*divmod(float(record.end_time) * 60, 60)))
            result.append((record.id, name))
        return result

    start_time = fields.Float(string='Start Time', required=True)
    end_time = fields.Float(string='End time', required=True)
    validity_date_beginning = fields.Date(string='The beginning of validity date', required=True)
    validity_date_ending = fields.Date(string='The ending of validity date', required=True)
    active = fields.Boolean(string='Active', default=True)
    project_id = fields.Many2one('project.project', string='Project', required=True,
                                 domain=[('project_type', '=', True)])
    shift = fields.Selection([('day', 'Day'), ('night', 'Night')], string='Shift', required=True)

    _sql_constraints = [
        ('name_uniq', 'CHECK(1=1)',
         'Validity date beginning and Validity date ending must be unique'),
        ('validity_date_ending_greater', 'CHECK (validity_date_ending>validity_date_beginning)',
         'Error ! Validity date ending is over validity date beginning'),
        ('start_time_check', 'CHECK (start_time>=0.0 AND start_time<=24.0)',
         "Start time must be between 00:00 and 24:00!"),
        ('end_time_check', 'CHECK (end_time>=0.0 AND end_time<=24.0)',
         "End time must be between 00:00 and 24:00!"),
    ]

    @api.model
    def create(self, vals):
        schedules = self.env['mining.schedule'].search(
            [('project_id', '=', vals.get('project_id')), ('shift', '=', vals.get('shift')), ('active', '=', True)])
        if schedules:
            for schedule in schedules:
                if schedule.validity_date_beginning <= vals.get(
                        'validity_date_beginning') <= schedule.validity_date_ending or schedule.validity_date_beginning <= vals.get(
                    'validity_date_ending') <= schedule.validity_date_ending \
                        or vals.get('validity_date_beginning') <= schedule.validity_date_beginning <= vals.get(
                    'validity_date_ending') or vals.get(
                    'validity_date_beginning') <= schedule.validity_date_ending <= vals.get('validity_date_ending'):
                    raise exceptions.except_orm(_('Error'), _(
                        'Validity date beginning and Validity date ending must be unique.'))
        return super(MiningSchedule, self).create(vals)

    @api.multi
    def write(self, vals):
        res = super(MiningSchedule, self).write(vals)
        schedules = self.env['mining.schedule'].search(
            [('project_id', '=', self.project_id.id), ('shift', '=', self.shift), ('active', '=', True),
             ('id', '!=', self.id)])

        if schedules:
            for schedule in schedules:
                if schedule.validity_date_beginning <= self.validity_date_beginning <= schedule.validity_date_ending or schedule.validity_date_beginning <= self.validity_date_ending <= schedule.validity_date_ending \
                        or self.validity_date_beginning <= schedule.validity_date_beginning <= self.validity_date_ending or self.validity_date_beginning <= schedule.validity_date_ending <= self.validity_date_ending:
                    raise exceptions.except_orm(_('Error'), _(
                        'Validity date beginning and Validity date ending must be unique.'))
        return res
