# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016-today Asterisk Technologies LLC (<http://www.asterisk-tech.mn>)

#
##############################################################################

from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo import exceptions


class mining_hole(models.Model):
    _name = 'mining.hole'
    _description = 'Mining Hole'
    _inherit = ['mail.thread']

    # Төсөл
    @api.multi
    def _get_project_ids(self):
        proj_ids = self.env['project.project'].search(
            [('favorite_user_ids', 'in', self.env.uid), ('project_type', '=', 'True')])
        if not proj_ids:
            raise exceptions.except_orm(_('No mining projects have been registered!'),
                                 _('Please adjust the mining project and the product category on the project!!!'))
        return proj_ids[0]

    line_ids = fields.One2many('mining.hole.line', 'hole_id', string='Hole Product')
    name = fields.Char(string='Name', required=True, help='The name location')
    project_id = fields.Many2one('project.project', string='Project', required=True, help='The Project',
                                 default=_get_project_ids)
    location_id = fields.Many2one('mining.location', string='Block', required=True)
    hole_deep_m = fields.Float(string='Hole deep m', required=True)
    x = fields.Float(string='X')
    y = fields.Float(string='Y')
    z = fields.Float(string='Z')
    dip = fields.Float(string='Dip')
    azimuth = fields.Float(string='Azimuth')

    # тэвшний багтаамж өөрчлөгдөх
    @api.onchange('project_id')
    def on_change_project_id(self):
        if self.project_id:
            location = self.env['mining.location'].search([('project_id','=',self.project_id.id)])
            if location:
                self.location_id = location[0]
            else:
                self.location_id = False
        else:
            self.location_id = False

    _sql_constraints = [
        ('name_uniq', 'unique(project_id,location_id,name)', 'Reference must be unique per Name Location! '),
    ]


class hole_element(models.Model):
    _name = 'hole.element'
    _description = 'Mining hole element'

    name = fields.Char(string='Element')


class mining_hole_line(models.Model):
    _name = 'mining.hole.line'
    _description = 'Mining Hole Line'

    @api.onchange('to_point', 'from_point', 'length')
    def onchange_length(self):
        if self.to_point and self.from_point:
            self.length = self.from_point - self.to_point

    hole_id = fields.Many2one('mining.hole', string='Items')
    from_point = fields.Float(string='End Point')
    to_point = fields.Float(string='Start Point')
    length = fields.Float(string='Diff Point')
    product_id = fields.Many2one('product.product', string='Product', help='Mining product of Project',
                                 track_visibility='onchange')
    element_id = fields.Many2one('hole.element', string='Element')
    content = fields.Integer(string='%')
