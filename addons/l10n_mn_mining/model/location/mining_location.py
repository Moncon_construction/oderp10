# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2014-today ASterisk-Tech LLC (<http://www.asterisk-tech.mn>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import api, fields, models, _
from datetime import datetime
from odoo import exceptions


# Блокын бараа материалуудын бүртгэл
class location_product(models.Model):
    _name = 'location.product'

    location_id = fields.Many2one('mining.location', string='Items')
    product_id = fields.Many2one('product.product', string='Product', help='Product of Location')
    measurement_line_id = fields.Many2one('mining.surveyor.measurement.line', string='Measurement Line', readonly=True)
    total_amount = fields.Float(string='Amount m3')
    total_amount_by_measurement = fields.Float(string='Amount of Location m3 via Markscheider', readonly=True)
    date = fields.Date(string='Markscheider Date')


# Блокын бүртгэл
class mining_location(models.Model):
    _name = 'mining.location'
    _description = 'Mining location'
    _inherit = ['mail.thread']

    # Тухайн блок дээр барааны давхардал гарч байгаа эсэхийг шалгах
    @api.multi
    def _check_product_duplicate(self):
        products = []
        for line in self.line_id:
            if line.product_id.id not in products:
                products.append(line.product_id.id)
        if len(products) != len(self.line_id):
            return False
        return True

        # Зөвхөн Ноорог төлөвтэй бүртгэлийг устгах

    @api.multi
    def unlink(self):
        for loc in self:
            if loc.state == 'draft':
                return super(mining_location, self).unlink()
            else:
                raise exceptions.except_orm(_('Invalid Action!'),
                                            _('In order to delete a mining location, you must Draft it first.'))

    @api.multi
    def _get_project_ids(self):
        proj_ids = self.env['project.project'].search(
            [('favorite_user_ids', 'in', self.env.uid), ('project_type', '=', True)])
        if not proj_ids:
            raise exceptions.except_orm(_(True),
                                        _(u'Please adjust the mining project and the product category on the project!!!'))
        return proj_ids[0]

    _constraints = [
        (_check_product_duplicate, 'Product is unique of block!!!', ['line_id'])
    ]

    line_id = fields.One2many('location.product', 'location_id', string='Location Product')
    state = fields.Selection([('draft', 'New'), ('approved', 'Approved')],
                             string='Status', readonly=True, track_visibility='onchange', default='draft')
    name = fields.Char(string='Name', size=50, required=True, help='The name location', track_visibility='onchange')
    where = fields.Char(string='Where', size=128, required=True, help='Where is the place?',track_visibility='onchange')
    project_id = fields.Many2one('project.project', string='Project', required=True, help='The Project',
                                 default=_get_project_ids, track_visibility='onchange')
    high = fields.Float(string='High')
    length = fields.Float(string='Length')
    width = fields.Float(string='Width')

    _sql_constraints = [
        ('name_uniq', 'unique(project_id,name)', 'Reference must be unique per Name!'),
    ]

    # Зөвшөөрөх
    @api.multi
    def set_approved(self, vals):
        return self.write({'state': 'approved'})

    # Ноороглох
    @api.multi
    def set_draft(self, vals):
        return self.write({'state': 'draft'})
