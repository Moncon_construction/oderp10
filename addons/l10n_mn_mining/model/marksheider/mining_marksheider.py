# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016-today Asterisk Technologies LLC (<http://www.asterisk-tech.mn>)

#
##############################################################################

from odoo import api, fields, models, _
from odoo import exceptions
from datetime import date, datetime, timedelta

TYPE_SELECTION = [('mining', 'Mining')
    , ('repair', 'Repair')
    , ('env', 'Environmental')
    , ('engineer', 'Engineer work')
    , ('other', 'Other')]


# Маркшейдерийн хэмжилтийн бүртгэлийн мөрүүд
class mining_surveyor_measurement_line(models.Model):
    _name = 'mining.surveyor.measurement.line'
    _description = 'Mining Surveyor Measurement Line'

    location_id = fields.Many2one('mining.location', string='Location', required=True)
    mining_surveyor_measurement_id = fields.Many2one('mining.surveyor.measurement',
                                                     string='Mining Surveyor Measurement',
                                                     help='Mining surveyor measurement', ondelete='cascade')
    product_id = fields.Many2one('product.product', string='Material', required=True, help='The Product',
                                 domain=[('is_mining_product', '=', 'True')])
    amount_by_measurement = fields.Float(string='Amount by Measurement')
    type_measurement = fields.Selection(
        [('is_production', 'Is Production'), ('is_reclamation', 'Is Reclamation'), ('is_engineer', 'Is Engineer')],
        'Type Measurement')
    bcm_coefficient = fields.Float(string='BCM Coefficient', digits=(16, 3), readonly=True)
    uom_id = fields.Char(string='Unit of Measure', readonly=True)

    _sql_constraints = [
        ('product_uniq', 'UNIQUE(mining_surveyor_measurement_id,product_id,location_id)',
         'Product and Location must be unique!')
    ]
    _defaults = {
        'type_measurement': 'is_production',
    }

    # Тухайн бараа материалын BCM ийг олж байна.
    @api.onchange('product_id', 'bcm_coefficient', 'uom_id')
    def onchange_bcm(self):
        res = {}
        bcm = 0.00
        uom_id = 0
        if self.product_id:
            material_ids = self.env['product.product'].search([('id', '=', self.product_id.id)])
            for material in material_ids:
                bcm = material.bcm_coefficient
                uom_id = material.uom_id.name
                res.update({'bcm_coefficient': bcm,
                            'uom_id': uom_id})
            return {'value': res}

    # Засварлагдахгүй талбарт onchange функцийн утгийг хадгалах
    @api.multi
    def write(self, vals):
        if vals.get('product_id'):
            product_obj = self.env['product.product'].browse(vals.get('product_id'))
            vals.update({'bcm_coefficient': product_obj.bcm_coefficient,
                         'uom_id': product_obj.uom_id.name})
        return super(mining_surveyor_measurement_line, self).write(vals)

    @api.model
    def create(self, vals):
        if vals.get('product_id'):
            product_obj = self.env['product.product'].browse(vals.get('product_id'))
            vals.update({'bcm_coefficient': product_obj.bcm_coefficient,
                         'uom_id': product_obj.uom_id.name})
        return super(mining_surveyor_measurement_line, self).create(vals)


# Маркшейдерийн хэмжилтийн бүртгэл
class mining_surveyor_measurement(models.Model):
    _name = 'mining.surveyor.measurement'
    _description = 'Mining Surveyor Measurement'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    # Өдөрөөр салгах
    @api.depends('date')
    @api.multi
    def _set_date(self):
        date_object = datetime.strptime(self.date, '%Y-%m-%d')
        self.year = date_object.year
        self.month = date_object.month
        self.day = date_object.day

    # Нийт хэмжээг олох
    @api.depends('line_ids')
    @api.multi
    def _total_amount(self):
        total = 0.00
        total_tn = 0.00
        avg_bcm = 0.00
        bcm_count = 0
        for line in self.line_ids:
            total += line.amount_by_measurement
            total_tn += line.amount_by_measurement * line.bcm_coefficient
            avg_bcm += line.bcm_coefficient
            bcm_count += 1

        if bcm_count != 0:
            avg_bcm = avg_bcm / bcm_count

        self.total_amount = total
        self.total_amount_tn = total_tn
        self.avg_bcm_coefficient = avg_bcm

    # Тухайн бүртгэл дээрх Блокын барааны давхцал шалгах
    @api.multi
    def _check_product_duplicate(self):
        products = 0
        block = 0
        lines = 0
        for line in self.line_ids:
            lines = line.id
            block = line.location_id
            products = line.product_id
            for line2 in self.line_ids:
                if line2.id != lines and block == line2.location_id and products == line2.product_id:
                    return False
        return True

    @api.multi
    def _get_project_ids(self):
        proj_ids = self.env['project.project'].search(
            [('favorite_user_ids', 'in', self.env.uid), ('project_type', '=', True)])
        if not proj_ids:
            raise exceptions.except_orm(_('No mining projects have been registered!'),
                                        _('Please adjust the mining project and the product category on the project!!!'))
        return proj_ids[0]

    _constraints = [
        (_check_product_duplicate, 'Product of Block is duplicate !!!', ['line_ids'])
    ]

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            name = '%s - %s / %s' % ('Surveyor Measurement', record.date, record.project_id.name)
            result.append((record.id, name))
        return result

    date = fields.Date(string='Date', required=True, states={'draft': [('readonly', False)]},
                       default=(datetime.now()).strftime('%Y-%m-%d'))
    project_id = fields.Many2one('project.project', string='Project',
                                 domain="[(create_uid,'in','favorite_user_ids.ids')]", required=True,
                                 help='The Project',
                                 states={'draft': [('readonly', False)]}, default=_get_project_ids)
    user_id = fields.Many2one('res.users', string='Marksheider Engineer', required=True,
                              default=lambda self: self.env.user.id, )
    line_ids = fields.One2many('mining.surveyor.measurement.line', 'mining_surveyor_measurement_id',
                               string='Mining Surveyor Measurement Lines', readonly=True,
                               states={'draft': [('readonly', False)]})
    state = fields.Selection([('draft', 'New'), ('approved', 'Approved')],
                             string='Status', readonly=True, track_visibility='onchange', default='draft')
    description = fields.Text(string='More Description')
    total_amount = fields.Float(compute='_total_amount', string='Total Amount m3', store=True)
    total_amount_tn = fields.Float(compute='_total_amount', string='Total Amount tn', store=True)
    avg_bcm_coefficient = fields.Float(compute='_total_amount', string='Average BCM', store=True)
    cats = fields.Many2many('product.category', 'project_id', 'project_product_category', string='Cates')
    year = fields.Integer(compute='_set_date', string='Year', readonly=True, store=True)
    month = fields.Integer(compute='_set_date', string='Month', readonly=True, store=True)
    day = fields.Integer(compute='_set_date', string='Day', readonly=True, store=True)

    _order = "date asc"
    _sql_constraints = [
        ('date_project_uniq', 'UNIQUE(date,project_id)', 'Date and Project  must be unique!')
    ]

    # Зөвхөн Ноорог төлөвтэй бүртгэлийг устгах
    @api.multi
    def unlink(self):
        for obj in self:
            if obj.state == 'draft':
                return super(mining_surveyor_measurement, self).unlink()
            else:
                raise exceptions.except_orm(_('Invalid Action!'),
                                            _('In order to delete a mining surveyor measurement, you must Draft it first.'))

    # Тухайн бүртгэлийг батлах ба блокуудын хэмжилтийг шинэчлэх
    @api.multi
    def confirm(self):
        obj = self.env['mining.surveyor.measurement'].search([('state', '=', 'approved'), ('date', '=', self.date)])
        if obj:
            raise exceptions.except_orm(_('Error'), _('On this date, another measurement has already been approved.'))
        else:
            self.write({'state': 'approved'})
        return True

    # Тухайн бүртгэлийг Ноорог төлөвт оруулах
    @api.multi
    def action_to_draft(self):
        return self.write({'state': 'draft'})
