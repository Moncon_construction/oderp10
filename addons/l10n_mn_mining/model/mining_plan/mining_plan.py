# -*- coding: utf-8 -*-
##############################################################################
#
#	OpenERP, Open Source Management Solution
#	Copyright (C) 2014-today Asterisk Technologies LLC (<http://www.asterisk-tech.mn>)

##############################################################################

from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo import exceptions


class mining_plan(models.Model):
    _name = 'mining.plan'
    _description = 'Mining Plan'

    STATE_SELECTION = [
        ('draft', 'Draft'),
        ('approved', 'Approved'),
    ]
    _inherit = ["mail.thread"]

    # Нэр оноох Өдөр+Ээлж
    @api.depends('date','project_id')
    @api.multi
    def _set_name(self):
        self.name = str(self.date) + '-' + self.project_id.name

    # Өдөрөөр салгах
    @api.depends('date')
    @api.multi
    def _set_date(self):
        date_object = datetime.strptime(self.date, '%Y-%m-%d')
        self.year = date_object.year
        self.month = date_object.month
        self.day = date_object.day

    # Явцыг тооцоолох
    @api.depends('date','project_id')
    @api.multi
    def _progress_rate(self):
        meas_ids = self.env['mining.surveyor.measurement'].search([('date', '=', self.date),
                                                                   ('project_id', '=', self.project_id.id)])
        sum_actual = 0.0
        for line in meas_ids:
            sum_actual += line.total_amount
        progress = 0
        if self.sum_forecast_m3 > 0:
            progress = (100 * sum_actual) / self.sum_forecast_m3

        self.actual_m3 = sum_actual
        self.progress_rate = progress

    # Тооцоолох
    @api.depends('plan_line_ids')
    @api.multi
    def _sum_all(self):
        bud_m3 = 0.0
        bud_tn = 0.0
        for_m3 = 0.0
        for_tn = 0.0
        bud_min_m3 = 0.0
        for_min_m3 = 0.0

        for item in self.plan_line_ids:
            bud_m3 += item.budget_m3
            for_m3 += item.forecast_m3
            bud_tn += item.budget_tn
            for_tn += item.forecast_tn
            if item.product_id.mining_product_type == 'mineral':
                bud_min_m3 += item.budget_m3
                for_min_m3 += item.forecast_m3
        self.sum_budget_m3 = bud_m3
        self.sum_forecast_m3 = for_m3
        self.sum_budget_tn = bud_tn
        self.sum_forecast_tn = for_tn
        self.sum_budget_mineral_m3 = bud_min_m3
        self.sum_forecast_mineral_m3 = for_min_m3

    # Төсөл авах
    @api.multi
    def _get_project_ids(self):
        proj_ids = self.env['project.project'].search(
            [('favorite_user_ids', 'in', self.env.uid), ('project_type', '=', True)])
        if not proj_ids:
            raise exceptions.except_orm(_('No mining projects have been registered!'),
                                        _('Please adjust the mining project and the product category on the project!!!'))
        return proj_ids[0]

    name = fields.Char(compute='_set_name', string='Name', readonly=True, store=True)
    date = fields.Date(string='Date', required=True, states={'approved': [('readonly', True)]},
                       default=(datetime.now()).strftime('%Y-%m-%d'))
    project_id = fields.Many2one('project.project', string='Project', required=True, help='The Project',
                                 states={'approved': [('readonly', True)]}, default=_get_project_ids)
    plan_line_ids = fields.One2many('mining.plan.line', 'plan_id', string='Plan Line',
                                states={'approved': [('readonly', True)]})
    plan_technic_line_ids = fields.One2many('mining.plan.technic.line', 'plan_id', 'Technic Plan Line',
                                        states={'approved': [('readonly', True)]})
    state = fields.Selection(STATE_SELECTION, string='State', readonly=True, track_visibility='onchange',
                             default='draft')
    year = fields.Integer(compute='_set_date', string='Year', readonly=True,
                          store=True)
    month = fields.Integer(compute='_set_date', string='Month', readonly=True,
                           store=True)
    day = fields.Integer(compute='_set_date', string='Day', readonly=True,
                         store=True)
    description = fields.Text(string='Description', states={'approved': [('readonly', True)]})
    progress_rate = fields.Float(compute='_progress_rate', string='Forecast Progress Rate',
                                 group_operator="avg",
                                 help="Percent of tasks closed according to the total of tasks todo.",
                                 store=True)
    actual_m3 = fields.Float(compute='_progress_rate', string='Actual m3',
                             store=True)
    sum_budget_m3 = fields.Float(compute='_sum_all', string='Sum Budget m3',
                                 track_visibility='onchange',
                                 store=True)
    sum_forecast_m3 = fields.Float(compute='_sum_all', string='Sum Forecast m3',
                                   track_visibility='onchange',
                                   store=True)
    sum_budget_tn = fields.Float(compute='_sum_all', string='Sum Budget tn',
                                 store=True)
    sum_forecast_tn = fields.Float(compute='_sum_all', string='Sum Forecast tn',
                                   store=True)
    sum_forecast_mineral_m3 = fields.Float(compute='_sum_all', string='Sum Forecast Mineral m3',
                                           store=True)
    sum_budget_mineral_m3 = fields.Float(compute='_sum_all', string='Sum Budget Mineral m3',
                                         store=True)

    _order = 'date asc'
    _sql_constraints = [
        ('name_uniq', 'UNIQUE(name)', 'Date must be unique')
    ]

    # Бүх техникийг импортлох
    @api.multi
    def import_technic(self):
        # Уулын төсөлд хамааралтай технкүүд (draft төлөвгүй)
        technic_ids = self.env['technic'].search([('technic_norm_id.scoop', '=', 'True'),
                                                  ('state', '=', 'ready'),
                                                  ('project', '=', self.project_id.id)])
        for item in self.plan_technic_line_ids:
            if item.technic_id.id in technic_ids.ids:
                technic_ids.ids.remove(item.technic_id.id)
        technic_line_ids = []
        for technic in self.env['technic'].browse(technic_ids.ids):
            mining_technic_ids = self.env['mining.technic.configure'].search([('project_id', '=', self.project_id.id),('technic_norm_id','=',technic.technic_norm_id.id)])
            if mining_technic_ids:
                data = {
                    'plan_id': self.id,
                    'technic_id': technic.id,
                }
                m_line_ids = self.env['mining.plan.technic.line'].create(data).ids
                technic_line_ids.append(m_line_ids)
        technic_line_ids = self.env['mining.plan.technic.line'].search([('plan_id', '=', self.id)])
        return {
            'value': {
                'plan_technic_line_ids': technic_line_ids.ids
            }
        }

    # Батлах
    @api.multi
    def action_to_approved(self):
        plan = self.env['mining.plan'].search([('state', '=', 'approved'), ('date', '=', self.date)])
        if plan:
            raise exceptions.except_orm(_('Error'), _('On this date, another plan has already been approved.'))
        else:
            self.write({'state': 'approved'})
        return True

    # Цуцлах
    @api.multi
    def action_to_draft(self):
        self.write({'state': 'draft'})
        return True


class mining_plan_line(models.Model):
    _name = 'mining.plan.line'
    _description = 'Mining Plan Line'

    # Нэр оноох Өдөр+Ээлж
    @api.depends('plan_id','product_id','level','location_id')
    @api.multi
    def _set_name(self):
        for obj in self:
            obj.name = str(obj.plan_id) + '-' + str(obj.product_id.id) + '-' + str(obj.level) + '-' + str(
                obj.location_id.id)

    @api.depends('budget_m3','product_id','forecast_m3')
    @api.multi
    def _sum_all(self):
        for obj in self:
            obj.budget_tn = obj.budget_m3 * obj.product_id.bcm_coefficient
            obj.forecast_tn = obj.forecast_m3 * obj.product_id.bcm_coefficient

    name = fields.Char(compute='_set_name', string='Name', readonly=True, store=True)
    plan_id = fields.Many2one('mining.plan', string='Plan ID', required=True, ondelete='cascade')
    product_id = fields.Many2one('product.product', string='Material', help='The Product', required=True)
    location_id = fields.Many2one('mining.location', string='Location', help='The Mining Location')
    level = fields.Char(string='Level', size=128)
    level_to = fields.Char(string='Level', size=128)
    budget_m3 = fields.Float(string='Budget m3', invisible=True)
    forecast_m3 = fields.Float('Forecast m3')
    budget_tn = fields.Float(compute='_sum_all', string='Budget tn',
                             store=True)
    forecast_tn = fields.Float(compute='_sum_all', string='Forecast tn',
                               store=True)
    bcm_coefficient = fields.Float(string='BCM Coefficient', digits=(16, 3), readonly=True)
    uom_id = fields.Char(string='Unit of Measure', readonly=True)
    is_reclamation = fields.Boolean(string='Is Reclamation')

    _sql_constraints = [
        ('name_uniq', 'UNIQUE(plan_id, product_id, level, location_id)', 'Material, Level, Location must be unique')
    ]

    # Тухайн бараа материалын BCM ийг олж байна.
    @api.onchange('product_id', 'bcm_coefficient', 'uom_id')
    def onchange_bcm(self):
        res = {}
        bcm = 0.00
        uom_id = 0
        if self.product_id:
            materials = self.env['product.product'].search([('id', '=', self.product_id.id)])
            for material in materials:
                bcm = material.bcm_coefficient
                uom_id = material.uom_id.name
                res.update({'bcm_coefficient': bcm,
                            'uom_id': uom_id})
            return {'value': res}

    # Засварлагдахгүй талбарт onchange функцийн утгийг хадгалах
    @api.multi
    def write(self, vals):
        if vals.get('product_id'):
            product_obj = self.env['product.product'].browse(vals.get('product_id'))
            vals.update({'bcm_coefficient': product_obj.bcm_coefficient,
                         'uom_id': product_obj.uom_id.name})
        return super(mining_plan_line, self).write(vals)

    @api.model
    def create(self, vals):
        if vals.get('product_id'):
            product_obj = self.env['product.product'].browse(vals.get('product_id'))
            vals.update({'bcm_coefficient': product_obj.bcm_coefficient,
                         'uom_id': product_obj.uom_id.name})
        return super(mining_plan_line, self).create(vals)


class mining_plan_technic_line(models.Model):
    _name = 'mining.plan.technic.line'
    _description = 'Mining Plan Technic Line'

    @api.depends('plan_id')
    @api.multi
    def _sum_hour(self):
        for item in self:
            sum_norm = 0.0
            technic_norm = item.technic_id.technic_norm_id.id
            production_ids = self.env['production.uom.line'].search([
                ('technic_norm_id', '=', technic_norm)])
            if not production_ids:
                raise exceptions.except_orm(_(u'Уулын техникийн бүтээлийн норм бүртгэгдээгүй байна!'))

            for prod in production_ids:
                if prod.production_norm:
                    sum_norm = item.run_hour * prod.production_norm
                else:
                    sum_norm = 0.0
            item.sum_norm = sum_norm

    plan_id = fields.Many2one('mining.plan', string='Plan ID', required=True, ondelete='cascade')
    technic_id = fields.Many2one('technic', string='Technic', required=True)
    repair_hour = fields.Float(string='Repair Hour',
                               store=True)
    run_hour = fields.Float(string='Run Hour',
                            store=True)
    sum_norm = fields.Float(compute='_sum_hour', string='Norm Production',
                            store=True)
    plan_norm = fields.Float(string='Norm Planning', store=True)

    _sql_constraints = [
        ('name_uniq', 'UNIQUE(plan_id, technic_id)', 'Technic must be unique')
    ]


