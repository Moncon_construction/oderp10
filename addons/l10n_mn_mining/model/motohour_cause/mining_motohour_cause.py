# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016-today Asterisk Technologies LLC (<http://www.asterisk-tech.mn>)

#
##############################################################################

from odoo import api, fields, models, _
from odoo import exceptions


# Мотоцагийн шалгаан
class mining_motohours_cause(models.Model):
    _name = 'mining.motohours.cause'
    _description = 'Mining Motohours Cause'
    _inherit = ['mail.thread']

    cause_name = fields.Char(string='Cause Name', size=100, required=True, track_visibility='onchange')
    name = fields.Char(string='Cause Code', size=10, required=True, track_visibility='onchange')
    cause_type = fields.Many2one('mining.motohours.cause.type', string='Cause type', required=True, help='',
                                  track_visibility='onchange')
    delay_cause_type = fields.Many2one('mining.motohours.cause.delay.type', string='Delay Cause type', help='',
                                  track_visibility='onchange')
    concentrator_name = fields.Char(string='Concentrator', size=40, track_visibility='onchange')
    color = fields.Selection(related='cause_type.color', string='Color', readonly=True,
                             track_visibility='onchange', default='green')
    calc_production = fields.Boolean(string='Calculate is Production')
    calc_actual = fields.Boolean('Calculate is Actual')
    is_repair = fields.Boolean('Repair')
    is_injury = fields.Boolean('Injury')
    is_norm_break = fields.Boolean('Is Normative Break')

    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Reference must be unique per Code!'),
    ]
    _order = 'name asc, cause_type asc'


# Мотоцагийн шалгааны төрөл
class mining_motohours_cause_type(models.Model):
    _name = 'mining.motohours.cause.type'
    _description = 'Mining Motohours Cause'
    _inherit = ['mail.thread']

    TYPE_SELECTION = [('smu', 'RUN A MOTOHOUR WITH ON'), ('non_smu', 'NOT RUN A MOTOHOUR WITH OFF')]

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            name = record.type_name
            result.append((record.id, name))
        return result

    type_name = fields.Char(string='Type Name', size=100, required=True, help='Motohours Cause Type', translate=True,
                             track_visibility='onchange')
    type = fields.Selection(TYPE_SELECTION, string='Type', required=True, track_visibility='onchange', default='non_smu')
    color = fields.Selection([('green', 'Green'), ('blue', 'Blue'), ('darkblue', 'Dark Blue'), ('gold', 'Yellow'),
                               ('darkorange', 'Orange'), ('red', 'Red'), ('brown', 'Brown'), ('purple', 'Purple'),
                               ('magenta', 'Magenta'), ('darkseagreen', 'Darkseagreen')], string='Color', required=True)
    level = fields.Integer(string='Level', required=True)

    _order = 'type_name asc, type asc'
    _sql_constraints = [
        ('name_uniq', 'unique(type_name)', 'Reference must be unique per Name!'),
    ]


class mining_motohours_cause_delay_type(models.Model):
    _name = 'mining.motohours.cause.delay.type'
    _description = 'Mining Motohours Cause Delay Type'
    _inherit = ['mail.thread']

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            name = record.type_name
            result.append((record.id, name))
        return result

    type_name = fields.Char(string='Type Name', size=100, required=True, help='Mining Motohours Cause Delay Type', translate=True,
                             track_visibility='onchange')
    color = fields.Selection([('green', 'Green'), ('blue', 'Blue'), ('darkblue', 'Dark Blue'), ('gold', 'Yellow'),
                               ('darkorange', 'Orange'), ('red', 'Red'), ('brown', 'Brown'), ('purple', 'Purple'),
                               ('magenta', 'Magenta'), ('darkseagreen', 'Darkseagreen')], string='Color', required=True)

    _order = 'type_name asc'
    _sql_constraints = [
        ('name_uniq', 'unique(type_name)', 'Reference must be unique per Name!'),
    ]
