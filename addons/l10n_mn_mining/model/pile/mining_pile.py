# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2014-today ASterisk-Tech LLC (<http://www.asterisk-tech.mn>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo import exceptions


class mining_pile(models.Model):
    _name = "mining.pile"
    _description = "Mining Pile"
    _inherit = ['mail.thread','ir.needaction_mixin']

    # Нэр олгох
    @api.depends('name','project_id')
    @api.multi
    def _set_name(self):
        self.location_name = self.name + '' + str(self.project_id.id)

    # Барааны ангилал олгох
    @api.depends('product_id')
    @api.multi
    def _set_material_type(self):
        self.product_categ = self.product_id.product_tmpl_id.categ_id.name

    # Төсөл авах
    @api.multi
    def _get_project_ids(self):
        proj_ids = self.env['project.project'].search(
            [('favorite_user_ids', 'in', self.env.uid), ('project_type', '=', True)])
        if not proj_ids:
            raise exceptions.except_orm(_('No mining projects have been registered!'),
                                        _('Please adjust the mining project and the product category on the project!!!'))
        return proj_ids[0]

    name = fields.Char(string='Name', size=128, required=True, track_visibility='onchange')
    location_name = fields.Char(compute='_set_name', string='Location Name', store=True,
                                track_visibility='onchange')
    product_id = fields.Many2one('product.product', string='Product', required=True, help='Mining product of Project',
                                 track_visibility='onchange')
    product_type = fields.Selection(related='product_id.mining_product_type', string='Is Waste ')
    product_categ = fields.Char(compute='_set_material_type', string='Product Type', store=True)
    project_id = fields.Many2one('project.project', string='Project', required=True, help='The Project',
                                 track_visibility='onchange', default=_get_project_ids)
    pile_location = fields.Char(string='Pile Location', size=128, track_visibility='onchange')
    balance_by_report_m3 = fields.Float(string='Balance by Report m3', digits=(16, 3), readonly=True)
    balance_by_report_tn = fields.Float(string='Balance by Report tn', digits=(16, 3), readonly=True)
    archive_balance_by_measurement_m3 = fields.Float(string='Archive Balance by Measurement m3', digits=(16, 3))
    archive_balance_by_measurement_tn = fields.Float(string='Archive Balance by Measurement tn', digits=(16, 3))
    balance_by_measurement_m3 = fields.Float(string='Balance by Measurement m3', digits=(16, 3), readonly=True)
    balance_by_measurement_tn = fields.Float(string='Balance by Measurement tn', digits=(16, 3), readonly=True,
                                             invisible=True)
    last_updated_date_by_measurement = fields.Date(string='Last Updated Date')
    archive_last_updated_date_by_measurement = fields.Date(string='Archive Last Updated Date')
    created_date = fields.Datetime(string='Created Date', readonly=True, default=fields.Date.context_today)
    state = fields.Selection([('opened', 'Opened'), ('closed', 'Closed')],
                             string='State', track_visibility='onchange', default='opened')
    product_uom_id = fields.Many2one(related='product_id.uom_id', string='Material Uom')
    from_production_line_ids = fields.One2many('mining.production.entry.line', 'from_pile_id', string='From Pile',
                                           readonly=True,
                                           order='date asc')
    for_production_line_ids = fields.One2many('mining.production.entry.line', 'for_pile_id', string='For Pile', readonly=True,
                                          order='date asc')
    comment = fields.Text(string='Comment')

    _sql_constraints = [
        # ('pile_balance','CHECK(balance_by_report_m3 >= 0)','Error ! Pile Balance Not Enough'),
        ('pile_name_uniq', 'UNIQUE(location_name,project_id)', 'Pile name must be unique!')
    ]

    # Зөвхөн Ноорог төлөвтэй бүртгэлийг устгах
    @api.multi
    def unlink(self):
        for obj in self:
            if obj.state == 'opened':
                return super(mining_pile, self).unlink()
            else:
                raise exceptions.except_orm(_('Invalid Action!'),
                                            _('In order to delete a mining pile, you must Opened it first.'))

    # Батлах
    @api.multi
    def action_to_closed(self):
        self.write({'state': 'closed'})
        return True

    # Цуцлах
    @api.multi
    def action_to_opened(self):
        self.write({'state': 'opened'})
        return True
