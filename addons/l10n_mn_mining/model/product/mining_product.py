# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016-today Asterisk-Technologies LLC (<http://www.asterisk-tech.mn>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo import exceptions
from lxml import etree


# Уулын бараа материалын бүртгэл
class product_template(models.Model):
    _inherit = 'product.template'

    # Уулын модулийн цэснээс бараа үүсгэхэд автоматаар сонгогдох
    @api.multi
    def _mining_product_default(self):
        if 'custom_view' in self.env.context:
            if self.env.context['custom_view'] == 'mining':
                return True
        return False

    # Уулын модулийн цэснээс бараа үүсгэхэд автоматаар сонгогдох
    @api.multi
    def _mining_type_default(self):
        if 'custom_view' in self.env.context:
            if self.env.context['custom_view'] == 'mining':
                return 'product'
        return False

    # Уулын модулийн цэснээс бараа үүсгэхэд автоматаар сонгогдох
    @api.multi
    def _mining_product_type(self):
        if 'custom_view' in self.env.context:
            if self.env.context['custom_view'] == 'mining':
                return 'soil'
        return False

    is_mining_product = fields.Boolean(string='Mining product', help="If checked, It's mining product.",
                                       default=_mining_product_default,readonly=True)
    mining_product_type = fields.Selection(
        [('soil', 'Soil'), ('mineral', 'Mineral'), ('mineral_reprocess', 'Mineral /Reprocess/'),
         ('engineering_work', 'Engineering Work')], default=_mining_product_type, string='Material Type', help='Mining product\'s type')
    is_productivity = fields.Boolean(string='Is productivity', help="If checked, It's productivity mining product.")
    bcm_coefficient = fields.Float(string='BCM Coefficient', digits=(16, 2), help='Bank Cubic Metre', default=0.0)
    average_content = fields.Float(string='Average Content', help='If that product is reprocess mineral, fill that field')

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(product_template, self).fields_view_get(
            view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])
        for node in doc.xpath("//form/sheet/notebook/page/group/group/field[@name='categ_id']"):
            if 'custom_view' in self.env.context:
                node.set('domain',
                         "[('is_mining_category', '=', True)]")
            else:
                node.set('domain',
                         "[('is_mining_category', '=', False)]")
        res['arch'] = etree.tostring(doc)
        return res


# Уулын бараа материалын ангиалалын бүртгэл
class product_category(models.Model):
    _inherit = 'product.category'

    # Уулын модулийн цэснээс барааны ангилал үүсгэхэд автоматаар сонгогдох
    @api.multi
    def _mining_category_default(self):
        if 'custom_view' in self.env.context:
            if self.env.context['custom_view'] == 'mining':
                return True
        return False

    # Нэр хадгалах
    @api.multi
    def _name_get_fnc(self):
        for cat in self:
            parent = ''
            if cat.parent_id:
                parent = cat.parent_id.name
            cat.complete_name = parent + "/" + cat.name

    is_mining_category = fields.Boolean(string='Mining product Category', help="If checked, It's mining product Category.",
                                        default=_mining_category_default, readonly=True)
    complete_name = fields.Char(compute='_name_get_fnc', string='Name')
