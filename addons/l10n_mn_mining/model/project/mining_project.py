# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo import exceptions


# Уулын төсөл буюу уурхайн бүртгэл
class project(models.Model):
    _inherit = "project.project"

    # Уулын модулиас төсөл үүсгэхэд уурхай төрөлтэй үүсэх
    def _project_type(self):
        if 'custom_view' in self.env.context:
            if self.env.context['custom_view'] == 'mining':
                return True
        return False

    # Даалгавруудыг хэрэглэхийг автоматаар сонгогддоггүй болгов
    @api.model
    def default_get(self, flds):
        result = super(project, self).default_get(flds)
        result['use_tasks'] = False
        return result

    project_type = fields.Boolean(string='Mining Project', default=_project_type)
    project_product_category_ids = fields.Many2many('product.category', 'project_product_category_rel', 'project_id',
                                                'category_id', string='Mining Categories')
    project_sequence = fields.Integer(string='Project Sequence')
