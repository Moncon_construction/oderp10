# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models

class Company(models.Model):
    _inherit = 'res.company'

    automate_check_motohour_error = fields.Boolean(
        'Automote Check Motohour Error',
        help="This check is show error message when technic's motohours are more than the technic's performance's motohours's 30 percent")
    right_motohour = fields.Boolean(string='Register Right Motohour')