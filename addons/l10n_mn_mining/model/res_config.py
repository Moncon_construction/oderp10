# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class MiningConfigSettings(models.TransientModel):
    _name = 'mining.config.settings'
    _inherit = 'res.config.settings'
    
    company_id = fields.Many2one('res.company', string='Company', required=True,
        default=lambda self: self.env.user.company_id)
    automate_check_motohour_error = fields.Boolean(related='company_id.automate_check_motohour_error',
        string='Automote Check Motohour Error',
        help="This check is show error message when technic's motohours are more than the technic's performance's motohours's 30 percent")
    right_motohour = fields.Boolean(related='company_id.right_motohour',
        string='Register Right Motohour')
