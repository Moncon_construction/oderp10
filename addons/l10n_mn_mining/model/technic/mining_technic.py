# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2014-today MNO LLC (<http://www.mno.mn>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo import exceptions


# Техник тохиргоо техникийн тэвшний багтаамжийг зааж өгөх
class mining_technic_configure(models.Model):
    _name = 'mining.technic.configure'
    _description = 'Mining Technic Configure'
    _inherit = ['mail.thread']

    # Тэвшний багтаамж тонн-оор бодох
    @api.onchange('product_id','body_capacity_m3')
    def _set_capacity_tn(self):
        for obj in self:
            if obj.project_id.project_type:
                obj.body_capacity_tn = obj.body_capacity_m3 * obj.product_id.bcm_coefficient
            else:
                obj.body_capacity_tn = 0.0

    # Төсөл авах
    @api.multi
    def _get_project_ids(self):
        proj_ids = self.env['project.project'].search([('favorite_user_ids', 'in', self.env.uid),('project_type','=','True')])
        if not proj_ids:
            raise exceptions.except_orm(_('No mining projects have been registered!'),
                                        _('Please adjust the mining project and the product category on the project!!!'))
        return proj_ids[0]

    name = fields.Char(string='Name', required=True, help='Name of mining technical configuration. Example: Dump excavator')
    technic_norm_id = fields.Many2one('technic.norm', string='Technic Type', required=True, help='')
    carrying_capacity = fields.Float(related='technic_norm_id.basket_capacity', string='Carrying capacity', readonly=True)
    scoop_capacity = fields.Float(related='technic_norm_id.scoop_capacity', string='Scoop capacity', readonly=True)
    product_id = fields.Many2one('product.product', string='Product', help='First select the project', required=True)
    project_id = fields.Many2one('project.project', string='Project', required=True, help='The Project',
                                 default=_get_project_ids)
    body_capacity_m3 = fields.Float(string='Body Capacity m3', required=True)
    body_capacity_tn = fields.Float(compute='_set_capacity_tn', string='Body Capacity tn', store=True)
    usage_uom_id = fields.Many2one('usage.uom', string='Usage unit of measure', ondelete = "restrict")
    second_usage_uom_id = fields.Many2one('usage.uom', string='Second Usage unit of measure', ondelete="restrict")
    creature_uom_id = fields.Many2one('production.uom', string='Creature unit of measure', ondelete="restrict")

    _sql_constraints = [
        ('name_uniq', 'unique(technic_norm_id, project_id, product_id)', 'Reference must be unique per Technic Configure, Material, Project!'),
    ]

    @api.model
    def create(self, vals):
        if 'product_id' in vals and 'body_capacity_m3' in vals:
            product = self.env['product.product'].browse(vals.get('product_id'))
            capacity = vals.get('body_capacity_m3') * product.bcm_coefficient
            vals.update({'body_capacity_tn': capacity})
            return super(mining_technic_configure, self).create(vals)

    @api.multi
    def write(self, vals):
        if 'product_id' in vals and 'body_capacity_m3' not in vals:
            product = self.env['product.product'].browse(vals.get('product_id'))
            capacity = self.body_capacity_m3 * product.bcm_coefficient
            vals.update({'body_capacity_tn': capacity})
        elif 'body_capacity_m3' in vals and 'product_id' not in vals:
            capacity = vals.get('body_capacity_m3') * self.product_id.bcm_coefficient
            vals.update({'body_capacity_tn': capacity})
        elif 'body_capacity_m3' in vals and 'product_id' in vals:
            product = self.env['product.product'].browse(vals.get('product_id'))
            capacity = vals.get('body_capacity_m3') * product.bcm_coefficient
            vals.update({'body_capacity_tn': capacity})
        return super(mining_technic_configure, self).write(vals)
