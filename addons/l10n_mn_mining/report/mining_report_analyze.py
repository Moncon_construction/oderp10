# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC
#    web: //www.asterisk-tech.mn
#
##############################################################################

from odoo import api, fields, models, _
from odoo import tools
from datetime import datetime, timedelta


class report_mining_plan_actual(models.Model):
    _name = 'report.mining.plan.actual'
    _description = 'Mining Plan Actual'
    _auto = False

    date = fields.Date(string='Date', readonly=True)
    project_id = fields.Many2one('project.project', string='Project', readonly=True)
    product_id = fields.Many2one('product.product', string='Material', readonly=True)
    actual_m3 = fields.Float(string='Actual m3', readonly=True)
    measurement_m3 = fields.Float(string='Measurement m3', readonly=True)
    plan_m3 = fields.Float('Plan m3', readonly=True)
    difference_tn = fields.Float(string='Waiting performance tn', readonly=True)
    difference_m3 = fields.Float(string='Waiting performance m3', readonly=True)
    actual_tn = fields.Float(string='Actual tn', readonly=True)
    plan_tn = fields.Float(string='Plan tn', readonly=True)
    categ_name = fields.Char(string='Material Category', readonly=True)

    _order = 'date desc'

    def init(self):
        tools.sql.drop_view_if_exists(self.env.cr, 'report_mining_plan_actual')
        self.env.cr.execute("""
            CREATE or REPLACE view report_mining_plan_actual as
            SELECT
                COALESCE(plan.id, meas.id*-1, disp.id*-777) as id,
                COALESCE(plan.project_id, meas.project_id, disp.project_id) as project_id,
                COALESCE(plan.product_id, meas.product_id, disp.product_id) as product_id,
                COALESCE(plan.date, meas.date, disp.date) as date,
                plan.plan_m3,
                plan.plan_tn,
                meas.meas_m3 as measurement_m3,
                CASE
                WHEN plan.plan_m3 is null then 0-disp.disp_m3
                WHEN disp.disp_m3 is null then  plan.plan_m3-0
                     ELSE plan.plan_m3 - disp.disp_m3
                END as difference_m3,

                CASE
                WHEN plan.plan_tn is null then 0-disp.disp_tn
                WHEN disp.disp_tn is null then  plan.plan_tn-0
                     ELSE plan.plan_tn - disp.disp_tn
                END as difference_tn,
                disp.disp_m3 as actual_m3,
                disp.disp_tn as actual_tn,
                (SELECT name FROM product_category WHERE id = (SELECT categ_id FROM product_template
                WHERE id = (select product_tmpl_id FROM product_product
                WHERE id=COALESCE(plan.product_id, meas.product_id, disp.product_id)))) as categ_name
                FROM
                (SELECT
                MIN(mpl.id) as id,
                mpl.product_id,
                mp.project_id,
                mp.date,
                SUM(mpl.forecast_m3) as plan_m3,
                SUM(mpl.forecast_tn) as plan_tn
                FROM
                mining_plan_line mpl
                LEFT JOIN mining_plan mp on (mp.id = mpl.plan_id)
                GROUP BY
                mpl.product_id,
                mp.project_id,
                mp.date) as plan
                FULL JOIN
                (SELECT
                MIN(sml.id) as id,
        sml.product_id,
           sm.date,
           sm.project_id,
           SUM(sml.amount_by_measurement) as meas_m3

           FROM mining_surveyor_measurement_line sml
           LEFT JOIN mining_surveyor_measurement sm on (sm.id = sml.mining_surveyor_measurement_id)


           GROUP BY
           sml.product_id,
           sm.date,
           sm.project_id
           
           ) as meas
    on (plan.project_id = meas.project_id and plan.date=meas.date and plan.product_id=meas.product_id)
    FULL JOIN
    (SELECT
        MIN(pl.id) as id,
        pl.product_id,
           md.date,
           md.project_id,
           SUM(pl.sum_m3) as disp_m3,
           SUM(pl.sum_tn) as disp_tn
           FROM mining_production_entry_line pl
           LEFT JOIN mining_daily_entry md on (md.id = pl.production_id)
       where 
       pl.is_production = True
           GROUP BY
           pl.product_id,
           md.date,
           md.project_id
           ) as disp
        on (plan.project_id = disp.project_id and plan.date=disp.date and plan.product_id=disp.product_id)
        """)
