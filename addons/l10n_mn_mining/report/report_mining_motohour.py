# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC
#    web: //www.asterisk-tech.mn
#
##############################################################################

from odoo import api, fields, models, _
from odoo import tools
from datetime import datetime, timedelta


class report_mining_motohour(models.Model):
    _name = 'report.mining.motohour'
    _description = 'Motohour entry report'
    _auto = False

    date = fields.Date(string='Date', readonly=True, help="Date on which this document has been created")
    project_id = fields.Many2one('project.project', string='Project', readonly=True)
    cause1 = fields.Char(string='Cause Type', readonly=True)  # many2one 'asset.technic.passpord',
    cause2 = fields.Char(string='Delay Cause Type', readonly=True)  # many2one 'asset.technic.passpord',
    technic_name = fields.Char(string='Technic', readonly=True)  # many2one 'asset.technic.passpord',
    shift = fields.Selection([('day', 'Day'), ('night', 'Night')], string='Shift', readonly=True)
    cause_id = fields.Many2one('mining.motohours.cause', string='Cause', readonly=True)
    diff_time = fields.Float(string='Cause Time', readonly=True)
    technic_type = fields.Char(string='Technic Type', readonly=True)
    location_id = fields.Many2one('mining.location', string='Location', readonly=True)
    percentage = fields.Integer(string='Percentage', readonly=True, group_operator="avg")
    cause_name = fields.Char(string='Cause Name', readonly=True)
    employee_id = fields.Many2one('hr.employee', string='Operator')
    employee_name = fields.Char(string='Operator Name')

    _order = 'date desc'

    def init(self):
        tools.sql.drop_view_if_exists(self.env.cr, 'report_mining_motohour')
        self.env.cr.execute("""
                CREATE or REPLACE view report_mining_motohour as
                SELECT
                        min(mcl.id) as id,
                        mcl.cause_id  as cause_id,
                        ml.technic_id,
                        ml.technic_name,
                        concat_ws(' ', e.last_name, r.name ) as employee_name,
                        e.id as employee_id,
                        min(mmc.cause_name) as cause_name,
                        mcl.location_id,
                        sum(mcl.diff_time) as diff_time,
                        min(mcl.percentage) as percentage,
                        md.date,
                        md.shift,
                        md.project_id,
                        ct1.type_name as cause1 ,
                        ct2.type_name as cause2,
                        tt.name as technic_type
                FROM mining_motohour_entry_cause_line mcl
                join mining_motohour_entry_line ml on (mcl.motohour_cause_id = ml.id)
                join mining_daily_entry md on (md.id = ml.motohour_id)
                        left join technic atp on (atp.id = ml.technic_id)
                        left join technic_type tt on (atp.technic_type_id = tt.id)
                        LEFT JOIN mining_motohours_cause mmc ON (mmc.id = mcl.cause_id)
                        LEFT JOIN mining_motohours_cause_type ct1 on (mmc.cause_type = ct1.id)
                        LEFT JOIN mining_motohours_cause_delay_type ct2 on (mmc.delay_cause_type = ct2.id)
                        LEFT JOIN mining_motohour_entry_operator_line ol on ol.motohour_cause_id = ml.id
                        LEFT JOIN hr_employee e on ol.operator_id  = e.id
                        LEFT JOIN resource_resource r on r.id = e.resource_id
                        group by
                        mcl.cause_id,
                        ml.technic_id,
                        ml.technic_name,
                        r.name,
                        e.last_name,
                        e.id,
                        md.date,
                        md.shift,
                        md.project_id,
                        ct1.type_name,
                        ct2.type_name,
                        tt.name,
                        mcl.location_id
            """)