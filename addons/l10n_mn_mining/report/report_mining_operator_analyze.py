# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC
#    web: //www.asterisk-tech.mn
#
##############################################################################

from odoo import api, fields, models, _
from odoo import tools
from datetime import datetime, timedelta


class report_mining_operator_analyze(models.Model):
    _name = 'report.mining.operator.analyze'
    _description = 'Report Mining Operator'
    _auto = False

    date = fields.Date(string='Date', readonly=True)
    project_id = fields.Many2one('project.project', string='Project', readonly=True)
    technic_id = fields.Many2one('technic', string='Technic', readonly=True)  # many2one 'asset.technic.passpord',
    shift = fields.Selection([('day', 'Day'), ('night', 'Night')], string='Shift', readonly=True)
    operator_id = fields.Many2one('hr.employee', string='Operator', readonly=True)
    last_odometer_value = fields.Float(string='Last Odometer', readonly=True, group_operator='avg')
    first_odometer_value = fields.Float(string='First Odometer', readonly=True, group_operator='avg')
    o_motohour_time = fields.Float(string='Motohour Time', readonly=True)

    _order = 'date desc'

    def init(self):
        tools.sql.drop_view_if_exists(self.env.cr, 'report_mining_operator_analyze')
        self.env.cr.execute("""
            CREATE or REPLACE view report_mining_operator_analyze as
            select
                MIN(mol.id) as id,
                md.project_id,
                md.date,
                md.shift,
                mel.technic_id,
                mol.operator_id,
            MIN(mol.first_odometer_value) as first_odometer_value,
                MIN(mol.last_odometer_value) as last_odometer_value,
                MIN(mol.o_motohour_time) as o_motohour_time
            FROM mining_motohour_entry_operator_line mol
                left join mining_motohour_entry_line mel on (mol.motohour_cause_id = mel.id)
                left join mining_daily_entry md on (mel.motohour_id = md.id)
            where
                mol.operator_id is not null
            group by
                md.project_id,
                mol.operator_id,
                md.date,
                md.shift,
                mel.technic_id
        """)