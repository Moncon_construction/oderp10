# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC
#    web: //www.asterisk-tech.mn
#
##############################################################################

from odoo import api, fields, models, _
from odoo import tools
from datetime import datetime, timedelta


class report_mining_production(models.Model):
    _name = 'report.mining.production'
    _description = 'Production entry report'
    _auto = False

    date = fields.Date(string='Date', readonly=True)
    project_id = fields.Many2one('project.project', string='Project', readonly=True)
    shift = fields.Selection([('day', 'Day'), ('night', 'Night')], string='Shift', readonly=True)
    dump_id = fields.Many2one('technic', string='Dump',
                              domain=[('technic_norm_id.technic_type_id.is_auto')], required=True)
    excavator_id = fields.Many2one('technic', string='Mechanism',
                                   domain=[('technic_norm_id.technic_type_id.is_mechanism')], required=True)
    sum_m3 = fields.Float(string='Sum m3', readonly=True)
    res_count = fields.Float(string='Sum Res', readonly=True)
    sum_tn = fields.Float(string='Sum tn', readonly=True)
    product_id = fields.Many2one('product.product', string='Material', help='The Product', readonly=True)
    from_pile_id = fields.Many2one('mining.pile', string='From pile')
    from_location = fields.Many2one('mining.location', string='From location')
    for_pile_id = fields.Many2one('mining.pile', string='For pile')
    for_location = fields.Many2one('mining.location', string='Location')
    level = fields.Char(string='Level', readonly=True)
    master_id = fields.Many2one('res.users', string='Master', readonly=True)
    is_production = fields.Boolean(string='Is Production')
    excavator_count = fields.Float(string='Excavator Count', readonly=True)
    dump_count = fields.Float(string='Dump Count', readonly=True)
    exca_production_time = fields.Float(string='Excavator Production Time', readonly=True)
    dump_production_time = fields.Float(string='Dump Production Time', readonly=True)
    employee_id = fields.Many2one('hr.employee', string='Operator')
    employee_name = fields.Char(string='Operator Name')

    _order = 'date desc'

    def init(self):
        tools.sql.drop_view_if_exists(self.env.cr, 'report_mining_production')
        self.env.cr.execute("""
            CREATE or REPLACE view report_mining_production as
            SELECT
                mpel.id AS id,
                mpel.dump_id,
                mpel.is_production,
                min(mmel.production_time)/min(dump.dump_too) as dump_production_time,
                min(dump.exca_too)/min(dump.dump_too) as excavator_count,
                min(mmel_exca.production_time)/min(exca.exca_too) as exca_production_time,
                min(exca.dump_too)/min(exca.exca_too) as dump_count,
                sum(DISTINCT mpel.sum_m3) AS sum_m3,
                sum(DISTINCT mpel.sum_tn) AS sum_tn,
                sum(DISTINCT mpel.res_count) AS res_count,
                mpel.excavator_id,
                mpel.product_id,
                mde.date,
                mde.shift,
                mde.project_id,
                mpel.from_location,
                mpel.for_pile_id,
                mpel.for_location,
                mpel.from_pile_id,
                mpel.level,
                mde.master_id,
                concat_ws(' ', e.last_name, r.name ) as employee_name,
                e.id as employee_id
            FROM mining_production_entry_line mpel
            LEFT JOIN mining_daily_entry mde ON (mde.id = mpel.production_id)
            LEFT JOIN
    (SELECT
        mde2.project_id,
        mde2.date,
        mpel2.dump_id,
        mde2.shift,
        COUNT(mpel2.dump_id) AS dump_too,
        COUNT(DISTINCT mpel2.excavator_id) AS exca_too
    FROM mining_production_entry_line mpel2
    LEFT JOIN mining_daily_entry mde2 ON (mde2.id = mpel2.production_id)

    GROUP BY
        mde2.project_id,
        mde2.date,
        mde2.shift,
        mpel2.dump_id
    ) AS dump ON (dump.project_id = mde.project_id and dump.date = mde.date and dump.shift = mde.shift and dump.dump_id = mpel.dump_id)
    LEFT JOIN mining_motohour_entry_line mmel ON (mmel.motohour_id = mde.id and dump.dump_id = mmel.technic_id)
    LEFT JOIN
    (SELECT
    mde3.project_id,
    mde3.date,
    mde3.shift,
    mpel3.excavator_id,
    COUNT(mpel3.excavator_id) AS exca_too,
    COUNT(DISTINCT mpel3.dump_id) AS dump_too
        FROM mining_production_entry_line mpel3
        LEFT JOIN mining_daily_entry mde3 ON (mde3.id = mpel3.production_id)

        GROUP BY
            mde3.project_id,
            mde3.date,
            mde3.shift,
            mpel3.excavator_id
        ) AS exca ON (exca.project_id = mde.project_id and exca.date = mde.date and exca.shift = mde.shift and  exca.excavator_id = mpel.excavator_id)
        LEFT JOIN mining_motohour_entry_line mmel_exca ON (mmel_exca.project_id = exca.project_id and mmel_exca.date = exca.date and mmel_exca.motohour_id = mde.id and exca.excavator_id = mmel_exca.technic_id)
        LEFT JOIN mining_motohour_entry_operator_line ol on (ol.motohour_cause_id = mmel_exca.id or ol.motohour_cause_id = mmel.id)
                    LEFT JOIN hr_employee e on ol.operator_id  = e.id
                    LEFT JOIN resource_resource r on r.id = e.resource_id
        GROUP BY
        mpel.id,
            mpel.dump_id,
            mpel.excavator_id,
            mpel.product_id,
            mde.date,
            mde.shift,
            mde.project_id,
            mpel.from_location,
            mpel.for_pile_id,
            mpel.for_location,
            mpel.from_pile_id,
            mpel.level,
            r.name,
            e.last_name,
            e.id,
            mde.master_id
                """)
