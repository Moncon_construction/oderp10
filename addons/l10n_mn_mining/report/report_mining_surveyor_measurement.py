# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC
#    web: //www.asterisk-tech.mn
#
##############################################################################

from odoo import api, fields, models, _
from odoo import tools
from datetime import datetime, timedelta


class report_mining_surveyor_measurement_analyze(models.Model):
    _name = 'report.mining.surveyor.measurement.analyze'
    _description = 'Mining surveyor measurement analyze'
    _auto = False

    date = fields.Date(string='Date', readonly=True)
    project_id = fields.Many2one('project.project', string='Project', readonly=True)
    product_id = fields.Many2one('product.product', string='Material', readonly=True)
    amount_by_measurement = fields.Float(string='Amount by Measurement')
    type_measurement = fields.Selection(
        [('is_production', 'Is Production'), ('is_reclamation', 'Is Reclamation'), ('is_engineer', 'Is Engineer')],
        string='Type Measurement')
    location_id = fields.Many2one('mining.location', string='Location')
    categ_name = fields.Char(string='Material Category', readonly=True)

    _order = 'date desc'

    def init(self):
        tools.sql.drop_view_if_exists(self.env.cr, 'report_mining_surveyor_measurement_analyze')
        self.env.cr.execute("""
            CREATE or REPLACE view report_mining_surveyor_measurement_analyze as
            SELECT
                min(msml.id) as id,
                msm.date,
                msm.project_id,
                msml.product_id,
                (SELECT name FROM product_category WHERE id = (SELECT categ_id FROM product_template
                WHERE id = (SELECT product_tmpl_id FROM product_product
                WHERE id=msml.product_id))) as categ_name,
                msml.location_id,
                msml.type_measurement,
                sum(msml.amount_by_measurement) as amount_by_measurement
                FROM mining_surveyor_measurement msm
                left join mining_surveyor_measurement_line msml ON (msm.id=msml.mining_surveyor_measurement_id)
                GROUP BY
                msm.date,
                msm.project_id,
                msml.product_id,
                msml.location_id,
                msml.type_measurement
        """)