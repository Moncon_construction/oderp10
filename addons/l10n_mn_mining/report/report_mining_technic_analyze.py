# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC
#    web: //www.asterisk-tech.mn
#
##############################################################################

from odoo import api, fields, models, _
from odoo import tools
from datetime import datetime, timedelta


class report_mining_technic_analyze(models.Model):
    _name = 'report.mining.technic.analyze'
    _description = 'Technic Fuel Motohour Production'
    _auto = False

    date = fields.Date(string='Date', readonly=True)
    project_id = fields.Many2one('project.project', string='Project', readonly=True)
    technic_id = fields.Many2one('technic', string='Technic', readonly=True)  # many2one 'asset.technic.passpord',
    sum_motohour_time = fields.Float(string='Motohour Time', readonly=True)
    sum_work_time = fields.Float(string='Work Time', readonly=True)
    sum_repair_time = fields.Float(string='Repair Time', readonly=True)
    sum_production_time = fields.Float(string='Production Time', readonly=True)
    sum_break_time = fields.Float(string='Normative Break Time', readonly=True)
    calendar = fields.Float(string='Calendar Time', readonly=True)
    ready_time = fields.Float(string='Ready Time', readonly=True)
    first_odometer_value = fields.Float(string='First Odometer', readonly=True, group_operator="min")
    last_odometer_value = fields.Float(string='Last Odometer', readonly=True, group_operator="max")
    availibility = fields.Float(string='Availibility Percentage', readonly=True, group_operator="avg")
    availibility_hour = fields.Float(string='Availibility Hour', readonly=True)
    utilization = fields.Float(string='Utilization Percentage', readonly=True, group_operator="avg")
    utilization_hour = fields.Float(string='Utilization Hour', readonly=True)
    production = fields.Float(string='Production Percentage', readonly=True, group_operator="avg")
    last_km = fields.Float(string='Last Km', readonly=True, group_operator="max")
    run_day = fields.Integer(string='Run day', readonly=True)

    _order = 'date desc'

    def init(self):
        tools.sql.drop_view_if_exists(self.env.cr, 'report_mining_technic_analyze')
        self.env.cr.execute("""
            CREATE or REPLACE view report_mining_technic_analyze as
            SELECT
                    id,
                    technic_id,
                    CASE WHEN first_odometer_value=0 THEN NULL ELSE first_odometer_value END as first_odometer_value,
                    CASE WHEN last_odometer_value=0 THEN NULL ELSE last_odometer_value END as last_odometer_value,
                    CASE WHEN last_km=0 THEN NULL ELSE last_km END as last_km,
                    date,
                    project_id,
                    sum_motohour_time,
                    sum_work_time,
                    CASE WHEN sum_production_time>0  THEN 1 ELSE 0 END as run_day,
                    sum_repair_time,
                    sum_break_time,
                    CASE WHEN sum_break_time = 0.0 THEN 24 ELSE (24-sum_break_time) END as calendar,
                    CASE WHEN sum_repair_time = 0.0 THEN calendar ELSE (calendar-sum_repair_time) END as ready_time,
                    sum_production_time,
                    CASE WHEN ready_time = 0.0 and calendar= 0.0 THEN 0.0 ELSE (ready_time / calendar)*100 END as availibility,
                    CASE WHEN calendar-sum_repair_time = 0.0 or sum_production_time = 0.0 THEN 0.0 ELSE (sum_production_time/ready_time)*100 END as availibility_hour,
                    CASE WHEN calendar-sum_repair_time = 0.0 or sum_production_time = 0.0 THEN 0.0 ELSE (sum_production_time/calendar)*100 END as utilization,
                    CASE WHEN sum_motohour_time = 0.0 THEN NULL ELSE
                    (CASE WHEN sum_repair_time=24.0 THEN 0.0 ELSE sum_motohour_time END) END as utilization_hour,
                    CASE WHEN sum_motohour_time = 0.0 THEN 0.0 ELSE (100*sum_production_time)/sum_motohour_time END as production
                    FROM
                    (SELECT
                    min(ml.id) as id,
                    ml.technic_id,
                    min(ml.first_odometer_value) as first_odometer_value,
                    max(ml.last_km) as last_km,
                    max(ml.last_odometer_value) as last_odometer_value,
                    md.date,
                    md.project_id,
                    sum(ml.motohour_time) as sum_motohour_time,
                    sum(ml.work_time) as sum_work_time,
                    sum(ml.production_time) as sum_production_time,
                    sum(ml.repair_time) as sum_repair_time,
                    sum(ml.norm_break_time) as sum_break_time,
                    sum(12- ml.norm_break_time) as calendar,
                    sum(12-ml.norm_break_time - ml.repair_time) as ready_time

            FROM mining_motohour_entry_line ml
                    LEFT JOIN mining_daily_entry md ON (md.id = ml.motohour_id)

                    LEFT JOIN (
            SELECT
            mpel3.excavator_id,
            mde3.date,
            mde3.project_id,
            sum(mpel3.sum_m3) as sum_m3
            FROM mining_production_entry_line mpel3
            LEFT JOIN mining_daily_entry mde3 ON (mde3.id = mpel3.production_id)
            where mpel3.is_production=True
            GROUP BY
            mpel3.excavator_id,
            mde3.date,
            mde3.project_id
                    ) as prod_exca ON (prod_exca.excavator_id = ml.technic_id AND prod_exca.date = md.date AND prod_exca.project_id = md.project_id )
                    LEFT JOIN (
            SELECT
            mpel2.dump_id,
            mde2.date,
            mde2.project_id,
            sum(mpel2.sum_m3) as sum_m3
            FROM mining_production_entry_line mpel2
            LEFT JOIN mining_daily_entry mde2 on (mde2.id = mpel2.production_id)
            WHERE mpel2.is_production=True
            GROUP BY
            mpel2.dump_id,
            mde2.date,
            mde2.project_id
                    ) as prod_dump on (prod_dump.dump_id = ml.technic_id AND prod_dump.date = md.date AND prod_dump.project_id = md.project_id )
                    LEFT JOIN technic atp ON (atp.id = ml.technic_id)
                GROUP BY
                    ml.technic_id,
                    md.date,
                    md.project_id) AS value_table

        """)


