odoo.define('l10n_mn_mining.technic_motohour', function (require) {
	"use strict";

    var core = require('web.core');
    var FormView = require('web.FormView');
    var form_common = require('web.form_common');
    var data = require('web.data');
    var utils = require('web.utils');
    var formats = require('web.formats');
    var _t = core._t;
    var QWeb = core.qweb;
    var Model = require('web.DataModel');
    var ajax = require('web.ajax');
    var time = require('web.time');
    var KanbanView = require('web_kanban.KanbanView');

    var TechnicMotohour = form_common.FormWidget.extend(form_common.ReinitializeWidgetMixin, {
        events: {
            "click .oe_technic_row td": "go_to",
            "click .oe_technic_row.oe_technic_motohour_row div": "go_to2",
            "click .oe_technic_motohour_fullscreen img": "launchFullscreen",
            "click .oe_technic_motohour_refresh img": "launchRefresh",
            "click .oe_technic_motohour_timeline_head_cause div": "cause_go_to",
        },
        init: function() {
            this._super.apply(this, arguments);
            var self = this;
            this.set({
                sheets: [],
                shift: false,
                date: false,
                project_id: false,
                st_value: [],
            });
            this.updating = false;
            this.clicked_cause = [];
            this.defs = [];
            this.field_manager.on("field_changed:motohour_line", this, this.query_sheets);
            this.field_manager.on("field_changed:date", this, function() {
                this.set({"date": time.str_to_date(this.field_manager.get_field_value("date"))});
            });
            this.field_manager.on("field_changed:project_id", this, function() {
                this.set({"project_id": this.field_manager.get_field_value("project_id")});
            });
            this.field_manager.on("field_changed:shift", this, function() {
                this.set({"shift": this.field_manager.get_field_value("shift")});
            });
            this.field_manager.on("field_changed:shift", this, this.set_st_value);
            this.field_manager.on("field_changed:project_id", this, this.set_st_value);

            this.res_o2m_drop = new utils.DropMisordered();
            this.render_drop = new utils.DropMisordered();
            this.description_line = _t("/");
            // Original save function is overwritten in order to wait all running deferreds to be done before actually applying the save.
            this.view.original_save = _.bind(this.view.save, this.view);
            this.view.save = function(prepend_on_create){
                self.prepend_on_create = prepend_on_create;
                return $.when.apply($, self.defs).then(function(){
                    return self.view.original_save(self.prepend_on_create);
                });
            };
            document.addEventListener("fullscreenchange", function(e) {
                self.setHide(self.check_fullscreen());
            });
            document.addEventListener("mozfullscreenchange", function(e) {
                self.setHide(self.check_fullscreen());
            });
            document.addEventListener("webkitfullscreenchange", function(e) {
                self.setHide(self.check_fullscreen());
            });
            document.addEventListener("msfullscreenchange", function(e) {
                self.setHide(self.check_fullscreen());
            });
        },
        check_fullscreen:function() {
            if (!window.screenTop && !window.screenY) {
               return true;
            } else {
               return false;
            }
        },
        go_to: function(event) {
            var line_id = JSON.parse($(event.target).data("id"));
            var self = this;
            if (event.target.id) {
               this.do_action({
                type: 'ir.actions.act_window',
                res_model: "mining.motohour.entry.cause.line.wizard",
                view_mode: 'form',
                view_type: 'form',
                views: [[false, 'form']],
                context: {default_motohour_cause_id: line_id},
                target: 'new',
            });
            } else {
               this.do_action({
                type: 'ir.actions.act_window',
                res_model: "mining.motohour.entry.line",
                res_id: line_id,
                views: [[false, 'form']],
                target: 'current'
            });
            }
        },
        clone: function() {
            return new Date(this.getTime());
        },
        cause_go_to: function(event){
            var cause_id = JSON.parse($(event.target).data("cause_id"));
            var self = this;
            _.each(self.mining_mh_causes, function(details) {
                if (details.id == cause_id){
                    if (self.clicked_cause.indexOf(details.name) == -1){
                        this.$('[data-cause-name="' + details.name + '"]').css({
                            'border-top':'0px solid ',
                            'border-bottom':'7px solid '+details.color,
                        });
                        self.clicked_cause.push(details.name);
                    }else{
                        this.$('[data-cause-name="' + details.name + '"]').css({
                            'border-top':'4px solid '+details.color,
                            'border-bottom':'0px solid ',
                        });
                        self.clicked_cause.pop(details.name);
                    }
                }
            }, this);
            _.each(self.motohour_lines, function(motohour) {
                        _.each(motohour.causes, function(item){
                            if (self.clicked_cause.length==0){
                                this.$('[data-start-time="' + item.line_id+'"]').show();
                            }
                            else{
                                if (self.clicked_cause.indexOf(item.cause) != -1){
                                    this.$('[data-start-time="' + item.line_id+'"]').show();
                                }
                                else{
                                    this.$('[data-start-time="' + item.line_id+'"]').hide();
                                }
                            }
                        },this);
            }, this);

        },
        query_sheets: function() {
            var self = this;
            if (self.updating)
                return;
            var commands = this.field_manager.get_field_value("motohour_line");
            this.res_o2m_drop.add(new Model(this.view.model).call("resolve_2many_commands", ["motohour_line", commands, [],
                    new data.CompoundContext()]))
                .done(function(result) {
                self.querying = true;
                self.set({sheets: result});
                self.querying = false;
            });
        },
        initialize_field: function() {
            form_common.ReinitializeWidgetMixin.initialize_field.call(this);
            var self = this;
            self.on("change:sheets", self, self.initialize_content);
            self.on("change:date", self, self.initialize_content);
            self.on("change:shift", self, self.initialize_content);
            self.on("change:project_id", self, self.initialize_content);
            self.on("change:st_value", self, self.initialize_content);
        },
        launchRefresh: function(){
            var self = this;
            self.clicked_cause = [];
            self.query_sheets();
            self.set_st_value();
        },
        initialize_content: function() {
            var self = this;
            this.destroy_content();
            var times;
            var motohour_lines;
            var default_get;
            var mining_mh_causes;
            var st;
            var et;
            var start_time;
            var end_time;

            times = [];
            var current_date = self.get('date');
            var st_time = self.get("date");
            var end_time = self.get("date");
            if (self.get('st_value')) {
                 if (self.get('st_value')['start_time'] < self.get('st_value')['end_time']) {
                 st = st_time.setHours(self.get('st_value')['start_time']);
                    et = end_time.setHours(self.get('st_value')['end_time']);
                    st_time = new Date(st);
                    start_time = st_time;
                    end_time = new  Date(et);
                    while (st_time <= end_time){
                        times.push(moment(st_time).format('HH:mm'));
                        st_time = moment(st_time).add(1, 'hours');
                    }
                 }
                 if (self.get('st_value')['start_time'] > self.get('st_value')['end_time']) {
                 st = st_time.setHours(self.get('st_value')['start_time']);
                 et = end_time.setHours(end_time.getHours()+(self.get('st_value')['end_time']+24-self.get('st_value')['start_time']));
                 st_time = new Date(st);
                 start_time = st_time;
                 end_time = new  Date(et);
                 while (st_time <= end_time){
                    times.push(moment(st_time).format('HH:mm'));
                    st_time = moment(st_time).add(1, 'hours');
                 }
                 }
            }
            motohour_lines = _(self.get("sheets")).chain()
            .map(function(el) {
                if (typeof(el.id) === "object")
                    el.id = el.id[0];
                return el;
            })
            .groupBy("id").value();
            var cause_line_ids = [];
            var motohour_ids = _.map(_.keys(motohour_lines), function(el) { return el === "false" ? false : Number(el) });
            _(motohour_lines).chain().map(function(lines, motohour_id) {
                        cause_line_ids = cause_line_ids.concat(lines[0].motohour_cause_line);
                        return {};
                    }).value();
            return new Model("mining.motohour.entry.cause.line").call("read", [cause_line_ids,[], new data.CompoundContext()])
            .then(function(cause_details) {
                motohour_lines = _(motohour_lines).chain().map(function(lines, motohour_id) {
                    // result = _.extend({}, default_get, (result[motohour_id] || {}).value || {});
                    // motohour_id = motohour_id === "false" ? false :  Number(motohour_id);
                    var technic_id = lines[0].technic_id[0];
                    var last_odometer_value = lines[0].last_odometer_value;
                    var first_odometer_value  = lines[0].first_odometer_value;
                    var operator_id = lines[0].operator_names;
                    var work_diff_time = lines[0].work_diff_time;
                    var diff_odometer_value = lines[0].diff_odometer_value
                    var norm_break_time = lines[0].norm_break_time
                    var production_time = lines[0].production_time;
                    var repair_time = lines[0].repair_time;
                    var work_time = lines[0].work_time;
                    var motohour_time = lines[0].motohour_time;
                    var technic_names = lines[0].technic_id[1];
                    var cause_ids = lines[0].motohour_cause_line;
                    var causes = [];
                    var Causes = new Model('mining.motohours.cause');
                    _.each(cause_ids, function(value){
                        _.each(cause_details, function(res){
                            if(res.id==value){
                                Causes.query(['name'])
                                     .filter([['id', '=', res.cause_id]])
                                     .limit(1)
                                     .first().then(function (get_causes) {

                                        var temp = {
                                        'start_time':res.start_time
                                        ,'r_start_time':res.r_start_time
                                        ,'description':res.description
                                        ,'diff_time': res.diff_time
                                        , 'cause':get_causes['name']
                                        , 'color': res.color
                                        , 'line_id':res.id
                                        , 'cause_name':res.cause_name}
                                        causes.push(temp);
                                });
                            }
                        });
                    });

                    return {motohour: motohour_id,
                    technic: technic_id, last_odometer_value: last_odometer_value, first_odometer_value: first_odometer_value,
                    operator_id: operator_id, work_diff_time: work_diff_time, diff_odometer_value:diff_odometer_value,
                    work_time: work_time, production_time: production_time, norm_break_time:norm_break_time,
                    repair_time:repair_time, motohour_time: motohour_time,technic_names:technic_names,
                    cause_ids: cause_ids,causes: causes};
                }).value();
                return new Model("mining.motohours.cause").call("search", [[["id", "!=", 0]]])
                .then(function(ids) {
                    return new Model("mining.motohours.cause").call("read", [ids,[],new data.CompoundContext()]).then(function(cause_details){
                        mining_mh_causes = cause_details;
                    });

                });
            }).then(function(result) {
                self.times = times;
                self.motohour_lines = motohour_lines;
                self.mining_mh_causes = mining_mh_causes;
                self.display_data();
            });
        },
        destroy_content: function() {
            if (this.dfm) {
                this.dfm.destroy();
                this.dfm = undefined;
            }
        },
        display_data: function() {
            var self = this;
            self.$el.html(QWeb.render("l10n_mn_mining.TechnicMotohour", {widget: self}));
            var today_now = new Date();
            var real_how_start = false;
            var time_value = parseInt(today_now.toString('HH'));
            if (self.get('date').toString('YY-MM-dd') == today_now.toString('YY-MM-dd')){
                    real_how_start = false;
            }
            _.each(self.mining_mh_causes, function(details) {
                this.$('[data-cause-name="' + details.name + '"]').css
                                    ({  'border-top':'4px solid '+details.color,
                                        'background-color': self.hex2rgb(self.nameToHex(details.color)),
                                    });
                this.$('[data-cause-name="' + details.name + '"]').attr('title'
                                    ,details.cause_name);
            }, this);
            if(self.timer==null && real_how_start==true){
                var d = new Date();
                var left_time = parseInt(d.toString('HH'));
                left_time = left_time*60+parseInt(d.toString('mm'));
                 _.each(self.motohour_lines, function(motohour) {
                            if (motohour.causes.length>0){
                                var item = motohour.causes[motohour.causes.length-1];
                                    var st_time = item.r_start_time;
                                    var hours = parseInt(st_time);
                                    var minutes = parseInt((st_time - hours)*60);
                                    if (((st_time - hours)*60 - minutes)>0.5) minutes++;
                                    var time_start = hours*60+minutes;
                                    var diff_time = left_time - time_start;
                                    this.$('[data-start-time="' + item.line_id+'"]').html(item.cause+'<br> <span>('+self.format_client(item.start_time)+')</span>');
                                    this.$('[data-start-time="' + item.line_id + '"]').css
                                    ({'width':diff_time,
                                        'border-top':'4px solid '+item.color,
                                        'background-color': self.hex2rgb(self.nameToHex(item.color)),
                                        'left':time_start+'px',
                                        'height': '28px'
                                    });
                                    hours = parseInt(d.toString('HH'));
                                    minutes = parseInt(d.toString('mm'));
                                    st_time = hours + minutes/60;
                                    var desc = item.description;
                                    if (desc == 'undefined / false') desc = '';
                                    this.$('[data-start-time="' + item.line_id+'"]').attr('title'
                                    ,item.cause_name+' ('+self.format_client(item.start_time)+'-'+self.format_client(st_time)+')'
                                    +'\n'+' WORK TIME '+self.format_client(st_time-item.start_time)+'\n'+desc);
                            }
                    },this);
                this.$('[class="'+'oe_technic_motohour_cursor'+'"]').css
                                ({'left':left_time-20});
                lin_height = this.$('[class="'+'oe_technic_motohour'+'"]').height() - 63;
                this.$('[class="'+'oe_technic_motohour_cursor_line'+'"]').css
                                ({'left':(left_time),
                                    'height':lin_height });
                this.$('[class="'+'oe_technic_motohour_cursor_indicator'+'"]').html(d.toString('HH:mm'));

                var timer = setInterval(function(){
                        var d = new Date();
                        var left_time = parseInt(d.toString('HH'));
                        left_time = left_time*60+parseInt(d.toString('mm'));
                        if (left_time>719 || real_how_start==false){
                            clearInterval(timer);
                            real_how_start =false;
                        }
                        _.each(self.motohour_lines, function(motohour) {
                            if (motohour.causes.length>0 && real_how_start==true){
                                var item = motohour.causes[motohour.causes.length-1];
                                    var st_time = item.r_start_time;
                                    var hours = parseInt(st_time);
                                    var minutes = parseInt((st_time - hours)*60);
                                    if (((st_time - hours)*60 - minutes)>0.5) minutes++;
                                    var time_start = hours*60+minutes;
                                    var diff_time = left_time - time_start;
                                    this.$('[data-start-time="' + item.line_id+'"]').html(item.cause+'<br> <span>('+self.format_client(item.start_time)+')</span>');
                                    this.$('[data-start-time="' + item.line_id + '"]').css
                                    ({'width':diff_time,
                                        'border-top':'4px solid '+item.color,
                                        'background-color': self.hex2rgb(self.nameToHex(item.color)),
                                        'left':time_start+'px',
                                        'height': '28px'
                                    });
                                    hours = parseInt(d.toString('HH'));
                                    minutes = parseInt(d.toString('mm'));
                                    st_time = hours + minutes/60;
                                    var desc = item.description;
                                    if (desc == 'undefined / false') desc = '';
                                    this.$('[data-start-time="' + item.line_id+'"]').attr('title'
                                    ,item.cause_name+' ('+self.format_client(item.start_time)+'-'+self.format_client(st_time)+')'
                                    +'\n'+' WORK TIME '+self.format_client(st_time-item.start_time)+'\n'+desc);
                            }
                    }, this);
                        this.$('[class="'+'oe_technic_motohour_cursor'+'"]').animate
                                ({'left':left_time-20.5,
                                });
                        lin_height = this.$('[class="'+'oe_technic_motohour'+'"]').height() - 63;
                        this.$('[class="'+'oe_technic_motohour_cursor_line'+'"]').animate
                                ({'left':(left_time),
                                    'height':lin_height
                                });
                        this.$('[class="'+'oe_technic_motohour_cursor_indicator'+'"]').html(d.toString('HH:mm'));
                    }, 300000);

            }else{
                clearInterval(timer);
                _.each(self.motohour_lines, function(motohour) {
                    if (motohour.causes.length>0){
                        var item = motohour.causes[motohour.causes.length-1];
                        if (item.diff_time==0){
                            this.$('[data-start-time="' + item.line_id + '"]').hide();
                        }
                    }
                }, this);
                real_how_start = false;
                this.$('[class="'+'oe_technic_motohour_cursor'+'"]').hide();
                this.$('[class="'+'oe_technic_motohour_cursor_line'+'"]').hide();
            }
            _.each(self.motohour_lines, function(motohour) {
                    var i = 0;
                    _.each(motohour.causes, function(item){
                        var time_start = item.r_start_time;
                        var how_start = true;
                        i++;
                        if (i == motohour.causes.length && real_how_start == true){
                            how_start = false;
                        }
                        if (how_start==true){
                            var st_time = (self.get('shift')=='night') ? item.r_start_time - 24 : item.r_start_time;
                            var hours = parseInt(st_time);
                            var minutes = parseInt((st_time - hours)*60);
                            if (((st_time - hours)*60 - minutes)>0.5) minutes++;
                            if (self.get('shift')=='day'){hours -= self.get('st_value')['start_time'];}
                            if (self.get('shift')=='night'){
                                if (st_time < (self.get('st_value')['start_time']))
                                {
                                    hours -= (self.get('st_value')['start_time']-24);
                                }
                                else {
                                    hours -= self.get('st_value')['start_time'];
                                }
                                }
                            time_start = hours*60+minutes;
                            st_time = item.diff_time;
                            hours = parseInt(st_time);
                            minutes = parseInt((st_time - hours)*60);
                            if (((st_time - hours)*60 - minutes)>0.5) minutes++;
                            var diff_time = hours*60 + minutes
                            this.$('[data-start-time="' + item.line_id+'"]').html(item.cause+'<br> <span>('+self.format_client(item.start_time)+')</span>');
                            this.$('[data-start-time="' + item.line_id + '"]').css
                                ({'width':diff_time,
                                    'border-top':'4px solid '+item.color,
                                    'background-color': self.hex2rgb(self.nameToHex(item.color)),
                                    'left':time_start+'px',
                                    'height': '28px'
                                });
                            var r_diff_time = item.start_time+item.diff_time;
                            if (r_diff_time>23){r_diff_time -= 24; }
                            var desc = item.description;
                            if (desc == 'undefined / false') desc = '';
                            this.$('[data-start-time="' + item.line_id+'"]').attr('title'
                                ,item.cause_name+' ('+self.format_client(item.start_time)+'-'+self.format_client(r_diff_time)+')'
                                +'\n'+' WORK TIME '+self.format_client(item.diff_time)+'\n'+desc);
                        }
                    }, this);
                    this.$('[data-first_odometer_value="'+motohour.motohour+'"]').html(motohour.first_odometer_value.toFixed(1));
                    this.$('[data-last_odometer_value="'+motohour.motohour+'"]').html(motohour.last_odometer_value.toFixed(1));
                    this.$('[data-work_diff_time="'+motohour.motohour+'"]').html(self.format_client(motohour.work_diff_time));
                    this.$('[data-diff_odometer_value="'+motohour.motohour+'"]').html(motohour.diff_odometer_value.toFixed(1));
                    this.$('[data-norm_break_time="'+motohour.motohour+'"]').html(self.format_client(motohour.norm_break_time));
                    this.$('[data-motohour_time="'+motohour.motohour+'"]').html(motohour.motohour_time.toFixed(1));
                    this.$('[data-production_time="'+motohour.motohour+'"]').html(self.format_client(motohour.production_time));
                    this.$('[data-repair_time="'+motohour.motohour+'"]').html(self.format_client(motohour.repair_time));
                    this.$('[data-work_time="'+motohour.motohour+'"]').html(self.format_client(motohour.work_time));
            }, this);
            self.real_time_display(real_how_start);
        },
        real_time_display:function(now_real_how_start){
            var self = this;
            var tagName = document.getElementsByTagName('img');
            var leftbar = 0;
            var i;
            for (i = 0; i < tagName.length; i++) {
                if (tagName[i].className =='fs_button'){ leftbar=i;}
            }
            if (self.real_timer == null && tagName[leftbar]!=undefined && now_real_how_start==true){
                var real_timer = setInterval(function(){
                    d = new Date();
                     self.query_sheets();
                     if (tagName[leftbar]!=undefined || now_real_how_start==false){
                        clearInterval(real_timer);
                     }
                },300000);
            }
        },
        launchFullscreen:function() {
            var element = document.documentElement;
            if(element.requestFullscreen) {
                element.requestFullscreen();
            } else if(element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
            } else if(element.webkitRequestFullscreen) {
                element.webkitRequestFullscreen();
            } else if(element.msRequestFullscreen) {
                element.msRequestFullscreen();
            }
        },
        setHide:function(isFulling){
            var tagName = document.getElementsByTagName('td');
            var leftbar = 0;
            var i;
            for (i = 0; i < tagName.length; i++) {
                if (tagName[i].className =='oe_leftbar'){ leftbar=i;}
            }
            if (isFulling == true){
                tagName[leftbar].style.display='none';
            }else{
                tagName[leftbar].style.display='table-cell';
            }
        },
        set_st_value: function(){
             var self = this;
             var p_id = self.get("project_id");
             var shift = self.get("shift");
             var Schedules = new Model('mining.schedule');
             if (p_id) {
                if (shift) {
                    Schedules.query(['start_time', 'end_time'])
                     .filter([['project_id', '=', p_id],['shift','=', shift],['active','=',true]])
                     .limit(1)
                     .first().then(function (schedules) {
                        if (schedules) {
                            self.querying = true;
                            self.set({st_value: schedules});
                            self.querying = false;
                        }
                        else {
                        self.querying = true;
                            self.set({st_value: []});
                            self.querying = false;
                        }
                     });
                }
                     }
        },
        format_client:function(value){
            return formats.format_value(value, { type:"float_time" });
        },
        nameToHex: function(colour) {
            var colours = {
                "aliceblue": "#f0f8ff",
                "antiquewhite": "#faebd7",
                "aqua": "#00ffff",
                "aquamarine": "#7fffd4",
                "azure": "#f0ffff",
                "beige": "#f5f5dc",
                "bisque": "#ffe4c4",
                "black": "#000000",
                "blanchedalmond": "#ffebcd",
                "blue": "#0000ff",
                "blueviolet": "#8a2be2",
                "brown": "#a52a2a",
                "burlywood": "#deb887",
                "cadetblue": "#5f9ea0",
                "chartreuse": "#7fff00",
                "chocolate": "#d2691e",
                "coral": "#ff7f50",
                "cornflowerblue": "#6495ed",
                "cornsilk": "#fff8dc",
                "crimson": "#dc143c",
                "cyan": "#00ffff",
                "darkblue": "#00008b",
                "darkcyan": "#008b8b",
                "darkgoldenrod": "#b8860b",
                "darkgray": "#a9a9a9",
                "darkgreen": "#006400",
                "darkkhaki": "#bdb76b",
                "darkmagenta": "#8b008b",
                "darkolivegreen": "#556b2f",
                "darkorange": "#ff8c00",
                "darkorchid": "#9932cc",
                "darkred": "#8b0000",
                "darksalmon": "#e9967a",
                "darkseagreen": "#8fbc8f",
                "darkslateblue": "#483d8b",
                "darkslategray": "#2f4f4f",
                "darkturquoise": "#00ced1",
                "darkviolet": "#9400d3",
                "deeppink": "#ff1493",
                "deepskyblue": "#00bfff",
                "dimgray": "#696969",
                "dodgerblue": "#1e90ff",
                "firebrick": "#b22222",
                "floralwhite": "#fffaf0",
                "forestgreen": "#228b22",
                "fuchsia": "#ff00ff",
                "gainsboro": "#dcdcdc",
                "ghostwhite": "#f8f8ff",
                "gold": "#ffd700",
                "goldenrod": "#daa520",
                "gray": "#808080",
                "green": "#008000",
                "greenyellow": "#adff2f",
                "honeydew": "#f0fff0",
                "hotpink": "#ff69b4",
                "indianred ": "#cd5c5c",
                "indigo": "#4b0082",
                "ivory": "#fffff0",
                "khaki": "#f0e68c",
                "lavender": "#e6e6fa",
                "lavenderblush": "#fff0f5",
                "lawngreen": "#7cfc00",
                "lemonchiffon": "#fffacd",
                "lightblue": "#add8e6",
                "lightcoral": "#f08080",
                "lightcyan": "#e0ffff",
                "lightgoldenrodyellow": "#fafad2",
                "lightgrey": "#d3d3d3",
                "lightgreen": "#90ee90",
                "lightpink": "#ffb6c1",
                "lightsalmon": "#ffa07a",
                "lightseagreen": "#20b2aa",
                "lightskyblue": "#87cefa",
                "lightslategray": "#778899",
                "lightsteelblue": "#b0c4de",
                "lightyellow": "#ffffe0",
                "lime": "#00ff00",
                "limegreen": "#32cd32",
                "linen": "#faf0e6",
                "magenta": "#ff00ff",
                "maroon": "#800000",
                "mediumaquamarine": "#66cdaa",
                "mediumblue": "#0000cd",
                "mediumorchid": "#ba55d3",
                "mediumpurple": "#9370d8",
                "mediumseagreen": "#3cb371",
                "mediumslateblue": "#7b68ee",
                "mediumspringgreen": "#00fa9a",
                "mediumturquoise": "#48d1cc",
                "mediumvioletred": "#c71585",
                "midnightblue": "#191970",
                "mintcream": "#f5fffa",
                "mistyrose": "#ffe4e1",
                "moccasin": "#ffe4b5",
                "navajowhite": "#ffdead",
                "navy": "#000080",
                "oldlace": "#fdf5e6",
                "olive": "#808000",
                "olivedrab": "#6b8e23",
                "orange": "#ffa500",
                "orangered": "#ff4500",
                "orchid": "#da70d6",
                "palegoldenrod": "#eee8aa",
                "palegreen": "#98fb98",
                "paleturquoise": "#afeeee",
                "palevioletred": "#d87093",
                "papayawhip": "#ffefd5",
                "peachpuff": "#ffdab9",
                "peru": "#cd853f",
                "pink": "#ffc0cb",
                "plum": "#dda0dd",
                "powderblue": "#b0e0e6",
                "purple": "#800080",
                "red": "#ff0000",
                "rosybrown": "#bc8f8f",
                "royalblue": "#4169e1",
                "saddlebrown": "#8b4513",
                "salmon": "#fa8072",
                "sandybrown": "#f4a460",
                "seagreen": "#2e8b57",
                "seashell": "#fff5ee",
                "sienna": "#a0522d",
                "silver": "#c0c0c0",
                "skyblue": "#87ceeb",
                "slateblue": "#6a5acd",
                "slategray": "#708090",
                "snow": "#fffafa",
                "springgreen": "#00ff7f",
                "steelblue": "#4682b4",
                "tan": "#d2b48c",
                "teal": "#008080",
                "thistle": "#d8bfd8",
                "tomato": "#ff6347",
                "turquoise": "#40e0d0",
                "violet": "#ee82ee",
                "wheat": "#f5deb3",
                "white": "#ffffff",
                "whitesmoke": "#f5f5f5",
                "yellow": "#ffff00",
                "yellowgreen": "#9acd32"
            };

            if (typeof colours[colour.toLowerCase()] != undefined) return colours[colour.toLowerCase()];
            return false;
        },
        hex2rgb: function(col) {
            var r, g, b;
            if (col.charAt(0) == '#') {
                col = col.substr(1);
            }
            r = col.charAt(0) + col.charAt(1);
            g = col.charAt(2) + col.charAt(3);
            b = col.charAt(4) + col.charAt(5);
            r = parseInt(r, 16);
            g = parseInt(g, 16);
            b = parseInt(b, 16);
            return 'rgba(' + r + ',' + g + ',' + b + ',0.2)';
        }

    });
    core.form_custom_registry.add('technic_motohour', TechnicMotohour);
});
