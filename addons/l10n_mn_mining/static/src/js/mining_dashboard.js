odoo.define('l10n_mn_mining.mining_dashboard', function (require) {
	"use strict";

    var core = require('web.core');
    var FormView = require('web.FormView');
    var form_common = require('web.form_common');
    var data = require('web.data');
    var utils = require('web.utils');
    var formats = require('web.formats');
    var _t = core._t;
    var QWeb = core.qweb;
    var Model = require('web.DataModel');
    var ajax = require('web.ajax');
    var time = require('web.time');

    var MiningDashBoard = form_common.FormWidget.extend(form_common.ReinitializeWidgetMixin, {
        init: function() {
            $("head").append('<link rel="stylesheet" href="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/styles/jqx.base.css" type="text/css" />');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxcore.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxdata.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxscrollbar.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxbuttons.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxdatatable.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/scripts/demos.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxdata.export.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxchart.core.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxdraw.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxmenu.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxlistbox.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxdropdownlist.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxgrid.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxgrid.selection.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxgrid.pager.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxgrid.filter.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxcheckbox.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxgrid.columnsresize.js"></script>');
            $("head").append('<script type="text/javascript" src="l10n_mn_dashboard/static/src/jqwidgets/jqwidgets/jqxgrid.sort.js"></script>');

            this._super.apply(this, arguments);
            var self = this;
            this.set({
                start_date: false,
                end_date: false,
                projects: false,
                products: false,
                measurement: false,
                group_by: false,
            });
            this.updating = false;
            this.field_manager.on("field_changed:start_date", this, function() {
                this.set({"start_date": this.field_manager.get_field_value("start_date")});
            });
            this.field_manager.on("field_changed:end_date", this, function() {
                this.set({"end_date": this.field_manager.get_field_value("end_date")});
            });
            this.field_manager.on("field_changed:measurement", this, function() {
                this.set({"measurement": this.field_manager.get_field_value("measurement")});
            });
            this.field_manager.on("field_changed:group_by", this, function() {
                this.set({"group_by": this.field_manager.get_field_value("group_by")});
            });
            this.field_manager.on("field_changed:project_ids", this, function() {
                this.set({"projects": this.field_manager.get_field_value("project_ids")});
            });
            this.field_manager.on("field_changed:product_ids", this, function() {
                this.set({"products": this.field_manager.get_field_value("product_ids")});
            });
            this.res_o2m_drop = new utils.DropMisordered();
            this.render_drop = new utils.DropMisordered();
            this.description_line = _t("/");
            // Original save function is overwritten in order to wait all running deferreds to be done before actually applying the save.
            this.view.original_save = _.bind(this.view.save, this.view);
            this.view.save = function(prepend_on_create){
                self.prepend_on_create = prepend_on_create;
                return $.when.apply($, self.defs).then(function(){
                    return self.view.original_save(self.prepend_on_create);
                });
            };
        },
        initialize_field: function() {
            form_common.ReinitializeWidgetMixin.initialize_field.call(this);
            var self = this;
            self.on("change:start_date", self, self.initialize_content);
            self.on("change:end_date", self, self.initialize_content);
            self.on("change:projects", self, self.initialize_content);
            self.on("change:products", self, self.initialize_content);
            self.on("change:measurement", self, self.initialize_content);
            self.on("change:group_by", self, self.initialize_content);
        },

        initialize_content: function() {
            var self = this;
            this.destroy_content();
            var plan_actual_obj;
            var start_date = self.get('start_date');
            var end_date = self.get('end_date');
            var measurement = self.get('measurement');
            var group_by = self.get('group_by');
            var projects = []
            var products = []
            if (self.get('projects').length == 0) {}
            else {projects = self.get('projects')[0][2]}
            if (self.get('products').length == 0) {}
            else {products = self.get('products')[0][2]}

            return new Model("mining.dashboard").call("get_plan_actual", [start_date, end_date, projects, products, measurement, group_by, new data.CompoundContext()])
            .then(function(details) {
                plan_actual_obj = details;

            }).then(function(result) {
                self.plan_actual_obj = plan_actual_obj;
                self.display_data();
            });
        },
        get_plan_actual: function(obj){
            var self = this;
            var sampleData = obj;
            // prepare jqxChart settings
            var toolTipCustomMeas = function (value, itemIndex, serie, group, categoryValue, categoryAxis) {
                return '<DIV style="text-align:left";><b>Маркшейдер <br/>'+value+' </b><br/><br/> </DIV>';
            };
            var toolTipCustomDisp = function (value, itemIndex, serie, group, categoryValue, categoryAxis) {
                return '<DIV style="text-align:left";><b>Диспетчер <br/>'+value+' </b><br/><br/> </DIV>';
            };
            var toolTipCustomPlan = function (value, itemIndex, serie, group, categoryValue, categoryAxis) {
                return '<DIV style="text-align:left";><b>Төлөвлөгөө <br/>'+value+' </b><br/><br/> </DIV>';
            };
            var toolTipCustomPerf = function (value, itemIndex, serie, group, categoryValue, categoryAxis) {
                return '<DIV style="text-align:left";><b>Бодит тн <br/>'+value+' </b><br/><br/> </DIV>';
            };
            var ms = self.get('measurement');
            var name = (ms == 'tn') ? "Уулын цул (тн)" : "Уулын цул (м3)";
            var settings = {
                title: name,
                description: "Төлөвлөгөө гүйцэтгэл",
                enableAnimations: true,
                showLegend: true,
                showBorderLine: false,
                toolTipHideDelay: 5000,
                padding: { left: 5, top: 5, right: 5, bottom: 5 },
                titlePadding: { left: 0, top: 0, right: 0, bottom: 0 },
                source: sampleData,
                colorScheme: 'scheme02',
                xAxis:
                {
                    dataField: 'Date',
                    labels: { visible: true,
                        angle:90,
                        },
                    gridLines: { visible: false,}
                },
                valueAxis:
                {
                    minValue: 0,
                    visible: true,
                    gridLines: { visible: false, }
                },
                seriesGroups:
                    [
                        {
                              type: 'area',
                              alignEndPointsWithIntervals: true,
                              series: [
                                      { dataField: 'Plan', displayText: 'Төлөвлөгөө',symbolType: 'circle',
                                      toolTipFormatFunction: toolTipCustomPlan,  opacity: 1},
                              ]
                          },
                        {
                            type: 'column',

                            columnsGapPercent: 40,
                            seriesGapPercent: 10,
                            series: [
                                    { dataField: 'Meas', displayText: 'Маркшейдер' , toolTipFormatFunction: toolTipCustomMeas,},
                                    { dataField: 'Disp', displayText: 'Диспетчер' , toolTipFormatFunction: toolTipCustomDisp,},
                                    { dataField: 'Perf', displayText: 'Бодит тн' , toolTipFormatFunction: toolTipCustomPerf,},
                            ]
                        },
                    ]
            };
            return settings;
        },
        destroy_content: function() {
            if (this.dfm) {
                this.dfm.destroy();
                this.dfm = undefined;
            }
        },
        display_data: function() {
            var self = this;
            self.$el.html(QWeb.render("l10n_mn_mining.mining_dashboard", {widget: self}));
            try {
                $('#plan_actual').jqxChart(self.get_plan_actual(self.plan_actual_obj.plan_actual));
                $('#plan_actual').jqxChart('addColorScheme', 'customColorScheme', ['#77bc65','#ff7a00','#2a6099', '#ec9ba4']);
                $('#plan_actual').jqxChart({ colorScheme: 'customColorScheme' });
            }
            catch(err) {
                console.log(err.message);
            }
        },
    });
    core.form_custom_registry.add('mining_dashboard', MiningDashBoard);
});