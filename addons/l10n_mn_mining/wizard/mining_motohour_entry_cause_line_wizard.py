# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016-today Asterisk Technologies LLC (<http://www.asterisk-tech.mn>)

#
##############################################################################

from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo import exceptions
from odoo.exceptions import ValidationError, UserError
from odoo.addons.l10n_mn_web.models.time_helper import *

class mining_motohour_entry_cause_line(models.TransientModel):
    _name = 'mining.motohour.entry.cause.line.wizard'
    _description = 'Mining Motohour Cause Line'

    @api.depends('start_time', 'motohour_cause_id')
    @api.multi
    def _set_start_time(self):
        for o in self:
            time = o.start_time
            if o.motohour_cause_id.shift == 'night':
                time += 24
            o.r_start_time = time
            
    @api.depends('finish_motohour', 'start_motohour')
    @api.multi
    def _compute_performance_motohour_km(self):
        for obj in self:
            if obj.finish_motohour > 0:
                obj.performance_motohour = obj.finish_motohour - obj.start_motohour
            else:
                obj.performance_motohour = 0
            if obj.finish_km > 0:
                obj.performance_km = obj.finish_km - obj.start_km
            else:
                obj.performance_km = 0

    @api.onchange('cause_id')
    def on_change_cause(self):
        cause_obj = self.env['mining.motohours.cause'].browse(self.cause_id.id)
        value = {}
        if cause_obj:
            value = {
                'is_repair': cause_obj.is_repair,
                'work_order_id': False
            }
        return {
            'value': value,
        }

    # Тухайн мотоцагийн шалтгааны нэрийг харуулах.
    @api.onchange('cause_id', 'cause_name')
    def onchange_name(self):
        res = {}
        name = ''
        if self.cause_id:
            cause_ids = self.env['mining.motohours.cause'].search([('id', '=', self.cause_id.id)])
            for cause in cause_ids:
                name = cause.cause_name
                res.update({'cause_name': name})
            return {'value': res}

    # Эхлэх цаг авах
    @api.multi
    def _get_start_time(self):
        now_time = get_day_like_display(datetime.now(), self.env.user)
        return now_time.hour + float(now_time.minute) / 60

    # Өмнөх мөр
    @api.multi
    def _default_previous_cause_line_id(self):
        default_motohour_cause_id = self._context.get('default_motohour_cause_id', [])
        if default_motohour_cause_id:
            motohour_cause_obj = self.env['mining.motohour.entry.line'].browse(default_motohour_cause_id)
            previous_cause_line_id = self.env['mining.motohour.entry.cause.line'].search([('motohour_cause_id', '=', default_motohour_cause_id)], order='start_motohour desc', limit=1)
            if not previous_cause_line_id:
                previous_entry_lines = self.env['mining.motohour.entry.line'].search([('id', '!=', default_motohour_cause_id),('motohour_id.state', '=', 'approved'), ('technic_id', '=', motohour_cause_obj.technic_id.id)], order='date desc', limit=1)
                previous_cause_line_id = self.env['mining.motohour.entry.cause.line'].search([('motohour_cause_id', '=', previous_entry_lines.id)], order='start_motohour, start_km desc', limit=1)
                return previous_cause_line_id.id
            else:
                return previous_cause_line_id.id
                
    company_id = fields.Many2one('res.company', string='Company', required=True,
        default=lambda self: self.env.user.company_id)
    previous_cause_line_id = fields.Many2one('mining.motohour.entry.cause.line', string='Previous Cause Line', default=_default_previous_cause_line_id)
    motohour_cause_id = fields.Many2one('mining.motohour.entry.line', string='Cause ID', required=True, ondelete='cascade')
    cause_id = fields.Many2one('mining.motohours.cause', string='Cause', required=True)
    cause_name = fields.Char(related='cause_id.cause_name', string='Cause Name')
    is_medium_technic = fields.Boolean(related='motohour_cause_id.is_medium_technic', string='Is Medium Technic')
    start_time = fields.Float(string='Start Time', digits=(16, 2), default=_get_start_time)
    r_start_time = fields.Float(compute='_set_start_time', digits=(16, 2), string='Real Start Time',
                                readonly=True, store=True)
    diff_time = fields.Float(string='Diff Time', digits=(16, 2), readonly=True)
    description = fields.Char(string='Description', size=150)
    location_id = fields.Many2one('mining.location', string='Location')
    color = fields.Selection(related='cause_id.color', string='Color')
    is_repair = fields.Boolean(string='Is Repair')
    percentage = fields.Integer(string='Percentage', readonly=True, invisible=True)
    product_id = fields.Many2one('product.product', string='Product', domain=[('is_mining_product', '=', True)])
    operator_id = fields.Many2one('res.users', string='Operator')
    carrier_id = fields.Many2one('technic', 'Carrier', domain=[('technic_type_id.is_mechanism', '=', True)])
    carrier_operator_id = fields.Many2one('res.users', string='Carrier Operator')
    start_motohour = fields.Float(string='Start Motohour')
    start_km = fields.Float(string='Start KM', default=0)
    finish_motohour = fields.Float(string='Start Motohour', default=0)
    finish_km = fields.Float(string='Start KM', default=0)
    performance_motohour = fields.Float(string='Performance Motohour', compute='_compute_performance_motohour_km', store=True)
    performance_km = fields.Float(string='Performance KM', compute='_compute_performance_motohour_km', store=True)
    
    @api.onchange('previous_cause_line_id')
    def onchange_previous_cause_line_id(self):
        self.operator_id = self.previous_cause_line_id.operator_id
        if self.company_id.automate_check_motohour_error and self.previous_cause_line_id.cause_id.cause_type.type == 'smu':
            return 0
        else:
            self.start_motohour = self.previous_cause_line_id.start_motohour
            
    def _prepare_cause_line(self):
        return {'cause_id': self.cause_id.id,
              'cause_name': self.cause_name,
              'start_time': self.start_time,
              'product_id': self.product_id.id,
              'operator_id': self.operator_id.id,
              'carrier_id': self.carrier_id.id,
              'carrier_operator_id': self.carrier_operator_id.id,
              'start_motohour': self.start_motohour,
              'start_km': self.start_km,
              'finish_motohour': self.finish_motohour,
              'finish_km': self.finish_km,
              'location_id': self.location_id.id,
              'description': self.description,}
            
    @api.multi
    def action_confirm(self):
        for obj in self:
            vals = self._prepare_cause_line()
            cause_line = self.env['mining.motohour.entry.cause.line'].create(vals)
            
