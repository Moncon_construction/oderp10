
from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo import exceptions


class mining_motohour_entry_reload(models.TransientModel):
	_name = 'mining.motohour.entry.reload'
	_description = 'The Motohour Entry Reload'
	
	@api.multi
	def reload_mh(self):
		obj = self.env['mining.daily.entry'].browse(self.env.context['active_ids'])
		for item in obj:
			for line in item.motohour_line:
				line._sum_time()
		return True
