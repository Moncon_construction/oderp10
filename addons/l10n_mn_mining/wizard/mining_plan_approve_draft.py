
from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo import exceptions

class mining_plan_approve(models.TransientModel):
	_name = 'mining.plan.approve'
	_description = 'Mining Plan Approve'

	@api.multi
	def reload_approve(self):
		obj = self.env['mining.plan'].browse(self.env.context['active_ids'])
		for item in obj:
			item.write({'state': 'approved'})
		return True


class mining_plan_draft(models.TransientModel):
	_name = 'mining.plan.draft'
	_description = 'Mining Plan Draft'

	@api.multi
	def reload_draft(self):
		obj = self.env['mining.plan'].browse(self.env.context['active_ids'])
		for item in obj:
			item.write({'state': 'draft'})
		return True
