
from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo import exceptions


class mining_production_entry_reload(models.TransientModel):
	_name = 'mining.production.entry.reload'
	_description = 'The Production Entry Reload'
	
	@api.multi
	def reload(self):
		obj = self.env['mining.daily.entry'].browse(self.env.context['active_ids'])
		for item in obj:
			for line in item.production_line_ids:
				res = line._sum_all(['sum_m3', 'sum_tn', 'body_capacity_tn', 'body_capacity_m3'], None)
				line.write(res[line.id])
		return True
