
from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo import exceptions


class mining_surveyor_measurement_reload(models.TransientModel):
    _name = 'mining.surveyor.measurement.reload'
    _description = 'Mining Surveyor Measurement Reload'
    
    @api.multi
    def reload_surveyor(self):
        obj = self.env['mining.surveyor.measurement'].browse(self.env.context['active_ids'])
        for item in obj:
            for line in item.line_ids:
                res = line._sum_all(['amount_by_measurement_tn'], None)
                self.env['mining.surveyor.measurement.line'].write(line.id, res[line.id])
        return True
