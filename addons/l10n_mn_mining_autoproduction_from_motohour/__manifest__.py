{
    'name': 'Mongolian Mining Autoproduction From Motohour',
    'version': '0.1',
    "author": "Asterisk Technologies LLC",
    'category': 'Mining',
    'description': 'Энэхүү модулийг суулгасанаар Мотоцагийн бүртгэлээс уулын бүтээлийн бртгэл автомат орж ирдэг болно.',
    "website": "http://asterisk-tech.mn",
    'depends': ['l10n_mn_mining', 'l10n_mn_task'],
    'data': [
        'views/mining_dispatcher_view.xml'
     ],
    'installable': True,
    'application': True,
    'auto_install': False,
}