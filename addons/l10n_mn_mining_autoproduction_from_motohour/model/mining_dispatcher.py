# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016-today Asterisk Technologies LLC (<http://www.asterisk-tech.mn>)

#
##############################################################################

from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo import exceptions
from odoo.exceptions import ValidationError, UserError
from odoo.addons.l10n_mn_web.models.time_helper import *

class mining_motohour_entry_cause_line(models.Model):
    _inherit = 'mining.motohour.entry.cause.line'
    
    is_autoproduction = fields.Boolean('Is Autoproduction')

class mining_daily_entry(models.Model):
    _inherit = 'mining.daily.entry'
    
    # Мотоцагийн бүртгэлээс уулын бүтээлийн бртгэл автомат орж ирэх функц
    @api.multi
    def action_autoproduction(self):
        for obj in self:
            mining_production_line_obj = self.env['mining.production.entry.line']
            for line in obj.motohour_line:
                self._cr.execute("""SELECT task_type_id as task_type_id, product_id as product_id, 
                        operator_id as operator_id, carrier_id as carrier_id, carrier_operator_id as carrier_operator_id, sum(diff_time) as diff_time
                        FROM mining_motohour_entry_cause_line
                        WHERE motohour_cause_id = %s and product_id is not null and task_type_id is not null and carrier_id is not null and is_autoproduction = False
                        GROUP BY task_type_id, product_id, operator_id, carrier_id, carrier_operator_id
                                """ % line.id)
                results = self._cr.dictfetchall()
                for result in results:
                    vals = {'dump_id': line.technic_id.id,
                            'product_id': result['product_id'],
                            'dump_driver_workhour': result['diff_time'],
                            'excavator_id': result['carrier_id'],
                            'land_avg_km': 0,
                            'task_type_id': result['task_type_id'] if result['task_type_id'] != None else '',
                            'production_id': obj.id}
                    if result['operator_id'] != None:
                        resource_ops = self.env['resource.resource'].search([('user_id', '=', result['operator_id'])])
                        employee_op = self.env['hr.employee'].search([('resource_id', 'in', resource_ops.ids)], limit=1)
                        vals.update({'dump_driver_id': employee_op.id})
                    if result['carrier_operator_id'] != None:
                        resource_carops = self.env['resource.resource'].search([('user_id', '=', result['carrier_operator_id'])])
                        employee_carop = self.env['hr.employee'].search([('resource_id', 'in', resource_carops.ids)], limit=1)
                        vals.update({'excavator_driver_id': employee_carop.id})
                    mining_production_line_obj.create(vals)
                for cause in line.motohour_cause_line:
                    cause.is_autoproduction = True
        print