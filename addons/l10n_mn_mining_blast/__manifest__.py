{
    'name': 'Mongolian Mining Blast',
    'version': '0.1',
    "author": "Asterisk Technologies LLC",
    'category': 'Mining',
    'description': 'Mining Blast',
    "website": "http://asterisk-tech.mn",
    'depends': ['l10n_mn_mining'],
    'data': [
        'security/mining_blast.xml',
        'security/ir.model.access.csv',
        'views/blast/mining_blast_view.xml',
        'views/blast/mining_blast_other_supplies_initial_data.xml',
        'views/product/blast_product_view.xml',
     ],
    'installable': True,
    'application': True,
    'auto_install': False,
}