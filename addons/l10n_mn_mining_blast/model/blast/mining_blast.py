# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016-today Asterisk Technologies LLC (<http://www.asterisk-tech.mn>)

#
##############################################################################

from odoo import api, fields, models, _
from datetime import datetime
from odoo import exceptions


class MiningBlast(models.Model):
    _name = 'mining.blast'
    _description = 'The Blast'
    _inherit = ["mail.thread"]
    STATE_SELECTION = [
        ('draft', 'Draft'),
        ('approved', 'Approved'),
    ]

    # Нэр оноох Өдөр+Төсөл
    @api.depends('date')
    @api.multi
    def _set_name(self):
        self.name = str(self.date)

    @api.depends('blast_line_ids')
    @api.multi
    def _sum_all(self):
        for obj in self:
            sum_blast = 0.0
            sum_substance = 0.0
            for item in obj.blast_line_ids:
                if item.product_id.mining_product_type == 'soil':
                    sum_substance += item.anfo + item.emulsion
                    sum_blast += item.blast_m3
                else:
                    if item.product_id.mining_product_type == 'mineral' or item.product_id.mining_product_type == 'mineral_reprocess':
                        sum_substance += item.anfo + item.emulsion
                        sum_blast += item.blast_m3
                    else:
                        raise osv.except_osv(_('Warning'),
                                             _('That location has not %s' %item.product_id.name))
                        continue

            obj.sum_blast_m3 = sum_blast
            if sum_blast != 0:
                obj.sum_substance_consumption = sum_substance / sum_blast

    # Өдөрөөр салгах
    @api.depends('date')
    @api.multi
    def _set_date(self):
        date_object = datetime.strptime(self.date, '%Y-%m-%d')
        self.year = date_object.year
        self.month = date_object.month
        self.day = date_object.day

    @api.multi
    def _get_project_ids(self):
        proj_ids = self.env['project.project'].search(
            [('favorite_user_ids', 'in', self.env.uid), ('project_type', '=', True)])
        if not proj_ids:
            raise exceptions.except_orm(_(True),
                                        _('Please adjust the mining project and the product category on the project!!!'))
        return proj_ids[0]

    name = fields.Char(compute='_set_name', string='Name', readonly=True, store=True)
    date = fields.Date(string='Date', required=True, default=(datetime.now()).strftime('%Y-%m-%d'),
                       states={'approved': [('readonly', True)]},
                       track_visibility='onchange')
    project_id = fields.Many2one('project.project', string='Project', default=_get_project_ids, required=True,
                                 help='The Project',
                                 states={'approved': [('readonly', True)]}, track_visibility='onchange')

    user_id = fields.Many2one('res.users', string='Registration', required=True, readonly=True,
                              default=lambda self: self.env.user.id)
    blast_engineer_id = fields.Many2one('hr.employee', string='Blast Engineer', required=True,
                                        track_visibility='onchange')

    blast_line_ids = fields.One2many('mining.blast.line', 'blast_id', states={'approved': [('readonly', True)]})
    blast_supplies_line_ids = fields.One2many('blast.supplies.line', 'blast_supplies_id', states={'approved': [('readonly', True)]})
    state = fields.Selection(STATE_SELECTION, 'State', readonly=True, track_visibility='onchange', default='draft')
    notes = fields.Text('Notes', required=True, states={'approved': [('readonly', True)]})
    sum_blast_m3 = fields.Float(compute='_sum_all', string='Total blast m3', readonly=True, store=True)
    sum_substance_consumption = fields.Float(compute='_sum_all', string='Total kg/m3', readonly=True, store=True)

    year = fields.Integer(compute='_set_date', string='Year', readonly=True, store=True)
    month = fields.Integer(compute='_set_date', string='Month', readonly=True, store=True)
    day = fields.Integer(compute='_set_date', string='Day', readonly=True, store=True)

    @api.multi
    def action_to_approved(self):
        blast = self.env['mining.blast'].search([('state', '=', 'approved'), ('date', '=', self.date)])
        if blast:
            raise exceptions.except_orm(_('Error'), _('On this date, another blast has already been approved.'))
        else:
            self.write({'state': 'approved'})
        return True

    @api.multi
    def action_to_draft(self):
        return self.write({'state': 'draft'})


class MiningBlastLine(models.Model):
    _name = 'mining.blast.line'
    _description = 'Blast Line'

    @api.depends('anfo','emulsion', 'blast_m3')
    @api.multi
    def _sum_blast(self):
        for obj in self:
            if obj.blast_m3 != 0:
                obj.substance_consumption = (obj.anfo + obj.emulsion) / (obj.blast_m3)

    blast_id = fields.Many2one('mining.blast', string='Blast ID', required=True, ondelete='cascade')
    blast_number = fields.Char(string='Blast number', required=True)
    location_id = fields.Many2one('mining.location', string='Block', required=True)
    product_id = fields.Many2one('product.template', string='Product', required=True, help='The Product',domain=[('is_mining_product', '=', 'True')])
    blast_m3 = fields.Float(string='Blast м3')
    anfo = fields.Float(string='Anfo')
    emulsion = fields.Float(string='Emulsion')
    substance_consumption = fields.Float(compute='_sum_blast', string='Consumption of explosives', store=True)
    description = fields.Text('Description', size=250)


class BlastSuppliesLine(models.Model):
    _name = 'blast.supplies.line'
    _description = 'Other Blast Supplies'

    blast_supplies_id = fields.Many2one('mining.blast', string='Blast ID', required=True, ondelete='cascade')
    material_id = fields.Many2one('blast.supplies', string='Name')
    total = fields.Float(string='Total')


class BlastSupplies(models.Model):
    _name = 'blast.supplies'
    _description = 'Other Blast Supplies'

    name = fields.Char('Name')
    uom_id = fields.Many2one('product.uom', string='Unit of Measure')

