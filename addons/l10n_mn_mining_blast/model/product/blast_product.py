# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016-today Asterisk-Technologies LLC (<http://www.asterisk-tech.mn>)
#
##############################################################################

from odoo import api, fields, models, _

class product_template(models.Model):
    _inherit = 'product.template'

    is_mining_blast_product = fields.Boolean(string='Blast product', default=False, help="If checked, It's mining blast product and that product used in blast plan of mining report")
