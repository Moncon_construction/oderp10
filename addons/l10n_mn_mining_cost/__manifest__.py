# -*- coding: utf-8 -*-

{
    'name': 'Mongolian Mining Cost',
    'version': '0.1',
    'author': 'Asterisk Technologies LLC',
    'category': 'Mongolian Modules',
    'description': '',
    'website': 'http://asterisk-tech.mn',
    'depends': ['l10n_mn_mining', 'l10n_mn_task'],
    'data': [
        'views/task_type.xml',
    ],

    'license': 'GPL-3',
    "installable": True,
    'auto_install': True
}
