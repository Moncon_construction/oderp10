# -*- coding: utf-8 -*-
##############################################################################
#
#	OpenERP, Open Source Management Solution
#	Copyright (C) 2014-today Asterisk Technologies LLC (<http://www.asterisk-tech.mn>)

##############################################################################

from odoo import api, fields, models, _



class MiningPlanLine(models.Model):

    _inherit = 'mining.plan.line'

    task_type_id = fields.Many2one('task.type', string="Task Type")

class MiningProductionEntryLine(models.Model):

    _inherit = 'mining.production.entry.line'

    task_type_id = fields.Many2one('task.type', string="Task Type")

class MiningSurveyorMeasurementLine(models.Model):

    _inherit = 'mining.surveyor.measurement.line'

    task_type_id = fields.Many2one('task.type', string="Task Type")