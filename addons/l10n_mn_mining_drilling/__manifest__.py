# -*- coding: utf-8 -*-

{
    'name': 'Mongolian Mining Drilling',
    'version': '0.1',
    'author': 'Asterisk Technologies LLC',
    'category': 'Mongolian Modules',
    'description': '',
    'website': 'http://asterisk-tech.mn',
    'depends': ['l10n_mn_mining'],
    'data': [
        'security/mining_drill.xml',
        'security/ir.model.access.csv',
        'views/mining_drill_view.xml',
        'report/mining_drill_report_analyze_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
