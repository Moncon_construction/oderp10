# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2014-today ASterisk Technologies LLC (<http://www.asterisk-tech.mn>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import api, fields, models, _
import time
import openerp.addons.decimal_precision as dp
from datetime import datetime
from odoo import exceptions


class MiningDrill(models.Model):
    _name = 'mining.drill'
    _description = 'Drill'
    _inherit = ["mail.thread"]
    STATE_SELECTION = [
        ('draft', 'Draft'),
        ('approved', 'Approved'),
    ]

    @api.depends('drill_line_ids')
    @api.multi
    def _sum_all(self):
        res = {}
        sum_drill = 0.0
        sum_water = 0
        sum_collapsed = 0
        sum_count_hole = 0
        sum_project_depth_m = 0.0
        for item in self.drill_line_ids:
            if item.is_production:
                sum_drill += item.actual_drilled_depth_m
                sum_count_hole += 1
                sum_project_depth_m += item.project_depth_m
                if item.is_water:
                    sum_water += 1
                if item.is_collapsed:
                    sum_collapsed += 1
        self.sum_drill_m = sum_drill
        self.sum_count_hole = sum_count_hole
        self.sum_count_water = sum_water
        self.sum_count_collapsed = sum_collapsed
        self.sum_total_project_m = sum_project_depth_m

    # Өдөрөөр салгах
    @api.depends('date')
    @api.multi
    def _set_date(self):
        date_object = datetime.strptime(self.date, '%Y-%m-%d')
        self.year = date_object.year
        self.month = date_object.month
        self.day = date_object.day

    # Нэр оноох Өдөр+Төсөл
    @api.depends('date')
    @api.multi
    def _set_name(self):
        self.name = str(self.date) + '-' + self.project_id.name

    @api.multi
    def _get_project_ids(self):
        proj_ids = self.env['project.project'].search(
            [('favorite_user_ids', 'in', self.env.uid), ('project_type', '=', True)])
        if not proj_ids:
            raise exceptions.except_orm(_(True),
                                        _('Please adjust the mining project and the product category on the project!!!'))
        return proj_ids[0]

    name = fields.Char(compute='_set_name', string='Name', readonly=True, store=True)
    date = fields.Date(string='Date', required=True, default=(datetime.now()).strftime('%Y-%m-%d'),
                       states={'approved': [('readonly', True)]},
                       track_visibility='onchange')
    project_id = fields.Many2one('project.project', string='Project', default=_get_project_ids, required=True,
                                 help='The Project',
                                 states={'approved': [('readonly', True)]}, track_visibility='onchange')

    user_id = fields.Many2one('res.users', string='Registration', required=True, readonly=True,
                              default=lambda self: self.env.user.id)
    drill_engineer_id = fields.Many2one('hr.employee', string='Drill Engineer', required=True,
                                        track_visibility='onchange')
    drill_line_ids = fields.One2many('mining.drill.line', 'drill_id', states={'approved': [('readonly', True)]})
    state = fields.Selection(STATE_SELECTION, 'State', readonly=True, track_visibility='onchange',default='draft')
    notes = fields.Text('Notes', required=True, states={'approved': [('readonly', True)]})
    sum_drill_m = fields.Float(compute='_sum_all', string='Total drilled m', readonly=True, store=True)
    sum_total_project_m = fields.Float(compute='_sum_all', string='Total project m', readonly=True, store=True)
    sum_count_hole = fields.Integer(compute='_sum_all', string='Total holes',store=True, help='Total drilled holes')
    sum_count_water = fields.Integer(compute='_sum_all', string='Water', store=True, help='Count holes with water')
    sum_count_collapsed = fields.Integer(compute='_sum_all', string='Collapsed', store=True, help='Count collapsed holes')
    year = fields.Integer(compute='_set_date', string='Year', readonly=True, store=True)
    month = fields.Integer(compute='_set_date', string='Month', readonly=True, store=True)
    day = fields.Integer(compute='_set_date', string='Day', readonly=True, store=True)

    @api.multi
    def action_to_approved(self):
        drill = self.env['mining.drill'].search([('state', '=', 'approved'), ('date', '=', self.date)])
        if drill:
            raise exceptions.except_orm(_('Error'), _('On this date, another drill has already been approved.'))
        else:
            self.write({'state': 'approved'})
        return True

    @api.multi
    def action_to_draft(self):
        return self.write({'state': 'draft'})


class MiningDrillLine(models.Model):
    _name = 'mining.drill.line'
    _description = 'Drill Line'

    @api.onchange('location_id')
    def onchange_location_id(self):
        value = {'hole_id': False}
        self.write(value)
        return {'value': value}

    drill_id = fields.Many2one('mining.drill', 'Drill ID', required=True, ondelete='cascade')
    drill_technic_id = fields.Many2one('technic', 'Drill technic', domain=[('technic_norm_id.drill', '=', 'True')], required=True)
    location_id = fields.Many2one('mining.location', 'Block number', required=True)
    hole_id = fields.Many2one('mining.hole', 'Hole number', required=True)
    drill_diameter_mm = fields.Float(string='Drill capacity')
    project_depth_m = fields.Float(related='hole_id.hole_deep_m', string='Project depth m', readonly=True)
    actual_drilled_depth_m = fields.Float(string='Actual drilled depth m')
    hard_rock_started_depth_m = fields.Float(string='Hard rock started depth m')
    hard_rock_finished_depth_m = fields.Float(string='Hard rock finished depth m')
    mineral_started_depth_m = fields.Float(string='Mineral started depth m')
    mineral_finished_depth_m = fields.Float(string='Mineral finished depth m')
    is_water = fields.Boolean(string='Is water')
    is_collapsed = fields.Boolean(string='Is collapsed')
    description = fields.Text(string='More information')
    is_production = fields.Boolean('Is Production', default=True)
