# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC
#    web: //www.asterisk-tech.mn
#
##############################################################################

from odoo import api, fields, models, _
from odoo import tools
from datetime import datetime, timedelta

class report_mining_drill_analyze(models.Model):
    _name = 'report.mining.drill.analyze'
    _description = 'Report Mining Drill'
    _auto = False

    date = fields.Date(string='Date', readonly=True)
    project_id = fields.Many2one('project.project', string='Project', readonly=True)
    location_id = fields.Many2one('mining.location', string='Location', readonly=True)
    hole_id = fields.Many2one('mining.hole', string='Hole', readonly=True)
    drill_technic_id = fields.Many2one('technic',string='Drill technic', readonly=True)
    project_depth_m = fields.Float(string='Project depth m', readonly=True)
    actual_drilled_depth_m = fields.Float(string='Actual drilled depth m', readonly=True)
    hole_count = fields.Integer(string='Hole Count', readonly=True)
    hard_rock_started_depth_m = fields.Float(string='Hard rock started depth m', readonly=True)
    hard_rock_finished_depth_m = fields.Float(string='Hard rock finished depth m', readonly=True)
    mineral_started_depth_m = fields.Float(string='Mineral started depth m', readonly=True)
    mineral_finished_depth_m = fields.Float(string='Mineral finished depth m', readonly=True)
    is_water = fields.Float(string='Is water', readonly=True)
    is_collapsed = fields.Float(string='Is collapsed', readonly=True)
    is_production = fields.Char(string='Is Production', readonly=True)

    _order = 'date desc'

    def init(self):
        tools.sql.drop_view_if_exists(self.env.cr, 'report_mining_drill_analyze')
        self.env.cr.execute("""
            CREATE or REPLACE view report_mining_drill_analyze as
            select
            min(mdl.id) as id,
            md.project_id as project_id,
            md.date as date,
            mdl.location_id as location_id,
            mdl.drill_technic_id,
            sum(mdl.actual_drilled_depth_m) as actual_drilled_depth_m,
            sum(mh.hole_deep_m) as project_depth_m,
            count(hole_id) as hole_count,
            mdl.hole_id,
            MAX(mdl.hard_rock_started_depth_m) as hard_rock_started_depth_m,
            MAX(mdl.hard_rock_finished_depth_m) as hard_rock_finished_depth_m,
            MAX(mdl.mineral_started_depth_m) as mineral_started_depth_m,
            MAX(mdl.mineral_finished_depth_m) as mineral_finished_depth_m,
            CASE WHEN mdl.is_water =True THEN 1 ELSE 0 END AS is_water,
            CASE WHEN mdl.is_collapsed =True THEN 1 ELSE 0 END AS is_collapsed,
        mdl.is_production
        FROM mining_drill_line mdl
                    left join mining_drill md on (md.id = mdl.drill_id)
                    left join mining_hole mh on (mh.id = mdl.hole_id)
                    left join location_product ml on (mdl.location_id = ml.id)
        group by
            md.project_id,
            md.date,
            mdl.location_id,
            mdl.drill_technic_id,
            mdl.hole_id,
            mdl.is_water,
            mdl.is_collapsed,
            mdl.is_production
        """)
