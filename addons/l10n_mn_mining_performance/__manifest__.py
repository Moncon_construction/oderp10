# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Mongolian Mining Performance',
    'version': '1.1',
    'category': 'Mongolian Modules',
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'sequence': 90,
    'depends': ['l10n_mn_mining'],
    'description': """
        Энэ модуль нь уулын бүтээлийн бүртгэл дээр бодит тоннын хэмжээг оруулдаг байна.
    """,
    'data': [
        'views/mining_dispatcher_view.xml',
        'wizard/mining_performance_view.xml'
    ],
    'demo': [
        ],
    'application': True,
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
