from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo import exceptions
from odoo.exceptions import ValidationError, UserError

class mining_production_entry_line(models.Model):
    _inherit = 'mining.production.entry.line'
    
    real_tonn = fields.Float(string='Real Tonn')
    notes = fields.Char('Notes')