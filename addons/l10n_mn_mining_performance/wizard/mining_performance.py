# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError
import math


class MiningPerformanceWiazrd(models.TransientModel):
    _name = 'mining.performance.wizard'

    line_ids = fields.One2many('mining.performance.line', 'wizard_id', 'Mining Performance Lines', required=True)
    
    @api.multi
    def action_calculate(self):
        for obj in self:
            for line in obj.line_ids:
                context = self._context
                minings = self.env['mining.daily.entry'].browse(context.get('active_ids'))
                total_tn = 0
                for mining in minings:
                    if mining.production_line_ids.filtered(lambda l: l.product_id.id == line.product_id.id):
                        for production_line in mining.production_line_ids.filtered(lambda l: l.product_id.id == line.product_id.id):
                            total_tn += production_line.sum_tn
                for mining in minings:
                    if mining.production_line_ids.filtered(lambda l: l.product_id.id == line.product_id.id) and total_tn > 0:
                        for production_line in mining.production_line_ids.filtered(lambda l: l.product_id.id == line.product_id.id):
                            percent = production_line.sum_tn / total_tn
                            production_line.real_tonn = line.total_tonn * percent
                            production_line.notes = line.notes
                
class MiningPerformanceLine(models.TransientModel):
    _name = 'mining.performance.line'
    
    wizard_id = fields.Many2one('mining.performance.wizard', string='Mining Performance')
    product_id = fields.Many2one('product.product', string='Product', domain=[('is_mining_product', '=', True)])
    total_tonn = fields.Float('Total Tonn')
    notes = fields.Char('Notes')
    
    _sql_constraints = [ 
         ('product_uniq', 'unique(product_id, wizard_id)', 'Product must be unique!'),
     ]