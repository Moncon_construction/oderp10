# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

# TODO: Үндсэн l10n_mn_mrp санхүүгээс хамаарахгүй байх тохиолдолд бүтцийн өөрчлөлт хийх
{
    'name': 'Mongolian Manufacturing',
    'version': '2.1',
    'category': 'Mongolian Modules',
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'sequence': 15,
    'depends': ['mrp', 'l10n_mn', 'l10n_mn_hr', 'sale_mrp', 'l10n_mn_stock'],
    'description': """
        Managing production by work order.
    """,
    'data': [
        'security/security_view.xml',
        'security/ir.model.access.csv',
        'data/account_journal.xml',
        'views/product_view.xml',
        'views/mrp_bom_views.xml',
        'views/mrp_config_setting_views.xml',
        'views/mrp_workcenter_views.xml',
        'views/mrp_routing_views.xml',
        'views/mrp_workorder_views.xml',
        'views/mrp_production_views.xml',
        'views/mrp_workorder_views.xml',
        'views/mrp_operation_norm_views.xml',
        'views/stock_move_views.xml',
        'wizard/extend_expiry_date_wizard.xml',
    ],
    'demo': [
        ],
    'application': True,
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
