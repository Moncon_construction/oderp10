# -*- coding: utf-8 -*-

import odoo
from odoo import _, api, fields, models, modules, tools
from odoo.exceptions import UserError, ValidationError


class Module(models.Model):
    _inherit = "ir.module.module"

    @api.multi
    def button_install(self):
        # 'l10n_mn_mrp_bom_substitution' болон 'l10n_mn_mrp_expense' модулиуд зэрэг суух гэж буйг шалгах, анхааруулга өгөх

        installing_l10n_mn_mrp_expense = False
        installing_l10n_mn_mrp_bom_substitution = False

        for module in self:
            if module.name == 'l10n_mn_mrp_expense':
                installing_l10n_mn_mrp_expense = True
            elif module.name == 'l10n_mn_mrp_bom_substitution':
                installing_l10n_mn_mrp_bom_substitution = True

        if installing_l10n_mn_mrp_bom_substitution:
            # l10n_mn_mrp_expense модуль суусан бол l10n_mn_mrp_bom_substitution модулийг суулгахгүй.
            self._cr.execute("SELECT id FROM ir_module_module WHERE name = 'l10n_mn_mrp_expense' AND state IN ('installed', 'to upgrade')")
            results = self._cr.dictfetchall()

            if results and len(results) > 0:
                raise ValidationError(_("If you want to install it, you must first UNINSTALL module named \'l10n_mn_mrp_expense\' !!!"))

        if installing_l10n_mn_mrp_expense:
            # l10n_mn_mrp_bom_substitution модуль суусан бол l10n_mn_mrp_expense модулийг суулгахгүй.
            self._cr.execute("SELECT id FROM ir_module_module WHERE name = 'l10n_mn_mrp_bom_substitution' AND state IN ('installed', 'to upgrade')")
            results = self._cr.dictfetchall()

            if results and len(results) > 0:
                raise ValidationError(_("If you want to install it, you must first UNINSTALL module named \'l10n_mn_mrp_bom_substitution\' !!!"))

        return super(Module, self).button_install()
