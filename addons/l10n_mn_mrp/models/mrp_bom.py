# -*- coding: utf-8 -*-

from datetime import datetime
from lxml import etree

from odoo import api, fields, models, _
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import UserError, ValidationError


class MrpBom(models.Model):
    _inherit = 'mrp.bom'

    tech_card_registered_date = fields.Date(default=lambda self: fields.datetime.now(), string='Technology Card Registered Date')
    tech_card_registered_user = fields.Many2one('res.users', default=lambda self: self.env.user, string='Technology Card Registered Employee')
    operating_instructions = fields.Text(string='Operating Instructions')
    end_product_view = fields.Text(string='End Product\'s View')
    check_bom_expiry_date = fields.Boolean(related='company_id.check_bom_expiry_date', readonly=True, string='Check BOM Expiry Date')
    start_date = fields.Date(string='Start Date')
    end_date = fields.Date(string='End Date')
    
    @api.model
    def filter_raw_materials(self, products, company_id):
        # ТЭМ-г шүүж буцаана.
        products = [pro for pro in products]
        raw_product_ids = self.env['mrp.bom.line'].search([('bom_id.company_id', '=', company_id.id), ('product_id', 'in', products)]).mapped('product_id')
        if self.env.get('bom.substitution', False) != False:
            raw_product_ids |= self.env['bom.substitution'].search([('company_id', '=', company_id.id), ('product_id', 'in', products)]).mapped('product_id')
        return list(set(raw_product_ids.mapped('id')))
    
    @api.model
    def filter_bom_products(self, products, company_id):
        # Эцсийн бүтээгдэхүүнийг шүүж буцаана.
        products = [pro for pro in products]
        product_template_ids = self.env['product.product'].search([('id', 'in', products)]).mapped('product_tmpl_id').mapped('id')
        bom_ids = self.env['mrp.bom'].search([('company_id', '=', company_id.id), '|', ('product_id', 'in', products), ('product_tmpl_id', 'in', product_template_ids)])
        bom_product_ids = self.env['product.product'].search([('id', 'in', products), '|', ('id', 'in', bom_ids.mapped('product_id').mapped('id')), ('product_tmpl_id', 'in', bom_ids.mapped('product_tmpl_id').mapped('id'))])
        return list(set(bom_product_ids.mapped('id')))

    @api.onchange('start_date', 'end_date')
    def check_expiry_date(self):
        for obj in self:
            if obj.start_date and obj.end_date and obj.start_date > obj.end_date:
                raise ValidationError(_('Please check expiry date !!!'))
            
    @api.model
    def _bom_find(self, product_tmpl=None, product=None, picking_type=None, company_id=False, date=False):
        """ 
            @Override: "Орцыг хугацаа шалгаж үйлдвэрлэнэ" тохиргоотой үед домайн нэмэхээр дарж override хийв.
            @note: Finds BoM for particular product, picking and company 
            @param date: "Орцыг хугацаа шалгаж үйлдвэрлэнэ" тохиргоотой үед үйлдвэрлэлийн огноог дамжуулна. Уг огноогоор хүчинтэй орц байгаа эсэхийг шалгана.
        """
        if product:
            if not product_tmpl:
                product_tmpl = product.product_tmpl_id
            domain = ['|', ('product_id', '=', product.id), '&', ('product_id', '=', False), ('product_tmpl_id', '=', product_tmpl.id)]
        elif product_tmpl:
            domain = [('product_tmpl_id', '=', product_tmpl.id)]
        else:
            # neither product nor template, makes no sense to search
            return False
        if picking_type:
            domain += ['|', ('picking_type_id', '=', picking_type.id), ('picking_type_id', '=', False)]
        if company_id or self.env.context.get('company_id'):
            domain = domain + [('company_id', '=', company_id or self.env.context.get('company_id'))]
            
        ########################## BEGIN CHANGE: "Орцыг хугацаа шалгаж үйлдвэрлэнэ" тохиргоотой үед домайн нэмэхээр дарж override хийв. ########################## 
        # "Орцыг хугацаа шалгаж үйлдвэрлэнэ" тохиргоотой үед орцны хугацааг шалгах
        company_id = company_id or self.env.context.get('company_id')
        if ((company_id and self.env['res.company'].browse(company_id).check_bom_expiry_date) or self.env.user.company_id.check_bom_expiry_date) and date:
            domain += ['&', '|', ('start_date', '=', False), ('start_date', '<=', date), '|', ('end_date', '>=', date), ('end_date', '=', False)]
        ########################## END CHANGE: "Орцыг хугацаа шалгаж үйлдвэрлэнэ" тохиргоотой үед домайн нэмэхээр дарж override хийв. ##########################
        
        # order to prioritize bom with product_id over the one without
        return self.search(domain, order='sequence, product_id', limit=1)
            
    
class MrpBomLine(models.Model):
    _inherit = 'mrp.bom.line'

    @api.model
    def fields_view_get(self, view_id=None, view_type='tree', toolbar=False, submenu=False):
        res = super(MrpBomLine, self).fields_view_get(view_id, view_type, toolbar=toolbar, submenu=submenu)
        root = etree.fromstring(res['arch'])
        root.set('create', 'false')
        root.set('import', 'false')
        root.set('edit', 'false')
        root.set('delete', 'false')
        res['arch'] = etree.tostring(root)

        return res