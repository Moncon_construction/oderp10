# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class MrpConfigSettings(models.TransientModel):
    _inherit = 'mrp.config.settings'

    group_update_mrp_routings = fields.Selection([
        (0, "No change mrp production routings"),
        (1, "Change mrp production routings")], "Change Routings",
        implied_group='l10n_mn_mrp.group_update_mrp_routings')
    group_update_mrp_materials = fields.Selection([
        (0, "No change mrp production raw materials"),
        (1, "Change mrp production raw materials")], "Change Raw Materials",
        implied_group='l10n_mn_mrp.group_update_mrp_materials')
    group_approve_mrp_cost = fields.Selection([
        (0, "No approved mrp production costs"),
        (1, "Approval costs for production orders")], "Approval Cost",
        implied_group='l10n_mn_mrp.group_approve_mrp_cost')
    
    module_l10n_mn_mrp_raw_material = fields.Selection([
        (0, "No raw material distribution"),
        (1, "Manufacture Raw Material Distribution")], "Raw Material Distribution",
        help='-This install the module l10n_mn_mrp_raw_material.')
    module_l10n_mn_mrp_account = fields.Selection([
        (0, "No mrp account"),
        (1, "Manufacturing Account")], "MRP Account",
        help='-This install the module l10n_mn_mrp_account.')
    module_l10n_mn_mrp_operations = fields.Selection([
        (0, "No mrp operations"),
        (1, "Records work order performance")], "Record Performance",
        help='-This install the module l10n_mn_mrp_operations.')
    module_l10n_mn_quality_mrp = fields.Selection([
        (0, "No quality control"),
        (1, "Manage quality control points, checks and measures")
        ], string="Quality",
        help='-This installs the module l10n_mn_quality_mrp.')
    module_l10n_mn_mrp_expense = fields.Boolean(string='Use MRP expense',
        help='-This installs the module l10n_mn_mrp_expense.')
    
    check_bom_expiry_date = fields.Boolean(related='company_id.check_bom_expiry_date', default=lambda x: x.company_id.check_bom_expiry_date, string='Check BOM Expiry Date')
    is_auto_produce = fields.Boolean(related='company_id.is_auto_produce', default=lambda x:x.company_id.is_auto_produce, string='Produce Automatically When Resource Insufficient')
    
    @api.onchange('group_mrp_routings')
    def _onchange_group_mrp_routings(self):
        """ Үйлдвэрлэлийн захиалгаар удирдах үед техникийн модуль
        ажлын захиалгын гүйцэтгэл хөтлөх,
        шугам дамжлага засварлах талбаруудыг хоосолж байна """
        for main in self:
            if main.group_mrp_routings == 0:
                main.module_l10n_mn_mrp_technic = 0
                main.group_update_mrp_routings = 0
                main.group_record_workorder_performance = 0
