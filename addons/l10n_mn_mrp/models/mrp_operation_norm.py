# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class MrpOperationNorm(models.Model):
    _name = 'mrp.operation.norm'
    _description = 'MRP Operation Norm'

    operation_norm_ids = fields.One2many('mrp.operation.norm.line', 'operation_norm_id', 'Operation Norm')
    name = fields.Char('Norm Name', required=True)
    workable_date = fields.Date('Workable Date')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id)

class MrpOperationNormLine(models.Model):
    _name = 'mrp.operation.norm.line'

    operation_norm_id = fields.Many2one('mrp.operation.norm', 'Operation Norm')
    code = fields.Char('Code', required=True)
    workcenter_id = fields.Many2one('mrp.workcenter', 'Workcenter', required=True)
    skill_level = fields.Selection([
                                ('1', '1'),
                                ('2', '2'),
                                ('3', '3'),
                                ('4', '4'),
                                ('5', '5')
                                ], required=True, string="Skill level")
    tariff = fields.Float('Tariff')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id)
    condition = fields.Selection([
                                ('normal', 'Normal'),
                                ('not_normal', 'Not Normal')], default='normal', string='Condition')
    norm_type = fields.Selection([
        ('time', 'Time'),
        ('quantity', 'Quantity')], default='quantity', string='Norm Type')
    _sql_constraints = [
        ('operation_norm_uniq', 'unique(code, company_id)', 'The code of the operation norm must be unique per company!')
    ]
