# -*- coding: utf-8 -*-
from datetime import timedelta
import math

from odoo import _, api, fields, models
import odoo.addons.decimal_precision as dp  
from odoo.addons.l10n_mn_base.models.tools import *
from odoo.exceptions import UserError, ValidationError
from odoo.tools.float_utils import float_round


class StockMove(models.Model):
    _inherit = 'stock.move'

    assigned_from_mrp = fields.Boolean(default=False, string='Whether It Is Already Assigned by MRP')


class MrpProduction(models.Model):
    _inherit = 'mrp.production'

    @api.model
    def _get_default_picking_type(self):
        default_warehouse = self.env.user.allowed_warehouses.ids
        picking_type_ids = self.env['stock.picking.type'].search([('warehouse_id', 'in', default_warehouse),('code','=','mrp_operation')], limit=1)
        return picking_type_ids

    @api.model
    def _get_default_location_src_id(self):
        return False

    @api.model
    def _get_default_location_dest_id(self):
        return False

    def _domain_picking_type(self):
        picking_type_ids = self.env['stock.picking.type'].search([('warehouse_id','in',self.env.user.allowed_warehouses.ids),('code','=','mrp_operation')])
        return [
            ('id', 'in', picking_type_ids.ids)
        ]

    date_planned_start_wo = fields.Datetime(string="Scheduled Start Date", index=True, copy=False)
    date_planned_finished_wo = fields.Datetime(compute='_compute_finished_date', string='Scheduled End Date', store=True, copy=False)
    standart_price = fields.Float('Standart Price')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirmed', 'Confirmed'),
        ('approved_cost', 'Approved Cost'),
        ('planned', 'Planned'),
        ('progress', 'In Progress'),
        ('done', 'Done'),
        ('cancel', 'Cancelled')], string='State',
        copy=False, default='draft', track_visibility='onchange')
    sale_order_id = fields.Many2one('sale.order', 'Sale Order')
    workcenter_ids = fields.One2many('mrp.production.workcenter', 'production_id', 'Production Workcenter')
    work_drawings = fields.One2many('mrp.work.drawings', 'production_id', 'Production Workcenter')
    update_routing = fields.Boolean('Update routing', compute='_get_mrp_conf')
    update_mrp_materials = fields.Boolean('Update MRP Materials', compute='_get_mrp_conf')
    picking_type_id = fields.Many2one('stock.picking.type', 'Picking Type', default= _get_default_picking_type, domain=_domain_picking_type, required=True)
    location_src_id = fields.Many2one(
        'stock.location', 'Raw Materials Location',
        default= _get_default_location_src_id,
        readonly=True, required=True,
        states={'confirmed': [('readonly', False)]})
    location_dest_id = fields.Many2one(
        'stock.location', 'Finished Products Location',
        default= _get_default_location_dest_id,
        readonly=True, required=True,
        states={'confirmed': [('readonly', False)]})
    sub_mo_count = fields.Integer('# Sub Manufacturing Order', compute='_compute_sub_mo_count')
    is_managed_by_workorder = fields.Boolean("Workorder manager settings", default=False, compute="_get_mrp_conf")
    button_workorder_visible = fields.Boolean("Button create workorder visibility", compute="show_button_create_workorder")
    price_unit = fields.Float(compute='get_finished_product_cost', digits=dp.get_precision('Product Price'), string='Unit Price')
    total_cost = fields.Float(compute='get_finished_product_cost', digits=dp.get_precision('Product Price'), string='Total Cost')

    @api.onchange('picking_type_id')
    def onchange_picking_type(self):
        self.location_src_id = self.picking_type_id.default_location_src_id.id
        self.location_dest_id = self.picking_type_id.default_location_dest_id.id

    @api.onchange('product_id', 'picking_type_id', 'company_id', 'date_planned_start')
    def onchange_product_id(self):
        """ Finds UoM of changed product. """
        if not self.product_id:
            self.bom_id = False
        else:
            bom = self.env['mrp.bom']._bom_find(product=self.product_id, picking_type=self.picking_type_id, company_id=self.company_id.id, date=self.date_planned_start)
            if bom.type == 'normal':
                self.bom_id = bom.id
            else:
                self.bom_id = False
            self.product_uom_id = self.product_id.uom_id.id
            return {'domain': {'product_uom_id': [('category_id', '=', self.product_id.uom_id.category_id.id)]}}

    @api.multi
    def _get_mrp_conf(self):
        config = self.env['mrp.config.settings'].search([('company_id', '=', self.company_id.id)], order='id DESC', limit=1)
        for production in self:
            if config.group_update_mrp_materials == 1:
                production.update_mrp_materials = True
            if config.group_update_mrp_routings == 1:
                production.update_routing = True
            if config.group_mrp_routings == 1:
                production.is_managed_by_workorder = True

    @api.depends("is_managed_by_workorder")
    def show_button_create_workorder(self):
        self.button_workorder_visible = False
        if self.is_managed_by_workorder:
           if self.state == 'planned' or self.state == 'progress':
               self.button_workorder_visible = True
           if self.workorder_count == 0:
               self.button_workorder_visible = True

    @api.multi
    @api.depends('workorder_ids.date_planned_finished')
    def _compute_finished_date(self):
        for production in self:
            date_finish = False
            for workorder in production.workorder_ids:
                if not date_finish:
                    date_finish = workorder.date_planned_finished
                if date_finish <= workorder.date_planned_finished:
                    enddate = workorder.date_planned_finished
                date_finish = enddate

            production.date_planned_finished_wo = date_finish

    @api.multi
    @api.depends('workorder_ids.state', 'move_finished_ids')
    def _get_produced_qty(self):
        # @Override: Оронгийн нарийвчлалыг тооцов
        for production in self:
            done_moves = production.move_finished_ids.filtered(lambda x: x.state != 'cancel' and x.product_id.id == production.product_id.id)
            qty_produced = sum(done_moves.mapped('quantity_done'))
            ######### START CHANGE: Оронгийн нарийвчлалыг тооцов ##########
            qty_produced = round(qty_produced, self.env['decimal.precision'].precision_get('Product Unit of Measure') or 2)
            ######### END   CHANGE: Оронгийн нарийвчлалыг тооцов ##########
            wo_done = True
            if any([x.state not in ('done', 'cancel') for x in production.workorder_ids]):
                wo_done = False
            production.check_to_done = done_moves and (qty_produced >= production.product_qty) and (production.state not in ('done', 'cancel')) and wo_done
            production.qty_produced = qty_produced
        return True
    
    @api.depends('move_finished_ids', 'move_finished_ids.price_unit', 'move_finished_ids.total_cost')
    def get_finished_product_cost(self):
        for obj in self:
            obj.price_unit = obj.move_finished_ids[0].price_unit if obj.move_finished_ids else 0
            obj.total_cost = sum(obj.mapped('move_finished_ids').mapped('total_cost'))
        
    def _compute_sub_mo_count(self):
        # Дэд үйлдвэрлэлийн тоог олох
        for obj in self:
            obj.sub_mo_count = len(self.env['mrp.production'].search([('origin', '=', obj.name)]))

    @api.multi
    def show_sub_production(self):
        # Дэд үйлдвэрлэл рүү үсрэх смарт товчноос дуудагдана
        production_ids = self.env['mrp.production'].search([('origin', 'in', tuple(obj.name for obj in self))])
        
        context = self._context.copy() or {}
        context['group_by'] = False
        context['search_default_todo'] = False
        
        return {
            'name': _('Sub Manufacturing Order'),
            'res_model': 'mrp.production',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'context': context,
            'views': [
                (self.env.ref('mrp.mrp_production_tree_view').id, 'tree'), 
                (False, 'form'),
            ],
            'target': 'current',
            'flags': {'search_view': True, 'action_buttons': True},
            'domain': [('id', 'in', production_ids.ids if production_ids else [])]
        }

    @api.multi
    def button_confirm(self):
        self.write({'state': 'confirmed'})

    @api.multi
    def button_plan(self):
        """ Create work orders. And probably do stuff, like things. """
        res = super(MrpProduction, self).button_plan()
        quantity = 0.0
        orders_to_plan = self.filtered(lambda order: order.routing_id and order.state == 'confirmed')
        for order in orders_to_plan:
            quantity = order.product_uom_id._compute_quantity(order.product_qty, order.bom_id.product_uom_id) / order.bom_id.product_qty
            boms, lines = order.bom_id.explode(order.product_id, quantity, picking_type=order.bom_id.picking_type_id)
            workorders = order._generate_workorders(boms)
            for workorder in workorders:
                workorder.write({'qty_producing': 0})
        res = orders_to_plan.write({'state': 'planned'})
        for production in self:
            planned_date = fields.Datetime.from_string(self.date_planned_start)
            production_values = {'date_planned_start_wo': False,
                                 'date_planned_finished_wo': False}
            values = {}
            for workorder in production.workorder_ids:
                name = workorder.production_id.name + '-' + workorder.workcenter_id.name 
                workorder.write({'qty_producing': 0,
                                 'name':name})
                date_start = planned_date
                date_end = date_start + timedelta(minutes=workorder.duration_expected)
                if not production_values['date_planned_start_wo']:
                    production_values['date_planned_start_wo'] = fields.Datetime.to_string(date_start)
                planned_date = date_end
                values['date_planned_start'] = fields.Datetime.to_string(date_start)
                values['date_planned_finished'] = fields.Datetime.to_string(date_end)
                for operation in self.routing_id.operation_ids:
                    if operation.compute_equals_order_qty == True and workorder.workcenter_id == operation.workcenter_id:
                        quantity = self.product_qty
                        workorder.write({'qty_workorder': quantity})
                    if operation.compute_equals_always_one == True and workorder.workcenter_id == operation.workcenter_id:
                        quantity = 1.0
                        workorder.write({'qty_workorder': quantity})
                    if operation.compute_equals_workcenter == True and workorder.workcenter_id == operation.workcenter_id:
                        quantity = operation.workcenter_quantity * self.product_qty
                        workorder.write({'qty_workorder': quantity})
                    cycle_number = math.ceil(quantity / operation.workcenter_id.capacity)  # TODO: float_round UP
                    duration_expected = (operation.workcenter_id.time_start + 
                                     operation.workcenter_id.time_stop + 
                                     cycle_number * operation.time_cycle * 100.0 / operation.workcenter_id.time_efficiency)
                    workorder.write({'duration_expected':duration_expected})
                workorder.write(values)
            production_values['date_planned_finished_wo'] = fields.Datetime.to_string(planned_date)
            production.write(production_values)
        return res

    @api.multi
    def button_unplan(self):
        for production in self:
            production.workorder_ids.unlink()
        self.write({'date_planned_start_wo': False,
                    'date_planned_finished_wo': False,
                    'state': 'confirmed'})

    '''
            Ажлын захиалгыг шугам дахь
        тохиргооноос утга авч үүсгэх функц
    '''

    @api.multi
    def button_create_workorder(self):
        """ Create work orders. And probably do stuff, like things. """
        orders_to_plan = self.filtered(lambda order: order.routing_id and order.state == 'confirmed')
        for order in orders_to_plan:
            quantity = order.product_uom_id._compute_quantity(order.product_qty, order.bom_id.product_uom_id) / order.bom_id.product_qty
            boms, lines = order.bom_id.explode(order.product_id, quantity, picking_type=order.bom_id.picking_type_id)
            order._generate_workorders(boms)
 
    @api.multi
    def _create_workcenter(self):
        for route in self.routing_id:
            for operation in route.operation_ids:
                self.env['mrp.production.workcenter'].create({
                    'name': operation.name,
                    'workcenter_id': operation.workcenter_id.id,
                    'production_id': self.id,
                    'routing_qty': operation.cycle_qty,
                    'workorder_qty': operation.cycle_qty * self.product_qty,
                })

    @api.multi
    def write(self, vals):
        res = super(MrpProduction, self).write(vals)
         
        if 'product_id' in vals or 'bom_id' in vals:
            self.change_bom()
                
        return res

    @api.multi
    def change_bom(self):
        for mrp in self:
            moves = mrp.move_raw_ids | mrp.move_finished_ids
            move_ids = str(moves.ids)[1:len(str(moves.ids))-1] if moves and str(moves.ids) != "[]" else False
            if move_ids:
                qry = """
                    /* 1. Агуулахын хөдөлгөөнтэй холбоотой quant-уудыг устгах */
                    DELETE FROM stock_quant WHERE id IN
                    (
                        SELECT quant_id FROM stock_quant_move_rel WHERE move_id IN (%s)
                    );
                     
                    /* 2. Холбоотой агуулахын хөдөлгөөнүүдийг устгах */
                    DELETE FROM stock_move WHERE id IN (%s);
                    """ %(move_ids, move_ids)
                self._cr.execute(qry)
            mrp._generate_moves()
        
    @api.multi
    def cancel_mrp_by_force(self):
        self.ensure_one()
        
        # Мөчлөг түгжигдсэн эсэхийг шалгах
        for mrp_id in self:
            lock_date = mrp_id.company_id.period_lock_date or False if not self.env.user.has_group('account.group_account_manager') else mrp_id.company_id.fiscalyear_lock_date or False
            if lock_date and lock_date > mrp_id.date_planned_start:
                raise ValidationError(_(u"Accounting date is locked until '%s' !!!") % lock_date)
    
        move_raw_ids = self.move_raw_ids
        
        if self.company_id.availability_compute_method != 'stock_move':
            move_raw_ids = False
            
            # Үйлдвэрлэлийг цуцлахад бэлэн болгож холбоотой журнал, журналын мөрүүд, quant ...-уудыг устгах
            Quant = self.env['stock.quant'].sudo()
            Move = self.env['stock.move'].sudo()
    
            # Хангасан материалуудаар гүйлгэж тухайн материалын үйлдвэрлэл хийхэд орсон тоо хэмжээг буцаах /цуцласан тоогоор/
            for move in self.move_raw_ids:
                production_loc = move.location_dest_id
                internal_loc = move.location_id
                internal_move, internal_quant = False, False
                product = move.product_id.sudo()
                product_qty = move.product_uom_qty
    
                # 1. Хангасан материалын бодит байрлалаас гарсан stock.quant-г олох /production_quant/
                qry = """SELECT sqml.quant_id AS id
                        FROM stock_quant_move_rel sqml
                        RIGHT JOIN stock_quant sq ON sq.id = sqml.quant_id
                        WHERE sqml.move_id = %s AND sq.location_id = %s AND sq.product_id = %s
                        ORDER BY sq.create_date DESC
                        LIMIT 1""" % (move.id, move.location_dest_id.id, product.id)
                self._cr.execute(qry)
                result = self._cr.dictfetchall()
                if not (result and len(result) > 0 and result[0]['id']):
                    continue
                production_quant = Quant.browse(result[0]['id'])
    
                # 2. Хангасан материалын бодит байрлалаас гарсан quant-н бодит байрлал дээрх холбоотой хөдөлгөөнийг олох
                qry = """SELECT sqml.move_id AS id
                        FROM stock_quant_move_rel sqml
                        RIGHT JOIN stock_quant sq ON sq.id = sqml.quant_id
                        RIGHT JOIN stock_move sm ON sm.id = sqml.move_id
                        WHERE sqml.quant_id = %s AND sm.location_dest_id = %s AND sm.product_id = %s
                        ORDER BY sq.create_date DESC
                        LIMIT 1""" % (production_quant.id, internal_loc.id, product.id)
                self._cr.execute(qry)
                result = self._cr.dictfetchall()
                if result and len(result) > 0 and result[0]['id']:
                    internal_move = Move.browse(result[0]['id'])
    
                # 3. Хангасан материалын бодит байрлал руу орсон quant-г олох
                qry = """SELECT id FROM stock_quant WHERE location_id = %s AND product_id = %s ORDER BY create_date DESC LIMIT 1""" % (internal_loc.id, product.id)
                self._cr.execute(qry)
                result = self._cr.dictfetchall()
                if result and len(result) > 0 and result[0]['id']:
                    internal_quant = Quant.browse(result[0]['id'])
    
                # 4. Хангасан материалын бодит байрлал руу орсон quant-г цуцласан тоогоор нэмэгдүүлэх
                if internal_quant:
                    # 4.1. Хангасан материалын бодит байрлал руу орсон quant олдсон бол тухайн quant дээрээ нэмэгдүүлэх
                    internal_quant.update({'qty': (internal_quant.qty + product_qty)})
                else:
                    # Дотоод байрлал руу хийгдсэн ямар нэг хөдөлгөөн олдохгүй бол 'MRP FORCE CANCELED: ' нэр бүхий хөдөлгөөн үүсгэх
                    if not internal_move:
                        bom_line = move.bom_line_id
                        production = self
                        original_quantity = (production.product_qty - production.qty_produced) or 1.0
                        internal_move = Move.create({
                            'sequence': bom_line.sequence or False,
                            'name': u"MRP FORCE CANCELED: %s" %production.name,
                            'date': production.date_planned_start,
                            'date_expected': production.date_planned_start,
                            'bom_line_id': bom_line.id or False,
                            'substitutabled_product_id': move.substitutabled_product_id.id if move.substitutabled_product_id else False,
                            'product_id': product.id,
                            'product_uom_qty': product_qty,
                            'quantity_done_store': product_qty,
                            'product_uom': product.uom_id.id,
                            'location_id': production_loc.id,
                            'location_dest_id': internal_loc.id,
                            'company_id': production.company_id.id,
                            'operation_id': bom_line.operation_id.id or False,
                            'price_unit': move.get_price_unit(),
                            'procure_method': 'make_to_stock',
                            'origin': production.name,
                            'warehouse_id': production_loc.get_warehouse().id,
                            'group_id': production.procurement_group_id.id,
                            'propagate': production.propagate,
                            'unit_factor': product_qty / original_quantity,
                        })
                        internal_move.action_done()
    
                    # 4.2. Хангасан материалын бодит байрлал руу орсон quant олдохгүй бол бодит байрлал дах quant үүсгэх
                    # Тухайн quant-г бодит байрлал руу орсон хөдөлгөөнтэй холбож өгөх
                    new_qnt = Quant.create({
                        'product_id': product.id,
                        'location_id': internal_loc.id,
                        'qty': float_round(product_qty, precision_rounding=internal_move.product_uom.rounding),
                        'cost': move.get_price_unit(),
                        'history_ids': [[6, 0, [internal_move.id]]],
                        'in_date': move.date,
                        'company_id': move.company_id.id,
                        'lot_id': False,
                        'owner_id': False,
                        'package_id': False,
                    })
    
                # 5. Хөдөлгөөнтэй холбоотой журналын бичилтүүдийг устгах
                qry = """
                    /* 5.1. Агуулахын хөдөлгөөний журналыг устгах */
                    DELETE FROM account_move WHERE id IN
                    (
                        SELECT move_id FROM account_move_line WHERE stock_move_id = %s
                    );
    
                    /* 5.2. Агуулахын хөдөлгөөнөөс үүссэн лотуудыг устгах */
                    DELETE FROM stock_move_lots WHERE move_id = %s;
                """ %(move.id, move.id)
                self._cr.execute(qry)
    
                # 6. Хангасан материалын хөдөлгөөнийг цуцлаж устгах
                move.write({'quantity_done_store': 0, 'state': 'cancel'})
                move.action_cancel()

        # 7.1. Хэрвээ quant-с үлдэгдлээ тооцох бол зөвхөн эцсийн бүтээгдэхүүн дэх хөдөлгөөнийг цуцласан төлөвт оруулж, холбоотой quant, журналын бичилтийг устгах
        # 7.2. Хэрвээ move-с үлдэгдлээ тооцох бол холбоотой бүх хөдөлгөөнийг цуцласан төлөвт оруулж, холбоотой quant, журналын бичилтийг устгах
        if self.move_finished_ids or move_raw_ids:
            move_ids = self.move_finished_ids
            if move_raw_ids:
                move_ids |= move_raw_ids
            move_ids = str(move_ids.ids).strip('[]')
            qry = """
                /* 7.1. Агуулахын хөдөлгөөнтэй холбоотой quant-уудыг устгах */
                DELETE FROM stock_quant WHERE id IN
                (
                    SELECT quant_id FROM stock_quant_move_rel WHERE move_id IN (%s)
                );

                /* 7.2. Агуулахын хөдөлгөөний журналыг устгах */
                DELETE FROM account_move WHERE id IN
                (
                    SELECT move_id FROM account_move_line WHERE stock_move_id IN (%s)
                );

                /* 7.3. Агуулахын хөдөлгөөнөөс үүссэн лотуудыг устгах */
                DELETE FROM stock_move_lots WHERE move_id IN (%s);

                /* 7.4. Дамжлагуудыг устгах */
                DELETE FROM mrp_workorder WHERE production_id IN (%s);

                UPDATE stock_move SET state = 'cancel', quantity_done_store = 0 WHERE id IN (%s);

            """ %(move_ids, move_ids, move_ids, str(self.ids)[1:len(str(self.ids))-1], move_ids)
            self._cr.execute(qry)

        # Үйлдвэрлэлийг цуцлах
        self.action_cancel()

    @api.multi
    def set_draft(self):
        self.change_bom()
        
        # Үйлдвэрлэлийг ноорог төлөвт оруулах/Бэлэн байдлыг шалгах
        self.write({'state': 'draft'})
        for obj in self:
            obj._compute_availability()

    @api.multi  
    def build_mrp_after_query_creation(self):
        # Query-р үүсгэсний дараа бусад шаардлагатай өгөгдлүүдийг энэ функцаар оноох: Хурдад аль болох бага нөлөөлөхөөр кодоо бичнэ үү.
        return self
        
    @api.model
    def create_mrp_by_query(self, product_qty, company_id, bom_id, product_id, origin, picking_type_id, location_src_id, location_dest_id, produce_date):
        mrp_move_ids = []
        uid = self.env.uid
        date = fields.Datetime.now()
        
        # Нэгж үнэ /price_unit/-г хөдөлгөөнийг хийгдсэн болгох үед буюу action_done() функц дээр олгох тул энэд орхив.
        self._cr.execute("""
            INSERT INTO mrp_production (create_uid, create_date, write_uid, write_date, company_id, user_id, name, 
                product_id, product_uom_id, product_qty, bom_id, origin, picking_type_id,  
                location_src_id, location_dest_id, date_planned_start, date_planned_finished, 
                state, cost_type, availability, propagate) 
            VALUES (%s, '%s', %s, '%s', %s, %s, '%s', 
                %s, %s, %s, %s, '%s', %s, 
                %s, %s, '%s', '%s', 
                '%s', '%s', '%s', False)
            RETURNING id
        """ % (uid, date, uid, date, company_id.id, uid, self.env['ir.sequence'].next_by_code('mrp.production') or _('New'), 
               product_id.id, product_id.uom_id.id, product_qty, bom_id.id, origin, picking_type_id.id,
               location_src_id.id, location_dest_id.id, produce_date, produce_date, 
               'draft', 'unit_cost', 'waiting'))
        
        auto_poduction_id = self.env.cr.fetchone()[0]
        
        new_production = self.env['mrp.production'].browse(auto_poduction_id) 
        
        factor = new_production.product_uom_id._compute_quantity(new_production.product_qty, new_production.bom_id.product_uom_id) / new_production.bom_id.product_qty
        boms, lines = new_production.bom_id.explode(new_production.product_id, factor, picking_type=new_production.bom_id.picking_type_id)
        
        # Нэгж үнэ /price_unit/-г хөдөлгөөнийг хийгдсэн болгох үед буюу action_done() функц дээр олгох тул энэд орхив.
        q = """INSERT INTO stock_move (create_uid, create_date, write_uid, write_date, sequence,
                            date, date_expected, name, bom_line_id, 
                            product_id, product_uom_qty, product_qty, product_uom, 
                            state, procure_method, company_id, 
                            location_id, location_dest_id, raw_material_production_id,
                            origin, propagate, warehouse_id, 
                            group_id, unit_factor) 
                        VALUES"""
        values = []
        for bom_line, line_data in lines:
            if bom_line.product_id.type == 'service':
                continue
            original_quantity = (new_production.product_qty - new_production.qty_produced) or 1.0
            values.append(
                    """(%s, '%s', %s, '%s', '%s', 
                            '%s', '%s', '%s', %s, 
                            %s, %s, %s, %s,
                            '%s', '%s', %s,
                            %s, %s, %s,
                            '%s', %s, %s, 
                            %s, %s)
                    """ % (uid, date, uid, date, bom_line.sequence,
                           new_production.date_planned_start, new_production.date_planned_start, get_qry_value(new_production.name), bom_line.id,
                           bom_line.product_id.id, line_data['qty'], line_data['qty'], bom_line.product_uom_id.id, 
                           'draft', 'make_to_stock', company_id.id, 
                           new_production.location_src_id.id, new_production.product_id.property_stock_production.id, new_production.id,
                           new_production.name, new_production.propagate, new_production.location_src_id.get_warehouse().id, 
                           new_production.procurement_group_id.id or 'Null', line_data['qty'] / original_quantity)
            )

        q += ", ".join(values)
        q += ' RETURNING id'
        self._cr.execute(q)
        raw_move_ids = self.env.cr.fetchall()
        for raw in raw_move_ids:
            mrp_move_ids.append(raw[0])
            
        self._cr.execute("""INSERT INTO stock_move (create_uid, create_date, write_uid, write_date,
                            date, date_expected, name, product_id, product_uom_qty, product_qty, product_uom, 
                            state, procure_method, company_id, location_id, location_dest_id, production_id,
                            origin, propagate) 
                        values (%s, '%s', %s, '%s', 
                            '%s', '%s', '%s', %s, %s, %s, %s, 
                            '%s', '%s', %s, %s, %s, %s,
                            '%s', %s
                        )
        RETURNING id
        """ % (uid, date, uid, date,
               new_production.date_planned_start, new_production.date_planned_start, new_production.name, new_production.product_id.id, new_production.product_qty, new_production.product_qty, new_production.product_uom_id.id, 
               'draft', 'make_to_stock', company_id.id, new_production.product_id.property_stock_production.id, new_production.location_dest_id.id, new_production.id,
               new_production.name, new_production.propagate))
        
        mrp_move_ids.append(self.env.cr.fetchone()[0])
        new_production.build_mrp_after_query_creation()
        
        return mrp_move_ids, auto_poduction_id
    
    @api.multi
    def action_assign(self):
        #Хэрэв үйлдвэрлэлийн тохиргоон дээр "Орц хүрэлцэхгүй үед зөвшөөрөгдсөн орцыг автоматаар үйлдвэрлэнэ" тохиргоо "Тийм" бол,
        #Үйлдвэрлэлийн бэлэн байдлыг шалгахад орцон дахь аль нэг үйлдвэрлэгддэг бараа үлдэгдэл хүрэлцэхгүй бол,
        #Тэр үлдэгдэл хүрэхгүй байгаа бараа нь "Нөөц хүрэлцэхгүй бол автомат үйлдвэрлэ" гэсэн чектэй бол автомат үйлдвэрлэж үлдэгдэлтэй болгоно.
        ready_move_ids = self.env['stock.move']
        
        if self.env.user.company_id.is_auto_produce:
            for production in self.filtered(lambda x: x.qty_produced == 0):
                #юуг хэдийг автомат үйлдвэрлэх?
                self._cr.execute("""
                    SELECT m.id FROM stock_move m, product_product p, product_template t 
                    WHERE m.raw_material_production_id = %s AND m.product_id = p.id AND p.product_tmpl_id = t.id
                        AND t.auto_produce = True AND m.state NOT IN ('assigned', 'done')
                        AND (assigned_from_mrp = False OR assigned_from_mrp IS NULL)
                """ % production.id)
                result = self.env.cr.fetchall()
                auto_produce_move_ids = []
                for move_id in result:
                    auto_produce_move_ids.append(move_id[0])
                auto_produce_moves = self.env['stock.move'].browse(auto_produce_move_ids)
                confirm_move_ids = []
                confirm_production_ids = []
                
                for move in auto_produce_moves:
                    rounded_quantity_available = round(move.quantity_available, self.env['decimal.precision'].precision_get('Product Unit of Measure') or 2)
                    rounded_product_uom_qty = round(move.product_uom_qty, self.env['decimal.precision'].precision_get('Product Unit of Measure') or 2)
                    if rounded_quantity_available < rounded_product_uom_qty:
                        bom_id = self.env['mrp.bom']._bom_find(product=move.product_id, date=production.date_planned_start)
                        if not bom_id:
                            raise UserError(_('There is no BOM in this product: %s %s') % (move.product_id.default_code, move.product_id.name))
                        
                        product_qty = move.product_uom_qty - max(move.quantity_available, 0)
                        company_id = production.company_id
                        product_id = move.product_id
                        origin = production.name
                        picking_type_id = production.picking_type_id
                        location_src_id = production.location_src_id
                        location_dest_id = production.location_src_id
                        produce_date = production.date_planned_start
                            
                        mrp_move_ids, auto_poduction_id = self.create_mrp_by_query(product_qty, company_id, bom_id, product_id, origin, picking_type_id, location_src_id, location_dest_id, produce_date)
                        confirm_move_ids.extend(mrp_move_ids or [])
                        confirm_production_ids.append(auto_poduction_id)
                        
                        ready_move_ids |= move

                confirm_moves = self.env['stock.move'].browse(confirm_move_ids)
                confirm_moves.action_confirm()
                
                confirm_productions = self.env['mrp.production'].browse(confirm_production_ids)
                confirm_productions.action_assign()
                confirm_productions.button_confirm()
                
                for production in confirm_productions:
                    context = {"active_model": "mrp.production", "active_ids": [production.id], "active_id": production.id}
                    product_consume = self.env['mrp.product.produce'].with_context(context).create({'product_qty': production.product_qty})
                    product_consume.do_produce()
                
                confirm_productions.post_inventory()
                confirm_productions.write({'state': 'done', 'date_finished': fields.Datetime.now()})
                
                auto_produce_moves.action_assign()

        ready_move_ids.write({'assigned_from_mrp': True})

        return super(MrpProduction, self).action_assign()
    
    @api.multi
    def action_cancel(self):
        (self.mapped('move_raw_ids') | self.mapped('move_finished_ids')).write({'quantity_done_store': 0})
        res = super(MrpProduction, self).action_cancel()
        
        # Дэд үйлдвэрлэлийг цуцлаж/устгах
        sub_productions = self.env['mrp.production'].search([('origin', 'in', [obj.name for obj in self])])
        if sub_productions:
            for sub_production in sub_productions.filtered(lambda x: x.state == 'done'):
                sub_production.cancel_mrp_by_force()
            for sub_production in sub_productions.filtered(lambda x: x.state != 'done'):
                sub_production.action_cancel()
            sub_productions.unlink()
        
        return res

    @api.multi
    def set_draft_in_bulk(self):
        #  Үйлдвэрлэл бөөнөөр нь цуцлаад НООРОГЛОХ
        for obj in self.filtered(lambda x: x.state != 'draft'):
            if obj.state == 'done':
                obj.cancel_mrp_by_force()
            elif obj.state != 'cancel':
                obj.action_cancel()
            obj.set_draft()
            
    @api.multi
    def cancel_in_bulk(self):
        #  Үйлдвэрлэл бөөнөөр нь ЦУЦЛАХ
        self = self.filtered(lambda x: x.state != 'cancel')
        mother_productions = self.filtered(lambda x: not x.origin)
        sub_productions = self.filtered(lambda x: x.origin and x.origin not in [mother.name for mother in mother_productions])
        
        #  Эх үйлдвэрлэлүүдийг цуцлах: Дэд үйлдвэрлэлүүд нь дагаад цуцлагдаж/устана.
        for obj in mother_productions.filtered(lambda x: x.state == 'done'):
            obj.cancel_mrp_by_force()
        for obj in mother_productions.filtered(lambda x: x.state != 'done'):
            obj.action_cancel()
            
        #  Бөөнөөр нь цуцлах гэж буй эх үйлдвэрлэлээс өөр эх үйлдвэрлэл бүхий дэд үйлдвэрлэлүүдийг цуцлах.
        for obj in sub_productions.filtered(lambda x: x.state == 'done'):
            obj.cancel_mrp_by_force()
        for obj in sub_productions.filtered(lambda x: x.state != 'done'):
            obj.action_cancel()
            
            
class MrpProductionWorkDrawings(models.Model):
    """ Manufacturing Workcenter """
    _name = 'mrp.work.drawings'
    _description = 'Manufacturing Order Workcenter'

    production_id = fields.Many2one('mrp.production', 'Manufacturing Order')
    file_name = fields.Char('File name')
    file_attach = fields.Binary('File')

class MrpProductionWorkcenter(models.Model):
    """ Manufacturing Workcenter """
    _name = 'mrp.production.workcenter'
    _description = 'Manufacturing Order Workcenter'

    workcenter_id = fields.Many2one('mrp.workcenter', 'Workcenter')
    name = fields.Char('Name', related='workcenter_id.name')
    routing_qty = fields.Float('Workcenter qty')
    workorder_qty = fields.Float('Workorder qty')
    production_id = fields.Many2one('mrp.production', 'Manufacturing Order')
    button_clicked = fields.Boolean(default=False, string="Button clicked")
    duration_expected = fields.Float('Duration Expected')
    
    '''
            Ажлын захиалга үүсгэх функц
    '''

    @api.one
    def add_workorder(self):
        workorder_obj = self.env['mrp.workorder']
        if self.name == False :
            name = self.production_id.name + '-' + self.workcenter_id.name
        else :
            name = self.production_id.name + '-' + self.workcenter_id.name
        if self.duration_expected != 0.0 or self.workorder_qty != 0.0:
            workorder = workorder_obj.create({
                'name': name,
                'production_id': self.production_id.id,
                'workcenter_id': self.workcenter_id.id,
                'state': 'pending',
                'qty_workorder': self.workorder_qty,
                'duration_expected' : self.duration_expected,
             })
            for record in self:
                record.write({
                    'button_clicked': True,
                })
        if self.duration_expected == 0.0 or self.workorder_qty == 0.0:
            raise UserError(_('Enter a different value of 0'))
