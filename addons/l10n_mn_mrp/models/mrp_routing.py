# -*- coding: utf-8 -*-
from odoo import api, fields, models

class MrpRoutingWorkcenter(models.Model):
    _inherit = 'mrp.routing.workcenter'

    time_mode = fields.Selection([
        ('auto', 'Compute based on real time'),
        ('manual', 'Set duration manually'),
        ('standard_time', 'Compute based on standard time')], string='Duration Computation',
        default='auto')
    cycle_qty = fields.Float('Cycle Quantity')
    time_cycle_standard = fields.Float('Cycle Standard Duration', compute="_compute_standard_time")
    compute_equals_order_qty = fields.Boolean('Order Qty')
    compute_equals_always_one = fields.Boolean('Always 1')
    compute_equals_workcenter = fields.Boolean('Compute Workcenter')
    workcenter_quantity = fields.Float('Workcenter Quantity')
    '''
        Нэгж цикл дэх хугацааг тооцоологч
    '''
    @api.multi
    def _compute_standard_time(self):
        for line in self:
            line.time_cycle_standard = line.cycle_qty * line.workcenter_id.unit_duration

    @api.multi
    @api.depends('time_cycle_manual', 'time_mode', 'workorder_ids')
    def _compute_time_cycle(self):
        manual_ops = self.filtered(lambda operation: operation.time_mode == 'manual')
        standards = self.filtered(lambda operation: operation.time_mode == 'standard_time')
        for operation in manual_ops:
            operation.time_cycle = operation.time_cycle_manual
        for operation in standards:
            operation.time_cycle = operation.time_cycle_standard
        for operation in self - manual_ops - standards:
            data = self.env['mrp.workorder'].read_group([
                ('operation_id', '=', operation.id),
                ('state', '=', 'done')], ['operation_id', 'duration', 'qty_produced'], ['operation_id'],
                limit=operation.time_mode_batch)
            count_data = dict((item['operation_id'][0], (item['duration'], item['qty_produced'])) for item in data)
            if count_data.get(operation.id) and count_data[operation.id][1]:
                operation.time_cycle = count_data[operation.id][0] / count_data[operation.id][1]
            else:
                operation.time_cycle = operation.time_cycle_manual
