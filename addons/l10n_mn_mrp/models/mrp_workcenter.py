# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models

class MrpWorkcenter(models.Model):
    _inherit = 'mrp.workcenter'

    workcenter_uom = fields.Many2one('product.uom', 'Unit of Measure')
    unit_duration = fields.Float('Time for 1 cycle', help="Time in hours for doing one unit.")
    unit_cost = fields.Float('Unit cost',help="Specify Cost of Work Center per cycle.")
    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic Account')
