# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import timedelta
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools import float_compare, float_round
from odoo.addons import decimal_precision as dp  # @UnresolvedImport


class MrpWorkorder(models.Model):
    _inherit = 'mrp.workorder'

    product_qty = fields.Float('Original Production Quantity', readonly=True, related='production_id.product_qty')
    qty_workorder = fields.Float(
        'Original Workorder Quantity',
        default=1.0, digits=dp.get_precision('Product Unit of Measure'),
        readonly=True, required=True,
        states={'confirmed': [('readonly', False)]})
    picking_type_id = fields.Many2one('stock.picking.type',required=False)
    company_id = fields.Many2one('res.company', "Company", states={'draft': [('readonly', False)]}, track_visibility='onchange', default=lambda self: self.env['res.company']._company_default_get())
    button_plan = fields.Char('test')
    qty_producing = fields.Float('Currently Produced Quantity', default=0.0)
    mrp_operations = fields.Boolean('Update MRP Workorder', compute='_get_mrp_conf')  # Одоогын тоо хэмжээ талбарыг АЗ-н гүйцэтгэл хөтлөх эсэхээс хамааруулан идэвхгүй болгох тооцоолол хйих талбар
    product_id = fields.Many2one(
        'product.product', 'Production',
        related='production_id.product_id', readonly=True,
        help='Technical: used in views only.')  # Орчуулгад зориулж дахин тодорхойлов.
    qty_produced = fields.Float('Cicle', default=0.0,
        readonly=True,
        digits=dp.get_precision('Product Unit of Measure'),
        help="The number of products already handled by this work order")
    
    '''АЗ-н гүйцэтгэл хөтлөх гэсэн сонголтыг мэдэх'''
    @api.multi
    def _get_mrp_conf(self):
        for workorder in self:
            config = self.env['mrp.config.settings'].search([('company_id', '=', workorder.company_id.id)], order='id DESC', limit=1)
            if config.module_l10n_mn_mrp_operations == 1:
                workorder.mrp_operations = False
            else:
                workorder.mrp_operations = True

    @api.onchange('workcenter_id', 'production_id')
    def onchange_workcenter_id(self):
        if self.workcenter_id and self.production_id:
            self.name = _(u'%s / %s') % (self.production_id.name, self.workcenter_id.name)

    @api.onchange('date_planned_start', 'duration_expected')
    def onchange_date_planned_start(self):
        if self.date_planned_start or self.duration_expected:
            planned_date = fields.Datetime.from_string(self.date_planned_start)
            self.date_planned_finished = planned_date + timedelta(minutes=self.duration_expected)

    @api.multi
    def record_production(self):
        self.ensure_one()
        conf_mod = self.env['mrp.config.settings'].sudo().search([('company_id', '=', self.company_id.id)], order="id desc", limit=1)
        if not conf_mod.module_l10n_mn_mrp_operations:
            self.qty_producing = self.qty_workorder
        return super(MrpWorkorder, self).record_production()
