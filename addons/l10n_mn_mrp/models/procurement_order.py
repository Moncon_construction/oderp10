# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models
# TODO: Тайлбар бичих
class ProcrementOrder(models.Model):
    _inherit = 'procurement.order'

    def _prepare_mo_vals(self, bom):
        res = super(ProcrementOrder, self)._prepare_mo_vals(bom)
        res['sale_order_id'] = self.move_dest_id.procurement_id.sale_line_id.order_id.id
        return res
