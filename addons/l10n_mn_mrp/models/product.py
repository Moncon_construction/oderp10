# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    auto_produce = fields.Boolean('Produce Automatically')
    show_auto_product = fields.Boolean('Is Auto Produce', related='company_id.is_auto_produce', readonly=True)
    has_bom = fields.Selection([('has', 'Has BOM'), ('has_not', 'Hasn\'t BOM')], default='has_not', string='Has BOM ?')
    
