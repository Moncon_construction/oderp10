# -*- coding: utf-8 -*-

from odoo import api, fields, models


class ResCompany(models.Model):
    _inherit = 'res.company'

    is_auto_produce = fields.Boolean(default=False, string='Produce Automatically When Resource Insufficient')
    check_bom_expiry_date = fields.Boolean(default=False, string='Check BOM Expiry Date')