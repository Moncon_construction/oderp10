# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _

class StockMove(models.Model):
    _inherit = 'stock.move'

    @api.multi
    def check_origin_compute(self):
        is_production = False
        for line in self:
            if line.production_id or line.raw_material_production_id:
                is_production = True
            line.check_origin = is_production

    expense_qty = fields.Float('Expense Qty', help='ТЭМ шаардсан тоо хэмжээг шалгахын тулд хэрэглэгдэнэ.')
    check_origin = fields.Boolean('Check Origin', compute="check_origin_compute")

    @api.multi
    def move_validate(self):
        # @Override: Үйлдвэрлэлийн огноогоор хөдөлгөөний огноог өөрчлөв.
        # core-н move_validate() функцэд хөдөлгөөний огнооог datetime.now()-р write хийдэг мөр байгаа.
        moves_todo = super(StockMove, self).move_validate()
        for move in moves_todo.filtered(lambda x: (x.production_id or x.raw_material_production_id) and not x.scrapped):
            production = move.production_id or move.raw_material_production_id
            move.write({'date': production.sudo().date_planned_start})
        return moves_todo
    
    @api.multi
    def stat_button_mrp(self):
        #   ухаалаг даруул дээр дарахад дуудагдах функц
        # trip = []
        for line in self:
            res = self.env['mrp.production'].search(['|',('id', '=', line.production_id.id),('id','=',line.raw_material_production_id.id)])
            # for line in res:
            #     trip.append(line.id)
            return {
                'type': 'ir.actions.act_window',
                'name': _('Mrp Production'),
                'res_model': 'mrp.production',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'view_id': False,
                'domain': [('id', 'in', res.ids)],
            }

    def update_product_price(self):
        # ТЭМ-н өртгийг шууд бараанаас бус функцаас авдаг болгов.
        for move_id in self.filtered(lambda move: move.location_dest_id.usage == 'production' and move.raw_material_production_id):
            move_id.write({'price_unit': move_id.get_product_standard_price()})
        
        # ЭЦСИЙН БҮТЭЭГДЭХҮҮНий өртгийг ТЭМ-н өртгүүдийн нийлбэрээр тооцдог болгов
        for move_id in self.filtered(lambda move: move.location_id.usage == 'production' and move.production_id):
            production_id = move_id.production_id.sudo()
            sum_cost = sum([move_raw_id.price_unit * move_raw_id.product_uom_qty for move_raw_id in production_id.move_raw_ids]) if production_id.move_raw_ids else 0
            cost = sum_cost / (production_id.product_qty or 1)
            production_id.move_finished_ids.write({'price_unit': cost})
            
        super(StockMove, self).update_product_price()
