# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError
import math


class ChangeProductionQty(models.TransientModel):
    _inherit = 'change.production.qty'

    mo_id = fields.Many2one('mrp.production', 'Manufacturing Order', required=True, ondelete='cascade') # @Override: ADD ondelete='cascade' for direct delete mrp_production

