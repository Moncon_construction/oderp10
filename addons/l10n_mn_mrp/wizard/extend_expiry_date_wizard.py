# -*- coding: utf-8 -*-

from datetime import datetime
from dateutil.relativedelta import relativedelta
from lxml import etree

from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError, ValidationError
from odoo.osv.orm import setup_modifiers
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT


class ExtendExpiryDate(models.TransientModel):
    _name = 'extend.expiry.date'

    company_id = fields.Many2one('res.company', default=lambda self: self.env.user.company_id, string='Company')
    extend_by = fields.Integer(required=True, string='Extend By')
    extend_type = fields.Selection([('days', 'Day'),
                                    ('weeks', 'Week'),
                                    ('months', 'Month'),
                                    ('years', 'Year')], default='years', required=True, string='Extend Type')
    extend_for = fields.Selection([('end_date', 'Only Extend End Date'),
                                   ('both', 'Extend Both of Start & End Date')], default='both', required=True, string='Extend For')
    bom_ids = fields.Many2many('mrp.bom', 'expiry_date_wizard_to_bom', 'wiz_id', 'bom_id', required=True, ondelete='cascade', string='BOM')

    @api.multi
    def extend_expiry_date(self):
        extend_type = self.extend_type
        if extend_type == 0:
            return False
        
        extended_boms = []
        for bom_id in self.bom_ids:
            if self.extend_type == 'days':
                start_date = datetime.strptime(bom_id.start_date, DEFAULT_SERVER_DATE_FORMAT) + relativedelta(days=self.extend_by) if bom_id.start_date else False
                end_date = datetime.strptime(bom_id.end_date, DEFAULT_SERVER_DATE_FORMAT) + relativedelta(days=self.extend_by) if bom_id.end_date else False
            elif self.extend_type == 'weeks':
                start_date = datetime.strptime(bom_id.start_date, DEFAULT_SERVER_DATE_FORMAT) + relativedelta(weeks=self.extend_by) if bom_id.start_date else False
                end_date = datetime.strptime(bom_id.end_date, DEFAULT_SERVER_DATE_FORMAT) + relativedelta(weeks=self.extend_by) if bom_id.end_date else False
            elif self.extend_type == 'months':
                start_date = datetime.strptime(bom_id.start_date, DEFAULT_SERVER_DATE_FORMAT) + relativedelta(months=self.extend_by) if bom_id.start_date else False
                end_date = datetime.strptime(bom_id.end_date, DEFAULT_SERVER_DATE_FORMAT) + relativedelta(months=self.extend_by) if bom_id.end_date else False
            else:
                start_date = datetime.strptime(bom_id.start_date, DEFAULT_SERVER_DATE_FORMAT) + relativedelta(years=self.extend_by) if bom_id.start_date else False
                end_date = datetime.strptime(bom_id.end_date, DEFAULT_SERVER_DATE_FORMAT) + relativedelta(years=self.extend_by) if bom_id.end_date else False

            values = {}
            if self.extend_for == 'end_date' and end_date:
                values['end_date'] = end_date
            elif self.extend_for == 'both':
                if start_date:
                    values['start_date'] = start_date
                if end_date:
                    values['end_date'] = end_date
            if values:
                bom_id.write(values)
                extended_boms.append(bom_id.id)
        
        if extended_boms:
            # Тохирох view-г буцаана
            action = self.env.ref('mrp.mrp_bom_form_action')
            result = action.read()[0]
            result.pop('id', None)
            result['context'] = {}
        
            # choose the view_mode accordingly
            result['domain'] = "[('id','in',[%s])]" % (','.join(map(str, extended_boms)) if len(extended_boms) > 0 else '')
            if len(extended_boms) == 1:
                res = self.env.ref('l10n_mn_mrp.view_mrp_bom_form_mn_mrp', False)
                result['views'] = [(res and res.id or False, 'form')]
                result['res_id'] = extended_boms and extended_boms[0] or False
            
            return result
        
        return False
        

class MrpBOM(models.Model):
    _inherit = 'mrp.bom'
    
    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        # ОРЦНЫ ХҮЧИНТЭЙ ХУГАЦАА ШАЛГАХ чек-с хамааруулан холбоотой талбаруудыг tree/search харагдацаас нуух
        res = super(MrpBOM, self).fields_view_get(view_id, view_type, toolbar=toolbar, submenu=submenu)
        if not self.env.user.has_group('mrp.group_mrp_manager') or not self.env.user.company_id.check_bom_expiry_date:
            if res.get('toolbar'):
                for action in res.get('toolbar').get('action'):
                    if action.get('xml_id', False) == 'l10n_mn_mrp.extend_expiry_date_server_action' or action.get('xml_id', False) == 'l10n_mn_mrp.extend_expiry_date_client_action':
                        res['toolbar']['action'].remove(action)
        if not self.env.user.company_id.check_bom_expiry_date:
            if view_type == 'tree' or view_type == 'search':
                doc = etree.XML(res['arch'])
                if doc.xpath("//field[@name='start_date']"):
                    node = doc.xpath("//field[@name='start_date']")[0]
                    doc.remove(node)
                if doc.xpath("//field[@name='end_date']"):
                    node = doc.xpath("//field[@name='end_date']")[0]
                    doc.remove(node)
                if view_type == 'search':
                    if doc.xpath("//filter[@name='group_start_date']"):
                        node = doc.xpath("//filter[@name='group_start_date']")[0]
                        doc.remove(node)
                    if doc.xpath("//filter[@name='group_end_date']"):
                        node = doc.xpath("//filter[@name='group_end_date']")[0]
                        doc.remove(node)
                    if doc.xpath("//filter[@name='expired_bom']"):
                        node = doc.xpath("//filter[@name='expired_bom']")[0]
                        doc.remove(node)
                    if doc.xpath("//filter[@name='expired_current_month']"):
                        node = doc.xpath("//filter[@name='expired_current_month']")[0]
                        doc.remove(node)
                    if doc.xpath("//filter[@name='expired_previous_month']"):
                        node = doc.xpath("//filter[@name='expired_previous_month']")[0]
                        doc.remove(node)
                res['arch'] = etree.tostring(doc)
        return res
    
    @api.model
    def extend_expiry_date(self, ids):
        if not self.env.user.has_group('mrp.group_mrp_manager'):
            raise ValidationError(_("If you want to extend expiry date, you must have 'Mrp/Manager' group users !!!"))
        
        if not ids:
            raise UserError(_("Please select at least one !!!"))
        
        wizard_id = self.env['extend.expiry.date'].create({
            'bom_ids': [(6, 0, ids)],
            'extend_by': 1
        })
            
        return {
            'type': 'ir.actions.act_window',
            'name': _('Extend Expiry Date'),
            'res_model': 'extend.expiry.date',
            'view_mode': 'form',
            'target': 'new',
            'res_id': wizard_id.id,
        }
        
