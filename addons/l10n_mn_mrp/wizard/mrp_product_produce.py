# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class MrpProductProduce(models.TransientModel):
    _inherit = "mrp.product.produce"

    product_tracking = fields.Selection(related="product_id.tracking", readonly=True)
