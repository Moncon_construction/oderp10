# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Mongolian Manufacturing Account',
    'version': '1',
    'category': 'Manufacturing',
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'sequence': 15,
    'depends': ['mrp', 'l10n_mn_mrp', 'l10n_mn_account'],
    'description': """
        Cost calculations and financial records of Production
    """,
    'data': [
        'views/ir_sequence.xml',
        'data/account_journal.xml',
        'security/ir.model.access.csv',
        'views/mrp_bom_views.xml',
        'views/mrp_workcenter_view.xml',
        'views/mrp_production_view.xml',
        'views/mrp_overhead_expense_view.xml',
        'views/product_category_view.xml'
    ],
    'demo': [
        ],
    'application': True,
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}