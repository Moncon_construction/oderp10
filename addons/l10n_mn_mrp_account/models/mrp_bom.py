# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError

from datetime import datetime

class MrpBom(models.Model):
    _inherit = 'mrp.bom'
    '''
        Стандарт өртөг тооцоологч
    '''
    @api.multi
    def _compute_standard_cost(self):
        for obj in self:
            total_sum = 0
            if obj.bom_line_ids:
                for line in obj.bom_line_ids:
                    total_sum += line.total_cost
    
            if obj.expense_ids:
                for line in obj.expense_ids:
                    total_sum += line.total_cost
            obj.standard_cost = total_sum

    standard_cost = fields.Float('Standart cost', compute=_compute_standard_cost)
    expense_ids = fields.One2many('mrp.bom.overhead.expense', 'bom_id')

    @api.multi
    def button_reset_cost(self):
        cost_list = self.env['mrp.bom.overhead.expense']
        if self.env['mrp.overhead.expense'].search([('state', '=', 'approved')]):
            cost_list = self.env['mrp.bom.overhead.expense']
            expense_objs = self.env['mrp.overhead.expense'].search([('state', '=', 'approved')])
            ids = []
            for line in expense_objs:
                if not self.expense_ids.search([('overhead_expense', '=', line.id), ('bom_id', '=', self.id)]):
                    expenses = cost_list.create({
                            'bom_id':self.id,
                            'overhead_expense':line.id,
                            })
                    ids.append(expenses.id)
            self.expense_ids = ids

    '''
        Гүйцэтгэлээрх өртөг тооцоологч
    '''
    @api.multi
    def button_reset_actual_cost(self):
        operation_cost = pay_cost = 0
        sum1 = 0
        curent_date = datetime.now()
        config = self.env['mrp.operation.norm']
        config_line = self.env['mrp.operation.norm.line']
        cost_list = self.env['mrp.bom.overhead.expense']

        cost_list_ids = cost_list.search([('production_id', '=', self.id)])
        if cost_list_ids:
            for lst in cost_list_ids:
                lst.unlink()

        bom_cost = self.bom_id.standard_cost

        cost_list.create({
            'name': self.bom_id.product_tmpl_id.name,
            'type': u'Шууд материал',
            'cost': self.bom_id.standard_cost,
            'production_id': self.id
            })

        if self.routing_id:
            for operation_line in self.routing_id.operation_ids:
                for line in operation_line.workcenter_id.expense_line:
                    if operation_line.time_mode != 'standard_time':
                        operation_cost = line.expense_amount * operation_line.time_cycle_manual
                        cost_list.create({
                                'name': operation_line.workcenter_id.name,
                                'overhead_expense': line.overhead_expense.name,
                                'cost': operation_cost,
                                'production_id': self.id,
                                })
                        sum1 += operation_cost
                    else:
                        operation_cost += line.expense_amount * operation_line.time_cycle_standard
                        cost_list.create({
                                'name': operation_line.workcenter_id.name,
                                'overhead_expense': line.overhead_expense.name,
                                'cost': operation_cost,
                                'production_id': self.id,
                                })
                        sum1 += operation_cost
                conf = config.search([('workable_date', '<=', curent_date)])
                conf_line = config_line.search([
                    ('operation_norm_id', '=', conf.id),
                    ('workcenter_id', '=', operation_line.workcenter_id.id)])
                if conf_line:
                    pay_cost += conf_line.tariff
        total_sum = sum1 + bom_cost + pay_cost

        self.actual_cost = total_sum

class MrpBomLine(models.Model):
    _inherit = 'mrp.bom.line'

    @api.depends('product_id', 'product_qty')
    def _compute_total_cost(self):
        for obj in self:
            if obj.product_id:
                obj.total_cost = obj.product_id.uom_id._compute_price(obj.product_id.standard_price, obj.product_uom_id) * obj.product_qty

    unit_cost = fields.Float(related='product_id.standard_price')
    total_cost = fields.Float(compute=_compute_total_cost)


class MrpBomOverheadExpense(models.Model):
    """ Manufacturing Bom Overhead Expense """
    _name = 'mrp.bom.overhead.expense'
    _description = 'Manufacturing BOM overhead expense'

    @api.multi
    @api.depends('overhead_expense')
    def _compute_total_cost(self):
        for obj in self:
            if obj.overhead_expense:
                for type in obj.overhead_expense:
                    obj.total_cost = obj.bom_id.product_uom_id._compute_price(type.unit_cost, type.uom_id) * obj.bom_id.product_qty

    name = fields.Char('Name')
    unit_cost = fields.Float(related='overhead_expense.unit_cost')
    product_uom = fields.Many2one(related='bom_id.product_uom_id')
    product_qty = fields.Float('Product QTY')
    total_cost = fields.Float(compute=_compute_total_cost)
    bom_id = fields.Many2one('mrp.bom', 'Manufacturing Order')
    overhead_expense = fields.Many2one('mrp.overhead.expense', 'Overhead expense')

    @api.onchange('overhead_expense')
    def _onchange_type_uom_check(self):
        if self.overhead_expense:
            if self.overhead_expense.uom_id.category_id != self.bom_id.product_uom_id.category_id:
                raise UserError(_("It's another category of product uom"))
