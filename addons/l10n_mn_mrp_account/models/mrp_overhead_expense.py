# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class MrpOverheadExpense(models.Model):
    _name = 'mrp.overhead.expense'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    name = fields.Char('Name', required=True, track_visibility='onchange')
    account_id = fields.Many2one('account.account', string='Control Account', required=True, track_visibility='onchange')
    fluctuation_account_id = fields.Many2one('account.account', string='Fluctuation Account', required=True, track_visibility='onchange')
    uom_id = fields.Many2one('product.uom', string="Basic Time UOM", required=True, track_visibility='onchange')
    uom_categ_id = fields.Many2one('product.uom.categ', string="Product UOM", required=True, track_visibility='onchange')
    unit_cost = fields.Float(string='Unit Cost', required=True, track_visibility='onchange')
    state = fields.Selection([
            ('new', 'New'),
            ('control', 'Control'),
            ('approved', 'Approved'),
            ('cancelled', 'Cancelled'),
    ], string='Status', index=True, readonly=True, default='new',
        track_visibility='onchange')
    description = fields.Text('Description', required=True, track_visibility='onchange')

    @api.multi
    def action_approve(self):
        self.write({'state' : 'approved'})

    @api.multi
    def action_control(self):
        self.write({'state' : 'control'})

    @api.multi
    def action_cancel(self):
        self.write({'state' : 'cancelled'})

    def action_new(self):
        self.write({'state' : 'new'})
