# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from datetime import datetime
from datetime import timedelta
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError  # @UnresolvedImport
from odoo.exceptions import UserError
import logging

class MrpProduction(models.Model):
    _inherit = 'mrp.production'

    cost_type = fields.Selection([
                                   ('unit_cost', 'Standard Cost'),
                                   ('actual_cost', 'Actual Cost Incurred')
                                   ], 'Cost type', required=True, default='unit_cost')
    unit_cost = fields.Float('Unit cost', help="Specify Cost of Production.",
                             states={'done': [('readonly', True)],
                                     'approved_cost': [('readonly', True)],
                                     'cancel': [('readonly', True)]},)
    actual_cost = fields.Float('Actual Cost Incurred', help="Actual Cost Incurred.",
                             states={'done': [('readonly', True)],
                                     'approved_cost': [('readonly', True)],
                                     'cancel': [('readonly', True)]},)

    cost_ids = fields.One2many('mrp.production.overhead.expense', 'production_id', 'Production Cost List')

    '''
        Стандарт өртөг тооцоологч
    '''

    @api.multi
    def create_account_journal(self, is_button_confirm, is_button_done):
        account_move_obj = self.env['account.move']
        account_move_line_obj = self.env['account.move.line']
        account_journal = self.env['account.journal'].search([('code', '=', 'MRP02')])
        for obj in self:
            """
                Хэрэв батлах товч дарагдсан бол
            """
            if is_button_confirm and not is_button_done:
                move_lines = []
                total = 0.0
                if not obj.product_qty > 0:
                     raise UserError(_('Please set the quantity. It can not be negative'))
                if not obj.product_id.categ_id.work_in_progress_account_id:
                     raise UserError(_('Please set the work in progress account id. It can not empty'))
                """
                Үйлдвэрлэлийн захиалга (ҮЗ) батлах үед:
                    - Журналийн бичилт хийгдэх дансууд:
                        + Кредит : Барааны ангилалаас ДҮ данc
                        + Дебит : Орц - Нэмэлт Зардлууд
                """

                if obj.bom_id.expense_ids:
                    for line in obj.bom_id.expense_ids:
                        """
                            ҮЗ Батлах үед өртгийн задаргааг орцоос авч дүүргэнэ.
                        """
                        if obj.cost_type == 'unit_cost':
                            self.cost_ids.create({
                                    'production_id':obj.id,
                                    'name': obj.product_tmpl_id.name,
                                    'type': line.overhead_expense.name,
                                    'standard_cost': line.total_cost,
                                    'overhead_expense': line.overhead_expense.id,
                                    })
                            total = total + line.total_cost
                            credit_line_vals = {
                                        'name': line.overhead_expense.name,
                                        'date_maturity': datetime.now(),
                                        'account_id': line.overhead_expense.account_id.id,
                                        'partner_id': obj.user_id.partner_id.id,
                                        'credit': line.total_cost,
                                        'quantity': obj.product_qty or 1,
                                        'production_id': obj.id,
                                        }
                            move_lines.append((0, 0, credit_line_vals))

                move_name = obj.name + ' / ' + '%s' % (self.env['ir.sequence'].get('account.move'))
                debit = total
                total = 0.0
                debit_line_vals = {
                        'name': (_('Overhead Expenses')),
                        'date_maturity': datetime.now(),
                        'account_id': obj.product_id.categ_id.work_in_progress_account_id.id,
                        'partner_id': obj.user_id.partner_id.id,
                        'debit': debit if debit else 0.0,
                        'quantity': obj.product_qty or 1,
                        'production_id': obj.id,
                    }
                move_lines.append((0, 0, debit_line_vals))
                move_values = {
                        'name': move_name,
                        'journal_id': account_journal.id,
                                          'date': datetime.now(),
                                          'state': 'posted',
                                          'description': obj.product_tmpl_id.name,
                                          'line_ids': move_lines
                    }
                move_id = account_move_obj.create(move_values)

            """
                Хэрэв дуусах товч дарагдсан бол
            """
            if not is_button_confirm and is_button_done:
                """
                    Журналийн бичилт хийгдэх дансууд:
                        + Кредит : БМ данс - Шууд материалын нийлбэр дүн
                        + Кредит : БМ данс - ҮНЗ нийлбэр дүн
                        + Дебит : ДҮ данс - ҮНЗ дүнгүүд
                        + Дебит : ДҮ данс - Материал буюу орцын бүрдэл хэсгүүдийн дүнгүүд
                """
                move_lines = []
                total_cost = 0.0
                total_bom = 0.0
                for e_line in obj.cost_ids:
                    if obj.cost_type == 'unit_cost':
                        credit = e_line.standard_cost
                    elif obj.cost_type == 'actual_cost':
                        credit = e_line.cost
                    credit_line_vals = {
                                        'name': e_line.overhead_expense.name,
                                        'date_maturity': datetime.now(),
                                        'account_id': obj.product_id.categ_id.work_in_progress_account_id.id,
                                        'partner_id': obj.user_id.partner_id.id,
                                        'credit': credit,
                                        'production_id': obj.id,
                                    }
                    total_cost = total_cost + credit
                    move_lines.append((0, 0, credit_line_vals))

                for e_line in obj.bom_id.bom_line_ids:
                    if obj.cost_type == 'unit_cost':
                        credit = obj.bom_id.product_qty * e_line.total_cost
                    elif obj.cost_type == 'actual_cost':
                        line = obj.move_raw_ids.search([('product_id', '=', e_line.product_id.id), ('production_id', '=', obj.id)])
                        credit = line.consumed_qty * e_line.total_cost
                    credit_line_vals = {
                                        'name': e_line.product_id.name,
                                        'date_maturity': datetime.now(),
                                        'account_id': obj.product_id.categ_id.work_in_progress_account_id.id,
                                        'partner_id': obj.user_id.partner_id.id,
                                        'credit': credit,
                                        'quantity': e_line.product_qty or 1,
                                        'production_id': obj.id,
                                    }
                    total_bom = total_bom + credit
                    move_lines.append((0, 0, credit_line_vals))
                debit_first = total_cost
                total = 0.0
                debit_line_vals_first = {
                        'name': (_('Overhead Expenses')),
                        'date_maturity': datetime.now(),
                        'account_id': obj.product_id.categ_id.property_stock_valuation_account_id.id,
                        'partner_id': obj.user_id.partner_id.id,
                        'debit': debit_first if debit_first else 0.0,
                        'production_id': obj.id,
                    }
                move_lines.append((0, 0, debit_line_vals_first))
                debit_second = total_bom
                debit_line_vals_second = {
                        'name': (_('Bill of Materials')),
                        'date_maturity': datetime.now(),
                        'account_id': obj.product_id.categ_id.property_stock_valuation_account_id.id,
                        'partner_id': obj.user_id.partner_id.id,
                        'debit': debit_second if debit_second else 0.0,
                        'production_id': obj.id,
                    }
                move_lines.append((0, 0, debit_line_vals_second))
                move_name = obj.name + ' / ' + '%s' % (self.env['ir.sequence'].get('account.move'))
                move_values = {
                        'name': move_name,
                        'journal_id': account_journal.id,
                        'date': datetime.now(),
                        'state': 'posted',
                        'description': obj.name,
                        'line_ids': move_lines
                    }
                move_id = account_move_obj.create(move_values)
                total_actual_cost = total_bom + total_cost
                obj.actual_cost = total_actual_cost

            if is_button_confirm and is_button_done:
                obj.button_confirm()
                obj.button_reset_cost()
                obj.button_mark_done()

##TODO: Үйлдвэрлэлийн захиалга батлах үед нэмэгдэл зардлуудыг ДҮ данс руу бичих доорхи код нь mrp.config.settings шалгахаар хийгдсэн боловч энгийн хэрэглэгчийн эрхээр нэвтэрсэн тохиолдолд уг тохиргоог шалгаж чадахгүй байна. Иймд mrp.config.settings-г шалгах бус, өөр шийдэл олтол энэ кодыг корын шийдэл рүү түр буцаав.

    @api.multi
    def button_confirm(self):
        """
            Үйлдвэрлэлийн тохиргооноос:
             - Үйлдвэрлэлийн захиалгаар удирдах
            тохиргоог хийсэн бол үйлдвэрлэлийн захиалга
            батлах үед хийгдэх журналын бичилт хийнэ.
        """

        for order in self:
            conf_mod =  self.env['mrp.config.settings'].sudo().search( [('company_id', '=', order.company_id.id)] , order="id desc", limit=1 )
            if conf_mod:
                if conf_mod.group_mrp_routings is False:
                    self.create_account_journal(is_button_confirm=True, is_button_done=False)
        self.write({'state': 'confirmed'})





    @api.multi
    def button_mark_done(self):
        res = super(MrpProduction, self).button_mark_done()
        """
            Үйлдвэрлэлийн тохиргооноос:
             - Үйлдвэрлэлийн захиалгаар удирдах
            тохиргоог хийсэн бол үйлдвэрлэлийн захиалга (ҮЗ)
            дуусахад хийгдэх журналын бичилт хийнэ.
        """
        conf_setting = self.env['mrp.config.settings'].search([], order='id Desc', limit=1)
        if conf_setting.group_mrp_routings is False:
            self.create_account_journal(is_button_confirm=False, is_button_done=True)
        return res

    """
        ҮЗ-н стандарт өртөг шинэчлэх даруулын функц.
    """

    @api.multi
    def button_reset_cost(self):
        self.bom_id.button_reset_cost()
        bom_cost = self.bom_id.standard_cost
        self.unit_cost = bom_cost
    """
        ҮЗ-н гүйцэтгэлээрх өртөг шинэчлэх даруулын функц.
    """

    @api.multi
    def button_reset_actual_cost(self):
        operation_cost = pay_cost = bom_cost = 0
        std_bom_cost = standard_cost = sum1 = 0
        curent_date = datetime.now()
        config = self.env['mrp.operation.norm']
        config_line = self.env['mrp.operation.norm.line']
        cost_list = self.env['mrp.production.overhead.expense']
        work_order_obj = self.env['mrp.workorder']
        cost_list_ids = cost_list.search([('production_id', '=', self.id)])
        if cost_list_ids:
            for lst in cost_list_ids:
                lst.unlink()
        if self.move_raw_ids:
            for line in self.move_raw_ids:
                bom_cost += line.product_id.standard_price * line.product_qty
        if self.bom_id:
            for line in self.bom_id.bom_line_ids:
                std_bom_cost += line.product_id.standard_price * line.product_qty

        work_order_ids = work_order_obj.search([('production_id', '=', self.id)])
        if work_order_ids:
            for order in work_order_ids:
                for line in order.workcenter_id.expense_line:
                    if order.duration > 0 and order.qty_produced > 0:
                        operation_cost = line.unit_cost * (order.duration / order.qty_produced)
                    else:
                        raise ValidationError(_('ZeroDivisionError: float division by zero! \n\
                                                The workcenter duration equals zero or The workcenter Current QTY equals zero '))

                    for operation_line in self.routing_id.operation_ids:
                        if operation_line:
                            if order.workcenter_id.id == operation_line.workcenter_id.id:
                                if operation_line.time_mode != 'standard_time':
                                    standard_cost = line.unit_cost * operation_line.time_cycle_manual
                                else:
                                    standard_cost = line.unit_cost * operation_line.time_cycle_standard
                    """
                        Гүйцэтгэлээрх өртөг шинэчлэх үед
                            - Өртгийн задаргааг шинэчлэнэ.
                    """
                    cost_list.create({
                            'name': order.workcenter_id.name,
                            'type': line.overhead_expense.name,
                            'cost': operation_cost,
                            'standard_cost': standard_cost,
                            'production_id': self.id,
                            'overhead_expense': line.overhead_expense.id,
                            })
                    sum1 += operation_cost
                conf = config.search([('workable_date', '<=', curent_date)])
                conf_line = config_line.search([
                    ('operation_norm_id', '=', conf.id),
                    ('workcenter_id', '=', order.workcenter_id.id)])
                if conf_line:
                    pay_cost += conf_line.tariff
        total_sum = sum1 + bom_cost / self.product_qty + pay_cost
        self.actual_cost = total_sum

    """
        ҮЗ-н өртгүүдийг тооцсоны дараа
            - Өртөг батлах даруулын функц.
    """
    def cost_confirm(self):
        stock_move = self.env['stock.move']
        acc_move = self.env['account.move']
        acc_move_line = self.env['account.move.line']
        sm = stock_move.search([('production_id', '=', self.id)])
        cost = 0
        move_lines = []
        for move in sm:
            if self.cost_type == 'unit_cost':
                cost = self.unit_cost
            elif self.cost_type == 'actual_cost':
                if not self.cost_ids:
                    raise UserError(_('You must update costs!'))
                cost = self.actual_cost
            else:
                raise ValidationError(_('Select cost type!'))
            move.price_unit = cost
            self.product_id.standard_price = cost
        for obj in self:
            if not obj.product_id.categ_id.property_stock_account_input_categ_id:
                raise UserError(_('"Inventory income account" is not found!'))
            """
                ҮЗ-н өртөг батлагдсан үед хийгдэх журналын бичилт хийх үйлдэл.
            """
            for line in obj.cost_ids:
                if line.overhead_expense:
                    if obj.cost_type == 'unit_cost':
                        debit_line_vals = {
                            'name': obj.name,
                            'date_maturity': datetime.now(),
                            'account_id': line.overhead_expense.account_id.id,
                            'debit': line.standard_cost,
                            'credit': 0
                            }
                        credit_line_vals = {
                            'name': obj.name,
                            'date_maturity': datetime.now(),
                            'account_id': obj.product_id.categ_id.property_stock_account_input_categ_id.id,
                            'credit': line.standard_cost,
                            'debit': 0
                            }
                    elif obj.cost_type == 'actual_cost':
                        debit_line_vals = {
                            'name': obj.name,
                            'date_maturity': datetime.now(),
                            'account_id': line.overhead_expense.account_id.id,
                            'debit': line.cost,
                            'credit': 0
                            }
                        credit_line_vals = {
                            'name': obj.name,
                            'date_maturity': datetime.now(),
                            'account_id': obj.product_id.categ_id.property_stock_account_input_categ_id.id,
                            'credit': line.cost,
                            'debit': 0
                            }
                    move_lines.append((0, 0, credit_line_vals))
                    move_lines.append((0, 0, debit_line_vals))
            am = acc_move.create({
                        'description': obj.name,
                        'journal_id': obj.product_id.categ_id.property_stock_journal.id,
                        'date': fields.Datetime.now(),
                        'line_ids': move_lines
                    })
            am.post()
        self.write({'state': 'approved_cost'})

    '''
        ТЭМ захиалга үүсгэх
    '''
    def post_inventory(self):
        mrp_config = self.env['mrp.config.settings']
        for order in self:
            if order.cost_type != 'unit_cost':
                if order.state != 'approved_cost':
                    raise UserError(_('You must be approve cost'))
            conf_mod = mrp_config.search([('company_id', '=', order.company_id.id)], order="id desc", limit=1)
            if conf_mod:
                if conf_mod.group_approve_mrp_cost:
                    if order.state == 'approved_cost':
                        if order.cost_type == 'unit_cost':
                            order.move_finished_ids.price_unit = order.unit_cost
                        elif order.cost_type == 'actual_cost':
                            order.move_finished_ids.price_unit = order.actual_cost
                    else:
                        raise ValidationError(_('Cost is not confirmed! \n\
                                                The cost must be approved'))
                        
        return super(MrpProduction, self).post_inventory()


class MrpProductionOverheadExpense(models.Model):
    """ Manufacturing production Overhead expense """
    _name = 'mrp.production.overhead.expense'
    _description = 'Manufacturing Order Workcenter'

    standard_cost = fields.Float('Standard Cost')
    cost = fields.Float('Actual Cost Incurred')
    production_id = fields.Many2one('mrp.production', 'Manufacturing Order')
    overhead_expense = fields.Many2one('mrp.overhead.expense', "Overhead expense")
