# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime
from odoo import fields, models, api
DATE_FORMAT = "%Y-%m-%d"


class MrpWorkcenter(models.Model):
    _inherit = 'mrp.workcenter'

    expense_line = fields.One2many('mrp.workcenter.overhead.expense', 'workcenter_id')

    @api.onchange('expense_line')
    def onchange_reset_cost_lines(self):
        if self.expense_line:
            total_sum = 0.0
            for line in self.expense_line:
                total_sum += line.unit_cost
            self.unit_cost = total_sum

    '''
        ҮНЗ-н жагсаалтад батлагдсан ҮНЗ-үүдийг шинэчлэх функц
    '''

    @api.multi
    def update_overhead_expenses(self):
        for obj in self:
            if obj.expense_line:
                total_sum = 0.0
                for line in obj.expense_line:
                    type_id = self.env['mrp.overhead.expense'].search([('id', '=', line.overhead_expense.id)])
                    line.unit_cost = type_id.unit_cost
                    total_sum += line.unit_cost
                obj.unit_cost = total_sum
            elif not obj.expense_line:
                for line in self.env['mrp.overhead.expense'].search([('state', '=', 'approved')]):
                    if not obj.expense_line.search([('overhead_expense', '=', line.id), ('workcenter_id', '=', obj.id)]):
                        obj.expense_line.create({
                            'workcenter_id':obj.id,
                            'overhead_expense':line.id,
                            })


class MrpWorkcenterOverheadExpense(models.Model):
    _name = 'mrp.workcenter.overhead.expense'

    workcenter_id = fields.Many2one('mrp.workcenter', 'Workcenter')
    overhead_expense = fields.Many2one('mrp.overhead.expense', string='Expense', required=True)
    unit_cost = fields.Float('Unit amount')

    @api.multi
    def write(self, vals):
        if 'overhead_expense' in vals:
            type_id = self.env['mrp.overhead.expense'].search([('id', '=', vals['overhead_expense'])])
            vals['unit_cost'] = type_id.unit_cost
        return super(MrpWorkcenterOverheadExpense, self).write(vals)

    @api.model
    def create(self, vals):
        if 'workcenter_id' in vals.keys() and 'overhead_expense' in vals.keys():
            workcenter_id = self.env['mrp.workcenter'].search([('id', '=', vals['workcenter_id'])])
            type_id = self.env['mrp.overhead.expense'].search([('id', '=', vals['overhead_expense'])])
            vals['unit_cost'] = type_id.unit_cost
        return super(MrpWorkcenterOverheadExpense, self).create(vals)

    @api.onchange('overhead_expense')
    def onchange_type_id(self):
        self.unit_cost = self.overhead_expense.unit_cost
