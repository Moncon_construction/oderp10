# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models

class ProductCategory(models.Model):
    _inherit = 'product.category'

    work_in_progress_account_id = fields.Many2one('account.account', string ='Work In Progress Account')

    
