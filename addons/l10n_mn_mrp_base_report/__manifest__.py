# -*- coding: utf-8 -*-
{
    'name': 'Mongolian manufacturing base reports',
    'version': '1.0',
    'category': 'Mongolian Modules',
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'depends': ['l10n_mn_mrp_food', 'l10n_mn_mrp_account'],
    'description': """
        Manufacturing reports included belown:
        * Manufacturing fluctuation report
    """,
    'data': [
        'wizard/manufacturing_fluctuation_report.xml',
    ],
    'application': True,
    'installable': True,
}
