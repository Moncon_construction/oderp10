# -*- coding: utf-8 -*-

import base64
from io import BytesIO
import time
import xlsxwriter

from odoo import models, fields, api, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.addons.l10n_mn_report.models.report_helper import *
from odoo.exceptions import AccessError


class ManufacturingFluctuationReport(models.TransientModel):
    """
        ҮЙЛДВЭРЛЭЛИЙН МАТЕРИАЛЫН ХЭЛБЭЛЗЛИЙН ТАЙЛАН
    """

    _name = 'manufacturing.fluctuation.report'

    def _domain_warehouse(self):
        return [('id', 'in', [wh.id for wh in self.env.user.allowed_warehouses])]

    company_id = fields.Many2one('res.company', string='Company', required=True, readonly=True, ondelete='cascade', default=lambda self: self.env.user.company_id)
    date_from = fields.Date("Start date", required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    date_to = fields.Date("End date", required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    product_warehouse_ids = fields.Many2many('stock.warehouse', 'final_product_wh_to_fluctuation_report', 'red_id', 'wh_id', domain=_domain_warehouse, ondelete='cascade', string="Final product warehouse")
    raw_material_warehouse_ids = fields.Many2many('stock.warehouse', 'raw_material_wh_to_fluctuation_report', 'red_id', 'wh_id', domain=_domain_warehouse, ondelete='cascade', string="Raw material warehouse")
    product_ids = fields.Many2many('product.product', 'final_product_to_fluctuation_report', 'red_id', 'pro_id', ondelete='cascade', string="Final product")
    raw_material_ids = fields.Many2many('product.product', 'raw_material_to_fluctuation_report', 'red_id', 'pro_id', ondelete='cascade', string="Raw material")
    mrp_ids = fields.Many2many('mrp.production', 'mrp_order_to_fluctuation_report', 'red_id', 'order_id', ondelete='cascade', string="Production")

    def get_sheet_format(self, sheet):
        sheet.set_portrait()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.39, 0.39, 0.39, 0.39)  # 1cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        sheet.hide_gridlines(2)
        return sheet

    def get_header(self, sheet, header_formats, rowx, report_name):
        sheet.merge_range(rowx, 0, rowx, 4, '%s: %s' % (_('Company name'), self.company_id.name), header_formats['format_filter'])
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, 14, report_name.upper(), header_formats['format_name'])
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, 4, '%s: %s - %s' % (_('Duration'), self.date_from, self.date_to), header_formats['format_filter'])
        rowx += 1
        sheet.merge_range(rowx, 0, rowx, 4, '%s: %s' % (_('Create Date'), get_day_like_display(fields.Datetime.now(), self.env.user)), header_formats['format_filter'])
        rowx += 1
        return sheet, rowx

    def get_data(self):
        raw_material_warehouse_ids = self.raw_material_warehouse_ids or self.env.user.allowed_warehouses.filtered(lambda x: x.company_id == self.company_id)
        product_warehouse_ids = self.product_warehouse_ids or self.env.user.allowed_warehouses.filtered(lambda x: x.company_id == self.company_id)
        if not raw_material_warehouse_ids and not product_warehouse_ids:
            raise AccessError(_("You aren't allowed to any warehouse !!!"))

        date_from = get_display_day_to_user_day(self.date_from + ' 00:00:00', self.env.user)
        date_to = get_display_day_to_user_day(self.date_to + ' 23:59:59', self.env.user)
        where = " AND pp_raw_material.id IN (" + ','.join(map(str, self.raw_material_ids.ids)) + ") " if self.raw_material_ids else ""
        where += " AND pp_finished.id IN (" + ','.join(map(str, self.product_ids.ids)) + ") " if self.product_ids else ""
        where += " AND loc.id IN (" + ','.join(map(str, raw_material_warehouse_ids.mapped('lot_stock_id').ids)) + ") " if raw_material_warehouse_ids.mapped('lot_stock_id') else ""
        where += " AND loc_dest.id IN (" + ','.join(map(str, product_warehouse_ids.mapped('lot_stock_id').ids)) + ") " if product_warehouse_ids.mapped('lot_stock_id') else ""
        where += " AND mrp.id IN (" + ','.join(map(str, self.mrp_ids.ids)) + ") " if self.mrp_ids else ""

        if self.env.get('product.warehouse.standard.price', False) != False:
            select = "wh_standard_price.standard_price"
            join = """
                    LEFT JOIN stock_warehouse wh ON wh.lot_stock_id = loc.id
                    LEFT JOIN product_warehouse_standard_price wh_standard_price ON wh_standard_price.product_id = pp_raw_material.id AND wh_standard_price.warehouse_id = wh.id
                """
        else:
            select = "ip.value_float"
            join = "LEFT JOIN ir_property ip ON ip.res_id = CONCAT('product.product,', pp_raw_material.id)"

        qry = """
            SELECT pp_finished.id AS pp_finished, pt_finished.id AS pt_finished, pt_finished.name AS pp_finished_name,
                mrp.id AS mrp_id, mrp.name AS mrp_name, sm.date AS date,
                pt_raw_material.default_code AS default_code, pp_raw_material.barcode AS barcode, pt_raw_material.name AS pt_raw_material_name,
                AVG(%s) AS standard_price, SUM(sm.product_uom_qty) AS gross_weigth,
                ROUND(SUM(sm.product_uom_qty * (100 - boml.decrease_percent) / 100), 6) AS net_weight, AVG(sm.price_unit) AS price_unit
            FROM stock_move sm
            LEFT JOIN product_product pp_raw_material ON pp_raw_material.id = sm.product_id
            LEFT JOIN product_template pt_raw_material ON pt_raw_material.id = pp_raw_material.product_tmpl_id
            INNER JOIN mrp_production mrp ON mrp.id = sm.raw_material_production_id
            LEFT JOIN stock_location loc ON loc.id = mrp.location_src_id
            LEFT JOIN stock_location loc_dest ON loc_dest.id = mrp.location_dest_id
            %s
            LEFT JOIN product_product pp_finished ON pp_finished.id = mrp.product_id
            LEFT JOIN product_template pt_finished ON pt_finished.id = pp_finished.product_tmpl_id
            LEFT JOIN mrp_bom bom ON bom.id = mrp.bom_id
            LEFT JOIN mrp_bom_line boml ON boml.bom_id = bom.id AND boml.product_id = pp_raw_material.id
            WHERE (boml.decrease_percent IS NOT NULL AND boml.decrease_percent != 0) AND sm.company_id = %s AND sm.date BETWEEN '%s' AND '%s'
                %s
            GROUP BY pp_finished.id, pt_finished.id, mrp.id, pp_raw_material.id, pt_raw_material.id, sm.date
            ORDER BY pt_finished.name, mrp.name, pt_raw_material.name, sm.date
        """ % (select, join, self.company_id.id, date_from, date_to, where)
        self._cr.execute(qry)
        return self._cr.dictfetchall()

    def get_value(self, sheet, rowx, content_formats):
        lines = self.get_data()
        product_ids, mrp_ids, product_group_rowxs, mrp_group_rowxs = [], [], [], []
        last_pro_rowx, last_mrp_rowx, group_start_rowx, group_end_rowx = -1, -1, -1, -1
        raw_seq = 1

        for line in lines:
            if line['pp_finished'] not in product_ids:
                product_ids.append(line['pp_finished'])
                sheet.merge_range(rowx, 0, rowx, 4, line['pp_finished_name'], content_formats['format_group_left'])

                if mrp_group_rowxs:
                    for i in range(5, 15):
                        sheet.write_formula(last_pro_rowx, i, get_sum_formula_from_list(i, mrp_group_rowxs), content_formats['format_group_float'])

                last_pro_rowx = rowx
                product_group_rowxs.append(rowx + 1)
                rowx += 1
                mrp_ids, mrp_group_rowxs = [], []

            if line['mrp_id'] not in mrp_ids:
                mrp_ids.append(line['mrp_id'])
                sheet.write(rowx, 0, '', content_formats['format_sub_title'])
                sheet.merge_range(rowx, 1, rowx, 4, line['mrp_name'], content_formats['format_sub_title'])

                if last_mrp_rowx != -1:
                    for i in range(5, 15):
                        sheet.write_formula(last_mrp_rowx, i, get_sum_formula(group_start_rowx + 1, group_end_rowx + 1, i), content_formats['format_sub_title_float'])

                last_mrp_rowx = rowx
                mrp_group_rowxs.append(rowx + 1)
                rowx += 1
                group_start_rowx = rowx

            sheet.write(rowx, 0, raw_seq, content_formats['format_content_center'])
            sheet.write(rowx, 1, str(get_day_like_display(line['date'], self.env.user)), content_formats['format_content_center'])
            sheet.write(rowx, 2, line['default_code'], content_formats['format_content_left'])
            sheet.write(rowx, 3, line['barcode'], content_formats['format_content_left'])
            sheet.write(rowx, 4, line['pt_raw_material_name'], content_formats['format_content_left'])
            sheet.write(rowx, 5, line['standard_price'], content_formats['format_content_float'])
            sheet.write(rowx, 6, line['gross_weigth'], content_formats['format_content_float'])
            sheet.write(rowx, 7, line['net_weight'], content_formats['format_content_float'])
            sheet.write(rowx, 8, line['price_unit'], content_formats['format_content_float'])
            sheet.write_formula(rowx, 9, get_arithmetic_formula(5, rowx, 6, rowx, '*'), content_formats['format_content_float'])
            sheet.write_formula(rowx, 10, get_arithmetic_formula(6, rowx, 8, rowx, '*'), content_formats['format_content_float'])
            sheet.write_formula(rowx, 11, get_arithmetic_formula(7, rowx, 8, rowx, '*'), content_formats['format_content_float'])
            sheet.write_formula(rowx, 12, get_arithmetic_formula(6, rowx, 7, rowx, '-'), content_formats['format_content_float'])
            sheet.write_formula(rowx, 13, get_arithmetic_formula(10, rowx, 11, rowx, '-'), content_formats['format_content_float'])
            sheet.write_formula(rowx, 14, get_arithmetic_formula(9, rowx, 10, rowx, '-'), content_formats['format_content_float'])

            raw_seq += 1
            group_end_rowx = rowx
            rowx += 1

        if lines:
            if mrp_group_rowxs:
                for i in range(5, 15):
                    sheet.write_formula(last_pro_rowx, i, get_sum_formula_from_list(i, mrp_group_rowxs), content_formats['format_group_float'])
            if last_mrp_rowx != -1:
                for i in range(5, 15):
                    sheet.write_formula(last_mrp_rowx, i, get_sum_formula(group_start_rowx + 1, rowx, i), content_formats['format_sub_title_float'])

            sheet.merge_range(rowx, 0, rowx, 4, _('Total amount').upper(), content_formats['format_title_float_center'])
            for i in range(5, 15):
                sheet.write_formula(rowx, i, get_sum_formula_from_list(i, product_group_rowxs), content_formats['format_title_float'])

        return sheet, rowx

    def get_footer(self, sheet, rowx, header_formats):
        executive_director = self.env['hr.employee'].search([('user_id', '=', self.company_id.executive_signature.sudo().id)]) if self.company_id.executive_signature else False
        general_accountant = self.env['hr.employee'].search([('user_id', '=', self.company_id.genaral_accountant_signature.sudo().id)]) if self.company_id.genaral_accountant_signature else False
        executive_director_name = ("%s%s" % (executive_director.name, ".%s" % (executive_director.last_name[0] if executive_director.last_name else ""))) if executive_director else "                          "
        general_accountant_name = ("%s%s" % (general_accountant.name, ".%s" % (general_accountant.last_name[0] if general_accountant.last_name else ""))) if general_accountant else "                          "

        sheet.merge_range(rowx, 1, rowx, 2, "%s:" % _('Executive Director'), header_formats['format_filter'])
        sheet.merge_range(rowx, 3, rowx, 14, "........................................... (%s)" % executive_director_name, header_formats['format_filter'])
        rowx += 2
        sheet.merge_range(rowx, 1, rowx, 2, "%s:" % _('General Accountant'), header_formats['format_filter'])
        sheet.merge_range(rowx, 3, rowx, 14, "........................................... (%s)" % general_accountant_name, header_formats['format_filter'])

        return sheet, rowx

    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Manufacturing fluctuation report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_content_header = book.add_format(ReportExcelCellStyles.format_content_header)
        format_group_left = book.add_format(ReportExcelCellStyles.format_group_left)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_content_center = book.add_format(ReportExcelCellStyles.format_content_center)
        format_content_left = book.add_format(ReportExcelCellStyles.format_content_left)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_title_float = book.add_format(ReportExcelCellStyles.format_title_float)
        format_title_float_center = book.add_format(ReportExcelCellStyles.format_title_float_center)
        format_sub_title = book.add_format(ReportExcelCellStyles.format_sub_title)
        format_sub_title_float = book.add_format(ReportExcelCellStyles.format_sub_title_float)
        format_sub_title_center = book.add_format(ReportExcelCellStyles.format_sub_title_center)

        header_formats = {
            'format_filter': format_filter,
            'format_name': format_name,
        }
        content_formats = {
            'format_content_header': format_content_header,
            'format_group_left': format_group_left,
            'format_group_float': format_group_float,
            'format_content_center': format_content_center,
            'format_content_left': format_content_left,
            'format_content_float': format_content_float,
            'format_title_float': format_title_float,
            'format_title_float_center': format_title_float_center,
            'format_sub_title': format_sub_title,
            'format_sub_title_float': format_sub_title_float,
            'format_sub_title_center': format_sub_title_center,
        }

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('manufacturing_fluctuation_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet = self.get_sheet_format(sheet)

        # compute column
        sheet.set_column('A:A', 3)
        sheet.set_column('B:D', 12)
        sheet.set_column('E:E', 20)
        sheet.set_column('G:H', 8)
        sheet.set_column('I:O', 12)

        # create header
        rowx = 1
        sheet, rowx = self.get_header(sheet, header_formats, rowx, report_name)

        # create content header
        sheet.write(rowx, 0, _('№'), format_content_header)
        sheet.write(rowx, 1, _('Date'), format_content_header)
        sheet.write(rowx, 2, _('Product code'), format_content_header)
        sheet.write(rowx, 3, _('Bar code'), format_content_header)
        sheet.write(rowx, 4, _('Raw material'), format_content_header)
        sheet.write(rowx, 5, _('Standard cost'), format_content_header)
        sheet.write(rowx, 6, _('Net weight'), format_content_header)
        sheet.write(rowx, 7, _('Gross weight'), format_content_header)
        sheet.write(rowx, 8, _('Unit cost'), format_content_header)
        sheet.write(rowx, 9, _('Total standard cost'), format_content_header)
        sheet.write(rowx, 10, _('Total cost(net weight)'), format_content_header)
        sheet.write(rowx, 11, _('Total cost(gross weight)'), format_content_header)
        sheet.write(rowx, 12, _('Weight difference'), format_content_header)
        sheet.write(rowx, 13, _('Gross, net weight difference'), format_content_header)
        sheet.write(rowx, 14, _('Standard cost, net weight difference'), format_content_header)
        sheet.set_row(rowx, 50)
        rowx += 1

        # create content
        sheet, rowx = self.get_value(sheet, rowx, content_formats)

        # create footer
        rowx += 3
        sheet, rowx = self.get_footer(sheet, rowx, header_formats)

        # close workbook and export report
        book.close()
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        return report_excel_output_obj.export_report()