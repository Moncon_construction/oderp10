# -*- coding: utf-8 -*-

{
    'name': 'Mongolian Bom Substitution',
    'version': '2.1',
    'category': 'Mongolian Modules',
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'depends': ['l10n_mn_web', 'l10n_mn_stock', 'l10n_mn_mrp_account'],
    'description': """
        For the continuous providing of the bom's material.
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/bom_substitution_views.xml',
        'views/mrp_bom_views.xml',
        'views/mrp_production_views.xml',
    ],
    'application': True,
    'installable': True,
}
