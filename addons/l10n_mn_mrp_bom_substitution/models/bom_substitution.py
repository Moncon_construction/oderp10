# -*- coding: utf-8 -*-

from datetime import datetime

import odoo.addons.decimal_precision as dp
from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError


class BomSubstitution(models.Model):
    _name = 'bom.substitution'
    _inherit = 'mail.thread'

    def _get_default_sequence(self):
        max_sq = 1

        if 'default_bom_line_id' in self._context.keys() and self._context['default_bom_line_id'] and isinstance(self._context['default_bom_line_id'], (int, long)):
            self._cr.execute("""
                SELECT MAX(sequence) AS seq FROM bom_substitution
                WHERE bom_line_id = %s
            """ % self._context['default_bom_line_id'])
            result = self._cr.dictfetchall()

            max_sq = (result[0]['seq'] or 0 + 1) if result and len(result) > 0 else 1

        return max_sq

    company_id = fields.Many2one('res.company', default=lambda self: self.env.user.company_id, string='Company')

    sequence = fields.Integer(string='Sequence', default=_get_default_sequence, required=True)
    product_id = fields.Many2one('product.product', string='Substitutable Product')
    product_uom_id = fields.Many2one('product.uom', 'Unit of Measure', related='product_id.product_tmpl_id.uom_id')
    weight = fields.Float(string='Weight', digits=dp.get_precision('Stock Weight'), default=1.0)
    bom_line_id = fields.Many2one('mrp.bom.line', string='Bom Line')
    bom_id = fields.Many2one('mrp.bom', related='bom_line_id.bom_id', store=True, search='_search_bom', string='Bom')

    @api.multi
    def _search_bom(self, operator, value):
        res = []
        if value:
            res = self.env['bom.substitution'].search(['|', '|', '|', ('bom_line_id.bom_id.code', 'ilike', value), ('bom_line_id.bom_id.product_tmpl_id.name', 'ilike', value), ('bom_line_id.bom_id.product_tmpl_id.barcode', 'ilike', value), ('bom_line_id.bom_id.product_tmpl_id.default_code', 'ilike', value)])
        return [('id', operator, res)]

    @api.onchange('bom_line_id')
    def set_domain_for_product_id(self):
        domain = []
        parent_pro_ids = []
        bom_line = self.bom_line_id

        if 'default_bom_line_id' in self._context.keys() and self._context['default_bom_line_id'] and isinstance(self._context['default_bom_line_id'], (int, long)):
            bom_line = self.env['mrp.bom.line'].browse(self._context['default_bom_line_id'])

        if bom_line and bom_line.product_id:
            parent_pro_ids = [bom_line.product_id.id]
        if bom_line.bom_id and bom_line.bom_id.product_tmpl_id:
            bom_products = self.env['product.product'].sudo().search([('product_tmpl_id', '=', bom_line.bom_id.product_tmpl_id.id)])
            if bom_products and len(bom_products) > 0:
                parent_pro_ids.extend(bom_products.ids)

        if parent_pro_ids:
            domain.append(('id', 'not in', parent_pro_ids))

        if self.bom_line_id.product_uom_id:
            domain.append(('product_tmpl_id.uom_id', '=', self.bom_line_id.product_uom_id.id))

        if len(domain) > 0:
            return {'domain': {'product_id': domain}}

    @api.onchange('weight')
    def check_weight_is_zero(self):
        for obj in self:
            if obj.weight == 0:
                raise ValidationError(_("Weight cannot be zero !!!"))

            if obj.weight < 0:
                raise ValidationError(_("Weight must be positive !!!"))

    @api.onchange('product_id')
    def check_product_is_duplicated(self):
        for obj in self:
            if obj.product_id and obj.bom_line_id and obj.bom_line_id.bom_id and (obj.product_id.product_tmpl_id == obj.bom_line_id.bom_id.product_tmpl_id or obj.product_id.product_tmpl_id == obj.bom_line_id.bom_id.product_id.product_tmpl_id):
                raise ValidationError(_('Substitution product cannot be same as BoM product !!!'))

            if obj.product_id and obj.bom_line_id and obj.product_id.product_tmpl_id == obj.bom_line_id.product_id.product_tmpl_id:
                raise ValidationError(_('Substitution product cannot be same as BoM Line product \'%s\' !!!') % obj.bom_line_id.product_id.display_name)

    @api.onchange('product_id', 'product_uom_id')
    def check_uom_is_same(self):
        for obj in self:
            if obj.product_id and obj.product_id.uom_id and obj.product_id.uom_id != obj.bom_line_id.product_uom_id:
                raise ValidationError(_("Product UOM must be same from MRP Bom !!!"))
