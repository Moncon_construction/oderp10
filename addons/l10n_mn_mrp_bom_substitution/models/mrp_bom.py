# -*- coding: utf-8 -*-

from datetime import datetime

from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError


class MrpBomLine(models.Model):
    _inherit = 'mrp.bom.line'

    substitution_ids = fields.One2many('bom.substitution', 'bom_line_id', string='Bom Substitution')

    @api.constrains('product_id', 'substitution_ids')
    def _check_product_recursion(self):
        for bom_line in self:
            if bom_line.substitution_ids.filtered(lambda x: x.product_id.product_tmpl_id == bom_line.product_id.product_tmpl_id):
                raise ValidationError(_('Substitution product cannot be same as BoM Line product \'%s\' !!!') % bom_line.product_id.display_name)

            if bom_line.substitution_ids.filtered(lambda x: x.product_id.product_tmpl_id == bom_line.bom_id.product_tmpl_id or x.product_id.product_tmpl_id == bom_line.bom_id.product_id.product_tmpl_id):
                raise ValidationError(_('Substitution product cannot be same as BoM product !!!'))

    @api.constrains('product_id', 'product_uom_id', 'substitution_ids')
    def _check_uom_is_same(self):
        for bom_line in self:
            if bom_line.substitution_ids.filtered(lambda x: x.product_id.uom_id != bom_line.product_uom_id):
                raise ValidationError(_('All substitution product \'%s\'\'s uom must be same as \'%s\' !!!') % (bom_line.product_id.display_name, bom_line.product_id.uom_id.display_name))

