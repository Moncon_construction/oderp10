# -*- coding: utf-8 -*-

import math
from collections import defaultdict

from odoo import _, api, fields, models
from odoo.addons import decimal_precision as dp
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import UserError
from odoo.tools import float_round


class MrpProduction(models.Model):
    _inherit = 'mrp.production'

    @api.multi
    def action_assign(self):
        res = super(MrpProduction, self).action_assign()
        
        """
            @Override:
                1. Хангасан материалаараа гүйлгэж БЭЛЭН ТОО ХЭМЖЭЭ нь ХАНГАХ тоо хэмжээнээс бага орлуулах бараа бүхий мөрүүдийг олох
                2. Дээрх нөхцөл бүхий мөрүүдэд орлох бараа байгаа эсэхийг шалгаж, орлох барааны боломжит тоо хэмжээгээр хөдөлгөөн үүсгэх
        """

        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure') or 2
        for order in self.filtered(lambda x: x.qty_produced == 0):
            if order.move_raw_ids:
                    
                for move in order.move_raw_ids:
                    # Дэд үйлдвэрлэл хийгдсэн бол орлуулалт хийгдэхгүй
                    if move.assigned_from_mrp:
                        continue
                    avlble_qty = move.quantity_available if move.quantity_available > 0 else 0
                    must_produce_qty = move.product_uom_qty
                    produced_qty = must_produce_qty if must_produce_qty <= avlble_qty else avlble_qty # Үйлдвэрлэлд оролцох боломжтой тоо хэмжээ
                        
                    # Орлох бараа бүхий хангах материалын БЭЛЭН ТОО ХЭМЖЭЭ дутсан бол ОРЛОХ БАРААнуудаас хөдөлгөөн үүсгэх
                    if round(produced_qty, precision) < round(must_produce_qty, precision) and move.bom_line_id and move.bom_line_id.substitution_ids and move.state not in ('assigned', 'done'):
                        move.quantity_done = 0

                        bom_line = move.bom_line_id
                        bom_qty = bom_line.bom_id.product_qty
                        bom_line_qty = bom_line.product_qty
                        production = move.raw_material_production_id
                        source_location = production.location_src_id
                        
                        lasts_qty = (must_produce_qty - produced_qty) * order.product_qty / must_produce_qty  # Барааг үйлдвэрлэх шаардлагатай үлдсэн тоо хэмжээ
                        original_quantity = (order.product_qty - order.qty_produced) or 1.0

                        NEW_MOVES = self.env['stock.move']
                        substitutions = sorted(move.bom_line_id.substitution_ids, key=lambda x: x.sequence, reverse=False)

                        # Орц дах орлох барааны тоо хэмжээг харьцуулж хөдөлгөөн үүсгэх давталт
                        while substitutions:
                            current_sbstttn = substitutions[0]
                            substitutions = substitutions[1:]

                            if not current_sbstttn.product_id or current_sbstttn.weight == 0 or lasts_qty == 0:
                                continue

                            current_sbstttn_qty = current_sbstttn.weight
                            current_sbstttn_avlble_qty = current_sbstttn.product_id.get_qty_availability([source_location.id], order.date_planned_start)  # Орлох барааны агуулах дах боломжит тоо хэмжээ
                            current_sbstttn_must_produce_qty = lasts_qty * current_sbstttn_qty / bom_qty  # Орлох барааны тухайн барааг үйлдвэрлэхэд шаардлагатай тоо хэмжээ
                            current_sbstttn_produced_qty = current_sbstttn_must_produce_qty if current_sbstttn_must_produce_qty <= current_sbstttn_avlble_qty else current_sbstttn_avlble_qty # Орлох барааны тухайн барааг үйлдвэрлэхэд орсон тоо хэмжээ

                            if round(current_sbstttn_avlble_qty, precision) <= 0 or round(current_sbstttn_produced_qty, precision) <= 0 or round(current_sbstttn_must_produce_qty, precision) <= 0:
                                continue

                            lasts_qty = ((current_sbstttn_must_produce_qty - current_sbstttn_produced_qty) * lasts_qty / current_sbstttn_must_produce_qty)  # Орлох барааг үйлдвэрлэсний дараах барааг үйлдвэрлэх шаардлагатай үлдсэн тоо хэмжээ
                            
                            NEW_MOVES += self.env['stock.move'].create({
                                'sequence': bom_line.sequence,
                                'state': 'assigned',
                                'name': production.name,
                                'date': production.date_planned_start,
                                'date_expected': production.date_planned_start,
                                'bom_line_id': bom_line.id,
                                'substitutabled_product_id': move.product_id.id,
                                'product_id': current_sbstttn.product_id.id,
                                'product_uom_qty': current_sbstttn_produced_qty,
                                'product_uom': current_sbstttn.product_uom_id.id,
                                'location_id': source_location.id,
                                'location_dest_id': order.product_id.property_stock_production.id,
                                'raw_material_production_id': order.id,
                                'company_id': order.company_id.id,
                                'operation_id': bom_line.operation_id.id or False,
                                'price_unit': current_sbstttn.product_id.standard_price,
                                'procure_method': 'make_to_stock',
                                'origin': order.name,
                                'warehouse_id': source_location.get_warehouse().id,
                                'group_id': order.procurement_group_id.id,
                                'propagate': order.propagate,
                                'unit_factor': current_sbstttn_produced_qty / original_quantity,
                            })

                            if lasts_qty == 0:
                                substitutions = []

                        # Хэрвээ үйлдвэрлэх тоо хэмжээ орлох бараанаас ч тооцоолоод хүрэхгүй бол БҮРЭЛДЭХҮҮН ХЭСЭГ дэх үндсэн барааны хөдөлгөөний тоо хэмжээг үлдсэн тоогоор шинэчлэх
                        lasts_qty = lasts_qty * bom_line_qty / bom_qty
                        lasts_qty += produced_qty
                        if round(lasts_qty, precision) > 0:
                            move.write({'product_uom_qty': lasts_qty, 'unit_factor': lasts_qty / original_quantity})
                        else:
                            qry = """
                                /* Агуулахын хөдөлгөөний журналыг устгах */
                                DELETE FROM account_move WHERE id IN
                                (
                                    SELECT move_id FROM account_move_line WHERE stock_move_id = %s
                                );
    
                                /* Агуулахын хөдөлгөөнөөс үүссэн лотуудыг устгах */
                                DELETE FROM stock_move_lots WHERE move_id = %s;
                                DELETE FROM stock_move WHERE id = %s;
                            """ % (move.id, move.id, move.id)
                            self._cr.execute(qry)

                # Үйлдвэрлэлээс хөдөлгөөн үүсгэчихээд хөдөлгөөн дээр тооцоолол хийдэг core дах функцүүдийг дуудав.
                # Check for all draft moves whether they are mto or not
                order._adjust_procure_method()

        return res

    @api.multi
    def cancel_mrp_by_force(self):
        res = super(MrpProduction, self).cancel_mrp_by_force()

        # Орлох бараанаас үүссэн хөдөлгөөнүүдийг устгах
        removable_moves = self.move_raw_ids.filtered(lambda self: self.substitutabled_product_id)
        removable_moves.write({'state': 'draft', 'quantity_done_store': 0})
        removable_moves.unlink()

        for production in self:
            if production.move_raw_ids:
                # Орлох бараанаас үүсээгүй хөдөлгөөнүүдийн тоо хэмжээг үйлдвэрлэх тоогоор шинэчлэх
                for move in production.move_raw_ids.filtered(lambda obj: not obj.substitutabled_product_id):
                    if move.bom_line_id:
                        qty = (self.product_qty * move.bom_line_id.product_qty / move.bom_line_id.bom_id.product_qty) if move.bom_line_id.bom_id and move.bom_line_id.bom_id.product_qty else move.product_uom_qty
                        move.write({'product_uom_qty': qty})
            else:
                # Хангах материалын хөдөлгөөн цуцалсны дараа үлдээгүй бол хангах материалын мөрүүдийг ахин шинэчлэх 
                # /Үндсэн орцын боломжит тоо нь 0 бгаад рлох барааны тоо хэмжээ нь үйлдвэрлэл хөтлөхөд хүрэлцсэн бол уг нөхцөл үүснэ./
                factor = production.product_uom_id._compute_quantity(production.product_qty, production.bom_id.product_uom_id) / production.bom_id.product_qty
                boms, lines = production.bom_id.explode(production.product_id, factor, picking_type=production.bom_id.picking_type_id)
                production._generate_raw_moves(lines)

        return res
