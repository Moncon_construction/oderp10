# -*- coding: utf-8 -*-
{
    'name': 'Sub Product Additional Features',
    'version': '1.0',
    'category': 'Mongolian Modules',
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'sequence': 15,
    'depends': ['mrp_byproduct','l10n_mn_mrp','l10n_mn_mrp_account'],
    'description': """
        Үйлдвэрлэлийн орцын дайвар бүртгэлийн нэмэлт хэсгүүд.
    """,
    'data': [
        'views/mrp_sub_product_views.xml',
        'views/mrp_production_view.xml'
    ],
    'demo': [
        ],
    'application': True,
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}
