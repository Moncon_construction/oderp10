# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models

class MrpBom(models.Model):
    _inherit = 'mrp.subproduct'

    real_qty = fields.Float('Real qty')
    subproduct_location_dest_id = fields.Many2one('stock.location', 'Sub Product Location', required=True)
