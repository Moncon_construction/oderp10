# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Change MRP Date',
    'version': '1',
    'category': 'Manufacturing',
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'sequence': 15,
    'depends': ['mrp'],
    'description': """
        Change MRP Date
    """,
    'data': [
        'views/mrp_change_date_views.xml'
    ],
    'demo': [
        ],
    'application': True,
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}