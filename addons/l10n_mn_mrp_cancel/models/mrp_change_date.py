# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-Today Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################

from odoo import api, fields, models, tools, SUPERUSER_ID, _
from odoo.exceptions import UserError, ValidationError

class change_mrp_product_date(models.Model):
    _name = 'change.mrp.date'
    
    new_date = fields.Datetime("New Date")

    @api.multi
    def change_date(self):
        active_id = self.env.context.get('active_id')
        move = self.env['stock.move'].search([('production_id', '=', active_id)])
        move1 = self.env['stock.move'].search([('raw_material_production_id', '=', active_id)])

        for obj in self:
            if obj.new_date:
                if len(move1) > 1:
                    self.env.cr.execute(""" UPDATE stock_move SET date = '%s', date_expected = '%s' WHERE id in %s""" % (obj.new_date, obj.new_date, tuple(move1.ids)))
                    self.env.cr.execute(""" UPDATE account_move_line SET date = '%s' WHERE stock_move_id in %s""" % (obj.new_date, tuple(move1.ids)))
                    self.env.cr.execute(""" UPDATE account_move SET date = '%s' WHERE id in (SELECT DISTINCT(move_id) FROM account_move_line WHERE stock_move_id in %s)"""
                                            % (obj.new_date, tuple(move1.ids)))
                elif len(move1) == 1:
                    self.env.cr.execute(""" UPDATE stock_move SET date = '%s', date_expected = '%s' WHERE id = %s""" % (obj.new_date, obj.new_date, move1.id))
                    self.env.cr.execute(""" UPDATE account_move_line SET date = '%s' WHERE stock_move_id = %s""" % (obj.new_date, move1.id))
                    self.env.cr.execute(""" UPDATE account_move SET date = '%s' WHERE id in (SELECT DISTINCT(move_id) FROM account_move_line WHERE stock_move_id = %s)"""
                                            % (obj.new_date, move1.id))
                if len(move) > 1:
                    self.env.cr.execute(""" UPDATE stock_move SET date = '%s', date_expected = '%s' WHERE id in %s""" % (obj.new_date, obj.new_date, tuple(move.ids)))
                    self.env.cr.execute(""" UPDATE account_move_line SET date = '%s' WHERE stock_move_id in %s""" % (obj.new_date, tuple(move.ids)))
                    self.env.cr.execute( """ UPDATE account_move SET date = '%s' WHERE id in (SELECT DISTINCT(move_id) FROM account_move_line WHERE stock_move_id in %s)"""
                                            % (obj.new_date, tuple(move.ids)))
                elif len(move) == 1:
                    self.env.cr.execute(""" UPDATE stock_move SET date = '%s', date_expected = '%s' WHERE id = %s""" % (obj.new_date, obj.new_date, move.id))
                    self.env.cr.execute(""" UPDATE account_move_line SET date = '%s' WHERE stock_move_id = %s""" % (obj.new_date, move.id))
                    self.env.cr.execute(""" UPDATE account_move SET date = '%s' WHERE id in (SELECT DISTINCT(move_id) FROM account_move_line WHERE stock_move_id = %s)"""
                                            % (obj.new_date, move.id))
                self.env.cr.execute(""" UPDATE mrp_production SET date_finished = '%s' WHERE id = %s""" % (obj.new_date, active_id))
        return True