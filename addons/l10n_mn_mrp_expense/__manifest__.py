# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Mongolian Manufacturing Expense',
    'version': '1',
    'category': 'Mongolian Modules',
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'sequence': 15,
    'depends': ['l10n_mn_mrp', 'l10n_mn_mrp_account', 'l10n_mn_product_expense'],
    'description': """
        Production raw materials for demand.
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/ir_sequence.xml',
        'wizard/create_expense_view.xml',
        'views/mrp_production_view.xml',
        'views/expense_view.xml',
    ],
    'demo': [
        ],
    'application': True,
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
