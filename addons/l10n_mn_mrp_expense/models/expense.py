# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError

class Expense(models.Model):
    _inherit = 'product.expense'

    production_id = fields.Many2one('mrp.production', string='Production')
    production_expense = fields.Boolean(string='Production Expense')
    raw_material_id = fields.Many2one('mrp.raw.material')
    expense_type = fields.Selection([('simple', 'Simple'),
                             ('mrp_expense', 'Mrp Expense')], 'Type', default='simple')

    expense_mrp_raw_material = fields.One2many('mrp.raw.material', 'expense_id', string="Expense mrp raw material")

    '''
        Гарах хүргэлт үүсгэх функц
    '''
    @api.multi
    def create_out_picking(self):
        stock_picking = self.env['stock.picking']
        stock_move = self.env['stock.move']
        stock_picking_type = self.env['stock.picking.type']
        outgoing_stock_picking_type = stock_picking_type.search([('code', '=', 'outgoing'),
                                                                 ('warehouse_id', '=', self.warehouse.id)])
        outgoing_stock_picking_type_id = None
        if outgoing_stock_picking_type:
            for ospt in outgoing_stock_picking_type:
                outgoing_stock_picking_type_id = ospt.id
        des_location = self.env['stock.location'].search([('scrap_location', '=', True)])
        for expense in self:
            if expense.production_id:
                values = {'expense': expense.id,
                          'origin': expense.code,
                          'company_id': expense.company.id,
                          'picking_type_id': outgoing_stock_picking_type_id,
                          'location_dest_id': des_location[0].id,
                          'location_id': expense.warehouse.lot_stock_id.id}
                stock_picking_expense = stock_picking.create(values)
                stock_picking_expense.message_post_with_view(
                        'l10n_mn_product_expense.message_link_for_picking_expense',
                        values = {
                            'self': stock_picking_expense,
                            'origin': expense
                        },
                        subtype_id=self.env.ref('mail.mt_note').id
                )
                if expense.expense_line:
                    for line in expense.expense_line:
                        move_lines_values = line._get_stock_move_vals(stock_picking_expense.id, expense.warehouse.lot_stock_id.id, des_location[0].id)
                        move_lines_values.update({'expense_line_id': line.id})
                        move_lines_values.update({'move_raw_material_id': line.expense_raw_material_id.id})
                        stock_move.create(move_lines_values)
            else:
                return super(Expense, self).create_out_picking()
    def unlink(self):
        for obj in self:
            related_expense = self.env['mrp.raw.material'].search([('related_expense', '=', obj.id)])
            if related_expense:
                for related in related_expense:
                    related.related_expense = ''
                    related.expense_qty = 0
        return super(Expense,  self).unlink()

    @api.multi
    def calculate(self):
        self.expense_line.unlink()
        product_ids = {}
        for obj in self.expense_mrp_raw_material:
            if obj.product_id.id not in product_ids:
                product_ids[obj.product_id.id] = {'qty_available': 0, 'mrp': [obj.id]}
            else:
                if obj.mrp_production.id not in product_ids[obj.product_id.id]['mrp']:
                    product_ids[obj.product_id.id]['mrp'].append(obj.id)
            product_ids[obj.product_id.id]['qty_available'] += obj.quantity_available
        for key, value in product_ids.items():
            if value['qty_available'] > 0:
                expense_line = {
                    'expense': self.id,
                    'name': 'mrp expense',
                    'product': key,
                    'quantity': value['qty_available'],
                    'expense_mrp_production_ids': [(6, 0, value['mrp'])]
                }
                self.env['product.expense.line'].create(expense_line)

    @api.multi
    def approve(self):
        if self.expense_type == 'mrp_expense':
            product_ids = {}
            for obj in self.expense_mrp_raw_material:
                if obj.product_id.id not in product_ids:
                    product_ids[obj.product_id.id] = {'qty_available': 0}
                product_ids[obj.product_id.id]['qty_available'] += obj.quantity_available
            for key, value in product_ids.items():
                if value['qty_available'] > 0:
                    for expense in self.expense_line:
                        if expense.product.id == key and expense.quantity != value['qty_available']:
                            raise UserError(_("Please click the Calculator first because the quantity is different from the quantity produced!"))
        return super(Expense, self).approve()


class ExpenseLine(models.Model):
    _inherit = 'product.expense.line'

    expense_raw_material_id = fields.Many2one('mrp.raw.material', string='Raw material')
    expense_mrp_production_ids = fields.Many2many('mrp.raw.material', string='Mrp production')





