# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models

class MrpProduction(models.Model):

    _inherit = 'mrp.production'


    mrp_raw_material_ids = fields.One2many('mrp.raw.material', 'mrp_production')

    @api.model
    def create(self, vals):
        res = super(MrpProduction, self).create(vals)
        bom_id = self.env['mrp.bom'].search([('id', '=', res.bom_id.id)], limit=1)


        for line_ids in bom_id.bom_line_ids:
            module = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_stock_account_cost_for_each_wh')])
            if module.state == 'installed':
                price_unit = self.env['product.warehouse.standard.price'].search([('warehouse_id', '=', res.picking_type_id.warehouse_id.id), ('product_id', '=', line_ids.product_id.id)], limit=1).standard_price

            else:
                price_unit = line_ids.product_id.standard_price
            bom_data = {
                'mrp_production': res.id,
                'product_id': line_ids.product_id.id,
                'product_uom': line_ids.product_uom_id.id,
                'quantity_available': line_ids.product_qty,
                'price_unit': price_unit
            }
            self.env['mrp.raw.material'].create(bom_data)

        return res
    @api.multi
    def button_mark_done(self):
        finish_price_unit_sum = 0
        for line_ids in self.mrp_raw_material_ids:
            module = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_stock_account_cost_for_each_wh')])
            if module.state == 'installed':
                price_unit = self.env['product.warehouse.standard.price'].search(
                    [('warehouse_id', '=',self.picking_type_id.warehouse_id.id),
                     ('product_id', '=', line_ids.product_id.id)], limit=1).standard_price
                line_ids.write({'price_unit': price_unit})
                finish_price_unit_sum += line_ids.price_unit * line_ids.quantity_available

            else:
                price_unit = line_ids.product_id.standard_price
                line_ids.write({'price_unit': price_unit})
                finish_price_unit_sum += line_ids.price_unit * line_ids.quantity_available
        for move_finshed_id in self.move_finished_ids:
            move_finshed_id.write({'price_unit': finish_price_unit_sum/move_finshed_id.quantity_done if move_finshed_id.quantity_done else 0})
        return super(MrpProduction, self).button_mark_done()

    @api.multi
    def _generate_moves(self):
        for production in self:
            production._generate_finished_moves()
        return True



class MrpRawMaterial(models.Model):

    _name = 'mrp.raw.material'

    expense_id = fields.Many2one('product.expense', string='Product expense', copy=False)
    mrp_raw_material = fields.Many2one('mrp.raw.material')
    mrp_production = fields.Many2one('mrp.production', string='Mrp production')
    product_id = fields.Many2one('product.product', 'Product', required="True")
    product_uom = fields.Many2one('product.uom', 'Product Unit of Measure', required="True")
    quantity_available = fields.Float('Quantity Available')
    price_unit = fields.Float('Price Unit')
    expense_qty = fields.Float('Expense Qty')
    granted_qty = fields.Float('Granted Qty', compute='_get_granted_qty')
    related_expense = fields.Many2one('product.expense', copy=False)
    expense_state = fields.Selection(related="related_expense.state", string='Expense state')


    @api.multi
    def name_get(self):
        res = []
        for obj in self:
            qty = 0
            if obj.quantity_available - obj.expense_qty > 0:
                qty = obj.quantity_available - obj.expense_qty
            name = '[' + str(obj.mrp_production.name) + '] ' + '[' + obj.product_id.product_tmpl_id.name + ']' +'[' + str(qty) + '] '
            res.append((obj.id, name))
        return res

    @api.onchange('mrp_raw_material')
    def onchange_mrp_raw_material(self):
        qty = self.mrp_raw_material.quantity_available - self.mrp_raw_material.expense_qty
        self.quantity_available = qty if qty > 0 else 0
        self.product_id = self.mrp_raw_material.product_id.id
        self.product_uom = self.mrp_raw_material.product_id.product_tmpl_id.uom_id.id
        self.mrp_production = self.mrp_raw_material.mrp_production.id

    @api.onchange('product_id')
    def onchange_product_id(self):
        self.product_uom = self.product_id.product_tmpl_id.uom_id.id

    @api.multi
    def _get_granted_qty(self):
        for move in self:
            expenses = self.env['product.expense'].search([('production_id', '=', move.mrp_production.id)])
            for expense in expenses:
                pickings = self.env['stock.picking'].search([('expense', '=', expense.id), ('state', '=', 'done')])
                for pick in pickings:
                    for stock_move in pick.move_lines:
                        if stock_move.move_raw_material_id.id == move.id and stock_move.state == 'done':
                            move.granted_qty = stock_move.product_uom_qty

