# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models

class StockMove(models.Model):
    _inherit = 'stock.move'

    expense_line_id = fields.Many2one('product.expense.line', string='Expense Line')
    move_raw_material_id = fields.Many2one('mrp.raw.material', string='Move Raw material')
