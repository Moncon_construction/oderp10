# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError

class CreateExpenseMrp(models.TransientModel):
    _name = "create.expense.mrp"
    _description = "Create Expense from MRP"

    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse', required=True)
    production_id = fields.Many2one('mrp.production', string='MRP Production', required=True)
    note = fields.Char(string='Note', required=True)
    employee_id = fields.Many2one('hr.employee', string='Employee', default=lambda self: self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1))
    department_id = fields.Many2one('hr.department', string='Department')

    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        self.department_id = self.employee_id.department_id

    @api.model
    def default_get(self, fields):
        res = {}
        line_ids = []
        production = self.env['mrp.production'].browse(self.env.context.get('active_id'))
        if production:
            for mrp_raw_material_id in production.mrp_raw_material_ids:
                quantity = mrp_raw_material_id.quantity_available - mrp_raw_material_id.expense_qty
                if quantity > 0:
                    line_ids.append((0, 0, {'product_id': mrp_raw_material_id.product_id.id, 'product_qty': quantity, 'expense_raw_material_id': mrp_raw_material_id.id}))

            if not line_ids:
                raise UserError(_("No products to return (only lines in Done state and not fully returned yet can be returned)!"))
            if 'production_id' in fields:
                res.update({'production_id': production.id})
            if 'line_ids' in fields:
                res.update({'line_ids': line_ids})
            if 'note' in fields:
                res.update({'note': str(production.name) + ' [' + str(production.product_id.default_code) + '] ' + str(production.product_id.name) + '-д бараа материал хүсэв'})
        return res

    @api.multi
    def _create_expense(self):
        expense_line = self.env['product.expense.line']
        production_id = self.env['mrp.production'].browse(self.env.context.get('active_id'))
        expense = self.env['product.expense'].create({
            'expense_line': [],
            'production_id': self.production_id.id,
            'production_expense': True,
            'warehouse': self.warehouse_id.id,
            'stock_picking_type': self.warehouse_id.pick_type_id.id,
            'state': 'draft',
            'name': self.note,
            'employee': self.employee_id.id,
            'department': self.department_id.id,
            'account':production_id.product_id.categ_id.work_in_progress_account_id.id})
        for line in self.line_ids:
            expense_line.create(line._expense_line_from_mrp_raw_line(expense))
            for mrp_raw_material_id in production_id.mrp_raw_material_ids:
                if mrp_raw_material_id.id == line.expense_raw_material_id.id:
                    if mrp_raw_material_id.quantity_available <= line.product_qty:
                        mrp_raw_material_id.write({'expense_qty': line.product_qty})
                        mrp_raw_material_id.write({'related_expense': expense.id})
                    else:
                        expense_quantity_available = mrp_raw_material_id.quantity_available - line.product_qty
                        expense_data = {
                            'mrp_production': mrp_raw_material_id.mrp_production.id,
                            'product_id': mrp_raw_material_id.product_id.id,
                            'product_uom': mrp_raw_material_id.product_uom.id,
                            'quantity_available': expense_quantity_available,
                            'price_unit': mrp_raw_material_id.price_unit,
                        }
                        self.env['mrp.raw.material'].create(expense_data)
                        mrp_raw_material_id.write({'quantity_available': line.product_qty})
                        mrp_raw_material_id.write({'expense_qty': line.product_qty})
                        mrp_raw_material_id.write({'related_expense': expense.id})
        return expense.id

    @api.multi
    def create_expense(self):
        for wizard in self:
            new_expense_id = wizard._create_expense()
        ctx = dict(self.env.context)
        return {
            'name': _('Expense'),
            'view_type': 'form',
            'view_mode': 'form,tree',
            'res_model': 'product.expense',
            'res_id': new_expense_id,
            'type': 'ir.actions.act_window',
            'context': ctx,
        }
        
class CreateExpenseMrpLine(models.TransientModel):
    _name = "create.expense.mrp.line"
    _rec_name = 'product_id'

    wizard_id = fields.Many2one('create.expense.mrp', string="Wizard")
    product_id = fields.Many2one('product.product', string="Product")
    expense_raw_material_id = fields.Many2one('mrp.raw.material', string="Move")
    product_qty = fields.Float(string='Product Qty', required=True)



    def _expense_line_from_mrp_raw_line(self, expense):
        return {
                'expense_raw_material_id': self.expense_raw_material_id.id,
                'expense': expense.id,
                'product': self.product_id.id,
                'name': self.product_id.name,
                'quantity': self.product_qty,
        }
        
class CreateExpenseMrp(models.TransientModel):
    _inherit = "create.expense.mrp"

    line_ids = fields.One2many('create.expense.mrp.line', 'wizard_id', string='Lines')
