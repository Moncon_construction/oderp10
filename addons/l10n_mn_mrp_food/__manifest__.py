# -*- coding: utf-8 -*-

{
    'name': "Mongolian Manufacture in the FOOD INDUSTRY",
    'version': '1.0',
    'depends': ['l10n_mn_mrp', 'l10n_mn_product_food'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
    Mongolian Manufacture in the FOOD INDUSTRY
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/mrp_bom_mineral_views.xml',
        'views/mrp_bom_views.xml',
    ],
    'application': True,
    'installable': True
}
