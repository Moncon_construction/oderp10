# -*- coding: utf-8 -*-

from odoo import api, fields, models
import odoo.addons.decimal_precision as dp


class MrpBomLine(models.Model):
    _inherit = 'mrp.bom.line'
    
    chopping_processing_view = fields.Text(string='Chopping & Processing View')
    decrease_percent = fields.Float(string='Yeild Factor', digits=dp.get_precision('Product Unit of Measure'))
    net_weight = fields.Float(compute='compute_net_weight', store=True, readonly=True, digits=dp.get_precision('Product Unit of Measure'), string='Net Weight')
    
    @api.multi
    @api.depends('product_qty', 'decrease_percent')
    def compute_net_weight(self):
        for obj in self:
            obj.net_weight = obj.product_qty * (100 - obj.decrease_percent) / 100
            
    
class MrpBom(models.Model):
    _inherit = 'mrp.bom'
    
    calories = fields.Float(string='Calories')
    mineral_ids = fields.One2many('mrp.bom.mineral', 'bom_id', string='Minerals')
    
    mature_yeild_by_gr = fields.Float(string='Mature Yeild /gr/')
    image = fields.Binary(string="Photo", attachment=True, help="This field holds the image used as photo for the bom, limited to 1024x1024px.")
    
    @api.multi
    def write(self, values):
        # One2many - руу onchange-с объект нэмэхэд тухайн one2many дахь many2one объектын утгыг авч чадахгүй байсан тул давхар write дотор нь нэмэж өгөв.
        if 'product_tmpl_id' in values.keys():
            new_minerals = []
            product_minerals = self.env['product.mineral'].sudo().search([('product_tmpl_id', '=', values['product_tmpl_id'])])
             
            # Өмнөх бараанаас үүссэн илчлэгийн мөрүүдийг цэвэрлэж байна.
            self.mineral_ids.filtered(lambda self: self.is_bom_product_mineral).unlink()
                 
            # Сонгогдсон барааны эрдэс бодисуудыг нэмэж values-д дамжуулж байна.
            if product_minerals and len(product_minerals) > 0:
                for mineral in product_minerals:
                    vals = {
                        'bom_id': self.id,
                        'name': mineral.sudo().name,
                        'company_id': values['company_id'] if 'company_id' in values.keys() else self.company_id.id,
                        'qty_by_gr': mineral.sudo().qty_by_gr,
                        'is_bom_product_mineral': True
                    }
                    new_minerals.append([0, False, vals])
                values['minerals'] = new_minerals
            
            values['calories'] = self.env['product.template'].sudo().browse(values['product_tmpl_id']).calories
                 
        return super(MrpBom, self).write(values)
         
    @api.onchange('product_tmpl_id')
    def _add_minerals(self):
        # One2many - руу onchange-с объект нэмэх.
        old_minerals = []
        minerals = self.product_tmpl_id.sudo().mineral_ids
          
        # Тухайн ЭРДЭС БОДИСЫН мөрүүдийг update хийх тул өмнөх мөрүүдийг авч байна.
        for mineral in self.mineral_ids:
            # Тухайн ЭРДЭС БОДИСЫН мөрүүд бараанаас үүссэн бол авахгүй. Өөр бараа сонгож байгаа учир 
            if not mineral.is_bom_product_mineral:
                old_minerals.append((0, 0, {
                    'bom_id': self.id,
                    'name': mineral.name,
                    'company_id': self.company_id.id,
                    'qty_by_gr': mineral.qty_by_gr
                }))
           
        # Сонгогдсон бараан дах ЭРДЭС БОДИСУУДЫГ авч байна.
        if minerals and len(minerals) > 0:
            new_minerals = []
            for mineral in minerals:
                vals = {
                    'bom_id': self.id,
                    'name': mineral.sudo().name,
                    'company_id': self.company_id.id,
                    'qty_by_gr': mineral.sudo().qty_by_gr,
                    'is_bom_product_mineral': True
                }
                new_minerals.append((0, 0, vals))
                   
            # Чиглүүлэх хөтөлбөрийн мөрүүдийн утгыг шинэчилж байна.
            if len(new_minerals) > 0:
                self.update({
                    'mineral_ids': new_minerals + old_minerals,
                })
                
        self.update({
            'calories': self.product_tmpl_id.sudo().calories,
        })
 
     
    