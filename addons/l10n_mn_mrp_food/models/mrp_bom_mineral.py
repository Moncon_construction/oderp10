# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.addons.l10n_mn_product_food.models.product_mineral import MINERAL_TYPE


class BomMineral(models.Model):
    _name = 'mrp.bom.mineral'
    
    company_id = fields.Many2one('res.company', default=lambda self: self.env.user.company_id, string='Company')
    
    name = fields.Selection(MINERAL_TYPE, required=True, string='Type')
    qty_by_gr = fields.Float(required=True, string='Quantity /gr/')
    
    bom_id = fields.Many2one('mrp.bom', string='Bom')
    
    is_bom_product_mineral = fields.Boolean(default=False, string='Is Product Mineral or Not')
    
    