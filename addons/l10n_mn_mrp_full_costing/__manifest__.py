# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2009-2014 Monos Group (<http://monos.mn>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'MRP Full Absorption Costing',
    'version': '1.0',
    'category': 'Manufacture',
    'description': """
MRP Cost Calculation
===============================================================================================

Үйлдвэрлэлийн өртгийн хэлбэлзлийг тооцоолж, үйлдвэрлэлийн нэмэлт зардлыг өртөгт бүрэн шингээнэ хийнэ.
-----------------------------------------------------------------------------------------------
    """,
    'author': 'Asterisk Technologies LLC',
    'depends': ['mrp','stock','l10n_mn_account'],
    "website" : "http://asterisk-tech.mn",
    'data': [
        'views/mrp_config_setting_views.xml',
        'views/mrp_full_costing_view.xml',
        'security/ir.model.access.csv',
    ],
    'installable': True,
    'auto_install': False,
}