# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.exceptions import Warning as UserError, ValidationError

class mrp_full_costing(models.Model):
    _name = 'mrp.full.costing'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    
    STATE_SELECTION = [
        ('draft','Draft'),
        ('calculated','Calculated'),
        ('expense_allocated','Expense Allocated'),
        ('cost_updated','Cost Updated'),
        ('cancelled','Cancelled'),
    ]
    
    start_date = fields.Date(string='Start Date', required=True)
    end_date = fields.Date(string='End Date', required=True)
    produced_products = fields.Many2many(comodel_name='product.product', string='Produced Products', domain=[('type','!=','service')])
    
    account_id = fields.Many2one('account.account', string='Expense Account', required=True)
    state = fields.Selection(STATE_SELECTION, string='State', readonly=True, default='draft', track_visibility='always')
    debit = fields.Float(string='Debit', digits=(16, 2))
    credit = fields.Float(string='Credit', digits=(16, 2))
    fluctuation = fields.Float(string='Fluctuation', digits=(16, 2))

    cost_allocation = fields.One2many('mrp.extra.cost.allocation', 'full_costing_id', string='Extra Cost Allocation')


    @api.multi
    def calculate(self):
        for obj in self:
            if not obj.produced_products:
                raise UserError(_('Please select products!'))

            if obj.cost_allocation:
                for allocate in self.cost_allocation:
                    allocate.unlink()

            for product in obj.produced_products:
                # Сонгосон хугацааны интервал дахь бүх үйлдвэрлэлийн бэлэн бүтээгдэхүүний хөдөлгөөн, нийт өртөг, тоо хэмжээг дуудах
                self.env.cr.execute(
                    "select COALESCE(sum(m.product_qty),0), COALESCE(sum(m.product_qty*m.price_unit),0), array_to_string(array_agg(m.id), ', ') from "
                    " mrp_production p, stock_move m, stock_location l "
                    " where m.production_id=p.id and m.location_id=l.id and p.product_id = " + str(product.id) +
                    " and m.date between '" + self.start_date + "' and '" + self.end_date + "' and l.usage = 'production' "
                                                                                            " group by m.product_id")

                results = self.env.cr.fetchall()
                #Хуваарилалт хавтасны мэдээллийг үүсгэх
                for item in results:
                    allocation_id = self.env['mrp.extra.cost.allocation'].create({
                        'product_id': product.id,
                        'produced_qty': item[0],
                        'total_cost': item[1],
                        'unit_cost': item[1] / item[0], # Бүх гүйлгээний жигнэсэн дундаж өртөг гаргана
                        'full_costing_id': obj.id
                    })
                    move_ids = [int(move_id.strip()) for move_id in item[2].split(",")]
                    moves = self.env['stock.move'].browse(move_ids)
                    # Сүүлд өртөг шинэчлэхэд зориулж холбоос бичих
                    for move in moves:
                        move.write({'cost_allocation_id': allocation_id.id})

            obj.write({'state': 'calculated'})
        return True

    #
    @api.multi
    def allocate(self):
        for obj in self:
            if obj.cost_allocation:
                total_cost = 0
                total_qty = 0
                # Эхний давталтаар нийт тоо хэмжээг олно
                for allocation in self.cost_allocation:
                    total_cost += allocation.total_cost
                    total_qty += allocation.produced_qty
                unit_overhead_cost = obj.fluctuation / total_qty
                unit_overhead_cost1 = obj.fluctuation / total_cost
                if self.env.user.company_id.mrp_full_costing == 'quantity':
                # Хоёр дахь давталтаар тоо хэмжээнд нь үндэслэж өртгийг хуваарилна.
                    for allocation in self.cost_allocation:
                        total_cost_allocation = allocation.produced_qty * unit_overhead_cost
                        # unit_cost_allocation = total_cost_allocation / allocation.produced_qty
                        allocation.write({'total_cost_allocation': total_cost_allocation,
                                          'unit_cost_allocation': unit_overhead_cost,
                                          'new_unit_cost': allocation.unit_cost + unit_overhead_cost})
                elif self.env.user.company_id.mrp_full_costing == 'cost':
                    # Хоёр дахь давталтаар өртөгт нь үндэслэж өртгийг хуваарилна.
                    for allocation in self.cost_allocation:
                        total_cost_allocation = allocation.total_cost * unit_overhead_cost1
                        allocation.write({'total_cost_allocation': total_cost_allocation,
                                          'unit_cost_allocation': total_cost_allocation / allocation.produced_qty,
                                          'new_unit_cost': allocation.unit_cost + total_cost_allocation / allocation.produced_qty})

                obj.write({'state': 'expense_allocated'})
        return True

    # Өртөг шинэчлэх товч
    def update_cost(self):
        for obj in self:
            for alloc in self.cost_allocation:
                moves = self.env['stock.move'].search([
                    ('cost_allocation_id','=',alloc.id),
                    ('product_id','=',alloc.product_id.id),
                ])
                moves.write({'price_unit': alloc.new_unit_cost})
            obj.write({'state':'cost_updated'})

    @api.multi
    def cancel(self):
        for obj in self:
            obj.write({'state': 'cancelled'})
        return True


    @api.multi
    @api.onchange('account_id', 'start_date', 'end_date')
    def onchange_account_id(self):
        if self.account_id and self.start_date and self.end_date:
            self.env.cr.execute(
                "select sum(l.debit), sum(l.credit), sum(l.debit)-sum(l.credit) from account_move_line l, account_move m "
                "where l.move_id=m.id and l.date between '" + self.start_date +
                "' and '" + self.end_date + "' and l.account_id=" + str(self.account_id.id) + " and m.state='posted'")

            results = self.env.cr.fetchall()
            for item in results:
                self.update({'debit': item[0], 'credit': item[1], 'fluctuation': item[2] and abs(item[2]) or 0.0})


    @api.multi
    def unlink(self):
        for obj in self:
            if obj.state not in ('draft', 'cancelled'):
                raise UserError(_('You can only delete draft or cancelled record.'))
        return super(mrp_full_costing, self).unlink()



class mrp_full_costing_allocation(models.Model):
    _name = 'mrp.extra.cost.allocation'
    
    product_id = fields.Many2one('product.product', string='Product', required=True)
    produced_qty = fields.Float(string='Produced Quantity', required=True)
    total_cost = fields.Float(string='Total Cost', required=True)
    unit_cost = fields.Float(string='Unit Cost', required=True)
    
    total_cost_allocation = fields.Float(string='Allocated Additional Cost')
    unit_cost_allocation = fields.Float(string='Additional Unit Cost')
    new_unit_cost = fields.Float(string='New Unit Cost')
    full_costing_id = fields.Many2one('mrp.full.costing', string='MRP Full Costing', required=True, ondelete="cascade")
    
    @api.multi
    def action_view_allocated_stock_moves(self):
        action = self.env.ref('stock.stock_move_action')
        result = action.read()[0]

        #override the context to get rid of the default filtering on picking type
        result.pop('id', None)
        result['context'] = {}
        move_ids = self.env['stock.move'].search([('cost_allocation_id','=',self.id)])
        # print 'move_ids: ', move_ids.ids
        #choose the view_mode accordingly
        if move_ids:
            result['domain'] = "[('id','in',[" + ','.join(str(move_id) for move_id in move_ids.ids) + "])]"
        return result    
    
class stock_move(models.Model):
    _inherit = 'stock.move'
    cost_allocation_id = fields.Many2one('mrp.extra.cost.allocation', string='Cost Allocation')
    
