# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class ResCompany(models.Model):
    _inherit = 'res.company'

    mrp_full_costing = fields.Selection([('quantity', 'Quantity'), ('cost', 'Cost')], string='Full Costing')
