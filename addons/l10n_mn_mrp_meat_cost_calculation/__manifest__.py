# -*- coding: utf-8 -*-
{
    'name': 'Manufacturing Meat Cost Calculation',
    'version': '1.0',
    'category': 'Mongolian Modules',
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'sequence': 15,
    'depends': ['l10n_mn_mrp', 'l10n_mn_mrp_account', 'l10n_mn_mrp_purchase', 'l10n_mn_mrp_byproduct'],
    'description': """
        Махны Үйлдвэрлэлийн захиалгын өртөг хуваарилах хөгжүүлэлтүүд хийгдэв.
    """,
    'data': [
        'security/ir.model.access.csv',
        'security/security.xml',
        'views/mrp_overhead_expense_view.xml',
        'views/res_company_view.xml',
        'views/mrp_direct_labor_view.xml',
        'views/mrp_bom_view.xml',
        'views/mrp_production_view.xml',
        'views/mrp_workorder_view.xml',
        'views/purchase_order_view.xml',
        'views/mrp_subproduct_weighing_view.xml',
    ],
    'demo': [
    ],
    'application': True,
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
