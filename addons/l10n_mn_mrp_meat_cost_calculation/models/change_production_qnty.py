# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError
import math


class ChangeProductionQty(models.TransientModel):
    _inherit = 'change.production.qty'

    mrp_sec_qty = fields.Float(string='Product Secondary Qty')
    mrp_sec_uom = fields.Many2one('product.uom', string='Secondary Unit')

    @api.model
    def default_get(self, fields):
        res = super(ChangeProductionQty, self).default_get(fields)
        prod_obj = self.env['mrp.production']
        prod = prod_obj.browse(self._context.get('active_id'))
        if 'product_qty' in fields:
            res.update({'product_qty': prod.product_qty})
        if 'mrp_sec_qty' in fields:
            res.update({'mrp_sec_qty': prod.mrp_sec_qty})
        if 'mrp_sec_uom' in fields:
            res.update({'mrp_sec_uom': prod.mrp_sec_uom.id})
        return res

    @api.multi
    def change_prod_qty(self):
        for wizard in self:
            production = wizard.mo_id
            produced = sum(production.move_finished_ids.mapped('quantity_done'))
            ratio = wizard.mrp_sec_qty/wizard.product_qty
            production.write({'product_qty': wizard.product_qty,
                              'mrp_sec_qty':  wizard.mrp_sec_qty,
                              'mrp_sec_uom': wizard.mrp_sec_uom.id,
                              'mrp_ratio_sec_uom': ratio
                              })
        return {}
