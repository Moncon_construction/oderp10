# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from odoo.exceptions import UserError


class MrpBomLine(models.Model):
    _inherit = 'mrp.bom.line'

    mrp_bom_sec_qty = fields.Float('Secondary Quantity')
    mrp_bom_sec_uom = fields.Many2one('product.uom', 'Secondary Unit')

class MrpBom(models.Model):
    _inherit = 'mrp.bom'

    product_id = fields.Many2one(
        'product.product', 'Product Variant',
        domain="['&', ('product_tmpl_id', '=', product_tmpl_id), ('type', 'in', ['product', 'consu'])]", required=True,
        help="If a product variant is defined the BOM is available only for this product.")
    cost_pricelist_id = fields.Many2one('product.pricelist', string='Cost allocater pricelist', required=True)
    cost_amount = fields.Float(string='Cost amount', required=True)
    direct_labor_ids = fields.One2many('mrp.bom.direct.labor', 'bom_id', 'Direct Labor Cost')
    is_unit_type = fields.Boolean('Is Unit Type', default=False)

    @api.multi
    def button_get_direct_labor_cost(self):
        labor_obj = self.env['mrp.bom.direct.labor']
        for obj in self:
            if self.env['mrp.direct.labor'].search(
                    [('state', '=', 'approved'), ('product_id', '=', obj.product_id.id)]):
                labor_ids = self.env['mrp.direct.labor'].search(
                    [('state', '=', 'approved'), ('product_id', '=', obj.product_id.id)])
                ids = []
                for line in labor_ids:
                    if not obj.direct_labor_ids.search([('direct_labor_id', '=', line.id), ('bom_id', '=', obj.id)]):
                        labor_id = labor_obj.create({
                            'bom_id': obj.id,
                            'direct_labor_id': line.id,
                        })
                        ids.append(labor_id.id)
                obj.direct_labor_ids = ids


    @api.multi
    def button_reset_cost(self):
        print 'asdasdads'
        for obj in self:
            cost_list = self.env['mrp.bom.overhead.expense']
            expense_ids = self.env['mrp.overhead.expense'].search(
                [('state', '=', 'approved'), ('product_id', '=', obj.product_id.id)])
            ids = []
            if expense_ids:
                obj.expense_ids.unlink()
                for line in expense_ids:
                    expenses = cost_list.create({
                        'bom_id': self.id,
                        'overhead_expense': line.id,
                    })
                    ids.append(expenses.id)
                obj.expense_ids = ids

class MrpBomDirectLabor(models.Model):
    _name = 'mrp.bom.direct.labor'
    _description = 'Manufacturing BOM direct labor'

    @api.multi
    @api.depends('direct_labor_id')
    def _compute_total_cost(self):
        for obj in self:
            if obj.direct_labor_id:
                for labor in obj.direct_labor_id:
                    obj.total_cost = labor.unit_cost * obj.bom_id.product_qty

    name = fields.Char('Name')
    unit_cost = fields.Float(related='direct_labor_id.unit_cost')
    product_qty = fields.Float('Product QTY')
    total_cost = fields.Float(compute=_compute_total_cost)
    bom_id = fields.Many2one('mrp.bom', 'Manufacturing Order')
    direct_labor_id = fields.Many2one('mrp.direct.labor', 'Direct Labor')
