# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class MrpDirectLabor(models.Model):
    _name = 'mrp.direct.labor'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    name = fields.Char('Name', required=True, track_visibility='onchange')
    product_id = fields.Many2one('product.product', 'Product', required=True)
    unit_cost = fields.Float(string='Unit Cost', required=True, track_visibility='onchange')
    state = fields.Selection([
        ('new', 'New'),
        ('control', 'Control'),
        ('approved', 'Approved'),
        ('cancelled', 'Cancelled'),
    ], string='Status', index=True, readonly=True, default='new',
        track_visibility='onchange')
    description = fields.Text('Description', required=True, track_visibility='onchange')

    @api.multi
    def action_approve(self):
        self.write({'state': 'approved'})

    @api.multi
    def action_control(self):
        self.write({'state': 'control'})

    @api.multi
    def action_cancel(self):
        self.write({'state': 'cancelled'})

    def action_new(self):
        self.write({'state': 'new'})
