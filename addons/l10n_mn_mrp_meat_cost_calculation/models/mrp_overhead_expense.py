# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class MrpOverheadExpense(models.Model):
    _inherit = 'mrp.overhead.expense'

    account_id = fields.Many2one('account.account', required=False, string='Control Account',
                                 track_visibility='onchange')
    product_id = fields.Many2one('product.product', 'Product', required=True)
    fluctuation_account_id = fields.Many2one('account.account', required=False, string='Fluctuation Account',
                                             track_visibility='onchange')
    uom_id = fields.Many2one('product.uom', string="Basic Time UOM", required=False, track_visibility='onchange')
    uom_categ_id = fields.Many2one('product.uom.categ', required=False, string="Product UOM",
                                   track_visibility='onchange')
