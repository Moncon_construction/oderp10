# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from odoo.exceptions import UserError
from odoo.addons import decimal_precision as dp


class MrpProduction(models.Model):
    _inherit = 'mrp.production'

    production_cost_pricelist_id = fields.Many2one(related='bom_id.cost_pricelist_id',
                                                   string='Cost allocater pricelist')
    production_cost_amount = fields.Float(string='Cost amount')

    account_move_count = fields.Integer(compute='_compute_production_account_move', string='Receptions', default=0)
    account_move_ids = fields.Many2many('account.move', compute='_compute_production_account_move',
                                        string='Productions',
                                        copy=False)
    direct_labor_ids = fields.One2many('mrp.production.direct.labor', 'production_id', 'Direct Labor Cost')

    subproduct_weighing_id = fields.Many2one('mrp.subproduct.weighing', 'Subproduct Weighing')
    is_sub_product_weighing_done = fields.Boolean('Is Sub Products Weight Done', default=False)

    @api.multi
    def action_assign(self):
        for production in self:
            move_to_assign = production.move_raw_ids.filtered(lambda x: x.state in ('confirmed', 'waiting', 'assigned'))
            move_to_assign.action_assign()
        return True

    def _compute_production_account_move(self):
        for obj in self:
            account_move_obj = self.env['account.move']
            account_move_ids = account_move_obj.search([('production_id', '=', obj.id)])
            if account_move_ids:
                obj.account_move_ids = account_move_ids
            finished_account_move_ids = obj.move_finished_ids.filtered(lambda a: a.state == 'done').mapped(
                'account_line_ids').mapped('move_id')
            raw_account_move_ids = obj.move_raw_ids.filtered(lambda a: a.state == 'done').mapped(
                'account_line_ids').mapped('move_id')
            if finished_account_move_ids:
                obj.account_move_ids += finished_account_move_ids
            if raw_account_move_ids:
                obj.account_move_ids += raw_account_move_ids

            obj.account_move_count = len(obj.account_move_ids)

    @api.multi
    def view_production_account_moves(self):
        for obj in self:
            action = self.env.ref('account.action_move_line_form')
            result = action.read()[0]

            # override the context to get rid of the default filtering on account move
            result.pop('id', None)
            result['context'] = {}
            account_move_ids = sum([obj.account_move_ids.ids for obj in self], [])
            if account_move_ids:
                result['domain'] = "[('id','in',[" + ','.join(map(str, account_move_ids)) + "])]"
            return result

    @api.model
    def create(self, vals):
        res = super(MrpProduction, self).create(vals)
        if res.bom_id:
            # ҮЗ-г үүсэх үед ОРЦ-д бүртгэсэн ҮНЗ-г ҮЗ-н тоо хэмжээгээр тооцож оруулах хэсэг
            if res.bom_id.expense_ids:
                overhead_expense_obj = self.env['mrp.production.overhead.expense']
                for expense in res.bom_id.expense_ids:
                    overhead_expense_obj.create({'cost': expense.total_cost * res.product_qty,
                                                 'overhead_expense': expense.overhead_expense.id,
                                                 'production_id': res.id})
            # ҮЗ-г үүсэх үед ОРЦ-д бүртгэсэн ҮНЗ-г ҮЗ-н тоо хэмжээгээр тооцож оруулах хэсэг
            if res.bom_id.direct_labor_ids:
                direct_labor_obj = self.env['mrp.production.direct.labor']
                for labor in res.bom_id.direct_labor_ids:
                    direct_labor_obj.create({'cost': labor.total_cost * res.product_qty,
                                             'direct_labor_id': labor.direct_labor_id.id,
                                             'production_id': res.id})
            # ҮЗ-г үүсгэх үед орц-д бүртгэсэн Дайварт өртөг хуваарилахад ашиглах
            # борлуулалтын үнийн хүснэгт болон хуваарилах өртөг талбарууд
            # тооцоологдож тус тусын талбарт утга онооно.
            if res.bom_id.is_unit_type:
                if res.bom_id.cost_pricelist_id:
                    res.production_cost_pricelist_id = res.bom_id.cost_pricelist_id.id
                if res.bom_id.cost_amount:
                    if 'production_cost_amount' in vals:
                        vals['production_cost_amount'] = res.bom_id.cost_amount * res.product_qty
            else:
                total = 0
                if res.move_raw_ids:
                    for line in res.bom_id.bom_line_ids:
                        total += line.product_id.standard_price * res.product_qty
                    if res.cost_ids:
                        for cost in res.cost_ids:
                            total += cost.cost
                    if res.direct_labor_ids:
                        for direct_labor in res.direct_labor_ids:
                            total += direct_labor.cost
                if 'production_cost_amount' in vals:
                    if total:
                        vals['production_cost_amount'] = total

        if res.routing_id:
            if res.routing_id.operation_ids:
                production_workcenter_obj = self.env['mrp.production.workcenter']
                for operation in res.routing_id.operation_ids:
                    production_workcenter_obj.create({'name': operation.name,
                                                      'workcenter_id': operation.workcenter_id.id,
                                                      'workorder_qty': operation.workcenter_quantity,
                                                      'duration_expected': operation.time_cycle_manual,
                                                      'production_id': res.id})
        return res

    @api.multi
    def write(self, vals):
        res = super(MrpProduction, self).write(vals)
        for obj in self:
            if obj.bom_id:
                # ҮЗ-г засах үед орц-д бүртгэсэн Дайварт өртөг хуваарилахад ашиглах
                # борлуулалтын үнийн хүснэгт болон хуваарилах өртөг талбарууд
                # тооцоологдож тус тусын талбарт утга онооно.
                if obj.bom_id.is_unit_type:
                    if obj.bom_id.cost_pricelist_id:
                        if not obj.production_cost_pricelist_id:
                            obj.production_cost_pricelist_id = obj.bom_id.cost_pricelist_id.id
                    if obj.bom_id.cost_amount:
                        if 'production_cost_amount' in vals:
                            vals['production_cost_amount'] = obj.bom_id.cost_amount * obj.product_qty
                else:
                    total = 0
                    if obj.move_raw_ids:
                        for move in obj.move_raw_ids:
                            total += move.product_id.standard_price * move.product_uom_qty
                    if obj.cost_ids:
                        for cost in obj.cost_ids:
                            total += cost.cost
                    if obj.direct_labor_ids:
                        for direct_labor in obj.direct_labor_ids:
                            total += direct_labor.cost
                    if 'production_cost_amount' in vals:
                        if total:
                            vals['production_cost_amount'] = total
            # ҮЗ-н Эцсийн бүтээгдэхүүн талбарт байгаа барааны хөдөлгөөнүүдийн өртгийг тооцоолох хэсэг
            obj.allocate_cost()
        return res

    # ҮЗ-н Эцсийн бүтээгдэхүүн талбарт байгаа барааны хөдөлгөөнүүдийн өртгийг тооцоолох функц
    @api.multi
    def allocate_cost(self):
        for obj in self:
            for move in obj.move_finished_ids:
                if obj.bom_id.is_unit_type:
                    if move.product_id.id != obj.product_id.id:
                        move.compute_total_standard_cost()
                        move.compute_real_cost()
                else:
                    move.compute_total_standard_cost()
                    move.compute_real_cost()

    # ҮЗ-д орц-г солих үед Дайварт өртөг хуваарилахад ашиглах
    # борлуулалтын үнийн хүснэгт болон хуваарилах өртөг талбарууд
    # тооцоологдож тус тусын талбарт утга онооно.
    # Орцод ширхэгээ үйлдвэрлэхгүй тохиолдолд ХМ-н өртгийг хуваарилана.
    @api.onchange('bom_id')
    def onchange_bom_to_pricelist(self):
        for obj in self:
            if obj.bom_id:
                if obj.bom_id.is_unit_type:
                    if obj.bom_id.cost_pricelist_id:
                        obj.production_cost_pricelist_id = obj.bom_id.cost_pricelist_id.id
                    else:
                        obj.production_cost_pricelist_id = False

                    if obj.bom_id.cost_amount:
                        if not obj.production_cost_amount:
                            obj.production_cost_amount = obj.bom_id.cost_amount * obj.product_qty
                else:
                    total = 0
                    for raw_id in obj.move_raw_ids:
                        total += raw_id.product_uom_qty * raw_id.product_id.standard_price
                    if obj.cost_ids:
                        for cost in obj.cost_ids:
                            total += cost.cost
                    if obj.direct_labor_ids:
                        for direct_labor in obj.direct_labor_ids:
                            total += direct_labor.cost
                    obj.production_cost_amount = total

    @api.multi
    def button_mark_done(self):
        self.ensure_one()
        self.create_account_move_from_overhead_expense()
        for wo in self.workorder_ids:
            if wo.time_ids.filtered(lambda x: (not x.date_end) and (x.loss_type in ('productive', 'performance'))):
                raise UserError(u'%s ажлын захиалга хийгдэж дуусаагүй байна! Гулуузыг жинлэж дуусгана уу!' % wo.name)
        self.post_inventory()
        moves_to_cancel = (self.move_raw_ids | self.move_finished_ids).filtered(
            lambda x: x.state not in ('done', 'cancel'))
        moves_to_cancel.action_cancel()
        self.write({'state': 'done', 'date_finished': fields.Datetime.now()})
        self.env["procurement.order"].search([('production_id', 'in', self.ids)]).check()
        return self.write({'state': 'done'})

    # ҮЗ батлах товч дарах үед Дайварт өртөг хуваарилах талбар зөв эсэхийг шалгаж буруу бол шинэчлэнэ.
    @api.multi
    def button_confirm(self):
        res = super(MrpProduction, self).button_confirm()
        for obj in self:
            if obj.bom_id.is_unit_type:
                if obj.bom_id.cost_amount:
                    production_cost_amount = obj.bom_id.cost_amount * obj.product_qty
            else:
                total = 0
                for move in obj.move_raw_ids:
                    total += move.product_id.standard_price * move.product_uom_qty
                production_cost_amount = total

            if obj.production_cost_amount != production_cost_amount:
                obj.production_cost_amount = production_cost_amount
        return res

    # ҮЗ-г хүчээр цуцлах товч нь ажлын захиалгуудыг цуцладаг байхаар дахин тодорхойлогдсон.
    @api.multi
    def cancel_mrp_by_force(self):
        res = super(MrpProduction, self).cancel_mrp_by_force()
        for production in self:
            production.is_sub_product_weighing_done = False
            production.workorder_ids.unlink()
            production.move_finished_ids.unlink()
            production.account_move_ids.unlink()
        self.write({'date_planned_start_wo': False,
                    'date_planned_finished_wo': False, })
        return res

    @api.multi
    def create_account_move_from_overhead_expense(self):
        account_move_obj = self.env['account.move']
        for obj in self:
            if obj.purchase_order_id:
                if not obj.product_id.categ_id.work_in_progress_account_id:
                    raise UserError(_('Work In Progress Account not found! Please select in product category'))
                if not obj.company_id.overhead_expense_account_id:
                    raise UserError(_('Overhead Expense Account not found! Please select in company MRP configuration'))
                if not obj.company_id.direct_labor_account_id:
                    raise UserError(_('Direct Labor Account not found! Please select in company MRP configuration'))
                if obj.cost_ids:
                    expense_line_ids = []
                    for expense in obj.cost_ids:
                        expense_line_ids.append((0, 0, {
                            'name': expense.overhead_expense.name,
                            'debit': expense.cost,
                            'credit': 0,
                            'account_id': obj.product_id.categ_id.work_in_progress_account_id.id,
                            'amount_currency': 0,
                            'currency_id': False,
                            'date': obj.date_planned_finished_wo,
                            'journal_id': obj.company_id.overhead_expense_journal_id.id,
                        }))
                        expense_line_ids.append((0, 0, {
                            'name': expense.overhead_expense.name,
                            'debit': 0,
                            'credit': expense.cost,
                            'account_id': obj.company_id.overhead_expense_account_id.id,
                            'amount_currency': 0,
                            'currency_id': False,
                            'date': obj.date_planned_finished_wo,
                            'journal_id': obj.company_id.overhead_expense_journal_id.id,
                        }))
                    account_move_obj.create({
                        'journal_id': obj.company_id.overhead_expense_journal_id.id,
                        'date': obj.date_planned_finished_wo,
                        'production_id': obj.id,
                        'description': obj.purchase_order_id.name + "/" + obj.name + "/" + u' - ҮНЗ-н бичилт',
                        'line_ids': expense_line_ids})
                if obj.direct_labor_ids:
                    direct_labor_ids = []
                    for labor in obj.direct_labor_ids:
                        direct_labor_ids.append((0, 0, {
                            'name': labor.direct_labor_id.name,
                            'debit': labor.cost,
                            'credit': 0,
                            'account_id': obj.product_id.categ_id.work_in_progress_account_id.id,
                            'amount_currency': 0,
                            'currency_id': False,
                            'date': obj.date_planned_finished_wo,
                            'journal_id': obj.company_id.overhead_expense_journal_id.id,
                        }))
                        direct_labor_ids.append((0, 0, {
                            'name': labor.direct_labor_id.name,
                            'debit': 0,
                            'credit': labor.cost,
                            'account_id': obj.company_id.direct_labor_account_id.id,
                            'amount_currency': 0,
                            'currency_id': False,
                            'date': obj.date_planned_finished_wo,
                            'journal_id': obj.company_id.overhead_expense_journal_id.id,
                        }))
                    account_move_obj.create({
                        'journal_id': obj.company_id.overhead_expense_journal_id.id,
                        'date': obj.date_planned_finished_wo,
                        'production_id': obj.id,
                        'description': obj.purchase_order_id.name + "/" + obj.name + "/" + u' - ШХ-н бичилт',
                        'line_ids': direct_labor_ids})


class MrpProductionOverheadExpense(models.Model):
    _inherit = 'mrp.production.overhead.expense'

    cost = fields.Float('Actual Cost Incurred', compute='compute_cost')

    @api.multi
    def compute_cost(self):
        for obj in self:
            for expense in obj.production_id.bom_id.expense_ids:
                if obj.overhead_expense.id == expense.overhead_expense.id:
                    if obj.cost != expense.total_cost * obj.production_id.product_qty:
                        obj.cost = expense.total_cost * obj.production_id.product_qty


class MrpProductionDirectLabor(models.Model):
    _name = 'mrp.production.direct.labor'

    direct_labor_id = fields.Many2one('mrp.direct.labor', 'Direct Labor')
    production_id = fields.Many2one('mrp.production', 'Manufacturing Order')
    cost = fields.Float('Actual Cost Incurred', compute='compute_cost')

    @api.multi
    def compute_cost(self):
        for obj in self:
            for labor in obj.production_id.bom_id.direct_labor_ids:
                if obj.direct_labor_id.id == labor.direct_labor_id.id:
                    if obj.cost != labor.unit_cost * obj.production_id.product_qty:
                        obj.cost = labor.unit_cost * obj.production_id.product_qty
