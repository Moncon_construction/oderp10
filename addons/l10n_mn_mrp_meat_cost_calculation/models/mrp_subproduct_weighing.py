# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from datetime import datetime


class MrpSubProductWeighing(models.Model):
    _name = 'mrp.subproduct.weighing'

    name = fields.Date('Date', default=datetime.now())
    product_id = fields.Many2one('product.product', 'Main Product', required=True)
    bom_id = fields.Many2one('mrp.bom', 'MRP Bill of materials', required=True)
    production_ids = fields.Many2many('mrp.production','subproduct_weighing_id','production_id','weighing_id', string='Productions')
    subproduct_weighing_line_ids = fields.One2many('mrp.subproduct.weighing.line', 'subproduct_weighing_id',
                                                   'Sub Product Line')
    state = fields.Selection([
        ('new', 'New'),
        ('approved', 'Approved'),
        ('cancelled', 'Cancelled'),
    ], string='Status', index=True, readonly=True, default='new',
        track_visibility='onchange')

    @api.multi
    def approve(self):
        stock_move_obj = self.env['stock.move']
        for obj in self:
            total_qty = 0
            for production in obj.production_ids:
                total_qty += production.product_qty
            for production in obj.production_ids:
                for line in obj.subproduct_weighing_line_ids:
                    move_id = stock_move_obj.search(
                        [('product_id', '=', line.product_id.id), ('production_id', '=', production.id)])
                    if move_id:
                        move_id.write(
                            {'quantity_done': line.quantity_done / total_qty * production.product_qty,
                             'location_dest_id': line.subproduct_location_dest_id.id})
                production.is_sub_product_weighing_done = True
                production.check_to_done = True
                production.button_mark_done()
            obj.state = 'approved'

    @api.multi
    def cancel(self):
        for obj in self:
            obj.state = 'cancelled'
            for production in obj.production_ids:
                production.is_sub_product_weighing_done = False

    @api.multi
    def to_new(self):
        for obj in self:
            obj.state = 'new'

    @api.multi
    def get_productions_move_finished_ids(self):
        subproduct_obj = self.env['mrp.subproduct']
        subproduct_line_obj = self.env['mrp.subproduct.weighing.line']
        for obj in self:
            if not obj.subproduct_weighing_line_ids:
                total_qty = 0
                for production in obj.production_ids:
                    total_qty += production.product_qty
                subproduct_ids = subproduct_obj.search([('product_id', '=', obj.product_id.id)])
                if obj.bom_id.is_unit_type:
                    for subproduct in obj.bom_id.sub_products:
                        if subproduct.product_id.id != obj.product_id.id:
                            subproduct_line_obj.create({'name': subproduct.product_id.name,
                                                        'product_id': subproduct.product_id.id,
                                                        'product_uom_id': subproduct.product_uom_id.id,
                                                        'product_uom_qty': subproduct.product_qty * total_qty,
                                                        'quantity_done': 0,
                                                        'subproduct_weighing_id': obj.id,
                                                        'subproduct_location_dest_id':subproduct.subproduct_location_dest_id.id,
                                                        })
                else:
                    for subproduct in obj.bom_id.sub_products:
                        subproduct_line_obj.create({'name': subproduct.product_id.name,
                                                    'product_id': subproduct.product_id.id,
                                                    'product_uom_id': subproduct.product_uom_id.id,
                                                    'product_uom_qty': subproduct.product_qty * total_qty,
                                                    'quantity_done': 0,
                                                    'subproduct_weighing_id': obj.id,
                                                    'subproduct_location_dest_id':subproduct.subproduct_location_dest_id.id,
                                                    })
                    product_id = obj.bom_id.product_id
                    subproduct_line_obj.create({'name': product_id.name,
                                                'product_id': product_id.id,
                                                'product_uom_id': product_id.uom_id.id,
                                                'product_uom_qty': 1,
                                                'quantity_done': 0,
                                                'subproduct_weighing_id': obj.id,
                                                })


class MrpSubProductWeighingLine(models.Model):
    _name = 'mrp.subproduct.weighing.line'

    name = fields.Char('Name')
    product_id = fields.Many2one('product.product', 'Main Product')
    product_uom_id = fields.Many2one('product.uom', 'Product UOM')
    product_uom_qty = fields.Float('Product QTY')
    quantity_done = fields.Float('Quantity Done')
    subproduct_weighing_id = fields.Many2one('mrp.subproduct.weighing', 'Subproduct Weighing')
    subproduct_location_dest_id = fields.Many2one('stock.location', 'Sub Product Location')
