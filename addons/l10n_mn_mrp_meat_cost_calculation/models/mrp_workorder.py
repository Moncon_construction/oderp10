# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from odoo.exceptions import UserError
from odoo.tools import float_compare, float_round


class MrpWorkOrder(models.Model):
    _inherit = 'mrp.workorder'

    tabled_line_ids = fields.One2many('mrp.workorder.tabled.line', 'workorder_id', 'Tabled Line')
    work_order_standard_cost_id = fields.Many2one('mrp.workorder.standard.cost', 'Work Order Standard Cost')
    qty_producing = fields.Float('Currently Produced Quantity', compute = 'compute_qty_producing')
    is_unit_type = fields.Boolean(related='production_id.bom_id.is_unit_type', store=True)

    # Ажлын захиалга үүсэхдээ стандарт өртгийн талбарыг дүүргэнэ
    @api.model
    def create(self, vals):
        res = super(MrpWorkOrder, self).create(vals)
        for obj in res:
            if obj.production_id.bom_id.is_unit_type:
                cost_id = self.env['mrp.workorder.standard.cost'].search([('product_id', '=', obj.product_id.id)])
                if cost_id:
                    obj.work_order_standard_cost_id = cost_id.id
                else:
                    raise UserError(_('Work Order standard cost not found! Please create for this product!'))
                obj.qty_workorder = obj.production_id.product_qty
        return res

    # Үйлдвэрдлэгдэж буй тоо хэмжээ тооцоолно
    @api.multi
    def compute_qty_producing(self):
        for obj in self:
            if obj.is_unit_type:
                if obj.tabled_line_ids:
                    counter = 0
                    for line in obj.tabled_line_ids:
                        if line.real_weight:
                            if line.unit_cost:
                                real_cost = line.unit_cost * line.real_weight
                                line.write({'real_cost': real_cost})
                            else:
                                for cost_line in obj.work_order_standard_cost_id.cost_line_ids:
                                    if cost_line.min_weight <= line.real_weight <= cost_line.max_weight:
                                        real_cost = cost_line.value * line.real_weight
                                        line.write({'real_cost': real_cost,
                                                    'unit_cost': cost_line.value})
                            counter += 1
                            line.write({'state': 'done'})
                        else:
                            line.write({'state': 'not_yet'})
                    obj.qty_producing = counter
            else:
                obj.qty_producing = obj.qty_producing

    # Бодит жинг оруулах үед өртөг, төлөв тооцоологдоно
    @api.multi
    @api.onchange('tabled_line_ids')
    def onchange_tabled_line_ids(self):
        for obj in self:
            if obj.tabled_line_ids:
                counter = 0
                for line in obj.tabled_line_ids:
                    if line.real_weight:
                        if line.unit_cost:
                            real_cost = line.unit_cost * line.real_weight
                            line.write({'real_cost': real_cost})
                        else:
                            for cost_line in obj.work_order_standard_cost_id.cost_line_ids:
                                if cost_line.min_weight <= line.real_weight <= cost_line.max_weight:
                                    real_cost = cost_line.value * line.real_weight
                                    line.write({'real_cost': real_cost,
                                                'unit_cost': cost_line.value})
                        counter += 1
                        line.write({'state': 'done'})
                    else:
                        line.write({'state': 'not_yet'})
                obj.qty_producing = counter

    @api.multi
    def get_avg_weight(self):
        for obj in self:
            if obj.production_id.purchase_order_line_id:
                for line in obj.tabled_line_ids:
                    line.write({'real_weight': obj.production_id.purchase_order_line_id.avg_weight})
                obj.onchange_tabled_line_ids()

    # Ажлын захиалга эхлэх үед үйлдвэрлэлийн захиалгын тоо хэмжээгээр бодит жин, өртөг тооцох талбар дүүргэгдэнэ.
    @api.multi
    def button_start(self):
        res = super(MrpWorkOrder, self).button_start()
        for obj in self:
            if not obj.tabled_line_ids and obj.production_id.bom_id.is_unit_type:
                work_order_table_line_obj = self.env['mrp.workorder.tabled.line']
                for line in range(int(obj.production_id.product_qty)):
                    work_order_table_line_obj.create(
                        {
                            'sequence': str(self.env['ir.sequence'].next_by_code('mrp.workorder.tabled.line')),
                            'real_weight': 0,
                            'state': 'not_yet',
                            'product_uom_id': obj.product_uom_id.id,
                            'workorder_id': obj.id
                        })
        return res

    # Ажлын захиалга дуусгах үед Үйлдвэрлэлийн захиалгын Эцсийн бүтээгдэхүүн
    # талбарт үйлдвэрлэх барааны жин, нэгж өртөг тооцоологдоно.
    @api.multi
    def button_finish(self):
        res = super(MrpWorkOrder, self).button_finish()
        for obj in self:
            total_expense_cost = 0
            for line in obj.production_id.cost_ids:
                total_expense_cost += line.cost
            for line in obj.production_id.direct_labor_ids:
                total_expense_cost += line.cost
            if obj.tabled_line_ids:
                total_weight = 0
                total_cost = 0
                total_unit_cost = 0
                for line in obj.tabled_line_ids:
                    total_cost += line.real_cost
                    total_weight += line.real_weight
                    total_unit_cost += line.unit_cost
                if total_cost and total_weight:
                    finished_move_id = self.env['stock.move'].search(
                        [('id', 'in', obj.production_id.move_finished_ids.ids), ('product_id', '=', obj.product_id.id)])
                    if finished_move_id:
                        unit_real_cost = total_cost / total_weight
                        unit_computed_cost = ((unit_real_cost * total_weight) + total_expense_cost) / total_weight
                        obj.product_id.product_tmpl_id.standard_price = unit_computed_cost
                        finished_move_id.write({'price_unit': unit_computed_cost,
                                                'total_real_cost': unit_computed_cost * total_weight,
                                                'product_uom': obj.product_id.uom_id.id,
                                                'product_uom_qty': total_weight,
                                                'stock_move_sec_uom': obj.product_uom_id.id,
                                                'stock_move_sec_qty_to_produce': obj.production_id.product_qty,
                                                'stock_move_sec_qty_produced': obj.qty_produced,
                                                'quantity_done': total_weight})
                    move_raw_ids = self.env['stock.move'].search(
                        [('id', 'in', obj.production_id.move_raw_ids.ids),
                         ('product_id', '=', obj.production_id.bom_id.bom_line_ids[0].product_id.id)])
                    if obj.production_id.purchase_order_line_id:
                        if move_raw_ids:
                            obj.product_id.product_tmpl_id.standard_price = obj.production_id.purchase_order_line_id.price_unit
                            move_raw_ids.write({'price_unit': obj.production_id.purchase_order_line_id.price_unit,
                                                'product_uom_qty': obj.product_qty,
                                                'quantity_done': obj.product_qty})
                # price_unit = (((total_unit_cost / total_weight) * total_weight) + total_expense_cost) / total_weight * (
                #         total_weight / obj.production_id.product_qty)
                # if price_unit:
                #     product_id = obj.production_id.bom_id.bom_line_ids[0].product_id
                #     raw_move_id = self.env['stock.move'].search(
                #         [('id', 'in', obj.production_id.move_raw_ids.ids), ('product_id', '=', product_id.id)])
                #     if raw_move_id:
                #         product_id.product_tmpl_id.standard_price = price_unit
                #         raw_move_id.write({'price_unit': price_unit})

        return res

    @api.multi
    def get_total_values(self):
        for obj in self:
            if obj.tabled_line_ids:
                total_weight = 0
                total_cost = 0
                for line in obj.tabled_line_ids:
                    total_cost += line.real_cost
                    total_weight += line.real_weight
        return total_cost, total_weight

    # OVERRIDE. Odoo-н функцийг дахин тодорхойлж ашиглав.
    # Үйлдвэрлэлийг үргэлжлүүлэх товч дарагдах бүрт үйлввэрлэгдсэн тоо хэмжээ нэмэгддэгийг болиулав.
    @api.multi
    def record_production(self):
        self.ensure_one()
        if self.is_unit_type:
            if self.qty_producing <= 0:
                raise UserError(_('Please set the quantity you produced in the Current Qty field. It can not be 0!'))

        if (self.production_id.product_id.tracking != 'none') and not self.final_lot_id:
            raise UserError(_('You should provide a lot for the final product'))

        # Update quantities done on each raw material line
        raw_moves = self.move_raw_ids.filtered(
            lambda x: (x.has_tracking == 'none') and (x.state not in ('done', 'cancel')) and x.bom_line_id)
        for move in raw_moves:
            if move.unit_factor:
                rounding = move.product_uom.rounding
                move.quantity_done += float_round(self.qty_producing * move.unit_factor,
                                                  precision_rounding=rounding)

        # Transfer quantities from temporary to final move lots or make them final
        for move_lot in self.active_move_lot_ids:
            # Check if move_lot already exists
            if move_lot.quantity_done <= 0:  # rounding...
                move_lot.sudo().unlink()
                continue
            if not move_lot.lot_id:
                raise UserError(_('You should provide a lot for a component'))
            # Search other move_lot where it could be added:
            lots = self.move_lot_ids.filtered(
                lambda x: (x.lot_id.id == move_lot.lot_id.id) and (not x.lot_produced_id) and (not x.done_move))
            if lots:
                lots[0].quantity_done += move_lot.quantity_done
                lots[0].lot_produced_id = self.final_lot_id.id
                move_lot.sudo().unlink()
            else:
                move_lot.lot_produced_id = self.final_lot_id.id
                move_lot.done_wo = True

        # One a piece is produced, you can launch the next work order
        if self.next_work_order_id.state == 'pending':
            self.next_work_order_id.state = 'ready'
        if self.next_work_order_id and self.final_lot_id and not self.next_work_order_id.final_lot_id:
            self.next_work_order_id.final_lot_id = self.final_lot_id.id

        self.move_lot_ids.filtered(
            lambda move_lot: not move_lot.done_move and not move_lot.lot_produced_id and move_lot.quantity_done > 0
        ).write({
            'lot_produced_id': self.final_lot_id.id,
            'lot_produced_qty': self.qty_producing
        })

        # If last work order, then post lots used
        # TODO: should be same as checking if for every workorder something has been done?
        if not self.next_work_order_id:
            production_moves = self.production_id.move_finished_ids.filtered(
                lambda x: (x.state not in ('done', 'cancel')))
            for production_move in production_moves:
                if production_move.product_id.id == self.production_id.product_id.id and production_move.product_id.tracking != 'none':
                    move_lot = production_move.move_lot_ids.filtered(lambda x: x.lot_id.id == self.final_lot_id.id)
                    if move_lot:
                        move_lot.quantity += self.qty_producing
                        move_lot.quantity_done += self.qty_producing
                    else:
                        move_lot.create({'move_id': production_move.id,
                                         'lot_id': self.final_lot_id.id,
                                         'quantity': self.qty_producing,
                                         'quantity_done': self.qty_producing,
                                         'workorder_id': self.id,
                                         })
                elif production_move.unit_factor:
                    rounding = production_move.product_uom.rounding
                    production_move.quantity_done += float_round(self.qty_producing * production_move.unit_factor,
                                                                 precision_rounding=rounding)
                else:
                    production_move.quantity_done += self.qty_producing  # TODO: UoM conversion?
        # BEGIN
        # Update workorder quantity produced
        if self.production_id.bom_id.is_unit_type:
            self.qty_produced += self.qty_producing
        else:
            self.qty_produced = self.qty_producing
        # END
        # Set a qty producing
        if self.qty_produced >= self.production_id.product_qty:
            self.qty_producing = 0
        elif self.production_id.product_id.tracking == 'serial':
            self.qty_producing = 1.0
            self._generate_lot_ids()
        else:
            self.qty_producing = self.production_id.product_qty - self.qty_produced
            self._generate_lot_ids()

        self.final_lot_id = False
        if self.qty_produced >= self.production_id.product_qty:
            self.button_finish()
        return True


class MrpWorkOrderTabledLine(models.Model):
    _name = 'mrp.workorder.tabled.line'

    sequence = fields.Char('Sequence')
    real_weight = fields.Float('Real Weight')
    unit_cost = fields.Float('Unit Cost')
    real_cost = fields.Float('Real Cost')
    state = fields.Selection([('not_yet', 'Not Yet'), ('done', 'Done')], 'State')
    product_uom_id = fields.Many2one('product.uom', 'Product UOM')
    workorder_id = fields.Many2one('mrp.workorder', 'Work Order')


class MrpWorkOrderStandardCost(models.Model):
    _name = 'mrp.workorder.standard.cost'

    name = fields.Char('Name')
    product_id = fields.Many2one('product.product', 'Work Order Product', required=True)
    cost_line_ids = fields.One2many('mrp.workorder.standard.cost.line', 'workorder_standard_cost_id', 'Cost lines')


class MrpWorkOrderStandardCost(models.Model):
    _name = 'mrp.workorder.standard.cost.line'

    min_weight = fields.Integer('Minimum Weight', required=True)
    max_weight = fields.Integer('Maximum Weight', required=True)
    value = fields.Integer('Value', required=True)
    state = fields.Selection([('low', 'Lower'), ('medium', 'Medium'), ('higher', 'Higher')], 'State', required=True)
    workorder_standard_cost_id = fields.Many2one('mrp.workorder.standard.cost')
