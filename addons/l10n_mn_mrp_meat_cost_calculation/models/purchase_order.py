# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from odoo.exceptions import UserError


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    has_production = fields.Boolean('has_production?', compute='compute_has_production')
    is_subproducts_done = fields.Boolean('Is Sub Products Done?')

    @api.multi
    def compute_has_production(self):
        for obj in self:
            has_production = False
            for line in obj.order_line:
                if line.create_production:
                    has_production = True
            if has_production:
                obj.has_production = has_production
            else:
                obj.has_production = False

    @api.multi
    def create_invoice_from_production(self):
        inv_obj = self.env['account.invoice']
        for obj in self:
            invoice_line_ids = []
            for production in obj.production_ids:
                for work_order in production.workorder_ids:
                    total_cost, total_weight = work_order.get_total_values()
                    purchase_line_id = self.env['purchase.order.line'].search(
                        [('product_id', '=', production.bom_id.bom_line_ids[0].product_id.id),
                         ('order_id', '=', obj.id)])
                    invoice_line_ids.append((0, 0, {
                        'name': work_order.product_id.name,
                        'origin': production.purchase_order_id.name,
                        'account_id': production.product_id.categ_id.property_account_expense_categ_id.id,
                        'price_unit': total_cost / total_weight,
                        'quantity': total_weight,
                        # 'discount': 0.0,
                        'uom_id': work_order.product_id.uom_id.id,
                        'product_id': work_order.product_id.id,
                        'purchase_id': production.purchase_order_id.id,
                        'purchase_line_id': purchase_line_id.id,
                        # 'account_analytic_id': order.project_id.id or False,
                    }))
            invoice_id = inv_obj.create({
                'name': '',
                'origin': obj.name,
                'type': 'in_invoice',
                'reference': False,
                'account_id': obj.partner_id.property_account_payable_id.id,
                'partner_id': obj.partner_id.id,
                # 'partner_shipping_id': ,
                'currency_id': obj.currency_id.id,
                'payment_term_id': obj.payment_term_id.id or False,
                'fiscal_position_id': obj.fiscal_position_id.id or False,
                'invoice_line_ids': invoice_line_ids,
            })
            obj.write({'invoice_ids': [[6, 0, [invoice_id.id]]],
                       'is_invoice_created': True})
