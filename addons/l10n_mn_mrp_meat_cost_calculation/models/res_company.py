# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class ResCompany(models.Model):
    _inherit = "res.company"

    overhead_expense_journal_id = fields.Many2one('account.journal', string='Overhead Expense Journal')
    overhead_expense_account_id = fields.Many2one('account.account', string='Overhead Expense Account')
    direct_labor_account_id = fields.Many2one('account.account', string='Direct Labor Account')
