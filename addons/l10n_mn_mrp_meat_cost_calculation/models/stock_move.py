# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api


class StockMove(models.Model):
    _inherit = 'stock.move'

    # Барааны хөдөлгөөнөөс ҮЗ-д бүртгэсэн борлуулалтын үнийн хүснэгтийн
    # дагуу дайварт өртгийг хуваарилахын тулд нийт өртгийг тооцоолно.
    def get_total_standard_cost(self):
        total = 0
        for obj in self:
            if obj.production_id.bom_id.is_unit_type:
                for move in obj.production_id.move_finished_ids:
                    if move.product_id.id != obj.production_id.product_id.id:
                        move.total_standard_cost = move.product_uom_qty * move.standard_cost
                        total += move.total_standard_cost
                for move in obj.production_id.move_raw_ids:
                    move.total_standard_cost = move.product_uom_qty * move.product_id.standard_price
                    move.price_unit = move.production_id.purchase_order_line_id.price_unit
                    move.product_id.standard_price = move.production_id.purchase_order_line_id.price_unit
            else:
                for move in obj.production_id.move_finished_ids:
                    move.write({'total_standard_cost': move.quantity_done * move.standard_cost})
                    total += move.total_standard_cost
        return total

    # Барааны хөдөлгөөнөөс ҮЗ-д бүртгэсэн борлуулалтын үнийн хүснэгтийн үнийг тохируулж байна.
    @api.multi
    def compute_real_cost(self):
        price_list_item_obj = self.env['product.pricelist.item']
        for obj in self:
            if obj.production_id:
                if obj.production_id.bom_id.is_unit_type:
                    if obj.product_id.id != obj.production_id.product_id.id:
                        if obj.production_id.production_cost_pricelist_id:
                            price_list_id = obj.production_id.production_cost_pricelist_id
                            price_list_item_ids = price_list_id.item_ids
                            price_list_id = price_list_item_obj.search(
                                [('id', 'in', price_list_item_ids.ids),
                                 ('product_id', '=', obj.product_id.id)], limit=1)
                            if price_list_id:
                                obj.standard_cost = price_list_id.fixed_price
                else:
                    if obj.production_id.production_cost_pricelist_id:
                        price_list_id = obj.production_id.production_cost_pricelist_id
                        price_list_item_ids = price_list_id.item_ids
                        price_list_id = price_list_item_obj.search(
                            [('id', 'in', price_list_item_ids.ids),
                             ('product_id', '=', obj.product_id.id)], limit=1)
                        if price_list_id:
                            obj.standard_cost = price_list_id.fixed_price

    # Барааны хөдөлгөөнөөс ҮЗ-д бүртгэсэн борлуулалтын үнийн хүснэгтийн дагуу дайварт өртгийг хуваарилж байна.
    @api.multi
    def compute_total_standard_cost(self):
        for obj in self:
            total = obj.get_total_standard_cost()
            if obj.production_id.bom_id.is_unit_type:
                if obj.product_id.id != obj.production_id.product_id.id:
                    if obj.quantity_done and total:
                        price_unit = (obj.total_standard_cost / total) * obj.production_id.production_cost_amount
                        obj.total_real_cost = price_unit
                        obj.price_unit = price_unit / obj.quantity_done
                        obj.product_id.product_tmpl_id.standard_price = price_unit / obj.quantity_done
            else:
                if obj.quantity_done and total:
                    price_unit = (obj.total_standard_cost / total) * obj.production_id.production_cost_amount
                    obj.total_real_cost = price_unit
                    obj.price_unit = price_unit / obj.quantity_done
                    obj.product_id.product_tmpl_id.standard_price = price_unit / obj.quantity_done


    standard_cost = fields.Float(string='Standard Cost')
    total_standard_cost = fields.Float(string='Total Standard Cost')
    total_real_cost = fields.Float('Total Real Cost')
