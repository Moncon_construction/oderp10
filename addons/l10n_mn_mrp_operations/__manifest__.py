# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'Mongolian Manufacturing Operations',
    'version': '1',
    'category': 'Manufacturing',
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'sequence': 15,
    'depends': ['l10n_mn_mrp'],
    'description': """
        If the ordering of production is executed, the contractor will review and approve the performance.
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/mrp_workorder_view.xml',
        'views/mrp_performance_view.xml',
        'wizard/mrp_performance_wizard_view.xml',
        'wizard/change_qty_workorder_wizard_view.xml',
    ],
    'demo': [
        ],
    'application': True,
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
