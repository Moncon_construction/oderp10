# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from datetime import timedelta
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from datetime import timedelta, datetime

class MrpWorkorderPerformance(models.Model):
    _name = 'mrp.workorder.performance'
    _order = 'performance_date_start DESC'

    @api.depends('workcenter_id')
    def _default_product_uom(self):
		for obj in self:
			if obj.workcenter_id:
				if not obj.workcenter_id.workcenter_uom:
					raise UserError(_('Workcenter Product Uom Not Found!'))
				obj.product_uom = obj.workcenter_id.workcenter_uom.id

    workorder_id = fields.Many2one('mrp.workorder', 'Workorder')
    performance_date_start = fields.Datetime('Start Date')
    performance_date_stop = fields.Datetime('End Date')
    employee_id = fields.Many2one('hr.employee', 'MRP Employee')
    quantity = fields.Float('Quantity')
    workcenter_id = fields.Many2one('mrp.workcenter', 'Procedure')
    product_uom = fields.Many2one(
    	compute=_default_product_uom, comodel_name='product.uom')
    state = fields.Selection([('draft', 'Draft'),
                           ('confirmed', 'Confirmed')],
                          'State', default='draft', required=True, readonly=True)
    button_clicked = fields.Selection([('done', 'Done'),  # Хийгдсэн төлөвт орох
                                    # Батлагдсан төлөвт орох
                                    ('confirmed', 'Confirmed'),
                                    ('draft', 'Draft')])  # Ноороглох төлөвт орох
    account_move_id = fields.Many2one('account.move', string='Account Move ID')

    @api.model
    def create(self, vals):
    	res = super(MrpWorkorderPerformance, self).create(vals)
    	if res.workorder_id.workcenter_id:
      		res.write({'workcenter_id': res.workorder_id.workcenter_id.id})
      	return res
    
    '''Ажлын захиалгын гүйцэтгэл батлах'''

    @api.multi
    def action_to_confirm(self):

		account_move_obj = self.env['account.move']
		account_move_line_obj = self.env['account.move.line']
		account_journal = self.env['account.journal'].search(
			[('code', '=', 'MRP01')])
		for line in self:
 			line.write({'button_clicked': 'confirmed'})
 			if line.quantity > 0:
	 		 	line.write({'state': 'confirmed'})
	 		 	line.workorder_id.qty_producing = line.workorder_id.onchange_line_qty() + \
                                    line.quantity
	 		else:
	 			raise UserError(_('Please set the quantity. It can not be negative'))
			move_name = line.workorder_id.name + ' / ' + \
				'%s' % (self.env['ir.sequence'].get('account.move'))
			"""
			Ажлын захиалгын гүйцэтгэл батлах үед:
				- Журналийн бичилт хийгдэх дансууд:
					+ Кредит : Барааны ангилалаас ДҮ данc
					+ Дебит : Ажлын захиалга - Дамжлага - Нэмэлт Зардлууд
			"""

			move_lines = []
			total = 0.0
	        for e_line in line.workorder_id.workcenter_id.expense_line:
				credit = e_line.unit_cost
				total = total + credit
				credit_line_vals = {
                                    'name': e_line.overhead_expense.name,
                                    'date_maturity': datetime.now(),
                      							       'account_id': e_line.overhead_expense.account_id.id,
                      							       'partner_id': line.workorder_id.production_id.user_id.partner_id.id,
                      							       'credit': credit,
                      							       'quantity': line.quantity or 1,
                                }
				move_lines.append((0, 0, credit_line_vals))
	        debit = total
	        total = 0.0
	        debit_line_vals = {
                    'name': (_('Overhead Expenses')),
                    'date_maturity': datetime.now(),
            								'account_id': line.workorder_id.production_id.product_id.categ_id.work_in_progress_account_id.id,
            								'partner_id': line.workorder_id.production_id.user_id.partner_id.id,
            								'debit': debit if debit else 0.0,
            								'quantity': line.quantity or 1,
                }
	        move_lines.append((0, 0, debit_line_vals))
	        move_values = {
                    'name': move_name,
                    'journal_id': account_journal.id,
              						'date': datetime.now(),
              						'state': 'posted',
              						'description': line.workorder_id.name,
              						'line_ids': move_lines
                }
	        move_id = account_move_obj.create(move_values)
	        self.account_move_id = move_id.id

 		return {'type': 'ir.actions.client', 'tag': 'reload', }

    '''Ажлын захиалгын гүйцэтгэл ноороглох'''

    @api.multi
    def action_to_draft(self):
 		model_obj = self.env['ir.model.data']

		mrp_manager_group = model_obj.get_object('mrp', 'group_mrp_manager')
		mrp_managers = self.env['res.users'].search(
			[('groups_id', 'in', mrp_manager_group.id)])

 		for line in self:
 			if line.button_clicked == 'done':
 				# хийгдсэн төлөвт байгаа гүйцэтгэлийн мөрийг устгаж болохгүй
 				raise UserError(_('You can not delete done operation.'))
			else:
				if not self.env.user in mrp_managers:  # зөвхөн manager эрхтэй хүн ноороглож болно
					raise UserError(_('Unable to delete the approved performance.'))
				else:
		 			if line.quantity > 0:
						self.write({'state': 'draft'})
						line.workorder_id.qty_producing = line.workorder_id.onchange_line_qty() - \
                                                    line.quantity
					line.account_move_id.write({'state': 'draft'})
					line.account_move_id.unlink()

 		return {'type': 'ir.actions.client', 'tag': 'reload', }

    '''Ажлын захиалгын гүйцэтгэлийг нөхцөл шалган устгах'''

    @api.multi
    def unlink(self):
 		model_obj = self.env['ir.model.data']

		mrp_manager_group = model_obj.get_object('mrp', 'group_mrp_manager')
		mrp_managers = self.env['res.users'].search(
			[('groups_id', 'in', mrp_manager_group.id)])
		for line in self:
			if line.button_clicked == 'done':
				# хийгдсэн төлөвт байгаа гүйцэтгэлийн мөрийг устгаж болохгүй
				raise UserError(_('You can not delete done workorder operation.'))
			elif line.button_clicked == 'draft':
				super(MrpWorkorderPerformance, self).unlink()
			else:
				if not self.env.user in mrp_managers:
					if line.state == 'confirmed':
						raise UserError(_('Unable to delete the approved performance.'))
					else:
						line.workorder_id.qty_producing -= line.quantity
				else:
					line.workorder_id.qty_producing -= line.quantity

		return super(MrpWorkorderPerformance, self).unlink()
