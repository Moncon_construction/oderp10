# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from datetime import timedelta, datetime
from odoo import api, fields, models, _
from odoo.tools import float_compare, float_round
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError

class MrpWorkorder(models.Model):
	_inherit = 'mrp.workorder'
	
 	@api.one
	def get_default_accounts(self):
		accs = []
		if self.performance_ids:
			for line in self.performance_ids:
				if line.state == 'confirmed':
					accs.append(line.account_move_id.id)
		self.account_move_ids = accs

	current_quantity = fields.Float(string='Remaining quantity', digits=dp.get_precision('Product Unit of Measure'))
  	performance_ids = fields.One2many('mrp.workorder.performance', 'workorder_id', 'Performance')
 	account_move_ids = fields.Many2many('account.move', 'workorder_id', 'acc_mrp_work_rel', 'acc_mrp_work_rels', compute=get_default_accounts, string='Account Move IDs')
 	duration_expected = fields.Float('Standar Duration', digits=(16, 2), states={'done': [('readonly', True)], 'cancel': [('readonly', True)]})
 	product_id = fields.Many2one('product.product', string='The MRP Product')

	@api.multi
	def write(self, vals):
		for obj in self:
			total = 0.0
			for line in obj.performance_ids:
				if line.state == 'confirmed':
					total += line.quantity
			vals['qty_producing'] = total
		return super(MrpWorkorder, self).write(vals)

 	'''Ажлын захиалгын батлагдсан гүйцэтгэлийг харуулах'''

	def onchange_line_qty(self):
		total = 0.0
		for obj in self:
			for line in obj.performance_ids:
				if line.state == 'confirmed':
					total += line.quantity
		return total
	'''
		Ажлын гүйцэтгэлийг батлагч
	'''
	@api.one
	def button_confirm_performance(self):
		for mrp in self:
 			for line in mrp.performance_ids:
 				if line.quantity > 0:
	 				line_ids = self.env['mrp.workorder.performance'].search(
	 					[('id', 'in', mrp.performance_ids.ids), ('state', '=', 'draft')])
	 				summary = sum([line.quantity for line in line_ids])
	 				mrp.qty_producing = summary
	 				for line in line_ids:
	 					line.write({'state': 'confirmed'})
	 					line.write({'button_clicked': 'confirmed'})
                	return True
                else:
 		 			raise UserError(_('Please set the quantity. It can not be negative'))
 	'''
		Үйлдвэрлэлийн захиалга үүсгэх
	'''
 	@api.multi
 	def record_production(self):
 		self.ensure_one()
 		if self.qty_producing <= 0:
 			raise UserError(
                            _('Please set the quantity you produced in the Current Qty field. It can not be 0!'))

 		if (self.production_id.product_id.tracking != 'none') and not self.final_lot_id:
 			raise UserError(_('You should provide a lot for the final product'))

 		# Update quantities done on each raw material line
 		raw_moves = self.move_raw_ids.filtered(lambda x: (x.has_tracking == 'none') and (
                    x.state not in ('done', 'cancel')) and x.bom_line_id)
 		for move in raw_moves:
 			if move.unit_factor:
 				rounding = move.product_uom.rounding
 				move.quantity_done = float_round(
                                    self.qty_producing * move.unit_factor, precision_rounding=rounding)

 		# Transfer quantities from temporary to final move lots or make them final
 		for move_lot in self.active_move_lot_ids:
 			# Check if move_lot already exists
 			if move_lot.quantity_done <= 0:  # rounding...
 				move_lot.sudo().unlink()
 				continue
 			if not move_lot.lot_id:
 				raise UserError(_('You should provide a lot for a component'))
 			# Search other move_lot where it could be added:
 			lots = self.move_lot_ids.filtered(lambda x: (x.lot_id.id == move_lot.lot_id.id) and (
                            not x.lot_produced_id) and (not x.done_move))
 			if lots:
 				lots[0].quantity_done = move_lot.quantity_done
 				lots[0].lot_produced_id = self.final_lot_id.id
 				move_lot.sudo().unlink()
 			else:
 				move_lot.lot_produced_id = self.final_lot_id.id
 				move_lot.done_wo = True

 		# One a piece is produced, you can launch the next work order
 		if self.next_work_order_id.state == 'pending':
 			self.next_work_order_id.state = 'ready'
 		if self.next_work_order_id and self.final_lot_id and not self.next_work_order_id.final_lot_id:
 			self.next_work_order_id.final_lot_id = self.final_lot_id.id

 		self.move_lot_ids.filtered(
                    lambda move_lot: not move_lot.done_move and not move_lot.lot_produced_id and move_lot.quantity_done > 0
 		).write({
                    'lot_produced_id': self.final_lot_id.id,
                 			'lot_produced_qty': self.qty_producing
 		})

 		# If last work order, then post lots used
 		# TODO: should be same as checking if for every workorder something has been done?
 		if not self.next_work_order_id:
 			production_move = self.production_id.move_finished_ids.filtered(lambda x: (
                            x.product_id.id == self.production_id.product_id.id) and (x.state not in ('done', 'cancel')))
 			if production_move.product_id.tracking != 'none':
 				move_lot = production_move.move_lot_ids.filtered(
                                    lambda x: x.lot_id.id == self.final_lot_id.id)
 				if move_lot:
 					move_lot.quantity = self.qty_producing
 				else:
 					move_lot.create({'move_id': production_move.id,
                                            'lot_id': self.final_lot_id.id,
                                            'quantity': self.qty_producing,
                                            'quantity_done': self.qty_producing,
                                            'workorder_id': self.id,
                       })
 			else:
 				production_move.quantity_done = self.qty_producing  # TODO: UoM conversion?
 		# Update workorder quantity produced
 		self.qty_produced += self.qty_producing

		self.qty_producing = 0

 		# Set a qty producing
 		if self.qty_produced >= self.qty_workorder:
 			self.current_quantity = 0
 		elif self.production_id.product_id.tracking == 'serial':
 			self.qty_producing = 1.0
 			self._generate_lot_ids()
 		else:
 			self.current_quantity = self.qty_workorder - self.qty_produced
 			self._generate_lot_ids()

 		self.final_lot_id = False
 		if self.qty_produced >= self.qty_workorder:
 			self.button_finish()
 		for line in self.performance_ids:
 			line_ids = self.env['mrp.workorder.performance'].search(
                            [('id', 'in', line.ids), ('state', '=', 'confirmed')])
 			for line in line_ids:
	 			line.write({'button_clicked': 'done'})
 		return True
