# -*- coding: utf-8 -*-

 # Part of Odoo. See LICENSE file for full copyright and licensing details.
 
from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp  # @UnresolvedImport
from odoo.exceptions import UserError
import math


class ChangeWorkorderQty(models.TransientModel):
	_name = 'change.workorder.qty'
	_description = 'Change Quantity of Workorder'

	mo_id = fields.Many2one('mrp.workorder', 'Manufacturing Order', required=True)
	workorder_qty = fields.Float('Quantity To Produce', digits=dp.get_precision('Product Unit of Measure'), required=True)

	@api.model
	def default_get(self, fields):
		res = super(ChangeWorkorderQty, self).default_get(fields)
		if 'mo_id' in fields and not res.get('mo_id') and self._context.get('active_model') == 'mrp.workorder' and self._context.get('active_id'):
			res['mo_id'] = self._context['active_id']
		if 'workorder_qty' in fields and not res.get('workorder_qty') and res.get('mo_id'):
			res['workorder_qty'] = self.env['mrp.workorder'].browse(res['mo_id']).qty_produced
		return res

	@api.multi
	def change_prod_qty(self):
		self.mo_id.qty_produced = self.workorder_qty
