# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from datetime import timedelta
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError


class ContractPriceBreakdownGoods(models.TransientModel):
	_name = 'mrp.workorder.performance.wizard'
	_description = 'Wizard for work order performance'

	@api.model
	def _get_employee(self):
		employee = self.env['hr.employee'].search([('user_id', '=', self.env.uid)])
		if employee:
			return employee[0].id
		else:
			return False

	workorder_id = fields.Many2one('mrp.workorder', 'Work order',required=True, domain="[('state','in',('progress','ready'))]")
	employee_id = fields.Many2one('hr.employee', 'Employees', default=_get_employee, readonly=True)
	performance_date = fields.Date('Date', default=fields.Date.context_today)
	quantity = fields.Float('Quantity',required=True)
	workcenter_id = fields.Many2one('mrp.workcenter', 'Procedure')

	@api.onchange('workorder_id')
	def onchange_workorder(self):
		self.workcenter_id = self.workorder_id.workcenter_id


	@api.multi
	def create_performance(self):
		vals = {}
		for work_order_id in self.env['mrp.workorder'].search([('id','=',self.workorder_id.id)]):
			doNot=True
			if self.workorder_id.state in ('ready','progress'):
				if doNot:
					vals = {
						'employee_id': self.employee_id.id,
						'workorder_id': self.workorder_id.id,
						'quantity': self.quantity,
						'performance_date': self.performance_date,
						'workcenter_id': self.workcenter_id.id,
						'state': 'draft',
						'performance_ids': self.workorder_id.id
					}
					move = self.env['mrp.workorder.performance'].create(vals)
					return move.id
			else:
				raise UserError(_("Work orders can be created and repaired in the Processed Finish state."))
