# -*- coding: utf-8 -*-

{
    'name': "Manufacturing Overhead Expense in Bom",
    'version': '1.0',
    'depends': ['l10n_mn_mrp_account', 'l10n_mn_product_food'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
    Manufacturing Overhead Expense in Bom
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/mrp_bom_views.xml',
    ],
    'installable': True
}