# -*- coding: utf-8 -*-

from odoo import api, fields, models


class MrpWorkcenterOverheadExpense(models.Model):
    _inherit = 'mrp.workcenter.overhead.expense'

    bom_id = fields.Many2one('mrp.bom', 'Bom')

    
class MrpBom(models.Model):
    _inherit = 'mrp.bom'
    
    expense_line_ids = fields.One2many('mrp.workcenter.overhead.expense', 'bom_id', string='Manufacturing Overhead Expense')
    total_overhead_expense = fields.Float(string='Total Overhead Expense')
    
    @api.model
    def create(self, vals):
        if 'expense_line_ids' in vals.keys():
            total_overhead_expense = 0
            
            for expense_line_ids in vals['expense_line_ids']:
                if expense_line_ids[2]:
                    total_overhead_expense += expense_line_ids[2]['unit_cost']
        
            vals['total_overhead_expense'] = total_overhead_expense
            
        return super(MrpBom, self).create(vals)
    
    @api.multi
    def write(self, vals):
        res = super(MrpBom, self).write(vals)
        
        if 'expense_line_ids' in vals.keys():
            self.total_overhead_expense = sum(line.unit_cost for line in self.expense_line_ids)
            
        return res
    
