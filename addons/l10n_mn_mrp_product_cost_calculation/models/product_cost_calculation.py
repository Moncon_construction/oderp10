# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class ProductCostCalculation(models.Model):
    _inherit = 'product.cost.calculation'
     
    product_type = fields.Selection([('raw_material', 'Raw Material'), 
                                     ('semi_manufacture', 'Semi Manufacture'), 
                                     ('final_product', 'Final Product'),
                                     ('other_products', 'Other Products')], default='other_products', string='Product Type')
    
    def filter_product_by_type(self, products):
        # Өртөг угаах бараанд 'Барааны төрөл'-с хамааран шүүлт тавих
        self.ensure_one()
        
        filtered_products = products
        products = [pro for pro in products]
        
        if self.product_filter == 'with_stock_move' and self.product_type and products:
            filtered_products = False
            
            product_ids = self.env['product.product'].search([('id', 'in', products)])
            raw_product_ids = self.env['product.product'].search([('id', 'in', self.env['mrp.bom'].filter_raw_materials(products, self.company_id) or [])])
            bom_product_ids = self.env['product.product'].search([('id', 'in', self.env['mrp.bom'].filter_bom_products(products, self.company_id) or [])])
            
            if self.product_type == 'raw_material':
                filtered_products = list(set(raw_product_ids) - set(bom_product_ids))
            elif self.product_type == 'semi_manufacture':
                filtered_products = list(set(raw_product_ids) & set(bom_product_ids))
            elif self.product_type == 'final_product':
                filtered_products = list(set(bom_product_ids) - set(raw_product_ids))
            else:
                filtered_products = list(set(product_ids) - set(bom_product_ids) - set(raw_product_ids))
                
            filtered_products = [pro.id for pro in filtered_products] if filtered_products else []

        return filtered_products
    
    def sort_ready_products(self, product_ids):
        # Өртөг угаах барааны угаах дарааллыг дараахаар тогтооно
        # 1. Үйлдвэрлэлд ямар ч хамаагүй бараанууд
        # 2. ТЭМ
        # 3. Хагас боловсруулсан бүтээгдэхүүн /Орцонд орсон бараа өөрөө ахиад орцтой эсэхийг тооцно./
        # 4. Эцсийн бүтээгдэхүүн
        self.ensure_one()
        sorted_pro_ids = []
        
        raw_product_ids = self.env['product.product'].search([('id', 'in', self.env['mrp.bom'].filter_raw_materials(product_ids.ids, self.company_id) or [])])
        bom_product_ids = self.env['product.product'].search([('id', 'in', self.env['mrp.bom'].filter_bom_products(product_ids.ids, self.company_id) or [])])
        founded = False
        
        raw_material = list(set(raw_product_ids) - set(bom_product_ids))
        semi_manufacture = list(set(raw_product_ids) & set(bom_product_ids))
        final_product = list(set(bom_product_ids) - set(raw_product_ids))
        other_products = list(set(product_ids) - set(bom_product_ids) - set(raw_product_ids))
            
        # Хагас боловсруулсан бүтээгдэхүүнийг эрэмбэлэх
        semi_manufacture_seqed_pros = list()
        if semi_manufacture:
            sub_index = 1
            sub_productions = self.env['mrp.production'].search([('company_id', '=', self.company_id.id), ('product_id', 'in', [pro.id for pro in semi_manufacture])])
            sub_production_product_ids = sub_productions.mapped('product_id')
            sub_pro_dic = {}
            
            while sub_productions:
                sub_pro_dic[sub_index] = sub_productions
                for mrp_product in sub_productions.mapped('product_id'):
                    if mrp_product in semi_manufacture:
                        semi_manufacture.remove(mrp_product)
                sub_productions = self.env['mrp.production'].search([('company_id', '=', self.company_id.id), ('product_id', 'in', [pro1.id for pro1 in semi_manufacture]), ('origin', 'in', [obj.name for obj in sub_productions])])
                sub_index += 1
            
            for i in range(sub_index - 1, 0, -1):
                semi_manufacture_seqed_pros.extend(list(set(sub_pro_dic[i].mapped('product_id').filtered(lambda x: x not in semi_manufacture_seqed_pros))))

        # Хагас боловсруулсан бараанд үйлдвэрлэлийн бус хөдөлгөөн хийгдсэн бол дараах if нөхцөл рүү орно.
        if semi_manufacture:
            semi_manufacture_seqed_pros.extend(semi_manufacture)

        # Орж ирсэн бараануудыг эрэмбэлж буцаах
        sorted_pro_ids.extend(other_products)
        sorted_pro_ids.extend(raw_material)
        sorted_pro_ids.extend(semi_manufacture_seqed_pros)
        sorted_pro_ids.extend(final_product)
        
        if len(sorted_pro_ids) > 0 and isinstance(sorted_pro_ids[0], list):
            return sorted_pro_ids[0]

        return sorted_pro_ids
        
    @api.multi
    def get_mrp_final_product_price(self, move):
        # Үйлдвэрлэлийн эцсийн бүтээгдэхүүни өртгийг түүхий эд материалын өртгөөр тооцож явуулах
        if not move.production_id:
            return move.price_unit
        sum_cost = sum([move_raw.price_unit * move_raw.product_uom_qty for move_raw in move.production_id.move_raw_ids]) if move.production_id.move_raw_ids else 0
        cost = sum_cost / (move.production_id.product_qty or 1)
        if move.price_unit != cost:
            move.price_unit = cost
        return cost
    
    @api.multi
    def get_standard_price(self, new_standard_price, qty, move):
        # Өртгийг тооцоолох: Үйлдвэрлэлийн өртгийг түүхий эдийн өртгөөс авах
        if not (move.location_id and move.location_id.usage == 'production' and move.production_id):
            return super(ProductCostCalculation, self).get_standard_price(new_standard_price, qty, move)
        else:
            return (new_standard_price * qty + move.product_qty * self.get_mrp_final_product_price(move)) / (qty + move.product_qty) if qty + move.product_qty != 0 else 0
    