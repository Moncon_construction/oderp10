# -*- coding: utf-8 -*-
{
    'name': "Manufacturing Cost Calculation",
    'version': '1.0',
    'depends': ['l10n_mn_product_cost_calculation_each_wh', 'l10n_mn_mrp_product_cost_calculation'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Үйлдвэрлэлийн эцсийн бүтээгдэхүүний өртөг тооцолох багаж
       - ТЭМ-н өртгөөр эцсийн бүтээгдэхүүний өртгийг тооцож шинэчилнэ.
    """,
    'data': [
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}