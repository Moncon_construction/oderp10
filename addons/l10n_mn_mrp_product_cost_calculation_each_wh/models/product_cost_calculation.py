# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class ProductCostCalculation(models.Model):
    _inherit = 'product.cost.calculation'
        
    @api.multi
    def get_standard_price(self, new_standard_price, qty, move):
        # Өртгийг тооцоолох: Үйлдвэрлэлийн өртгийг түүхий эдийн өртгөөс авах
        if not (move.location_id and move.location_id.usage == 'production' and move.production_id):
            return super(ProductCostCalculation, self).get_standard_price(new_standard_price, qty, move)
        else:
            product_qty = 0
            if move.location_dest_id.id in qty.keys():
                product_qty = qty[move.location_dest_id.id]
            if move.location_dest_id.id in new_standard_price.keys():
                new_standard_price[move.location_dest_id.id] = (new_standard_price[move.location_dest_id.id] * product_qty + self.get_mrp_final_product_price(move) * move.product_qty) / \
                                                               (product_qty + move.product_qty) if product_qty + move.product_qty != 0 else 0
            else:
                new_standard_price[move.location_dest_id.id] = self.get_mrp_final_product_price(move)
            return new_standard_price[move.location_dest_id.id]
    
