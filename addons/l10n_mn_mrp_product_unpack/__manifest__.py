# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'MRP - Unpack Product Easily',
    'version': '2.1',
    'category': 'Mongolian Modules',
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'sequence': 15,
    'depends': ['l10n_mn_mrp'],
    'description': """
        Бараа задлах үйлдлийг хялбар хийнэ
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/mrp_product_unpack_view.xml',
        'views/sequence_view.xml',
    ],
    'demo': [
        ],
    'application': True,
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
