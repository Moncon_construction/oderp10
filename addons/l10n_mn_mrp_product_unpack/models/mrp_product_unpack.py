# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError
from odoo import api, fields, models, _

class MrpProductUnpack(models.Model):
    _name = 'mrp.product.unpack'

    name = fields.Char(string='Number', required=True, copy=False, default=lambda self: self.env['ir.sequence'].next_by_code('mrp.product.unpack'))
    date = fields.Date('Date', required=True)
    product_id = fields.Many2one('product.product', 'Product', required=True)
    uom_id = fields.Many2one(related='product_id.uom_id', required=True)
    product_qty = fields.Float('Product Quantity', required=True)
    bom_id = fields.Many2one('mrp.bom', 'Bill Of Material', required=True, domain="[('product_id','=',product_id.product_tmpl_id)]")
    state = fields.Selection([('draft', 'Draft'), ('done', 'Done')], string='State', required=True, readonly=True, copy=False, default='draft')
    picking_type_id = fields.Many2one('stock.picking.type', 'Stock Picking Type', required=True)
    production_id = fields.Many2one('mrp.production', 'MRP Production')
    
    @api.onchange('bom_id')
    def _onchange_bom_id(self):
        self.product_qty = self.bom_id.product_qty
    
    @api.multi
    def make_draft(self):
        if self.production_id.state == 'done':
            raise UserError(_('Production is done. Please cancel production first.'))
        self.state = 'draft'
            
        
    @api.multi
    def produce(self):
        mrp_obj = self.env['mrp.production']
        for obj in self:
            production_id = mrp_obj.create({
                'product_id': obj.product_id.id,
                'product_uom_id': obj.product_id.uom_id.id,
                'product_qty': obj.product_qty,
                'bom_id': obj.bom_id.id,
                'picking_type_id': obj.picking_type_id.id
                })
            
            self.production_id = production_id
            #production_id.button_confirm()
            production_id.action_assign()
            #if production_id.state == 'confirmed':
            #    raise UserError(_('There are not enough resources in this warehouse'))
            production_id.button_mark_done()
            self.state = 'done'
            
        return True