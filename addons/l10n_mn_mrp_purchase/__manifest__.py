# -*- coding: utf-8 -*-
{
    'name': 'Manufacturing with Purchase',
    'version': '1.0',
    'category': 'Mongolian Modules',
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'sequence': 15,
    'depends': ['l10n_mn_mrp', 'l10n_mn_mrp_account','l10n_mn_purchase'],
    'description': """
        Үйлдвэрлэлийн захиалгын худалдан авалттай холбогдох нэмэлт хэсгүүд.
    """,
    'data': [
        'views/purchase_order_view.xml',
        'views/mrp_bom_view.xml',
        'views/mrp_production_view.xml'
    ],
    'demo': [
    ],
    'application': True,
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}
