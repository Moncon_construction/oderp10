# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _


class MrpProduction(models.Model):
    _inherit = 'mrp.production'

    purchase_order_id = fields.Many2one('purchase.order', string='Origin Purchase Order')
    purchase_order_line_id = fields.Many2one('purchase.order.line', string='Origin Purchase Line Order')
    supplier_partner_id = fields.Many2one('res.partner', string='Supplier')

    @api.onchange('purchase_order_id')
    def onchange_supplier_partner_id(self):
        for obj in self:
            if obj.purchase_order_id:
                obj.supplier_partner_id = obj.purchase_order_id.partner_id.id
            else:
                obj.supplier_partner_id = False