# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from odoo.exceptions import UserError


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    production_count = fields.Integer(compute='_compute_production', string='Receptions', default=0)
    production_ids = fields.Many2many('mrp.production', compute='_compute_production', string='Productions', copy=False)

    def _compute_production(self):
        for order in self:
            production_obj = self.env['mrp.production']
            production_ids = production_obj.search([('purchase_order_id', '=', order.id)])
            if production_ids:
                order.production_ids = production_ids
                order.production_count = len(production_ids)

    def view_mrp_production(self):
        for obj in self:
            for pick in obj.picking_ids:
                if pick.state != 'done':
                    raise UserError(_('You must be approve stock picking %s' % pick.name))
            action = self.env.ref('mrp.mrp_production_action')
            result = action.read()[0]

            # override the context to get rid of the default filtering on picking type
            result.pop('id', None)
            result['context'] = {}
            prod_ids = sum([obj.production_ids.ids for order in self], [])
            # choose the view_mode accordingly
            if len(prod_ids) > 1:
                result['domain'] = "[('id','in',[" + ','.join(map(str, prod_ids)) + "])]"
            elif len(prod_ids) == 1:
                res = self.env.ref('mrp.mrp_production_form_view')
                result['views'] = [(res and res.id or False, 'form')]
                result['res_id'] = prod_ids and prod_ids[0] or False
            return result

    @api.multi
    def create_mrp_production_from_purchase_order(self):
        for obj in self:
            production_obj = self.env['mrp.production']
            bom_obj = self.env['mrp.bom']
            bom_line_obj = self.env['mrp.bom.line']
            for line in obj.order_line:
                if line.create_production:
                    bom_id = bom_obj.search([('is_purchase_bom', '=', True)])
                    if bom_id:
                        bom_line_id = bom_line_obj.search(
                            [('product_id', '=', line.product_id.id), ('bom_id', 'in', bom_id.ids)])
                        if bom_line_id:
                            production_obj.create({
                                'purchase_order_id': obj.id,
                                'purchase_order_line_id': line.id,
                                'supplier_partner_id': obj.partner_id.id,
                                'date_planned_start': obj.date_order,
                                'company_id': obj.company_id.id,
                                'product_qty': line.product_qty,
                                'bom_id': bom_line_id.bom_id.id,
                                'product_uom_id': line.product_id.uom_id.id,
                                'product_id': bom_line_id.bom_id.product_id.id,
                                'picking_type_id': obj.picking_type_id.warehouse_id.manu_type_id.id,
                                'location_src_id': obj.picking_type_id.warehouse_id.manu_type_id.default_location_src_id.id,
                                'location_dest_id': obj.picking_type_id.warehouse_id.manu_type_id.default_location_dest_id.id,
                            })
                    else:
                        raise UserError(_('BOM not found from this product %s' % line.product_id.name))

    @api.multi
    def button_confirm(self):
        res = super(PurchaseOrder, self).button_confirm()
        for obj in self:
            obj.create_mrp_production_from_purchase_order()
        return res

    @api.multi
    def button_cancel(self):
        res = super(PurchaseOrder, self).button_cancel()
        for obj in self:
            obj.production_ids.action_cancel()
        return res


class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    avg_weight = fields.Float('Average Weigth')
    create_production = fields.Boolean('Create Production')
