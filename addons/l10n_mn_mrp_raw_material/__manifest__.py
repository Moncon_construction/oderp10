# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

# TODO: Үндсэн l10n_mn_mrp санхүүгээс хамаарахгүй байх тохиолдолд бүтцийн өөрчлөлт хийх
{
    'name': 'Mongolian Manufacturing Raw Materials',
    'version': '2.1',
    'category': 'Mongolian Modules',
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'sequence': 15,
    'depends': ['mrp', 'l10n_mn_mrp_operations','l10n_mn_mrp_account'],
    'description': """
        Managing production by work order.
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/mrp_raw_material_views.xml',
    ],
    'demo': [
        ],
    'application': True,
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
