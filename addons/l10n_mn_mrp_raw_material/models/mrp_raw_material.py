from odoo import api, fields, models


class MrpRawMaterialWorkorder(models.Model):
    _inherit = 'mrp.workorder'

    raw_material_ids = fields.One2many('mrp.raw.material', 'raw_material_id', 'Raw Material')
    compute_difference = fields.Float('Compute Difference', compute='_compute_difference')
    bom_id = fields.Many2one('mrp.bom', 'Bom')

    @api.onchange('raw_material_ids')
    def _compute_difference(self):
        for material in self:
            for raw_material in material.raw_material_ids:
                if raw_material.consumed_qty > 0 or raw_material.receipt_qty > 0:
                    difference = raw_material.provide_qty - raw_material.consumed_qty - raw_material.receipt_qty
                    raw_material.write({'difference': difference})


class MrpRawMaterial(models.Model):
    _name = 'mrp.raw.material'

    employee_id = fields.Many2one('hr.employee', 'Employee', required=True)
    product_id = fields.Many2one('product.product', 'Product')
    raw_material_id = fields.Many2one('mrp.workorder', 'Raw Material')
    provide_qty = fields.Float('Provide Quantity')
    consumed_qty = fields.Float('Consumed Quantity')
    receipt_qty = fields.Float('Receipt Quantity')
    difference = fields.Float('Difference')
    bom_line_id = fields.Many2one('mrp.bom.line', 'Bom')
    bom_id = fields.Many2one('mrp.bom', 'Bom')

    @api.onchange('bom_id')
    def _compute_difference(self):
        for a in self:
            a.bom_id = a.raw_material_id.production_id.bom_id
