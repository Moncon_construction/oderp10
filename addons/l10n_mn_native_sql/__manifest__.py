# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Native_SQL",
    'version': '10.0.1.0',
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
         Native SQl
    """,
    'data': [
        'views/native_view_sql.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
