# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
import ast
import json


class NativeSQL(models.Model):
    _name = 'native.sql'
    _description = 'Native SQL'

    name = fields.Text('Query', required=True, size=128)
    result = fields.Text('Result')

    @api.multi
    def check_sql(self, cr):
        table_body = ''
        query = self.name
        # THIS ONLY DELETE QUERY\
        delete = query.split()
        if delete[0].lower() == 'delete':
            self._cr.execute(query)
            row_count = self.env.cr.rowcount
            self.result = 'Query returned successfully:', row_count, 'rows affected'
        else:
            if delete[0].lower() == 'update':
                self._cr.execute(query)
                row_count = self.env.cr.rowcount
                self.result = 'Query returned successfully:', row_count, 'rows affected'
            else:
                if delete[0].lower() == 'insert':
                    self._cr.execute(query)
                    row_count = self.env.cr.rowcount
                    self.result = 'Query returned successfully:', row_count, 'rows affected'
                else:
                    if delete[0].lower() == 'select':
                        self._cr.execute(query)
                        # THIS ONLY SELECT QUERY
                        rows = self.env.cr.dictfetchall()
                        if rows:
                            line = 1
                            for r in rows:
                                if line == 1:
                                    table_body = str(r.keys()) + '\n'

                                univalue = str(r.values())

                                data = ast.literal_eval(univalue)
                                j = json.dumps(data)
                                t = j.encode('utf-8').decode('unicode-escape')
                                table_body = table_body + '\n' + t + '\n '
                                line += 1
                            self.result = table_body

                    else:
                        raise ValidationError(_("Wrong error"))







