# -*- coding: utf-8 -*-
{
    "name": "Mongolian Notification",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "description": """
Custom Notification
""",
    "website": "http://asterisk-tech.mn",
    "category": "Base",
    "depends": ['base', 'mail'],
    "init": [],
    "data": [
        'email_templates/notification_email_template.xml',
        'security/ir.model.access.csv',
        'views/notification_data_view.xml',
        'views/notification_view.xml',
    ],
    'license': 'GPL-3',
    "active": False,
    "installable": True,
}