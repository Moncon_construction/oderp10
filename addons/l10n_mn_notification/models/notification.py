# -*- coding: utf-8 -*-

import time
from datetime import date, datetime
from datetime import timedelta
from odoo import fields, models, api
import logging

_logger = logging.getLogger('openerp')


class Notification_Config(models.Model):
    _name = 'notification.config'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    name = fields.Char('Name')
    model_id = fields.Many2one('ir.model', 'Model')
    date_field = fields.Many2one('ir.model.fields', 'Date Field')
    identification_field = fields.Many2one('ir.model.fields', 'Identification Field')
    by_month = fields.Boolean('By Month')
    days = fields.Integer('Days')

    send_to_followers = fields.Boolean('Send To Followers')
    send_to_group = fields.Boolean('Send To Group')
    send_to_user_field = fields.Boolean('Send To User Field')
    send_to_user = fields.Boolean('Send To User')

    company_id = fields.Many2one('res.company', 'Company')
    groups = fields.Many2many('res.groups', 'notification_group_rel', 'notification_id', 'group_id', 'Groups')
    users = fields.Many2many('res.users', 'notification_user_rel', 'notification_id', 'user_id', 'Users')
    user_fields = fields.Many2many('res.users', 'notification_user_rel', 'notification_id', 'user_id', 'Users')
    user_field = fields.Many2one('ir.model.fields', 'User Field')
    Notification_histories = fields.One2many('notification.mail.history', 'notification_id', 'History')

    @api.multi
    def _send_notification(self, automatic=False):

        notification_ids = self.env['notification.config'].search([])

        for notification in notification_ids:


            record_ids = []
            records = []
            try:
                current_date = time.strftime('%Y-%m-%d')
                if notification.days > 0:
                    current_date = datetime.strptime(current_date, '%Y-%m-%d') + timedelta(days=notification.days)
                model = self.env['ir.model'].browse(notification.model_id.id)[0]
                "'сараар' талбар сонгох үед тухайн сард дуусах даалгаваруудын сануулгыг илгээх "
                if notification.by_month:

                    month = current_date.month

                    day = current_date.day

                    self.env.cr.execute('SELECT id  FROM %s WHERE extract(month from %s) = %s  and extract(day from %s) = %s ' % (str.replace(str(model.model), '.', '_'), notification.date_field.name, month, notification.date_field.name, day))
                    print 'SELECT id  FROM %s WHERE extract(month from %s) = %s  and extract(day from %s) = %s ' % (str.replace(str(model.model), '.', '_'), notification.date_field.name, month, notification.date_field.name, day)

                    record_ids = map(lambda x: x[0], self.env.cr.fetchall())
                    records = self.env[model.model].browse(record_ids)
                    "өдрөөр талбарыг сонгох үед даалгавар дуусах өдрөөс өмнөх өдөр нь сануулга илгээх"
                else:


                    record_ids = self.env[model.model].search([(notification.date_field.name, '=', current_date)]).ids
                    if record_ids:
                     records = self.env[model.model].browse(record_ids)

                user_ids = []
                "групп лүү сануулга илгээх"
                if notification.send_to_group:
                    sel_user_ids = self.env['res.users'].search([('groups_id', 'in', notification.groups.ids)])
                    if sel_user_ids:
                        for user_id in sel_user_ids:
                            if user_id not in user_ids:
                                user_ids.append(user_id)
                    else:
                        pass
                "хэрэглэгчрүү сануулга илгээх "
                if notification.send_to_user:
                    for user in notification.users:
                        if user.id not in user_ids:
                            user_ids.append(user.id)

                if records and user_ids:
                    users = self.env['res.users'].browse(user_ids)


                    for user in users:
                        email_ids = []
                        email_body = u'<table style="width:400px;" border="1">'

                        for record in records:
                            outgoing_email = self.env['ir.mail_server'].sudo().search([])
                            email_body += u'<tr><th align="left">' + u'%s' % record[notification.identification_field.name] + u'</th><td align="right">' + u'%s' % record[notification.date_field.name] + u'</td></tr>'
                        email_body += u'</table>'
                        vals = {
                            'state': 'outgoing',
                            'subject': u'%s %s' % (notification.name, notification.company_id.name),
                            'body_html': u'<p>Сайн байна уу </br></p><p>Дараах ажилтаны %s болоход %s өдөр үлдсэн байна. </p> %s  <p> Баярлалаа,</p><pre>--<br>OdooERP Автомат Имэйл </pre>' % (
                                notification.date_field.field_description, notification.days, email_body),
                            'email_to': user.partner_id.email,
                            'email_from': outgoing_email.smtp_user
                        }

                        email_ids = self.env['mail.mail'].create(vals)


                    if email_ids != []:
                        for email in email_ids:
                            email.send()
                    return True
                "сануулгын түүх"
                self.env['notification.mail.history'].create({
                        'date': time.strftime('%Y-%m-%d'),
                        'type': 'multiple',
                        'mail_users': [[6, False, [user_id for user_id in user_ids]]],
                        'notification_id': notification.id

                    })
                "дагагчид руу сануудга илгээх"
                all_follower_user_ids = []
                for record in records:
                    follower_user_ids = []

                    if notification.send_to_followers and record.message_follower_ids:
                        follower_user_ids = self.env['res.users'].search([('id', 'not in', [user_id for user_id in user_ids]), ('partner_id', 'in', record.message_follower_ids.ids)])

                    if notification.send_to_user_field:
                        follower_user_ids.append(record[notification.user_field.name].id)

                    action_id = self.env['ir.actions.act_window'].search([('res_model', '=', model.model), ('view_type', '=', 'form'), ('src_model', '=', False)])[0]

                    if follower_user_ids:
                        if record.partner_id and 'partner_id' in notification.identification_field.name:
                            rec_name = record.partner_id.name
                        else:
                            rec_name = ''
                        data = {
                            'name': notification.name,
                            'base_url': self.env['ir.config_parameter'].get_param('web.base.url'),
                            'action_id': action_id,
                            'doi': tuple(record_ids),
                            'db_name': self.env.cr.dbname,
                            'model': model.model,
                            'sender': self.env['res.users'].name,
                            'record_name': record.partner_id.name if rec_name else record[notification.identification_field.name],
                            'date': record[notification.date_field.name],
                            'field_name': notification.date_field.field_description,
                            'remaining_days': notification.days
                        }
                        template_id = self.env['ir.model.data'].get_object_reference('l10n_mn_notification', 'notification_email_template')[1]

                        users = self.env['res.users'].browse(follower_user_ids)
                        user_emails = []
                        for user in users:
                            user_emails.append(user.login)
                            template = self.env['mail.template'].browse(template_id)
                            template.send_mail(user.id, context=data)

                        for follower_id in follower_user_ids:
                            if follower_id not in all_follower_user_ids:
                                all_follower_user_ids.append(follower_id)



                if all_follower_user_ids:
                    self.env['notification.mail.history'].create({
                        'date': time.strftime('%Y-%m-%d'),
                        'type': 'single',
                        'mail_users': [[6, False, [user_id for user_id in all_follower_user_ids]]],
                        'notification_id': notification.id
                    })

                if automatic:
                    self.env.cr.commit()


            except Exception:
                if automatic:
                    self.env.cr.rollback()
                    _logger.exception('Failed to send  notification %s', notification.id)
                else:
                    raise


    "Хэрэглэгчрүү дуусч буй даалгаварын сануулга илгээх крон"
    @api.multi
    def _cron_send_notification(self):
        return self._send_notification(automatic=True)


class Notification_Mail_History(models.Model):
    _name = 'notification.mail.history'

    notification_id = fields.Many2one('notification.config', 'notification')
    date = fields.Datetime('Date')
    type = fields.Selection([('single', 'Single'), ('multiple', 'Multiple')], string='Type')
    mail_users = fields.Many2many('res.users', 'mail_history_user_rel', 'history_id', 'user_id', 'Users')
