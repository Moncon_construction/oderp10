# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Mongolian Outlook calendar sync',
    'version': '1.0',
    'sequence': 130,
    'author': 'Asterisk Technologies LLC',
    'website': 'http://asterisk-tech.mn',
    'depends': ['l10n_mn_base', 'l10n_mn_calendar', 'l10n_mn_hr'],
    'summary': 'Personal & Shared Calendar',
    'description': """
This is a Outlook calendar sync exchange system.
========================================

It supports:
------------
    - Calendar of events sync
    - outlook events sync
    - install pip install pyexchange

If you need to manage your meetings, you should install the CRM module.
    """,
    'category': 'Extra Tools',
    'data': [
        'views/res_users_view.xml',
        'views/res_config_views.xml',
        'views/outlook_event_download_cron.xml',
        'wizard/outlook_password_wizard_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
