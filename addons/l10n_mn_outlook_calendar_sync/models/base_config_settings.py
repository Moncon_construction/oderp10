# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
from odoo.exceptions import UserError


class BaseConfigSettings(models.TransientModel):
    _inherit = 'base.config.settings'

    ews_exchange_url = fields.Char(related='company_id.ews_exchange_url', string='Ews exchange url', default=lambda self: self.company_id.ews_exchange_url)
    outlook_days_to_download = fields.Integer(related='company_id.outlook_days_to_download', string='Outlook event days to sync',
                                              help='Number of days to sync. (This field number should be at least 1)',
                                              default=lambda self: self.company_id.outlook_days_to_download)
    is_outlook_erp_login_same = fields.Boolean(related='company_id.is_outlook_erp_login_same',
                                               string='Use ERP login in Outlook',
                                               help="Outlook login will be same as ERP login when this is checked")

    @api.multi
    def execute(self):
        if self.outlook_days_to_download is not (False or None) and self.outlook_days_to_download < 1:
            raise UserError(_("'Outlook event days to sync' field must be at least 1!."))
        if self.is_outlook_erp_login_same:
            for user in self.env['res.users'].search([('is_outlook_user', '=', True)]):
                user.write({'outlook_username': user.login})
        return super(BaseConfigSettings, self).execute()
