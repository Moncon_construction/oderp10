# -*- coding: utf-8 -*-
from odoo import api, fields, models, SUPERUSER_ID, _
from pyexchange import Exchange2010Service, ExchangeNTLMAuthConnection
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from cryptography.fernet import Fernet
import pytz
import logging
from odoo.exceptions import AccessDenied, AccessError, UserError, ValidationError, DeferredException
from odoo.addons.l10n_mn_web.models.time_helper import *
import pickle
_logger = logging.getLogger(__name__)


class Meeting(models.Model):
    _inherit = 'calendar.event'

    calendar_event_history = fields.Many2one('calendar.event.history')
    exchange_id = fields.Char('exchange id')
    change_key = fields.Char('exchange id')
    ews_exchange_url = fields.Char(related='company_id.ews_exchange_url', string='Ews exchange url')

    @api.model
    def create(self, values):
        meet = super(Meeting, self).create(values)
        meet.create_outlook(values)
        return meet

    def list_to_str_tuple(self, value):
        return str(value).replace('[', '(').replace(']', ')')

    # erp аас outlook руу арга хэмжээг үүсгэх хэсэг
    @api.model
    def create_outlook(self, values):
        user = self.env.user
        if self.env.user.is_outlook_user and self.env.user.outlook_username and self.env.user.outlook_password:
            name = values['name']
            description = values.get('description', '')
            start_datetime = values.get('start_datetime', values['start'])
            end_datetime = values.get('stop_datetime', values['stop'])
            start_date = values.get('start_date', values['start'])
            end_date = values.get('stop_date', values['stop'])
            service = self.get_outlook_service(user)

            mails = [partner.email for partner in self.partner_ids if partner.email]
            location = self.location_room_id.name
            event = service.calendar().new_event(
                subject=name,
                attendees=mails,
                location=location,
            )
            user_time_zone = pytz.UTC
            if start_datetime:
                event.start = user_time_zone.localize(datetime.strptime(str(start_datetime), DEFAULT_SERVER_DATETIME_FORMAT))
                event.end = user_time_zone.localize(datetime.strptime(str(end_datetime), DEFAULT_SERVER_DATETIME_FORMAT))
            else:
                if self.allday:
                    event.is_all_day = True
                    event.start = user_time_zone.localize(datetime.strptime(start_date, "%Y-%m-%d"))
                    event.end = user_time_zone.localize(datetime.strptime(start_date, "%Y-%m-%d"))
                else:
                    event.start = user_time_zone.localize(datetime.strptime(start_date, "%Y-%m-%d"))
                    event.end = user_time_zone.localize(datetime.strptime(end_date, "%Y-%m-%d"))

            event.html_body = u'%s' % description + u'</td></tr>'
            try:
                event.create()
            except Exception as ex:
                _logger.warning(ex)
            self.exchange_id = event.id
            self.change_key = event.change_key

    # erp аас outlook руу арга хэмжээг үүсгэх хэсэг
    @api.multi
    def update(self):
        if self.env.user.is_outlook_user and self.env.user.outlook_username and self.env.user.outlook_password:
            username = self.env.user.outlook_username
            f = Fernet(str(self.env.user.outlook_token_key))
            password = f.decrypt(str(self.env.user.outlook_password))
            ews_url = self.env.user.company_id.ews_exchange_url
            connection = ExchangeNTLMAuthConnection(url=ews_url,
                                                    username=username,
                                                    password=password)
            service = Exchange2010Service(connection)
            user_time_zone = pytz.UTC
            allday = self.allday
            if not allday:
                date_start = user_time_zone.localize(
                    datetime.strptime(str(self.start_datetime)[0:19], DEFAULT_SERVER_DATETIME_FORMAT))
                date_end = user_time_zone.localize(
                    datetime.strptime(str(self.stop_datetime)[0:19], DEFAULT_SERVER_DATETIME_FORMAT))
            else:
                date_start = user_time_zone.localize(datetime.strptime(str(self.start_date), "%Y-%m-%d"))
                date_end = user_time_zone.localize(datetime.strptime(str(self.stop_date), "%Y-%m-%d"))

            mails = [partner.email for partner in self.partner_ids]
            location = self.location_room_id.name
            exchange_id = self.exchange_id
            if exchange_id:
                try:
                    event = service.calendar().get_event(id=exchange_id)
                    event.subject = self.name
                    event.is_all_day = allday
                    event.end = date_end
                    event.start = date_start
                    if not location:
                        event.location = 'No location'
                    else:
                        event.location = location
                    event.attendees = mails
                    event.update()
                except Exception as ex:
                    _logger.warning(ex)

    # erp аас outlook руу арга хэмжээг засах хэсэг
    @api.multi
    def write(self, values):
        res = super(Meeting, self).write(values)
        if res:
            self.update()
        return res

    # erp аас арга хэмжээг устгахад outlook дээр устгах хэсэг
    @api.multi
    def unlink(self, can_be_deleted=True):
        user = self.env.user
        if user.is_outlook_user and user.outlook_username and user.outlook_password:
            service = self.get_outlook_service(user)
            for exchange in self:
                if not exchange.exchange_id:
                    exchange.exchange_id = 'no id'
                else:
                    try:
                        event = service.calendar().get_event(id=exchange.exchange_id)
                        event.cancel()
                    except Exception as ex:
                        _logger.warning(ex)
        return super(Meeting, self).unlink()

    #  outlook ийн арга хэмжээг erp руу татах функц
    @api.multi
    def get_outlook_event_cron(self):
        days_to_download = self.env.user.company_id.outlook_days_to_download or 1
        for user in self.env['res.users'].search([('is_outlook_user', '=', True), ('outlook_username', '!=', False), ('outlook_password', '!=', False)]):
            ews_url = self.env.user.company_id.ews_exchange_url
            user_tz = self.env.user.tz or pytz.utc
            local = pytz.timezone(user_tz)
            offset = local.utcoffset(datetime.now())
            user_datetime = datetime.now(tz=local)
            start_datetime = user_datetime.replace(hour=0, minute=0, second=0) - offset
            end_datetime = user_datetime.replace(hour=23, minute=59, second=59) - offset + timedelta(days=days_to_download - 1)
            start_datetime = start_datetime.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            end_datetime = end_datetime.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            if ews_url:
                service = self.get_outlook_service(user)
                my_calendar = service.calendar()
                user_time_zone = pytz.timezone(self.env.user.partner_id.tz)
                try:
                    events = my_calendar.list_events(
                        start=user_time_zone.localize(
                            datetime.strptime(str(start_datetime)[0:19], DEFAULT_SERVER_DATETIME_FORMAT)),
                        end=user_time_zone.localize(
                            datetime.strptime(str(end_datetime)[0:19], DEFAULT_SERVER_DATETIME_FORMAT)),
                        details=True
                    )
                    erp_events = self.search([('exchange_id', '!=', False), ('user_id', '=', user.id), ('start', '>=', start_datetime)]).ids
                    for event in events.events:
                        erp_event = self.search([('exchange_id', '=', event.id)]) if event.id else self
                        outlook_event_attendees = [attendee.email for attendee in event.attendees]
                        outlook_event_attendee_ids = self.env['res.partner'].search([('email', 'in', outlook_event_attendees)]).ids
                        outlook_event_attendee_ids.append(user.partner_id.id)
                        app = []
                        for attendees in outlook_event_attendee_ids:
                            if event.is_all_day:
                                start = get_day_like_display(str(event.start)[0:19], self.env.user).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                                end = str(event.end)[0:10]
                                copy_events = self.search([('name', '=', event.subject),('allday', '=', event.is_all_day),('start_date', '=', start[0:10]),('stop_date', '=',end),('partner_ids', '=', attendees)])
                            else:
                                copy_events = self.search([('name', '=', event.subject),('start_datetime', '=', str(event.start)),('stop_datetime', '=', str(event.end)),('partner_ids', '=', attendees)])
                            app = copy_events
                        if erp_event:
                            erp_event.change_diff_of_event(event, user)
                            if erp_event.id in erp_events:
                                erp_events.remove(erp_event.id)
                        else:
                            if app:
                                pass
                            else:
                                erp_event.create_from_outlook(event, user)
                    self.browse(erp_events).unlink()
                except Exception as ex:
                    _logger.warning(ex)
                    pass
            else:
                raise ValidationError(_('Error! EWS EXCHANGE URL not found please configure it in Settings -> General Settings'))

    def get_outlook_service(self, user_id):
        if user_id:
            username = user_id.outlook_username
            f = Fernet(str(user_id.outlook_token_key))
            password = f.decrypt(str(user_id.outlook_password))
            ews_url = self.env.user.company_id.ews_exchange_url
            connection = ExchangeNTLMAuthConnection(url=ews_url,
                                                    username=username,
                                                    password=password)
            return Exchange2010Service(connection)

    def change_diff_of_event(self, outlook_event, user):
        # core-н write функцын query-г авав. Query injection-ы хамгаалалт
        vals = dict()
        if self.change_key != outlook_event.change_key:
            vals['change_key'] = outlook_event.change_key
        if self.name != outlook_event.subject:
            vals['name'] = outlook_event.subject or 'No subject !!!'
        if self.allday != outlook_event.is_all_day:
            vals['allday'] = outlook_event.is_all_day
            if outlook_event.is_all_day:
                vals['start_date'] = outlook_event.start.strftime(DEFAULT_SERVER_DATE_FORMAT)
                vals['stop_date'] = outlook_event.end.strftime(DEFAULT_SERVER_DATE_FORMAT)
        if self.start_datetime != outlook_event.start:
            vals['start_datetime'] = outlook_event.start.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            vals['stop_datetime'] = outlook_event.end.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            vals['duration'] = (outlook_event.end - outlook_event.start).total_seconds() / 3600
        outlook_event_attendees = [attendee.email for attendee in outlook_event.attendees]
        outlook_event_attendee_ids = self.env['res.partner'].search([('email', 'in', outlook_event_attendees)]).ids
        partners_to_remove = []
        if self.partner_ids.ids != outlook_event_attendee_ids:
            for partner in self.partner_ids.ids:
                if partner not in outlook_event_attendee_ids:
                    partners_to_remove.append(partner)
                else:
                    outlook_event_attendee_ids.remove(partner)
            if user.partner_id.id in partners_to_remove:
                partners_to_remove.remove(user.partner_id.id)
            if partners_to_remove:
                self.env.cr.execute("DELETE FROM calendar_event_res_partner_rel WHERE calendar_event_id = %s AND res_partner_id in %s" % (self.id, self.list_to_str_tuple(partners_to_remove)))
            for partner in set(outlook_event_attendee_ids):
                self.env.cr.execute("""INSERT INTO calendar_event_res_partner_rel (calendar_event_id, res_partner_id) 
                                       VALUES (%s, %s)""" % (self.id, partner))
        updates = []
        for keys, val in vals.iteritems():
            updates.append((keys, '%s', val))
        query = 'UPDATE "%s" SET %s WHERE id = %%s' % (
            'calendar_event', ','.join('"%s"=%s' % (u[0], u[1]) for u in updates),
        )
        params = tuple(u[2] for u in updates if len(u) > 2)
        self.env.cr.execute(query, params + (self.id,))

    def create_from_outlook(self, outlook_event, user):
        # core-н create функцын query-г авав. Query injection-ы хамгаалалт
        vals = [
            ('id', "nextval('%s')" % self._sequence),
            ('name', '%s', outlook_event.subject or 'No subject !!!'),
            ('allday', '%s', outlook_event.is_all_day),
            ('exchange_id', '%s', outlook_event.id),
            ('start', '%s', outlook_event.start),
            ('stop', '%s', outlook_event.end),
            ('active', '%s', True),
            ('privacy', '%s', 'public'),
            ('user_id', '%s', user.id),
            ('create_uid', '%s', user.id),
        ]
        if outlook_event.is_all_day:
            vals.append(('start_date', '%s', outlook_event.end.strftime(DEFAULT_SERVER_DATE_FORMAT)))
            vals.append(('stop_date', '%s', outlook_event.end.strftime(DEFAULT_SERVER_DATE_FORMAT)))
        else:
            vals.append(('start_datetime', '%s', outlook_event.start.strftime(DEFAULT_SERVER_DATETIME_FORMAT)))
            vals.append(('stop_datetime', '%s', outlook_event.end.strftime(DEFAULT_SERVER_DATETIME_FORMAT)))
            vals.append(('duration', '%s', (outlook_event.end - outlook_event.start).total_seconds() / 3600))
        query = """INSERT INTO "%s" (%s) VALUES(%s) RETURNING id""" % (
                self._table,
                ', '.join('"%s"' % u[0] for u in vals),
                ', '.join(u[1] for u in vals),
            )
        self.env.cr.execute(query, tuple(u[2] for u in vals if len(u) > 2))
        event_id = self._cr.dictfetchall()[0]['id']
        outlook_event_attendees = [attendee.email for attendee in outlook_event.attendees]
        outlook_event_attendee_ids = self.env['res.partner'].search([('email', 'in', outlook_event_attendees)]).ids
        outlook_event_attendee_ids.append(user.partner_id.id)
        for partner in set(outlook_event_attendee_ids):
            self.env.cr.execute("""INSERT INTO calendar_event_res_partner_rel (calendar_event_id, res_partner_id) 
                                   VALUES (%s, %s)""" % (event_id, partner))
