# -*- coding: utf-8 -*-
from odoo import fields, models


class ResCompany(models.Model):

    _inherit = 'res.company'

    ews_exchange_url = fields.Char(string='Ews exchange url')
    is_outlook_erp_login_same = fields.Boolean(string='Use ERP login in Outlook',
                                               help="Outlook login will be same as ERP login when this is checked")
    outlook_days_to_download = fields.Integer(default=1)
