# -*- coding: utf-8 -*-
from odoo import fields, models, SUPERUSER_ID, api, _, registry
from cryptography.fernet import Fernet


class Users(models.Model):
    _inherit = "res.users"

    outlook_username = fields.Char(string="Outlook username")
    outlook_password = fields.Char(string="Outlook password")
    is_outlook_user = fields.Boolean(default=True)
    outlook_token_key = fields.Char(default=str(Fernet.generate_key()))
    is_outlook_erp_login_same = fields.Boolean(related='company_id.is_outlook_erp_login_same', readonly=1)

    @classmethod
    def _login(cls, db, login, password):
        user_id = super(Users, cls)._login(db, login, password)
        with registry(db).cursor() as cr:
            if user_id:
                self = api.Environment(cr, SUPERUSER_ID, {})[cls._name]
                user = self.browse(user_id)
                if user.company_id.is_outlook_erp_login_same:
                    f = Fernet(str(user.outlook_token_key))
                    user.write({
                        'outlook_username': login,
                        'outlook_password': f.encrypt(str(password))})

        return user_id

    def change_outlook_password(self):
        return {
            'name': _('Outlook password changing wizard'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'outlook.password.wizard',
            'type': 'ir.actions.act_window',
            'target': 'new'
        }

    @api.multi
    def write(self, vals):
        if self.is_outlook_erp_login_same:
            if vals.get('password'):
                f = Fernet(str(self.outlook_token_key))
                vals['outlook_password'] = f.encrypt(str(vals['password']))
            if vals.get('login'):
                vals['outlook_username'] = vals['login']
        return super(Users, self).write(vals)

