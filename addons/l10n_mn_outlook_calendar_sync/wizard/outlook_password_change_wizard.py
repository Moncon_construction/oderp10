# -*- coding: utf-8 -*-
from odoo import api, fields, models, SUPERUSER_ID, _
from pyexchange import Exchange2010Service, ExchangeNTLMAuthConnection
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from cryptography.fernet import Fernet
import pytz
import logging
from odoo.exceptions import AccessDenied, AccessError, UserError, ValidationError, DeferredException
from odoo.addons.l10n_mn_web.models.time_helper import *
import pickle
_logger = logging.getLogger(__name__)


class OutlookPasswordWizard(models.TransientModel):
    _name = "outlook.password.wizard"

    user_id = fields.Many2one('res.users', default=lambda self: self.env['res.users'].browse(self._context['active_id']), readonly=True)
    password = fields.Char()

    def change_password(self):
        f = Fernet(str(self.user_id.outlook_token_key))
        self.user_id.outlook_password = f.encrypt(str(self.password))
        self.write({'password': False})

