# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Partner Sequence",
    'version': '1.0',
    'depends': ['sale_stock', 'l10n_mn_base'],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Modules',
    'description': """
    Mongolian Partner Code Sequencer
    """,
    'data': [
        'views/res_company_view.xml',
        'views/sale_config_view.xml',
        'views/res_partner_category_view.xml',
    ],
    'license': 'GPL-3',
}
