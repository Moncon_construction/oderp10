# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _


class ResCompany(models.Model):
    _inherit = 'res.company'

    partner_sequence = fields.Selection([
        ('manual', 'Manual'),
        ('auto', 'Auto')], string='Partner Sequence', default='manual')
