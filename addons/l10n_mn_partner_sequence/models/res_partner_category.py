# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class ResPartnerCategoryHierarchy(models.Model):
    _inherit = "res.partner.category.hierarchy"

    def _check_sequence(self):
        for line in self:
            line.check_sequence = True if self.env.user.company_id.partner_sequence == 'auto' else False

    sequence_id = fields.Many2one('ir.sequence', store=True, string="Partner Sequence")
    check_sequence = fields.Boolean('Check Sequence', compute=_check_sequence)

    @api.model
    def create(self, vals):
        rec = super(ResPartnerCategoryHierarchy, self).create(vals)
        if not rec.sequence_id:
            rec._create_check_sequence()
        return rec

    @api.one
    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        rec = super(ResPartnerCategoryHierarchy, self).copy(default)
        rec._create_check_sequence()
        return rec

    @api.one
    def _create_check_sequence(self):
        # Борлуулалтын тохиргоон дээр харилцагчийн кодлолт автомат үед ангилал тус бүр дээр дараалал үүсгэх
        if self.env.user.company_id.partner_sequence == 'auto':
            self.sequence_id = self.env['ir.sequence'].sudo().create({
                'name': self.name + _(" : Partner Sequence"),
                'implementation': 'standard',
                'padding': 3,
                'number_increment': 1,
                'company_id': False,
                'code': 'res.partner' + self.code,
                'number_next': 1
            })

class ResPartner(models.Model):
    _inherit = "res.partner"

    @api.model
    def create(self, vals):
        # Борлуулалтын тохиргоон дээр харилцагчийн кодлолт автомат үед
        if self.env.user.company_id.partner_sequence == 'auto':
            # Харилцагчын кодчилолыг ангилалаас хамааран автоматаар авах
            if 'category_hierarchy_id' in vals:
                category = self.env['res.partner.category.hierarchy'].search([('id', '=', vals['category_hierarchy_id'])], limit=1)
                if category:
                    if not category.sequence_id:
                        category.write({})
                    # Ангилалын код + дарааллын кодын хамт өгөх
                    vals['ref'] = category.code + self.env["ir.sequence"].next_by_code(category.sequence_id.code)
        return super(ResPartner, self).create(vals)

    @api.multi
    def write(self, vals):
        # Борлуулалтын тохиргоон дээр харилцагчийн кодлолт автомат үед
        if self.env.user.company_id.partner_sequence == 'auto':
            # Ангилал өөрчилөгдсөн үед кодчилолыг дахин тооцох
            if vals.get('category_hierarchy_id'):
                category = self.env['res.partner.category.hierarchy']
                if 'category_hierarchy_id' in vals:
                    category = category.search([('id', '=', vals['category_hierarchy_id'])], limit=1)
                    if category.sequence_id:
                        vals['ref'] = category.code + self.env["ir.sequence"].next_by_code(category.sequence_id.code)
        return super(ResPartner, self).write(vals)
