# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _


class SaleConfig(models.TransientModel):
    _inherit = 'sale.config.settings'

    partner_sequence = fields.Selection([('manual', 'Manual'),
                                         ('auto', 'Auto')], default='manual', related='company_id.partner_sequence', string="Partner Sequence")
