# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    "name" : "Mongolian Password Policy",
    "version" : "1.0",
    "author" : "Asterisk Technologies LLC",
    "description": """Password Policy""",
    "website" : "http://asterisk-tech.mn",
    "category" : "base",
    "depends" : ['l10n_mn_base'],
    "init": [],
    "data" : [
        'security/ir.model.access.csv',
    ],
    'license': 'GPL-3',
    "active": False,
    "installable": True,
}
