# -*- coding: utf-8 -*-
from odoo.exceptions import UserError
from odoo import fields, models, _
from odoo import SUPERUSER_ID
import re
import logging
from collections import defaultdict

_logger = logging.getLogger(__name__)


class ResUsers(models.Model):
    _inherit = 'res.users'
    __uid_cache = defaultdict(dict)

    def write(self, vals):
        res = super(ResUsers, self).write(vals)
        if 'company_id' in vals:
            for user in self.browse(self.ids):
                # if partner is global we keep it that way
                if user.partner_id.company_id and user.partner_id.company_id.id != vals['company_id']:
                    user.partner_id.write({'company_id': user.company_id.id})
        # clear caches linked to the users
        self.env['ir.model.access'].call_cache_clearing_methods()
        db = self._cr.dbname
        for id in self.ids:
            self.__uid_cache[db].pop(id, None)
        self.context_get.clear_cache(self)
        if 'password' in vals:
            if re.match(r'[A-Za-z0-9@#$%^&+=]{8,}', vals['password']):
                if re.search(r'[A-Z]', vals['password']) and re.search(r'[a-z]', vals['password']) and re.search(
                        r'[0-9]', vals['password']):
                    res = super(ResUsers, self).write(vals)
                else:
                    raise UserError(_('Password length must be 8 characters or more, with large letters, numbers, and special characters.'))
            else:
                raise UserError(_('Password length must be 8 characters or more, with large letters, numbers, and special characters.'))
        return res

    def password_policy(self, uid, new_passwd):
        users = self.env['user.password.policy']
        user_obj = self.browse()
        if re.match(r'[A-Za-z0-9@#$%^&+=]{8,}', new_passwd):
            user = users.search(SUPERUSER_ID, [('user_id', '=', uid)])
            split_new_pass = re.split(r'[0-9@#$%^&+=.]', new_passwd)
            split_username = re.split(r'[0-9@#$%^&+=.]', str(user_obj.login))
            for split in split_new_pass:
                if split.lower() == split_username[0].lower():
                    raise UserError(_('Password length must be 8 characters or more, with large letters, numbers, and special characters.'))
            if user:
                users.check_password(uid, user[0], new_passwd)
            else:
                users.create(SUPERUSER_ID, {'sequence': '1', 'user_id': uid, 'first_password': new_passwd})
            return
        else:
            raise UserError(_('Password length must be 8 characters or more, with large letters, numbers, and special characters.'))

    def change_password(self, old_passwd, new_passwd):
        """Change current user password. Old password must be provided explicitly
        to prevent hijacking an existing user session, or for cases where the cleartext
        password is not used to authenticate requests.

        :return: True
        :raise: odoo.exceptions.AccessDenied when old password is wrong
        :raise: odoo.exceptions.UserError when new password is not set or empty
        """
        self.check(self._cr.dbname, self._uid, old_passwd)
        self.password_policy(self._uid, new_passwd)
        if new_passwd:
            return self.env.user.write({'password': new_passwd})
        raise UserError(_('Password length must be 8 characters or more, with large letters, numbers, and special characters.'))


class UserPasswordPolicy(models.Model):
    _name = 'user.password.policy'

    user_id = fields.Many2one('res.users', 'User')
    first_password = fields.Char('First Password')
    second_password = fields.Char('Second Password')
    third_password = fields.Char('Third Password')
    sequence = fields.Char('Sequence')

    def check_password(self, ids, new_passwd):
        user_obj = self.browse(SUPERUSER_ID, ids)
        if new_passwd == user_obj.first_password or new_passwd == user_obj.second_password or new_passwd == user_obj.third_password:
            raise UserError(_('This password is already used.'))
        elif user_obj.sequence == '3':
            if user_obj.third_password:
                self.write(SUPERUSER_ID, ids, {'first_password': new_passwd, 'sequence': '1'})
        elif user_obj.sequence == '1':
            if user_obj.first_password:
                self.write(SUPERUSER_ID, ids, {'second_password': new_passwd, 'sequence': '2'})
        elif user_obj.sequence == '2':
            if user_obj.second_password:
                self.write(SUPERUSER_ID, ids, {'third_password': new_passwd, 'sequence': '3'})
