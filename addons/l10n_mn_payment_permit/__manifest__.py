# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    "name" : "Mongolian Payment Permission",
    "version" : "1.0",
    "author" : "Asterisk Technologies LLC",
    "description": """Payment Permission""",
    "website" : "http://asterisk-tech.mn",
    "category" : "Account",
    "depends" : ['l10n_mn_account'],
    "init": [],
    "data" : [
        'security/security.xml',
        'security/ir.model.access.csv',        
        'wizard/account_invoice_add_view.xml',
        'views/payment_permit_view.xml'
    ],
    'license': 'GPL-3',
    "active": False,
    "installable": True,
}
