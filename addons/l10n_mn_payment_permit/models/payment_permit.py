# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
import time
import datetime
from odoo.exceptions import UserError


class TransactionPaymentPermission(models.Model):
    _name='transaction.payment.permission'
    _description='Transaction Payment Permission'
    _inherit = ['mail.thread']
    _order='date desc'

    def _mnt_sum(self):
        #Төгрөгийн нийлбэр бодох
        res = {}
        sum = 0
        for t_list in self:
            for l in t_list.transaction_lines:
                if l.currency.name == "MNT":
                    sum += l.pay_value
            t_list.mnt_sum = sum

    def _mnt_usd(self):
        #Долларын нийлбэр бодох
        res = {}
        sum = 0
        for t_list in self:
            for l in t_list.transaction_lines:
                if l.currency.name == "USD":
                    sum += l.pay_value
            t_list.mnt_usd = sum

    def _mnt_eur(self):
        #Еврогийн нийлбэр бодох
        res = {}
        sum = 0
        for t_list in self:
            for l in t_list.transaction_lines:
                if l.currency.name == "EUR":
                    sum += l.pay_value 
            t_list.mnt_eur = sum

    def _mnt_cny(self):
        #Юаны нийлбэр бодох
        res = {}
        sum = 0
        for t_list in self:
            for l in t_list.transaction_lines:
                if l.currency.name == "CNY":
                    sum += l.pay_value
            t_list.mnt_cny = sum

    date=fields.Date('Created Date', readonly=1, default=lambda *a: time.strftime('%Y-%m-%d'))
    company_id=fields.Many2one('res.company', 'Company',readonly=True,default=lambda self: self.env.user.company_id)
    transaction_lines=fields.One2many('payment.permit','transaction_id', 'Payment Permit', domain = [('state','!=','paid')])
    paid_transaction_lines=fields.One2many('payment.permit','transaction_id', 'Payment Permit', domain = [('state','=','paid')])
    mnt_sum=fields.Float(compute='_mnt_sum', string='Sum MNT')
    mnt_usd=fields.Float(compute='_mnt_usd', string='Sum USD')
    mnt_eur=fields.Float(compute='_mnt_eur', string='Sum EUR')
    mnt_cny=fields.Float(compute='_mnt_cny', string='Sum CNY')
    bank_cash_mnt_line_ids = fields.One2many('bank.cash.list.mnt', 'permission_id', 'Bank Cash List MNT', readonly=1)
    bank_cash_usd_line_ids = fields.One2many('bank.cash.list.usd','permission_id', 'Bank Cash List USD', readonly=1)
    bank_cash_eur_line_ids = fields.One2many('bank.cash.list.eur', 'permission_id', 'Bank Cash List EUR', readonly=1)
    bank_cash_cny_line_ids = fields.One2many('bank.cash.list.cny', 'permission_id', 'Bank Cash List CNY', readonly=1)
    bank_cash_krw_line_ids = fields.One2many('bank.cash.list.krw', 'permission_id', 'Bank Cash List KRW', readonly=1)
    bank_cash_jpy_line_ids = fields.One2many('bank.cash.list.jpy', 'permission_id', 'Bank Cash List JPY', readonly=1)
    bank_cash_unite_ids = fields.One2many('bank.cash.unite', 'permission_id', 'Bank Cash Unite', readonly=1)

    @api.model
    def create(self, vals):
        res = super(TransactionPaymentPermission, self).create(vals)
        # Нэгтгэл табны утга авах
        journals = self.env['account.journal'].search([('company_id', '=', res.company_id.id), ('type', 'in', ('bank', 'cash'))])
        journal_ids = []
        is_mnt_journal_id = False
        for journal in journals:
            if not journal.currency_id in journal_ids and journal.currency_id:
                journal_ids.append(journal.currency_id)
        for journal in journals:
            if not journal.currency_id:
                is_mnt_journal_id = True
        for unite in journal_ids:
            self.env['bank.cash.unite'].create({
                'permission_id': res.id,
                'currency_id': unite.id
            })
        if is_mnt_journal_id:
            self.env['bank.cash.unite'].create({
                'permission_id': res.id,
                'currency_id': self.env.user.company_id.currency_id.id
            })

        # MNT табны утга авах
        journals_mnt = self.env['account.journal'].search(
            [('currency_id.name', '=', 'MNT'), ('company_id', '=', res.company_id.id), ('type', 'in', ('bank', 'cash'))])
        journals_false = self.env['account.journal'].search(
            [('currency_id', '=', False), ('company_id', '=', res.company_id.id), ('type', 'in', ('bank', 'cash'))])
        for obj in journals_mnt:
            self.env['bank.cash.list.mnt'].create({
                'permission_id': res.id,
                'journal_id': obj.id
            })
        for obj in journals_false:
            self.env['bank.cash.list.mnt'].create({
                'permission_id': res.id,
                'journal_id': obj.id
            })

        # EUR табны утга авах
        journals_eur = self.env['account.journal'].search([('currency_id.name', '=', 'EUR'), ('company_id', '=', res.company_id.id), ('type', 'in', ('bank', 'cash'))])
        for obj in journals_eur:
            self.env['bank.cash.list.eur'].create({
                'permission_id': res.id,
                'journal_id': obj.id
            })

        # USD табны утга авах
        journals_usd = self.env['account.journal'].search([('currency_id.name', '=', 'USD'), ('company_id', '=', res.company_id.id), ('type', 'in', ('bank', 'cash'))])
        for obj in journals_usd:
            self.env['bank.cash.list.usd'].create({
                'permission_id': res.id,
                'journal_id': obj.id
            })
        # KRW табны утга авах
        journals_krw = self.env['account.journal'].search([('currency_id.name', '=', 'KRW'), ('company_id', '=', res.company_id.id), ('type', 'in', ('bank', 'cash'))])
        for obj in journals_krw:
            self.env['bank.cash.list.krw'].create({
                'permission_id': res.id,
                'journal_id': obj.id
            })
        # CNY табны утга авах
        journals_cny = self.env['account.journal'].search([('currency_id.name', '=', 'CNY'), ('company_id', '=', res.company_id.id), ('type', 'in', ('bank', 'cash'))])
        for obj in journals_cny:
            self.env['bank.cash.list.cny'].create({
                'permission_id': res.id,
                'journal_id': obj.id
            })
        # JPY табны утга авах
        journals_jpy = self.env['account.journal'].search([('currency_id.name', '=', 'JPY'), ('company_id', '=', res.company_id.id), ('type', 'in', ('bank', 'cash'))])
        for obj in journals_jpy:
            self.env['bank.cash.list.jpy'].create({
                'permission_id': res.id,
                'journal_id': obj.id
            })

        return res

    @api.multi
    def unlink(self):
        #Төлбөр зөвшөөрөх хуудас устгах
        for obj in self:
            for line in obj.transaction_lines:
                if line.state != 'draft':
                    raise UserError(_(u'Баталсан болон Төлсөн төлөвт устгах боломжгүй'))
                else:
                    line.unlink()
            for line in obj.bank_cash_unite_ids:
                line.unlink()
        return super(TransactionPaymentPermission, self).unlink()
    

    def archive_list(self):
        #Төлбөрийн зөвшөөрөх хуудас архивлах
         history_id = self.env['transaction.payment.permission.history'].search([('date', '=',self.date),( 'company_id','=', self.env.user.company_id.id)])
         if not history_id:
             history_id = self.env['transaction.payment.permission.history'].create({'date': self.date, 'company_id': self.env.user.company_id.id})
         if history_id:
             if self.message_ids:
                 for message in self.message_ids:
                     self.env['mail.message'].write({'model':"transaction.payment.permission.history", 'res_id': history_id.id})
             for transaction in self.transaction_lines:
                 transaction.write({'permission_history_id': history_id.id, 'transaction_id':False})
             for line in self.transaction_lines:
                 if line.state != 'paid':
                     raise UserError(_(u'Төлөөгүй нэхэмжлэл архивлах боломжгүй'))
                 else: 
                     line.unlink()
             return super(TransactionPaymentPermission, self).unlink()
                    
class TransactionPermissionHistory(models.Model):
    _name='transaction.payment.permission.history'
    _description='Transaction Permission History'
    _inherit = ['mail.thread']
    _order='date desc'
    
    date=fields.Date('Created Date', required=True)
    company_id=fields.Many2one('res.company', 'Company',readonly=True)
    transaction_history_lines=fields.One2many('payment.permit','permission_history_id', 'Payment Permit')
    
class PaymentPermit(models.Model):
    _name='payment.permit'
    _description='Payment Permit'
    _inherit = ['mail.thread']
    
    def _get_number_of_days(self, date_from, date_to):
        #Хугацааны хооронд хоног бодох
        """Returns a float equals to the timedelta between two dates given as string."""
        DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
        from_dt = datetime.datetime.strptime(date_from, DATETIME_FORMAT)
        to_dt = datetime.datetime.strptime(date_to, DATETIME_FORMAT)
        timedelta = to_dt - from_dt
        diff_day = timedelta.days + float(timedelta.seconds) / 86400
        return diff_day
    
    def _calculate_diff_day(self):
        #Зөрүү хоног тооцоолох
        res = {}
        diff = 0
        for tzh in self:
            diff_day=self._get_number_of_days(str(tzh.date) +' 00:00:00', time.strftime('%Y-%m-%d %H:%M:%S'))
            res[tzh.id]=round(diff_day,0)
        return res
    
    def _calculate_paid_value(self):
        #Төлсөн дүнг тооцоолох
        res = {}
        for t_list in self:
            paid_amount = 0.0
            if t_list.invoice_id:
                payment_permits=self.env['payment.permit'].search([('invoice_id','=',t_list.invoice_id.id),('state','in',['paid','closed'])])
                for list in payment_permits:
                    paid_amount+=list.pay_value    
                    t_list.paid_value = paid_amount

            elif t_list.expense_id:
                payment_permits=self.env['payment.permit'].search([('expense_id','=',t_list.expense_id.id),('state','in',['paid','closed'])])
                for list in payment_permits:
                    paid_amount+=list.pay_value    
                    t_list.paid_value = paid_amount

    def _calculate_remaining_value(self):
        res = {}
        paid_amount = 0

        for t_list in self:
            t_list.remaining_value = t_list.total_amount-t_list.paid_value
    
    name=fields.Char(size=32, default ='/', required=True)
    company_account=fields.Many2one('res.partner.bank','Company Account')
    transaction_id=fields.Many2one('transaction.payment.permission', 'Transaction Permission', ondelete='cascade')
    permission_history_id=fields.Many2one('transaction.payment.permission.history', 'Transaction Permission History')
    invoice_id=fields.Many2one('account.invoice', 'Invoice')
    partner_account=fields.Many2one('res.partner.bank','Partner Account')
    diff_day=fields.Float(compute='_calculate_diff_day', string='Diff Day')
    color=fields.Selection([('red','Immidiatelly'),('yellow','General'),('green','On Schedule')],'Color', default ='green' )
    company_id=fields.Many2one('res.company','Company',default=lambda self: self.env.user.company_id)
    date=fields.Date('Created Date', required=True, default = time.strftime('%Y-%m-%d'))
    date_end=fields.Date('Payment Date',required=True)
    res_partner=fields.Many2one('res.partner','Partner')
    descrip=fields.Char('Transaction Description')
    account_id=fields.Many2one('account.account','Account', required=True)
    journal_id=fields.Many2one('account.journal','Journal')
    total_amount=fields.Float('Total Amount')
    amount=fields.Float('Amount')
    currency=fields.Many2one('res.currency','Currency')
    pay_value=fields.Float('Pay value')
    paid_value=fields.Float(compute='_calculate_paid_value', string='Paid value')
    remaining_value=fields.Float(compute='_calculate_remaining_value', string='Remaining value')
    comment=fields.Char('Comment')
    overdue=fields.Date('Overdue')       
    cash_flow_type=fields.Many2one('account.cashflow.type', 'Cashflow Type')
    state=fields.Selection([('draft','Waiting Permission'),('allowed','Allowed'),('paid','Paid'),('closed','Closed')],'State',default ='draft',  required=True)
    statement_line_id=fields.Many2one('account.bank.statement.line', 'Statement Line Id')
            
    @api.multi
    def allow(self):
        #Төлбөр зөвшөөрөх
        if self.state =='draft':
            if self.transaction_id:
                self.transaction_id.message_post(body=_(u'%s дугаартай төлбөрийн хүсэлтийг зөвшөөрсөн' %self.invoice_id.name))
            return self.write({'state': "allowed"})
        return False
    
    @api.multi
    def cancel(self):
        #Төлбөр цуцлах
            if self.statement_line_id:
                raise UserError(_(u'Холбоотой касс харилцахын бичилтийг устгана уу!!!'))
            else:
                if self.transaction_id:
                    self.transaction_id.message_post(body=_(u'%s дугаартай төлбөрийн хүсэлтийг цуцалсан' %self.invoice_id.name))
                return self.write({'state':"draft"})
    @api.multi
    def pay(self):
        # Төлбөр төлөх
        statement_line_id = False
        import_line_id = False
        today = datetime.date.today()

        if self.journal_id and self.cash_flow_type:
            statements=self.env['account.bank.statement'].search([('date','=',today),('journal_id','=',self.journal_id.id)])
            if self.invoice_id:
                if self.invoice_id.move_id:
                    for move_line in self.invoice_id.move_id.line_ids:
                        if move_line.account_id.user_type_id.type == 'payable':
                            import_line_id = move_line.id
                            
            if statements:
                statement_line_id = self.create_transactions(statements.id, import_line_id)
            else:
                statements=self.env['account.bank.statement'].search([('state','=','confirm'),('journal_id','=',self.journal_id.id)],order='date',limit=1)
                balance=0.0
                if statements:
                    statement=self.env['account.bank.statement'].browse(statements.id)
                    balance=statement[0].balance_end_real
                statement_id=self.env['account.bank.statement'].create({
                                                'journal_id':self.journal_id.id,
                                                'date': today,
                                                'balance_start':balance
                                                })
                statement_line_id = self.create_transactions(statement_id.id, import_line_id)
                    
            self.write({'state':"paid", 'statement_line_id': statement_line_id.id})
        else :
            raise UserError(_(u'Журнал болон Гүйлгээний төрөл сонгоно уу!!!'))   
        if self.transaction_id:
            self.transaction_id.message_post(body=_(u'%s дугаартай төлбөрийн хүсэлтийг төлсөн' %self.invoice_id.name))
        
        self.create_payment()
        return True
    
    def create_payment(self):
        today = datetime.date.today()
        for payment in self:
            if payment.remaining_value > payment.paid_value:
#                 diff_amount  = payment.remaining_value - payment.paid_value
                self.env['payment.permit'].create({
                                                'name':payment.name,
                                                'transaction_id': payment.transaction_id.id,
                                                'invoice_id':payment.invoice_id.id or False,
                                                'expense_id':payment.expense_id.id or False,
                                                'date':payment.date,
                                                'res_partner':payment.res_partner.id,
                                                'descrip':payment.descrip,
                                                'account_id':payment.account_id.id,
                                                'total_amount':payment.total_amount,
                                                'currency':payment.currency.id,
                                                'state':'draft',
                                                'date_end':today,
                                                })
        return True
                
    
    def create_transactions(self, statement_id, import_line_id):
        #Төлбөр үүсгэх
        statement_line_id = False
        line=self.env['payment.permit'].browse()   
        
        statement=self.env['account.bank.statement'].browse(statement_id)
        
        if isinstance(statement,(list)):
            
            statement=statement[0]
        statement_line_id=self.env['account.bank.statement.line'].create({
                                'name':self.descrip,
                                'date':statement.date,
                                'partner_id':self.res_partner.id,
                                'cash_flow_type':self.cash_flow_type.company_id,
                                'account_id':self.account_id.id,
                                'amount':self.pay_value*(-1),
                                'statement_id':statement.id,
                                'cashflow_id': self.cash_flow_type.id,
                                'ref': self.descrip,
                                'import_line_id': import_line_id,  
                                })
        return statement_line_id
    
    @api.multi
    def write (self, vals):
        if 'state' not in vals.keys():  
            for line in self:
                state= 'draft'   
                if 'currency' in vals.keys():
                    currency=self.env['res.currency'].browse(vals['currency'])
                    rate2=currency.rate   
                else :
                     rate2=self.currency.rate
                if not rate2:
                     raise UserError(_(u'Валют сонгож өгнө үү!!!'))
                amount = self.total_amount  
                if 'pay_value' in vals.keys():
                    pay_value=float(vals['pay_value'])
                    
                    if line.remaining_value < pay_value:
                         raise UserError(_(u'Төлөх гэж буй дүн төлөх дүнгээс хэтэрсэн байнa'))        
                 
        return super(PaymentPermit, self).write(vals)

class BankCashListMnt(models.Model):
    _name='bank.cash.list.mnt'
    _description='Bank Cash List MNT'

    @api.multi
    @api.depends('journal_id')
    def _calculate_balance_bank(self):
        for obj in self:
            statements_bank = self.env['account.bank.statement'].search(
                [('journal_id.type', 'in', ('bank', 'cash')),
                 ('journal_id.currency_id', '=', obj.journal_id.currency_id.id)])
            balance_sum = 0
            for balance in statements_bank:
                if balance.journal_id == obj.journal_id:
                    balance_sum += balance.balance_end_real
            obj.balance = balance_sum

    @api.multi
    def _calculate_mnt_balance(self):
        for obj in self:
            currency_rate = 0
            rate_id = self.env['res.currency.rate'].search(
                [('currency_id', '=', obj.journal_id.currency_id.id), ('name', '<=', obj.abs_date)], order='name desc')
            if not rate_id:
                rate_id = self.env['res.currency.rate'].search(
                    [('currency_id', '=', self.env.user.company_id.currency_id.id), ('name', '<=', obj.abs_date)],
                    order='name desc')
                if not rate_id:
                    currency_rate = self.env['res.currency'].search(
                        [('name', '=', self.env.user.company_id.currency_id.name)]).rate
            rate = 0
            if rate_id:
                rate = rate_id[0].alter_rate
            if currency_rate != 0:
                rate = currency_rate
            obj.balance_mnt = obj.balance * rate

    journal_id = fields.Many2one('account.journal', 'Journal', domain=[('currency_id.name', '=', 'MNT')])
    permission_id = fields.Many2one('transaction.payment.permission', 'Permission', readonly=True)
    abs_date = fields.Date('Date', readonly=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    balance = fields.Float('MNT Balance', compute=_calculate_balance_bank, readonly=True)
    balance_mnt = fields.Float('Balance MNT', compute=_calculate_mnt_balance, readonly=True)
    code = fields.Char('Code', readonly=True)

class BankCashListUsd(models.Model):
    _name='bank.cash.list.usd'
    _description='Bank Cash List USD'

    @api.multi
    @api.depends('journal_id')
    def _calculate_balance_bank(self):
        for obj in self:
            statements_bank = self.env['account.bank.statement'].search(
                [('journal_id.type', 'in', ('bank', 'cash')),
                 ('journal_id.currency_id', '=', obj.journal_id.currency_id.id)])
            balance_sum = 0
            for balance in statements_bank:
                if balance.journal_id == obj.journal_id:
                    balance_sum += balance.balance_end_real
            obj.balance = balance_sum

    @api.multi
    def _calculate_mnt_balance(self):
        for obj in self:
            rate_id = self.env['res.currency.rate'].search(
                [('currency_id', '=', obj.journal_id.currency_id.id), ('name', '<=', obj.abs_date)], order='name desc')
            rate = 0
            if rate_id:
                rate = rate_id[0].alter_rate
            obj.balance_mnt = obj.balance * rate

    journal_id = fields.Many2one('account.journal', 'Journal', domain=[('currency_id.name', '=', 'USD')])
    permission_id = fields.Many2one('transaction.payment.permission', 'Permission', readonly=True)
    abs_date = fields.Date('Date', readonly=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    balance = fields.Float('Balance USD', compute=_calculate_balance_bank, readonly=True)
    balance_mnt = fields.Float('Balance MNT', compute=_calculate_mnt_balance, readonly=True)
    code = fields.Char('Code', readonly=True)


class BankCashListEur(models.Model):
    _name = 'bank.cash.list.eur'
    _description = 'Bank Cash List Eur'

    @api.multi
    @api.depends('journal_id')
    def _calculate_balance_bank(self):
        for obj in self:
            statements_bank = self.env['account.bank.statement'].search(
                [('journal_id.type', 'in', ('bank', 'cash')),
                 ('journal_id.currency_id', '=', obj.journal_id.currency_id.id)])
            balance_sum = 0
            for balance in statements_bank:
                if balance.journal_id == obj.journal_id:
                    balance_sum += balance.balance_end_real
            obj.balance = balance_sum

    @api.multi
    def _calculate_mnt_balance(self):
        for obj in self:
            rate_id = self.env['res.currency.rate'].search(
                [('currency_id', '=', obj.journal_id.currency_id.id), ('name', '<=', obj.abs_date)], order='name desc')
            rate = 0
            if rate_id:
                rate = rate_id[0].alter_rate
            obj.balance_mnt = obj.balance * rate

    journal_id = fields.Many2one('account.journal', 'Journal', domain=[('currency_id.name', '=', 'EUR')])
    permission_id = fields.Many2one('transaction.payment.permission', 'Permission', readonly=True)
    abs_date = fields.Date('Date', readonly=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    balance = fields.Float('Balance EUR', compute=_calculate_balance_bank, readonly=True)
    balance_mnt = fields.Float('Balance MNT', compute=_calculate_mnt_balance, readonly=True)
    code = fields.Char('Code', readonly=True)


class BankCashListCny(models.Model):
    _name = 'bank.cash.list.cny'
    _description = 'Bank Cash List CNY'

    @api.multi
    @api.depends('journal_id')
    def _calculate_balance_bank(self):
        for obj in self:
            statements_bank = self.env['account.bank.statement'].search(
                [('journal_id.type', 'in', ('bank', 'cash')),
                 ('journal_id.currency_id', '=', obj.journal_id.currency_id.id)])
            balance_sum = 0
            for balance in statements_bank:
                if balance.journal_id == obj.journal_id:
                    balance_sum += balance.balance_end_real
            obj.balance = balance_sum

    @api.multi
    def _calculate_mnt_balance(self):
        for obj in self:
            rate_id = self.env['res.currency.rate'].search(
                [('currency_id', '=', obj.journal_id.currency_id.id), ('name', '<=', obj.abs_date)], order='name desc')
            rate = 0
            if rate_id:
                rate = rate_id[0].alter_rate
            obj.balance_mnt = obj.balance * rate

    journal_id = fields.Many2one('account.journal', 'Journal', readonly=True, domain=[('currency_id.name', '=', 'CNY')])
    permission_id = fields.Many2one('transaction.payment.permission', 'Permission', readonly=True)
    abs_date = fields.Date('Date', readonly=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    balance = fields.Float('Balance CNY', compute=_calculate_balance_bank, readonly=True)
    balance_mnt = fields.Float('Balance MNT', compute=_calculate_mnt_balance, readonly=True)
    code = fields.Char('Code', readonly=True)

class BankCashListKrw(models.Model):
    _name = 'bank.cash.list.krw'
    _description = 'Bank Cash List KRW'

    @api.multi
    @api.depends('journal_id')
    def _calculate_balance_bank(self):
        for obj in self:
            statements_bank = self.env['account.bank.statement'].search(
                [('journal_id.type', 'in', ('bank', 'cash')),
                 ('journal_id.currency_id', '=', obj.journal_id.currency_id.id)])
            balance_sum = 0
            for balance in statements_bank:
                if balance.journal_id == obj.journal_id:
                    balance_sum += balance.balance_end_real
            obj.balance = balance_sum

    @api.multi
    def _calculate_mnt_balance(self):
        for obj in self:
            rate_id = self.env['res.currency.rate'].search(
                [('currency_id', '=', obj.journal_id.currency_id.id), ('name', '<=', obj.abs_date)], order='name desc')
            rate = 0
            if rate_id:
                rate = rate_id[0].alter_rate
            obj.balance_mnt = obj.balance * rate

    journal_id = fields.Many2one('account.journal', 'Journal', readonly=True, domain=[('currency_id.name', '=', 'KRW')])
    permission_id = fields.Many2one('transaction.payment.permission', 'Permission', readonly=True)
    abs_date = fields.Date('Date', readonly=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    balance = fields.Float('Balance KRW', compute=_calculate_balance_bank, readonly=True)
    balance_mnt = fields.Float('Balance MNT', compute=_calculate_mnt_balance, readonly=True)
    code = fields.Char('Code', readonly=True)

class BankCashListJpy(models.Model):
    _name = 'bank.cash.list.jpy'
    _description = 'Bank Cash List JPY'

    @api.multi
    @api.depends('journal_id')
    def _calculate_balance_bank(self):
        for obj in self:
            statements_bank = self.env['account.bank.statement'].search(
                [('journal_id.type', 'in', ('bank', 'cash')),
                 ('journal_id.currency_id', '=', obj.journal_id.currency_id.id)])
            balance_sum = 0
            for balance in statements_bank:
                if balance.journal_id == obj.journal_id:
                    balance_sum += balance.balance_end_real
            obj.balance = balance_sum

    @api.multi
    def _calculate_mnt_balance(self):
        for obj in self:
            rate_id = self.env['res.currency.rate'].search(
                [('currency_id', '=', obj.journal_id.currency_id.id), ('name', '<=', obj.abs_date)], order='name desc')
            rate = 0
            if rate_id:
                rate = rate_id[0].alter_rate
            obj.balance_mnt = obj.balance * rate

    journal_id = fields.Many2one('account.journal', 'Journal', readonly=True, domain=[('currency_id.name', '=', 'JPY')])
    permission_id = fields.Many2one('transaction.payment.permission', 'Permission', readonly=True)
    abs_date = fields.Date('Date', readonly=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    balance = fields.Float('Balance JPY', compute=_calculate_balance_bank, readonly=True)
    balance_mnt = fields.Float('Balance MNT', compute=_calculate_mnt_balance, readonly=True)
    code = fields.Char('Code', readonly=True)

class BankCashUnite(models.Model):
    _name = 'bank.cash.unite'
    _description = 'Bank Cash Unite'

    @api.multi
    @api.depends('currency_id')
    def _calculate_balance_bank(self):
        for obj in self:
            balance_bank = 0
            statements_bank = self.env['account.bank.statement'].search(
                [('journal_id.type', '=', 'bank'), ('journal_id.currency_id', '=', obj.currency_id.id)])
            statements_bank_mnt = self.env['account.bank.statement'].search(
                [('journal_id.type', '=', 'bank'), ('journal_id.currency_id', '=', False)])
            for balance in statements_bank:
                balance_bank += balance.balance_end_real
            if not statements_bank:
                for balance in statements_bank_mnt:
                    balance_bank += balance.balance_end_real
            obj.balance_bank = balance_bank

    @api.multi
    @api.depends('currency_id')
    def _calculate_balance_cass(self):
        for obj in self:
            balance_cash = 0
            statements_cash = self.env['account.bank.statement'].search(
                [('journal_id.type', '=', 'cash'), ('journal_id.currency_id', '=', obj.currency_id.id)])
            statements_cash_mnt = self.env['account.bank.statement'].search(
                [('journal_id.type', '=', 'cash'), ('journal_id.currency_id', '=', False)])
            for balance in statements_cash:
                balance_cash += balance.balance_end_real
            if not statements_cash:
                for balance in statements_cash_mnt:
                    balance_cash += balance.balance_end_real
            obj.balance_cass = balance_cash

    @api.multi
    @api.depends('balance_bank')
    def _calculate_balance(self):
        for obj in self:
            obj.balance = obj.balance_bank + obj.balance_cass

    @api.multi
    @api.depends('balance')
    def _calculate_balance_mnt(self):
        for obj in self:
            currency_rate = self.env['res.currency.rate'].search(
                [('currency_id', '=', obj.currency_id.id), ('create_date', '<=', obj.abs_date)], limit=1).alter_rate
            if not currency_rate:
                currency_rate = self.env['res.currency'].search(
                [('name', '=', obj.currency_id.name)]).rate
            obj.balance_mnt = obj.balance * currency_rate

    permission_id = fields.Many2one('transaction.payment.permission', 'Permission', readonly=True)
    currency_id = fields.Many2one('res.currency', 'Currency')
    abs_date = fields.Date('Date', readonly=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    balance_bank = fields.Float('Balance Bank', compute=_calculate_balance_bank, readonly=True)
    balance_cass = fields.Float('Balance Cass', compute=_calculate_balance_cass, readonly=True)
    balance = fields.Float('Balance Unite', compute=_calculate_balance, readonly=True)
    balance_mnt = fields.Float('Balance MNT', compute=_calculate_balance_mnt, readonly=True)


