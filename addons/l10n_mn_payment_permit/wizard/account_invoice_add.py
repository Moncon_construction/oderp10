 # -*- coding: utf-8 -*-
 ##############################################################################
 #
 #    OpenERP, Open Source Management Solution
 #    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
 #
 #    This program is free software: you can redistribute it and/or modify
 #    it under the terms of the GNU Affero General Public License as
 #    published by the Free Software Foundation, either version 3 of the
 #    License, or (at your option) any later version.
 #
 #    This program is distributed in the hope that it will be useful,
 #    but WITHOUT ANY WARRANTY; without even the implied warranty of
 #    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #    GNU Affero General Public License for more details.
 #
 #    You should have received a copy of the GNU Affero General Public License
 #    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #
 ##############################################################################
 
from odoo import models
import time
import datetime
 
class AccountInvoiceAddTransaction(models.TransientModel):
 
     _name = "account.invoice.add.transaction"
     _description = "add invoice to payment permit"
 
     def invoice_add(self, context=None):
         #Нэхэмжлэл нэмэх          
         if context is None:
             context = {}
         invoice_obj = self.env['account.invoice']
         transaction_obj = self.env['payment.permit']
         active_ids = context.get('active_ids', []) or []
         today = datetime.date.today()
         company_id = False
         permission_ids = []
         existing_ids = []
         user_ids = self.env['res.users']

         for user in self.env['res.users']:
             company_id = user.company_id.id               
         permission_obj = self.env['transaction.payment.permission'].search([('date','=',today),('company_id','=',company_id)])
         if permission_obj:
             for permission in permission_obj:
               permission_ids.append(permission.id)
         
         if permission_ids:
             #Хэрэв permission_ids байвал
             for invoice in invoice_obj.browse(active_ids): 
                 existings = transaction_obj.search([('invoice_id','=',invoice.id),('transaction_id','in',permission_ids),('state','=','draft')])                
                 
                 if existings:
                     #Хэрэв payment permit дээр бичилт үүссэн бол
                     for exist in existings:
                           existing_ids.append(exist.id)
                     if invoice.state in ('open'):
                         if invoice.type == 'in_invoice':
                             user = self.env['res.users'].browse(self.env.uid)
                             if invoice.company_id.id == user.company_id.id:
                                 obj = self.env['account.invoice'].browse([invoice.id])[0]
                                 residual = 0.0
                                 partial_reconciliations_done = []
                                 for line in obj.move_id.line_ids:
                                     if line.account_id.user_type_id.type not in ('payable'):
                                         continue
                                     if line.full_reconcile_id and line.full_reconcile_id.id in partial_reconciliations_done:
                                         continue
                                     if line.currency_id == obj.currency_id:
                                         line_amount = line.amount_residual_currency if line.currency_id else line.amount_residual
                                     else:
                                         from_currency = line.company_id.currency_id.with_context(date=line.date)
                                         line_amount = from_currency.compute(line.amount_residual, obj.currency_id)
                                     if line.full_reconcile_id:
                                         partial_reconciliation_invoices = set()
                                         for pline in line.full_reconcile_id.line_partial_ids:
                                             if pline.invoice and obj.type == pline.invoice.type:
                                                 partial_reconciliation_invoices.update([pline.invoice.id])
                                         line_amount = obj.currency_id.round(line_amount / len(partial_reconciliation_invoices))
                                         partial_reconciliations_done.append(line.full_reconcile_id.id)
                                     residual += line_amount
                                 residual = max(residual, 0.0)
                                 transaction=self.env['payment.permit'].write(existing_ids, {
                                     'invoice_id': invoice.id,
                                     'company_id':invoice.company_id.id,
                                     'descrip':invoice.number or '',
                                     'date': time.strftime('%Y-%m-%d %H:%M:%S'),
                                     'res_partner': invoice.partner_id.id,
                                     'account_id': invoice.account_id.id,
                                     'state':'draft',
                                     'total_amount':invoice.amount_total,
                                     'currency':invoice.currency_id.id,
                                     'date_end': invoice.date_due,
                                     'amount': residual,
                                     'pay_value': residual,
                                     'transaction_id': permission_ids[0]
                                 })
                 else:
                     #Хэрэв payment permit дээр бичилт үүсээгүй бол
                     if invoice.state in ('open'):
                         if invoice.type == 'in_invoice':
                             user = self.env['res.users'].browse(self.env.uid)
                             if invoice.company_id.id == user.company_id.id:
                                 obj = self.env['account.invoice'].browse([invoice.id])[0]
                                 residual = 0.0
                                 partial_reconciliations_done = []
                                 for line in obj.move_id.line_ids:
                                     if line.account_id.user_type_id.type not in ('payable'):
                                         continue
                                     if line.full_reconcile_id and line.full_reconcile_id.id in partial_reconciliations_done:
                                         continue
                                     if line.currency_id == obj.currency_id:
                                         line_amount = line.amount_residual_currency if line.currency_id else line.amount_residual
                                     else:
                                         from_currency = line.company_id.currency_id.with_context(date=line.date)
                                         line_amount = from_currency.compute(line.amount_residual, obj.currency_id)
                                     if line.full_reconcile_id:
                                         partial_reconciliation_invoices = set()
                                         for pline in line.full_reconcile_id.line_partial_ids:
                                             if pline.invoice and obj.type == pline.invoice.type:
                                                 partial_reconciliation_invoices.update([pline.invoice.id])
                                         line_amount = obj.currency_id.round(line_amount / len(partial_reconciliation_invoices))
                                         partial_reconciliations_done.append(line.full_reconcile_id.id)
                                     residual += line_amount
                                 residual = max(residual, 0.0)
                                 transaction=self.env['payment.permit'].create({
                                     'name':invoice.number,
                                     'invoice_id': invoice.id,
                                     'company_id':invoice.company_id.id,
                                     'descrip':invoice.number or '',
                                     'date': time.strftime('%Y-%m-%d %H:%M:%S'),
                                     'res_partner':invoice.partner_id.id,
                                     'account_id':invoice.account_id.id,
                                     'state':'draft',
                                     'total_amount':invoice.amount_total,
                                     'currency':invoice.currency_id.id,
                                     'date_end':invoice.date_due,
                                     'amount':residual,
                                     'pay_value':residual,
                                     'transaction_id':permission_ids[0]
                                 })
         else:
             #Хэрэв permission_ids байхгүй бол
             transaction_id = self.env['transaction.payment.permission'].create({'date':today, 'company_id': company_id})
             for invoice in invoice_obj.browse(active_ids):
                 if invoice.state in ('open'):
                     if invoice.type == 'in_invoice':
                         user = self.env['res.users'].browse(self.env.uid)
                         if invoice.company_id.id == user.company_id.id:
                             obj = self.env['account.invoice'].browse([invoice.id])[0]
                             residual = 0.0
                             partial_reconciliations_done = []
                             for line in obj.move_id.line_ids:
                                 if line.account_id.user_type_id.type not in ('payable'):
                                     continue
                                 if line.full_reconcile_id and line.full_reconcile_id.id in partial_reconciliations_done:
                                     continue
                                 if line.currency_id == obj.currency_id:
                                     line_amount = line.amount_residual_currency if line.currency_id else line.amount_residual
                                 else:
                                     from_currency = line.company_id.currency_id.with_context(date=line.date)
                                     line_amount = from_currency.compute(line.amount_residual, obj.currency_id)
                                 if line.full_reconcile_id:
                                     partial_reconciliation_invoices = set()
                                     for pline in line.full_reconcile_id.line_partial_ids:
                                         if pline.invoice and obj.type == pline.invoice.type:
                                             partial_reconciliation_invoices.update([pline.invoice.id])
                                     line_amount = obj.currency_id.round(line_amount / len(partial_reconciliation_invoices))
                                     partial_reconciliations_done.append(line.full_reconcile_id.id)
                                 residual += line_amount
                             residual = max(residual, 0.0)
                             transaction=self.env['payment.permit'].create({
                                 'name':invoice.number,
                                 'invoice_id': invoice.id,
                                 'company_id':invoice.company_id.id,
                                 'descrip':invoice.number or '',
                                 'date': time.strftime('%Y-%m-%d %H:%M:%S'),
                                 'res_partner': invoice.partner_id.id,
                                 'account_id': invoice.account_id.id,
                                 'state':'draft',
                                 'total_amount':invoice.amount_total,
                                 'currency':invoice.currency_id.id,
                                 'date_end': invoice.date_due,
                                 'amount': residual,
                                 'pay_value': residual,
                                 'transaction_id': transaction_id.id
                             })
 
     def invoice_update(self):
         #Нэхэмжлэл засах
         if context is None:
             context = {}
         invoice_obj = self.env['account.invoice']
         active_ids = context.get('active_ids', []) or []
         i = 1
         for invoice in invoice_obj.browse(active_ids, context=context): 
             invoice_obj.write([invoice.id],{'update_invoice': True})
             i+=1 
 
class ApproveTransaction(models.TransientModel):
 
     _name = "approve.transaction"
     _description = "Approve Transaction"
 
     def approve_transaction(self):
         #Төлбөр зөвшөөрөх
         if context is None:
             context = {}
         transaction_obj = self.env['payment.permit']
         active_ids = context.get('active_ids', []) or []
         for transaction in transaction_obj.browse(active_ids, context=context):
             if transaction.state =='draft':
                 return transaction_obj.write([transaction.id], {'state': 'allowed'})
         return False 
 
class PayTransaction(models.TransientModel):
 
     _name = "pay.transaction"
     _description = "Pay Transaction"
 
     def pay_transaction(self):
         #Төлбөр төлөх
         statement_line_id = False
         import_line_id = False
         today = datetime.date.today()
         if context is None:
             context = {}
         transaction_obj = self.env['payment.permit']
         active_ids = context.get('active_ids', []) or []
         for transaction in transaction_obj.browse(active_ids, context=context):
             if transaction.state =='allowed':    
                 if transaction.journal_id and transaction.cash_flow_type:   
                     statements=self.env['account.bank.statement'].search([('date','=',today),('journal_id','=',transaction.journal_id.id)])
                     if transaction.invoice_id:
                         if transaction.invoice_id.move_id:
                             if transaction.invoice_id.move_id.line_ids:
                                 for move_line in transaction.invoice_id.move_id.line_ids:
                                     if move_line.account_id.user_type_id.type == 'payable':
                                         import_line_id = move_line.id
                     if statements:
                             statement_line_id = self.create_transactions(ids, transaction.id, statements, import_line_id, context=None)
                     else:
                             statements=self.env['account.bank.statement'].search([('state','=','confirm'),('journal_id','=',transaction.journal_id.id)],order='date')
                             balance=0.0
                             if statements:
                                 statement=self.env['account.bank.statement'].browse(statements)
                                 balance=statement[0].balance_end_real                             
                             statement_id=self.env['account.bank.statement'].create({
                                                             'journal_id':transaction.journal_id.id,
                                                             'date': today,
                                                             'balance_start':balance
                                                             })
                             statement_line_id = self.create_transactions(ids, transaction.id, statement_id, import_line_id, context=None)
                         
         return transaction_obj.write([transaction.id], {'state': 'paid', 'statement_line_id': statement_line_id})
     
     def create_transactions(self):
         #Төлбөр үүсгэх
         line=self.env['payment.permit'].browse(line_id)
         
         statement=self.env['account.bank.statement'].browse(statement_id)
         
         if isinstance(statement,(list)):
             statement=statement[0]
         transaction_id=self.env['account.bank.statement.line'].create({
                                 'name':line.descrip,
                                 'partner_id':line.res_partner.id,
                                 'cash_flow_type':line.cash_flow_type.id,
                                 'account_id':line.account_id.id,
                                 'amount':line.pay_value*(-1),
                                 'statement_id':statement.id,
                                 'cashflow_account_id': line.cash_flow_type.id,
                                 'ref': line.descrip,
                                 'import_line_id': import_line_id   
                                 })
         return transaction_id