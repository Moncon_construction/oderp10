# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Payroll EMD print",
    'version': '1.0',
    'depends': ['l10n_mn_hr_payroll'],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Modules',
    'description': """
  ЭМД дээр бичилт хийх модуль
    """,
    'data': [
        'views/payroll_emd_print_view.xml',
        'views/report_emdprint_template.xml',
        'views/payroll_emd_print_reports.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
