# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from openerp import api, fields, models, _
from datetime import datetime, date, timedelta
import time

class PayrollEmdPrint(models.TransientModel):
    _name = "payroll.emd.print"
    _description = "Payroll EMD print"

    employee_id = fields.Many2one('hr.employee', 'Employees', required=True)
    start_period_id = fields.Many2one('account.period', 'Start Period' , required=True)
    end_period_id = fields.Many2one('account.period', 'End Period', required=True)
    above = fields.Integer('Period way', required=True)
    page = fields.Integer('Page', required=True)
    spacing = fields.Float('Spacing', required=True, default=5.3)
    view_page = fields.Selection([('1', 'Left'),('2', 'Right')], required=True)

    @api.multi
    def print_report(self):
        self.ensure_one()
        [data] = self.read()
        data['emp'] = self.env.context.get('active_ids', [])
        employees = self.env['hr.employee'].browse(data['emp'])
        datas = {
            'ids': [],
            'model': 'hr.employee',
            'form': data
        }
        return self.env['report'].get_action(employees, 'l10n_mn_payroll_emd_print.report_emdprint_template', data=datas)
    
class report_paperformat(models.Model):
    _inherit = "report.paperformat"
    
    print_emd = fields.Boolean("Print emd")