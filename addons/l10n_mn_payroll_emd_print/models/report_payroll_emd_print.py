# -*- coding: utf-8 -*-
from decimal import *
from openerp import models, api


class ReportPayrollEmdPrint(models.AbstractModel):
    _name = 'report.l10n_mn_payroll_emd_print.report_emdprint_template'
    
    @api.model
    def render_html(self, docids, data=None):
        context = self._context.copy() or {}
        payroll_emd_obj = self.env['payroll.emd.print']
        report_obj = self.env['report']
        emd_report = report_obj._get_report_from_name('l10n_mn_payroll_emd_print.report_emdprint_template')
        active_ids = context['active_ids']
        ids = payroll_emd_obj.browse(active_ids)
        slipids = []
        period_ids = self.env['account.period'].search([('date_start', '>=', ids.start_period_id.date_start),
                                                        ('date_stop', '<=' , ids.end_period_id.date_stop),
                                                        ('company_id', '=', ids.employee_id.company_id and ids.employee_id.company_id.id)])
        if ids.employee_id:
            for emp in ids.employee_id:
                for date in period_ids:
                    hhtemd = btemd = 0
                    slip_ids = self.env['hr.payslip'].search([('employee_id','=', emp.id), ('period_id', '=', date.id), ('company_id', '=', emp.company_id and emp.company_id.id), ('salary_type', '=', 'last_salary')])
                    for slip in slip_ids.details_by_salary_rule_category:
                        if slip.code == "HHTEMD":
                            hhtemd = slip.amount
                        if slip.code == "BTEMD":
                            btemd = slip.amount
                    if not slip_ids.details_by_salary_rule_category:
                        slipids.append({'HHTEMD' : 0, 'BTEMD' : 0})
                    if hhtemd > 0 or btemd > 0:
                        slipids.append({'HHTEMD' : hhtemd, 'BTEMD' : btemd})
        obj = self.env['report.paperformat'].search([('print_emd','=',True)])
        if ids.view_page == '1':
            obj.write({'margin_top': ids.above})
            obj.write({'margin_left': ids.page})
            page_view = ids.view_page
        elif ids.view_page == '2':
            obj.write({'margin_top': ids.above})
            obj.write({'margin_left': ids.page})
            page_view = ids.view_page
        docargs = {
            'doc_ids': docids,
            'doc_model': emd_report.model,
            'get_slipids': slipids,
            'page_view': page_view,
            'spacing': str(ids.spacing) + 'mm',
            'docs': ids
        }
        return report_obj.render('l10n_mn_payroll_emd_print.report_emdprint_template' , docargs)