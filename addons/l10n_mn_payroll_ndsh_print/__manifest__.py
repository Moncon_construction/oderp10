# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Payroll NDSH print",
    'version': '1.0',
    'depends': ['l10n_mn_hr_payroll'],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Modules',
    'description': """
  НДШ дэвтэр дээр бичилт хийх модуль
    """,
    'data': [
        'data/ndsh_paperformat.xml',
        'security/ir.model.access.csv',
        'views/report_paperformat_ndsh.xml',
        'views/payroll_ndsh_print_view.xml',
        'views/report_ndshprint_template.xml',
        'views/payroll_ndsh_print_reports.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
