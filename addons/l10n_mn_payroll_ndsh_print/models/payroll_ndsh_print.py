# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, _


class ReportPaperformatNDSH(models.Model):
    _name = "report.paperformat.ndsh"

    name = fields.Char(string='Name', required=True)
    top_margin = fields.Float(string="Top margin", required=True)
    left_margin = fields.Float(string="Left margin", default=0)
    left_margin_right_page = fields.Float(string="Left margin right page", default=0)
    year = fields.Integer(string='Year')
    spacing = fields.Float(string='Spacing', required=True)
    first_col = fields.Float(string='First column width', required=True)
    second_col = fields.Float(string='Second column width', required=True)
    third_col = fields.Float(string='Third column width', required=True)


class PayrollNdshPrint(models.TransientModel):
    _name = "payroll.ndsh.print"
    _description = "Payroll Ndsh print"

    employee_id = fields.Many2one('hr.employee', 'Employees', required=True)
    start_period_id = fields.Many2one('account.period', 'Start Period', required=True)
    end_period_id = fields.Many2one('account.period', 'End Period', required=True)
    paperformat_id = fields.Many2one('report.paperformat.ndsh', string='Paper format', required=True)
    view_page = fields.Selection([('1', 'Left'), ('2', 'Right')], required=True)

    @api.multi
    def print_report(self):
        self.ensure_one()
        [data] = self.read()
        data['emp'] = self.env.context.get('active_ids', [])
        employees = self.env['hr.employee'].browse(data['emp'])
        datas = {
            'ids': [],
            'model': 'hr.employee',
            'form': data
        }
        return self.env['report'].get_action(employees, 'l10n_mn_payroll_ndsh_print.report_ndshprint_template', data=datas)


class ReportPaperformat(models.Model):
    _inherit = "report.paperformat"

    print_ndsh = fields.Boolean("Print ndsh")