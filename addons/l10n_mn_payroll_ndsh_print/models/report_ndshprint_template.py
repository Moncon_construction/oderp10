# -*- coding: utf-8 -*-
from odoo import models, api


class ReportPayrollNdshPrint(models.AbstractModel):
    _name = 'report.l10n_mn_payroll_ndsh_print.report_ndshprint_template'

    @api.model
    def render_html(self, docids, data=None):
        context = self._context.copy() or {}
        payroll_ndsh_obj = self.env['payroll.ndsh.print']
        report_obj = self.env['report']
        ndsh_report = report_obj._get_report_from_name('l10n_mn_payroll_ndsh_print.report_ndshprint_template')
        active_ids = context['active_ids']
        ids = payroll_ndsh_obj.browse(active_ids)
        slipids = []
        slip_one = []
        period_ids = self.env['account.period'].search([('date_start', '>=', ids.start_period_id.date_start),
                                                        ('date_stop', '<=', ids.end_period_id.date_stop),
                                                        ('company_id', '=', ids.employee_id.company_id.id)])
        if ids.employee_id:
            for emp in ids.employee_id:
                for date in period_ids:
                    total = ndsh = totalndsh = 0
                    slip_ids = self.env['hr.payslip'].search([('employee_id', '=', emp.id), ('period_id', '=', date.id),
                                                              ('company_id', '=', emp.company_id and emp.company_id.id),
                                                              ('salary_type', '=', 'last_salary')])
                    for slip in slip_ids.details_by_salary_rule_category:

                        if slip.code == "TOTALGROSS":
                            total = slip.amount
                        if slip.code == "NDSH":
                            ndsh = slip.amount
                        if slip.code == "TOTALNDSH":
                            totalndsh = slip.amount
                    if total > 0:
                        slip_one.append({'TOTALGROSS': total, 'NDSH': ndsh, 'TOTALNDSH': totalndsh})
                    else:
                        slip_one.append({'TOTALGROSS': 0, 'NDSH': 0, 'TOTALNDSH': 0})
        if len(period_ids) != 12:
            a = 12 - len(period_ids)

            for i in range(a):
                slipids.append({'TOTALGROSS': 0, 'NDSH': 0, 'TOTALNDSH': 0})

        slipids.extend(slip_one)


        obj = self.env['report.paperformat'].search([('print_ndsh', "=", True)])

        if ids.view_page == '1':
            obj.write({'margin_top': ids.paperformat_id.top_margin})
            obj.write({'margin_left': ids.paperformat_id.left_margin})
        elif ids.view_page == '2':
            obj.write({'margin_top': ids.paperformat_id.top_margin})
            obj.write({'margin_left': ids.paperformat_id.left_margin_right_page})
        page_view = ids.view_page
        docargs = {
            'doc_ids': docids,
            'doc_model': ndsh_report.model,
            'get_slipids': slipids,
            'page_view': page_view,
            'docs': ids,
            'spacing': str(ids.paperformat_id.spacing) + 'mm',
            'first_col': str(ids.paperformat_id.first_col) + 'mm',
            'second_col': str(ids.paperformat_id.second_col) + 'mm',
            'third_col': str(ids.paperformat_id.third_col) + 'mm',
        }
        return report_obj.render('l10n_mn_payroll_ndsh_print.report_ndshprint_template', docargs)
