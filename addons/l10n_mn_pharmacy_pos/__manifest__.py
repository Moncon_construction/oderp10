# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Pharmacy Pos ",
    'version': '2.1',
    'depends': [
        'l10n_mn_point_of_sale',
        'product_expiry'
        ],
    'author': "Asterisk Technologies LLC",
    'sequence': 15,
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Mongolian Point of Sale for pharmacies
    """,
    'data': [
        'views/web_asset.xml',
        'views/res_config_view.xml',
        'security/ir.model.access.csv',
        'wizard/pos_insurance_send_view.xml',
        'views/point_of_sale.xml',
        'views/pos_config_view.xml',
        'views/pos_insurance_sale_view.xml',
        'views/pos_order_view.xml',
        'views/insurance_discount_list_view.xml',
        'views/send_insurance_cron.xml',
        'views/product_view.xml',
    ],
    'qweb': ['static/src/xml/pos.xml',
             'static/src/xml/qweb.xml',
             ],
    'application': True,
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
