# -*- coding: utf-8 -*-
import json
import logging
import requests
import datetime

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError  # @UnresolvedImport
_logger = logging.getLogger(__name__)

class InsuranceDiscountList(models.Model):
    _name = "insurance.discount.list"

    tbltId = fields.Integer('EMD ID')
    tbltNameMon = fields.Char('Mon Name')
    tbltNameInter = fields.Char('Inter Name')
    tbltNameSales = fields.Char('Sales Name')
    tbltSizeMixture = fields.Char('Size Mixture')
    tbltDiscountPerc = fields.Float('Discount')
    tbltPackingCnt = fields.Char('Quantity')
    tbltMaxPrice = fields.Float('Max Price')
    tbltDiscountAmt = fields.Float('Discount Amount')

    tbltBarCode = fields.Char('Barcode')
    tbltIsDiscount = fields.Integer('Is Discount')
    tbltMinPrice = fields.Float('Min Price')
    tbltCountryName = fields.Char('Country Name')
    packGroup = fields.Integer('General Number')
    tbltMaxQuantity = fields.Float('Max Quantity')
    tbltType = fields.Integer('Is Adult')
    tbltDocType = fields.Char('Doctor"s Category')
    tbltIcdCode = fields.Char('Icd code')
    tbltUnitDisAmt = fields.Float('Unit Discount Amount')
    tbltUnitMaxAmt = fields.Float('Unit Max Amount')
    tbltRegCode = fields.Char('Regist Code')
    tbltForm = fields.Char('Form')
    tbltUnitPrice = fields.Float('New Max Price')

    @api.multi
    @api.depends('tbltNameSales')
    def name_get(self):
        result = []
        for insurance_list in self:
            name = insurance_list.tbltNameSales
            result.append((insurance_list.id, name))
        return result
    
    @api.multi
    def update_list(self):
        access_token = None
        list_obj = self.env['insurance.discount.list']
        pos_conf_obj = self.env['pos.config']
        config_ids = pos_conf_obj.search([('user_name','!=', False)])[0]
        token_head = {'Authorization': 'Basic VV9mZl05Qmp5WlhMbUcmZHcmOlo3JHtFenlyRDRheUN9RkxkJg=='}
        token_data = {
                'grant_type': 'password',
                'username': config_ids.user_name,
                'password': config_ids.password,
                }
        token_url = 'https://ws.emd.gov.mn/oauth/token?'
        token_response = requests.post(token_url, params=token_data, headers=token_head, verify=False)
        if token_response.status_code == 200:
            new_data = json.loads(token_response.text)
            access_token = new_data['access_token']
            _logger.info(u'ЭМД-н системээс авсан токен %s' % (access_token))
        else: 
            error_message = u'ЭМД-ийн системтэй холбогдоход алдаа гарлаа'
            _logger.error(u'Аccess token авахад алдаа гарлаа.')
            raise ValidationError(u'ЭМД системтэй холбогдож чадсангүй!')
         
        url = 'https://ws.emd.gov.mn/tablet/findAll?page=1&size=1000&access_token='+ access_token
        head = {"Content-Type": "application/json"}
        params = {'field':None,
                  'value':None,
                  'type':None,
                  "order":None,
                  "dir":None }
        datas = json.dumps(params)
        response = requests.post(url, headers=head, data=datas, verify=False)
        response_ids = []
        if response.status_code == 200:
            insurancce_data = json.loads(response.text)
            for tbltData in insurancce_data['data']:
                vals = {
                    'tbltId': tbltData['id'],
                    'tbltNameMon': tbltData['tbltNameMon'],
                    'tbltNameInter': tbltData['tbltNameInter'],
                    'tbltNameSales': tbltData['tbltNameSales'],
                    'tbltSizeMixture': tbltData['tbltSizeMixture'],
                    'tbltDiscountPerc': tbltData['tbltDiscountPerc'],
                    'tbltPackingCnt': tbltData['tbltPackingCnt'],
                    'tbltMaxPrice': tbltData['tbltMaxPrice'],
                    'tbltDiscountAmt': tbltData['tbltDiscountAmt'],

                    'tbltBarCode': tbltData['tbltBarCode'],
                    'tbltIsDiscount': tbltData['tbltIsDiscount'],
                    'packGroup': tbltData['packGroup'],
                    'tbltType': tbltData['tbltType'],
                    'tbltUnitDisAmt': tbltData['tbltUnitDisAmt'],
                    'tbltRegCode': tbltData['tbltRegCode'],
                    'tbltUnitPrice': tbltData['tbltUnitPrice']
                }
                list = list_obj.search([('tbltId', '=', tbltData['id'])])
                if len(list) > 0:
                    list[0].write(vals)
                else:
                    list_obj.create(vals)
                response_ids.append(tbltData['id'])

            delete_list = list_obj.search([('tbltId', 'not in', tuple(response_ids))])
            for delete_data in delete_list:
                delete_data.unlink()
        else:
            error_message = u'ЭМД-ийн системтэй холбогдоход алдаа гарлаа'
            _logger.error(u'Аccess token авахад алдаа гарлаа.')
            raise ValidationError(u'ЭМД системтэй холбогдож чадсангүй!')
        return True