from odoo import fields, models


class PosConfig(models.Model):
    _inherit = 'pos.config'

    user_name = fields.Char('User name')
    password = fields.Char('Password')
    emd_url = fields.Char('EMD url')
    user_id = fields.Many2one('res.users', 'User')
    emd_price_list_id = fields.Many2one('product.pricelist', 'EMD Price List')
    emd_partner_id = fields.Many2one('res.partner', 'EMD Partner')
    no_manual_lot = fields.Boolean(string='No manual lot', help='Manual lot input screen will not appear if checked')
    emd_account_id = fields.Many2one('account.account', 'EMD Account')