# -*- coding: utf-8 -*-
import json
import logging
import requests
from datetime import datetime

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError  # @UnresolvedImport
from pychart.arrow import default

_logger = logging.getLogger(__name__)


class PosInsuranceSale(models.Model):
    _name = "pos.insurance.sale"
    _order = "date desc, id desc"

    name = fields.Char('Reciept Number', required=True)
    date = fields.Datetime('Date')
    totalAmt = fields.Float('Total Amount')
    insAmt = fields.Float('Insurance Amount')
    vatAmt = fields.Float('Vat Amount')
    netAmt = fields.Float('Net Amount')
    lastName = fields.Char('Last Name')
    firstName = fields.Char('First Name')
    register = fields.Char('Register Number', required=True)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('sented', 'Sented')], default="draft")
    insurance_line = fields.One2many('pos.insurance.sale.line', 'parent_id', 'Line')
    origin = fields.Char(string='Origin')
    partner_id = fields.Many2one('res.partner', string='Partner')
    user_id = fields.Many2one('res.users', string='User', default=lambda self: self.env.user)
    ddtd = fields.Char('Unique Number')
    config_id = fields.Many2one('pos.config', string='POS Configure', required=True)
    receipt_id = fields.Integer('Receipt ID')
    
    def action_draft(self):
        recs = self.filtered(lambda x : x.state == 'sented')
        if recs:
            recs.write({'state':'draft'})

    # Уг функц нь Жорын дугаар, регистрийн дугаар, ПОС-н тохиргоо 
    # гэсэн заавал бөглөх 3 талбар авч "Жорын дугаараар татах" 
    # товч дарах үед ЭМД-н системээс ирэх утгуудыг дэлгэцэнд оруулна.
    def get_data(self):
        pos_order_obj = self.env['pos.order']
        data_dict = False
        for obj in self:
            data_dict = pos_order_obj.check_number(obj.register, obj.name, obj.config_id.id)
            if data_dict:    
                data = json.loads(data_dict['json_data'])
                if data:
                    obj.receipt_id = data['id']
                    obj.lastName = data['patientLastName']
                    obj.firstName = data['patientFirstName']
                    obj.date = datetime.now()
                    receipt_detail = data['receiptDetails']
                    obj.insurance_line.unlink()  # дахин дарагдах үед ажиллана.
                    line_obj = self.env['pos.insurance.sale.line']
                    vals = []
                    for line in receipt_detail:
                        vals.append({
                            'intern_name':line['tbltName'],
                            'parent_id':obj.id,
                            'product_id':False,
                            'detail_id':line['id']
                        })
                    if vals:
                        for line in vals:
                            line_obj.create(line)
                else:
                    raise UserError(u'Алдаатай бичилт оруулсан эсвэл жорын хугацаа дууссан байна!')
            else:
                raise UserError(u'Алдаатай бичилт оруулсан эсвэл жорын хугацаа дууссан байна!')

    @api.multi
    def send_emd_cron(self):
        draft_insurances = self.search([('state', '=', 'draft')])
        if draft_insurances:
            for pos_insurance_id in draft_insurances:
                _logger.info(u' %s дугаартай жорыг автомат крон функц илгээж байна. ' % pos_insurance_id.name)
                check_number = pos_insurance_id.name
                recipt_data = {}
                json_data = []
                access_token = ''
                error_message = ''
                head = {'Authorization': 'Basic VV9mZl05Qmp5WlhMbUcmZHcmOlo3JHtFenlyRDRheUN9RkxkJg=='}
                data = {
                    'grant_type': 'password',
                    'username': pos_insurance_id.config_id.user_name,
                    'password': pos_insurance_id.config_id.password,
                }
                emd_url = 'https://ws.emd.gov.mn/oauth/token?'
                response = requests.post(emd_url, params=data, headers=head, verify=False)
                if response.status_code == 200:
                    new_data = json.loads(response.text)
                    access_token = new_data['access_token']
                    regNumber = pos_insurance_id.register
                    _logger.info(u' %s дугаартай жороор ЭМД-н системээс авсан токен %s' % (pos_insurance_id.name, access_token))
                    recipt_data = self.env['pos.insurance.send'].check_reciept(access_token, regNumber, check_number)
                    if not recipt_data:
                        _logger.warn(u' %s дугаартай жороор ЭМД-н системээс авсан токеноор илгээх боломжгүй!!!' % pos_insurance_id.name)
                    json_data = json.dumps(recipt_data)
                    self.env['pos.insurance.send'].send_emd(pos_insurance_id.id, access_token, pos_insurance_id.config_id.id)
                else: 
                    error_message = u'ЭМД-ийн системтэй холбогдоход алдаа гарлаа'
                    _logger.error(u'Аccess token авахад алдаа гарлаа.')
                    raise ValidationError(u'%s дугаартай жор ЭМД системтэй холбогдож чадсангүй! ' % pos_insurance_id.name)


class PosInsuranceSaleLine(models.Model):
    _name = "pos.insurance.sale.line"

    product_id = fields.Many2one('product.product', 'Product Name')
    quantity = fields.Float('Quantity')
    price = fields.Float('Price')
    totalAmt = fields.Float('Total Amount')
    insAmt = fields.Float('Insurance Amount')
    parent_id = fields.Many2one('pos.insurance.sale', 'Parent')
    detail_id = fields.Integer('Detail ID')
    intern_name = fields.Char('International Name')
    date = fields.Datetime('Date', related='parent_id.date', readonly=True)
    config_id = fields.Many2one(string='POS Configure', related='parent_id.config_id', readonly=True)
    product_category_id = fields.Many2one(string='POS Configure', related='product_id.categ_id', readonly=True)

