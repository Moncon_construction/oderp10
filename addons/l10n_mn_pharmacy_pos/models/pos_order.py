# -*- coding: utf-8 -*-
import json
import logging
import requests
import time
from odoo import api, fields, models, _
from odoo.http import request
from odoo.exceptions import ValidationError  # @UnresolvedImport
from itertools import product

_logger = logging.getLogger(__name__)


class PosOrderInherit(models.Model):
    _inherit = "pos.order"

    emd_check_number = fields.Char('Check Number')

    # Override the function to assign both tracked and untracked products
    def _force_picking_done(self, picking):
        """Force picking in order to be set as done."""
        self.ensure_one()
        contains_tracked_products = any([(product_id.tracking != 'none') for product_id in self.lines.mapped('product_id')])
        # do not reserve for tracked products, the user will have manually specified the serial/lot numbers
        # New code starts here
        if contains_tracked_products and not self.session_id.config_id.no_manual_lot:
            picking.action_confirm()
        else:
            picking.action_assign()
        # New code ends here
        picking.force_assign()
        self.set_pack_operation_lot(picking)
        # New code starts here
        # If the picking has products that are tracked, this fills the
        # lot quantities to do in order to finish the picking.
        can_done = True
        if contains_tracked_products and self.session_id.config_id.no_manual_lot:
            for move in picking.move_lines:
                if move.ordered_qty > move.product_id.qty_available:
                    can_done = False
                    break
            if can_done:
                for operation in picking.pack_operation_ids:
                    if operation.product_id.tracking != 'none':
                        for lot in operation.pack_lot_ids:
                            lot.operation_id.write(
                                {'qty_done': sum(operation_lot.qty for operation_lot in lot.operation_id.pack_lot_ids)})
                picking.action_done()
        # New code ends here
        if not contains_tracked_products:
            for move in picking.move_lines:
                if move.ordered_qty > move.product_id.qty_available:
                    can_done = False
                    break
            if can_done:
                picking.action_done()
    @api.model
    def get_url(self, uuuuu):
        url2 = uuuuu
        resp = requests.get(url=url2)
        data = None
        try:
            data = json.loads(resp.text)
        except Exception as e:
            _logger.error('Connection failed.', exc_info=True)
            raise Warning(_('Error'), _('Could not connect to json device. \n%s') % e.message)
        data_json = json.dumps(data)
        return data_json

    @api.model
    def get_emd_discount(self, json_data):
        data = json.loads(json_data)
        price = data['price']
        qty = data['quantity']
        pid = data['productId']
        pl_id = data['pricelist_id']
        product_pl_obj = self.env['product.pricelist']
        product_obj = self.env['product.product']
        product_pli_obj = self.env['product.pricelist.item']
        pricelist = product_pl_obj.browse(pl_id)
        product = product_obj.search([('id', '=', pid)])
        pli = product_pli_obj.search([('pricelist_id', '=', pl_id), ('product_tmpl_id', '=', product.product_tmpl_id.id)], order='id DESC', limit=1)
        max_price = pli.price_max_margin
        em_price = pricelist.price_get(pid, qty)
        
        if max_price != 0:  # max_price < price and max_price != 0:
            dis_amount = pli.price_surcharge
            discount = ((max_price - (max_price + dis_amount)) * 100 / max_price) if max_price else 0
        else:
            if em_price:
                if em_price[pl_id] == 0:
                    discount = 0
                else:
                    discount = ((price - em_price[pl_id]) * 100 / price) if price else 0
            else:
                discount = 0
                max_price = price
                
        res = {
            'emd_discount': discount,
            'max_price': max_price,
            'product_name': product.name,
            'qty': qty
        }
        return res

    @api.model
    def check_connection(self):
        '''
        Интернет холболт байгаа эсэхийг шалгахад ашиглах функц
        '''
        return True

    @api.model
    def check_number(self, regNumber, check_number, pos_id):
        '''
        ЭМД -ын цахим системээс токен авч жорын дугаараар шалгах функц
        '''
        recipt_data = {}
        json_data = []
        access_token = ''
        error_message = ''
        conf_obj = self.env['pos.config']
        config = conf_obj.browse(pos_id)
        head = {'Authorization': 'Basic VV9mZl05Qmp5WlhMbUcmZHcmOlo3JHtFenlyRDRheUN9RkxkJg=='}
        data = {
            'grant_type': 'password',
            'username': config.user_name,
            'password': config.password,
        }
        emd_url = config.emd_url + '/oauth/token?'
        response = requests.post(emd_url, params=data, headers=head, verify=False)
        

        if response.status_code == 200:
            new_data = json.loads(response.text)
            access_token = new_data['access_token']
            recipt_data = self.check_reciept(access_token, regNumber, check_number)
            json_data = json.dumps(recipt_data)
        else:
            error_message = u'ЭМД-ийн системтэй холбогдоход алдаа гарлаа'
            _logger.error(u'Аccess token авахад алдаа гарлаа. %s', data)

        return {
            'error_message': error_message,
            'access_token': access_token,
            'json_data': json_data,
        }

    @api.model
    def check_reciept(self, token, regNumber, check_number):
        '''
        ЭМД -ын цахим системээс авсан токеноор жорын мэдээлэл авах функц
        '''
        myUrl = 'https://ws.emd.gov.mn/receipt/checkNumberBy'
        data = {'regNo': regNumber, 'receiptNumber': check_number, 'access_token': token}
        response = requests.get(myUrl, params=data, verify=False)
        new_data = []
        if response.status_code == 200:
            if response.text:
                new_data = json.loads(response.text)
        else:
            _logger.error(u'Жорын мэдээлэл авахад алдаа гарлаа. %s', data)

        if new_data:
            return new_data

    @api.model
    def send_emd(self, json_arg, pos_id, access_token):
        '''
        ЭМД -ын цахим системээс токен авч жорын дугаараар шалгах функц
        '''
        new_data = []
        insurance_id = False
        config_id = pos_id
        conf_obj = self.env['pos.config']
        insurance_obj = self.env['pos.insurance.sale']
        insuranceLine_obj = self.env['pos.insurance.sale.line']
        invoice_obj = self.env['account.invoice']
        invoice_line_obj = self.env['account.invoice.line']
        product_obj = self.env['product.product']
        config = conf_obj.browse(config_id)
        head = {"Content-Type": "application/json"}
        myUrl = 'https://ws.emd.gov.mn/ebarimt/send?access_token=' + access_token
        datas = json.dumps(json_arg)
        
        # try:
        response1 = requests.post(myUrl, headers=head, data=datas, verify=False)

        if response1.text:
            new_data = json.loads(response1.text)
            if new_data['code'] != '200':
                _logger.error(u'ЭМД системтэй холбогдож чадсангүй!. %s', new_data)
                raise ValidationError(u'ЭМД системтэй холбогдож чадсангүй!')

        unix_time = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(json_arg['salesDate'] / 1000.))
        unix_date = time.strftime('%Y-%m-%d', time.gmtime(json_arg['salesDate'] / 1000.))
        vals = {
            'name': json_arg['receiptNumber'],
            'date': unix_time,
            'totalAmt': json_arg['totalAmt'],
            'insAmt': json_arg['insAmt'],
            'lastName': json_arg['lastName'],
            'firstName': json_arg['firstName'],
            'register': json_arg['register'],
            'partner_id': config.emd_partner_id.id,
            'origin': json_arg['origin'],
            'state': 'sented',
            'netAmt': json_arg['netAmt'],
            'vatAmt': json_arg['vatAmt'],
            'ddtd': json_arg['posRno'],
            'receipt_id': json_arg['receiptId'],
            'config_id': config.id,
        }
        insurance_id = insurance_obj.create(vals)
        self._cr.execute("select id from account_invoice where partner_id = %s \
                    and date_invoice = '%s' and journal_id = %s order by id"
                         % (config.emd_partner_id.id, unix_date, config.invoice_journal_id.id))
        fetch_inv_id = self._cr.fetchone()
        if fetch_inv_id == [] or not fetch_inv_id:
            inv_id = invoice_obj.create(
                {
                    'journal_id': config.invoice_journal_id.id,
                    'date_invoice': unix_date,
                    'company_id': config.company_id.id,
                    'account_id': config.emd_partner_id.property_account_receivable_id.id,
                    'type': 'out_invoice',
                    'state': 'draft',
                    'partner_id': config.emd_partner_id.id,
                })
        else:
            inv_id = invoice_obj.search([('id', '=', fetch_inv_id[0])])
        if insurance_id:
            for details in json_arg['ebarimtDetails']:
                lvals = {
                    'parent_id': insurance_id.id,
                    'detail_id': details['detailId'],
                    'product_id': details['productCode'],
                    'quantity': details['quantity'],
                    'insAmt': details['insAmt'],
                    'price': details['price'],
                    'totalAmt': details['totalAmt'],
                }
                insurance_line = insuranceLine_obj.create(lvals)
                if inv_id:
                    product = product_obj.search([('id', '=', details['productCode'])])
                    price_unit = float(details['insAmt']) / float(details['quantity'])
                    account_id = config.emd_account_id.id or False
                    invoice_line_obj.create(
                        {
                            'product_id': product.id,
                            'name': details['productName'],
                            'invoice_id': inv_id.id,
                            'quantity': float(details['quantity']),
                            'account_id': account_id,
                            'price_unit': float(details['insAmt']) / float(details['quantity']),
                        })

        # except Exception as e:
        #     raise ValidationError(u'Сүлжээнд алдаа гарлаа')
        return new_data

    #Модуль суусан эсэхийг шалгах функц
    def is_module_installed(self, module_name):
        self._cr.execute("SELECT id FROM ir_module_module WHERE name = '%s' AND state IN ('installed', 'to upgrade')" %module_name)
        results = self._cr.dictfetchall()
        if results and len(results) > 0:
            return True
        else:
            return False

class PosOrderLine(models.Model):
    _inherit = "pos.order.line"
    
    qty = fields.Float('Quantity', digits=(3, 8), default=1)
    detail_id = fields.Integer('EMD detail id')
    emd_discount = fields.Float(string='EMD Discount (%)', digits=0, default=0.0)
    emd_discount_amount = fields.Float(string='EMD Discount Amount', compute="compute_emd_discount_amount")
    
    @api.depends('emd_discount', 'price_unit_after_discount')
    def compute_emd_discount_amount(self):
        for obj in self:
            obj.emd_discount_amount = obj.price_unit_after_discount * obj.emd_discount / 100.0
    
class PosOrder(models.Model):
    _inherit = "pos.order"
    
    emd_discount_amount = fields.Float(string='EMD Discount Amount', compute="compute_emd_discount_amount")
    
    @api.depends('lines.emd_discount_amount')
    def compute_emd_discount_amount(self):
        for obj in self:
            obj.emd_discount_amount = sum(line.emd_discount_amount * line.qty or 0 for line in obj.lines)
            
    def test_paid(self):
        #@Override: ЭМД-г шалгалтад тооцов.
        """A Point of Sale is paid when the sum
        @return: True
        """
        prec_acc = self.env['decimal.precision'].precision_get('Account')
        
        for order in self:
            amount_total = order.amount_total - order.emd_discount_amount
            
            if order.lines and not amount_total:
                continue
            if (not order.lines) or (not order.statement_ids) or (abs(amount_total - order.amount_paid) > 10 ** (-prec_acc or -5)):
                return False
        return True
    