from odoo import api, fields, models, tools, _

class Company(models.Model):
    _inherit = 'res.company'

    emd_list_start_id = fields.Integer('EMD list download start ID')
