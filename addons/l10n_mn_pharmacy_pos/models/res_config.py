# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _

class PosConfiguration(models.TransientModel):
    _inherit = 'pos.config.settings'
    
    emd_list_start_id = fields.Integer(related='company_id.emd_list_start_id', string="EMD list download start ID")

