odoo.define('l10n_mn_pharmacy_pos.DB', function (require) {
"use strict";
    
    var PosDB = require('point_of_sale.DB');
    PosDB.include({
        init: function(options){
            this._super(options);
            this.insure_by_id = {};
        },
        // ЭМД -ын хөнгөлөлттэй жагсаалтын тохиргоо хийгдсэн барааг авах
        
        get_product_by_emd_list: function(category_id){
       		var product_ids  = this.product_by_category_id[category_id];
	        var list = [];
	        if (product_ids) {
	            for (var i = 0, len = product_ids.length; i < len; i++) {
	            	var product = this.product_by_id[product_ids[i]]
	            	if (product.insurance_list_id.length > 0){
		                list.push(product);
	            	}
	            }
	        }
	        return list;
	    },

	    add_insure: function(insure){
        	for(var i=0, len = insure.length; i < len; i++){
                this.insure_by_id[insure[i].id] = insure[i];
            }
        },

	    get_insure: function(insure_id){
       		var insure  = this.insure_by_id[insure_id];
	        return insure;
	    }
    });
    return PosDB;
});