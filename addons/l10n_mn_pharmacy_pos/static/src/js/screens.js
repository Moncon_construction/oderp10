odoo.define('l10n_mn_pharmacy_pos.screens', function (require) {
	"use strict";
	
	var core = require('web.core');
	var Model = require('web.Model');
	var utils = require('web.utils');

	var gui = require('point_of_sale.gui');
	var guiModule = gui.Gui;
	
	var posScreens = require('l10n_mn_point_of_sale.screens');
	var productScreen = posScreens.productScreen;
	var paymentScreen = posScreens.paymentScreen;

	var actionpadWidget = require('point_of_sale.screens').ActionpadWidget;
	var numpadWidget = require('point_of_sale.screens').NumpadWidget;
	var orderWidget = require('point_of_sale.screens').OrderWidget;

	var round_pr = utils.round_precision;
	var QWeb = core.qweb;
	var _t = core._t;

	guiModule.include({
		_close: function () { // Борлуулалтын цэгээ хаах
			var self = this;
			if (self.pos.config.vatps_address) { // local service-ийн sendData-г дуудах
				var url = self.pos.config.vatps_address.replace("put", "sendData");
				$.get(url, function (data, status) {
					console.log(data, status);
				});
			}
			this._super();
		}
	});

	actionpadWidget.include({ // Хөнгөлөлт, Захиалагч, Төлбөр товчууд
		renderElement: function () {
			var self = this;
			this._super();
			this.$('.pay').unbind("click");
			this.$('.pay').click(function () {
				var order = self.pos.get_order();
				var has_valid_product_lot = _.every(order.orderlines.models, function (line) {
					return line.has_valid_product_lot();
				});
				/** EMD VALIDATAION */
				if (order.ordersWithEmdDiscount) {
					if (order.ordersWithEmdDiscount.length == 0) {
						order.pharmDiscount = undefined;
						order.accessToken = undefined;
					}
					var foundCnt = 0;
					for (var i = 0; i < order.ordersWithEmdDiscount.length; i++) {
						var cid = order.ordersWithEmdDiscount[i].cid;
						var found = false;
						for (var j = 0; j < order.orderlines.models.length; j++) {
							if (order.orderlines.models[j].cid == cid) {
								found = true;
								break;
							}
						}
						if (found) {
							foundCnt++;
						}
					}

					console.log("pay check");
					if (foundCnt == 0) {
						alert("Жор дээр буй эмүүд олдоогүй тул даатгалын хөнгөлөлт олгоогүйг анхаарна уу!");
						order.pharmDiscount = undefined;
						order.accessToken = undefined;
						console.log("DISCOUNT REJECTED ordersWithEmdDiscount", order.ordersWithEmdDiscount, "order.pharmDiscount", order.pharmDiscount);
					} else {
						console.log("DISCOUNT GRANTED ordersWithEmdDiscount", order.ordersWithEmdDiscount, "order.pharmDiscount", order.pharmDiscount);
					}
				}

				if (!has_valid_product_lot && !self.pos.config.no_manual_lot) {
					self.gui.show_popup('confirm', {
						'title': _t('Empty Serial/Lot Number'),
						'body': _t('One or more product(s) required serial/lot number.'),
						confirm: function () {
							self.gui.show_screen('payment');
						},
					});
				} else {
					self.gui.show_screen('payment');
				}
			});
			this.$('.discount-pharm').click(function () {
				//Check internet connection
				var custom_model = new Model('pos.order');
				custom_model.call('check_connection').then(
					function () {
						self.pos.gui.show_screen('pharmDiscount');
					},
					function () {
						alert("Сервэртэй холбогдох боломжгүй байна. Интернетийн холболтоо шалгана уу");
					}
				);
			});
		}
	});

	numpadWidget.include({ // Тоонууд
		start: function () {
			this.inputRestriction = this.inputRestriction.bind(this);
			this._super();
		},
		clickChangeMode: function (event) {
			if (event.currentTarget.attributes['data-mode'].nodeValue == 'loose') {
				var self = this;
				$(".loose-input-container").show();
				$(".loose-input").keydown(this.inputRestriction);
				$(".loose-input").val("");
				$(".loose-input").focus();
			} else {
				$(".loose-input-container").off('keydown', '.loose-input', this.inputRestriction);
				$(".loose-input-container").hide();
			}
			this._super(event);
		},
		inputRestriction: function (e) {
			if (e.keyCode == 13) {
				if ($(".mode-button.selected-mode[data-mode=loose]").length > 0) {
					var loose_goods_qty = this.pos.get_order().get_selected_orderline().product.loose_goods_qty;
					if (loose_goods_qty == 0) {
						alert("Барааны задгай тоо оруулаагүй байна.");
					} else {
						var quantity = parseInt($(".loose-input").val()) / loose_goods_qty;
						this.pos.get_order().get_selected_orderline().set_quantity(quantity);
					}
					$(".loose-input").val("");
					$(".product-screen .mode-button[data-mode='quantity']").click();
					
				}
			}
			// Allow: backspace, delete, tab, escape, enter and .
			if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
				// Allow: Ctrl+A, Command+A
				(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
				// Allow: home, end, left, right, down, up
				(e.keyCode >= 35 && e.keyCode <= 40)) {
				// let it happen, don't do anything
				return;
			}
			// Ensure that it is a number and stop the keypress
			if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
			}
		}
	});

	productScreen.include({
		keyDownHandler: function (e) {
			var self = this;
			if (self.checkKeypadAvailability()) {
				switch (e.keyCode) {
					case 115: //F4 - EMD хөнгөлөлт
						e.preventDefault();
						$(".product-screen .button.discount-pharm").click();
						break;
					case 118: //F7 - Үнэ
						e.preventDefault();
						$(".product-screen .mode-button[data-mode='loose']").click();
						break;
					case 13: //enter - төлбөр
						e.preventDefault();
						if ($(".product-screen .loose-input").is(":focus")) {
							return false;
						} 
				}
			}
			return this._super(e);
		},
		close: function () {
			window.document.body.removeEventListener('keydown', this.keyDownHandler);
		}
	});

	paymentScreen.include({
        validate_order: function (force_validation) {
            var pharmacy = true;
			var self = this;
			var order = this.pos.get_order();
			var plines = order.get_paymentlines();
            var total_payment = order.get_total_with_tax();
            for (var i = 0; i < plines.length; i++) {
                total_payment -= plines[i].get_amount()
                if (plines[i].get_type() === 'bank') {
                    if (total_payment.toFixed(2) < 0) {
                        this.gui.show_popup('error', {
                            'title': _t('Incorrect Bank Payment'),
                            'body': _t('Payment amount exceeds the necessary amount on Banks.'),
                        });
                        return false;
                    }
                }
            }
			var custom_model = new Model('pos.order');
			order.totalVat = 0;
			if ($(".payment-screen .button.next.highlight").length > 0) {
				var stocks = [];
				var emdStocks = [];
				var totalVat = 0;
				var totalCityTax = 0;
				var taxInfo = {};
				var totalInsAmt = 0;
				/* Бүх tax-уудын цуглуулгыг үүсгэнэ */
				order.get_tax_details().forEach(function (item) {
					var taxId = item.tax.id;
					var taxAmount = item.tax.amount;
					taxInfo[taxId] = taxAmount;
					
					if (item.tax.children_tax_ids.length > 0){
						taxInfo[taxId] = 1;
					}
				});
				/* stock-ийг угсрах */
				order.orderlines.models.forEach(function (item) {
					var totalAmount = item.get_unit_price_after_discount() * item.quantity;
					var productCode = item.product.default_code;
					var categoryCodeId, regex, m;
					
					if (!productCode) { // барааны код байхгүй бол БАРКОД-оо авна.
						productCode = item.product.barcode;
					}
					if (!productCode) { // БАРКОД байхгүй бол Ангилалын код авна
						if (item.product.vat_category_code){
							productCode = item.product.vat_category_code;
						} else {
							productCode = "";
						}
					}
					
					var productVat = totalAmount / 1.1 * 0.10;
					var cityTax = 0;
					
					for (var i = 0; i < item.product.taxes_id.length; i++) {
						if (taxInfo[item.product.taxes_id[i]] == 1) {
							cityTax = totalAmount / 1.11 * 0.01
							productVat = totalAmount / 1.11 * 0.10;
							break;
						}
					}
					
					totalCityTax += cityTax;
					totalVat += productVat;
					
					var barcode = item.product.barcode;
					if (!barcode) { // БАРКОД байхгүй бол Ангилалын код авна
						if (item.product.vat_category_code){
							barcode = item.product.vat_category_code;
						} else {
							barcode = "";
						}
					}
					stocks.push({
						"code": productCode.toString(),
						"name": item.product.display_name.replace("&", " ЭНД "),
						"measureUnit": item.product.uom_id[1],
						"qty": item.quantity.toFixed(2),
						"unitPrice": item.get_unit_price_after_discount().toFixed(2),
						"totalAmount": totalAmount.toFixed(2),
						"cityTax": cityTax.toFixed(2),
						"vat": productVat.toFixed(2),
						"barcode": barcode.toString()
					});
					if(item.detail_id){
						var insAmt = item.get_emd_discount() * item.get_unit_price_after_discount() / 100.0 * item.quantity;
						totalInsAmt += insAmt;
						var qty = 1;
						if(item.product.loose_goods_qty > 0){
							qty = item.quantity.toFixed(2)* item.product.loose_goods_qty;
						}

						var tbltId = 0;
                        if(item.product.insurance_list_id.length > 1){
                            var insure_id = item.product.insurance_list_id[0];
                            var insure = self.pos.db.get_insure(insure_id);
                            productCode = insure.tbltBarCode;
                            tbltId = insure.tbltId;
                        }
						emdStocks.push({
						    "tbltId": tbltId.toString(),
							"detailId": item.detail_id,
							"barCode": productCode.toString(),
							"productCode": item.product.id,
							"productName": item.product.display_name.replace("&", " ЭНД "),
							"quantity": qty,
							"insAmt": insAmt.toFixed(2),
							"price": item.get_unit_price_after_discount().toFixed(2),
							"totalAmt": totalAmount.toFixed(2) - insAmt.toFixed(2),
							"status": 1
						});
						console.log('emdStocks==='+emdStocks['detailId']);
					}
				});
				var billType = order.ebarimt.companyReg ? "3" : "1";
				var customerNo = order.ebarimt.companyReg ? order.ebarimt.companyReg.toString() : "";

				var billIdSuffix = order.pharmDiscount && order.pharmDiscount.receiptNumber ?
					order.pharmDiscount.receiptNumber.toString() : "";
				/* ebarimt API-ийн PUT лүү явуулах датаг угсрах */
				order.totalVat = totalVat;
				var amount = order.get_gross_amount_after_discount() - order.get_total_emd_discount_amount();
				var data = {
					"amount": amount.toFixed(2),
					"vat": totalVat.toFixed(2),
					"cashAmount": amount.toFixed(2),
					"nonCashAmount": "0.00",
					"cityTax": totalCityTax.toFixed(2),
					"reportMonth": "",
					"districtCode": "26",
					"posNo": "0001",
					"customerNo": customerNo,
					"billType": billType,
					"billIdSuffix": billIdSuffix, 
					"returnBillId": "",
					"stocks": stocks
				};
				/* POSAPI-ийн PUT лүү ajax явуулах */
				console.log("SERVICE PUT DATA", data);
				var ebarimtConnectionFailed = false,
					alertMsg = "Ebarimt service-тэй холбогдох үед алдаа гарлаа. Компьютераа унтраагаад асаана уу!";

                if(self.pos.company.check_vatpsp_service == true){
                    try {
                        var xmlHttp = new XMLHttpRequest();
                        xmlHttp.open("GET", self.pos.config.vatps_address.replace("put", "getInformation"), false );
                        xmlHttp.send();
                        console.log(xmlHttp.responseText);
                    } catch(e) {
                        alert("Ebarimt сервистэй холбогдоход алдаа гарлаа", e);
                        return false
                    }
                }

				$.ajax({
					type: "POST",
					url: self.pos.config.vatps_address, //http://10.0.9.239:8028/put/
					data: "param=" + JSON.stringify(data),
					async: false,
					success: function (response) {
						console.log("SERVICE PUT response", response);
						try {
							var jsonResp = JSON.parse(response);
							if (response.success == 'false' || response.success === false) {
								alert(response.message);
							} else {
								order.ebarimt.billId = jsonResp.billId;
								order.ebarimt.lottery = jsonResp.lottery;
								order.ebarimt.qrData = jsonResp.qrData;
								order.ebarimt.amount = jsonResp.amount;
								
								order.ebarimt.total_amount = order.get_gross_amount();
								order.ebarimt.discount_amount = order.get_total_discount();
                                order.ebarimt.total_amount_with_tax = order.get_gross_amount_after_discount();
                                order.ebarimt.total_amount_without_tax = order.get_gross_amount_after_discount() - order.get_total_vat() - order.get_total_cityTax();
                                order.ebarimt.vat_amount = order.get_total_vat();
                                order.ebarimt.city_tax_amount = order.get_total_cityTax();
                                order.ebarimt.register_amount = jsonResp.amount;
							}
						} catch (e) {
							alert("JSON хөрвүүлэхэд алдаа гарлаа", e);
						}
					},
					error: function (request, status, error) {
						console.error(request.responseText, error);
					}
				});
				console.log("ORDER PHARM DISCOUNT");
				alertMsg = "";
				if (order.pharmDiscount && order.pharmDiscount.receiptNumber) {
					if (order.ebarimt.billId && order.ebarimt.billId.length == 33) {
						var pos_id = self.pos.config.id;
						var jsonData = {
							"receiptId": order.pharmDiscount.receipt_id,
							"posRno": order.ebarimt.billId,
							"salesDate": new Date().getTime(),
							"totalAmt": order.get_total_with_tax().toFixed(2),
							"status": 1,
							"insAmt": totalInsAmt.toFixed(2),
							"vatAmt": totalVat.toFixed(2),
							"netAmt": (order.get_total_with_tax() - totalVat).toFixed(2),
							"receiptNumber": order.pharmDiscount.receiptNumber,
							"lastName": order.pharmDiscount.lastName,
							"firstName": order.pharmDiscount.firstName,
							"register": order.pharmDiscount.register,
							"origin": order.name,
							"ebarimtDetails": emdStocks
						};
						console.log("SERVICE SEND DATA", jsonData);
						custom_model.call('send_emd', [jsonData, pos_id, order.accessToken]).then(
							function (response) {
								order.pharmDiscount = undefined;
								order.accessToken = undefined;
								console.log('send_emd response', response);
							},
							function (error) {
								alert("[Send emd] Сервертэй холбогдоход алдаа гарлаа. " + error.message);
							}
						);
					} else {
						console.log("billId: ", order.ebarimt.billId);
						var checkURL = self.pos.config.vatps_address.replace("/put/", "/checkAPI/");
						$.ajax({
							type: "GET",
							url: checkURL,
							async: false,
							success: function (response) {
								try {
									var jsonResp = JSON.parse(response);
									if (jsonResp.success) {
										alertMsg = ("Ebarimt-аас ДДТД авах үед алдаа гарлаа. POS-оо хаагаад нээнэ үү!");
									} else {
										alertMsg = ("Ebarimt service-тэй холбогдох үед алдаа гарлаа. Компьютераа унтраагаад асаана уу!");
									}
								} catch (e) {
									alertMsg = ("JSON хөрвүүлэхэд алдаа гарлаа", e);
								}
							},
							error: function (request, status, error) {
								console.error(request.responseText, status, error);
								alertMsg = "Ebarimt service-тэй холбогдох үед алдаа гарлаа. Компьютераа унтраагаад асаана уу!";
							}
						});
						return;
					}
				}
				self.ordersWithEmdDiscount = [];
			}
			return this._super(force_validation, pharmacy);
		},
	});

    orderWidget.include({
        set_value: function(val) {
            var order = this.pos.get_order();
            if (order.get_selected_orderline()) {
                var mode = this.numpad_state.get('mode');
                if( mode === 'quantity'){
                    order.get_selected_orderline().set_quantity(val);
                    if (typeof order.get_selected_orderline() != "undefined"){
                        if (order.get_selected_orderline().has_product_lot){
                            var pack_lot_lines = order.get_selected_orderline().pack_lot_lines
                            var lot_lines = pack_lot_lines.sortBy('lot_name').slice(0, pack_lot_lines.length);
                            pack_lot_lines.remove(lot_lines);
                        }
                    }
                }else if( mode === 'discount'){
                    order.get_selected_orderline().set_discount(val);
                }else if( mode === 'price'){
                    order.get_selected_orderline().set_unit_price(val);
                }
            }
        },
        update_summary: function(){
            var order = this.pos.get_order();
            if (!order.get_orderlines().length) {
                return;
            }
            
            var total     = order ? order.get_total_with_tax() - order.get_total_emd_discount_amount() : 0;
            var taxes     = order ? order.get_total_with_tax() - order.get_total_without_tax() : 0;

            this.el.querySelector('.summary .total > .value').textContent = this.format_currency(total);
            this.el.querySelector('.summary .total .subentry .value').textContent = this.format_currency(taxes);
        },
    });

	var PharmDiscountScreenWidget = require('point_of_sale.screens').ScreenWidget.extend({
		template: 'PharmDiscountScreenWidget',
		auto_back: true,
		next_screen: 'products',
		previous_screen: 'products',
		show: function () {
			var self = this;
			var detail_list = [];
			var receipt_id = null;
			this._super();
			var errorTimeout;
			var order = self.pos.get_order();
			self.clearFields();
			$(".discount-screen .top-content .next").addClass("oe_hidden");
			/* Button events*/
			this.backClickHandler = function () {
				$(".receipt-number-input").val("");
				$('.register-number-input').val("");
				self.gui.back();
			};
			this.nextClickHandler = function () {
				//Хөнгөлөлттэй пос ордер үүсгэх
				$('#medicine-table tr').each(function() {
				 	var detailId = $(this).find("td:eq(0)").html();    
				 	var qtyProd = $(this).find("td:eq(2)").text();
				 	var prod = $(this).find("td:eq(5) input[type='text']").val();
				 	var newOrderLine;
				 	if (prod){
				 		var prodId = prod.split('-');
					 	var product = self.pos.db.get_product_by_id(prodId[0]);
					 	
						var dict = []; // create an empty array
						
				 		// ЭМД-н хөнгөлөлт үзүүлсэн бол тухайн барааны default тоог буцаахдаа
					 	// барааны задгай тоо хэмжээг тооцож буцаадаг болгов.
				 		if (product.loose_goods_qty){
				 			qtyProd = qtyProd / product.loose_goods_qty;
				 		}
						dict['detailId']= detailId;
						dict['quantity']= qtyProd;
					 	newOrderLine = self.pos.get_order().add_product(product, dict);
					 	if (product.loose_goods_qty == 0){
					 		alert(product.display_name +"Барааны задгай тоо оруулаагүй байна.");
					 	}
				 	}
				 });
			
				
				var orders = order.get_orderlines(); //бүртгэсэн бараануудын жагсаалт
				order.pharmDiscount = Object.assign({
					lastName: $(".medicine-details-box .lastName").val(),
					firstName: $(".medicine-details-box .firstName").val(),
					register: $(".medicine-details-box .register").val(),
					emdNumber: $(".medicine-details-box .emdNumber").val(),
					discount_no: $(".medicine-details-box .discount_no").val(),
					receipt_id: receipt_id,
					
				}, self.pharmDiscount);

				order.ordersWithEmdDiscount = [];
				var ordersLength = orders.length;
				var ordersCount = 0;
				orders.forEach(function (orderElement) {
					var orderData = {
						"pricelist_id": self.pos.config.emd_price_list_id[0],
						"price": orderElement.price,
						"quantity": orderElement.quantity,
						"productId": orderElement.product.id
					}; // get_discount луу явуулах дата (JSON)
					var custom_model = new Model('pos.order');
					custom_model.call('get_emd_discount', [JSON.stringify(orderData)]).then(
						function (response) {
							if (orderElement.detail_id){
								var emd_discount = JSON.parse(response.emd_discount);
								var max_price = JSON.parse(response.max_price);
								orderElement.set_emd_discount(emd_discount);
								orderElement.set_unit_price(max_price);
								if (emd_discount > 0) {
									order.ordersWithEmdDiscount.push(orderElement);
								}
							}
							ordersCount++;
							if (ordersLength == ordersCount) {
								console.log("ORDER WITH EMD DISCOUNT", order.ordersWithEmdDiscount);
								console.log("order.pharmDiscount", order.pharmDiscount);
								self.gui.back();
							}
						},
						function (error) {
							alert("[Get discount] Сервертэй холбогдоход алдаа гарлаа. Ахин оролдно уу!" + error.message);
						}
					);
				});
			};

			this.searchMedicineEnterHandler = function (e) {
				if (e.keyCode == 13) {
					if ($(".register-number-input").is(":focus")) {
						if ($(".register-number-input").val().trim() == "") {
							$(".discount-screen .top-content .next:not(.oe_hidden)").click();
						} else {
							$('.search-medicine').click();
						}
					} else {
						if ($(".discount-screen .top-content .next:not(.oe_hidden)").length > 0) {
							$(".discount-screen .top-content .next:not(.oe_hidden)").click();
						} else {
							$('.search-medicine').click();
						}
					}
				}
			};
			

			this.searchHandler = function () {
				$(".discount-screen .top-content .next").addClass("oe_hidden");
				var receiptNumber = $(".receipt-number-input").val();
				var regNumber = $(".register-number-input").val();
				var custom_model = new Model('pos.order');
				var pos_id = self.pos.config.id;
				self.clearFields();
				
				custom_model.call('check_number', [regNumber, receiptNumber, pos_id]).then(
					function (response) { // Success
						console.log('check number whole response', response);
						if (response.error_message.length == 0) {
							var jsonRes = JSON.parse(response.json_data);
							order.accessToken = response.access_token;
							console.log('accessToken avlaa', order.accessToken);
							console.log('Check_number json response', jsonRes);
							if (jsonRes === null) {
								$(".error-field").text("Эмийн бүртгэл олдсонгүй").height("1.2em");
								clearTimeout(errorTimeout);
								errorTimeout = setTimeout(function () {
									$(".error-field").height("0em");
								}, 4000);
							} else {
								var receiptType = "",
									status = "";
								switch (parseInt(jsonRes.receiptType)) {
									case 1:
										receiptType = "Хөнгөлөлттэй жор";
										break;
									case 2:
										receiptType = "Канттай";
										break;
									case 3:
										receiptType = "13А";
										break;
									case 4:
										receiptType = "Үзлэг";
										break;
									case 5:
										receiptType = "Эмнэлэгийн хуудас";
										break;
								}
								switch (parseInt(jsonRes.status)) {
									case 0:
										status = "Идэвхигүй";
										break;
									case 1:
										status = "Идэвхитэй";
										break;
									case 2:
										status = "Энгийн жор";
										break;
									case 3:
										status = "Худалдаж авсан";
										break;
									case 4:
										status = "Хянасан";
										break;
									case 5:
										status = "Хугацаа дууссан";
										break;
								}
								var receiptExpireDate = new Date(jsonRes.receiptExpireDate);
								var expireDate = receiptExpireDate.getFullYear() + "-" + (receiptExpireDate.getMonth() + 1) + "-" + receiptExpireDate.getDate();
								var receiptPrintedDate = new Date(jsonRes.receiptPrintedDate);
								var printedDate = receiptPrintedDate.getFullYear() + "-" + (receiptPrintedDate.getMonth() + 1) + "-" + receiptPrintedDate.getDate();
								var receiptDiag = !!jsonRes.receiptDiag ? jsonRes.receiptDiag : "";
								if (jsonRes.patientLastName) {
									$('.patientInfo .lastName').val(jsonRes.patientLastName);
								}
								if (jsonRes.patientFirstName) {
									$('.patientInfo .firstName').val(jsonRes.patientFirstName);
								}
								if (jsonRes.patientRegNo) {
									$('.patientInfo .register').val(jsonRes.patientRegNo);
								}
								$(".medicine-details-box .receiptNumber").text(jsonRes.receiptNumber);
								$(".medicine-details-box .receiptType").text(receiptType);
								$(".medicine-details-box .receiptDiag").text(receiptDiag);
								$(".medicine-details-box .receiptExpireDate").text(expireDate);
								$(".medicine-details-box .receiptPrintedDate").text(printedDate);
								$(".medicine-details-box .status").text(status);
								$(".medicine-details-box .hosOfficeName").text(jsonRes.hosOfficeName);
								$(".medicine-details-box .hosSubOffName").text(jsonRes.hosSubOffName);
								$(".medicine-details-box .hosName").text(jsonRes.hosName);
								$(".medicine-details-box .cipherCode").text(jsonRes.cipherCode);
								$(".medicine-details-box .tbltCount").text(jsonRes.tbltCount);
								receipt_id = jsonRes.id;
								
								var receiptDetails = jsonRes.receiptDetails;
								console.log(receiptDetails);
								
								for (var i = 0; i < receiptDetails.length; i++) {
									var receiptDetail = receiptDetails[i];	
									detail_list.push(receiptDetail)
									console.log(receiptDetail)
									var tbltTypeName = !!receiptDetail.tbltTypeName ? receiptDetail.tbltTypeName : "";
									$(".medicine-list .medicine-list-contents").append(
										"<tr class='medicine-line'>" +
										"<td id='detailId'>" + receiptDetail.id + "</td>" +
										"<td>" + receiptDetail.tbltName + "</td>" +
										"<td id='tbltSize'>" + receiptDetail.tbltSize + "</td>" +
										"<td>" + tbltTypeName + "</td>" +
										"<td>" + (parseInt(receiptDetail.status) == 1 ? "Борлуулсан" : "Борлуулаагүй") + "</td>" +
										"<td id='prodId'>" +
												" <input list='screens.screenid-datalist' id='input-id' type='text' \
													class='description form-control' onblur='self.on_create_line'>" +
												" <datalist id='screens.screenid-datalist'> " +
											  "</datalist>" + 
											  "</input> " +
											"</td>" +
										"</tr>" +
                                        "<tr class='medicine-line'>" +
										    "<td> Өдөрт уух тоо: <br>" + receiptDetail.dailyTimes + "</td>" +
										    "<td> Нэг удаагийн уух хэмжээ: <br>" + receiptDetail.oneTimeDose + "</td>" +
										    "<td> Уух хоногийн тоо: <br>" + receiptDetail.totalDays  + "</td>" +
										    "<td colspan='3'> Тайлбар: <br>" + receiptDetail.tbltDesc  + "</td>" +
										"</tr>" );
									
								}
								
								// Даатгалын хөнгөлөлттэй эмээр input datalist цэнэглэх
								var product_list = self.pos.db.get_product_by_emd_list(0);
								var options = '';
								for(var i = 0, len = product_list.length; i < len; i++){
									 if (product_list[i]['insurance_list_id']){
									 	
							    	 	options += '<option value="'+product_list[i]['id']+ '-'+product_list[i]['display_name']+ '" />';
							    	 	}
									}
  								document.getElementById('screens.screenid-datalist').innerHTML = options;
								self.pharmDiscount = jsonRes;
								if (parseInt(jsonRes.status) == 1 || parseInt(jsonRes.status) == 3){
									$(".discount-screen .top-content .next").removeClass("oe_hidden");
								}
							}
						} else { // ЭМД-ийн системтэй холбогдоогүй
							alert(response.error_message);
						}
					},
					function (error) { // Failed
						alert("[check number] Сервертэй холбогдоход алдаа гарлаа. " + error.message);
					}
					
					
				);
			};
			
			$('.back').on("click", self.backClickHandler);
			$('.next').on("click", self.nextClickHandler);
			$('.register-number-input').on('keyup', self.searchMedicineEnterHandler);
			$('.search-medicine').on("click", self.searchHandler);
			$('.search-product').on("click", self.searchHandlerProduct);
			$('.receipt-number-input').focus();
			$(document.getElementById('screens.screenid-datalist')).on('keyup', self.createlineEnterHandler)
		},
								
		clearFields: function () {
			/** Clear inputs */
			$(".patientInfo .lastName").val("");
			$(".patientInfo .firstName").val("");
			$(".patientInfo .register").val("");
			$(".patientInfo .emdNumber").val("");
			$(".patientInfo .discount_no").val("");
			/** Clear spans */
			$(".medicine-details-box .receiptNumber").text("");
			$(".medicine-details-box .receiptType").text("");
			$(".medicine-details-box .receiptDiag").text("");
			$(".medicine-details-box .receiptExpireDate").text("");
			$(".medicine-details-box .receiptPrintedDate").text("");
			$(".medicine-details-box .status").text("");
			$(".medicine-details-box .hosOfficeName").text("");
			$(".medicine-details-box .hosSubOffName").text("");
			$(".medicine-details-box .hosName").text("");
			$(".medicine-details-box .cipherCode").text("");
			$(".medicine-details-box .tbltCount").text("");
			$(".medicine-list .medicine-list-contents").empty();
			//Clear input
			$('.receipt-number-input').val("");
			$('.register-number-input').val("");
		},
		close: function () {
			var self = this;
			this._super();
			$(".discount-screen .top-content .next").addClass("oe_hidden");
			/** unbind handlers from events */
			$('.back').off("click", self.backClickHandler);
			$('.next').off("click", self.nextClickHandler);
			$('.register-number-input').off('keyup', self.searchMedicineEnterHandler);
			$('.search-medicine').off("click", self.searchHandler);
		}
	});
	
	gui.define_screen({
		name: 'pharmDiscount',
		widget: PharmDiscountScreenWidget
	});
});