# -*- coding: utf-8 -*-
import json
import urllib
import logging
import requests
import time 
import datetime
import dateutil.parser

from compiler.syntax import check
from time import mktime
from datetime import date, datetime

from odoo.exceptions import UserError, ValidationError  # @UnresolvedImport
from odoo import api, models, tools

_logger = logging.getLogger(__name__)


class PosInsuranceSend(models.TransientModel):
    _name = 'pos.insurance.send'
    _description = 'Pos Insurance Send'

    @api.model
    def check_reciept(self, token, regNumber, check_number):
        '''
            ЭМД -ын цахим системээс авсан токеноор жорын мэдээлэл авах функц
        '''
        myUrl = 'https://ws.emd.gov.mn/receipt/checkNumberBy'
        data = {'regNo': regNumber, 'receiptNumber': check_number, 'access_token': token}
        response = requests.get(myUrl, params=data, verify=False)
        new_data = []
        if response.status_code != 200:
            _logger.error(u'Жорын мэдээлэл авахад алдаа гарлаа')
            if self:
                raise ValidationError(u'ЭМД системтэй холбогдож чадсангүй!')
    
    @api.model
    def check_number(self):
        _logger.info(u'ЭМД -ын цахим системээс токен авч жорын дугаараар шалгаж байна')
        '''
            ЭМД -ын цахим системээс токен авч жорын дугаараар шалгах функц
        '''
        pos_insurance_id = self.env['pos.insurance.sale'].browse(self._context['active_ids'])[0]
        check_number = pos_insurance_id.name
        recipt_data = {}
        json_data = []
        access_token = ''
        error_message = ''
        head = {'Authorization': 'Basic VV9mZl05Qmp5WlhMbUcmZHcmOlo3JHtFenlyRDRheUN9RkxkJg=='}
        data = {
            'grant_type': 'password',
            'username': pos_insurance_id.config_id.user_name,
            'password': pos_insurance_id.config_id.password,
        }
        emd_url = 'https://ws.emd.gov.mn/oauth/token?'
        response = requests.post(emd_url, params=data, headers=head, verify=False)
        if response.status_code == 200:
            new_data = json.loads(response.text)
            access_token = new_data['access_token']
            _logger.info(u' %s дугаартай жороор ЭМД-н системээс авсан токен %s' % (pos_insurance_id.name, access_token))
            
            regNumber = pos_insurance_id.register
            recipt_data = self.check_reciept(access_token, regNumber, check_number)
            
            if not recipt_data:
               _logger.warn(u' %s дугаартай жороор ЭМД-н системээс авсан токеноор илгээх боломжгүй!!!' % pos_insurance_id.name)
            json_data = json.dumps(recipt_data)
            self.send_emd(pos_insurance_id.id, access_token, pos_insurance_id.config_id.id)
        else: 
            error_message = u'ЭМД-ийн системтэй холбогдоход алдаа гарлаа'
            _logger.error(u'%s дугаартай жороор ЭМД-н системээс токен авахад алдаа гарлаа.' % pos_insurance_id.name)
            if self:
                raise ValidationError(u'ЭМД системтэй холбогдож чадсангүй!')
            
    @api.model
    def send_emd(self, insurance_id, access_token, config_id):
        '''
        ЭМД -ын цахим системээс токен авч жорын дугаараар шалгах функц
        '''
        
        insurance_obj = self.env['pos.insurance.sale']
        insurance = insurance_obj.browse(insurance_id)
        insu_line = insurance.insurance_line
        if insu_line and access_token and config_id:
            ebarimtDetails = []
            conf_obj = self.env['pos.config']
            insuranceLine_obj = self.env['pos.insurance.sale.line']
            invoice_obj = self.env['account.invoice']
            invoice_line_obj = self.env['account.invoice.line']
            product_obj = self.env['product.product']
            config = conf_obj.browse(config_id)
            
            for line in insu_line:
                dt = datetime.strptime(insurance.date, '%Y-%m-%d %H:%M:%S')
                dt = dt.date()
                unix_secs = int(mktime(dt.timetuple())) * 1000
                ebarimtDetails.append({
                        'detailId': line.detail_id,
                        'barCode': line.product_id.default_code,
                        'productName': line.product_id.name,
                        'quantity': line.quantity,
                        'insAmt': line.insAmt,
                        'totalAmt': line.totalAmt,
                        'price': line.price,
                        })
            vals = [{
                    'access_token': str(access_token),
                    'receiptId': insurance.receipt_id,
                    'posRno': insurance.ddtd,
                    'receiptNumber': insurance.name,
                    'salesDate': unix_secs,
                    'totalAmt': insurance.totalAmt,
                    'insAmt': insurance.insAmt,
                    'vatAmt': insurance.vatAmt,
                    'netAmt': insurance.netAmt,
                    'status': 1,
                    'ebarimtDetails': ebarimtDetails}]
            
            myUrl = 'https://ws.emd.gov.mn/ebarimt/batch?access_token=' + access_token
            head = {"Content-Type": "application/json"}
            datas = json.dumps(vals)
            response1 = requests.post(myUrl, data=datas , headers=head, verify=False)
               
            if response1.status_code == 200:
                _logger.warn(u' %s дугаартай жор амжилттай илгээгдлээ!' % insurance.name) 
            if response1.status_code == 400:
                _logger.error(u'Тохиргооны мэдээлэл татагдаагүй, дутуу татагдсан эсвэл тохиргооны мэдээллийг хүчээр сольсон үед илрэх алдаа') 
            if response1.status_code == 500:
                _logger.error(u'Мэдээлэл дамжуулалт хэт удаан байна. Мэдээлэл дамжуулах дээд хязгаар {0} секунд байна!!!') 
            if response1.status_code != 200:
                _logger.error(u'ЭМД системтэй холбогдож чадсангүй!')
                if self:
                    raise ValidationError(u'ЭМД системтэй холбогдож чадсангүй!')
            insurance.write({'state':'sented'})
            self._cr.execute("select id from account_invoice where partner_id = %s \
                        and date_invoice = '%s' and journal_id = %s order by id"
                        % (config.emd_partner_id.id, insurance.date, config.invoice_journal_id.id))
             
            fetch_inv_id = self._cr.fetchone()
 
            if fetch_inv_id == [] or not fetch_inv_id:
                inv_id = invoice_obj.create(
                    {
                        'journal_id': config.invoice_journal_id.id,
                        'date_invoice': insurance.date,
                        'company_id': config.company_id.id,
                        'account_id': config.emd_partner_id.property_account_receivable_id.id,
                        'type': 'out_invoice',
                        'state': 'draft',
                        'partner_id': config.emd_partner_id.id,
                    })
            else:
                inv_id = invoice_obj.search([('id', '=', fetch_inv_id[0])])
            if insurance_id:
                for details in insu_line:
                    if inv_id:
                        price_unit = float(details.insAmt) / float(details.quantity)
                        invoice_line_obj.create(
                            {
                                'product_id': details.product_id.id,
                                'name': details.product_id.name,
                                'invoice_id': inv_id.id,
                                'quantity': float(details.quantity),
                                'account_id': config.emd_account_id.id or False,
                                'price_unit': price_unit
                            })
                return True

    @api.multi
    def decision_yes(self):
        if self:
            self.check_number()
        return {'type': 'ir.actions.act_window_close'}
