# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.addons.account.wizard.pos_box import CashBox
from odoo.tools.misc import formatLang

class AccountBankStatement(models.Model):
    _inherit = 'account.bank.statement'

    #ПОС-той тохиолдолд Касын хяналт сонгоогүй үед бэлэн мөнгөний
    #журнальд ашиг алдагдалруу бичихгүй байх нөхцлийг
    #тооцож l10n_mn_account модуль дахь функцийг дарж бичив.
    @api.multi
    def _balance_check(self):
        for stmt in self:
            if not stmt.currency_id.is_zero(stmt.difference):
                if stmt.journal_type == 'cash':
                    #BEGIN
                    active_model = self.env.context.get('active_model', False)
                    if active_model == 'pos.session':
                        return True
                    #END
                    if stmt.difference < 0.0:
                        account = stmt.journal_id.loss_account_id
                        name = _('Loss')
                    else:
                        # statement.difference > 0.0
                        account = stmt.journal_id.profit_account_id
                        name = _('Profit')
                    if not account:
                        raise UserError(_('There is no account defined on the journal \"%s\" for \"%s\" involved in a cash difference.') % (stmt.journal_id.name, name))
                    #Автомат тооцоололд мөнгөн гүйлгээний төрөл нэмэв
                    if not stmt.journal_id.cashflow_id:
                        raise UserError(_('There is no default cashflow type defined on the journal \"%s\"') % (stmt.journal_id.name))

                    values = {
                        'statement_id': stmt.id,
                        'account_id': account.id,
                        'amount': stmt.difference,
                        'cashflow_id': stmt.journal_id.cashflow_id.id,
                        'name': _("Cash difference observed during the counting (%s)") % name,
                    }
                    self.env['account.bank.statement.line'].create(values)
                else:
                    balance_end_real = formatLang(self.env, stmt.balance_end_real, currency_obj=stmt.currency_id)
                    balance_end = formatLang(self.env, stmt.balance_end, currency_obj=stmt.currency_id)
                    if balance_end_real != balance_end:
                        raise UserError(_('The ending balance is incorrect !\nThe expected balance (%s) is different from the computed one. (%s)')
                                        % (balance_end_real, balance_end))
        return True

class AccountBankStatementLine(models.Model):
    _inherit = 'account.bank.statement.line'
    
    @api.model
    def create(self, vals):
        if vals.has_key('pos_statement_id'):
            if self.env.user.company_id.cashflow_id:
                cashflow_id =  self.env.user.company_id.cashflow_id.id
                vals.update({'cashflow_id': cashflow_id})
            else:
                raise UserError(_('Configure cashflow on company settings!'))
        return super(AccountBankStatementLine, self).create(vals)

    banknote_report = fields.Binary(string='Banknote Report')#Дэвсгэртээрх тайлан хадгалах талбар
    