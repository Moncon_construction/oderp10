#@PydevCodeAnalysisIgnore
from odoo import fields, models


class AccountJournal(models.Model):
    _inherit = 'account.journal'

    pos_cashflow_type = fields.Many2one('account.cashflow.type',
                                        string="Cashflow Type",
                                        help='Cashflow type')