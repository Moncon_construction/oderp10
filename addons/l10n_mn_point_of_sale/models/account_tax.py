# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _


class AccountTax(models.Model):
    _inherit = 'account.tax'
    
    name_for_pos = fields.Char(string="Name for POS", translate=True, default=_(u"НӨАТ"), help="This name printed on POS document.")