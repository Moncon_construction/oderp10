# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError
import logging
import base64
_logger = logging.getLogger(__name__)
from odoo.addons.point_of_sale.wizard.pos_box import PosBox as PosBoxMain
from odoo.addons.account.wizard.pos_box import CashBox


class PosBox(PosBoxMain):
    _register = False
    """
    Борлуулалтын цэгийн сэшнээс мөнгө гадагш гаргах үед 
    дэвсгэртээрх мөнгөн дүнг системд хадгалах.
    """
    # Override
    @api.multi
    def run(self):
        active_model = self.env.context.get('active_model', False)
        if active_model == 'pos.session':#ПОС-с дуудагдаж байгаа эсэх
            active_ids = self.env.context.get('active_ids', [])
            bank_statements = [session.cash_register_id for session in self.env[active_model].browse(active_ids) if session.cash_register_id]
            if not bank_statements:
                raise UserError(_("There is no cash register for this PoS Session"))
            #BEGIN
            else:
                if self.cash_control:#Кассын хяналт чеклсэн эсэх
                    total_amount = 0
                    total_amount = self.ten*10 + self.twenty*20 + self.fifty*50 + self.hundred*100 + self.five_hundred*500 + self.thousand*1000 + self.five_thousand*5000 + self.ten_thousand*10000 + self.twenty_thousand*20000
                    if total_amount != self.amount:
                        raise UserError(_('Amount does not equal!'))
                    active_id = self.env.context.get('active_id', [])
                    pos_session = self.env['pos.session'].search([('id','=',active_id)],limit=1)
                    data = {
                            'name':self.name,
                            'company':pos_session.config_id.company_id.name,
                            'pos':pos_session.config_id.name,
                            'pos_user':pos_session.user_id.name,
                            'ten': self.ten,
                            'ten_amount':self.ten*10,
                            'twenty': self.twenty,
                            'twenty_amount':self.twenty*20,
                            'fifty': self.fifty,
                            'fifty_amount':self.fifty*50,
                            'hundred' : self.hundred,
                            'hundred_amount':self.hundred*100,
                            'five_hundred' : self.five_hundred,
                            'five_hundred_amount':self.five_hundred*500,
                            'thousand' : self.thousand,
                            'thousand_amount':self.thousand*1000,
                            'five_thousand' : self.five_thousand,
                            'five_thousand_amount':self.five_thousand*5000,
                            'ten_thousand' : self.ten_thousand,
                            'ten_thousand_amount':self.ten_thousand*10000,
                            'twenty_thousand' : self.twenty_thousand,
                            'twenty_thousand_amount':self.twenty_thousand*20000,  
                            'total_amount':self.amount, 
                            }

                    pdf = self.env['report'].get_pdf([self.id], 'l10n_mn_point_of_sale.cash_box_out_banknote', data=data)
                    #Тайлангийн утга
                    result = base64.b64encode(pdf)
                    #Хавсралтууд цэсэнд давхар хадгалж явах
                    attachment_data = {
                        'name': str(pos_session.name),
                        'type':'binary',
                        'mimetype': 'pdf',
                        'datas_fname': 'banknote_report',
                        'datas': result,
                        'res_model': 'pos.session',
                        'res_id': pos_session.id,
                        'public':True
                    }
                    pos_session.banknote_report = result
                    attach_id = self.env['ir.attachment'].create(attachment_data)
                    #END
                return self._run(bank_statements)
        else:
            return super(PosBoxMain, self).run()

class CashBoxOut(PosBox):
    _inherit = 'cash.box.out'
    """
    ПОС-н тохиргоонд Кассын хяналт эрхтэй бол
    дэвсгэртээрх дүн авч баримтын талбар харуулах 
    """
    @api.onchange('amount')
    def compute_cash_control(self):
        for obj in self:
            if obj.amount >= 10:
                active_model = self.env.context.get('active_model', False)
                if active_model == 'pos.session':
                    active_id = self.env.context.get('active_id', [])
                    obj.cash_control = True
            else:
                obj.cash_control = False
    """
    ПОС-н сэшнээс ирсэн бол банкны хуулгын 
    мөрд дэвсгэртээрх тайланг хадгалах.
    """
    # Override
    @api.multi
    def _calculate_values_for_statement_line(self, record):
        if not record.journal_id.company_id.transfer_account_id:
            raise UserError(_("You should have defined an 'Internal Transfer Account' in your cash register's journal!"))
        #BEGIN
        amount = self.amount or 0.0
        active_model = self.env.context.get('active_model', False)
        if active_model == 'pos.session':
            active_id = self.env.context.get('active_id', [])
            pos_session = self.env['pos.session'].search([('id','=',active_id)],limit=1)
            pos_session.banknote_report
            return {
                'date': record.date,
                'statement_id': record.id,
                'journal_id': record.journal_id.id,
                'cashflow_id': self.cashflow_id.id,
                'amount': -amount if amount > 0.0 else amount,
                'partner_id': self.partner_id.id if self.partner_id.id else False,
                'account_id': self.account_id.id if self.account_id.id else record.journal_id.company_id.transfer_account_id.id,
                'name': self.name,
                'banknote_report':pos_session.banknote_report
            }
        else:
        #END
            return {
                'date': record.date,
                'statement_id': record.id,
                'journal_id': record.journal_id.id,
                'cashflow_id': self.cashflow_id.id,
                'amount': -amount if amount > 0.0 else amount,
                'partner_id': self.partner_id.id if self.partner_id.id else False,
                'account_id': self.account_id.id if self.account_id.id else record.journal_id.company_id.transfer_account_id.id,
                'name': self.name,
            }

    #Дэвсгэртээрх дүнг аввх талбарууд
    cash_control = fields.Boolean('Cash Control')
    ten = fields.Integer('10')
    twenty = fields.Integer('20')
    fifty = fields.Integer('50')
    hundred = fields.Integer('100')
    five_hundred = fields.Integer('500')
    thousand = fields.Integer('1000')
    five_thousand = fields.Integer('5000')
    ten_thousand =  fields.Integer('10000')
    twenty_thousand = fields.Integer('20000')
    banknote_report = fields.Binary(string='Banknote Report')