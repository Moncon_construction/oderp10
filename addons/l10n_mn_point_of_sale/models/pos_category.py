# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.addons.account.wizard.pos_box import CashBox
from odoo.tools.misc import formatLang


class PosCategory(models.Model):
    _inherit = 'pos.category'

    company_id = fields.Many2one("res.company", string='Company')
