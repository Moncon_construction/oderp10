# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from lxml import etree


class PosConfig(models.Model):
    _inherit = 'pos.config'

    vatps_address = fields.Char(string='vatps address', default='http://localhost:8025/put/')
    ebarimt_info_address = fields.Char(string='ebarimt info address', default='http://info.ebarimt.mn/rest/merchant/info?regno=')
    receipt_print_type = fields.Selection([('customer', _('Only Customer')),
                                           ('both', _('Customer and company'))], default='customer', required=1)
    district_code_id = fields.Many2one('vat.district.code', string='Province/District code')
    logo = fields.Binary(string="Logo")
    show_product_images = fields.Boolean(string='Display Product Pictures', default=True,
                                         help="The product will be displayed with pictures.")

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(PosConfig, self).fields_view_get(view_id, view_type, toolbar, submenu)
        if view_type == 'form':
            doc = etree.XML(res['arch'])

            # picking_type_id-д зөвшөөрөгдсөн агуулахын дүрэм бичив.
            if self.env.user.allowed_warehouses:
                domain = """[
                    '|', ('warehouse_id', '=', False), ('warehouse_id', 'in', %s),
                    '|', '|', ('warehouse_id', '=', False), ('warehouse_id.company_id', '=', False), ('warehouse_id.company_id', 'child_of', [%s])
                ]""" % (str(self.env.user.allowed_warehouses.ids), str(self.env.user.company_id.id))
            else:
                domain = """[
                    '|', '|', ('warehouse_id', '=', False), ('warehouse_id.company_id', '=', False), ('warehouse_id.company_id', 'child_of', [%s])
                ]""" % (str(self.env.user.company_id.id))

            for node in doc.xpath("//field[@name='picking_type_id']"):
                node.set('domain', domain)

            # stock_location_id-д зөвшөөрөгдсөн агуулахын дүрэм бичив.
            if self.env.user.allowed_warehouses:
                allowed_whs = self.env.user.allowed_warehouses.filtered(lambda wh: wh.company_id == self.env.user.company_id)
                allowed_location_ids = [wh.lot_stock_id.id for wh in allowed_whs]
                if allowed_location_ids and len(allowed_location_ids) > 0:
                    domain = "[('id', 'in', %s)]" % (str(allowed_location_ids))
                else:
                    domain = "[]"

                for node in doc.xpath("//field[@name='stock_location_id']"):
                    node.set('domain', domain)

            res['arch'] = etree.tostring(doc)
        return res

    @api.depends('session_ids')
    def _compute_current_session_user(self):
        # @Override: 'Нээлтийн хяналт' /opening_control/ төлөв бүхий session-ы харгалзах config дээр харилцагчийг харуулдаг болгов.
        for pos_config in self:
            session = pos_config.session_ids.filtered(lambda s: s.state in ('opened', 'opening_control') and '(RESCUE FOR' not in s.name)
            pos_config.pos_session_username = session and session[0].user_id.name or False
