# -*- coding: utf-8 -*-
import json
import requests
import logging
import psycopg2

from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError
from odoo.tools import float_is_zero
from datetime import datetime

from lxml import etree
from odoo.addons.l10n_mn_report.models.report_helper import verbose_numeric, comma_me, convert_curr  # @UnresolvedImport
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.addons.l10n_mn_web.models.time_helper import *
from functools import partial

import base64
_logger = logging.getLogger(__name__)

class EtaxBills(models.Model):
    _name = "etax.bills"

    pos_order_id = fields.Many2one('pos.order', string='Order Ref', ondelete='cascade')

    receipt_type = fields.Selection([('person', 'Person'), ('company', 'Company')], string='Receipt Type',
                                    readonly=True)
    ddtd_number = fields.Char('DDTD')
    lottery_number = fields.Char('Lottery')
    ebarimt_register_amount = fields.Monetary(string='Amount for Register', readonly=True)
    qrData = fields.Text('QR code')
    customer_register = fields.Char('Customer Register')
    customer_name = fields.Char('Customer Name')
    is_bill_returned = fields.Boolean(string='Is Bill Returned', default=False)
    bill_returned_date = fields.Datetime(string="Bill Rerurned Date")
    currency_id = fields.Many2one('res.currency', related='pos_order_id.session_id.currency_id', string="Currency")

    def _tax_bills_fields(self, line):
        return line

class PosOrder(models.Model):
    _inherit = "pos.order"

    company_id = fields.Many2one('res.company', string='Company', required=True, readonly=True, default=lambda self: self.env.user.company_id)
    currency_id = fields.Many2one('res.currency', related='session_id.currency_id', string="Currency")
   
    amount_return = fields.Float(compute='_compute_amount_all', string='Returned', digits=0, store=True)
    lottery_number = fields.Char('Lottery')
    ddtd_number = fields.Char('DDTD')
    qrData = fields.Text('QR code')
    is_bill_returned = fields.Boolean(string='Is Bill Returned', default=False)
    bill_returned_date = fields.Datetime(string="Bill Rerurned Date")
    receipt_type = fields.Selection([('person', 'Person'), ('company', 'Company')], string='Receipt Type', readonly=True)
    customer_register = fields.Char('Customer Register')
    customer_name = fields.Char('Customer Name')
    
    ebarimt_discount_amount = fields.Monetary(string='Discount Amount', readonly=True)
    ebarimt_total_amount_with_tax = fields.Monetary(string='Total Amount with Tax', readonly=True)
    ebarimt_total_amount_without_tax = fields.Monetary(string='Total Amount without Tax', readonly=True)
    ebarimt_vat_amount = fields.Monetary(string='Vat Amount', readonly=True)
    ebarimt_city_tax_amount = fields.Monetary(string='City Tax Amount', readonly=True)
    ebarimt_register_amount = fields.Monetary(string='Amount for Register', readonly=True)

    product_id = fields.Many2one('product.product', related='lines.product_id', string='Product')
    etax_bills = fields.One2many('etax.bills', 'pos_order_id', string='Ebarimt Bills')
    #Override
    # This deals with orders that belong to a closed session. In order
    # to recover from this situation we create a new rescue session,
    # making it obvious that something went wrong.
    # A new, separate, rescue session is preferred for every such recovery,
    # to avoid adding unrelated orders to live sessions.
    def _get_valid_session(self, order):
        PosSession = self.env['pos.session']
        closed_session = PosSession.browse(order['pos_session_id'])

        _logger.warning('session %s (ID: %s) was closed but received order %s (total: %s) belonging to it',
                        closed_session.name,
                        closed_session.id,
                        order['name'],
                        order['amount_total'])
        rescue_session = PosSession.search([
            ('name', 'like', '(RESCUE FOR %(session)s)' % {'session': closed_session.name}),
            ('state', 'not in', ('closed', 'closing_control')),
        ], limit=1)
        if rescue_session:
            _logger.warning('reusing recovery session %s for saving order %s', rescue_session.name, order['name'])
            return rescue_session

        _logger.warning('attempting to create recovery session for saving order %s', order['name'])
        #     Борлуулалтын цэгийн сэшин хаагдсан үед 
        #     үргэлжлүүлэн борлуулалт хийх үед шинээр RESCUE 
        #     сэшин үүсэхдээ админ дээр биш худалдагч дээр үүсгэдэг болгов.
        new_session = PosSession.create({
            'config_id': closed_session.config_id.id,
            #BEGIN
            'user_id':closed_session.user_id.id,
            #END
            'name': '(RESCUE FOR %(session)s)' % {'session': closed_session.name},
        })
        # bypass opening_control (necessary when using cash control)
        new_session.action_pos_session_open()

        return new_session

    @api.model
    def _order_fields(self, ui_order):
        # create_from_ui функцыг ебаримтын талбаруудыг оруулж өгөхийн тулд override хийсэн байсан
        # Энэ method-г ашиглаж болно.

        fields = super(PosOrder, self)._order_fields(ui_order)
        fields['lottery_number'] = ui_order.get('lottery', False)
        fields['ddtd_number'] = ui_order.get('billId', False)
        fields['qrData'] = ui_order.get('qrData', False)
        fields['ebarimt_total_amount_with_tax'] = ui_order.get('ebarimt_total_amount_with_tax', 0)
        fields['ebarimt_total_amount_without_tax'] = ui_order.get('ebarimt_total_amount_without_tax', 0)
        fields['ebarimt_vat_amount'] = ui_order.get('ebarimt_vat_amount', 0)
        fields['ebarimt_city_tax_amount'] = ui_order.get('ebarimt_city_tax_amount', 0)
        fields['ebarimt_register_amount'] = ui_order.get('ebarimt_register_amount', 0)
        fields['receipt_type'] = ui_order.get('billType', False)
        fields['customer_register'] = ui_order.get('customerNo', False)
        fields['customer_name'] = ui_order.get('companyName', False)
        bill_row = partial(self.env['etax.bills']._tax_bills_fields)
        fields['etax_bills'] = [bill_row(l) for l in ui_order['etax_bills']] if ui_order['etax_bills'] else False
        return fields

    @api.model
    def get_merchant_info(self, urlInput):
        """ Get metchant info from ebarimt REST api """
        resp = requests.get(url=urlInput)
        data = None

        try:
            data = json.loads(resp.text)
        except Exception as e:
            raise Warning(_('Error'), _('Could not connect to json device. \n%s') % e.message)

        data_json = json.dumps(data)
        return data_json
    
    @api.multi
    def get_picking_ids(self):
        return self.mapped('picking_id')

    @api.multi
    def get_order_data(self):
        group = self.user_has_groups('l10n_mn_point_of_sale.group_do_back_order')
        date = str(get_day_like_display(datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT), self.env.user))
        return {"group": group, "date": date}

    @api.multi
    def update_order(self):
        self.is_bill_returned = True
        self.bill_returned_date = datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        return True
    
    @api.multi
    def reprint_bill(self):
        for obj in self:
            view = obj.env.ref('l10n_mn_point_of_sale.pos_bill_reprint_view')
            return {
                'name': _('Reprinting Reason'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'pos.bill.reprint.history',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new'
            }

    @api.multi
    def refund(self):
        PosOrder = self.env['pos.order']
        for order in self:
            if order.config_id:
                # Тухайн захиалга үүссэн Борлуулалтын цэгийн нээлттэй сэшн рүү буцаалт үүсгэх
                current_session = self.env['pos.session'].search([('config_id', '=', order.config_id.id),('state', '=', 'opened')], order="start_at desc", limit=1)
                # Нээлттэй сэшн байхгүй бол анхааруулга өгөх
                if not current_session:
                    raise UserError(_("The Opened session hasn't at \"%s\" point of sale." % order.config_id.name))
        for order in self:
            clone = order.copy({
                # ot used, name forced by create
                'name': order.name + _(' REFUND'),
                'session_id': current_session.id,
                'date_order': fields.Datetime.now(),
                'pos_reference': order.pos_reference,
            })
            PosOrder += clone
        for clone in PosOrder:
            for order_line in clone.lines:
                order_line.write({'qty': -order_line.qty})
        return {
            'name': _('Return Products'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'pos.order',
            'res_id': PosOrder.ids[0],
            'view_id': False,
            'context': self.env.context,
            'type': 'ir.actions.act_window',
            'target': 'current',
        }

    def create_picking(self):
        _logger.info('pos_order create_picking')
        """Create a picking for each order and validate it."""
        Picking = self.env['stock.picking']
        Move = self.env['stock.move']
        StockWarehouse = self.env['stock.warehouse']
        for order in self:
            if not order.lines.filtered(lambda l: l.product_id.type in ['product', 'consu']):
                continue
            address = order.partner_id.address_get(['delivery']) or {}
            picking_type = order.picking_type_id
            return_pick_type = order.picking_type_id.return_picking_type_id or order.picking_type_id
            order_picking = Picking
            return_picking = Picking
            location_id = order.location_id.id
            if order.partner_id:
                destination_id = order.partner_id.property_stock_customer.id
            else:
                if (not picking_type) or (not picking_type.default_location_dest_id):
                    customerloc, supplierloc = StockWarehouse._get_partner_locations()
                    destination_id = customerloc.id
                else:
                    destination_id = picking_type.default_location_dest_id.id

            if picking_type:
                message = _(
                    "This transfer has been created from the point of sale session: <a href=# data-oe-model=pos.order data-oe-id=%d>%s</a>") % (
                          order.id, order.name)
                picking_vals = {
                    'origin': order.name,
                    'partner_id': address.get('delivery', False),
                    'date_done': order.date_order,
                    'picking_type_id': picking_type.id,
                    'company_id': order.company_id.id,
                    'move_type': 'direct',
                    'note': order.note or "",
                    'location_id': location_id,
                    'location_dest_id': destination_id,
                }
                pos_qty = any([x.qty > 0 for x in order.lines if x.product_id.type in ['product', 'consu']])
                if pos_qty:
                    order_picking = Picking.create(picking_vals.copy())
                    order_picking.message_post(body=message)
                neg_qty = any([x.qty < 0 for x in order.lines if x.product_id.type in ['product', 'consu']])
                if neg_qty:
                    return_vals = picking_vals.copy()
                    return_vals.update({
                        'location_id': destination_id,
                        'location_dest_id': return_pick_type != picking_type and return_pick_type.default_location_dest_id.id or location_id,
                        'picking_type_id': return_pick_type.id
                    })
                    return_picking = Picking.create(return_vals)
                    return_picking.message_post(body=message)

            for line in order.lines.filtered(
                    lambda l: l.product_id.type in ['product', 'consu'] and not float_is_zero(l.qty, precision_rounding=l.product_id.uom_id.rounding)):
                # stock_move create функц нь удаан ажиллаж байгаа тул insert query болгож солив
                _logger.info('pos_order create stock move')
                self.env.cr.execute("""INSERT INTO stock_move (create_uid, create_date, write_uid, write_date,
                                                               date, date_expected, name, picking_id, 
                                                               picking_type_id, product_id, product_uom_qty, product_qty, product_uom, 
                                                               state, procure_method, order_line_id, company_id, analytic_account_id, 
                                                               location_id, location_dest_id) 
                                                    values (%s, '%s', %s, '%s', '%s', '%s', '%s', %s, %s, %s, %s, %s, %s, '%s', '%s', %s, %s, %s, %s, %s) """
                                    % (self.env.user.id, datetime.now(), self.env.user.id, datetime.now(),
                                       order.date_order, order.date_order, line.name, order_picking.id if line.qty >= 0 else return_picking.id,
                                       picking_type.id if line.qty >= 0 else return_pick_type.id, line.product_id.id, abs(line.qty), abs(line.qty), line.product_id.uom_id.id,
                                       'draft', 'make_to_stock', line.id, order.company_id.id, line.analytic_account_id and line.analytic_account_id.id or 'NULL',
                                       location_id if line.qty >= 0 else destination_id,
                                       destination_id if line.qty >= 0 else return_pick_type != picking_type and return_pick_type.default_location_dest_id.id or location_id
                                       ))

            # prefer associating the regular order picking, not the return
            order.write({'picking_id': order_picking.id or return_picking.id})

            if return_picking:
                order._force_picking_done(return_picking)
            if order_picking:
                order._force_picking_done(order_picking)

            # when the pos.config has no picking_type_id set only the moves will be created
            if not return_picking and not order_picking:
                moves = Move.search([('order_line_id', 'in', order.lines.ids)])
                if moves:
                    tracked_moves = moves.filtered(lambda move: move.product_id.tracking != 'none')
                    untracked_moves = moves - tracked_moves
                    tracked_moves.action_confirm()
                    untracked_moves.action_assign()
                    moves.filtered(lambda m: m.state in ['confirmed', 'waiting']).force_assign()
                    moves.filtered(lambda m: m.product_id.tracking == 'none').action_done()

        return True

    @api.model
    def _amount_line_tax(self, line, fiscal_position_id):
        # @Override: Хэрвээ груп татвар тохируулагдсан бол давхар бүртгэсэн бусад дан татваруудыг авахгүй
        taxes = line.tax_ids.filtered(lambda r: (not line.order_id.company_id or r.company_id == line.order_id.company_id))
        
        #################### START: Хэрвээ груп татвар тохируулагдсан бол давхар бүртгэсэн бусад дан татваруудыг авахгүй ####################
        grouped_taxes_for_pos = line.tax_ids.filtered(lambda r: (not line.order_id.company_id or r.company_id == line.order_id.company_id) and r.amount_type == 'group' and r.type_tax_use == 'sale')
        if grouped_taxes_for_pos and len(grouped_taxes_for_pos) > 0:
            taxes = grouped_taxes_for_pos
        if fiscal_position_id:
            taxes = fiscal_position_id.map_tax(taxes, line.product_id, line.order_id.partner_id)
        #################### END: Хэрвээ груп татвар тохируулагдсан бол давхар бүртгэсэн бусад дан татваруудыг авахгүй ####################
        
        price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
        taxes = taxes.compute_all(price, line.order_id.pricelist_id.currency_id, line.qty, product=line.product_id, partner=line.order_id.partner_id or False)['taxes']
        
        return sum(tax.get('amount', 0.0) for tax in taxes)
    
    def test_paid(self):
        #@Override: Оронгын нарийвчлалаас болж мөн шинэ төлөвтэйгээр үүсээд бсан тул оронгын нарийвчлалыг динамикаар авдаг болгов
        """A Point of Sale is paid when the sum
        @return: True
        """
        prec_acc = self.env['decimal.precision'].precision_get('Account')
        for order in self:
            if order.lines and not order.amount_total:
                continue
            if (not order.lines) or (not order.statement_ids) or (abs(order.amount_total - order.amount_paid) > 10 ** (-prec_acc or -5)):
                return False
        return True
    
    @api.model
    def _process_order(self, pos_order):
        # @Override
        prec_acc = self.env['decimal.precision'].precision_get('Account')
        pos_session = self.env['pos.session'].browse(pos_order['pos_session_id'])
        if pos_session.state == 'closing_control' or pos_session.state == 'closed':
            pos_order['pos_session_id'] = self._get_valid_session(pos_order).id
        order = self.create(self._order_fields(pos_order))
        journal_ids = set()
        # ### BEGIN ####
        '''
            Борлуулалтын цэгээс хариулт мөнгөний дүнгээр
            төлбөрийн буцаалт үүсгэхгүй тохиргоог шалгах
        '''
        if order['company_id'].non_create_move_for_cash_change:
            # set last cash payment
            lst_cash_pmnt = False
            for payment in pos_order['statement_ids']:
                if payment[2]['journal_id']:
                    cash_journal = self.env['account.journal'].search([
                        ('type', '=', 'cash'),
                        ('id', '=', payment[2]['journal_id']),
                    ])
                    if cash_journal:
                        lst_cash_pmnt = payment
                    
            for payments in pos_order['statement_ids']:
                # deduct return amount from cash payment without create bank_statement_line from return amount
                if not float_is_zero(pos_order['amount_return'], prec_acc):
                    # check payment is last cash or not: if true deduct return amount from it
                    if lst_cash_pmnt and lst_cash_pmnt == payments:
                        payments[2]['amount'] = payments[2]['amount'] - pos_order['amount_return']
                if not float_is_zero(payments[2]['amount'], precision_digits=prec_acc):
                    order.add_payment(self._payment_fields(payments[2]))
                journal_ids.add(payments[2]['journal_id'])
            if pos_session.sequence_number <= pos_order['sequence_number']:
                pos_session.write({'sequence_number': pos_order['sequence_number'] + 1})
                pos_session.refresh()

        else:
            # ### END ####
            for payments in pos_order['statement_ids']:
                if not float_is_zero(payments[2]['amount'], precision_digits=prec_acc):
                    order.add_payment(self._payment_fields(payments[2]))
                journal_ids.add(payments[2]['journal_id'])

            if pos_session.sequence_number <= pos_order['sequence_number']:
                pos_session.write({'sequence_number': pos_order['sequence_number'] + 1})
                pos_session.refresh()

            if not float_is_zero(pos_order['amount_return'], prec_acc):
                cash_journal_id = pos_session.cash_journal_id.id
                if not cash_journal_id:
                    # Select for change one of the cash journals used in this
                    # payment
                    cash_journal = self.env['account.journal'].search([
                        ('type', '=', 'cash'),
                        ('id', 'in', list(journal_ids)),
                    ], limit=1)
                    if not cash_journal:
                        # If none, select for change one of the cash journals of the POS
                        # This is used for example when a customer pays by credit card
                        # an amount higher than total amount of the order and gets cash back
                        cash_journal = [statement.journal_id for statement in pos_session.statement_ids if statement.journal_id.type == 'cash']
                        if not cash_journal:
                            raise UserError(_("No cash statement found for this session. Unable to record returned cash."))
                    cash_journal_id = cash_journal[0].id
                order.add_payment({
                    'amount': -pos_order['amount_return'],
                    'payment_date': fields.Datetime.now(),
                    'payment_name': _('return'),
                    'journal': cash_journal_id,
                })
        return order

    def get_qry_value(self, value, key):
        # check dictionary value and convert it for postgresql query and return
        if key in value.keys() and value[key]:
            if isinstance(value[key], basestring):
                return value[key].replace("'", "''")
            else:
                return value[key]
        if key in ('credit', 'debit'):
            return 0
        else:
            return 'NULL'
            
    def create_move_by_qry(self, move, grouped_data):
        # _create_account_move_line функц дах журналын бичилт үүсгэх хэсгийг тусдаа функц болгов.
        # create account.move.lines using postgresql query
        if move:
            company_id = move.company_id.id
            journal_id = move.journal_id.id
            move_id = move.id
            ref = move.ref
            move_date = move.date or datetime.now()
            for group_key, group_data in grouped_data.iteritems():
                for value in group_data:
                    # set partner_id when account can reconcile
                    account = self.env['account.account'].browse(value['account_id'])
                    order_for_partner = self[0] if self and len(self) > 0 else self
                    order_seller_id = order_for_partner.session_id.user_id.partner_id.id if order_for_partner.session_id and order_for_partner.session_id.user_id and order_for_partner.session_id.user_id.partner_id else False
                    if account.reconcile and order_seller_id and ('partner_id' not in value.keys() or ('partner_id' in value.keys() and not value['partner_id'])):
                        value['partner_id'] = order_seller_id

                    uid = self.env.uid
                    date = fields.Datetime.now()
                    
                    self.env.cr.execute(
                    """
                        INSERT INTO account_move_line (create_uid, create_date, write_uid, write_date, company_id, journal_id, move_id, ref, name, quantity, product_id, account_id, analytic_account_id, credit, debit, partner_id, tax_line_id, currency_rate, date_maturity, date, register_date) 
                        VALUES (%s, '%s', %s, '%s', %s, %s, %s, '%s', '%s', %s, %s, %s, %s, %s, %s, %s, %s, 1.0, '%s', '%s', '%s')
                        RETURNING id 
                    """ %(uid, date, uid, date, company_id, journal_id, move_id, ref, self.get_qry_value(value, 'name'), self.get_qry_value(value, 'quantity'), self.get_qry_value(value, 'product_id'),
                          self.get_qry_value(value, 'account_id'), self.get_qry_value(value, 'analytic_account_id'),
                          self.get_qry_value(value, 'credit'), self.get_qry_value(value, 'debit'),
                          self.get_qry_value(value, 'partner_id'), self.get_qry_value(value, 'tax_line_id'),
                          str(move_date) if move_date else 'NULL', str(move_date) if move_date else 'NULL', str(move_date) if move_date else 'NULL')
                    )
                    new_move_line_id = self.env.cr.fetchone()[0]

                    # if account.move.lines object has tax, then create linked module's object for it
                    insert_qry = ""
                    if new_move_line_id and 'tax_ids' in value.keys():
                        tax_ids = value['tax_ids'][0][2]
                        if tax_ids and len(tax_ids) > 0:
                            uniq_tax_ids = []
                            for tax_id in tax_ids:
                                if tax_id not in uniq_tax_ids:
                                    uniq_tax_ids.append(tax_id)

                            insert_qry = "INSERT INTO account_move_line_account_tax_rel (account_move_line_id, account_tax_id) VALUES "
                            for tax_id in uniq_tax_ids:
                                if tax_id == uniq_tax_ids[0]:
                                    insert_qry += "(" + str(new_move_line_id) + ", " + str(tax_id) + ")"
                                else:
                                    insert_qry += ", (" + str(new_move_line_id) + ", " + str(tax_id) + ")"

                            self.env.cr.execute(insert_qry)

            move.sudo().post()

        return move
          
    def _flatten_tax_and_children(self, taxes, group_done=None):
        # Inner /_create_account_move_line/ -> Outer: Кодод өөрчлөлт оруулалгүйгээр тусдаа функц болгов.
        children = self.env['account.tax']
        if group_done is None:
            group_done = set()
        for tax in taxes.filtered(lambda t: t.amount_type == 'group'):
            if tax.id not in group_done:
                group_done.add(tax.id)
                children |= self._flatten_tax_and_children(tax.children_tax_ids, group_done)
        return taxes + children
          
    def add_anglosaxon_lines(self, move, grouped_data, order):
        # Inner /_create_account_move_line/ -> Outer: Кодод өөрчлөлт оруулалгүйгээр тусдаа функц болгов.
        Product = self.env['product.product']
        Analytic = self.env['account.analytic.account']
        for product_key in list(grouped_data.keys()):
            if product_key[0] == "product":
                line = grouped_data[product_key][0]
                product = Product.browse(line['product_id'])
                # In the SO part, the entries will be inverted by function compute_invoice_totals
                price_unit = - product._get_anglo_saxon_price_unit()
                account_analytic = Analytic.browse(line.get('analytic_account_id'))
                res = Product._anglo_saxon_sale_move_lines(
                    line['name'], product, product.uom_id, line['quantity'], price_unit,
                        fiscal_position=order.fiscal_position_id,
                        account_analytic=account_analytic)
                if res:
                    line1, line2 = res
                    line1 = Product._convert_prepared_anglosaxon_line(line1, order.partner_id)
                    grouped_data = self.insert_data('counter_part', order, move, grouped_data, {
                        'name': line1['name'],
                        'account_id': line1['account_id'],
                        'credit': line1['credit'] or 0.0,
                        'debit': line1['debit'] or 0.0,
                        'partner_id': line1['partner_id']
                    })

                    line2 = Product._convert_prepared_anglosaxon_line(line2, order.partner_id)
                    grouped_data = self.insert_data('counter_part', order, move, grouped_data, {
                        'name': line2['name'],
                        'account_id': line2['account_id'],
                        'credit': line2['credit'] or 0.0,
                        'debit': line2['debit'] or 0.0,
                        'partner_id': line2['partner_id']
                    })
        return grouped_data
                        
    def get_income_account(self, line):
        # _create_account_move_line функц дах income_account тооцоолох хэсгийг тусдаа функц болгов.
        income_account = False
        if line.product_id.property_account_income_id.id:
            income_account = line.product_id.property_account_income_id.id
        elif line.product_id.categ_id.property_account_income_categ_id.id:
            income_account = line.product_id.categ_id.property_account_income_categ_id.id
        else:
            raise UserError(_('Please define income '
                              'account for this product: "%s" (id:%d).')
                            % (line.product_id.name, line.product_id.id))
        return income_account
                           
    def get_group_key(self, data_type, values):
        # _create_account_move_line/insert_data inner функц дах group-н key олох кодыг тусдаа функц болгов.
        key = False
        if data_type == 'product':
            key = ('product', values['partner_id'], (values['product_id'], tuple(values['tax_ids'][0][2]), values['name']), values['debit'] > 0)
        elif data_type == 'tax':
            key = ('tax', values['partner_id'], values['tax_line_id'], values['debit'] > 0)
        elif data_type == 'counter_part':
            key = ('counter_part', values['partner_id'], values['account_id'], values['debit'] > 0)
        return key
                
    def insert_data(self, data_type, order, move, grouped_data, values):
        # Inner /_create_account_move_line/ -> Outer: Журналын мөр үүсгэхэд ашиглах dictionay-г груплэж буцаах функц
        # if have_to_group_by:

        values.update({'move_id': move.id}) # partner_id-г update хийх шаардлагагүй

        key = self.get_group_key(data_type, values)
        if not key:
            return
        grouped_data.setdefault(key, [])

        have_to_group_by = order.session_id and order.session_id.config_id.group_by or False
        if have_to_group_by:
            if not grouped_data[key]:
                grouped_data[key].append(values)
            else:
                current_value = grouped_data[key][0]
                current_value['quantity'] = current_value.get('quantity', 0.0) + values.get('quantity', 0.0)
                current_value['credit'] = current_value.get('credit', 0.0) + values.get('credit', 0.0)
                current_value['debit'] = current_value.get('debit', 0.0) + values.get('debit', 0.0)
        else:
            grouped_data[key].append(values)
            
        return grouped_data
    
    def insert_balance_data(self, data_type, order, line, move, grouped_data, values):
        # Балансын дансуудыг шинжилгээний дансаар салгаж журналын бичилт үүсгэх үед /l10n_mn_analytic_balance_pos/ ашиглагдана.
        grouped_data = self.insert_data(data_type, order, move, grouped_data, values)
        return grouped_data
    
    """ЭНЭХҮҮ ФУНКЦ ДЭЭР ӨӨРЧЛӨЛТ ОРУУЛАХААР БОЛ l10n_mn_pos_loyalty_card дээр мөн оруулж өгнө үү"""
    def _create_account_move_line(self, session=None, move=None):
        # @Override: Дэд функц болгон дарж бичив. CR хийв.
        # Tricky, via the workflow, we only have one id in the ids variable
        """Create a account move line of order grouped by products or not."""
        IrProperty = self.env['ir.property']
        ResPartner = self.env['res.partner']

        if session and not all(session.id == order.session_id.id for order in self):
            raise UserError(_('Selected orders do not have the same session!'))

        grouped_data = {}
        have_to_group_by = session and session.config_id.group_by or False
        rounding_method = session and session.config_id.company_id.tax_calculation_rounding_method

        for order in self.filtered(lambda o: not o.account_move or o.state == 'paid'):
            current_company = order.sale_journal.company_id
            account_def = IrProperty.get(
                'property_account_receivable_id', 'res.partner')
            order_account = order.partner_id.property_account_receivable_id.id or account_def and account_def.id
            partner_id = ResPartner._find_accounting_partner(order.partner_id).id or False
            if move is None:
                # Create an entry for the sale
                journal_id = self.env['ir.config_parameter'].sudo().get_param(
                    'pos.closing.journal_id_%s' % current_company.id, default=order.sale_journal.id)
                move = self._create_account_move(
                    order.session_id.start_at, order.name, int(journal_id), order.company_id.id)

            # because of the weird way the pos order is written, we need to make sure there is at least one line,
            # because just after the 'for' loop there are references to 'line' and 'income_account' variables (that
            # are set inside the for loop)
            # TOFIX: a deep refactoring of this method (and class!) is needed
            # in order to get rid of this stupid hack
            assert order.lines, _('The POS order must have lines when calling this method')
            # Create an move for each order line
            cur = order.pricelist_id.currency_id
            for line in order.lines:
                amount = line.price_subtotal

                # Search for the income account
                income_account = self.get_income_account(line)

                name = line.product_id.name
                if line.notice:
                    # add discount reason in move
                    name = name + ' (' + line.notice + ')'

                # Create a move for the line for the order line
                # Just like for invoices, a group of taxes must be present on this base line
                # As well as its children
                base_line_tax_ids = self._flatten_tax_and_children(line.tax_ids_after_fiscal_position).filtered(lambda tax: tax.type_tax_use in ['sale', 'none'])
                grouped_data = self.insert_balance_data('product', order, line, move, grouped_data, {
                    'name': name,
                    'quantity': line.qty,
                    'product_id': line.product_id.id,
                    'account_id': income_account,
                    'analytic_account_id': self._prepare_analytic_account(line),
                    'credit': ((amount > 0) and amount) or 0.0,
                    'debit': ((amount < 0) and -amount) or 0.0,
                    'tax_ids': [(6, 0, base_line_tax_ids.ids)],
                    'partner_id': partner_id
                })

                # Create the tax lines
                taxes = line.tax_ids_after_fiscal_position.filtered(lambda t: t.company_id.id == current_company.id)
                if not taxes:
                    continue
                price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
                for tax in taxes.compute_all(price, cur, line.qty)['taxes']:
                    ################## START: Татварын мөр дээрхи харилцагчийг татварын ХОЛБООТОЙ ХАРИЛЦАГЧ-с авах хэсэг ################
                    taxes = self.env['account.tax'].browse(tax['id'])
                    tax_obj = False
                    if taxes and len(taxes) > 0:
                        tax_obj = taxes[0]
                    grouped_data = self.insert_balance_data('tax', order, line, move, grouped_data, {
                        'name': _('Tax') + ' ' + tax['name'],
                        'product_id': line.product_id.id,
                        'quantity': line.qty,
                        'account_id': tax['account_id'] or income_account,
                        'credit': ((tax['amount'] > 0) and tax['amount']) or 0.0,
                        'debit': ((tax['amount'] < 0) and -tax['amount']) or 0.0,
                        'tax_line_id': tax['id'],
                        'partner_id': tax_obj.partner_id.id if (tax_obj and tax_obj.partner_id) else partner_id
                    })
                    ################## END: Татварын мөр дээрхи харилцагчийг татварын ХОЛБООТОЙ ХАРИЛЦАГЧ-с авах хэсэг ################

            # round tax lines per order
            if rounding_method == 'round_globally':
                for group_key, group_value in grouped_data.iteritems():
                    if group_key[0] == 'tax':
                        for line in group_value:
                            line['credit'] = cur.round(line['credit'])
                            line['debit'] = cur.round(line['debit'])

            # counterpart
            grouped_data = self.insert_balance_data('counter_part', order, False, move, grouped_data, {
                'name': _("Trade Receivables"),  # order.name,
                'account_id': order_account,
                'credit': ((order.amount_total < 0) and -order.amount_total) or 0.0,
                'debit': ((order.amount_total > 0) and order.amount_total) or 0.0,
                'partner_id': partner_id
            })
            order.write({'state': 'done', 'account_move': move.id})

        if self and order.company_id.anglo_saxon_accounting:
            grouped_data = self.add_anglosaxon_lines(move, grouped_data, order)

        ################## START: Сэшн хаахад хэтэрхий удаж байсан тул qry ашиглаж журналын мөр үүсгэдэг болгов ################
        self.create_move_by_qry(move, grouped_data)
        ################## END: Сэшн хаахад хэтэрхий удаж байсан тул qry ашиглаж журналын мөр үүсгэдэг болгов ################

        return True
    
    def _prepare_bank_statement_line_payment_values(self, data):
        """Create a new payment for the order"""
        
        args = {
            'amount': data['amount'],
            'date': data.get('payment_date', fields.Date.today()),
            'name': self.name + ': ' + (data.get('payment_name', '') or ''),
            'partner_id': self.env["res.partner"]._find_accounting_partner(self.partner_id).id or (self.user_id.partner_id.id if self.user_id.partner_id else False) or False,
        }

        journal_id = data.get('journal', False)
        statement_id = data.get('statement_id', False)
        assert journal_id or statement_id, "No statement_id or journal_id passed to the method!"

        journal = self.env['account.journal'].browse(journal_id)
        # use the company of the journal and not of the current user
        company_cxt = dict(self.env.context, force_company=journal.company_id.id)
        account_def = self.env['ir.property'].with_context(company_cxt).get('property_account_receivable_id', 'res.partner')
        args['account_id'] = (self.partner_id.property_account_receivable_id.id) or (account_def and account_def.id) or False

        if not args['account_id']:
            if not args['partner_id']:
                msg = _('There is no receivable account defined to make payment.')
            else:
                msg = _('There is no receivable account defined to make payment for the partner: "%s" (id:%d).') % (
                    self.partner_id.name, self.partner_id.id,)
            raise UserError(msg)

        context = dict(self.env.context)
        context.pop('pos_session_id', False)
        for statement in self.session_id.statement_ids:
            if statement.id == statement_id:
                journal_id = statement.journal_id.id
                break
            elif statement.journal_id.id == journal_id:
                statement_id = statement.id
                break
        if not statement_id:
            raise UserError(_('You have to open at least one cashbox.'))

        args.update({
            'statement_id': statement_id,
            'pos_statement_id': self.id,
            'journal_id': journal_id,
            'ref': self.session_id.name,
        })

        return args
    
    def _force_picking_done(self, picking):
        """Force picking in order to be set as done."""
        self.ensure_one()
        if self.user_id.company_id.force_assign_when_insufficient_residual == 'check':
            picking.with_context(pos=True).force_assign()
        if picking.state not in ('done', 'cancel'):
            contains_tracked_products = any([(product_id.tracking != 'none') for product_id in self.lines.mapped('product_id')])
            # do not reserve for tracked products, the user will have manually specified the serial/lot numbers
            if contains_tracked_products:
                picking.action_confirm()
            else:
                picking.action_assign()
            if picking.state == 'assigned':
                self.set_pack_operation_lot(picking)
                if not contains_tracked_products:
                    picking.action_done()

class PosOrderLine(models.Model):
    _inherit = "pos.order.line"

    price_subtotal = fields.Float(compute='_compute_amount_line_all', digits=0, string='Subtotal w/o Tax', store=True)
    price_subtotal_incl = fields.Float(compute='_compute_amount_line_all', digits=0, string='Subtotal', store=True)
    standard_price = fields.Float(related="product_id.standard_price", string='Cost', groups="point_of_sale.group_pos_user", store=True)
    pos_config = fields.Many2one('pos.config', related='order_id.config_id', store=True, string="Point of Sale")
    price_unit_after_discount = fields.Float(string="Unit Price with Discount", compute="compute_unit_price_after_discount")
    
    @api.model
    def fields_view_get(self, view_id=None, view_type='tree', toolbar=False, submenu=False):

        res = super(PosOrderLine,self).fields_view_get(view_id, view_type, toolbar=toolbar, submenu=submenu)
        root = etree.fromstring(res['arch'])
        
        # Хэрвээ Борлуулалтын цэг/Захиалга/Захиалгын мөрүүд цэснээс харж байгаа бол create болон импорт байхгүй байх
        if self._context.get('from_menu') == True:
            root.set('create', 'false')
            root.set('import', 'false')
            root.set('edit', 'false')
            root.set('delete', 'false')
        else:
            root.set('create', 'true')
            root.set('import', 'true')
            root.set('edit', 'false')
            root.set('delete', 'false')
            
        res['arch'] = etree.tostring(root)  
        
        return res

    @api.model
    def create(self, vals):
        # @Override: ИМПОРТ хийж оруулах үед татвараа бараанаасаа авдаг болгов.
        if self._context.get('import_file') and not vals.get('tax_ids') and vals.get('product_id'):
            product = self.env['product.product'].browse(vals['product_id'])
            taxes = product.taxes_id.filtered(lambda r: (r.type_tax_use == 'sale'))
            # Хэрвээ груп татвар тохируулагдсан бол давхар бүртгэсэн бусад дан татваруудыг авахгүй
            grouped_taxes_for_pos = taxes.filtered(lambda r: (r.amount_type == 'group'))
            if grouped_taxes_for_pos and len(grouped_taxes_for_pos) > 0:
                taxes = grouped_taxes_for_pos
            if taxes:
                vals['tax_ids'] = [(6, 0, [x.id for x in taxes])]
        return super(PosOrderLine, self).create(vals)

    @api.depends('price_unit', 'tax_ids', 'qty', 'discount', 'product_id')
    def _compute_amount_line_all(self):
        for line in self:
            fpos = line.order_id.fiscal_position_id
            tax_ids_after_fiscal_position = fpos.map_tax(line.tax_ids, line.product_id, line.order_id.partner_id) if fpos else line.tax_ids
            price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            taxes = tax_ids_after_fiscal_position.compute_all(price, line.order_id.pricelist_id.currency_id, line.qty, product=line.product_id, partner=line.order_id.partner_id)
            line.update({
                'price_subtotal_incl': taxes['total_included'],
                'price_subtotal': taxes['total_excluded'],
            })
            
    def _order_line_fields(self, line):
        if line:
            product = self.env['product.product'].browse(line[2]['product_id'])
            #################### START: Хэрвээ груп татвар тохируулагдсан бол давхар бүртгэсэн бусад дан татваруудыг авахгүй ####################
            taxes = product.taxes_id.filtered(lambda r: (r.type_tax_use == 'sale'))
            grouped_taxes_for_pos = product.taxes_id.filtered(lambda r: (r.amount_type == 'group' and r.type_tax_use == 'sale'))
            if grouped_taxes_for_pos and len(grouped_taxes_for_pos) > 0:
                taxes = grouped_taxes_for_pos
            #################### END: Хэрвээ груп татвар тохируулагдсан бол давхар бүртгэсэн бусад дан татваруудыг авахгүй ####################    
            line[2]['tax_ids'] = [(6, 0, [x.id for x in taxes])]
        return line
    
    @api.onchange('product_id')
    def _onchange_product_id(self):
        if self.product_id:
            if not self.order_id.pricelist_id:
                raise UserError(
                    _('You have to select a pricelist in the sale form !\n'
                      'Please set one before choosing a product.'))
            price = self.order_id.pricelist_id.get_product_price(
                self.product_id, self.qty or 1.0, self.order_id.partner_id, order_date=self.order_id.date_order)
            self._onchange_qty()
            
            #################### START: Хэрвээ груп татвар тохируулагдсан бол давхар бүртгэсэн бусад дан татваруудыг авахгүй ####################
            grouped_taxes_for_pos = self.tax_ids.filtered(lambda r: (not self.company_id or r.company_id == self.company_id) and r.amount_type == 'group' and r.type_tax_use == 'sale')
            if grouped_taxes_for_pos and len(grouped_taxes_for_pos) > 0:
                self.tax_ids = grouped_taxes_for_pos
            else:
                self.tax_ids = self.tax_ids.filtered(lambda r: (not self.company_id or r.company_id == self.company_id))
            #################### END: Хэрвээ груп татвар тохируулагдсан бол давхар бүртгэсэн бусад дан татваруудыг авахгүй ####################
            
            fpos = self.order_id.fiscal_position_id
            tax_ids_after_fiscal_position = fpos.map_tax(self.tax_ids, self.product_id, self.order_id.partner_id) if fpos else self.tax_ids
            self.price_unit = self.env['account.tax']._fix_tax_included_price_company(price, self.product_id.taxes_id, tax_ids_after_fiscal_position, self.company_id)

    @api.depends('price_unit', 'discount')
    def compute_unit_price_after_discount(self):
        for obj in self:
            obj.price_unit_after_discount = obj.price_unit * (1.0 - (obj.discount / 100.0))


class PosBillReprintHistory(models.TransientModel):
    _name = "pos.bill.reprint.history"

    reprinted_order = fields.Many2one('pos.order', string="Reprinted order", default=lambda self: self.env['pos.order'].search([('id', '=', self._context.get('active_id'))]))
    reprinted_user = fields.Many2one('res.users', 'Reprinted User', default=lambda self: self.env.user)
    reprinted_date = fields.Date(string="Reprinted date", default=lambda self: fields.datetime.now())
    reprinted_reason = fields.Char(string="Cancelled reason", required=True)

    @api.multi
    def reprint(self):
        data = {
            "pos_order_id" : self.reprinted_order.id
        }
        return self.env['report'].get_action([self.id], 'l10n_mn_point_of_sale.reprint_ebarimt_receipt', data=data)
