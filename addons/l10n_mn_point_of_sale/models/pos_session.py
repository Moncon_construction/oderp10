# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime

class PosSession(models.Model):
    _inherit = 'pos.session'

    @api.one
    def count_pos_order(self):
        self.count_orders = self.env['pos.order'].search_count([('session_id','=',self.id)])

    @api.one
    def compute_filling_basket(self):
        total = sum([statement.total_entry_encoding for statement in self.statement_ids])
        if self.count_orders > 0:
            self.count_filling_basket = float(total / self.count_orders)
        else:
            self.count_filling_basket = 0

    @api.one
    def _amount_balance(self):
        self.total_balance_start = sum([statement.balance_start for statement in self.statement_ids])
        self.total_balance_end_income = sum([statement.balance_end_income for statement in self.statement_ids])
        self.total_balance_end_outcome = sum([statement.balance_end_outcome for statement in self.statement_ids])
        self.total_balance_end_real = sum([statement.balance_end_real for statement in self.statement_ids])

    @api.one
    @api.depends('total_balance_start', 'total_balance_end_real')
    def _amount_balance_diff(self):
        self.total_balance_diff = self.total_balance_start - self.total_balance_end_real

    count_orders = fields.Integer(compute='count_pos_order', string='Total customers')
    count_filling_basket = fields.Float(compute='compute_filling_basket', string='Filling Backet')
    total_balance_start = fields.Monetary(string='Total Balance Start', readonly=True, compute='_amount_balance')
    total_balance_end_income = fields.Monetary(string='Total Income', readonly=True, compute="_amount_balance")
    total_balance_end_outcome = fields.Monetary(string='Total Outcome', readonly=True, compute="_amount_balance")
    total_balance_end_real = fields.Monetary(string='Total Balance End', readonly=True, compute="_amount_balance")
    total_balance_diff = fields.Monetary(string='Diffrence', readonly=True, compute="_amount_balance_diff")

    total_balance_start_store = fields.Monetary(related='total_balance_start', string='Total Balance Start', store=True)
    total_balance_end_real_store = fields.Monetary(related='total_balance_end_real', string='Total Balance End', store=True)
    total_balance_diff_store = fields.Monetary(related='total_balance_diff', string='Diffrence', store=True)

    closing_user_id = fields.Many2one('res.users','Closing user')
    description = fields.Text('Description')
    start_day = fields.Char('Day')
    stock_location_id = fields.Many2one(related="config_id.stock_location_id", string='Stock Location', store=True)
    banknote_report = fields.Binary(string='Banknote Report')

    #Override
    @api.depends('config_id', 'statement_ids')
    def _compute_cash_all(self):
        for session in self:
            session.cash_journal_id = session.cash_register_id = session.cash_control = False
            if session.config_id.cash_control:
                for statement in session.statement_ids:
                    if statement.journal_id.type == 'cash':
                        session.cash_control = True
                        session.cash_journal_id = statement.journal_id.id
                        session.cash_register_id = statement.id
                if not session.cash_control and session.state != 'closed':
                    raise UserError(_("Cash control can only be applied to cash journals."))
            #BEGIN
            #Касс хяналт сонгоогүй үед бэлэн мөнгөны төлбөрийн арга тооцох
            elif not session.config_id.cash_control:
                for statement in session.statement_ids:
                    if statement.journal_id.type == 'cash':
                        session.cash_register_id = statement.id
            #END

    @api.constrains('start_at')
    def _compute_start_day(self):
        for session in self:
            if session.start_at:
                session.start_day = str(datetime.strptime(session.start_at, '%Y-%m-%d %H:%M:%S').date())

    # Чектэй байх үед сэшн хаах үед үлдэгдэл шалгах
    @api.multi
    def action_pos_session_closing_control_window_check(self):
        # Банкны хуулгын эхний эцсийн үлдэгдлийг зөв тооцоолдог болгох
        for session in self:
            for statement in session.statement_ids:
                balance_end_real = statement.balance_start + statement.balance_end_income - statement.balance_end_outcome
                statement.sudo().write({'balance_end_real': balance_end_real})
        if self.user_id.company_id.check_residuals_when_closing == 'check':
            errors = []
            for session in self:
                orders = self.env['pos.order'].search([('session_id', '=', session.id)])
                # BEGIN picking нь хийгдсэн төлөвтэй бол шууд сешн хаагдана
                if orders:
                    for order in orders:
                        picking_state = order.picking_id.state or False
                        if order.picking_id.state == 'done':
                            # END
                            cash_statement = session.statement_ids.search([('journal_id.type', '=', 'cash')])
                            if cash_statement and len(cash_statement) > 0:
                                journal = cash_statement[0].journal_id
                                if journal.amount_authorized_diff < abs(
                                        session.total_balance_diff) and not self.user_id.has_group(
                                    'point_of_sale.group_pos_manager'):
                                    raise UserError(_(
                                        "Your right does not reach to close session named '%s'!!!\n\nBecause this session's journal named '%s''s 'AUTHORIZED AMOUNT DIFFERENCE' value is lesser                                        than this session's 'TOTAL BALANCE END AMOUNT'.\n\nIf you want to close this session please contact with user who has 'group_pos_manager' role...") % (
                                                        session.name, journal.name))
                        # BEGIN  хийгдсэнээс бусад төлөвь хаагдахгүй
                        elif picking_state not in ('done', 'cancel'):
                            errors.append(_(
                                "(%s) Document source ,(%s) There is no residual value of the document for preparing.!!!") % (
                                    order.pos_reference, order.picking_id.name))

                    if len(errors) > 0:
                        error_message = _('There are %s errors found.\n') % len(errors)
                        for index in range(len(errors)):
                            error_message += (str(index + 1) + ":\t" + errors[index] + "\n")
                        raise ValidationError(error_message)
                    # END
            return {
                'type': 'ir.actions.act_window',
                'name': _('Close pos session'),
                'res_model': 'close.pos.session.wizard',
                'view_mode': 'form',
                'target': 'new',
                'context': {
                    'default_ids': self.ids,
                }
            }
            # Чекгүй байх үед сэшн хаах үед үлдэгдэл шалгахгүй
        elif self.user_id.company_id.check_residuals_when_closing == 'not_check':
            for session in self:
                cash_statement = session.statement_ids.search([('journal_id.type', '=', 'cash')])
                if cash_statement and len(cash_statement) > 0:
                    journal = cash_statement[0].journal_id
                    if journal.amount_authorized_diff < abs(session.total_balance_diff) and not self.user_id.has_group(
                            'point_of_sale.group_pos_manager'):
                        raise UserError(_(
                            "Your right does not reach to close session named '%s'!!!\n\nBecause this session's journal named '%s''s 'AUTHORIZED AMOUNT DIFFERENCE' value is lesser                                        than this session's 'TOTAL BALANCE END AMOUNT'.\n\nIf you want to close this session please contact with user who has 'group_pos_manager' role...")
                                        % (session.name, journal.name))

            return {
                'type': 'ir.actions.act_window',
                'name': _('Close pos session'),
                'res_model': 'close.pos.session.wizard',
                'view_mode': 'form',
                'target': 'new',
                'context': {
                    'default_ids': self.ids,
                }
            }


    @api.multi
    def button_journal_entries(self):
        self.ensure_one()
        for session in self:
            order = self.env['pos.order'].search([('session_id', '=', session.id),('account_move', '!=', False)], limit=1)
            move_id = order.account_move
        return {
            "type": "ir.actions.act_window",
            "res_model": "account.move",
            "views": [[False, "form"]],
            "res_id": move_id.id,
            "context": {"create": False, "show_sale": True},
        }

    @api.multi
    def action_all_stock_picking(self):
        picking_ids = []
        for session in self:
            orders = self.env['pos.order'].search([('session_id', '=', session.id)])
            if orders:
                for order in orders:
                    picking_ids.append(order.picking_id.id)
        return {
            'name': _('Stock Picking'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'stock.picking',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', picking_ids)],
        }

    @api.multi
    def button_stock_journal_entries(self):
        mlines = []
        for session in self:
            orders = self.env['pos.order'].search([('session_id', '=', session.id)])
            if orders:
                for order in orders:
                    if order.picking_id.account_move_id:
                        mlines = self.env['account.move.line'].search([('move_id', '=', order.picking_id.account_move_id.id)]).ids
        action = self.env.ref('l10n_mn_point_of_sale.act_account_moves_pos_session').read()[0]
        action['domain'] = [('id', 'in', mlines)]
        action['context'] = {
            'search_default_group_account_account': 1,
        }
        return action

    @api.multi
    def action_pos_session_close(self):
        # Close CashBox
        for session in self:
            if not session.cash_register_id.cashbox_end_id and session.state == 'closing_control' and session.config_id.cash_control:
                raise UserError(_('Must be configured ending cashbox!'))
        return super(PosSession,self).action_pos_session_close()

    @api.multi
    def action_view_invoice(self):
        invoice_ids = []
        for session in self:
            orders = self.env['pos.order'].search([('session_id', '=', session.id)])
            if orders:
                for order in orders:
                    invoice_ids.append(order.invoice_id.id)
        return {
            'name': _('Customer Invoice'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.invoice',
            'view_id': False,
            'context': "{'type':'out_invoice'}",
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', invoice_ids)],
        }