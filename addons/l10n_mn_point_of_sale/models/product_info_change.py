# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, _
from odoo.exceptions import UserError

class ProductInfoChange(models.TransientModel):
    _inherit = 'product.info.change'

    available_in_pos = fields.Boolean(string='Available in the Point of Sale', help='Check if you want this product to appear in the Point of Sale', default=False)
    to_weight = fields.Boolean(string='To Weigh With Scale', help="Check if the product should be weighted using the hardware scale integration")
    pos_categ_id = fields.Many2one(
        'pos.category', string='Point of Sale Category',
        help="Those categories are used to group similar products for point of sale.")
    change_available_in_pos = fields.Boolean(string='To Change The Value of Available in the Point of Sale')
    
    @api.multi
    def product_info_change(self):
        context = dict(self._context or {})
        product_ids = self.env['product.template'].browse(context.get('active_ids'))
        if self.change_available_in_pos:
            for a in product_ids:
                a.available_in_pos = self.available_in_pos
                a.to_weight = self.to_weight
                a.pos_categ_id = self.pos_categ_id
        
        return super(ProductInfoChange, self).product_info_change()

