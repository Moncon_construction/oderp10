# -*- coding: utf-8 -*-
from odoo import api, fields, models


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    loose_goods_qty = fields.Integer('Loose Goods QTY', track_visibility='onchange')
    pos_categ_id = fields.Many2one(track_visibility='onchange')
