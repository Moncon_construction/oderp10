from odoo import api, fields, models, tools, _


class Company(models.Model):
    _inherit = 'res.company'

    non_create_move_for_cash_change = fields.Boolean('Non create account move for cash change from POS')
    check_vatpsp_service = fields.Boolean('Check Vatpsp Service', help='Checks if the Vatpsp service is working '
                                                                       'correctly, and if the service is not working'
                                                                       'correctly POS will not work',
                                          default=True)

    check_residuals_when_closing = fields.Selection([
        ('check', _('Check')),
        ('not_check', _('Not check'))
    ], default='not_check')

    force_assign_when_insufficient_residual = fields.Selection([
        ('check', _('Check')),
        ('not_check', _('Not check'))
    ], default='check')
    pos_header = fields.Text(string='POS Header')
    pos_footer = fields.Text(string='POS Footer')
    pos_logo = fields.Binary(string="POS Logo")
