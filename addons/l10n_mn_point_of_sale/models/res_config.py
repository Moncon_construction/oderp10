# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _


class PosConfiguration(models.TransientModel):
    _inherit = 'pos.config.settings'

    company_id = fields.Many2one('res.company', string='Company', required=True,
                                 default=lambda self: self.env.user.company_id)
    non_create_move_for_cash_change = fields.Boolean(related='company_id.non_create_move_for_cash_change', string="Cash change",
                                                     help="Non create account move for cash change from POS")

    module_l10n_mn_point_of_sale_hide_amount = fields.Selection([
        (0, "The salesperson sees its daily sales amount"),
        (1, "Salesperson does not show sales of POS")
    ], string="Salesperson permission")
    check_vatpsp_service = fields.Boolean(related='company_id.check_vatpsp_service',
                                          string='Check Vatpsp Service',
                                          help='Checks if the Vatpsp service is working correctly, and if the service '
                                               'is not working correctly POS will not work')

    module_l10n_mn_point_of_sale_remove_goods = fields.Selection([
        (0, "Salesperson will be allowed to remove goods from POS screens."),
        (1, "Salesperson will not be allowed to remove goods from POS screens.")
    ], string="Right to deduct")

    check_residuals_when_closing = fields.Selection([
        ('check', _('Check')),
        ('not_check', _('not Check'))
    ], _('Check Residuals When Closing'), default='not_check', related='company_id.check_residuals_when_closing')

    force_assign_when_insufficient_residual = fields.Selection([
        ('check', _('Check')),
        ('not_check', _('Not check'))
    ], _('Force Assign When Insufficient Residual'), default='check', related='company_id.force_assign_when_insufficient_residual')
    pos_header = fields.Text(string='Header', related='company_id.pos_header')
    pos_footer = fields.Text(string='Footer', related='company_id.pos_footer')
    pos_logo = fields.Binary(string="Logo", related='company_id.pos_logo')
