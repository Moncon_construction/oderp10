# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class StockMove(models.Model):
    _inherit = "stock.move"

    order_line_id = fields.Many2one('pos.order.line', string='Pos Order Line')
    
    @api.depends('state', 'write_date')
    def _move_type(self):
        res = super(StockMove, self)._move_type()
        for move in self:
            # борлуулалтын цэгийн зарлага
            if move.location_id.usage == 'internal' and move.location_dest_id.usage == 'customer':
                if move.picking_id.name:
                    if self.env['pos.order'].search([('picking_id', '=', move.picking_id.id)]):
                        move.move_type = 'pos'
            # борлуулалтын цэгийн буцаалтын орлого
            elif move.location_id.usage == 'customer' and move.location_dest_id.usage == 'internal':
                if move.picking_id.name and move.origin_returned_move_id:
                    if self.env['pos.order'].search([('picking_id', '=', move.origin_returned_move_id.picking_id.id)]):
                        move.move_type = 'pos_return'
        return res

    move_type = fields.Selection(selection_add=[('pos', 'Point of Sale'), ('pos_return', 'Return of Point of Sale')])
