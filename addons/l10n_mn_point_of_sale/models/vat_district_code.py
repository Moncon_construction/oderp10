# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class VatDistrictCode(models.Model):
    _name = 'vat.district.code'
    _description = 'Vat/District'

    name = fields.Char(string='Province/District name', required=True, translate=True, help='Full name of Province/District')
    code = fields.Char("Province/District code")

    @api.multi
    @api.constrains('name', 'code')
    def _check_district_name_code(self):
        for record in self:
            res_district = self.env['vat.district.code'].search([('code', '=', record.code), ('name', '=', record.name)])
            if len(res_district) > 1:
                raise ValidationError(_("This district already exists"))