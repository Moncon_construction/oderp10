# -*- coding: utf-8 -*-
from odoo import api, models
from datetime import datetime
from odoo.addons.l10n_mn_report.models.report_helper import verbose_numeric, comma_me, convert_curr  # @UnresolvedImport


class CashBoxOutBanknote(models.AbstractModel):

    _name = 'report.l10n_mn_point_of_sale.cash_box_out_banknote'

    @api.model
    def render_html(self, ids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('l10n_mn_point_of_sale.cash_box_out_banknote')
        docargs = {
            'doc_ids': ids,
            'doc_model': report.model,
            'name':data['name'],
            'company':data['company'],
            'pos':data['pos'],
            'pos_user':data['pos_user'],
            'date':datetime.now(),
            'ten': data['ten'],
            'ten_amount': data['ten_amount'],
            'twenty': data['twenty'],
            'twenty_amount': data['twenty_amount'],
            'fifty': data['fifty'],
            'fifty_amount': data['fifty_amount'],
            'hundred' : data['hundred'],
            'hundred_amount' : data['hundred_amount'],
            'five_hundred' : data['five_hundred'],
            'five_hundred_amount' : data['five_hundred_amount'],
            'thousand' : data['thousand'],
            'thousand_amount' : data['thousand_amount'],
            'five_thousand' : data['five_thousand'],
            'five_thousand_amount' : data['five_thousand_amount'],
            'ten_thousand' : data['ten_thousand'],
            'ten_thousand_amount' : data['ten_thousand_amount'],
            'twenty_thousand' : data['twenty_thousand'],
            'twenty_thousand_amount' : data['twenty_thousand_amount'],  
            'total_amount':int(data['total_amount']),
            'data_report_margin_top': 5,
            'data_report_header_spacing': 5,
        }
        return report_obj.render('l10n_mn_point_of_sale.cash_box_out_banknote', docargs)
