# -*- coding: utf-8 -*-
from odoo import api, models, _
from odoo.exceptions import UserError


class ReportStockPicking(models.AbstractModel):
    _name = 'report.l10n_mn_point_of_sale.closing_report'

    @api.multi
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        session_obj = self.env['pos.session']
        order_obj = self.env['pos.order']
        sessions = session_obj.browse(docids)
        products = {}
        totals = {}
        journals = {}
        journal_totals = {}
        incomes_from_cash_in = {}
        outcomes_from_cash_out = {}
        for session in sessions:
            if session.state != 'closed':
                raise UserError(_("The session %s is not closed" % session.name))
            orders = order_obj.search([('session_id', '=', session.id)])
            if not orders:
                raise UserError(_("The session %s has no orders" % session.name))
            else:
                for order in orders:
                    if not order.lines:
                        raise UserError(_("The order %s has no lines" % order.name))
            if session.statement_ids:
                statement_lines = 0
                for statement in session.statement_ids:
                    statement_lines += len(statement.line_ids)
                if statement_lines == 0:
                    raise UserError(_("The session %s has statement  lines" % session.name))
            self._cr.execute("SELECT pp.barcode, pt.name, sum(pol.qty), avg(pol.price_unit), sum(pol.qty) * "
                             "avg(pol.price_unit) as total FROM pos_order_line pol left join product_product pp on "
                             "pol.product_id = pp.id left join product_template pt on pt.id = pp.product_tmpl_id WHERE "
                             "pol.order_id in (select id from pos_order where session_id = %s) group by pol.product_id,"
                             " pt.name, pp.barcode" % session.id)
            product = self._cr.fetchall()
            products[session.id] = product
            self._cr.execute("SELECT sum(pol.qty), sum(pol.qty) * avg(pol.price_unit) as total FROM pos_order_line pol "
                             "left join product_product pp on pol.product_id = pp.id left join product_template pt on "
                             "pt.id = pp.product_tmpl_id WHERE pol.order_id in (select id from pos_order where "
                             "session_id = %s)" % session.id)
            total = self._cr.fetchall()
            totals[session.id] = total

            journals[session.id] = []
            journal_totals[session.id] = []
            journal_total = [0, 0, 0, 0, 0, 0, 0, 0]
            for statement in session.statement_ids:
                journal = [statement.journal_id.name, statement.name, statement.balance_start]
                journal_total[0] += journal[2]
                self._cr.execute("select sum(amount) from account_bank_statement_line where statement_id = %s and "
                                 "pos_statement_id is not null and amount > 0" % statement.id)
                sale_amount = self._cr.fetchone()
                journal.append(sale_amount[0] if sale_amount[0] else 0)
                journal_total[1] += journal[3] if journal[3] else 0
                if statement.journal_id.type == 'cash':
                    self._cr.execute("select sum(amount) from account_bank_statement_line where statement_id = %s and "
                                     "pos_statement_id is null and amount > 0" % statement.id)
                    money_put_in = self._cr.fetchone()
                    money_put_in = 0 if not money_put_in[0] else money_put_in[0]
                    journal.append(money_put_in)
                    journal_total[2] += money_put_in
                    self._cr.execute("select name, amount from account_bank_statement_line where statement_id = %s and "
                                     "pos_statement_id is null and amount > 0" % statement.id)
                    incomes_from_cash_in[session.id] = self._cr.fetchall()
                    self._cr.execute("select name, amount from account_bank_statement_line where statement_id = %s and "
                                     "pos_statement_id is null and amount < 0" % statement.id)
                    outcomes_from_cash_out[session.id] = self._cr.fetchall()
                else:
                    journal.append(0)
                # Total income
                if journal[3]:
                    journal.append(journal[3] + journal[4])
                    journal_total[3] += journal[3] + journal[4]
                else:
                    journal.append(journal[4])
                    journal_total[3] += journal[4]
                # Refund
                self._cr.execute("select sum(amount) from account_bank_statement_line where statement_id = %s and "
                                 "pos_statement_id is not null and amount < 0" % statement.id)
                refund_amount = self._cr.fetchone()
                refund_amount = 0 if not refund_amount[0] else refund_amount[0]
                journal.append(refund_amount)
                journal_total[4] += refund_amount
                # Money get out
                if statement.journal_id.type == 'cash':
                    self._cr.execute("select sum(amount) from account_bank_statement_line where statement_id = %s and "
                                     "pos_statement_id is null and amount < 0" % statement.id)
                    money_get_out = self._cr.fetchone()
                    money_get_out = 0 if not money_get_out[0] else money_get_out[0]
                    journal.append(money_get_out)
                    journal_total[5] += money_get_out
                else:
                    journal.append(0)
                # Total outcome
                journal.append(journal[6] + journal[7])
                journal_total[6] += journal[6] + journal[7]
                # End Balance
                journal.append(journal[2] + journal[5] - journal[8])
                journal_total[7] += journal[2] + journal[5] - journal[8]
                journals[session.id].append(journal)
                journal_totals[session.id].append(journal_total)

        docargs = {
            'docs': sessions,
            'products': products,
            'totals': totals,
            'journals': journals,
            'journal_totals': journal_totals,
            'incomes_from_cash_in': incomes_from_cash_in,
            'outcomes_from_cash_out': outcomes_from_cash_out
        }
        return report_obj.render('l10n_mn_point_of_sale.closing_report', docargs)
