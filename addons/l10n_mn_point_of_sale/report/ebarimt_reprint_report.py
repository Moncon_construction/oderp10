# -*- coding: utf-8 -*-
from odoo import api, models

from odoo.addons.l10n_mn_report.models.report_helper import verbose_numeric, comma_me, convert_curr  # @UnresolvedImport


class ParticularReport(models.AbstractModel):

    _name = 'report.l10n_mn_point_of_sale.reprint_ebarimt_receipt'

    @api.model
    def render_html(self, ids, data=None):
        pos_order = self.env['pos.order'].search([('id', '=', data['pos_order_id'])])

        report_obj = self.env['report']
        report = report_obj._get_report_from_name('l10n_mn_point_of_sale.reprint_ebarimt_receipt')
        docargs = {
            'doc_ids': ids,
            'doc_model': report.model,
            'docs': pos_order,
            'data_report_margin_top': 5,
            'data_report_header_spacing': 5
        }

        return report_obj.render('l10n_mn_point_of_sale.reprint_ebarimt_receipt', docargs)
