odoo.define('l10n_mn_point_of_sale.DB', function (require) {
"use strict";
    
    var PosDB = require('point_of_sale.DB');
    PosDB.include({
        init: function(options){
            this._super(options);
            
            this.vat_code_by_id = {};
            this.tax_type_by_id = {};
            this.district_by_id = {};
        },
        /* Татварын Бараа үйлчилгээний ангилалын код */
        add_vat_codes: function(vat_codes){
        	for(var i=0, len = vat_codes.length; i < len; i++){
                this.vat_code_by_id[vat_codes[i].id] = vat_codes[i];
            }
        },
        /* Татварын төрөл нэмэх НӨАТ тооцох бараа, НӨАТ чөлөөлөгдсөн, 0% тай */
        add_tax_category_types: function(tax_types){
        	for(var i=0, len = tax_types.length; i < len; i++){
                this.tax_type_by_id[tax_types[i].id] = tax_types[i];
            }
        },
        /* Татварын төрөл нэмэх НӨАТ тооцох бараа, НӨАТ чөлөөлөгдсөн, 0% тай */
        add_districts: function(district){
        	for(var i=0, len = district.length; i < len; i++){
                this.district_by_id[district[i].id] = district[i];
            }
        },
        
        get_vat_category_code_id: function(category_id){
       		var vat_category  = this.vat_code_by_id[category_id];
	        return vat_category;
	    },    
        get_vat_category_code: function(category_id){
       		var vat_category_ids  = this.vat_code_by_id[category_id];
	        var type_id = this.get_tax_type(vat_category_ids.type_id[0]);
	        var type_code = type_id.code;
	        return type_code;
	    },
	    get_tax_type: function(type_id){
       		var type_ids  = this.tax_type_by_id[type_id];
	        return type_ids;
	    },
	    
	    get_district: function(district_id){
       		var district  = this.district_by_id[district_id];
	        return district;
	    }
    });
    return PosDB;
});