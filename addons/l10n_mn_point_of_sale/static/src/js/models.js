odoo.define('l10n_mn_point_of_sale.models', function (require) {
    "use strict";
    var utils = require('web.utils');
    var round_pr = utils.round_precision;
    var round_di = utils.round_decimals;
    var formats = require('web.formats');
    var models = require('point_of_sale.models');
    var _super_posmodel = models.PosModel.prototype;
    var _super_order = models.Order.prototype;

    models.PosModel = models.PosModel.extend({

        initialize: function (session, attributes){
            // New code
            var partner_model = _.find(this.models, function(model){
                return model.model === 'product.product';
            });
            partner_model.fields.push('vat_category_code_id');
            partner_model.fields.push('loose_goods_qty');
            var company_model = _.find(this.models, function(model){
                return model.model === 'res.company';
            });
            company_model.fields.push('check_vatpsp_service');
            company_model.fields.push('pos_logo');

            // Add 'name_for_pos' field to account.tax  
            this.models.push({
            	model:  'account.tax',
	            fields: ['name', 'name_for_pos', 'amount', 'price_include', 'include_base_amount', 'amount_type', 'children_tax_ids',  'choose_taxes', 'python_compute'],
	            domain: function(self) {return [['company_id', '=', self.company && self.company.id || false]]},
	            loaded: function(self, taxes){
	                self.taxes = taxes;
	                self.taxes_by_id = {};
	                _.each(taxes, function(tax){
	                    self.taxes_by_id[tax.id] = tax;
	                });
	                _.each(self.taxes_by_id, function(tax) {
	                    tax.children_tax_ids = _.map(tax.children_tax_ids, function (child_tax_id) {
	                        return self.taxes_by_id[child_tax_id];
	                    });
	                });
	            },
            },
            );
            /* Татварын Бараа үйлчилгээний ангилалын код */
            this.models.push({
            	model:  'vat.category.code',
	            fields: ['name', 'code', 'type_id'],
	            domain: null,
	            loaded: function(self, vat_code){
                    self.db.add_vat_codes(vat_code);
	                }
	            },
            );
            /* Татварын Бараа үйлчилгээний ангилалын кодын татварын төрөл */
            this.models.push({
            	model:  'category.type',
	            fields: ['id', 'name', 'code',],
	            domain: null,
	            loaded: function(self, tax_type){
                    self.db.add_tax_category_types(tax_type);
	                }
	            },
            );
            /* Аймаг дүүргийн код */
            this.models.push({
            	model:  'vat.district.code',
	            fields: ['id', 'name', 'code',],
	            domain: null,
	            loaded: function(self, district){
                    self.db.add_districts(district);
	                }
	            },
            );
            // Inheritance
            return _super_posmodel.initialize.call(this, session, attributes);
        },

    });

    var OrderTaxCollection = Backbone.Collection.extend({
        model: models.EtaxBills,
    });
    models.Order = models.Order.extend({
        initialize: function(attributes, options){
            this.ebarimt = {};
            this.etaxbills = new OrderTaxCollection();
            // // Inheritance
            return _super_order.initialize.call(this, attributes, options);
        },
         set_ebarimt_data: function(key, value){
            if(arguments.length === 2){
                this.ebarimt[key] = value;
            }else if(arguments.length === 1){
                for(var key in arguments[0]){
                    this.ebarimt[key] = arguments[0][key];
                }
            }
        },
        get_ebarimt_data: function(key){
            return this.ebarimt[key];
        },
        get_gross_amount: function(){
        	var gross_amount = 0;
            var lines = this.get_orderlines();
            if (lines){
            	for (var i = 0; i < lines.length; i++) {
            		gross_amount += lines[i].get_gross_amount();
                }
            }
            return gross_amount;
        },
        get_gross_amount_after_discount: function(){
            return this.get_gross_amount() - this.get_total_discount();
        },
        get_total_with_tax: function() {
            return this.get_total_without_tax() + this.get_total_vat()+ this.get_total_cityTax();
        },
        /* Нийт НӨАТ тооцоох */
        get_total_vat: function(){
	    	var productVat = 0;
	    	var lines = this.get_orderlines();
            if (lines){
            	for (var i = 0; i < lines.length; i++) {
            		productVat += lines[i].get_all_prices().tax;
                }
            }
	    	return productVat;
	    },
	    /* Нийт НХАТ тооцоох */
	    get_total_cityTax: function(){
	    	var cityTax = 0;
	    	var lines = this.get_orderlines();
            if (lines){
            	for (var i = 0; i < lines.length; i++) {
            		cityTax += lines[i].get_all_prices().cityTax;
                }
            }
	    	return cityTax;
	    },
	    /* Татварын төрөл авах */
	    get_vat_category_code: function(){
	    	var tax_type = '1';
	    	var lines = this.get_orderlines();
            if (lines){
            	for (var i = 0; i < lines.length; i++) {
            		var tax_type_id = lines[i].get_product().vat_category_code_id;
            		tax_type = this.pos.db.get_vat_category_code(tax_type_id[0]);
                }
            }
	    	return tax_type;
	    },
        get_tax_details: function(){
            var details = {};
            var fulldetails = [];

            this.orderlines.each(function(line){
                var ldetails = line.get_tax_details();
                for(var id in ldetails){
                    if(ldetails.hasOwnProperty(id)){
                        details[id] = (details[id] || 0) + ldetails[id];
                    }
                }
            });
            
            for(var id in details){
                if(details.hasOwnProperty(id) && this.pos.taxes_by_id[id]){
                    fulldetails.push({amount: details[id], tax: this.pos.taxes_by_id[id], name_for_pos: this.pos.taxes_by_id[id].name_for_pos});
                }
            }

            return fulldetails;
        },
        get_change: function(paymentline) {
        	// @Override: l10n_mn_pharmacy_pos, l10n_mn_pos_loyalty_card 2 дээр удамшуулж ашиглахын тулд дахин тодорхойлов.
        	// Дахин тодорхойлсон шалтгаан: Math.max(0,change) функциар хасах хариултыг тэглээд байсан тул удамшилд уг функцыг дуудахаар тооцооллын алдаа гараад байсан бөгөөд
        	//  	get_signed_change() нэмэлт функцын тусламжтай уг тооцооллын алдаа гарахааргүй болгож өөрчлөв.
        	var change = this.get_signed_change(paymentline);
            return round_pr(Math.max(0,change), this.pos.currency.rounding);
        },
        get_signed_change: function(paymentline) {
        	// Хариулт дүнг буцаадаг функц: Core-н хариулт буцаах get_change() функцтай үндсэн хариулт бодох арга нь ижил бөгөөд гол ялгаа нь: 
        	// 		Тооцоолсон утгыг Math.max(0,change) функц ашиглан хасах дүнг 0-лэлгүй буцаадгаараа ондоо юм.
            if (!paymentline) {
                var change = this.get_total_paid() - this.get_total_with_tax();
            } else {
                var change = -this.get_total_with_tax(); 
                var lines  = this.paymentlines.models;
                for (var i = 0; i < lines.length; i++) {
                    change += lines[i].get_amount();
                    if (lines[i] === paymentline) {
                        break;
                    }
                }
            }
            return round_pr(change, this.pos.currency.rounding);
        },

        add_taxbill: function(line){
            this.assert_editable();
            line.order = this;
            this.etaxbills.add(line);
        },

        get_taxbill: function(id){
            var orderlines = this.etaxbills.models;
            for(var i = 0; i < etaxbills.length; i++){
                if(etaxbills[i].id === id){
                    return etaxbills[i];
                }
            }
            return null;
        },

        get_taxbills: function(){
            return this.etaxbills.models;
        },

        is_paid: function(){
            var client = this.attributes.client;
            if (client && this.to_invoice)
                return true;
            return this.get_due() <= 0;
        },
        export_as_JSON: function() {
            var result = _super_order.export_as_JSON.call(this);
            result['lottery'] = this.get_ebarimt_data('lottery');
            result['billId'] = this.get_ebarimt_data('billId');
            result['qrData'] = this.get_ebarimt_data('qrData');
            result['amount'] = this.get_ebarimt_data('amount');
            result['billType'] = this.get_ebarimt_data('billType');
            result['customerNo'] = this.get_ebarimt_data('customerNo');
            result['companyName'] = this.get_ebarimt_data('companyName');
            
            result['ebarimt_discount_amount'] = this.get_ebarimt_data('discount_amount');
            result['ebarimt_total_amount_with_tax'] = this.get_ebarimt_data('total_amount_with_tax');
            result['ebarimt_total_amount_without_tax'] = this.get_ebarimt_data('total_amount_without_tax');
            result['ebarimt_vat_amount'] = this.get_ebarimt_data('vat_amount');
            result['ebarimt_city_tax_amount'] = this.get_ebarimt_data('city_tax_amount');
            result['ebarimt_register_amount'] = this.get_ebarimt_data('register_amount');
            var etaxBills = [];
            this.etaxbills.each(_.bind( function(item) {
                return etaxBills.push([0, 0, item.export_as_JSON()]);
            }, this));
            result['etax_bills'] = etaxBills;
            return result;
        }
    });

    models.EtaxBills = Backbone.Model.extend({
        initialize: function(attr,options){
            this.pos   = options.pos;
            this.order = options.order;
            if (options.json) {
                this.init_from_JSON(options.json);
                return;
            }
            this.pos_order_id;
            this.ebarimt_register_amount;
            this.receipt_type;
            this.ddtd_number;
            this.lottery_number;
            this.qrData;
            this.customer_register;
            this.customer_name;
        },

        export_as_JSON: function(){
            return {
                receipt_type: this.receipt_type,
                ddtd_number: this.ddtd_number,
                lottery_number: this.lottery_number,
                qrData: this.qrData,
                ebarimt_register_amount: this.ebarimt_register_amount,
                customer_register: this.customer_register,
                customer_name: this.customer_name
            };
        },
    });

    models.Orderline = models.Orderline.extend({
    	set_quantity: function(quantity){
            this.order.assert_editable();
            if(quantity === 'remove'){
                this.order.remove_orderline(this);
                return;
            }else{
                var quant = parseFloat(quantity) || 0;
                var unit = this.get_unit();
                if(unit){
                    if (unit.rounding) {
                        this.quantity    = round_pr(quant, unit.rounding);
                        var decimals = this.pos.dp['Product Unit of Measure'];
                        this.quantity = round_di(this.quantity, decimals)
                       if (Number.isInteger(this.quantity)){
                            this.quantityStr = '' + this.quantity;
                       }else{
                       this.quantityStr = formats.format_value(this.quantity, { type: 'float', digits: [69, decimals]});
                       }
                    } else {
                        this.quantity    = round_pr(quant, 1);
                        this.quantityStr = this.quantity.toFixed(0);
                    }
                }else{
                    this.quantity    = quant;
                    this.quantityStr = '' + this.quantity;
                }
            }
            this.trigger('change',this);
        },
        get_unit_price_after_discount: function(){
        	return this.get_unit_price() * (1.0 - (this.get_discount() / 100.0));
        },
        get_discount_amount: function(){
        	var rounding = this.pos.currency.rounding;
            return round_pr((this.get_unit_price() - this.get_unit_price_after_discount()) * this.get_quantity(), rounding);
        },
        get_gross_amount: function(){
        	var rounding = this.pos.currency.rounding;
            return round_pr(this.get_unit_price() * this.get_quantity(), rounding);
        },
        _compute_all: function(tax, base_amount, quantity) {
            if (tax.amount_type === 'fixed') {
                var sign_base_amount = base_amount >= 0 ? 1 : -1;
                return (Math.abs(tax.amount) * sign_base_amount) * quantity;
            }
            if ((tax.amount_type === 'percent' && !tax.price_include) || (tax.amount_type === 'division' && tax.price_include)){
                return base_amount * tax.amount / 100;
            }
            if (tax.amount_type === 'percent' && tax.price_include){
                return base_amount - (base_amount / (1 + tax.amount / 100));
            }
            if (tax.amount_type === 'division' && !tax.price_include) {
                return base_amount / (1 - tax.amount / 100) - base_amount;
            }
            if (tax.amount_type === 'code' && !tax.price_include) {
            	var price_unit = base_amount
            	var formula_text = tax.python_compute;
            	var entryArray = formula_text.split(' = ');
                return eval(entryArray[1]);
            }
            return false;
        },
        
        compute_all: function(taxes, price_unit, quantity, currency_rounding, no_map_tax) {
            var self = this;
            var list_taxes = [];
            var currency_rounding_bak = currency_rounding;
            if (this.pos.company.tax_calculation_rounding_method == "round_globally"){
               currency_rounding = currency_rounding * 0.00001;
            }
            var total_excluded = round_pr(price_unit * quantity, currency_rounding);
            var total_included = total_excluded;
            var base = total_excluded;
            _(taxes).each(function(tax) {
                if (!no_map_tax){
                    tax = self._map_tax_fiscal_position(tax);
                }
                if (!tax){
                    return;
                }
                if (tax.amount_type === 'group'){
                    var ret = self.compute_all(tax.children_tax_ids, price_unit, quantity, currency_rounding);
                    total_excluded = ret.total_excluded;
                    base = ret.total_excluded;
                    total_included = ret.total_included;
                    list_taxes = list_taxes.concat(ret.taxes);
                }
                else {
                    var tax_amount = self._compute_all(tax, base, quantity);
                    tax_amount = round_pr(tax_amount, currency_rounding);

                    if (tax_amount){
                        if (tax.price_include) {
                            total_excluded -= tax_amount;
                            base -= tax_amount;
                        }
                        else {
                            total_included += tax_amount;
                        }
                        if (tax.include_base_amount) {
                            base += tax_amount;
                        }
                        
                        var data = {
                            id: tax.id,
                            amount: tax_amount,
                            choose_taxes: tax.choose_taxes,
                            name: tax.name,
//                            name_for_pos: tax.name_for_pos // pass 'name_for_pos'
                        };
                        
                        list_taxes.push(data);
                    }
                }
            });
            return {
                taxes: list_taxes,
                total_excluded: round_pr(total_excluded, currency_rounding_bak),
                total_included: round_pr(total_included, currency_rounding_bak)
            };
        },
        
        get_all_prices: function(){
            var price_unit = this.get_unit_price() * (1.0 - (this.get_discount() / 100.0));
            var taxtotal = 0;

            var product =  this.get_product();
            var taxes_ids = product.taxes_id;
            var taxes =  this.pos.taxes;
            var taxdetail = {};
            var product_taxes = [];
            var city_tax = 0;

            _(taxes_ids).each(function(el){
                product_taxes.push(_.detect(taxes, function(t){
                    return t.id === el;
                }));
            });
           
            var all_taxes = this.compute_all(product_taxes, price_unit, this.get_quantity(), this.pos.currency.rounding);
            _(all_taxes.taxes).each(function(tax) {
            	/* НХАТ шалгаж тооцоох */
            	if(tax.choose_taxes == '2'){
            		city_tax += tax.amount;
            	}
            	else{
            		taxtotal += tax.amount;
            		taxdetail[tax.id] = tax.amount;          		
            	}
            });
            return {
                "priceWithTax": all_taxes.total_included,
                "priceWithoutTax": all_taxes.total_excluded,
                "tax": taxtotal,
                "cityTax": city_tax,
                "taxDetails": taxdetail,
            };
        },
    });
    
	return {
		Orderline: models.Orderline,
		Order: models.Order,
		PosModel: models.PosModel,
	};
});
