odoo.define('l10n_mn_point_of_sale.return_bill', function (require){
"use strict";

    var core = require('web.core');
    var FormView = require('web.FormView');
    var QWeb = core.qweb;
    var Model = require('web.Model');

    FormView.include({
        load_record: function(record) {

            var self = this;
            this._super(record);
            var custom_model = new Model('pos.order');
            var date = "";
            if(this.$buttons){
                custom_model.call('get_order_data',[self.datarecord.id]).done(function(result) {
                    date = result.date;
                    if(result.group == false || self.datarecord.is_bill_returned == true || self.datarecord.state == 'draft'){
                        $(".refund_button").hide();
                    }
                })

                this.$buttons.find('.refund_button').click(function(){
                    var data = {
                        'returnBillId': self.datarecord.ddtd_number,
                        'date': date,
                    }
                    $.ajax({
                        type: "POST",
                        url: "http://localhost:8025/returnBill/",
                        data: "param=" + JSON.stringify(data),
                        async: false,
                        success: function (response) {
                            var response1 = JSON.parse(response);
                            console.log('response ' + response);
                            if(response1.success == true){
                                custom_model.call('update_order',[self.datarecord.id])
                                .done(function(result) {
                                    alert("Ebarimt's refund is succes");
                                })
                            }
                            if(response1.success == false){
                                alert(response1.message);
                            }
                            $(".refund_button").hide();
                        },
                        error: function (request, status, error) {
                            alert(request + '\n' + status + '\n' + error);
                            $(".refund_button").hide();
                        }
                    });
                });
            }
        },
    });
});
