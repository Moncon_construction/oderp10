odoo.define('l10n_mn_point_of_sale.screens', function (require) {
	"use strict";

	var productScreen = require('point_of_sale.screens').ProductScreenWidget;
	var clientListScreen = require('point_of_sale.screens').ClientListScreenWidget;
	var paymentScreen = require('point_of_sale.screens').PaymentScreenWidget;
	var receiptScreen = require('point_of_sale.screens').ReceiptScreenWidget;
	var OrderWidget = require('point_of_sale.screens').OrderWidget;
	var actionScreen = require('point_of_sale.screens').ActionpadWidget;
	var productList = require('point_of_sale.screens').ProductListWidget;
	var core = require('web.core');
	var Model = require('web.Model');
	var utils = require('web.utils');
	var round_pr = utils.round_precision;
	var QWeb = core.qweb;
	var models = require('point_of_sale.models');

	var _t = core._t;

	productList.include({

        render_product: function(product){
            var cached = this.product_cache.get_node(product.id);
            if(!cached){
                var image_url = this.get_product_image_url(product);
                console.log(product);

                var withpics = this.pos.config.show_product_images;
                var product_html = QWeb.render('Product',{
                    widget:  this,
                    product: product,
                    image_url: withpics ? this.get_product_image_url(product) : null,
                });

                var product_node = document.createElement('div');
                product_node.innerHTML = product_html;
                product_node = product_node.childNodes[1];
                this.product_cache.cache_node(product.id,product_node);
                return product_node;
            }
            return cached;
        },
    });

    function sleep(milliseconds) {
      const date = Date.now();
      let currentDate = null;
      do {
        currentDate = Date.now();
      } while (currentDate - date < milliseconds);
    }
	productScreen.include({
		show: function () {
			this._super();
			this.keyDownHandler = this.keyDownHandler.bind(this);
			window.document.body.addEventListener('keydown', this.keyDownHandler);
		},
		keyDownHandler: function (e) {
			if (this.checkKeypadAvailability()) {
				switch (e.keyCode) {
					case 40: //DOWN
						$(".orderlines .orderline.selected").next(".orderlines .orderline").click();
						break;
					case 38: //UP
						$(".orderlines .orderline.selected").prev(".orderlines .orderline").click();
						break;
					case 96:
					case 48: //0
						$(".product-screen .input-button.number-char:contains('0')").click();
						break;
					case 97:
					case 49: //1
						$(".product-screen .input-button.number-char:contains('1')").click();
						break;
					case 98:
					case 50: //2
						$(".product-screen .input-button.number-char:contains('2')").click();
						break;
					case 99:
					case 51: //3
						$(".product-screen .input-button.number-char:contains('3')").click();
						break;
					case 100:
					case 52: //4
						$(".product-screen .input-button.number-char:contains('4')").click();
						break;
					case 101:
					case 53: //5
						$(".product-screen .input-button.number-char:contains('5')").click();
						break;
					case 102:
					case 54: //6
						$(".product-screen .input-button.number-char:contains('6')").click();
						break;
					case 103:
					case 55: //7
						$(".product-screen .input-button.number-char:contains('7')").click();
						break;
					case 104:
					case 56: //8
						$(".product-screen .input-button.number-char:contains('8')").click();
						break;
					case 105:
					case 57: //9
						$(".product-screen .input-button.number-char:contains('9')").click();
						break;
					case 110:
					case 190: //.
						$(".product-screen .input-button.number-char:contains('.')").click();
						break;
					case 8: //backspace
						$(".product-screen .input-button.numpad-backspace").click();
						break;
					case 13: //enter - төлбөр
						e.preventDefault();
						$(".product-screen .button.pay").click();
						break;
					case 81: //q - Тоо хэмжээ
						e.preventDefault();
						$(".product-screen .mode-button[data-mode='quantity']").click();
						break;
					case 68: //d - Хөнглөлт
						e.preventDefault();
						$(".product-screen .mode-button[data-mode='discount']").click();
						break;
					case 67: //c - харилцагч хайх
						e.preventDefault();
						$(".product-screen .button.set-customer").click();
						break;
				}
			}
			if (e.keyCode == 113) { //f12 - Бараа хайх товч
				e.preventDefault();
				if ($(".product-screen .searchbox input").is(":focus")) {
					$(".product-screen .searchbox input").blur();
				} else {
					$(".product-screen .searchbox input").focus();
				}
			}
		},
		checkKeypadAvailability: function () {
			if ($(".popups .modal-dialog:not(.oe_hidden)").length) {
				return false;
			}
			if ($(".product-screen .searchbox input").is(":focus")) {
				return false;
			}
			return true;
		},
		close: function () {
			this._super();
			window.document.body.removeEventListener('keydown', this.keyDownHandler);
		}
	});

    // OVERRIDE.
    actionScreen.include({
    renderElement: function() {
        var self = this;
        this._super();
        this.$('.pay').click(function(){
            var order = self.pos.get_order();
            var check_quantity = true;
            var has_valid_product_lot = _.every(order.orderlines.models, function(line){
            // START CHANGE
                if(line.quantity <= 0){
                    alert(_t("Барааны тоо хэмжээ 0 байж болохгүй тул засна уу\n"+line.get_product().display_name));
                    self.gui.show_screen('products');
                    check_quantity = false;
                }
                return line.has_valid_product_lot();
            });
            if(!check_quantity){
                return;
            }// END CHANGE
               if(!has_valid_product_lot){
                self.gui.show_popup('confirm',{
                    'title': _t('Empty Serial/Lot Number'),
                    'body':  _t('One or more product(s) required serial/lot number.'),
                    confirm: function(){
                        self.gui.show_screen('payment');
                    },
                });
            }else{
                self.gui.show_screen('payment');
            }
        });

        this.$('.set-customer').click(function(){
            self.gui.show_screen('clientlist');
        });
    }
    });
	clientListScreen.include({
		show: function () {
			this._super();
			$(".clientlist-screen .searchbox input").focus();
			this.keyDownHandler = this.keyDownHandler.bind(this);
			window.document.body.addEventListener('keydown', this.keyDownHandler);
		},
		keyDownHandler: function (e) {
			switch (e.keyCode) {
				case 13: // ENTER
					if ($(".client-list .client-line.highlight").length) {
						$(".clientlist-screen .top-content .button.next.highlight").click();
					}
					break;
				case 27: // ESC
					e.preventDefault();
					$(".clientlist-screen .top-content .button.back").click();
					break;
				case 40: //DOWN
					if ($(".client-list .client-line.highlight").length == 0) {
						$(".client-list .client-line:first").click();
					} else {
						$(".client-list .client-line.highlight").next(".client-list .client-line").click();
					}
					break;
				case 38: //UP
					if ($(".client-list .client-line.highlight").length == 0) {
						$(".client-list .client-line:first").click();
					} else {
						$(".client-list .client-line.highlight").prev(".client-list .client-line").click();
					}
					break;
				case 113: //f2 - toggle search selection
					e.preventDefault();
					if ($(".clientlist-screen .searchbox input").is(":focus")) {
						$(".clientlist-screen .searchbox input").blur();
					} else {
						$(".clientlist-screen .searchbox input").focus();
					}
					break;
			}
		},
		close: function () {
			this._super();
			window.document.body.removeEventListener('keydown', this.keyDownHandler);
		}
	});

	paymentScreen.include({
		init: function (parent, options) {
			var self = this;
			this._super(parent, options);
			this.keyboard_keydown_handler = function (e){
				switch (e.keyCode) {
				case 27: // ESC
					self.click_back();
					break;
				case 69: // e letter
					e.preventDefault();
					if ($(".bill-type .paymentmethod").is(":focus")) {
						$(".bill-type .paymentmethod").blur();
					} else {
						$(".bill-type .paymentmethod").focus();
					}
					break;
				case 8: // BACKSPACE
					self.payment_input('BACKSPACE');
					break;
				case 46: // DELETE
					self.payment_input('CLEAR');
					break;
				}
			}
			this.keyboard_handler = function (event) {
				var key = '';
				var x = event.which;
				if (event.type === "keypress") {
					if (x === 13) {// Enter
						if ($('#register').is(':focus')) {
						} else {
							self.validate_order();
						}
					} else if (x === 190 || // Dot
						x === 110 || // Decimal point (numpad)
						x === 188 || // Comma
						x === 46) { // Numpad dot
						key = self.decimal_point;
					} else if (x >= 48 && x <= 57) { // Numbers
						key = '' + (x - 48);
					} else if (x === 45) { // Minus
						key = '-';
					} else if (x === 43) { // Plus
						key = '+';
					} else {
						key =event.key
					}
				} else { // keyup/keydown
					if (x === 46) { // Delete
						key = 'CLEAR';
					} else if (x === 8) { // Backspace
						key = 'BACKSPACE';
					}
				}
				if ($('#register').is(':focus')) {
					self.company_input(key);
				} else {
					self.payment_input(key);
				}
				event.preventDefault();
			};
		},

		show: function () {
			var self = this;
			this._super();
			var order = this.pos.get_order();
			if (order.ebarimt === undefined) { //initialize property ebarmt
				order.ebarimt = {
					companyName: "",
					companyReg: 0,
					ddtd: 0,
					lottery: '',
					qrData: 0,
					amount: 0,
					discount_amount: 0,
					total_amount: 0,
					total_amount_with_tax: 0,
					total_amount_without_tax: 0,
					vat_amount: 0,
					city_tax_amount: 0,
					register_amount: 0,
				};
			}
			var register = $('#register');

			/* Bill-type events */
			$('.bill-type .paymentmethod').change(function () {
				var order = self.pos.get_order();
				var selected = document.getElementById("is_print_bill").value;
				var rows = $('.bill-type .flex-row');
				if (selected === 'company') {
					$(rows[0]).removeClass('only-child');
					register.val('');
					$('.bill-type .company-name').text('');
					rows.show();
				} else {
					$(rows[0]).addClass('only-child');
					rows.slice(-2).hide();
				}

				order.ebarimt.companyReg = 0;
				order.ebarimt.companyName = "";
			});
			$('.bill-type .paymentmethod').val('person');
			$('.bill-type .paymentmethod').change();

			register.keyup(function (e) {
				if (e.keyCode === 13) {
					var resp = self.pos.config.ebarimt_info_address + register.val();
					var custom_model = new Model('pos.order')
					custom_model.call('get_merchant_info', [resp]).then(function (your_return_value) {
						var jsonfile = JSON.parse(your_return_value);

						order.ebarimt.companyReg = register.val();
						order.ebarimt.companyName = jsonfile.name;
						if (register.val() == "") {
							alert("Татвар төлөгчийн дугаарыг оруулна уу!");
						}
						if (register.val() == self.pos.company.company_registry) {
							alert("Та өөрийн компани луу борлуулалт хийж болохгүй!");
						}
						if (jsonfile.found == false) {
							alert(register.val() + " регистрийн дугаартай компани бүртгэлгүй байна!");
						}

						document.getElementById("company_name").innerHTML = jsonfile.name;
					});
				}
			});
		},

		order_is_valid: function(force_validation) {
            var self = this;
            var order = this.pos.get_order();

            // FIXME: this check is there because the backend is unable to
            // process empty orders. This is not the right place to fix it.
            if (order.get_orderlines().length === 0) {
                this.gui.show_popup('error',{
                    'title': _t('Empty Order'),
                    'body':  _t('There must be at least one product in your order before it can be validated'),
                });
                return false;
            }
            var plines = order.get_paymentlines();
            // New code starts here
            var total_payment = order.get_total_with_tax();
            for (var i = 0; i < plines.length; i++) {
                total_payment -= plines[i].get_amount()
                if (plines[i].get_type() === 'bank') {
                    if (total_payment.toFixed(2) < 0) {
                        this.gui.show_popup('error',{
                        'title': _t('Incorrect Bank Payment'),
                        'body': _t('Payment amount exceeds the necessary amount on Banks.'),
                        });
                        return false;
                    }
                }
            }
            // New code ends here
            for (var i = 0; i < plines.length; i++) {
                if (plines[i].get_type() === 'bank' && plines[i].get_amount() < 0) {
                    this.gui.show_popup('error',{
                        'title': _t('Negative Bank Payment'),
                        'body': _t('You cannot have a negative amount in a Bank payment. Use a cash payment method to return money to the customer.'),
                    });
                    return false;
                }
            }

            if (!order.is_paid() || this.invoicing) {
                return false;
            }

            // The exact amount must be paid if there is no cash payment method defined.
            if (Math.abs(order.get_total_with_tax() - order.get_total_paid()) > 0.00001) {
                var cash = false;
                for (var i = 0; i < this.pos.cashregisters.length; i++) {
                    cash = cash || (this.pos.cashregisters[i].journal.type === 'cash');
                }
                if (!cash) {
                    this.gui.show_popup('error',{
                        title: _t('Cannot return change without a cash payment method'),
                        body:  _t('There is no cash payment method available in this point of sale to handle the change.\n\n Please pay the exact amount or add a cash payment method in the point of sale configuration'),
                    });
                    return false;
                }
            }

            // if the change is too large, it's probably an input error, make the user confirm.
            if (!force_validation && order.get_total_with_tax() > 0 && (order.get_total_with_tax() * 1000 < order.get_total_paid())) {
                this.gui.show_popup('confirm',{
                    title: _t('Please Confirm Large Amount'),
                    body:  _t('Are you sure that the customer wants to  pay') +
                           ' ' +
                           this.format_currency(order.get_total_paid()) +
                           ' ' +
                           _t('for an order of') +
                           ' ' +
                           this.format_currency(order.get_total_with_tax()) +
                           ' ' +
                           _t('? Clicking "Confirm" will validate the payment.'),
                    confirm: function() {
                        self.validate_order('confirm');
                    },
                });
                return false;
            }

            return true;
        },

		validate_order: function (force_validation, pharmacy) {
			var self = this;
			if ($(".payment-screen .button.next.highlight").length > 0) {
				var order = this.pos.get_order();
                var plines = order.get_paymentlines();
                var total_payment = order.get_total_with_tax();
                for (var i = 0; i < plines.length; i++) {
                    total_payment -= plines[i].get_amount()
                    if (plines[i].get_type() === 'bank') {
                        if (total_payment.toFixed(2) < 0) {
                            this.gui.show_popup('error', {
                                'title': _t('Incorrect Bank Payment'),
                                'body': _t('Payment amount exceeds the necessary amount on Banks.'),
                            });
                            return false;
                        }
                    }
                }
				var stocks = [];
				var totalVat = 0;
				var receiptTotalAmount = 0;
				var totalCityTax = 0;
				var taxInfo = {};
				var taxType = null;

				var stocks2 = [];
				var totalVat2 = 0;
				var receiptTotalAmount2 = 0;
				var totalCityTax2 = 0;

				var stocks3 = [];
				var totalVat3 = 0;
				var receiptTotalAmount3 = 0;
				var totalCityTax3 = 0;

                var res_ok = true;
				
				taxType = order.get_vat_category_code();
				/* Бүх tax-уудын цуглуулгыг үүсгэнэ */
				order.get_tax_details().forEach(function (item) {
					var taxId = item.tax.id;
					var taxAmount = item.tax.amount;
					taxInfo[taxId] = taxAmount;
					if (item.tax.children_tax_ids.length > 0){
						taxInfo[taxId] = 1;
					}
				});

				/* stock-ийг угсрах */
				order.orderlines.models.forEach(function (item) {

                    var totalAmount = item.get_unit_price_after_discount() * item.quantity;
                    var productCode = item.product.default_code;
                    var categoryCodeId, regex, m;
                    if (!productCode) { // барааны код байхгүй бол БАРКОД-оо авна.
                        productCode = item.product.barcode;
                    }
                    if (!productCode) { // БАРКОД байхгүй бол Ангилалын код авна
                        if (item.product.vat_category_code_id)
                            var productCode = self.pos.db.get_vat_category_code_id(item.product.vat_category_code_id[0]).code;
                        else
                            productCode = "";
                    }
                    var productVat = item.get_all_prices().tax
                    var cityTax = item.get_all_prices().cityTax
                    var barcode = item.product.barcode;
                    if (!barcode) { // БАРКОД байхгүй бол Ангилалын код авна
                        if (item.product.vat_category_code_id){
                            barcode = self.pos.db.get_vat_category_code_id(item.product.vat_category_code_id[0]).code;
                        } else {
                            barcode = "";
                        }
                    }
                    if(item.get_product().vat_category_code_id[0] == 1){
                        receiptTotalAmount += totalAmount;
                        totalCityTax += cityTax;
                        totalVat += productVat;
                        stocks.push({
                            "code": productCode.toString(),
                            "name": item.product.display_name.replace("&", " ЭНД "),
                            "measureUnit": item.product.uom_id[1],
                            "qty": item.quantity.toFixed(2),
                            "unitPrice": item.get_unit_price_after_discount().toFixed(2),
                            "totalAmount": totalAmount.toFixed(2),
                            "cityTax": cityTax.toFixed(2),
                            "vat": productVat.toFixed(2),
                            "barcode": barcode.toString()
                        });
                    }

					if(item.get_product().vat_category_code_id[0] == 2){
					    if(productVat > 0)
					    {
					        alert(item.product.display_name + " барааны НӨАТ -ын ангилалын код эсвэл бодогдох татвар буруу байна!");
					        res_ok = false;
					    }
                        receiptTotalAmount2 += totalAmount;
                        totalCityTax2 += cityTax;
                        totalVat2 += productVat;
                        stocks2.push({
                            "code": productCode.toString(),
                            "name": item.product.display_name.replace("&", " ЭНД "),
                            "measureUnit": item.product.uom_id[1],
                            "qty": item.quantity.toFixed(2),
                            "unitPrice": item.get_unit_price_after_discount().toFixed(2),
                            "totalAmount": totalAmount.toFixed(2),
                            "cityTax": cityTax.toFixed(2),
                            "vat": productVat.toFixed(2),
                            "barcode": barcode.toString()
                        });
					}

					if(item.get_product().vat_category_code_id[0] == 3 ){

					    if(productVat > 0)
					    {
					        alert(item.product.display_name + " барааны НӨАТ -ын ангилалын код эсвэл бодогдох татвар буруу байна!");
					        res_ok = false;
					    }
                        receiptTotalAmount3 += totalAmount;
                        totalCityTax3 += cityTax;
                        totalVat3 += productVat;
                        stocks3.push({
                            "code": productCode.toString(),
                            "name": item.product.display_name.replace("&", " ЭНД "),
                            "measureUnit": item.product.uom_id[1],
                            "qty": item.quantity.toFixed(2),
                            "unitPrice": item.get_unit_price_after_discount().toFixed(2),
                            "totalAmount": totalAmount.toFixed(2),
                            "cityTax": cityTax.toFixed(2),
                            "vat": productVat.toFixed(2),
                            "barcode": barcode.toString()
                        });
					}
				});

				var billType = order.ebarimt.companyReg ? "3" : "1";
				var customerNo = order.ebarimt.companyReg ? order.ebarimt.companyReg.toString() : "";
				var district_id = self.pos.config.district_code_id[0];
				var district = this.pos.db.get_district(district_id);
				/* ebarimt API-ийн PUT лүү явуулах датаг угсрах */
				if(stocks.length > 0 && res_ok == true){
                    var data = {
                        "amount": receiptTotalAmount.toFixed(2),
                        "vat": totalVat.toFixed(2),
                        "cashAmount": receiptTotalAmount.toFixed(2),
                        "nonCashAmount": "0.00",
                        "cityTax": totalCityTax.toFixed(2),
                        "reportMonth": "",
                        "districtCode": district.code.toString(),
                        "posNo": "0001",
                        "customerNo": customerNo,
                        "billType": billType,
                        "taxType": "1",
                        "billIdSuffix": "",
                        "returnBillId": "",
                        "stocks": stocks
                    };
                    console.log("SERVICE SEND DATA", data);
                    /* POSAPI-ийн PUT лүү ajax явуулах */

                    if(pharmacy != true){
                        var vat_url = self.pos.config.vatps_address;
                        $.ajax({
                            type: "POST",
                            url: self.pos.config.vatps_address, //http://10.0.9.239:8028/put/aba
                            data: "param=" + JSON.stringify(data),
                            async: false,
                            success: function (response) {
                                console.log("SERVICE PUT response", response);
                                try {
                                    var jsonResp = JSON.parse(response);
                                    if (jsonResp.success == 'false' || jsonResp.success === false) {
                                        alert(jsonResp.message);
                                        res_ok = false;
                                    }
                                    else{
                                        order.ebarimt.billId = jsonResp.billId;
                                        order.ebarimt.lottery = jsonResp.lottery;
                                        order.ebarimt.qrData = jsonResp.qrData;
                                        order.ebarimt.amount = jsonResp.amount;
                                        order.ebarimt.registerNo = jsonResp.merchantId;
                                        order.ebarimt.customerNo = jsonResp.customerNo;
                                        if (jsonResp.billType == 3)
                                            order.ebarimt.billType = 'company';
                                        else
                                            order.ebarimt.billType = 'person';
                                        order.ebarimt.total_amount = order.get_gross_amount();
                                        order.ebarimt.discount_amount = order.get_total_discount();
                                        order.ebarimt.total_amount_with_tax = order.get_gross_amount_after_discount();
                                        order.ebarimt.total_amount_without_tax = order.get_gross_amount_after_discount() - order.get_total_vat() - order.get_total_cityTax();
                                        order.ebarimt.vat_amount = order.get_total_vat();
                                        order.ebarimt.city_tax_amount = order.get_total_cityTax();
                                        order.ebarimt.register_amount = jsonResp.amount;

                                        var bill = new models.EtaxBills({}, {pos: order.pos, order: order});
                                        if (jsonResp.billType == 3)
                                            bill.receipt_type = 'company';
                                        else
                                            bill.receipt_type = 'person';
                                        bill.ddtd_number = jsonResp.billId;
                                        bill.lottery_number = jsonResp.lottery;
                                        bill.ebarimt_register_amount = jsonResp.amount;
                                        bill.qrData = jsonResp.qrData;
                                        bill.registerNo = jsonResp.merchantId;
                                        bill.customer_register = jsonResp.customerNo;
                                        bill.customer_name = order.ebarimt.companyName

                                        order.add_taxbill(bill);
                                    }
                                    if(jsonResp.lotteryWarningMsg){

                                        xmlHttp.open("GET", vat_url.replace("put", "sendData"), false );
                                        xmlHttp.send();
                                        console.log('lotteryWarningMsg sendData ==>>', xmlHttp.responseText);
                                    }
                                } catch (e) {
                                    alert("JSON хөрвүүлэхэд алдаа гарлаа:\t" + e);
                                }
                            },
                            error: function (request, status, error) {
                                console.error(request.responseText, error);
                            }
                        });
                    }
                }

//             ===================================================================================

                if(stocks2.length > 0 && res_ok == true){
                    var data = {
                        "amount": receiptTotalAmount2.toFixed(2),
                        "vat": totalVat2.toFixed(2),
                        "cashAmount": receiptTotalAmount2.toFixed(2),
                        "nonCashAmount": "0.00",
                        "cityTax": totalCityTax.toFixed(2),
                        "reportMonth": "",
                        "districtCode": district.code.toString(),
                        "posNo": "0001",
                        "customerNo": customerNo,
                        "billType": billType,
                        "taxType": "2",
                        "billIdSuffix": "",
                        "returnBillId": "",
                        "stocks": stocks2
                    };
                    console.log("SERVICE SEND DATA", data);

                    sleep(1000);
                    /* POSAPI-ийн PUT лүү ajax явуулах */
                    if(pharmacy != true){
                        var vat_url = self.pos.config.vatps_address;
                        $.ajax({
                            type: "POST",
                            url: self.pos.config.vatps_address, //http://10.0.9.239:8028/put/
                            data: "param=" + JSON.stringify(data),
                            async: false,
                            success: function (response) {
                                console.log("SERVICE PUT response", response);
                                try {
                                    var jsonResp = JSON.parse(response);
                                    if (jsonResp.success == 'false' || jsonResp.success === false) {
                                        alert(jsonResp.message);
                                        res_ok = false;
                                    }
                                    else{
                                        order.ebarimt.billId = jsonResp.billId;
                                        order.ebarimt.lottery = jsonResp.lottery;
                                        order.ebarimt.qrData = jsonResp.qrData;
                                        order.ebarimt.amount = jsonResp.amount;
                                        order.ebarimt.registerNo = jsonResp.merchantId;
                                        order.ebarimt.customerNo = jsonResp.customerNo;
                                        if (jsonResp.billType == 3)
                                            order.ebarimt.billType = 'company';
                                        else
                                            order.ebarimt.billType = 'person';
                                        order.ebarimt.total_amount = order.get_gross_amount();
                                        order.ebarimt.discount_amount = order.get_total_discount();
                                        order.ebarimt.total_amount_with_tax = order.get_gross_amount_after_discount();
                                        order.ebarimt.total_amount_without_tax = order.get_gross_amount_after_discount() - order.get_total_vat() - order.get_total_cityTax();
                                        order.ebarimt.vat_amount = order.get_total_vat();
                                        order.ebarimt.city_tax_amount = order.get_total_cityTax();
                                        order.ebarimt.register_amount = jsonResp.amount;

                                        var bill = new models.EtaxBills({}, {pos: order.pos, order: order});
                                        if (jsonResp.billType == 3)
                                            bill.receipt_type = 'company';
                                        else
                                            bill.receipt_type = 'person';
                                        bill.ddtd_number = jsonResp.billId;
                                        bill.lottery_number = jsonResp.lottery;
                                        bill.ebarimt_register_amount = jsonResp.amount;
                                        bill.qrData = jsonResp.qrData;
                                        bill.registerNo = jsonResp.merchantId;
                                        bill.customer_register = jsonResp.customerNo;
                                        bill.customer_name = order.ebarimt.companyName

                                        order.add_taxbill(bill);

                                        if(jsonResp.lotteryWarningMsg){
                                            xmlHttp.open("GET", vat_url.replace("put", "sendData"), false );
                                            xmlHttp.send();
                                            console.log('lotteryWarningMsg sendData ==>>', xmlHttp.responseText);
                                        }
                                    }
                                } catch (e) {
                                    alert("JSON хөрвүүлэхэд алдаа гарлаа:\t" + e);
                                }
                            },
                            error: function (request, status, error) {
                                console.error(request.responseText, error);
                            }
                        });
                    }
                }

//                ===================================================================================================

                if(stocks3.length > 0 && res_ok == true){
                    var data = {
                        "amount": receiptTotalAmount3.toFixed(2),
                        "vat": totalVat3.toFixed(2),
                        "cashAmount": receiptTotalAmount3.toFixed(2),
                        "nonCashAmount": "0.00",
                        "cityTax": totalCityTax.toFixed(2),
                        "reportMonth": "",
                        "districtCode": district.code.toString(),
                        "posNo": "0001",
                        "customerNo": customerNo,
                        "billType": billType,
                        "taxType": "3",
                        "billIdSuffix": "",
                        "returnBillId": "",
                        "stocks": stocks3
                    };
                    console.log("SERVICE SEND DATA", data);
                    sleep(1000);
//
                    /* POSAPI-ийн PUT лүү ajax явуулах */

                    if(pharmacy != true){
                        var vat_url = self.pos.config.vatps_address;
                        $.ajax({
                            type: "POST",
                            url: self.pos.config.vatps_address, //http://10.0.9.239:8028/put/
                            data: "param=" + JSON.stringify(data),
                            async: false,
                            success: function (response) {
                                console.log("SERVICE PUT response", response);
                                try {
                                    var jsonResp = JSON.parse(response);
                                    if (jsonResp.success == 'false' || jsonResp.success === false) {
                                        alert(jsonResp.message);
                                        res_ok = false;
                                    }
                                    else{
                                        order.ebarimt.billId = jsonResp.billId;
                                        order.ebarimt.lottery = jsonResp.lottery;
                                        order.ebarimt.qrData = jsonResp.qrData;
                                        order.ebarimt.amount = jsonResp.amount;
                                        order.ebarimt.registerNo = jsonResp.merchantId;
                                        order.ebarimt.customerNo = jsonResp.customerNo;
                                        if (jsonResp.billType == 3)
                                            order.ebarimt.billType = 'company';
                                        else
                                            order.ebarimt.billType = 'person';
                                        order.ebarimt.total_amount = order.get_gross_amount();
                                        order.ebarimt.discount_amount = order.get_total_discount();
                                        order.ebarimt.total_amount_with_tax = order.get_gross_amount_after_discount();
                                        order.ebarimt.total_amount_without_tax = order.get_gross_amount_after_discount() - order.get_total_vat() - order.get_total_cityTax();
                                        order.ebarimt.vat_amount = order.get_total_vat();
                                        order.ebarimt.city_tax_amount = order.get_total_cityTax();
                                        order.ebarimt.register_amount = jsonResp.amount;

                                        var bill = new models.EtaxBills({}, {pos: order.pos, order: order});
                                        if (jsonResp.billType == 3)
                                            bill.receipt_type = 'company';
                                        else
                                            bill.receipt_type = 'person';
                                        bill.ddtd_number = jsonResp.billId;
                                        bill.lottery_number = jsonResp.lottery;
                                        bill.ebarimt_register_amount = jsonResp.amount;
                                        bill.qrData = jsonResp.qrData;
                                        bill.registerNo = jsonResp.merchantId;
                                        bill.customer_register = jsonResp.customerNo;
                                        bill.customer_name = order.ebarimt.companyName

                                        order.add_taxbill(bill);

                                        if(jsonResp.lotteryWarningMsg){

                                            xmlHttp.open("GET", vat_url.replace("put", "sendData"), false );
                                            xmlHttp.send();
                                            console.log('lotteryWarningMsg sendData ==>>', xmlHttp.responseText);
                                        }
                                    }
                                } catch (e) {
                                    alert("JSON хөрвүүлэхэд алдаа гарлаа:\t" + e);
                                }
                            },
                            error: function (request, status, error) {
                                console.error(request.responseText, error);
                            }
                        });
                    }
                }
			}
			if(self.pos.company.check_vatpsp_service == true){
                try {
                    var xmlHttp = new XMLHttpRequest();
                    xmlHttp.open("GET", self.pos.config.vatps_address.replace("put", "getInformation"), false );
                    xmlHttp.send();
                    console.log(xmlHttp.responseText);
                    var jsonResp1= JSON.parse(xmlHttp.responseText);
                    if(jsonResp1.extraInfo.countLottery<=1000){

                        xmlHttp.open("GET", self.pos.config.ebarimt_service_address.replace("put", "sendData"), false );
                        xmlHttp.send();
                        console.log('sendData ==>>', xmlHttp.responseText);
                    }
                    console.log(jsonResp1.extraInfo.countLottery);
                } catch(e) {
                    alert("Ebarimt сервистэй холбогдоход алдаа гарлаа", e);
                    return false
                }
            }
			if (res_ok == true){
				return this._super();
			}
			else{ 
				return false;
				}
		},

		company_input: function (input) {
			if (input === 'BACKSPACE') {
				$("#register").val(
					function (index, value) {
						return value.substr(0, value.length - 1);
					})
			} else {
				/* Регистрийн дугаар авах */
				var xx = $("#register").val();
				$("#register").val(xx + input);
			}
		},
		
		click_paymentmethods: function(id) {
	        var cashregister = null;
	        for ( var i = 0; i < this.pos.cashregisters.length; i++ ) {
	            if ( this.pos.cashregisters[i].journal_id[0] === id ){
	                cashregister = this.pos.cashregisters[i];
	                break;
	            }
	        }
	        /* Нэг төлбөрийн төрлөөр нэг л төлбөрийн мөр үүсгэдэг болгов*/
	        var already_added = false;
	        for ( var i = 0; i < this.pos.get_order().get_paymentlines().length; i++ ) {
	            if ( this.pos.get_order().get_paymentlines()[i].name === cashregister.journal_id[1] ){
	                already_added = true;
	                break;
	            }
	        }
	        if (already_added === false){
		        this.pos.get_order().add_paymentline( cashregister );
		        this.reset_input();
	        }
	        this.render_paymentlines();
	    },

        click_invoice: function(){
            var order = this.pos.get_order();
            order.set_to_invoice(!order.is_to_invoice());
            var partner = this.pos.get_client();
            if (order.is_to_invoice()) {
                this.$('.js_invoice').addClass('highlight');
                if (partner){
                    this.$('.next').addClass('highlight');
                }
            } else {
                this.$('.js_invoice').removeClass('highlight');
                this.$('.next').removeClass('highlight');
            }
        },

        payment_input: function (input) {
			if(input !== '-')
                this._super(input);
		},
	});

	receiptScreen.include({
		show: function () {
			this._super();
			this.keyDownHandler = this.keyDownHandler.bind(this);
			window.document.body.addEventListener('keydown', this.keyDownHandler);
		},
		keyDownHandler: function (e) {
			switch (e.keyCode) {
				case 35: //END - дараагийн захиалга
					e.preventDefault();
					$(".receipt-screen .top-content .button.next.highlight").click();
					break;
				case 116: //F5 - хэвлэх
					e.preventDefault();
					$(".receipt-screen .centered-content .button.print").click();
					break;
				case 13: //Enter - Дараагийн хуудас руу шилжих
					e.preventDefault();
					this.click_next();
					break;
			}
		},
		click_next: function () {
			this._super();
			//detach keyDownHandler
			window.document.body.removeEventListener('keydown', this.keyDownHandler);
		},
		get_ebarimt_data: function () {
			var order = this.pos.get_order();
			var ebarimt = order.ebarimt;

			return {
				companyName: ebarimt.companyName,
				ddtd: ebarimt.billId,
				lottery: ebarimt.lottery,
				qrData: ebarimt.qrData,
				registerNo: ebarimt.registerNo,
				customerNo: ebarimt.customerNo,
				billType: ebarimt.billType,
				total_amount: ebarimt.total_amount,
				amount: ebarimt.amount,
				discount_amount: ebarimt.discount_amount,
				total_amount_with_tax: ebarimt.total_amount_with_tax,
				total_amount_without_tax: ebarimt.total_amount_without_tax,
				vat_amount: ebarimt.vat_amount,
				city_tax_amount: ebarimt.city_tax_amount,
				register_amount: ebarimt.register_amount,
				total_amount_with_tax: ebarimt.total_amount_with_tax,
			};
		},		
		format_precision: function (quantity) {
			var quant = parseFloat(quantity) || 0;
			return round_pr(quant, 0.01);
		},
		render_receipt: function () {
			var order = this.pos.get_order();
			var ebarimtData = this.get_ebarimt_data();
			var partedOrder = [];
			var taxes = false;
			var taxesFree = false;
			var taxesZero = false;

			for(var i = 0; i < order.orderlines.models.length; i++){
				if(order.orderlines.models[i].product.vat_category_code_id[0] == 3)
				{
				    if(taxesZero == false){
				        var neworder = new models.Order({},{
                            pos: this.pos,
                            temporary: true,
                        });
                        taxesZero = true;
                        neworder.add_taxbill(order.etaxbills.models[order.etaxbills.models.length - 1]);

                    }
                    neworder.orderlines.models.push(order.orderlines.models[i]);
				}
			}
			if(taxesZero == true){
			    partedOrder.push(neworder);
            }

			for(var i = 0; i < order.orderlines.models.length; i++){
				if(order.orderlines.models[i].product.vat_category_code_id[0] == 2)
				{
				    if(taxesFree == false){
				        var neworder = new models.Order({},{
                            pos: this.pos,
                            temporary: true,
                        });
                        taxesFree = true;
                        if(taxesZero == true)
                            neworder.add_taxbill(order.etaxbills.models[order.etaxbills.models.length - 2]);
                        else
                            neworder.add_taxbill(order.etaxbills.models[order.etaxbills.models.length - 1]);

                    }
                    neworder.orderlines.models.push(order.orderlines.models[i]);
				}
			}
			if(taxesFree == true){
			    partedOrder.push(neworder);
            }

            for(var i = 0; i < order.orderlines.models.length; i++){
				if(order.orderlines.models[i].product.vat_category_code_id[0] == 1)
				{
				    if(taxes == false){
				        var neworder = new models.Order({},{
                            pos: this.pos,
                            temporary: true,
                        });
                        taxes = true;
                        if(taxesZero == false && taxesFree == false){
                            neworder.add_taxbill(order.etaxbills.models[order.etaxbills.models.length - 1]);
                        }
                        if((taxesZero == true && taxesFree == false) || (taxesZero == false && taxesFree == true)){
                            neworder.add_taxbill(order.etaxbills.models[order.etaxbills.models.length - 2]);

                        }
                        if(taxesZero == true && taxesFree == true)
                            neworder.add_taxbill(order.etaxbills.models[order.etaxbills.models.length - 3]);
                    }
                    neworder.orderlines.models.push(order.orderlines.models[i]);
				}
			}
			if(taxes == true)
			    partedOrder.push(neworder);

			/* render QWeb template */
			var receiptContainer = this.$('.pos-receipt-container').html(QWeb.render('PosTicket', {
				widget: this,
				order: partedOrder,
				receipt: order.export_for_printing(),
				orderlines: null,
				paymentlines: order.get_paymentlines(),
				pharmDiscount: order.pharmDiscount,
				ebarimt: ebarimtData
			}));
		}
	});
	
	// Displays the current Order.

    OrderWidget.include({

    	update_summary: function(){
            var order = this.pos.get_order();
            if (!order.get_orderlines().length) {
                return;
            }
            var total     = order ? order.get_total_with_tax() : 0;
            var tax     = order ? order.get_total_vat() : 0;
            var city_taxes     = order ? order.get_total_cityTax() : 0;
            var taxes = tax + city_taxes;

            this.el.querySelector('.summary .total > .value').textContent = this.format_currency(total);
             /*Хотын татвартай бол татваруудыг харуулна.*/ 
            if(city_taxes > 0){
            	this.el.querySelector('.summary .total .subentry .nuat').textContent = this.format_currency(tax);
            	this.el.querySelector('.summary .total .subentry .nhat').textContent = this.format_currency(city_taxes);
            }
            this.el.querySelector('.summary .total .subentry .value').textContent = this.format_currency(taxes);
        },
    });
    
	return {
		productScreen: productScreen,
		clientListScreen: clientListScreen,
		OrderWidget: OrderWidget,
		paymentScreen: paymentScreen,
		receiptScreen: receiptScreen,
	};
	
});