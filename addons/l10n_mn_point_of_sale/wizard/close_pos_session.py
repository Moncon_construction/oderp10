# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class ClosePosSessionWizard(models.TransientModel):
    _name = 'close.pos.session.wizard'
    _description = 'Close pos session'
    
    user_id = fields.Many2one('res.users','User',default=lambda self: self.env.user)
    description = fields.Text('Description')
    
    @api.multi
    def close(self):
        context = self._context
        sessions = self.env['pos.session'].browse(context['default_ids'])
        for session in sessions:
            session.action_pos_session_closing_control()
            session.write({'closing_user_id': self.user_id.id,
                           'description': self.description})