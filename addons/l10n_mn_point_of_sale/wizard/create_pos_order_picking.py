# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError
import warnings


class CreatePosOrderPicking(models.TransientModel):
    _name = 'create.pos.order.picking'

    # "ПОС - н захиалгуудаас агуулахын хөдөлгөөн нь үүсээгүй захиалгуудын Picking нөхөж үүсгэх"
    def action_create_pos_order_picking(self):
        active_ids = self._context.get('active_ids', []) or []
        pos_obj = self.env['pos.order'].search([('id', 'in', active_ids)])
        pos_config_id = pos_obj[0].session_id.config_id.id
        for obj in pos_obj:
            if obj.session_id.config_id.id != pos_config_id:
                raise UserError(_(u'Must be in the same pos!'))

        for obj in pos_obj:
            if not obj.picking_id and obj.state == 'paid':
                obj.create_picking()


            elif obj.picking_id:
                raise UserError(_(u'Created stock picking!'))
            elif obj.state != 'paid':
                raise UserError(_(u'Pos order must be in paid state!'))
        view = self.env.ref('l10n_mn_point_of_sale.sh_message_wizard')
        view_id = view and view.id or False
        context = dict(self._context or {})
        context['message'] = _(u'Successfully created!')
        return {
            'name': 'Success',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'sh.message.wizard',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'target': 'new',
            'context': context
        }