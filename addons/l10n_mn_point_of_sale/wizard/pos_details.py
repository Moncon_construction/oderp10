# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import fields, models

class PosDetails(models.TransientModel):
    _inherit = 'pos.details.wizard'

    pos_config_ids = fields.Many2many('pos.config', 'pos_detail_configs', default='')
