# -*- coding: utf-8 -*-
{
    'name': "Mongolian Point of Sale 2nd UOM",
    'version': '2.1',
    'author': "Asterisk Technologies LLC",
    'sequence': 15,
    'website': 'http://asterisk-tech.mn',
    'depends': [
		'l10n_mn_shts',
        'l10n_mn_point_of_sale',
    ],
    'category': 'Mongolian Modules',
    'description': """
       Mongolian Point of Sale
    """,
    'data': [
        'security/security_view.xml',
        'views/point_of_sale.xml',
        'views/pos_order_view.xml',
        'views/second_measure_in_stock_move.xml',
    ],
    'qweb': ['static/src/xml/pos.xml'],
    'application': True,
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
