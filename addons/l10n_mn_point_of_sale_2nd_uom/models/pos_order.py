# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import logging
from datetime import datetime, timedelta
from functools import partial

import psycopg2
import pytz

from odoo import api, fields, models, tools, _
from odoo.tools import float_is_zero
from odoo.exceptions import UserError
from odoo.http import request
import odoo.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)

class PosOrder(models.Model):
    _inherit = "pos.order"
    
    # Нэвтэрсэн хэрэглэгч нь 2дахь хэмжих нэгжийн эрхтэй эсэхийг шалгах функц энэ нь screens.js-д ашиглагдана.
    @api.model
    def is_group_user_of_second_uom(self):
        return self.env.user.has_group('l10n_mn_point_of_sale_2nd_uom.group_user_of_second_uom')

    def create_picking(self):
        _logger.warning('pos_order create_picking')
        # @Override
        """Create a picking for each order and validate it."""
        Picking = self.env['stock.picking']
        Move = self.env['stock.move']
        StockWarehouse = self.env['stock.warehouse']
        for order in self:
            if not order.lines.filtered(lambda l: l.product_id.type in ['product', 'consu']):
                continue
            address = order.partner_id.address_get(['delivery']) or {}
            picking_type = order.picking_type_id
            return_pick_type = order.picking_type_id.return_picking_type_id or order.picking_type_id
            order_picking = Picking
            return_picking = Picking
            location_id = order.location_id.id
            if order.partner_id:
                destination_id = order.partner_id.property_stock_customer.id
            else:
                if (not picking_type) or (not picking_type.default_location_dest_id):
                    customerloc, supplierloc = StockWarehouse._get_partner_locations()
                    destination_id = customerloc.id
                else:
                    destination_id = picking_type.default_location_dest_id.id

            if picking_type:
                message = _("This transfer has been created from the point of sale session: <a href=# data-oe-model=pos.order data-oe-id=%d>%s</a>") % (order.id, order.name)
                picking_vals = {
                    'origin': order.name,
                    'partner_id': address.get('delivery', False),
                    'date_done': order.date_order,
                    'picking_type_id': picking_type.id,
                    'company_id': order.company_id.id,
                    'move_type': 'direct',
                    'note': order.note or "",
                    'location_id': location_id,
                    'location_dest_id': destination_id,
                }
                pos_qty = any([x.qty > 0 for x in order.lines if x.product_id.type in ['product', 'consu']])
                if pos_qty:
                    order_picking = Picking.create(picking_vals.copy())
                    order_picking.message_post(body=message)
                neg_qty = any([x.qty < 0 for x in order.lines if x.product_id.type in ['product', 'consu']])
                if neg_qty:
                    return_vals = picking_vals.copy()
                    return_vals.update({
                        'location_id': destination_id,
                        'location_dest_id': return_pick_type != picking_type and return_pick_type.default_location_dest_id.id or location_id,
                        'picking_type_id': return_pick_type.id
                    })
                    return_picking = Picking.create(return_vals)
                    return_picking.message_post(body=message)

            for line in order.lines.filtered(lambda l: l.product_id.type in ['product', 'consu'] and not float_is_zero(l.qty, precision_rounding=l.product_id.uom_id.rounding)):
                #### BEGIN ####
                # 2дахь хэмжих нэгж болон түүний тоо хэмжээг авч барааны хөдөлгөөн үүсдэг болгов.
                # stock_move create функц нь удаан ажиллаж байгаа тул insert query болгож солив
                _logger.info('pos_order create stock move')
                self.env.cr.execute("""INSERT INTO stock_move (create_uid, create_date, write_uid, write_date,
                                                    date, date_expected, name, picking_id, 
                                                    picking_type_id, product_id, product_uom_qty, product_qty, 
                                                    product_uom, second_product_qty, second_uom_id, state, procure_method, 
                                                    order_line_id, company_id, analytic_account_id, 
                                                    location_id, location_dest_id)
                                                    values (%s, '%s', %s, '%s','%s', '%s', '%s', %s, %s, %s, %s, %s, %s, %s, %s, '%s', '%s', %s, %s, %s, %s, %s) """
                                    % (self.env.user.id, datetime.now(), self.env.user.id, datetime.now(),
                                       order.date_order, order.date_order, line.name, order_picking.id if line.qty >= 0 else return_picking.id,
                                       picking_type.id if line.qty >= 0 else return_picking.picking_type_id.id,  line.product_id.id, abs(line.qty), abs(line.qty),
                                       line.product_id.uom_id.id, abs(line.second_uom_qty), line.second_uom_id.id or 'NULL', 'draft', 'make_to_stock',
                                       line.id, order.company_id.id, line.analytic_account_id and line.analytic_account_id.id or 'NULL',
                                       location_id if line.qty >= 0 else destination_id,
                                       destination_id if line.qty >= 0 else return_pick_type != picking_type and return_pick_type.default_location_dest_id.id or location_id
                                       ))
                _logger.info('pos_order create stock move done')
                #### END ####

            # prefer associating the regular order picking, not the return
            order.write({'picking_id': order_picking.id or return_picking.id})
            if return_picking:
                order._force_picking_done(return_picking)
            if order_picking:
                order._force_picking_done(order_picking)
            # when the pos.config has no picking_type_id set only the moves will be created
            if not return_picking and not order_picking:
                moves = Move.search([('order_line_id', 'in', order.lines.ids)])
                if moves:
                    tracked_moves = moves.filtered(lambda move: move.product_id.tracking != 'none')
                    untracked_moves = moves - tracked_moves
                    tracked_moves.action_confirm()
                    untracked_moves.action_assign()
                    moves.filtered(lambda m: m.state in ['confirmed', 'waiting']).force_assign()
                    moves.filtered(lambda m: m.product_id.tracking == 'none').action_done()
        return True

class PosOrderLine(models.Model):
    _inherit = "pos.order.line"

    second_uom_id = fields.Many2one(related='product_id.second_uom_id')
    second_uom_qty = fields.Float('Second Product Quantity')