odoo.define('l10n_mn_point_of_sale_2nd_uom.DB', function (require) {
    "use strict";
    
    var PosDB = require('point_of_sale.DB');
    PosDB.include({
        init: function(options){
            this.second_uom_by_product_tmpl_id = [];
            this._super(options);
        },
        // Өгөгдлийн сангаас product.template моделоос өгөгдөл авахдаа 
        // second_uom_by_product_tmpl_id хүснэгтэд
        // product_template -н ID-р индекслэж хадгална.
        add_second_uom_id: function(product_template){
            for (let index = 0; index < product_template.length; index++) {
                if (product_template[index].second_uom_id!=false){
                    this.second_uom_by_product_tmpl_id.push({
                        product_template_id: product_template[index].id,
                        second_uom_id: product_template[index].second_uom_id[0],
                        second_uom_name: product_template[index].second_uom_id[1]
                    });
                }
            }
        },
        // 2дахь хэмжих нэгжийг авахдаа second_uom_by_product_tmpl_id хүснэгтээс
        // product_template -н ID-р индекслэж авна.
        get_second_uom_by_product_tmpl_id: function(product_tmpl_id){
            for (let index = 0; index < this.second_uom_by_product_tmpl_id.length; index++) {
                if(product_tmpl_id == this.second_uom_by_product_tmpl_id[index].product_template_id){
                    if(this.second_uom_by_product_tmpl_id[index].second_uom_id){
                        return this.second_uom_by_product_tmpl_id[index].second_uom_id;
                    }
                }
            }
        }
    });
    return PosDB;
});