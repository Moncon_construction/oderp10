odoo.define('l10n_mn_point_of_sale_2nd_uom.models', function (require) {
    "use strict";
    
    var utils = require('web.utils');
    var round_pr = utils.round_precision;
    var round_di = utils.round_decimals;
    var formats = require('web.formats');
    var models = require('point_of_sale.models');
    var _super_posmodel = models.PosModel.prototype;
    var _super_orderline = models.Orderline.prototype;

    models.PosModel = models.PosModel.extend({
        initialize: function (session, attributes) {
            // Өгөгдлийн сангаас өгөгдөл авах
            this.models.push({
                model:  'product.template',
                fields: ['second_uom_id','id'],
                domain: [['second_uom_id','!=',false]],
                loaded: function(self, product_template){ 
                    self.db.add_second_uom_id(product_template);
                },
            });
            return _super_posmodel.initialize.call(this, session, attributes);
        }
    });

    models.Orderline = models.Orderline.extend({
        initialize: function() {
          _super_orderline.initialize.apply(this,arguments);  
          this.second_quantity_str = '';  
          this.second_quantity = 0;
        },
        // Моделруу утга авахад оролтын утгуудыг буцаах функц дахин тодорхойлж
        // second_uom_qty талбар тохируулдгаар буюу set функц дуудаж удамшуулав.
        init_from_JSON: function(json){
            _super_orderline.init_from_JSON.apply(this,arguments);
            this.set_second_quantity(json.second_uom_qty);         
        }, 
        // Моделоос утга авахад гарах утгуудыг буцаах функц дахин тодорхойлж
        // second_uom_qty, product_tmpl_id талбарууд нэмэв.
        export_as_JSON: function() {
            var pack_lot_ids = [];
            if (this.has_product_lot){
                this.pack_lot_lines.each(_.bind( function(item) {
                    return pack_lot_ids.push([0, 0, item.export_as_JSON()]);
                }, this));
            }
            return {
                qty: this.get_quantity(),
                second_uom_qty: this.get_second_quantity(),
                product_tmpl_id: this.get_product().product_tmpl_id,
                price_unit: this.get_unit_price(),
                discount: this.get_discount(),
                product_id: this.get_product().id,
                tax_ids: [[6, false, _.map(this.get_applicable_taxes(), function(tax){ return tax.id; })]],
                id: this.id,
                pack_lot_ids: pack_lot_ids
            };
        },
        // Барааны мөрд утга авах функц
        clone: function(){
            _super_orderline.clone.apply(this,arguments);
            orderline.second_quantity = this.second_quantity;
            orderline.second_quantity_str = this.second_quantity_str;
            return orderline;
        },
        // 2дахь тоо хэмжээ авах
        get_second_quantity: function(){
            return this.second_quantity;
        },
        // 2дахь тоо хэмжээ тэмдэгт төрлөөр авах
        get_second_quantity_str: function(){
            return " " + this.second_quantity_str + " / ";
        },
        // 2дахь хэмжих нэгж авах
        get_second_unit: function(){
            var second_unit = this.pos.units_by_id[this.get_second_uom_id(this.product.product_tmpl_id)];
            if (second_unit){
                return second_unit;
            }
            else{
                return false;
            }
        },
        // 2дахь хэмжих нэгжийг тоо хэмжээтэй тэмдэгт төрлөөр авах
        get_second_quantity_str_with_unit: function(){
            var unit = this.get_second_unit();
            if(unit && !unit.is_unit){
                return this.second_quantity_str + ' ' + unit.name;
            }else{
                return this.second_quantity_str;
            }
        },
        // Барааны product_tmpl_id-р product.template-н 2дох хэмжих нэгжийг авах
        get_second_uom_id: function(product_tmpl_id) {
            var second_uom_id = this.pos.db.get_second_uom_by_product_tmpl_id(product_tmpl_id);            
            if(second_uom_id != null){
                return second_uom_id;
            }
            else{
                return undefined; 
            }
        },
         // Барааны 2дох тоо хэмжээг авч 2дох хэмжих нэгжийг тохируулах буюу set функц
        set_second_quantity: function(quantity){
            this.order.assert_editable();
            if(quantity === 'remove'){
                this.order.remove_orderline(this);
                return;
            }else{
                var quant = parseFloat(quantity) || 0;
                var unit = this.get_second_unit();
                if(unit){
                    if (unit.rounding) {
                        this.second_quantity    = round_pr(quant, unit.rounding);
                        var decimals = this.pos.dp['Product Unit of Measure'];
                        this.second_quantity = round_di(this.second_quantity, decimals)
                        this.second_quantity_str = formats.format_value(this.second_quantity, { type: 'float', digits: [69, decimals]});
                    } else {
                        this.second_quantity    = round_pr(quant, 1);
                        this.second_quantity_str = this.second_quantity.toFixed(0);
                    }
                }else{
                    this.second_quantity    = quant;
                    this.second_quantity_str = '' + this.second_quantity;
                }
            }
            this.trigger('change',this);
        },
        // when we add an new orderline we want to merge it with the last line to see reduce the number of items
        // in the orderline. This returns true if it makes sense to merge the two
        // override хийж нэмэлтээр 2дахь тоо хэмжээ шалгадагаар дарав
        can_be_merged_with: function(orderline){
            if( this.get_product().id !== orderline.get_product().id){    //only orderline of the same product can be merged
                return false;
            }else if(!this.get_unit() || !this.get_unit().groupable){
                return false;
            }else if(!this.get_second_unit() || !this.get_second_unit().groupable){
                return false;
            }else if(this.get_product_type() !== orderline.get_product_type()){
                return false;
            }else if(this.get_discount() > 0){             // we don't merge discounted orderlines
                return false;
            }else if(this.price !== orderline.price){
                return false;
            }else{ 
                return true;
            }
        },
        // override хийж нэмэлтээр 2дахь тоо хэмжээ авах функцууд нэгтгэгч функцийг дарав
        merge: function(orderline){
            this.order.assert_editable();
            this.set_second_quantity(this.get_second_quantity() + orderline.get_second_quantity());
        },
        });
	return {
		PosModel: models.PosModel,
		Orderline: models.Orderline,
	};
});