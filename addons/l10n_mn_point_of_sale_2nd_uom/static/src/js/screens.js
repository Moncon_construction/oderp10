odoo.define('l10n_mn_point_of_sale_2nd_uom.screens', function (require) {
"use strict";
var core = require('web.core');
var Model = require('web.Model');
var utils = require('web.utils');

var actionPadScreen = require('point_of_sale.screens').OrderWidget;
// Гараас утга авах хэсэгт сонгосон бараанд 2дахь хэмжих
// нэгжийн тоо хэмжээг авах хэсгийг удамшуулж нөхцөл нэмэв
actionPadScreen.include({
    template:'OrderWidget',
    set_value: function(val) {
        this._super();
    	var order = this.pos.get_order();
    	if (order.get_selected_orderline()) {
            var mode = this.numpad_state.get('mode');
            if( mode === 'quantity'){
                order.get_selected_orderline().set_quantity(val);
            }else if( mode === 'discount'){
                order.get_selected_orderline().set_discount(val);
            }else if( mode === 'price'){
                order.get_selected_orderline().set_unit_price(val);
            }else if( mode === 'second_uom_qty'){
                order.get_selected_orderline().set_second_quantity(val);
            }
    	}
    },
    });

var productScreen = require('point_of_sale.screens').ProductScreenWidget;
// Нэвтэрсэн хэрэглэгч 2дахь хэмжих нэгжийн борлуулалт хийх эрхтэй бол
// 2дахь хэмжих нэгжийн товч харах буюу ашиглах эсэхийг шийдэх функц 
productScreen.include({
    start: function () {
        this._super();
        var custom_model = new Model('pos.order');        
        custom_model.call('is_group_user_of_second_uom').then(function (val) {
            var button = document.getElementById('second_uom_button');
            if (val == false){
                    button.style.display = "none";
            }else{
                button.style.display = 'block';
            }
        }
    )
}
});

});