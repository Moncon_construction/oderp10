from odoo import fields, models


class PosConfig(models.Model):
    _inherit = 'pos.config'

    analytic_account_id = fields.Many2one('account.analytic.account', string='Analytic account')
    cost_center = fields.Selection(related='company_id.cost_center', readonly=True, string='Cost Center')
