# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.tools import float_is_zero
from odoo.exceptions import UserError


class PosOrder(models.Model):
    _inherit = "pos.order"

    def _prepare_analytic_account(self, line):
        # Өртгийн төвөөс хамааран шинжилгээний дансыг сонгодог болгов
        config_id = line.order_id.session_id.config_id
        analytic_account_id = False
        
        if config_id.cost_center in ('department', 'warehouse', 'sales_team', 'project', 'technic', 'contract'):
            analytic_account_id = config_id.analytic_account_id
        elif line.product_id:
            if config_id.cost_center == 'brand':
                analytic_account_id = line.product_id.sudo().brand_name.analytic_account_id or False
            elif config_id.cost_center == 'product_categ':
                analytic_account_id = line.product_id.sudo().categ_id.analytic_account_id or False
        return analytic_account_id.id if analytic_account_id else False

    def _payment_fields(self, ui_paymentline):
        res = super(PosOrder, self)._payment_fields(ui_paymentline)

        # update analytic account
        res.update({
            'analytic_account_id': ui_paymentline['analytic_account_id'],
        })
        return res

    @api.model
    def _process_order(self, pos_order):
        # @Override
        prec_acc = self.env['decimal.precision'].precision_get('Account')
        pos_session = self.env['pos.session'].browse(pos_order['pos_session_id'])
        if pos_session.state == 'closing_control' or pos_session.state == 'closed':
            pos_order['pos_session_id'] = self._get_valid_session(pos_order).id
        order = self.create(self._order_fields(pos_order))
        journal_ids = set()
        # ### BEGIN ####
        '''
            Борлуулалтын цэгээс хариулт мөнгөний дүнгээр
            төлбөрийн буцаалт үүсгэхгүй тохиргоог шалгах
        '''
        if order['company_id'].non_create_move_for_cash_change:
            # set last cash payment
            lst_cash_pmnt = False
            for payment in pos_order['statement_ids']:
                if payment[2]['journal_id']:
                    cash_journal = self.env['account.journal'].search([
                        ('type', '=', 'cash'),
                        ('id', '=', payment[2]['journal_id']),
                    ])
                    if cash_journal:
                        lst_cash_pmnt = payment
                        
            for payments in pos_order['statement_ids']:
                # deduct return amount from cash payment without create bank_statement_line from return amount
                if not float_is_zero(pos_order['amount_return'], prec_acc):
                    # check payment is last cash or not: if true deduct return amount from it
                    if lst_cash_pmnt and lst_cash_pmnt == payments:
                        payments[2]['amount'] = payments[2]['amount'] - pos_order['amount_return']
                if not float_is_zero(payments[2]['amount'], precision_digits=prec_acc):
                    # ############ BEGIN CHANGE ###############
                    # Add analytic account
                    payments[2].update({'analytic_account_id': pos_session.config_id.analytic_account_id.id, })
                    # ############# END CHANGE ################
                    order.add_payment(self._payment_fields(payments[2]))
                journal_ids.add(payments[2]['journal_id'])
            if pos_session.sequence_number <= pos_order['sequence_number']:
                pos_session.write({'sequence_number': pos_order['sequence_number'] + 1})
                pos_session.refresh()

        else:
            # ### END ####
            for payments in pos_order['statement_ids']:
                if not float_is_zero(payments[2]['amount'], precision_digits=prec_acc):
                    # ############ BEGIN CHANGE ###############
                    # Add analytic account
                    payments[2].update({'analytic_account_id': pos_session.config_id.analytic_account_id.id, })
                    # ############# END CHANGE ################
                    order.add_payment(self._payment_fields(payments[2]))
                journal_ids.add(payments[2]['journal_id'])

            if pos_session.sequence_number <= pos_order['sequence_number']:
                pos_session.write({'sequence_number': pos_order['sequence_number'] + 1})
                pos_session.refresh()

            if not float_is_zero(pos_order['amount_return'], prec_acc):
                cash_journal_id = pos_session.cash_journal_id.id
                if not cash_journal_id:
                    # Select for change one of the cash journals used in this
                    # payment
                    cash_journal = self.env['account.journal'].search([
                        ('type', '=', 'cash'),
                        ('id', 'in', list(journal_ids)),
                    ], limit=1)
                    if not cash_journal:
                        # If none, select for change one of the cash journals of the POS
                        # This is used for example when a customer pays by credit card
                        # an amount higher than total amount of the order and gets cash back
                        cash_journal = [statement.journal_id for statement in pos_session.statement_ids if statement.journal_id.type == 'cash']
                        if not cash_journal:
                            raise UserError(_("No cash statement found for this session. Unable to record returned cash."))
                    cash_journal_id = cash_journal[0].id
                order.add_payment({
                    'amount': -pos_order['amount_return'],
                    'payment_date': fields.Datetime.now(),
                    'payment_name': _('return'),
                    'journal': cash_journal_id,
                    # ############ BEGIN CHANGE ###############
                    # Add analytic account
                    'analytic_account_id': pos_session.config_id.analytic_account_id.id,
                    # ############# END CHANGE ################
                })
        return order

    def create_move_by_qry(self, move, grouped_data):
        # create account.move.lines using postgresql query
        if move:
            company_id = move.company_id.id
            journal_id = move.journal_id.id
            move_id = move.id
            ref = move.ref
            move_date = move.date or datetime.now()
            for group_key, group_data in grouped_data.iteritems():
                for value in group_data:
                    # set partner_id when account can reconcile
                    account = self.env['account.account'].browse(value['account_id'])
                    order_for_partner = self[0] if self and len(self) > 0 else self
                    order_seller_id = order_for_partner.session_id.user_id.partner_id.id if order_for_partner.session_id and order_for_partner.session_id.user_id and order_for_partner.session_id.user_id.partner_id else False
                    if account.reconcile and order_seller_id and ('partner_id' not in value.keys() or ('partner_id' in value.keys() and not value['partner_id'])):
                        value['partner_id'] = order_seller_id
                        
                    uid = self.env.uid
                    date = fields.Datetime.now()
                    
                    self.env.cr.execute(
                    """
                        INSERT INTO account_move_line (create_uid, create_date, write_uid, write_date, company_id, journal_id, move_id, ref, name, quantity, product_id, account_id, analytic_account_id, credit, debit, partner_id, tax_line_id, currency_rate, date_maturity, date, register_date) 
                        VALUES (%s, '%s', %s, '%s', %s, %s, %s, '%s', '%s', %s, %s, %s, %s, %s, %s, %s, %s, 1.0, '%s', '%s', '%s')
                        RETURNING id 
                    """ %(uid, date, uid, date, company_id, journal_id, move_id, ref,self.get_qry_value(value, 'name'),self.get_qry_value(value, 'quantity'),self.get_qry_value(value, 'product_id'),
                         self.get_qry_value(value, 'account_id'),self.get_qry_value(value, 'analytic_account_id'),
                         self.get_qry_value(value, 'credit'),self.get_qry_value(value, 'debit'),
                         self.get_qry_value(value, 'partner_id'),self.get_qry_value(value, 'tax_line_id'),
                          str(move_date) if move_date else 'NULL', str(move_date) if move_date else 'NULL', str(move_date) if move_date else 'NULL')
                    )
                    new_move_line_id = self.env.cr.fetchone()[0]

                    # if account.move.lines object has tax, then create linked module's object for it
                    insert_qry = ""
                    if new_move_line_id and 'tax_ids' in value.keys():
                        tax_ids = value['tax_ids'][0][2]
                        if tax_ids and len(tax_ids) > 0:
                            uniq_tax_ids = []
                            for tax_id in tax_ids:
                                if tax_id not in uniq_tax_ids:
                                    uniq_tax_ids.append(tax_id)

                            insert_qry = "INSERT INTO account_move_line_account_tax_rel (account_move_line_id, account_tax_id) VALUES "
                            for tax_id in uniq_tax_ids:
                                if tax_id == uniq_tax_ids[0]:
                                    insert_qry += "(" + str(new_move_line_id) + ", " + str(tax_id) + ")"
                                else:
                                    insert_qry += ", (" + str(new_move_line_id) + ", " + str(tax_id) + ")"

                            self.env.cr.execute(insert_qry)
                    if new_move_line_id and 'analytic_account_id' in value.keys() and value['analytic_account_id']:
                        new_move_line = self.env['account.move.line'].browse([new_move_line_id])
                        new_move_line.create_analytic_lines()

            move.sudo().post()
        ################## END: Сэшн хаахад хэтэрхий удаж байсан тул qry ашиглаж журналын мөр үүсгэдэг болгов ################

        return move

class PosOrderLine(models.Model):
    _inherit = 'pos.order.line'

    analytic_account_id = fields.Many2one('account.analytic.account', string='Analytic account')

    @api.model
    def create(self, values):
        res = super(PosOrderLine, self).create(values)

        # update analytic account
        order_id = self.order_id.browse(values['order_id'])
        res.update({
            'analytic_account_id': order_id._prepare_analytic_account(res)
        })
         
        return res
