# -*- coding: utf-8 -*-

from odoo import fields, models


class StockMove(models.Model):
    _inherit = "stock.move"

    def _prepare_account_move_line(self, qty, cost, credit_account_id, debit_account_id):
        res = super(StockMove, self)._prepare_account_move_line(qty, cost, credit_account_id, debit_account_id)

        # update analytic account
        analytic_account_id = self.order_line_id.analytic_account_id if self.order_line_id and self.order_line_id.analytic_account_id else False
        if analytic_account_id:
            for i in res:
                if i[2]['debit'] != 0:
                    i[2]['analytic_account_id'] = analytic_account_id.id

        return res
