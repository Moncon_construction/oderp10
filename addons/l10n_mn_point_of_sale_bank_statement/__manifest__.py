# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Point of Sale Bank Statement",
    'version': '2.1',
    'author': "Asterisk Technologies LLC",
    'sequence': 15,
    'website': 'http://asterisk-tech.mn',
    'depends': [
        'l10n_mn_point_of_sale',
    ],
    'category': 'Mongolian Modules',
    'description': """
       Mongolian Point of Sale Bank Statement
    """,
    'data': [],
    'qweb': ['static/src/xml/pos.xml'],
    'application': True,
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
