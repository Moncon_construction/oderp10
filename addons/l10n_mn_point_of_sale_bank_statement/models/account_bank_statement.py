# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.addons.account.wizard.pos_box import CashBox
from odoo.tools.misc import formatLang
import logging
_logger = logging.getLogger(__name__)

class AccountBankStatement(models.Model):
    _inherit = 'account.bank.statement'

    total_entry_encoding = fields.Monetary('Transactions Subtotal', compute='_end_balance1', help="Total of transaction lines.")
    balance_end = fields.Monetary('Computed Balance', compute='_end_balance1', help='Balance as calculated based on Opening Balance and transaction lines')
    difference = fields.Monetary(compute='_end_balance1', help="Difference between the computed ending balance and the specified ending balance.")

    @api.one
    @api.depends('line_ids', 'balance_start', 'line_ids.amount')
    def _end_balance(self):
        _logger.info(_('Od10 Start: Bank statement end balance function'))
        amount_income = amount_outcome = 0
        amount_income += sum(lines.amount if lines.amount >= 0 else 0 for lines in self.line_ids)
        amount_outcome += sum(lines.amount if lines.amount < 0 else 0 for lines in self.line_ids)
        self.balance_end_income = amount_income
        self.balance_end_outcome = abs(amount_outcome)
        _logger.info(_('Od10 Finished: Bank statement end balance function'))

    @api.one
    @api.depends('balance_start', 'balance_end_real')
    def _end_balance1(self):
        _logger.info(_('Start: Bank statement end balance function'))
        self.total_entry_encoding = self.balance_end_income - self.balance_end_outcome
        self.balance_end = self.balance_start + self.total_entry_encoding
        self.difference = self.balance_end_real - self.balance_end
        _logger.info(_('Finished: Bank statement end balance function'))

    @api.multi
    def calculate_after_bank_statements(self):
        for statement in self:
            income = outcome = 0
            # Тухайн банкны хуулгын дүнгүүдийг мөн тооцоолдог болсон
            for line in statement.line_ids:
                if line.amount >= 0:
                    income += line.amount
                else:
                    outcome += line.amount
            statement.balance_end_income = income
            statement.balance_end_outcome = abs(outcome)
            statement.balance_end_real = statement.balance_start + income - abs(outcome)
            res = super(AccountBankStatement, self).calculate_after_bank_statements()
        return res