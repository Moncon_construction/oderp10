# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Point of Sale Change",
    'version': '1.0',
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'depends': [
        'l10n_mn_point_of_sale', 'l10n_mn_point_of_sale_analytic'
    ],
    'category': 'Mongolian Modules',
    'description': """
       Посоор буруу бичигдсэн барааг солих
    """,
    'data': [
        'wizard/pos_order_product_change_view.xml',
        'views/pos_order_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
