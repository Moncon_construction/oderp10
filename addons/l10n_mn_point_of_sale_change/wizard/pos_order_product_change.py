# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools import float_compare, float_round


class PosOrderProductChange(models.TransientModel):
    _name = 'pos.order.product.change'
    _description = 'POS Order Product Change'

    product_id = fields.Many2one('product.product', 'Product', required=True)
    line_id = fields.Many2one('pos.order.line', 'POS Order Line', required=True)

    @api.model
    def default_get(self, fields):
        if len(self.env.context.get('active_ids', list())) > 1:
            raise UserError(_('You may only change one order line!'))
        res = super(PosOrderProductChange, self).default_get(fields)
        line = self.env['pos.order.line'].browse(self.env.context.get('active_id'))
        if line:
            res.update({'line_id': line.id})
        return res

    def change_product(self):
        obj = self.browse(self.id)
        operations = self.env['stock.pack.operation']
        line_obj = self.env['account.move.line']
        if obj.line_id.product_id == obj.product_id:
            raise UserError(_('Product no change!'))
        prod = obj.line_id.product_id
        # picking олж буй хэсэг
        if obj.line_id.order_id and obj.line_id.order_id.get_picking_ids():
            for picking in obj.line_id.order_id.get_picking_ids():
                # тухайн барааны мөрийг буюу stock_move олж буй хэсэг
                moves = self.env['stock.move'].search([('picking_id', '=', picking.id), ('product_id', '=', prod.id), ('order_line_id', '=', obj.line_id.id)])
                # олсон stock_move давталт хийж буй хэсэг
                for move in moves:
                    # пос-н бараа солих мөрийн тоо ширхэг stock_move тоо ширхэгтэй тэнцүү тохиолдолд
                    if move.product_qty == obj.line_id.qty:
                        if obj.line_id.order_id and obj.line_id.order_id.account_move:
                            account_move = obj.line_id.order_id.account_move
                            # account_move мөчлөг шалгах
                            period = self.env['account.period'].search([('date_start', '<=', account_move.date),
                                                                        ('date_stop', '>=', account_move.date),
                                                                        ('company_id', '=', account_move.company_id.id)], limit=1)
                            if period and period.state == 'done':
                                raise UserError(_('You cannot cancel entry in closed period!'))
                            lines = self.env['account.move.line'].search([('move_id', '=', account_move.id), ('product_id', '=', prod.id)])
                            for line in lines:
                                line.write({'name': obj.product_id.name, 'product_id': obj.product_id.id})
                        # stock_move ноорог төлөвтэй болгон мэдээлэл засаж байгаа хэсэг
                        self._cr.execute('''
                            DELETE FROM account_move_line
                            WHERE stock_move_id = ''' + str(move.id))
                        move.write({'state': 'draft'})
                        move.write({'product_id': obj.product_id.id, 'product_uom': obj.product_id.uom_id.id, 'name': obj.product_id.name_get()[0][1]})
                        qty = 0
                        # үүссэн stock_quant-г устгах эсвэл байрлалыг сольж байгаа хэсэг
                        for quant in move.quant_ids:
                            if quant and quant.location_id and quant.location_id.id == move.location_dest_id.id:
                                qty += quant.qty
                            elif quant and quant.location_id and quant.location_id.id == move.location_id.id and quant.qty < 0:
                                qty += quant.qty
                        for quant in move.quant_ids:
                            if quant.qty == qty:
                                quant.sudo().write({'location_id': move.location_id.id})
                                move.write({'quant_ids': [(3, quant.id)]})
                            else:
                                self.env.cr.execute('DELETE FROM stock_quant WHERE id=%s', (quant.id,))
                                # quant.unlink()
                        move.action_assign()
                        move.with_context(force_period_date=move.date).action_done()
                        cost = 0
                        # барааны өртгийг олох хэсэг
                        for quant in move.quant_ids:
                            if quant and quant.location_id and quant.location_id.id == move.location_dest_id.id:
                                cost += quant.cost * quant.qty
                        # stock_pack_operation-ий бараа болон хэмжих нэгжийг засах хэсэг
                        for link in move.linked_move_operation_ids:
                            operations |= link.operation_id
                        for ops in operations:
                            ops.write({'product_id': obj.product_id.id, 'product_uom_id': obj.product_id.uom_id.id})
                        # тухайн stock_move-тэй холбогдсон account_move_line-ы дүн болон барааг засах хэсэг
                        lines = line_obj.search([('stock_move_id', '=', move.id)])
                        for line in lines:
                            period = self.env['account.period'].search([('date_start', '<=', line.date),
                                                                        ('date_stop', '>=', line.date),
                                                                        ('company_id', '=', line.company_id.id)], limit=1)
                            if period and period.state == 'done':
                                raise UserError(_('You cannot cancel entry in closed period!'))
                            if line.debit > 0:
                                line.with_context(check_move_validity=False, pos_order_product_change=True).write({'debit': cost, 'product_id': obj.product_id.id, 'name': obj.product_id.name})
                            elif line.credit > 0:
                                line.with_context(check_move_validity=False, pos_order_product_change=True).write({'credit': cost, 'product_id': obj.product_id.id, 'name': obj.product_id.name})
        # pos_order_line бараа солих хэсэг
        obj.line_id.product_id = obj.product_id
