# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Point of Sale, Pharmacy Pos & Stock Account Cost For Each Warehouse",
    'version': '1.0',
    'depends': ['l10n_mn_point_of_sale','l10n_mn_stock_account_cost_for_each_wh'],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Modules',
    'description': """
        Агуулах бүрээрх өртөг хөтлөх тохиолдолд дансыг агуулахын тохиргоон дээрээс авна.
    """,
    'data': [
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}
