# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import logging
import json
import requests
import time
from datetime import timedelta
from functools import partial

import psycopg2
import pytz

from odoo import api, fields, models, tools, _
from odoo.tools import float_is_zero
from odoo.exceptions import UserError
from odoo.http import request
import odoo.addons.decimal_precision as dp
from odoo.exceptions import ValidationError  # @UnresolvedImport
from itertools import product
_logger = logging.getLogger(__name__)


class PosOrder(models.Model):
    _inherit = "pos.order"

    def get_income_account(self, line):
        # Search for the income account
        #  Агуулах бүрээр өртөг хөтлөх үед ПОС-н хаалт хийхдээ орлогын дансыг агуулахын тохиргооноос авах
        income_account = False
        if line.order_id.config_id.picking_type_id.warehouse_id.stock_account_income_id.id:
            income_account = line.order_id.config_id.picking_type_id.warehouse_id.stock_account_income_id.id
        else:
            raise UserError(_('Please define income '
                              'account for this warehouse: "%s"')
                            % (line.order_id.config_id.picking_type_id.warehouse_id.name))
        return income_account
    
# Борлуулалтын цэгээс нэхэмжлэлийн борлуулалт хийхэд нэхэмжлэлийн мөрд агуулахын данс авах засвар хийж функц дарав.
    def _action_create_invoice_line(self, line=False, invoice_id=False):
        InvoiceLine = self.env['account.invoice.line']
        inv_name = line.product_id.name_get()[0][1]
        income_account = line.order_id.config_id.picking_type_id.warehouse_id.stock_account_income_id.id
        inv_line = {
            'invoice_id': invoice_id,
            'product_id': line.product_id.id,
            'account_id': income_account,
            'quantity': line.qty,
            'account_analytic_id': self._prepare_analytic_account(line),
            'name': inv_name,
        }
        # Oldlin trick
        invoice_line = InvoiceLine.sudo().new(inv_line)
        invoice_line._onchange_product_id()
        invoice_line.invoice_line_tax_ids = invoice_line.invoice_line_tax_ids.filtered(lambda t: t.company_id.id == line.order_id.company_id.id).ids
        fiscal_position_id = line.order_id.fiscal_position_id
        if fiscal_position_id:
            invoice_line.invoice_line_tax_ids = fiscal_position_id.map_tax(invoice_line.invoice_line_tax_ids, line.product_id, line.order_id.partner_id)
        invoice_line.invoice_line_tax_ids = invoice_line.invoice_line_tax_ids.ids
        # We convert a new id object back to a dictionary to write to
        # bridge between old and new api
        inv_line = invoice_line._convert_to_write({name: invoice_line[name] for name in invoice_line._cache})
        inv_line.update(price_unit=line.price_unit, discount=line.discount)
        return InvoiceLine.sudo().create(inv_line)