# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Point of sale hide amount",
    'version': '2.1',
    'depends': ['l10n_mn_point_of_sale', 'l10n_mn_account','l10n_mn_stock_account'],
    'author': "Asterisk Technologies LLC",
    'sequence': 15,
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Mongolian Point of Sale hide amount
    """,
    'data': [

        'security/ir.model.access.csv',
        'views/pos_session_views.xml',




    ],
    'qweb': ['static/src/xml/pos.xml'],
    'application': True,
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}