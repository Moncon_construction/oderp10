# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Point of Sale Mrp",
    'version': '1.0',
    'depends': ['l10n_mn_mrp', 'l10n_mn_point_of_sale'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Mongolian Point of Sale
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/mrp_pos_view.xml',
        'views/pos_template.xml',
        'views/pos_config_view.xml',
        'views/product_view.xml'
    ],
    'qweb': ['static/src/xml/pos.xml'],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}