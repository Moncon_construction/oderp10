# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError

class MrpPos(models.Model):
    _name = 'mrp.pos'
    _order = 'date desc,id'
    
    @api.multi
    def _get_default_location(self):
        return self.env['stock.warehouse'].search([('company_id', '=', self.env.user.company_id.id)], limit=1).lot_stock_id


    name = fields.Char('name')
    product_id = fields.Many2one('product.product','Product')
    quantity = fields.Float('Quantity')
    table_info = fields.Char('Table Info')
    note = fields.Char('Note')
    state = fields.Selection([('draft','Draft'),
                            ('progress','Progress'),
                            ('done','Done'),
                            ('cancel','Cancelled')],
                            string='Status', default='draft')
    owner_id = fields.Many2one('res.users','Owner')
    production_id = fields.Many2one('mrp.production','Production',compute='_compute_production')
    location_src_id = fields.Many2one('stock.location','Src Location') 
    location_dest_id = fields.Many2one('stock.location','Destination Location') 
    date = fields.Datetime('Date', copy=False, default=fields.Datetime.now)
    
    @api.multi
    def _compute_production(self):
#        Үйлдвэрийн захиалга харуулах функц
        res = []
        for line in self:
            res = self.env['mrp.production'].search([('origin','=',line.name),('product_id.name','=',line.product_id.name)],limit=1)
            if res:
                line.production_id = res.id
    
    @api.multi
    def to_start(self):
        res = self.env['mrp.production'].search([('origin','=',self.name),('product_id.name','=',self.product_id.name)])
        for i in self:
            if i.owner_id:
                for line in res:
                    line.write({'state': 'progress',
                                'user_id':self.owner_id.id})
                self.state = 'progress'
            else:
                UserError(_('Start before must be choose user!'))
    @api.multi
    def to_stop(self):
        mrp_obj = self.env['mrp.production']
        res = mrp_obj.search([('origin','=',self.name),('product_id.name','=',self.product_id.name)])
        if self.state == 'progress':
            for line in res:
                line.write({'state': 'done'})
                for row in line.move_finished_ids :
                    row.write({'quantity_done':self.quantity})
#             ПОС-с үүссэн ҮЗ ДУУСАХ товч дарах үед санхүүгийн бичилт хийх функц
            production_id = mrp_obj.search([('id', '=', res.id)])
            production_id.button_mark_done()
            self.state = 'done' 
        
    @api.model
    def create(self, vals):
        return super(MrpPos, self).create(vals) 
    
    @api.multi
    def unlink(self):
    #Pos ын захиалгыг устгаж, үйлдвэрийн захиалгыг цуцалсан төлөвт оруулах
        res = self.env['mrp.production']
        for line in self:
            production_obj = res.search([('origin','=',line.name),('product_id.name','=',line.product_id.name)])
            production_obj.write({'state':'cancel'})
        return super(MrpPos, self).unlink()
       