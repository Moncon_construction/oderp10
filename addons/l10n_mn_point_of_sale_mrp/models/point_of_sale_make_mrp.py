from odoo import models, fields, api
from odoo.exceptions import Warning
 
class MrpProduction(models.Model):
    _inherit = 'mrp.production'
    
    is_origin = fields.Boolean(default=False)
    
    @api.multi
    def create_mrp_from_pos(self, products):
        product_quantity = 0
        location_src_id = 1
        location_dest_id = 1
        mrp_obj = self.env['mrp.production']
        config_obj = self.env['pos.config'].search([('id', '=', self.id)])
        if config_obj:
            location_src_id = config_obj.material_location_id.id
            location_dest_id = config_obj.stock_location_id.id
        product_ids = []
        if products:
            for product in products:
                flag = 1
                if product_ids:
                    for product_id in product_ids:
                        if product_id['id'] == product['id']:
                            product_id['qty'] = product['qty']
                            flag = 0
                        product_quantity = product_id['qty']
                if flag:
                    product_ids.append(product)
            for prod in product_ids:
                if prod['qty'] > 0:
                    product = self.env['product.template'].search([('id', '=', prod['product_tmpl_id'])])
                    bom_count = self.env['mrp.bom'].search([('product_tmpl_id', '=', prod['product_tmpl_id'])])
                    if bom_count:
                        bom_temp = self.env['mrp.bom'].search([('product_tmpl_id', '=', prod['product_tmpl_id']),
                                                               ('product_id', '=', False)])
                        bom_prod = self.env['mrp.bom'].search([('product_id', '=', prod['id'])])
                        if bom_prod:
                            bom = bom_prod[0]
                        elif bom_temp:
                            bom = bom_temp[0]
                        else:
                            bom = []
                            print "This product variants have no exact BOM"
                        if bom:
                            vals = {
                                'origin': 'POS-' + prod['pos_reference'],
                                'state': 'confirmed',
                                'product_id': prod['id'],
                                'product_tmpl_id': prod['product_tmpl_id'],
                                'product_uom_id': prod['uom_id'],
                                'product_qty': prod['qty'],
                                'bom_id': bom.id,
                                'is_origin': True,
                                'location_src_id':location_src_id,
                                'location_dest_id':location_dest_id,
                            }
                        res = super(MrpProduction, self).create(vals)
                        
                        production_id = mrp_obj.search([('id', '=', res.id)])
                        if production_id.bom_id.product_tmpl_id.to_make_mrp and not production_id.bom_id.product_tmpl_id.to_make_res:
                            production_id.create_account_journal(is_button_confirm=True, is_button_done=True) 
                        elif production_id.bom_id.product_tmpl_id.to_make_mrp and production_id.bom_id.product_tmpl_id.to_make_res:
                            production_id.create_account_journal(is_button_confirm=True, is_button_done=False) 

                        if product.to_make_mrp == True and product.to_make_res == False:
                            if res:
                                for line in res:
                                    line.write({'state': 'done'})
                                    for row in line.move_finished_ids:
                                        row.write({'quantity_done': product_quantity})
        return True
    
 
class MrpPos(models.Model):
    _inherit = 'mrp.pos'
    
    @api.multi
    def create_mrp_from_pos(self, products):
        config_obj = self.env['pos.config'].search([('id', '=', self.id)])
        employee = self.env['hr.employee'].search([('user_id', '=', self.env.uid)])
        if config_obj:
            location_src_id = config_obj.material_location_id
            location_dest_id = config_obj.stock_location_id
        product_ids = []
        if products:
            for product in products:
                flag = 1
                if product_ids:
                    for product_id in product_ids:
                        if product_id['id'] == product['id']:
                            product_id['qty'] = product['qty']
                            flag = 0
                if flag:
                    product_ids.append(product)
            for prod in product_ids:
                if prod['qty'] > 0:
                    product = self.env['product.template'].search([('id', '=', prod['product_tmpl_id'])])
                    bom_count = self.env['mrp.bom'].search([('product_tmpl_id', '=', prod['product_tmpl_id'])])
                    if bom_count:
                        bom_temp = self.env['mrp.bom'].search([('product_tmpl_id', '=', prod['product_tmpl_id']),
                                                               ('product_id', '=', False)])
                        bom_prod = self.env['mrp.bom'].search([('product_id', '=', prod['id'])])
                        if bom_prod:
                            bom = bom_prod[0]
                        elif bom_temp:
                            bom = bom_temp[0]
                        else:
                            bom = []
                            print "This product variants have no exact BOM"
                        if bom:
                            vals = {
                                'name': 'POS-' + prod['pos_reference'] ,
                                'state': 'draft',
                                'product_id': prod['id'],
                                'quantity': prod['qty'],
                                'location_src_id':location_src_id.id,
                                'location_dest_id':location_dest_id.id,
                                'owner_id':employee.user_id.id,
                                'table_info': prod['table_info'],
                                'note': prod['note'],
                            }
                            if product.to_make_res == True:
                                self.sudo().create(vals)
        return True


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    to_make_res = fields.Boolean(string='To Create Restaurant Order')
    to_make_mrp = fields.Boolean(string='To Create MRP Order',
                                 help="Check if the product should be make mrp order")

    @api.onchange('to_make_mrp')
    def onchange_to_make_mrp(self):
        if self.to_make_mrp:
            if not self.bom_count:
                raise Warning('Please set Bill of Material for this product.')

    @api.onchange('to_make_res')
    def onchange_to_make_res(self):
        if self.to_make_res == True:
            self.to_make_mrp = True

    @api.model
    def create(self, vals):
        if 'to_make_res' in vals:
            if vals['to_make_res'] == True:
                vals['to_make_mrp'] = True
        return super(ProductTemplate, self).create(vals)

    @api.multi
    def write(self, vals):
        if 'to_make_res' in vals:
            if vals['to_make_res'] == True:
                vals['to_make_mrp'] = True
        return super(ProductTemplate, self).write(vals)


class ProductProduct(models.Model):
    _inherit = 'product.product'

    @api.onchange('to_make_mrp')
    def onchange_to_make_mrp(self):
        if self.to_make_mrp:
            if not self.bom_count:
                raise Warning('Please set Bill of Material for this product.')

    @api.onchange('to_make_res')
    def onchange_to_make_res(self):
        if self.to_make_res == True:
            self.to_make_mrp = True
