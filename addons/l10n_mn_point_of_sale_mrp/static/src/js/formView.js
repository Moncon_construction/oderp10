odoo.define('l10n_mn_point_of_sale_mrp.formView', function (require) {
    "use strict";
    var FormView = require('web.FormView');

    FormView.include({
        do_show: function () {
            if (this.model != 'mrp.pos' && window.mrp_refresh) {
                console.log("Clear FormView");
                clearInterval(window.mrp_refresh);
                window.mrp_refresh = undefined;
            }
            return this._super.apply(this, arguments);
        }
    });
});