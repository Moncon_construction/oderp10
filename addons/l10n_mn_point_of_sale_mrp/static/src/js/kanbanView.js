odoo.define('l10n_mn_point_of_sale_mrp.kanbanView', function (require) {
    "use strict";
    var kanbanView = require('web_kanban.KanbanView');

    kanbanView.include({
        do_show: function () {
            if (window.mrp_refresh) {
                console.log("Clear KANBAN");
                clearInterval(window.mrp_refresh);
                window.mrp_refresh = undefined;
            }
            return this._super.apply(this, arguments);
        }
    });
});