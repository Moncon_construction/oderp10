odoo.define('l10n_mn_point_of_sale_mrp.listView', function (require) {
    "use strict";
    var ListView = require('web.ListView');

    ListView.include({
        do_show: function () {
            if (this.model == 'mrp.pos') {
                if (window.mrp_refresh == undefined) {
                    console.log("START TICKING");
                    window.mrp_refresh = setInterval(function () {
                        console.log("Ticking ");
                        $(".o_cp_switch_list").click();
                    }, 30 * 1000);
                }
            } else {
                if (window.mrp_refresh) {
                    console.log("Clear LIST VIEW");
                    clearInterval(window.mrp_refresh);
                    window.mrp_refresh = undefined;
                }
            }
            return this._super.apply(this, arguments);
        }
    });
});