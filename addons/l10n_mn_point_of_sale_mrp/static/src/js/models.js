odoo.define('l10n_mn_point_of_sale_mrp.models', function (require) {
    "use strict";

    var pos_model = require('point_of_sale.models');

    var models = pos_model.PosModel.prototype.models;

    for (var i = 0; i < models.length; i++) {
        var model = models[i];
        if (model.model === 'product.product') {
            model.fields.push('to_make_mrp');
            model.fields.push('to_make_res');
        }
    }
});