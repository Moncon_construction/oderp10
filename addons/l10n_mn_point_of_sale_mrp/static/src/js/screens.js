odoo.define('l10n_mn_point_of_sale_mrp.screens', function (require) {
    "use strict";

    var Model = require('web.DataModel');
    var models = require('point_of_sale.models');
    var posScreens = require('point_of_sale.screens');

    var actionpadWidget = posScreens.ActionpadWidget;
    var orderWidget = posScreens.OrderWidget;

    actionpadWidget.include({
        renderElement: function () {
            var self = this;
            this._super();
            this.$('.mrp-order').click(function () {
                var order = self.pos.get_order();
                var order_line = order.orderlines.models;
                var list_product = [];
                var make_res;

                if (order.hasChangesToPrint()) {
                    var changes = order.computeChanges();

                    for (var i = 0; i < changes.new.length; i++) {

                        if (changes.new[i].to_make_res && changes.new[i].to_make_mrp){
                            make_res = changes.new[i].to_make_res;
                            var product = self.pos.db.get_product_by_id(changes.new[i].id);

                            var product_dict = {
                                'id': changes.new[i].id,
                                'qty': changes.new[i].qty,
                                'product_tmpl_id': product.product_tmpl_id,
                                'pos_reference': order.name,
                                'uom_id': product.uom_id[0],
                                'table_info': (order.table.floor.name + "," + order.table.name),
                                'note': changes.new[i].note,
                            };
                            list_product.push(product_dict);
                        }

                        else if (changes.new[i].to_make_mrp) {
                            var product = self.pos.db.get_product_by_id(changes.new[i].id);

                            var product_dict = {
                                'id': changes.new[i].id,
                                'qty': changes.new[i].qty,
                                'product_tmpl_id': product.product_tmpl_id,
                                'pos_reference': order.name,
                                'uom_id': product.uom_id[0],
                                'table_info': (order.table.floor.name + "," + order.table.name),
                                'note': changes.new[i].note,
                            };
                            list_product.push(product_dict);
                        }

                    }

                    if (list_product.length) {
                        new Model("mrp.production")
                            .call("create_mrp_from_pos", [1, list_product]);
                        new Model("mrp.pos")
                            .call("create_mrp_from_pos", [1, list_product]);
                    }
                    order.saveChanges();
                }
            });
        }
    });

    var _super_orderline = models.Orderline.prototype;
    models.Orderline = models.Orderline.extend({
        initialize: function () {
            _super_orderline.initialize.apply(this, arguments);
            if (this.mrp_dirty === undefined) {
                this.mrp_dirty = this.printable() || undefined;
            }
        },
        printable: function () {
            return this.product.to_make_mrp;
        },
        init_from_JSON: function (json) {
            _super_orderline.init_from_JSON.apply(this, arguments);
            this.mrp_dirty = json.mrp_dirty;
        },
        export_as_JSON: function () {
            var json = _super_orderline.export_as_JSON.apply(this, arguments);
            json.mrp_dirty = this.mrp_dirty;
            return json;
        },
        set_quantity: function (quantity) {
            if (quantity !== this.quantity && this.printable()) {
                this.mrp_dirty = true;
            }
            _super_orderline.set_quantity.apply(this, arguments);
        },
        set_dirty: function (dirty) {
            this.mrp_dirty = dirty;
            this.trigger('change', this);
        },
    });

    orderWidget.include({
        render_orderline: function (orderline) {
            var node = this._super(orderline);
            if (orderline.mrp_dirty) {
                node.classList.add('dirty');
            }
            return node;
        },
        update_summary: function () {
            this._super();
            var changes = this.pos.get_order().hasChangesToPrint();
            $('.mrp-order').toggleClass('highlight', !!changes);
        },
    });

    var _super_order = models.Order.prototype;
    models.Order = models.Order.extend({
        build_line_resume: function () {
            var resume = {};
            this.orderlines.each(function (line) {
                if (line.mp_skip) {
                    return;
                }
                var line_hash = line.get_line_diff_hash();
                var qty = Number(line.get_quantity());
                var note = line.get_note();
                var product_id = line.get_product().id;
                var to_make_mrp = line.product.to_make_mrp;
                var to_make_res = line.product.to_make_res;

                if (typeof resume[line_hash] === 'undefined') {
                    resume[line_hash] = {
                        qty: qty,
                        note: note,
                        product_id: product_id,
                        product_name_wrapped: line.generate_wrapped_product_name(),
                        to_make_mrp: to_make_mrp,
                        to_make_res: to_make_res,
                    };
                } else {
                    resume[line_hash].qty += qty;
                }
            });
            return resume;
        },
        computeChanges: function () {
            var current_res = this.build_line_resume();
            var old_res = this.saved_resume || {};
            var json = this.export_as_JSON();
            var add = [];
            var rem = [];
            var line_hash;
            var old;

            for (line_hash in current_res) {
                var curr = current_res[line_hash];
                old = old_res[line_hash];

                if (typeof old === 'undefined') {
                    add.push({
                        'id': curr.product_id,
                        'name': this.pos.db.get_product_by_id(curr.product_id).display_name,
                        'name_wrapped': curr.product_name_wrapped,
                        'note': curr.note,
                        'qty': curr.qty,
                        'to_make_mrp': curr.to_make_mrp,
                        'to_make_res': curr.to_make_res,
                    });
                } else if (old.qty < curr.qty) {
                    add.push({
                        'id': curr.product_id,
                        'name': this.pos.db.get_product_by_id(curr.product_id).display_name,
                        'name_wrapped': curr.product_name_wrapped,
                        'note': curr.note,
                        'qty': curr.qty - old.qty,
                        'to_make_mrp': curr.to_make_mrp,
                        'to_make_res': curr.to_make_res,
                    });
                } else if (old.qty > curr.qty) {
                    rem.push({
                        'id': curr.product_id,
                        'name': this.pos.db.get_product_by_id(curr.product_id).display_name,
                        'name_wrapped': curr.product_name_wrapped,
                        'note': curr.note,
                        'qty': old.qty - curr.qty,
                        'to_make_mrp': curr.to_make_mrp,
                        'to_make_res': curr.to_make_res,
                    });
                }
            }

            for (line_hash in old_res) {
                if (typeof current_res[line_hash] === 'undefined') {
                    old = old_res[line_hash];
                    rem.push({
                        'id': old.product_id,
                        'name': this.pos.db.get_product_by_id(old.product_id).display_name,
                        'name_wrapped': old.product_name_wrapped,
                        'note': old.note,
                        'qty': old.qty,
                        'to_make_mrp': old.to_make_mrp,
                        'to_make_res': old.to_make_res,
                    });
                }
            }

            var self = this;

            var _add = [];
            var _rem = [];

            var i;
            for (i = 0; i < add.length; i++) {
                if (add[i].to_make_mrp) {
                    _add.push(add[i]);
                }
                if (add[i].to_make_res){
                    _add.push(add[i]);
                }
            }
            add = _add;

            for (i = 0; i < rem.length; i++) {
                if (rem[i].to_make_mrp) {
                    _rem.push(rem[i]);
                }
                if (rem[i].to_make_res){
                    _add.push(rem[i]);
                }
            }
            rem = _rem;

            var d = new Date();
            var hours = '' + d.getHours();
            hours = hours.length < 2 ? ('0' + hours) : hours;
            var minutes = '' + d.getMinutes();
            minutes = minutes.length < 2 ? ('0' + minutes) : minutes;

            return {
                'new': add,
                'cancelled': rem,
                'table': json.table || false,
                'floor': json.floor || false,
                'name': json.name || 'unknown order',
                'time': {
                    'hours': hours,
                    'minutes': minutes,
                },
            };
        },
        hasChangesToPrint: function () {
            var changes = this.computeChanges();
            if (changes.new.length > 0 || changes.cancelled.length > 0) {
                return true;
            }
            return false;
        },
    });
});