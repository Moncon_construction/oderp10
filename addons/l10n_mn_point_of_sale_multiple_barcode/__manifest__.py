# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'Point of Sale Multiple Barcode',
    'version': '1.0',
    'category': 'Mongolian Modules',
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'sequence': 15,
    'depends': [
        'l10n_mn_product_barcode',
        'l10n_mn_point_of_sale'],
    'description': """Manage the Multiple Product Barcode process in Odoo""",
    'data': [
        'views/point_of_sale_asset.xml'
    ],
    'demo': [
        ],
    'application': True,
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}
