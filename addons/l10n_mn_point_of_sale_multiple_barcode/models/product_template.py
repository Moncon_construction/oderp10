# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class ProductTemplate(models.Model):
    _inherit = 'product.template'
    barcode_str = fields.Char('Barcode', compute='get_barcode')

    @api.multi    
    def get_barcode(self):
        for line in self:
            self._cr.execute("SELECT barcode from product_template_barcode where product_id =  %s " %str(line.id))
            line.barcode_str = self._cr.fetchall()	
