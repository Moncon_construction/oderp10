odoo.define('l10n_mn_product_barcode.DB', function (require) {
	"use strict";

	var PosDB = require('point_of_sale.DB');
	PosDB.include({

        init: function(options){
            this._super(options);
        },

        /** 
			POS-с бараа хайхад зураасан код, дотоод код, болон нэмэлт баркодоор
			хайхад зориулагдсан.
        **/
        _product_search_string: function(product){
       		var str = product.display_name;
	        if (product.barcode) {
	            str += '|' + product.barcode;
	        }
	        if (product.default_code) {
	            str += '|' + product.default_code;
	        }
	        if (product.barcode_str){
	            str += '|' + product.barcode_str;
	        }
	        str  = product.id + ':' + str.replace(/:/g,'') + '\n';
	        return str;
	    }
    });

    return PosDB;
});
