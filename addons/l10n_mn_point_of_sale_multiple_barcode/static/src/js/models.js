odoo.define('l10n_mn_point_of_sale_multiple_barcode.models', function (require){
	"use strict";

	var utils = require('web.utils');
    var round_pr = utils.round_precision;
    var round_di = utils.round_decimals;
    var formats = require('web.formats');
    var models = require('point_of_sale.models');
    var _super_posmodel = models.PosModel.prototype;
    var _super_order = models.Order.prototype;
    models.PosModel = models.PosModel.extend({
        initialize: function (session, attributes) {
            // New code
            var product_model = _.find(this.models, function(model){
                return model.model === 'product.product';
            });
            product_model.fields.push('barcode_str');

            // Inheritance
            return _super_posmodel.initialize.call(this, session, attributes);
        }
    });

    /**
        barcode_str --> Нэмэлт зураасан кодын талбарыг нэмж оруулав.
    **/
    models.Order = models.Order.extend({
    	models: [{
            model:  'product.product',
            fields: ['display_name', 'list_price','price','pos_categ_id', 'taxes_id', 'barcode', 'default_code', 
                     'to_weight', 'uom_id', 'description_sale', 'description',
                     'product_tmpl_id','tracking', 'barcode_str'],
            order:  ['sequence','default_code','name'],
            domain: [['sale_ok','=',true],['available_in_pos','=',true]],
            context: function(self){ 
                return { pricelist: self.pricelist.id, display_default_code: false }; 
            },
            loaded: function(self, products){
                self.db.add_products(products);
            },
        }]
    });
});
