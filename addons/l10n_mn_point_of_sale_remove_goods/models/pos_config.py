# -*- coding: utf-8 -*-
from odoo import fields, models, _


class PosConfig(models.Model):
    _inherit = 'pos.config'


    #худалдагч эрхтэй хүнийг посын дэлгэцнд харуулхад ашиглагдах функц
    def _get_group_pos_salesperson(self):
        return self.env.ref('l10n_mn_point_of_sale.group_pos_salesperson')

    #худалдагч эрхтэй хүнийг посын дэлгэцнд харуулхад ашиглагдах талбар
    group_pos_salesperson_id = fields.Many2one('res.groups',string='Point of Sale salesperson Group',default=_get_group_pos_salesperson)

