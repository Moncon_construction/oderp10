# -*- coding: utf-8 -*-

import logging


from odoo import api, fields, models, tools, _
_logger = logging.getLogger(__name__)

class PosOrder(models.Model):
    _inherit = "pos.order"

    #пос ын дэлгэс дээр устгах товчыг групп тавьж нуусан функц
    @api.model
    def is_user(self, uid):
        return self.env['res.users'].search([('id', '=', uid)]).has_group('point_of_sale.group_pos_user')

    @api.model
    def is_user_screen(self):
        return self.env.user.has_group('point_of_sale.group_pos_user')

    @api.model
    def is_button_delete_screen(self):
        return self.env.user.has_group('point_of_sale.group_pos_user')

    @api.model
    def is_button_delete(self, uid):
        return self.env['res.users'].search([('id', '=', uid)]).has_group('point_of_sale.group_pos_user')

    @api.model
    def disc_button_screen(self):
        return self.env.user.has_group('point_of_sale.group_pos_user')

    @api.model
    def disc_button_gui(self, uid):
        return self.env['res.users'].search([('id', '=', uid)]).has_group('point_of_sale.group_pos_user')

    @api.model
    def is_price_button_screen(self):
        return self.env.user.has_group('point_of_sale.group_pos_user')

    @api.model
    def is_price_button(self, uid):
        return self.env['res.users'].search([('id', '=', uid)]).has_group('point_of_sale.group_pos_user')