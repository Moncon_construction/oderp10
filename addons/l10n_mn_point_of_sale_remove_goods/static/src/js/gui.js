odoo.define('l10n_mn_point_of_sale_remove_goods.gui', function (require) {
"use strict";
// this file contains the Gui, which is the pos 'controller'.
// It contains high level methods to manipulate the interface
// such as changing between screens, creating popups, etc.
//
// it is available to all pos objects trough the '.gui' field.

var core = require('web.core');
var Model = require('web.DataModel');
var formats = require('web.formats');
var session = require('web.session');
var gui = require('point_of_sale.gui').Gui;
var _t = core._t;



gui.include({
  // пос ны дэлгэц дээр худалдагч эрхтэй хүнд бараа хасах товч харагдахгүй харин менежер эрхтэй хүнд л харах эрх байна хэрэглэгч солигдоход товч гарч ирж, алга болох юм
  //override begin
    //хэрэглэгч сонгох функц
    select_user: function(options){
        options = options || {};
        var self = this;
        var def  = new $.Deferred();

        var list = [];
        for (var i = 0; i < this.pos.users.length; i++) {
            var user = this.pos.users[i];
            if (!options.only_managers || user.role === 'manager') {
                list.push({
                    'label': user.name,
                    'item':  user,
                });
            }
        }
        // хэрэглэгч сонгох хэсгийн харуулах сонгох хэсэг
        this.show_popup('selection',{
            'title': options.title || _t('Select User'),
            list: list,
            confirm: function(user){ def.resolve(user); },
            cancel:  function(){ def.reject(); },
        });
         //худалдагч эрхтэй хүн менежер эрхтэй хүнтэй солигдход user pin хийсний дараа устгах товч гарч ирэх эсвэл алга код
        return def.then(function(user){
            if (options.security && user !== options.current_user && user.pos_security_pin) {
                return self.ask_password(user.pos_security_pin).then(function(){

                    //new code// pos order model  зарласан
                    var custom_model = new Model('pos.order');
                    //зарласан model-оос is_user функцийг дуудаж ажиллуулж тухайн функцийн your_return_value д хадгалан
                    custom_model.call('is_user', [user.id]).then(function (your_return_value) {

                        //тухайн хадгалсан утга хэрвээ FALSE байх юм бол backspace_button гэсэн id тай талбар худалдагч эрхтэй хүнд харагдахгүй болно
                        if (your_return_value == false) {
                            document.getElementById('backspace_button').style.display = 'none';
                        }
                        //хэрвээ менежер буюу group_pos_manager гэх эрхээр нэвтэрсэн тохиолдолд backspace_button буцаад харагдах юм
                        else {
                            document.getElementById('backspace_button').style.display = 'block';
                        }
                    });
                    custom_model.call('is_button_delete', [user.id]).then(function (your_return_value) {

                        //тухайн хадгалсан утга хэрвээ FALSE байх юм бол delete_button гэсэн id тай талбар худалдагч эрхтэй хүнд харагдахгүй болно
                        if (your_return_value == false) {
                            document.getElementById('delete_button').style.display = 'none';
                        }
                        //хэрвээ менежер буюу group_pos_manager гэх эрхээр нэвтэрсэн тохиолдолд backspace_button буцаад харагдах юм
                        else {
                            document.getElementById('delete_button').style.display = 'block';
                        }
                    });
                      custom_model.call('disc_button_gui', [user.id]).then(function (your_return_value) {

                        //тухайн хадгалсан утга хэрвээ FALSE байх юм бол disc_button гэсэн id тай талбар худалдагч эрхтэй хүнд харагдахгүй болно
                        if (your_return_value == false) {
                            document.getElementById('disc_button').style.display = 'none';
                        }
                        //хэрвээ менежер буюу group_pos_manager гэх эрхээр нэвтэрсэн тохиолдолд backspace_button буцаад харагдах юм
                        else {
                            document.getElementById('disc_button').style.display = 'block';
                        }
                    });
                     custom_model.call('is_price_button', [user.id]).then(function (your_return_value) {

                        //тухайн хадгалсан утга хэрвээ FALSE байх юм бол disc_button гэсэн id тай талбар худалдагч эрхтэй хүнд харагдахгүй болно
                        if (your_return_value == false) {
                            document.getElementById('price_button').style.display = 'none';
                        }
                        //хэрвээ менежер буюу group_pos_manager гэх эрхээр нэвтэрсэн тохиолдолд disc_button буцаад харагдах юм
                        else {
                            document.getElementById('price_button').style.display = 'block';
                        }
                    });
                    //хэрэглэгчийг буцаах
                    return user;
                });
            } else {
                //анх нэвтэрсэн хэрэглэгч хэрвээ худалдагч байвал устгах товч харагдахгүй
                var custom_model = new Model('pos.order');
                custom_model.call('is_user').then(function (your_return_value) {
                    if (your_return_value == false) {
                        document.getElementById('backspace_button').style.display = 'none';
                    }
                });
                custom_model.call('is_button_delete').then(function (your_return_value) {
                    if (your_return_value == false) {
                        document.getElementById('button_delete').style.display = 'none';
                    }
                });
                 custom_model.call('disc_button_gui').then(function (your_return_value) {
                    if (your_return_value == false) {
                        document.getElementById('disc_button').style.display = 'none';
                    }
                });
                custom_model.call('is_price_button').then(function (your_return_value) {
                    if (your_return_value == false) {
                        document.getElementById('price_button').style.display = 'none';
                    }
                });
                return user;
                //end
            }
        });
    },
//override end
});
});
