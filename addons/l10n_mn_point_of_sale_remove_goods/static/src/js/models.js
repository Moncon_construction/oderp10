odoo.define('l10n_mn_point_of_sale_remove_goods.models', function (require) {
    "use strict";
    
    var utils = require('web.utils');
    var round_pr = utils.round_precision;
    var round_di = utils.round_decimals;
    var formats = require('web.formats');
    var models = require('point_of_sale.models');
    var _super_posmodel = models.PosModel.prototype;
    var _super_order = models.Order.prototype;
  //Пос ын дэлгэц дээр хэрэглэгч солиход худалдагч эрхтэй хүн харагдадгүй байсныг нэмж өгсөн
 //override begin
 models.load_models([
    {
                 model:  'res.users',
                 fields: ['name','pos_security_pin','groups_id','barcode'],
                 domain: function(self){ return ['|', '|', ['groups_id','=', self.config.group_pos_salesperson_id[0]], ['groups_id','=', self.config.group_pos_manager_id[0]],['groups_id','=', self.config.group_pos_user_id[0]]]; },
                 loaded: function(self,users){
                 var pos_users = [];
                 var current_cashier = self.get_cashier();
                 for (var i = 0; i < users.length; i++) {
                     var user = users[i];
                     for (var j = 0; j < user.groups_id.length; j++) {
                        var group_id = user.groups_id[j];
                        if (group_id === self.config.group_pos_manager_id[0]) {
                            user.role = 'manager';
                            break;
                        } else if (group_id === self.config.group_pos_user_id[0]) {
                            user.role = 'cashier';

                        //new code begin// Пос ын дэлгэц дээр хэрэглэгч солиход худалдагч эрхтэй хүн харагдадгүй байсныг нэмж өгсөн
                        } else if (group_id === self.config.group_pos_salesperson_id[0]) {
                            user.role = 'salesperson';
                        }
                        //end
                    }
                    if (user.role) {
                        pos_users.push(user);
                        console.log(pos_users)
                    }
                    // replace the current user with its updated version
                    if (user.id === self.user.id) {
                        self.user = user;
                    }
                    if (user.id === current_cashier.id) {
                        self.set_cashier(user);
                    }
                }

            self.users = pos_users;
        }
    },
    ],
    );
//override END


    });
    


