odoo.define('l10n_mn_point_of_sale_remove_goods.popups', function (require) {
	"use strict";
    var passwordPopup = require('point_of_sale.popups');
	var core = require('web.core');
	var Model = require('web.Model');
	var utils = require('web.utils');
	var round_pr = utils.round_precision;
	var QWeb = core.qweb;
    var PosBaseWidget = require('point_of_sale.BaseWidget');
    var gui = require('point_of_sale.gui');
	var _t = core._t;



  passwordPopup.include({

    show: function(options){
        options = options || {};
        this._super(options);
        window.document.body.addEventListener('keydown', this.keyDownHandler);
    },
     keyDownHandler: function (e) {
				switch (e.keyCode) {
					case 48: //0
						$(".popup-password  .input-button.number-char:contains('0')").click();
						break;
					case 97:
					case 49: //1
						$(".popup-password  .input-button.number-char:contains('1')").click();
						break;
					case 98:
					case 50: //2
						$(".popup-password  .input-button.number-char:contains('2')").click();
						break;
					case 99:
					case 51: //3
						$(".popup-password  .input-button.number-char:contains('3')").click();
						break;
					case 100:
					case 52: //4
						$(".popup-password  .input-button.number-char:contains('4')").click();
						break;
					case 101:
					case 53: //5
						$(".popup-password  .input-button.number-char:contains('5')").click();
						break;
					case 102:
					case 54: //6
						$(".popup-password   .input-button.number-char:contains('6')").click();
						break;
					case 103:
					case 55: //7
						$(".popup-password   .input-button.number-char:contains('7')").click();
						break;
					case 104:
					case 56: //8
						$(".popup-password   .input-button.number-char:contains('8')").click();
						break;
					case 105:
					case 57: //9
						$(".popup-password  .input-button.number-char:contains('9')").click();
						break;
                    case 8: //backspace
						$(".popup-password  .input-button.numpad-backspace").click();
						break;
				}


			},


});
});
