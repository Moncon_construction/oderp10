odoo.define('l10n_mn_point_of_sale_remove_goods.screens', function (require) {
	"use strict";

	var productScreen = require('point_of_sale.screens').ProductScreenWidget;

	var core = require('web.core');
	var Model = require('web.Model');
	var utils = require('web.utils');
	var round_pr = utils.round_precision;
	var QWeb = core.qweb;

	var _t = core._t;


 //посын тохиргоо дээр бараа хасах эрх цэснээс худалдагч эрхтэй хүн бараа хасахыг зөвшөөрөхгүй гэсэн сонголтын сонгоход group_pos_salesperson гэсэн групптэй хэрэглэгчид устгах товчыг харагдуулахгүй байх код
	productScreen.include({
        start: function () {
            this._super();
            //new code //
             //begin// pos order model  зарласан
            var custom_model = new Model('pos.order');

            //зарласан model-оос is_user_screen функцийг дуудаж ажиллуулж тухайн функцийн your_return_value д хадгалан
            custom_model.call('is_user_screen').then(function (your_return_value) {

            //тухайн хадгалсан утга хэрвээ FALSE байх юм бол backspace_button гэсэн id тай талбар худалдагч эрхтэй хүнд харагдахгүй болно
                if (your_return_value == false) {
                    document.getElementById('backspace_button').style.display = 'none';
                }
            });

            //зарласан model-оос is_user_screen функцийг дуудаж ажиллуулж тухайн функцийн your_return_value д хадгалан
            custom_model.call('is_button_delete_screen').then(function (your_return_value) {

            //тухайн хадгалсан утга хэрвээ FALSE байх юм бол backspace_button гэсэн id тай талбар худалдагч эрхтэй хүнд харагдахгүй болно
                if (your_return_value == false) {
                    document.getElementById('delete_button').style.display = 'none';
                }
            });

            //зарласан model-оос is_user_screen функцийг дуудаж ажиллуулж тухайн функцийн your_return_value д хадгалан
            custom_model.call('disc_button_screen').then(function (your_return_value) {

            //тухайн хадгалсан утга хэрвээ FALSE байх юм бол backspace_button гэсэн id тай талбар худалдагч эрхтэй хүнд харагдахгүй болно
                if (your_return_value == false) {
                    document.getElementById('disc_button').style.display = 'none';
                }
            });
             custom_model.call('is_price_button_screen').then(function (your_return_value) {

            //тухайн хадгалсан утга хэрвээ FALSE байх юм бол backspace_button гэсэн id тай талбар худалдагч эрхтэй хүнд харагдахгүй болно
                if (your_return_value == false) {
                    document.getElementById('price_button').style.display = 'none';
                }
            });
            },//end

            // посын дэлгэц дээр худалдагч эрхтэй хүн гараас backspace дарагдахгүй бараа устгахгүй боломжгүй болгосон
            //override гараас дархад ажиллах backspace товчыг хассан
            keyDownHandler: function (e) {
			if (this.checkKeypadAvailability()) {
				switch (e.keyCode) {
					case 40: //DOWN
						$(".orderlines .orderline.selected").next(".orderlines .orderline").click();
						break;
					case 38: //UP
						$(".orderlines .orderline.selected").prev(".orderlines .orderline").click();
						break;
					case 96:
					case 48: //0
						$(".product-screen .input-button.number-char:contains('0')").click();
						break;
					case 97:
					case 49: //1
						$(".product-screen .input-button.number-char:contains('1')").click();
						break;
					case 98:
					case 50: //2
						$(".product-screen .input-button.number-char:contains('2')").click();
						break;
					case 99:
					case 51: //3
						$(".product-screen .input-button.number-char:contains('3')").click();
						break;
					case 100:
					case 52: //4
						$(".product-screen .input-button.number-char:contains('4')").click();
						break;
					case 101:
					case 53: //5
						$(".product-screen .input-button.number-char:contains('5')").click();
						break;
					case 102:
					case 54: //6
						$(".product-screen .input-button.number-char:contains('6')").click();
						break;
					case 103:
					case 55: //7
						$(".product-screen .input-button.number-char:contains('7')").click();
						break;
					case 104:
					case 56: //8
						$(".product-screen .input-button.number-char:contains('8')").click();
						break;
					case 105:
					case 57: //9
						$(".product-screen .input-button.number-char:contains('9')").click();
						break;
					case 110:
					case 190: //.
						$(".product-screen .input-button.number-char:contains('.')").click();
						break;
					case 13: //enter - төлбөр
						e.preventDefault();
						$(".product-screen .button.pay").click();
						break;
					case 81: //q - Тоо хэмжээ
						e.preventDefault();
						$(".product-screen .mode-button[data-mode='quantity']").click();
						break;
					case 67: //c - харилцагч хайх
						e.preventDefault();
						$(".product-screen .button.set-customer").click();
						break;
				}
			}
			if (e.keyCode == 113) { //f12 - Бараа хайх товч
				e.preventDefault();
				if ($(".product-screen .searchbox input").is(":focus")) {
					$(".product-screen .searchbox input").blur();
				} else {
					$(".product-screen .searchbox input").focus();
				}
			}

			},

    });


    });