# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Point of Sale analytic share",
    'version': '2.1',
    'depends': [
        'l10n_mn_point_of_sale',
        'l10n_mn_analytic_share',
    ],
    'author': "Asterisk Technologies LLC",
    'sequence': 15,
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Mongolian Point of Sale for analytic share
    """,
    'data': [
    ],
    'qweb': [''],
    'application': True,
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
