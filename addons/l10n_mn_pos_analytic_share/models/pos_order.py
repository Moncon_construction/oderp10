# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.
import logging

from odoo import api, fields, models, _
from odoo.tools import float_is_zero
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)

class PosOrder(models.Model):
    _inherit = "pos.order"

    def create_move_by_qry(self, move, grouped_data):
        # create account.move.lines using postgresql query
        if move:
            company_id = move.company_id.id
            journal_id = move.journal_id.id
            move_id = move.id
            ref = move.ref
            move_date = move.date or datetime.now()
            for group_key, group_data in grouped_data.iteritems():
                for value in group_data:
                    # set partner_id when account can reconcile
                    account = self.env['account.account'].browse(value['account_id'])
                    order_for_partner = self[0] if self and len(self) > 0 else self
                    order_seller_id = order_for_partner.session_id.user_id.partner_id.id if order_for_partner.session_id and order_for_partner.session_id.user_id and order_for_partner.session_id.user_id.partner_id else False
                    if account.reconcile and order_seller_id and ('partner_id' not in value.keys() or ('partner_id' in value.keys() and not value['partner_id'])):
                        value['partner_id'] = order_seller_id
                        
                    uid = self.env.uid
                    date = fields.Datetime.now()
                    
                    self.env.cr.execute(
                    """
                        INSERT INTO account_move_line (create_uid, create_date, write_uid, write_date, company_id, journal_id, move_id, ref, name, quantity, product_id, account_id, analytic_account_id, credit, debit, partner_id, tax_line_id, currency_rate, date_maturity, date, register_date) 
                        VALUES (%s, '%s', %s, '%s', %s, %s, %s, '%s', '%s', %s, %s, %s, %s, %s, %s, %s, %s, 1.0, '%s', '%s', '%s')
                        RETURNING id 
                    """ %(uid, date, uid, date, company_id, journal_id, move_id, ref, self.get_qry_value(value, 'name'), self.get_qry_value(value, 'quantity'), self.get_qry_value(value, 'product_id'),
                          self.get_qry_value(value, 'account_id'), self.get_qry_value(value, 'analytic_account_id'),
                          self.get_qry_value(value, 'credit'), self.get_qry_value(value, 'debit'),
                          self.get_qry_value(value, 'partner_id'), self.get_qry_value(value, 'tax_line_id'),
                          str(move_date) if move_date else 'NULL', str(move_date) if move_date else 'NULL', str(move_date) if move_date else 'NULL')
                    )
                    new_move_line_id = self.env.cr.fetchone()[0]

                    # if account.move.lines object has tax, then create linked module's object for it
                    insert_qry = ""
                    if new_move_line_id and 'tax_ids' in value.keys():
                        tax_ids = value['tax_ids'][0][2]
                        if tax_ids and len(tax_ids) > 0:
                            uniq_tax_ids = []
                            for tax_id in tax_ids:
                                if tax_id not in uniq_tax_ids:
                                    uniq_tax_ids.append(tax_id)

                            insert_qry = "INSERT INTO account_move_line_account_tax_rel (account_move_line_id, account_tax_id) VALUES "
                            for tax_id in uniq_tax_ids:
                                if tax_id == uniq_tax_ids[0]:
                                    insert_qry += "(" + str(new_move_line_id) + ", " + str(tax_id) + ")"
                                else:
                                    insert_qry += ", (" + str(new_move_line_id) + ", " + str(tax_id) + ")"

                            self.env.cr.execute(insert_qry)
                    if new_move_line_id and 'analytic_account_id' in value.keys() and value['analytic_account_id']:
                            share_vals = []
                            share_vals.append((0,0,{'analytic_account_id': value['analytic_account_id'],
                                                          'rate': 100}))
                            new_move_line = self.env['account.move.line'].browse([new_move_line_id])
                            new_move_line.write({'analytic_share_ids': share_vals})
                            new_move_line.create_analytic_lines()

            move.sudo().post()
            
        return move
    
    