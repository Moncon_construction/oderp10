# -*- coding: utf-8 -*-
import logging
from collections import defaultdict
from datetime import datetime
from odoo import fields, models

_logger = logging.getLogger(__name__)


class StockMove(models.Model):
    _inherit = "stock.move"

    def _prepare_account_move_line(self, qty, cost, credit_account_id, debit_account_id):
        res = super(StockMove, self)._prepare_account_move_line(qty, cost, credit_account_id, debit_account_id)

        # update analytic account
        analytic_account_id = False
        if self.order_line_id:
            analytic_account_id = self.order_line_id.order_id.session_id.config_id.analytic_account_id.id
        _logger.info(u'analytic_account_id = %s', analytic_account_id)
        if analytic_account_id:
            for i in res:
                if i[2]['debit'] != 0:
                    _logger.info(u'debit = %s', i[2]['debit'])
                    share_vals = []
                    share_vals.append((0, 0, {'analytic_account_id': analytic_account_id,
                                              'rate': 100}))
                    i[2].update({'analytic_share_ids': share_vals})
        return res


class StockQuant(models.Model):
    _inherit = "stock.quant"

    ######################################################
    #    Агуулахын хөдөлгөөний батлах үед l10n_mn_stock_account модулийн
    #    _create_account_move_line функц дуудагдаж
    #    analytic_account_id, analytic_share_ids талбаруудын утгыг устгаж байсан тул
    #    дахин тодорхойлов.
    #######################################################
    def _create_account_move_line(self, move, credit_account_id, debit_account_id, journal_id):
        setting = self.env['stock.config.settings'].search([], order='id desc', limit=1)
        AccountMove = self.env['account.move']
        if setting.group_stock_production_lot == 0:
            # Цуврал хөтөлдөггүй бол stock.move-н price_unit-р ажил гүйлгээг үүсгэнэ. Энэ сонголтыг Od ERP дээр цуврал хөтөлдөггүй харилцагчид зориулж нэмж хийж өгсөн
            move_lines = move._prepare_account_move_line(move.product_qty, move.price_unit, credit_account_id,
                                                         debit_account_id)
            if move_lines:
                date = self._context.get('force_period_date', fields.Date.context_today(self))
                if not move.picking_id.account_move_id:
                    new_account_move = AccountMove.create({
                        'journal_id': journal_id,
                        'line_ids': move_lines,
                        'date': date,
                        'ref': move.picking_id.name})
                    new_account_move.post()
                    move.picking_id.write({'account_move_id': new_account_move.id})
                    #  Шинэ үүссэн ажил гүйлгээ нь тооллогоос дамжиж үүсэж байвал тооллоготой холбож өгнө.
                    if not move.picking_id and move.inventory_id:
                        move.inventory_id.write({'account_move_id': new_account_move.id})
                else:
                    company_id = move.company_id and move.company_id.id or self.env.user.company_id.id
                    user_id = move and move.create_uid and move.create_uid.id or self.env.user.id
                    all_creates = True
                    for line in move_lines:
                        if line[0] != 0 or line[1] != 0:
                            all_creates = False
                    if all_creates:
                        total = move.picking_id.account_move_id and move.picking_id.account_move_id.amount or 0
                        for line in move_lines:
                            if line[2]['debit'] != 0:
                                self._cr.execute('''
                                        INSERT INTO
                                            account_move_line(
                                                create_uid,
                                                write_uid,
                                                journal_id,
                                                date,
                                                account_id,
                                                analytic_account_id,
                                                credit,
                                                debit,
                                                inventory_id,
                                                name,
                                                partner_id,
                                                product_id,
                                                product_uom_id,
                                                quantity,
                                                ref,
                                                stock_move_id,
                                                move_id,
                                                date,
                                                date_maturity,
                                                company_id
                                            )
                                        VALUES(
                                            ''' + str(user_id) + ''',
                                            ''' + str(user_id) + ''',
                                            ''' + str(journal_id) + ''',
                                            \'''' + date + '''\',
                                            ''' + str(line[2]['account_id']) + ''',
                                            ''' + (str(line[2]['analytic_account_id']) if line[2]['analytic_account_id'] else 'NULL') + ''',
                                            ''' + str(line[2]['credit']) + ''',
                                            ''' + str(line[2]['debit']) + ''',
                                            ''' + (
                                    str(line[2]['inventory_id']) if line[2]['inventory_id'] else 'NULL') + ''',
                                            \'''' + str(line[2]['name']) + '''\',
                                            ''' + (str(line[2]['partner_id']) if line[2]['partner_id'] else 'NULL') + ''',
                                            ''' + (str(line[2]['product_id']) if line[2]['product_id'] else 'NULL') + ''',
                                            ''' + (str(line[2]['product_uom_id']) if line[2][
                                    'product_uom_id'] else 'NULL') + ''',
                                            ''' + (str(line[2]['quantity']) if line[2]['quantity'] else 'NULL') + ''',
                                            \'''' + str(line[2]['ref']) + '''\',
                                            ''' + (str(line[2]['stock_move_id']) if line[2][
                                    'stock_move_id'] else 'NULL') + ''',
                                            ''' + str(move.picking_id.account_move_id.id) + ''',
                                            \'''' + datetime.now().strftime('%Y-%m-%d') + '''\',
                                            \'''' + datetime.now().strftime('%Y-%m-%d') + '''\',
                                            ''' + str(company_id) + '''
                                        )
                                    ''')
                                self._cr.execute('SELECT currval(\'account_move_line_id_seq\')')
                                id_of_new_row = self._cr.fetchone()[0]
                                self.env['account.analytic.share'].create({
                                    'analytic_account_id': line[2]['analytic_share_ids'][0][2]['analytic_account_id'],
                                    'rate': line[2]['analytic_share_ids'][0][2]['rate'],
                                    'move_line_id': id_of_new_row
                                })
                                move_line = self.env['account.move.line'].browse(id_of_new_row)
                                move_line.create_analytic_lines()
                            else:
                                self._cr.execute('''
                                INSERT INTO
                                    account_move_line(
                                        create_uid,
                                        write_uid,
                                        journal_id,
                                        date,
                                        account_id,
                                        credit,
                                        debit,
                                        inventory_id,
                                        name,
                                        partner_id,
                                        product_id,
                                        product_uom_id,
                                        quantity,
                                        ref,
                                        stock_move_id,
                                        move_id,
                                        date_maturity,
                                        company_id
                                    )
                                VALUES(
                                    ''' + str(user_id) + ''',
                                    ''' + str(user_id) + ''',
                                    ''' + str(journal_id) + ''',
                                    \'''' + date + '''\',
                                    ''' + str(line[2]['account_id']) + ''',
                                    ''' + str(line[2]['credit']) + ''',
                                    ''' + str(line[2]['debit']) + ''',
                                    ''' + (str(line[2]['inventory_id']) if line[2]['inventory_id'] else 'NULL') + ''',
                                    \'''' + str(line[2]['name']) + '''\',
                                    ''' + (str(line[2]['partner_id']) if line[2]['partner_id'] else 'NULL') + ''',
                                    ''' + (str(line[2]['product_id']) if line[2]['product_id'] else 'NULL') + ''',
                                    ''' + (str(line[2]['product_uom_id']) if line[2]['product_uom_id'] else 'NULL') + ''',
                                    ''' + (str(line[2]['quantity']) if line[2]['quantity'] else 'NULL') + ''',
                                    \'''' + str(line[2]['ref']) + '''\',
                                    ''' + (str(line[2]['stock_move_id']) if line[2]['stock_move_id'] else 'NULL') + ''',
                                    ''' + str(move.picking_id.account_move_id.id) + ''',
                                    \'''' + datetime.now().strftime('%Y-%m-%d') + '''\',
                                    ''' + str(self.env.user.company_id.id) + '''
                                )
                            ''')
                            total += line[2]['debit']
                        if move.picking_id.account_move_id:
                            move.picking_id.account_move_id.write({'amount': total})
                    else:
                        move.picking_id.account_move_id.write({'line_ids': move_lines})
        elif setting.group_stock_production_lot == 1:
            # Цуврал хөтөлдөг бол stock.quant-н cost-р ажил гүйлгээг үүсгэнэ. Энэ нь default хувилбар нь
            quant_cost_qty = defaultdict(lambda: 0.0)
            for quant in self:
                quant_cost_qty[quant.cost] += quant.qty
            for cost, qty in quant_cost_qty.iteritems():
                move_lines = move._prepare_account_move_line(qty, cost, credit_account_id, debit_account_id)
                if move_lines:
                    date = self._context.get('force_period_date', fields.Date.context_today(self))
                    new_account_move = AccountMove.create({
                        'journal_id': journal_id,
                        'line_ids': move_lines,
                        'date': date,
                        'ref': move.picking_id.name})
                    new_account_move.post()
