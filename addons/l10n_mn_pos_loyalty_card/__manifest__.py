# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian POS Loyalty Card",
    'version': '2.1',
    'author': "Asterisk Technologies LLC",
    'sequence': 15,
    'website': 'http://asterisk-tech.mn',
    'depends': [
        'l10n_mn_point_of_sale',
        'l10n_mn_sale_loyalty_card',
        'l10n_mn_account',
        'l10n_mn_product',
        'l10n_mn_vatpsp',
    ],
    'category': 'Mongolian Modules',
    'description': """
       Mongolian Point Loyalty Card
    """,
    'data': [
        # 'views/pos_config_settings_view.xml',
        'views/pos_config_view.xml',
        'views/pos_loyalty.xml',
        'data/account_journal_for_pos_loyalty_data.xml',
        'security/ir.model.access.csv',
        'views/pos_order_view.xml',
    ],
    'qweb': [
        'static/src/xml/pos.xml',
    ],
    'auto_install': False
}
