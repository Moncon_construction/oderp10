# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from datetime import datetime
import json


class LoyaltyCard(models.Model):
    _inherit = "loyalty.card.card"

    def get_card_by_card_number(self, loyalty_card_number=None):
        if loyalty_card_number:
            loyalty_card_number = "%%%s%%" % loyalty_card_number
            cards_id = self.env['loyalty.card.card'].search([('card_number', 'like', loyalty_card_number), ('state', '=', 'usable')], limit=10)
            if cards_id:
                cards_list = list()
                for obj in cards_id:
                    tmp_copy_data = obj.copy_data()
                    tmp_copy_data[0]['id'] = obj.id
                    tmp_copy_data[0]['card_type'] = obj.card_type.name
                    tmp_copy_data[0]['card_type_id'] = obj.card_type.id
                    tmp_copy_data[0]['active_to'] = obj.active_to
                    tmp_copy_data[0]['card_type_string'] = obj.card_type_string
                    if tmp_copy_data[0]['card_type_string'] == "Collects sale amount":
                        tmp_copy_data[0]['sale_percentage'] = obj.sale_percentage
                    elif tmp_copy_data[0]['card_type_string'] == "Percentage":
                        tmp_copy_data[0]['sale_percentage'] = obj.discount_percentage
                    cards_list += tmp_copy_data
                return json.dumps(cards_list)

    def get_partner_info(self):
        partners = self.env['res.partner']
        for obj in self:
            partners = obj.primary_holder | obj.other_holders
        return json.dumps(partners.copy_data())

    def get_card_type(self):
        return json.dumps(self.card_type.copy_data()[0])

    def buy(self, order_name=None, total_amount=0, sale_percentage=0, sale_amount=0, pay_amount=0, bonus_added=0, bonus_spent=0):
        res = super(LoyaltyCard, self).buy(order_name=order_name, total_amount=total_amount,
                                           sale_percentage=sale_percentage, sale_amount=sale_amount,
                                           pay_amount=pay_amount, bonus_added=bonus_added, bonus_spent=bonus_spent)
        # пос-н захиалгыг холбож өгч байна
        history = self.env['loyalty.card.history'].browse(res)
        order_id = self.env['pos.order'].search([('pos_reference', '=', order_name)])
        history.write({
            'pos_order_id': order_id.id
        })
        return res


class LoyaltyCardHistory(models.Model):
    _inherit = "loyalty.card.history"
    pos_order_id = fields.Many2one('pos.order', string="Pos order", readonly=True)
