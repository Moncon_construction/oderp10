# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import AccessDenied, UserError
from odoo.addons.base.module import module


class PosConfig(models.Model):
    _inherit = 'pos.config'

    account_debit = fields.Many2one('account.account', 'Debit Account', domain=[('deprecated', '=', False)],
                                    default=lambda self: self.env.ref(
                                        'l10n_mn_sale_loyalty_card.account_account_3201_0000').id)

    account_credit = fields.Many2one('account.account', 'Credit Account', domain=[('deprecated', '=', False)],
                                     default=lambda self: self.env.ref(
                                         'l10n_mn_sale_loyalty_card.account_account_3201_0000').id
                                     )
    loyalty_partner_id = fields.Many2one('res.partner', string='Partner',
                                        help="Partner to be choosen when journal entry is created")


