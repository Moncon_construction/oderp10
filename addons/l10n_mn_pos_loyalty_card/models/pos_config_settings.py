# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import AccessDenied, UserError
from odoo.addons.base.module import module


class PosConfiguration(models.TransientModel):
    _inherit = 'pos.config.settings'

    module_l10n_mn_sale_loyalty_card_by_buy_amount = fields.Boolean(
                                                    related="company_id.module_l10n_mn_sale_loyalty_card_by_buy_amount", default=False,
                                                    help="installs l10n_mn_sale_loyalty_card_by_buy_amount",
                                                    string="Bonus Card")  # Бонус картны модуль суулгана

    module_l10n_mn_sale_loyalty_card_discount = fields.Boolean(
                                                            related="company_id.module_l10n_mn_sale_loyalty_card_discount", default=False,
                                                            help="installs l10n_mn_sale_loyalty_card_discount",
                                                            string="Discount Card")  # хөнгөлөлтийн картны модуль суулгана

    module_l10n_mn_sale_loyalty_card_gift = fields.Boolean(related="company_id.module_l10n_mn_sale_loyalty_card_gift",
                                                           default=False,
                                                           help="installs l10n_mn_sale_loyalty_card_gift",
                                                           string="Gift Card")  # Бэлгийн картны модуль суулгана

    module_l10n_mn_sale_loyalty_card_reward_point = fields.Boolean(
                                                            related="company_id.module_l10n_mn_sale_loyalty_card_reward_point", default=False,
                                                            help="installs l10n_mn_sale_loyalty_card_reward_point",
                                                            string="Reward Point Card")  # Оноо цуглуулах картны модуль суулгана.

    module_l10n_mn_sale_loyalty_card_debit = fields.Boolean(related="company_id.module_l10n_mn_sale_loyalty_card_debit",
                                                            default=False,
                                                            help="installs l10n_mn_sale_loyalty_card_debit",
                                                            string="Loyalty Debit Card")  # Цэнэглэдэг картны модуль суулгана


