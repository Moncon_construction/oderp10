# -*- coding: utf-8 -*-
import json
import requests
import logging
import psycopg2
from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError
from odoo.tools import float_is_zero
from datetime import datetime
from lxml import etree
import base64
_logger = logging.getLogger(__name__)


class PosOrder(models.Model):
    _inherit = "pos.order"

    is_loyalty_used = fields.Boolean(default=False)
    loyalty_card_init_balance = fields.Float(string="Initial Balance")
    loyalty_card_added_points = fields.Float(string="Added")
    loyalty_card_spent_points = fields.Float(string="Spent")
    loyalty_card_spent_points_on_order = fields.Float(related='loyalty_card_spent_points', string="Loyalty card spent amount")
    loyalty_card_end_balance = fields.Float(string="End Balance")
    loyalty_card_id = fields.Many2one('loyalty.card.card', string="Card Number")
    loyalty_card_type = fields.Many2one('loyalty.card.type', string="Card Type")
    loyalty_card_holder = fields.Many2one('res.partner', string="Primary Card Holder")
    loyalty_card_sale_percentage = fields.Float(string="Sale percentage")

    def test_paid(self):
        res = super(PosOrder, self).test_paid()
        # Урамшууллын картаар төлбөр төлсөн бол төлбөрийн дүнг хасч өгнө
        for order in self:
            if (order.amount_total - order.amount_paid - order.loyalty_card_spent_points) < 0.00001:
                res = True
        return res

    def get_group_key(self, data_type, values):
        # @Override
        key = False
        if data_type == 'product':
            key = ('product', values['partner_id'],
                   (values['product_id'], tuple(values['tax_ids'][0][2]), values['name']),
                   values['analytic_account_id'], values['debit'] > 0)
        elif data_type == 'tax':
            key = ('tax', values['partner_id'], values['tax_line_id'], values['debit'] > 0)
        elif data_type == 'counter_part':
            key = ('counter_part', values['partner_id'], values['account_id'], values['debit'] > 0)
        elif data_type == 'loyalty_credit':
            key = ('loyalty_credit', values['partner_id'], values['account_id'], values['credit'] > 0)
        elif data_type == 'loyalty_debit':
            key = ('loyalty_debit', values['partner_id'], values['account_id'], values['debit'] > 0)
        return key
    
    # OVERRIDE
    # Урамшууллын картаас журналын бичилт үүсгэх шаарлдагаар OVERRIDE хийв
    def _create_account_move_line(self, session=None, move=None):

        # Tricky, via the workflow, we only have one id in the ids variable
        """Create a account move line of order grouped by products or not."""
        IrProperty = self.env['ir.property']
        ResPartner = self.env['res.partner']

        if session and not all(session.id == order.session_id.id for order in self):
            raise UserError(_('Selected orders do not have the same session!'))

        grouped_data = {}
        have_to_group_by = session and session.config_id.group_by or False
        rounding_method = session and session.config_id.company_id.tax_calculation_rounding_method
        loyalty_account_partner = None

        for order in self.filtered(lambda o: not o.account_move or o.state == 'paid'):
            partner_id = ResPartner._find_accounting_partner(order.partner_id).id or False
            if session and session.config_id and session.config_id.account_debit:
                loyalty_account_debit = session.config_id.account_debit
                loyalty_account_credit = session.config_id.account_credit
                loyalty_account_partner = session.config_id.loyalty_partner_id.id or partner_id
            else:
                loyalty_account_debit = loyalty_account_credit = self.env.ref('l10n_mn_sale_loyalty_card.account_account_3201_0000')
            current_company = order.sale_journal.company_id
            account_def = IrProperty.get(
                'property_account_receivable_id', 'res.partner')
            order_account = order.partner_id.property_account_receivable_id.id or account_def and account_def.id
            if move is None:
                # Create an entry for the sale
                journal_id = self.env['ir.config_parameter'].sudo().get_param(
                    'pos.closing.journal_id_%s' % current_company.id, default=order.sale_journal.id)
                move = self._create_account_move(
                    order.session_id.start_at, order.name, int(journal_id), order.company_id.id)

            # because of the weird way the pos order is written, we need to make sure there is at least one line,
            # because just after the 'for' loop there are references to 'line' and 'income_account' variables (that
            # are set inside the for loop)
            # TOFIX: a deep refactoring of this method (and class!) is needed
            # in order to get rid of this stupid hack
            assert order.lines, _('The POS order must have lines when calling this method')
            # Create an move for each order line
            cur = order.pricelist_id.currency_id

            ############################## POS_LOYALTY OVERRIDE BEGINS #######################################
            # loyalty_credit нь лояалти картын журналын бичилтийн кредит хэсэгт бичигдэх үнийн дүнг цуглуулж байна
            loyalty_credit = 0.0
            for line in order.lines:
                # Лояалти карт тухайн захиалгад ашиглагдсан бол урамшууллын цуглуулах дүнг нийт үнийн дүнгээс хасч
                # Хассан дүнгээ лояалти картын журналын кредитд нэмж байна
                taxes = line.tax_ids_after_fiscal_position.filtered(lambda t: t.company_id.id == current_company.id)
                if order.is_loyalty_used:
                    sale_percentage_amount = round((line.price_subtotal_incl * (order.loyalty_card_sale_percentage / 100)), 2)
                    loyalty_credit += sale_percentage_amount

                    amount = taxes.compute_all(line.price_subtotal_incl - sale_percentage_amount, cur)['total_excluded']
                else:
                    amount = line.price_subtotal
            ############################## POS_LOYALTY OVERRIDE ENDS #######################################

                # Search for the income account
                income_account = self.get_income_account(line)

                name = line.product_id.name
                if line.notice:
                    # add discount reason in move
                    name = name + ' (' + line.notice + ')'

                # Create a move for the line for the order line
                # Just like for invoices, a group of taxes must be present on this base line
                # As well as its children
                base_line_tax_ids = self._flatten_tax_and_children(line.tax_ids_after_fiscal_position).filtered(
                    lambda tax: tax.type_tax_use in ['sale', 'none'])
                grouped_data = self.insert_data('product', order, move, grouped_data, {
                    'name': name,
                    'quantity': line.qty,
                    'product_id': line.product_id.id,
                    'account_id': income_account,
                    'analytic_account_id': self._prepare_analytic_account(line),
                    'credit': ((amount > 0) and amount) or 0.0,
                    'debit': ((amount < 0) and -amount) or 0.0,
                    'tax_ids': [(6, 0, base_line_tax_ids.ids)],
                    'partner_id': partner_id
                })

                # Create the tax lines
                if not taxes:
                    continue
                price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)

                ############################## POS_LOYALTY OVERRIDE BEGINS #######################################
                if order.is_loyalty_used:
                    # Лояалти карт ашигладсан бол татварт бодогдох дүнгээс лояалти картын урамшууллын дүнг хасч өгнө
                    price = round(price * (1 - (order.loyalty_card_sale_percentage / 100)), 2)
                ############################## POS_LOYALTY OVERRIDE ENDs #######################################

                for tax in taxes.compute_all(price, cur, line.qty)['taxes']:
                    ################## START: Татварын мөр дээрхи харилцагчийг татварын ХОЛБООТОЙ ХАРИЛЦАГЧ-с авах хэсэг ################
                    taxes = self.env['account.tax'].browse(tax['id'])
                    tax_obj = False
                    if taxes and len(taxes) > 0:
                        tax_obj = taxes[0]
                    grouped_data = self.insert_data('tax', order, move, grouped_data, {
                        'name': _('Tax') + ' ' + tax['name'],
                        'product_id': line.product_id.id,
                        'quantity': line.qty,
                        'account_id': tax['account_id'] or income_account,
                        'credit': ((tax['amount'] > 0) and tax['amount']) or 0.0,
                        'debit': ((tax['amount'] < 0) and -tax['amount']) or 0.0,
                        'tax_line_id': tax['id'],
                        'partner_id': tax_obj.partner_id.id if (tax_obj and tax_obj.partner_id) else partner_id
                    })
                    ################## END: Татварын мөр дээрхи харилцагчийг татварын ХОЛБООТОЙ ХАРИЛЦАГЧ-с авах хэсэг ################

            # round tax lines per order
            if rounding_method == 'round_globally':
                for group_key, group_value in grouped_data.iteritems():
                    if group_key[0] == 'tax':
                        for line in group_value:
                            line['credit'] = cur.round(line['credit'])
                            line['debit'] = cur.round(line['debit'])

            ############################## POS_LOYALTY OVERRIDE BEGINS #######################################
            # counterpart
            if loyalty_credit and loyalty_credit > 0:
                # Кредит хэсэгт лояалти картын цуглуулсан дүнг оруулж байна
                grouped_data = self.insert_data('loyalty_credit', order, move, grouped_data, {
                    'name': _("Loyalty card added"),  # order.name,
                    'account_id': loyalty_account_credit.id,
                    'credit': loyalty_credit,
                    'debit': 0.0,
                    'partner_id': loyalty_account_partner
                })
            if order.loyalty_card_spent_points and order.loyalty_card_spent_points > 0:
                # Дебит хэсэгт лояалти картын зарцуулсан дүнг оруулж байна
                grouped_data = self.insert_data('loyalty_debit', order, move, grouped_data, {
                    'name': _("Loyalty card spent"),  # order.name,
                    'account_id': loyalty_account_debit.id,
                    'credit': 0.0,
                    'debit': order.loyalty_card_spent_points,
                    'partner_id': loyalty_account_partner
                })
                grouped_data = self.insert_data('counter_part', order, move, grouped_data, {
                    'name': _("Trade Receivables"),  # order.name,
                    'account_id': order_account,
                    'credit': ((order.amount_total - order.loyalty_card_spent_points < 0) and -order.amount_total + order.loyalty_card_spent_points) or 0.0,
                    'debit': ((order.amount_total - order.loyalty_card_spent_points > 0) and order.amount_total - order.loyalty_card_spent_points) or 0.0,
                    'partner_id': partner_id
                })
            ############################## POS_LOYALTY OVERRIDE ENDS #######################################
            else:
                grouped_data = self.insert_data('counter_part', order, move, grouped_data, {
                    'name': _("Trade Receivables"),  # order.name,
                    'account_id': order_account,
                    'credit': ((order.amount_total < 0) and -order.amount_total) or 0.0,
                    'debit': ((order.amount_total > 0) and order.amount_total) or 0.0,
                    'partner_id': partner_id
                })
            order.write({'state': 'done', 'account_move': move.id})
        if self and order.company_id.anglo_saxon_accounting:
            grouped_data = self.add_anglosaxon_lines(move, grouped_data, order)

        ################## START: Сэшн хаахад хэтэрхий удаж байсан тул qry ашиглаж журналын мөр үүсгэдэг болгов ################
        self.create_move_by_qry(move, grouped_data)
        ################## END:   Сэшн хаахад хэтэрхий удаж байсан тул qry ашиглаж журналын мөр үүсгэдэг болгов ################

        return True

    @api.model
    def _order_fields(self, ui_order):
        fields = super(PosOrder, self)._order_fields(ui_order)
        # Урамшууллын карт ашигласан бол талбарын утгуудыг авч байна
        fields['is_loyalty_used'] = ui_order.get('is_loyalty_used', False)
        if fields['is_loyalty_used']:
            fields['loyalty_card_init_balance'] = ui_order.get('loyalty_card_init_balance', 0)
            fields['loyalty_card_added_points'] = ui_order.get('loyalty_card_added_points', 0)
            fields['loyalty_card_spent_points'] = ui_order.get('loyalty_card_spent_points', 0)
            fields['loyalty_card_end_balance'] = ui_order.get('loyalty_card_end_balance', 0)
            fields['loyalty_card_id'] = ui_order.get('loyalty_card_id', False)
            fields['loyalty_card_type'] = ui_order.get('loyalty_card_type', False)
            fields['loyalty_card_holder'] = ui_order.get('loyalty_card_holder', False)
            fields['loyalty_card_sale_percentage'] = ui_order.get('loyalty_card_sale_percentage', 0)
            if fields.get('ebarimt_total_amount_with_tax', False):
                fields['ebarimt_total_amount_with_tax'] -= float(fields['loyalty_card_added_points'])
                if fields['ebarimt_total_amount_with_tax'] > 0:
                    fields['ebarimt_vat_amount'] = float(fields['ebarimt_total_amount_with_tax'])/10
                    fields['ebarimt_total_amount_without_tax'] = float(fields['ebarimt_total_amount_with_tax']) - float(fields['ebarimt_vat_amount'])
                    if 'ebarimt_city_tax_amount' in fields and fields['ebarimt_city_tax_amount'] > 0:
                        fields['ebarimt_city_tax_amount'] = float(fields['ebarimt_total_amount_with_tax']) / 100
                    fields['ebarimt_register_amount'] = fields['ebarimt_total_amount_with_tax']
                else:
                    fields['ebarimt_total_amount_with_tax'] = 0

        return fields
