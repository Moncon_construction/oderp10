# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
import json


class ResPartner(models.Model):
    _inherit = 'res.partner'

    def get_card_info(self):
        cards_id = self.env['loyalty.card.card']
        for obj in self:
            cards_id = cards_id | cards_id.search([('primary_holder', 'in', obj.ids), ('state', '=', 'usable')])
            cards_id = cards_id | cards_id.search([('other_holders', 'in', obj.ids), ('state', '=', 'usable')])
        if cards_id:
            cards_list = list()
            for obj in cards_id:
                tmp_copy_data = obj.copy_data()
                tmp_copy_data[0]['id'] = obj.id
                tmp_copy_data[0]['card_type'] = obj.card_type.name
                tmp_copy_data[0]['card_type_id'] = obj.card_type.id
                tmp_copy_data[0]['active_to'] = obj.active_to
                tmp_copy_data[0]['card_type_string'] = obj.card_type_string
                if tmp_copy_data[0]['card_type_string'] == "Collects sale amount":
                    tmp_copy_data[0]['sale_percentage'] = obj.sale_percentage
                elif tmp_copy_data[0]['card_type_string'] == "Percentage":
                    tmp_copy_data[0]['sale_percentage'] = obj.discount_percentage
                cards_list += tmp_copy_data
            return json.dumps(cards_list)

    def get_card_info_by_partner_id(self, partner_id):
        cards_id = self.env['loyalty.card.card'].search([('primary_holder','=', partner_id),('state','=','usable')])
        if cards_id:
            cards_list = list()
            for obj in cards_id:
                tmp_copy_data = obj.copy_data()
                tmp_copy_data[0]['id'] = obj.id
                tmp_copy_data[0]['card_type'] = obj.card_type.name
                tmp_copy_data[0]['card_type_id'] = obj.card_type.id
                tmp_copy_data[0]['active_to'] = obj.active_to
                cards_list += tmp_copy_data
            return json.dumps(cards_list)
