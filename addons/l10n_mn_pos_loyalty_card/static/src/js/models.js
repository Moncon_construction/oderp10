odoo.define('l10n_mn_pos_loyalty_card.models', function (require) {
    "use strict";

    var utils = require('web.utils');
    var round_pr = utils.round_precision;
    var round_di = utils.round_decimals;
    var formats = require('web.formats');
    var models = require('point_of_sale.models');
    var _super_posmodel = models.PosModel.prototype;
    var _super_order = models.Order.prototype;

    models.Order = models.Order.extend({

        initialize: function(attributes, options){
//        Захиалгад бичигдэх лояалти картын утгуудыг хадгалах
            this.loyalty = {};
//            Захиалгын үеэр ашиглагдах лояалти картын утгуудыг хадгалах
            this.loyalty_card = {};

            return _super_order.initialize.call(this, attributes, options);
        },
        set_loyalty_data: function(key, value){
//        Ирж байгаа аргументийн утга нь 2 бол key, value 2 хамт ирсэн гэж үзнэ
            if(arguments.length === 2){
                this.loyalty[key] = value;
            }else if(arguments.length === 1){
//        Ирж байгаа аргументийн утга нь value, key 2 нь нэг гэж үзнэ
                for(var key in arguments[0]){
                    this.loyalty[key] = arguments[0][key];
                }
            }
        },
        get_loyalty_data: function(key){
            return this.loyalty[key];
        },
        export_as_JSON: function() {
            var result = _super_order.export_as_JSON.call(this);
            result['is_loyalty_used'] = (this.get_loyalty_data('is_loyalty_used'))? true : false;
            result['loyalty_card_init_balance'] = this.get_loyalty_data('loyalty_card_init_balance');
            result['loyalty_card_added_points'] = this.get_loyalty_data('loyalty_card_added_points');
            result['loyalty_card_spent_points'] = this.get_loyalty_data('loyalty_card_spent_points');
            result['loyalty_card_end_balance'] = this.get_loyalty_data('loyalty_card_end_balance');
            result['loyalty_card_id'] = this.get_loyalty_data('loyalty_card_id');
            result['loyalty_card_type'] = this.get_loyalty_data('loyalty_card_type');
            result['loyalty_card_holder'] = this.get_loyalty_data('loyalty_card_holder');
            result['loyalty_card_sale_percentage'] = this.get_loyalty_data('loyalty_card_sale_percentage');
            return result;
        },
        get_due: function(paymentline, bonus) {
            if(!bonus){
                bonus = 0;
            }
            var due = _super_order.get_due.call(this, paymentline) - bonus;
            return round_pr(Math.max(0,due), this.pos.currency.rounding);
        },
        get_change: function(paymentline, bonus) {
            if(!bonus || isNaN(bonus)){
                bonus = 0;
            }
            var change = _super_order.get_signed_change.call(this, paymentline) + parseFloat(bonus);
            return round_pr(Math.max(0,change), this.pos.currency.rounding);
        },
        is_paid: function(bonus){
            var client = this.attributes.client;
            if (client && this.to_invoice)
                return true;
            return this.get_due(false, bonus) <= 0;
        },
        get_total_with_tax: function(bonus_amount) {
            if(!bonus_amount || isNaN(bonus_amount)){
                bonus_amount = 0;
            }
            return _super_order.get_total_with_tax.call(this) - bonus_amount;
        },
        get_total_paid: function(bonus){
            if(!bonus){
                bonus = 0;
            }
        	var total_paid = _super_order.get_total_paid.call(this) + bonus;
            return round_pr(total_paid, this.pos.currency.rounding);
        }
    });
});