odoo.define('l10n_mn_pos_loyalty_card.loyalty_card_pos', function (require) {
	"use strict";

	var screen = require('point_of_sale.screens');
	var clientListScreen = require('l10n_mn_point_of_sale.screens').clientListScreen;
	var paymentScreen = require('point_of_sale.screens').PaymentScreenWidget;
    var receiptScreen = require('point_of_sale.screens').ReceiptScreenWidget;
    var gui = require('point_of_sale.gui').Gui;
	var core = require('web.core');
	var Model = require('web.Model');
	var utils = require('web.utils');
	var round_pr = utils.round_precision;
	var QWeb = core.qweb;
	var _t = core._t;

    // Харилцагч дээр карт сонгох дэлгэц зурах хэсэг
    var card;
    var pin_verified = false;
    var card_type;
    var card_changed = false;

	clientListScreen.include({
        display_client_details: function (visibility,partner,clickpos) {
			this._super(visibility,partner,clickpos);
            var model = new Model('res.partner');
            var loyalty_model = new Model('loyalty.card.card');
            self = this;
            if (partner){
                model.call("get_card_info",[partner.id]).then(function(result){
                    if(result){
                        var cards = JSON.parse(result);
                        var contents = self.$('.loyalty-cards-section');
                        contents.empty();
                        contents.append($(QWeb.render('PartnerLoyaltyCards',{widget:this,cards:cards})));
                        if (partner.loyalty_card){
                            var selected_card = $(document).find(`[data-id='${partner.loyalty_card.card_number}']`);
                            if (selected_card[0])
                                selected_card[0].classList.add('highlight');
                        }
                        else{
                            card = null;
                            pin_verified = false
                        }
                        self.$('.loyalty-cards-section').delegate('.card-line','click',function(event){
                            self.$('.loyalty-cards-section tr').removeClass('highlight');
                            if($('span.button.next').html().includes("Захиалагчийг үл сонгох")){
                                $('span.button.back').html("<i class='fa fa-angle-double-left'></i> Хадгалах(ESC)");
                            }
                            else{
                                $('span.button.back').html("<i class='fa fa-angle-double-left'></i> Цуцлах(ESC)");
                            }
                            if(partner.loyalty_card && partner.loyalty_card.card_number == $(this).data("id")){
                                partner.loyalty_card = null;
                                card = null;
                                card_type = null;
                                pin_verified = false;
                            }
                            else{
                                this.classList.add('highlight');
                                for(var obj in cards){
                                    if(cards[obj].card_number == $(this).data("id")){
                                        partner.loyalty_card = cards[obj];
                                        break;
                                    }
                                }
                            }
                        });
                    }
                });
            }
		},
	});
	var gue_screen_name;
    var gui_self;
    var payment_self;
	gui.include({

        show_screen: function(screen_name,params,refresh,skip_close_popup) {
            $('span.button.back').html("<i class='fa fa-angle-double-left'></i> Буцах(ESC)");
            this.show_bonus_card_panel(screen_name, this);
            this._super(screen_name,params,refresh,skip_close_popup);
        },
        //Картны дэлгэцийг зурах функц хэсэг
        show_bonus_card_panel: function(screen_name, self){
            gue_screen_name = screen_name;
            gui_self = self;
            var partner = this.pos.get_client();
            var loyalty_model = new Model('loyalty.card.card');
            var contents = $('.card-section');
            contents.empty();
            if(screen_name.includes('payment') && partner && partner.loyalty_card){
                if(card && card !== partner.loyalty_card){
                    card_changed = true;
                    pin_verified = false;
                }
                card = partner.loyalty_card
                loyalty_model.call("get_card_type",[partner.loyalty_card.id]).then(function(result){
                    if(result){
                        card_type = JSON.parse(result);
                    }
                    contents.empty();
                    contents.append($(QWeb.render('PosLoyaltyCardScreen',{widget:self, card:card, card_type:card_type})));
                });
            }
            else{
                contents.empty();
                contents.append($(QWeb.render('PosLoyaltyCardScreen',{widget:self})));
            }
            if (payment_self)
                payment_self.render_paymentlines();
        },
	});

    var paybonuscard_clicked = false;
    var card_payment_line;
    var bonus_amount = 0;
    var card_discount = false;

    var amount_bonus = 0 ;
    var bonus_added = 0 ;

    paymentScreen.include({
        events: {
            "click .paybonuscard": "add_loyalty_payment_line",
            "click .search-bar":"show_cards_of_partner",
        },

        add_loyalty_payment_line: function(){
            var self  = this;
            var order = this.pos.get_order();
            if (!order) {
                return;
            }
            var lines = order.get_paymentlines();
            var due   = order.get_due(false, (bonus_amount ? bonus_amount : 0));
            var extradue = 0;
            if(!this.pos.get_client()){
                card = null;
                pin_verified = false
            }
            if (!card){
                card_payment_line = null;
                bonus_amount = 0;
                paybonuscard_clicked = false;
                if(card_discount){
                    card_discount = false;
                    this.clear_discount();
                }
            }
            else if (card_changed ){
                card_changed = false;
                paybonuscard_clicked = false;
                bonus_amount = 0;
                card_payment_line = null;
                if(card_discount){
                    card_discount = false;
                    this.clear_discount();
                }
            }
            if(card && card.card_type_string == "Percentage" && !card_discount){
                this.set_discount(card.sale_percentage);
                card_discount = true;
            }

            if (event && event.path[0].className.includes("paybonuscard")){
                if(!card){
                    alert("Карт сонгогдоогүй байна!");
                }
                else if(card.card_type_string != "Collects sale amount"){
                    alert("Үнийн дүн цуглуулдаг карт биш байна !");
                }
                else if(!paybonuscard_clicked){
                    if(pin_verified){
                        var maximum_pay = (card_type.maximum_amount_percentage/100)*order.get_total_with_tax()
                        bonus_added = ((order.get_total_with_tax() *card.sale_percentage ) / 100 ).toFixed(2);
                        card_type.bonus_added = bonus_added || 0;
                        card_payment_line = {
                            'cid':'LLTYCRD',
                            'name':card.card_type,
                        }
                        if(card.card_balance > due){
                            if(maximum_pay > due){
                                card.card_balance -= due;
                                card_payment_line.amount = due;
                                due = 0;
                            }
                            else{
                                card.card_balance -= maximum_pay;
                                card_payment_line.amount = maximum_pay;
                                due -= maximum_pay;
                            }
                        }
                        else{
                            if(card.card_balance > maximum_pay){
                                card_payment_line.amount = maximum_pay;
                                due -= maximum_pay;
                                card.card_balance -= maximum_pay;
                            }
                            else{
                                card_payment_line.amount = card.card_balance;
                                due -= card.card_balance;
                                card.card_balance = 0;
                            }
                        }
                        gui_self.show_bonus_card_panel(gue_screen_name, gui_self);
                        paybonuscard_clicked = true;
                        bonus_amount = card_payment_line.amount;
                        amount_bonus = card_payment_line.amount;
                        self.render_paymentlines();
                        self.order_changes();
                    }
                    else{
                        this.gui.ask_password(card.password).then(function () {
                            pin_verified = true;
                            var maximum_pay = (card_type.maximum_amount_percentage/100)*order.get_total_with_tax()
                            bonus_added = ((order.get_total_with_tax() *card.sale_percentage ) / 100 ).toFixed(2);
                            card_type.bonus_added = bonus_added || 0;
                            card_payment_line = {
                                'cid':'LLTYCRD',
                                'name':card.card_type,
                            }
                            if(card.card_balance > due){
                                if(maximum_pay > due){
                                    card.card_balance -= due;
                                    card_payment_line.amount = due;
                                    due = 0;
                                }
                                else{
                                    card.card_balance -= maximum_pay;
                                    card_payment_line.amount = maximum_pay;
                                    due -= maximum_pay;
                                }
                            }
                            else{
                                if(card.card_balance > maximum_pay){
                                    card_payment_line.amount = maximum_pay;
                                    due -= maximum_pay;
                                    card.card_balance -= maximum_pay;
                                }
                                else{
                                    card_payment_line.amount = card.card_balance;
                                    due -= card.card_balance;
                                    card.card_balance = 0;
                                }
                            }
                            gui_self.show_bonus_card_panel(gue_screen_name, gui_self);
                            paybonuscard_clicked = true;
                            bonus_amount = card_payment_line.amount;
                            amount_bonus = card_payment_line.amount;
                            self.render_paymentlines();
                            self.order_changes();
                        });
                    }
                }
                else{
                    return;
                }
            }
        },

        show: function () {
            payment_self = this;
			var self = this;
			this._super();
			this.keyDownHandler = this.keyDownHandler.bind(this);
			window.document.body.addEventListener('keydown', this.keyDownHandler);

			var order = this.pos.get_order();
			if (order.ebarimt === undefined) { //initialize property ebarmt
				order.ebarimt = {
					companyName: "",
					companyReg: 0,
					ddtd: 0,
					lottery: '',
					qrData: 0
				};
			}
			var register = $('#register');

			/* Bill-type events */
			$('.bill-type .paymentmethod').change(function () {
				var order = self.pos.get_order();
				var selected = document.getElementById("is_print_bill").value;
				var rows = $('.bill-type .flex-row');
				if (selected === 'company') {
					$(rows[0]).removeClass('only-child');
					register.val('');
					$('.bill-type .company-name').text('');
					rows.show();
				} else {
					$(rows[0]).addClass('only-child');
					rows.slice(-2).hide();
				}

				order.ebarimt.companyReg = 0;
				order.ebarimt.companyName = "";
			});
			$('.bill-type .paymentmethod').val('person');
			$('.bill-type .paymentmethod').change();

			register.keyup(function (e) {
				if (e.keyCode === 13) {
					var resp = self.pos.config.ebarimt_info_address + register.val();
					var custom_model = new Model('pos.order')
					custom_model.call('get_merchant_info', [resp]).then(function (your_return_value) {
						var jsonfile = JSON.parse(your_return_value);

						order.ebarimt.companyReg = register.val();
						order.ebarimt.companyName = jsonfile.name;
						if (register.val() == "") {
							alert("Татвар төлөгчийн дугаарыг оруулна уу!");
						}
						if (register.val() == self.pos.company.company_registry) {
							alert("Та өөрийн компани луу борлуулалт хийж болохгүй!");
						}
						if (jsonfile.found == false) {
							alert(register.val() + " регистрийн дугаартай компани бүртгэлгүй байна!");
						}

						document.getElementById("company_name").innerHTML = jsonfile.name;
					});
				}
			});
		},

		keyDownHandler: function (e) {
            var key;
            var $password_input = $(".popup-input.value");
			if (e.keyCode == 113) { //f2 - Карт хайх товч
				e.preventDefault();
				if ($(".search-bar input").is(":focus")) {
					$(".search-bar input").blur();
				} else {
					$(".search-bar input").focus();
				}
            }
            else if($(".search-bar input").is(":focus")){
                if (e.keyCode === 8) {// BACKSPACE
                    key='BACKSPACE';
                }else if (e.keyCode === 13) {// Enter
                    key='ENTER';
                } else if (e.keyCode >= 48 && e.keyCode <= 57) { // Numbers
                    key = '' + (e.keyCode - 48);
                }
                else if (e.keyCode >= 96 && e.keyCode <= 105) { // Numbers
                    key = '' + (e.keyCode - 96);
                }
                this.search_input_value(key);
                e.preventDefault();
            }
            else if ($password_input.is(':visible')){
                if (e.keyCode === 8) {// BACKSPACE
                    key='BACKSPACE';
                }else if (e.keyCode === 13) {// Enter
                    key='ENTER';
                } else if (e.keyCode >= 48 && e.keyCode <= 57) { // Numbers
                    key = '' + (e.keyCode - 48);
                }
                else if (e.keyCode >= 96 && e.keyCode <= 105) { // Numbers
                    key = '' + (e.keyCode - 96);
                }
                this.pass_password_input_value(key);
                e.preventDefault();
		    }
        },

		close: function () {
			this._super();
			window.document.body.removeEventListener('keydown', this.keyDownHandler);
		},

        pass_password_input_value: function(key){
            if(key){
                switch (key) {
					case '0':
						$(".popup-numpad .input-button.number-char:contains('0')").filter(':visible').click();
						break;
					case '1':
						$(".popup-numpad .input-button.number-char:contains('1')").filter(':visible').click();
						break;
					case '2':
                        $(".popup-numpad .input-button.number-char:contains('2')").filter(':visible').click();
						break;
					case '3':
						$(".popup-numpad .input-button.number-char:contains('3')").filter(':visible').click();
						break;
					case '4':
						$(".popup-numpad .input-button.number-char:contains('4')").filter(':visible').click();
						break;
					case '5':
						$(".popup-numpad .input-button.number-char:contains('5')").filter(':visible').click();
						break;
					case '6':
						$(".popup-numpad .input-button.number-char:contains('6')").filter(':visible').click();
						break;
					case '7':
						$(".popup-numpad .input-button.number-char:contains('7')").filter(':visible').click();
						break;
					case '8':
						$(".popup-numpad .input-button.number-char:contains('8')").filter(':visible').click();
						break;
					case '9':
						$(".popup-numpad .input-button.number-char:contains('9')").filter(':visible').click();
						break;
                    case "ENTER":
                        $(".footer.centered .button.confirm").filter(':visible').click();
						break;
                    case "BACKSPACE":
						$(".popup-numpad .input-button.numpad-backspace").filter(':visible').click();
						break;
				}
            }
        },

		search_input_value: function(key){
		    if(key){
		        if(key =="BACKSPACE"){
                    $(".search-bar input").val(
                        function (index, value) {
                            return value.substr(0, value.length - 1);
                        })
		        }
		        else if(key =="ENTER"){
                    var value = $(".search-bar input").val();
                    if(value.length < 4){
                        alert("Картын хайлтанд хамгийн багадаа 6 орон оруулна уу!");
                    }
                    else{
                        this.show_cards_of_partner(false, value);
                    }
		        }
                else{
                    var input = $(".search-bar input").val() + key;
                    $(".search-bar input").val(input);
                    this.show_cards_of_partner(false, input);
                }
		    }
		},

        show_cards_of_partner: function(event, value){
            var partner = this.pos.get_client();
            var show_card_this = this;
            var cards;
            if(value){
                var loyalty_model = new Model('loyalty.card.card');
                var loyalty_card_number = {
                    "loyalty_card_number": value,
                };
                loyalty_model.call("get_card_by_card_number", [1], loyalty_card_number).then(function(result){
                    if(result){
                        cards = JSON.parse(result);
                        if(cards.length>1){
                            var options = '';
                            for (var i = 0; i < cards.length; i++) {
                                var card_1 = cards[i];
                                options += '<option value="' + card_1.card_number + '" />';
                            }
                            document.getElementById('card_datalist').innerHTML = options;

                            var contents = self.$('.card-section .card-search');
                            self.$('.card-section .search-bar .card-search').delegate('.card-line','click',function(event){
                                for(var card in cards){
                                    if(cards[card].card_number == $(this).data("card_number")){
                                        partner.loyalty_card = cards[card];
                                        break;
                                    }
                                }
                            });
                        }
                        else{
                            var partner = show_card_this.pos.db.get_partner_by_id(cards[0].primary_holder);
                            if(partner){
                                show_card_this.pos.get_order().set_client(partner);
                                partner = show_card_this.pos.get_client();
                                partner.loyalty_card = cards[0];
                                gui_self.show_bonus_card_panel(gue_screen_name, gui_self);
                            }
                        }
                    }
                });
            }
        },

        set_discount: function(val) {
            var order = this.pos.get_order();
            var orders = order.get_orderlines();
            for(var i=0;i<orders.length;i++){
                orders[i].set_discount(val);
            }
        },

        clear_discount: function() {
            var order = this.pos.get_order();
            var orders = order.get_orderlines();
            for(var i=0;i<orders.length;i++){
                orders[i].set_discount(0);
            }
        },

        clear_bonus_values: function(){
            card_payment_line = null;
            card.card_balance += bonus_amount;
            gui_self.show_bonus_card_panel(gue_screen_name, gui_self);
            bonus_amount = 0;
            paybonuscard_clicked = false;
        },
        // Override
        render_paymentlines: function(event) {
            var self  = this;
            var order = this.pos.get_order();
            if (!order) {
                return;
            }
            var lines = order.get_paymentlines();
            var due   = order.get_due(false, (bonus_amount ? bonus_amount : 0));
            var extradue = 0;
            if (!card){
                card_payment_line = null;
                bonus_amount = 0;
                paybonuscard_clicked = false;
                if(card_discount){
                    card_discount = false;
                    this.clear_discount();
                }
            }
            else if (card_changed ){
                card_changed = false;
                paybonuscard_clicked = false;
                bonus_amount = 0;
                card_payment_line = null;
                if(card_discount){
                    card_discount = false;
                    this.clear_discount();
                }
            }
            if (due && lines.length  && due !== order.get_due(lines[lines.length-1], bonus_amount)) {
                extradue = due;
            }
            else if(!lines.length){
                extradue = order.get_total_with_tax(bonus_amount);
            }
            this.$('.paymentlines-container').empty();
            var lines = $(QWeb.render('PaymentScreen-Paymentlines', {
                widget: this,
                order: order,
                paymentlines: lines,
                extradue: extradue,
                // BEGIN CHANGES
                //adding bonus card value to render screen
                card_payment_line: card_payment_line,
                //END CHANGES
            }));

            //Deleting bonus payment line
            lines.on('click','.loyalty-delete-button',function(event){
                self.clear_bonus_values();
                self.order_changes();
                self.render_paymentlines();
            });
            lines.on('click','.delete-button',function(){
                self.click_delete_paymentline($(this).data('cid'));
            });

            lines.on('click','.paymentline',function(){
                self.click_paymentline($(this).data('cid'));
            });
            lines.appendTo(this.$('.paymentlines-container'));
        },

        order_changes: function(){
            var self = this;
            var order = this.pos.get_order();
            if (!order) {
                return;
            } else if (order.is_paid(bonus_amount)) {
                self.$('.next').addClass('highlight');
            }else{
                self.$('.next').removeClass('highlight');
            }
        },

		validate_order: function (force_validation, pharmacy, bonus_amount, card) {
			var self = this;
			if ($(".payment-screen .button.next.highlight").length > 0) {
			    if(bonus_amount){
                    amount_bonus = bonus_amount;
			    }
				var order = this.pos.get_order();

			    if (card){
                    var total_amount = order.get_total_with_tax();
                    bonus_added = ((total_amount *card.sale_percentage ) / 100 ).toFixed(2);
			        card_type = card.card_type;
			    }

				var stocks = [];
				var totalVat = 0;
				var totalCityTax = 0;
				var taxInfo = {};

				var total_with_tax = order.get_total_with_tax();
				var paid_amount = total_with_tax - bonus_amount;
				var pay_to_percent = paid_amount/total_with_tax;

				/* Бүх tax-уудын цуглуулгыг үүсгэнэ */
				order.get_tax_details().forEach(function (item) {
					var taxId = item.tax.id;
					var taxAmount = item.tax.amount;
					taxInfo.taxId = taxAmount;
				});

				/* stock-ийг угсрах */
                order.orderlines.models.forEach(function (item) {
					var totalAmount = (item.price * item.quantity);
					var productCode = item.product.default_code;
					var categoryCodeId, regex, m;

					if (!productCode) { // барааны код байхгүй бол БАРКОД-оо авна.
						productCode = item.product.barcode;
					}
					if (!productCode) { // БАРКОД байхгүй бол Ангилалын код авна
						if (item.product.vat_category_code){
							productCode = item.product.vat_category_code;
						} else {
							productCode = "";
						}
					}

					var productVat = totalAmount / 1.1 * 0.10;
					totalVat += productVat;

					var cityTax = 0;
					for (var i = 0; i < item.product.taxes_id.length; i++) {
						if (taxInfo[item.product.taxes_id[i]] == 1) {
							cityTax = (item.price / 1.1 * item.quantity * 0.01);
							totalCityTax = cityTax;
							break;
						}
					}
					var barcode = item.product.barcode;
					if (!barcode) { // БАРКОД байхгүй бол Ангилалын код авна
						if (item.product.vat_category_code){
							barcode = item.product.vat_category_code;
						} else {
							barcode = "";
						}
					}
					stocks.push({
						"code": productCode.toString(),
						"name": item.product.display_name.replace("&", " ЭНД "),
						"measureUnit": item.product.uom_id[1],
						"qty": item.quantity.toFixed(2),
						"unitPrice": (item.price.toFixed(2) * pay_to_percent).toFixed(2),
						"totalAmount": (totalAmount * pay_to_percent).toFixed(2),
						"cityTax": (cityTax * pay_to_percent).toFixed(2),
						"vat": (productVat * pay_to_percent).toFixed(2),
						"barcode": barcode.toString()
					});
				});
				var billType = order.ebarimt.companyReg ? "3" : "1";
				var customerNo = order.ebarimt.companyReg ? order.ebarimt.companyReg.toString() : "";

				/* ebarimt API-ийн PUT лүү явуулах датаг угсрах */
				var data = {
					"amount": (order.get_total_with_tax() - bonus_added).toFixed(2),
					"vat": (totalVat * pay_to_percent).toFixed(2),
					"cashAmount": (order.get_total_with_tax() - bonus_added).toFixed(2),
					"nonCashAmount": "0.00",
					"cityTax": (totalCityTax * pay_to_percent).toFixed(2),
					"reportMonth": "",
					"districtCode": "26",
					"posNo": "0001",
					"customerNo": customerNo,
					"billType": billType,
					"billIdSuffix": "",
					"returnBillId": "",
					"stocks": stocks
				};
                if(self.pos.company.check_vatpsp_service == true){
                    try {
                        var xmlHttp = new XMLHttpRequest();
                        xmlHttp.open("GET", self.pos.config.vatps_address.replace("put", "getInformation"), false );
                        xmlHttp.send();
                        console.log(xmlHttp.responseText);
                    } catch(e) {
                        alert("Ebarimt сервистэй холбогдоход алдаа гарлаа", e);
                        return false
                    }
                }
                /* POSAPI-ийн PUT лүү ajax явуулах */
                if(pharmacy != true){
                    $.ajax({
                        type: "POST",
                        url: self.pos.config.vatps_address, //http://10.0.9.239:8028/put/
                        data: "param=" + JSON.stringify(data),
                        async: false,
                        success: function (response) {
                            console.log("SERVICE PUT response", response);
                            try {
                                var jsonResp = JSON.parse(response);
                                if (response.success == 'false' || response.success === false) {
                                    alert(response.message);
                                } else {
                                    order.ebarimt.billId = jsonResp.billId;
                                    order.ebarimt.lottery = jsonResp.lottery;
                                    order.ebarimt.qrData = jsonResp.qrData;
                                }
                            } catch (e) {
                                alert("JSON хөрвүүлэхэд алдаа гарлаа", e);
                            }
                        },
                        error: function (request, status, error) {
                            console.error(request.responseText, error);
                        }
                    });
                }


			}
			return this._super();
		},

        order_is_valid: function(force_validation) {
            var self = this;
            var order = this.pos.get_order();

            // process empty orders. This is not the right place to fix it.
            if (order.get_orderlines().length === 0) {
                this.gui.show_popup('error',{
                    'title': _t('Empty Order'),
                    'body':  _t('There must be at least one product in your order before it can be validated'),
                });
                return false;
            }

            var plines = order.get_paymentlines();
            for (var i = 0; i < plines.length; i++) {
                if (plines[i].get_type() === 'bank' && plines[i].get_amount() < 0) {
                    this.gui.show_popup('error',{
                        'message': _t('Negative Bank Payment'),
                        'comment': _t('You cannot have a negative amount in a Bank payment. Use a cash payment method to return money to the customer.'),
                    });
                    return false;
                }
            }
            if (!order.is_paid(bonus_amount) || this.invoicing) {
                return false;
            }

            // The exact amount must be paid if there is no cash payment method defined.
            if (Math.abs(order.get_total_with_tax() - order.get_total_paid(bonus_amount)) > 0.00001) {
                var cash = false;
                for (var i = 0; i < this.pos.cashregisters.length; i++) {
                    cash = cash || (this.pos.cashregisters[i].journal.type === 'cash');
                }
                if (!cash) {
                    this.gui.show_popup('error',{
                        title: _t('Cannot return change without a cash payment method'),
                        body:  _t('There is no cash payment method available in this point of sale to handle the change.\n\n Please pay the exact amount or add a cash payment method in the point of sale configuration'),
                    });
                    return false;
                }
            }

            // if the change is too large, it's probably an input error, make the user confirm.
            if (!force_validation && order.get_total_with_tax() > 0 && (order.get_total_with_tax() * 1000 < order.get_total_paid(bonus_amount))) {
                this.gui.show_popup('confirm',{
                    title: _t('Please Confirm Large Amount'),
                    body:  _t('Are you sure that the customer wants to  pay') +
                           ' ' +
                           this.format_currency(order.get_total_paid()) +
                           ' ' +
                           _t('for an order of') +
                           ' ' +
                           this.format_currency(order.get_total_with_tax()) +
                           ' ' +
                           _t('? Clicking "Confirm" will validate the payment.'),
                    confirm: function() {
                        self.validate_order('confirm');
                    },
                });
                return false;
            }

            return true;
        },
        //OVERRIDE
        /*Loyalty card history has to be created after order is created so "buy" method is called inside this function
        after "push_order" is called
        Урамшууллын картын түүх ПОС захиалга үүссэний дараа үүсэх шаардлагатай тул "buy" method-г "push_order" дуудаж
        буй энэ функц дотор оруулж өгч байна.*/
        finalize_validation: function() {
            var self = this;
            var order = this.pos.get_order();
            var vals = {}
            if(card){
                var total_amount = order.get_total_with_tax();
                bonus_added = ((total_amount * card.sale_percentage ) / 100 ).toFixed(2);
                if(!bonus_amount){
                    bonus_amount = 0;
                }
                vals = {
                    'order_name': order.name,
                    'total_amount': total_amount,
                    'sale_percentage': card.sale_percentage,
                    'sale_amount': total_amount - bonus_amount,
                    'pay_amount': total_amount - bonus_amount,
                    'bonus_added': bonus_added,
                    'bonus_spent': bonus_amount,
                };
//              BEGINS  лояалти карт ашигласан бол мэдээллийг нэмж байна
                order.set_loyalty_data("is_loyalty_used", true);
                order.set_loyalty_data('loyalty_card_init_balance', parseFloat(parseFloat(card.card_balance) + parseFloat(bonus_amount)));
                order.set_loyalty_data('loyalty_card_added_points', bonus_added);
                order.set_loyalty_data('loyalty_card_spent_points', bonus_amount);
                order.set_loyalty_data('loyalty_card_end_balance', parseFloat(parseFloat(card.card_balance) + parseFloat(bonus_added)));
                order.set_loyalty_data('loyalty_card_id', card.id);
                order.set_loyalty_data('loyalty_card_type', card.card_type_id);
                order.set_loyalty_data('loyalty_card_holder', card.primary_holder);
                order.set_loyalty_data('loyalty_card_sale_percentage', card.sale_percentage);
//              ENDS  лояалти карт ашигласан бол мэдээллийг нэмж байна

            }

            if (order.is_paid_with_cash() && this.pos.config.iface_cashdrawer) {

                    this.pos.proxy.open_cashbox();
            }

            order.initialize_validation_date();
            order.finalized = true;

            if (order.is_to_invoice()) {
                var invoiced = this.pos.push_and_invoice_order(order);
                this.invoicing = true;

                invoiced.fail(function(error){
                    self.invoicing = false;
                    order.finalized = false;
                    if (error.message === 'Missing Customer') {
                        self.gui.show_popup('confirm',{
                            'title': _t('Please select the Customer'),
                            'body': _t('You need to select the customer before you can invoice an order.'),
                            confirm: function(){
                                self.gui.show_screen('clientlist');
                            },
                        });
                    } else if (error.code < 0) {        // XmlHttpRequest Errors
                        self.gui.show_popup('error',{
                            'title': _t('The order could not be sent'),
                            'body': _t('Check your internet connection and try again.'),
                        });
                    } else if (error.code === 200) {    // OpenERP Server Errors
                        self.gui.show_popup('error-traceback',{
                            'title': error.data.message || _t("Server Error"),
                            'body': error.data.debug || _t('The server encountered an error while receiving your order.'),
                        });
                    } else {                            // ???
                        self.gui.show_popup('error',{
                            'title': _t("Unknown Error"),
                            'body':  _t("The order could not be sent to the server due to an unknown error"),
                        });
                    }
                });

                invoiced.done(function(){
                    self.invoicing = false;
                    self.gui.show_screen('receipt');
                });
            } else {
                order.loyalty_card = card
                this.pos.push_order(order).then(function(result){
                //BEGINS Захиалга үүсний дараа лояалти картын түүх рүү бичилт хийж байна
                    if(order.loyalty_card){
                        var loyalty_model = new Model('loyalty.card.card');
                        loyalty_model.call("buy",[order.loyalty_card.id], vals);
                    }
                });
                //ENDS Захиалга үүсний дараа лояалти картын түүх рүү бичилт хийж байна

                this.gui.show_screen('receipt');
            }
            this._super();
        },
    });

	receiptScreen.include({
		show: function () {
			this._super();
			window.document.body.addEventListener('keydown', this.keyDownHandler);
		},
		keyDownHandler: function (e) {
		    if(e.target.className != 'card-line'){
                switch (e.keyCode) {
                    case 35: //END - дараагийн захиалга
                        e.preventDefault();
                        $(".receipt-screen .top-content .button.next.highlight").click();
                        break;
                    case 116: //F5 - хэвлэх
                        e.preventDefault();
                        $(".receipt-screen .centered-content .button.print").click();
                        break;
                    case 13: //Enter - Дараагийн хуудас руу шилжих
                        e.preventDefault();
                        this.click_next();
                        break;
                }
		    }
		},
		click_next: function () {
			this._super();
			//detach keyDownHandler
			window.document.body.removeEventListener('keydown', this.keyDownHandler);
		},
		get_ebarimt_data: function () {
			var order = this.pos.get_order();
			var ebarimt = order.ebarimt;

			return {
				companyName: ebarimt.companyName,
				ddtd: ebarimt.billId,
				lottery: ebarimt.lottery,
				qrData: ebarimt.qrData
			};
		},
		format_precision: function (quantity) {
			var quant = parseFloat(quantity) || 0;
			return round_pr(quant, 0.01);
		},
		render_receipt: function () {
			var order = this.pos.get_order();
			var ebarimtData = this.get_ebarimt_data();
			console.log("Order PharmDiscount", order.pharmDiscount);
			/* render QWeb template */
			amount_bonus = parseFloat(amount_bonus).toFixed(2);
			var receiptContainer = this.$('.pos-receipt-container').html(QWeb.render('PosTicket', {
				widget: this,
				order: order,
				receipt: order.export_for_printing(),
				orderlines: order.get_orderlines(),
				paymentlines: order.get_paymentlines(),
				pharmDiscount: order.pharmDiscount,
				ebarimt: ebarimtData,
				amount_bonus: amount_bonus,
				card_type : card_type,
				bonus_added: parseFloat(bonus_added).toFixed(2),
				card_balance: card? (parseFloat(card.card_balance) + parseFloat(bonus_added)).toFixed(2) : 0,
			}))
			card_type = undefined;
			amount_bonus = undefined;
			bonus_added = undefined;
			card = undefined;
			pin_verified = false;
			/* draw qrcode */
			if (ebarimtData.qrData) {
				var qrcode = new QRCode(receiptContainer.find('#myid_info_qrcode')[0], {
					width: 180,
					height: 180,
					correctLevel: QRCode.CorrectLevel.L
				});
				qrcode.makeCode(ebarimtData.qrData);
			}
			/* set ebarimt data */
			for(var key in ebarimtData){
				order.set_ebarimt_data(key, ebarimtData[key]);
			}
			// $(".receipt-screen .centered-content .button.print").click();
		}
	});
});