# -*- coding: utf-8 -*-

{
    'name': 'Mongolian Point of Sale Menu at Restaurant',
    'version': '1.0',
    'author': "Asterisk Technologies LLC",
    'category': 'Point of Sale',
    'sequence': 7,
    'summary': 'Restaurant extensions for the Mongolian Point of Sale',
    'depends': ['pos_restaurant', 'l10n_mn_point_of_sale'],
    'website': 'http://asterisk-tech.mn',
    'description': """

=======================

This module adds several restaurant features to the Point of Sale:
- Menu: Allows you to register menus for POS & filter point of sale products depends on configured menu on POS configuration.

""",
    'data': [
        'security/ir.model.access.csv',
        'security/ir_rule.xml',
        'views/pos_menu_views.xml',
        'views/pos_config_views.xml',
        'views/template.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
