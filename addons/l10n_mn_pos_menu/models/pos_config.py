# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class PosConfig(models.Model):
    _inherit = 'pos.config'

    menu_id = fields.Many2one('pos.menu', string='Menu')
    
    @api.multi
    def get_products_from_config(self, orders, fields, domain):
        # Called from .js
        # Return product.product fields for set products at POS.
        
        # Build order of product
        full_orders = ""
        if orders:
            for order in orders:
                if order != orders[-1]:
                    full_orders += (order + " DESC, ")
                else:
                    full_orders += (order + " DESC")
                    
        # Get products
        products = self.env['product.product'].sudo().search(domain, order=full_orders)
        prod_ctx = products.with_context(pricelist=self.pricelist_id.id, display_default_code=False, lang=self.env.user.lang)
        
        # Return product fields as list of dictionary.
        return prod_ctx.read(fields)
    
