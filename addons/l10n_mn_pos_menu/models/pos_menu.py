# -*- coding: utf-8 -*-
from odoo import _, api, fields, models, tools


class PosMenu(models.Model):
    _name = 'pos.menu'
    _inherit = 'mail.thread'

    name = fields.Char(required=True, track_visibility='on_change', string='Menu name')
    active = fields.Boolean(default=True, track_visibility='on_change', string='Active')
    company_id = fields.Many2one('res.company', track_visibility='on_change', string='Company')
    product_ids = fields.Many2many('product.product', 'menu_to_product_rel', 'menu_id', 'product_id', required=True, track_visibility='on_change', string='Product')

