odoo.define('l10n_mn_pos_menu.DB', function (require) {
"use strict";
    
	var PosDB = require('point_of_sale.DB');

    PosDB.include({
        init: function(options){
            this._super(options);

            this.menu_id = {};
			this.product_by_menu_id = {};
			
			this.config_id = {};
			this.menu_by_config_id = {};
        },
        add_menus: function(menus){
        	for(var i=0, len = menus.length; i < len; i++){
                this.menu_id[menus[i].id] = menus[i];
				this.product_by_menu_id[menus[i].id] = menus[i].product_ids;
            }
        },
		add_configs: function(configs){
        	for(var i=0, len = configs.length; i < len; i++){
                this.config_id[configs[i].id] = configs[i];
				this.menu_by_config_id[configs[i].id] = configs[i].menu_id;
            }
        },
	    get_menu: function(menu_id){
       		var menu = this.menu_id[menu_id];
	        return menu;
	    },
		get_product_by_menu: function(menu_id){
       		var product_ids  = this.product_by_menu_id[menu_id];
	        return product_ids;
	    },
		get_menu_by_config: function(config_id){
			var menu = this.menu_by_config_id[config_id];
	        return menu;
		}
    });

    return PosDB;
});