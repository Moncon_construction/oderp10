odoo.define('l10n_mn_pos_menu.models', function (require) {
    "use strict";
    var models = require('point_of_sale.models');
    var core = require('web.core');
	var Model = require('web.DataModel');
    var _super_posmodel = models.PosModel.prototype;
	var _t = core._t;
    
	models.PosModel = models.PosModel.extend({
		
		initialize: function (session, attributes) {
			models.load_models([
				// Add 'pos.menu' model to "point_of_sale.DB"
				{
					model:  'pos.menu',
			        fields: ['id', 'name', 'product_ids'],
			        loaded: function(self, menus){
						self.db.add_menus(menus);
			        }
				},
				// link 'pos.menu' to 'pos.config'
				{
					model:  'pos.config',
			        fields: ['id', 'name', 'menu_id'],
			        loaded: function(self, configs){
						self.db.add_configs(configs);
			        }
				}
			]);
			return _super_posmodel.initialize.call(this, session, attributes);
        },

        load_server_data: function () {
			var self = this;
			
			// Get parent 'product.product' models & its attributes for filter 'product.product' by 'pos.config' menu.
            var product_index = _.findIndex(this.models, function (model) {
                return model.model === "product.product";
            });
            if(product_index != -1){
            	var product_model = this.models[product_index];
    			var product_order = product_model.order;
                var product_fields = product_model.fields;
                var product_domain = product_model.domain;

    			// for load product.product by menu, remove old data.
                this.models.splice(product_index, 1);

    			// after load_server_data filter products by menu products. 
                return _super_posmodel.load_server_data.apply(this, arguments).then(function () {
    				var menu = self.db.get_menu_by_config(self.pos_session.config_id[0]);
    				var product_ids = self.db.get_product_by_menu(menu[0]);
    				
    				if (menu && product_ids){
    					product_domain.push(["id", 'in', product_ids]);
    				}
    				
                    var records = new Model('pos.config').call('get_products_from_config', [self.pos_session.config_id[0], product_order, product_fields, product_domain]);
                    self.chrome.loading_message(_t('Loading') + ' product.product', 1);
                    return records.then(function (product) {
                        self.db.add_products(product);
                    });
                });
            }else{
            	return _super_posmodel.load_server_data.apply(this, arguments);
            }
        }
    });

	return {
		PosModel: models.PosModel,
	};
    
});