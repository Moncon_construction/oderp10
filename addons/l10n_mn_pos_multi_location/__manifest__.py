# -*- coding: utf-8 -*-

{
    'name': 'Mongolian Point of Sale at Multi Location at Restaurant',
    'version': '1.0',
    'author': "Asterisk Technologies LLC",
    'category': 'Point of Sale',
    'summary': 'Restaurant extensions for the Mongolian Point of Sale at Multi Location',
    'depends': ['l10n_mn_pos_menu'],
    'website': 'http://asterisk-tech.mn',
    'description': """

=======================

 Уг модуль нь дараах боломжуудыг өгнө:
 1. Борлуулалтын цэг дээр олон байрлалын тохиргоог меню бүрээр хийх боломж.
 2. Борлуулалтын цэгээр зарагдсан барааны бүртгэгдсэн менюнд харгалзах байрлалаар агуулахын ажилбар үүсгэх боломж. /Нэг захиалгад холбоотой олон ажилбаруудыг хянах боломж/
 3. Захиалга үүсэх үед ямар ч менюнд бүртгэгдээгүй бараа борлуулсан бол сэшн хаахаас өмнө уг барааг менюнд бүртгээд, сэшн хаах үед уг барааны ажилбарыг автоматаар үүсгэх боломж.

""",
    'data': [
        'security/ir.model.access.csv',
        'security/ir_rule.xml',
        'views/pos_menu_location_views.xml',
        'views/pos_config_views.xml',
        'views/pos_config_settings_views.xml',
        'views/pos_order_views.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
