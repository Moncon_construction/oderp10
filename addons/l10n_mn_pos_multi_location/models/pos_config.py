# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class PosConfig(models.Model):
    _inherit = 'pos.config'

    use_multi_location = fields.Boolean(related='company_id.use_multi_location', string="Use Multi Location on POS")
    menu_location_ids = fields.One2many('pos.menu.location', 'pos_config_id', string='Pos Menu Location')
    menu_location_id = fields.Many2one('pos.menu', related='menu_location_ids.menu_id', string='Pos Menu')

