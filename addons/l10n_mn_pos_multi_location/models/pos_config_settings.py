# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class PosConfigSettings(models.TransientModel):
    _inherit = 'pos.config.settings'

    use_multi_location = fields.Boolean(related='company_id.use_multi_location', default=lambda x: x.company_id.use_multi_location, string="Use Multi Location on POS")

