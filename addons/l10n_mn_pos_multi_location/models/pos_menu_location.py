# -*- coding: utf-8 -*-
from odoo import _, api, fields, models, tools


class PosMenuLocation(models.Model):
    _name = 'pos.menu.location'
    _inherit = 'mail.thread'

    company_id = fields.Many2one('res.company', default=lambda x: x.env.user.company_id, string='Company')
    pos_config_id = fields.Many2one('pos.config', string='Pos Config')
    picking_type_id = fields.Many2one('stock.picking.type', required=True, string='Picking Type')
    location_src_id = fields.Many2one('stock.location', related='picking_type_id.default_location_src_id', store=True, readonly=True, string='Product Location')
    menu_id = fields.Many2one('pos.menu', required=True, string='Pos Menu')
    
    
class PosMenu(models.Model):
    _inherit = 'pos.menu'

    menu_location_ids = fields.One2many('pos.menu.location', 'menu_id', string='Pos Menu Location')

