# -*- coding: utf-8 -*-
from odoo import _, api, fields, models, tools
from odoo.exceptions import UserError, ValidationError
from odoo.tools import float_is_zero


class PosOrderLine(models.Model):
    _inherit = 'pos.order.line'

    picking_id = fields.Many2one('stock.picking', string='Picking')

class PosOrder(models.Model):
    _inherit = 'pos.order'

    use_multi_location = fields.Boolean(related='company_id.use_multi_location', store=True, string="Use Multi Location on POS")
    picking_ids = fields.One2many('stock.picking', 'pos_order_id', string='Outcome Receipts')
    picking_count = fields.Integer(compute="_count_picking", string='Outcome Receipts Count')

    @api.multi
    def _count_picking(self):
        for obj in self:
            obj.picking_count = len(obj.picking_ids) + (1 if obj.picking_id else 0)

    @api.multi
    def show_linked_pickings(self):
        action = self.env.ref('stock.action_picking_tree')
        result = action.read()[0]
        #override the context to get rid of the default filtering on picking type
        result.pop('id', None)
        result['context'] = {}

        pick_ids = sum([order.picking_ids.ids for order in self], [])
        pick_ids.extend(self.mapped('picking_id').ids or [])
        
        # choose the view_mode accordingly
        if len(pick_ids) > 1:
            result['domain'] = "[('id', 'in', [" + ','.join(map(str, pick_ids)) + "])]" 
        elif len(pick_ids) == 1:
            res = self.env.ref('l10n_mn_stock.view_picking_inherit_picking_mn', False)
            result['views'] = [(res and res.id or False, 'form')]
            result['res_id'] = pick_ids and pick_ids[0] or False
        
        return result
    
    @api.multi
    def get_picking_ids(self):
        return self.mapped('picking_id') | self.mapped('picking_ids')
    
    def make_picking_by_order_line(self, menu_lines, menu_loc):
        """ 
            @param: menu_lines /pos.order.line/
            * pos.order.line-н объектуудыг нэгтгэж stock.picking үүсгэнэ.
            * Үүссэн picking-г хүчээр баталж ПОС-ын захиалгатай холбоно.
        """  
        Picking = self.env['stock.picking'].sudo()
        Move = self.env['stock.move'].sudo()
        StockWarehouse = self.env['stock.warehouse'].sudo()
        PosMenuLocation = self.env['pos.menu.location'].sudo()
        address = self.partner_id.address_get(['delivery']) or {}

        if menu_lines and len(menu_lines) >= 1:
            
            # Менюнээс харгалзах picking_type-г авах
            picking_type = menu_loc.picking_type_id
            return_pick_type = menu_loc.picking_type_id.return_picking_type_id or menu_loc.picking_type_id
            order_picking = Picking
            return_picking = Picking
            moves = Move
            
            # stock.move үүсгэхэд шаардлагатай байрлалуудыг мөн менюнд харгалзах stock.picking.type-с авах
            location_id = picking_type.default_location_src_id.id
            if self.partner_id:
                destination_id = self.partner_id.property_stock_customer.id
            else:
                if (not picking_type) or (not picking_type.default_location_dest_id):
                    customerloc, supplierloc = StockWarehouse._get_partner_locations()
                    destination_id = customerloc.id
                else:
                    destination_id = picking_type.default_location_dest_id.id
                    
            if picking_type:
                message = _("This transfer has been created from the point of sale session: <a href=# data-oe-model=pos.order data-oe-id=%d>%s</a>") % (self.id, self.name)
                picking_vals = {
                    'origin': self.name,
                    'pos_order_id': self.id,
                    'partner_id': address.get('delivery', False),
                    'date_done': self.date_order,
                    'picking_type_id': picking_type.id,
                    'company_id': self.company_id.id,
                    'move_type': 'direct',
                    'note': self.note or "",
                    'location_id': location_id,
                    'location_dest_id': destination_id,
                }
                pos_qty = any([x.qty > 0 for x in menu_lines if x.product_id.type in ['product', 'consu']])
                if pos_qty:
                    order_picking = Picking.create(picking_vals.copy())
                    order_picking.message_post(body=message)
                neg_qty = any([x.qty < 0 for x in menu_lines if x.product_id.type in ['product', 'consu']])
                if neg_qty:
                    return_vals = picking_vals.copy()
                    return_vals.update({
                        'location_id': destination_id,
                        'location_dest_id': return_pick_type != picking_type and return_pick_type.default_location_dest_id.id or location_id,
                        'picking_type_id': return_pick_type.id
                    })
                    return_picking = Picking.create(return_vals)
                    return_picking.message_post(body=message)

            for line in menu_lines.filtered(lambda l: l.product_id.type in ['product', 'consu'] and not float_is_zero(l.qty, precision_rounding=l.product_id.uom_id.rounding)):
                moves |= Move.create({
                    'name': line.name,
                    'product_uom': line.product_id.uom_id.id,
                    'picking_id': order_picking.id if line.qty >= 0 else return_picking.id,
                    'picking_type_id': picking_type.id if line.qty >= 0 else return_pick_type.id,
                    'product_id': line.product_id.id,
                    'product_uom_qty': abs(line.qty),
                    'state': 'draft',
                    'location_id': location_id if line.qty >= 0 else destination_id,
                    'location_dest_id': destination_id if line.qty >= 0 else return_pick_type != picking_type and return_pick_type.default_location_dest_id.id or location_id,
                    'order_line_id': line.id,
                })
                line.write({'picking_id': order_picking.id if line.qty >= 0 else return_picking.id})

            if return_picking:
                self._force_picking_done(return_picking)
            if order_picking:
                self._force_picking_done(order_picking)

            # when the pos.config has no picking_type_id set only the moves will be created
            if moves and not return_picking and not order_picking:
                tracked_moves = moves.filtered(lambda move: move.product_id.tracking != 'none')
                untracked_moves = moves - tracked_moves
                tracked_moves.action_confirm()
                untracked_moves.action_assign()
                moves.filtered(lambda m: m.state in ['confirmed', 'waiting']).force_assign()
                moves.filtered(lambda m: m.product_id.tracking == 'none').action_done()

    def create_picking(self):
        if self.env.user.company_id.use_multi_location:
            # Борлуулалтын цэг/Тохиргоо use_multi_location = True бол: Нэг захиалгын хувьд тухайн захиалга дахь нийт бараа нь хэдэн менюнд салж бүртгэлтэй байгаагаас хамаараад менюний тоогоор picking үүсгэдэг болгов.
            # * core-н create_picking() функцын кодыг дахин бүтэцлэж, picking үүсгэж байгаа хэсгийн кодыг def make_picking_by_order_line() функцэд салгаж бичив. 
            for order in self:
                if not order.lines.filtered(lambda l: l.product_id.type in ['product', 'consu']):
                    continue
                for menu_loc in order.session_id.config_id.menu_location_ids:
                    # Тухайн менюнд харгалзах захиалгын бараануудыг шүүж уг бараануудар нэгж picking үүсгэх
                    menu_lines = order.lines.filtered(lambda x: x.product_id in menu_loc.menu_id.product_ids and not x.picking_id)
                    order.make_picking_by_order_line(menu_lines, menu_loc)
        else:
            return super(PosOrder, self).create_picking()
        
    def get_income_account(self, line):
        # Search for the income account
        # ПОС-н хаалт хийхдээ орлогын дансыг менюн дэх бэлтгэх төрлийн агуулахаас авах
        if self.env.user.company_id.use_multi_location:
            income_account, income_wh = False, False
            
            for menu_loc in line.order_id.session_id.config_id.menu_location_ids:
                if line.product_id in menu_loc.menu_id.product_ids:
                    income_wh = menu_loc.picking_type_id.warehouse_id
                    income_account = income_wh.stock_account_income_id.id
                    break
            
            if not income_account:
                raise UserError(_('Please define income account for this warehouse: "%s"') % (income_wh.name))
            
            return income_account
        else:
            return super(PosOrder, self).get_income_account(line)
    