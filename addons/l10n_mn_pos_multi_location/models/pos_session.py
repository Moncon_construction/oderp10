# -*- coding: utf-8 -*-
from odoo import _, api, fields, models, tools
from odoo.exceptions import UserError, ValidationError
from odoo.tools import float_is_zero


class PosSession(models.Model):
    _inherit = 'pos.session'

    @api.multi
    def action_all_stock_picking(self):
        # @Override: Сэшнд холбоотой ажилбарууд руу үсрэх СМАРТ товчны функц
        # Борлуулалтын цэг/Тохиргоо use_multi_location = True бол picking_ids талбараас мөн тооцоолдог болгов.
        res = super(PosSession, self).action_all_stock_picking()

        if res.get('domain'):
            StockPicking = self.mapped('order_ids').mapped('picking_ids')
            if self.env.user.company_id.use_multi_location and StockPicking:
                domain = ['|', ('id', 'in', StockPicking.ids)]
                domain.extend(res['domain'])
                res['domain'] = domain

        return res

    def check_product_location(self):
        # Менюнд бүртгэгдээгүй бараа байвал raise өгөх
        not_registered_products = []
        for session in self:
            # Сэшнд харгалзах бүх захиалгын мөрийг олох
            orders = self.env['pos.order'].sudo().search([('session_id', '=', session.id)])
            lines = self.env['pos.order.line'].sudo()
            for order in orders:
                lines |= order.lines

            # Менюнд бүртгэгдээгүй бараа бүхий захиалгын мөрийг олох
            not_registered_lines = list(lines)
            for menu_loc in session.config_id.menu_location_ids:
                if lines:
                    menu_lines = lines.filtered(lambda x: x.product_id in menu_loc.menu_id.product_ids)
                    not_registered_lines = list(set(not_registered_lines) - set(menu_lines))

            # Менюнд бүртгэгдээгүй бараануудыг захиалгын мөрөөс түүж авах
            for not_registered_line in not_registered_lines:
                if not_registered_line.product_id not in not_registered_products:
                    not_registered_products.append(not_registered_line.product_id)

        if len(not_registered_products) > 0:
            error_msg = _("Belown products not registered at menu:\n")
            count = 1
            for not_registered_product in not_registered_products:
                error_msg += "\n%s.\t\t[%s] %s" %(count, (not_registered_product.default_code if not_registered_product.default_code else ""), not_registered_product.name)
                if not_registered_product != not_registered_products[-1]:
                    error_msg += ","
                count += 1

            raise ValidationError(error_msg)

        # Ямар ч raise өгөх шаардлагагүй бол True буцаах
        return True

    @api.multi
    def action_pos_session_closing_control_window_check(self):
        # @Override: Борлуулалтын цэг/Тохиргоо use_multi_location = True бол: Тухайн ПОС-ын тохиргоонд бүртгэсэн менюнд байхгүй бараа байвал raise өгч сэшн хаах боломжгүй болгов.
        if self.env.user.company_id.use_multi_location:
            has_not_error = self.check_product_location()
            if has_not_error:
                # Сэшн хаахад менюнд бүртгэсэн ч picking үүсээгүй бараануудыг олж picking үүсгэх
                not_picked_lines = self.env['pos.order.line'].sudo().search([('picking_id', '=', False)])

                if not_picked_lines:
                    for order in not_picked_lines.mapped('order_id'):
                        if not not_picked_lines.filtered(lambda l: l.order_id == order and l.product_id.type in ['product', 'consu']):
                            continue
                        for menu_loc in order.session_id.config_id.menu_location_ids:
                            menu_lines = not_picked_lines.filtered(lambda x: x.order_id == order and x.product_id in menu_loc.menu_id.product_ids and not x.picking_id)
                            order.make_picking_by_order_line(menu_lines, menu_loc)

        return super(PosSession, self).action_pos_session_closing_control_window_check()
