# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class ResCompany(models.Model):
    _inherit = 'res.company'

    use_multi_location = fields.Boolean(default=False, string="Use Multi Location on POS")

