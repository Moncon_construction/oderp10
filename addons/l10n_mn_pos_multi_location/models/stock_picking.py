# -*- coding: utf-8 -*-
from odoo import _, api, fields, models, tools


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    pos_order_id = fields.Many2one('pos.order', string='Pos Order')

