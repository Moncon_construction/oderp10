# -*- coding: utf-8 -*-

{
    'name': 'Mongolian Point of Sale at Restaurant',
    'version': '1.0',
    'author': "Asterisk Technologies LLC",
    'category': 'Point of Sale',
    'sequence': 7,
    'summary': 'Restaurant extensions for the Mongolian Point of Sale ',
    'depends': ['l10n_mn_pos_multi_location', 'l10n_mn_mrp', 'l10n_mn_stock_account'],
    'website': 'http://asterisk-tech.mn',
    'description': """

=======================

Уг модуль нь дараах боломжуудыг өгнө:
    1. ПОС-ын захиалга импортлох товчийг харагддаг болгосон.
        1.1 Захиалгын огноог импортлох боломжтой болгов.
    2. Борлуулалтын цэгийн нэг сэшний хувьд үйлдвэрлэлийг дараахаар хөтлөх боломж:
        2.1 ПОС-оор бараа зарагдаад ERP-д захиалга үүсэх үед буюу realtime үйлдвэрлэл үүсгэх
            * Тухайн нэгж захиалгын хувьд бараагаар багцалж үйлдвэрлэл үүсгэх
        2.2 ПОС-ын сэшн хаах үед үйлдвэрлэл үүсгэх:
            * Тухайн сэшнд харгалзах нийт захиалга дахь бараагаар багцалж үйлдвэрлэл үүсгэх
            * 2.1-тэй ижлээр захиалга бүрийн хувьд бараагаар багцалж үйлдвэрлэл үүсгэх
""",
    'data': [
        'security/ir.model.access.csv',
        'data/data.xml',

        'views/pos_config_views.xml',
        'views/pos_session_views.xml',
        'views/pos_order_views.xml',
        'views/pos_production_views.xml',
        'views/pos_production_line_views.xml',
        'views/pos_batch_defective_views.xml',
        'views/pos_batch_defective_line_views.xml',
        'views/pos_batch_production_views.xml',
        'views/menus.xml',

        'wizard/pop_notif_wizard.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
