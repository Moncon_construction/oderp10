# -*- coding: utf-8 -*-
from datetime import datetime
from dateutil.relativedelta import relativedelta

from odoo import _, api, fields, models, tools
from odoo.addons.l10n_mn_base.models.tools import *
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT


class MrpProduction(models.Model):
    _inherit = 'mrp.production'
    
    @api.multi
    def post_inventory(self):
        if self.env.user.company_id.availability_compute_method == 'stock_quant':
            return super(MrpProduction, self).post_inventory()
        elif self.env.user.company_id.availability_compute_method == 'stock_move':
            """  
                @attention: Дараах өөрчлөлтүүд орсон. /post_inventory функцэд core-роо журнал бичилт үүсгээд, хөдөлгөөнүүдээ 'done' болгодог байсныг ажиллагааг нь query болгож дараахаар өөрчлөв./
                    1. Энэ кодонд журнал бичилт үүсгэх хэсгийг query болгосон тул ирээдүйд арчилгаа их хэрэгтэй болох байх.
                    2. Барааны өртгийг stock.move-н update_product_price() функцаар авдаг болгосон:
                        2.1 product_price_update_before_done/product_price_update_after_done функцуудыг барааны компани дахь өртгийг шинэчлэх зорилгоор l10n_mn_stock_account_cost_for_each_wh модуль суугаагүй үед дуудаж байгаа.
                        2.2 l10n_mn_stock_account_cost_for_each_wh суусан үед агуулах бүрээрх өртгийг бөөнөөр нь query-р засаж байгаа.
            """

            move_ids = self.env['stock.move']
            move_raw_ids = self.mapped('move_raw_ids')
            move_finished_ids = self.mapped('move_finished_ids')
            
            if move_raw_ids:
                move_ids |= move_raw_ids
            if move_finished_ids:
                move_ids |= move_finished_ids
                
            if move_ids:
                move_ids.update_product_price()
                move_ids.write({'state': 'done'})
                # Багцаар нь үйлдвэрлэж байгаа үед журнал бичилт болон агуулахын өртгийг бөөнөөр нь ../pos_batch_production/def produce() функцад шинэчлэх тул энд хийхгүй.
                if not self._context.get('bulk_move', False):
                    # Журналын бичилт үүсгэх
                    move_ids.create_account_move_by_qry()
                    # Агуулах бүрээрх өртөг шинэчлэх
                    if self.env.get('product.warehouse.standard.price', False) != False:
                        self.update_warehouse_standard_price(move_finished_ids)
                if self.env.get('product.warehouse.standard.price', False) == False:
                    move_ids.product_price_update_before_done()
                    move_ids.product_price_update_after_done()

        return True

    @api.model
    def update_warehouse_standard_price(self, move_ids):
        # Үйлдвэрлэлийн эцсийн бүтээгдэхүүний хөдөлгөөнөөс агуулах бүрээрх өртөг бөөнөөр нь шинэчлэх/үүсгэх функц
        if self.env.get('product.warehouse.standard.price', False) != False:
            new_values, update_values = [], []
            for move_id in move_ids:
                if move_id.location_dest_id.usage == 'internal':
                    origin_name = move_id.name
                    warehouse_id = move_id.location_dest_id.get_warehouse().id
                    price_id = self.env['product.warehouse.standard.price'].search([('product_id', '=', move_id.product_id.id), ('warehouse_id', '=', warehouse_id)], order="create_date desc", limit=1)
                    if price_id:
                        # Тухайн хөдөлгөөнөөс өмнөх тухайн агуулах дахь нийт үлдэгдлийг олж өмнөх агуулахын өртөгтэй дундажлах
                        product_qty = move_id.product_id.get_qty_availability([move_id.location_dest_id.id], datetime.strptime(move_id.date, DEFAULT_SERVER_DATETIME_FORMAT) - relativedelta(microseconds=1)) or 0
                        price = (product_qty * price_id.standard_price + move_id.product_uom_qty * move_id.price_unit) / (product_qty + move_id.product_uom_qty) if product_qty + move_id.product_uom_qty > 0 else 0
                        update_values.append((price_id.id, price or 0, origin_name))
                    else:
                        already_added = False
                        for new_value in new_values:
                            if new_value[0] == move_id.product_id.id and new_value[1] == warehouse_id:
                                product_qty = move_id.product_id.get_qty_availability([move_id.location_dest_id.id], datetime.strptime(move_id.date, DEFAULT_SERVER_DATETIME_FORMAT) - relativedelta(microseconds=1)) or 0
                                price = (product_qty * new_value[2] + move_id.product_uom_qty * move_id.price_unit) / (product_qty + move_id.product_uom_qty) if product_qty + move_id.product_uom_qty > 0 else 0
                                new_value[2] = price
                                
                        if not already_added:
                            new_values.append([move_id.product_id.id, warehouse_id, move_id.price_unit, get_qry_value(origin_name)])
                        
            if new_values:
                qry = """
                    INSERT INTO product_warehouse_standard_price (product_id, warehouse_id, standard_price, origin) 
                    VALUES 
                """
                qry += ", ".join("(%s, %s, %s, '%s')" % (value[0], value[1], value[2], value[3]) for value in new_values)
                self._cr.execute(qry)
                
            if update_values:
                update_tuple = ", ".join("(%s, %s, '%s')" % (update_value[0], update_value[1], get_qry_value(update_value[2])) for update_value in update_values)
                qry = """
                    UPDATE product_warehouse_standard_price sprice SET standard_price = u.price, origin = u.origin
                    FROM (VALUES %s) AS u(id, price, origin)
                    WHERE u.id = sprice.id
                """ % update_tuple
                self._cr.execute(qry)
