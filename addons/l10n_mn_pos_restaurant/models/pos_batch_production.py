# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
import time

from odoo import _, api, fields, models, tools
import odoo.addons.decimal_precision as dp
from odoo.addons.l10n_mn_base.models.tools import *
from odoo.exceptions import UserError, ValidationError


class POSBatchDefectiveProductionLine(models.Model):
    _name = 'pos.batch.defective.line'
    _description = 'Detailed Information About Defectiveness by each BOM'
     
    defective_id = fields.Many2one('pos.batch.defective', ondelete='cascade', string='Batch Defective')
    company_id = fields.Many2one('res.company', related='defective_id.company_id', store=True, string='Company')
    bom_id = fields.Many2one('mrp.bom', string='Bill of Material')
    pos_order_id = fields.Many2one('pos.order', string='POS Order')
    product_id = fields.Many2one('product.product', string='Goods')
    product_qty = fields.Float(compute='compute_need', store=True, digits=dp.get_precision('Product Unit of Measure'), string='Needed Quantity')
    
    @api.depends('pos_order_id.lines.qty')
    def compute_need(self):
        for obj in self:
            obj.product_qty = sum(obj.pos_order_id.lines.filtered(lambda x: x.product_id == obj.product_id).mapped('qty'))
     
             
class POSBatchDefectiveProduction(models.Model):
    _name = 'pos.batch.defective'
    _description = 'Information About Defectiveness by each Product'
     
    batch_id = fields.Many2one('pos.batch.production', ondelete='cascade', string='Batch Production')
    company_id = fields.Many2one('res.company', related='batch_id.company_id', store=True, string='Company')
    bom_id = fields.Many2one('mrp.bom', string='Bill of Material')
    product_id = fields.Many2one('product.product', string='Goods')
    line_ids = fields.One2many('pos.batch.defective.line', 'defective_id', string='Defective Lines')
    product_qty = fields.Float(compute='compute_need', store=True, digits=dp.get_precision('Product Unit of Measure'), string='Needed Quantity')
    available_qty = fields.Float(digits=dp.get_precision('Product Unit of Measure'), string='Available Quantity')
    defective_qty = fields.Float(compute='compute_defectiviness', store=True, digits=dp.get_precision('Product Unit of Measure'), string='Defective Quantity')
     
    @api.depends('line_ids.product_qty')
    def compute_need(self):
        for obj in self:
            product_qty = sum(obj.mapped('line_ids.product_qty'))
            if obj.available_qty > product_qty:
                obj.available_qty = product_qty
            obj.product_qty = product_qty
         
    @api.depends('product_qty', 'available_qty')
    def compute_defectiviness(self):
        for obj in self:
            diff = obj.product_qty - obj.available_qty
            obj.defective_qty = diff if diff > 0 else 0
            

class POSProductionLine(models.Model):
    _name = 'pos.production.line'
    _description = 'Detailed Information About Production by each POS Order'
     
    pos_poduction_id = fields.Many2one('pos.production', ondelete='cascade', string='POS Production')
    company_id = fields.Many2one('res.company', related='pos_poduction_id.company_id', store=True, string='Company')
    pos_order_id = fields.Many2one('pos.order', string='POS Order')
    product_id = fields.Many2one('product.product', string='Goods')
    product_qty = fields.Float(compute='compute_need', store=True, digits=dp.get_precision('Product Unit of Measure'), string='Needed Quantity')
    
    @api.depends('pos_order_id.lines.qty')
    def compute_need(self):
        for obj in self:
            obj.product_qty = sum(obj.pos_order_id.lines.filtered(lambda x: x.product_id == obj.product_id).mapped('qty'))
            

class POSProduction(models.Model):
    _name = 'pos.production'
    _description = 'Information About Production by each Product'
    _order = 'production_id desc, mrp_sequence asc'
     
    batch_id = fields.Many2one('pos.batch.production', ondelete='cascade', string='Batch Production')
    company_id = fields.Many2one('res.company', related='batch_id.company_id', store=True, string='Company')
    bom_id = fields.Many2one('mrp.bom', string='Bill of Material')
    product_id = fields.Many2one('product.product', string='Goods')
    product_tmpl_id = fields.Many2one('product.template', related='product_id.product_tmpl_id', store=True, string='Product Template')
    product_qty = fields.Float(compute='compute_need', store=True, digits=dp.get_precision('Product Unit of Measure'), string='Needed Quantity')
    pos_order_id = fields.Many2one('pos.order', string='POS Order')
    line_ids = fields.One2many('pos.production.line', 'pos_poduction_id', string='POS Production Lines')
    picking_type_id = fields.Many2one('stock.picking.type', required=True, string='Picking Type')
    location_src_id = fields.Many2one('stock.location', 'Raw Materials Location', help="Location where the system will look for components.")
    location_dest_id = fields.Many2one('stock.location', 'Finished Products Location', help="Location where the system will stock the finished products.")
    production_id = fields.Many2one('mrp.production', string='Production')
    production_state = fields.Selection([
        ('confirmed', 'Confirmed'),
        ('planned', 'Planned'),
        ('progress', 'In Progress'),
        ('done', 'Done'),
        ('cancel', 'Cancelled')], related='production_id.state', store=True, readonly=True, string='Production State', copy=False, default='confirmed', track_visibility='onchange')
    mrp_sequence = fields.Selection([(1, 'Final Product'),
                                     (2, 'Semi Manufacture')], string='Manufacturing Sequence')
    
    @api.depends('line_ids.product_qty')
    def compute_need(self):
        for obj in self:
            obj.product_qty = sum(obj.mapped('line_ids.product_qty'))
    
    def set_mrp_sequence(self):
        # Эцсийн бүтээгдэхүүн-1 / Хагас боловсруулсан-2 гэсэн дараалал өгнө.
        products = self.mapped('product_id').mapped('id')
        company_id = self[0].company_id if self and len(self) > 0 else self.env.user.company_id
        if products:
            raw_product_ids = self.env['product.product'].search([('id', 'in', self.env['mrp.bom'].filter_raw_materials(products, company_id) or [])])
            bom_product_ids = self.env['product.product'].search([('id', 'in', self.env['mrp.bom'].filter_bom_products(products, company_id) or [])])
            
            final_products = set(bom_product_ids) - set(raw_product_ids)
            semi_manufactures = set(raw_product_ids) & set(bom_product_ids)
                    
            if final_products:
                final_products = list(final_products)
                self.filtered(lambda x: x.product_id in final_products).write({'mrp_sequence': 1})
            if semi_manufactures:
                semi_manufactures = list(semi_manufactures)
                self.filtered(lambda x: x.product_id in semi_manufactures).write({'mrp_sequence': 2})
            
    def _create_production(self):
        new_production = None
        
        for obj in self:
            product_qty = obj.product_qty
            company_id = obj.company_id
            product_id = obj.product_id
            origin = obj.batch_id.session_id.name
            picking_type_id = obj.picking_type_id
            location_src_id = obj.location_dest_id
            location_dest_id = obj.location_src_id
            produce_date = obj.batch_id.date
            bom_id = obj.bom_id
                
            mrp_move_ids, auto_poduction_id = self.env['mrp.production'].create_mrp_by_query(product_qty, company_id, bom_id, product_id, origin, picking_type_id, location_src_id, location_dest_id, produce_date)
            
            confirm_moves = self.env['stock.move'].browse(mrp_move_ids)
            confirm_moves.action_confirm()
            new_production = self.env['mrp.production'].browse(auto_poduction_id)
            
        return new_production
    
    @api.multi
    def produce(self):
        # Үйлдвэрлэл үүсгэх
        for obj in self.filtered(lambda x: not x.production_id):
            production_id = obj.production_id
            if not production_id and obj.product_id and obj.bom_id:
                production_id = obj._create_production()
                production_id.action_assign()
                production_id.button_confirm()
                
                pos_orders = obj.line_ids.mapped('pos_order_id')
                pos_orders.mapped('lines').filtered(lambda x: x.product_id == obj.product_id).write({'production_id': production_id.id})
            else:
                production_id.action_assign()
                
            if production_id.availability == 'assigned':
                context = {'active_model': 'mrp.production', 'active_ids': [production_id.id], 'active_id': production_id.id}
                product_consume = self.env['mrp.product.produce'].with_context(context).create({'product_qty': obj.product_qty})
                product_consume.do_produce()
                production_id.button_mark_done()
                
            if not obj.production_id:
                obj.production_id = production_id
            obj.batch_id.check_state()
            
    def done_produce(self):
        # Үйлдвэрлэлийг дуусгах
        for line in self.filtered(lambda x: x.production_id and x.production_id.state != 'done'):
            production_id = line.production_id
            production_id.action_assign()
            if production_id.availability == 'assigned':
                context = {'active_model': 'mrp.production', 'active_ids': [production_id.id], 'active_id': production_id.id}
                product_consume = self.env['mrp.product.produce'].with_context(context).create({'product_qty': line.product_qty})
                product_consume.do_produce()
                production_id.button_mark_done()
                    
    
class POSBatchProduction(models.Model):
    _name = 'pos.batch.production'
    _description = 'Bach Production for POS'
    _inherit = 'mail.thread'

    company_id = fields.Many2one('res.company', default=lambda x: x.env.user.company_id, string='Company')
    name = fields.Char(string='Reference', required=True, readonly=True, copy=False, default='New')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('progress', 'In Progress'),
        ('done', 'Done')], string='State', copy=False, default='draft', track_visibility='onchange')
    session_id = fields.Many2one('pos.session', string='Pos Session')
    user_id = fields.Many2one('res.users', string='Respondent')
    date = fields.Datetime(string='Manufacturing Date', default=fields.Datetime.now(), track_visibility='onchange')
    defective_product_ids = fields.One2many('pos.batch.defective', 'batch_id', string='Defective Goods')
    finished_product_ids = fields.One2many('pos.production', 'batch_id', string='Finished Product Production', track_visibility='onchange')
    finished_product_id = fields.Many2one('product.product', related='finished_product_ids.product_id', search='search_finished_product_id', string='Finished Product')
    mo_count = fields.Integer('# Manufacturing Orders', compute='_compute_mo_count')
    produce_by = fields.Integer(default=10, required=True, string='Produce Products By')
    
    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('pos.batch.production') or '/'
        return super(POSBatchProduction, self).create(vals)
    
    @api.multi
    def write(self, vals):
        if vals.get('produce_by', False):
            if not (vals['produce_by'] > 0 and vals['produce_by'] <= 200):
                raise ValidationError(_('Produce products by 1 - 200 !!!'))
            
        res = super(POSBatchProduction, self).write(vals)
        
        if vals.get('date', False):
            self.check_available_bom()
            
        return res
    
    @api.multi
    def search_finished_product_id(self, operator, value):
        res = []
        if value:
            res = self.search(['|', '|', ('finished_product_ids.product_id.product_tmpl_id.name', 'ilike', value), ('finished_product_ids.product_id.default_code', 'ilike', value), ('finished_product_ids.product_id.product_tmpl_id.barcode', 'ilike', value)])
        return [('id', search_operator, res)]
    
    @api.multi
    @api.onchange('date')
    def check_available_bom(self):
        for obj in self:
            date = obj.date
            for line in obj.finished_product_ids:
                bom_id = self.env['mrp.bom']._bom_find(product=line.product_id, date=date)
                if line.bom_id != bom_id:
                    line.write({'bom_id': bom_id.id or False})
            
    def get_all_production_ids(self):
        production_ids = self.mapped('finished_product_ids').mapped('production_id')
        if production_ids:
            sub_production_ids = self.env['mrp.production'].search([('origin', 'in', tuple(obj.name for obj in production_ids))])
            production_ids |= sub_production_ids
            while sub_production_ids:
                sub_production_ids = self.env['mrp.production'].search([('origin', 'in', tuple(obj.name for obj in sub_production_ids))])
                production_ids |= sub_production_ids
        return production_ids
    
    @api.multi
    @api.depends('finished_product_ids.production_id')
    def _compute_mo_count(self):
        # Холбоотой үйлдвэрлэлийн тоог олох
        for obj in self:
            obj.mo_count = len(obj.get_all_production_ids())
        
    @api.multi
    def show_linked_production(self):
        # Холбоотой үйлдвэрлэл рүү үсрэх смарт товчноос дуудагдана
        production_ids = self.get_all_production_ids()
        return {
            'name': _('Manufacturing Order'),
            'res_model': 'mrp.production',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'views': [
                (self.env.ref('mrp.mrp_production_tree_view').id, 'tree'), 
                (False, 'form'),
            ],
            'target': 'current',
            'flags': {'search_view': True, 'action_buttons': True},
            'domain': [('id', 'in', production_ids.ids if production_ids else [])]
        }
        
    def check_state(self):
        for obj in self:
            if obj.finished_product_ids.filtered(lambda x: x.production_id):
                if obj.finished_product_ids.filtered(lambda x: not x.production_id):
                    obj.write({'state': 'progress'})
                else:
                    obj.write({'state': 'done'})
            else:
                obj.write({'state': 'draft'})
        
    def check_availability(self):
        for obj in self:
            if obj.defective_product_ids:
                self._cr.execute("DELETE FROM pos_batch_defective WHERE id IN (%s)" % str(obj.defective_product_ids.ids).strip('[]'))
            obj.create_defective_lines()
            
    def set_mrp_sequence(self):
        # Эцсийн бүтээгдэхүүн-1 / Хагас боловсруулсан-2 гэсэн дараалал өгнө.
        self.mapped('finished_product_ids').set_mrp_sequence()
    
    @api.multi
    def check_producability(self):
        # Багц үйлдвэрлэл үүссэний дараа тухайн багцын сэшн дэх захиалгын мөрүүдийг ахин шалгаж үйлдвэрлэх боломжтой ч багцад бүртгэгдээгүй мөр байвал багцад нэмэх үйлдлийг хийнэ.
        bomed_products = self.env['product.product'].get_bommed_products()
        new_pos_production_ids = self.env['pos.production']
        newly_added = []
        
        for obj in self:
            finished_product_ids = obj.finished_product_ids.filtered(lambda x: not x.production_id)
            order_ids = self.env['pos.order'].sudo().search([('session_id', '=', obj.session_id.id)]).mapped('lines').filtered(lambda x: not x.production_id and x.product_id in bomed_products).mapped('order_id')
            
            for order_id in order_ids:
                producable_po_line_ids = order_id.lines.filtered(lambda x: not x.production_id and x.product_id in bomed_products)
                if producable_po_line_ids:
                    if finished_product_ids:
                        # Тухайн захиалгын мөрийн бараа нь аль хэдийн багцад үйлдвэрлэлгүйгээр бүртгэгдсэн бол тухайн барааны үйлдвэрлэх тоо хэмжээг нэмэхэд болно.
                        addable_po_line_ids = producable_po_line_ids.filtered(lambda x: x.product_id in finished_product_ids.mapped('product_id'))
                        if addable_po_line_ids:
                            producable_po_line_ids -= addable_po_line_ids
                            for order_line_id in addable_po_line_ids:
                                pos_production_ids = finished_product_ids.filtered(lambda x: x.product_id == order_line_id.product_id)
                                pos_production_id = pos_production_ids[0]
                                if order_id not in pos_production_ids.mapped('line_ids').mapped('pos_order_id'):
                                    new_production_line = self.env['pos.production.line'].create({
                                        'pos_poduction_id': pos_production_id.id,
                                        'pos_order_id': order_id.id,
                                        'product_id': order_line_id.product_id.id
                                    })
                                    new_pos_production_ids |= pos_production_id
                                    new_production_line.compute_need()
                                    if order_line_id.product_id not in newly_added:
                                        newly_added.append(order_line_id.product_id)
                    
                    # Тухайн захиалгын мөрийн бараа багцад үйлдвэрлэлгүйгээр бүртгэгдээгүй бол тухайн барааг шинээр мөр /pos.production/ бүртгэн үйлдвэрлэхээр бүртгэх
                    if producable_po_line_ids:
                        pos_poduction_ids = obj.do_batch_production_lines(producable_po_line_ids)
                        if pos_poduction_ids:
                            pos_poductions = self.env['pos.production'].browse(pos_poduction_ids)
                            pos_poductions.compute_need()
                            finished_product_ids |= pos_poductions
                            newly_added.extend(pos_poductions.mapped('product_id').filtered(lambda x: x not in newly_added))
          
        notif = {'default_title': _('Check Finished')}
        if len(newly_added) > 0:
            notif['default_message'] = _('%s producable product found and those are successfully added to batch production.') % len(newly_added)
        else:
            notif['default_message'] = _('No manufacturing product found.')
        
        self.check_state()
        new_pos_production_ids.set_mrp_sequence()
            
        return {
            'name': notif['default_title'],
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'notif.popup',
            'target': 'new',
            'context': notif 
        }
        
    @api.multi
    def set_draft(self):
        if self.filtered(lambda x: x.mo_count > 0):
            raise ValidationError(_("It is not possible to set draft a produced batch. Please delete the related productions first !!!"))
        self.write({'state': 'draft'})
        
    @api.multi
    def produce(self):
        self.ensure_one()
        finished_product_ids = self.env['pos.production'].search([('batch_id', 'in', self.ids), ('production_id', '=', False)], limit=self.produce_by, order='mrp_sequence asc')
        finished_product_ids.with_context({'bulk_move': True}).produce()
        
        # Санхүүгийн бичилтийг бөөнөөр нь үүсгэх
        move_finished_ids = self.get_all_production_ids().mapped('move_finished_ids')
        move_raw_ids = self.get_all_production_ids().mapped('move_raw_ids')
        (move_finished_ids | move_raw_ids).create_account_move_by_qry()
        
        # Агуулах бүрээрх өртөг шинэчлэх
        if self.env.get('product.warehouse.standard.price', False) != False:
            self.env['mrp.production'].update_warehouse_standard_price(move_finished_ids)
            
        self.check_state()
        
    def create_defective_lines(self):
        # Үйлдвэрлэхэд шаардагдах түүхий эд материал, дэд орцуудад дутагдал байгаа эсэхийг шалгах
        products = []
        batch_def_ids = []
        batch_def_line_insert_qry = []
        product_dict_by_bom = {}
        
        # Үйлдвэрлэлийн орцод ямар бараа хэдий хэмжээтэй орсоныг тооцоолж орцоор нь бүлэглэн dictionary бэлдэв
        for obj in self.finished_product_ids:
            for bom_line in obj.bom_id.bom_line_ids:
                product_qty = obj.product_qty * bom_line.product_qty / bom_line.bom_id.product_qty
                if obj.bom_id.id not in product_dict_by_bom.keys():
                    product_dict_by_bom[obj.bom_id.id] = {bom_line.product_id.id: product_qty}
                else:
                    if bom_line.product_id.id not in product_dict_by_bom[obj.bom_id.id]:
                        product_dict_by_bom[obj.bom_id.id][bom_line.product_id.id] = product_qty
                    else:
                        product_dict_by_bom[obj.bom_id.id][bom_line.product_id.id] += product_qty
                if bom_line.product_id not in products:
                    products.append(bom_line.product_id)
                
        # Үйлдвэрлэлийн орцод орсон бараагаар бүлэглэж дутагдалтай бараануудыг үүсгэх    
        for product in products:
            product_qty, available_qty = 0, 0
            
            # Тухайн барааны үйлдвэрлэлд оролцох нийт тоо хэмжээг олох
            for finished_product_line in self.finished_product_ids.mapped('bom_id'):
                if finished_product_line.id in product_dict_by_bom.keys() and product.id in product_dict_by_bom[finished_product_line.id].keys():
                    product_qty += product_dict_by_bom[finished_product_line.id][product.id]
        
            # Барааны боломжит тоо хэмжээг олох
            product_checked_locs = []
            for finished_product in self.finished_product_ids:
                if product in finished_product.bom_id.mapped('bom_line_ids').mapped('product_id'):
                    location_id = finished_product.location_src_id
                    product_qty_by_bom = product_dict_by_bom[finished_product.bom_id.id][product.id]
                    if product_qty_by_bom and location_id and location_id not in product_checked_locs:
                        available_qty_by_bom = product.get_qty_availability([location_id.id], self.date)
                        available_qty_by_bom = available_qty_by_bom if available_qty_by_bom <= product_qty_by_bom else product_qty_by_bom
                        available_qty += available_qty_by_bom
                        product_checked_locs.append(location_id)
            
            # Дутагдалтай бараа үүсгэх
            available_qty = round(available_qty, self.env['decimal.precision'].precision_get('Product Unit of Measure') or 2)                
            product_qty = round(product_qty, self.env['decimal.precision'].precision_get('Product Unit of Measure') or 2)
            if available_qty < product_qty:
                uid = self.env.uid
                date = fields.Datetime.now()
                self._cr.execute("""
                    INSERT INTO pos_batch_defective (create_uid, create_date, write_uid, write_date, batch_id, company_id, product_id, available_qty) 
                    VALUES (%s, '%s', %s, '%s', %s, %s, %s, %s)
                    RETURNING id
                """ % (uid, date, uid, date, self.id, self.company_id.id, product.id, available_qty))
                
                batch_def_id = self.env.cr.fetchone()[0]
                batch_def_ids.append(batch_def_id) 
                for bom in self.finished_product_ids.mapped('bom_id'):
                    if bom.id in product_dict_by_bom.keys() and product.id in product_dict_by_bom[bom.id].keys():
                        product_qty = product_dict_by_bom[bom.id][product.id]
                        batch_def_line_insert_qry.append("(%s, '%s', %s, '%s', %s, %s, %s, %s, %s)" % (uid, date, uid, date, batch_def_id, self.company_id.id, bom.id, product.id, product_qty))
        
        # Дутагдалтай барааны дэлгэрэнгүй үүсгэх
        if batch_def_line_insert_qry:
            qry = """
                INSERT INTO pos_batch_defective_line (create_uid, create_date, write_uid, write_date, defective_id, company_id, bom_id, product_id, product_qty)
                VALUES 
            """
            for qry_line in batch_def_line_insert_qry:
                qry += qry_line
                if qry_line != batch_def_line_insert_qry[-1]:
                    qry += ", "
            self._cr.execute(qry)
            
        self.env['pos.batch.defective'].browse(batch_def_ids).compute_need()
    
    def do_batch_production_lines(self, pos_order_lines):
        # Бараагаар нь бүлэглэж багц үйлдвэрлэлийн өгөгдлийг бэлдэхы
        pos_poduction_ids = []
        bomed_product_templates = self.env['mrp.bom'].search([]).mapped('product_tmpl_id').sudo()
        pos_config = pos_order_lines[0].order_id.session_id.config_id.sudo() if pos_order_lines else False
        
        for product in pos_order_lines.mapped('product_id'):
            bom_id = self.env['mrp.bom']._bom_find(product=product, date=self.date)
            if not bom_id:
                raise UserError(_('There is no BOM in this product: %s %s') % (product.default_code, product.name))
                    
            uid = self.env.uid
            date = fields.Datetime.now()
            order_lines_by_product = pos_order_lines.filtered(lambda x: x.product_id == product)
            picking_type, raw_mat_location, finished_prod_location = pos_config.get_production_locations(product)
            self._cr.execute("""
                INSERT INTO pos_production (create_uid, create_date, write_uid, write_date, batch_id, company_id, product_id, picking_type_id, location_src_id, location_dest_id, bom_id) 
                VALUES (%s, '%s', %s, '%s', %s, %s, %s, %s, %s, %s, %s)
                RETURNING id
            """ % (uid, date, uid, date, self.id, self.company_id.id, product.id,
                   picking_type.id or 'NULL', raw_mat_location.id or 'NULL', finished_prod_location.id or 'NULL',
                   bom_id.id))
    
            pos_poduction_id = self.env.cr.fetchone()[0]
            pos_poduction_ids.append(pos_poduction_id)
            qry = """
                INSERT INTO pos_production_line (create_uid, create_date, write_uid, write_date, pos_poduction_id, company_id, pos_order_id, product_id, product_qty)
                VALUES 
            """
            orders = order_lines_by_product.mapped('order_id')
            for order in orders:
                product_qty = sum(order_line.qty for order_line in order.mapped('lines').filtered(lambda x: x.product_id == product))
                qry += "(%s, '%s', %s, '%s', %s, %s, %s, %s, %s)" % (uid, date, uid, date, pos_poduction_id, self.company_id.id, order.id, product.id, product_qty)
                if order != orders[-1]:
                    qry += ", "
            self._cr.execute(qry)
        
        return pos_poduction_ids
                
    def do_batch_production(self):
        # Багц үйлдвэрлэл үүсгэх
        bomed_product_templates = self.env['mrp.bom'].search([]).mapped('product_tmpl_id').sudo()
        pos_poduction_ids = []
        for obj in self:
            if obj.company_id.manufacturing_batch == 'by_product':
                pos_order_lines = self.env['pos.order.line']
                for order in obj.session_id.order_ids:
                    pos_order_lines |= order.lines.filtered(lambda x: x.product_id.product_tmpl_id in bomed_product_templates)
                pos_poduction_ids.extend(obj.do_batch_production_lines(pos_order_lines))
            elif self.company_id.manufacturing_batch == 'by_order':
                for order in obj.session_id.order_ids:
                    pos_order_lines = order.lines.filtered(lambda x: x.product_id.product_tmpl_id in bomed_product_templates)
                    pos_poduction_ids.extend(obj.do_batch_production_lines(pos_order_lines))
        
        pos_productions = self.env['pos.production'].browse(pos_poduction_ids)
        pos_productions.compute_need()
        pos_productions.set_mrp_sequence()
        pos_productions.mapped('batch_id').create_defective_lines()

  