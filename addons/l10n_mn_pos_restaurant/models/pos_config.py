# -*- coding: utf-8 -*-
from lxml import etree
from odoo import _, api, fields, models, tools
from odoo.addons.l10n_mn_pos_restaurant.models.res_company import manufacturing_period, manufacturing_batch
from odoo.exceptions import UserError, ValidationError


class PosConfigSettings(models.TransientModel):
    _inherit = 'pos.config.settings'

    manufacturing_period = fields.Selection(manufacturing_period, related='company_id.manufacturing_period', default=lambda x: x.company_id.manufacturing_period or 'session_close', string='Manufacturing Period')
    manufacturing_batch = fields.Selection(manufacturing_batch, related='company_id.manufacturing_batch', default=lambda x: x.company_id.manufacturing_batch or 'by_product', string='Manufacturing Batch Type')
    
    
class POSConfig(models.Model):
    _inherit = 'pos.config'
    
    def get_production_locations(self, product):
        # Бараа үйлдвэрлэгдэх бэлтгэх төрөл/байрлалуудыг олж буцаах
        picking_type, src_loc, dest_loc = False, False, False
        
        if self.env.user.company_id.use_multi_location:
            # Харгалзах менюний бэлтгэх төрөлтэй адилхан агуулах бүхий үйлдвэрлэлийн бэлтгэх төрлийг олох
            for menu_loc in self.menu_location_ids:
                if product in menu_loc.menu_id.product_ids:
                    picking_type = self.env['stock.picking.type'].search([('code', '=', 'mrp_operation'), ('warehouse_id', '=', menu_loc.picking_type_id.warehouse_id.id)], limit=1)
                    if not picking_type:
                        raise ValidationError(_("Please configure stock picking type for belown warehouse: %s") % menu_loc.picking_type_id.warehouse_id.name)
                    break
            if not picking_type:
                raise ValidationError(_("Belown product not registered at menu: %s") % product.name)
        else:
            # Тохиргоон дах бэлтгэх төрөлтэй адилхан агуулах бүхий үйлдвэрлэлийн бэлтгэх төрлийг олох
            picking_type = self.env['stock.picking.type'].search([('code', '=', 'mrp_operation'), ('warehouse_id', '=', self.picking_type_id.warehouse_id.id)], limit=1)
        
        src_loc, dest_loc = picking_type.default_location_src_id, picking_type.default_location_dest_id
        if picking_type and not (src_loc and dest_loc):
            raise ValidationError(_("Please configure locations at belown picking type: %s") % picking_type.name)
            
        return picking_type, src_loc, dest_loc
    
