# -*- coding: utf-8 -*-
from lxml import etree
from odoo import _, api, fields, models, tools
from odoo.addons.l10n_mn_web.models.time_helper import *


class ProductProduct(models.Model):
    _inherit = 'product.product'
    
    @api.model
    def get_bommed_products(self):
        bomed_pro_templates = self.env['mrp.bom'].sudo().search([]).mapped('product_tmpl_id')
        bomed_products = self.env['product.product'].sudo().search([('product_tmpl_id', 'in', bomed_pro_templates.ids or [])])
        return bomed_products
    
    
class PosOrder(models.Model):
    _inherit = "pos.order"

    mrp_production_ids = fields.Many2many('mrp.production', 'pos_order_mrp_production_rel', 'order_id', 'production_id', string='Production')
    date_order = fields.Datetime(string='Order Date', readonly=False, index=True, default=fields.Datetime.now)
    
    @api.model
    def fields_view_get(self, view_id=None, view_type='tree', toolbar=False, submenu=False):

        res = super(PosOrder, self).fields_view_get(view_id, view_type, toolbar=toolbar, submenu=submenu)
        root = etree.fromstring(res['arch'])

        if self.env.user.has_group('point_of_sale.group_pos_manager'):
            root.set('import', 'true')
            root.set('create', 'true')
        
        res['arch'] = etree.tostring(root)
        return res
    
    @api.model
    def create_from_ui(self, orders):
        # @Override: Захиалга үүсэх үед үйлдвэрлэл хөтлөх
        order_ids = super(PosOrder, self).create_from_ui(orders)
        
        if self.env.user.company_id.manufacturing_period == 'realtime':
            bomed_products = self.env['product.product'].get_bommed_products()
            if bomed_products:
                orders = self.env['pos.order'].sudo().search([('id', 'in', order_ids)])
                for order in orders:
                    order.mapped('lines').filtered(lambda x: not x.production_id and x.product_id in bomed_products).create_mrp()
        
        return order_ids
    
    def _force_picking_done(self, picking):
        # @Override: Код дунд өөрчлөлт хийх болсон тул бүхэлд нь override хийв.
        self.ensure_one()
        ########### START CHANGE1 - Үйлдвэрлэл хийгдсэний дараа хүргэлтийг дуусгах тул дуусаагүй төлвүүдийг шалгах #############
        if picking.state == 'done':
            return
        ########### END CHANGE1 #############
        contains_tracked_products = any([(product_id.tracking != 'none') for product_id in self.lines.mapped('product_id')])
        
        # do not reserve for tracked products, the user will have manually specified the serial/lot numbers
        if contains_tracked_products:
            picking.action_confirm()
        else:
            picking.action_assign()
        
        if self.user_id.company_id.force_assign_when_insufficient_residual == 'check':
            picking.force_assign()
            
        ########### START CHANGE2 - Үйлдвэрлэл хийх ёстой бараа зарагдсан бол хүргэлтийг батлалгүй орхив /Учир нь өртгийг нь үйлдвэрлэл хийгдсэний дараа авна/  #############
        if not self._context.get('all_mrp_done'):
            bomed_products = self.env['product.product'].get_bommed_products()
            if bomed_products and self.mapped('lines').filtered(lambda x: x.product_id in bomed_products):
                return
        ########### END CHANGE2 #############
        
        if picking.state == 'assigned':
            self.set_pack_operation_lot(picking)
            if not contains_tracked_products:
                picking.action_done()

        ########### START CHANGE3 - Өнгөрсөн огноогоорх захиалга импортолж болдогтой холбогдуулан ажилбарын огноог өөрчлөв #############
        context = {"active_model": "stock.picking", "active_ids": [picking.id], "active_id": picking.id}
        change_date_wizard = self.env['stock.picking.change.date'].with_context(context).create({'date_time': self.date_order})
        change_date_wizard.action_picking_change_date({'active_ids': [picking.id]})
        ########### END CHANGE3 #############


class PosOrderLine(models.Model):
    _inherit = "pos.order.line"

    production_id = fields.Many2one('mrp.production', string='Production')
    
    @api.model
    def fields_view_get(self, view_id=None, view_type='tree', toolbar=False, submenu=False):

        res = super(PosOrderLine, self).fields_view_get(view_id, view_type, toolbar=toolbar, submenu=submenu)
        root = etree.fromstring(res['arch'])

        if self.env.user.has_group('point_of_sale.group_pos_manager'):
            root.set('create', 'true')
            root.set('import', 'true')

        res['arch'] = etree.tostring(root)
        return res
    
    def create_mrp(self):
        """ 
            @note: Захиалгын мөрүүдийг бараагаар нь бүлэглэж бүлэглэлт бүр дээр үйлдвэрлэл үүсгэнэ.
                   Өөрөөр хэлбэл self захиалгын мөрүүдэд нэг бараа олон захиалгын мөрөөр худалдагдсан бол тэдгээрийн тоо хэмжээг нэгтгэж HЭГ үйлдвэрлэл үүсгэнэ.
            @attention: Тиймээс self доторх захиалгын мөрүүд нь нэг захиалгад харъяалагдах ёстой !!!       
        """
        self = self.filtered(lambda x: not x.production_id)
        if self:
            pos_config = self[0].order_id.session_id.config_id.sudo()
            products = self.mapped('product_id')
            
            for product in products:
                lines_py_product = self.filtered(lambda x: x.product_id == product)
                qty = sum(obj.qty for obj in lines_py_product)
                line = lines_py_product[0]
                pos_config = line.order_id.session_id.config_id.sudo()
                MrpProduction = self.env['mrp.production'].sudo()
                bom = self.env['mrp.bom'].sudo().search([('product_tmpl_id', '=', product.product_tmpl_id.id)], order='sequence asc', limit=1)
                if bom:
                    picking_type, raw_mat_location, finished_prod_location = pos_config.get_production_locations(product)
                    mrp = MrpProduction.create({
                        'product_id': product.id,
                        'product_uom_id': product.uom_id.id,
                        'product_qty': qty,
                        'bom_id': bom.id,
                        'origin': line.order_id.session_id.name,
                        'picking_type_id': picking_type.id,
                        'location_src_id': raw_mat_location.id,
                        'location_dest_id': finished_prod_location.id,
                        'date_planned_start': (get_day_by_user_timezone(line.order_id.date_order, self.env.user) + relativedelta(seconds=-1)),
                        'company_id': line.company_id.sudo().id,
                        'user_id': line.order_id.user_id.sudo().id,
                    })
                    lines_py_product.write({'production_id': mrp.id})
                    mrp.action_assign()
                    mrp.button_confirm()
                    if mrp.availability == 'assigned':
                        context = {'active_model': 'mrp.production', 'active_ids': [mrp.id], 'active_id': mrp.id}
                        product_consume = self.env['mrp.product.produce'].with_context(context).create({'product_qty': qty})
                        product_consume.do_produce()
                        mrp.button_mark_done()
