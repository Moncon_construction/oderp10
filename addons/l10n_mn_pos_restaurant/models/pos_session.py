# -*- coding: utf-8 -*-
from odoo import _, api, fields, models, tools
from odoo.exceptions import UserError, ValidationError
from odoo.addons.l10n_mn_pos_restaurant.models.res_company import manufacturing_period
from odoo.tools import float_is_zero


class PosSession(models.Model):
    _inherit = 'pos.session'

    batch_production_id = fields.Many2one('pos.batch.production', string='Pos Batch Production')
    manufacturing_period = fields.Selection(manufacturing_period, related='config_id.company_id.manufacturing_period', string='Manufacturing Period')
    sold_producable_product = fields.Boolean(default=False, compute='sold_producable_product_or_not', string='Sold producable product or not ?')
    can_create_journal = fields.Boolean(default=False, compute='can_create_journal_or_not', string='Is all account journal created or not ?')

    def sold_producable_product_or_not(self):
        # Сэшнд үйлдвэрлэх бараа зарагдсан эсэх
        for obj in self:
            bomed_products = self.env['product.product'].get_bommed_products()
            line_ids = self.env['pos.order'].search([('session_id', '=', obj.id)]).mapped('lines').filtered(lambda x: x.product_id in bomed_products)
            obj.sold_producable_product = (line_ids and len(line_ids) > 0)

    def can_create_journal_or_not(self):
        # Сэшнд холбоотой бүх бэлтгэх төрлүүдэд журналын бичилт хийгдсэн эсэхийг шалгах
        for obj in self:
            if not obj.sold_producable_product:
                obj.can_create_journal = False
                continue
            order_ids = self.env['pos.order'].search([('session_id', '=', obj.id)])
            not_moved_picking_ids = order_ids.mapped('picking_id').filtered(lambda x: not x.account_move_id or x.state != 'done')
            if self.env.user.company_id.use_multi_location:
                not_moved_picking_ids |= order_ids.mapped('picking_ids').filtered(lambda x: not x.account_move_id or x.state != 'done')
            can_create_journal = not_moved_picking_ids and len(not_moved_picking_ids) > 0
            if can_create_journal and obj.batch_production_id and len(obj.batch_production_id.finished_product_ids.filtered(lambda x: x.production_state != 'done')) > 0:
                can_create_journal = False
            obj.can_create_journal = can_create_journal

    def check_product_bom(self):
        # Бараан дээр "Орцтой" гэж тэмдэглэгдсэн ч ямар нэг орц бүртгэгдээгүй бараа бүхий захиалгын мөрүүдийг олж raise өгөх
        not_bomed_lines = self.env['pos.order.line']
        for session in self:
            orders = self.env['pos.order'].sudo().search([('session_id', '=', session.id)])
            not_bomed_lines |= orders.mapped('lines').filtered(lambda x: not x.product_id.product_tmpl_id.bom_ids and x.product_id.product_tmpl_id.has_bom == 'has')

        if len(not_bomed_lines) > 0:
            not_bomed_products = not_bomed_lines.mapped('product_id')
            error_msg = _("Please register BOM for belown products:\n")
            count = 1
            for not_bomed_product in not_bomed_products:
                error_msg += "\n%s.\t\t[%s] %s" %(count, (not_bomed_product.default_code if not_bomed_product.default_code else ""), not_bomed_product.name)
                if not_bomed_product != not_bomed_products[-1]:
                    error_msg += ","
                count += 1

            raise ValidationError(error_msg)

    @api.multi
    def action_pos_session_closing_control_window_check(self):
        # @Override: Борлуулалтын цэг/Тохиргоо manufacturing_period = 'session_close' бол: Тухайн ПОС-ын захиалгад үйлдвэрлэл үүсээгүй бараа байвал raise өгөх.
        if self.env.user.company_id.use_multi_location:
            self.check_product_location()

        if self.env.user.company_id.manufacturing_period == 'session_close':
            self.check_product_bom()

            # Орц үүссэн бараануудыг олж авах
            bomed_products = self.env['product.product'].get_bommed_products()

            # Сэшнд харгалзах бүх үйлдвэрлэл үүсээгүй орц бүхий бараатай захиалгын мөрүүдийг олох
            orders = self.env['pos.order'].sudo().search([('session_id', 'in', self.ids)])
            lines = self.env['pos.order.line'].sudo()
            for order in orders:
                lines |= order.lines.filtered(lambda x: not x.production_id and x.product_id in bomed_products)

            # Барааны тоог олж raise өгөх
            products = lines.mapped('product_id')
            producable_product_count = len(list(products))
            if producable_product_count > 0:
                raise ValidationError(_('There are %s producable product purchased.\nPlease make manufacturing for those products !!!') % producable_product_count)

            if self.batch_production_id:
                not_produced_product_count = len(self.batch_production_id.finished_product_ids.filtered(lambda x: x.production_state != 'done'))
                if not_produced_product_count > 0:
                    raise ValidationError(_('%s product not produced. Please do producing !!!') % not_produced_product_count)
                if self.can_create_journal:
                    raise ValidationError(_('Please first create journal !!!'))

        return super(PosSession, self).action_pos_session_closing_control_window_check()

    def make_mrp(self):
        if self.env.user.company_id.use_multi_location:
            self.check_product_location()

        self.check_product_bom()

        # Багц үйлдвэрлэл рүү үсрэх
        if self.env.user.company_id.manufacturing_period == 'session_close':
            if not self.batch_production_id:
                self.batch_production_id = self.env['pos.batch.production'].create({
                    'session_id': self.id,
                    'user_id': self.user_id.sudo().id
                })
                self.batch_production_id.do_batch_production()
            return {
                'name': _('POS Batch Production'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'pos.batch.production',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'domain': [('id', '=', self.batch_production_id.id)],
            }

    def make_journal(self):
        # Бэлтгэх төрөлд харгалзах журнал үүсээгүй бол үйлдвэрлэл хийгдсэний дараа журнал үүсгэх гэж оролдох
        # Журналыг үүсгээд хүргэлтүүдийг батлах
        for obj in self:
            order_ids = self.env['pos.order'].search([('session_id', '=', obj.id)])
            move_ids = order_ids.mapped('picking_id').filtered(lambda x: not x.account_move_id or x.state != 'done').mapped('move_lines')
            if self.env.user.company_id.use_multi_location:
                move_ids |= order_ids.mapped('picking_ids').filtered(lambda x: not x.account_move_id or x.state != 'done').mapped('move_lines')
            production_ids = order_ids.mapped('lines').mapped('production_id')
            for move in move_ids.filtered(lambda x: x.product_id.product_tmpl_id in production_ids.mapped('product_tmpl_id') and not x.price_unit):
                move.write({'price_unit': move.get_product_standard_price()})
            for order in order_ids:
                for picking in (order.picking_id | order.picking_ids):
                    order.with_context({'all_mrp_done': True})._force_picking_done(picking)




