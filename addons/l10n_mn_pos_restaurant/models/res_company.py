# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

manufacturing_period = [('realtime', _('When order is registered')), ('session_close', _('When session is closed'))]
manufacturing_batch = [('by_product', _('By Each Product')), ('by_order', _('By Each Order'))]


class ResCompany(models.Model):
    _inherit = 'res.company'

    manufacturing_period = fields.Selection(manufacturing_period, default='session_close', string='Manufacturing Period')
    manufacturing_batch = fields.Selection(manufacturing_batch, default='by_product', string='Manufacturing Batch Type')
