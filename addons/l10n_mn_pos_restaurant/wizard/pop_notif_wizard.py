# -*- coding: utf-8 -*-
import time

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class PopUpNotification(models.TransientModel):
    
    _name = "notif.popup"
    _description = "POP Up Notification"
    
    message = fields.Text(string='Text')
    
