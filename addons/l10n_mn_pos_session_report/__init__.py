# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################

import report
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: