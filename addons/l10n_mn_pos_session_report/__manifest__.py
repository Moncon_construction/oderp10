# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################

{
    'name': 'Mongolian POS Session Report',
    'version': '1.0',
    'depends': ['point_of_sale', 'report'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """Борлуулалтын цэгийн тайлан""",
    'data': [
        'wizard/pos_session_report_view.xml',
        'security/ir.model.access.csv',
    ],
    "auto_install": False,
    "installable": True,
}
