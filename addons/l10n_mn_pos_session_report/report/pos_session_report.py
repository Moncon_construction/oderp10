# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://asterisk-tech.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

from odoo import models, fields, api, _
from odoo.tools.translate import _
from xlsxwriter.utility import xl_rowcol_to_cell
import xlsxwriter
from io import BytesIO
import base64
import time
from datetime import datetime, timedelta
from itertools import *


class PosSessionReport(models.Model):
    _name = "pos.session.report"
    _description = "POS Session Report"
        
    date_from = fields.Date("Start Date", required=True)
    date_to = fields.Date("End Date", required=True)
    pos_config_ids = fields.Many2many('pos.config', 'pos_session_report_pos_config_rel', 'wizard_id', 'pos_config_id', 'POS Config', required=True)
    company_id = fields.Many2one('res.company', 'Company', readonly=True, default=lambda self: self.env.user.company_id.id)
    
    def export(self):
        this = self
        sheetname_1 = '1'
        
        output = BytesIO()
        
        report_name = _('POS Session Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('pos_session_report'), form_title=file_name).create({})
        
        workbook = xlsxwriter.Workbook(output)
        worksheet2 = workbook.add_worksheet(sheetname_1)
        
        title = workbook.add_format({
            'bold': 1,
            'align': 'left',
            'valign': 'vcenter',
            'font_size': 14,
            'font_name': 'Times New Roman',
            })
        title1 = workbook.add_format({
            'bold': 1,
            'align': 'left',
            'valign': 'vcenter',
            'font_size': 9,
            'font_name': 'Times New Roman',
            })
        title2 = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 9,
            'font_name': 'Times New Roman',
            'bg_color': '#99ccff',
            'text_wrap':1,
            })

        header = workbook.add_format({
            'border': 1,
            'bold': 1,
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'on',
            'font_size':8,
            'font_name': 'Times New Roman',
            })
        
        header1 = workbook.add_format({
            'border': 1,
            'bold': 1,
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'on',
            'fg_color': '#ffd700',
            'font_size':8,
            'font_name': 'Times New Roman',
            })
        header2 = workbook.add_format({
            'border': 1,
            'bold': 1,
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'on',
            'fg_color': '#9acd32',
            'font_size':8,
            'font_name': 'Times New Roman',
            })
        header3 = workbook.add_format({
            'border': 1,
            'align': 'left',
            'bold': 1,
            'valign': 'vcenter',
            'font_size':8,
            'font_name': 'Times New Roman',
            'fg_color': '#ccffff',
            'num_format':'0.0'
            })
        header4 = workbook.add_format({
            'border': 1,
            'align': 'right',
            'bold': 1,
            'valign': 'vcenter',
            'font_size':8,
            'font_name': 'Times New Roman',
            'fg_color': '#ccffff',
            'num_format': '#,##0.0'
            })
        header5_rota = workbook.add_format({
            'border': 1,
            'bold': 1,
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'on',
            'font_size':8,
            'rotation': 90,
            'font_name': 'Times New Roman',
            })
        
        cell_format = workbook.add_format({
            'border': 1,
            'bold': 1,
            'align': 'left',
            'valign': 'vcenter',
            'font_size':7,
            'font_name': 'Times New Roman',
            'text_wrap':1,
            })
        cell_format1 = workbook.add_format({
            'border': 1,
            'align': 'right',
            'valign': 'vcenter',
            'font_size':7,
            'font_name': 'Times New Roman',
            'num_format': '#,##0.0'
            })
        cell_format2 = workbook.add_format({
            'border': 1,
            'align': 'left',
            'valign': 'vcenter',
            'font_size':7,
            'font_name': 'Times New Roman',
            'text_wrap':1,
            })
        cell_format3 = workbook.add_format({
            'border': 1,
            'align': 'center',
            'bold': 1,
            'valign': 'vcenter',
            'font_size':7,
            'font_name': 'Times New Roman',
            'text_wrap':1,
            'num_format':'0.0'
            })

        worksheet2.set_column('A:A', 8)
        worksheet2.set_column('B:B', 15)

        date_format = workbook.add_format()
        
        pos_config_ids = []
        if self.pos_config_ids:
            pos_config_ids = self.pos_config_ids.ids
        else:
            pos_config_ids = self.env['pos.config'].search([])
        
        row = 1
        col = 0
        
        worksheet2.write(0, 0 , this.company_id.name, title1)
        worksheet2.write(row + 1, col + 1 , u'Борлуулалтын цэгийн сэшн тайлан', title)
        
        row += 3
        worksheet2.write(row, 0 , u'Огноо', title2)
        worksheet2.write(row, 1 , u'Сэшн', title2)
        worksheet2.write(row, 2 , u'Бэлэн мөнгө', title2)
        worksheet2.write(row, 3 , u'Карт', title2)
        worksheet2.write(row, 4 , u'Нийт', title2)
        
        row += 1
        records = {}
        self._cr.execute("SELECT s.start_at as date, s.id as session_id, c.id as pos_id, c.name as pos_name, s.state as pos_state, "
                         "s.name as session_name, a.id as statement_id, j.name as journal, j.type as journal_type, a.id as bank_statement_id, a.balance_end_real-a.balance_start as amount "
                         "FROM pos_session s, pos_config c, account_bank_statement a, account_journal j "
                         "WHERE c.id=s.config_id and a.journal_id=j.id and s.id=a.pos_session_id and "
                                "s.start_at between '%s' and '%s' and c.id in (%s)"
                         "ORDER BY c.id, s.id" % (self.date_from, self.date_to + " 23:59:59", ','.join(str(a) for a in pos_config_ids)))
        
        records = self._cr.dictfetchall()
        
        if records:
            pos_ids = []
            session_ids = []
            cash_writen, card_writen = False, False
            
            def write_summary(row_for_sum, cash_writen, card_writen):
                if not cash_writen:
                    worksheet2.write(row_for_sum - 1, 2, 0, cell_format1)
                if not card_writen:
                    worksheet2.write(row_for_sum - 1, 3, 0, cell_format1)
                worksheet2.write_formula(row_for_sum, 4, '{=SUM(C' + str(row_for_sum + 1) + ':D' + str(row_for_sum + 1) + ')}', cell_format3)
                
            for record in records:
                if record['pos_id'] not in pos_ids:
                    if session_ids and len(session_ids) > 1:
                        write_summary(row, cash_writen, card_writen)
                        cash_writen, card_writen = False, False
                    worksheet2.merge_range(row, 0, row, 4, record['pos_name'], header3)
                    pos_ids.append(record['pos_id'])
                    row += 1
                if record['session_id'] not in session_ids:
                    worksheet2.write(row, 0, record['date'][:10], cell_format3)
                    worksheet2.write(row, 1, record['session_name'], cell_format3)
                    write_summary(row, cash_writen, card_writen)
                    cash_writen, card_writen = False, False
                    session_ids.append(record['session_id'])
                else:
                    row -= 1
                    
                total_amount = 0
                bank_statement_id = self.env['account.bank.statement'].search([('id', '=', record['bank_statement_id'])])
                if bank_statement_id:
                    for line in bank_statement_id.line_ids:
                        # Only add amounts from POS /others haven't ref/
                        total_amount += line.amount if line.ref else 0
                    
                if record['journal_type'] == 'cash':
                    cash_writen = True
                    worksheet2.write(row, 2, total_amount, cell_format1)
                else:
                    card_writen = True
                    worksheet2.write(row, 3, total_amount, cell_format1)
                    
                if record == records[-1] and session_ids and len(session_ids) > 1:
                    write_summary(row, cash_writen, card_writen)
                    cash_writen, card_writen = False, False
                    
                row += 1
        
        worksheet2.set_zoom(100)

        workbook.close()
        out = base64.encodestring(output.getvalue())
        file_name = u'Борлуулалтын цэг сэшн тайлан'
        
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        
        # call export function
        return report_excel_output_obj.export_report()
        
        excel_id = self.env['report.excel.output'].create({'data': out, 'name': file_name + '.xlsx'})
        
        return {
            'name': 'Export Result',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'report.excel.output',
            'res_id': excel_id,
            'view_id': False,
            'context': context,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'nodestroy': True,
        }
