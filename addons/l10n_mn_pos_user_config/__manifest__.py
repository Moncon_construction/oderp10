# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Pos user config",
    'version': '2.1',
    'depends': ['point_of_sale'],
    
    'author': "Asterisk Technologies LLC",
    'sequence': 15,
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Mongolian Point of Sale for pharmacies
    """,
    'data': [
        'views/res_users_view.xml',
        'security/ir_rule.xml',
    ],
    
    'application': True,
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
    }