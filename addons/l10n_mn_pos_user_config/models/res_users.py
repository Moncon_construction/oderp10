# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models
import logging
_logger = logging.getLogger(__name__)


class InheritedResUsers(models.Model):
    _inherit = 'res.users'
    allowed_pos_config = fields.Many2many(
        'pos.config', 'res_users_pos_config_rel', 'uid', 'pc_id', string='Allowed Pos Config')