# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime
from odoo import api, fields, models, _, tools
from odoo.exceptions import UserError


class AccountBankStatement(models.Model):
    _inherit = "account.bank.statement"
    
    def get_opened_bank_statements(self):
        ids = []
        self.env.cr.execute(
            "select * from account_bank_statement abs \
            left join pos_session ps on abs.pos_session_id = ps.id \
            where abs.state != 'confirm' and ps.state = 'closed'")
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]

        tree_id = self.env.ref("l10n_mn_pos_wrong_transactions.view_bank_statement_tree_pos_wrong_transaction")
        form_id = self.env.ref("account.view_bank_statement_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Opened Bank Statement for closed session'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.bank.statement',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }