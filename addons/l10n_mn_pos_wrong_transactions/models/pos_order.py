# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime
from odoo import api, fields, models, _, tools
from odoo.exceptions import UserError


class PosOrder(models.Model):
    _inherit = "pos.order"
    
    @api.model
    def is_module_installed(self, module_name=None):
        return module_name in self.env['ir.module.module']._installed()
    
    def get_under_payment_orders(self):
        ids = []
        self.env.cr.execute(
            "select * from pos_order po \
            left join (select pos_statement_id, sum(amount)as amount from account_bank_statement_line group by pos_statement_id) as absl on absl.pos_statement_id = po.id \
            left join (select order_id, sum(price_unit * (1-discount/100) * qty)as amount_total from pos_order_line group by order_id) as pol on pol.order_id = po.id \
            left join pos_session ps on po.session_id = ps.id \
            where absl.amount is null or pol.amount_total != absl.amount and ps.state = 'closed'")
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]

        tree_id = self.env.ref("point_of_sale.view_pos_order_tree")
        form_id = self.env.ref("point_of_sale.view_pos_pos_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Under Payment Orders'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'pos.order',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }
        
    def get_uncreated_account_move_orders(self):
        ids = []
        self.env.cr.execute(
            "select * from pos_order po \
            left join pos_session ps on po.session_id = ps.id \
            where po.account_move is null and ps.state = 'closed'")
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]
        tree_id = self.env.ref("point_of_sale.view_pos_order_tree")
        form_id = self.env.ref("point_of_sale.view_pos_pos_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Uncreated account move Pos Orders'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'pos.order',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }
        
    def get_duplicate_ddtd_orders(self):
        ids = []
        self.env.cr.execute(
            "select * from pos_order po inner join \
            (select ddtd_number from pos_order group by ddtd_number \
             having count(*) > 1) sub \
            on po.ddtd_number = sub.ddtd_number")
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]
        tree_id = self.env.ref("l10n_mn_pos_wrong_transactions.view_pos_order_tree_inherit_l10n_mn_pos_wrong_transactions")
        form_id = self.env.ref("point_of_sale.view_pos_pos_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Duplicated DDTD Pos Orders'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'pos.order',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }
        
    def get_uncreated_stock_picking_orders(self):
        ids = []
        self.env.cr.execute(
            "select * from pos_order po \
            left join pos_session ps on po.session_id = ps.id \
            where po.picking_id is null and ps.state = 'closed'")
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]
        tree_id = self.env.ref("point_of_sale.view_pos_order_tree")
        form_id = self.env.ref("point_of_sale.view_pos_pos_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Uncreated Stock Picking Pos Orders'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'pos.order',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }