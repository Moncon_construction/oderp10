# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime
from odoo import api, fields, models, _, tools
from odoo.exceptions import UserError


class PosOrderLine(models.Model):
    _inherit = "pos.order.line"
    
    @api.model
    def is_module_installed(self, module_name=None):
        return module_name in self.env['ir.module.module']._installed()
    
    def get_difference_quantity_pos_order_lines(self):
        ids = []
        self.env.cr.execute(
            "select * from pos_order_line pol \
            left join stock_move sm on sm.order_line_id=pol.id \
            left join stock_picking sp on sm.picking_id = sp.id \
            left join pos_order po on pol.order_id=po.id \
            left join pos_session ps on po.session_id = ps.id \
            where pol.qty != sm.product_qty and ps.state = 'closed' or \
            (not exists (select id from stock_move sm where sm.order_line_id = pol.id and sm.picking_id = po.picking_id) and ps.state = 'closed') \
            ")
        fetched = self.env.cr.fetchall()

        if len(fetched) > 0:
            ids = [line[0] for line in fetched]

        tree_id = self.env.ref("point_of_sale.view_pos_order_line")
        form_id = self.env.ref("point_of_sale.view_pos_order_line_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Difference between point of sale line quantity and stock move quantity'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'pos.order.line',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }