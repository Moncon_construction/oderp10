# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime
from odoo import api, fields, models, _, tools
from odoo.exceptions import UserError


class StockPicking(models.Model):
    _inherit = "stock.picking"
    
    def get_undelivered_stock_pickings(self):
        ids = []
        self.env.cr.execute("select * from stock_picking sp \
        left join stock_move sm on sm.picking_id=sp.id \
        left join pos_order_line pol on sm.order_line_id=pol.id \
        left join pos_order po on pol.order_id=po.id \
        left join pos_session ps on po.session_id=ps.id \
        where sp.state != 'done' and ps.state = 'closed'")
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]
        tree_id = self.env.ref("stock.vpicktree")
        form_id = self.env.ref("stock.view_picking_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Undelivered stock pickings'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'stock.picking',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }