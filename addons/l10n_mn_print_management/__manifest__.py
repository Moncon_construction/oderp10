# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'Mongolian Print Management',
    'version': '2.1',
    'category': 'Mongolian Modules',
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'sequence': 15,
    'depends': ['mrp'],
    'description': """
Manage the Manufacturing process in Odoo
===========================================

The manufacturing module allows you to cover planning, ordering, stocks and the manufacturing or assembly of products from raw materials and components. It handles the consumption and production of products according to a bill of materials and the necessary operations on machinery, tools or human resources according to routings.

    """,
    'data': [
        'views/mrp_workcenter_views.xml',
    ],
    'demo': [
        ],
    'application': True,
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
