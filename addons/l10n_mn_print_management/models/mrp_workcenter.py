# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models

class MrpWorkcenter(models.Model):
    _inherit = ['mrp.workcenter']

    is_printing_press = fields.Boolean('Is Printing Press', default=False)
    cycle_major_capacity = fields.Float('Cycle Major Capacity')
    cycle_major_measure = fields.Many2one('product.uom', 'Cycle Major Measure')
    cycle_assistant_capacity = fields.Float('Cycle Assistant Capacity')
    cycle_assistant_measure = fields.Many2one('product.uom', 'Cycle Assistant Measure')
    cycle_date = fields.Float('Cycle Date (Time)')
    cycle_cost = fields.Float('Cycle Cost')
    cycle_price = fields.Float('Cycle Price')
#     analytic_journal_id         = fields.Many2one('account.analytic.account', 'Analytic Account')
    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic Account')
    general_account_id = fields.Many2one('account.account', 'Financial Account')
