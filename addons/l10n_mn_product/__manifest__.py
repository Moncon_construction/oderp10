# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Product",
    'version': '1.0',
    'depends': [
        'stock',
        'account'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
   Product Additional Features
    """,
    'data': [
        'data/product_standard_price_precision_data.xml',
        'security/security.xml',
        'data/product_sequence.xml',
        'views/product_create_view.xml',
        'views/product_view.xml',
        'views/product_brand_view.xml',
        'wizard/product_info_change_view.xml',
        'security/ir.model.access.csv',
        'views/product_size_view.xml',
        'views/stock_config_settings.xml',
        'views/product_size_view.xml',
        'views/product_create_checker_view.xml',
        'views/merge_inventory_view.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
