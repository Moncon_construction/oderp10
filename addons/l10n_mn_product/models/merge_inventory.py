# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2019 Asterisk Technologies LLC Co.,ltd (<http://www.asterisk-tech.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

import logging
import base64
import xlrd
from odoo import fields, models, api, _  # @UnresolvedImport
from tempfile import NamedTemporaryFile
from odoo.exceptions import UserError, ValidationError  # @UnresolvedImport
from io import BytesIO
import base64


class MergeInventory(models.TransientModel):
    _name = 'merge.inventory'
    '''
       Тооллого нэгтгэх
    '''

    main_inventory_id = fields.Many2one('stock.inventory', string='Main inventory', required=False)
    merge_inventory_id = fields.Many2one('stock.inventory', string='Merge inventory', required=False)

    def inventory_merge(self):
        if self.main_inventory_id.state != 'confirm' or self.merge_inventory_id.state != 'confirm':
            raise UserError(_('Please, You can only merge a confirm inventory.'))
        if not self.merge_inventory_id.line_ids:
            raise UserError(_('Please, Inventory empty.'))
        self.merge_inventory_id.line_ids.write({'inventory_id': self.main_inventory_id.id})
        self.merge_inventory_id.write({'state': 'draft'})
        self.merge_inventory_id.unlink()
