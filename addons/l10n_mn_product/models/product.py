# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2016 Asterisk Technologies LLC Co.,ltd
#    (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462
#
##############################################################################

from odoo import api, fields, models, _
from odoo import exceptions
from odoo.tools.float_utils import float_round
import odoo.addons.decimal_precision as dp  # @UnresolvedImport
from odoo.exceptions import UserError, AccessError


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    def _get_default_category_id(self):
        if self._context.get('categ_id') or self._context.get('default_categ_id'):
            return self._context.get('categ_id') or self._context.get('default_categ_id')
        category = self.env['product.category'].search([('company_id', '=', self.env.user.company_id.id), ('parent_id', '=', False)])
        if category:
            category = category[0]
        return category and category.type == 'normal' and category.id or False

#   Барааны Нөөцөлсөн, Боломжит, Замд яваа, Ирээдүйн тоог тооцож байна.
    @api.multi
    def _compute_qty(self):
        move_qty = 0
        qty = 0
        company = self.env.user.company_id
        warehouses = self.env['stock.warehouse'].search([('company_id', '=', self.env.user.company_id.id)])
        for product in self:
            transit_qty = reserved_qty = availability_qty = forecast_qty = 0
            locations = []
            products = product.mapped('product_variant_ids').ids
            for wh in warehouses:
                location = self.env['stock.location'].search([('usage', '=', 'internal'), ('location_id', 'child_of', [wh.view_location_id.id])])
                locations += location
            loc_ids = [loc.id for loc in locations]
            if len(products) > 0:
                products = tuple(products)
            on_hand_qty = product.qty_available
            if products and loc_ids:
                self._cr.execute("SELECT sum(product_qty)::decimal(16,2) AS product_qty from stock_move "
                                 "where location_dest_id in %s and product_id in %s and state = 'assigned'", (tuple(loc_ids), products))
                transit_moves = self._cr.fetchall()
                transit_qty = sum([qty[0] if qty[0] is not None else 0 for qty in transit_moves if transit_moves is not list])

                if company.availability_compute_method == 'stock_quant':
                    self._cr.execute("SELECT sum(sm.product_qty)::decimal(16,2) AS product_qty from stock_quant sq "
                                     "LEFT JOIN stock_move sm ON sm.id = sq.reservation_id "
                                     "where sq.location_id in %s and sq.product_id in %s and sq.reservation_id is not null and sm.state = 'assigned'", (tuple(loc_ids), products))
                    quants = self._cr.fetchall()
                    if quants:
                        reserved_qty = sum([qty[0] if qty[0] is not None else 0 for qty in quants if quants is not list])
                    else:
                        self._cr.execute("SELECT sum(product_qty)::decimal(16,2) AS product_qty from stock_move "
                                         "where location_id in %s and product_id in %s and state = 'assigned'", (tuple(loc_ids), products))
                        reserved_moves = self._cr.fetchall()
                        reserved_qty = sum([qty[0] if qty[0] is not None else 0 for qty in reserved_moves if reserved_moves is not list])

                    self._cr.execute("SELECT sum(product_qty)::decimal(16,2) AS product_qty from stock_move "
                                     "where location_dest_id in %s and product_id in %s and state = 'assigned'", (tuple(loc_ids), products))
                    transit_moves = self._cr.fetchall()
                    transit_qty = sum([qty[0] if qty[0] is not None else 0 for qty in transit_moves if transit_moves is not list])

                elif company.availability_compute_method == 'stock_move':
                    self._cr.execute("SELECT sum(product_qty)::decimal(16,2) AS product_qty from stock_move "
                                     "where location_id in %s and product_id in %s and state = 'assigned'", (tuple(loc_ids), products))
                    reserved_moves = self._cr.fetchall()
                    reserved_qty = sum([qty[0] if qty[0] is not None else 0 for qty in reserved_moves if reserved_moves is not list])

            reserved_qty = float_round(reserved_qty, precision_rounding=product.uom_id.rounding)
            transit_qty = float_round(transit_qty, precision_rounding=product.uom_id.rounding)

            availability_qty = on_hand_qty - reserved_qty
            forecast_qty = availability_qty + transit_qty

            availability_qty = float_round(availability_qty, precision_rounding=product.uom_id.rounding)
            forecast_qty = float_round(forecast_qty, precision_rounding=product.uom_id.rounding)

            product.availability_qty = availability_qty
            product.forecast_qty = forecast_qty
            product.transit_qty = transit_qty
            product.reserved_qty = reserved_qty

    name = fields.Char(track_visibility='onchange')
    type = fields.Selection(track_visibility='onchange', default= 'product')
    default_code = fields.Char(track_visibility='onchange')
    barcode = fields.Char(track_visibility='onchange')
    brand_name = fields.Many2one('product.brand', 'Product Brand', index=True, ondelete="cascade", track_visibility='onchange')
    list_price = fields.Float(track_visibility='onchange')
    uom_id = fields.Many2one(track_visibility='onchange')
    uom_po_id = fields.Many2one(track_visibility='onchange')
    company_id = fields.Many2one(track_visibility='onchange')
    categ_id = fields.Many2one('product.category', 'Internal Category',
                               change_default=True, default=_get_default_category_id, domain="[('type','=','normal')]",
                               required=True, track_visibility='onchange', help="Select category for the current product")
    weight = fields.Float(track_visibility='onchange')
    life_time = fields.Integer(track_visibility='onchange')
    use_time = fields.Integer(track_visibility='onchange')
    removal_time = fields.Integer(track_visibility='onchange')
    alert_time = fields.Integer(track_visibility='onchange')
    supplier_id = fields.Many2one('res.partner', 'Basic Supplier', track_visibility='onchange')
    available_in_mobile = fields.Boolean(track_visibility='onchange')
    available_in_pos = fields.Boolean(track_visibility='onchange')
    property_account_income_id = fields.Many2one(track_visibility='onchange')
    property_account_expense_id = fields.Many2one(track_visibility='onchange')
    active = fields.Boolean(track_visibility='onchange')
    reserved_qty = fields.Float(compute='_compute_qty', string='Reserved Qty')
    availability_qty = fields.Float(compute='_compute_qty', string='Reserved Qty')
    transit_qty = fields.Float(compute='_compute_qty', string='Reserved Qty')
    forecast_qty = fields.Float(compute='_compute_qty', string='Reserved Qty')

    @api.onchange('categ_id')
    def onchange_categ_id(self):
        partner_list = []
        if self.seller_ids:
            for info in self.seller_ids:
                partner_list.append(info.name.id)
            return {'domain': {'supplier_id': [('id', 'in', partner_list)]}}

    @api.model
    def create(self, vals):
        user = self.env['res.users'].search([('id', '=', self._uid)])
        if not user.has_group('l10n_mn_product.group_product_creator'):
            raise AccessError(_('This user has no right to create product'))
        return super(ProductTemplate, self).create(vals)

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(ProductTemplate, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar,
                                                           submenu=submenu)
        if view_type == 'form':
            if 'type' in res['fields']:
                res['fields']['type']['selection'] = [('product', _('ProductS')), ('service', _('Service'))]
        return res

    @api.multi
    def check_categ(self, categ_id):
        products = self.mapped('product_variant_ids')
        stock_moves = self.env['stock.move'].search([('product_id', 'in', products.ids)])
        if stock_moves:
            categ = self.env['product.category'].browse(categ_id)
            if self.categ_id.property_stock_valuation_account_id.id == categ.property_stock_valuation_account_id.id:
                return True
            else:
                return False
        else:
            return True
    
    @api.multi
    def _set_list_price(self, value):
        ''' Store the standard price change in order to be able to retrieve the cost of a product for a given date'''
        PriceHistory = self.env['product.price.history']
        for temp in self:
            product = self.env['product.product'].search([
                            ('product_tmpl_id', '=', temp.id)])
            PriceHistory.create({
                'product_id': product.id,
                'price_type': 'list_price',
                'product_template_id': temp.id,
                'list_price': value,
                'old_list_price': product.list_price,
                'company_id': self._context.get('force_company', self.env.user.company_id.id),
            })

    @api.multi
    def write(self, vals):
        # Бараа засах эрх шалгах
        user = self.env['res.users'].search([('id', '=', self._uid)])
        if not user.has_group('l10n_mn_product.group_product_creator'):
            raise AccessError(_('This user has no right to create product'))
        #       Барааны дотоод код давхацаж байгааг шалгах
        if 'categ_id' in vals:
            if not self.check_categ(vals['categ_id']):
                raise UserError(_('Cant change product category. Must be same account category'))
        if ('default_code' in vals) or ('company_id' in vals):
            for obj in self:
                company_id = vals.get('company_id') if 'company_id' in vals else obj.company_id.id
                default_code = vals.get('default_code') if 'default_code' in vals else obj.default_code
                product_found_obj = self.env['product.template'].search([('id', '!=', obj.id), ('company_id', '=', company_id), ('default_code', '=', default_code)], limit=1)
                if product_found_obj:
                    if product_found_obj.default_code:
                        raise UserError(_('"[%s] %s" барааны Дотоод кодтой давхардсан байна. Өөр ялгаатай код оруулна уу' % (product_found_obj.default_code, product_found_obj.name)))
        if 'list_price' in vals:
            self._set_list_price(vals['list_price'])
#       Return
        return super(ProductTemplate, self).write(vals)

    @api.multi
    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        """OVERRIDE
            Хуулбарлан үүсгэх функц ажиллах үед орчуулгагүйгээр үүсгэж дараа нь орчуулга хийж байна.
            Орчуулга хийх үед constraint ажиллаж байгаа тул орчуулгатайгаар үүсгэж орчуулга хийх хэсгийг хасав
        """
        default['name'] = self.name + _('(copy)')
        self.ensure_one()
        vals = self.copy_data(default)[0]
        new = self.create(vals)
        #  OVERRIDE BEGINS #
        # self.with_context(from_copy_translation=True).copy_translations(new)
        #  OVERRIDE ENDS #
        return new

    @api.constrains('name')
    def _check_product_name(self):
        if self.env.user.company_id.check_product_unique_name:
            for product in self:
                if product.name:
                    other_products = self.env['product.template'].search([
                        ('name', '=', product.name),
                        ('id', '!=', product.id)])
                    if other_products:
                        exception = _(' Product Name duplicated: ') + product.name
                        raise exceptions.ValidationError(exception)


class ProductPriceHistory(models.Model):
    _inherit = 'product.price.history'

    def _price_field_get(self):
        mf = self.pool.get('product.price.type')
        ids = mf.search([('field', 'in', ('list_price', 'retail_price', 'standard_price'))])
        res = []
        for ptype in mf.browse(ids):
            res.append((ptype.field, ptype.name))
        return res

    price_type = fields.Selection([
        ('list_price', 'list_price'),
        ('standard_price', 'standard_price')],
        string='Price Type', default='standard_price')
    list_price = fields.Float('Historized Price', digits=dp.get_precision('Product Price'))
    old_list_price = fields.Float('Old Price', digits=dp.get_precision('Product Price'))
    create_uid = fields.Many2one('res.users', 'Created by', readonly=True)
    product_template_id = fields.Many2one('product.template', string='Template')
    cost = fields.Float('Cost', digits=dp.get_precision('Product Standard Price'))


class ProductProduct(models.Model):
    _inherit = 'product.product'

    @api.model
    def create(self, vals):
        user = self.env['res.users'].search([('id', '=', self._uid)])
        if not user.has_group('l10n_mn_product.group_product_creator'):
            raise AccessError(_('This user has no right to create product'))
        return super(ProductProduct, self).create(vals)

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(ProductProduct, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar,
                                                          submenu=submenu)
        if view_type == 'form':
            if 'type' in res['fields']:
                res['fields']['type']['selection'] = [('product', _('ProductS')), ('service', _('Service'))]
        return res

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        """ 
            @Override: Барааг 'Зураасан Код /barcode/' болон 'Дотоод Код /default_code/'-р хайхдаа ILIKE operator-р хайдаг болгов. 
        """

        # if 'search_by_ilike' key not passed from *.xml file then return super's result.
        super_result = super(ProductProduct, self).name_search(name, args, operator, limit)
        if 'search_by_ilike' not in self._context.keys():
            return super_result

        products = []
        args = [] if not args else args

        # search products using ILIKE operator
        if name and operator in ['=', 'ilike', '=ilike', 'like', '=like']:
            products = self.search(['|', ('default_code', 'ilike', name), ('barcode', 'ilike', name)] + args, limit=limit)

        if not products:
            return super_result

        # append super's result to ILIKE operator's given result --> new_result
        # then return new_result by limit
        new_result = products.name_get()
        if len(new_result) >= limit:
            return new_result
        else:
            extendable_count = limit - len(new_result)
            extended_count = 0
            if extendable_count:
                for sresult in super_result:
                    if sresult not in new_result:
                        new_result.append(sresult)
                        extended_count += 1
                    if extended_count > extendable_count:
                        break

            return new_result

    @api.constrains('barcode')
    def _constraint_barcode(self):
        self.env.cr.execute("SELECT pt.name "
                            "FROM product_product pp "
                            "LEFT JOIN product_template pt ON pt.id = pp.product_tmpl_id "
                            "WHERE pt.company_id = %s AND pp.id != %s AND pp.barcode = '%s'" % (
                            self.company_id.id, self.id, self.barcode))
        check_product = self.env.cr.dictfetchall()
        if check_product != []:
            raise UserError(_('%s\nBarcode is exists! Try different barcode.') % (check_product[0]['name']))

        if 'product.template.barcode' in self.env:
            self.env.cr.execute("SELECT pt.name "
                                "FROM product_template_barcode ptb "
                                "LEFT JOIN product_template pt ON ptb.product_id = pt.id "
                                "WHERE ptb.barcode = '%s'" % (self.barcode))
            check_addtional = self.env.cr.dictfetchall()
            if check_addtional != []:
                raise UserError(_('%s\nBarcode is exists! Try different barcode.') % (check_addtional[0]['name']))

    @api.multi
    def _set_standard_price(self, value):
        ''' Store the standard price change in order to be able to retrieve the cost of a product for a given date'''
        PriceHistory = self.env['product.price.history']
        for product in self:
            PriceHistory.create({
                'product_id': product.id,
                'price_type': 'standard_price',
                'product_template_id': product.product_tmpl_id.id,
                'cost': value,
                'company_id': self._context.get('force_company', self.env.user.company_id.id),
                'datetime': self.env.context.get('force_date')
            })

    @api.multi
    def write(self, values):
        user = self.env['res.users'].search([('id', '=', self._uid)])
        if not user.has_group('l10n_mn_product.group_product_creator'):
            raise AccessError(_('This user has no right to edit product'))
        ''' Store the standard price change in order to be able to retrieve the list_price of a product for a given date'''

        res = super(ProductProduct, self).write(values)
        return res

    _sql_constraints = [
        ('barcode_uniq', 'unique(company_id, barcode)', _("A barcode can only be assigned to one product on a company!")),
    ]

    @api.multi
    def name_get(self):
        # TDE: this could be cleaned a bit I think

        def _name_get(d):
            name = d.get('name', '')
            code = self._context.get('display_default_code', True) and d.get('default_code', False) or False
            barcode = d.get('barcode', False) or False

            if code:
                name = '[%s] %s' % (code, name)
            if barcode:
                name = '[%s] %s' % (barcode, name)
            return (d['id'], name)

        partner_id = self._context.get('partner_id')
        if partner_id:
            partner_ids = [partner_id, self.env['res.partner'].browse(partner_id).commercial_partner_id.id]
        else:
            partner_ids = []

        # all user don't have access to seller and partner
        # check access and use superuser
        self.check_access_rights("read")
        self.check_access_rule("read")

        result = []
        for product in self.sudo():
            # display only the attributes with multiple possible values on the template
            variable_attributes = product.attribute_line_ids.filtered(lambda l: len(l.value_ids) > 1).mapped('attribute_id')
            variant = product.attribute_value_ids._variant_name(variable_attributes)

            name = variant and "%s (%s)" % (product.name, variant) or product.name
            sellers = []
            if partner_ids:
                sellers = [x for x in product.seller_ids if (x.name.id in partner_ids) and (x.product_id == product)]
                if not sellers:
                    sellers = [x for x in product.seller_ids if (x.name.id in partner_ids) and not x.product_id]
            if sellers:
                for s in sellers:
                    seller_variant = s.product_name and (
                        variant and "%s (%s)" % (s.product_name, variant) or s.product_name
                    ) or False
                    mydict = {
                        'id': product.id,
                        'name': seller_variant or name,
                        'default_code': s.product_code or product.default_code,
                        'barcode': product.barcode,
                    }
                    temp = _name_get(mydict)
                    if temp not in result:
                        result.append(temp)
            else:
                mydict = {
                    'id': product.id,
                    'name': name,
                    'default_code': product.default_code,
                    'barcode': product.barcode,
                }
                result.append(_name_get(mydict))
        return result

    def get_product_available(self, ids):
        context = self.env.context
        if context is None:
            context = {}
        location_obj = self.env['stock.location']
        warehouse_obj = self.env['stock.warehouse']
        #shop_obj = self.pool.get('sale.shop')
        user = self.env['res.users'].browse(self.env.uid)
        
        states = context.get('states',[])
        what = context.get('what',())
        if not ids:
            ids = self.search([]).ids
        res = {}.fromkeys(ids, 0.0)
        if not ids:
            return res
        
        is_not_internal = False
        if context.get('type', False):
            is_not_internal = context['type']
        
        '''
        if context.get('shop', False):
            warehouse_id = shop_obj.read(cr, uid, int(context['shop']), ['warehouse_id'])['warehouse_id'][0]
            if warehouse_id:
                context['warehouse'] = warehouse_id
        '''
        if context.get('warehouse', False):
            lot_id = warehouse_obj.read(int(context['warehouse']), ['lot_stock_id'])['lot_stock_id'][0]
            if lot_id:
                context['location'] = lot_id
        
        if context.get('location', False):
            if type(context['location']) == type(1):
                location_ids = [context['location']]
            elif type(context['location']) in (type(''), type(u'')):
                location_ids = location_obj.search([('name','ilike',context['location'])], context=context)
            else:
                location_ids = context['location']
        else:
            location_ids = []
            wids = warehouse_obj.search([], context=context)
            if not wids:
                return res
            for w in warehouse_obj.browse(wids, context=context):
                location_ids.append(w.lot_stock_id.id)
                    
        # build the list of ids of children of the location given by id
        if context.get('compute_child',True):
            child_location_ids = location_obj.search([('location_id', 'child_of', location_ids)])
            location_ids = child_location_ids.ids or location_ids
        
        # this will be a dictionary of the product UoM by product id
        product2uom = {}
        uom_ids = []
        #for product in self.with_context(context).read(ids, ['uom_id']):
        #    product2uom[product['id']] = product['uom_id'][0]
        #    uom_ids.append(product['uom_id'][0])
        # this will be a dictionary of the UoM resources we need for conversion purposes, by UoM id
        uoms_o = {}
        for uom in self.env['product.uom'].with_context(context).browse(uom_ids):
            uoms_o[uom.id] = uom

        results = []
        results2 = []

        from_date = context.get('from_date',False)
        to_date = context.get('to_date',False)
        date_str = False
        date_values = False
        where = [tuple(location_ids),tuple(location_ids),tuple(ids),tuple(states)]
        if from_date and to_date:
            date_str = "m.date>=%s and m.date<=%s"
            where.append(tuple([from_date]))
            where.append(tuple([to_date]))
        elif from_date:
            date_str = "m.date>=%s"
            date_values = [from_date]
        elif to_date:
            date_str = "m.date<=%s"
            date_values = [to_date]
        if date_values:
            where.append(tuple(date_values))

        prodlot_id = context.get('prodlot_id', False)
        prodlot_clause = ''
        if prodlot_id:
            prodlot_clause = ' and m.prodlot_id = %s '
            where += [prodlot_id]
            
        # TODO: perhaps merge in one query.
        if 'in' in what:
            # all moves from a location out of the set to a location in the set
            if is_not_internal:
                self._cr.execute(
                    'select sum(m.product_qty), m.product_id, m.product_uom '\
                    'from stock_move m, stock_picking p, stock_picking_type t '\
                    'where m.location_id NOT IN %s '\
                    'and m.location_dest_id IN %s '\
                    'and m.product_id IN %s '\
                    'and m.picking_id = p.id and p.picking_type_id=t.id and t.code != \'internal\' '\
                    'and m.state IN %s ' + (date_str and 'and '+date_str+' ' or '') +' '\
                    + prodlot_clause + 
                    'group by m.product_id,m.product_uom',tuple(where))
                results = self._cr.fetchall()
            else:
                a = ('select sum(m.product_qty), m.product_id, m.product_uom '\
                    'from stock_move m '\
                    'where m.location_id NOT IN %s '\
                    'and m.location_dest_id IN %s '\
                    'and m.product_id IN %s '\
                    'and m.state IN %s ' + (date_str and 'and '+date_str+' ' or '') +' ' + prodlot_clause + 'group by m.product_id,m.product_uom') % tuple(where)
                self._cr.execute(
                    'select sum(m.product_qty), m.product_id, m.product_uom '\
                    'from stock_move m '\
                    'where m.location_id NOT IN %s '\
                    'and m.location_dest_id IN %s '\
                    'and m.product_id IN %s '\
                    'and m.state IN %s ' + (date_str and 'and '+date_str+' ' or '') +' '\
                    + prodlot_clause + 
                    'group by m.product_id,m.product_uom',tuple(where))
                results = self._cr.fetchall()
        if 'out' in what:
            # all moves from a location in the set to a location out of the set
            if is_not_internal:
                self._cr.execute(
                    'select sum(m.product_qty), m.product_id, m.product_uom '\
                    'from stock_move m, stock_picking p, stock_picking_type t '\
                    'where m.location_id IN %s '\
                    'and m.location_dest_id NOT IN %s '\
                    'and m.product_id  IN %s '\
                    'and m.picking_id = p.id and p.picking_type_id=t.id and t.code != \'internal\' '\
                    'and m.state in %s ' + (date_str and 'and '+date_str+' ' or '') + ' '\
                    + prodlot_clause + 
                    'group by m.product_id,m.product_uom',tuple(where))
                results2 = self._cr.fetchall()
            else:
                self._cr.execute(
                    'select sum(m.product_qty), m.product_id, m.product_uom '\
                    'from stock_move m '\
                    'where m.location_id IN %s '\
                    'and m.location_dest_id NOT IN %s '\
                    'and m.product_id  IN %s '\
                    'and m.state in %s ' + (date_str and 'and '+date_str+' ' or '') + ' '\
                    + prodlot_clause + 
                    'group by m.product_id,m.product_uom',tuple(where))
                results2 = self._cr.fetchall()
            
        # Get the missing UoM resources
        uom_obj = self.env['product.uom']
        uoms = map(lambda x: x[2], results) + map(lambda x: x[2], results2)
        if context.get('uom', False):
            uoms += [context['uom']]
        uoms = filter(lambda x: x not in uoms_o.keys(), uoms)
        if uoms:
            uoms = uom_obj.with_context(context).browse(list(set(uoms)))
            for o in uoms:
                uoms_o[o.id] = o
                
        #TOCHECK: before change uom of product, stock move line are in old uom.
        context = {'raise-exception': False}
        # Count the incoming quantities
        for amount, prod_id, prod_uom in results:
            #amount = uom_obj.with_context(context)._compute_qty_obj(uoms_o[prod_uom], amount, uoms_o[context.get('uom', False) or product2uom[prod_id]])
            
            res[prod_id] += amount
        # Count the outgoing quantities
        for amount, prod_id, prod_uom in results2:
            #amount = uom_obj.with_context(context)._compute_qty_obj(uoms_o[prod_uom], amount, uoms_o[context.get('uom', False) or product2uom[prod_id]])
            res[prod_id] -= amount
        return res

class ProductCategory(models.Model):
    _inherit = "product.category"

    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id.id)
    category_code = fields.Char('Category Code')
    product_code_sequence = fields.Many2one('ir.sequence', readonly=True, store=True, string="Category Sequence")
    code_size = fields.Integer('Code Size', default=3)

    def check_category_code(self, vals):
        if 'parent_id' in vals:
            self.env.cr.execute("SELECT * FROM product_category where id!= %s and category_code='%s' AND parent_id ='%s'" % (self.id,                                                                                                    vals['category_code'], vals['parent_id']))
        else:
            self.env.cr.execute("SELECT * FROM product_category where  id!= %s and category_code='%s' AND parent_id IS NULL" % (self.id, vals['category_code']))
        cate_check = self.env.cr.dictfetchall()
        if cate_check != []:
            return True
        return False

    @api.constrains('category_code')
    def _constraint_category_code(self):
        vals = {'category_code': self.category_code}
        if self.parent_id:
            vals['parent_id'] = self.parent_id.id
        if self.check_category_code(vals):
            raise UserError(_('Category code exists! Try different category code.'))

    def set_category_code(self):
        vals = {'category_code': self.env["ir.sequence"].next_by_code("mongolian.product.sequence")}

        while self.check_category_code(vals):
            vals['category_code'] = self.env["ir.sequence"].next_by_code("mongolian.product.sequence")
        return vals['category_code']

    @api.model
    def create(self, vals):
        categ_code = self.env["ir.sequence"].next_by_code("mongolian.product.sequence")
        code_sz = 3
#       Ижил ангилалын кодтой бараа байгаа эсэхийг шалгаж байна
        if 'category_code' in vals:
            if vals['category_code'] is not False:
                categ_code = vals["category_code"]

        if 'category_code' in vals.keys() and vals['category_code'] is not False:
            if vals['code_size'] is not False:
                code_sz = vals["code_size"]

#       Ангилалын дарааллыг үүсгэж product_code_sequence талбарт холбож өгч байна.
        seq_obj = self.env['ir.sequence']
        seq_vals = {
            u'padding': code_sz,
            u'code': categ_code,
            u'name': (u'Product(%s)' % vals['name']),
            u'implementation': u'standard',
            u'company_id': 1,
            u'use_date_range': False,
            u'number_increment': 1,
            u'prefix': False,
            u'date_range_ids': [],
            u'number_next_actual': 0,
            u'active': True,
            u'suffix': False
        }
        seq_id = seq_obj.sudo().create(seq_vals)
        vals.update({'product_code_sequence': seq_id.id, 'category_code': categ_code})
        return super(ProductCategory, self).create(vals)

    @api.constrains('name')
    def _check_product_category_name(self):
        for category in self:
            if category.name:
                other_categorys = self.env['product.category'].search([
                    ('name', '=', category.name),
                    ('id', '!=', category.id),
                    ('company_id', '=', category.company_id.id)])
            for other_category in other_categorys:
                if other_category:
                    exception = _('Category Name duplicated: ') + other_category.name
                    raise exceptions.ValidationError(exception)

    @api.multi
    def write(self, vals):
        # Ангилалын name эсвэл category_code талбар өөрчлөгдөхөд тус ангилалын дарааллын талбарууд мөн өөрчлөгддөг болгож өгч байна.
        for obj in self:
            if 'category_code' not in vals:
                if not obj.category_code:
                    vals['category_code'] = obj.set_category_code()
            elif not vals['category_code']:
                vals['category_code'] = obj.set_category_code()
        if 'name' or 'category_code' in vals:
            for cat in self:
                seq = cat.product_code_sequence
                seq_vals = {}

                # Ангилалын дараалал байгаа эсэхийг шалгаж байхгүй бол үүсгэнэ.
                if not seq:
                    seq_vals = {
                        u'padding': 4,
                        u'code': (u'%s' % vals.get('category_code') if 'category_code' in vals else cat.category_code),
                        u'name': (u'Product(%s)' % vals.get('name') if 'name' in vals else cat.name),
                        u'implementation': u'standard',
                        u'company_id': 1,
                        u'use_date_range': False,
                        u'number_increment': 1,
                        u'prefix': False,
                        u'date_range_ids': [],
                        u'number_next_actual': 0,
                        u'active': True,
                        u'suffix': False
                    }
                    seq = self.env['ir.sequence'].sudo().create(seq_vals)
                    vals.update({'product_code_sequence': seq.id})
                else:
                    if 'name' in vals:
                        seq_vals['name'] = (u'Product(%s)' % vals['name'])
                    if 'category_code' in vals:
                        seq_vals['code'] = (u'%s' % vals['category_code'])
                    seq.sudo().write(seq_vals)

        return super(ProductCategory, self).write(vals)

    @api.multi
    def unlink(self):
        for obj in self:
            obj.product_code_sequence.sudo().unlink()
        return super(ProductCategory, self).unlink()


class ProductBrand(models.Model):
    _name = 'product.brand'
    _rec_name = 'brand_name'

    brand_name = fields.Char(string="Brand name", required=True)
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id.id)
    image = fields.Binary("Photo", attachment=True,
                          help="This field holds the image used as photo for the employee, limited to 1024x1024px.")
