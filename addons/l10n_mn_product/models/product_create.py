# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2016 Asterisk Technologies LLC Co.,ltd
#    (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462
#
##############################################################################

import logging
import base64
import xlrd
from odoo import fields, models, _  # @UnresolvedImport
from tempfile import NamedTemporaryFile
from odoo.exceptions import UserError, ValidationError  # @UnresolvedImport
import decimal


class product_create_tool(models.TransientModel):
    _name = 'product.create.tool'
    '''
        Бараа материалын эхний үлдэгдлийг Excel файлаас импортлох
    '''

    data_file = fields.Binary('Excel File')
    date = fields.Datetime('Date', default=fields.Date.context_today)
    warehouse_id = fields.Many2one('stock.warehouse',
                                   'Warehouse', required=True)
    product_code_type = fields.Selection([('barcode', 'Import by Barcode'), ('default_code', 'Import By Default Code'),('product_name','Import By Product Name')], 'Product Code Type')

    def is_module_installed(self, module_name):
        self._cr.execute("SELECT id FROM ir_module_module WHERE name = '%s' AND state IN ('installed', 'to upgrade')" %module_name)
        results = self._cr.dictfetchall()
        
        if results and len(results) > 0:
            return True
        else:
            return False
        
    def set_wh_standart_price(self, prod_id, cost):
        # Агуулах бүрээр өртөг хөтлөх бол тухайн мөрийн бараагаар
        # 'Барааны агуулах бүрээрх өртөг' цэсний объект үүсгэх/өртөгийг нь шинэчлэх
        
        if self.is_module_installed("l10n_mn_stock_account_cost_for_each_wh"):
            self._cr.execute("""
                SELECT id FROM product_warehouse_standard_price 
                WHERE company_id = %s AND warehouse_id = %s AND product_id = %s""" %(self.warehouse_id.company_id.id, self.warehouse_id.id, prod_id.id))
            results = self._cr.dictfetchall()
            
            if results and len(results) > 0:
                self._cr.execute("""
                    UPDATE product_warehouse_standard_price set initial_standard_price = %s
                    WHERE id = %s
                """, (cost, results[0]['id']))
            else:
                self._cr.execute("""
                    INSERT INTO product_warehouse_standard_price (company_id, warehouse_id, product_id, initial_standard_price, standard_price) 
                    VALUES (%s, %s, %s, %s, %s)
                """ %(self.warehouse_id.company_id.id, self.warehouse_id.id, prod_id.id, cost, 0))
                
    """"
    Байрлалын зураасан код оруулах
    Цувралын дугаарыг шалгах: Product_template -> tracking талбарыг шалгах
    Байрлал тус бүрээр тооллого үүсгэх
    """

    def process(self):
        wiz = self.browse(self.id)
        try:
            fileobj = NamedTemporaryFile('w+', delete=False)
            fileobj.write(base64.decodestring(wiz.data_file))
            fileobj.seek(0)
            file_name = fileobj.name
            book = xlrd.open_workbook(file_name)
        except:
            raise UserError(_('Error loading data file. \
                                \ Please try again!'))

        # Эксэлиэс float төрлөөр уншигдаад байгаа тул char төрөл рүү хөрвүүлэх функц
        def float_to_str(f, precision=1):
            if isinstance(f, float):
                return '{0:.{prec}f}'.format(
                    decimal.Context(prec=50).create_decimal(f),
                    prec=precision,
                ).rstrip('0').rstrip('.') or '0'
            else:
                return f


        sheet = book.sheet_by_index(0)
        nrows = sheet.nrows
        ncols = sheet.ncols
        rowi = 1
        locations = []
        exists = []
        prod_temp = []
        product_obj = self.env['product.product']
        inventory_obj = self.env['stock.inventory']
        inventory_line_obj = self.env['stock.inventory.line']
        prodlot_obj = self.env['stock.production.lot']
        locatioin_obj = self.env['stock.location']
        history_obj = self.env['product.price.history']
        product_temp = self.env['product.template']
        inventory_id = False
        is_each_wh = self.is_module_installed("l10n_mn_stock_account_cost_for_each_wh")
        errors = []
        if is_each_wh:
            column = ['location_code','prod_code', 'bar_code','prod_name', 'qty', 'cost','track_lot', 'removal_date']
        else:
            column = ['location_code','prod_code', 'bar_code','prod_name', 'qty', 'track_lot', 'removal_date']
        while rowi < nrows:
            row = sheet.row(rowi)
            if ncols >= len(column):
                if is_each_wh:
                    location_code = float_to_str(row[0].value) or False
                    prod_code = float_to_str(row[1].value)
                    bar_code = float_to_str(row[2].value)
                    prod_name = row[3].value or False
                    qty = row[4].value
                    cost = row[5].value
                    track_lot = row[6].value
                    removal_date = row[7].value
                else:
                    location_code = float_to_str(row[0].value) or False
                    prod_code = float_to_str(row[1].value)
                    bar_code = float_to_str(row[2].value)
                    prod_name = row[3].value or False
                    qty = row[4].value
                    track_lot = row[5].value
                    removal_date = row[6].value
            else:
                raise UserError(_('Sorry, please check the excel file fields!'))

            rowi += 1
            tracking_type = 'None'
            if wiz.product_code_type == 'barcode':
                exists = product_obj.search([('barcode', '=', bar_code)])
            elif wiz.product_code_type == 'default_code':
                exists = product_obj.search([('default_code', '=', prod_code)])
            elif wiz.product_code_type == 'product_name':
                prod_temp = product_temp.search([('name', '=', prod_name)])
                exists = product_obj.search([('product_tmpl_id', '=', prod_temp[0].id)]) if prod_temp else False

            if not location_code:
                errors.append(_('Data error %s row! \n \
                        Location columns must have a value!' % (rowi)))
            if exists and location_code:
                prod_id = exists[0]
                if location_code not in locations:
                    Location = locatioin_obj.search(
                        [('barcode', '=', location_code)])
                    if Location and Location.usage == 'internal':
                        inventory_id = inventory_obj.create({
                            'date': wiz.date,
                            'accounting_date': wiz.date,
                            'location_id': Location.id,
                            'is_initial' : True,
                            'owner': self.env.user.partner_id.id, 
                            'name': u'%s /%s -%s эхний үлдэгдэл'
                            % (wiz.warehouse_id.name, Location.name, wiz.date)
                        })
                    elif Location and Location.usage != 'internal':
                        errors.append(_('Data error %s row! \n \
                        %s location type not intenal!' % (rowi, Location.name)))
                    else:
                        errors.append(_('Data error %s row! \n %s \
                        barcode with location not exists!' % (rowi, location_code)))
                    locations.append(location_code)
                '''
                Цуварлын дугаарыг үүсгэхдээ Барааны хөтлөлт
                талбараас хамааруулан шалгаж үүсгэх
                '''
                if Location and inventory_id and prod_id:
                    if is_each_wh:
                        self.set_wh_standart_price(prod_id, cost)
                        prod_id.with_context(force_date=wiz.date).standard_price = cost
                    else:
                        cost = prod_id.standard_price
                            
                    if prod_id.tracking == 'serial':
                        tracking_type = 'serial'
                    elif prod_id.tracking == 'lot':
                        tracking_type = 'lot'
                    prodlot = False
                    if track_lot and tracking_type == 'None':
                        errors.append(_('Data error %s row! \n %s with code %s \
                        product no tracking!' % (rowi, prod_code, prod_name)))
                    elif track_lot and tracking_type == 'serial':
                        exists = prodlot_obj.search(
                            [('product_id', '=', prod_id.id), ('name', '!=', track_lot)])
                        if exists:
                            errors.append(_('Data error %s row! \n %s with code %s \
                            product by unique serial number!' % (rowi, prod_code, prod_name)))
                        else:
                            exists = prodlot_obj.search([
                                ('product_id', '=', prod_id.id),
                                ('name', '=', track_lot)])
                            if exists and len(exists) == 1:
                                prodlot_id = exists[0]
                                prodlot = prodlot_id.id
                            elif exists and len(exists) > 1:
                                errors.append(_('Data error %s row! \n %s with code %s \
                                product by unique serial number!' % (rowi, prod_code, prod_name)))
                            else:
                                prodlot_id = prodlot_obj.create({
                                    'product_id': prod_id.id,
                                    'name': track_lot,
                                    'ref': prod_id.default_code,
                                    'create_date': wiz.date,
                                    'removal_date': removal_date})
                                prodlot = prodlot_id.id
                    elif track_lot and tracking_type == 'lot':
                        exists = prodlot_obj.search([
                            ('product_id', '=', prod_id.id),
                            ('name', '=', track_lot)])
                        if exists:
                            prodlot_id = exists[0]
                            prodlot = prodlot_id.id
                        else:
                            prodlot_id = prodlot_obj.create({
                                'product_id': prod_id.id,
                                'name': track_lot,
                                'ref': prod_id.default_code,
                                'create_date': wiz.date,
                                'removal_date': removal_date})
                            prodlot = prodlot_id.id
                    inventory_line_obj.create({
                        'location_id': Location.id,
                        'product_id': prod_id.id,
                        'product_uom_id': prod_id.uom_id.id,
                        'product_qty': qty,
                        'prod_lot_id': prodlot,
                        'standard_price':cost,
                        'inventory_id': inventory_id.id,
                    })
                    inventory_id.prepare_inventory()
            else:
                errors.append(_('Data error %s row! \n %s with code %s \
                product not exists!' % (rowi, prod_code, prod_name)))
                
        if len(errors) > 0:
            error_message = _('There are %s errors found.\n') % len(errors)
            for index in range(len(errors)):
                error_message += (str(index+1) + ":\t" + errors[index] + "\n")
            raise ValidationError(error_message)
        
        return True