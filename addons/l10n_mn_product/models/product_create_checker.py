# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2019 Asterisk Technologies LLC Co.,ltd (<http://www.asterisk-tech.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

import logging
import base64
import xlrd
from odoo import fields, models, api, _  # @UnresolvedImport
from tempfile import NamedTemporaryFile
from odoo.exceptions import UserError, ValidationError  # @UnresolvedImport
from io import BytesIO
import base64
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
import time
from datetime import datetime

_logger = logging.getLogger(__name__)


class product_create_tool(models.TransientModel):
    _name = 'checkout.product.create.tool'
    '''
        Бараа материалын эхний үлдэгдлийг Excel файлаас импортлоход шалгалт хийж
        алдаатай болон алдаагүй өгөгдлүүдийг тусад нь файлд хийж үзүүлэх
    '''

    data_file = fields.Binary('Excel File')
    is_result_set = fields.Boolean(default=False, readonly=True)
    has_error = fields.Boolean(default=False, readonly=True)
    result_data_file = fields.Binary('Excel File', readonly=True)
    result = fields.Text(string="Checker Result", readonly=True)
    error_result = fields.Text(string="Checker Result", readonly=True)
    product_code_type = fields.Selection([('barcode', 'Import by Barcode'), ('default_code', 'Import By Default Code'),('product_name','Import By Product Name')], default='barcode', required=True, string='Product Code Type')

    def download(self):
        file_name = "%s_%s.xls" % ('product_create_checker', time.strftime('%Y%m%d_%H%M'))
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('product_create_checker'), form_title=file_name).create({})
        report_excel_output_obj.filedata = self.result_data_file
        return report_excel_output_obj.export_report() 
        
    def add_error(self, error_msgs, error_str, error_index, total_error_count, row_index_with_err, rowi):
        error_msgs[error_index]['total_error_count'] += 1
        error_msgs[error_index]['errors'].append(error_str)
        total_error_count += 1
        row_index_with_err[str(rowi)].append(error_index)
        
        return error_msgs, total_error_count, True, row_index_with_err
        
    """
    Дараахи 7-н нөхцөлийн дагуу мөрийг алдаатай эсэхийг шийднэ:
        1. Байрлалын зураасан код оруулах 
        2. Байрлалын зураасан код байгаа ч тухайн кодоор байрлал олдохгүй бол
        3. Байрлалын зураасаар байрлал олдсон ч тухайн байрлалын төрөл нь 'internal' биш бол
        4. Барааны кодын төрөлд сонгосон сонголтоос хамаарч уг барааг /баркод, дотоод код, нэр/-р нь хайхад тухайн бараа олдоогүй бол
        5. Цувралын дугаар баганад утга оруулсан ч тухайн мөрийн барааны 'Хөтлөлт' талбарт 'Хөтлөлт байхгүй' гэж сонгосон бол
        6. Цувралын дугаар баганад утга оруулсан бөгөөд тухайн барааны 'Хөтлөлт' талбарын утга 'Цувралуудаар' байгаад тухайн бараагаар цуврал олдохгүй бол
        7. Цувралын дугаар баганад утга оруулсан бөгөөд тухайн барааны 'Хөтлөлт' талбарын утга 'Цувралуудаар' байгаад тухайн бараагаар 1-с олон цуврал олдох юм бол
    """
    def ger_error_by_name(self, error_index):
        if error_index == 0:
            return _(u"Location columns must have a value") 
        elif error_index == 1:
            return _(u"Location type not internal") 
        elif error_index == 2:
            return _(u"Barcode with location not exist") 
        elif error_index == 3:
            return _(u"Product no tracking")
        elif error_index == 4:
            return _(u"Serial number with the product not found")
        elif error_index == 5:
            return _(u"Product not exists")
   
    def is_module_installed(self, module_name):
        self._cr.execute("SELECT id FROM ir_module_module WHERE name = '%s' AND state IN ('installed', 'to upgrade')" %module_name)
        results = self._cr.dictfetchall()
        
        if results and len(results) > 0:
            return True
        else:
            return False

    def get_row_values(self, row):
        coly = 0
        
        location_code = row[coly].value or False
        prod_name = row[coly+1].value or False
        prod_code = row[coly+2].value or False
        bar_code = row[coly+3].value or False
        track_lot = row[coly+4].value or False
        
        return location_code, prod_name, prod_code, bar_code, track_lot
        
    def process(self):
        _logger = logging.getLogger('jacara')
        try:
            fileobj = NamedTemporaryFile('w+', delete=False)
            fileobj.write(base64.decodestring(self.data_file))
            fileobj.seek(0)
            file_name = fileobj.name
            book = xlrd.open_workbook(file_name)
        except:
            raise UserError(_('Error loading data file. \
                                \ Please try again!'))

        sheet = book.sheet_by_index(0)
        nrows = sheet.nrows
        
        rowi = 1
        exists = []
        prod_temp = []
        
        ProductProduct = self.env['product.product']
        StockInventory = self.env['stock.inventory']
        StockInventoryLine = self.env['stock.inventory.line']
        StockProductionLot = self.env['stock.production.lot']
        StockLocation = self.env['stock.location']
        ProductPriceHistory = self.env['product.price.history']
        ProductTemplate = self.env['product.template']
        
        error_count = 0
        error_msgs = [{}, {}, {}, {}, {}, {}]
        for i in range(6):
            error_msgs[i] = {
                'total_error_count': 0,
                'errors': []
            } 

        row_index_with_err = {}
        wrong_row_indeces = []
        
        for i in range(nrows):
            row_index_with_err[str(i+1)] = []

        while rowi < nrows:
            row_has_error = False
            exists = False
            
            row = sheet.row(rowi)
            
            # Тухайн мөрийн харгалзах баганын утгаар доорхи талбарын утгуудыг дүүргэж байна.
            location_code, prod_name, prod_code, bar_code, track_lot = self.get_row_values(row)
            main_check_is_empty = False

            if self.product_code_type == 'barcode':
                exists = ProductProduct.search([('barcode', '=', bar_code)])
                main_check_is_empty = True if not bar_code else main_check_is_empty
            elif self.product_code_type == 'default_code':
                exists = ProductProduct.search([('default_code', '=', prod_code)])
                main_check_is_empty = True if not prod_code else main_check_is_empty
            elif self.product_code_type == 'product_name':
                exists = ProductProduct.search([('product_tmpl_id.name', '=', prod_name)])
                main_check_is_empty = True if not prod_name else main_check_is_empty

            if not location_code:
                msg = _(u'Data error %s row! \n Location columns must have a value!') % (rowi)
                error_msgs, error_count, row_has_error, row_index_with_err = self.add_error(error_msgs, msg, 0, error_count, row_index_with_err, rowi)
           
            if not (exists and len(exists) > 0 and not main_check_is_empty):
                if self.product_code_type == 'barcode':
                    msg = _(u'Data error %s row! \n %s with bar code %s product not exists!') % (rowi, bar_code, prod_name) 
                elif self.product_code_type == 'default_code':
                    msg = _(u'Data error %s row! \n %s with code %s product not exists!') % (rowi, prod_code, prod_name)
                elif self.product_code_type == 'product_name':
                    msg = _(u'Data error %s row! \n %s named product not exists!') % (rowi, prod_name) 
 
                error_msgs, error_count, row_has_error, row_index_with_err = self.add_error(error_msgs, msg, 5, error_count, row_index_with_err, rowi)

            location = StockLocation.search(
                    [('barcode', '=', location_code)])
            if location and len(location) > 0:
                location = location[0]
                
            if not location:
                msg = _(u'Data error %s row! \n %s barcode with location not exists!') % (rowi, location_code)
                error_msgs, error_count, row_has_error, row_index_with_err = self.add_error(error_msgs, msg, 2, error_count, row_index_with_err, rowi)
            
            if location and location.usage != 'internal' and location_code:
                msg = _(u'Data error %s row! \n \%s location type not internal!') % (rowi, location.name)
                error_msgs, error_count, row_has_error, row_index_with_err = self.add_error(error_msgs, msg, 1, error_count, row_index_with_err, rowi)
                    
            if exists and len(exists) > 0 and location_code:
                product = exists[0]
                '''
                    /8-р багана/ Цувралын дугаар бөглөсөн эсэхээс хамааран дараахи шалгалтуудыг хийнэ.
                    Шалгалт хийхэд тухайн барааны 'Агуулах' таб доторх 'Хөтлөлт' /tracking/ талбарын утгаас хамааруулан шалгалтуудыг хийнэ
                '''
                tracking_type = 'None'
                if product and track_lot:
                    if product.tracking == 'serial':
                        tracking_type = 'serial'
                    elif product.tracking == 'lot':
                        tracking_type = 'lot'
                    prodlot = False
                    if track_lot and tracking_type == 'None':
                        msg = _(u'Data error %s row! \n %s code with %s product\'s tracking field value is \'no tracking\'!') % (rowi, prod_code, prod_name)
                        error_msgs, error_count, row_has_error, row_index_with_err = self.add_error(error_msgs, msg, 3, error_count, row_index_with_err, rowi)
                    elif track_lot and tracking_type == 'serial':
                        lot_exists = StockProductionLot.search(
                            [('product_id', '=', product.id), ('name', '=', track_lot)])
                        if not (lot_exists and len(lot_exists) > 0):
                            msg = _(u'Data error %s row! \n %s with code %s product not found by serial number!') % (rowi, prod_code, prod_name)
                            error_msgs, error_count, row_has_error, row_index_with_err = self.add_error(error_msgs, msg, 4, error_count, row_index_with_err, rowi)
                
            if row_has_error:
                wrong_row_indeces.append(rowi-1)
                
            rowi += 1
            
        if len(wrong_row_indeces) > 0:
            output = BytesIO()
            result_book = xlsxwriter.Workbook(output)
            
            format_cell_error1 = {'bg_color': '#9370db'}
            format_cell_error2 = {'bg_color': '#ef5166'}
            format_cell_error3 = {'bg_color': '#ff8c00'}
            format_cell_error4 = {'bg_color': '#ff99c9'}
            format_cell_error5 = {'bg_color': '#fafad2'}
            format_cell_error6 = {'bg_color': '#98fb98'}
            
            format_text_left = {
                'font_name': 'Times New Roman',
                'font_size': 11,
                'align': 'left',
                'valign': 'vcenter',
                'border': 1
            }
            
            format_text_header = format_text_left.copy()
            format_text_header['bg_color'] = '#99ccff'
            format_text_header['bold'] = True
            format_text_header['align'] = 'center'
            format_text_header['font_size'] = 11
            
            format_text_header1 = format_text_left.copy()
            format_text_header1['bg_color'] = '#ccffff'
            format_text_header1['bold'] = True
            
            format_number_right = format_text_left.copy()
            format_number_right['num_format'] = '#,##0.00'
            format_number_right['align'] = 'right'
            
            format_cell_error1 = result_book.add_format(format_cell_error1)
            format_cell_error2 = result_book.add_format(format_cell_error2)
            format_cell_error3 = result_book.add_format(format_cell_error3)
            format_cell_error4 = result_book.add_format(format_cell_error4)
            format_cell_error5 = result_book.add_format(format_cell_error5)
            format_cell_error6 = result_book.add_format(format_cell_error6)
            
            format_text_left = result_book.add_format(format_text_left)
            format_text_header = result_book.add_format(format_text_header)
            format_text_header.set_text_wrap()
            format_text_header.set_align('vcenter')
            format_text_header1 = result_book.add_format(format_text_header1)
            format_text_header1.set_text_wrap()
            format_text_header1.set_align('vcenter')
            format_number_right = result_book.add_format(format_number_right)
            
            def get_error_format(index):
                if index == 0:
                    return format_cell_error1
                elif index == 1:
                    return format_cell_error2
                elif index == 2:
                    return format_cell_error3
                elif index == 3:
                    return format_cell_error4
                elif index == 4:
                    return format_cell_error5
                else:
                    return format_cell_error6
                
            columns = [u'Байрлалын Зураасан Код', u'Барааны нэр', u'Дотоод код', u'Баркод', u'Цувралын дугаар', u'Алдаа']
            
            result_sheet = result_book.add_worksheet(_(u"Normal rows"))
            result_sheet.set_landscape()
            result_sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
            rowx = 0
            
            # compute column
            result_sheet.set_column('A:A', 15)
            result_sheet.set_column('B:B', 30)
            result_sheet.set_column('C:C', 15)
            result_sheet.set_column('D:D', 15)
            result_sheet.set_column('E:E', 20)
            result_sheet.set_row(0, 30)
            
            result_sheet_error = result_book.add_worksheet(_(u"Errored rows"))
            result_sheet_error.set_landscape()
            result_sheet_error.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
            rowx_error = 0
            
            # compute column
            result_sheet_error.set_column('A:A', 15)
            result_sheet_error.set_column('B:B', 30)
            result_sheet_error.set_column('C:C', 15)
            result_sheet_error.set_column('D:D', 20)
            result_sheet_error.set_column('E:E', 20)
            result_sheet_error.set_column('F:F', 2)
            result_sheet_error.set_column('G:G', 2)
            result_sheet_error.set_column('H:H', 2)
            result_sheet_error.set_column('I:I', 2)
            result_sheet_error.set_column('J:J', 2)
            result_sheet_error.set_column('K:K', 2)
            result_sheet_error.set_column('L:L', 5)
            result_sheet_error.set_column('M:M', 50)
            result_sheet_error.set_column('N:N', 2)
            result_sheet_error.set_row(0, 30)
            
            result_sheet_error_desc = result_book.add_worksheet(_(u"Errored rows detailed description"))
            result_sheet_error_desc.set_landscape()
            result_sheet_error_desc.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
            rowx_error_desc = 0
            
            # compute column
            result_sheet_error_desc.set_column('A:A', 5)
            result_sheet_error_desc.set_column('B:B', 30)
            result_sheet_error_desc.set_column('C:C', 100)
            result_sheet_error_desc.set_row(0, 30)
            
            for i in range(len(columns)):
                if i != (len(columns) - 1):
                    result_sheet.write(rowx, i, columns[i], format_text_header)
                    result_sheet_error.write(rowx_error, i, columns[i], format_text_header)
                else:
                    result_sheet_error.merge_range(rowx_error, i, rowx_error, i+5, columns[i], format_text_header)
                    
            result_sheet_error.write(1, i+7, _(u"Location columns must have a value"), format_text_left)
            result_sheet_error.write(1, i+8, '', format_cell_error1)
            result_sheet_error.write(2, i+7, _(u"Location type not internal"), format_text_left)
            result_sheet_error.write(2, i+8, '', format_cell_error2)
            result_sheet_error.write(3, i+7, _(u"Barcode with location not exists"), format_text_left)
            result_sheet_error.write(3, i+8, '', format_cell_error3)
            result_sheet_error.write(4, i+7, _(u"Product no tracking"), format_text_left)
            result_sheet_error.write(4, i+8, '', format_cell_error4)
            result_sheet_error.write(5, i+7, _(u"Serial number overlapped"), format_text_left)
            result_sheet_error.write(5, i+8, '', format_cell_error5)
            result_sheet_error.write(6, i+7, _(u"Product not exists"), format_text_left)
            result_sheet_error.write(6, i+8, '', format_cell_error6)
        
            for i in range(1, nrows, 1):
                row = sheet.row(i)
                if (i-1) in wrong_row_indeces:
                    rowx_error += 1
                    result_sheet_error.write(rowx_error, 0, row[0].value, format_text_left)
                    result_sheet_error.write(rowx_error, 1, row[1].value, format_text_left)
                    result_sheet_error.write(rowx_error, 2, row[2].value, format_text_left)
                    result_sheet_error.write(rowx_error, 3, row[3].value, format_text_left)
                    result_sheet_error.write(rowx_error, 4, row[4].value, format_text_left)
                    
                    for error_index in row_index_with_err[str(i)]:
                        result_sheet_error.write(rowx_error, 5+error_index, '', get_error_format(error_index))
                    
                elif (i-1) not in wrong_row_indeces:
                    rowx += 1
                    result_sheet.write(rowx, 0, row[0].value, format_text_left)
                    result_sheet.write(rowx, 1, row[1].value, format_text_left)
                    result_sheet.write(rowx, 2, row[2].value, format_text_left)
                    result_sheet.write(rowx, 3, row[3].value, format_text_left)
                    result_sheet.write(rowx, 4, row[4].value, format_text_left)
             
            result_sheet_error_desc.write(rowx_error_desc, 0, u'№', format_text_header)
            result_sheet_error_desc.write(rowx_error_desc, 1, _(u'Error Description'), format_text_header)
            result_sheet_error_desc.write(rowx_error_desc, 2, _(u'Error Detailed Description'), format_text_header)
            
            total_error_count = 0
            for i in range(len(error_msgs)):
                if error_msgs[i]['total_error_count'] > 0:
                    detailed_error_count = 0
                    rowx_error_desc += 1
                    total_error_count += 1
                    result_sheet_error_desc.set_row(rowx_error_desc, 30)
                    result_sheet_error_desc.write(rowx_error_desc, 0, str(total_error_count), format_text_header1)
                    result_sheet_error_desc.write(rowx_error_desc, 1, self.ger_error_by_name(i), format_text_header1)
                    result_sheet_error_desc.write(rowx_error_desc, 2, '', format_text_header1)
                    for j in range(len(error_msgs[i]['errors'])):
                        detailed_error_count += 1
                        rowx_error_desc += 1
                        result_sheet_error_desc.write(rowx_error_desc, 0, '', format_text_left)
                        result_sheet_error_desc.write(rowx_error_desc, 1, str(total_error_count) + "." + str(detailed_error_count), format_text_left)
                        result_sheet_error_desc.write(rowx_error_desc, 2, error_msgs[i]['errors'][j].replace("<br/>", "").replace("\n", ""), format_text_left)
            
            result_book.close()
            
            # set result data
            self.result_data_file = base64.encodestring(output.getvalue())
            self.is_result_set = True
            self.has_error = True
            if len(wrong_row_indeces) == 1:
                error_string = _(u"<p style=\"display: inline;\">You have </p><p style=\"display: inline; font-weight:bold;\">%s</p> rows. But belown <p style=\"display: inline; color:red;\">%s</p> error occured in <p style=\"display: inline; font-weight:bold;\">%s</p> row.<br/>") %((nrows-1), error_count, len(wrong_row_indeces))
            else:
                error_string = _(u"<p style=\"display: inline;\">You have </p><p style=\"display: inline; font-weight:bold;\">%s</p> rows. But belown <p style=\"display: inline; color:red;\">%s</p> errors occured in <p style=\"display: inline; font-weight:bold;\">%s</p> rows.<br/>") %((nrows-1), error_count, len(wrong_row_indeces))
            error_string += u"<ul>"
            error_message = ""
            for i in range(len(error_msgs)):
                if error_msgs[i]['total_error_count'] > 0:
                    error_message += _(u"<li>" + u"<p style=\"display: inline; color:red;\">%s</p> rows: %s.</li>") %(error_msgs[i]['total_error_count'], self.ger_error_by_name(i))
                    # Хэрэв визард дээр нь алдааг харуулах хэрэгтэй гэвэл доорхи комментыг авна.
#                     error_message += _(u"<br/>" + u"<p style=\"display: inline; color:red;\">%s</p> rows: %s.") %(self.ger_error_by_name(i), error_msgs[i]['total_error_count'])
#                     for j in range(len(error_msgs[i]['errors'])):
#                         error_message += (str(i+1) + u"." + str(j+1) + u":&nbsp;" + error_msgs[i]['errors'][j] + u"<br/>")
                        
            error_string += (u"<br/>" + error_message)
            
            error_string += _(u"<br/>You can download result file by belown button which named <p style=\"display: inline; color:orange;\">'Download'</p>.")
            
            self.error_result = error_string
            self.result = False
        else:
            self.is_result_set = False
            self.has_error = False
            self.error_result = False
            self.result = _(u"Congratulations. There is no error occured.")
            self.result_data_file = False
            
        return {
            'name': 'Checkout Product Create Tool',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'checkout.product.create.tool',
            'target':'new',
            'context':{
                'default_product_code_type': self.product_code_type,
                'default_is_result_set': self.is_result_set,
                'default_has_error': self.has_error,
                'default_result': self.result,
                'default_error_result': self.error_result,
                'default_result_data_file': self.result_data_file,
                'default_data_file': self.data_file
            } 
        }
