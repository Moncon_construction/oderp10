# -*- coding: utf-8 -*-
from odoo import api, models, fields


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    available_in_mobile = fields.Boolean(string='Available in the Mobile Application',
                                         help='Check if you want this product to appear in the Mobile',
                                         default=False)

    @api.multi
    def product_template_sync(self):
        res = []
        for line in self.env['product.template'].search([]):
            dic = {'id': line.id, 'name': line.name, 'tracking': line.tracking, 'default_code': line.default_code,
                   'create_date': line.create_date, 'write_date': line.write_date}

            res.append(dic)

        return res

    @api.multi
    def get_product_template_list_sale_mobile(self, local_product_templates):
        update_product_template = []
        insert_product_template = []
        local_ids = []
        result = []

        for p_template in local_product_templates:
            local_ids.append(p_template.get('id'))
            if not p_template.get('write_date'):
                write_date = '2000-01-01 00:00:00.999999'
            else:
                write_date = p_template.get('write_date') + '.999999'
            product_t = self.env['product.template'].search([('id', '=', p_template.get('id')),
                                                             ('write_date', '>', write_date)])
            if product_t:
                update_product_template.append({'id': product_t.id, 'write_date': product_t.write_date,
                                                'name': product_t.name, 'default_code': product_t.default_code,
                                                'tracking': product_t.tracking})
        if len(local_product_templates) > 0:
            insert_product_temp_list = self.env['product.template'].search([('id', 'not in', tuple(local_ids)),
                                                                            ('available_in_mobile', '=', True),
                                                                            ('active', '=', True)])
        else:
            insert_product_temp_list = self.env['product.template'].search([('available_in_mobile', '=', True),
                                                                            ('active', '=', True)])
        for product_template in insert_product_temp_list:
            insert_product_template.append({'id': product_template.id, 'write_date': product_template.write_date,
                                            'name': product_template.name,
                                            'default_code': product_template.default_code,
                                            'tracking': product_template.tracking})
        result.append({'update': update_product_template})
        result.append({'insert': insert_product_template})
        return result


class ProductProduct(models.Model):
    _inherit = 'product.product'

    @api.multi
    def product_product_sync(self, lang):
        res = []
        for line in self.env['product.product'].search([('available_in_mobile', '=', True)]):
            if line.product_tmpl_id.company_id.id == self.env.user.company_id.id:
                taxes_ids = []
                self.env.context = {'lang': lang}
                for l in line.taxes_id:
                    taxes_ids.append(l.id)

                dic = {'id': line.id, 'name': line.name,'default_code': line.default_code,
                       'product_tmpl_id': line.product_tmpl_id.id, 'lst_price': line.lst_price,
                       'list_price': line.list_price, 'standard_price': line.standard_price,
                       'sale_ok': line.sale_ok, 'uom_id': line.uom_id.id, 'type': line.type,
                       'categ_id': line.categ_id.id, 'taxes_id': taxes_ids, 'create_date': line.create_date,
                       'write_date': line.write_date, 'brand_name': line.brand_name.id}
                res.append(dic)
        return res

    @api.multi
    def get_product_list_sale_mobile(self, local_products):
        update_product = []
        insert_product = []
        local_ids = []
        result = []
        self.env.context = {'lang': self.env.user.lang}
        for product in local_products:
            local_ids.append(product.get('id'))
            if not product.get('write_date'):
                write_date = '2000-01-01 00:00:00.999999'
            else:
                write_date = product.get('write_date') + '.999999'
            product_p = self.env['product.product'].search([('id', '=', product.get('id')),
                                                            ('product_tmpl_id.write_date', '>', write_date)])
            if product_p:
                update_product.append({'id': product_p.id, 'write_date': product_p.product_tmpl_id.write_date,
                                       'name': product_p.name, 'default_code': product_p.default_code,
                                       'lst_price': product_p.lst_price, 'list_price': product_p.list_price,
                                       'standard_price': product_p.standard_price, 'type': product_p.type,
                                       'barcode': product_p.barcode, 'product_tmpl_id': product_p.product_tmpl_id.id,
                                       'uom_id': product_p.uom_id.id, 'categ_id': product_p.categ_id.id,
                                       'brand_name': product_p.brand_name.id, 'taxes_id': product_p.taxes_id.ids})

        if len(local_products) > 0:
            insert_product_list = self.env['product.product'].search([('id', 'not in', tuple(local_ids)),
                                                                      ('available_in_mobile', '=', True),
                                                                      ('active', '=', True)])
        else:
            insert_product_list = self.env['product.product'].search([('available_in_mobile', '=', True),
                                                                      ('active', '=', True)])

        for product in insert_product_list:
            insert_product.append({'id': product.id, 'write_date': product.product_tmpl_id.write_date,
                                   'name': product.name, 'default_code': product.default_code,
                                   'lst_price': product.lst_price, 'list_price': product.list_price,
                                   'standard_price': product.standard_price, 'type': product.type,
                                   'barcode': product.barcode, 'product_tmpl_id': product.product_tmpl_id.id,
                                   'uom_id': product.uom_id.id, 'categ_id': product.categ_id.id,
                                   'brand_name': product.brand_name.id, 'taxes_id': product.taxes_id.ids})

        result.append({'update': update_product})
        result.append({'insert': insert_product})
        return result
