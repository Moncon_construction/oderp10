# -*- coding: utf-8 -*-
from odoo import fields, models, _


class ResCompany(models.Model):
    _inherit = "res.company"

    module_l10n_mn_product_code_sequencer = fields.Boolean(string="Calculate product default code automatically",
                                                           default=False)
    check_product_unique_name = fields.Boolean(string="Check Product unique name", default=True)
