# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError


class ProductSequencerConfig(models.TransientModel):
    _inherit = 'stock.config.settings'

    module_l10n_mn_product_code_sequencer = fields.Boolean(related="company_id.module_l10n_mn_product_code_sequencer", default=False,
                   help="When this is checked, default code of product is given automatically.",
                   string="Calculate product default code automatically")  # l10n_mn_product_code_sequencer модуль суулгана.
    check_product_unique_name = fields.Boolean(related="company_id.check_product_unique_name", string="Check Product unique name")

    @api.model
    def get_default_company_check_product_unique_name(self, fields):
        company = self.env.user.company_id
        return {
            'check_product_unique_name': company.check_product_unique_name,

        }