# -*- coding: utf-8 -*-
from odoo import api, fields, models


class ProductInfoChange(models.TransientModel):
    _name = 'product.info.change'
    _description = 'Wizard for product info change'
    brand_name = fields.Many2one('product.brand', 'Product Brand', index=True, ondelete="cascade")
    type = fields.Selection([('product', 'Stockable Product'),
                             ('service', 'Service')], string='Product Type', default='',
                            help='A stockable product is a product for which you manage stock. The "Inventory" app has '
                                 'to be installed.\n'
                                 'A consumable product, on the other hand, is a product for which stock is not managed.'
                                 '\n'
                                 'A service is a non-material product you provide.\n'
                                 'A digital content is a non-material product you sell online. The files attached to th'
                                 'e products are the one that are sold on '
                                 'the e-commerce such as e-books, music, pictures,... The "Digital Product" module has '
                                 'to be installed.')
    list_price = fields.Float(
        'Sale Price', default=0.0,
        help="Base price to compute the customer price. Sometimes called the catalog price.")
    categ_id = fields.Many2one(
        'product.category', 'Internal Category',
        change_default=True, default='', domain="[('type','=','normal')]",
        help="Select category for the current product")
    invoice_policy = fields.Selection([
        ('order', 'Order'),
        ('delivery', 'Delivery')], string='Invoice Policy', default='')
    taxes = fields.Many2many('account.tax', domain=[('type_tax_use', '=', 'sale')], string="Customer Taxes")
    supplier_taxes = fields.Many2many('account.tax', domain=[('type_tax_use', '=', 'purchase')], string="Vendor Taxes")
    purchase_method = fields.Selection([
        ('purchase', 'On ordered quantities'),
        ('receive', 'On received quantities'),
        ], string="Control Purchase Bills",
        help="On ordered quantities: control bills based on ordered quantities.\n"
        "On received quantities: control bills based on received quantity.", default='')
    product_variant_ids = fields.One2many('product.product', 'product_tmpl_id', 'Products', required=False)

    @api.multi
    def product_info_change(self):
        context = dict(self._context or {})
        product_ids = self.env['product.template'].browse(context.get('active_ids'))
        for product in product_ids:
            if self.type:
                product.type = self.type
            if self.categ_id:
                product.write({'categ_id': self.categ_id.id})
            if self.brand_name:
                product.brand_name = self.brand_name
            if self.list_price:
                product.list_price = self.list_price
            if self.purchase_method:
                product.purchase_method = self.purchase_method
            if self.invoice_policy:
                product.invoice_policy = self.invoice_policy
            if self.taxes:
                self.env.cr.execute("delete from product_taxes_rel where prod_id = %d" % product.id)
                for tax in self.taxes:
                    self.env.cr.execute("insert into product_taxes_rel (prod_id, tax_id) values(%s, %s)" % (product.id, tax.id))
            if self.supplier_taxes:
                self.env.cr.execute("delete from product_supplier_taxes_rel where prod_id = %d" % product.id)
                for tax in self.supplier_taxes:
                    self.env.cr.execute("insert into product_supplier_taxes_rel (prod_id, tax_id) values(%s, %s)" % (product.id, tax.id))