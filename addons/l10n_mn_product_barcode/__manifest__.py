# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'Mongolian Product Barcode',
    'version': '1.0',
    'category': 'Mongolian Modules',
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'sequence': 15,
    'depends': ['l10n_mn_product'],
    'description': """
  Manage the Product Barcode process in Odoo

    """,
    'data': [
        'security/ir.model.access.csv',
        'views/product_template_view.xml',
        'report/product_pricelist_templates.xml',
        'report/product_reports.xml',
        'wizard/product_price_list_views.xml'
    ],
    'demo': [
        ],
    'application': True,
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
