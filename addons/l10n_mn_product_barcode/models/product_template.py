# -*- coding: utf-8 -*-

import re

from odoo import api, fields, models, _
from odoo.osv import expression
from odoo.exceptions import UserError, AccessError

class ProductTemplate(models.Model):
    _inherit = 'product.template'
       
    barcode_ids = fields.One2many('product.template.barcode','product_id','barcode')#Нэмэлтээр бар код үүсгэх

class ProductTemplateBarcode(models.Model):
    _name = 'product.template.barcode'
     
    barcode = fields.Char('Barcode')
    product_id = fields.Many2one('product.template','Product')

    @api.constrains('barcode')
    def _constraint_additional_barcode(self):
        self.env.cr.execute("SELECT pt.name "
                            "FROM product_product pp "
                            "LEFT JOIN product_template pt ON pt.id = pp.product_tmpl_id "
                            "WHERE pp.barcode = '%s'" % (self.barcode))
        check_product = self.env.cr.dictfetchall()
        if check_product != []:
            raise UserError(_('%s\nBarcode is exists! Try different barcode.') % (check_product[0]['name']))

        self.env.cr.execute("SELECT pt.name "
                            "FROM product_template_barcode ptb "
                            "LEFT JOIN product_template pt ON ptb.product_id = pt.id "
                            "WHERE ptb.id!= %s AND ptb.barcode = '%s'" % (self.id, self.barcode))
        check_addtional = self.env.cr.dictfetchall()
        if check_addtional != []:
            raise UserError(_('%s\nBarcode is exists! Try different barcode.') % (check_addtional[0]['name']))

    @api.multi
    def get_product_barcode_list_mobile(self, local_barcodes):
        """
        :param local_products: Төхөөрөмжид байгаа барааны баркодын id болон write_date утгууд
        :return: Insert болон Update хийх барааны баркодын өгөгдөлийг илгээнэ
        """
        update_barcode = []
        local_ids = []
        result = []
        for barcode in local_barcodes:
            local_ids.append(barcode.get('id'))
            self.env.cr.execute("""SELECT ptb.id, pp.id AS product_id, ptb.barcode, ptb.write_date 
                                            FROM product_template_barcode ptb 
                                            LEFT JOIN product_product pp ON pp.product_tmpl_id = ptb.product_id
                                            WHERE ptb.id = %s AND ptb.write_date > '%s'""" % (str(barcode.get('id')),
                                                                                              barcode.get(
                                                                                                  'write_date')))
            barcode_query = self.env.cr.dictfetchall()
            if len(barcode_query) > 0:
                for line in barcode_query:
                    dic = {'id': line.get('id'), 'product_id': line.get('product_id'), 'barcode': line.get('barcode'),
                           'write_date': line.get('write_date')}
                update_barcode.append(dic)
        if len(local_barcodes) > 0:
            self.env.cr.execute("SELECT ptb.id, pp.id AS product_id, ptb.barcode, ptb.write_date "
                                "FROM product_template_barcode ptb "
                                "LEFT JOIN product_product pp ON pp.product_tmpl_id = ptb.product_id "
                                "WHERE ptb.id NOT IN " + str(tuple(local_ids)))
        else:
            self.env.cr.execute("SELECT ptb.id, pp.id AS product_id, ptb.barcode, ptb.write_date "
                                "FROM product_template_barcode ptb "
                                "LEFT JOIN product_product pp ON pp.product_tmpl_id = ptb.product_id")
        insert_barcode = self.env.cr.dictfetchall()

        result.append({'update': update_barcode})
        result.append({'insert': insert_barcode})
        return result
    

class ProductProduct(models.Model):
    _inherit = 'product.product'
    
    @api.multi
    @api.onchange("barcode")
    def onchange_barcode(self):
        product_template_obj = self.env['product.template'].search([('name','=',self.name)])
        if product_template_obj:
            vals =  [(0, 0, {'barcode':self.barcode })]
            product_template_obj.write({'barcode_ids':vals})

    # Барааны Нэмэлт баркодонд хайдаг болгосон
    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        for index in range(len(args or [])):
            if args[index][0] == 'barcode':
                barcode = self.env['product.template.barcode'].search([('barcode', '=', args[index][0][2])])
                if barcode:
                    args = [('product_tmpl_id', '=', barcode.product_id.id)]
        return super(ProductProduct, self).search(args, offset=offset, limit=limit, order=order, count=count)

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        if not args:
            args = []
        if name:
            positive_operators = ['=', 'ilike', '=ilike', 'like', '=like']
            products = self.env['product.product']
            if operator in positive_operators:
                products = self.search([('default_code', '=', name)] + args, limit=limit)
#           Барааны Нэмэлт баркодонд хайдаг болгосон
#           Эндээс
                if not products:
                    products = self.search([('barcode', '=', name)] + args, limit=limit)
                    if not products:
                        product_template_barcode_ids = self.env['product.template.barcode'].search([('barcode','=',name)])
                        product_id_list = []
                        for barcode in product_template_barcode_ids:
                            product_id_list.append(barcode.product_id.id)
                        products = self.search([('product_tmpl_id', 'in', product_id_list)] + args, limit=limit)
#           Энэ хооронд
            if not products and operator not in expression.NEGATIVE_TERM_OPERATORS:
                # Do not merge the 2 next lines into one single search, SQL search performance would be abysmal
                # on a database with thousands of matching products, due to the huge merge+unique needed for the
                # OR operator (and given the fact that the 'name' lookup results come from the ir.translation table
                # Performing a quick memory merge of ids in Python will give much better performance
                products = self.search(args + [('default_code', operator, name)], limit=limit)
                if not limit or len(products) < limit:
                    # we may underrun the limit because of dupes in the results, that's fine
                    limit2 = (limit - len(products)) if limit else False
                    products += self.search(args + [('name', operator, name), ('id', 'not in', products.ids)], limit=limit2)
            elif not products and operator in expression.NEGATIVE_TERM_OPERATORS:
                domain = expression.OR([
                    ['&', ('default_code', operator, name), ('name', operator, name)],
                    ['&', ('default_code', '=', False), ('name', operator, name)],
                ])
                domain = expression.AND([args, domain])
                products = self.search(domain, limit=limit)
            if not products and operator in positive_operators:
                ptrn = re.compile('(\[(.*?)\])')
                res = ptrn.search(name)
                if res:
                    products = self.search([('default_code', '=', res.group(2))] + args, limit=limit)
            # still no results, partner in context: search on supplier info as last hope to find something
            if not products and self._context.get('partner_id'):
                suppliers = self.env['product.supplierinfo'].search([
                    ('name', '=', self._context.get('partner_id')),
                    '|',
                    ('product_code', operator, name),
                    ('product_name', operator, name)])
                if suppliers:
                    products = self.search([('product_tmpl_id.seller_ids', 'in', suppliers.ids)], limit=limit)
        else:
            products = self.search(args, limit=limit)
        return products.name_get()
