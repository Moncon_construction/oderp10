# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class product_price_list(models.TransientModel):
    _inherit = 'product.price_list'

    @api.multi
    def print_report(self):
        """
        To get the date and print the report
        @return : return report
        """
        datas = {'ids': self.env.context.get('active_ids', [])}
        res = self.read(['price_list', 'qty1'])
        res = res and res[0] or {}
        res['price_list'] = res['price_list'][0]
        print 'res: ', res
        datas['form'] = res
        return self.env['report'].get_action([], 'l10n_mn_product_barcode.report_pricelist', data=datas)
