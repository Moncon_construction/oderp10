# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Product Code sequence fill",
    'version': '1.0',
    'depends': ['l10n_mn_product'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
   Product Additional Features
    """,
    'data': [
        'views/product_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
