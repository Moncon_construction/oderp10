# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2016 Asterisk Technologies LLC Co.,ltd
#    (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462
#
##############################################################################

from odoo import api, fields, models, _  # @UnresolvedImport

import odoo.addons.decimal_precision as dp
from odoo.exceptions import UserError

class ProductTemplate(models.Model):
    _inherit = 'product.template'
    
    @api.model
    def create(self, vals):
#       Барааны дотоод кодыг автоматаар авч байна  
        temp_num = ""
        product_categ_obj = self.env['product.category'].search([('id','=',vals['categ_id'])])
        if product_categ_obj:
            categ = product_categ_obj
            while categ:
                if categ.category_code:
                    temp_num = categ.category_code + temp_num
                categ = categ.parent_id
            sequence = product_categ_obj.product_code_sequence
            temp=temp_num +sequence.next_by_id()
            #Барааны кодын давхцал шалгаж байна.
            self.env.cr.execute("SELECT default_code FROM product_template")
            product_codes = self.env.cr.dictfetchall()
            if product_codes:
                while True:
                    change = 0;
                    for codes in product_codes:
                        if temp == codes['default_code']:
                            temp = temp_num+sequence.next_by_id()
                            change = 1
                            break
                    if change == 0:
                        break
        vals.update({'default_code': temp})
        
#       Return   
        return super(ProductTemplate, self).create(vals)
    
    @api.multi
    def write(self, vals):
#       Барааны дотоод кодыг автоматаар засч байна
        if 'categ_id' in vals:
            temp_num = ""
            product_categ_obj = self.env['product.category'].search([('id','=',vals['categ_id'])])
            if product_categ_obj:
                categ = product_categ_obj
                while categ:
                    if categ.category_code:
                        temp_num = categ.category_code + temp_num
                        categ = categ.parent_id
                    else:
                        category_code = categ.set_category_code()
                        categ.write({'category_code': category_code})
            sequence = product_categ_obj.product_code_sequence
            temp=temp_num +sequence.next_by_id()
            #Барааны кодын давхцал шалгаж байна.
            self.env.cr.execute("SELECT default_code FROM product_template")
            product_codes = self.env.cr.dictfetchall()
            if product_codes:
                while True:
                    change = 0;
                    for codes in product_codes:
                        if temp == codes['default_code']:
                            temp = temp_num+sequence.next_by_id()
                            change = 1
                            break
                    if change == 0:
                        break
            vals['default_code'] = temp
#       Return 
        return super(ProductTemplate, self).write(vals)

