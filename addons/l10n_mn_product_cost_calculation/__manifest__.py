# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Product Cost Calculation",
    'version': '1.0',
    'depends': [
        'l10n_mn_stock_account',
        'l10n_mn_stock_minus_balance_report',
        'l10n_mn_stock_account_wrong_transactions',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Product Cost Calculation
    """,
    'data': [
        'security/ir.model.access.csv',
        'security/ir_rule.xml',
        'views/product_cost_calculation_view.xml',
        'views/stock_inventory_views.xml',
        'views/fix_stock_quant_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}
