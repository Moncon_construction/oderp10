# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import time

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.float_utils import float_compare

class FixStockQuant(models.Model):
    _name = 'fix.stock.quant'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = 'Fix Stock Quant'
    _order = 'create_date DESC'

    name = fields.Char('Name')
    product_filter = fields.Selection([
        ('different', 'Different between quant and move'),
        ('manual', 'Select Products Manually'),
        ], string='Product of fix stock quant',
        default='different', required=True)
    calculate_by = fields.Selection([
        ('1', 'Calculate Products by 1'),
        ('50', 'Calculate Products by 50'),
        ('100', 'Calculate Products by 100'),
        ('500', 'Calculate Products by 500'),
        ('1000', 'Calculate Products by 1000'),
        ('5000', 'Calculate Products by 5000'),
        ], string='Calculate Products By',
        default='100', required=True)
    description = fields.Text('Description')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('started', 'Started'),
        ('calculated', 'Calculated'),
        ], string='State',
        default='draft', required=True)
    line_ids = fields.One2many('fix.stock.quant.line', 'fix_stock_quant_id', string='Lines')
    start_date = fields.Date('Start Date', required=1)
    end_date = fields.Date('End Date', default=fields.Datetime.now, required=1)
    
    @api.multi
    def _get_products(self):
        products = False
        for obj in self:
            if obj.product_filter == 'different':
                self.env.cr.execute("""select sm.product_id, (round(sm.product_uom_qty,1) - round(cast(sum(sq.qty) as numeric),1)) as difference
                                        from
                                        stock_quant sq join stock_quant_move_rel sqm on sq.id = sqm.quant_id
                                        join stock_move sm on sm.id = sqm.move_id
                                        where name not like 'INV%'
                                        group by sm.id, sm.name, sm.product_id, round(sm.product_uom_qty,1)
                                        having  round(sm.product_uom_qty) <> round(cast(sum(sq.qty) as numeric))
                                        order by sm.id, sm.name, sm.product_id, round(sm.product_uom_qty,1)""")
                products = self.env.cr.dictfetchall()
        return products
    
    @api.multi
    def start(self):
        products = self._get_products()
        lines = self.env['fix.stock.quant.line']
        for obj in self:
            if products:
                for product in products:
                    lines.create({'product_id': product['product_id'],
                                  'difference': product['difference'],
                                  'fix_stock_quant_id': obj.id})
            obj.write({'state': 'started'})
            
    @api.multi
    def make_draft(self):
        self.write({'state': 'draft'})
        
    @api.multi
    def calculating(self):
        for obj in self:
            for line in obj.line_ids:
                if not line.product_id:
                    continue
                self.env.cr.execute("""DELETE FROM stock_quant where product_id = %s and (in_date <= %s and %s <= in_date)""",
                                    (str(line.product_id.id), obj.end_date, obj.start_date))
                moves = self.env['stock.move'].search([('product_id', '=', line.product_id.id), ('state', 'not in', ['cancel']),
                                                       ('date', '<=', obj.end_date), ('date', '>', obj.start_date)], order='date ASC')

                for move in moves:
                    if move.picking_id.state == 'done' or (not move.picking_id and move.state == 'done'):
                        self.env.cr.execute("""DELETE FROM stock_quant_move_rel where move_id = """ + str(move.id))
                        links = self.env['stock.move.operation.link'].search([('move_id', '=', move.id)])
                        if move.picking_id and \
                                (move.picking_id.picking_type_id.use_existing_lots or move.picking_id.picking_type_id.use_create_lots) and \
                                move.product_id.tracking != 'none' and \
                                not (move.restrict_lot_id):
                            lot_id = obj.get_lot_id(move.product_id.id, move)
                            move.restrict_lot_id = lot_id[0]
                        if links:
                            # 1 stock.move-н ард олон stock.move.operation.link үүсэж байгаа тул тэд нарын нийлбэрээр шалгасан болно.
                            # operation_id өөр өөр байх боломжтой тул давталтаар доорх байдлаар шийдэв
                            link_qty = 0
                            for link in links:
                                link_qty += link.qty
                            if move.product_qty > link_qty:
                                link.qty = link.qty + move.product_qty - link_qty
                            elif move.product_qty < link_qty:
                                diff = link_qty - move.product_qty
                                for link in links:
                                    if diff != 0:
                                        if diff == link.qty:
                                            self.env.cr.execute("""DELETE FROM stock_move_operation_link where id = """ + str(link.id))
                                            diff = 0
                                        elif diff < link.qty:
                                            link.qty = link.qty - diff
                                            diff = 0
                                        elif diff > link.qty:
                                            self.env.cr.execute("""DELETE FROM stock_move_operation_link where id = """ + str(link.id))
                                            diff -= link.qty
                        if not move.picking_id:
                            move.picking_type_id = ''
                        account_moves = self.env['account.move.line'].search([('stock_move_id', '=', move.id)])
                        for account_move in account_moves:
                            if len(account_move.move_id.line_ids) > 1:
                                account_move.move_id.button_cancel()
                                account_move.unlink()
                                #self.env.cr.execute("""DELETE FROM account_move_line where id = """ + str(account_move.id))
                            else:
                                account_move.move_id.button_cancel()
                                account_move.move_id.unlink()
                        if move.product_qty != move.product_uom_qty:
                            self.env.cr.execute("""UPDATE stock_move set product_qty = product_uom_qty where id =  """ + str(move.id))
                        move_date = move.date
                        move.action_done_for_fix_quant()
                        move.date = move_date
                                
        self.write({'state': 'calculated'})
    
    @api.one
    def get_lot_id(self, product, move):
        lots = self.env['stock.production.lot'].search([('product_id', '=', product)])
        lot_id = False
        removal_date = False
        for lot in lots:
            if lot.removal_date:
                if not removal_date:
                    removal_date = lot.removal_date
                    lot_id = lot.id
                else:
                    if lot.removal_date < move.date:
                        removal_date = lot.removal_date
                        lot_id = lot.id
        return lot_id
    
class FixStockQuantLine(models.Model):
    _name = 'fix.stock.quant.line'
    _description = 'Fix Stock Quant Line'
    
    fix_stock_quant_id = fields.Many2one('fix.stock.quant', string='Fix Stock Quant')
    product_id = fields.Many2one('product.product', 'Product')
    location_id = fields.Many2one('stock.location', 'Location')
    difference = fields.Float('Difference')
    date = fields.Date('Fix Stock Quant Date')
    
class StockMove(models.Model):
    _inherit = "stock.move"
    
    @api.multi
    def action_done_for_fix_quant(self):
        # Quant засах багажинд зориулан core-н def action_done(self) функцыг хуулж бичив.
        # Шалтгаан: 
         
        """ Process completely the moves given and if all moves are done, it will finish the picking. """
        self.filtered(lambda move: move.state == 'draft').action_confirm()

        Uom = self.env['product.uom']
        Quant = self.env['stock.quant']

        pickings = self.env['stock.picking']
        procurements = self.env['procurement.order']
        operations = self.env['stock.pack.operation']

        remaining_move_qty = {}

        for move in self:
            if move.picking_id:
                pickings |= move.picking_id
            remaining_move_qty[move.id] = move.product_qty
            for link in move.linked_move_operation_ids:
                operations |= link.operation_id
                pickings |= link.operation_id.picking_id

        # Sort operations according to entire packages first, then package + lot, package only, lot only
        operations = operations.sorted(key=lambda x: ((x.package_id and not x.product_id) and -4 or 0) + (x.package_id and -2 or 0) + (x.pack_lot_ids and -1 or 0))

        for operation in operations:

            # product given: result put immediately in the result package (if False: without package)
            # but if pack moved entirely, quants should not be written anything for the destination package
            quant_dest_package_id = operation.product_id and operation.result_package_id.id or False
            entire_pack = not operation.product_id and True or False

            # compute quantities for each lot + check quantities match
            lot_quantities = dict((pack_lot.lot_id.id, operation.product_uom_id._compute_quantity(pack_lot.qty, operation.product_id.uom_id)
            ) for pack_lot in operation.pack_lot_ids)

            qty = operation.product_qty
            if operation.product_uom_id and operation.product_uom_id != operation.product_id.uom_id:
                qty = operation.product_uom_id._compute_quantity(qty, operation.product_id.uom_id)
            if operation.pack_lot_ids and float_compare(sum(lot_quantities.values()), qty, precision_rounding=operation.product_id.uom_id.rounding) != 0.0:
                raise UserError(_('You have a difference between the quantity on the operation and the quantities specified for the lots. '))

            quants_taken = []
            false_quants = []
            lot_move_qty = {}

            prout_move_qty = {}
            prout_move_ids = {}
            for link in operation.linked_move_operation_ids:
                # BEGIN
                # stock.move.operation.link давхардаж үүсэний улмаас quant -ын тоо ширхэг буруу үүсэж байсныг засав
                if link.move_id not in prout_move_ids:
                    prout_move_ids[link.move_id] = link.move_id
                    prout_move_qty[link.move_id] = prout_move_qty.get(link.move_id, 0.0) + link.qty
                # END
            # Process every move only once for every pack operation
            for move in prout_move_qty.keys():
                # TDE FIXME: do in batch ?
                move.check_tracking(operation)

                # TDE FIXME: I bet the message error is wrong
                ####################### START: Хэрвээ move-р нь холбоотой stock_pack_operation олдсон ч уг stock_pack_operation-ы холбоотой move-д тухайн move-с гадна өөр move холбогдсон байвал алдаа өгдөг байсныг болиулж алгасдаг болгов. #######################
                if not remaining_move_qty.get(move.id):
                    continue
                ####################### END: Хэрвээ move-р нь холбоотой stock_pack_operation олдсон ч уг stock_pack_operation-ы холбоотой move-д тухайн move-с гадна өөр move холбогдсон байвал алдаа өгдөг байсныг болиулж алгасдаг болгов. #######################
                
                if not operation.pack_lot_ids:
                    preferred_domain_list = [[('reservation_id', '=', move.id)], [('reservation_id', '=', False)], ['&', ('reservation_id', '!=', move.id), ('reservation_id', '!=', False)]]
                    quants = Quant.quants_get_preferred_domain(prout_move_qty[move], move, ops=operation, domain=[('qty', '>', 0)],
                                                               preferred_domain_list=preferred_domain_list)
                    Quant.quants_move(quants, move, operation.location_dest_id, location_from=operation.location_id,lot_id=False, owner_id=operation.owner_id.id,
                                      src_package_id=operation.package_id.id,dest_package_id=quant_dest_package_id, entire_pack=entire_pack)
                else:
                    # Check what you can do with reserved quants already
                    qty_on_link = prout_move_qty[move]
                    rounding = operation.product_id.uom_id.rounding
                    for reserved_quant in move.reserved_quant_ids:
                        if (reserved_quant.owner_id.id != operation.owner_id.id) or (reserved_quant.location_id.id != operation.location_id.id) or \
                                (reserved_quant.package_id.id != operation.package_id.id):
                            continue
                        if not reserved_quant.lot_id:
                            false_quants += [reserved_quant]
                        elif float_compare(lot_quantities.get(reserved_quant.lot_id.id, 0), 0, precision_rounding=rounding) > 0:
                            if float_compare(lot_quantities[reserved_quant.lot_id.id], reserved_quant.qty, precision_rounding=rounding) >= 0:
                                lot_quantities[reserved_quant.lot_id.id] -= reserved_quant.qty
                                quants_taken += [(reserved_quant, reserved_quant.qty)]
                                qty_on_link -= reserved_quant.qty
                            else:
                                quants_taken += [(reserved_quant, lot_quantities[reserved_quant.lot_id.id])]
                                lot_quantities[reserved_quant.lot_id.id] = 0
                                qty_on_link -= lot_quantities[reserved_quant.lot_id.id]
                    lot_move_qty[move.id] = qty_on_link

                remaining_move_qty[move.id] -= prout_move_qty[move]

            # Handle lots separately
            if operation.pack_lot_ids:
                # TDE FIXME: fix call to move_quants_by_lot to ease understanding
                self._move_quants_by_lot(operation, lot_quantities, quants_taken, false_quants, lot_move_qty, quant_dest_package_id)

            # Handle pack in pack
            if not operation.product_id and operation.package_id and operation.result_package_id.id != operation.package_id.parent_id.id:
                operation.package_id.sudo().write({'parent_id': operation.result_package_id.id})

        # Check for remaining qtys and unreserve/check move_dest_id in
        move_dest_ids = set()
        for move in self:
            if float_compare(remaining_move_qty[move.id], 0, precision_rounding=move.product_id.uom_id.rounding) > 0:  # In case no pack operations in picking
                move.check_tracking(False)  # TDE: do in batch ? redone ? check this

                preferred_domain_list = [[('reservation_id', '=', move.id)], [('reservation_id', '=', False)], ['&', ('reservation_id', '!=', move.id), ('reservation_id', '!=', False)]]
                quants = Quant.quants_get_preferred_domain(remaining_move_qty[move.id], move, domain=[('qty', '>', 0)], preferred_domain_list=preferred_domain_list)
                Quant.quants_move(quants, move, move.location_dest_id, lot_id=move.restrict_lot_id.id, owner_id=move.restrict_partner_id.id)

            # If the move has a destination, add it to the list to reserve
            if move.move_dest_id and move.move_dest_id.state in ('waiting', 'confirmed'):
                move_dest_ids.add(move.move_dest_id.id)

            if move.procurement_id:
                procurements |= move.procurement_id

            # unreserve the quants and make them available for other operations/moves
            move.quants_unreserve()

        # Check the packages have been placed in the correct locations
        self.mapped('quant_ids').filtered(lambda quant: quant.package_id and quant.qty > 0).mapped('package_id')._check_location_constraint()

        # set the move as done
        self.write({'state': 'done', 'date': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)})
        procurements.check()
        # assign destination moves
        if move_dest_ids:
            # TDE FIXME: record setise me
            self.browse(list(move_dest_ids)).action_assign()

        pickings.filtered(lambda picking: picking.state == 'done' and not picking.date_done).write({'date_done': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)})

        return True
