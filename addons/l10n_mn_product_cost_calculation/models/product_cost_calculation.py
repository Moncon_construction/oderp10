# -*- coding: utf-8 -*-
from datetime import date, datetime
import logging
import time

from odoo import api, fields, models, _
import odoo.addons.decimal_precision as dp
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import calendar


_logger = logging.getLogger(__name__)

class StockInventory(models.Model):
    _inherit = 'stock.inventory'
    
    calculation_id = fields.Many2one('product.cost.calculation', string='Product Cost Calculation')
    
class ProductCostCalculation(models.Model):
    _name = 'product.cost.calculation'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = 'Product Cost Calculation'

    def _count_created_inventory(self):
        for obj in self:
            inventories = self.env['stock.inventory'].search([('calculation_id', '=', obj.id)])
            obj.created_inventory_count = len(inventories) if inventories else 0
     
    name = fields.Char('Name')
    product_filter = fields.Selection([('with_stock_move', 'Select Products With Stock Move'),
                                       ('manual', 'Select Products Manually'), ], string='Calculate cost of', default='with_stock_move', required=True)
    calculation_period = fields.Selection([('all_time', 'Calculate All Time'),
                                           ('from_inventory', 'Calculate From Inventory'),
                                           ('from_date', 'Calculate Date From')], string='Cost Calculation Period', default='all_time', required=True)
    calculate_by = fields.Selection([('1', 'Calculate Products by 1'),
                                     ('5', 'Calculate Products by 5'),
                                     ('10', 'Calculate Products by 10'),
                                     ('25', 'Calculate Products by 25'),
                                     ('50', 'Calculate Products by 50'),
                                     ('100', 'Calculate Products by 100'),
                                     ('500', 'Calculate Products by 500'),
                                     ('1000', 'Calculate Products by 1000'),
                                     ('5000', 'Calculate Products by 5000')], string='Calculate Products By', default='100', required=True)
    fix_account = fields.Selection([('fix_only_account_amount', 'Fix Only Account Amount'),
                                    ('fix_account_and_amount', 'Fix Account and Amount'), ], string='Fixing type',
                                    default='fix_only_account_amount', required=True)
    date_from = fields.Date('Date From')
    date_to = fields.Date('Date To')
    description = fields.Text('Description')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    manual_product_ids = fields.Many2many('product.product', 'manual_products_cost_calculation', 'calculcation_id',
                                         'product_id', 'Manual Products for Calculation')
    ready_product_ids = fields.Many2many('product.product', 'ready_products_cost_calculation', 'calculcation_id',
                                         'product_id', 'Ready Products for Calculation')
    inventory_ids = fields.Many2many('stock.inventory', 'inventory_cost_calculation_rel', 'calculcation_id',
                                     'inventory_id', 'Inventories', domain=[('is_initial', '=', True), ('state', '=', 'done')])
    minus_line_ids = fields.One2many('product.cost.calculation.minus.line', "calculation_id", string='Minus Products', readonly=True, copy=False)
    cost_calculated_product_ids = fields.One2many('product.cost.calculation.done', "calculation_id", string='Calculated Products',
                                                  readonly=True, copy=False, domain=[('state', '=', 'stock_move_corrected')])
    account_corrected_product_ids = fields.One2many('product.cost.calculation.done', "calculation_id", string='Calculated Products',
                                                    readonly=True, copy=False, domain=[('state', '=', 'account_move_corrected')])
    created_inventory_count = fields.Integer(compute="_count_created_inventory", string='Created Inventory Count')
    state = fields.Selection([('draft', 'Draft'),
                              ('minus_balance', 'Minus Balance'),
                              ('started', 'Started'),
                              ('cost_corrected', 'Cost Corrected'),
                              ('account_corrected', 'Account Moves are Corrected'), ], string='State', default='draft', required=True)

    @api.multi
    def make_draft(self):
        # Ноорог болгох
        self.write({'state': 'draft'})

    @api.multi
    def get_calc_start_date(self):
        # Тооллогоос тохиолдолд өртөг тооцоолол эхлэх өдрийг олох функц
        calc_date_from = {}
        product_ids = []
        self.env.cr.execute("""SELECT distinct(l.product_id), i.date FROM stock_inventory i, stock_move l 
                                    where i.id=l.inventory_id and l.company_id=%s and i.id in (%s) order by i.date""" % (self.company_id.id, ','.join([str(i) for i in self.inventory_ids.ids])))
        result = self.env.cr.fetchall()
        for res in result:
            product_ids.append(res[0])
            calc_date_from[res[0]] = res[1] #тооллогын огнооноос эхэлж өртөг тооцно
        return calc_date_from

    @api.multi
    def refresh_minus_products(self):
        '''Хасах үлдэгдэлтэй бараагаа засчихаад энэ товчийг дарж засагдсан эсэхийг дахин шалгуулна. 'Хасах үлдэгдэлтэй бараанууд' табан дахь
            бараануудыг дахин шалгаад нэмэх рүү орсон барааг нь энэ жагсаалтнаас хасч, 'Өртөг тооцоход бэлэн' таб руу шилжүүлнэ.
            Хасах үлдэгдэлтэй хэвээр байгаа барааны мөрийн утгыг шинэчилнэ.
        '''
        minus_product_ids = []
        ready_product_ids = []
        for minus in self.minus_line_ids:
            if minus.product_id and minus.product_id.id not in minus_product_ids:
                minus_product_ids.append(minus.product_id.id)
        if minus_product_ids:
            product_ids = self.check_minus_product(minus_product_ids)
            if product_ids:
                for product in minus_product_ids:
                    if product not in product_ids:
                        ready_product_ids.append(product)
                        self.ready_product_ids = [(4, product_id, False) for product_id in ready_product_ids]
            else:
                self.ready_product_ids = [(4, product_id, False) for product_id in minus_product_ids]
                self.write({'state': 'started'})
        return True

    @api.multi
    def create_inventory_minus_products(self):
        '''
            1. Тооллогыг өртөг угаалтын эхлэх огнооноос 1, 1 сараар сарын эцэст хасахтай бараа байгаа эсэхийг шалгаж эцсээрх хасахтай бараануудыг агуулах бүрээр тооллого үүсгэнэ.
                1.1 Өртөг угаалтын эхлэх огноог дараахаар сонгоно:
                    1.1.1 "Хамрах хугацаа" нь "Огнооноос эхэлж тооцоолох" бол өртөг угаалт дээрх ЭХЛЭХ ОГНОО байна.
                    1.1.2 Үгүй бол өртөг угаалт дээрх "Хасах үлдэгдэлтэй бараанууд" дундаас хамгийн эхэнд "Хасах руу орсон огноо" байна.
                Уг огнооноос хойш сар бүрээр шалгаж тооллого үүсгэхийг оролдоно.
            2. Эхний үүсгэсэн тооллогыг заавал дуусгаж байж ТООЛЛОГО ҮҮСГЭХ товчин дээр дарж дараагийн сард дараагийн тооллогуудыг үүсгэнэ.
        '''
        # Өмнөх тооллогуудыг дуусгахгүй бол давхардаж орлого авах магадлалтай.
        undone_inventory_ids = self.env['stock.inventory'].search([('calculation_id', 'in', self.ids), ('state', 'not in', ['cancel', 'done'])])
        if undone_inventory_ids:
            raise UserError(_("Please complete previous inventories first !!!%s") % ("\n\n%s" % ",\n".join(inventory_id.name for inventory_id in undone_inventory_ids)))
        
        products = self.get_calculation_product_ids()
        if not products:
            return False
        
        # Өртөг тооцоолол дээр огноо тавигдаагүй бол тухайн угаалтын хувьд хамгийн эхэнд хасах руу орсон огнооноос өнөөдрийг хүртэлх хасахтай бараагаар тооллого үүсгэхээр шалгах
        if self.calculation_period == 'from_date':
            date_from = self.date_from + " 00:00:00"
            date_to = self.date_to + " 23:59:59"
        else:
            self._cr.execute("SELECT MIN(date) AS date FROM stock_move WHERE product_id IN (%s)" % (",".join(str(product) for product in products)))
            result = self._cr.dictfetchall()
            date_to = datetime.today().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            date_from = result[0]['date'] if result and len(result) > 0 else date_to
                
        # done: Эхний сарын үйлдвэрлэлийг хийгээд цааш үйлдэл хийхгүй байхад ашиглана
        date_from = datetime.strptime(date_from, DEFAULT_SERVER_DATETIME_FORMAT)
        date_to = datetime.strptime(date_to, DEFAULT_SERVER_DATETIME_FORMAT)
        query_dicts, inventory_ids = [], []
        date = fields.Datetime.now()
        uid = self.env.uid
        done = False
        
        while date_from <= date_to and not done:
            # Сарын эцсээр хасах руу орсон бараануудыг шалгана.
            end_date = date_from
            end_date = date_from.replace(day=calendar.monthrange(end_date.year, end_date.month)[1], hour=23, minute=59, second=59)
            wizard_id = self.env['report.stock.minus.balance'].create({
                'company_id': self.company_id.id,
                'date_from': date_from,
                'date_to': end_date,
                'report_type': 'end',
                'product_ids': [(6, 0, products)],
                'warehouse_ids': [(6, 0, self.env.user.allowed_warehouses.ids)] if self.env.user.allowed_warehouses else False 
            })
            minus_lines_by_wh = wizard_id.get_pdf_lines()
            
            # Тухайн хугацаанд хасах руу ороогүй бол дараагийн сарыг шалгана
            if not minus_lines_by_wh:
                date_from = (date_from + relativedelta(months=1)).replace(day=1, hour=0, minute=0, second=0)
                continue
            
            # Агуулах бүр дээр тооллого үүсгэнэ.
            for lines_by_wh in minus_lines_by_wh:
                if not lines_by_wh['minus_lines']:
                    continue
                inv_date = get_display_day_to_user_day(date_from, self.env.user)
                location = self.env['stock.warehouse'].browse(lines_by_wh['wh_id']).sudo().lot_stock_id
                insert_qry = """
                    INSERT INTO stock_inventory (create_uid, create_date, write_uid, write_date, date, accounting_date, location_id, filter, calculation_id, name, state, company_id) 
                    VALUES (%s, '%s', %s, '%s', '%s', '%s', %s, 'partial', %s, '%s', 'confirm', %s) RETURNING id""" % (uid, date, uid, date, 
                                                                                                                       inv_date, inv_date, location.id, self.id, 
                                                                                                                       _('Minus Products Inventory for %s') % (location.name_get()[0][1]), 
                                                                                                                       self.env.user.company_id.id)
                self.env.cr.execute(insert_qry)
                inventory_id = self.env.cr.fetchone()[0]
                inventory_ids.append(inventory_id)
                
                # Тухайн 1 барааны хувьд хасах руу орсон хөдөлгөөн бүрээр өгөгдөлөө авах тул эндээс хамгийн сүүлд хасах руу орсон тоогоор нь stock.inventory.line үүсгэхээр dict бэлдэнэ.
                product_ids = {}
                coun = 1
                for result in lines_by_wh['minus_lines']:
                    product_ids[result['product_id']] = result['product_qty']
                    coun += 1
                for key in product_ids.keys():
                    query_dicts.append({
                        'location_id': location.id,
                        'product_id': key, 
                        'product_qty': product_ids[key] * (-1), 
                        'product_uom_id': self.env['product.product'].browse(key).uom_id.id or 'NULL', 
                        'inventory_id': inventory_id
                    })
                    
            # 1 сард тооллого үүсгээд зогсооно. 
            if inventory_ids:
                done = True
            else:
                date_from = (date_from + relativedelta(months=1)).replace(day=1, hour=0, minute=0, second=0)
            
        # Тооллогын мөрүүдийг бөөнөөр нь insert хийнэ.
        if query_dicts:
            insert_qry = "INSERT INTO stock_inventory_line (create_uid, create_date, write_uid, write_date, location_id, product_id, product_qty, product_uom_id, inventory_id, company_id) VALUES " 
            insert_qry += ", ".join("(%s, '%s', %s, '%s', %s, %s, %s, %s, %s, %s)" % (uid, date, uid, date, qry['location_id'], qry['product_id'], qry['product_qty'], 
                                                                                      qry['product_uom_id'], qry['inventory_id'], self.env.user.company_id.id) for qry in query_dicts)
            self._cr.execute(insert_qry)
        
        if inventory_ids:
            # Тооллогуудын онолын тоог шинэчилнэ.
            inventory_ids = self.env['stock.inventory'].browse(inventory_ids)
            inventory_ids.prepare_inventory()
            inventory_ids.fix_theoretical_qty()

            # Шинэчилсэн онолын тоон дээр хасах руу орсон тоог нэмэгдүүлж тоолно.
            self._cr.execute("""
                UPDATE stock_inventory_line line SET product_qty = product_qty + theoretical_qty
                WHERE line.inventory_id IN (%s)
            """ % (",".join(str(inventory_id.id) for inventory_id in inventory_ids)))

        return True

    @api.multi
    def check_minus_balance(self, product, date_to, locations):
        # Бараа өгөхөд хасах руу эсэхийг шалгаж хасах руу орсон бол өдөр, байрлал, хөдөлгөөнийг буцаана. Хасах руу ороогүй бол ороогүйг мэдэгдэнэ.
        move_obj = self.env['stock.move']
        product_remainder = {}
        if self.calculation_period == 'from_date':
            date_from = get_display_day_to_user_day('%s 00:00:00' % str(self.date_from), self.env.user)
            date_to = get_display_day_to_user_day('%s 23:59:59' % str(date_to), self.env.user)
            initials = move_obj.get_initial_balance(product, self.date_from, locations, self.company_id, 'location')
            for initial in initials:
                qty = initial['qty']
                product_remainder[initial['lid']] = qty
                if qty < (-1 / (10 ** self.env['decimal.precision'].precision_get('Product Unit of Measure'))):
                    return True, initial['lid'], False, qty
            moves = move_obj.search([('product_id', '=', product), ('state', '=', 'done'), ('date', '>=', str(date_from)),
                                     ('date', '<=', str(date_to)), ('company_id', '=', self.company_id.id)], order="date")
        else:
            moves = move_obj.search([('product_id', '=', product), ('state', '=', 'done'), ('company_id', '=', self.company_id.id)], order="date")
            
        # Ижил огноонд хийгдсэн орлого, зарлагын хувьд орлогыг түрүүнд тавьдаг болгов.
        moves = self.env['stock.move'].order_by_type(moves)
        for move in moves:
            product_qty = move.product_qty
            # Хүрэх байрлал нь Дотоод бөгөөд Гарах байрлал нь Худалдан авалт, Тооллого, Үйлдвэрлэл, Нөхөн дүүргэлт болон Борлуулалтын буцаалт байвал орлого гэж үзнэ
            if move.location_id.usage in ('supplier', 'inventory', 'production', 'transit', 'consume', 'customer') and move.location_dest_id.usage == 'internal':
                if move.location_dest_id.id not in product_remainder.keys():
                    product_remainder[move.location_dest_id.id] = product_qty
                else:
                    product_remainder[move.location_dest_id.id] += product_qty
                if product_remainder[move.location_dest_id.id] < 0:
                    return True, move.location_dest_id.id, move, product_remainder[move.location_dest_id.id]
            # Дотоод байрлалаас Дотоод байрлалаас харилцагч, нийлүүлэгч, дамжин өнгөрөх, зарлагын байрлал руу шилжүүлвэл зарлага гэж үзнэ
            elif (move.location_id.usage == 'internal' and move.location_dest_id.usage in ['inventory', 'customer', 'supplier', 'transit', 'consume', 'production']):
                if move.location_id.id not in product_remainder.keys():
                    product_remainder[move.location_id.id] = product_qty * (-1)
                else:
                    product_remainder[move.location_id.id] = product_remainder[move.location_id.id] - product_qty
                if product_remainder[move.location_id.id] < (-1 / (10 ** self.env['decimal.precision'].precision_get('Product Unit of Measure'))):
                    return True, move.location_id.id, move, product_remainder[move.location_id.id]
        return False, False, False, False

    @api.multi
    def check_minus_product(self, product_ids):
        # Хасах үлдэгдэлтэй бараануудыг ХАСАХ ҮЛДЭГДЭЛТЭЙ БАРААНЫ ТАЙЛАНГААС ТООЦООЛНО.
        self.ensure_one()
        self.minus_line_ids = None
        minus_product_ids = []

        if self.calculation_period == 'from_date':
            date_to = self.date_to
            date_from = self.date_from
        else:
            date_to = datetime.today().strftime('%Y-%m-%d')
            self._cr.execute("""
                SELECT MIN(date) AS date FROM stock_move WHERE product_id IN (%s)
            """ % (",".join(str(product_id) for product_id in product_ids)))
            result = self._cr.dictfetchall()
            if result and len(result) > 0:
                date_from = result[0]['date']
            else:
                date_from = date_to

        wizard_id = self.env['report.stock.minus.balance'].create({
            'company_id': self.company_id.id,
            'date_from': date_from,
            'date_to': date_to,
            'report_type': 'middle',
            'product_ids': [(6, 0, product_ids)],
            'warehouse_ids': [(6, 0, self.env.user.allowed_warehouses.ids)] if self.env.user.allowed_warehouses else False 
        })
        minus_lines_by_wh = wizard_id.get_pdf_lines()

        if minus_lines_by_wh:
            insert_qry = """INSERT INTO product_cost_calculation_minus_line (create_date, write_date, create_uid, write_uid, company_id, product_id, minus_date, location_id, minus_qty, calculation_id, user_id, checked_date) VALUES """
            values = []
            uid = self.env.uid
            date = fields.Datetime.now()
            for wh_line in minus_lines_by_wh:
                location_id = self.env['stock.warehouse'].sudo().browse(wh_line['wh_id']).lot_stock_id
                if not (location_id and wh_line['minus_lines']):
                    continue
                for minus_line in wh_line['minus_lines']:
                    values.append("""('%s', '%s', %s, %s, %s, %s, '%s', %s, %s, %s, %s, '%s')""" % (date, date, uid, uid, self.company_id.id, 
                                                                                                    minus_line['product_id'], get_display_day_to_user_day(minus_line['date'], self.env.user),
                                                                                                    location_id.id, minus_line['product_qty'], self.id, uid, date))
                    if minus_line['product_id'] not in minus_product_ids:
                        minus_product_ids.append(minus_line['product_id'])
            if values:
                insert_qry += ", ".join(values)
                self._cr.execute(insert_qry)

        return minus_product_ids

    def filter_product_by_type(self, products):
        # l10n_mn_mrp_product_cost_calculation модульд дахин тодорхойлохоор зарлав.
        # Өртөг угаах бараанд ямар нэг шүүлт тавих бол ашиглах
        self.ensure_one()
        return products
    
    def sort_ready_products(self, product_ids):
        # l10n_mn_mrp_product_cost_calculation модульд дахин тодорхойлохоор зарлав.
        # Өртөг угаах барааны угаах дарааллыг тогтоох функц
        self.ensure_one()
        return product_ids

    def get_calculation_product_ids(self):
        product_ids = []
        where = " WHERE company_id = " + str(self.company_id.id) + " AND state = 'done' "
        if self.calculation_period == 'from_inventory':
            if not self.inventory_ids:
                raise UserError(_("Please select inventories."))
            else:
                where += ' AND inventory_id in  (' + ','.join(map(str, self.inventory_ids.ids)) + ') '
        if self.calculation_period == 'date':
            if not self.date_from:
                raise UserError(_("Please select date from field"))
            if not self.date_to:
                where += " AND date >= '" + self.date_from + "' "
            else:
                where += " AND date BETWEEN '" + self.date_from + "' AND '" + self.date_to + "' "
        if self.product_filter == 'with_stock_move':  # Агуулахад хөдөлгөөн хийгдсэн бараануудыг сонгох
            self.env.cr.execute("SELECT DISTINCT(product_id) FROM stock_move" + where + " ")
            result = self.env.cr.fetchall()
            for res in result:
                product_ids.append(res[0])
            product_ids = self.filter_product_by_type(product_ids)
        else:
            product_ids = self.manual_product_ids.ids
        return product_ids
        
    @api.multi
    def start(self):
        # Эхлэх функц: Өртөг тооцоолоход бэлэн болон хасах үлдэгдэлтэй бараануудыг шууд гаргах
        self.minus_line_ids = None
        self.ready_product_ids = None
        self.cost_calculated_product_ids = None
        self.account_corrected_product_ids = None
        product_ids = self.get_calculation_product_ids()
        if product_ids:
            self.ready_product_ids = product_ids
            delete_product_ids = self.check_minus_product(product_ids)
            if delete_product_ids:
                self.ready_product_ids = [(2, product_id, False) for product_id in delete_product_ids]
                self.write({'state': 'minus_balance'})
            else:
                self.write({'state': 'started'})
        return True

    @api.multi
    def get_initial_balance(self, product, date_from, locations):
        return self.env['stock.move'].get_initial_balance(product, date_from, locations, self.company_id, 'product')

    @api.multi
    def get_moves(self, product, calc_date_from, locations):
        # Тухайн барааны эхний үлдэгдэл болон огнооны хооронд хийгдсэн хөдөлгөөнийг олох
        stock_move_obj = self.env['stock.move']
        initial = []
        if self.calculation_period == 'from_inventory':
            # Хэрэв тооллогоноос хойш тооцох бол
            initial = self.get_initial_balance(product.id, calc_date_from[product.id], locations)
            moves = stock_move_obj.search([('product_id', '=', product.id), ('state', '=', 'done'), ('date', '>=', calc_date_from[product.id]),
                                           ('company_id', '=', self.company_id.id)], order="date")
        elif self.calculation_period == 'from_date':
            # Хэрэв огнооны хооронд бол
            date_from = get_display_day_to_user_day('%s 00:00:00' % str(self.date_from), self.env.user)
            date_to = get_display_day_to_user_day('%s 23:59:59' % str(self.date_to), self.env.user)
            initial = self.get_initial_balance(product.id, self.date_from, locations)
            moves = stock_move_obj.search([('product_id', '=', product.id), ('state', '=', 'done'), ('date', '>=', str(date_from)),
                                           ('date', '<=', str(date_to)), ('company_id', '=', self.company_id.id)], order="date")
        else:
            # Хэрэв бүх хугацаагаар тооцох бол
            moves = stock_move_obj.search([('product_id', '=', product.id), ('state', '=', 'done'), ('company_id', '=', self.company_id.id)], order="date")
        # Ижил огноонд хийгдсэн орлого, зарлагын хувьд орлогыг түрүүнд тавьдаг болгов.
        moves = self.env['stock.move'].order_by_type(moves)
        return initial, moves

    @api.multi
    def get_standard_price(self, new_standard_price, qty, move):
        # Өртгийг тооцоолох
        return (new_standard_price * qty + move.product_qty * move.price_unit) / (qty + move.product_qty) if qty + move.product_qty != 0 else 0

    @api.multi
    def get_transit_price(self, product, move):
        # Нөхөн дүүргэлтийн өртөг тооцоолох
        if move.picking_id and move.picking_id.transit_order_id:
            self.env.cr.execute("SELECT m.id, m.price_unit "
                                "FROM stock_move m "
                                "JOIN stock_picking p ON (m.picking_id = p.id) "
                                "JOIN stock_location l ON (m.location_id = l.id) "
                                "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                                "WHERE l.usage = 'internal' AND l1.usage = 'transit' AND m.state = 'done' AND m.origin_returned_move_id is NULL "
                                        "AND m.product_id = " + str(product.id) + " AND p.transit_order_id = " + str(move.picking_id.transit_order_id.id) + ""
                                "ORDER BY m.id LIMIT 1")
            result = self.env.cr.fetchone()
            if result:
                return result[1]
        return False

    @api.multi
    def create_price_history(self, product, new_standard_price, date):
        # Барааны үнийн түүх үүсгэх
        self.env['product.price.history'].create({'product_id': product.id,
                                                  'price_type': 'standard_price',
                                                  'cost': new_standard_price,
                                                  'company_id': self.company_id.id,
                                                  'product_template_id': product.product_tmpl_id and product.product_tmpl_id.id,
                                                  'datetime': date
                                                  })
        return True

    @api.multi
    def create_done_product(self, product, old_standard_price, new_standard_price, note):
        # Өртөг тооцсон бараанууд үүсгэх
        self.env['product.cost.calculation.done'].create({'product_id': product.id,
                                                          'old_standard_price': old_standard_price,
                                                          'new_standard_price': new_standard_price,
                                                          'calculation_note': note,
                                                          'calculation_id': self.id,
                                                          'state': 'stock_move_corrected',
                                                          })
        return True

    @api.multi
    def calculate_cost(self):
        '''Өртгийг stock move бүрээр дундажлаж тооцно
            Энэ товчийг нэг удаа дараад 'Өртөг тооцоход бэлэн' табнаас барааг 'Нэг удаагийн уншуулах хэмжээ' талбарт заасан тоогоор уншиж,
            уншсан бараануудын өртгийг тооцоолж бүх stock.move-үүд дэх өртгийг зөв болгож, барааны өнөөдрийн өртгийг засна. Амжилттай өртөг
            тооцогдсон бараануудыг 'Өртөг тооцогдсон бараанууд' таб руу шилжүүлнэ.
        '''
        calc_date_from = []
        if self.calculation_period == 'from_inventory':
            calc_date_from = self.get_calc_start_date()

        loc_id = self.env['stock.location'].search([('usage', '=', 'internal'), ('company_id', '=', self.company_id.id)]).ids
        if len(loc_id) > 0:
            locations = ','.join(map(str, loc_id))
        else:
            raise UserError(_('no locations'))

        range = int(self.calculate_by)
        count = 0
        delete_product_ids = []

        # Бараануудад өртөг тооцох эрэмбэ дарааллыг тогтоох
        ready_product_ids = self.sort_ready_products(self.ready_product_ids)
        for product in ready_product_ids:
            if count == range:
                break
            initial, moves = self.get_moves(product, calc_date_from, locations)
            qty = 0
            new_standard_price = 0
            old_standard_price = product.standard_price
            if initial:
                qty = round(initial[0]['qty'], self.env['decimal.precision'].precision_get('Product Unit of Measure'))
                cost = round(initial[0]['cost'], self.env['decimal.precision'].precision_get('Account'))
                if qty > 0:
                    new_standard_price = cost / qty
                    old_standard_price = cost / qty
            note = ''
            if self.calculation_period == 'all_time':
                note += u'Бүх хугацааны хөдөлгөөнийг тооцоолж байна...'
            first_move = True
            for move in moves:
                move_price = move.price_unit
                # Хүрэх байрлал нь Дотоод бөгөөд Гарах байрлал нь Худалдан авалт, Тооллого, Үйлдвэрлэл, Нөхөн дүүргэлт болон Борлуулалтын буцаалт байвал орлого гэж үзнэ
                if move.location_id.usage in ('supplier', 'inventory', 'production', 'transit', 'customer') and move.location_dest_id.usage == 'internal':
                    note += u'\n Орлого. Хөдөлгөөний огноо: %s. Нэр: %s. id: %s. Өртөг: %s. Тоо: %s.' % (move.date, move.name, move.id, move.price_unit, move.product_qty)
                    if move.location_id.usage == 'inventory':
                        if not initial and first_move:
                            new_standard_price = move.price_unit
                            note += u'\n     Эхний үлдэгдлээрх тооллогын орлого тул тооллогын өртгийг эхлэлийн өртөг болгож хадгалав. Эхлэлийн өртөг: %s' % (move.price_unit)
                        else:
                            move.price_unit = new_standard_price if new_standard_price > 0 else 0
                            note += u'\n     Тооллогын орлого. Хөдөлгөөний өртгийг ингэж өөрчлөв: %s --> %s' % (move_price, move.price_unit)
                    elif move.location_id.usage == 'transit':
                        if move.origin_returned_move_id and move.origin_returned_move_id.state == 'done':
                            move.price_unit = move.origin_returned_move_id.price_unit
                            new_standard_price = self.get_standard_price(new_standard_price, qty, move)
                            note += u'\n     Нөх.дүүр-н эсвэл нөх.дүүр буцаалтаарх орлого. Хөдөлгөөний өртгийг ингэж өөрчлөв: %s --> %s' % (move_price, move.price_unit)
                        else:
                            move.price_unit = self.get_transit_price(product, move) or new_standard_price
                            new_standard_price = self.get_standard_price(new_standard_price, qty, move)
                            note += u'\n     Нөх.дүүр-н эсвэл нөх.дүүр орлого. Хөдөлгөөний өртгийг ингэж өөрчлөв: %s --> %s' % (move_price, move.price_unit)
                    elif move.location_id.usage in ('supplier'):
                        new_standard_price = self.get_standard_price(new_standard_price, qty, move)
                        note += u'\n     Худ.авалтын орлого. Хөдөлгөөний өртгийг өөрчлөхгүй. Дундаж өртгийг шинэчлэв: '
                    elif move.location_id.usage in ('production'):
                        new_standard_price = self.get_standard_price(new_standard_price, qty, move)
                        note += u'\n     Үйлдвэрлэлийн орлого. Хөдөлгөөний өртгийг өөрчлөхгүй. Дундаж өртгийг шинэчлэв: '
                    elif move.location_id.usage in ('customer'):
                        # Борлуулалтын буцаалтын бол эх баримтныхаа өртгийг авна. Өртгийг дундажлана.
                        if move.origin_returned_move_id and move.origin_returned_move_id.state == 'done':
                            move.price_unit = move.origin_returned_move_id.price_unit
                            new_standard_price = self.get_standard_price(new_standard_price, qty, move)
                            note += u'\n     Буцаалтаарх орлого. Буцаалтын эх баримтын өртгөөр хөдөлгөөний өртгийг шинэчлэв: %s --> %s. Дундаж өртгийг шинэчлэв: ' % (move_price, move.price_unit)
                        else:
                            # Хэрэв борлуулалтын буцаалтын батлагдсан эх баримт нь олдоогүй бол одоогийн өртгийг авна.
                            move.price_unit = new_standard_price
                            note += u'\n     Буцаалтаарх орлого. Буцаалтын эх баримт олдоогүй тул хөдөлгөөний өртгийг одоогийн өртгөөр шинэчлэв: %s --> %s.' % (move_price, move.price_unit)
                    qty += move.product_uom_qty / move.product_uom.factor * move.product_id.uom_id.factor
                    note += u'\n        Шинэчлэгдсэн тоо: %s' % qty
                    note += u'\n        Шинэчлэгдсэн өртөг: %s' % new_standard_price
                # Дотоод байрлалаас Дотоод байрлалаас харилцагч, нийлүүлэгч, дамжин өнгөрөх, зарлагын байрлал руу шилжүүлвэл зарлага гэж үзнэ
                elif (move.location_id.usage == 'internal' and move.location_dest_id.usage in ['inventory', 'customer', 'supplier', 'transit', 'production']):
                    note += u'\n Зарлага. Хөдөлгөөний огноо: %s. Нэр: %s. id: %s. Өртөг: %s. Тоо: %s.' % ( move.date, move.name, move.id, move.price_unit, move.product_qty)
                    move.price_unit = new_standard_price
                    if move.location_dest_id.usage == 'inventory':
                        note += u'\n     Тооллогоорх зарлага. Хөдөлгөөний өртгийг ингэж өөрчлөв: %s --> %s' % (move_price, move.price_unit)
                    elif move.location_dest_id.usage == 'customer':
                        note += u'\n     Борлуулалтын зарлага. Хөдөлгөөний өртгийг ингэж өөрчлөв: %s --> %s' % (move_price, move.price_unit)
                    elif move.location_dest_id.usage == 'supplier':
                        note += u'\n     Худ.авалтын буцаалтаарх зарлага. Хөдөлгөөний өртгийг ингэж өөрчлөв: %s --> %s' % (move_price, move.price_unit)
                    elif move.location_dest_id.usage == 'transit':
                        note += u'\n     Нөх.дүүр-н эсвэл нөх.дүүр буцаалтаарх зарлага. Хөдөлгөөний өртгийг ингэж өөрчлөв: %s --> %s' % (move_price, move.price_unit)
                    elif move.location_dest_id.usage == 'production':
                        note += u'\n     Үйлдвэрлэлийн буцаалтаарх зарлага. Хөдөлгөөний өртгийг ингэж өөрчлөв: %s --> %s' % (move_price, move.price_unit)
                    qty -= move.product_uom_qty / move.product_uom.factor * move.product_id.uom_id.factor

                    note += u'\n        Шинэчлэгдсэн тоо: %s' % qty
                    note += u'\n        Шинэчлэгдсэн өртөг: %s' % new_standard_price
                first_move = False
                # # Компани дундаа өртөг хөтөлж байгаа үед тооцоолсон шинэ өртгийг үнийн түүхэнд нэмнэ
                # self.create_price_history(product, new_standard_price, move.date)
            if new_standard_price == 0:
                new_standard_price = old_standard_price
            # Компани дундаа өртөг хөтөлж байгаа үед тооцоолсон шинэ өртгийг үнийн түүхэнд нэмнэ
            self.create_done_product(product, old_standard_price, new_standard_price, note)
            delete_product_ids.append(product.id)
            count += 1
            product.standard_price = new_standard_price
        if delete_product_ids:
            self.ready_product_ids = [(2, product_id, False) for product_id in delete_product_ids]
        pass

    def get_moves_for_fix_account(self, line):
        if self.calculation_period == 'from_inventory':
            calc_date_from = self.get_calc_start_date()
            # Хэрэв тооллогоноос хойш тооцох бол
            moves = self.env['stock.move'].search([('product_id', '=', line.product_id.id), ('state', '=', 'done'),
                                           ('date', '>=', calc_date_from[line.product_id.id]), ('company_id', '=', self.company_id.id)], order="date")
        else:
            # Хэрэв бүх хугацаагаар тооцох бол
            moves = self.env['stock.move'].search([('product_id', '=', line.product_id.id), ('state', '=', 'done'), ('company_id', '=', self.company_id.id)], order="date")
        return moves
        
    @api.multi
    def get_wrong_moves(self, limit):
        # Журнал бичилт нь үүсээгүй агуулахын хөдөлгөөнүүдийг олох
        wrong_move_ids = []

        where_qry = ""
        if self.calculation_period == 'from_date':
            # Хэрэв огнооны хооронд бол
            date_from = get_display_day_to_user_day('%s 00:00:00' % str(self.date_from), self.env.user)
            date_to = get_display_day_to_user_day('%s 23:59:59' % str(self.date_to), self.env.user)
            where_qry = " AND m.date BETWEEN '%s' AND '%s' " % (date_from, date_to)

        qry = """
            SELECT m.id
            FROM product_cost_calculation_done c, stock_move m 
            LEFT JOIN account_move_line l ON m.id = l.stock_move_id
            WHERE l.id IS NULL AND m.state = 'done' 
                AND m.company_id = %s AND c.product_id = m.product_id AND m.price_unit != 0
                AND c.id IN (
                    SELECT c1.id 
                    FROM product_cost_calculation_done c1 
                    WHERE c1.calculation_id = %s AND c1.state = 'stock_move_corrected' 
                    order by c1.id 
                    limit %s
                ) %s
        """ % (self.company_id.id, self.id, limit, where_qry)
        self.env.cr.execute(qry)

        fetched = self.env.cr.fetchall()
        
        if len(fetched) > 0:
            for line in fetched:
                wrong_move_ids.append(line[0])

        return wrong_move_ids
    
    @api.multi
    def wrong_move_ids_one_line(self, limit):
        # Сондгой бичилттэй буюу 1 мөр нь л үүссэн журнал бичилттэй агуулахын хөдөлгөөнүүдийг олох /ДТ, КТ-н аль нэг тал нь хийгдээгүй гэсэн үг/
        wrong_move_ids_one_line = []
        
        where_qry = ""
        if self.calculation_period == 'from_date':
            # Хэрэв огнооны хооронд бол
            date_from = get_display_day_to_user_day('%s 00:00:00' % str(self.date_from), self.env.user)
            date_to = get_display_day_to_user_day('%s 23:59:59' % str(self.date_to), self.env.user)
            where_qry = " AND m.date BETWEEN '%s' AND '%s' " % (date_from, date_to)
            
        self.env.cr.execute("""
            SELECT m.id FROM product_cost_calculation_done c, stock_move m 
            LEFT JOIN account_move_line l ON m.id = l.stock_move_id 
            WHERE m.product_id = c.product_id AND l.id IS NOT NULL AND m.state = 'done' AND m.company_id = %s
                AND c.id in (
                    SELECT c1.id 
                    FROM product_cost_calculation_done c1 
                    WHERE c1.calculation_id = %s AND c1.state = 'stock_move_corrected' 
                    ORDER BY c1.id LIMIT %s
                ) %s
            GROUP BY m.id 
            HAVING count(*) = 1
        """ % (self.company_id.id, self.id, limit, where_qry))
        fetched = self.env.cr.fetchall()
        
        if len(fetched) > 0:
            for line in fetched:
                wrong_move_ids_one_line.append(line[0])
                
        return wrong_move_ids_one_line
    
    @api.multi
    def move_ids_more_than_one_line(self, limit):
        # 2-с илүү мөртэй журналын бичилт бүхий агуулахын хөдөлгөөнүүдийг олж буцаана.
        move_ids_more_than_one_line = []
        
        where_qry = ""
        if self.calculation_period == 'from_date':
            # Хэрэв огнооны хооронд бол
            date_from = get_display_day_to_user_day('%s 00:00:00' % str(self.date_from), self.env.user)
            date_to = get_display_day_to_user_day('%s 23:59:59' % str(self.date_to), self.env.user)
            where_qry = " AND m.date BETWEEN '%s' AND '%s' " % (date_from, date_to)
            
        self.env.cr.execute("""
            SELECT m.id FROM product_cost_calculation_done c, stock_move m 
            LEFT JOIN account_move_line l ON m.id = l.stock_move_id 
            WHERE m.product_id = c.product_id AND l.id IS NOT NULL AND m.state = 'done' AND m.company_id = %s
                AND c.id in (
                    SELECT c1.id 
                    FROM product_cost_calculation_done c1 
                    WHERE c1.calculation_id = %s AND c1.state = 'stock_move_corrected' 
                    ORDER BY c1.id LIMIT %s
                ) %s
            GROUP BY m.id 
            HAVING count(*) > 2
        """ % (self.company_id.id, self.id, limit, where_qry))
        fetched = self.env.cr.fetchall()
        move_ids = []
        if len(fetched) > 0:
            for line in fetched:
                move_ids.append(line[0])
                
        return move_ids    
    
    @api.multi
    def get_corrected_stock_moves(self, limit):
        where_qry = ""
        if self.calculation_period == 'from_date':
            # Хэрэв огнооны хооронд бол
            date_from = get_display_day_to_user_day('%s 00:00:00' % str(self.date_from), self.env.user)
            date_to = get_display_day_to_user_day('%s 23:59:59' % str(self.date_to), self.env.user)
            where_qry = " AND m.date BETWEEN '%s' AND '%s' " % (date_from, date_to)
            
        self.env.cr.execute("""
            SELECT c.id, m.id, COALESCE(SUM(m.price_unit * m.product_qty), 0) / COALESCE(COUNT(*), 1), SUM(l.debit), SUM(l.credit) 
            FROM product_cost_calculation_done c, stock_move m 
            LEFT JOIN account_move_line l ON m.id = l.stock_move_id
            WHERE m.product_id = c.product_id AND l.id IS NOT NULL AND m.state = 'done' AND m.company_id = %s
                AND c.id IN (
                    SELECT c1.id FROM product_cost_calculation_done c1 
                    WHERE c1.calculation_id = %s AND c1.state = 'stock_move_corrected' 
                    ORDER BY c1.id LIMIT %s
                ) %s
            GROUP BY m.id, c.id
        """ % (self.company_id.id, self.id, limit, where_qry))
        fetched = self.env.cr.fetchall()
        move_dict = []
        if len(fetched) > 0:
            for line in fetched:
                move_dict.append({'line_id': line[0],
                                   'move_id': line[1],
                                   'correct_amount': line[2],
                                   'old_debit_amount': line[3],
                                   'old_credit_amount': line[4]})
                
        return move_dict
        
    @api.multi
    def fix_acount_moves(self):
        '''Энэ товч нь 'Өртөг тооцогдсон таб' дахь бараануудын барааны хөдөлгөөнүүдийг 'Нэг удаагийн уншуулах хэмжээ' талбарт заасан тоогоор уншиж,
            холбоотой ажил гүйлгээнүүдийг нь зөв дүнгээр засна. Засагдсан мөрүүдийг 'Дансны дүн засагдсан' таб руу шилжүүлнэ.
        '''
        _logger.info("###########COST CALCULATION: FIX ACCOUNT MOVES###########:\tSTARTING...")
        account_obj = self.env['account.account']
        account_move_line_obj = self.env['account.move.line']
        line_obj = self.env['product.cost.calculation.done']

        acc_editables = []
        amount_editables = []

        def get_acc_note(acc_type, account_move, note, new_acc):
            old_acc = account_move.account_id
            if old_acc != new_acc:
                note += u'\n        %s дансыг ингэж өөрчлөв: %s --> %s' % (acc_type, old_acc, new_acc)
                acc_editables.append((account_move, new_acc))
            else:
                note += u'\n        %s данс зөв тул засаагүй: %s' % (acc_type, old_acc)
                
        range = int(self.calculate_by)
        count = 0
        to_zero = []
        
        # Журнал бичилт үүсээгүй хөдөлгөөнүүдийг олоод эхний удаа үүсгэх гэж оролдох, тэгээд ч үүсэхгүй бол raise өгөх
        _logger.info("###########COST CALCULATION: FIX ACCOUNT MOVES###########:\tCHECKING WRONG 'stock.move' WHICH HAVENT'T 'account.move.line'...")
        wrong_move_ids = self.get_wrong_moves(int(self.calculate_by))
        if wrong_move_ids:
            _logger.info("###########TRYING TO CREATE ACCOUNT MOVES IN %s STOCK MOVES." % len(wrong_move_ids))
            wrong_move_ids = self.env['stock.move'].browse(wrong_move_ids)
            for wrong_move_id in wrong_move_ids:
                try:
                    wrong_move_id.create_account_move_line()
                except:  # just ignore
                    continue
            wrong_move_ids = self.get_wrong_moves(int(self.calculate_by))
            _logger.info("###########FINISHED TRYING PROCESS. NOW WE HAVE %s WRONG MOVES WHICH HAVEN'T ACCOUNT LINE." % len(wrong_move_ids or []))
            if wrong_move_ids:
                raise UserError(_("This stock move has no account move line! %s") % wrong_move_ids)
        
        _logger.info("###########COST CALCULATION: FIX ACCOUNT MOVES###########:\tCHECKING WRONG 'stock.move' WHICH HAVE ODD 'account.move.line'...")
        wrong_move_ids_one_line = self.wrong_move_ids_one_line(int(self.calculate_by))
        if wrong_move_ids_one_line:
            raise UserError(_("This stock move has one account move line! %s") % wrong_move_ids_one_line)
        
        _logger.info("###########COST CALCULATION: FIX ACCOUNT MOVES###########:\tCHECKING AND COLLECTING 'account.move.line' OF WRONG 'stock.move' WHICH HAVE MORE THAN 2 'account.move.line'...")
        move_ids_more_than_one_line = self.move_ids_more_than_one_line(int(self.calculate_by))
        if move_ids_more_than_one_line:
            # 1. 2-с олон мөрөөр журнал бичилт үүссэн байвал эхний 2 бичилтээс бусдын ДТ, КТ дүнг 0 болгох / Холбоотой шинжилгээний бичилтийн дүнг 0-лэх
            moves = self.env['stock.move'].browse(move_ids_more_than_one_line)
            for move in moves:
                debit_line = account_move_line_obj.search([('stock_move_id','=',move.id),('debit', '>', 0)], limit=1)
                credit_line = account_move_line_obj.search([('stock_move_id','=',move.id),('credit', '>', 0)], limit=1)
                # ДТ, КТ 2-уулаа 0 бол доорх шалгалт руу орно.
                if not debit_line and not credit_line:
                    aml_ids = account_move_line_obj.search([('stock_move_id','=',move.id)], order='id asc', limit=2)
                    debit_line = aml_ids[0]
                    credit_line = aml_ids[1]
                
                if len(move.account_line_ids) > 2 and debit_line and credit_line:
                    for l in move.account_line_ids:
                        if l.id not in [debit_line.id, credit_line.id]:
                            to_zero.append(l.id)
                '''
                elif move.location_id.usage == 'transit' or move.location_dest_id.usage == 'transit':
                    # Компани дундаа өртөг хөтөлж байгаа үед нөхөн дүүргэлтээс ажил гүйлгээ үүсэхгүй тул дамжин
                    # өнгөрөх байрлалын тохиолдолд ажил гүйлгээ засах функц ажиллах шаардлагагүй.
                    note += u'\n     Нөх.дүүр-н эсвэл нөх.дүүр буцаалтаарх орлого. Дотоод хөдөлгөөн тул журналын бичилт хийгдэхгүй.'
                '''
        # 1. 2-с олон мөрөөр журнал бичилт үүссэн байвал эхний 2 бичилтээс бусдын ДТ, КТ дүнг 0 болгох / Холбоотой шинжилгээний бичилтийн дүнг 0-лэх
        if to_zero:
            _logger.info("###########COST CALCULATION: FIX ACCOUNT MOVES###########:\tUPDATING ALL 'account.move.line'S DEBIT/CREDIT TO ZERO EXCEPT 2...")
            aml_ids = ", ".join(str(aml_id) for aml_id in to_zero)
            update_zero_amount_qry = """
                UPDATE account_move_line SET debit = 0, credit = 0 WHERE id IN (%s);
                UPDATE account_analytic_line SET amount = 0 WHERE move_id IN (%s);
            """ % (aml_ids, aml_ids)
            self._cr.execute(update_zero_amount_qry)
            
        
        _logger.info("###########COST CALCULATION: FIX ACCOUNT MOVES###########:\tCHECKING AND COLLECTING 'account.move.line' FOR CHANGE DEBIT/CREDIT...")
        note = ''
        notes = {}
        move_dict = self.get_corrected_stock_moves(int(self.calculate_by))
        if move_dict:
            for item in move_dict:
                amount_editables.append(item['move_id'])
                if item['line_id'] not in notes.keys():
                    note = ''
                note += u'\n        Move ID: %s' % (item['move_id'])
                note += u'\n        Дебит дүнг ингэж өөрчлөв: %s --> %s' % (item['old_debit_amount'], item['correct_amount'] or 0)
                note += u'\n        Кредит дүнг ингэж өөрчлөв: %s --> %s' % (item['old_credit_amount'], item['correct_amount'] or 0)
                notes[item['line_id']] = note
                    
        # Журналын бичилтийн дүнг солих
        if amount_editables:
            _logger.info("###########COST CALCULATION: FIX ACCOUNT MOVES###########:\tCHANGING 'account.move.line' DEBIT/CREDIT...")
            update_tuple = ", ".join(str(move_id) for move_id in amount_editables)
            update_amount_qry = """
                UPDATE account_move_line aml SET debit = sm.price_unit * sm.product_uom_qty
                FROM stock_move sm
                WHERE sm.id = aml.stock_move_id AND sm.id IN (%s) AND aml.debit > 0;
                
                UPDATE account_move_line aml SET credit = sm.price_unit * sm.product_uom_qty
                FROM stock_move sm
                WHERE sm.id = aml.stock_move_id AND sm.id IN (%s) AND aml.credit > 0;
            """ % (update_tuple, update_tuple)
            self._cr.execute(update_amount_qry)
            
            if self.env.get('account.analytic.share', False) == False or not self.env.user.company_id.show_analytic_share:
                 # TODO: analytic_amount_currency-н утгыг оноох хэсгийг хийгээгүй байгаа.
                _logger.info("###########COST CALCULATION: FIX ACCOUNT MOVES###########:\tCHANGING 'account.analytic.line's AMOUNT...")
                update_analytic_acc_amnt_qry = """
                    UPDATE account_analytic_line aal SET amount = CASE WHEN (aml.credit - aml.debit) IS NULL THEN 0 else (aml.credit - aml.debit) END
                    FROM account_move_line aml
                    WHERE aml.id = aal.move_id AND aml.stock_move_id IN (%s);
                """ % (update_tuple)
                self._cr.execute(update_analytic_acc_amnt_qry)
            else:
                _logger.info("###########COST CALCULATION: FIX ACCOUNT MOVES###########:\tCHANGING 'account.analytic.line's AMOUNT...")
                # TODO: Шинжилгээний тархалттай үеийн шинжилгээний дүнг засах хэсгийн кодыг энд хийнэ үү.
                update_analytic_acc_amnt_qry = """
                    UPDATE account_analytic_line aal SET amount = (aml.credit - aml.debit) * aas.rate / 100
                    FROM account_move_line aml, account_analytic_share aas
                    WHERE aml.id = aal.move_id AND aas.move_line_id = aml.id AND aas.analytic_line_id = aal.id AND aml.stock_move_id IN (%s);
                """ % (update_tuple)
                self._cr.execute(update_analytic_acc_amnt_qry)
            
        #Данс засах сонголттой үед note-г notes dic рүү зөв бичдэг болгож дараа нь засна
        note = ''
        if self.fix_account == 'fix_account_and_amount':
            _logger.info("###########COST CALCULATION: FIX ACCOUNT MOVES###########:\tCOLLECTING 'account.move.line' FOR FIX ACCOUNT...")
            for line in self.cost_calculated_product_ids:
                if count == range:
                    break
                moves = self.get_moves_for_fix_account(line)
                for move in moves:
                    # Данс засах тохиргоотойгоор өртөг тооцоолж байгаа бол
                    if len(move.account_line_ids) < 2:
                        continue
                    
                    journal_id, acc_src, acc_dest, acc_valuation = move._get_accounting_data_for_valuation()
                    
                    debit_line = account_move_line_obj.search([('stock_move_id','=',move.id), ('debit', '>', 0)], limit=1)
                    credit_line = account_move_line_obj.search([('stock_move_id','=',move.id), ('credit', '>', 0)], limit=1)
                    # ДТ, КТ 2-уулаа 0 бол доорх шалгалт руу орно.
                    if not debit_line and not credit_line:
                        aml_ids = account_move_line_obj.search([('stock_move_id','=',move.id)], order='id asc', limit=2)
                        debit_line = aml_ids[0]
                        credit_line = aml_ids[1]
                    
                    if len(move.account_line_ids) > 2 and debit_line and credit_line:
                        for l in move.account_line_ids:
                            if l.id not in [debit_line.id, credit_line.id]:
                                to_zero.append(l.id)
    
                    # Орлого бол
                    if move.location_id.usage in ('supplier', 'inventory', 'production', 'customer') and move.location_dest_id.usage == 'internal':
                        note += u'\n Орлого. Хөдөлгөөний огноо: %s. Нэр: %s. id: %s. Өртөг: %s. Тоо: %s.' % (move.date, move.name, move.id, move.price_unit, move.product_qty)
    
                        if credit_line.credit == 0 and credit_line.account_id.id == account_obj.browse(acc_valuation).id:
                            # Хэрэв Дт, Кт 2-уулаа 0 дүнтэй бөгөөд автоматаар эхнийхийг нь дебит гэж хадгалсан байсан бол энд дансыг нь шалгаж үзээд хэрэгтэй бол солино.
                            # Кт талд БМ данс сонгогдсон бол Кт мөр, Дт мөр 2-ын байрыг сольж байна.
                            l = credit_line
                            debit_line = credit_line
                            credit_line = l
    
                        if move.location_id.usage == 'inventory':
                            note += u'\n     Тооллогын орлого:'
    
                            # Дебит дансыг 'Бараа материалын данс'-р солино
                            get_acc_note(u'Дебит', debit_line, note, account_obj.browse(acc_valuation))

                            # Кредит данс нь Эхний үлдэгдлийн тооллого эсэхээс хамаарна. Хэрэв Эхний үлдэгдлийн тооллого бол компани
                            # дээр тохируулсан Эхний үлдэгдлийн тохируулгын дансаар засна. Үгүй бол бараа/барааны ангилал дээрх 'Бараа материалын орлогын данс'-р засна.
                            if move.inventory_id and move.inventory_id.is_initial:
                                # Эхний үлдэгдлийн тооллого бол
                                if not self.company_id.initial_config_account_id:
                                    raise UserError(_("Please configure 'Initial Balance Configuration Account' of the company!"))
                                get_acc_note(u'Кредит', credit_line, note, self.company_id.initial_config_account_id)
                            else:
                                # Эхний үлдэгдлийн биш тооллого бол бараа/барааны ангилал дээр тохируулсан 'Бараа материалын орлогын данс'-р солино. (Замд яваа бараа)
                                get_acc_note(u'Кредит', credit_line, note, account_obj.browse(acc_src))
                                    
                        elif move.location_id.usage in ('supplier'):
                            note += u'\n     Худ.авалтын орлого: '
                            # Дебит дансыг 'Бараа материалын данс'-р солино
                            get_acc_note(u'Дебит', debit_line, note, account_obj.browse(acc_valuation))
                            # Кредит дансыг 'Бараа материалын орлогын данс'-р солино. (Замд яваа бараа)
                            get_acc_note(u'Кредит', credit_line, note, account_obj.browse(acc_src))
    
                        elif move.location_id.usage in ('production'):
                            note += u'\n     Үйлдвэрлэлийн орлого'
    
                            # Дебит дансыг 'Бараа материалын данс'-р солино
                            get_acc_note(u'Дебит', debit_line, note, account_obj.browse(acc_valuation))

                            # Кредит дансыг үйлдвэрлэлийн байрлал дээр тохируулсан 'Бараа материалын зарлагын данс (Зарлага)' дансаар солино. (Дуусаагүй үйлдвэрлэл)
                            if not move.location_id.valuation_out_account_id.id:
                                raise UserError(_("Please configure 'Stock Valuation Account (outgoing)' of production location. Product: [%s] %s, Move name: %s" % (line.product_id.default_code, line.product_id.name, move.name)))
                            get_acc_note(u'Кредит', credit_line, note, move.location_id.valuation_out_account_id)
    
                        elif move.location_id.usage in ('customer'):
                            note += u'\n     Буцаалтаарх орлого: '
                            # Дебит дансыг 'Бараа материалын данс'-р солино
                            get_acc_note(u'Дебит', debit_line, note, account_obj.browse(acc_valuation))
                            # Кредит дансыг 'Бараа материалын зарлагын данс'-р солино. (ББӨ)
                            get_acc_note(u'Кредит', credit_line, note, account_obj.browse(acc_dest))
    
                    # Дотоод байрлалаас Дотоод байрлалаас харилцагч, нийлүүлэгч, дамжин өнгөрөх, зарлагын байрлал руу шилжүүлвэл зарлага гэж үзнэ
                    elif (move.location_id.usage == 'internal' and move.location_dest_id.usage in ['inventory', 'customer', 'supplier', 'production']):
                        note += u'\n Зарлага. Хөдөлгөөний огноо: %s. Нэр: %s. id: %s. Өртөг: %s. Тоо: %s.' % (move.date, move.name, move.id, move.price_unit, move.product_qty)
    
                        if debit_line.debit == 0 and debit_line.account_id.id == account_obj.browse(acc_valuation).id:
                            # Хэрэв Дт, Кт 2-уулаа 0 дүнтэй бөгөөд автоматаар эхнийхийг нь дебит гэж хадгалсан байсан бол энд дансыг нь шалгаж үзээд хэрэгтэй бол солино.
                            # Кт талд БМ данс сонгогдсон бол Кт мөр, Дт мөр 2-ын байрыг сольж байна.
                            l = credit_line
                            debit_line = credit_line
                            credit_line = l
    
                        if move.location_dest_id.usage == 'inventory':
                            note += u'\n     Тооллогоорх зарлага: '
                            
                            # Кредит дансыг 'Бараа материалын данс'-р солино
                            get_acc_note(u'Кредит', credit_line, note, account_obj.browse(acc_valuation))
                            
                            # Дебит данс нь Эхний үлдэгдлийн тооллого эсэхээс хамаарна. Хэрэв Эхний үлдэгдлийн тооллого бол компани
                            # дээр тохируулсан Эхний үлдэгдлийн тохируулгын дансаар засна. Үгүй бол бараа/барааны ангилал дээрх 'Бараа зарлагын орлогын данс'-р засна. (ББӨ)
                            if move.inventory_id and move.inventory_id.is_initial:
                                # Эхний үлдэгдлийн тооллого бол
                                if not self.company_id.initial_config_account_id:
                                    raise UserError(_("Please configure 'Initial Balance Configuration Account' of the company!"))

                                get_acc_note(u'Дебит', debit_line, note, self.company_id.initial_config_account_id)
                            else:
                                # Эхний үлдэгдлийн биш тооллого бол бараа/барааны ангилал дээр тохируулсан 'Бараа материалын зарлагын данс'-р солино. (ББӨ)
                                get_acc_note(u'Дебит', debit_line, note, account_obj.browse(acc_dest))
    
                        elif move.location_dest_id.usage == 'customer':
                            note += u'\n     Борлуулалтын зарлага: '
                            # Кредит дансыг 'Бараа материалын данс'-р солино
                            get_acc_note(u'Кредит', credit_line, note, account_obj.browse(acc_valuation))
                            # Дебит даныг ангилал дээр тохируулсан 'Бараа материалын зарлагын данс'-р солино. (ББӨ)
                            get_acc_note(u'Дебит', debit_line, note, account_obj.browse(acc_dest))
    
                        elif move.location_dest_id.usage == 'supplier':
                            note += u'\n     Худ.авалтын буцаалтаарх зарлага: '
                            # Кредит дансыг 'Бараа материалын данс'-р солино
                            get_acc_note(u'Кредит', credit_line, note, account_obj.browse(acc_valuation))
                            # Дебит даныг ангилал дээр тохируулсан 'Бараа материалын орлогын данс'-р солино. (Замд яваа бараа)
                            get_acc_note(u'Дебит', debit_line, note, account_obj.browse(acc_src))
                            
                        elif move.location_dest_id.usage == 'production':
                            note += u'\n     Үйлдвэрлэлийн буцаалтаарх зарлага: '
                            # Кредит дансыг 'Бараа материалын данс'-р солино
                            get_acc_note(u'Кредит', credit_line, note, account_obj.browse(acc_valuation))
                            
                            if not move.location_dest_id.valuation_in_account_id.id:
                                raise UserError(_("Please configure 'Stock Valuation Account (incoming)' of production location. Product: [%s] %s, Move name: %s" % (line.product_id.default_code, line.product_id.name, move.name)))
                            
                            # Дебит дансыг үйлдвэрлэлийн байрлал дээр тохируулсан 'Бараа материалын данс (Зарлага)'-р солино. (Дуусаагүй үйлдвэрлэл)
                            get_acc_note(u'Дебит', debit_line, note, move.location_dest_id.valuation_in_account_id)

                count += 1
                
            # 1. 2-с олон мөрөөр журнал бичилт үүссэн байвал эхний 2 бичилтээс бусдын ДТ, КТ дүнг 0 болгох / Холбоотой шинжилгээний бичилтийн дүнг 0-лэх
            if to_zero:
                _logger.info("###########COST CALCULATION: FIX ACCOUNT MOVES###########:\tUPDATING ALL 'account.move.line'S DEBIT/CREDIT TO ZERO EXCEPT 2...")
                aml_ids = ", ".join(str(aml_id) for aml_id in to_zero)
                update_zero_amount_qry = """
                    UPDATE account_move_line SET debit = 0, credit = 0 WHERE id IN (%s);
                    UPDATE account_analytic_line SET amount = 0 WHERE move_id IN (%s);
                """ % (aml_ids, aml_ids)
                self._cr.execute(update_zero_amount_qry)

            # Журналын бичилт/шинжилгээний бичилтүүдийн дансыг солих
            if acc_editables:
                _logger.info("###########COST CALCULATION: FIX ACCOUNT MOVES###########:\tUPDATING 'account.move.line'S ACCOUNT...")
                update_tuple = ", ".join("(%s, %s)" % (acc_editable[0].id, acc_editable[1].id) for acc_editable in acc_editables)
                update_acc_qry = """
                    UPDATE account_move_line SET account_id = aml.account_id
                    FROM (VALUES %s) AS aml(id, account_id)
                    WHERE aml.id = account_move_line.id
                """ % (update_tuple)
                self._cr.execute(update_acc_qry)
                
                _logger.info("###########COST CALCULATION: FIX ACCOUNT MOVES###########:\tUPDATING 'account.analytic.line' ACCOUNT...")
                update_analytic_acc_qry = """
                    UPDATE account_analytic_line aal SET general_account_id = aml.account_id
                    FROM (VALUES %s) AS aml(id, account_id)
                    WHERE aml.id = aal.move_id AND aal.general_account_id != aml.account_id
                """ % (update_tuple)
                self._cr.execute(update_analytic_acc_qry)
                    
        for line_id, note in notes.iteritems():
            line_obj.browse(line_id).write({'state':'account_move_corrected', 'account_fix_note': note})
                    
        _logger.info("###########COST CALCULATION: FIX ACCOUNT MOVES###########:\tDONE")
        return wrong_move_ids      


class ProductCostCalculationMinusLine(models.Model):
    _name = 'product.cost.calculation.minus.line'
    _description = 'Product With Minus Balance'
    
    product_id = fields.Many2one('product.product', required=True, string='Product')
    location_id = fields.Many2one('stock.location', required=True, string='Stock Location')
    stock_move_id = fields.Many2one('stock.move', string='Stock Move') # Ижил огноонд 2-с дээш удаа хасах руу орсон бол 1 л мөрөөр харуулдаг болсон тул уг талбарыг view-с авсан.
    minus_qty = fields.Float('Minus Quantity', digits=dp.get_precision('Product Unit of Measure'), required=True, readonly=True)
    minus_date = fields.Datetime('Minus Date', readonly=True)
    calculation_id = fields.Many2one('product.cost.calculation', string='Product Cost Calculation')
    user_id = fields.Many2one('res.users', required=True, string='Checked User', default=lambda self: self.env.user)
    checked_date = fields.Date('Checked Date', required=True, readonly=True, default=fields.Date.context_today)
    initial_balance = fields.Boolean('Initial Balance', readonly=True)
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    

class ProductCostCalculationDone(models.Model):
    _name = 'product.cost.calculation.done'
    _description = 'Calculated Products'
    
    product_id = fields.Many2one('product.product', required=True, string='Product')
    old_standard_price = fields.Float('Old Standard Price', required=True)
    new_standard_price = fields.Float('New Standard Price', required=True)
    calculation_note = fields.Text('Cost Calculation Note')
    account_fix_note = fields.Text('Account Move Fix Note')
    user_id = fields.Many2one('res.users', required=True, string='Checked User', default=lambda self: self.env.user)
    calculated_date = fields.Datetime('Calculated Date', required=True, readonly=True, default=fields.Date.context_today)
    calculation_id = fields.Many2one('product.cost.calculation', string='Product Cost Calculation')
    state = fields.Selection([('stock_move_corrected', 'Stock Move Corrected'),
                               ('account_move_corrected', 'Account Move Correct'),
                               ], string='State', default='stock_move_corrected', required=True)