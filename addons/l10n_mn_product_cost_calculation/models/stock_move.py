# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class StockMove(models.Model):
    _inherit = 'stock.move'
    
    @api.model
    def order_by_type(self, moves):
        # Ижил огноонд хийгдсэн орлого, зарлагын хувьд орлогыг түрүүнд тавьдаг болгов.
        if not moves:
            return moves

        qry = """
            SELECT sm.id
            FROM stock_move sm
            LEFT JOIN stock_location dest_loc ON dest_loc.id = sm.location_dest_id
            WHERE sm.id IN (%s)
            ORDER BY sm.date, COALESCE(CASE WHEN dest_loc.usage = 'internal' THEN 'input' ELSE 'output' END, '')
        """ % (", ".join(str(move.id) for move in moves))
        self._cr.execute(qry)
        ids = [result_tuple[0] for result_tuple in self._cr.fetchall()]
        
        moves = self.env['stock.move'].browse(ids)
        return moves