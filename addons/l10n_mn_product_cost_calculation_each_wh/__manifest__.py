# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Product Cost Calculation. Cost Calculation for Each Warehouse",
    'version': '1.0',
    'depends': ['l10n_mn_product_cost_calculation', 'l10n_mn_stock_account_cost_for_each_wh'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Product Cost Calculation
    """,
    'data': [
        'views/product_cost_calculation_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}