# -*- coding: utf-8 -*-
from io import BytesIO
import base64
import time
import xlsxwriter
import logging
from tempfile import NamedTemporaryFile
import xlwt, xlrd

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from odoo.addons.l10n_mn_web.models.time_helper import *

_logger = logging.getLogger(__name__)

class ProductCostCalculationDone(models.Model):
    _inherit = 'product.cost.calculation.done'

    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse')

class ProductCostCalculation(models.Model):
    _inherit = 'product.cost.calculation'
        
    @api.multi
    def get_calc_start_date(self):
        #Аль барааг хэднээс эхэлж тооцох харгалзааг dictionary-д эхлээд авч байна
        calc_date_from = {}
        product_ids = []
        self.env.cr.execute("""SELECT distinct(l.product_id), i.date FROM stock_inventory i, stock_move l 
                                    where i.id=l.inventory_id and l.company_id=%s and i.id in (%s)""" % (self.company_id.id, ','.join([str(i) for i in self.inventory_ids.ids])))
        result = self.env.cr.fetchall()
        for res in result:
            product_ids.append(res[0])
            calc_date_from[res[0]] = res[1] #тооллогын огнооноос эхэлж өртөг тооцно
        #Аль барааг хэднээс эхэлж тооцох харгалзааг dictionary-д эхлээд авч байна
        return calc_date_from

    @api.multi
    def get_initial_balance(self, product, date_from, locations):
        return self.env['stock.move'].get_initial_balance(product, date_from, locations, self.company_id, 'location')

    @api.multi
    def get_qty(self, qty, move, location, sign):
        # Тухайн байрлалын тоо хэмжээг тооцоолох
        if location.id in qty.keys():
            qty[location.id] += sign * move.product_uom_qty / move.product_uom.factor * move.product_id.uom_id.factor
        else:
            qty[location.id] = sign * move.product_uom_qty / move.product_uom.factor * move.product_id.uom_id.factor
        return qty[location.id]

    @api.multi
    def get_standard_price(self, new_standard_price, qty, move):
        # Өртгийг тооцоолох
        product_qty = 0
        if move.location_dest_id.id in qty.keys():
            product_qty = qty[move.location_dest_id.id]
        if move.location_dest_id.id in new_standard_price.keys():
            new_standard_price[move.location_dest_id.id] = (new_standard_price[move.location_dest_id.id] * product_qty + move.product_qty * move.price_unit) / \
                                                           (product_qty + move.product_qty) if product_qty + move.product_qty != 0 else 0
        else:
            new_standard_price[move.location_dest_id.id] = move.price_unit
        return new_standard_price[move.location_dest_id.id]

    def get_standard_price_of_expenditure(self, move):
        # Өртөг угаалтаарх тухайн байрлал дээрх эхний хөдөлгөөн байгаад тухайн хөдөлгөөн нь зарлагын хөдөлгөөн байвал уг функцаас өмнөх өртгөө авна
        # Өртөг угаалтаас дуудах тул өртөг угаалтын огнооны дундуур хасах үлдэгдэлгүй байна гэж үзнэ. Мөн огнооноос өмнөх өртгүүдийг засагдсан өртөг гэж үзнэ.
        # Хүрэх байрлал нь Дотоод бөгөөд Гарах байрлал нь Худалдан авалт, Тооллого, Үйлдвэрлэл, Нөхөн дүүргэлт болон Борлуулалтын буцаалт байвал орлого гэж үзнэ
        self._cr.execute("""
            SELECT CASE WHEN t.total_qty > 0 THEN t.total_cost / t.total_qty ELSE 0 END AS price_unit
            FROM
            (
                SELECT COALESCE(SUM(sm.product_uom_qty * sm.price_unit), 0) AS total_cost, COALESCE(SUM(sm.product_uom_qty), 0) AS total_qty
                FROM stock_move sm
                LEFT JOIN stock_location l ON l.id = sm.location_id
                WHERE sm.product_id = %s AND sm.location_dest_id = %s AND sm.date < '%s'
                      AND l.usage IN ('supplier', 'inventory', 'production', 'transit', 'customer')
            ) AS t
        """ % (move.product_id.id, move.location_id.id, move.date))
        standard_price = self._cr.fetchall()[0][0]
        return standard_price

    @api.multi
    def get_wrong_moves(self, limit):
        # Журнал бичилт нь үүсээгүй агуулахын хөдөлгөөнүүдийг олох
        wrong_move_ids = []

        where_qry = ""
        if self.calculation_period == 'from_date':
            # Хэрэв огнооны хооронд бол
            date_from = get_display_day_to_user_day('%s 00:00:00' % str(self.date_from), self.env.user)
            date_to = get_display_day_to_user_day('%s 23:59:59' % str(self.date_to), self.env.user)
            where_qry = " AND m.date BETWEEN '%s' AND '%s' " % (date_from, date_to)

        qry = """
            SELECT m.id
            FROM product_cost_calculation_done c, stock_warehouse wh, stock_move m 
            LEFT JOIN account_move_line l ON m.id = l.stock_move_id
            WHERE l.id IS NULL AND m.state = 'done' 
                AND m.company_id = %s AND c.product_id = m.product_id AND m.price_unit != 0
                AND wh.id = c.warehouse_id AND (wh.lot_stock_id = m.location_id OR wh.lot_stock_id = m.location_dest_id)
                AND c.id IN (
                    SELECT c1.id 
                    FROM product_cost_calculation_done c1 
                    WHERE c1.calculation_id = %s AND c1.state = 'stock_move_corrected' 
                    order by c1.id 
                    limit %s
                ) %s
        """ % (self.company_id.id, self.id, limit, where_qry)
        self.env.cr.execute(qry)
        
        fetched = self.env.cr.fetchall()
        
        if len(fetched) > 0:
            for line in fetched:
                wrong_move_ids.append(line[0])
        return wrong_move_ids
    
    @api.multi
    def wrong_move_ids_one_line(self, limit):
        # Сондгой бичилттэй буюу 1 мөр нь л үүссэн журнал бичилттэй агуулахын хөдөлгөөнүүдийг олох /ДТ, КТ-н аль нэг тал нь хийгдээгүй гэсэн үг/
        wrong_move_ids_one_line = []
        
        where_qry = ""
        if self.calculation_period == 'from_date':
            # Хэрэв огнооны хооронд бол
            date_from = get_display_day_to_user_day('%s 00:00:00' % str(self.date_from), self.env.user)
            date_to = get_display_day_to_user_day('%s 23:59:59' % str(self.date_to), self.env.user)
            where_qry = " AND m.date BETWEEN '%s' AND '%s' " % (date_from, date_to)
            
        self.env.cr.execute("""
            SELECT m.id FROM product_cost_calculation_done c, stock_warehouse wh, stock_move m
            LEFT JOIN account_move_line l ON m.id = l.stock_move_id 
            WHERE m.product_id = c.product_id AND l.id IS NOT NULL AND m.state = 'done' AND m.company_id = %s
                AND wh.id = c.warehouse_id AND (wh.lot_stock_id = m.location_id OR wh.lot_stock_id = m.location_dest_id)
                AND c.id in (
                    SELECT c1.id 
                    FROM product_cost_calculation_done c1 
                    WHERE c1.calculation_id = %s AND c1.state = 'stock_move_corrected' 
                    ORDER BY c1.id LIMIT %s
                ) %s
            GROUP BY m.id 
            HAVING count(*) = 1
        """ % (self.company_id.id, self.id, limit, where_qry))
        fetched = self.env.cr.fetchall()
        
        if len(fetched) > 0:
            for line in fetched:
                wrong_move_ids_one_line.append(line[0])
        return wrong_move_ids_one_line
    
    @api.multi
    def move_ids_more_than_one_line(self, limit):
        # 2-с илүү мөртэй журналын бичилт бүхий агуулахын хөдөлгөөнүүдийг олж буцаана.
        move_ids_more_than_one_line = []
        
        where_qry = ""
        if self.calculation_period == 'from_date':
            # Хэрэв огнооны хооронд бол
            date_from = get_display_day_to_user_day('%s 00:00:00' % str(self.date_from), self.env.user)
            date_to = get_display_day_to_user_day('%s 23:59:59' % str(self.date_to), self.env.user)
            where_qry = " AND m.date BETWEEN '%s' AND '%s' " % (date_from, date_to)
            
        self.env.cr.execute("""
            SELECT m.id FROM product_cost_calculation_done c, stock_warehouse wh, stock_move m
            LEFT JOIN account_move_line l ON m.id = l.stock_move_id 
            WHERE m.product_id = c.product_id AND l.id IS NOT NULL AND m.state = 'done' AND m.company_id = %s
                AND wh.id = c.warehouse_id AND (wh.lot_stock_id = m.location_id OR wh.lot_stock_id = m.location_dest_id)
                AND c.id in (
                    SELECT c1.id 
                    FROM product_cost_calculation_done c1 
                    WHERE c1.calculation_id = %s AND c1.state = 'stock_move_corrected' 
                    ORDER BY c1.id LIMIT %s
                ) %s
            GROUP BY m.id 
            HAVING count(*) > 2
        """ % (self.company_id.id, self.id, limit, where_qry))
        fetched = self.env.cr.fetchall()
        move_ids = []
        if len(fetched) > 0:
            for line in fetched:
                move_ids.append(line[0])
        return move_ids    
    
    @api.multi
    def get_corrected_stock_moves(self, limit):
        where_qry = ""
        if self.calculation_period == 'from_date':
            # Хэрэв огнооны хооронд бол
            date_from = get_display_day_to_user_day('%s 00:00:00' % str(self.date_from), self.env.user)
            date_to = get_display_day_to_user_day('%s 23:59:59' % str(self.date_to), self.env.user)
            where_qry = " AND m.date BETWEEN '%s' AND '%s' " % (date_from, date_to)
            
        self.env.cr.execute("""
            SELECT c.id, m.id, COALESCE(SUM(m.price_unit * m.product_qty), 0) / COALESCE(COUNT(*), 1), SUM(l.debit), SUM(l.credit) 
            FROM product_cost_calculation_done c, stock_warehouse wh, stock_move m
            LEFT JOIN account_move_line l ON m.id = l.stock_move_id
            WHERE m.product_id = c.product_id AND l.id IS NOT NULL AND m.state = 'done' AND m.company_id = %s
                AND wh.id = c.warehouse_id AND (wh.lot_stock_id = m.location_id OR wh.lot_stock_id = m.location_dest_id)
                AND c.id IN (
                    SELECT c1.id FROM product_cost_calculation_done c1 
                    WHERE c1.calculation_id = %s AND c1.state = 'stock_move_corrected' 
                    ORDER BY c1.id LIMIT %s
                ) %s
            GROUP BY m.id, c.id
        """ % (self.company_id.id, self.id, limit, where_qry))
        fetched = self.env.cr.fetchall()
        move_dict = []
        if len(fetched) > 0:
            for line in fetched:
                move_dict.append({'line_id': line[0],
                                   'move_id': line[1],
                                   'correct_amount': line[2],
                                   'old_debit_amount': line[3],
                                   'old_credit_amount': line[4]})
        return move_dict
    
    def get_note(self, location_id, note, msg):
        # Тэмдэглэл тооцоолол
        if location_id in note.keys():
            note[location_id] += msg
        else:
            note[location_id] = msg
        return note
            
    def calculate_cost(self):
        '''Өртгийг stock move бүрээр дундажлаж тооцно
            Энэ товчийг нэг удаа дараад 'Өртөг тооцоход бэлэн' табнаас барааг 'Нэг удаагийн уншуулах хэмжээ' талбарт заасан тоогоор уншиж,
            уншсан бараануудын өртгийг тооцоолж бүх stock.move-үүд дэх өртгийг зөв болгож, барааны өнөөдрийн өртгийг засна. Амжилттай өртөг
            тооцогдсон бараануудыг 'Өртөг тооцогдсон бараанууд' таб руу шилжүүлнэ.
        '''
        wh_standard_price_obj = self.env['product.warehouse.standard.price']
        done_products_obj = self.env['product.cost.calculation.done']
        calc_date_from = []
        if self.calculation_period == 'from_inventory':
            calc_date_from = self.get_calc_start_date()

        loc_id = self.env['stock.location'].search([('usage', '=', 'internal'), ('company_id', '=', self.company_id.id)]).ids
        if len(loc_id) > 0:
            locations = ','.join(map(str, loc_id))
        else:
            raise UserError(_('no locations'))
        warehouse_dic = {}
        warehouses = self.env['stock.warehouse'].search([('company_id', '=', self.company_id.id)])
        for wh in warehouses:
            if wh.lot_stock_id:
                warehouse_dic[wh.lot_stock_id.id] = wh

        range = int(self.calculate_by)
        count = 0
        delete_product_ids = []

        # Бараануудад өртөг тооцох эрэмбэ дарааллыг тогтоох
        ready_product_ids = self.sort_ready_products(self.ready_product_ids)
        total = len(ready_product_ids)
        for product in ready_product_ids:
            if count == range:
                break
            first_move = {}
            loc_id = self.env['stock.location'].search([('usage', '=', 'internal'), ('company_id', '=', self.company_id.id)]).ids
            for l in loc_id:
                first_move[l] = True
            new_standard_price = {}
            new_standard_price_without_initial = {}
            qty = {}
            note = {}
            # Энэхүү бараан агуулах бүр дээрх өртгийн объектыг хадгалж авч үлдэж байна
            _logger.info(u'\n Өртөг тооцоолол %s/%s. Бараа %s %s. ' % (count + 1, total, product.barcode, product.name))
            wh_prices = wh_standard_price_obj.search([('company_id', '=', self.company_id.id), ('product_id', '=', product.id)])
            location_price = {}
            for price in wh_prices:
                location_price[price.warehouse_id.lot_stock_id.id] = price.standard_price
            initials, moves = self.get_moves(product, calc_date_from, locations)
            for initial in initials:
                init_qty = round(initial['qty'], self.env['decimal.precision'].precision_get('Account'))
                cost = round(initial['cost'], self.env['decimal.precision'].precision_get('Account'))
                if init_qty > 0:
                    new_standard_price[initial['lid']] = cost / init_qty
                    location_price[initial['lid']] = cost / init_qty
                    qty[initial['lid']] = init_qty
                    note = self.get_note(initial['lid'], note, u'\n     Эхний үлдэгдлээрх өртөг: %s' % (new_standard_price[initial['lid']]))
                    note = self.get_note(initial['lid'], note, u'\n     Эхний үлдэгдлээрх тоо ширхэг: %s' % (init_qty))
            for move in moves:
                move_price = move.price_unit
                # Хүрэх байрлал нь Дотоод бөгөөд Гарах байрлал нь Худалдан авалт, Тооллого, Үйлдвэрлэл, Нөхөн дүүргэлт болон Борлуулалтын буцаалт байвал орлого гэж үзнэ
                if move.location_id.usage in ('supplier', 'inventory', 'production', 'transit', 'customer') and move.location_dest_id.usage == 'internal':
                    note = self.get_note(move.location_dest_id.id, note, u'\n\n Орлого. Хөдөлгөөний огноо: %s. Нэр: %s. id: %s. Өртөг: %s. Тоо: %s.'
                                         % (move.date, move.name, move.id, move.price_unit, move.product_qty))
                    if move.location_id.usage == 'inventory':
                        if not move.location_dest_id.id in qty.keys() and first_move[move.location_dest_id.id]:
                            new_standard_price[move.location_dest_id.id] = move.price_unit
                            new_standard_price_without_initial[move.location_dest_id.id] = move.price_unit
                            note = self.get_note(move.location_dest_id.id, note,
                                                 u'\n     Эхний үлдэгдлээрх тооллогын орлого тул тооллогын өртгийг эхлэлийн өртөг болгож хадгалав. Эхлэлийн өртөг: %s' % (move.price_unit))
                        else:
                            # Өртөг угаалтаарх тухайн байрлал дээрх эхний хөдөлгөөн байгаад new_standard_price-д өртөг нь оноогдоогүй бол тухайн хөдөлгөөний өртгийг эхний өртөг гэж үзэх
                            if first_move[move.location_dest_id.id] and move.location_dest_id.id not in new_standard_price.keys():
                                new_standard_price[move.location_dest_id.id] = move.price_unit
                                new_standard_price_without_initial[move.location_dest_id.id] = move.price_unit
                            move.price_unit = new_standard_price[move.location_dest_id.id] if move.location_dest_id.id in new_standard_price.keys() else 0
                            note = self.get_note(move.location_dest_id.id, note, u'\n     Тооллогын орлого. Хөдөлгөөний өртгийг ингэж өөрчлөв: %s --> %s' % (move_price, move.price_unit))
                    elif move.location_id.usage == 'transit':
                        if move.origin_returned_move_id and move.origin_returned_move_id.state == 'done':
                            move.price_unit = move.origin_returned_move_id.price_unit
                            standard_price = self.get_standard_price(new_standard_price, qty, move)
                            new_standard_price[move.location_dest_id.id] = standard_price
                            new_standard_price_without_initial[move.location_dest_id.id] = standard_price
                            note = self.get_note(move.location_dest_id.id, note,
                                                 u'\n     Нөх.дүүр-н эсвэл нөх.дүүр буцаалтаарх орлого. Хөдөлгөөний өртгийг ингэж өөрчлөв: %s --> %s' % (move_price, move.price_unit))
                        else:
                            move.price_unit = self.get_transit_price(product, move) or (new_standard_price[move.location_dest_id.id] if move.location_dest_id.id in new_standard_price.keys() else 0) or 0
                            standard_price = self.get_standard_price(new_standard_price, qty, move)
                            new_standard_price[move.location_dest_id.id] = standard_price
                            new_standard_price_without_initial[move.location_dest_id.id] = standard_price
                            note = self.get_note(move.location_dest_id.id, note, u'\n     Нөх.дүүр-н эсвэл нөх.дүүр орлого. Хөдөлгөөний өртгийг ингэж өөрчлөв: %s --> %s' % (move_price, move.price_unit))
                    elif move.location_id.usage in ('supplier'):
                        standard_price = self.get_standard_price(new_standard_price, qty, move)
                        new_standard_price[move.location_dest_id.id] = standard_price
                        new_standard_price_without_initial[move.location_dest_id.id] = standard_price
                        note = self.get_note(move.location_dest_id.id, note, u'\n     Худ.авалтын орлого. Хөдөлгөөний өртгийг өөрчлөхгүй. Дундаж өртгийг шинэчлэв:')
                    elif move.location_id.usage in ('production'):
                        standard_price = self.get_standard_price(new_standard_price, qty, move)
                        new_standard_price[move.location_dest_id.id] = standard_price
                        new_standard_price_without_initial[move.location_dest_id.id] = standard_price
                        note = self.get_note(move.location_dest_id.id, note,  u'\n     Үйлдвэрлэлийн орлого. Хөдөлгөөний өртгийг өөрчлөхгүй. Дундаж өртгийг шинэчлэв:')
                    elif move.location_id.usage in ('customer'):
                        # Борлуулалтын буцаалтын бол эх баримтныхаа өртгийг авна. Өртгийг дундажлана.
                        if move.origin_returned_move_id and move.origin_returned_move_id.state == 'done':
                            move.price_unit = move.origin_returned_move_id.price_unit
                            standard_price = self.get_standard_price(new_standard_price, qty, move)
                            new_standard_price[move.location_dest_id.id] = standard_price
                            new_standard_price_without_initial[move.location_dest_id.id] = standard_price
                            note = self.get_note(move.location_dest_id.id, note,
                                                 u'\n     Буцаалтаарх орлого. Буцаалтын эх баримтын өртгөөр хөдөлгөөний өртгийг шинэчлэв: %s --> %s. Дундаж өртгийг шинэчлэв: ' % (move_price, move.price_unit))
                        else:
                            # Хэрэв борлуулалтын буцаалтын батлагдсан эх баримт нь олдоогүй бол одоогийн өртгийг авна.
                            move.price_unit = new_standard_price[move.location_dest_id.id] if move.location_dest_id.id in new_standard_price.keys() else 0
                            note = self.get_note(move.location_dest_id.id, note,
                                                 u'\n     Буцаалтаарх орлого. Буцаалтын эх баримт олдоогүй тул хөдөлгөөний өртгийг одоогийн өртгөөр шинэчлэв: %s --> %s. Дундаж өртгийг шинэчлэв: ' % (
                                                 move_price, move.price_unit))
                    qty[move.location_dest_id.id] = self.get_qty(qty, move, move.location_dest_id, 1)
                    note = self.get_note(move.location_dest_id.id, note, u'\n        Шинэчлэгдсэн тоо: %s' % qty[move.location_dest_id.id])
                    note = self.get_note(move.location_dest_id.id, note, u'\n        Шинэчлэгдсэн өртөг: %s' % new_standard_price[move.location_dest_id.id])
                    first_move[move.location_dest_id.id] = False
                # Дотоод байрлалаас Дотоод байрлалаас харилцагч, нийлүүлэгч, дамжин өнгөрөх, зарлагын байрлал руу шилжүүлвэл зарлага гэж үзнэ
                elif (move.location_id.usage == 'internal' and move.location_dest_id.usage in ['inventory', 'customer', 'supplier', 'transit', 'production']):
                    note = self.get_note(move.location_id.id, note, u'\n\n Зарлага. Хөдөлгөөний огноо: %s. Нэр: %s. id: %s. Өртөг: %s. Тоо: %s.'
                                         % (move.date, move.name, move.id, move.price_unit, move.product_qty))
                    # Өртөг угаалтаарх тухайн байрлал дээрх эхний хөдөлгөөн байгаад тухайн хөдөлгөөн нь зарлагын хөдөлгөөн байвал угааж байгаа огнооноос өмнөх дундаж өртгийг тооцоолж оноох
                    if first_move[move.location_id.id]:
                        standard_price = self.get_standard_price_of_expenditure(move)
                        new_standard_price[move.location_id.id] = standard_price
                        new_standard_price_without_initial[move.location_id.id] = standard_price
                        move.price_unit = standard_price
                    else:
                        move.price_unit = new_standard_price[move.location_id.id] if move.location_id.id in new_standard_price.keys() else 0
                    if move.location_dest_id.usage == 'inventory':
                        note = self.get_note(move.location_id.id, note, u'\n     Тооллогоорх зарлага. Хөдөлгөөний өртгийг ингэж өөрчлөв: %s --> %s' % (move_price, move.price_unit))
                    elif move.location_dest_id.usage == 'customer':
                        note = self.get_note(move.location_id.id, note, u'\n     Борлуулалтын зарлага. Хөдөлгөөний өртгийг ингэж өөрчлөв: %s --> %s' % (move_price, move.price_unit))
                    elif move.location_dest_id.usage == 'supplier':
                        note = self.get_note(move.location_id.id, note, u'\n     Худ.авалтын буцаалтаарх зарлага. Хөдөлгөөний өртгийг ингэж өөрчлөв: %s --> %s' % (move_price, move.price_unit))
                    elif move.location_dest_id.usage == 'transit':
                        note = self.get_note(move.location_id.id, note, u'\n     Нөх.дүүр-н эсвэл нөх.дүүр буцаалтаарх зарлага. Хөдөлгөөний өртгийг ингэж өөрчлөв: %s --> %s'
                                             % (move_price, move.price_unit))
                    elif move.location_dest_id.usage == 'production':
                        note = self.get_note(move.location_id.id, note, u'\n     Үйлдвэрлэлийн буцаалтаарх зарлага. Хөдөлгөөний өртгийг ингэж өөрчлөв: %s --> %s'
                                             % (move_price, move.price_unit))
                    if first_move[move.location_id.id]:
                        note = self.get_note(move.location_id.id, note, u'\n        Шинэчлэгдсэн өртөг: %s' % new_standard_price[move.location_id.id])
                    qty[move.location_id.id] = self.get_qty(qty, move, move.location_id, -1)
                    note = self.get_note(move.location_id.id, note, u'\n        Шинэчлэгдсэн тоо: %s' % qty[move.location_id.id])
                    first_move[move.location_id.id] = False
            # Энэ бараа агуулах бүрийн өртгийг шинээр бодсон өртгөөр шинэчилж байна.
            new_standard_price_tuple = new_standard_price.iteritems() if self.calculation_period == 'all_time' else new_standard_price_without_initial.iteritems()
            for loc_id, new_price in new_standard_price_tuple:
                if loc_id not in warehouse_dic.keys():
                    raise UserError(_("This location has no warehouse. Location ID: %s" % (loc_id)))
                standard_price = wh_standard_price_obj.search([('product_id', '=', product.id), ('warehouse_id', '=', warehouse_dic[loc_id].id)], limit=1)
                if standard_price:
                    standard_price[0].standard_price = new_standard_price[loc_id]
                else:
                    wh_standard_price_obj.create({'product_id': product.id,
                                                  'warehouse_id': warehouse_dic[loc_id].id,
                                                  'standard_price': new_standard_price[loc_id],
                                                  'initial_standard_price': 0,
                                                  })
                # Өртөг тооцогдсон барааны жагсаалт руу нэмж байна.
                done_products_obj.create({'product_id': product.id,
                                          'warehouse_id': warehouse_dic[loc_id].id,
                                          'old_standard_price': location_price[loc_id] if loc_id in location_price.keys() else 0,
                                          'new_standard_price': new_price,
                                          'calculation_note': note[loc_id],
                                          'calculation_id': self.id,
                                          'state': 'stock_move_corrected',
                                          })
            delete_product_ids.append(product.id)
            count += 1
        if delete_product_ids:
            self.ready_product_ids = [(2, product_id, False) for product_id in delete_product_ids]
        pass

    def get_moves_for_fix_account(self, line):
        stock_move_obj = self.env['stock.move']
        if self.calculation_period == 'from_inventory':
            calc_date_from = self.get_calc_start_date()
            # Хэрэв тооллогоноос хойш тооцох бол
            moves = stock_move_obj.search([('product_id', '=', line.product_id.id), ('state', '=', 'done'),('date', '>=', calc_date_from[line.product_id.id]),
                                           ('company_id', '=', self.company_id.id)], order="date")
        elif self.calculation_period == 'from_date':
            # Хэрэв хугацааны интервалаар тооцох бол
            start_date = str(get_display_day_to_user_day(datetime.strptime(self.date_from + " 00:00:00", DEFAULT_SERVER_DATETIME_FORMAT), self.env.user))
            end_date = str(get_display_day_to_user_day(datetime.strptime(self.date_to + " 23:59:59", DEFAULT_SERVER_DATETIME_FORMAT), self.env.user)) if self.date_to else False
            if self.date_to:
                moves = stock_move_obj.search([('product_id', '=', line.product_id.id), ('state', '=', 'done'), ('date', '>=', start_date), ('date', '<=', end_date),
                                               ('company_id', '=', self.company_id.id), '|', ('location_id', '=', line.warehouse_id.lot_stock_id.id),
                                               ('location_dest_id', '=', line.warehouse_id.lot_stock_id.id)], order="date")
            else:
                moves = stock_move_obj.search([('product_id', '=', line.product_id.id), ('state', '=', 'done'), ('date', '>=', start_date), ('company_id', '=', self.company_id.id),
                                               '|', ('location_id', '=', line.warehouse_id.lot_stock_id.id), ('location_dest_id', '=', line.warehouse_id.lot_stock_id.id)], order="date")
        else:
            # Хэрэв бүх хугацаагаар тооцох бол
            moves = stock_move_obj.search([('product_id', '=', line.product_id.id), ('state', '=', 'done'), ('company_id', '=', self.company_id.id),
                                           '|', ('location_id', '=', line.warehouse_id.lot_stock_id.id), ('location_dest_id', '=', line.warehouse_id.lot_stock_id.id)], order="date")

        return moves

    @api.multi
    def fix_acount_moves(self):
        '''Энэ товч нь 'Өртөг тооцогдсон таб' дахь бараануудын барааны хөдөлгөөнүүдийг 'Нэг удаагийн уншуулах хэмжээ' талбарт заасан тоогоор уншиж,
            холбоотой ажил гүйлгээнүүдийг нь зөв дүнгээр засна. Засагдсан мөрүүдийг 'Дансны дүн засагдсан' таб руу шилжүүлнэ.
        '''
        wrong_move_ids = super(ProductCostCalculation, self).fix_acount_moves()
        if wrong_move_ids:
            self.export_report(wrong_move_ids)
        return wrong_move_ids
    
    @api.multi
    def export_report(self, records):
        # create workbook

        output = BytesIO()
        #         book = xlsxwriter.Workbook(output)
        book = xlwt.Workbook(encoding="utf-8")

        # FORMAT
        header_text = {
            'font_name': 'Times New Roman',
            'font_size': 10,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter',
            'border': 1,
            'text_wrap': 1,
            'bg_color': '#83CAFF'
        }
        sub_cat_text = {
            'font_name': 'Times New Roman',
            'font_size': 10,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter',
            'border': 1,
            'text_wrap': 1,
            'bg_color': '#CFE7F5'
        }
        # Улаан текст
        red_text = {
            'font_name': 'Times New Roman',
            'font_size': 10,
            'font_color': '#ff4000',
            'align': 'right',
            'valign': 'vcenter',
            'border': 1,
            'num_format': '#,##0.00'
        }
        sub_cat_text_float = {
            'font_name': 'Times New Roman',
            'font_size': 10,
            'bold': True,
            'align': 'right',
            'valign': 'vcenter',
            'border': 1,
            'bg_color': '#CFE7F5',
            'num_format': '#,##0.00'
        }
        # create name
        report_name = _('Worng Move Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        # create formats
        #         format_name = book.add_format(ReportExcelCellStyles.format_name)
        #         format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        #         format_filter_right = book.add_format(ReportExcelCellStyles.format_filter_right)
        #         format_title = book.add_format(sub_cat_text)
        #
        #         format_title_float = book.add_format(ReportExcelCellStyles.format_title_float)
        #         format_group = book.add_format(header_text)
        #         format_group_left = book.add_format(ReportExcelCellStyles.format_group_left)
        #         format_group_right = book.add_format(ReportExcelCellStyles.format_group_right)
        #         format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        #         format_content_center = book.add_format(ReportExcelCellStyles.format_content_center)
        #         format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        #         format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        #         format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        #         format_content_left = book.add_format(ReportExcelCellStyles.format_content_left)
        #         format_content_bold_number = book.add_format(ReportExcelCellStyles.format_content_bold_number)
        #         format_red_text = book.add_format(red_text)
        #         format_sub_text_float = book.add_format(sub_cat_text_float)
        # Хэрэглэх хувьсагчид
        seq = 1
        rowx = 0
        counter = 0

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(
            filename_prefix=('worng_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_sheet("Sheet")
        #         sheet.set_portrait()
        #         # sheet.set_page_view()
        #         sheet.set_paper(9)  # A4
        #         sheet.set_margins(0.39, 0.39, 0.39, 0.39)  # 1cm, 1cm, 1cm, 1cm
        #         sheet.fit_to_pages(1, 0)
        #         sheet.set_footer('&C&"Times New Roman"&10&P', {'margin': 0.1})

        # compute column
        #         sheet.set_column('A:A', 4)
        #         sheet.set_row(2, 20)
        #         sheet.set_row(5, 30)
        #         sheet.set_row(6, 30)

        # create name
        sheet.write(rowx, 0, _(u'Company name: %s') % self.company_id.name)

        # Толгой хэсэг
        #         self._fill_header(rowx, sheet)
        rowx += 2

        # get data
        #         records = self._get_data()
        sheet.write(rowx, 0, _('№'))
        sheet.write(rowx, 1, _('Location complect name'))
        sheet.write(rowx, 2, _('Product code'))
        sheet.write(rowx, 3, _('Product name'))
        sheet.write(rowx, 4, _('Origin'))

        # Тайлан өрөлт
        for record in records:
            rowx += 1
            sheet.write(rowx, 0, '%s' % (seq))
            sheet.write(rowx, 1, record['location_name'] or '')
            sheet.write(rowx, 2, record['code'] or '')
            sheet.write(rowx, 3, record['product_name'] or '')
            sheet.write(rowx, 4, record['origin'] or '')
            seq += 1

        rowx += 3
        # END OF THE REPORT
        sheet.write(rowx, 2, _(u'Made by ……………………………./                         /'))
        rowx += 2

        sheet.write(rowx, 2, _(u'Check by ……………………………./                        /'))

        book.save("aldaatai.xls")
        #         book.close()
        # set file data
        #         report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        mod_obj = self.env['ir.model.data']
        form_res = mod_obj.get_object_reference('l10n_mn_report', 'report_excel_output_view_form')
        form_id = form_res and form_res[1] or False
        # call export function
        return {
            'name': 'Worg Report',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'oderp.report.excel.output',
            'res_id': self.id,
            'views': [(form_id, 'form')],
            'context': self.env.context,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }


    def _fill_header(self, rowx, sheet):
        colx = 0
        sheet.write_merge(rowx, colx, rowx + 1, colx, _('№'))
        colx += 1
        sheet.write_merge(rowx, colx, rowx + 1, colx, _('Location complect name'))
        colx += 1
        sheet.write_merge(rowx, colx, rowx + 1, colx, _('Product code'))
        colx += 1
        sheet.write_merge(rowx, colx, rowx + 1, colx, _('Product name'))
        colx += 1
        sheet.write_merge(rowx, colx, rowx + 1, colx, _('Origin'))
        colx += 1