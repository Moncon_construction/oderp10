# -*- coding: utf-8 -*-
{
    'name': "Product Expense",

    'summary': """Mongolian Product Expense""",

    'description': """ Expense products """,

    'author': "Asterisk Technologies LLC",
    'website': "http://www.asterisk-tech.mn",

    'category': 'Product Expense',
    'version': '0.1',

    'depends': [
        'l10n_mn_stock',
        'l10n_mn_hr',
        'l10n_mn_account',
        'l10n_mn_workflow_config'
    ],

    'data': [
        'data/mail_templates.xml',
        'data/expense_quantity_data_view.xml',
        'view/expense_view.xml',
        'view/stock_warehouse_view.xml',
        'view/stock_config_settings_view.xml',
        'security/ir.model.access.csv',
        'data/product_expense_data_view.xml',
        'report/product_expense_report_views_main.xml',
        'report/product_expense_templates.xml'

    ]
}
