# -*- coding: utf-8 -*-

import sys
from datetime import datetime

import pytz

from odoo import _, api, exceptions, fields, models
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

reload(sys)
sys.setdefaultencoding('utf-8')


class ProductExpense(models.Model):
    _name = 'product.expense'
    _inherit = ['mail.thread']
    _rec_name = 'name'
    _order = 'date DESC'

    STATE_SELECTION = [('draft', 'Draft'),
                       ('waiting_approve', 'Waiting for approve'),
                       ('approved', 'Approved'),
                       ('returned', 'Returned'),
                       ('done', 'Done'),
                       ('refused', 'Refused'),
                       ('cancel', 'Cancelled'), ]

    def _default_employee(self):
        employee = self.env['hr.employee'].search([('user_id', '=', self._uid)])
        if len(employee) > 1:
            employee = employee[0]
        if not employee:
            raise exceptions.AccessError(_("Can't find any related employee for your user. Only employee can create bill of expense."))
        return employee.id

    def _compute_is_stock_manager(self):
        if self.env.user.has_group('stock.group_stock_manager'):
            self.is_stock_manager = True
        else:
            self.is_stock_manager = False

    @api.multi
    def _default_warehouse_id(self):
        default_warehouse = self.env.user.allowed_warehouses.ids
        warehouse_ids = self.env['stock.warehouse'].search([('id', 'in', default_warehouse), ('company_id', 'child_of', [self.env.user.company_id.id])], limit=1)
        return warehouse_ids

    def _domain_warehouses(self):
        return [
            ('id', 'in', self.env.user.allowed_warehouses.ids),
            '|', ('company_id', '=', False), ('company_id', 'child_of', [self.env.user.company_id.id])
        ]

    product = fields.Many2one('product.product', 'Product', related='expense_line.product')
    req_analytic_account = fields.Boolean(related='account.req_analytic_account', readonly=True)
    warehouse = fields.Many2one('stock.warehouse', 'Warehouse', required=True, default=_default_warehouse_id, domain=_domain_warehouses, track_visibility='onchange')
    department = fields.Many2one('hr.department', 'Department', required=True, track_visibility='onchange')
    employee = fields.Many2one('hr.employee', 'Employee', default=_default_employee, required=True, track_visibility='onchange')
    company = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id, required=True, track_visibility='onchange')
    cost_center = fields.Selection(related='company.cost_center', readonly=True, string='Cost center')
    date = fields.Date(string='Date', default=datetime.today(), track_visibility='onchange')
    account = fields.Many2one('account.account', 'Account', track_visibility='onchange')
    analytic_account = fields.Many2one('account.analytic.account', 'Analytic account', track_visibility='onchange')
    name = fields.Text('Note', required=True)
    state = fields.Selection(STATE_SELECTION, default='draft', track_visibility='onchange')
    expense_line = fields.One2many('product.expense.line', 'expense', 'Expense line',
                                   states={'approved': [('readonly', True)],
                                           'done': [('readonly', True)]}, copy=True)
    workflow_id = fields.Many2one('workflow.config', 'Workflow', copy=False)
    check_sequence = fields.Integer('Workflow Step', default=0, copy=False)
    history_lines = fields.One2many('product.expense.workflow.history', 'history', 'Workflow History', copy=False)
    is_validator = fields.Boolean(compute='_compute_is_validator')
    is_creator = fields.Boolean(compute='_compute_is_creator')
    count_out_stock_picking = fields.Integer(compute='_count_out_stock_picking')
    stock_picking = fields.One2many('stock.picking', 'expense', 'Stock picking')
    code = fields.Char('Expense Code', copy=False)
    must_choose_account = fields.Boolean(compute='_compute_must_choose_account')
    stock_picking_type = fields.Many2one('stock.picking.type', string="Location", domain="[('code', '=', 'outgoing'),('warehouse_id','=',warehouse)]", required=True, track_visibility='onchange')
    is_stock_manager = fields.Boolean(compute='_compute_is_stock_manager')
    total_amount = fields.Float('Total Amount', compute='_compute_total_amount')

    @api.onchange('department')
    def onchange_department(self):
        if self.department:
            employees = self.env['hr.employee'].search([('department_id', 'child_of', self.department.id)])
            if self.employee not in employees:
                self.employee = False
        else:
            employees = self.env['hr.employee'].search([])
        return {
            'domain': {
                'employee': [('id', 'in', employees.ids)]
            }
        }

    @api.onchange('employee')
    def onchange_employee(self):
        if self.employee:
            self.department = self.employee.department_id

    @api.onchange('account')
    def _onchange_account(self):
        lines_to_change = self.env['product.expense.line']
        for line in self.expense_line:
            if not line.account or self._origin.account.id == line.account.id:
                lines_to_change |= line
        lines_to_change.write({'account': self.account.id})

    @api.onchange('analytic_account')
    def _onchange_analytic_account(self):
        # Шаардахын дансыг соливол мөрүүдийн дансыг сонгогдсон эсэхээс үл хамааран шаардахын дансаар сольдог болгов
        for obj in self:
            for line in obj.expense_line:
                line.analytic_account = obj.analytic_account

    @api.depends('expense_line')
    def _compute_total_amount(self):
        for obj in self:
            for line in obj.expense_line:
                obj.total_amount += line.price_subtotal

    @api.onchange('warehouse')
    def onchange_warehouse(self):
        # filter stock picking
        if self.warehouse:
            for obj in self:
                stock_picking_type = self.env['stock.picking.type'].search(
                    [('warehouse_id', '=', obj.warehouse.id), ('code', '=', 'outgoing')], limit=1)
                if stock_picking_type:
                    obj.stock_picking_type = stock_picking_type.id
                if obj.warehouse._fields.get('stock_account_output_id'):
                    obj.account = obj.warehouse.stock_account_output_id

        self.account = self.warehouse.expense_account_id

        # create warehouse domain
        domain = {}
        _warehouses = []
        for warehouse in self.env.user.allowed_warehouses:
            _warehouses.append(warehouse.id)
        if _warehouses:
            domain['warehouse'] = [('id', 'in', _warehouses)]
        return {'domain': domain}

    @api.onchange('cost_center', 'warehouse')
    def set_analytic_account(self):
        for obj in self:
            if obj.cost_center == 'warehouse':
                obj.analytic_account = obj.warehouse.analytic_account_id
                for line in obj.expense_line:
                    line.analytic_account = obj.warehouse.analytic_account_id
            elif obj.cost_center in ('department', 'sales_team', 'project', 'technic', 'contract', 'brand', 'product_categ'):
                obj.analytic_account = False
                for line in obj.expense_line:
                    line.analytic_account = False

    @api.multi
    def _compute_must_choose_account(self):
        if self.check_sequence == len(self.workflow_id.line_ids):
            self.must_choose_account = True
        else:
            self.must_choose_account = False

    def _count_out_stock_picking(self):
        stock_picking = self.env['stock.picking'].search([('expense', '=', self.id)])
        self.count_out_stock_picking = len(stock_picking)
    @api.multi
    def action_view_expense(self):
        action = self.env.ref('stock.action_picking_tree')
        result = action.read()[0]

        result.pop('id', None)
        result['context'] = {}
        pick_ids = self.env['stock.picking'].search([('expense', '=', self.id)]).ids
        if len(pick_ids) > 1 or len(pick_ids) == 0:
            result['domain'] = "[('id','in',[" + ','.join(map(str, pick_ids)) + "])]"
        elif len(pick_ids) == 1:
            res = self.env.ref('stock.view_picking_form', False)
            result['views'] = [(res and res.id or False, 'form')]
            result['res_id'] = pick_ids and pick_ids[0] or False
        return result

    @api.depends('check_sequence')
    def _compute_is_validator(self):
        for rec in self:
            history_obj = self.env['product.expense.workflow.history']
            validators = history_obj.search([('history', '=', rec.id), ('line_sequence', '=', rec.check_sequence)], limit=1, order='sent_date DESC').user_ids
            if self.env.user in validators:
                rec.is_validator = True
            else:
                rec.is_validator = False

    @api.multi
    def _compute_is_creator(self):
        for rec in self:
            if rec.create_uid == self.env.user:
                rec.is_creator = True
            else:
                rec.is_creator = False

    @api.multi
    def unlink(self):
        model_obj = self.env['ir.model.data']
        stock_manager_group = model_obj.get_object('stock', 'group_stock_manager')
        stock_manager = self.env['res.users'].search([('groups_id', 'in', stock_manager_group.id)])
        for line in self:
            if line.state == 'draft' or line.state == 'refused':
                if self.env.user not in stock_manager:
                    if self.env.user == line.create_uid:
                        super(ProductExpense, line).unlink()
                    else:
                        raise UserError(_('Product expense delete only own!'))
                else:
                    super(ProductExpense, line).unlink()
            else:
                raise UserError(_('Product expense delete only draft state!'))

    def is_module_installed(self, module_name):
        self._cr.execute("SELECT id FROM ir_module_module WHERE name = '%s' AND state IN ('installed', 'to upgrade')" % module_name)
        results = self._cr.dictfetchall()

        if results and len(results) > 0:
            return True
        else:
            return False

    @api.multi
    def cancel(self):
        for expense in self:
            for pick in expense.stock_picking:
                if pick.state in ('done'):
                    raise UserError(
                        _('Cannot cancel product expense !\n Because picking state is Done of reception attached to this product expense.'))
                else:
                    pick.action_cancel()
                    pick.unlink()
        self.state = 'cancel'

    @api.multi
    def draft(self):
        self.ensure_one()
        self.check_sequence = 0
        self.state = 'draft'
        self.workflow_id = ""

    @api.multi
    def send(self):
        """    Ажлын урсгалыг 1. Шаардахын агуулахаас хайна
                              2. Агуулахад тохируулаагүй тохиолдолд Агуулахын тохиргооны Шаардахын ажлын урсгалаас хайна
                              3. Агуулахын тохиргоонд тохируулаагүй тохиолдолд Ажлын урсгалын бүртгэлээс ажилтны хэлтсээр хайна.
        """
        for expense in self:
            workflow = ''
            if expense.warehouse and expense.warehouse.workflow:
                workflow = expense.warehouse.workflow.id
            elif expense.env.user.company_id.workflow:
                workflow = self.env.user.company_id.workflow.id
            else:
                workflow = self.env['workflow.config'].get_workflow('employee', 'product.expense', expense.employee.id, None)
            if not workflow:
                raise exceptions.Warning(_('There is no workflow defined!'))
        
            expense.workflow_id = workflow
            if workflow:
                # Шаардахын мөр байгаа эсэхийн шалгаж байна
                if expense.expense_line:
                    success, current_sequence = self.env['workflow.config'].send('product.expense.workflow.history',
                                                                                 'history', expense, expense.create_uid.id)
                    if success:
                        expense.check_sequence = current_sequence
                        expense.state = 'waiting_approve'
                        expense.code = self.env['ir.sequence'].get('product.expense')
                else:
                    raise UserError(_('Product expense line is not created'))

    @api.multi
    def check_quanitity(self, lines):
        product_name = []
        for line in lines:
            if line.available_qty < line.quantity:
                product_name.append('[' + line.product.default_code if line.product.default_code else ' ' + '] ' + str(line.product.name))
        return product_name

    @api.model
    def _check_analytic_account(self):
        for line in self.expense_line:
            if line.account and line.account.req_analytic_account:
                if line.analytic_account:
                    continue
                else:
                    raise UserError(_('Сонгосон данс шинжилгээний данс шаардах данс тул шинжилгээний дансыг мөрөн дээр сонгож өгнө үү'))

    @api.one
    def approve(self):
        # Данс сонгосон тохиолдолд тухайн данс нь Шинжилгээний данс шаардах данс бол Шинжилгээний данс сонгохыг шаардана
        self._check_analytic_account()
        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve('product.expense.workflow'
                                                                                         '.history', 'history', self,
                                                                                         self.env.user.id)
            if success:
                if sub_success:
                    check_list = self.check_quanitity(self.expense_line)
                    if len(check_list) > 0:
                        raise UserError(_(u'Not enough quantity!. Product %s') % ', '.join(check_list))
                    else:
                        for line in self.expense_line:
                            if not line.quantity:
                                raise UserError(_('Шаардахын мөрүүдын тоо хэмжээ 0 байна'))
                        self.create_out_picking()
                        self.state = 'approved'
                else:
                    self.check_sequence = current_sequence

    @api.multi
    def refuse(self):
        if self.workflow_id:
            success = self.env['workflow.config'].reject('product.expense.workflow.history', 'history', self, self.env.user.id)
            if success:
                pickings = self.env['stock.picking'].search([('expense', '=', self.id)])
                if pickings:
                    for picking in pickings:
                        picking.unlink()
                self.state = 'refused'

    @api.multi
    def previous(self):
        if self.workflow_id:
            success, current_sequence = self.env['workflow.config'].action_return('product.expense.workflow.history',
                                                                                  'history', self, self.env.user.id)
            if success:
                self.check_sequence = current_sequence

    def action_force_cancel(self):
        self.mapped('stock_picking').action_force_cancel()
        self.cancel()
        # self.write({'state': 'cancel'})

    @api.multi
    def create_out_picking(self):
        StockPicking = self.env['stock.picking']
        StockMove = self.env['stock.move']
        des_location = self.env['stock.location'].search([('scrap_location', '=', True)])

        for record in self:
            # Данс нь авлага болон өглөг төрөлтэй үед хүргэх захиалгаас ажил гүйлгээ үүсэхдээ харилцагч авч үүсдэг
            # болгох. Шаардах хуудас үүсгэсэн ажилтантай холбоотой харилцагчийг авна.
            if str(record.account.user_type_id.type) in ('receivable', 'payable'):
                values = {'expense': record.id,
                          'origin': record.code,
                          'company_id': record.company.id,
                          'picking_type_id': self.stock_picking_type.id,
                          'location_dest_id': des_location[0].id,
                          'location_id': self.stock_picking_type.default_location_src_id.id,
                          'partner_id': record.employee.address_home_id.id or False}
            else:
                values = {'expense': record.id,
                          'origin': record.code,
                          'company_id': record.company.id,
                          'picking_type_id': self.stock_picking_type.id,
                          'location_dest_id': des_location[0].id,
                          'location_id': self.stock_picking_type.default_location_src_id.id}
            stock_picking_object = StockPicking.create(values)
            stock_picking_object.message_post_with_view(
                'l10n_mn_product_expense.message_link_for_picking_expense',
                values={
                    'self': stock_picking_object,
                    'origin': record
                },
                subtype_id=self.env.ref('mail.mt_note').id
            )
            if record.expense_line:
                for line in record.expense_line:
                    move_lines_values = line._get_stock_move_vals(stock_picking_object.id, self.stock_picking_type.default_location_src_id.id, des_location[0].id)
                    StockMove.sudo().create(move_lines_values)

    @api.multi
    def check_done(self):
        ''' шаардах хуудаснаас үүссэн барааны гарах хөдөлгөөн батлагдаж дуусахад
        "Дууссан" төлөвт орох
        '''
        ok = True
        for picking in self.stock_picking:
            if picking.picking_type_id.code == 'outgoing' and picking.state not in ('done'):
                ok = False
        if ok:
            self.state = 'done'


class ExpenseLine(models.Model):
    _name = 'product.expense.line'

    @api.multi
    def _get_stock_move_vals(self, picking_id, location_id, location_dest_id):
        vals = {}
        vals.update({'name': self.name})
        vals.update({'origin': self.code})
        vals.update({'picking_type_id': self.expense.stock_picking_type.id})
        vals.update({'picking_id': picking_id})
        vals.update({'product_id': self.product.id})
        vals.update({'product_uom': self.product.product_tmpl_id.uom_id.id})
        vals.update({'location_id': location_id})
        vals.update({'location_dest_id': location_dest_id})
        vals.update({'product_uom_qty': self.quantity})
        vals.update({'analytic_account_id': self.analytic_account and self.analytic_account.id or False})
        vals.update({'company_id': self.expense.company.id})
        return vals

    @api.depends('product')
    def _compute_available_qty(self):
        move_qty = 0
        pro_qty = 0
        for obj in self:
            warehouse = obj.expense.warehouse
            if not warehouse:
                raise UserError(_('Warning!\nYou must select supply warehouse before add expense line!'))
            if obj.product:
                obj.available_qty = obj.product.get_qty_availability([obj.expense.stock_picking_type.default_location_src_id.id], obj.expense.date, qty_dp_digit=self.env['decimal.precision'].precision_get('Expense Available Qty'))
                obj.name = obj.product.name_get()[0][1]
            else:
                obj.available_qty = 0

    expense = fields.Many2one('product.expense', 'Expense', ondelete='cascade')
    name = fields.Char('Name', required=True)
    product = fields.Many2one('product.product', 'Product', required=True, domain=[('type', '!=', 'service')])
    quantity = fields.Float('Quantity', digits=dp.get_precision('Expense Quantity'))
    returned_quantity = fields.Float('Returned Quantity', readonly=True, digits=dp.get_precision('Expense Returned Quantity'))
    available_qty = fields.Float(compute='_compute_available_qty', string='Available quantity', digits=dp.get_precision('Expense Available Qty'))
    account = fields.Many2one('account.account', string='Account')
    analytic_account = fields.Many2one('account.analytic.account', 'Analytic account')
    product_type = fields.Selection('stock.move', related='expense.stock_picking.move_lines.state', string='Product type')
    must_choose_account = fields.Boolean(related='expense.must_choose_account')
    price_subtotal = fields.Float(string='Amount', store=True, readonly=True, compute='_compute_price')
    standard_price = fields.Float(readonly=True, compute='_compute_standard_price', digits=dp.get_precision('Expense product cost'))
    cost_center = fields.Selection(related='expense.cost_center', readonly=True, string='Cost center')
    date = fields.Date(related='expense.date', string='Date', store=True)
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse', related='expense.warehouse', store=True)
    state = fields.Selection(related='expense.state', string='State', store=True)
    code = fields.Char(related='expense.code', string='Expense Code', copy=False)

    @api.one
    @api.depends('quantity', 'product')
    def _compute_price(self):
        self.price_subtotal = self.standard_price * self.quantity

    @api.one
    @api.depends('product')
    def _compute_standard_price(self):
        if self.expense.is_module_installed('l10n_mn_stock_account_cost_for_each_wh'):
            # Агуулах бүрээр өртөг тооцоолдог үед тухайн барааны агуулахаарх өртөг шаардах хуудасын
            # мөрүүдэд өртгөө авна
            if self.product and self.expense.company and self.expense.warehouse:
                self._cr.execute("""
                                SELECT standard_price FROM product_warehouse_standard_price
                                WHERE company_id = %s AND warehouse_id = %s AND product_id = %s""" % (
                    self.expense.company.id, self.expense.warehouse.id, self.product.id))
                results = self._cr.dictfetchall()
                if results and len(results) > 0:
                    if results[0]['standard_price']:
                        self.standard_price = results[0]['standard_price']
        else:
            self.standard_price = self.product.standard_price

    @api.constrains('quantity')
    def _check_quantity_value(self):
        for line in self:
            if line.quantity <= 0:
                raise UserError(_('Error!\nProduct quantitiy must be greater than Zero.'))

    @api.onchange('cost_center', 'product')
    def set_analytic_account(self):
        for obj in self:
            obj.name = obj.product.name if obj.product.name else '/'
            if obj.cost_center == 'brand':
                obj.analytic_account = obj.product.sudo().brand_name.analytic_account_id
            elif obj.cost_center == 'product_categ':
                obj.analytic_account = obj.product.sudo().categ_id.analytic_account_id


class ExpenseWorkflowHistory(models.Model):
    _name = 'product.expense.workflow.history'
    _order = 'history, sent_date'

    STATE_SELECTION = [('waiting', 'Waiting'),
                       ('confirmed', 'Confirmed'),
                       ('approved', 'Approved'),
                       ('return', 'Return'),
                       ('rejected', 'Rejected')]

    history = fields.Many2one('product.expense', 'Expense', readonly=True, ondelete='cascade')
    line_sequence = fields.Integer('Workflow Step')
    name = fields.Char('Verification Step', readonly=True)
    user_ids = fields.Many2many('res.users', 'res_users_expense_workflow_history_ref', 'history_id', 'user_id', 'Validators')
    sent_date = fields.Datetime('Sent date', required=True, readonly=True)
    user_id = fields.Many2one('res.users', 'Validator', readonly=True)
    action_date = fields.Datetime('Action date', readonly=True)
    action = fields.Selection(STATE_SELECTION, 'Action', readonly=True)
