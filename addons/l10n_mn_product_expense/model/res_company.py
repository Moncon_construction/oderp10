# -*- coding: utf-8 -*-

from odoo import api, fields, models

class ResCompany(models.Model):
    _inherit = 'res.company'

    workflow = fields.Many2one('workflow.config', 'Workflow')
    
