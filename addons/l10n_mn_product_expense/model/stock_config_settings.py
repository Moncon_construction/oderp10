# -*- coding: utf-8 -*-

from odoo import api, fields, models

class StockSettings(models.TransientModel):
    _inherit = 'stock.config.settings'

    workflow = fields.Many2one('workflow.config', related='company_id.workflow', string="Workflow")

    @api.model
    def get_default_wk(self, fields):
        return {'workflow': self.env.user.company_id.workflow.id}
