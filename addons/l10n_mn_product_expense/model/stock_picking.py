# -*- coding: utf-8 -*-

from odoo import models, fields, api

class StockPicking(models.Model):
    _inherit = 'stock.picking'

    expense = fields.Many2one('product.expense', 'Expense')

    @api.multi
    def check_product_expense(self):
        for object in self:
            for move in object.move_lines:
                if move.origin_returned_move_id and move.origin_returned_move_id.picking_id.expense:
                    for line in move.origin_returned_move_id.picking_id.expense.expense_line:
                        if line.product.id == move.product_id.id:
                            line.write({'returned_quantity': move.product_qty})

    @api.multi
    def do_transfer(self):
        self.check_product_expense()
        res = super(StockPicking, self).do_transfer()
        for object in self:
            if object.expense:
                object.expense.check_done()
        return res

