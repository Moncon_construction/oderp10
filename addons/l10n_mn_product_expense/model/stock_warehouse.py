# -*- coding: utf-8 -*-

from odoo import models, fields, api

class StockWareHouse(models.Model):
    _inherit = 'stock.warehouse'

    workflow = fields.Many2one('workflow.config', 'Workflow', help='Allow expense workflow by each warehouses')
