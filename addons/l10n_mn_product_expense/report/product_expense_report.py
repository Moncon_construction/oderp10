# -*- coding: utf-8 -*-

from odoo import api, models


class ProductExpenseReport(models.AbstractModel):
    _name = 'report.product_expense_templates'

    @api.multi
    def render_html(self, docids, data=None):

        docargs = {
            'groups': self.env.user.has_group('l10n_mn_stock.group_stock_view_cost'),
            'doc_ids': docids,
            'doc_model': 'product.expense',
            'user': self.env.user,
            'docs': self.env['product.expense'].browse(docids),
            'data': data
        }

        return self.env['report'].render('product_expense_report_call_slip', docargs)
