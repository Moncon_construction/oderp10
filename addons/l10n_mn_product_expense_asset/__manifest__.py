# -*- coding: utf-8 -*-
{
    "name": "Mongolian Product Expense Asset",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "website": "http://asterisk-tech.mn",
    "depends": [
        'l10n_mn_product_expense',
        'l10n_mn_account_asset'
    ],
    "data": [
        'wizard/product_expense_asset.xml',
    ],
    "active": False,
    "installable": True,
}
