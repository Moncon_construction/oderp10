# -*- coding: utf-8 -*-
from datetime import datetime
from odoo import api, fields, models, _
from odoo.exceptions import UserError

class ProductExpenseAsset(models.Model):
    _inherit = "product.expense"

    def _count_product_asset(self):
        for obj in self:
            product_asset = obj.env['account.asset.asset'].search([('expense_id', '=', self.id)])
            obj.count_product_asset_button = len(product_asset)

    to_asset = fields.Boolean('To Asset', default= False)
    count_product_asset_button = fields.Integer(compute='_count_product_asset')

class ProductExpenseAssetWizard(models.TransientModel):
    _name = 'product.asset.wizard'

    def _fill_lines(self):
        res = []
        context = self._context
        record_id = context and context.get('active_id', False) or False
        obj = self.env[context.get('active_model')].browse(record_id)
        for lines in obj.expense_line:
            res.append(
                (0, 0, {
                    'product_id': lines.product.id,
                    'price_unit': lines.standard_price,
                    'product_qty': lines.quantity,
                    'sub_total':  lines.standard_price * lines.quantity
                })
            )

        return res or False

    product_asset_ids = fields.One2many('product.asset.wizard.line', 'product_asset_id', default=_fill_lines)

    def action_done(self):
        asset_ids = []
        for asset in self:
            context = self._context
            for line in asset.product_asset_ids:
                qty = line.product_qty
                i = 1
                while i <= qty:
                    i += 1
                    asset_id = self.env['account.asset.asset'].create({
                                'expense_id': context.get('active_id'),
                                'date': datetime.now(),
                                'name': line.product_id.name,
                                'category_id': line.category_id.id,
                                'purchase_date': line.date,
                                'date': line.date,
                                'value': line.product_id.standard_price,
                                'value_residual': line.product_id.standard_price,
                                'location_id':line.location_id.id,
                                'method':line.method,
                                'method_period':line.method_period,
                                'method_number':line.method_number,
                    })
                    asset_ids.append(asset_id.id)
        return {
            'domain': "[('id','in', " + str(asset_ids) + ")]",
            'name': _('Asset Created'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.asset.asset',
            'view_id': False,
            'type': 'ir.actions.act_window',
        }

class ProductExpenseAssetWizardLine(models.TransientModel):
    _name = 'product.asset.wizard.line'



    @api.onchange('category_id')
    def _default_vals(self):
        for obj in self:
            if obj.category_id:
                self.method_number = obj.category_id.method_number
                self.method = obj.category_id.method
                self.method_period = obj.category_id.method_period

    @api.multi
    @api.onchange('product_qty')
    def _onchange_product_qty(self):
        for obj in self:
            obj.sub_total = obj.price_unit * obj.product_qty


    product_asset_id = fields.Many2one('product.asset.wizard', 'Product Asset')
    product_id = fields.Many2one('product.product', string='Product', required = True)
    product_qty = fields.Float('Quantity')
    category_id = fields.Many2one('account.asset.category', string='Asset Category', required = True)
    method = fields.Selection([('linear', 'Linear'), ('degressive', 'Degressive')], 'Method', required = True)
    method_number = fields.Float('Method number')
    method_period = fields.Integer('Method Period', required = True)
    price_unit = fields.Float('Price unit',readonly = True)
    sub_total = fields.Float('Sub total', readonly = True)
    date = fields.Date(string='Date', default=fields.Date.context_today)
    location_id = fields.Many2one('account.asset.location', string='Asset Location', required = True)


class AccountAssetAsset(models.Model):
    _inherit = 'account.asset.asset'

    expense_id = fields.Many2one('product.expense')

