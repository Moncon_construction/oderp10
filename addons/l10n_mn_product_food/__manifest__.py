# -*- coding: utf-8 -*-

{
    'name': "Additional Information on Goods in the FOOD INDUSTRY",
    'version': '1.0',
    'depends': ['l10n_mn_product'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
        Additional Information on Goods in the FOOD INDUSTRY:
            * Product Nutrition Information
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/product_mineral_views.xml',
        'views/product_views.xml',
    ],
    'installable': True
}
