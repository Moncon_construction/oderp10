# -*- coding: utf-8 -*-

from odoo import api, fields, models


MINERAL_TYPE = [
   ('protein', 'Protein'),
   ('fat', 'Fat'),
   ('carbohydrate', 'Carbohydrate'),
   ('water', 'Water'),
   ('vitamin', 'Vitamin'),
   ('mineral', 'Mineral')
]

class ProductMineral(models.Model):
    _name = 'product.mineral'
    
    company_id = fields.Many2one('res.company', default=lambda self: self.env.user.company_id, string='Company')
    
    name = fields.Selection(MINERAL_TYPE, required=True, string='Type')
    qty_by_gr = fields.Float(required=True, string='Quantity /gr/')
    
    product_tmpl_id = fields.Many2one('product.template', string='Product')
    
    