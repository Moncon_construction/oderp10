# -*- coding: utf-8 -*-

from odoo import api, fields, models


class ProductTemplate(models.Model):
    _inherit = 'product.template'
    
    calories = fields.Float(string='Calories')
    mineral_ids = fields.One2many('product.mineral', 'product_tmpl_id', string='Minerals')
    