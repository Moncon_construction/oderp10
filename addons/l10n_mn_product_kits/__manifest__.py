# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-Now Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 77228080
#
##############################################################################
{
    "name" : "Product Kits",
    "version" : "1.0",
    "author" : "Asterisk Technologies LLC",
    "category" : "Mongolian Modules ",
    "description": """
    """,
    "website" : "http://asterisk-tech.mn",
    "depends":[
               'product', 'sale', 'sales_team'
               ],
    "demo_xml" : [],
    "init": [],
    "data" : [
        'security/ir.model.access.csv',
        'views/product_kits_view.xml',
        'views/sale_order_view.xml',

    ],
    "auto_install": False,
    "installable": True,

}