# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-Now Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 77228080
#
##############################################################################

from odoo import api, fields, models, _

class ProductKits(models.Model):
    _name = 'product.kits'

    kit_product_id = fields.Many2one('product.product','Kit Product', required=True)
    product_id = fields.Many2one('product.template','Product')
    qty = fields.Float('Quantity', required=True)

class ProductTemplate(models.Model):
    _inherit = 'product.template'
     
    has_product_kits = fields.Boolean('Has Product Kits')
    product_kits = fields.One2many('product.kits', 'product_id', 'Product Kits')