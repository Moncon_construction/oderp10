# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    product_kit_ids = fields.One2many('sale.product.kits', 'so_id', string='Product Kits')

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    sale_product_kit = fields.Many2one('sale.product.kits', 'Product Kits')

    @api.multi
    def write(self, vals):
        context = self._context
        for line in self:
            if 'product_uom_qty' in vals and line.sale_product_kit:
                if 'sale_product_kit' not in context:
                    raise UserError(_('Unable to edit the product kits, first upgrade it product kits.'))
        return super(SaleOrderLine, self).write(vals)

    @api.multi
    def unlink(self):
        context = self._context
        for line in self:
            if line.sale_product_kit:
                if 'sale_product_kit' not in context:
                    raise UserError(_('Unable to edit the product kits, first upgrade it product kits.'))
        return super(SaleOrderLine, self).unlink()

class SalePorductKits(models.Model):
    _name = 'sale.product.kits'

    product_id = fields.Many2one('product.template', 'Product Kits')
    qty = fields.Float('Qty')
    so_id = fields.Many2one('sale.order', string="Sale order")

    @api.multi
    def unlink(self):
        #  Устгах үед холбоотой үүссэн захиалгын мөрүүдийг устгана.
        for line in self:
            sale_product_kit = {'sale_product_kit': True}
            so_line = self.env['sale.order.line'].search([('sale_product_kit', '=', line.id)])
            if so_line:
                so_line.with_context(sale_product_kit).unlink()
        return super(SalePorductKits, self).unlink()

    @api.model
    def create(self, vals):
        res = super(SalePorductKits, self).create(vals)
        if 'qty' in vals and 'product_id' in vals:
            product = self.env['product.template'].search([('id', '=', vals.get('product_id'))])
            if product:
                for kit in product.product_kits:
                    self.env['sale.order.line'].create({'order_id': vals.get('so_id'),
                                                      'product_id': kit.kit_product_id.id,
                                                      'product_uom': self.env.ref('product.product_uom_unit').id,
                                                      'name': kit.kit_product_id.name,
                                                      'product_uom_qty': kit.qty * vals.get('qty'),
                                                      'sale_product_kit': res.id,
                                                      'price_unit': kit.kit_product_id.list_price,
                                                      })
        return res

    @api.multi
    def write(self, vals):
        for line in self:
            so_line = self.env['sale.order.line'].search([('sale_product_kit', '=', line.id)])
            if 'qty' in vals:
                sale_product_kit = {'sale_product_kit': True}
                if so_line:
                    for sline in so_line:
                        for product_kit in line.product_id.product_kits:
                            if sline.product_id.id == product_kit.kit_product_id.id:
                                sline.with_context(sale_product_kit).write({'product_uom_qty': vals.get('qty') * product_kit.qty or 0})
        return super(SalePorductKits, self).write(vals)