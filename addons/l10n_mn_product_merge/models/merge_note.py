# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2019 Asterisk Technologies LLC Co.,ltd (<http://www.asterisk-tech.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

from odoo import fields, models
from odoo.tools.translate import _

class MergeNote(models.Model):
    _name = 'merge.note'
    _description = "Merge Note"
    
    datetime = fields.Datetime('Calculated DateTime')
    user_id = fields.Many2one('res.users','User')
    merge_from_product_id = fields.Many2one('product.product', 'Merge From Product')
    merge_to_product_id = fields.Many2one('product.product', 'Merge To Product')
    note = fields.Text('Calculation note')
    
