# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2019 Asterisk Technologies LLC Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

from odoo import fields, models
from odoo.tools.translate import _
from odoo.exceptions import UserError
import logging
import time

_logger = logging.getLogger(__name__)

class MergeProducts(models.TransientModel):
    _name = "merge.products"
    _description = "Merge Products"

    merge_from_product_id = fields.Many2one('product.product', 'Merge From Product', required=True, ondelete='cascade')
    merge_to_product_id = fields.Many2one('product.product', 'Merge To Product', required=True, ondelete='cascade')
    merged_user = fields.Many2one('res.users')

    def is_module_installed(self, module_name):
        self._cr.execute("SELECT id FROM ir_module_module WHERE name = '%s' AND state IN ('installed', 'to upgrade')" %module_name)
        results = self._cr.dictfetchall()

        if results and len(results) > 0:
            return True
        else:
            return False

    def merge(self):
        product_id_from = self.merge_from_product_id
        product_id_to = self.merge_to_product_id
        self.merged_user = self.env.user
        if not self.is_module_installed('l10n_mn_stock_account_cost_for_each_wh'):
            if product_id_from.product_tmpl_id.property_account_income_id.id == product_id_to.product_tmpl_id.property_account_income_id.id:
                if product_id_from.product_tmpl_id.property_account_expense_id.id == product_id_to.product_tmpl_id.property_account_expense_id.id:
                    if product_id_from.product_tmpl_id.categ_id.property_account_income_categ_id.id == product_id_to.product_tmpl_id.categ_id.property_account_income_categ_id.id:
                        if product_id_from.product_tmpl_id.categ_id.property_stock_account_output_categ_id.id == product_id_to.product_tmpl_id.categ_id.property_stock_account_output_categ_id.id:
                            return self.sudo().merge_like_admin()
                        else:
                            raise UserError(_('Merging is not posible, if account configuration of the product categories are different.”'))
                    else:
                        raise UserError(_('Merging is not posible, if account configuration of the product categories are different.”'))
                else:
                    raise UserError(_('Merging is not posible, if account configuration of the product categories are different.”'))
            else:
                raise UserError(_('Merging is not posible, if account configuration of the product categories are different.”'))
        else:
            return self.sudo().merge_like_admin()

    def merge_like_admin(self):
        _logger.info("PRODUCT MERGE PROCESS IS STARTING...")
        report_obj = self
        product_id_from = report_obj.merge_from_product_id
        product_id_to = report_obj.merge_to_product_id
        note = ''
        #Merge stock moves
        move_ids = self.env['stock.move'].search([('product_id', '=', product_id_from.id)]).ids
        if move_ids and len(move_ids) > 0:
            moves = self.env['stock.move'].browse(move_ids)
            for move in moves:
                note += _(u'\n        stock.move successfully merged: %s %s') % (move.picking_id.name, move.name)
                
            self._cr.execute('UPDATE stock_move ' \
                        'SET product_id = %s, product_uom=%s ' \
                        'WHERE id in %s', (product_id_to.id, product_id_to.uom_id.id, tuple(move_ids)))
            _logger.info("PRODUCT MERGING: stock.move successfully merged...")
        
        #Merge stock pack operations
        pack_ids = self.env['stock.pack.operation'].search([('product_id', '=', product_id_from.id)]).ids
        if pack_ids and len(pack_ids) > 0:
            packs = self.env['stock.pack.operation'].browse(pack_ids)
            
            for line in packs:
                note += _(u'\n        stock.pack.operation successfully merged: %s id: %s') % (line.picking_id.name, line.id)
            self._cr.execute('UPDATE stock_pack_operation ' \
                        'SET product_id = %s, product_uom_id = %s ' \
                        'WHERE id in %s', (product_id_to.id, product_id_to.uom_id.id, tuple(pack_ids)))
            _logger.info("PRODUCT MERGING: stock.pack.operation successfully merged...")
        
        #Merge stock quants
        quant_ids = self.env['stock.quant'].search([('product_id','=',product_id_from.id)]).ids
        if quant_ids and len(quant_ids) > 0:
            quants = self.env['stock.quant'].browse(quant_ids)
            for line in quants:
                note += _(u'\n        stock.quant successfully merged: %s id: %s') % (line.product_id.name, line.id)
            quants.write({'product_id': product_id_to.id})
            _logger.info("PRODUCT MERGING: stock.quant successfully merged...")
        
        #Merge procurement orders
        proc_ids = self.env['procurement.order'].search([('product_id','=',product_id_from.id)]).ids
        if proc_ids and len(proc_ids) > 0:
            procs = self.env['procurement.order'].browse(proc_ids)
            for proc in procs:
                note += _(u'\n        procurement.order successfully merged: %s') % proc.name
            procs.write({
                'product_id':product_id_to.id,
                'product_uom':product_id_to.uom_id.id,
                'name':product_id_to.name
            })
            _logger.info("PRODUCT MERGING: procurement.order successfully merged...")
        
        #Merge stock inventory line
        inv_line_ids = self.env['stock.inventory.line'].search([('product_id','=',product_id_from.id)]).ids
        if inv_line_ids and len(inv_line_ids) > 0:
            inv_lines = self.env['stock.inventory.line'].browse(inv_line_ids)
            for line in inv_lines:
                note += _(u'\n        stock.inventory.line successfully merged: %s') % line.inventory_id.name
            inv_lines.write({
                'product_id': product_id_to.id,
                'product_uom_id': product_id_to.uom_id.id,
            })
            _logger.info("PRODUCT MERGING: stock.inventory.line successfully merged...")
        
        #Merge invoice line
        invoice_line_ids = self.env['account.invoice.line'].search([('product_id','=',product_id_from.id)]).ids
        if invoice_line_ids and len(invoice_line_ids) > 0:
            invoice_lines = self.env['account.invoice.line'].browse(invoice_line_ids)
            for line in invoice_lines:
                note += _(u'\n        account.invoice.line successfully merged: %s') % line.invoice_id.name
            invoice_lines.write({'product_id':product_id_to.id})
            _logger.info("PRODUCT MERGING: account.invoice.line successfully merged...")
        
        # Merge sale order line
        if self.is_module_installed('sale'):
            sale_line_ids = self.env['sale.order.line'].search([('product_id','=',product_id_from.id)]).ids
            if sale_line_ids and len(sale_line_ids) > 0:
                sale_lines = self.env['sale.order.line'].browse(sale_line_ids)
                for line in sale_lines:
                    note += _(u'\n        sale.order.line successfully merged: %s %s') % (line.order_id.name, line.name)
                sale_lines.write({
                    'product_id':product_id_to.id,
                    'product_uom':product_id_to.uom_id.id,
                })
                _logger.info("PRODUCT MERGING: sale.order.line successfully merged...")
        
        # Merge purchase order line
        if self.is_module_installed('purchase'):
            pur_line_ids = self.env['purchase.order.line'].search([('product_id','=',product_id_from.id)]).ids
            if pur_line_ids and len(pur_line_ids) > 0:
                pur_lines = self.env['purchase.order.line'].browse(pur_line_ids)
                for line in pur_lines:
                    note += _(u'\n        purchase.order.line successfully merged: %s %s') % (line.order_id.name, line.name)
                pur_lines.write({
                    'product_id':product_id_to.id,
                    'product_uom':product_id_to.uom_id.id,
                })
                _logger.info("PRODUCT MERGING: purchase.order.line successfully merged...")

        if self.is_module_installed('point_of_sale'):
            # Merge pos order line
            pos_order_line_ids = self.env['pos.order.line'].search([('product_id','=',product_id_from.id)]).ids
            if pos_order_line_ids and len(pos_order_line_ids) > 0:
                pos_lines = self.env['pos.order.line'].browse(pos_order_line_ids)
                for line in pos_lines:
                    note += _(u'\n        pos.order.line successfully merged: %s %s') % (line.order_id.name, line.name)
                pos_lines.write({
                    'product_id':product_id_to.id,
                })
                _logger.info("PRODUCT MERGING: pos.order.line successfully merged...")

        if self.is_module_installed('l10n_mn_stock'):
            # Merge transit order line
            transit_line_ids = self.env['stock.transit.order.line'].search([('product_id','=',product_id_from.id)]).ids
            if transit_line_ids and len(transit_line_ids) > 0:
                order_lines = self.env['stock.transit.order.line'].browse(transit_line_ids)
                for line in order_lines:
                    note += _(u'\n        transit.order.line successfully merged: %s %s') % (line.transit_order_id.name, line.name)
                order_lines.write({
                    'product_id':product_id_to.id,
                    'product_uom':product_id_to.uom_id.id,
                })
                _logger.info("PRODUCT MERGING: transit.order.line successfully merged...")
        
        #Merge manufacture order line
        if self.is_module_installed('mrp'):
            if self.is_module_installed('l10n_mn_mrp_bom_substitution'):
                # Merge mrp.bom.line
                bom_subs_ids = self.env['bom.substitution'].search([('product_id', '=', product_id_from.id)])
                if bom_subs_ids and len(bom_subs_ids) > 0:
                    for line in bom_subs_ids:
                        line = line.sudo()
                        note += _(u'\n        bom.substitution successfully merged: %s %s') % ('', line.product_id.name)
                        line.write({'product_id': product_id_to.id, 'product_uom': product_id_to.uom_id.id})
                    _logger.info("PRODUCT MERGING: bom.substitution successfully merged...")
                
            mrp_ids = self.env['mrp.production'].search([('product_id','=',product_id_from.id)]).ids
            if mrp_ids and len(mrp_ids) > 0:
                mrps = self.env['mrp.production'].browse(mrp_ids)
                for line in mrps:
                    note += _(u'\n        mrp.production successfully merged: %s %s') % (line.name, line.name)
                mrps.write({
                    'product_id':product_id_to.id,
                    'product_uom':product_id_to.uom_id.id,
                })
                _logger.info("PRODUCT MERGING: mrp.production successfully merged...")
        
            # Merge mrp.bom.line
            bom_line_ids = self.env['mrp.bom.line'].search([('product_id', '=', product_id_from.id)])
            if bom_line_ids and len(bom_line_ids) > 0:
                for line in bom_line_ids:
                    line = line.sudo()
                    note += _(u'\n        mrp.bom.line successfully merged: %s %s') % ('', line.product_id.name)
                    line.write({'product_id': product_id_to.id, 'product_uom': product_id_to.uom_id.id})
                _logger.info("PRODUCT MERGING: mrp.bom.line successfully merged...")
                
            # Merge mrp boms
            bom_ids = self.env['mrp.bom'].search([('product_id','=',product_id_from.id)])
            if bom_ids and len(bom_ids) > 0:
                for line in bom_ids:
                    line = line.sudo()
                    note += _(u'\n        mrp.bom successfully merged: %s %s') % (line.name, line.product_id.name)
                    line.write({'product_id':product_id_to.id,
                                'product_uom':product_id_to.uom_id.id,
                                })
                _logger.info("PRODUCT MERGING: mrp.bom successfully merged...")
            
        _logger.info("PRODUCT MERGED: '%s'   ---------->  '%s'" %(product_id_from.name, product_id_to.name)) 
        note_id = False
        
        if note:
            note_id = self.env['merge.note'].create({'note': note, 'user_id': self.merged_user.id,
                                                   'merge_from_product_id':product_id_from.id,
                                                   'merge_to_product_id':product_id_to.id,
                                                   'datetime': time.strftime('%Y-%m-%d %H:%M:%S')})
        if note_id:
            try:
                dummy, view_id = self.env['ir.model.data'].get_object_reference('l10n_mn_merge_products', 'view_product_calc_note_form')
            except ValueError, e:
                view_id = False
                
            result = {
                'name': _('Merge Note'),
                'view_type': 'form',
                'view_mode': 'form',
                'view_id': view_id,
                'res_model': 'merge.note',
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'current',
                'res_id': note_id.id,
            }
            return result
        
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: