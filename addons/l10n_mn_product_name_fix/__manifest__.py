# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2019 Asterisk Technologies LLC Co.,ltd (<http://www.asterisk-tech.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

{
    "name" : "Product Name Fix",
    "version" : "1.0",
    "author" : "Asterisk Technologies LLC",
    "description": """
        Merge Products 
    """,
    "website" : 'http://asterisk-tech.mn',
    "category" : 'Mongolian Modules',
    "depends" : ['l10n_mn_stock'],
    "data" : [
        'security/product_name_fix_security.xml',
        'wizard/product_name_fix_view.xml',
    ],
    "installable": True,
}