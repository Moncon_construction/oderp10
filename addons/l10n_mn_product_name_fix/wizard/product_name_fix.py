# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2019 Asterisk Technologies LLC Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

from odoo import fields, models
import logging

_logger = logging.getLogger(__name__)

class ProductNameFix(models.TransientModel):
    _name = "product.name.fix"
    _description = "Product Name Fix"

    def name_fix(self):
        self.env.cr.execute("SELECT id, value FROM ir_translation WHERE name = 'product.template,name' AND src != value")
        product_names = self.env.cr.dictfetchall()
        _logger.info('product count %s' % len(product_names))
        for product_name in product_names:
            translation = self.env['ir.translation'].browse(product_name['id'])
            if translation.id and translation.source:
                _logger.info('translation %s' % translation.id)
                translation.sudo().write({'source': product_name['value'], 'src': product_name['value']})
        return {'type': 'ir.actions.act_window_close'}