# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Product Pricelist",
    'version': '1.0',
    'depends': [
        'sale',
        'l10n_mn_sale'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Product Pricelist Additional Features
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/product_pricelist_views.xml',
        'views/product_pricelist_category_view.xml',
        'report/sale_discount_report_view.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}
