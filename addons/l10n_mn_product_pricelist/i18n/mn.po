# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* l10n_mn_product_pricelist
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 10.0+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-04-29 13:38+0000\n"
"PO-Revision-Date: 2020-04-29 13:38+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: l10n_mn_product_pricelist
#: code:addons/l10n_mn_product_pricelist/models/sale_order.py:66
#, python-format
msgid "%s Account Tax Empty!! "
msgstr "%s Татварын данс хоосон байна! "

#. module: l10n_mn_product_pricelist
#: code:addons/l10n_mn_product_pricelist/models/product_pricelist.py:12
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_accessory_ids
#, python-format
msgid "Accessory Product"
msgstr "Доорх бараанууд хамт зарагдвал:"

#. module: l10n_mn_product_pricelist
#: model:ir.ui.view,arch_db:l10n_mn_product_pricelist.product_pricelist_item_search
msgid "Active"
msgstr "Идэвхитэй"

#. module: l10n_mn_product_pricelist
#: code:addons/l10n_mn_product_pricelist/report/sale_discount_report.py:163
#, python-format
msgid "Amount"
msgstr "Дүн"

#. module: l10n_mn_product_pricelist
#: code:addons/l10n_mn_product_pricelist/models/product_pricelist.py:13
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_bonus_product_ids
#, python-format
msgid "Bonus Product"
msgstr "Доорх бараануудыг дагалдуулна:"

#. module: l10n_mn_product_pricelist
#: model:ir.actions.act_window,help:l10n_mn_product_pricelist.action_product_pricelist_category_form
msgid "Click to add a new product pricelist category."
msgstr "Энд дарж шинээр үнийн хүснэгтийн ангилал үүсгэнэ үү."

#. module: l10n_mn_product_pricelist
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_sale_discount_report_company_id
#: model:ir.ui.view,arch_db:l10n_mn_product_pricelist.product_pricelist_item_search
msgid "Company"
msgstr "Компани"

#. module: l10n_mn_product_pricelist
#: code:addons/l10n_mn_product_pricelist/report/sale_discount_report.py:152
#, python-format
msgid "Company Name: %s"
msgstr "Компаний нэр: %s"

#. module: l10n_mn_product_pricelist
#: model:ir.ui.view,arch_db:l10n_mn_product_pricelist.view_product_pricelist_item_form_inherit
msgid "Compute Price"
msgstr "Үнэ тооцоолох"

#. module: l10n_mn_product_pricelist
#: code:addons/l10n_mn_product_pricelist/models/sale_order.py:102
#, python-format
msgid "Create sale order Limit?"
msgstr "Борлуулалтын захиалгын хязгээр үүсгэх үү?"

#. module: l10n_mn_product_pricelist
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_accessories_create_uid
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_bonus_products_create_uid
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_sale_discount_report_create_uid
msgid "Created by"
msgstr "Үүсгэгч"

#. module: l10n_mn_product_pricelist
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_accessories_create_date
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_bonus_products_create_date
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_sale_discount_report_create_date
msgid "Created on"
msgstr "Үүсгэсэн"

#. module: l10n_mn_product_pricelist
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_accessories_display_name
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_bonus_products_display_name
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_sale_discount_report_display_name
msgid "Display Name"
msgstr "Дэлгэцийн Нэр"

#. module: l10n_mn_product_pricelist
#: code:addons/l10n_mn_product_pricelist/report/sale_discount_report.py:156
#, python-format
msgid "Duration: %s - %s"
msgstr "Хугацаа: %s - %s"

#. module: l10n_mn_product_pricelist
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_sale_discount_report_date_to
msgid "End Date"
msgstr "Дуусах огноо"

#. module: l10n_mn_product_pricelist
#: model:ir.ui.view,arch_db:l10n_mn_product_pricelist.sale_discount_report_view
msgid "Export"
msgstr "Экспорт"

#. module: l10n_mn_product_pricelist
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_accessories_id
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_bonus_products_id
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_sale_discount_report_id
msgid "ID"
msgstr "ID"

#. module: l10n_mn_product_pricelist
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_sale_order_line_created_from_pricelist
msgid "Is created from pricelist"
msgstr "Үнийн хүснэгтээс үүссэн эсэх"

#. module: l10n_mn_product_pricelist
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_accessories_item_id
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_bonus_products_item_id
msgid "Item id"
msgstr "Үнийн хүснэгтийн мөр"

#. module: l10n_mn_product_pricelist
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_accessories___last_update
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_bonus_products___last_update
msgid "Last Modified on"
msgstr "Сүүлийн засвар хийсэн огноо"

#. module: l10n_mn_product_pricelist
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_accessories_write_uid
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_bonus_products_write_uid
msgid "Last Updated by"
msgstr "Сүүлийн засвар хийсэн"

#. module: l10n_mn_product_pricelist
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_accessories_write_date
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_bonus_products_write_date
msgid "Last Updated on"
msgstr "Сүүлийн засвар хийсэн огноо"

#. module: l10n_mn_product_pricelist
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_bonus_products_max_qty
msgid "Max Quantity"
msgstr "Хамгийн их тоо хэмжээ"

#. module: l10n_mn_product_pricelist
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_category_name
msgid "Name"
msgstr "Нэр"

#. module: l10n_mn_product_pricelist
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_category_parent_id
msgid "Parent Category"
msgstr "Дээд ангилал"

#. module: l10n_mn_product_pricelist
#: model:ir.ui.view,arch_db:l10n_mn_product_pricelist.view_product_pricelist_item_form_inherit
msgid "Price Computation"
msgstr "Үнэ тооцоолол"

#. module: l10n_mn_product_pricelist
#: model:ir.ui.view,arch_db:l10n_mn_product_pricelist.product_pricelist_item_tree_view_inherit
msgid "Pricelist Items"
msgstr "Үнийн хүснэгтийн мөр"

#. module: l10n_mn_product_pricelist
#: model:ir.model,name:l10n_mn_product_pricelist.model_product_pricelist_item
msgid "Pricelist item"
msgstr "Үнийн дүрэм"

#. module: l10n_mn_product_pricelist
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_sale_discount_report_pricelist_ids
msgid "Pricelists"
msgstr "Үнийн хүснэгт"

#. module: l10n_mn_product_pricelist
#: model:ir.model.fields,help:l10n_mn_product_pricelist.field_sale_discount_report_pricelist_ids
msgid "Pricelists for current sales order."
msgstr "Одоогийн борлуулалтын захиалгын үнийн хүснэгтүүд."

#. module: l10n_mn_product_pricelist
#: model:ir.ui.view,arch_db:l10n_mn_product_pricelist.sale_discount_report_view
msgid "Product Pricelists"
msgstr "Үнийн хүснэгтүүд"

#. module: l10n_mn_product_pricelist
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_accessories_product_id
msgid "Product"
msgstr "Бараа"

#. module: l10n_mn_product_pricelist
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_bonus_products_product_id
msgid "Product1"
msgstr "Бараа"

#. module: l10n_mn_product_pricelist
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_accessories_qty
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_bonus_products_qty
msgid "Quantity"
msgstr "Тоо хэмжээ"

#. module: l10n_mn_product_pricelist
#: model:ir.actions.act_window,name:l10n_mn_product_pricelist.action_product_pricelist_category_form
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_category_id
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_pricelist_category_id
#: model:ir.ui.menu,name:l10n_mn_product_pricelist.menu_product_pricelist_category
#: model:ir.ui.view,arch_db:l10n_mn_product_pricelist.product_pricelist_category_free
#: model:ir.ui.view,arch_db:l10n_mn_product_pricelist.product_pricelist_category_search
msgid "Product Pricelist Category"
msgstr "Үнийн хүснэгтийн ангилал"

#. module: l10n_mn_product_pricelist
#: model:ir.ui.view,arch_db:l10n_mn_product_pricelist.product_pricelist_category_search
msgid "Product Pricelist Category Search"
msgstr "Үнийн хүснэгтийн ангилалаас хайх"

#. module: l10n_mn_product_pricelist
#: code:addons/l10n_mn_product_pricelist/report/sale_discount_report.py:94
#: code:addons/l10n_mn_product_pricelist/report/sale_discount_report.py:130
#: model:ir.actions.act_window,name:l10n_mn_product_pricelist.action_sale_discount_report
#: model:ir.ui.menu,name:l10n_mn_product_pricelist.menu_action_sale_discount_report
#, python-format
msgid "Sale Discount Report"
msgstr "Борлуулалтын хөнгөлөлтийн тайлан"

#. module: l10n_mn_product_pricelist
#: code:addons/l10n_mn_product_pricelist/report/sale_discount_report.py:162
#, python-format
msgid "Sale Pricelist Name"
msgstr "Үнийн хүснэгт"

#. module: l10n_mn_product_pricelist
#: model:ir.model,name:l10n_mn_product_pricelist.model_sale_order
msgid "Sales Order"
msgstr "Борлуулалтын захиалга"

#. module: l10n_mn_product_pricelist
#: model:ir.model,name:l10n_mn_product_pricelist.model_sale_order_line
msgid "Sales Order Line"
msgstr "Борлуулалтын Захиалгын Мөр"

#. module: l10n_mn_product_pricelist
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_sale_discount_report_date_from
msgid "Start Date"
msgstr "Эхлэл огноо"

#. module: l10n_mn_product_pricelist
#: code:addons/l10n_mn_product_pricelist/models/sale_order.py:95
#, python-format
msgid "Warning: Your loan sales amount is over limit. You cannot confirm sale order"
msgstr "Санамж: Таны зээлийн борлуулалтын хэмжээ хэтэрсэн байна. Та борлуулалтын захиалгыг баталгаажуулж чадахгүй"

#. module: l10n_mn_product_pricelist
#: code:addons/l10n_mn_product_pricelist/models/sale_order.py:81
#, python-format
msgid "You have new delivery order"
msgstr "Танд шинэ хүргэх захиалга байна"

#. module: l10n_mn_product_pricelist
#: code:addons/l10n_mn_product_pricelist/models/product_pricelist.py:11
#, python-format
msgid "accessories"
msgstr "Дагалдах бараанууд"

#. module: l10n_mn_product_pricelist
#: model:ir.ui.view,arch_db:l10n_mn_product_pricelist.sale_discount_report_view
msgid "or"
msgstr "эсвэл"

#. module: l10n_mn_product_pricelist
#: model:ir.model,name:l10n_mn_product_pricelist.model_product_pricelist_category
msgid "product.pricelist.category"
msgstr "Үнийн хүснэгтийн ангилал"

#. module: l10n_mn_product_pricelist
#: model:ir.model,name:l10n_mn_product_pricelist.model_product_pricelist_item_accessories
msgid "product.pricelist.item.accessories"
msgstr "product.pricelist.item.accessories"

#. module: l10n_mn_product_pricelist
#: model:ir.model,name:l10n_mn_product_pricelist.model_product_pricelist_item_bonus_products
msgid "product.pricelist.item.bonus.products"
msgstr "product.pricelist.item.bonus.products"

#. module: l10n_mn_product_pricelist
#: model:ir.ui.menu,name:l10n_mn_product_pricelist.menu_product_pricelist_items
msgid "Product Pricelist Item"
msgstr "Үнийн хүснэгтийн мөрүүд"

#. module: l10n_mn_product_pricelist
#: model:ir.ui.view,arch_db:l10n_mn_product_pricelist.product_pricelist_item_tree2_view_inherit
msgid "Applicable On"
msgstr "Дараахад Хэрэглэх боломжтой"

#. module: l10n_mn_product_pricelist
#: model:ir.ui.view,arch_db:l10n_mn_product_pricelist.product_pricelist_item_tree2_view_inherit
msgid "Price"
msgstr "Үнэ"

#. module: l10n_mn_product_pricelist
#: model:ir.ui.view,arch_db:l10n_mn_product_pricelist.product_pricelist_item_search
msgid "Active"
msgstr "Архивласан үнийн хүснэгтийн мөрүүд"

#. module: l10n_mn_product_pricelist
#: model:ir.model,name:l10n_mn_product_pricelist.model_product_pricelist
#: model:ir.ui.view,arch_db:l10n_mn_product_pricelist.product_pricelist_item_search
msgid "Pricelist"
msgstr "Үнийн хүснэгт"

#. module: l10n_mn_product_pricelist
#: model:ir.ui.view,arch_db:l10n_mn_product_pricelist.product_pricelist_item_search
msgid "Pricelist Category"
msgstr "Үнийн хүснэгтийн ангилал"

#. module: l10n_mn_product_pricelist
#: model:ir.model.fields,field_description:l10n_mn_product_pricelist.field_product_pricelist_item_accessories_product_id
#: model:ir.ui.view,arch_db:l10n_mn_product_pricelist.product_pricelist_item_search
msgid "Product"
msgstr "Бараа"

#. module: l10n_mn_product_pricelist
#: model:ir.ui.view,arch_db:l10n_mn_product_pricelist.product_pricelist_item_search
msgid "Product Category"
msgstr "Барааны ангилал"

#. module: l10n_mn_product_pricelist
#: model:ir.ui.view,arch_db:l10n_mn_product_pricelist.product_pricelist_item_search
msgid "Company"
msgstr "Компани"

#. module: l10n_mn_product_pricelist
#: model:ir.ui.view,arch_db:l10n_mn_product_pricelist.product_pricelist_item_search
msgid "Date Start"
msgstr "Эхлэх огноо"

#. module: l10n_mn_product_pricelist
#: model:ir.ui.view,arch_db:l10n_mn_product_pricelist.product_pricelist_item_search
msgid "Date End"
msgstr "Дуусах огноо"

#. module: l10n_mn_product_pricelist
#: code:addons/l10n_mn_product_pricelist/models/product_pricelist.py:39
#, python-format
msgid "You can not have 2 pricelist %s overlaps on same day!"
msgstr "Үнийн хүснэгтийн мөрд (%s) барааны эхлэх огноо, дуусах огноо давхацсан мөрүүд агуулж байгаа тул хадгалах боломжгүй. "

#. module: l10n_mn_product_pricelist
#: model:ir.actions.act_window,name:l10n_mn_product_pricelist.product_pricelist_action3
msgid "Pricelist Item List"
msgstr "Үнийн хүснэгтийн мөрүүд"

#. module: l10n_mn_product_pricelist
#: model:ir.model,name:l10n_mn_product_pricelist.model_sale_discount_report
msgid "sale.discount.report"
msgstr "Борлуулалтын хөнгөлөлтийн тайлан"