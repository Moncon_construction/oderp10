# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _
from datetime import date, datetime, timedelta
import pytz
import time
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

class Pricelist(models.Model):
    _inherit = "product.pricelist"

    category_id = fields.Many2one('product.pricelist.category', string='Product Pricelist Category')

class PricelistItem(models.Model):
    _inherit = "product.pricelist.item"
    
    applied_on = fields.Selection(selection_add=[('4_product_accessories', _('accessories'))])
    accessory_ids = fields.One2many('product.pricelist.item.accessories', 'item_id', string=_("Accessory Product"))
    bonus_product_ids = fields.One2many('product.pricelist.item.bonus.products', 'item_id', string=_("Bonus Product"))
    pricelist_category_id = fields.Many2one('product.pricelist.category', string="Product Pricelist Category", related="pricelist_id.category_id", store=True, readonly=True)
    active = fields.Boolean('product.pricelist', related="pricelist_id.active", store=True, readonly=True)

    def check_valid(self, date_check=False):
        """
        Checks if product pricelist item is valid
        :return: False if it is not, True if it is valid
        """
        user_time_zone = pytz.timezone(self.env.user.tz or pytz.UTC)
        datetime_now = datetime.now(tz=user_time_zone)
        offset = datetime_now.utcoffset()
        datetime_now = datetime.strptime(date_check, DEFAULT_SERVER_DATETIME_FORMAT) + offset if date_check else datetime_now.replace(tzinfo=None)
        if self.date_start and self.date_start > str(datetime_now.date()):
            return False
        if self.date_end and self.date_end < str(datetime_now.date()):
            return False
        if self.adjust_day:
            weekday = datetime_now.weekday()
            if weekday == 0 and not self.is_monday:
                return False
            if weekday == 1 and not self.is_tuesday:
                return False
            if weekday == 2 and not self.is_wednesday:
                return False
            if weekday == 3 and not self.is_thursday:
                return False
            if weekday == 4 and not self.is_friday:
                return False
            if weekday == 5 and not self.is_saturday:
                return False
            if weekday == 6 and not self.is_sunday:
                return False
        if self.adjust_hour:
            hours = datetime_now.hour + datetime_now.minute/60.0
            if self.start_hour and self.start_hour > hours:
                return False
            if self.end_hour and self.end_hour < hours:
                return False
        return True


class AccessoryProduct(models.Model):
    _name = 'product.pricelist.item.accessories'

    product_id = fields.Many2one('product.product', string='Product', required=True)
    qty = fields.Integer(string='Quantity', required=True)

    item_id = fields.Many2one('product.pricelist.item')


class BonusProduct(models.Model):
    _name = 'product.pricelist.item.bonus.products'

    product_id = fields.Many2one('product.product', string='Product1', required=True)
    qty = fields.Integer(string='Quantity', required=True)
    max_qty = fields.Integer(string='Max Quantity')

    item_id = fields.Many2one('product.pricelist.item')
