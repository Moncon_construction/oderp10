# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools

class PricelistCategory(models.Model):
    _name = "product.pricelist.category"

    name = fields.Char(string='Name')
    parent_id = fields.Many2one('product.pricelist.category', string='Parent Category')
    
    @api.multi
    def name_get(self):
        def get_names(cat):
            res = []
            while cat:
                res.append(cat.name)
                cat = cat.parent_id
            return res

        return [(cat.id, " / ".join(reversed(get_names(cat)))) for cat in self]
