# -*- coding: utf-8 -*-


from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from pyfcm import FCMNotification
from datetime import datetime, timedelta
from odoo import exceptions
import odoo.addons.decimal_precision as dp


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def bonus_line_create(self, item_id):
        # Захиалгын мөрүүд" талбар дээр сонгогдсон бараа, дагалдах урамшуулалтай бол бонус барааны тоо хэмжээ, хөнгөлөлт зэргийг тооцно
        sold_products = {}
        order_line_obj = self.env['sale.order.line']
        vals = {}
        order_products = self.order_line.filtered(lambda l: l.created_from_pricelist is False)
        if item_id.applied_on == '4_product_accessories':
            number_ratio = False
            for obj in item_id.accessory_ids:
                if obj.product_id in order_products.mapped('product_id'):
                    total_qty = sum(order_products.filtered(lambda l: l.product_id == obj.product_id).mapped('product_uom_qty'))
                    if not number_ratio and int(total_qty) / int(obj.qty) > 0:
                        number_ratio = int(total_qty) / int(obj.qty)
                    elif number_ratio and int(total_qty) / int(obj.qty) > 0:
                        number_ratio = min(int(total_qty) / int(obj.qty), number_ratio)
                    else:
                        number_ratio = 0
                        break
                else:
                    number_ratio = 0
                    break
            if number_ratio:
                for accessory in item_id.bonus_product_ids:
                    create_number = accessory.max_qty if accessory.max_qty and accessory.max_qty < number_ratio * accessory.qty else number_ratio * accessory.qty
                    if create_number:
                        same_line = self.order_line.filtered(lambda l: l.pricelist_item_id.id == item_id.id and l.product_id == accessory.product_id)
                        if not same_line:
                            vals.update({
                                'discount': 100.0,
                                'state': 'draft',
                                'order_id': self.id,
                                'product_id': accessory.product_id.id,
                                'created_from_pricelist': True,
                                'product_uom_qty': create_number,
                                'pricelist_item_id': item_id.id,
                                'name': u'BONUS -%s' % accessory.product_id.name
                            })
                            order_line_obj.create(vals)
                        elif same_line.product_uom_qty != accessory.qty * create_number:
                            same_line.write({'product_uom_qty': accessory.qty * create_number})

    @api.multi
    def action_confirm(self):
        # Борлуулалтын захиалга" цонхны "Борлуулалт Батлах" товчыг дархад "Захиалгын мөрүүд" талбар дээр сонгогдсон бараа дагалдуулах урамшуулалтай бол бонус барааг "Захиалгын мөрүүд" дээр нэмнэ
        line_product_ids = []
        sold_product_ids = []
        create_bonus = False
        for order in self:
            for line in order.order_line:
                for obj in line.tax_id:
                    if obj.amount_type != 'group' and not obj.account_id:
                        raise UserError(_("%s Account Tax Empty!! " % line.tax_id.name))
                line_product_ids.append(line.product_id.id)
            for item_id in order.pricelist_id.item_ids:
                # Барааны дагалдах бараа мөрд нэмэх хэсэг
                if item_id.check_valid(date_check=self.date_order):
                    order.bonus_line_create(item_id)
            res = super(SaleOrder, self).action_confirm()
            registration_ids = []
            res_users = self.env['res.users'].search([('sale_team_id', '=', order.team_id.id)])
            for user_id in res_users:
                if user_id.has_group('l10n_mn_sale.sale_delivery_worker'):
                    if user_id.token_id:
                        registration_ids.append(user_id.token_id)
            push_service = FCMNotification(
                api_key="AAAA1wUq8SU:APA91bGsATa9Vc3uw5THZcEarNuh-1XIfj2ry947i-y9fbdeFFEkG_EeUze7DPRA93rL-3j7KGV6sdKTDviFeMEx99LA_xDqZWcGk4seFOTccsV2zxiBpmD3uRvfnKNdJZnAwtEqGlks")
            message_title = (_('You have new delivery order'))
            message_body = "" + order.partner_id.name
            push_service.notify_multiple_devices(registration_ids=registration_ids,
                                                 message_title=message_title,
                                                 message_body=message_body,
                                                 sound="DEFAULT_SOUND")
            if order.user_id.partner_id.is_limit_loan_sale:
                limit = order.user_id.partner_id.limit_of_credit_sales
                loan_amount = order.user_id.partner_id.loan_sales_amount
                amount_total = order.amount_total
                check_value = limit - loan_amount - amount_total if limit != 0 else 0
                if check_value < 0:
                    # •    (Лимит – Нээх төлөвтэй нэхэмжлэхийн дүн – Борлуулалтын үнийн саналын дүн) = Сөрөг
                    raise ValidationError(
                        _('Warning: Your loan sales amount is over limit. You cannot confirm sale order'))
                # •    (Лимит – Нээх төлөвтэй нэхэмжлэхийн дүн – Борлуулалтын үнийн саналын дүн) / Лимит = > 0.8
                limit_check_value = (loan_amount + amount_total) / limit if limit != 0 else 0
                if limit_check_value >= 0.8:
                    view = self.env.ref('l10n_mn_sale.view_saleorder_confirmation')
                    wiz = self.env['sale.order.confirmation'].create({'order_id': order.id})
                    return {
                        'name': _('Create sale order Limit?'),
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'sale.order.confirmation',
                        'views': [(view.id, 'form')],
                        'view_id': view.id,
                        'target': 'new',
                        'res_id': wiz.id,
                        'context': self.env.context
                    }
                order.user_id.partner_id._compute_sales_amount()

            return res


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    created_from_pricelist = fields.Boolean(string="Is created from pricelist", default=False)
    pricelist_item_id = fields.Many2one('product.pricelist.item', string="Pricelist Item", ondelete='SET NULL')
