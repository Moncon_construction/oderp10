# -*- encoding: utf-8 -*-
##############################################################################

import time
import base64
import xlsxwriter
import pytz
from xlsxwriter.utility import xl_rowcol_to_cell
from io import BytesIO
from datetime import datetime

from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles  # @UnresolvedImport

class SelectionSheetTeamReport(models.TransientModel):
    """
       Борлуулалтын хөнгөлөлтийн тайлан
    """
    _name = 'sale.discount.report'
    
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env['res.company']._company_default_get('sale.discount.report'))
    date_from = fields.Date("Start Date", required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    date_to = fields.Date("End Date", required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    pricelist_ids = fields.Many2many('product.pricelist', string='Pricelists', help="Pricelists for current sales order.")
    
    def get_lines(self, pricelist):
        """Хөнгөлөлтийн дүнг олох функц"""
        if pricelist:
            query = """
                    SELECT d.id as id 
                    FROM sale_order_line a 
                    left join sale_order d on a.order_id = d.id  
                    left join product_pricelist c on d.pricelist_id = c.id  
                    WHERE a.state in ('sale', 'done') and a.company_id = %s AND d.date_order between '%s' and '%s' and d.pricelist_id = %s 
                    group by d.id  
                    ORDER BY d.id  ASC
            """ %(str(self.company_id.id), str(self.date_from) + " 00:00:00", str(self.date_to) + " 23:59:59", str(pricelist))
            self._cr.execute(query)
            results = self._cr.dictfetchall()
            return results if results else False
        else:
            return False
    
    def get_total_amount(self, pricelist):
        total_amount = 0
        discount_amount_row = 0
        """Хөнгөлөлтийн ангилалын дүнг олох функц"""
        while pricelist:
            pricelists2 = pricelist
            for pricelist in pricelists2:
                product_pricelist = self.env['product.pricelist'].search([('category_id', '=', pricelist.id)])
                for product_pro in product_pricelist:
                    orders = self.get_lines(product_pro.id)
                    if orders:
                        for order in orders:
                            order_name_row = ''
                            discount_amount_row = 0
                            order_obj = self.env['sale.order'].browse(order['id'])
                            for line in order_obj.order_line:
                                discount_amount_row = 0
                                order_obj = self.env['sale.order'].browse(order['id'])
                                for line in order_obj.order_line:
                                    discount_amount = procedure_amount = dis_amount = 0
                                    price2 = price = price3 = procedure_price = pro_price = p_type = 0
                                    if line.product_id:
                                        price2 = line.product_id.price_compute('list_price')[line.product_id.id]
                                        price_tmp = product_pro._compute_price_rule([(line.product_id, line.product_uom_qty, order_obj.partner_id.id)])[line.product_id.id][0]
                                        price = product_pro.currency_id.compute(price_tmp, order_obj.currency_id, round=False)
                                        total_amount += price2 - price
            pricelist = self.env['product.pricelist.category'].search([('parent_id', '=', pricelist.id)])
        return total_amount
    
    def set_spec_level(self, sheet, rowx, start_lvl):
        """
            @note: Used for levelize all rows for collapse rows.
        """
        sheet.set_row(rowx, None, None, {'level': start_lvl})

        return sheet

    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Sale Discount Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_title_float = book.add_format(ReportExcelCellStyles.format_title_float)
        format_title_center = book.add_format(ReportExcelCellStyles.format_title_small)
        format_content_text_footer = book.add_format(ReportExcelCellStyles.format_filter)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)

        # parameters
        rowx = 0
        col = 0
        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('sale_discount_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(_('Sale Discount Report'))
        sheet.set_portrait()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.39, 0.39, 0.39, 0.39)  # 1cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})

        # compute column
        sheet.set_column('A:A', 3)
        sheet.set_column('B:B', 27)
        sheet.set_column('C:C', 12)
        sheet.set_column('D:D', 10)
        sheet.set_column('E:E', 10)
        sheet.set_column('F:F', 10)
        sheet.set_column('G:G', 10)
        sheet.set_row(5, 40)

        local = pytz.UTC
        if self.env.user.tz:
            local = pytz.timezone(self.env.user.tz)

        # create name
        sheet.merge_range(rowx, 0, rowx, 4, _('Company Name: %s') % self.company_id.name, format_filter)
        rowx += 1
        sheet.merge_range(rowx, 0, rowx, 4, report_name.upper(), format_name)
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, 13, _('Duration: %s - %s') % (pytz.utc.localize(datetime.strptime(self.date_from, '%Y-%m-%d')).astimezone(local).strftime('%Y-%m-%d'),
                                                                        pytz.utc.localize(datetime.strptime(self.date_to, '%Y-%m-%d')).astimezone(local).strftime('%Y-%m-%d')),format_filter)

        rowx += 1
            
        sheet.write(rowx, col, _('№'), format_title_center)
        sheet.merge_range(rowx , col+1, rowx, col+3, _('Sale Pricelist Name'), format_title_center)
        sheet.write(rowx, col+4, _('Amount'), format_title_center)
        number = 0
        order_name_row = ''
        if self.pricelist_ids:
            #Сонгосон хөнгөлөлтөөр давтаж байна.
            for pri in self.pricelist_ids:
                orders = self.get_lines(pri.id)
                rowx+=1
                if orders:
                    sheet.merge_range(rowx , col, rowx, col+3, pri.name, format_title_float)
                    sheet.write_formula(rowx, 4, '{=SUM(' + xl_rowcol_to_cell(rowx+1,4)  + ':' + xl_rowcol_to_cell(rowx+len(orders),4) + ')}', format_title_float)
                    for order in orders:
                        discount_amount_row = 0
                        order_obj = self.env['sale.order'].browse(order['id'])
                        number+=1
                        rowx+=1
                        for line in order_obj.order_line:
                            price2 = price = price3 = procedure_price = pro_price = p_type = 0
                            if line.product_id:
                                price2 = line.product_id.price_compute('list_price')[line.product_id.id]
                                price_tmp = pri._compute_price_rule([(line.product_id, line.product_uom_qty, order_obj.partner_id.id)])[line.product_id.id][0]
                                price = pri.currency_id.compute(price_tmp, order_obj.currency_id, round=False)
                                discount_amount_row += price2 - price
                        order_name_row = order_obj.name
                        sheet = self.set_spec_level(sheet, rowx, 1)
                        sheet.write(rowx, col, number, format_content_float)
                        sheet.merge_range(rowx , col+1, rowx, col+3, order_name_row, format_content_float)
                        sheet.write(rowx, col+4, discount_amount_row, format_content_float)
                else:
                    sheet.merge_range(rowx , col, rowx, col+3, pri.name, format_title_float)
                    sheet.write(rowx, col+4, 0, format_title_float)
        else:
            pricelists = self.env['product.pricelist.category'].search([('parent_id', '=', False)])
            #Эцэг ангилалгүй хөнгөлөлтийн ангилалаар давтаж байна.
            for pri in pricelists:
                #Хөнгөлөлтийн эцэг ангилалгүй болох хүртэл давтаж байна.
                while pri:
                    pricelists2 = pri
                    for pricelist in pricelists2:
                        rowx += 1
                        product_pricelist = self.env['product.pricelist'].search([('category_id', '=', pricelist.id)])
                        total_amount = self.get_total_amount(pricelist)
                        sheet.merge_range(rowx , col, rowx, col+3, pricelist.name, format_title_center)
                        sheet.write(rowx, col+4, total_amount, format_title_center)
                        #Хөнгөлөлтөөр давтаж байна.
                        for product_pro in product_pricelist:
                            orders = self.get_lines(product_pro.id)
                            rowx+=1
                            if orders:
                                sheet.merge_range(rowx , col, rowx, col+3, product_pro.name, format_title_float)
                                sheet.write_formula(rowx, 4, '{=SUM(' + xl_rowcol_to_cell(rowx+1,4)  + ':' + xl_rowcol_to_cell(rowx+len(orders),4) + ')}', format_title_float)
                                for order in orders:
                                    order_name_row = ''
                                    discount_amount_row = 0
                                    order_obj = self.env['sale.order'].browse(order['id'])
                                    for line in order_obj.order_line:
                                        order_obj = self.env['sale.order'].browse(order['id'])
                                        for line in order_obj.order_line:
                                            price2 = price = price3 = procedure_price = pro_price = p_type = 0
                                            if line.product_id:
                                                price2 = line.product_id.price_compute('list_price')[line.product_id.id]
                                                price_tmp = product_pro._compute_price_rule([(line.product_id, line.product_uom_qty, order_obj.partner_id.id)])[line.product_id.id][0]
                                                price = product_pro.currency_id.compute(price_tmp, order_obj.currency_id, round=False)
                                                discount_amount_row += price2 - price
                                    number+=1
                                    rowx+=1
                                    sheet = self.set_spec_level(sheet, rowx, 1)
                                    sheet.write(rowx, col, number, format_content_float)
                                    sheet.merge_range(rowx , col+1, rowx, col+3, order_obj.name, format_content_float)
                                    sheet.write(rowx, col+4, discount_amount_row, format_content_float)
                            else:
                                sheet.merge_range(rowx , col, rowx, col+3, product_pro.name, format_title_float)
                                sheet.write(rowx, col+4, 0, format_title_float)
                    pri = self.env['product.pricelist.category'].search([('parent_id', '=', pricelist.id)])
        
        sheet.set_zoom(100)
        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()
