# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Product Quantity Repair tool",
    'version': '1.0',
    'depends': ['l10n_mn_product'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
        Бараа материалын гарт байгаа тоо хэмжээг засах
    """,
    'data': [
        'wizard/set_product_quantity_from_location_report_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
