# -*- coding: utf-8 -*-
import base64
import xlrd
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from tempfile import NamedTemporaryFile 

class SetProductQuantityFromLocationReport(models.TransientModel):
    _name = 'set.product.quantity.from.location.report'
    _description = 'Product quantity repair tool'
    
    data_file = fields.Binary('Product Data File', required=True)
    column = fields.Char('Column')
    location_id = fields.Many2one('stock.location', 'Location', required=True)
    warehouse_id = fields.Many2one('stock.warehouse', 'Warehouse')
    
    @api.onchange('warehouse_id')
    def onchange_warehouse_id(self):
        self.location_id = False
        if self.warehouse_id:
            if self.warehouse_id.lot_stock_id:
                location_ids = [(self.warehouse_id.lot_stock_id.id)]
                return {'domain':{'location_id':[('id', 'in', location_ids)]}}
    
    @api.multi
    def import_data(self):
        try:
            fileobj = NamedTemporaryFile('w+')
            fileobj.write(base64.decodestring(self.data_file))
            fileobj.seek(0)
            book = xlrd.open_workbook(fileobj.name)
        except ValueError:
            raise UserError(_('Error loading data file.\nPlease try again!'))
        
        product_obj = self.env['product.product']
        lot_obj = self.env['stock.production.lot']
        
        self.env.cr.execute("SELECT product_id FROM stock_quant WHERE location_id = '" + str(self.location_id.id) + "' GROUP BY product_id")
        fetched_ids = self.env.cr.dictfetchall()
        
        all_product_ids = []
        for pid in fetched_ids:
            all_product_ids.append(pid['product_id'])
        products = {}
        for sheet in book.sheets():
            is_product = False
            for rowi in range(sheet.nrows):
                row_data = sheet.row(rowi)
                if len(row_data) < 5:
                    raise UserError(_("Unloaded file column. There should be columns such as №, code, product name, series, and quantity etc."))
                entry = row_data[1].value
                lot = row_data[3].value
                qty = row_data[4].value
                if not entry:
                    entry = is_product  
                if entry:
                    product_ids = product_obj.search([('default_code', '=', entry)])
                    if product_ids:
                        is_product = "[" + entry + "]"
                        for product in product_ids:
                            lot_id = lot_obj.search([('name', '=', lot), ('product_id', '=', product.id)])
                            if lot_id and qty > 0.0:
                                lot_id = lot_id[0]
                                self.env.cr.execute("SELECT sum(qty) FROM stock_quant WHERE lot_id = '" + str(lot_id.id) + 
                                           "' AND product_id = '" + str(product.id) + "' AND location_id = '" + str(self.location_id.id) + "'")
                                fetch = self.env.cr.dictfetchall()
                                """Агуулахын сонгосон байрлал дээрх барааны нийт тоо ширхгийг серээр нь бүлэглэн олж байна."""
                                if fetch != False and fetch != None and fetch != []:   
                                    if fetch[0]['sum'] != None:                                  
                                        diff = qty - fetch[0]['sum']
                                        if diff != 0:
                                            """Нөөцийн тайлангийн үлдэгдэл quant-аас их үед quant-ийг нөөцийн тайлангийн үлдэгдлээр засаж байна."""                                      
                                            self.env.cr.execute("INSERT INTO stock_quant (lot_id,product_id,location_id,qty,company_id) VALUES ('" + str(lot_id.id) + "','" + str(product.id) + "','" + str(self.location_id.id) + "','" + str(diff) + "',1)")
                                            self.env.cr.commit()
                                    else:
                                        self.env.cr.execute("INSERT INTO stock_quant (lot_id,product_id,location_id,qty,company_id) VALUES ('" + str(lot_id.id) + "','" + str(product.id) + "','" + str(self.location_id.id) + "','" + str(qty) + "',1)")
                                        self.env.cr.commit()
                                if products.has_key(product.id):
                                    products[product.id].append(lot_id)
                                else:
                                    products.update({product.id:lot_id})
                            elif not lot:
                                self.env.cr.execute("SELECT sum(qty) FROM stock_quant WHERE lot_id is null AND product_id = '" + str(product.id) + 
                                                    "' and location_id = '" + str(self.location_id.id) + "'")
                                fetch = self.env.cr.dictfetchall()
                                """Хэрэв сиергүй бараа байвал"""
                                if fetch != False and fetch != None and fetch != []:
                                    if fetch[0]['sum'] != None:
                                        diff = qty - fetch[0]['sum']
                                        if diff != 0:
                                            """Нөөцийн тайлангийн үлдэгдэл quant-аас их үед quant-ийг нөөцийн тайлангийн үлдэгдлээр засаж байна."""                                      
                                            self.env.cr.execute("INSERT INTO stock_quant (product_id,location_id,qty,company_id) VALUES ('" + str(product.id) + "','" + str(self.location_id.id) + "','" + str(diff) + "',1)")
                                            self.env.cr.commit()   
                                    else:
                                        self.env.cr.execute("INSERT INTO stock_quant (product_id,location_id,qty,company_id) VALUES ('" + str(product.id) + "','" + str(self.location_id.id) + "','" + str(qty) + "',1)")
                                        self.env.cr.commit()
        return True
        
        