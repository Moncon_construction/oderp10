# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2016 Asterisk Technologies LLC Co.,ltd
#    (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462
#
##############################################################################

from odoo import api, fields, models, _

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    lenght = fields.Float('Lenght')
    height = fields.Float('Height')
    width = fields.Float('Width')
    net_weight = fields.Float('Net Weight')