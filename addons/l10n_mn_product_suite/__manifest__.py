# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-Today Asterisk Technologies LLC Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

{
    'name': "Product Suite",
    'version': '1.0',
    'depends': ['stock'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules/Product',
    'description': """
       Барааны иж бүрдэл
    """,
    'data': [
        'views/product_suite_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
