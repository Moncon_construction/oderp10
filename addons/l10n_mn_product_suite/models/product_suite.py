# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2016 Asterisk Technologies LLC Co.,ltd
#    (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462
#
##############################################################################

from odoo import api, fields, models, _

#Inherited object
class ProductSuite(models.Model):
    _name = 'product.suite'
    
    code = fields.Char('Code', size=64, required=True)
    name = fields.Char('Name', size=128, required=True)
    list_price = fields.Float('List Price')
    supplier_id = fields.Many2one('res.partner','Supplier', required=True, domain=[('supplier','=',True)])
    
    image = fields.Binary("Photo", attachment=True,
                          help="This field holds the image used as photo for the product suite, limited to 1024x1024px.")
    image_medium = fields.Binary("Medium-sized photo", attachment=True,
                                 help="Medium-sized photo of the asset. It is automatically "
                                 "resized as a 128x128px image, with aspect ratio preserved. "
                                 "Use this field in form views or some kanban views.")
    image_small = fields.Binary("Small-sized photo", attachment=True,
                                help="Small-sized photo of the asset. It is automatically "
                                "resized as a 64x64px image, with aspect ratio preserved. "
                                "Use this field anywhere a small image is required.")
    
    _sql_constraints = [
        ('product_suite_code_unique', 'unique(code)', _('The product suite code must be unique!')),
    ]
    
class ProductTemplate(models.Model):
    _inherit = 'product.template'

    suite_id = fields.Many2one('product.suite', 'Product Suite')
    