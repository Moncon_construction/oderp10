# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Product Transaction Import Tool",
    'version': '1.0',
    'depends': ['base', 'stock', 'product', 'l10n_mn_purchase', 'sale'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
        * Import purchase
        * Import sales
    """,
    'data': [
                "views/import_purchase_view.xml",
                "views/import_sale_view.xml",
                "views/import_transit_view.xml"
            ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
