# -*- coding: utf-8 -*-
import logging
import base64
import xlrd
from odoo import fields, api, models, _
from tempfile import NamedTemporaryFile
from odoo.exceptions import UserError
import datetime
from datetime import date
from datetime import datetime

_logger = logging.getLogger(__name__)

class ImportSale(models.TransientModel):
    _name = 'import.sale'
    _description = 'Sale Import Tool'
    '''
        Борлуулалтын захиалгыг Excel файлаас импортлох
    '''
    data = fields.Binary(string='Sale Import File', required=True)

    def import_data(self):
        obj = self.browse(self.id)
        try:
            fileobj = NamedTemporaryFile('w+')
            fileobj.write(base64.decodestring(obj.data))
            fileobj.seek(0)
            file_name = fileobj.name
            book = xlrd.open_workbook(file_name)
        except:
            raise UserError(_(u'Мэдээллийн файлыг уншихад алдаа гарлаа.\nЗөв файл эсэхийг шалгаад дахин оролдоно уу!'))

        sheet = book.sheet_by_index(0)
        nrows = sheet.nrows

        rowi = 1
        data = {}
        product_problem = []
        product_ids = []
        partner_ids = []
        warehouse_ids = []
        not_found_products = []
        not_found_partners = []
        not_found_warehouses = []
        in_type_not_found_warehouses = []

        while rowi < nrows:
            try:
                row = sheet.row(rowi)
                doc_num = row[0].value
                date_order = row[1].value
                partner_code = row[2].value
                warehouse_name = row[4].value
                product_code = row[6].value
                product_qty = row[7].value
                price_unit = row[8].value

                if not doc_num:
                    raise UserError(_(u'Энэ мөрийн Баримтын дугаар багана хоосон байна: %s' % rowi))

                if not date_order:
                    raise UserError(_(u'Энэ мөрийн Огноо багана хоосон байна: %s' % rowi))
                if not product_qty:
                    raise UserError(_(u'Энэ мөрийн Тоо хэмжээ багана хоосон байна: %s' % rowi))
                if not price_unit:
                    raise UserError(_(u'Энэ мөрийн Нэгж өртөг багана хоосон байна: %s' % rowi))
                if product_code:
                    product_id = obj.env['product.product'].search([('default_code','=',int(product_code))])
                    if not product_id:
                        not_found_products.append(product_code)
                    product_ids.append(product_id)

                if partner_code:
                    partner_id = obj.env['res.partner'].search([('ref', '=', partner_code)])
                    if not partner_id and partner_code not in not_found_partners:
                        not_found_partners.append(partner_code)
                    partner_ids.append(partner_id)

                if warehouse_name:
                    warehouse_id = obj.env['stock.warehouse'].search([('name','=',warehouse_name)])
                    if not warehouse_id:
                        not_found_warehouses.append(warehouse_name)
                    if not warehouse_id.in_type_id:
                        in_type_not_found_warehouses.append(warehouse_name)

                    warehouse_ids.append(warehouse_id)

                rowi += 1
            except IndexError:
                raise UserError(_('Excel sheet must be 2 columned : error on row: %s ' % rowi))

        if not_found_products:
            raise UserError(_(u'Дараах кодтой бараанууд системд бүртгэгдээгүй байна. %s !' % (not_found_products)))
        
        if not_found_warehouses:
            raise UserError(_(u'Дараах агуулахууд системд бүртгэгдээгүй байна. %s !' % (not_found_warehouses)))
        
        if not_found_partners:
            raise UserError(_(u'Дараах кодтой харилцагч нар системд бүртгэгдээгүй байна. %s !' % (not_found_partners)))
        
        if in_type_not_found_warehouses:
            raise UserError(_(u'Эдгээр агуулахууд дээр оролтын байрлал бүртгэгдээгүй байна. %s !' % (in_type_not_found_warehouses)))
        
#         pricelist_id = obj.env['product.pricelist'].search([('type','=','sale')])
#         if not pricelist_id:
#             raise osv.except_osv(u'Алдаа',u'Борлуулалтын үнийн хүснэгт бүртгэгдээгүй байна. %s !')
        
        current_doc_num = None
        rowi = 1
        while rowi < nrows:
            try:
                row = sheet.row(rowi)
                doc_num = row[0].value
                date_order = row[1].value
                partner_code = row[2].value
                warehouse_name = row[4].value
                note = row[5].value
                product_code = row[6].value
                product_qty = row[7].value
                price_unit = row[8].value
                discount = row[10].value
                tax = row[11].value
                product_id = obj.env['product.product'].search([('default_code','=',int(product_code))])
                partner_id = obj.env['res.partner'].search([('ref','=',partner_code)])
                warehouse_id = obj.env['stock.warehouse'].search([('name','=',warehouse_name)])
                tax_ids = False
                if tax:
                    tax_name = tax.split(",")
                    tax_ids = obj.env['account.tax'].search([('name','in',tax_name),('type_tax_use','=','sale')]).ids
                if current_doc_num != doc_num:
                    current_doc_num = doc_num
                    sales_values = {'partner_id': partner_id.id,
                                    'warehouse_id': warehouse_id.id,
                                    'date_order': datetime.strptime(date_order, '%Y-%m-%d'),
#                                     'pricelist_id': pricelist_id.id,
                                    'company_id': obj.env.user.company_id.id,
                                    'notes': ('%s %s')% ((note if note else ''),(doc_num if doc_num else '')),
                    }
                    sale_id = obj.env['sale.order'].create(sales_values)
                    
                order_line_values = {
                    'product_id': product_id.id,
                    'product_uom': product_id.uom_id.id,
#                     'product_uos': product_id.uom_id.id,
                    'name': product_id.name,
#                     'product_uos_qty': product_qty,
                    'product_uom_qty': product_qty,
                    'price_unit': price_unit,
                    'tax_id': [[4,[tax_ids]]] if tax_ids else False,
                    'order_id': sale_id.id,
                    'date_planned': datetime.strptime(date_order, '%Y-%m-%d'),
                    'discount':discount if discount else False,
                    }
#                 tax_name = False
                line_id = obj.env['sale.order.line'].create(order_line_values)
                
                rowi += 1
            except IndexError:
                raise UserError(_('Error', 'Excel sheet must be 2 columned : error on row: %s ' % rowi))