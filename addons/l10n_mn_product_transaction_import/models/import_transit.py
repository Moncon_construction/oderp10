# -*- coding: utf-8 -*-
import logging
import base64
import xlrd
from odoo import fields, api, models, _
from tempfile import NamedTemporaryFile
from odoo.exceptions import UserError
from datetime import date

_logger = logging.getLogger(__name__)


class import_purchase(models.TransientModel):
    _name = 'import.stock.transit.order'
    _description = 'Purchase Import Tool'
    '''
        Нөхөн дүүргэлтийн захиалгыг Excel файлаас импортлох
    '''
    data = fields.Binary(string='Stock Transit Order Import File', required=True)
    supply_warehouse_id = fields.Many2one('stock.warehouse', string='Supply Warehouse', required=True)
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse', required=True)    
    date = fields.Datetime(string='Date', required=True)

    def import_data(self):
        obj = self.browse(self.id)
        try:
            fileobj = NamedTemporaryFile('w+')
            fileobj.write(base64.decodestring(obj.data))
            fileobj.seek(0)
            file_name = fileobj.name
            book = xlrd.open_workbook(file_name)
        except:
            raise UserError(_(u'Мэдээллийн файлыг уншихад алдаа гарлаа.\nЗөв файл эсэхийг шалгаад дахин оролдоно уу!'))

        sheet = book.sheet_by_index(0)
        nrows = sheet.nrows

        rowi = 1
        product_ids = []
        not_found_products = []

        while rowi < nrows:
            try:
                row = sheet.row(rowi)
                barcode = row[1].value
                qty = row[2].value

                if not barcode:
                    raise UserError(_(u'Энэ мөрийн Зураасан код дугаар багана хоосон байна: %s' % rowi))
                if not qty:
                    raise UserError(_(u'Энэ мөрийн Тоо хэмжээ багана хоосон байна: %s' % rowi))
                if barcode:
                    product_id = self.env['product.product'].search([('barcode', '=', barcode)])
                    if not product_id:
                        not_found_products.append(barcode)
                    product_ids.append(product_id)
                rowi += 1
            except IndexError:
                raise UserError(_('Excel sheet must be 2 columned : error on row: %s ' % rowi))

        if not_found_products:
            raise UserError(_(u'Дараах кодтой бараанууд системд бүртгэгдээгүй байна. %s !' % (not_found_products)))
        
        supply_picking_type_id = False
        receive_picking_type_id = False
        if self.warehouse_id:
            stock_picking_type = self.env['stock.picking.type'].search([('warehouse_id', '=', self.warehouse_id.id), ('code', '=', 'incoming')], limit=1)
            if stock_picking_type:
                receive_picking_type_id = stock_picking_type.id

        if self.supply_warehouse_id:
            stock_picking_type = self.env['stock.picking.type'].search([('warehouse_id', '=', self.supply_warehouse_id.id), ('code', '=', 'outgoing')], limit=1)
            if stock_picking_type:
                supply_picking_type_id = stock_picking_type.id
                    
        transit_values = {
                'supply_warehouse_id': self.supply_warehouse_id.id,
                'warehouse_id': self.warehouse_id.id,
                'supply_picking_type_id': supply_picking_type_id,
                'receive_picking_type_id': receive_picking_type_id,
                'date': date,
            }
        transit_id = obj.env['stock.transit.order'].create(transit_values)
        if transit_id:
            rowi = 1
            while rowi < nrows:
                try:
                    row = sheet.row(rowi)
                    default_code = row[1].value
                    qty = row[2].value
                    product_id = self.env['product.product'].search([('barcode', '=', default_code)])
    
                    order_line_values = {
                        'product_id':product_id.id,
                        'product_qty':qty,
                        'name':product_id.display_name,
                        'product_uom':product_id.uom_id.id,
                        'transit_order_id':transit_id.id,
                        
                        }
                    obj.env['stock.transit.order.line'].create(order_line_values)
                     
                    rowi += 1
                except IndexError:
                    raise UserError(_('Error', 'Excel sheet must be 2 columned : error on row: %s ' % rowi))
