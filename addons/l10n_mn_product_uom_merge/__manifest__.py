# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian UOM merger",
    'version': '1.0',
    'depends': [
        'l10n_mn_stock',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'summary': 'Mongolian features for UOM. This module is for merging same UOM-s. ',
    'description': """
        Давхцаж үүссэн хэмжих нэгжийг нэгтгэх хэрэгсэл.
    """,
    'data': [
        'security/uom_merge_group.xml',
        'wizard/uom_merge_wizard.xml'
    ],
}
