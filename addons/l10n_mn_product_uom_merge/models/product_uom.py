# -*- coding: utf-8 -*-

from odoo import fields, _, models, api
from odoo.exceptions import UserError


class ProductUOM(models.Model):
    _inherit = "product.uom"

    @api.multi
    def check_uoms_for_merge(self):
        if not self.env.user.has_group('l10n_mn_product_uom_merge.group_uom_merger'):
            raise UserError(_("You do not have access to this function. Please contact your system administrator\n Need access: Unit of measure merge "))

        category_id = self[0].category_id
        uom_type = self[0].uom_type
        if len(self) <= 1:
            raise UserError(_("You must select at least 2 records"))
        if self.filtered(lambda uom: uom.category_id.id != category_id.id or uom.uom_type != uom_type):
            raise UserError(_("Selected records' category and type must be same. Please check their category and type."))
        view = self.env.ref('l10n_mn_product_uom_merge.product_uom_merge_view')
        return {
            'name': _('Merging view'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'context': self.env.context,
            'res_model': 'product.uom.merge',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'target': 'new'
        }
