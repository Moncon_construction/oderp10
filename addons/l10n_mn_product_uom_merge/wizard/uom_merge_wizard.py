# -*- coding: utf-8 -*-

from odoo import fields, _, models, api
from odoo.exceptions import UserError


class ProductUomMerge(models.TransientModel):
    _name = "product.uom.merge"

    def _domain_uom_ids(self):
        if self.env.context.get('active_ids'):
            return [('id', 'in', self.env.context.get('active_ids'))]

    product_uom_id = fields.Many2one('product.uom', string=" Product UOM", domain=_domain_uom_ids)

    def merge(self):
        uoms_to_delete = self.product_uom_id.browse(self.env.context.get('active_ids')) - self.product_uom_id
        self._cr.execute("""
                SELECT  tc.table_name, kcu.column_name
                    FROM information_schema.table_constraints tc
                JOIN information_schema.key_column_usage kcu ON tc.constraint_name = kcu.constraint_name
                JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_name = tc.constraint_name
                WHERE constraint_type = 'FOREIGN KEY'
                AND ccu.table_name='product_uom'
        """)
        tables = self._cr.dictfetchall()
        for table in tables:
            qry = """
                UPDATE %s SET %s=%s WHERE %s in %s
            """ % (table['table_name'], table['column_name'], self.product_uom_id.id, table['column_name'], self.list_to_str_tuple(uoms_to_delete.ids))
            self._cr.execute(qry)
        uoms_to_delete.unlink()

    def list_to_str_tuple(self, value):
        return str(value).replace('[', '(').replace(']', ')')