# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Project",
    'version': '1.0',
    'depends': [
        'project',
        'hr_timesheet'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Project Modules',
    'summary': 'Mongolian Project Additional Features',
    'description': """
        Ажлын бодит явцыг харуулна.1-100 хүртлэх утгаар явцыг үнэлгээг оруулж өгч болно.
    """,
    'data': [
        'security/security.xml',
        'views/project_views.xml',
        'views/project_date_cron.xml',
        'views/project_task_view.xml',
        'wizard/task_info_change_view.xml'
    ],
}
