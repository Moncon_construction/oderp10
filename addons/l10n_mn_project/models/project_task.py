# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime, timedelta
from odoo.http import request


class ProjectTaskType(models.Model):
    _inherit = 'project.task.type'
    
    closed = fields.Selection([('cancel', 'Canceled'),
                               ('done', 'Done')], string='Closed?', help='The task at this stage is done')
    main_stage = fields.Boolean('Main Stage')

class ProjectTask(models.Model):
    _inherit = 'project.task'

    # change fields
    date_start = fields.Date(string='Start Date')  # Төрлийг datetime->date болгож сольсон
    planned_hours = fields.Float(track_visibility='onchange')
    date_deadline = fields.Date(track_visibility='onchange')
    stage_id = fields.Many2one(ondelete='restrict')

    # additional fields
    actual_process = fields.Integer(compute='_compute_actual_process', string='Actual process')
    progress_value = fields.Integer(string='Progress Value', default=0)

    @api.onchange('progress_value')
    def _compute_actual_process(self):
        for order in self:
            if order.child_ids:
                count = 1
                process = 0
                for child_task in order.child_ids:
                    count = len(order.child_ids)
                    process += (child_task.progress_value) / count
                order.write({'progress_value': process})
            order.actual_process = order.progress_value

    @api.model
    def create(self, vals):
        task = super(ProjectTask, self).create(vals)
        if self.progress_value > 100:
            raise ValidationError(_("Your value has been exceeded. Enter a value of 1-100."))
        elif self.progress_value < 0:
            raise ValidationError(_("Your value must be greater than 0."))
        return task

    @api.multi
    def write(self, vals):
        if 'progress_value' in vals:
            if vals['progress_value'] > 100:
                raise ValidationError(_("Your value has been exceeded. Enter a value of 1-100."))
            elif vals['progress_value'] < 0:
                raise ValidationError(_("Your value must be greater than 0."))

        task = super(ProjectTask, self).write(vals)
        return task

    @api.constrains('date_start', 'date_deadline')
    def _check_deadline(self):
        if any(self.filtered(
                lambda task: task.date_start and task.date_deadline and task.date_start > task.date_deadline)):
            raise ValidationError(_('Error ! Task starting date must be lower than its deadline.'))

    def check_project_deadline(self):
        start = datetime.now() + timedelta(days=1)
        stop = start + timedelta(days=2)
        upcoming_expire_tasks = self.env['project.task'].search(
            [('date_deadline', '<=', stop), ('date_deadline', '>=', start)])
        today_expire_tasks = self.env['project.task'].search([('date_deadline', '=', datetime.now())])
        user_ids = []
        emails = []

        # Даалгаварын хугацаа дуусахад 3-с багагүй хоног үлсдэн тохиолдолд дагаж байгаа хэрэглэгчид имайл илгээж байгаа
        if upcoming_expire_tasks:
            for value in upcoming_expire_tasks:
                if value.date_deadline:
                    if value.message_follower_ids:
                        user_ids = value.message_follower_ids
                        object_name = str('project.task')
                        db_name = request.session['db']
                        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
                        current_url = unicode(base_url) + '/web?db=' + unicode(db_name) + '#id=' + str(
                            value.id) + '&view_type=form&model=' + object_name
                        if user_ids:
                            for user in user_ids:
                                outgoing_email = self.env['ir.mail_server'].sudo().search([])
                                if not outgoing_email:
                                    raise UserError(_(
                                        'There is no configuration for outgoing mail server. Please contact system administrator.'))
                                else:
                                    outgoing_email = self.env['ir.mail_server'].sudo().search(
                                        [('id', '=', outgoing_email[0].id)])
                                    vals = {
                                        'state': 'outgoing',
                                        'subject': 'Таны даалгаварын хугацаа дуусах дөхсөн байна.',
                                        'body_html': '<p>Сайн байна уу, <br/></p>'
                                                     '<p>"' + str(
                                            value.name) + '" - энэ ажлын дуусах хугацаа   "' + str(
                                            value.date_deadline) + '" -өдөр дуусна!  <br/><br/></p>'
                                                     + '<b><a href="' + str(
                                            current_url) + '" target="_blank">Холбоос</a></b>'
                                                     + '<p>Баярлалаа,<br/> -- <br/>ERP Автомат Имэйл </p>',
                                        'email_to': user.partner_id.email,
                                        'email_from': outgoing_email.smtp_user,
                                    }

                                    emails.append(self.env['mail.mail'].create(vals))

        # өнөөдөр дуусаж байгаа ажилуудыг үүсгэсэн ажилчидад имайл явуулах
        if today_expire_tasks:
            for create in today_expire_tasks:
                if create.date_deadline:
                    if create.create_uid:
                        user_ids1 = create.create_uid
                        object_name = str('project.task')
                        db_name = request.session['db']
                        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
                        current_url = unicode(base_url) + '/web?db=' + unicode(db_name) + '#id=' + str(
                            create.id) + '&view_type=form&model=' + object_name
                        if user_ids1:
                            for user in user_ids1:
                                outgoing_email = self.env['ir.mail_server'].sudo().search([])
                                if not outgoing_email:
                                    raise UserError(_(
                                        'There is no configuration for outgoing mail server. Please contact system administrator.'))
                                else:
                                    outgoing_email = self.env['ir.mail_server'].sudo().search(
                                        [('id', '=', outgoing_email[0].id)])
                                    vals = {
                                        'state': 'outgoing',
                                        'subject': 'Таны үүсгэсэн даалгаврын хугацаа өнөөдөр дуусж байна.',
                                        'body_html': '<p>Сайн байна уу, <br/></p>'
                                                     '<p>"' + str(
                                            create.name) + '" - энэ ажил өнөөдөр дуусаж байна!<br/><br/></p>' + '<b><a href="' + str(
                                            current_url) + '" target="_blank">Холбоос</a></b>' + '<p>Баярлалаа,<br/> -- <br/>ERP Автомат Имэйл </p>',
                                        'email_to': user.partner_id.email,
                                        'email_from': outgoing_email.smtp_user,
                                    }

                                    emails.append(self.env['mail.mail'].create(vals))

        if emails:
            for email in emails:
                email.send()

    @api.multi
    def cron_project_date(self):
        return self.check_project_deadline()

    @api.multi
    def unlink(self):
        account_analytic_line = self.env['account.analytic.line'].search([('task_id', '=', self.ids)])
        for analytic_line in account_analytic_line:
            if analytic_line.sheet_id:
                hr_time_sheet = self.env['hr_timesheet_sheet.sheet'].search([('id', '=', analytic_line.sheet_id.id)])
                if hr_time_sheet.state in ('confirm', 'done'):
                    raise UserError(_('You cannot delete project which is already confirmed or done.'))
        return super(ProjectTask, self).unlink()


class Project(models.Model):
    _inherit = 'project.project'

    @api.model
    def create(self, vals):
        res = super(Project, self).create(vals)
        task_type_ids = self.env['project.task.type'].search([('main_stage', '=', True)])
        stage_ids = []
        for task in task_type_ids:
            for project in task.project_ids:
                stage_ids.append(project.id)
        for super_task in res:
            stage_ids.append(super_task.id)
        for type in task_type_ids:
            type.write({'project_ids': [(6, 0, stage_ids)]})
        return res

    @api.multi
    def _task_done_count(self):
        for obj in self:
            tasks = self.env['project.task'].search([('project_id', 'in', [obj.id])])
            count = 0
            for task in tasks:
                if task.stage_id.closed == 'done':
                    count += 1
            obj.task_done_count = count

    @api.multi
    def _task_open_count(self):
        for obj in self:
            tasks = self.env['project.task'].search([('project_id', 'in', [obj.id])])
            count = 0
            for task in tasks:
                if task.stage_id.closed != 'done':
                    count += 1
            obj.task_open_count = count

    @api.depends('task_done_count', 'task_count')
    @api.multi
    def _progress_rate(self):
        for obj in self:
            if obj.task_count != 0:
                obj.progress_rate = obj.task_done_count / obj.task_count * 100
            else:
                obj.progress_rate = 0.0

    @api.multi
    def _is_project_head(self):
        for obj in self:
            user = self.env['res.users'].search([('id', '=', self._uid)])
            obj.is_project_head = True if user.has_group('l10n_mn_project.group_project_head') else False

    task_done_count = fields.Integer(compute='_task_done_count', string="Done tasks", store=True)
    label_done_tasks = fields.Char(string=u'Done Tasks',
                                   help="Gives label to done tasks on project's kanban view.")
    task_open_count = fields.Integer(compute='_task_open_count', string="Open tasks", store=True)
    label_open_tasks = fields.Char(string=u'Open Tasks',
                                   help="Gives label to open tasks on project's kanban view.")
    progress_rate = fields.Float(compute='_progress_rate', string='Progress Rate',store=True)
    label_progress_rate = fields.Char(string=u'Progress rate',
                                      help="Gives label to progress rate on project's kanban view.")
    is_project_head = fields.Boolean('Is Project Head', compute=_is_project_head)
    project_budget = fields.Float('Project Budget')
    state = fields.Selection([('draft', 'Draft'),
                              ('in_progress', 'In Progress'),
                              ('warranty', 'Warranty'),
                              ('waiting', 'Waiting'),
                              ('done', 'Done'),
                              ('closed', 'Closed')],
                             string='Status', required=True, copy=False, default='draft')
