# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################
from odoo import api, fields, models, _


class TaskInfoChange(models.TransientModel):
    _name = 'task.info.change'
    _description = 'Wizard for task info change'

    project_id = fields.Many2one('project.project', string='Project')
    user_id = fields.Many2one('res.users', string='Assigned to')
    planned_hours = fields.Float(string='Initially Planned Hours')
    date_start = fields.Date(string='Start Date')
    date_deadline = fields.Date(string='Deadline')
    tag_ids = fields.Many2many('project.tags', string='Tags')
    add_tag = fields.Boolean('Add', help='If checked the selected tags will be added.', default=False)

    @api.multi
    def task_info_change(self):
        context = dict(self._context or {})
        task_ids = self.env['project.task'].browse(context.get('active_ids'))
        for task in task_ids:
            if self.project_id:
                task.project_id = self.project_id
            if self.user_id:
                task.write({'user_id':self.user_id.id})
            if self.planned_hours:
                task.planned_hours = self.planned_hours
            if self.date_deadline:
                task.date_deadline = self.date_deadline
            if self.date_start:
                task.write({'date_start': self.date_start})
            if self.add_tag:
                if self.tag_ids:
                    for id in self.tag_ids.ids:
                        task_ids.write({'tag_ids': [(4, id)]})
            else:
                if self.tag_ids:
                    task.tag_ids = self.tag_ids
