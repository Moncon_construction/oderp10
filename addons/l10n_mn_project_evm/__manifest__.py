# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Project",
    'version': '1.0',
    'depends': [
        'l10n_mn_project', 'l10n_mn_project_plan'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Project Modules',
    'summary': 'Mongolian Project Additional Features',
    'description': """
        Төслийн төлөвлөгөө
    """,
    'data': [
        #'security/security.xml',
        'views/project_plan_view.xml',
    ],
}
