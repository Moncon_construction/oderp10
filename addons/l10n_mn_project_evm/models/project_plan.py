# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError

class project(models.Model):
    _inherit = "project.project"
    progress = fields.Float('Progress', digits=(16, 2))
    planning_budget = fields.Float('Planning Budget', digits=(16, 2))

class ProjectPlan(models.Model):
    '''Project Plan'''
    _name='project.plan'
    _description = 'Project Plan'
    _rec_name = 'project_id'
    STATE_SELECTION = [
        ('draft','Draft'),
        ('done','Done')
    ]
    
    project_id = fields.Many2one('project.project', 'Project', required=True, readonly=True, states={'draft': [('readonly', False)]})
    start_date = fields.Date('Start date', required=True, readonly=True, states={'draft': [('readonly', False)]})
    end_date = fields.Date('End date', required=True, readonly=True, states={'draft': [('readonly', False)]})
    state = fields.Selection(STATE_SELECTION, 'State', readonly=True, default='draft')
    
    @api.multi
    @api.onchange('project_id')
    def onchange_project_id(self):
        if self.project_id:
            self.update({'start_date':self.project_id.date_start, 'end_date':self.project_id.date})
    
    @api.multi
    def confirm(self):
        for obj in self:
             obj.write({'state':'done'})
        return True
     
class ProjectPlanLine(models.Model):
    '''Project Plan'''
    _name='project.plan.line'
    _description = 'Project Plan Line'
    _rec_name = 'project_id'
    STATE_SELECTION = [
        ('draft','Draft'),
        ('done','Done')
    ]
    
    parent_id = fields.Many2one('project.plan', 'Parent', readonly=True, ondelete="cascade")
    project_id = fields.Many2one('project.project', 'Project')
    
    phase_id = fields.Many2one('project.phase', 'Phase', required=True)
    period_id = fields.Many2one('account.period', 'Period', required=True)
    process_goals = fields.Float('Process Goals %', required=True, digits=(16, 2))

class ProjectPlan(models.Model):
    _inherit='project.plan'
    
    lines = fields.One2many('project.plan.line','parent_id', 'Lines', readonly=True, states={'draft': [('readonly', False)]})

    @api.multi
    def unlink(self):
        for obj in self:
            if obj.state != 'draft':
                raise UserError(_('You can only delete draft record.'))
        return super(ProjectPlan, order).unlink()