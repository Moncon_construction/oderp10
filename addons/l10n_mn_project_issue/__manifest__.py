# -*- coding: utf-8 -*-
{

    'name': 'Mongolian Project Issue',
    'version': '1.1',
    'author': 'Asterisk Technologies LLC',
    'category': 'Project',
    'description': """
        Төслийн асуудал дээр асуудал гаргасан хүнийг бүртгэдэг болгоно.
    """,
    'website': 'http://asterisk-tech.mn/',
    'depends': [
        'project_issue',
        'l10n_mn_project',
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/project_issue_view.xml',
        'views/project_task_views.xml',
        'report/project_issue_report_views.xml',
    ],
    'installable': True,
    'auto_install': False,
}
