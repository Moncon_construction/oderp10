# -*- coding: utf-8 -*-
from odoo import fields, models, api
from datetime import date, datetime, timedelta


class ProjectIssueCategory(models.Model):
    _name = "project.issue.category"

    name = fields.Char('Name', required=True)
    parent_id = fields.Many2one('project.issue.category', string='Issue Category')


class ProjectIssue(models.Model):
    _inherit = "project.issue"

    @api.multi
    @api.depends('date_finished', 'create_date')
    def _compute_aged(self):
        for record in self:
            if record.create_date:
                create_date = datetime.strptime(str(record.create_date)[:10], '%Y-%m-%d')
                if record.date_finished:
                    date_finished = datetime.strptime(str(record.date_finished)[:10], '%Y-%m-%d')
                else:
                    date_finished = datetime.strptime(str(fields.Date.today())[:10], '%Y-%m-%d')
                record.aged = (date_finished - create_date).days

    # changed fields
    date_deadline = fields.Date('Deadline', required=True, copy=False, track_visibility='onchange')
    stage_id = fields.Many2one(ondelete='restrict')

    # additional fields
    issue_categ_id = fields.Many2one('project.issue.category', string='Issue Category')
    issue_owner_ids = fields.Many2many('res.users', 'project_issue_user_rel', 'issue_id', 'user_id', required=True, string='Issue owners')
    is_free_fixed = fields.Boolean(string='Is free fixed')
    date_finished = fields.Datetime('Date finished', readonly=False, store=True)
    aged = fields.Integer(string='Aged', readonly=False, compute='_compute_aged', store=True)

    @api.multi
    def write(self, vals):
        if 'stage_id' in vals and vals['stage_id']:
            stage = self.env['project.task.type'].browse(vals['stage_id'])
            if stage.closed:
                vals.update({'date_finished': fields.Datetime.now()})
            else:
                vals.update({'date_finished': False})
        return super(ProjectIssue, self).write(vals)

    @api.multi
    def create_task(self):
        for issue in self:
            if not issue.task_id:
                created_task = self.env['project.task'].create({
                    'name': issue.name,
                    'project_id': issue.project_id.id,
                    'stage_id': issue.stage_id.id,
                    'user_id': issue.user_id.id,
                    'partner_id': issue.partner_id.id,
                    'tag_ids': issue.tag_ids.ids,
                    'date_deadline': issue.date_deadline,
                    'description': issue.description
                })
                issue.task_id = created_task
                IrAttachment = issue.env['ir.attachment']
                attachments = IrAttachment.search([('res_model', '=', self._name), ('res_id', '=', self.id)])
                for attachment in attachments:
                    IrAttachment.create({
                        'res_model': created_task._name,
                        'res_id': created_task.id,
                        'company_id': self.env.user.company_id.id,
                        'file_size': attachment.file_size,
                        'index_content': attachment.index_content,
                        'type': attachment.type,
                        'public': attachment.public,
                        'store_fname': attachment.store_fname,
                        'description': attachment.description,
                        'mimetype': attachment.mimetype,
                        'name': attachment.name,
                        'url': attachment.url,
                        'checksum': attachment.checksum,
                        'datas_fname': attachment.datas_fname
                    })