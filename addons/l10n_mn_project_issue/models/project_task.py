# -*- coding: utf-8 -*-

# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class ProjectTask(models.Model):
    _inherit = 'project.task'

    issues = fields.One2many('project.issue', 'task_id', string='Related Issues')
    issue_count = fields.Integer(compute='_compute_issue_count', string="Issue Count")

    @api.multi
    def _compute_issue_count(self):
        for task in self:
            task.issue_count = len(task.issues)

    @api.multi
    def write(self, vals):
        res = super(ProjectTask, self).write(vals)

        # update related issue fields
        issue_vals = {}
        if 'project_id' in vals:
            issue_vals.update({'project_id': vals.get('project_id')})
        if 'stage_id' in vals:
            issue_vals.update({'stage_id': vals.get('stage_id')})
        if 'user_id' in vals:
            issue_vals.update({'user_id': vals.get('user_id')})
        if 'partner_id' in vals:
            issue_vals.update({'partner_id': vals.get('partner_id')})
        if 'date_deadline' in vals:
            issue_vals.update({'date_deadline': vals.get('date_deadline')})
        related_issues = self.env['project.issue']
        for task in self:
            related_issues |= task.issues
        related_issues.write(issue_vals)

        return res
