# -*- coding: utf-8 -*-
from odoo import fields, models, api, tools


class ProjectIssueReport(models.Model):
    _inherit = "project.issue.report"

    name = fields.Char(string='Issue', required=True)
    issue_owner_id = fields.Many2one('res.users', 'Issue Owner')
    issue_categ_id = fields.Many2one('project.issue.category', string='Issue Category')
    date_finished = fields.Datetime('Date finished', readonly=False)

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self._cr, 'project_issue_report')
        self._cr.execute("""
            CREATE OR REPLACE VIEW project_issue_report AS (
                SELECT
                    c.id as id,
                    c.date_open as opening_date,
                    c.create_date as create_date,
                    c.name as name,
                    c.issue_categ_id as issue_categ_id,
                    c.date_finished as date_finished,
                    c.date_last_stage_update as date_last_stage_update,
                    c.user_id,
                    c.working_hours_open,
                    c.working_hours_close,
                    c.stage_id,
                    c.date_closed as date_closed,
                    c.company_id as company_id,
                    c.priority as priority,
                    c.project_id as project_id,
                    1 as nbr_issues,
                    c.partner_id,
                    c.day_open as delay_open,
                    c.day_close as delay_close,
                    ru.id as issue_owner_id,
                    (SELECT count(id) FROM mail_message WHERE model='project.issue' AND message_type IN ('email', 'comment') AND res_id=c.id) AS email
                FROM
                    project_issue c
                LEFT JOIN project_task t on c.task_id = t.id
                LEFT JOIN project_issue_user_rel rel on rel.issue_id = c.id
                LEFT JOIN res_users ru on ru.id = rel.user_id
                WHERE c.active= 'true'
            )""")
