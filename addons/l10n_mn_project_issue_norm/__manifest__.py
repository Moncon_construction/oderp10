# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details

{

    'name': 'Mongolian Project Issue Norm',
    'version': '1.1',
    'author': 'Asterisk Technologies LLC',
    'website': 'http://asterisk-tech.mn/',
    'category': 'Project',
    'description': """
        Төслийн асуудал дээр ажилбарын норм сонгодог болсон
                    """,
    'depends': [
        'l10n_mn_project_task_norm',
        'l10n_mn_project_issue'
    ],
    'data': [
        'views/project_issue_norm_view.xml',
        'report/project_issue_report_views.xml',
    ],
    'installable': True,
    'auto_install': True,
}
