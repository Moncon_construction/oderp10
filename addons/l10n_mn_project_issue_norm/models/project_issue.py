# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details

from odoo import fields, models, api


class ProjectIssue(models.Model):
    _inherit = "project.issue"

    norm_id = fields.Many2one('project.task.norm', 'Project Task norm', domain=[('type', '=', 'regular'), ('active', '=', True)], required=True)

    @api.multi
    def create_task(self):
        for issue in self:
            if not issue.task_id:
                created_task = self.env['project.task'].create({
                    'name': issue.name,
                    'project_id': issue.project_id.id,
                    'stage_id': issue.stage_id.id,
                    'user_id': issue.user_id.id,
                    'partner_id': issue.partner_id.id,
                    'tag_ids': issue.tag_ids.ids,
                    'date_deadline': issue.date_deadline,
                    'description': issue.description,
                    'is_norm': True,
                    'norm_id': issue.norm_id.id,
                    'planned_hours': issue.norm_id.time
                })
                issue.task_id = created_task
