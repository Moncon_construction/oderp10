# -*- coding: utf-8 -*-

# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models


class ProjectTask(models.Model):
    _inherit = 'project.task'

    @api.multi
    def write(self, vals):
        res = super(ProjectTask, self).write(vals)

        # update related issue fields
        issue_vals = {}
        if 'norm_id' in vals:
            issue_vals.update({'norm_id': vals.get('norm_id')})
        related_issues = self.env['project.issue']
        for task in self:
            related_issues |= task.issues
        related_issues.write(issue_vals)

        return res
