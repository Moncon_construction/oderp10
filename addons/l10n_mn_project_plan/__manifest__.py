# -*- coding: utf-8 -*-
# Part of OdERP. See LICENSE file for full copyright and licensing details
{

    'name' : 'Mongolian - Project Plan',
    'version' : '1.0',
    'author' : 'Asterisk Technologies LLC',
    'category' : 'Project',
    'description' : """
Project Plan
========================
Төслийн төлөвлөгөө
""",
    'website': 'http://www.asterisk-tech.mn',
    'depends' : ['project', 'l10n_mn_account_period'],
    'data':[
        'views/project_plan_view.xml',
        'security/ir.model.access.csv',
    ],
    'installable': True,
    'auto_install': False,
}
