# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError

class ProjectPlanLine(models.Model):
    '''Project Plan'''
    _name='project.plan.line'
    _description = 'Project Plan Line'
    _order = 'period_id'
    
    STATE_SELECTION = [
        ('draft','Draft'),
        ('done','Done')
    ]
    
    @api.multi
    def _compute_actual_cost(self):
        for obj in self:
            if obj.project_id.analytic_account_id.id:
                cost=0
                moves = self.env['account.analytic.line'].search([('account_id','=',obj.project_id.analytic_account_id.id),
                                                          ('date','<=',obj.period_id.date_stop),
                                                          ('amount','<',0)])
                for move in moves:
                    cost += move.amount
            obj.actual_cost = cost * (-1)
                
    project_id = fields.Many2one('project.project', 'Project')
    period_id = fields.Many2one('account.period', 'Period', required=True)
    progress_goal = fields.Float('Progress Goal %', required=True, digits=(16, 2))
    period_progress = fields.Float('Period Progress %', digits=(16, 2))
    description = fields.Text('Description')
    actual_cost = fields.Float(compute='_compute_actual_cost', string='Actual Cost', digits=(16, 2))
    weight = fields.Float(string='Weight', default=1.0)

class ProjectProject(models.Model):
    _inherit = "project.project"
    
    progress = fields.Float('Progress', digits=(16, 2))
    planning_budget = fields.Float('Planning Budget', digits=(16, 2))
    project_plan_lines = fields.One2many('project.plan.line', 'project_id', 'Project Plan Lines')