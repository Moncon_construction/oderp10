# -*- coding: utf-8 -*-
{
    'name': "Reports for Project Task",
    'version': '1.0',
    'depends': ['report_py3o', 'l10n_mn_project'],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Modules',
    'description': """
        Reports for Project Task. report_py3o module required => https://github.com/tsengel2/reporting-engine.git
    """,
    'data': [
        'project_task_report.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
