# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models, fields
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from datetime import datetime

THOUSANDDECIMAL = "{:,.2f}"


class ProjectTask(models.Model):
    _inherit = "project.task"

    @api.multi
    def get_py3o_report_values(self, report_name, data):
        class TaskLine(object):
            pass

        vals = []
        num = 1
        for task in self:
            line = TaskLine()
            line.name = task.name
            line.assignee = task.user_id.name
            line.project = task.project_id.name
            line.date_start = task.date_start
            line.date_end = task.date_deadline or ""
            line.progress = task.actual_process
            line.planned = task.planned_hours
            line.state = task.stage_id.name
            line.num = num
            num += 1
            line.tags = ""
            for tag in task.tag_ids:
                print tag.name
                line.tags += "%s ," % tag.name
            vals.append(line)
        return {
            'lines': vals,
        }
