# -*- coding: utf-8 -*-
# Part of OdERP. See LICENSE file for full copyright and licensing details
{

    'name': 'Mongolian Project Task Norm',
    'version': '1.1',
    'author': 'Asterisk Technologies LLC',
    'website': 'http://asterisk-tech.mn/',
    'category': 'Project',
    'description': """
        Төслийн даалгавар дээр даалгаврын норм сонгодог болгоно. Даалгаврын нормыг Төслийн тохиргоонд тохируулна.
                    """,
    'depends': [
        'l10n_mn_project',
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/project_task_norm_views.xml',
        'views/project_task_views.xml',
        'wizard/task_info_change_view.xml'
    ],
    'installable': True,
    'auto_install': False,
}
