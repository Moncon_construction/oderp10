# -*- coding: utf-8 -*-
from odoo import api, fields, models


class ProjectTask(models.Model):
    _inherit = "project.task"

    is_norm = fields.Boolean('Is Norm Job')
    norm_id = fields.Many2one('project.task.norm', 'Project Task Norm', domain=[('type', '=', 'regular'),('active','=',True)])

    @api.onchange('norm_id')
    def onchange_norm(self):
        for obj in self:
            if obj.norm_id:
                obj.planned_hours = obj.norm_id.time
                if obj.norm_id.project_id:
                    obj.project_id = obj.norm_id.project_id.id 
