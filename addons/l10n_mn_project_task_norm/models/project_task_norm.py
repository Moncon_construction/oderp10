# -*- coding: utf-8 -*-
from odoo import fields, models

class ProjectTaskNorm(models.Model):
    _name = "project.task.norm"
    _description = 'Project Task Norm'

    name = fields.Char('Name')
    project_id = fields.Many2one('project.project', 'Project')
    parent_id = fields.Many2one('project.task.norm', 'Parent Norm')
    type = fields.Selection([('regular', 'Regular'), ('view', 'View')], string='Type', required=True, index=True)
    time = fields.Float('Time')
    active = fields.Boolean('Active', default=True)
