# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################
from odoo import api, fields, models


class TaskInfoChange(models.TransientModel):
    _inherit = 'task.info.change'

    change_is_norm = fields.Boolean('Change Norm')
    is_norm = fields.Boolean('Is Norm Job')
    norm_id = fields.Many2one('project.task.norm', 'Project Task Norm', domain=[('type', '=', 'regular')])

    @api.multi
    def task_info_change(self):
        super(TaskInfoChange, self).task_info_change()
        context = dict(self._context or {})
        task_ids = self.env['project.task'].browse(context.get('active_ids'))
        for task in task_ids:
            if self.change_is_norm:
                task.is_norm = self.is_norm
                if self.norm_id and self.is_norm:
                    task.write({'norm_id': self.norm_id.id,
                                'planned_hours': self.norm_id.time})
