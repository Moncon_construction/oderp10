# -*- coding: utf-8 -*-
# Part of OdERP. See LICENSE file for full copyright and licensing details
{

    'name' : 'Mongolian - Project Task Plan',
    'version' : '1.0',
    'author' : 'Asterisk Technologies LLC',
    'category' : 'Project',
    'description' : """
Project Plan
========================
Төслийн даалгаварын төлөвлөгөө
""",
    'website': 'http://www.asterisk-tech.mn',
    'depends' : ['l10n_mn_project_task_norm', 'l10n_mn_hr', 'l10n_mn_account_period'],
    'data':[
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/task_plan_views.xml',
        'views/task_user_plan_views.xml',
        'views/task_views.xml',
        'views/task_plan_modify_view.xml',
        'views/task_plan_modify_line_view.xml',
        'wizard/add_plan_task.xml'
    ],
    'installable': True,
    'auto_install': False,
}
