# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime


class TaskPlan(models.Model):
    _name = 'task.plan'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    @api.multi
    def _total_performance_percent(self):
        for obj in self:
            if obj.total_performed_time > 0:
                obj.total_performance_percent = obj.total_performed_time / obj.total_planned_time * 100
            else:
                obj.total_performance_percent = 0

    name = fields.Char('Name', required=True, track_visibility='onchange')
    department_id = fields.Many2one('hr.department', 'Department', required=True, track_visibility='onchange')
    period_id = fields.Many2one('account.period', 'Period', required=True, track_visibility='onchange')
    state = fields.Selection([('draft', 'Draft'),
                              ('send', 'Send'),
                              ('confirm', 'Confirm'),
                              ('done', 'Done')], 'State', default='draft', track_visibility='onchange')
    total_planned_time = fields.Float('Total Planned Time')
    total_performed_time = fields.Float('Total Performed Time')
    total_performance_percent = fields.Float('Total Performance Percent', compute=_total_performance_percent)
    user_plan_ids = fields.One2many('task.user.plan', 'task_plan_id', 'Users Plan')

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        domain = list(domain)
        for index in range(len(domain)):
            if (type(domain[index]) == list):
                if domain[index][2]:
                    if domain[index][2] == 'this_month_plan':
                        date_now = datetime.today()
                        period_id = self.env['account.period'].search([('date_start','<=',date_now),('date_stop','>=',date_now)])
                        plan_ids = self.env['task.plan'].search([('period_id','=',period_id.id)])
                        domain[index] = ('id', 'in', plan_ids.ids)
        return super(TaskPlan, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                      orderby=orderby, lazy=lazy)

    @api.multi
    def calculate_performance(self):
        for obj in self:
            total_planned_time = 0.0
            total_performed_time = 0.0
            for line in obj.user_plan_ids:
                line.calculate_performance()
                total_planned_time += line.planned_time
                total_performed_time += line.performed_time
            obj.total_planned_time = total_planned_time
            obj.total_performed_time = total_performed_time

    @api.multi
    def unlink(self):
        for this in self:
            if this.state not in ('draft'):
                raise UserError(_('You cannot delete plan which is not draft.'))
        return super(TaskPlan, self).unlink()

    @api.onchange('department_id', 'period_id')
    def onchange_type(self):
        if self.department_id and self.period_id:
            self.update(
                {'name': u'%s хэлтэсийн %s -н төлөвлөгөө.' % (self.department_id.name, self.period_id.name)})

    @api.multi
    def action_send(self):
        for obj in self:
            obj.state = 'send'
            obj.line_state('send')

    @api.multi
    def action_draft(self):
        for obj in self:
            if obj.state in ('confirm', 'done'):
                for line in obj.user_plan_ids:
                    for task in line.planned_task_ids:
                        task.create_plan_history('draft_plan', line.id)
            obj.state = 'draft'
            obj.line_state('draft')

    @api.multi
    def action_confirm(self):
        for obj in self:
            obj.state = 'confirm'
            obj.line_state('confirm')

    @api.multi
    def action_done(self):
        for obj in self:
            obj.state = 'done'
            obj.line_state('done')

    @api.multi
    def line_state(self, state):
        for obj in self:
            for line in obj.user_plan_ids:
                if state == 'confirm' and line.state != 'confirm':
                    for task in line.planned_task_ids:
                        task.create_plan_history('planned', line.id)
                if state == 'done' and line.state != 'done':
                    for task in line.planned_task_ids:
                        if task.stage_id.closed == 'done':
                            task.create_plan_history('done', line.id)
                line.state = state

    @api.multi
    def action_create_employee_plan(self):
        for obj in self:
            users = []
            if obj.department_id:
                employees = self.env['hr.employee'].search([('department_id', '=', obj.department_id.id),
                                                            ('state_id.type', 'not in',
                                                             ('resigned', 'maternity', 'retired', 'annual_leave'))])
                for employee in employees:
                    if employee.user_id:
                        users.append(employee.user_id)
            for user in users:
                plan_line = self.env['task.user.plan'].search([('user_id', '=', user.id), ('task_plan_id', '=', obj.id)])
                if not plan_line:
                    vals = {
                        'user_id': user.id,
                        'task_plan_id': obj.id,
                    }
                    self.env['task.user.plan'].create(vals)


class TaskUserPlan(models.Model):
    _name = 'task.user.plan'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            result.append((record.id, "[%s] %s" % (record.task_plan_id.name, record.user_id.name)))
        return result

    def _performance_percent(self):
        for obj in self:
            if obj.planned_time > 0:
                obj.performance_percent = obj.performed_time / obj.planned_time * 100
            else:
                obj.performance_percent = 0

    def _performance_percent_plan(self):
        for obj in self:
            if obj.time_to_plan > 0:
                obj.performance_percent_plan = obj.performed_time / obj.time_to_plan * 100
            else:
                obj.performance_percent_plan = 0

    user_id = fields.Many2one('res.users', 'Employee', required=True)
    task_plan_id = fields.Many2one('task.plan', 'Task Plan')
    department_id = fields.Many2one('hr.department', 'Department')
    period_id = fields.Many2one('account.period', 'Period', required=True)
    time_to_plan = fields.Float('Time to plan')
    planned_time = fields.Float('Planned Time')
    performed_time = fields.Float('Performed Time')
    performance_percent = fields.Float('Performance Percent', compute=_performance_percent)
    performance_percent_plan = fields.Float('Performance Percent(Time to plan)', compute=_performance_percent_plan)
    planned_task_ids = fields.Many2many('project.task', 'project_task_plan_rel', 'plan_id', 'task_id', 'Planned Tasks')
    state = fields.Selection([('draft', 'Draft'),
                              ('send', 'Send'),
                              ('confirm', 'Confirm'),
                              ('done', 'Done')], 'State', default='draft', track_visibility='onchange')

    @api.multi
    def calculate_performance(self):
        for obj in self:
            planned_time = 0.0
            performed_time = 0.0
            for task in obj.planned_task_ids:
                if task.stage_id.closed != 'cancel':
                    if task.is_norm:
                        planned_time += task.norm_id.time
                        if task.stage_id.closed == 'done':
                            performed_time += task.norm_id.time
                    else:
                        planned_time += task.planned_hours
                        if task.stage_id.closed == 'done':
                            performed_time += task.planned_hours
            obj.planned_time = planned_time
            obj.performed_time = performed_time

    @api.multi
    def unlink(self):
        for this in self:
            if this.state not in ('draft'):
                raise UserError(_('You cannot delete plan which is not draft.'))
        return super(TaskUserPlan, self).unlink()

    @api.onchange('task_plan_id')
    def onchange_task_plan_id(self):
        for obj in self:
            obj.department_id = obj.task_plan_id.department_id.id
            obj.period_id = obj.task_plan_id.period_id.id

    @api.model
    def create(self, vals):
        if 'task_plan_id' in vals:
            task_plan = self.env['task.plan'].search([('id', '=', vals['task_plan_id'])])
            vals['department_id'] = task_plan.department_id.id
            vals['period_id'] = task_plan.period_id.id
        return super(TaskUserPlan, self).create(vals)

    @api.multi
    def action_line_confirm(self):
        for obj in self:
            obj.state = 'confirm'
            for task in obj.planned_task_ids:
                task.create_plan_history('planned', obj.id)

    @api.multi
    def action_line_done(self):
        for obj in self:
            obj.state = 'done'
            for task in obj.planned_task_ids:
                task.create_plan_history('done', obj.id)

class ProjectPlanHistory(models.Model):
    _name = 'project.plan.history'

    task_id = fields.Many2one('project.task', 'Task')
    plan_id = fields.Many2one('task.user.plan', 'Plan')
    state = fields.Selection([('planned', 'Planned'),
                              ('done', 'Done'),
                              ('add_plan', 'Add Plan'),
                              ('switch_plan', 'Switch Plan'),
                              ('remove_plan', 'Remove From Plan'),
                              ('old_plan', 'From Old Plan'),
                              ('draft_plan', 'Plan to draft')], 'State')


class ProjectTask(models.Model):
    _inherit = 'project.task'

    plan_id = fields.Many2one('task.user.plan', 'Plan', copy=False)
    modify_id = fields.Many2one('task.plan.modify.line', 'Modify')
    plan_history_ids = fields.One2many('project.plan.history', 'task_id', 'History')

    @api.multi
    def unlink_plan(self):
        for obj in self:
            obj.action_remove_plan()

    @api.multi
    def create_plan_history(self, history, plan):
        for obj in self:
            vals = {
                'task_id': obj.id,
                'plan_id': plan,
                'state': history
            }
            self.env['project.plan.history'].create(vals)

    @api.multi
    def action_add_plan(self):
        return {
            'name': 'Note',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'add.plan.task',
            'context': self._context,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
        }

    @api.multi
    def action_remove_plan(self):
        for obj in self:
            if obj.plan_id.state not in ('draft', 'send'):
                obj.create_plan_history('remove_plan', obj.plan_id.id)
            obj.plan_id.write({
                'planned_task_ids': [(3, obj.id)]
            })
            obj.plan_id = False

    @api.multi
    def unlink(self):
        for this in self:
            if this.plan_id:
                raise UserError(_('You cannot delete planned task.'))
        return super(ProjectTask, self).unlink()



