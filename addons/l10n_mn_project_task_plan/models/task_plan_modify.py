# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime
import time


class TaskPlanModify(models.Model):
    _name = 'task.plan.modify'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    name = fields.Char('Name', required=True, track_visibility='onchange')
    department_id = fields.Many2one('hr.department', 'Department', required=True, track_visibility='onchange')
    period_id = fields.Many2one('account.period', 'Period', required=True, track_visibility='onchange')
    plan_id = fields.Many2one('task.plan', 'Modify Plan', required=True, track_visibility='onchange')
    send_date = fields.Date('Send Date', track_visibility='onchange')
    confirm_date = fields.Date('Confirm Date', track_visibility='onchange')
    state = fields.Selection([('draft', 'Draft'),
                              ('send', 'Send'),
                              ('manager_confirm', 'Manager Confirm'),
                              ('confirm', 'Confirm')], 'State', default='draft', track_visibility='onchange')
    line_ids = fields.One2many('task.plan.modify.line', 'modify_id', string='Modify lines')

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        domain = list(domain)
        for index in range(len(domain)):
            if (type(domain[index]) == list):
                if domain[index][2]:
                    if domain[index][2] == 'this_month_plan_modify':
                        date_now = datetime.today()
                        period_id = self.env['account.period'].search([('date_start','<=',date_now),('date_stop','>=',date_now)])
                        plan_ids = self.env['task.plan.modify'].search([('period_id','=',period_id.id)])
                        domain[index] = ('id', 'in', plan_ids.ids)
        return super(TaskPlanModify, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                      orderby=orderby, lazy=lazy)


    @api.onchange('plan_id')
    def onchange_plan_id(self):
        if self.plan_id:
            self.update({
                'name': u'%s - ний тодотгол.' % (self.plan_id.name),
                'period_id': self.plan_id.period_id.id
                 })

    @api.onchange('department_id')
    def onchange_department_id(self):
        plan_ids = self.env['task.plan'].search([('department_id', '=', self.department_id.id),
                                                 ('state', '=', 'confirm')])
        return {'domain': {'plan_id': [('id', 'in', plan_ids.ids)]}}

    @api.multi
    def action_send(self):
        for obj in self:
            obj.state = 'send'
            obj.send_date = time.strftime('%Y-%m-%d')
            for line in obj.line_ids:
                if line.state == 'draft':
                    line.action_send()

    @api.multi
    def action_confirm_manager(self):
        for obj in self:
            obj.state = 'manager_confirm'
            for line in obj.line_ids:
                if line.state == 'send':
                    line.action_confirm_manager()

    @api.multi
    def action_confirm(self):
        for obj in self:
            obj.state = 'confirm'
            obj.confirm_date = time.strftime('%Y-%m-%d')
            for line in obj.line_ids:
                if line.state == 'manager_confirm':
                    line.action_confirm()

    @api.multi
    def action_create_line(self):
        for obj in self:
            for line in obj.plan_id.user_plan_ids:
                modify_plan_id = self.env['task.plan.modify.line'].search([('modify_id','=',obj.id), ('user_id','=', line.user_id.id)])
                if not modify_plan_id:
                    vals = {
                        'user_id': line.user_id.id,
                        'period_id': line.period_id.id,
                        'department_id': line.department_id.id,
                        'modify_id': obj.id,
                        'user_plan_id': line.id,
                        'before_planned_time': line.planned_time,
                        'state': 'draft',
                    }
                    self.env['task.plan.modify.line'].create(vals)


class TaskPlanModifyLine(models.Model):
    _name = 'task.plan.modify.line'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    @api.multi
    @api.depends('modify_id', 'user_id')
    def name_get(self):
        result = []
        for obj in self:
            name = obj.modify_id.name + u' - ' + obj.user_id.name
            result.append((obj.id, name))
        return result

    user_id = fields.Many2one('res.users', 'Employee', required=True)
    period_id = fields.Many2one('account.period', 'Period', required=True, track_visibility='onchange')
    department_id = fields.Many2one('hr.department', 'Department', required=True, track_visibility='onchange')
    modify_id = fields.Many2one('task.plan.modify', 'Modify')
    user_plan_id = fields.Many2one('task.user.plan', 'Employee Plan', required=True)
    before_planned_time = fields.Float('Before Planned Time')
    after_planned_time = fields.Float('After Planned Time')
    state = fields.Selection([('draft', 'Draft'),
                              ('send', 'Send'),
                              ('manager_confirm', 'Manager Confirm'),
                              ('confirm', 'Confirm')], 'State', default='draft', track_visibility='onchange')
    remove_line_ids = fields.One2many('user.plan.remove.line', 'modify_line', string='Remove lines')
    add_line_ids = fields.One2many('user.plan.add.line', 'modify_line', string='Add lines')
    change_line_ids = fields.One2many('user.plan.change.line', 'modify_line', string='Change lines')

    @api.multi
    def action_line_state(self, state):
        for obj in self:
            for line in obj.remove_line_ids:
                line.state = state
            for line in obj.add_line_ids:
                line.state = state
            for line in obj.change_line_ids:
                line.state = state

    @api.multi
    def action_send(self):
        for obj in self:
            obj.state = 'send'
            obj.action_line_state('send')

    @api.multi
    def action_draft(self):
        for obj in self:
            obj.state = 'draft'
            obj.action_line_state('draft')

    @api.multi
    def action_confirm_manager(self):
        for obj in self:
            obj.state = 'manager_confirm'
            obj.action_line_state('manager_confirm')

    @api.multi
    def action_calculate(self):
        for obj in self:
            obj.user_plan_id.calculate_performance()
            time = obj.user_plan_id.planned_time
            obj.before_planned_time = time
            if obj.remove_line_ids:
                for line in obj.remove_line_ids:
                    if line.is_confirm:
                        time -= line.planned_time
            if obj.add_line_ids:
                for line in obj.add_line_ids:
                    if line.is_confirm:
                        time += line.planned_time
            if obj.change_line_ids:
                for line in obj.change_line_ids:
                    if line.is_confirm:
                        diff = line.time - line.planned_time
                        time += diff
            obj.after_planned_time = time

    @api.multi
    def action_confirm(self):
        for obj in self:
            obj.user_plan_id.calculate_performance()
            obj.before_planned_time = obj.user_plan_id.planned_time
            for line in obj.remove_line_ids:
                if line.is_confirm:
                    line.task_id.action_remove_plan()
                    line.task_id.modify_id = obj.id
            for line in obj.add_line_ids:
                if line.is_confirm:
                    if line.task_id.plan_id:
                        line.task_id.plan_id.write({
                            'planned_task_ids': [(3, line.task_id.id)]
                        })
                        line.task_id.write({
                            'plan_id': obj.user_plan_id.id,
                            'user_id': obj.user_id.id
                        })
                        obj.user_plan_id.write({
                            'planned_task_ids': [(4, line.task_id.id)]
                        })
                        line.task_id.create_plan_history('switch_plan', obj.user_plan_id.id)
                    else:
                        line.task_id.create_plan_history('add_plan', obj.user_plan_id.id)
                        line.task_id.plan_id = obj.user_plan_id.id
                        obj.user_plan_id.write({
                            'planned_task_ids': [(4, line.task_id.id)]
                        })
                    line.task_id.modify_id = obj.id
            for line in obj.change_line_ids:
                if line.is_confirm:
                    if line.change_norm_id:
                        if not line.task_id.is_norm:
                            line.task_id.is_norm = True
                        line.task_id.norm_id = line.change_norm_id
                        line.task_id.planned_hours = line.change_norm_id.time
                    else:
                        if line.task_id.is_norm:
                            line.task_id.is_norm = False
                        line.task_id.planned_hours = line.time
                    line.task_id.modify_id = obj.id
            obj.state = 'confirm'
            obj.action_line_state('confirm')
            obj.user_plan_id.calculate_performance()
            obj.after_planned_time = obj.user_plan_id.planned_time
            

class UserPlanAmendmentRemove(models.Model):
    _name = 'user.plan.remove.line'

    modify_line = fields.Many2one('task.plan.modify.line')
    task_id = fields.Many2one('project.task', 'Task')
    project_id = fields.Many2one('project.project', 'Project')
    planned_time = fields.Float('Planned Time')
    reason = fields.Text('Remove Reason')
    manager_description = fields.Char('Manager Description')
    confirm_description = fields.Char('Confirm Description')
    is_confirm = fields.Boolean('Is Confirm')
    confirm_user = fields.Many2one('res.users','Confirm User')
    user_plan_id = fields.Many2one('task.user.plan', 'Employee Plan', required=True)
    state = fields.Selection([('draft', 'Draft'),
                              ('send', 'Send'),
                              ('manager_confirm', 'Manager Confirm'),
                              ('confirm', 'Confirm')], 'State', default='draft', track_visibility='onchange')

    @api.onchange('task_id')
    def onchange_task_id(self):
        for obj in self:
            if obj.task_id.project_id:
                obj.project_id = obj.task_id.project_id.id
            if obj.task_id.is_norm:
                obj.planned_time = obj.task_id.norm_id.time
            else:
                obj.planned_time = obj.task_id.planned_hours

    @api.multi
    def action_confirm(self):
        for obj in self:
            obj.is_confirm = True
            obj.confirm_user = self._uid

    def action_draft(self):
        for obj in self:
            obj.is_confirm = False
            obj.confirm_user = False


class UserPlanAmendmentAdd(models.Model):
    _name = 'user.plan.add.line'

    modify_line = fields.Many2one('task.plan.modify.line')
    task_id = fields.Many2one('project.task', 'Task')
    project_id = fields.Many2one('project.project', 'Project')
    planned_time = fields.Float('Planned Time')
    reason = fields.Text('Add Reason')
    manager_description = fields.Char('Manager Description')
    confirm_description = fields.Char('Confirm Description')
    is_confirm = fields.Boolean('Is Confirm')
    confirm_user = fields.Many2one('res.users','Confirm User')
    user_plan_id = fields.Many2one('task.user.plan', 'Employee Plan', required=True)
    state = fields.Selection([('draft', 'Draft'),
                              ('send', 'Send'),
                              ('manager_confirm', 'Manager Confirm'),
                              ('confirm', 'Confirm')], 'State', default='draft', track_visibility='onchange')

    @api.onchange('task_id')
    def onchange_task_id(self):
        for obj in self:
            if obj.task_id.project_id:
                obj.project_id = obj.task_id.project_id.id
            if obj.task_id.is_norm:
                obj.planned_time = obj.task_id.norm_id.time
            else:
                obj.planned_time = obj.task_id.planned_hours

    @api.multi
    def action_confirm(self):
        for obj in self:
            obj.is_confirm = True
            obj.confirm_user = self._uid

    def action_draft(self):
        for obj in self:
            obj.is_confirm = False
            obj.confirm_user = False


class UserPlanChangeTime(models.Model):
    _name = 'user.plan.change.line'

    modify_line = fields.Many2one('task.plan.modify.line')
    task_id = fields.Many2one('project.task', 'Task')
    project_id = fields.Many2one('project.project', 'Project')
    planned_time = fields.Float('Planned Time')
    norm_id = fields.Many2one('project.task.norm', 'Norm')
    change_norm_id = fields.Many2one('project.task.norm', 'Modify Norm')
    time = fields.Float('Modify Time')
    reason = fields.Text('Reason')
    manager_description = fields.Char('Manager Description')
    confirm_description = fields.Char('Confirm Description')
    is_confirm = fields.Boolean('Is Confirm')
    confirm_user = fields.Many2one('res.users','Confirm User')
    user_plan_id = fields.Many2one('task.user.plan', 'Employee Plan', required=True)
    state = fields.Selection([('draft', 'Draft'),
                              ('send', 'Send'),
                              ('manager_confirm', 'Manager Confirm'),
                              ('confirm', 'Confirm')], 'State', default='draft', track_visibility='onchange')

    @api.onchange('task_id')
    def onchange_task_id(self):
        for obj in self:
            if obj.task_id.project_id:
                obj.project_id = obj.task_id.project_id.id
            if obj.task_id.is_norm:
                obj.planned_time = obj.task_id.norm_id.time
                obj.norm_id = obj.task_id.norm_id.id
            else:
                obj.planned_time = obj.task_id.planned_hours

    @api.onchange('change_norm_id')
    def onchange_change_norm_id(self):
        for obj in self:
            obj.time = obj.change_norm_id.time

    @api.multi
    def action_confirm(self):
        for obj in self:
            obj.is_confirm = True
            obj.confirm_user = self._uid

    def action_draft(self):
        for obj in self:
            obj.is_confirm = False
            obj.confirm_user = False