# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import time

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class AddPlanTask(models.TransientModel):
    _name = 'add.plan.task'

    # task_id = fields.Many2one('project.task', string='Task')
    user_id = fields.Many2one('res.users', string='User')
    plan_id = fields.Many2one('task.user.plan', string='Plan')
    type = fields.Selection([('add_plan', 'Add Plan'),
                             ('switch_plan', 'Switch Plan'),
                             ('next_plan', 'Next Plan')], string='Type', default='add_plan', required=True)

    @api.onchange('user_id')
    def onchange_user_id(self):
        return {'domain': {'plan_id': [('user_id', '=', self.user_id.id), ('state', '!=', 'done')]}}

    @api.model
    def default_get(self, fields):
        result = super(AddPlanTask, self).default_get(fields)
        if self._context.get('active_id'):
            task_obj = self.env['project.task']
            task = task_obj.browse(self._context['active_id'])
        result.update({
                        # 'task_id': task.id,
                        'user_id': task.user_id.id if task.user_id else False
                       })
        return result

    @api.multi
    def add_button(self):
        for obj in self:
            context = dict(self._context or {})
            task_ids = self.env['project.task'].browse(context.get('active_ids'))
            if obj.type == 'add_plan':
                for task in task_ids:
                    if obj.user_id.id == task.user_id.id:
                        if not task.plan_id:
                            if obj.plan_id.state not in ('draft', 'send'):
                                task.create_plan_history('add_plan', obj.plan_id.id)
                            task.plan_id = obj.plan_id.id
                            obj.plan_id.write({
                                'planned_task_ids': [(4, task.id)]
                            })
                        else:
                            raise UserError(_(u"Аль хэдийн төлөвлөгдсөн даалгаварыг төлөвлөгөө нэмэх үйлдэл хийх боломжгүй."))
                    else:
                        raise UserError(_(u"Төлөвлөгөө болон даалгаварын хэрэглэгч адилхан байх шаардлагатай."))
            if obj.type == 'switch_plan':
                for task in task_ids:
                    if task.plan_id:
                        task.plan_id.write({
                            'planned_task_ids': [(3, task.id)]
                        })
                        task.write({
                            'plan_id': obj.plan_id.id,
                            'user_id': obj.user_id.id
                        })
                        obj.plan_id.write({
                            'planned_task_ids': [(4, task.id)]
                        })
                        task.create_plan_history('switch_plan', obj.plan_id.id)
                    else:
                        raise UserError(_(u"Төлөвлөгдөөгүй даалгаварыг төлөлөвлөгөө шилжүүлэх үйлдэл хийх боломжгүй."))
            if obj.type == 'next_plan':
                for task in task_ids:
                    if task.plan_id:
                        task.write({
                            'plan_id': obj.plan_id.id,
                        })
                        obj.plan_id.write({
                            'planned_task_ids': [(4, task.id)]
                        })
                        task.create_plan_history('old_plan', obj.plan_id.id)
                    else:
                        raise UserError(_(u"Төлөвлөгдөөгүй даалгаварыг дараа сарын төлөлөвлөгөөнд шилжүүлэх үйлдэл хийх боломжгүй."))
