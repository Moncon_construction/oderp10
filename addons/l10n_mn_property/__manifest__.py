# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Property",
    'version': '1.0',
    'depends': ['l10n_mn_base', 'base', 'project','l10n_mn_contract'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
    Description texts
    """,
    'data': [
        'security/property_security.xml',
        'security/ir.model.access.csv',
        'views/property_apartment_view.xml',
        'views/property_apartment_room_view.xml',
        'data/res_khoroo_data.xml',
        'data/res_town_location_data.xml',
        'views/property_room_type_view.xml',
        'views/property_address_view.xml',
        'views/project_view.xml',
        'views/internall_used_room_history.xml',
        'wizard/internall_used_room_history_wizard.xml',
    ],
    'installable': True,
    'auto_install': True
}
