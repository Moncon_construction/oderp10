# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError

class District(models.Model):
    _inherit = 'res.district'
    
    @api.multi
    def name_get(self):
        res = []
        for obj in self:
            name = obj.name
            if obj.country_id:
                name += ' | ' + obj.country_id.name
            res.append((obj.id, name))
        return res
    
class SumKhoroo(models.Model):
    _inherit = 'res.khoroo'
    
    @api.multi
    def name_get(self):
        res = []
        for obj in self:
            name = obj.name
            if obj.district_id:
                name += ' | ' + obj.district_id.name
            res.append((obj.id, name))
        return res

class TownLocation(models.Model):
    _name = 'res.town.location'
    _description = 'Town/Location'
    
    name = fields.Char(string='Town/Location name', required=True, translate=True, help='Full name of Town/Location.')
    sumkhoroo_id = fields.Many2one('res.khoroo', string='Sum/Khoroo name', ondelete='cascade', required=True)
    code = fields.Char(string="Town/Location code")
    
    @api.multi
    def name_get(self):
        res = []
        for obj in self:
            name = obj.name + ' | ' + obj.sumkhoroo_id.name
            res.append((obj.id, name))
        return res
    
    @api.model
    def create(self, vals):
        #  Давхцал шалгаж байна.
        self.env.cr.execute("select name from res_town_location where sumkhoroo_id = %s" %vals['sumkhoroo_id'])
        found_obj = self.env.cr.dictfetchall()
        for obj in found_obj:
            if vals['name'] == obj['name']:
                sumkhoroo_names = self.env['res.khoroo'].search([('name','=', vals['name'])])
                raise UserError(_("There is a same named town or location in '%s'.\n Please choose different name!") %','.join([str(obj.name) for obj in sumkhoroo_names]))
             
        self.env.cr.execute("select code from res_town_location where sumkhoroo_id = %s" %vals['sumkhoroo_id'])
        found_obj = self.env.cr.dictfetchall()
        for obj in found_obj:
            if vals['code'] == obj['code']:
                raise UserError(_("There is a town or location in '%s' which has same code.\n Please choose different code!") %code)
        return super(TownLocation, self).create(vals)   
    
    @api.model
    def write(self, vals):
        if 'name' in vals:
            self.env.cr.execute("select name from res_town_location where sumkhoroo_id = %s" %vals['sumkhoroo_id'])
            found_obj = self.env.cr.dictfetchall()
            for obj in found_obj:
                if vals['name'] == obj['name']:
                    sumkhoroo_names = self.env['res.khoroo'].search([('name','=', vals['name'])])
                    raise UserError(_("There is a same named town or location in '%s'.\n Please choose different name!") %','.join([str(obj.name) for obj in sumkhoroo_names]))
        
        if 'code' in vals:
            self.env.cr.execute("select code from res_town_location where sumkhoroo_id = %s" %vals['sumkhoroo_id'])
            found_obj = self.env.cr.dictfetchall()
            for obj in found_obj:
                if vals['code'] == obj['code']:
                    raise UserError(_("There is a town or location in '%s' which has same code.\n Please choose different code!") %code)
        return super(TownLocation, self).write(vals) 
    
class PropertyApartmentAddress(models.Model): 
    _name = 'property.apartment.address'
    
    country_id = fields.Many2one('res.country', string='Country', ondelete='restrict')
    district_id = fields.Many2one('res.district', string='Province/District', ondelete='restrict')
    sumkhoroo_id = fields.Many2one('res.khoroo', string='Sum/Khoroo', ondelete='restrict')
    townlocation_id = fields.Many2one('res.town.location', string='Town/Location', ondelete='set null')
    street = fields.Char("Street")
    
    @api.multi
    def name_get(self):
        res = []
        for property_address in self:
            name = "" 
            hasPrefix = False
            if property_address.street:
                name += property_address.street + _(" Street")
                hasPrefix = True
            if hasPrefix and property_address.townlocation_id:
                name += " | " + property_address.townlocation_id.name + _(" Town/Location")
                hasPrefix = True
            else: 
                if property_address.townlocation_id:
                    name += property_address.townlocation_id.name + _(" Town/Location")
                    hasPrefix = True
            if hasPrefix and property_address.sumkhoroo_id:
                name += " | " + property_address.sumkhoroo_id.name + _(" Sum/Khoroo")
                hasPrefix = True
            else: 
                if property_address.sumkhoroo_id:
                    name += property_address.sumkhoroo_id.name + _(" Sum/Khoroo")
                    hasPrefix = True
            if hasPrefix and property_address.district_id:
                name += " | " + property_address.district_id.name
                hasPrefix = True
            else: 
                if property_address.district_id:
                    name += property_address.district_id.name
                    hasPrefix = True
            res.append((property_address.id, name))
        return res
    
    @api.onchange('country_id')
    def _country_id_change(self):
        if self.district_id:
            if self.district_id.country_id != self.country_id:
                self.district_id = None
        
    @api.onchange('district_id')
    def _district_id_change(self):
        self.country_id = self.district_id.country_id
        if self.sumkhoroo_id:
            if self.sumkhoroo_id.district_id != self.district_id:
                self.sumkhoroo_id = None
        
    @api.onchange('sumkhoroo_id')
    def _sumkhoroo_id_change(self):
        self.district_id = self.sumkhoroo_id.district_id
        if self.townlocation_id:
            if self.townlocation_id.sumkhoroo_id != self.sumkhoroo_id:
                self.townlocation_id = None
        
    @api.onchange('townlocation_id')
    def _townlocation_id_change(self):
        self.sumkhoroo_id = self.townlocation_id.sumkhoroo_id
    
    
    
    