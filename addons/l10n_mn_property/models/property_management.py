# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import time
from dateutil.relativedelta import relativedelta
import datetime
from datetime import datetime

from odoo import api, fields, models, _
from odoo.exceptions import UserError

class PropertyApartment(models.Model):
    _name = 'property.apartment'
    
    name = fields.Char(string='Apartment Name', required=True)
    apartment_number = fields.Char(string='Apartment Number')
    apartment_floor = fields.Char(string='Apartment Floor', required=True)
    apartment_square = fields.Float(string='Apartment Total Square', readonly=True, store=True)
    apartment_living_space = fields.Float(string='Apartment Total Living Space', compute="_compute_total_living_space", readonly=True)
    apartment_used_date = fields.Date(string="Apartment Used Date")
    apartment_project =  fields.Many2one('project.project', string='Project', domain="[('used_apartments','=',True)]")
    apartment_address = fields.Many2one('property.apartment.address', string="Address")
    
    ap_address_country = fields.Many2one(related='apartment_address.country_id', string="Country", store=True)
    ap_address_district = fields.Many2one(related='apartment_address.district_id', string="Province/District", store=True)
    ap_address_sumkhoroo = fields.Many2one(related='apartment_address.sumkhoroo_id', string="Sum/Khoroo", store=True)
    ap_address_townlocation = fields.Many2one(related='apartment_address.townlocation_id', string="Town/Location", store=True)
    ap_address_street = fields.Char(related='apartment_address.street', string="Street", store=True)
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    
    rooms = fields.One2many('property.apartment.room', 'room_apartment')
    
    @api.multi
    def name_get(self):
        res = []
        for apartment in self:
            name = ""
            if apartment.name and apartment.apartment_number:
                name += apartment.name + " | " + apartment.apartment_number
            elif apartment.name:
                name += apartment.name
            res.append((apartment.id, name))
        return res
    
    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        recs = self.browse()
        if name:
            recs = self.search([('apartment_number', 'ilike', name)] + args, limit=limit)
        if not recs:
            recs = self.search([('name', operator, name)] + args, limit=limit)
        return recs.name_get()
    
    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        " Overwrite the read_group in order to sum the function field 'apartment_living_space' in group by "
        res = super(PropertyApartment, self).read_group(domain, fields, groupby, offset=offset, limit=limit, orderby=orderby, lazy=lazy)
        if 'apartment_living_space' in fields:
            for line in res:
                if '__domain' in line:
                    lines = self.search(line['__domain'])
                    apartment_living_space = sum(line2.apartment_living_space for line2 in lines)
                    line['apartment_living_space'] = apartment_living_space
        return res
    
    @api.model
    def create(self, vals):
        if not self.env.user.has_group("l10n_mn_property.group_property_register"):
            raise UserError(u"Зөвхөн Өрөө/Байр бүртгэлийн ажилтан эрхтэй хэрэглэгч БАЙР үүсгэх боломжтой.")
        
        return super(PropertyApartment, self).create(vals)
        
    @api.multi
    def add_apartment_square(self, add_sqr):
        for obj in self:
            obj.apartment_square += add_sqr
    
    @api.multi
    def _compute_total_living_space(self):
        for obj in self:
            obj.apartment_living_space = sum(room.room_total_square for room in obj.rooms)
    
            
class PropertyRoomType(models.Model):
    _name = 'property.room.type'

    name = fields.Char(string='Room Type', required=True)
    description = fields.Char('Description')
    
    _sql_constraints = [
            ('name_uniq', 'unique (name)', "Property Room Type already exists !"),
    ]

    
class PropertyApartmentRoom(models.Model):
    _name = 'property.apartment.room'
    _inherit = ['mail.thread']

    room_apartment = fields.Many2one('property.apartment', string='Apartment Name')
    room_apartment_name = fields.Char(related="room_apartment.name", string='Apartment Pure Name', store=True)
    room_apartment_number = fields.Char(related='room_apartment.apartment_number', string='Apartment Number', store=True)
    name = fields.Char(string='Room Number')
    apartment_owner = fields.Many2one('res.partner', string = 'Apartment Owner')
    room_type = fields.Many2one('property.room.type', string = 'Room Type')
    room_floor = fields.Char(string='Room Floor')
    room_total_square = fields.Float(string='Total Square')
    
    to_sell = fields.Boolean(string='To Sell')
    sell_square_price = fields.Float(string='Square Price of Sell')
    sell_total_square_price = fields.Float(string='Total Square Price Of Sell', readonly=True, compute='compute_total_square_price')
    sell_unit_price = fields.Float(string='Unit Price of Sell')
    
    to_rent = fields.Boolean(string='To Rent')
    rent_square_price = fields.Float(string='Square Price of Rent')
    rent_total_square_price = fields.Float(string='Total Square Price Of Rent', readonly=True, compute='compute_total_square_price')
    rent_management_payment = fields.Float(string="Monthly Management Payment")
    rent_unit_price = fields.Float(string="Unit Price of Rent")
    
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    
    state = fields.Selection([('ready', 'Ready'),
                              ('saved', 'Saved'),
                              ('rented','Rented'),
                              ('sold','Sold'),
                              ('in_repair','In Repair'),
                              ('used','Used In Internally')],
                              string='State', default='ready',required=True)
    description = fields.Text(string='Description')
    state_history_ids = fields.One2many(
        'property.apartment.room.state.history', 'appartment_room_id', string='History', readonly = True)
    rent_history_ids = fields.One2many(
        'property.apartment.room.rent.history', 'appartment_room_id', string='Rent History')
    
    currency = fields.Many2one('res.currency', string='Currency', default=lambda self: self.env.user.company_id.currency_id)
    
    used_histories = fields.One2many('internall.used.room.history', 'room', 'Internal Used History')
    
    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        recs = self.browse()
        if name:
            recs = self.search([('room_apartment_number', 'ilike', name)] + args, limit=limit)
        if not recs:
            recs = self.search([('name', operator, name)] + args, limit=limit)
        return recs.name_get()

    @api.onchange('room_floor')
    def _check_room_floor(self):
        for obj in self:
            try:
                if float(obj.room_floor) and float(obj.room_apartment.apartment_floor):
                    ap_total_floor = int(float(obj.room_apartment.apartment_floor))
                    room_floor = int(float(obj.room_floor))
                    if room_floor > ap_total_floor:
                        raise UserError(_(u"%s-р давхарт өрөө бүртгэх боломжгүй. \n\nТухайн байр нь нийт %s давхартайгаар бүртгэгдсэн байна.") %(room_floor, ap_total_floor))
            except ValueError:
                pass
    @api.multi
    def name_get(self):
        res = []
        for room in self:
            name = room.name + " [" + room.room_type.name + "]" if room.room_type else ''
            res.append((room.id, name))
        return res
    
    @api.model
    def create(self, vals):
        if not self.env.user.has_group("l10n_mn_property.group_property_register"):
            raise UserError(u"Зөвхөн Өрөө/Байр бүртгэлийн ажилтан эрхтэй хэрэглэгч БАЙР үүсгэх боломжтой.")
        
        created_room = super(PropertyApartmentRoom, self).create(vals)
        created_room.room_apartment.add_apartment_square(created_room.room_total_square)
        created_room.add_property_apartment_room_state_history(created_room.id, created_room.state)
        return created_room

    @api.one
    def unlink(self):
        self.room_apartment.add_apartment_square(-self.room_total_square)
        return super(PropertyApartmentRoom, self).unlink()
    
    @api.multi
    def write(self, vals):
        if vals.get('state'):
            self.add_property_apartment_room_state_history(self.id, vals.get('state'))
        if vals.get('room_total_square'):
            self.room_apartment.add_apartment_square(vals.get('room_total_square')-self.room_total_square)
        updated_room = super(PropertyApartmentRoom, self).write(vals)
        return updated_room
    
    @api.one
    def add_property_apartment_room_state_history(self, room, state):
        #????????????Энэ кодоо дараа нь засах odoo-н one2many-г хэрхэн нэмэх дүрмийг судлах
        state_history_ids1 = self.state_history_ids
        
        room_state = dict(self.fields_get(allfields=['state'])['state']['selection'])[state]
        new_history = self.env['property.apartment.room.state.history'].create({
            'appartment_room_id': room,
            'user_id': self.env.uid,
            'date': time.strftime('%Y-%m-%d %H:%M:%S'),
            'state': room_state,
        })
        
        self.state_history_ids = new_history + state_history_ids1
    
    @api.one
    def add_property_apartment_room_rent_history(self, contract, pterm):
        #????????????Энэ кодоо дараа нь засах odoo-н one2many-г хэрхэн нэмэх дүрмийг судлах
        rent_history_ids1 = self.rent_history_ids
        new_history = self.env['property.apartment.room.rent.history'].create({
            'appartment_room_id': self.id,
            'apartment_room_contract': contract.id,
            'rent_user': contract.partner_id.id, 
            'user_id': self.env.uid,
            'date': time.strftime('%Y-%m-%d %H:%M:%S'),
            'rent_begin_date': pterm.start_date,
            'rent_end_date': pterm.end_date,
        })
        self.rent_history_ids = new_history + rent_history_ids1
        
    @api.depends('room_total_square', 'rent_square_price', 'sell_square_price', 'rent_management_payment')
    def compute_total_square_price(self):
        for obj in self:
            if obj.rent_square_price and obj.room_total_square:
                obj.rent_total_square_price = (obj.rent_square_price + obj.rent_management_payment)*obj.room_total_square
            if obj.sell_square_price and obj.room_total_square:
                obj.sell_total_square_price = obj.sell_square_price*obj.room_total_square
  
class InternallUsedRoomHistory(models.Model):
    _name = 'internall.used.room.history'
    _inherit = ['mail.thread']
    
    room = fields.Many2one('property.apartment.room', string='Room', required=True)
    apartment = fields.Many2one('property.apartment', related='room.room_apartment', store=True, search='_search_apartment', string='Apartment')
    date_from = fields.Date(string='Date From')
    date_to = fields.Date(string='Date To')
    note = fields.Char(string='Note')
    
    @api.multi
    def _search_apartment(self, operator, value):
        res = []
        if value:
            res = self.env['internall.used.room.history'].search(['|', ('room.room_apartment.name', 'ilike', value), ('room.room_apartment.apartment_number', '=', value)])
        return [('id', search_operator, res)]
          
class PropertyApartmentRoomStateHistory(models.Model):
    _name = 'property.apartment.room.state.history'
    _order = 'date desc'

    appartment_room_id = fields.Many2one('property.apartment.room', string='Reference')
    user_id = fields.Many2one('res.users', string='User Name')
    date = fields.Datetime(string='Date')
    state = fields.Char(string='State', help='State')
    
    
class PropertyApartmentRoomRentHistory(models.Model):
    _name = 'property.apartment.room.rent.history'
    _order = 'rent_begin_date desc'
    _order = 'rent_end_date desc'
    
    appartment_room_id = fields.Many2one('property.apartment.room', string='Reference')
    apartment_room_contract = fields.Many2one('contract.management', string='Contract')
    rent_user = fields.Many2one('res.partner', string='Partner Name', help="Customer who rent room")
    user_id = fields.Many2one('res.users', string='User Name', help="User who register contract for room")
    rent_begin_date = fields.Date(string='Start date')
    rent_end_date = fields.Date(string='End date')
    date = fields.Datetime(string='History Registered Date', default=datetime.now(), help="History Registered Date")
