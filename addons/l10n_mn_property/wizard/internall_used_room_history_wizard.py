# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import time
from dateutil.relativedelta import relativedelta
import datetime
from datetime import datetime

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class InternallUsedRoomHistoryWizard(models.TransientModel):
    _name = 'internall.used.room.history.wizard'
    
    rooms = fields.Many2many('property.apartment.room', 'property_apartment_room_to_used_history', 'wizard_id', 'room_id', string='Room')
    date_from = fields.Date(string='Date From')
    date_to = fields.Date(string='Date To')
    note = fields.Char(string='Note')

    def create_used_history(self):
        values = ""
        for room in self.rooms:
            values += "(%s, '%s', '%s', '%s'), " %(room.sudo().id, self.date_from, self.date_to, self.note if self.note else '')
        values = values[:-2] if values[-2:] == ", " else values

        self._cr.execute("""
            INSERT INTO internall_used_room_history (room, date_from, date_to, note)
            VALUES %s
        """ %values)
         

class PropertyApartmentRoom(models.Model):
    _inherit = 'property.apartment.room'
    
    @api.multi
    def create_used_state_history(self):
        ids = [obj.id for obj in self.filtered(lambda x: x.state == 'used')]
        
        if not ids:
            raise UserError(_(u"Түүх үүсгэх боломжтой ямар ч өрөө сонгогдоогүй байна."))
        else:
            wizard = self.env['internall.used.room.history.wizard'].create({
                'date_from': datetime.now()
            })
            
            values = ""
            for id in ids:
                values += "(%s, %s), " %(wizard.id, id)
            values = values[:-2] if values[-2:] == ", " else values
        
            self._cr.execute("""
                INSERT INTO property_apartment_room_to_used_history (wizard_id, room_id) 
                VALUES %s
            """ % values)
            
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'internall.used.room.history.wizard',
                'view_mode': 'form',
                'target': 'new',
                'res_id': wizard.id,
            } 
            
            


