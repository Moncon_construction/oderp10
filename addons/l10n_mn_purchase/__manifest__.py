# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Purchase",
    'version': '1.0',
    'depends': [
        'purchase',
        'l10n_mn_product',
        'l10n_mn_stock_account'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
        Purchase Additional Features
    """,
    'data': [
            'security/ir.model.access.csv',
            'security/security.xml',
            'security/ir_rule.xml',
            'data/data.xml',
            'wizard/count_unit_total_view.xml',
            'views/purchase_order_view.xml',
            'views/purchase_order_tag_views.xml',
            'views/purchase_cost_detailed_report_view.xml',
            'views/res_config_view.xml',
            'views/purchase_detailed_report_by_display.xml',
            'views/purchase_refund_views.xml',
            'wizard/purchase_quotation_by_xls_report.xml',
            'wizard/purchase_refund_wizard.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
