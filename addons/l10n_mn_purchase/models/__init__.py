# -*- coding: utf-8 -*-

import res_company
import res_config
import purchase_order_tag
import purchase_order
import account_invoice
import purchase_order_line
import product_template
import stock_move
import stock_picking
import purchase_refund
import stock_return_picking