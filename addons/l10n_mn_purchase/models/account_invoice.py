# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from datetime import date, datetime
from odoo.exceptions import UserError


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.onchange('journal_id')
    def _onchange_journal_id(self):
        if self.journal_id:
            # self.currency_id = self.journal_id.currency_id.id or self.journal_id.company_id.currency_id.id
            pass

    @api.onchange('date_invoice')
    def _onchange_date_invoice(self):
        res = super(AccountInvoice, self)._onchange_date_invoice()
        purchase_ids = self.invoice_line_ids.mapped('purchase_id')
        if purchase_ids:
            if not self.date_invoice and self.currency_id != self.company_id.currency_id:
                for purchase in purchase_ids:
                    self.currency_rate = purchase.currency_rate
        return res

    def _prepare_invoice_line_from_po_line(self, line):
        res = super(AccountInvoice, self)._prepare_invoice_line_from_po_line(line)
        res.update({'discount': line.discount})
        return res