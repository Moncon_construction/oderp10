# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from datetime import date, datetime, timedelta
from odoo.exceptions import UserError
from odoo import exceptions


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    READONLY_STATES = {
        'purchase': [('readonly', True)],
        'done': [('readonly', True)],
        'cancel': [('readonly', True)],
    }

    @api.multi
    def _default_warehouse_id(self):
        default_warehouse = self.env.user.allowed_warehouses.ids
        warehouse_ids = self.env['stock.warehouse'].search([('id', 'in', default_warehouse), ('company_id', '=', self.env.user.company_id.id)], limit=1)
        return warehouse_ids

    def _domain_warehouses(self):
        return [
            ('id', 'in', self.env.user.allowed_warehouses.ids),
            '|', ('company_id', '=', False), ('company_id', 'child_of', [self.company_id.id or self.env.user.company_id.id])
        ]

    def _count_transit(self):
        for obj in self:
            transit_order = obj.env['stock.transit.order'].search([('purchase_order_id', '=', obj.id)])
            obj.count_transit = len(transit_order)

    def _compute_create_invoice_invoice(self):
        for obj in self:
            create_invoice = False
            if not obj.invoice_count:
                if obj.state == 'purchase':
                    create_invoice = True
            elif obj.invoice_count:
                if self.amount_total > sum(self.mapped('invoice_ids').mapped('amount_total')):
                    create_invoice = True
            obj.create_invoice = create_invoice

    currency_rate = fields.Float(string='Currency Rate', default=1.0)
    currency_cal_date = fields.Datetime(string='Currency Calculate Date', default=datetime.now())
    purchase_order_type = fields.Selection([('foreign', 'Foreign'), ('local', 'Local')], 'Purchase type', default='local', help='If purchase order is foreign purchase order then you should check this field.', states=READONLY_STATES)
    purchase_returned = fields.Boolean(compute='_compute_purchase_returned', string='Purchases Returned', default=False, search='_purchase_returned_search')
    is_invoiced = fields.Boolean(compute='_is_invoiced', string='Invoiced', default=False)
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse',
                                   required=True, readonly=True,
                                   states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
                                   default=_default_warehouse_id, domain=_domain_warehouses)
    transfer_date_time = fields.Integer('Transfer Date Time/date/', default=0)
    ub_come_date = fields.Datetime('Ub come date', compute='_compute_ub_come_date', store=True)
    purchase_employee = fields.Many2one('res.users', 'Purchase Employee', default=lambda self: self.env.uid)
    tag_ids = fields.Many2many('purchase.order.tag', 'purchase_order_to_tags', 'order_id', 'tag_id', string='Tags')
    count_transit = fields.Integer(compute='_count_transit', string='Transit Count')
    is_product_received = fields.Boolean(string='Is product received', default=False, store=True)
    create_invoice = fields.Boolean(compute='_compute_create_invoice_invoice', string='create invoice', default=False)

    @api.depends('transfer_date_time', 'date_order')
    @api.multi
    def _compute_ub_come_date(self):
        for obj in self:
            if obj.transfer_date_time and obj.date_order:
                session_start = fields.Datetime.from_string(obj.date_order)
                interval = timedelta(days=obj.transfer_date_time)
                obj.ub_come_date = fields.Datetime.to_string(session_start + interval)
            elif not obj.transfer_date_time:
                obj.ub_come_date = ''

    @api.multi
    def recompute_amount(self):
        for order in self:
            for line in order.order_line:
                line._compute_amount()
        return True

    @api.onchange('warehouse_id')
    def onchange_warehouse(self):
        if self.warehouse_id:
            stock_picking_type = self.env['stock.picking.type'].search([('warehouse_id', '=', self.warehouse_id.id), ('code', '=', 'incoming')],
                                                                       limit=1)
            if stock_picking_type:
                self.picking_type_id = stock_picking_type.id

    @api.onchange('picking_type_id')
    def onchange_picking_type_id(self):
        if self.picking_type_id:
            self.warehouse_id = self.picking_type_id.warehouse_id

    @api.multi
    def _is_invoiced(self):
        for order in self:
            if order.picking_ids and all([x.state == 'open' or x.state == 'paid' for x in order.invoice_ids]) and order.invoice_ids:
                order.is_invoiced = True
            else:
                order.is_invoiced = False
                    
    @api.depends('picking_ids', 'picking_ids.state')
    @api.multi
    def _compute_purchase_returned(self):
        for order in self:
            if order.picking_ids and any([x.state == 'done' and x.picking_type_id.code == 'outgoing' for x in order.picking_ids]):
                order.purchase_returned = True
            else:
                order.purchase_returned = False
                
    @api.multi
    def _purchase_returned_search(self, operator, value):
        recs = self.search([]).filtered(lambda x: x.purchase_returned is True)
        if recs:
            return [('id', 'in', [x.id for x in recs])]
        else:
            return [('id', 'in', [])]

    @api.multi
    def button_confirm(self):
        res = super(PurchaseOrder, self).button_confirm()
        for order in self:
            for line in order.order_line:
                if line.taxes_id:
                    for tax in line.taxes_id:
                        if tax.amount_type != 'group' and not tax.account_id:
                            raise UserError(_("%s Account Tax Empty!! " % tax.name))
        return res

    @api.multi
    def currency_filter(self):
        if self.currency_id or self.currency_cal_date:
            currency_obj = self.env['res.currency'].search([('name', '=', self.currency_id.name)])
            date = datetime.strptime(self.currency_cal_date, '%Y-%m-%d %H:%M:%S')
            day = date.replace(hour=0, minute=0, second=0)
            day = str(day)
            if currency_obj:
                for currency in currency_obj:
                    for rate in currency.rate_ids:
                        if rate.name == day:
                            return rate.alter_rate
        return 1.0

    @api.onchange('date_order')
    def _onchange_date_order(self):
        self.currency_cal_date = self.date_order

    @api.onchange('currency_id', 'currency_cal_date')
    def _onchange_currency_cal_date(self):
        val = self.currency_filter()
        self.currency_rate = val

    @api.multi
    def action_view_invoice(self):
        result = super(PurchaseOrder, self).action_view_invoice()
        # override the context to get rid of the default filtering
        result['context'].update({'default_currency_id': self.currency_id.id})
        return result

    @api.constrains('name')
    def _check_order_reference(self):
        # Захиалгын дугаарын давхардал шалгах
        for purchase_order in self:
            if purchase_order.name:
                other_orders = self.env['purchase.order'].search([
                    ('name', '=', purchase_order.name),
                    ('id', '!=', purchase_order.id)])
            for other_order in other_orders:
                if other_order:
                    exception = _('Purchase order duplicated: ') + other_order.name
                    raise exceptions.ValidationError(exception)

    @api.multi
    def button_create_transit(self):
        for warehouse in self.env.user.allowed_warehouses:
            if warehouse[0] != self.warehouse_id.id:
                receive_warehouse = warehouse[0]
            else:
                receive_warehouse = warehouse[1]

        vals = {
            'purchase_order_id': self.id,
            'supply_warehouse_id': self.warehouse_id.id,
            'warehouse_id': receive_warehouse.id,
            'supply_picking_type_id': self.picking_type_id.id,
            'company_id': self.company_id.id,
            'user_id': self.env.user.id,
            'date_order': self.date_order,
            'receive_date': self.date_order,
            'state': 'draft'
        }
        transit_order_id = self.env['stock.transit.order'].create(vals)

        for line in self.order_line:
            self.env['stock.transit.order.line'].create({
                'transit_order_id': transit_order_id.id,
                'product_id': line.product_id.id,
                'name': line.name,
                'product_qty': line.product_qty,
                'product_uom': line.product_uom.id,
                'state': transit_order_id.state
            })

    # core-n function-g override hiisen
    @api.multi
    def action_rfq_send(self):
        '''
        This function opens a window to compose an email, with the edi purchase template message loaded by default
        '''
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            if self.env.context.get('send_rfq', False):
                template_id = ir_model_data.get_object_reference('purchase', 'email_template_edi_purchase')[1]
            else:
                template_id = ir_model_data.get_object_reference('purchase', 'email_template_edi_purchase_done')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False

        new_report = self.env['purchase.quotation.report'].create({
            'order_ids': [(6, 0, self.ids)],
        })
        attachment_value = {
            'name': 'Purchase Quotation',
            'datas': new_report.get_report_data(),
            'datas_fname': 'Purchase Quotation XLS',
            'res_model': 'purchase.order',
            'res_id': self.id,
        }

        attachment_id = self.env['ir.attachment'].create(attachment_value)

        ctx = dict(self.env.context or {})
        ctx.update({
            'default_model': 'purchase.order',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_attachment_ids': [(6, 0, [attachment_id.id])],
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'purchase_mark_rfq_sent': True,
        })
        return {
            'name': _('Compose Email'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    @api.multi
    def action_create_invoice(self):
        for obj in self:
            invoice = self.env['account.invoice'].create({
                'origin': obj.name,
                'type': 'in_invoice',
                'reference': False,
                'account_id': obj.partner_id.property_account_receivable_id.id,
                'partner_id': obj.partner_id.id,
                'partner_shipping_id': obj.partner_id.id,
                'currency_id': obj.company_id.currency_id.id
            })
            for line in obj.order_line:
                line_dic = invoice._prepare_invoice_line_from_po_line(line)
                if line_dic.get('quantity', 0) > 0:
                    line_dic['invoice_id'] = invoice.id
                    if line_dic.get('invoice_line_tax_ids', False):
                        line_dic['invoice_line_tax_ids'] = [(6, 0, line_dic['invoice_line_tax_ids'])]
                    rr = self.env['account.invoice.line'].create(line_dic)
            invoice._onchange_invoice_line_ids()

        return {
            'name': _('Customer Invoice'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'account.invoice',
            'view_id': self.env.ref('account.invoice_form').id,
            'type': 'ir.actions.act_window',
            'res_id': invoice.id,
        }

    # core-n function-g override hiisen
    class MailComposer(models.TransientModel):
        _inherit = 'mail.compose.message'

        @api.multi
        def onchange_template_id(self, template_id, composition_mode, model, res_id):
            if template_id and composition_mode == 'mass_mail':
                template = self.env['mail.template'].browse(template_id)
                fields = ['subject', 'body_html', 'email_from', 'reply_to', 'mail_server_id']
                values = dict((field, getattr(template, field)) for field in fields if getattr(template, field))
                if template.attachment_ids:
                    values['attachment_ids'] = [att.id for att in template.attachment_ids]
                if template.mail_server_id:
                    values['mail_server_id'] = template.mail_server_id.id
                if template.user_signature and 'body_html' in values:
                    signature = self.env.user.signature
                    values['body_html'] = tools.append_content_to_html(values['body_html'], signature, plaintext=False)
            elif template_id:
                values = self.generate_email_for_composer(template_id, [res_id])[res_id]

            else:

                default_values = self.with_context(default_composition_mode=composition_mode, default_model=model, default_res_id=res_id).default_get(['composition_mode', 'model', 'res_id', 'parent_id', 'partner_ids', 'subject', 'body', 'email_from', 'reply_to', 'mail_server_id'])
                values = dict((key, default_values[key]) for key in ['subject', 'body', 'partner_ids', 'email_from', 'reply_to', 'mail_server_id'] if key in default_values)

            if values.get('body_html'):
                values['body'] = values.pop('body_html')

            values = self._convert_to_write(values)

            return {'value': values}

    class StockTransitOrder(models.Model):
        _inherit = 'stock.transit.order'

        purchase_order_id = fields.Many2one('purchase.order', string = 'Purchase Order')
