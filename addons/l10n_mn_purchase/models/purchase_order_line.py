# -*- coding: utf-8 -*-
from datetime import datetime
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, SUPERUSER_ID, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError
from mimetools import choose_boundary


class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    def is_module_installed(self, module_name):
        self._cr.execute("SELECT id FROM ir_module_module WHERE name = '%s' AND state IN ('installed', 'to upgrade')" %module_name)
        results = self._cr.dictfetchall()
        
        if results and len(results) > 0:
            return True
        else:
            return False

    @api.multi
    @api.depends('name', 'order_id')
    def name_get(self):
        result = []
        for po_line in self:
            name = '[' + po_line.order_id.name + '] ' + po_line.name
            result.append((po_line.id, name))
        return result

    @api.multi
    def _is_received(self):
        for order_line in self:
            show = False
            count = 0
            move = self.env['stock.move'].search([('purchase_line_id', '=', order_line.id)])
            # print move
            if move:
                for move_obj in move:
                    if move_obj.picking_type_id.code == 'incoming' and move_obj.state == 'done':
                        count += 1
                if count > 0:
                    show = True
            order_line.received = show

    @api.multi
    def _is_invoiced(self):
        for order_line in self:
            show = False
            account_invoice_line = self.env['account.invoice.line'].search([('purchase_line_id', '=', order_line.id)])
            if account_invoice_line:
                for line in account_invoice_line:
                    if line.invoice_id.state == 'open':
                        show = True
            order_line.invoiced = show

    @api.multi
    def _compute_price_unit_mnt(self):
        for line in self:
            line.price_unit_mnt = line.price_unit * line.order_id.currency_rate
            
    discount = fields.Float('Discount %')
    received = fields.Boolean(compute='_is_received', string='Is received')
    invoiced = fields.Boolean(compute='_is_invoiced', string='Is invoiced')
    price_unit_mnt = fields.Float('Price Unit MNT', compute='_compute_price_unit_mnt')

    @api.multi
    @api.constrains('product_qty')
    def _check_quantity_value(self):
        for sale_line in self:
            if sale_line.product_qty <= 0:
                raise UserError(_('Error!\nProduct quantitiy must be greater than Zero.'))

    @api.depends('product_qty', 'price_unit', 'taxes_id', 'discount')
    def _compute_amount(self):
        for line in self:
            taxes = line.taxes_id.compute_all(line.price_unit * (1 - (line.discount or 0.0) / 100.0), line.order_id.currency_id, line.product_qty, product=line.product_id, partner=line.order_id.partner_id)
            line.update({
                'price_tax': taxes['total_included'] - taxes['total_excluded'],
                'price_total': taxes['total_included'],
                'price_subtotal': taxes['total_excluded'],
            })

    @api.onchange('product_id')
    def onchange_product_id(self):
        rslt = super(PurchaseOrderLine, self).onchange_product_id()
        fpos = self.order_id.fiscal_position_id
        company_id = self.env.user.company_id.id
        list = self.env['account.tax']
        if not self.order_id.partner_id.is_taxpayer:
            for chs_taxes in self.product_id.supplier_taxes_id:
                if chs_taxes.choose_taxes != '1':
                    list = list | chs_taxes
            self.taxes_id = fpos.map_tax(list.filtered(lambda r: r.company_id.id == company_id))
        else:
            for chs_taxes in self.product_id.supplier_taxes_id:

                if chs_taxes.choose_taxes == '1':
                    list = list | chs_taxes
            self.taxes_id = fpos.map_tax(self.product_id.supplier_taxes_id.filtered(lambda r: r.company_id.id == company_id))

    # Худалдан авалт батлагдахад stock_move үүсгэх функц override хийв
    @api.multi
    def _get_stock_move_price_unit(self):
        self.ensure_one()
        line = self[0]
        order = line.order_id
        # Override starts here
        # Overrode function to use purchase order line's 'discount' field
        price_unit = line.price_unit / 100 * (100 - line.discount or 0)
        # Override ends here
        if line.taxes_id:
            price_unit = line.taxes_id.with_context(round=False).compute_all(price_unit, currency=line.order_id.currency_id, quantity=1.0, product=line.product_id, partner=line.order_id.partner_id)['total_excluded']
        if line.product_uom.id != line.product_id.uom_id.id:
            price_unit *= line.product_uom.factor / line.product_id.uom_id.factor
        if order.currency_id != order.company_id.currency_id:
            price_unit = order.currency_id.compute(price_unit, order.company_id.currency_id, round=False)
        return price_unit
