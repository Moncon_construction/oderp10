# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions, _


class PurchaseOrderTag(models.Model):
    _name = 'purchase.order.tag'
    _inherit = ['mail.thread']

    name = fields.Char(string='Tag Name', required=True, translate=True, track_visibility='onchange')
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    active = fields.Boolean(default=True, track_visibility='onchange')
    
    color = fields.Integer(string='Color Index')
    description = fields.Text(string='Description', translate=True)
    purchase_count = fields.Integer(compute="_count_purchase_order")

    @api.multi
    def _count_purchase_order(self):
        # Уг пайз бүртгэгдсэн ХА-уудын тоог олох
        for obj in self:
            self._cr.execute("""
                SELECT COUNT(order_id) AS count FROM purchase_order_to_tags WHERE tag_id IN (%s)
            """ % str(self.ids).strip('[]'))
            
            result = self._cr.dictfetchall()
            obj.purchase_count = result[0]['count'] if result else 0
            
    @api.multi
    def show_linked_orders(self):
        # Уг пайз бүртгэгдсэн ХА-ууд руу СМАРТ ТОВЧноос үсрэхэд ашиглах функц
        self._cr.execute("""
            SELECT order_id FROM purchase_order_to_tags WHERE tag_id IN (%s)
        """ % str(self.ids).strip('[]'))
        
        order_ids = [result['order_id'] for result in self._cr.dictfetchall()]

        return {
            'name': _('Purchase Order'),
            'res_model': 'purchase.order',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form,graph',
            'views': [
                (self.env.ref('l10n_mn_purchase.view_purchase_order_tree_inherit').id, 'tree'), 
                (False, 'form'),
            ],
            'target': 'current',
            'flags': {'search_view': True, 'action_buttons': True},
            'domain': [('id', 'in', order_ids)]
        }
        
