# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError
from docx.opc import part
from lxml import etree


class PurchaseRefund(models.Model):
    _name = "purchase.refund"
    _inherit = 'mail.thread'

    company_id = fields.Many2one('res.company', default=lambda self: self.env.user.company_id, string='Company')
    name = fields.Char(string='Reference', required=True, readonly=True, copy=False, default='New')
    partner_id = fields.Many2one('res.partner', required=True, string='Partner')
    employee_id = fields.Many2one('hr.employee', string='Returned Employee')
    picking_ids = fields.Many2many('stock.picking', 'purchase_refund_to_stock_picking', 'refund_id', 'pick_id', required=True, ondelete='cascade', string='Picking by Refund')
    purchase_count = fields.Integer(compute="compute_purchase_count", string='# Purchase')
    stock_move_count = fields.Integer(compute="compute_move_count", string='# Returned Product')
    picking_count = fields.Integer(compute="compute_picking_count", string='# Picking by Refund')
    
    @api.model
    def fields_view_get(self, view_id=None, view_type='tree', toolbar=False, submenu=False):
        # Зөвхөн ҮЙЛДЭЛ цэснээс үүсгэх тул үндсэн цэснээс засах/үүсгэх боломжгүй болгов
        res = super(PurchaseRefund,self).fields_view_get(view_id, view_type, toolbar=toolbar, submenu=submenu)
        root = etree.fromstring(res['arch'])
        root.set('create', 'false')
        root.set('import', 'false')
        root.set('edit', 'false')
        res['arch'] = etree.tostring(root)  
        return res
    
    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('purchase.refund') or '/'
        return super(PurchaseRefund, self).create(vals)
    
    def get_linked_purchase(self):
        purchases = []
        for obj in self:
            for picking_id in obj.picking_ids:
                purchase = self.env['stock.picking'].get_po_from_picking(picking_id.id)
                if purchase not in purchases:
                    purchases.append(purchase)
        return purchases
                    
    @api.multi
    def compute_purchase_count(self):
        for obj in self:
            obj.purchase_count = len(obj.get_linked_purchase())
            
    @api.multi
    def compute_picking_count(self):
        for obj in self:
            obj.picking_count = len(obj.mapped('picking_ids'))
            
    @api.multi
    def compute_move_count(self):
        for obj in self:
            obj.stock_move_count = len(obj.mapped('picking_ids').mapped('move_lines'))
            
    @api.multi
    def show_linked_purchase(self):
        # Холбоотой худалдан авалт рүү үсрэх смарт товчноос дуудагдана
        ids = self.get_linked_purchase()
        return self.env['stock.picking'].get_object_views(ids, 'purchase.purchase_form_action', False)

    @api.multi
    def show_linked_stock_moves(self):
        # Холбоотой stock.move рүү үсрэх смарт товчноос дуудагдана
        ids = self.mapped('picking_ids').mapped('move_lines').mapped('id')
        return self.env['stock.picking'].get_object_views(ids, 'stock.action_receipt_picking_move', 'l10n_mn_stock.view_move_form_inherit')
    
    @api.multi
    def show_linked_pickings(self):
        # Холбоотой буцаалтруу үсрэх смарт товчноос дуудагдана
        ids = self.mapped('picking_ids').mapped('id')
        return self.env['stock.picking'].get_object_views(ids, 'stock.action_picking_tree', 'stock.view_picking_form')
    
    