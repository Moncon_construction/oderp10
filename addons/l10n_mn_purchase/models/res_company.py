# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _

class Company(models.Model):
    _inherit = 'res.company'

    purchase_method = fields.Selection([
        ('purchase', 'On ordered quantities'),
        ('receive', 'On received quantities'),
        ], string="Control Purchase Bills",
        help="On ordered quantities: control bills based on ordered quantities.\n"
        "On received quantities: control bills based on received quantity.", default="purchase")
    
    purchase_amount_method = fields.Selection([
        (0, 'On Net Method'),
        (1, 'On Gross Method'),
    ], string="Purchase Discount")

    purchase_show_tax = fields.Selection([
        ('subtotal', 'Show line subtotals without taxes (B2B)'),
        ('total', 'Show line subtotals with taxes included (B2C)')], string="Tax Display",
        default='subtotal',
        required=True)

    group_show_purchase_price_total = fields.Boolean(
        "Show total",
        implied_group='l10n_mn_purchase.group_show_purchase_price_total',
        group='base.group_portal,base.group_user,base.group_public')