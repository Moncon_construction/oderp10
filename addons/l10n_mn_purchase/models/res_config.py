# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _

class PurchaseConfigSettings(models.TransientModel):
    _inherit = 'purchase.config.settings'
  
    purchase_method = fields.Selection(related='company_id.purchase_method', string="Control Purchase Bills")
    module_l10n_mn_purchase_gross_method = fields.Selection([
        (0, 'On Net Method'),
        (1, 'On Gross Method')], "Purchase Amount Method", 
        help="""Install the module that allows to calculate purchase discount with GROSS METHOD.""")
    purchase_show_tax = fields.Selection(related='company_id.purchase_show_tax')
    group_show_purchase_price_total = fields.Boolean(related='company_id.group_show_purchase_price_total')

    @api.multi
    def set_purchase_tax_defaults(self):
        return self.env['ir.values'].sudo().set_default(
            'res.config', 'purchase_show_tax', self.purchase_show_tax)

    @api.onchange('purchase_show_tax')
    def _onchange_purchase_tax(self):
        if self.purchase_show_tax == "subtotal":
            self.update({
                'group_show_purchase_price_total': False,
            })
        else:
            self.update({
                'group_show_purchase_price_total': True,
            })
