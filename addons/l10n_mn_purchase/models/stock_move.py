# -*- coding: utf-8 -*-
from odoo import models, api


class StockMove(models.Model):
    _inherit = "stock.move"

    @api.multi
    def get_price_unit(self):
        # Худалдан авалтын мөрд хөнгөлөлт байвал хөнгөлөлттэй үнийг тооцно
        price_unit = super(StockMove, self).get_price_unit()
        if self.purchase_line_id:
            if self.purchase_line_id.discount and self.purchase_line_id.discount > 0:
                price_unit = price_unit * (1-(self.purchase_line_id.discount / 100))
        return price_unit

    @api.model
    def create(self, vals):
        order_line_obj = self.env['purchase.order.line']
        if 'purchase_line_id' in vals:
            order_line = order_line_obj.browse(vals['purchase_line_id'])
            if order_line.order_id.date_planned:
                vals.update({'date_expected': order_line.order_id.date_planned})
        return super(StockMove, self).create(vals)
