# -*- coding: utf-8 -*-

from odoo import models, fields, api

class StockPicking(models.Model):
    _inherit = 'stock.picking'

    @api.multi
    def check_purchase_received(self):
        for picking in self:
            purchase_order = False
            # Худалдан авалтаас үүссэн орох хөдөлгөөн эсэхийг шалгах
            if picking.picking_type_id.code == 'incoming':
                for move in picking.move_lines:
                    if move.purchase_line_id:
                        purchase_order = move.purchase_line_id.order_id
                        break
            if purchase_order:
                count = 0
                count_incoming_pickings = 0
                for p in purchase_order.picking_ids:
                    if p.state == 'done' and p.picking_type_id.code == 'incoming':
                        count += 1
                    if p.picking_type_id.code == 'incoming':
                        count_incoming_pickings += 1
                if count > 0 and count_incoming_pickings > 0:
                    if count == count_incoming_pickings:
                        purchase_order.is_product_received = True
                    else:
                        purchase_order.is_product_received = False

    @api.multi
    def do_transfer(self):
        res = super(StockPicking, self).do_transfer()
        self.check_purchase_received()
        return res
    
    @api.model    
    def get_po_from_picking(self, child_id):
        # Хүргэлттэй холбоотой худалдан авалтын id-г олж буцаана.
        purchase = False
        self._cr.execute("""
        SELECT id FROM purchase_order WHERE name IN (
            SELECT origin
            FROM stock_picking WHERE id IN 
            (
            WITH RECURSIVE whosYourDaddy AS (
            
                /* non-recursive term */
                SELECT c.id, c.name, c.origin
                FROM stock_picking c
                WHERE c.id = %s
            
                UNION
            
                /* recursive term */
                SELECT sp.id, sp.name, sp.origin
                FROM stock_picking sp 
                INNER JOIN whosYourDaddy p1 ON p1.origin = sp.name
                     
            ) 
            
            SELECT MIN(id)
            FROM whosYourDaddy
            )
        )
        """ % child_id)
        results = self._cr.fetchall()
        if results and len(results) > 0:
            purchase = results[0]
            
        return purchase
        
    @api.model
    def get_purchase_picking_ids(self):
        # Худалдан авалтаас үүссэн батлагдсан хүргэлтүүд болон тэдгээрийн батлагдсан буцаалтуудын id-г буцаана.
        self.env.cr.execute("""
            WITH RECURSIVE sub_move AS (

            /* non-recursive term */
            SELECT id, origin_returned_move_id
            FROM stock_move
            WHERE purchase_line_id IS NOT NULL
        
            UNION
        
                /* recursive term */
                SELECT sm.id, sm.origin_returned_move_id
                FROM stock_move sm
        
                INNER JOIN sub_move s ON s.id = sm.origin_returned_move_id
                                 
            ) 
            
            SELECT sp.id
            FROM sub_move
            INNER JOIN stock_move m ON m.id = sub_move.id
            INNER JOIN stock_picking sp ON sp.id = m.picking_id
            WHERE sp.state = 'done'
            ORDER BY sp.id
        """)
        qry_results = self.env.cr.fetchall()
        ids = [qry_result[0] for qry_result in qry_results] if len(qry_results) > 0 else []
        
        return ids
        
    def get_object_views(self, ids, action_id, form_view_id):
        # Тохирох view-г буцаана
        action = self.env.ref(action_id)
        result = action.read()[0]
        result.pop('id', None)
        result['context'] = {}

        # choose the view_mode accordingly
        result['domain'] = "[('id','in',[%s])]" % (','.join(map(str, ids)) if len(ids) > 0 else '')
        if len(ids) == 1 and form_view_id:
            res = self.env.ref(form_view_id, False)
            result['views'] = [(res and res.id or False, 'form')]
            result['res_id'] = ids and ids[0] or False
        
        return result
    
    def get_purchase_received_picking(self):
        # Худалдан авалт/Ирсэн бараанууд цэснээс дуудагдаж худалдан авалттай холбоотой бүх хүргэлтийг олж буцаана.
        ids = self.get_purchase_picking_ids()
        return self.get_object_views(ids, 'stock.action_picking_tree', 'stock.view_picking_form')
    
    
    
