# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class StockReturnPicking(models.TransientModel):
    _inherit = 'stock.return.picking'

    purchase_refund_id = fields.Many2one('purchase.refund', ondelete='cascade', string='Purchase Refund')
    
    @api.multi
    def _create_returns(self):
        # Худалдан авалттай холбоотой хүргэлт бол буцаалтын мэдээллийг үүсгэж ХУДАЛДАН АВАЛТЫН БУЦААЛТ цэс рүү нэмдэг болгов.
        res = super(StockReturnPicking, self)._create_returns()
        
        picking = res[0]
        purchase = self.env['stock.picking'].get_po_from_picking(picking)
        if purchase:
            purchase_id = self.env['purchase.order'].browse(purchase).sudo()
            employee_id = self.env['hr.employee'].sudo().search([('company_id', '=', purchase_id.company_id.id), ('resource_id.user_id', '=', self.env.uid)], limit=1)
            
            refund_id = self.env['purchase.refund'].create({
                'partner_id': purchase_id.partner_id.id,
                'employee_id': employee_id.id or False,
                'picking_ids': [(6, 0, [picking])]
            })
            
            self.write({'purchase_refund_id': refund_id.id})
        
        return res