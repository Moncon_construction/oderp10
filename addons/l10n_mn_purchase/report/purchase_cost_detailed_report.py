# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
from datetime import date, datetime, timedelta
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from operator import itemgetter
from odoo.exceptions import ValidationError, UserError

from odoo import api, fields, models, _
from openerp.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class PurchaseDetailedReport(models.TransientModel):
    """
        Худалдан Авалтын  Дэлгэрэнгүй Тайлан
    """
    _name = "purchase.detailed.report"
    _description = "Mongolian Purchase Detailed Report"

    date_from = fields.Date("Start Date", required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    date_to = fields.Date("End Date", required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    company_id = fields.Many2one('res.company', string='Company', required=True,
                                 default=lambda self: self.env.user.company_id.id)

    partner_id = fields.Many2one('res.partner', string='Supplier')
    purchase_order = fields.Many2one('purchase.order', string='Purchase Order', domain="[('state', 'in', ('purchase','done'))]")
    state = fields.Selection([('confirmed', 'Confirmed'), ('delivered', 'Delivered')],
                             string='Type', required=True, default='confirmed')
    purchase_type = fields.Selection([('all', 'All'),
                                      ('foreign', 'Foreign'),
                                      ('local', 'Local'), ], string='Purchase Type', default='all')

    def row_column(self, order_amount_rows, col):
        rowcol = ""
        for i in order_amount_rows:
            rowcol += xl_rowcol_to_cell(i, col) + "+"
        return rowcol

    def purchase(self, type):
        if type == 'all':
            if self.purchase_order:
                purchase_order_obj = self.purchase_order
            else:
                if self.partner_id:
                    purchase_order_obj = self.env['purchase.order'].search([
                        ('partner_id', '=', self.partner_id.id),
                        ('date_order', '>=', self.date_from),
                        ('date_order', '<=', self.date_to),
                        ('company_id', '=', self.company_id.id),
                        ('state', 'in', ('purchase', 'done')),
                    ], order='date_order')
                else:
                    purchase_order_obj = self.env['purchase.order'].search([
                        ('date_order', '>=', self.date_from),
                        ('date_order', '<=', self.date_to),
                        ('company_id', '=', self.company_id.id),
                        ('state', 'in', ('purchase', 'done'))], order='date_order')
        else:
            if self.purchase_order:
                purchase_order_obj = self.purchase_order
            else:
                if self.partner_id:
                    purchase_order_obj = self.env['purchase.order'].search([
                        ('partner_id', '=', self.partner_id.id),
                        ('date_order', '>=', self.date_from),
                        ('date_order', '<=', self.date_to),
                        ('company_id', '=', self.company_id.id),
                        ('state', 'in', ('purchase', 'done')),
                        ('purchase_order_type', '=', type)
                    ], order='date_order')
                else:
                    purchase_order_obj = self.env['purchase.order'].search([
                        ('date_order', '>=', self.date_from),
                        ('date_order', '<=', self.date_to),
                        ('company_id', '=', self.company_id.id),
                        ('state', 'in', ('purchase', 'done')),
                        ('purchase_order_type', '=', type)], order='date_order')
        return purchase_order_obj

    def get_header(self, sheet, colx, rowx, format_title, num_of_extra_cost):
        sheet.merge_range(rowx, 0, rowx + 1, 0, _('Order Date'), format_title)
        sheet.merge_range(rowx, 1, rowx + 1, 1, _('Purchase Type'), format_title)
        sheet.merge_range(rowx, 2, rowx, 3, _('Inventories'), format_title)
        sheet.merge_range(rowx, 4, rowx + 1, 4, _('Currency'), format_title)
        sheet.merge_range(rowx, 5, rowx + 1, 5, _('Rate'), format_title)
        sheet.merge_range(rowx, 6, rowx + 1, 6, _('Quantity'), format_title)
        sheet.merge_range(rowx, 7, rowx + 1, 7, _('Unit price (Currency)'), format_title)
        sheet.merge_range(rowx, 8, rowx + 1, 8, _('Total amount (Currency)'), format_title)
        sheet.write(rowx + 1, 2, _('Code'), format_title)
        sheet.write(rowx + 1, 3, _('Name'), format_title)
        colx = 9
        return sheet, colx, num_of_extra_cost

    def get_value(self, sheet, order_line, rowx, purchase, index, format_content_text, format_content_float, format_content_float_amount):
        sheet.write(rowx, 0, index, format_content_text)
        if purchase.purchase_order_type == 'foreign':
            sheet.write(rowx, 1, _('Foreign Purchase'), format_content_text)
        elif purchase.purchase_order_type == 'local':
            sheet.write(rowx, 1, _('Local Purchase'), format_content_text)
        else:
            sheet.write(rowx, 1, 'Undefined', format_content_text)
        amount_discount = order_line.price_unit * order_line.discount / 100
        unit_amount_discount = order_line.price_unit - amount_discount
        sheet.write(rowx, 2, '%s' % (order_line.product_id.default_code or ""), format_content_text)
        sheet.write(rowx, 3, order_line.product_id.name, format_content_text)
        sheet.write(rowx, 4, purchase.currency_id.name, format_content_text)
        currency_rate = self.env['res.currency.rate'].search(
            [('currency_id', '=', purchase.currency_id.id), ('name', '<=', purchase.currency_cal_date)], order='name desc')
        currency_rate = purchase.currency_id.rate if not currency_rate else currency_rate[0].alter_rate
        sheet.write(rowx, 5, currency_rate, format_content_float)
        sheet.write(rowx, 6, order_line.product_qty, format_content_float)
        sheet.write(rowx, 7, unit_amount_discount * currency_rate, format_content_float)
        sheet.write(rowx, 8, unit_amount_discount * order_line.product_qty * currency_rate, format_content_float)
        return sheet

    def get_footer(self, sheet, rowx, format_group_right, format_content_float_amount, order_amount_rows, num, num_of_extra_cost):
        sheet.merge_range(rowx, 0, rowx, 5, _('Order Amount'), format_group_right)
        order_amount_rows.append(rowx)
        sheet.write_formula(rowx, 6, '{=SUM(' + xl_rowcol_to_cell(rowx - num, 6) + ':' + xl_rowcol_to_cell(rowx - 1, 6) + ')}', format_content_float_amount)
        sheet.write_formula(rowx, 7, '{=SUM(' + xl_rowcol_to_cell(rowx - num, 7) + ':' + xl_rowcol_to_cell(rowx - 1, 7) + ')}', format_content_float_amount)
        sheet.write_formula(rowx, 8, '{=SUM(' + xl_rowcol_to_cell(rowx - num, 8) + ':' + xl_rowcol_to_cell(rowx - 1, 8) + ')}', format_content_float_amount)

        return sheet, order_amount_rows

    def get_footer1(self, sheet, rowx, order_amount_rows, format_title_right, num_of_extra_cost):
        sheet.merge_range(rowx, 0, rowx, 5, _("Total Amount"), format_title_right)

        rowcol = self.row_column(order_amount_rows, 6)
        sheet.write_formula(rowx, 6, '{=SUM(%s0)}' % rowcol, format_title_right)

        sheet.write(rowx, 7, " ", format_title_right)

        rowcol = self.row_column(order_amount_rows, 8)
        sheet.write_formula(rowx, 8, '{=SUM(%s0)}' % rowcol, format_title_right)
        rowx += 1
        return sheet, order_amount_rows

    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        format_title = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 9,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter',
            'border': 1,
            'text_wrap': 1,
            'bg_color': '#83CAFF'
        })
        format_content_float_amount = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 9,
            'align': 'right',
            'valign': 'vcenter',
            'border': 1,
            'num_format': '#,##0.00',
            'bg_color': '#CFE7F5'
        })
        format_title_right = book.add_format({
            'font_name': 'Times New Roman',
            'font_size': 9,
            'bold': True,
            'align': 'right',
            'valign': 'vcenter',
            'border': 1,
            'text_wrap': 1,
            'bg_color': '#83CAFF',
            'num_format': '#,##0.00'
        })
        # create name
        report_name = _('Purchase Detailed Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_filter_right = book.add_format(ReportExcelCellStyles.format_filter_right)
        format_group_right = book.add_format(ReportExcelCellStyles.format_group_right)
        format_group_left = book.add_format(ReportExcelCellStyles.format_group_left)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_float_color = book.add_format(ReportExcelCellStyles.format_content_float_color)
        format_content_bold_text = book.add_format(ReportExcelCellStyles.format_content_bold_text)
        format_content_bold_right = book.add_format(ReportExcelCellStyles.format_content_bold_right)
        format_content_bold_left = book.add_format(ReportExcelCellStyles.format_content_bold_left)
        format_content_bold_float = book.add_format(ReportExcelCellStyles.format_content_bold_float)

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(
            filename_prefix=('Purchase  Detailed Report'), form_title=file_name, date_to=self.date_to,
            date_from=self.date_from).create({})

        #         create sheet

        sheet = book.add_worksheet(report_name)
        sheet.set_column('A:A', 20)
        sheet.set_column('B:B', 15)
        sheet.set_column('C:C', 13)
        sheet.set_column('D:D', 9)
        sheet.set_column('E:E', 7)
        sheet.set_column('F:F', 8)
        sheet.set_column('G:G', 8)
        sheet.set_column('H:H', 11)
        sheet.set_column('I:I', 11)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 0
        sheet.write(rowx, 0, '%s: %s' % (_('Company'), self.company_id.name), format_filter)
        rowx += 4
        sheet.write(rowx, 0, '%s: %s' % (_('Printed date'), time.strftime('%Y-%m-%d')), format_filter)
        rowx += 1

        sheet.set_row(6, 30)

        # Нийлүүлэгч сонгосон эсэхийг шалгах
        if self.purchase_type == 'all':
            purchase_order_obj = self.purchase('all')
        elif self.purchase_type == 'local':
            purchase_order_obj = self.purchase('local')
        elif self.purchase_type == 'foreign':
            purchase_order_obj = self.purchase('foreign')

        # Төрөл шалгах
        if self.state == "delivered":
            for obj in purchase_order_obj:
                if not obj.is_shipped:
                    purchase_order_obj = purchase_order_obj - obj

        colx = 10
        num_of_extra_cost = 0
        sheet, colx, num_of_extra_cost = self.get_header(sheet, colx, rowx, format_title, num_of_extra_cost)
        sheet.merge_range(2, 0, 2, colx, report_name.upper(), format_name)
        rowx += 2
        order_amount_rows = []
        if purchase_order_obj:
            for purchase in purchase_order_obj:
                sheet.write(rowx, 0, '%s: %s' % (_('Supplier'), purchase.partner_id.name), format_title)
                for i in range(1, colx):
                    sheet.write(rowx, i, '', format_title)
                end_date = purchase.date_order.split(" ")
                sheet.write(rowx + 1, 0, end_date[0], format_group_left)
                sheet.merge_range(rowx + 1, 1, rowx + 1, 2, '%s: %s' % (_('Order Number'), purchase.name),
                                  format_group_left)
                for i in range(3, colx):
                    sheet.write(rowx + 1, i, '', format_group_left)
                rowx += 2
                index = 1
                if purchase.order_line:
                    num = 0
                    for order_line in purchase.order_line:
                        sheet = self.get_value(sheet, order_line, rowx, purchase, index, format_content_text, format_content_float, format_content_float_amount)
                        rowx += 1
                        index += 1
                        num += 1
                    sheet, order_amount_rows = self.get_footer(sheet, rowx, format_group_right, format_content_float_amount, order_amount_rows, num, num_of_extra_cost)
                rowx += 1
            sheet, order_amount_rows = self.get_footer1(sheet, rowx, order_amount_rows, format_title_right, num_of_extra_cost)

            rowx += 2
            sheet.write(rowx, 0, '%s..........................(                                               ) ' % _('Inventory Accountant'), format_filter)
            rowx += 1
            sheet.write(rowx, 0, '%s..........................(                                               ) ' % _('General Accountant'), format_filter)

        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()
