# -*- encoding: utf-8 -*-
##############################################################################
import calendar
from io import BytesIO
import base64
import time
from odoo.exceptions import UserError
from datetime import date, timedelta
from odoo.tools.translate import _
import datetime
from odoo import api, fields, models
from openerp.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell


class PurchaseDetailedDisplayReport(models.Model):
    """
        Худалдан авалтын дэлгэрэнгүй тайлан /дэлгэцээр/
    """
    _name = "purchase.detailed.report.display"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = "Purchased Detailed Display Report"

    def group_by_selection(self):
        type_selection = [('by_warehouse', _('By warehouse')), ('by_supplier', _('By supplier')), ('by_purchase', _('By purchase')),
                          ('no_group_by', _('No group by'))]
        is_shipment_model_exist = self.env.get('purchase.shipment')
        if is_shipment_model_exist is not None:
            type_selection.append(('by_shipment', _('By shipment')))
        return type_selection

    @api.multi
    def _get_month_last_day(self):
        date = datetime.date.today()
        end_date = datetime.datetime(date.year, date.month, calendar.mdays[date.month])
        return end_date

    @api.multi
    def name_get(self):
        result = []
        for rep in self:
            result.append((rep.id, (_(u"%s - [%s - %s] term of detailed purchase report")) % (rep.company_id.name, rep.date_from, rep.date_to)))
        return result

    @api.depends('company_id','date_from','date_to')
    def _compute_name(self):
        for rep in self:
            if rep.company_id and rep.date_to and rep.date_from:
                rep.name = (_(u"%s - [%s - %s] term of detailed purchase report")) % (rep.company_id.name, rep.date_from,
                                                                              rep.date_to)

    @api.multi
    def _default_warehouse(self):
        _warehouse_ids = []
        for warehouse in self.env.user.allowed_warehouses:
            _warehouse_ids.append(warehouse.id)
        if _warehouse_ids:
            return [('id', 'in', _warehouse_ids)]
        else:
            return False

    def _domain_warehouses(self):
        return [
            ('id', 'in', self.env.user.allowed_warehouses.ids),
            '|', ('company_id', '=', False), ('company_id', 'child_of', [self.env.user.company_id.id])
        ]

    name = fields.Char(compute='_compute_name', string='Name', required=True)
    date_from = fields.Date("Start Date", required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    date_to = fields.Date("End Date", required=True, default=lambda self: self._get_month_last_day())
    company_id = fields.Many2one('res.company', string='Company', required=True,
                                 default=lambda self: self.env.user.company_id.id)
    type = fields.Selection([('confirmed', 'Confirmed Order'), ('delivered', 'Delivered Order')],
                             string='Type', required=True, default='confirmed')
    purchase_type = fields.Selection([('all', 'All'),
                                      ('foreign', 'Foreign'),
                                      ('local', 'Local'), ], string='Purchase Type', default='all')
    state = fields.Selection([('draft', 'Draft'),
                              ('approved', 'Approved')], string='State', default='draft', required=True,
                             track_visibility='onchange')
    group_by = fields.Selection(group_by_selection, string='Group By', default='by_purchase', required=True)
    warehouse_ids = fields.Many2many('stock.warehouse', string='Warehouses', default=_default_warehouse,
                                     domain=_domain_warehouses)
    supplier_ids = fields.Many2many('res.partner', string='Suppliers')
    category_ids = fields.Many2many('product.category', string='Product Categories')
    product_ids = fields.Many2many('product.product', string='Products')
    purchase_order_ids = fields.Many2many('purchase.order', string='Purchase Orders')
    line_ids = fields.One2many('purchase.detailed.display.report.line', 'purchased_detailed_display_report_id', string='Lines', readonly=True, copy=False)

    @api.multi
    def get_filter(self):
        where = ' WHERE p.company_id = ' + str(self.company_id.id) + ' '
        where += " AND p.date_order BETWEEN '" + self.date_from + "' AND '" + self.date_to + "' "
        if self.purchase_type != 'all':
            where += "AND p.purchase_order_type = '" + self.purchase_type + "' "
        if self.type == 'confirmed':
            where += " AND p.state='purchase'"
        else:
            where += " AND sm.state='done'"

        if self.warehouse_ids:
            where += ' AND p.warehouse_id IN (' + ','.join(map(str, self.warehouse_ids.ids)) + ') '

        if self.supplier_ids:
            where += ' AND p.partner_id IN (' + ','.join(map(str, self.supplier_ids.ids)) + ') '
        if self.category_ids:
            categ_ids = self.env['product.category'].search([('parent_id', 'child_of', self.category_ids.ids)])
            where += ' AND pt.categ_id IN (' + ','.join(map(str, categ_ids.ids)) + ') '

        if self.product_ids:
            where += ' AND l.product_id IN (' + ','.join(map(str, self.product_ids.ids)) + ') '

        if self.purchase_order_ids:
            where += ' AND p.id IN (' + ','.join(map(str, self.purchase_order_ids.ids)) + ') '

        return where

    @api.multi
    def get_select(self):
        select = "p.date_order as order_date, p.id as po_id, p.warehouse_id as w_id, " \
                 "p.name as po_name, l.id as line_id, " \
                 "rp.id as partner_id, p.purchase_order_type as po_type, pp.default_code as default_code, " \
                 "pp.id as product_id, c.id as currency_id, l.product_qty as qty "
        return select

    @api.multi
    def get_join(self):
        join = ''
        if not self.type == 'confirmed':
            join += "LEFT JOIN purchase_order_line pol on p.id = pol.order_id " \
             "LEFT JOIN stock_move sm on pol.id = sm.purchase_line_id " 
        return join

    @api.multi
    def get_data(self):
        select = self.get_select()
        where = self.get_filter()
        join = self.get_join()
        self.env.cr.execute("SELECT " + select + ""
                            "FROM purchase_order p LEFT JOIN purchase_order_line l ON p.id=l.order_id "
                            "LEFT JOIN res_partner rp ON (rp.id = p.partner_id) "
                            "LEFT JOIN product_product pp ON (pp.id = l.product_id) "
                            "LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id) "
                            "LEFT JOIN res_currency c ON l.currency_id = c.id " + join + where + " order by p.date_order ")
        lines = self.env.cr.dictfetchall()
        return lines

    @api.multi
    def create_lines(self, data, line):
        line_obj = self.env['purchase.detailed.display.report.line']
        line_obj.create(data[0])
        return True

    @api.multi
    def compute(self):
        # Тооцоолол хийх функц
        self.line_ids.unlink()
        lines = self.get_data()
        for line in lines:
            data = []
            order = self.env['purchase.order'].browse(line['po_id'])
            line_obj = self.env['purchase.order.line'].browse(line['line_id'])
            currency_rate_obj = self.env['res.currency.rate'].search(
                [('currency_id', '=', order.currency_id.id), ('name', '<=', order.currency_cal_date)],
                order='name desc')
            currency_rate = order.currency_id.rate if not currency_rate_obj else currency_rate_obj[0].alter_rate
            amount_discount = line_obj.price_unit * line_obj.discount / 100
            unit_amount_discount = line_obj.price_unit - amount_discount
            # Мөрийг үүсгэх
            data.append({
                'purchased_detailed_display_report_id': self.id,
                'order_id': line['po_id'],
                'order_date': line['order_date'],
                'order_number': line['po_name'],
                'supplier_id': line['partner_id'],
                'purchase_order_type': line['po_type'],
                'product_default_code': line['default_code'],
                'product_id': line['product_id'],
                'currency_id': line['currency_id'],
                'currency_rate': currency_rate,
                'quantity': line['qty'],
                'price_unit': unit_amount_discount * currency_rate,
                'price_total': unit_amount_discount * currency_rate * line_obj.product_qty,
            })
            self.create_lines(data, line)
        return True

    @api.multi
    def unlink(self):
        for report in self:
            if report.state != 'draft':
                raise UserError(_('Delete only draft in state'))
        return super(PurchaseDetailedDisplayReport, self).unlink()

    @api.multi
    def validate(self):
        # Батлах
        self.write({'state': 'approved'})

    @api.multi
    def action_to_draft(self):
        # Ноороглох
        self.write({'state': 'draft'})

    def get_header(self, sheet, colx, rowx, format_title, num_of_extra_cost):
        is_shipment_model_exist = self.env.get('purchase.shipment')
        colx = 0
        sheet.merge_range(rowx, colx, rowx+1, colx, _('Order Date (Display Report)'), format_title)
        colx += 1
        sheet.merge_range(rowx, colx, rowx+1, colx, _('Order Number (Display Report)'), format_title)
        colx += 1
        if is_shipment_model_exist is not None:
            sheet.merge_range(rowx, colx, rowx + 1, colx, _('Shipment №'), format_title)
            colx += 1
        sheet.merge_range(rowx, colx, rowx+1, colx, _('Supplier (Display Report)'), format_title)
        colx += 1
        sheet.merge_range(rowx, colx, rowx+1, colx, _('Purchase type'), format_title)
        colx += 1
        sheet.merge_range(rowx, colx, rowx+1, colx, _('Product code (Display Report)'), format_title)
        colx += 1
        sheet.merge_range(rowx, colx, rowx+1, colx, _('Product name (Display Report)'), format_title)
        colx += 1
        sheet.merge_range(rowx, colx, rowx+1, colx, _('Currency (Display Report)'), format_title)
        colx += 1
        sheet.merge_range(rowx, colx, rowx+1, colx, _('Currency rate'), format_title)
        colx += 1
        sheet.merge_range(rowx, colx, rowx+1, colx, _('Quantity (Display Report)'), format_title)
        colx += 1
        sheet.merge_range(rowx, colx, rowx+1, colx, _('Received Quantity (Display Report)'), format_title)
        colx += 1
        sheet.merge_range(rowx, colx, rowx+1, colx, _('Unit price (Currency Display Report)'), format_title)
        colx += 1
        sheet.merge_range(rowx, colx, rowx+1, colx, _('Total amount (Currency Display Report)'), format_title)
        colx += 1
        return sheet, colx, num_of_extra_cost

    def row_column(self, order_amount_rows, col):
        rowcol = ""
        for i in order_amount_rows:
            rowcol += xl_rowcol_to_cell(i, col) + "+"
        return rowcol

    def get_value(self, sheet, order_line, purchase, rowx, format_content_text, format_content_float):
        is_shipment_model_exist = self.env.get('purchase.shipment')
        order = self.env['purchase.order'].browse(purchase.id)
        order_line = self.env['purchase.order.line'].browse(order_line.id)
        currency_rate_obj = self.env['res.currency.rate'].search(
            [('currency_id', '=', order.currency_id.id), ('name', '<=', order.currency_cal_date)],
            order='name desc')
        currency_rate = order.currency_id.rate if not currency_rate_obj else currency_rate_obj[0].alter_rate
        amount_discount = order_line.price_unit * order_line.discount / 100
        unit_amount_discount = order_line.price_unit - amount_discount
        type_name = ''
        if order.purchase_order_type == 'local':
            type_name = (_(u'Local'))
        elif order.purchase_order_type == 'foreign':
            type_name = (_(u'Foreign'))
        colx = 0
        sheet.write(rowx, colx, datetime.datetime.strptime(order.date_order, '%Y-%m-%d %H:%M:%S').strftime('%Y.%m.%d'),
                    format_content_text)
        colx += 1
        sheet.write(rowx, colx, order.name, format_content_text)
        colx += 1
        if is_shipment_model_exist is not None:
            if order_line.shipment_id:
                shipment = self.env['purchase.shipment'].browse(order_line.shipment_id.id)
                sheet.write(rowx, colx, shipment.name if shipment else '', format_content_text)
            else:
                sheet.write(rowx, colx, '', format_content_text)
            colx += 1
        sheet.write(rowx, colx, order.partner_id.name, format_content_text)
        colx += 1
        sheet.write(rowx, colx, type_name, format_content_text)
        colx += 1
        sheet.write(rowx, colx, order_line.product_id.default_code, format_content_text)
        colx += 1
        sheet.write(rowx, colx, order_line.product_id.name, format_content_text)
        colx += 1
        sheet.write(rowx, colx, order.currency_id.name, format_content_text)
        colx += 1
        sheet.write(rowx, colx, currency_rate, format_content_float)
        colx += 1
        sheet.write(rowx, colx, order_line.product_qty, format_content_float)
        colx += 1
        sheet.write(rowx, colx, order_line.qty_received, format_content_float)
        colx += 1
        sheet.write(rowx, colx, unit_amount_discount, format_content_float)
        colx += 1
        sheet.write(rowx, colx, unit_amount_discount * currency_rate * order_line.product_qty, format_content_float)
        colx += 1

        return sheet

    def get_footer(self, sheet, rowx, start_row,format_group_right, format_group_float, num_of_extra_cost):
        is_shipment_model_exist = self.env.get('purchase.shipment')
        if is_shipment_model_exist is not None:
            coly = 8
        else:
            coly = 7
        sheet.merge_range(rowx, 0, rowx, coly, _("Total Amount (Display Report)"), format_group_right)
        sheet.write_formula(rowx, coly+1, '{=SUM(' + xl_rowcol_to_cell(start_row, coly+1) + ':' + xl_rowcol_to_cell(rowx-1, coly+1) + ')}', format_group_float)
        sheet.write(rowx, coly+2, " ", format_group_right)
        sheet.write_formula(rowx, coly+3, '{=SUM(' + xl_rowcol_to_cell(start_row, coly+3) + ':' + xl_rowcol_to_cell(rowx-1, coly+3) + ')}', format_group_float)

        return sheet, rowx

    def get_header_sub(self,sheet, rowx, rows,order_amount_rows, format_group_right,format_group_float, num_of_extra_cost, num):
        is_shipment_model_exist = self.env.get('purchase.shipment')
        if is_shipment_model_exist is not None:
            coly = 8
        else:
            coly = 7
        order_amount_rows.append(rows-1)
        sheet.write_formula(rows-1, coly+1,
                            '{=SUM(' + xl_rowcol_to_cell(rows, coly+1) + ':' + xl_rowcol_to_cell(rowx-1, coly+1) + ')}',
                            format_group_float)
        sheet.write_formula(rows-1, coly+2,
                            '{=SUM(' + xl_rowcol_to_cell(rows, coly+2) + ':' + xl_rowcol_to_cell(rowx-1, coly+2) + ')}',
                            format_group_float)
        sheet.write_formula(rows-1, coly+4,
                            '{=SUM(' + xl_rowcol_to_cell(rows, coly+4) + ':' + xl_rowcol_to_cell(rowx-1, coly+4) + ')}',
                            format_group_float)
        return sheet, order_amount_rows

    def get_footer1(self, sheet, rowx, order_amount_rows, format_group_right,format_group_float, num_of_extra_cost):
        is_shipment_model_exist = self.env.get('purchase.shipment')
        if is_shipment_model_exist is not None:
            coly = 8
        else:
            coly = 7

        sheet.merge_range(rowx, 0, rowx, coly, _("Total Amount (Display Report)"), format_group_right)
        rowcol = self.row_column(order_amount_rows, coly+1)
        sheet.write_formula(rowx, coly+1, '{=SUM(%s0)}' % rowcol, format_group_float)
        rowcol = self.row_column(order_amount_rows, coly+2)
        sheet.write_formula(rowx, coly+2, '{=SUM(%s0)}' % rowcol, format_group_float)
        sheet.write(rowx, coly+3, " ", format_group_right)
        rowcol = self.row_column(order_amount_rows, coly + 4)
        sheet.write_formula(rowx, coly+4, '{=SUM(%s0)}' % rowcol, format_group_float)
        rowx += 1
        return sheet, order_amount_rows

    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Purchase Detailed Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_filter_right = book.add_format(ReportExcelCellStyles.format_filter_right)
        format_group_right = book.add_format(ReportExcelCellStyles.format_group_right)
        format_group_left = book.add_format(ReportExcelCellStyles.format_group_left)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_float_color = book.add_format(ReportExcelCellStyles.format_content_float_color)
        format_content_bold_text = book.add_format(ReportExcelCellStyles.format_content_bold_text)
        format_content_bold_right = book.add_format(ReportExcelCellStyles.format_content_bold_right)
        format_content_bold_left = book.add_format(ReportExcelCellStyles.format_content_bold_left)
        format_content_bold_float = book.add_format(ReportExcelCellStyles.format_content_bold_float)

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(
            filename_prefix=('Purchase  Detailed Report'), form_title=file_name, date_to=self.date_to,
            date_from=self.date_from).create({})

        #         create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_column('A:A', 15)
        sheet.set_column('B:B', 15)
        sheet.set_column('C:C', 13)
        sheet.set_column('D:D', 20)
        sheet.set_column('E:E', 15)
        sheet.set_column('F:F', 15)
        sheet.set_column('G:G', 15)
        sheet.set_column('H:H', 8)
        sheet.set_column('I:I', 8)
        sheet.set_column('J:J', 15)
        sheet.set_column('K:K', 15)
        sheet.set_column('L:L', 15)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 0
        sheet.write(rowx, 0, '%s: %s' % (_('Company (Display Report)'), self.company_id.name), format_filter)
        rowx += 4
        sheet.write(rowx, 0, '%s: %s - %s' % (_('Report period'), self.date_from, self.date_to), format_filter)
        rowx += 1
        sheet.write(rowx, 0, '%s: %s' % (_('Created date'), time.strftime('%Y-%m-%d')), format_filter)
        rowx += 1

        sheet.set_row(6, 30)
        colx = 10
        data = self.get_data()
        is_shipment_model_exist = self.env.get('purchase.shipment')
        line_ids = []
        warehouse_ids = []
        supplier_ids = []
        order_ids = []
        shipment_ids = []
        for line in data:
            if line['line_id'] not in line_ids:
                line_ids.append(line['line_id'])
            if line['w_id'] not in warehouse_ids:
                warehouse_ids.append(line['w_id'])
            if line['partner_id'] not in supplier_ids:
                supplier_ids.append(line['partner_id'])
            if line['po_id'] not in order_ids:
                order_ids.append(line['po_id'])
            if is_shipment_model_exist is not None:
                if line['shipment_id'] not in shipment_ids:
                    shipment_ids.append(line['shipment_id'])

        num_of_extra_cost = 0
        sheet, colx, num_of_extra_cost = self.get_header(sheet, colx, rowx, format_title, num_of_extra_cost)
        sheet.merge_range(2, 0, 2, colx-1, report_name.upper(), format_name)
        rowx += 2
        lines = self.env['purchase.order.line'].browse(line_ids)
        warehouse = self.env['stock.warehouse'].browse(warehouse_ids)
        suppliers = self.env['res.partner'].browse(supplier_ids)
        orders = self.env['purchase.order'].browse(order_ids)
        start_row = rowx
        if lines:
            if self.group_by == 'no_group_by':
                num = 0
                for order_line in lines:
                    purchase = order_line.order_id
                    sheet = self.get_value(sheet, order_line, purchase, rowx, format_content_text, format_content_float)
                    rowx += 1
                    num += 1
                sheet, rowx = self.get_footer(sheet, rowx, start_row, format_group_right,format_group_float, num_of_extra_cost)
            elif self.group_by == 'by_warehouse' and warehouse:
                num = 0
                order_amount_rows = []
                for wh in warehouse:
                    sheet.merge_range(rowx, 0, rowx, 8 if is_shipment_model_exist is not None else 7, '%s' % wh.name, format_group_left)
                    for i in range(9 if is_shipment_model_exist is not None else 8, colx):
                        sheet.write(rowx, i, '', format_group_left)
                    rowx += 1
                    rows = rowx
                    for order_line in lines:
                        purchase = order_line.order_id
                        if purchase.warehouse_id.id == wh.id:
                            sheet = self.get_value(sheet, order_line, purchase, rowx, format_content_text,
                                                   format_content_float)
                            rowx += 1
                            num += 1
                    sheet, order_amount_rows = self.get_header_sub(sheet, rowx, rows,order_amount_rows, format_group_right,format_group_float, num_of_extra_cost, num)
                sheet, order_amount_rows = self.get_footer1(sheet, rowx, order_amount_rows, format_group_right,format_group_float,
                                                            num_of_extra_cost)
            elif self.group_by == 'by_supplier' and suppliers:
                num = 0
                order_amount_rows = []
                for supplier in suppliers:
                    sheet.merge_range(rowx, 0, rowx, 8 if is_shipment_model_exist is not None else 7, '%s' % supplier.name,
                                      format_group_left)
                    for i in range(9 if is_shipment_model_exist is not None else 8, colx):
                        sheet.write(rowx, i, '', format_group_left)
                    rowx += 1
                    rows = rowx
                    for order_line in lines:
                        purchase = order_line.order_id
                        if purchase.partner_id.id == supplier.id:
                            sheet = self.get_value(sheet, order_line, purchase, rowx, format_content_text,
                                                   format_content_float)
                            rowx += 1
                            num += 1
                    sheet, order_amount_rows = self.get_header_sub(sheet, rowx, rows, order_amount_rows,
                                                                   format_group_right,format_group_float, num_of_extra_cost, num)
                sheet, order_amount_rows = self.get_footer1(sheet, rowx, order_amount_rows, format_group_right,format_group_float,
                                                            num_of_extra_cost)
            elif self.group_by == 'by_purchase':
                num = 0
                order_amount_rows = []
                for order in orders:
                    sheet.merge_range(rowx, 0, rowx, 8 if is_shipment_model_exist is not None else 7,
                                      '%s' % order.name,
                                      format_group_left)
                    for i in range(9 if is_shipment_model_exist is not None else 8, colx):
                        sheet.write(rowx, i, '', format_group_left)
                    rowx += 1
                    rows = rowx
                    for order_line in lines:
                        purchase = order_line.order_id
                        if purchase.id == order.id:
                            sheet = self.get_value(sheet, order_line, purchase, rowx, format_content_text,
                                                   format_content_float)
                            rowx += 1
                            num += 1
                    sheet, order_amount_rows = self.get_header_sub(sheet, rowx, rows, order_amount_rows,
                                                                   format_group_right, format_group_float,num_of_extra_cost, num)
                sheet, order_amount_rows = self.get_footer1(sheet, rowx, order_amount_rows, format_group_right,format_group_float,
                                                            num_of_extra_cost)
            elif is_shipment_model_exist is not None and self.group_by == 'by_shipment':
                shipments = self.env['purchase.shipment'].browse(shipment_ids)
                num = 0
                order_amount_rows = []
                for shipment in shipments:
                    sheet.merge_range(rowx, 0, rowx, 8 if is_shipment_model_exist is not None else 7,
                                      '%s' % shipment.name if shipment.id is not None else _('Shipment Undefined'),
                                      format_group_left)
                    for i in range(9 if is_shipment_model_exist is not None else 8, colx):
                        sheet.write(rowx, i, '', format_group_left)
                    rowx += 1
                    rows = rowx
                    if shipment.id:
                        for order_line in lines:
                            purchase = order_line.order_id
                            if shipment.id == order_line.shipment_id.id:
                                sheet = self.get_value(sheet, order_line, purchase, rowx, format_content_text,
                                                       format_content_float)
                                rowx += 1
                                num += 1
                        sheet, order_amount_rows = self.get_header_sub(sheet, rowx, rows, order_amount_rows,
                                                                   format_group_right,format_group_float, num_of_extra_cost, num)
                    elif shipment.id is None:
                        for order_line in lines:
                            purchase = order_line.order_id
                            if not order_line.shipment_id:
                                sheet = self.get_value(sheet, order_line, purchase, rowx, format_content_text,
                                                       format_content_float)
                                rowx += 1
                                num += 1
                        sheet, order_amount_rows = self.get_header_sub(sheet, rowx, rows, order_amount_rows,
                                                                       format_group_right, format_group_float, num_of_extra_cost, num)

                sheet, order_amount_rows = self.get_footer1(sheet, rowx, order_amount_rows, format_group_right,format_group_float,
                                                            num_of_extra_cost)
        rowx += 2
        sheet.write(rowx, 12, '%s..........................(                                               ) ' % _(
            'Inventory Accountant'), format_filter)
        rowx += 1
        sheet.write(rowx, 12, '%s..........................(                                               ) ' % _(
            'General Accountant'), format_filter)

        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()


class PurchaseDetailedDisplayReportLine(models.Model):
    _name = 'purchase.detailed.display.report.line'
    _description = 'Purchased Detailed Display Report Line'

    purchased_detailed_display_report_id = fields.Many2one('purchase.detailed.report.display', string="Purchase Detailed Report", ondelete="cascade")
    order_id = fields.Many2one('purchase.order', string='Purchase Order')
    order_date = fields.Date(string='Order date')
    order_number = fields.Char(string='Order Number')
    supplier_id = fields.Many2one('res.partner', string='Supplier')
    purchase_order_type = fields.Selection([('foreign', 'Foreign'), ('local', 'Local')], string='Purchase type')
    product_default_code = fields.Char('Product default code')
    product_id = fields.Many2one('product.product', string='Product')
    currency_id = fields.Many2one('res.currency', string='Currency')
    currency_rate = fields.Float(string='Currency rate')
    quantity = fields.Float('Quantity')
    price_unit = fields.Float(string='Unit Price (By MNT)')
    price_total = fields.Float(string='Total Price (By MNT)')






