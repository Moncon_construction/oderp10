# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
from datetime import date,datetime,timedelta
from odoo.tools.float_utils import float_is_zero, float_compare
from odoo.tools.misc import formatLang
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from operator import itemgetter
from odoo.exceptions import ValidationError, UserError

from odoo import api, fields, models, _
from openerp.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
import odoo.addons.decimal_precision as dp

class PurchaseQuotationReport(models.TransientModel):
    """
        Mongolian Purchase Quotation Report
    """
    _name = "purchase.quotation.report"
    _description = "Mongolian Purchase Quotation Report"

    order_ids = fields.Many2many('purchase.order', 'quotation_report_to_order', 'report_id', 'order_id', string='Purchase Order')
    
    @api.multi
    def get_report_data(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_content_center = book.add_format(ReportExcelCellStyles.format_content_center)
        format_content_float_center = book.add_format(ReportExcelCellStyles.format_content_float_center)
        format_title_float_center = book.add_format(ReportExcelCellStyles.format_title_float_center)
        
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        # create name
        report_name = _('Purchase Quotation By XLS')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        
        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(
            filename_prefix=('Purchase Quotation By XLS'), form_title=file_name).create({})
            
            # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 1

        # compute column
        sheet.set_column('A:A', 4)
        sheet.set_column('B:B', 15)
        sheet.set_column('C:C', 8)
        sheet.set_column('D:D', 40)
        sheet.set_column('E:E', 30)
        sheet.set_column('F:F', 30)
        sheet.set_column('H:H', 25)
        
        active_ids = self._context.get('active_ids', []) or []
        purchases = self.env['purchase.order'].search([('id', 'in', active_ids)])
        
        if len(active_ids) == 0:
            purchases = self.order_ids
        
        for purchase in purchases:
            sheet.write(rowx+1, 0,'%s: %s' % (_('Company'), self.env.user.company_id.sudo().name), format_filter)
            sheet.merge_range(1, 0, 1, 7, report_name.upper(), format_name) 
            sheet.write(rowx+2, 0, '%s: %s' % (_('Purchase Order'), purchase.name), format_filter)
            sheet.write(rowx+3, 0, '%s: %s' % (_('Currency'), purchase.currency_id.name), format_filter)
            sheet.write(rowx+4, 0, '%s: %s' % (_('Order Date'), purchase.date_order), format_filter)
         
            rowx += 6  
            sheet.write(rowx, 0, _('Seq'), format_title)
            sheet.write(rowx, 1, _('Default Code'), format_title)
            sheet.write(rowx, 2, _('Bar Code'), format_title)
            sheet.write(rowx, 3, _('Product Name'), format_title)
            sheet.write(rowx, 4, _('Product Unit Of Measure'), format_title)
            sheet.write(rowx, 5, _('Suppleir Code'), format_title)
            sheet.write(rowx, 6, _('Unit Price'), format_title)
            sheet.write(rowx, 7, _('Order Quantity'), format_title)
            sheet.write(rowx, 8, _('Total Price'), format_title)       
        
            rowx += 1
            index = 1
            order_amount_rows =[]
            if purchase.order_line:
                num = 0
                for order_line in purchase.order_line:
                    sheet.write(rowx, 0, index, format_content_center)
                    sheet.write(rowx, 1, '%s' % (order_line.product_id.default_code or " "), format_content_center)
                    sheet.write(rowx, 2, '%s' % (order_line.product_id.barcode or " "), format_content_text)
                    sheet.write(rowx, 3, '%s' % (order_line.product_id.name or " "), format_content_text)
                    sheet.write(rowx, 4, '%s' % (order_line.product_uom.name  or " "), format_content_center)
                    sheet.write(rowx, 5, '%s' % (purchase.partner_ref or " "), format_content_center)
                    sheet.write(rowx, 6, order_line.price_unit , format_content_float_center)
                    sheet.write(rowx, 7, order_line.product_qty , format_content_float_center)
                    sheet.write(rowx, 8, order_line.price_unit * order_line.product_qty , format_content_float_center)
                    rowx += 1
                    index += 1
                    num += 1
                sheet.merge_range(rowx, 0, rowx, 6, _('Total Amount'), format_title)
                order_amount_rows.append(rowx)
                sheet.write_formula(rowx, 7, '{=SUM(' + xl_rowcol_to_cell(rowx - num, 7) + ':' + xl_rowcol_to_cell(rowx - 1, 7) + ')}', format_title_float_center)
                sheet.write_formula(rowx, 8, '{=SUM(' + xl_rowcol_to_cell(rowx - num, 8) + ':' + xl_rowcol_to_cell(rowx - 1 , 8) + ')}', format_title_float_center)
        rowx += 2
        book.close()

        return base64.encodestring(output.getvalue())

    @api.multi
    def export_report(self):
        
        report_name = _('Purchase Quotation By XLS')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(
            filename_prefix=('Purchase Quotation By XLS'), form_title=file_name).create({})
        
        
        report_excel_output_obj.filedata = self.get_report_data()  
       
        return report_excel_output_obj.export_report()
