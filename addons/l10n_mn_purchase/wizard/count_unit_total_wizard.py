# -*- coding: utf-8 -*-

from odoo import api, fields, models


class CountUnitTotalWizard(models.TransientModel):
    _name = 'count.unit.total.wizard'
 
    quantity = fields.Float(string='Enter Quantity')
    amount_of_row = fields.Float(string='Amount of row')
        
    def count_unit_total(self):
        line_ids = self.env['purchase.order.line'].browse(self._context.get('active_id'))
        
        for line in line_ids:
          unit_price = self.amount_of_row / self.quantity
          line_ids.update({
                     'price_unit': unit_price,
                     'product_qty': self.quantity
                 })        