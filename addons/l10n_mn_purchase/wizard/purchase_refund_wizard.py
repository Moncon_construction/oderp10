# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError, ValidationError


class PurchasePickingRefundLine(models.TransientModel):
    _name = "purchase.picking.refund.line"

    purchase_id = fields.Many2one('purchase.order', string='Purchase', ondelete='cascade')
    picking_id = fields.Many2one('stock.picking', required=False, ondelete='cascade', string='Picking')
    product_id = fields.Many2one('product.product', string="Product", required=True)
    quantity = fields.Float(string="Quantity", digits=dp.get_precision('Product Unit of Measure'), required=True)
    wizard_id = fields.Many2one('purchase.picking.refund', string="Wizard", ondelete='cascade')
    move_id = fields.Many2one('stock.move', string='Move')
    location_id = fields.Many2one('stock.location', related='move_id.location_dest_id', readonly=True, string='Return Location')
    returned_quantity = fields.Float(compute='compute_all_returned', string="Returned Quantity", digits=dp.get_precision('Product Unit of Measure'))
    delivered_qty = fields.Float(related='move_id.product_uom_qty', readonly=True, string='Delivered Quantity')
    all_returned = fields.Boolean(compute='compute_all_returned', string='Already Returned')
    
    @api.multi
    def compute_all_returned(self):
        def get_returned_qty(picking_id, product_id):
            self._cr.execute("""
                WITH RECURSIVE sub_move AS (
    
                    /* non-recursive term */
                    SELECT m.id, m.origin_returned_move_id, m.product_id,
                        CASE WHEN s.usage = 'internal' THEN -1 * m.product_uom_qty ELSE m.product_uom_qty END AS qty
                    FROM stock_move m
                    LEFT JOIN stock_location s ON s.id = m.location_dest_id
                    WHERE m.picking_id = %s AND m.product_id = %s
                
                    UNION
                
                    /* recursive term */
                    SELECT sm.id, sm.origin_returned_move_id, sm.product_id,
                        CASE WHEN s1.usage = 'internal' THEN -1 * sm.product_uom_qty ELSE sm.product_uom_qty END AS qty
                    FROM stock_move sm
                    LEFT JOIN stock_location s1 ON s1.id = sm.location_dest_id
                
                    INNER JOIN sub_move s ON s.id = sm.origin_returned_move_id AND s.product_id = sm.product_id
                         
                ) 
                
                SELECT COALESCE(SUM(qty), 0)
                FROM sub_move
                INNER JOIN stock_move m ON m.id = sub_move.id
                WHERE m.state != 'cancel' AND m.picking_id != %s
            """ % (picking_id, product_id, picking_id))
            return self._cr.fetchall()[0][0]
        
        for obj in self:
            returned_quantity = get_returned_qty(obj.picking_id.id, obj.product_id.id)
            obj.returned_quantity = returned_quantity
            if returned_quantity < 0 or returned_quantity > obj.delivered_qty:
                obj.all_returned = True
            else:
                obj.all_returned = False
            

class PurchasePickingRefund(models.TransientModel):
    _name = 'purchase.picking.refund'

    company_id = fields.Many2one('res.company', default=lambda self: self.env.user.company_id, string='Company')
    partner_id = fields.Many2one('res.partner', required=True, string='Partner')
    picking_ids = fields.Many2many('stock.picking', 'purchase_refund_wizard_to_picking', 'wiz_id', 'pick_id', required=True, ondelete='cascade', string='Picking')
    product_return_moves = fields.One2many('purchase.picking.refund.line', 'wizard_id', 'Moves')

    @api.multi
    def _create_returns(self):
        self.ensure_one()
        
        new_pickings = []
        return_moves = self.product_return_moves.mapped('move_id')
        unreserve_moves = self.env['stock.move']
        for move in return_moves:
            to_check_moves = self.env['stock.move'] | move.move_dest_id
            while to_check_moves:
                current_move = to_check_moves[-1]
                to_check_moves = to_check_moves[:-1]
                if current_move.state not in ('done', 'cancel') and current_move.reserved_quant_ids:
                    unreserve_moves |= current_move
                split_move_ids = self.env['stock.move'].search([('split_from', '=', current_move.id)])
                to_check_moves |= split_move_ids

        if unreserve_moves:
            unreserve_moves.do_unreserve()
            # break the link between moves in order to be able to fix them later if needed
            unreserve_moves.write({'move_orig_ids': False})

        returned_lines = 0
        for picking in self.picking_ids:
            # Бүх бараа нь буцаагдсан бол буцаалтын хүргэлт үүсгэхгү
            if not self.product_return_moves.filtered(lambda x: x.picking_id == picking and not x.all_returned):
                continue
            
            # create new picking for returned products
            picking_type_id = picking.picking_type_id.return_picking_type_id.id or picking.picking_type_id.id
            new_picking = picking.copy({
                'move_lines': [],
                'picking_type_id': picking_type_id,
                'state': 'draft',
                'partner_id': picking.partner_id.id or False,
                'origin': picking.name,
                'location_id': picking.location_dest_id.id,
                'location_dest_id': picking.location_id.id})
            new_picking.message_post_with_view('mail.message_origin_link',
                values={'self': new_picking, 'origin': picking},
                subtype_id=self.env.ref('mail.mt_note').id)
    
            for return_line in self.product_return_moves.filtered(lambda x: x.picking_id == picking and not x.all_returned):
                if not return_line.move_id:
                    raise UserError(_("You have manually created product lines, please delete them to proceed"))
                new_qty = return_line.quantity
                if new_qty:
                    # The return of a return should be linked with the original's destination move if it was not cancelled
                    if return_line.move_id.origin_returned_move_id.move_dest_id.id and return_line.move_id.origin_returned_move_id.move_dest_id.state != 'cancel':
                        move_dest_id = return_line.move_id.origin_returned_move_id.move_dest_id.id
                    else:
                        move_dest_id = False
    
                    returned_lines += 1
                    return_line.move_id.copy({
                        'product_id': return_line.product_id.id,
                        'product_uom_qty': new_qty,
                        'picking_id': new_picking.id,
                        'state': 'draft',
                        'location_id': return_line.move_id.location_dest_id.id,
                        'location_dest_id': return_line.move_id.location_id.id,
                        'picking_type_id': picking_type_id,
                        'warehouse_id': picking.picking_type_id.warehouse_id.id,
                        'origin_returned_move_id': return_line.move_id.id,
                        'procure_method': 'make_to_stock',
                        'move_dest_id': move_dest_id,
                    })
                    
            new_picking.action_confirm()
            new_pickings.append(new_picking.id)

        if not new_pickings:
           raise UserError(_("All pickings already returned !!!")) 
        if not returned_lines:
            raise UserError(_("Please specify at least one non-zero quantity."))

        employee_id = self.env['hr.employee'].sudo().search([('company_id', '=', self.company_id.id), ('resource_id.user_id', '=', self.env.uid)], limit=1)
        refund_id = self.env['purchase.refund'].create({
            'partner_id': self.partner_id.id,
            'employee_id': employee_id.id or False,
            'picking_ids': [(6, 0, new_pickings)]
        })
        
        return refund_id.id

    @api.multi
    def create_returns(self):
        refund_ids = []
        for wizard_id in self:
            refund_ids.append(wizard_id._create_returns())
        return self.env['stock.picking'].get_object_views(refund_ids, 'l10n_mn_purchase.purchase_refund_action', 'l10n_mn_purchase.purchase_refund_form_view')
        

class StockPicking(models.Model):
    _inherit = 'stock.picking'
    
    @api.model
    def purchase_bulk_refund(self, pickings):
        if not self.env.user.has_group('purchase.group_purchase_manager'):
            raise ValidationError(_("Refunds are only available for 'Purchase/Manager' group users !!!"))
        
        purchase_pickings = self.env['stock.picking'].get_purchase_picking_ids()
        partner_id = False
        picking_po_dict = {}
        
        # Бүгд хийгдсэн эсэхийг шалгах
        self._cr.execute("""
            SELECT COUNT(id) FROM stock_picking WHERE id IN (%s) AND state != 'done'
        """ % ",".join(str(picking) for picking in pickings))
        undone_pickings = self._cr.fetchall()
        if undone_pickings and undone_pickings[0][0] != 0:
            raise UserError(_("Refunds are only available for done pickings !!!"))
        
        for picking in pickings:
            purchase = self.get_po_from_picking(picking)
            if not purchase:
                raise UserError(_("Refunds are only available for pickings related to the purchase !!!"))
            else:
                current_partner_id = self.env['purchase.order'].sudo().browse(purchase).sudo().partner_id
                if partner_id and current_partner_id != partner_id:
                    raise UserError(_("Pickings must be related to the one partner !!!"))
                partner_id = current_partner_id
            picking_po_dict[picking] = purchase[0]
        
        product_return_moves = []
        for picking in pickings:
            picking_id = self.browse(picking)
            purchase = picking_po_dict.get(picking)
            for move in picking_id.move_lines:
                if move.scrapped:
                    continue
                product_return_moves.append((0, 0, {'product_id': move.product_id.id, 'quantity': 0, 'move_id': move.id, 'picking_id': picking, 'purchase_id': purchase}))
        
        wizard_id = self.env['purchase.picking.refund'].create({
            'partner_id': partner_id.sudo().id,
            'picking_ids': [(6, 0, pickings)],
            'product_return_moves': product_return_moves
        })
            
        return {
            'type': 'ir.actions.act_window',
            'name': _('Refund Purchase Picking'),
            'res_model': 'purchase.picking.refund',
            'view_mode': 'form',
            'target': 'new',
            'res_id': wizard_id.id,
        }
        
