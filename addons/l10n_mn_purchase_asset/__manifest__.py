# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Asset Purchase",
    'version': '1.0',
    'depends': ['l10n_mn_account_asset', 'l10n_mn_purchase', 'hr'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Asset Purchase Additional Features
    """,
    'data': [
        'wizard/receive_assets_wizard_view.xml',
        'views/account_asset_view.xml',
        'views/purchase_view.xml',
        'views/purchase_asset_view.xml',
        'security/ir.model.access.csv',
        'security/ir_rule.xml',
        'report/print_report_asset_receiving.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}