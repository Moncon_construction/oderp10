# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
from odoo.exceptions import UserError


class AccountAssetAsset(models.Model):
    _inherit = 'account.asset.asset'

    picking_id = fields.Many2one('purchase.asset.picking', 'Asset Picking')
    purchase_id = fields.Many2one('purchase.order', string='Purchase', related='picking_id.purchase_id', store=True)
    po_line_id = fields.Many2one('purchase.order.line', 'Purchase Order Line')
    purchase_order_count = fields.Integer(compute='_purchase_order_count')

    @api.multi
    def unlink(self):
        for asset in self:
            if asset.purchase_id and asset.po_line_id:
                raise UserError(_(
                    "You cannot unlink the proceeds from the purchase. First %s purchase order unlink" % asset.purchase_id.name))
        return super(AccountAssetAsset, self).unlink()

    @api.multi
    def _purchase_order_count(self):
        for asset in self:
            if asset.purchase_id or asset.picking_id.purchase_id:
                asset.purchase_order_count = len(asset.purchase_id) if asset.purchase_id else len(asset.picking_id.purchase_id)

    @api.multi
    def to_purchase_order(self):
        context = dict(self.env.context or {})
        context.update(create=False)
        ids = self.purchase_id.ids if self.purchase_id else self.picking_id.purchase_id.ids
        return {
            'name': _('Purchase Order'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'purchase.order',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'context': context,
            'domain': [('id', 'in', ids)]
        }
