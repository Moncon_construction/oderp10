# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.tools.float_utils import float_compare

# TODO : Нийлүүлэгчийн нэхэмжлэхээс Хөрөнгө худалдан авах функц болиулсныг хасав. Шалгах шаардлагатай

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'
    """
                Үндсэн хөрөнгийн худалдан авалтаас үндсэн хөрөнгийн 
            нэхэмжлэл үүсгэх үед тухайн барааны тоо хэмжээг авах функц
    """
    def _prepare_invoice_line_from_po_line(self, line):
        res = super(AccountInvoice, self)._prepare_invoice_line_from_po_line(line)
        qty = res['quantity']
        asset_categ_id = False
        if line.is_asset_purchase:
            qty = line.product_qty
            asset_categ_id = line.asset_categ_id and line.asset_categ_id.id
        res.update({'asset_category_id': asset_categ_id})
        res['quantity'] = qty
        return res


class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'

    @api.one
    def asset_create(self):
        """OVERRIDE--> Хөрөнгө хүлээн авах үед нэхэмжлэл дээр үүссэн хөрөнгө дахин үүсэж байгаа
            тул override хийн нэхэмжлэлээс хөрөнгө үүсэхгүй болгов
        """
        return True
