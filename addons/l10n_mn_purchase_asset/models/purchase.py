# -*- coding: utf-8 -*-
from odoo import tools
from odoo import models, api, fields, _
from odoo.tools.translate import _
from odoo.exceptions import UserError
import time
import odoo.addons.decimal_precision as dp  # @UnresolvedImport
from __builtin__ import True


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    @api.multi
    def button_confirm(self):
        is_button_confirm = False
        order_line = 0
        order_line2 = 0
        for order in self:
            order_line = self.env['purchase.order.line'].search([('order_id', '=', order.id), ('is_asset_purchase', '=', True)])
            order_line2 = self.env['purchase.order.line'].search([('order_id', '=', order.id), ('is_asset_purchase', '=', False)])
        if order_line:
            if order_line2:
                res = super(PurchaseOrder, self).button_confirm()
                self.action_asset_picking_create()
            else:
                for a in self:
                    a.write({'state': 'purchase'})
                    res = self.action_asset_picking_create()
            return res
        elif order_line2:
                res = super(PurchaseOrder, self).button_confirm()
                return res

    @api.multi
    def _count_all(self):
        for purchase in self:
            asset_picking_ids = self.env['purchase.asset.picking'].sudo().search([('purchase_id', '=', purchase.id)]).ids
            if asset_picking_ids:
                purchase.asset_picking_count = len(asset_picking_ids)
            else:
                purchase.asset_picking_count = 0


    @api.multi
    def check_purchase_asset_picking_state(self):
        # Хөрөнгө хүлээн авсан эсэхийг шалгана.
        is_state_done = True
        for order in self:
            for line in order.order_line:
                asset_picking_line_obj = self.env['purchase.asset.picking.line'].sudo().search([('po_line_id', '=', line.id)])
                if asset_picking_line_obj and asset_picking_line_obj.asset_picking_id.state != 'done':
                    is_state_done = False
                    break
        return is_state_done

    def _has_asset_product(self):
        is_asset = False
        is_state_done = self.check_purchase_asset_picking_state()
        for purchase in self:
            for line in purchase.order_line:
                if line.is_asset_purchase:
                    is_asset = True
                    categ = line.product_id.product_tmpl_id.asset_category_id
                    line.product_id.product_tmpl_id.asset_category_id = categ
                    break
            if is_state_done is True:
                is_asset = False
            purchase.has_asset = is_asset


    has_asset = fields.Boolean(compute=_has_asset_product, string='Asset Picking')
    asset_picking_count = fields.Integer(compute=_count_all, string='Count Asset Pickings', default=0)

    @api.multi
    def has_asset_product(self):
        for order in self:
            for order_line in order.order_line:
                if order_line.state == 'cancel':
                    continue
                if order_line.product_id and order_line.asset_categ_id:
                    return True
        return False

    @api.multi
    def has_stockable_product(self):
        for order in self:
            for order_line in order.order_line:
                if order_line.state == 'cancel':
                    continue
                if order_line.product_id and not order_line.asset_categ_id and order_line.product_id.type in ('product', 'consu'):
                    return True
        return False

    def asset_picking_done(self):
        return True

    def _create_asset_moves(self, order_lines, asset_picking_id):
        for line in order_lines:
            if line.asset_categ_id:
                val = {
                    'code': '',
                    'name': line.name,
                    'asset_categ_id': line.asset_categ_id.id,
                    'po_line_id': line.id,
                    'product_id': line.product_id.id,
                    'qty': line.product_qty,
                    'asset_picking_id': asset_picking_id
                }
                new_line_id = self.env['purchase.asset.picking.line'].sudo().create(val)
        return True

    def action_asset_picking_create(self):
        asset_picking_ids = self.env['purchase.asset.picking'].sudo().search([('purchase_id', '=', self.id)])
        asset_picking_ids.with_context(ignore=True).unlink()
        purchase_order_line = self.env['purchase.order.line']
        for order in self:
            asset_picking_vals = {
                'purchase_id': order.id,
                'supplier_id': order.partner_id.id,
                'create_date': time.strftime('%Y-%m-%d'),
                'user_id': order._uid,
                'state': 'ready',
                'company_id': order.company_id.id
            }
            asset_picking = self.env['purchase.asset.picking'].sudo().create(asset_picking_vals)
            self._create_asset_moves(purchase_order_line.search([('order_id', '=', order.id), ('is_asset_purchase', '=', True)]), asset_picking.id)
        return asset_picking.id

    @api.multi
    def action_view_asset_picking(self):
        action = self.env.ref('l10n_mn_purchase_asset.action_purchase_asset_picking')
        result = action.read()[0]
        asset_picking_ids = []
        for order in self:
            asset_picking_ids = self.env['purchase.asset.picking'].sudo().search([('purchase_id', '=', order.id)]).ids
        if len(asset_picking_ids) > 1:
            result['domain'] = "[('id','in',[" + ','.join(map(str, asset_picking_ids)) + "])]"
        elif len(asset_picking_ids) == 1:
            res = self.env.ref('l10n_mn_purchase_asset.purchase_asset_picking_form', False)
            result['views'] = [(res and res.id or False, 'form')]
            result['res_id'] = asset_picking_ids and asset_picking_ids[0] or False
        return result

    @api.multi
    def button_cancel(self):
        for purchase in self:
            # ХАЗ цуцлахад үүссэн хөрөнгө устан. Хөрөнгө хүлээн авах Ноорог төлөвтэй болдог болгосон
            asset_picking_ids = self.env['purchase.asset.picking'].search([('purchase_id', '=', purchase.id)])
            for picking in asset_picking_ids:
                for asset in picking.asset_ids:
                    asset.write({'po_line_id': False, 'picking_id': False})
                    asset.set_to_draft()
                    asset.unlink()
                picking.state = 'draft'
            purchase.is_product_received = False
        return super(PurchaseOrder, self).button_cancel()

    @api.multi
    def unlink(self):
        if not self._context.get('ignore'):
            for purchase in self:
                # ХАЗ устгахад үүссэн хөрөнгө устан. Хөрөнгө хүлээн авах мөн устдаг болгосон
                asset_picking_ids = self.env['purchase.asset.picking'].search([('purchase_id', '=', purchase.id)])
                for picking in asset_picking_ids:
                    for asset in picking.asset_ids:
                        asset.write({'po_line_id': False, 'picking_id': False})
                        asset.set_to_draft()
                        asset.unlink()
                    picking.unlink()
                purchase.has_asset = False
        return super(PurchaseOrder, self).unlink()


PurchaseOrder()


class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    @api.multi
    def _amount_line_tax_inc(self):
        res = {}
        cur_obj = self.env['res.currency']
        tax_obj = self.env['account.tax']
        for line in self:
            line_price = line.price_unit
            line_qty = line.product_qty
            line_cur = line.order_id.currency_rate
            line_tax = line.taxes_id
            line_prod = line.product_id
            line_partner = line.order_id.partner_id
            taxes = tax_obj.compute_all(line_price, line.currency_id, line_qty, line_prod, line_partner)
            res[line.id] = cur_obj.round(taxes['total_included'])
        return res

    @api.multi
    def _create_stock_moves(self, picking):
        moves = self.env['stock.move']
        done = self.env['stock.move'].browse()
        for line in self:
            if line.is_asset_purchase != True:
                for val in line._prepare_stock_moves(picking):
                    done += moves.create(val)
        return done

    @api.multi
    @api.onchange('is_asset_purchase')
    def onchange_has_asset(self):
        if self.is_asset_purchase:
            self.asset_categ_id = self.product_id.product_tmpl_id.asset_category_id

    asset_categ_id = fields.Many2one('account.asset.category', 'Asset Category')
    is_asset_purchase = fields.Boolean('Is Asset Purchase')
    price_subtotal_tax_included = fields.Float(compute=_amount_line_tax_inc, string='Subtotal')


PurchaseOrderLine()
