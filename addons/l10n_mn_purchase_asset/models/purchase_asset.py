# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.tools.translate import _
from odoo.exceptions import UserError
import time
from datetime import datetime


class PurchaseAssetPickingLine(models.Model):
    _name = 'purchase.asset.picking.line'

    code = fields.Char('Code', size=64)
    name = fields.Char('Name', size=128, required=True)
    asset_categ_id = fields.Many2one('account.asset.category', 'Asset Category', required=True)
    po_line_id = fields.Many2one('purchase.order.line', 'Purchase Order Line', required=True)
    product_id = fields.Many2one('product.product', 'Product', required=True)
    qty = fields.Integer('Quantity', required=True)
    asset_id = fields.Many2one('account.asset.asset', 'Asset')
    owner_id = fields.Many2one('hr.employee', 'Owner')
    location_id = fields.Many2one('account.asset.location', 'Asset Location')


PurchaseAssetPickingLine()


class PurchaseAssetPicking(models.Model):
    _name = 'purchase.asset.picking'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    @api.multi
    def do_enter_transfer_details(self):
        created_id = self.env['receive.assets.wizard'].create({'picking_id': self.id or False})
        wiz = self.env['receive.assets.wizard'].wizard_view(created_id)
        return self.env['receive.assets.wizard'].wizard_view(created_id)

    name = fields.Char('Name', default=lambda self: self.env['ir.sequence'].get('purchase.asset.picking') or '/', required=True)
    purchase_id = fields.Many2one('purchase.order', 'Purchase Order', required=True)
    supplier_id = fields.Many2one('res.partner', 'Supplier', required=True)
    create_date = fields.Datetime('Create Date', default=lambda *a:time.strftime('%Y-%m-%d'), required=True)
    receive_date = fields.Datetime('Receive Date')
    user_id = fields.Many2one('res.users', 'User', default=lambda self: self.env.user.id, required=True)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('ready', 'Ready'),
            ('done', 'Done'),
            ], track_visibility='onchange', default='draft', string='State')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env['res.users'].company_id.id)
    description = fields.Text('Description')

    @api.multi
    def action_receive_assets(self):
        return True

    @api.multi
    def action_ready(self):
        self.write({'state': 'ready'})
        return True

    @api.multi
    def action_cancel(self):
        self.write({'state': 'draft'})
        return True


PurchaseAssetPicking()


class InheritedPurchaseAssetPickingLine(models.Model):
    _inherit = 'purchase.asset.picking.line'

    def _compute_received_qty(self):
        vals = {}
        for line in self:
            qty = 0
            if line.asset_picking_id.asset_ids:
                for asset in line.asset_picking_id.asset_ids:
                    if line.po_line_id.id == asset.po_line_id.id:
                        qty += 1
            vals = {
                line.id: {
                    'received_qty': qty,
                }
            }
        return vals

    asset_picking_id = fields.Many2one('purchase.asset.picking', required=True, ondelete='cascade')
    recieved_qty = fields.Integer(compute=_compute_received_qty, string='Received Quantity', multi=True)


InheritedPurchaseAssetPickingLine()


class InheritedPurchaseAssetPicking(models.Model):
    _inherit = 'purchase.asset.picking'

    @api.multi
    def _count_all(self):
        assets = self.env['account.asset.asset']
        assets = assets.search([('picking_id', '=', self.id), ('is_supply_asset', '=', False)])
        self.asset_count = len(assets)
        
    @api.multi
    def _count_supply_asset(self):
        self.supply_asset_count = len(self.env['account.asset.asset'].search([('picking_id', '=', self.id), ('is_supply_asset', '=', True)]))

    asset_count = fields.Integer(compute=_count_all, string='Counted')
    supply_asset_count = fields.Integer(compute=_count_supply_asset, string='Supply Assets Count')
    line_ids = fields.One2many('purchase.asset.picking.line', 'asset_picking_id', 'Picking Lines')
    asset_ids = fields.One2many('account.asset.asset', 'picking_id', 'Assets')
    
    @api.multi
    def asset_open(self):
        #         Замд яваа хөрөнгөөс үүссэн хөрөнгөрүү үсрэх
        asset_ids = []
        assets = self.env['account.asset.asset']
        assets = assets.search([('picking_id', '=', self.id), ('is_supply_asset', '=', False)])
        for pick in assets:
            if pick:
                asset_ids.append(pick.id)
        return {
            'name': _('Account Assets'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.asset.asset',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', asset_ids)],
        }

    @api.multi
    def open_supply_asset(self):
        # Замд яваа хөрөнгөөс үүссэн хангамж руу үсрэх
        action = self.env.ref('l10n_mn_account_asset.action_supply_asset').read()[0]
        action['domain'] = [('picking_id', '=', self.id), ('is_supply_asset', '=', True)]
        return action
        

InheritedPurchaseAssetPicking()
