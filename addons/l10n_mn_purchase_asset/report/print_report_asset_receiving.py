#-*- coding: utf-8 -*-
from odoo import api, models, fields
from datetime import datetime

class ReportReceivingAsset(models.AbstractModel):
    _name = 'report.l10n_mn_purchase_asset.asset_receiving_report'
    
    @api.model
    def render_html(self, docids, data=None):
        
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('l10n_mn_purchase_asset.asset_receiving_report')
        purchase = self.env['purchase.asset.picking']
        assets= purchase.browse(docids)
        
        docargs={
            'doc_ids': docids,
            'doc_model': report.model,
            'docs': assets
        }
        return report_obj.render('l10n_mn_purchase_asset.asset_receiving_report', docargs)
