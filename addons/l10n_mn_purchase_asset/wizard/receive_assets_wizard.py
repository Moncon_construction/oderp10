# -*- coding: utf-8 -*-
##############################################################################

from odoo import api, fields, models
from odoo.exceptions import UserError
from odoo.tools.translate import _
from datetime import datetime


class ReceiveAssetsWizardLine(models.TransientModel):
    _name = 'receive.assets.wizard.line'

    code = fields.Char('Code', required=True)
    name = fields.Char('Name')
    asset_categ_id = fields.Many2one('account.asset.category', 'Asset Category', required=True)
    location_id = fields.Many2one('account.asset.location', 'Asset Location', required=True)
    owner_id = fields.Many2one('hr.employee', 'Owner')
    po_line_id = fields.Many2one('purchase.order.line', 'Purchase Order Line')
    is_code_auto = fields.Boolean()
    buy_date = fields.Date(default=fields.Date.context_today)
    is_supply_asset = fields.Boolean(default=False, string='Is Supply Asset or Not')


class ReceiveAssetsWizard(models.TransientModel):
    _name = 'receive.assets.wizard'
    _description = 'Receive Assets Wizard'

    name = fields.Char('Name')
    picking_id = fields.Many2one('purchase.asset.picking', String='Purchase Asset Picking')

    def get_asset_data(self, item):
        price_unit = (item.po_line_id.price_subtotal / item.po_line_id.product_qty) if item.po_line_id.product_qty else 0
        currency_rate = item.po_line_id.order_id.currency_rate if item.po_line_id.order_id and item.po_line_id.order_id.currency_rate else 0
        asset_datas = {
            'code': item.code,
            'user_id': item.owner_id.id,
            'name': item.name,
            'salvage_value': 0.0,
            'category_id': item.asset_categ_id.id,
            'location_id': item.location_id.id,
            'purchase_date': item.buy_date if item.buy_date else self.picking_id.receive_date,
            'value': price_unit * currency_rate,
            'company_id': self.picking_id.company_id.id,
            'partner_id': item.po_line_id.order_id.partner_id.id,
            'method': item.asset_categ_id.method,
            'method_number': item.asset_categ_id.method_number,
            'method_period': item.asset_categ_id.method_period,
            'po_line_id': item.po_line_id.id,
            'picking_id': self.picking_id.id,
            'date': item.buy_date,
            'is_supply_asset': item.is_supply_asset,
        }
        return asset_datas

    @api.multi
    def do_transfer(self):
        self.ensure_one()
        if self.picking_id.state not in ['ready']:
            raise Warning(_('Please confirm asset picking'))
        asset_codes = []
        for item in self.line_ids:
            asset_id = self.env['account.asset.asset'].search([('code', '=', item.code)])
            if asset_id:
                raise UserError(_('This asset code is overlapping: %s') % item.code)
            if item.code not in asset_codes:
                asset_codes.append(item.code)
            else:
                raise UserError(_('This asset code is overlapping: %s') % item.code)
        for item in self.line_ids:
            asset_datas = self.get_asset_data(item)
            asset_id = self.env['account.asset.asset'].create(asset_datas)
        done = True
        is_all_received = False
        for line in self.picking_id.line_ids:
            asset_ids = self.env['account.asset.asset'].search([('po_line_id', '=', line.po_line_id.id)])
            if line.qty != len(asset_ids):
                done = False
        if done:
            self.picking_id.write({'state':'done'})
            self.picking_id.signal_workflow('asset_picking_done')
            is_all_received = True
        if self.picking_id.purchase_id:
            self.picking_id.purchase_id.is_product_received = is_all_received
        return True


class InheritReceiveAssetsWizardLine(models.TransientModel):
    _inherit = 'receive.assets.wizard.line'

    wizard_id = fields.Many2one('receive.assets.wizard', 'Receive Assets Wizard')


class InheritedReceiveAssetsWizard(models.TransientModel):
    _inherit = 'receive.assets.wizard'
    _description = 'Receive Assets Wizard'

    line_ids = fields.One2many('receive.assets.wizard.line', 'wizard_id', 'Assets lines')

    @api.model
    def default_get(self, fields):
        context = dict(self._context or {})
        res = super(InheritedReceiveAssetsWizard, self).default_get(fields)

        picking_id = self._context.get('active_id')
        active_model = self._context.get('active_model')

        assert active_model in ('purchase.asset.picking'), 'Bad context propagation'

        picking = self.env['purchase.asset.picking'].search([('id', '=', picking_id)])
        res.update({'picking_id':picking_id})
        items = []
        self._cr.execute(
            "SELECT id FROM ir_module_module WHERE name = 'l10n_mn_account_asset_code_sequencer' AND state IN ('installed', 'to upgrade')")
        results = self._cr.dictfetchall()
        results = True if results and len(results) > 0 else False
        if context.get('active_model') == 'purchase.asset.picking' and context.get('active_ids'):
            purchases = self.env[context.get('active_model')].browse(context.get('active_ids'))
            for purchase in purchases:
                for line in purchase.line_ids:
                    for i in range(line.qty):
                        item = [0, False, {
                            'code': line.code,
                            'name': line.name,
                            'asset_categ_id': line.asset_categ_id.id,
                            'owner_id': line.owner_id.id,
                            'location_id': line.location_id.id,
                            'po_line_id': line.po_line_id.id,
                            'is_code_auto': results,
                            'buy_date': str(datetime.now().date())
                        }
                        ]
                        items.append(item)
                res.update({'line_ids':items})
                return res

    @api.multi
    def wizard_view(self, created_id):
        view = self.env.ref('l10n_mn_purchase_asset.view_receive_asset_wizard')
        return {
            'name': _('Enter transfer details'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'receive.assets.wizard',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'target': 'new',
            'res_id': created_id.id,
            'context': self.env.context,
        }
