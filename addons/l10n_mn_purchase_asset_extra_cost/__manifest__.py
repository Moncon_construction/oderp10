# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Asset Purchase Extra Costs",
    'version': '1.0',
    'depends': ['l10n_mn_purchase_extra_cost', 'l10n_mn_purchase_asset'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Asset Purchase Extra Cost Features
    """,
    'data': [
        'views/purchase_asset_view.xml'
        ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}