# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.tools.translate import _
from odoo.exceptions import UserError

class InheritedPurchaseAssetPicking(models.Model):
    _inherit = 'purchase.asset.picking'

    @api.multi
    def do_receive_asset(self):
        for pick in self:
            if pick.purchase_id and len(pick.purchase_id.extra_cost_ids) > 0 and not pick.purchase_id.cost_ok:
                raise UserError(_("You cannot process asset receiving until purchase order cost approved!"))
        return {
            'name': _('Receive Assets Wizard'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'receive.assets.wizard',
            'target': 'new',
            'context': {'active_model': 'purchase.asset.picking'}
        }
