# -*- coding: utf-8 -*-
##############################################################################

from odoo import api, fields, models
from odoo.exceptions import UserError
from odoo.tools.translate import _
from datetime import datetime


class ReceiveAssetsWizard(models.TransientModel):
    _inherit = 'receive.assets.wizard'

    def get_asset_data(self, item):
        asset_datas = super(ReceiveAssetsWizard, self).get_asset_data(item)
        asset_datas['value'] = item.po_line_id.cost_unit
        return asset_datas

