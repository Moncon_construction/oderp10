# -*- coding: utf-8 -*-
{
    'name': "Mongolian Purchase Cancel",
    'version': '1.0',
    'description': """
Purchase Cancel
===============================================================================================

Ирж буй хүргэлт, худалдан авалт цуцлах боломжтой болгоно
-----------------------------------------------------------------------------------------------
    """,
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Modules',
    'depends': ['purchase', 'stock', 'l10n_mn_stock'],
    'data': [
        'security/purchase_cancel_security.xml',
        'views/purchase_cancel_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False,
}
