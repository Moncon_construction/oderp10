# -*- coding: utf-8 -*-
from odoo import models, api
from odoo.tools.translate import _
from odoo.exceptions import UserError, Warning


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    @api.multi
    def action_force_cancel(self):
        """Худалдан авалтыг цуцлана."""
        for purchase in self:
            for pick in purchase.picking_ids:
                if pick.state == 'done':
                    raise Warning(_('Unable to cancel the purchase order %s. \n You have already received some goods for it.  ') % (purchase.name))
            for inv in purchase.invoice_ids:
                if inv and inv.state not in ('cancel', 'draft'):
                    raise Warning(_('Unable to cancel this purchase order. \n You must first cancel all invoices related to this purchase order.'))
            self.env['account.invoice'].signal_workflow('invoice_cancel')
        self.signal_workflow('purchase_cancel')
        self.button_cancel()
        return True


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    @api.multi
    def action_force_cancel(self):
        """Барааны тээвэрлэлтийг цуцлана."""
        context = self.env.context.copy()
        context.update({'purchase_cancel': True})
        for pick in self:
            if not pick.picking_type_id.warehouse_id.lock_move or (pick.picking_type_id.warehouse_id.lock_move and pick.date_done >= pick.picking_type_id.warehouse_id.lock_move_until):
                # delete quant, cancel move
                if pick.move_lines and len(pick.move_lines) > 0:
                    move_ids_str = str(pick.move_lines.ids)[1:len(str(pick.move_lines.ids)) - 1]
                    # Борлуулалт болон шаардахын агуулахын хөдөлгөөнийг хүчээр цуцлахад quant-н байрлалыг
                    # барааны хөдөлгөөний эх байрлалаар солив
                    # Борлуулалтын модуль суусан эсэхийг шалгав
                    if self.env.get('sale.order', False) != False:
                        self.env.cr.execute("select "
                                            "so.id as id, so.name as name, sp.id as picking_id "
                                            "from stock_move sm "
                                            "JOIN stock_picking sp on sm.picking_id = sp.id "
                                            "JOIN procurement_order po on po.id = sm.procurement_id "
                                            "JOIN sale_order_line sol on sol.id = po.sale_line_id "
                                            "JOIN sale_order so on so.id = sol.order_id "
                                            "where "
                                            "sm.origin_returned_move_id is NULL and sp.id = %s " % (pick.id))
                        lines = self.env.cr.dictfetchall()
                        line_ids = []
                        for line in lines:
                            line_ids.append(line['id'])
                        sale_order_obj = self.env['sale.order'].search([('id', 'in', line_ids)])
                        if sale_order_obj:
                            for move in pick.move_lines:
                                self.env.cr.execute(
                                    """SELECT quant_id FROM stock_quant_move_rel WHERE move_id = %s""" % move.id)
                                fetch = self.env.cr.dictfetchall()
                                for f in fetch:
                                    self.env.cr.execute("""UPDATE stock_quant set location_id=%s WHERE id=%s""",
                                                        (move.location_id.id, f['quant_id']))
                            # Борлуулалтын захиалга дээр is_delivered гэсэн талбар нэмсэн бөгөөд хүргэлтийг цуцлахад
                            # үүнийг False болгоно. Учир нь энэ талбар нь бүх хүргэлт хийгдсэн үед True байдаг болно.
                            if pick.picking_type_id.code == 'outgoing':
                                sale_order_obj.is_delivered = False
                    # Шаардахын модуль суусан эсэхийг шалгах
                    if self.env.get('product.expense', False) != False:
                        if pick.expense:
                            for move in pick.move_lines:
                                self.env.cr.execute(
                                    """SELECT quant_id FROM stock_quant_move_rel WHERE move_id = %s""" % move.id)
                                fetch = self.env.cr.dictfetchall()
                                for f in fetch:
                                    self.env.cr.execute("""UPDATE stock_quant set location_id=%s WHERE id=%s""",
                                                        (move.location_id.id, f['quant_id']))

                    # Нөхөн дүүргэлтийн захиалгын агуулахын хөдөлгөөнийг цуцлахад quant-н байрлал солих
                    if pick.transit_order_id:
                        if pick.picking_type_id.code == 'outgoing':
                            count_cancelled_incoming_pickings = 0
                            incoming_pickings = []
                            for picking in pick.transit_order_id.picking_ids:
                                if picking.id != pick.id and picking.picking_type_id.code == 'incoming':
                                    incoming_pickings.append(picking.id)
                                    if picking.state == 'cancel':
                                        count_cancelled_incoming_pickings += 1
                            if count_cancelled_incoming_pickings == len(incoming_pickings):
                                for move in pick.move_lines:
                                    self.env.cr.execute(
                                        """SELECT quant_id FROM stock_quant_move_rel WHERE move_id = %s""" % move.id)
                                    fetch = self.env.cr.dictfetchall()
                                    for f in fetch:
                                        self.env.cr.execute("""UPDATE stock_quant set location_id=%s WHERE id=%s""",
                                                            (move.location_id.id, f['quant_id']))
                            else:
                                raise UserError(_('Please cancel incoming pickings first'))
                        elif pick.picking_type_id.code == 'incoming':
                            for move in pick.move_lines:
                                self.env.cr.execute(
                                    """SELECT quant_id FROM stock_quant_move_rel WHERE move_id = %s""" % move.id)
                                fetch = self.env.cr.dictfetchall()
                                for f in fetch:
                                    self.env.cr.execute("""UPDATE stock_quant set location_id=%s WHERE id=%s""",
                                                        (move.location_id.id, f['quant_id']))
                    else:
                        # Худалдан авалт үед хуучин байснаар нь үлдээв
                        is_purchase = False
                        purchase_order = False
                        for line in pick.move_lines:
                            if line.purchase_line_id:
                                purchase_order = line.purchase_line_id.order_id
                                is_purchase = True
                                break
                        if is_purchase:
                            self._cr.execute("""
                                                DELETE FROM stock_quant WHERE id IN (SELECT quant_id FROM stock_quant_move_rel WHERE move_id IN (%s))
                                            """ % (move_ids_str))
                        # Худалдан авалтын захиалга дээр is_product_received гэсэн талбар нэмсэн бөгөөд тээвэрлэлтийг цуцлахад
                        # үүнийг False болгоно. Учир нь энэ талбар нь бүх тээвэрлэлт хийгдсэн үед True байдаг болно.
                        if purchase_order and pick.picking_type_id.code == 'incoming':
                            purchase_order.is_product_received = False
                    pick.move_lines.action_cancel_state()
                # delete account move
                if pick.account_move_id:
                    account_move = self.env['account.move'].search([('id', '=', pick.account_move_id.id)])
                    account_move.with_context(context).unlink()
    
                # delete pack operation
                operations = self.env['stock.pack.operation'].search([('picking_id', '=', pick.id)])
                if operations:
                    operations.with_context(context).unlink()
    
                pick.write({'state': 'cancel'})
            else:
                raise UserError(_('Sorry, this warehouse is locked until %s') % pick.picking_type_id.warehouse_id.lock_move_until)
        return True


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    @api.multi
    def _update_check(self):
        """Хүчээр цуцлах товч дарагдсан эсхийг шалгаж тийм бол журнал бичилтийн мөрүүдийг устгана."""
        move_ids = set()
        for line in self:
            err_msg = _('Move name (id): %s (%s)') % (line.move_id.name, str(line.move_id.id))
            if self._context.get('purchase_cancel') is not True and self._context.get('pos_order_product_change') is not True:
                if line.move_id.state != 'draft':
                    raise UserError(_('You cannot do this modification on a posted journal entry, you can just change some non legal fields. You must revert the journal entry to cancel it.\n%s.') % err_msg)
                if line.reconciled and not (line.debit == 0 and line.credit == 0):
                    raise UserError(_('You cannot do this modification on a reconciled entry. You can just change some non legal fields or you must unreconcile first.\n%s.') % err_msg)
            if line.move_id.id not in move_ids:
                move_ids.add(line.move_id.id)
            self.env['account.move'].browse(list(move_ids))._check_lock_date()
        return True


class PackOperation(models.Model):
    _inherit = 'stock.pack.operation'

    @api.multi
    def unlink(self):
        '''Худалдан авалтын захиалгын мөрүүдийг хийгдсэн төвөлтэй байсан ч устгана.'''
        if self._context.get('purchase_cancel') is not True:
            if any([operation.state in ('done', 'cancel') for operation in self]):
                raise UserError(_('You can not delete pack operations of a done picking'))
        return models.Model.unlink(self)


class StockMove(models.Model):
    _inherit = 'stock.move'

    @api.multi
    def action_cancel_state(self):
        """ Агуулахын хөдөлгөөнийг цуцлана. """

        procurements = self.env['procurement.order']
        for move in self:
            if move.reserved_quant_ids:
                move.quants_unreserve()
            if self.env.context.get('cancel_procurement'):
                if move.propagate:
                    pass
            else:
                move._propagate_cancel()
                if move.procurement_id:
                    procurements |= move.procurement_id
        self.write({'state': 'cancel', 'move_dest_id': False})
        if procurements:
            procurements.check()
        return True
