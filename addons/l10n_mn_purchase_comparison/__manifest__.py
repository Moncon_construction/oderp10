# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Purchase Comparison",
    'version': '1.0',
    'depends': ['l10n_mn_purchase', 'l10n_mn_workflow_config'],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Modules',
    'description': """
        Purchase Additional Features
    """,
    'data': [
            'security/ir.model.access.csv',
            'security/ir_rule.xml',
            'data/purchase_comparison_sequence.xml',
            'data/purchase_comparison_template_for_approvasion.xml',
            'data/purchase_comparison_template_for_voter.xml',
            'data/purchase_comparison_notification_template.xml',
            'views/comparison_indication_view.xml',
            'views/purchase_indication_view.xml',
            'views/purchase_comparison_view.xml',
            'views/purchase_comparison.xml',
            'views/comparison_reject_cancel_reason_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False,
    'qweb': ['static/src/xml/comparison.xml',],
}