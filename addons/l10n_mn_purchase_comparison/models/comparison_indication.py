from odoo import api, fields, models, _

class ComparisonIndication(models.Model):
    _name = 'comparison.indication'
    _description = "Comparison Indication"
    
    name = fields.Char('Indication Name', required=True)
    indication_id = fields.Many2one('purchase.indication', 'Indication Category')
    inner_type = fields.Selection([
                                    ('multiple_line', 'Multiple Line'),
                                    ('one_line', 'One Line'),
                                    ('int', 'Integer'),
                                    ('date', 'Date'),
                                    ('boolean_field', 'Boolean Field')], default='int', string='Inner Type')
    comparison_id = fields.Many2one('comparison.ref', 'Comparison')
    purchase_comparison_id = fields.Many2one('purchase.comparison', 'Comparison', ondelete='cascade')
    data_ids = fields.One2many('comparison.data', 'comparison_indication_id', 'Data')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)