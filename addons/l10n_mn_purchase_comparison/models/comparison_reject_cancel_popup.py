# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
import time
from odoo.exceptions import UserError

class RejectComparisonPopup(models.TransientModel):
    
    _name = "reject.comparison.popup"
    
    comparison = fields.Many2one('purchase.comparison', required=True, ondelete='cascade')
    type = fields.Selection([('reject', 'Reject'), ('cancel', 'Cancel')], 'Type', default='reject', required=True)
    reason = fields.Char(string='Reason')
    
    def reject(self):
        self.ensure_one()
        if not self.reason:
            raise UserError(_('Please write your reason.'))
        else:
            new_note = self.env['purchase.note'].sudo().create({
                'name': self.reason,
                'user': self.env.uid,
                'comparison_id': self.comparison.id
            })
            if self.type == 'reject': # Санал хураалт эхлүүлэхээс татгалзсан бол
                self.comparison.set_state_history('draft')
                self.comparison.with_context(operator=_('rejected')).send_mail_about_comparison(self.comparison.sudo().user_id, self.env.user, 'comparison_notification', _('Comparison rejection notification'))
                
                # Ажлын урсгалыг өмнөх шат руу шилжүүлэх
                current_workflow_history = self.comparison.current_workflow_history
                if self.comparison.check_sequence == 1:
                    if current_workflow_history:
                        current_workflow_history.write({
                            'action_date': time.strftime('%Y-%m-%d %H:%M:%S'),
                            'action': 'rejected', 
                            'user_id': self.env.uid
                        })
                    self.comparison.current_workflow_history = False
                    return
                
                next_step, next_user_ids = self.comparison.workflow_id.get_next_step(self.comparison, self.env.uid, self.comparison.check_sequence - 1, 'back')
                if not next_step:
                    raise UserError(_("There is no approve step for workflow: %s\nPlease check workflow !!!") %self.comparison.workflow_id.name)
                
                self.comparison.check_sequence = next_step[0].sudo().sequence
                self.comparison.current_workflow_history = self.env['workflow.history'].create({
                    'user_ids': [(6, 0, next_user_ids)],
                    'name': next_step.sudo().name,
                    'sent_date': time.strftime('%Y-%m-%d %H:%M:%S'),
                    'line_sequence': next_step.sudo().sequence,
                    'comparison_id': self.comparison.id
                })
                if current_workflow_history:
                    current_workflow_history.write({
                        'action_date': time.strftime('%Y-%m-%d %H:%M:%S'),
                        'action': 'rejected', 
                        'user_id': self.env.uid
                    })
            else: # Харьцуулалтыг цуцласан бол
                self.comparison.set_state_history('cancelled')
                self.comparison.with_context(operator=_('canceled')).send_mail_about_comparison(self.comparison.sudo().user_id, self.env.user, 'comparison_notification', _('Comparison canceled notification'))
                self.comparison.chosen_order.write({'state': 'draft'})
                self.comparison.chosen_order = False
            
                  