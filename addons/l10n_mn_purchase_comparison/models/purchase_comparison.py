# -*- coding: utf-8 -*-
import json
import time
from datetime import datetime

from odoo import _, api, fields, models
from odoo.exceptions import RedirectWarning, UserError, ValidationError
from odoo import exceptions


class ComparisonRefUser(models.Model):
    _name = 'comparison.user'
    _description = "Reference of comparison and users"

    user_id = fields.Many2one('res.users', 'User', required=True)
    comparison_ref_id = fields.Many2one('comparison.ref', 'Comparison User')
    purchase_comparison_id = fields.Many2one('purchase.comparison', 'Comparison User')


class ComparisonRef(models.Model):
    _name = 'comparison.ref'
    _description = "Comparison Reference"

    indication_id = fields.Many2one('purchase.indication', 'Indication Category')
    indication_ids = fields.One2many('comparison.indication', 'comparison_id', string='Comparison Indication')
    user_ids = fields.One2many('comparison.user', 'comparison_ref_id', 'Users', required=True)

    @api.onchange('indication_id')
    def onchange_indication_id(self):
        indication = self.env['comparison.indication'].search([('indication_id', '=', self.indication_id.id)])
        if indication:
            self.indication_ids= [(6, 0, indication.ids)]

    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        context = dict(self._context or {})
        res = super(ComparisonRef, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        orders = self.env['purchase.order'].browse(context.get('active_ids'))
        if context.get('active_model', '') == 'purchase.order' and len(orders) < 2:
            raise UserError(_('You must comparise at least 2 supplier'))
        if orders[0].company_id != orders[1].company_id:
            raise UserError(_('Company must be the same!'))
        if orders[0].partner_id == orders[1].partner_id:
            raise UserError(_('Comparising purchase order partner must not be the same'))
        if orders[0].purchase_type == 'directly' and len(orders) > 1:
            raise UserError(_('You can not comparise 2 directly pucrhase order'))
        else:
            for order in orders:
                if order.state not in ['draft', 'sent']:
                    raise UserError(_('You can create purchase comparison only draft state'))
        return res

    @api.multi
    def create_comparison(self):
        # Харьцуулалт үүсгэх
        context = dict(self._context or {})
        indication = self.env['comparison.indication'].search([('indication_id', '=', self.indication_id.id)])
        orders = self.env['purchase.order'].browse(context.get('active_ids'))
        partner_ids = []
        first_order = orders[0]

        # Харьцуулалт үүсгэх боломжтой эсэхийг шалгах
        for order in orders:
            if order.partner_id.id not in partner_ids:
                partner_ids.append(order.partner_id.id)
            if order.order_line:
                if order.id != first_order.id:
                    checks = []
                    for line in order.order_line:
                        check = 0
                        for fline in first_order.order_line:
                            if fline.product_id.id == line.product_id.id and fline.product_qty == line.product_qty:
                                check = 1
                        checks.append(check)
                    if 0 in checks:
                        raise exceptions.except_orm(_('Warning'), _('Product qty or products are different.'))
            else:
                raise UserError(_('It is not possible to create a comparison from order which has no order line.'))

        # Нэг нийлүүлэгчийг олон сонгосон байна уу шалгах
        if len(partner_ids) != len(orders):
            raise UserError(_('Comparising purchase order partner must not be the same'))

        # Худалдан авалтын харьцуулалт үүсгэж байна.
        comparison_id = self.env['purchase.comparison'].create({
                                                              'date': orders[0].date_order,
                                                              'user_id': self.env.user.id,
                                                              'state': 'draft',
                                                              })
        data_id = self.env['comparison.data']
        comparison_order_user = self.env['comparison.order.user']
        # Худалдан авалтын захиалга дээр харьцуулалтыг холбов.
        for order in orders:
            order.write({'comparison_id': comparison_id.id})
        if orders:
            comparison_id.write({'order_ids': [(6, 0, orders.ids)],
                                 'comparison_id': comparison_id.id})
        # Үзүүлэлт тус бүрээр нь харьцуулалтын өгөгдөл нэмэв.
        if indication:
            comparison_id.write({'indication_ids': [(6, 0, indication.ids)]})
            for b in indication:
                for a in orders:
                    data_id.create({'comparison_indication_id': b.id,
                                    'comparison_id': comparison_id.id,
                                    'order_id': a.id})
        # Хэрэглэгч болгоноор харьцуулалтын өгөгдөл нэмэв.
        for a in orders:
            for x in self.user_ids:
                if x:
                    comparison_order_user.create({'user_id': x.user_id.id,
                                                  'comparison_id': comparison_id.id,
                                                  'order_id': a.id,
                                                  'states': 'not selected'})
                else:
                    comparison_order_user.create({'comparison_id': comparison_id.id,
                                                  'order_id': a.id,
                                                  'states': 'not selected'})
        return {
            'type': 'ir.actions.act_window',
            'name': _('Purchase Comparison'),
            'res_model': 'purchase.comparison',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'view_id': False,
            'domain': [('id', '=', comparison_id.id)],
        }


class PurchaseNote(models.Model):
    _name = "purchase.note"

    name = fields.Char(string='Note', size=256, required=True)
    date = fields.Datetime(string='Writen Date', required=True, readonly=True, default=datetime.now())
    user = fields.Many2one('res.users', string="Writen User", required=True, readonly=True, default=lambda self: self.env.uid)
    comparison_id = fields.Many2one('purchase.comparison', required=True, ondelete='cascade')


class ComparisonStateHistory(models.Model):
    _name = 'comparison.state.history'
    _description = "Purchase Comparison State History"

    state = fields.Selection([('draft', 'New'),
                              ('req_sent_for_open_vote', 'REQ Sent for Open Vote'),
                              ('collecting_vote', 'Collecting Vote'),
                              ('vote_collected', 'Vote Collected'),
                              ('done', 'Done'),
                              ('cancelled', 'Cancelled'),
                            ], 'State', default='draft', copy=False, required=True)

    user = fields.Many2one('res.users', 'User', default=lambda self: self.env.user)
    date = fields.Datetime('Date', default=lambda self: datetime.now())
    comparison_id = fields.Many2one('purchase.comparison', required=True, ondelete='cascade')


class WorkflowHistory(models.Model):
    _inherit = 'workflow.history'

    comparison_id = fields.Many2one('purchase.comparison', 'Purchase Comparison')


class PurchaseComparison(models.Model):
    _name = 'purchase.comparison'
    _description = "Purchase Comparison"
    _inherit = ['mail.thread']

    def _domain_workflow_id(self):
        return [
            ('model_id', '=', self.env['ir.model'].search([('model', '=', self._name)])[0].id), ('line_ids', '!=', False),
            '|', ('company_id', '=', False), ('company_id', 'child_of', [self.company_id.id])
        ]

    state = fields.Selection([('draft', 'New'),
                              ('req_sent_for_open_vote', 'REQ Sent for Open Vote'),
                              ('collecting_vote', 'Collecting Vote'),
                              ('vote_collected', 'Vote Collected'),
                              ('done', 'Done'),
                              ('cancelled', 'Cancelled'),
                            ], 'State', default='draft', copy=False, required=True)

    # Худалдан авалтад санал өгөх хэрэглэгчид нь динамикаар үүсдэг тул худалдан авалт дахь ажлын урсгал нь бусад объектуудын ажлын урсгалаас ялгаатай.
    # Динамикаар үүсч буй санал өгөгч бүрт урсгал тохируулах боломжгүй тул харьцуулалт САНАЛ ХУРААЖ БУЙ төлөвт байх үеийг ажлын урсгалд тохируулалгүй АЛГАСАХ шаардлагатай.
    # Тиймээс l10n_mn_workflow дахь үндсэн send() функцыг ашиглаагүй болно.
    workflow_id = fields.Many2one('workflow.config', 'Workflow', domain=_domain_workflow_id, track_visibility='on_change')
    check_sequence = fields.Integer(string='Workflow Step', default=1)
    workflow_history_ids = fields.One2many('workflow.history', 'comparison_id', string='Workflow History', readonly=True)

    user_id = fields.Many2one('res.users', 'Responsible User', default=lambda self: self.env.user, track_visibility='on_change')
    name = fields.Char('Name', copy=False, readonly=True, default=lambda self: self.env['ir.sequence'].get('purchase.comparison'))
    date = fields.Date('Date')
    company_id = fields.Many2one('res.company', default=lambda self: self.env.user.company_id, string='Company')
    conclusion = fields.Text('Conclusion', track_visibility='on_change')
    saving_type = fields.Selection([
                                    ('budget', 'Saving to budget'),
                                    ('last_purchase', 'Saving to last purchase'),
                                    ('deal', 'Saving to deal'),
                                    ('other', 'Other')], default='budget', string='Saving Type')
    order_ids = fields.Many2many('purchase.order', string='Purchase Orders')
    chosen_order = fields.Many2one('purchase.order', string='Chosen Purchase Order')
    saving_amount = fields.Float('Saving Amount', track_visibility='on_change')
    currency = fields.Many2one('res.currency', 'Currency')
    total_amount = fields.Float('Total Amount')
    tax = fields.Float('Tax')
    tax_amount = fields.Float('Tax Amount')
    state_history = fields.One2many('comparison.state.history', 'comparison_id', readonly=True, string='State History')
    active = fields.Boolean('Active', default=True)
    indication_ids = fields.Many2many('comparison.indication', string='Comparison Indication')
    comparison_id = fields.Integer('Comparison')
    user_ids = fields.One2many('comparison.user', 'purchase_comparison_id', 'Users')
    purchase_notes = fields.One2many('purchase.note', 'comparison_id', string="Purchase Notes")
    current_user = fields.Many2one('res.users', compute="_set_current_user")
    can_click_button = fields.Boolean(compute="_check_user_can_click_button")
    is_creator = fields.Boolean(compute="_check_user_is_creator")
    current_workflow_history = fields.Many2one('workflow.history')

    @api.model
    def create(self, vals):
        new_comparisons = super(PurchaseComparison, self).create(vals)
        for obj in new_comparisons:
            self.env['comparison.state.history'].sudo().create({
                'comparison_id': obj.id,
                'user': self.env.uid,
                'state': obj.state
            })
        return new_comparisons

    @api.multi
    def _check_user_is_creator(self):
        for obj in self:
            obj.is_creator = True if obj.sudo().create_uid == self.env.user else False

    @api.multi
    def _check_user_can_click_button(self):
        """
            Батлуулахаар илгээх: Зөвхөн үүсгэсэн хэрэглэгч дарна
            Батлах: Зөвхөн харгалзах ажлын урсгалын 2 дахь түвшинд тохируулсан хэрэглэгч дарна
            Бусад: Хариуцсан хэрэглэгч эсвэл худалдан авалтын менежер түвшин бүхий хэрэглэгч дарна
        """
        for obj in self:
            if obj.state == 'req_sent_for_open_vote' and not obj.workflow_id:
                raise UserError(_("Please choose workflow config for this comparison !!!"))

            if (obj.state == 'draft' or obj.state == 'cancelled') and obj.sudo().create_uid == self.env.user:
                obj.can_click_button = True
            elif obj.state == 'req_sent_for_open_vote':
                next_step, next_user_ids = obj.workflow_id.get_next_step(obj, self.env.uid, obj.check_sequence, 'next')
                if not next_step:
                    raise UserError(_("There is no approve step for workflow: %s\nPlease check workflow !!!") % obj.workflow_id.name)

                obj.check_sequence = next_step[0].sudo().sequence

                if len(next_user_ids) > 0 and self.env.uid in next_user_ids:
                    obj.can_click_button = True
            elif obj.state == 'vote_collected':
                next_step, next_user_ids = obj.workflow_id.get_next_step(obj, self.env.uid, obj.check_sequence, 'next')
                if not next_step:
                    raise UserError(_("There is no approve step for workflow: %s\nPlease check workflow !!!") % obj.workflow_id.name)

                obj.check_sequence = next_step[0].sudo().sequence

                if len(next_user_ids) > 0 and self.env.uid in next_user_ids:
                    obj.can_click_button = True
            elif obj.state not in ['draft', 'req_sent_for_open_vote', 'vote_collected', 'cancelled'] and (obj.user_id == self.env.user or self.env.user.has_group('purchase.group_purchase_manager')):
                obj.can_click_button = True
            else:
                obj.can_click_button = False

    def _set_current_user(self):
        for obj in self:
            obj.current_user = self.env.user

    @api.multi
    def get_groups(self):
        group_manager = self.env['res.users'].has_group('purchase.group_purchase_manager')
        group_user = self.env['res.users'].has_group('purchase.group_purchase_user')
        if group_manager == group_user:
            return 'group_purchase_manager'
        else:
            return 'group_purchase_user'

    @api.model
    def get_name(self, user_name):
        if self.env.user.name == user_name:
            return True
        else:
            return False

    @api.model
    def update_value(self, obj1):
        indication_ids = []
        order_ids = []
        # Худалдан авалтын харьцуулалт цонхноос ирсэн үзүүлэлтийн өгөгдлийг шинэчилж байна.
        if obj1:
            for a in obj1:
                indication_ids.append(a['comparison_indication_id'])
                order_ids.append(a['order_id'])
            orders = self.env['purchase.order'].search([('id', 'in', order_ids)])
            for a in orders:
                for b in indication_ids:
                    for c in obj1:
                        comparison_ids = self.env['comparison.data'].search([('order_id', '=', a.id), ('comparison_indication_id', '=', b), ('comparison_id', '=', c['comparison_id'])])
                        if c['order_id'] == a.id and c['comparison_indication_id'] == b:
                            if 'boolean_data' in c.keys() and c['boolean_data'] is not None:
                                comparison_ids.write({'boolean_data': c['boolean_data']})
                            if 'oneline_data' in c.keys() and c['oneline_data'] is not None:
                                comparison_ids.write({'oneline_data': c['oneline_data']})
                            if 'date_data' in c.keys() and c['date_data'] is not None:
                                comparison_ids.write({'date_data': c['date_data']})
                            if 'int_data' in c.keys() and c['int_data'] is not None:
                                comparison_ids.write({'int_data': c['int_data']})
                            if 'multiline_data' in c.keys() and c['multiline_data'] is not None:
                                comparison_ids.write({'multiline_data': c['multiline_data']})
        return True

    @api.model
    def update_comparison_user(self, data):
        # Худалдан авалтын харьцуулалт цонхноос ирсэн хэрэглэгчийн өгөгдлийг шинэчилж байна.
        comparison = []
        for c in data:
            comparison.append(c['id'])
        comparison_user = self.env['comparison.order.user'].search([('id', 'in', comparison)])
        for a in comparison_user:
            for b in data:
                if a.id == b['id'] and a.order_id.id == b['order_id']:
                    if b['boolean_data'] is not None:
                        a.boolean_data = b['boolean_data']
                        a.write({'boolean_data': b['boolean_data']})
                    if b['states'] is not None:
                        a.states = b['states']
                        a.write({'states': b['states']})

        return True

    @api.model
    def check_all_voten(self, id, chosen_po, last_voten_uid):
        """
            1. Бүх ажилтан санал өгсөн эсэхийг шалгаж, сонгогдсон худалдан авалтын төлвийг өөрчилнө.
            2. Харьцуулалтын төлвийг өөрчилнө.
            3. Ажлын урсгалыг дараагийн төлөвт шилжүүлнэ. Уг шилжих төлөвт харьцуулалтыг дуусгах эрхтэй хүнийг тохируулсан байна.

            * id: Харьцуулалтын id-г дамжуулна.
            * chosen_po: Ялсан саналын дугаарыг дамжуулна.
        """
        comparison = self.env['purchase.comparison'].browse(id)
        if comparison.state == 'vote_collected':
            return {
                'type': 'ir.actions.client',
                'tag': 'reload'
            }

        self._cr.execute("""
            SELECT DISTINCT(user_id)
            FROM comparison_order_user
            WHERE comparison_id = %s AND boolean_data = True
        """ % id)
        chosen_results = [x[0] for x in self._cr.fetchall()]

        self._cr.execute("""
            SELECT DISTINCT(user_id)
            FROM comparison_order_user
            WHERE comparison_id = %s AND boolean_data = False
        """ % id)
        not_chosen_results = [x[0] for x in self._cr.fetchall()]

        difference = set(not_chosen_results) - set(chosen_results)
        if len(difference) == 0:
            comparison.set_state_history('vote_collected')
            comparison.write({'chosen_order': self.env['purchase.order'].search([('name', '=', chosen_po)])[0].id})
            next_step, next_user_ids = comparison.workflow_id.get_next_step(comparison, self.env.uid, comparison.check_sequence + 1, 'next')
            if not next_step:
                raise UserError(_("There is no approve step for workflow: %s\nPlease check workflow !!!") %comparison.workflow_id.name)

            comparison.check_sequence = next_step[0].sudo().sequence
            comparison.current_workflow_history = self.env['workflow.history'].create({
                'user_ids': [(6, 0, next_user_ids)],
                'name': next_step.sudo().name,
                'sent_date': time.strftime('%Y-%m-%d %H:%M:%S'),
                'line_sequence': next_step.sudo().sequence,
                'comparison_id': comparison.id
            })

            for user_id in next_user_ids:
                comparison.with_context(operator=_('vote_collected')).send_mail_about_comparison(self.env['res.users'].browse(user_id), self.env.user, 'comparison_notification', _('Notification about vote collection done'))

            return {
                'type': 'ir.actions.client',
                'tag': 'reload'
            }

        return False

    def complete_comparison(self):
        self.ensure_one()
        # Сонгогдсон худалдан авах захиалгын хүсэлтийн төлвийг өөрчилж бусад харьцуулагдсан захиалгуудын төлвийг цуцлагдсан төлөвт оруулах
        if self.chosen_order:
            self.with_context(operator=_('done')).send_mail_about_comparison(self.user_id, self.env.user, 'comparison_notification', _('Comparison done notification'))
            self.chosen_order.button_confirm()
            self.set_state_history('done')
            if self.current_workflow_history:
                self.current_workflow_history.write({
                    'action_date': time.strftime('%Y-%m-%d %H:%M:%S'),
                    'action': 'approved',
                    'user_id': self.env.uid
                })
            for order in self.env['purchase.order'].search([('comparison_id','=',self.id)]):
                if order.id != self.chosen_order.id:
                    order.write({'state': 'cancel','is_lost': True})
        else:
            raise UserError(_("Comparison cannot be complete !!!\nBecause all voters are not submitted !!!"))

    def set_state_history(self, state):
        self.ensure_one()
        self.env['comparison.state.history'].create({
            'comparison_id': self.id,
            'state': state,
            'user': self.env.uid
        })
        self.write({'state': state})

    def send_mail_about_comparison(self, mail_to, mail_from, template_name, body_remainder_msg):
        self.ensure_one()
        # Өөрөө өөрлүүгээ мэйл явуулах хэрэггүй
        if mail_to == mail_from:
            return True

        if not mail_to.sudo().partner_id.email:
            raise UserError(_("There is no mail configured on this partner: '%s'!!!") %mail_to.sudo().partner_id.name)

        if not mail_from.sudo().partner_id.email:
            raise UserError(_("There is no mail configured on this partner: '%s'!!!") %mail_from.sudo().partner_id.name)

        outgoing_email = self.env['ir.mail_server'].sudo().search([])
        if not outgoing_email:
            raise UserError(_("There is no configuration for outgoing mail server. Please contact system administrator."))

        data = {
            'comparison': self.name,
            'id': self.id,
            'base_url': self.env['ir.config_parameter'].get_param( 'web.base.url'),
            'action_id': self.env['ir.model.data'].get_object_reference('l10n_mn_purchase_comparison', "action_purchase_comparison")[1],
            'db_name': self.env.cr.dbname,
            'model': 'purchase.comparison'
        }
        if template_name == 'comparison_notification':
            data['operator'] = self._context['operator']
            data['user'] = self.env.user.sudo().partner_id.name

        template_id = self.env['ir.model.data'].get_object_reference('l10n_mn_purchase_comparison', template_name)[1]
        template = self.env['mail.template'].browse(template_id)
        mail_successfully_send = template.with_context(data).sudo().send_mail(mail_to.sudo().id, force_send=True)
        if mail_successfully_send and template_name != 'comparison_for_vote':
            self.message_post(body= _(" %s mail successfully sent to '%s' mail with '%s' user.") %(body_remainder_msg, mail_to.sudo().partner_id.email, mail_to.sudo().partner_id.name))

        return mail_successfully_send

    def send_req_for_start_vote(self):
        self.ensure_one()
        if not self.workflow_id:
            raise UserError(_("Please choose workflow config for this comparison !!!"))

        next_step, next_user_ids = self.workflow_id.get_next_step(self, self.env.uid, self.check_sequence, 'next')
        if not next_step:
            raise UserError(_("There is no approve step for workflow: %s\nPlease check workflow !!!") %self.workflow_id.name)

        self.current_workflow_step = next_step[0].sudo().sequence
        for user_id in next_user_ids:
            self.sudo().send_mail_about_comparison(self.env['res.users'].browse(user_id), self.env.user, 'comparison_for_approve', _('Approvation'))
            current_workflow_history = self.current_workflow_history
            self.current_workflow_history = self.env['workflow.history'].create({
                'user_ids': [(6, 0, next_user_ids)],
                'name': next_step.sudo().name,
                'sent_date': time.strftime('%Y-%m-%d %H:%M:%S'),
                'line_sequence': next_step.sudo().sequence,
                'comparison_id': self.id
            })
            if current_workflow_history:
                current_workflow_history.write({
                    'action_date': time.strftime('%Y-%m-%d %H:%M:%S'),
                    'action': 'approved',
                    'user_id': self.env.uid
                })

        self.set_state_history('req_sent_for_open_vote')

    def open_vote(self):
        self.ensure_one()
        self._cr.execute("""
            SELECT DISTINCT(user_id)
            FROM comparison_order_user
            WHERE comparison_id = %s AND boolean_data = False
        """ % self.id)
        voter_ids = [x[0] for x in self._cr.fetchall()]
        success_send_to = ""
        for voter_id in voter_ids:
            voter = self.env['res.users'].sudo().browse(voter_id)
            success = self.sudo().send_mail_about_comparison(voter, self.env.user, 'comparison_for_vote', _('Voting invitation'))
            if success: success_send_to += str(voter.sudo().partner_id.email) + ", "

        self.set_state_history('collecting_vote')
        if self.current_workflow_history:
            self.current_workflow_history.write({
                'action_date': time.strftime('%Y-%m-%d %H:%M:%S'),
                'action': 'approved',
                'user_id': self.env.uid
            })
            self.check_sequence += 1

        if len(success_send_to) > 0:
            self.message_post(body = _("%s mail successfully sent: %s") % (_('Voting invitation'), success_send_to))

    @api.multi
    def reject(self):
        new_reject = self.env['reject.comparison.popup'].create({
            'comparison': self.id,
            'type': 'reject'
        })
        return {
            'name': 'Reject Comparison',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'reject.comparison.popup',
            'target': 'new',
            'res_id': new_reject.id
        }

    @api.multi
    def cancel(self):
        new_reject = self.env['reject.comparison.popup'].create({
            'comparison': self.id,
            'type': 'cancel'
        })
        return {
            'name': 'Cancel Comparison',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'reject.comparison.popup',
            'target': 'new',
            'res_id': new_reject.id
        }

    def reset(self):
        self.ensure_one()
        self.set_state_history('draft')
        self.check_sequence = 1
        self.current_workflow_history = self.env['workflow.history'].create({
            'name': _("Comparison canceled"),
            'line_sequence': 0,
            'comparison_id': self.id,
            'action_date': time.strftime('%Y-%m-%d %H:%M:%S'),
            'sent_date': time.strftime('%Y-%m-%d %H:%M:%S'),
            'action': 'drafted',
            'user_id': self.env.uid
        })
