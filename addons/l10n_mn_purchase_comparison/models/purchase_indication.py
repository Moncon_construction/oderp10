# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class PurchaseIndication(models.Model):
    _name = 'purchase.indication'
    _description = "Purchase Indication"
    
    name = fields.Char('Indication Name', required=True)  
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    
class ComparisonData(models.Model):
    _name = 'comparison.data'
    _description = "Comparison Data"
    
    int_data = fields.Char('Int Data')
    oneline_data = fields.Char('OneLine Data')
    multiline_data = fields.Char('MultiLine Data')
    date_data = fields.Char('Date Data')
    boolean_data = fields.Char('Boolean Data')
    comparison_indication_id = fields.Many2one('comparison.indication', 'Comparison Indication')
    comparison_id = fields.Many2one('purchase.comparison', 'Purchase Comparison')
    order_id = fields.Many2one('purchase.order', 'Purchase Order')
    
class ComparisonOrderUser(models.Model):
    _name = 'comparison.order.user'
    _description = "Comparison data of purchase orders and users"
    
    user_id = fields.Many2one('res.users', 'User')
    order_id = fields.Many2one('purchase.order', 'Purchase Orders')
    comparison_id = fields.Many2one('purchase.comparison', ondelete='cascade')
    boolean_data = fields.Boolean('Boolean Data')
    states = fields.Selection([
        ('selected', 'Selected'),
        ('not selected', 'Not selected')], string='States',
        copy=False, default='not selected')
    
    @api.multi
    def write(self, vals):
        if 'boolean_data' in vals.keys() and vals['boolean_data']:
            for obj in self:
                obj.comparison_id.check_all_voten(obj.comparison_id.id, obj.order_id.name, self.env.uid)

        return super(ComparisonOrderUser, self).write(vals)