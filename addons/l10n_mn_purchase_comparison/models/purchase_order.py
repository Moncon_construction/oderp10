from odoo import api, fields, models, _
from odoo.exceptions import RedirectWarning, UserError, ValidationError

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    @api.multi
    def _compute_is_comparison_created(self):
        for obj in self:
            if obj.comparison_id.id:
                obj.is_comparison_created = True
    
    purchase_type = fields.Selection([
                                    ('directly', 'Directly'),
                                    ('research', 'Research')], default='directly', string='Purchase Type')
    comparison_id = fields.Many2one('purchase.comparison', 'Purchase Comparison')
    is_selected_by_purchase_user = fields.Boolean('Is Selected Purchase By User')
    is_selected_by_purchase_manager = fields.Boolean('Is Selected Purchase By Manager')
    comparison_order_user_id = fields.Many2one('comparison.order.user', 'Comparison Order User')
    is_comparison_created = fields.Boolean('Is comparison created', compute='_compute_is_comparison_created', default=False)
    is_lost = fields.Boolean('Is lost', default=False)

    @api.multi
    def button_confirm(self):
        for obj in self:
            if obj.purchase_type == 'research' and not obj.is_comparison_created:
                raise ValidationError(_('Purchase type is Research, so please create a purchase comparison.'))
        return super(PurchaseOrder, self).button_confirm()

    def button_cancel(self):
        for obj in self:
            obj.is_lost = False
        return super(PurchaseOrder, self).button_cancel()
    
class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'
    
    comparison_id = fields.Many2one('purchase.comparison', 'Comparison')