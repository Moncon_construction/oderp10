odoo.define('l10n_mn_purchase_comparison.comparison', function (require) {
    "use strict";

    var core = require('web.core');
    var data = require('web.data');
    var form_common = require('web.form_common');
    var formats = require('web.formats');
    var Model = require('web.DataModel');
    var utils = require('web.utils');
    var supps = [];
    var supp_ids = {};
    var data_ids = {};
    var data_order_ids = {};
    var group_name = [];
    var QWeb = core.qweb;
    var _t = core._t;

    var Comparison = form_common.FormWidget.extend(form_common.ReinitializeWidgetMixin, {
        events: {
            "click .oe_partner a": "go_to",
            "click .oe_supplier a": "go_to_supp",
            "change .buyer": "on_buyer_check",
            "change .indication": "on_buyer_update", // Харьцуулалтын boolean талбар бүхий үнэлгээг хэд хэдэн айл дээр хийж болно гэж үзсэн тул шууд on_buyer_check-г дамжуулав.
        },
        init: function () {
            this._super.apply(this, arguments);
            this.set({
                sheets: [],
                date_to: false,
                date_from: false,
            });
            this.field_manager.on("field_changed:order_ids", this, this.query_sheets);
            this.on("change:sheets", this, this.update_sheets);
            this.res_o2m_drop = new utils.DropMisordered();
            this.render_drop = new utils.DropMisordered();
            this.get_group_drop = new utils.DropMisordered();
            this.description_line = _t("/");
            this.get_group();
        },
        on_buyer_check:function(e) {
        	// Нэвтэрсэн хэрэглэгч зөвхөн өөрийн санал өгөх мөр дээр л саналаа тэмдэглэх боломжтой болгов.
        	var current_check_field_user = $(e.target).parent().context.dataset['buyerCheck'];
        	var current_check_field_user_id = current_check_field_user.split("_")[0];
        	var current_user_id = this.field_manager.get_field_value("current_user");
            if (current_check_field_user_id != current_user_id){
            	if ($(e.target).prop('checked')){
            		$(e.target).prop('checked', false);
            	}else{
            		$(e.target).prop('checked', true);
            	}
            	alert("Зөвхөн өөрийн харгалзах хэсэгт санал өгнө үү !!!");
            }else{
            	this.check_one('.buyer', e.target);
            	var comparison_model = new Model('purchase.comparison');
            	var comparison_id = self.field_manager.get_field_value("comparison_id");
            }
        },
        check_one: function (classes, target) {
            $(target).parent().parent().find(classes).prop('checked', false);
            $(target).prop('checked', true);

            this.on_buyer_update();
        },
        get_group: function () {
            self = this;
            this.get_group_drop.add(
                new Model(this.view.model).call("get_groups", [new data.CompoundContext()])
            ).done(function (result) {
                self.querying = true;
                self.group_name = result;
                self.querying = false;
            });
        },
        get_formatted_amount: function (amount) {
            var amount1 = Math.round(amount);
            return amount1.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
        },
        on_buyer_update: function (event) {
            self.querying = false;
            this.update_sheets();
        },
        query_sheets: function () {
            var self = this;
            if (self.updating)
                return;
            var commands = this.field_manager.get_field_value("order_ids");
            this.res_o2m_drop.add(
                new Model(this.view.model).call("resolve_2many_commands", ["order_ids", commands, [], new data.CompoundContext()])
            ).done(function (result) {
                self.querying = true;
                self.set({
                    sheets: result
                });
                self.querying = false;
            });
        },
        update_sheets: function () {
            var self = this;
            if (self.querying) {
                return;
            }
            self.updating = true;
            var sheet = self.get("sheets");

            var ops = [],
                orders = [],
                order_data = [],
                indi_orders = [];

            _.each(self.get("sheets"), function (order) {
                var tmp = _.clone(order);
                _.each(order, function (v, k) {
                    if (v instanceof Array) {
                        tmp[k] = v[0];
                    }
                });
                ops.push(tmp);
            });

            _.each(self.data_ids, function (data_id) {
                var indication_data = {
                    id: data_id.id,
                    order_id: data_id.order_id[0],
                    comparison_id: data_id.comparison_id[0],
                    comparison_indication_id: data_id.comparison_indication_id[0],
                    boolean_data: null,
                    oneline_data: null,
                    multiline_data: null,
                    date_data: null,
                    int_data: null
                };

                _.each(self.order_ids, function (order_id_item) {
                    if (indication_data.order_id == order_id_item) {
                        switch(self.inner_type[data_id.comparison_indication_id[0]]) {
                            case 'boolean_field': 
                                indication_data.boolean_data = this.$('[data-indication="' + data_id.comparison_indication_id[0] + '_indication_field"][order_id="' + order_id_item + '"]').is(":checked");
                                break;
                            case 'one_line':
                                indication_data.oneline_data = this.$('[data-indication="' + data_id.comparison_indication_id[0] + '_indication_field"][order_id="' + order_id_item + '"]').val();
                                break;
                            case 'multiple_line':
                                indication_data.multiline_data = this.$('[data-indication="' + data_id.comparison_indication_id[0] + '_indication_field"][order_id="' + order_id_item + '"]').val();
                                break;
                            case 'date':
                                indication_data.date_data = this.$('[data-indication="' + data_id.comparison_indication_id[0] + '_indication_field"][order_id="' + order_id_item + '"]').val();
                                break;
                            case 'int':
                                indication_data.int_data = this.$('[data-indication="' + data_id.comparison_indication_id[0] + '_indication_field"][order_id="' + order_id_item + '"]').val();
                                break;
                        }
                    }
                }, this);

                indi_orders.push(indication_data);
            }, this);

            new Model(this.view.model).call('update_value', [indi_orders]).then(function (responded) {});

            _.each(self.data_order_ids, function (data_order_id) {
                var order_comparison_data = {
                    id: data_order_id.id,
                    order_id: data_order_id.order_id[0],
                    user_id: data_order_id.user_id[0],
                    comparison_id: data_order_id.comparison_id[0],
                    boolean_data: null,
                    states: null
                };

                _.each(self.order_ids, function (order_id_item) {
                    if (order_comparison_data.order_id == order_id_item) {
                        order_comparison_data.boolean_data = this.$('[data-buyer-check="' + data_order_id.user_id[0] + '_buyer_check2"][order_id="' + order_id_item + '"]').is(":checked");
                    }
                    if (order_comparison_data.boolean_data == true) {
                        order_comparison_data.states = "selected";
                    } else {
                        order_comparison_data.states = "not selected";
                    }
                }, this);

                order_data.push(order_comparison_data);
            }, this);

            new Model(this.view.model).call('update_comparison_user', [order_data]).then(function (responded) {});
        },
        go_to: function (event) {
            var id = JSON.parse(event.target.dataset.id);
            this.do_action({
                type: 'ir.actions.act_window',
                res_model: "res.partner",
                res_id: id,
                views: [
                    [false, 'form']
                ],
                target: 'current'
            });
        },
        go_to_supp: function (event) {
            var id = JSON.parse(event.target.dataset.id);
            this.do_action({
                type: 'ir.actions.act_window',
                res_model: "purchase.order",
                res_id: id,
                views: [
                    [false, 'form']
                ],
                target: 'current'
            });
        },
        initialize_field: function () {
            form_common.ReinitializeWidgetMixin.initialize_field.call(this);
            var self = this;
            self.on("change:sheets", self, self.initialize_content);
        },
        fetch: function (model, fields, domain, ctx) {
            return new Model(model).query(fields).filter(domain).context(ctx).all();
        },
        initialize_content: function () {
            var self = this;
            self.accounts = ['account1', 'account2'];
            self.suppliers = [];
            self.order_ids = [];
            self.indication_ids = [];
            self.products = [];
            self.product_objs = [];
            self.product_names = {};
            self.supplier_names = {};
            self.supplier_id = {};
            self.order_id = {};
            self.indications = [];

            self.orders = this.field_manager.get_field_value("order_ids");
            self.indications = this.field_manager.get_field_value("indication_ids");

            _.each(self.orders, function (order) {
                self.order_ids = order[2];
            });

            _.each(self.indications, function (indication) {
                self.indication_ids = indication[2];
            });

            var loaded1 = self.fetch('comparison.indication', ['id', 'name', 'inner_type', ], [
                ['id', 'in', self.indication_ids]
            ]).then(function (indication_objs) {
                self.indication_name = {};
                self.inner_type = {};
                _.each(indication_objs, function (indication_obj) {
                    self.indication_name[indication_obj.id] = indication_obj.name;
                    self.inner_type[indication_obj.id] = indication_obj.inner_type;
                });
            });

            var loaded2 = self.fetch('comparison.data', ['id', 'int_data', 'oneline_data', 'order_id', 'multiline_data', 'date_data', 'boolean_data', 'comparison_indication_id', 'comparison_id'], [
                ['comparison_id', '=', self.field_manager.get_field_value("comparison_id")]
            ]).then(function (data_obj) {
                self.data_ids = data_obj;
            });

            var loaded3 = self.fetch('comparison.order.user', ['id', 'user_id', 'comparison_id', 'order_id', 'boolean_data', 'user_name', 'states'], [
                ['order_id', 'in', self.order_ids],
                ['comparison_id', '=', self.field_manager.get_field_value("comparison_id")]
            ]).then(function (data_order_objs) {
                self.user_name = {};
                self.users = [];
                self.user_ids = [];
                self.data_order_ids = data_order_objs;
                _.each(data_order_objs, function (data_order_obj) {
                    self.user_name[data_order_obj.user_id[0]] = data_order_obj.user_id[1];
                    // Санал өгөх хэрэглэгчдийг хадгалж авах
                    if (self.user_ids.indexOf(data_order_obj.user_id[0]) == -1){
                    	self.user_ids.push(data_order_obj.user_id[0]);
                    	self.users.push(data_order_obj.user_id);
                    }
                });
            });

            var loaded = self.fetch('purchase.order', ['id', 'name', 'partner_id', 'amount_cost', 'order_line', 'calculated_price', 'transport_cost', 'prod_insurance', 'is_selected_by_purchase_user', 'is_selected_by_purchase_manager', 'states', 'states2', 'experience_real', 'guarantee_real', 'warranty_years', 'supply_dur_real', 'cost_unit', 'currency_id', 'incoterm_id', 'amount_total'], [
                ['id', 'in', self.order_ids]
            ]).then(function (order_objs) {
                self.suppliers = [];
                self.chosen_supp = false;
                self.order_names = {};
                self.quotation_price = {};
                self.calculated_price = {};
                self.transport_cost = {};
                self.prod_insurance = {};
                self.product_qty = {};
                self.product_price = {};
                self.products = [];
                self.product_objs = [];
                self.product_names = {};
                self.supplier_names = {};
                self.supplier_id = {};
                self.order_id = {};
                self.cost_unit = {};
                self.currency_id = {};
                self.incoterm_id = {};
                self.experience_real = {};
                self.guarantee_real = {};
                self.supply_dur_real = {};
                self.is_selected_by_purchase_user = {};
                self.is_selected_by_purchase_manager = {};
                self.states = {};
                self.states2 = {};
                self.attachments = {};
                var order_lines = {};

                _.each(order_objs, function (order_obj) {
                    self.suppliers.push(order_obj.name);
                    self.attachments[order_obj.name] = [];
                    var attch = self.fetch('ir.attachment', ['id', 'datas_fname'], [
                        ['res_model', '=', 'purchase.order'],
                        ['res_id', '=', order_obj.id]
                    ]).then(function (attachs) {
                        _.each(attachs, function (attach) {
                            var found = 0;
                            for (var i in self.attachments[order_obj.name]) {
                                if (self.attachments[order_obj.name][i].id == attach.id) {
                                    found = 1;
                                }
                            }
                            if (found == 0) {
                                self.attachments[order_obj.name].push({
                                    'id': attach.id,
                                    'filename': attach.datas_fname
                                });
                            }
                        });
                    });

                    supp_ids[order_obj.name] = order_obj.id;
                    if (order_obj.amount_cost == false) {
                        self.quotation_price[order_obj.name] = 0;
                        self.calculated_price[order_obj.name] = 0;
                    } else {
                        self.quotation_price[order_obj.name] = order_obj.amount_total;
                    }
                    self.order_names[order_obj.id] = order_obj.name;
                    self.calculated_price[order_obj.name] = self.quotation_price[order_obj.name] + order_obj.transport_cost + order_obj.prod_insurance;
                    self.supplier_names[order_obj.name] = order_obj.partner_id[1];
                    self.supplier_id[order_obj.name] = order_obj.partner_id[0];
                    self.transport_cost[order_obj.name] = order_obj.transport_cost;
                    self.prod_insurance[order_obj.name] = order_obj.prod_insurance;
                    self.order_id[order_obj.name] = order_obj.id;
                    order_lines[order_obj.name] = order_obj.order_line;
                    self.incoterm_id[order_obj.name] = order_obj.incoterm_id.id;
                    self.experience_real[order_obj.name] = order_obj.experience_real;
                    self.guarantee_real[order_obj.name] = order_obj.warranty_years;
                    self.supply_dur_real[order_obj.name] = order_obj.supply_dur_real;
                    self.currency_id[order_obj.name] = order_obj.currency_id.id;
                    self.cost_unit[order_obj.name] = order_obj.cost_unit;
                    self.is_selected_by_purchase_user[order_obj.name] = Boolean(order_obj.is_selected_by_purchase_user);
                    self.is_selected_by_purchase_manager[order_obj.name] = Boolean(order_obj.is_selected_by_purchase_manager);
                    self.states[order_obj.name] = order_obj.states;
                    self.states2[order_obj.name] = order_obj.states2;
                });
                supps = self.suppliers;
                return order_lines;
            }).then(function (order_lines) {
                _.each(order_lines, function (line) {
                    _.each(self.suppliers, function (supplier) {
                        self.product_price[supplier] = {};
                        self.product_qty[supplier] = {};
                        var products = self.fetch('purchase.order.line', ['id', 'product_id', 'price_unit', 'product_qty'], [
                            ['id', 'in', order_lines[supplier]]
                        ]).then(function (lines) {
                            _.each(lines, function (line) {
                                if (self.products.indexOf(line.product_id[1]) < 0) {
                                    self.products.push(line.product_id[1]);
                                }
                                if (!self.has_product(line.product_id[0])) {
                                    self.product_objs.push({
                                        'id': line.product_id[0],
                                        'name': line.product_id[1]
                                    });
                                }
                                self.product_price[supplier][line.product_id[1]] = line.price_unit;
                                self.product_qty[supplier][line.product_id[1]] = line.product_qty;
                            });

                            self.display_data();
                            return products;
                        }).then(function (products) {
                            return self.products;
                        });
                    });
                });
            });
        },
        has_product: function (id) {
            var index, obj = [];
            for (index in this.product_objs) {
                obj = this.product_objs[index];
                if (obj.id == id) {
                    return true;
                }
            }
            return false;
        },
        get_price: function (supplier, product) {
            self = this;
            for (var field in self.product_price[supplier]) {
                if (field == product) {
                    return this.get_formatted_amount(self.product_price[supplier][product]);
                }
            }
            return 0;
        },
        get_states: function (supplier) {
            var data_state = "";
            var selections = [];
            var supp_index = -1;
            var last_emp_voten_index = -1;
            
            for (var i in this.suppliers){
            	selections[i] = 0;
            }
            if (self.users.length >0) {
                var last_user_id = self.users[self.users.length - 1][0];
                _.each(self.data_order_ids, function (data_order_id) {
                    if (data_order_id.states == "selected") {
                        var index = -1;
                        var chosen_index = -1;
                        for (var sup in this.suppliers){
                            index += 1;
                            // Тухайн шалгаж буй нийлүүлэгчийн индексийг хадгалах
                            if (this.suppliers[sup] == supplier){
                                supp_index = index;
                            }
                            if (this.suppliers[sup] == self.order_names[data_order_id.order_id[0]]){
                                selections[index] += 1;
                                chosen_index = index;
                            }
                        }
                        // Хамгийн сүүлийн ажилтны санал: Хэрвээ нийлүүлэгч дээр санал тэнцвэл сүүлийн ажилтны санал шийдвэрлэх санал байна.
                        if (last_user_id == data_order_id.user_id[0]){
                            last_emp_voten_index = chosen_index;
                        }
                    }
                }, this);
                }
            
            var indexOfMaxValue = selections.reduce((iMax, x, i, arr) => x >= arr[iMax] ? i : iMax, 0);
            var maxValue = Math.max(...selections);
            
            var count_of_duplicated_max_value = 0;
            for (var i in selections){
            	if (selections[i] == maxValue){
            		count_of_duplicated_max_value += 1;
            	}
            }
            // Хамгийн их санал авсан нийлүүлэгч давхцсан бол хамгийн сүүлийн санал өгөх ажилтныг хүчин төгөлдөрт тооцно
            if (count_of_duplicated_max_value > 1){
            	if (supp_index == last_emp_voten_index){
            		data_state = "selected";
            	}else{
            		data_state = "not selected";
            	}
            }else{
	            if (supp_index == indexOfMaxValue){
	            	data_state = "selected";
	            }else{
	            	data_state = "not selected";
	            }
            }
            if (data_state == "selected"){
            	for (var i in this.suppliers){
            		if (supplier == this.suppliers[i]){
            			this.chosen_supp = supplier;
            		}
            	}
            }
            
            return data_state;
        },
        get_input_type: function (inner_type) {
            switch (self.inner_type[inner_type]) {
                case 'boolean_field':
                    return 'checkbox';
                case 'one_line':
                    return 'text';
                case 'multiple_line':
                    return 'text';
                case 'date':
                    return 'date';
                case 'int':
                    return 'number';
                default:
                    return 'text';
            }
        },
        get_value: function (inner_type, order_id) {
            var value = "";
            switch (self.inner_type[inner_type]) {
                case 'one_line':
                    _.each(self.data_ids, function (data_id) {
                        if (inner_type == data_id.comparison_indication_id[0] && data_id.order_id[0] == order_id) {
                            if (data_id.oneline_data != "") {
                                value = data_id.oneline_data;
                            } else {
                                value = "";
                            }
                        }
                    });
                    break;
                case 'multiple_line':
                    _.each(self.data_ids, function (data_id) {
                        if (inner_type == data_id.comparison_indication_id[0] && data_id.order_id[0] == order_id) {
                            if (data_id.multiline_data == "") {
                                value = "";
                            } else {
                                value = data_id.multiline_data;
                            }
                        }
                    });
                    break;
                case 'date':
                    _.each(self.data_ids, function (data_id) {
                        if (inner_type == data_id.comparison_indication_id[0] && data_id.order_id[0] == order_id) {
                            if (data_id.date_data == "") {
                                value = "";
                            } else {
                                value = data_id.date_data;
                            }
                        }
                    });
                    break;
                case 'int':
                    _.each(self.data_ids, function (data_id) {
                        if (inner_type == data_id.comparison_indication_id[0] && data_id.order_id[0] == order_id) {
                            if (data_id.int_data == "") {
                                value = "";
                            } else {
                                value = data_id.int_data;
                            }
                        }
                    });
                    break;
            }
            return value;
        },
        get_value2: function (inner_type, order_id) {
            var data = false;
            if (self.inner_type[inner_type] == 'boolean_field') {
                _.each(self.data_ids, function (data_id) {
                    if (inner_type == data_id.comparison_indication_id[0] && data_id.order_id[0] == order_id && data_id.boolean_data != "") {
                        data = true;
                    }
                });
            }
            return data;
        },
        get_value6: function (user_name, order_id) {
            var data = false;
            _.each(self.data_order_ids, function (data_order_id) {
                if (user_name == self.user_name[data_order_id.user_id[0]] &&
                    self.field_manager.get_field_value("comparison_id") == data_order_id.comparison_id[0] &&
                    data_order_id.order_id[0] == order_id && data_order_id.boolean_data != "") {
                    data = true;
                }
            });
            return data;
        },
        get_indication_name: function (inner_type) {
            return this.indication_name[inner_type];
        },
        get_user_name: function (user_id) {
            return this.user_name[user_id];
        },
        get_qty: function (supplier, product) {
            self = this;
            for (var field in self.product_qty[supplier]) {
                if (field == product) {
                    return this.get_formatted_amount(self.product_qty[supplier][product]);
                }
            }
            return 0;
        },
        is_valid_value: function (value) {
            var split_value = value.split(":");
            var valid_value = true;
            if (split_value.length > 2)
                return false;
            _.detect(split_value, function (num) {
                if (isNaN(num)) {
                    valid_value = false;
                }
            });
            return valid_value;
        },
        display_data: function () {
            var self = this;
            self.$el.html(QWeb.render("l10n_mn_purchase_comparison.Comparison", {
                widget: self
            }));
        },
        get_box: function (account, day_count) {
            return this.$('[data-account="' + account.account + '"][data-day-count="' + day_count + '"]');
        },
        get_total: function (account) {
            return this.$('[data-account-total="' + account.account + '"]');
        },
        get_day_total: function (day_count) {
            return this.$('[data-day-total="' + day_count + '"]');
        },
        get_super_total: function () {
            return this.$('.oe_timesheet_weekly_supertotal');
        },
        generate_o2m_value: function () {
            var self = this;
            var ops = [];

            _.each(self.accounts, function (account) {
                var auth_keys = _.extend(_.clone(account.account_defaults), {
                    name: true,
                    amount: true,
                    unit_amount: true,
                    date: true,
                    account_id: true,
                });
                _.each(account.days, function (day) {
                    _.each(day.lines, function (line) {
                        if (line.unit_amount !== 0) {
                            var tmp = _.clone(line);
                            tmp.id = undefined;
                            _.each(line, function (v, k) {
                                if (v instanceof Array) {
                                    tmp[k] = v[0];
                                }
                            });
                            // we have to remove some keys, because analytic lines are shitty
                            _.each(_.keys(tmp), function (key) {
                                if (auth_keys[key] === undefined) {
                                    tmp[key] = undefined;
                                }
                            });
                            ops.push(tmp);
                        }
                    });
                });
            });
            return ops;
        },
        sync: function () {
            var self = this;
            self.setting = true;
            self.set({
                sheets: this.generate_o2m_value()
            });
            self.setting = false;
        },
        //converts hour value to float
        parse_client: function (value) {
            return formats.parse_value(value, {
                type: "float_time"
            });
        },
        //converts float value to hour
        format_client: function (value) {
            return formats.format_value(value, {
                type: "float_time"
            });
        },
        // Санал хураалт эхэлсэн эсэх
        start_vote: function(user_id){
            var current_state = this.field_manager.get_field_value("state");
            if (current_state == 'collecting_vote'){
            	return true;
            }else{
            	return false;
            }
        },
        // Ноорог төлөвтэй үед л үзүүлэлтүүд засах
        is_draft: function(){
            var current_state = this.field_manager.get_field_value("state");
            if (current_state == 'draft' || current_state == 'req_sent_for_open_vote'){
            	return true;
            }else{
            	return false;
            }
        },
    });

    core.form_custom_registry.add('comparison_widget', Comparison);
});