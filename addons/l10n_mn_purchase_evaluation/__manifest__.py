# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Purchase Evaluation",
    'version': '1.0',
    'depends': ['l10n_mn_purchase'],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Account Modules',
    'description': """
         Нийлүүлэгчийн үнэлгээ.
    """,
    'website' : 'http://asterisk-tech.mn',
    'data': [
        'security/ir.model.access.csv',
        'views/supplier_evaluation_indicator_view.xml',
        'views/supplier_evaluation_view.xml',
        'views/supplier_evaluation_history.xml',
        'data/ir_sequence_data.xml',

    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}