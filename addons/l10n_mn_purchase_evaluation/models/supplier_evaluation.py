# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from datetime import datetime

from odoo.exceptions import UserError


class SupplierEvaluation(models.Model):
    _name = 'supplier.evaluation'
    _description = 'Supplier Evaluation'

    name = fields.Char(string="Sequence", readonly=True)
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id, required=True)
    evaluated_date = fields.Date(string='Date', default=datetime.today(), required=True)
    user_id = fields.Many2one('res.users', required=True, string='User', default=lambda self: self.env.user)
    purchase_order_id = fields.Many2one('purchase.order', string='Purchase', domain="[('state', '=', ('done'))]",
                                        required=True)
    evaluation_id = fields.Many2one('res.partner', related='purchase_order_id.partner_id', string='Supplier',
                                    readonly=True, store=True)
    avg_evaluation = fields.Float('Average Evaluation', readonly=True, copy=False,
                                    compute='_calculate_avg_evaluation',  digits=(12, 1))
    note = fields.Text('Notes')
    evaluation_line_ids = fields.One2many('supplier.evaluation.line', 'evaluation_id', string='Weight')
    state = fields.Selection([('draft', 'Draft'),
                              ('sent', 'Sent'),
                              ('finished', 'Finished'),
                              ('canceled', 'Canceled')], default='draft', string='State')

    @api.onchange('evaluation_line_ids')
    def _calculate_avg_evaluation(self):
        for point in self:
            summer = 0.0
            count = 0
            for obj in point.evaluation_line_ids:
                summer += int(obj.calculate_by)
                count += 1
            point.avg_evaluation = summer / count if count > 0 else 0

    @api.model
    def create(self, vals):
        res = super(SupplierEvaluation, self).create(vals)
        res.name = self.env['ir.sequence'].next_by_code('supplier.evaluation')
        return res

    def unlink(self):
        for order in self:
            if order.state not in ('draft', 'canceled'):
                raise UserError(
                    _('You can not delete this evaluation. You can only be deleted during the draft and canceled!'))
        return super(SupplierEvaluation, self).unlink()


    def button_draft(self):
        self.state = 'draft'

    def button_sent(self):
        if self.evaluation_line_ids:
            self.state = 'sent'
        else:
            raise UserError(
                _('Enter evaluation line'))
    def button_finished(self):
        self.state = 'finished'

    def button_canceled(self):
        if self.state == 'finished':
            self.evaluation_line_ids.unlink()
        self.state = 'canceled'


class SupplierEvaluationLine(models.Model):
    _name = 'supplier.evaluation.line'
    _description = 'Supplier Evaluation Line'

    evaluation_id = fields.Many2one('supplier.evaluation', string='Supplier')
    indicator = fields.Many2one('supplier.evaluation.indicator', string='Indicator', required=True)
    calculate_by = fields.Selection([('1', '1'),
                                     ('2', '2'),
                                     ('3', '3'),
                                     ('4', '4'),
                                     ('5', '5')], string='Evaluate', required=True)
    description = fields.Text('Description')


class ResPartner(models.Model):
    _inherit = 'res.partner'

    evaluation_history_ids = fields.One2many('supplier.evaluation', 'evaluation_id', readonly=True,
                                             domain=lambda self: [('state', '=', 'finished')])
    evaluation_point = fields.Float(compute='_evaluation_point', digits=(12, 1))

    @api.onchange('evaluation_history_ids')
    def _evaluation_point(self):
        for point in self:
            summer = 0.0
            count = 0
            for obj in point.evaluation_history_ids:
                summer += obj.avg_evaluation
                count += 1
            point.evaluation_point = (summer / count) if count > 0 else 0
