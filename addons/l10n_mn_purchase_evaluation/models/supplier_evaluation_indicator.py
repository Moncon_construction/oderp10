# -*- coding: utf-8 -*-
from odoo import fields, models


class SupplierEvaluationIndicator(models.Model):
    _name = 'supplier.evaluation.indicator'
    _description = 'Supplier Evaluation Indicator'

    name = fields.Char('Name', required=True)
    description = fields.Text('Description')
