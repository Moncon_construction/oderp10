# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

{
    'name': "Mongolian Purchase Extra Cost",
    'version': '1.0',
    'depends': [
        'l10n_mn_extra_cost_item',
        'l10n_mn_purchase',
        'l10n_mn_stock'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
        Purchase Additional Features
    """,
    'data': [
        'security/ir.model.access.csv',
        'wizard/purchase_extra_cost_print.xml',
        'wizard/purchase_cost_approval_views.xml',
        'wizard/purchase_nocost_approval_views.xml',
        'views/purchase_order_views.xml',
        'report/purchase_detailed_report_by_display.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
