# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.tools.translate import _
from odoo.addons.l10n_mn_extra_cost_item.models.extra_cost_item import SPLIT_METHOD_EC as ec_item  # @UnresolvedImport
from odoo.exceptions import ValidationError


class PurchaseExtraCost(models.Model):
    _name = 'purchase.extra.cost'
    _description = 'Purchase Extra Cost'

    order_id = fields.Many2one('purchase.order', 'Purchase', ondelete='cascade')
    name = fields.Char('Name', size=128, required=True)
    item_id = fields.Many2one('extra.cost.item', 'Item', required=True)
    state = fields.Selection([('draft', 'Draft'), ('completed', 'Completed')], string='State', default='draft', required=True)
    amount = fields.Float('Amount', required=True)
    currency_id = fields.Many2one('res.currency', 'Currency', required=True)
    account_id = fields.Many2one('account.account', 'Account', required=True)
    allocmeth = fields.Selection(ec_item, string='Allocation Method', default='by_subtotal', required=True)
    partner_id = fields.Many2one('res.partner', 'Related Partner', required=True)
    account_analytic_id = fields.Many2one('account.analytic.account', 'Analytic Account', domain="[('type','=','normal')]")
    invoice_id = fields.Many2one('account.invoice', 'Invoice')
    date_invoice = fields.Date('Extra Cost Invoice Date', required=True)
    adjustment_lines = fields.One2many('purchase.extra.cost.line', 'extra_cost_id', 'Cost Adjustment Lines', readonly=True)

    # Additional Field Displaying the used currency
    currency_rate = fields.Float('Extra Cost Currency Rate', readonly=True, compute='_get_ec_ratebydate', store=True)
    currency_amount = fields.Float('Extra Cost Currency Amount', readonly=True, compute='_get_ec_ramountbydate', store=True)
    taxes_id = fields.Many2one('account.tax', 'Taxes')
    vat_indication_id = fields.Many2one('vat.indication', 'Vat Indication', domain=[('indication_type', '=', 'outcome')])

    @api.multi
    @api.depends('date_invoice', 'currency_id')
    def _get_ec_ratebydate(self):
        for this in self:
            if this.currency_id.id != this.env.user.company_id.currency_id.id:
                this.ensure_one()
                this.currency_rate = this.currency_id.with_context(date=this.date_invoice).compute(1, this.env.user.company_id.currency_id)
            else:
                this.currency_rate = 1

    @api.multi
    @api.depends('date_invoice', 'currency_id', 'amount')
    def _get_ec_ramountbydate(self):
        for this in self:
            if this.currency_id.id != this.env.user.company_id.currency_id.id:
                this.currency_amount = this.currency_id.with_context(date=this.date_invoice).compute(this.amount, this.env.user.company_id.currency_id)
            else:
                this.currency_amount = this.amount

    @api.multi
    @api.onchange('item_id')
    def onchange_item_id(self):
        for this in self:
            this.currency_id = self.item_id.currency.id
            this.allocmeth = self.item_id.allocmeth
            this.name = self.item_id.name
            this.partner_id = self.order_id.partner_id
            this.date_invoice = self.order_id.date_order
            this.account_id = self.item_id.account_id

    @api.multi
    def unlink(self):
        self.mapped('invoice_id').unlink()
        return super(PurchaseExtraCost, self).unlink()

    @api.multi
    def check_module_installed(self, module_name):
        for record in self:  # @UnusedVariable
            module = self.sudo().env['ir.module.module'].search([
                ('name', '=', module_name)
            ])
            if module and module.state == 'installed' or module.state == 'to upgrade':
                return True
            else:
                return False

    # Нэхэмжлэх үүсгэх функц 2017.10.13
    @api.multi
    def make_invoice(self):
        product_uom_unit = self.env['product.uom']
        inv_obj = self.env['account.invoice']
        invoice_line_ids = []
        for extra in self:
            order = extra.order_id
            if order:
                journal_ids = self.env['account.journal'].search(
                    [('type', '=', 'purchase'),
                     ('company_id', '=', self.env.user.company_id.id)],
                    limit=1
                )
                if not journal_ids:
                    raise ValidationError(_("Define purchase journal for this company! %s (id:%d).") % (self.env.user.company_id.name, self.env.user.company_id.id))

                account = extra.partner_id.property_account_payable_id
                
                invoice_line_ids.append((0, 0, {
                    'name': extra.name,
                    'account_id': extra.account_id.id,
                    'price_unit': extra.amount,
                    'quantity': 1.0,
                    'product_id': False,
                    'uom_id': product_uom_unit,
                    'invoice_line_tax_ids': [(6, 0, extra.taxes_id.ids)],
                    'account_analytic_id': extra.account_analytic_id.id or False}))
                
                inv_vals = {
                    'purchase_id': order.id,
                    'name': order.partner_ref or order.name,
                    'reference': order.partner_ref or order.name,
                    'account_id': account.id,
                    'type': 'in_invoice',
                    'partner_id': extra.partner_id.id,
                    'currency_id': extra.currency_id.id,
                    'journal_id': len(journal_ids) and journal_ids[0].id or False,
                    'invoice_line_ids': invoice_line_ids,
                    'origin': order.name,
                    'fiscal_position_id': order.fiscal_position_id.id or False,
                    'payment_term_id': False,
                    'company_id': self.env.user.company_id.id,
                    'date_invoice': extra.date_invoice or order.date_order,
                    'currency_rate': self.currency_rate
                }

                inv_id = inv_obj.create(inv_vals)
                order.invoice_ids = inv_id

                order.write({'invoice_ids': [(4, inv_id.id)]})
                extra.write({'invoice_id': inv_id.id})
        return True

    @api.multi
    def calculate_cost(self, order_lines):
        ''' Тухайн худалдан авалт дээр оруулсан нэмэлт зардлуудыг
            захиалгын мөр бүрт шингээнэ
        '''
        cost = self

        line_cost_obj = self.env['purchase.extra.cost.line']
        self.mapped('adjustment_lines').unlink()

        order = cost.order_id
        total_cost_plus = 0

        cost_line = cost

        total_qty = 0.0
        total_amount = 0.0
        total_price = 0.0
        total_weight = 0.0
        total_volume = 0.0
        total_line = 0.0

        lines = order_lines

        products_without_weight = []
        products_without_volume = []
        
        at_least_one_weight = False
        at_least_one_volume = False
        
        
        if self.allocmeth == 'by_weight':
            for line in lines:
                if line.product_id.weight != 0:
                    at_least_one_weight = True
                else:
                    products_without_weight.append(u'[%s] %s' % (line.product_id.default_code, line.product_id.name))
            if not at_least_one_weight:
                raise ValidationError(_("Please select weight of products"))
            
        if self.allocmeth == 'by_volume':
            for line in lines:
                if line.product_id.volume != 0:
                    at_least_one_volume = True
                else:
                    products_without_volume.append(u'[%s] %s' % (line.product_id.default_code, line.product_id.name))
            if not at_least_one_volume:
                raise ValidationError(_("Please select volume of products"))
        
        warning_mess = {}
        
        if products_without_volume:
            order.message_post(body=_(u"Products without volume. %s") % (','.join(v for v in products_without_volume)))
        
        if products_without_weight:
            order.message_post(body=_(u"Products without weight. %s") % (','.join(w for w in products_without_weight)))
        
        for line in lines:
            if line.product_id:
                total_weight += line.product_id.weight * line.product_qty
                total_volume += line.product_id.volume * line.product_qty
            total_amount += line.price_unit * line.product_qty
            total_price += line.price_unit
            total_qty += line.product_qty
            total_line += 1

        per_unit = 0
        costlineamount = cost_line.amount
        if cost_line.allocmeth == 'by_quantity' and total_qty != 0:
            per_unit = (costlineamount / total_qty)
        elif cost_line.allocmeth == 'by_weight' and total_weight != 0:
            per_unit = (costlineamount / total_weight)
        elif cost_line.allocmeth == 'by_volume' and total_volume != 0:
            per_unit = (costlineamount / total_volume)
        elif cost_line.allocmeth == 'equal' and total_line != 0:
            per_unit = (costlineamount / total_line)
        elif cost_line.allocmeth == 'by_subtotal' and total_amount != 0:
            per_unit = (costlineamount / total_amount)

        for line in lines:
            _cost_plus = 0
            if cost_line.allocmeth == 'by_quantity':
                _cost_plus = per_unit
            elif cost_line.allocmeth == 'by_weight':
                _cost_plus = per_unit * line.product_id.weight
            elif cost_line.allocmeth == 'by_volume':
                _cost_plus = per_unit * line.product_id.volume
            elif cost_line.allocmeth == 'equal' and per_unit:
                _cost_plus = per_unit / line.product_qty
            elif cost_line.allocmeth == 'by_subtotal':
                _cost_plus = per_unit * line.price_unit

            total_cost_plus += _cost_plus

            line_cost_obj.create({
                'purchase_line_id': line.id,
                'extra_cost_id': cost_line.id,
                'unit_cost': _cost_plus,
                'total_cost': _cost_plus * line.product_qty})

        self.write({'state': 'completed'})
        
        return True
