# -*- coding: utf-8 -*-

from odoo import fields, models
import odoo.addons.decimal_precision as dp  # @UnresolvedImport


class PurchaseExtraCostLine(models.Model):
    _name = 'purchase.extra.cost.line'
    _description = 'Purchase Extra Cost Line'

    purchase_line_id = fields.Many2one('purchase.order.line', 'Purchase Line', required=True, readonly=True)
    extra_cost_id = fields.Many2one('purchase.extra.cost', 'Extra Cost', readonly=True, ondelete='cascade')
    unit_cost = fields.Float('+ unit cost', readonly=True, digits=dp.get_precision('Extra Cost Item Amount'))
    total_cost = fields.Float('+ total cost', readonly=True, digits=dp.get_precision('Extra Cost Item Amount'))
