# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.exceptions import ValidationError
from odoo.tools.translate import _
import odoo.addons.decimal_precision as dp  # @UnresolvedImport
from datetime import date, datetime
from odoo.tools.float_utils import float_is_zero, float_compare


class PurchaseOrder(models.Model):
    _inherit = ['purchase.order']

    cost_ok = fields.Boolean('Cost Approved', readonly=True, copy=False)

    extra_cost_ids = fields.One2many('purchase.extra.cost', 'order_id', 'Extra Costs', ondelete='cascade')
    total_ec_amount_cost = fields.Float(compute='_compute_total_ec_amount', string='Total Extra Cost Amount Cost', store=True, digits=dp.get_precision('Extra Cost Item Amount'))
    total_ec_amount_noncost = fields.Float(compute='_compute_total_ec_amount', string='Total Extra Cost Amount Non Cost', store=True, digits=dp.get_precision('Extra Cost Item Amount'))
    total_ec_amount = fields.Float(compute='_compute_total_ec_amount', string='Total Extra Cost Amount', store=True, digits=dp.get_precision('Extra Cost Item Amount'))
    total_cost = fields.Float(compute='_compute_total_cost', string='Total Cost', digits=0)
    company_currency_id = fields.Many2one('res.currency', related='company_id.currency_id', readonly=True, string='Company Currency')
    extra_cost_status = fields.Selection(
        [('no_cost', 'No Extra Cost Recorded'),
         ('new_cost', 'Extra Cost Recorded'),
         ('cost_calculated', 'Extra Cost Allocated'),
         ('cost_ok', 'Cost Approved')], string='Purchase Extra Cost Status', compute='_compute_ec_state', copy=False, index=True, readonly=True, store=True, track_visibility='onchange',
        help=" * No Extra Cost Recorded: \n"
             " * Extra Cost Recorded: Extra Cost Allocation Pending\n"
             " * Extra Cost Allocated: Cost Approval Pending\n"
             " * Cost Approved")

    @api.depends('extra_cost_ids', 'order_line')
    def _compute_ec_state(self):
        ''' Extra Cost State of a picking depends
         on the state of its related extra cost ids and order line
        '''
        for po in self:
            if po.cost_ok:
                po.extra_cost_status = 'cost_ok'
                if po.extra_cost_ids:
                    raise ValidationError(_("Extra cost cannot be recalculated "
                                            " after cost confirmation."
                                            " Changing Purchase order line items"
                                            " will cause discrepancy with extra"
                                            " cost allocation!"))
            else:
                prev_status = po.extra_cost_status

                if not po.extra_cost_ids:
                    po.extra_cost_status = 'no_cost'
                elif prev_status == 'no_cost':
                    po.extra_cost_status = 'new_cost'
                elif not prev_status:
                    po.extra_cost_status = 'new_cost'

    @api.depends('extra_cost_ids', 'extra_cost_ids.state')
    def _compute_total_ec_amount(self):
        for order in self:
            extra_cost = 0
            extra_cost_noncalc = 0
            # BEGIN: Calculate total price
            for extra in order.extra_cost_ids:
                if not extra.item_id.non_cost_calc and extra.state == 'completed':
                    extra_cost += extra.currency_amount
                else:
                    extra_cost_noncalc += extra.currency_amount
            # END: Calculate total price

            order.total_ec_amount_cost = extra_cost
            order.total_ec_amount_noncost = extra_cost_noncalc
            order.total_ec_amount = extra_cost_noncalc + extra_cost

    @api.depends('extra_cost_ids', 'order_line', 'currency_rate')
    def _compute_total_cost(self):
        for order in self:
            order.total_cost = (order.currency_rate * order.amount_untaxed) + order.total_ec_amount_cost

    @api.multi
    def button_compute_extra_cost(self):
        ''' Тухайн худалдан авалт дээр оруулсан нэмэлт зардлуудыг
            захиалгын мөр бүрт шингээнэ
        '''
        for order in self:
            for cost_line in order.extra_cost_ids:
                #  [Өртөг тооцох] товч дарахад татваргүй дүнг барааны өртөгт нэмж тооцоолно
                if cost_line.currency_id.id != cost_line.env.user.company_id.currency_id.id and cost_line.state == 'draft':
                    if cost_line.taxes_id:
                        amount = cost_line.currency_id.with_context(date=cost_line.date_invoice).compute(cost_line.amount, cost_line.env.user.company_id.currency_id)
                        cost_line.currency_amount = cost_line.taxes_id.with_context(round=False, date=cost_line.date_invoice).compute_all(amount, currency=cost_line.currency_id,  quantity=1.0,  partner=cost_line.partner_id)['total_excluded']
                    else:
                        cost_line.currency_amount = cost_line.currency_id.with_context(date=cost_line.date_invoice).compute(cost_line.amount, cost_line.env.user.company_id.currency_id)
                elif cost_line.state == 'draft':
                    amount = cost_line.currency_id.with_context(date=cost_line.date_invoice).compute(cost_line.amount, cost_line.env.user.company_id.currency_id)
                    cost_line.currency_amount = cost_line.taxes_id.with_context(round=False, date=cost_line.date_invoice).compute_all(amount, currency=cost_line.currency_id, quantity=1.0, partner=cost_line.partner_id)['total_excluded']
                cost_line.calculate_cost(order.order_line)
        if self.extra_cost_status == 'new_cost':
            self.extra_cost_status = 'cost_calculated'
            self.message_post(body=_("The total extra cost " +
                                     "calculated for adjustment."))
        return True

    @api.multi
    def button_confirm_cost(self):
        for pick in self:
            if (pick.extra_cost_status):
                ec_status = pick.extra_cost_status
                if (ec_status == 'new_cost'):
                    # BEGIN: Handle new_cost Extra Cost Status Warning
                    raise ValidationError(_("Please click " +
                                            " [Calculate Cost Adjustment]" +
                                            " button!"))
                    # END: Handle new_cost Extra Cost Status Warning
                if (pick.extra_cost_status == 'no_cost'):
                    # BEGIN: Handle no_cost Extra Cost Status Warning
                    view = pick.env.ref(
                        'l10n_mn_purchase_extra_cost.view_ec_nocost_approval')
                    wiz = pick.env['purchase.nocost.approval'].create({'picking': pick.id})
                    return {
                        'name': _('Extra Cost Has No Records!'),
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'purchase.nocost.approval',
                        'views': [(view.id, 'form')],
                        'view_id': view.id,
                        'target': 'new',
                        'res_id': wiz.id,
                        'context': pick.env.context,
                    }
                    # END: Handle no_cost Extra Cost Status Warning
                if (pick.extra_cost_status == 'cost_calculated'):
                    # BEGIN: Handle Extra Cost Status Approval
                    view = pick.env.ref(
                        'l10n_mn_purchase_extra_cost.view_ec_cost_approval2')
                    wiz = pick.env['purchase.cost.approval'].create({'picking': pick.id})
                    return {
                        'name': _('Extra Cost Approval'),
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'purchase.cost.approval',
                        'views': [(view.id, 'form')],
                        'view_id': view.id,
                        'target': 'new',
                        'res_id': wiz.id,
                        'context': pick.env.context
                    }
                    # END: Handle Extra Cost Status Approval
        self.message_post(body=_('Purchase Cost approved.'))
        return True

    # Дахин тодорхойлов
    @api.depends('state', 'order_line.qty_invoiced', 'order_line.qty_received', 'order_line.product_qty','extra_cost_ids.invoice_id.state')
    def _get_invoiced(self):
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        for order in self:
            extra_invoices = self.env['account.invoice']
            order_invoices = self.env['account.invoice']
            if order.state not in ('purchase', 'done'):
                order.invoice_status = 'no'
                continue

            if any(float_compare(line.qty_invoiced,
                                 line.product_qty if line.product_id.purchase_method == 'purchase' else line.qty_received,
                                 precision_digits=precision) == -1 for line in order.order_line):
                order.invoice_status = 'to invoice'
            elif all(float_compare(line.qty_invoiced,
                                   line.product_qty if line.product_id.purchase_method == 'purchase' else line.qty_received,
                                   precision_digits=precision) >= 0 for line in order.order_line) and order.invoice_ids:
                order.invoice_status = 'invoiced'
            else:
                order.invoice_status = 'no'

            status = order.invoice_status

            # Худалдан авалтын нэхэмжлэлүүдийг олох
            for line in order.order_line:
                order_invoices |= line.invoice_lines.mapped('invoice_id')

            # Худалдан авалтын нэмэлт зардлын нэхэмжлэлүүдийг олох
            if order.extra_cost_ids:
                for extra in order.extra_cost_ids:
                    extra_invoices |= extra.mapped('invoice_id')
                if extra_invoices:
                    if len(extra_invoices) == len(order.extra_cost_ids):
                        count_open_extra = 0
                        count_extra = 0
                        for extra_inv in extra_invoices:
                            count_extra += 1
                            if extra_inv.state in ('open','paid'):
                                count_open_extra += 1
                        if count_open_extra == count_extra:
                            if order_invoices:
                                if order.invoice_status == 'invoiced':
                                    status = 'invoiced'
                                else:
                                    status = 'to invoice'
                            else:
                                status = 'to invoice'
                        else:
                            status = 'to invoice'
                    else:
                        if order.invoice_status == 'invoiced':
                            status = 'to invoice'
                else:
                    if order.invoice_status == 'invoiced':
                        status = 'to invoice'
            order.invoice_status = status

    @api.depends('order_line.invoice_lines.invoice_id.state', 'extra_cost_ids.invoice_id.state')
    def _compute_invoice(self):
        for order in self:
            invoices = self.env['account.invoice']
            for line in order.order_line:
                invoices |= line.invoice_lines.mapped('invoice_id')
            for extra in order.extra_cost_ids:
                invoices |= extra.mapped('invoice_id')
            order.invoice_ids = invoices
            order.invoice_count = len(invoices)

    @api.multi
    def button_cancel(self):
        self.extra_cost_ids.unlink()
        self.write({'cost_ok': False})
        self.extra_cost_status = 'no_cost'
        return super(PurchaseOrder, self).button_cancel()
    
    @api.multi
    def recompute_cost(self):
        self.write({
            'cost_ok': False,
            'extra_cost_status': 'cost_calculated'
        })

    @api.multi
    def write(self, values):
        """
            @Override: Хүргэлт үүссэн худалдан авалт дээр нэмэлт зардал шингээх үед ХА-ын мөрийн өртгийг дахин тооцоолж
                       холбоотой хүргэлт, журналын бичилт, агуулахын хөдөлгөөний өртөг шинэчлэхийн тулд дахин тодорхойлов.
        """
        if 'extra_cost_status' in values.keys() and values['extra_cost_status'] == 'cost_ok':
            # ХА-н мөрүүдэд өртөг тооцох
            self.button_compute_extra_cost()
            
            # ХА-ын бүх мөрийг авах
            recomputable_line_ids = []
            for obj in self:
                recomputable_line_ids.extend([line.id for line in obj.order_line])
                
            # Өртөг шинэчлэх
            self.recompute_purchase_cost(recomputable_line_ids)
            
        return super(PurchaseOrder, self).write(values)
    
    def recompute_purchase_cost(self, recomputable_line_ids):
        """
            @note: Хүргэлт үүссэн худалдан авалтын мөрийн нэгж өртөг өөрчилөгдөхөд холбоотой 
                   журналын бичилт, агуулахын хөдөлгөөний өртөг үнэ өөрчлөх функц.
                   
            @param recomputable_line_ids: Өртөг нь өөрчлөгдсөн худалдан авалтын мөрүүдийн ID.
        """
        
        for id in recomputable_line_ids:
            line = self.env['purchase.order.line'].browse(id)
            move = self.env['stock.move'].search([('purchase_line_id', '=', line.id)])
            
            if move:
                # ХА-с үүссэн хүргэлтүүдийн хөдөлгөөний өртөг засах
                self._cr.execute("""
                    UPDATE stock_move SET price_unit = %s WHERE purchase_line_id = %s 
                """ %(line.cost_unit, line.id))
                
                # Хүргэлттэй холбоотой журналын бичилтийн debit засах
                self._cr.execute("""
                    UPDATE account_move_line aml_for_upd SET debit = %s * sm.product_uom_qty
                    FROM account_move_line aml
                    LEFT JOIN stock_move sm ON sm.id = aml.stock_move_id
                    WHERE aml.id = aml_for_upd.id AND aml.debit > 0 AND sm.purchase_line_id = %s
                """ %(line.cost_unit, line.id))
                
                # Хүргэлттэй холбоотой журналын бичилтийн credit засах
                self._cr.execute("""
                    UPDATE account_move_line aml_for_upd SET credit = %s * sm.product_uom_qty
                    FROM account_move_line aml
                    LEFT JOIN stock_move sm ON sm.id = aml.stock_move_id
                    WHERE aml.id = aml_for_upd.id AND aml.credit > 0 AND sm.purchase_line_id = %s
                """ %(line.cost_unit, line.id))
    
class PurchaseOrderLine(models.Model):
    _inherit = ['purchase.order.line']

    cost_unit = fields.Float('Unit Cost', readonly=True, compute='_compute_cost_unit')
    po_line_cost_ids = fields.One2many('purchase.extra.cost.line', 'purchase_line_id', string = 'Lines Cost')

    @api.multi
    def _compute_cost_unit(self):
        for line in self:
            price_unit = line.price_unit
            # extra cost тооцох үед худалдан авалтын мөрд хөнгөлөлт байвал хөнгөлөттэй үнэ тооцно
            if line.discount and line.discount > 0:
                price_unit = price_unit * (1 - (line.discount / 100))
            cost = line.taxes_id.compute_all(price_unit, line.currency_id, line.product_qty)['total_excluded']
            if line.order_id.currency_id != line.order_id.company_id.currency_id:
                cost = line.order_id.currency_id.with_context(date=line.order_id.currency_cal_date).compute(cost, line.order_id.company_id.currency_id)

                # Хэрэв гараас валютын ханшыг өөрчилсөн бол тухайн өөрчилсөн утгаар нэгж өртгийг бодож гаргав.
                currency_rate = 1
                currency_obj = self.env['res.currency'].search([('name', '=', line.order_id.currency_id.name)])
                date_time = datetime.strptime(line.order_id.currency_cal_date   , '%Y-%m-%d %H:%M:%S')
                day = date_time.replace(hour=0, minute=0, second=0)
                day = str(day)
                if currency_obj:
                    for currency in currency_obj:
                        for rate in currency.rate_ids:
                            if rate.name == day:
                                currency_rate = rate.alter_rate
                if currency_rate != line.order_id.currency_rate:
                    cost = line.order_id.currency_rate * line.taxes_id.compute_all(price_unit, line.currency_id, line.product_qty)['total_excluded']
            if line.product_qty:
                cost /= line.product_qty
            if line.order_id.extra_cost_ids:
                for extra in line.order_id.extra_cost_ids:
                    if not extra.item_id.non_cost_calc and extra.state == 'completed':
                        current_extra_cost_lines = extra.adjustment_lines.search([('purchase_line_id', '=', line.id), ('extra_cost_id', '=', extra.id)])
                        if current_extra_cost_lines:
                            current_extra_cost_line = current_extra_cost_lines[0]
                            curr_cost = current_extra_cost_line.unit_cost
                            if line.order_id.company_id.currency_id.id != extra.currency_id.id:
                                #curr_cost = extra.currency_id.with_context(date=extra.date_invoice).compute(current_extra_cost_line.unit_cost, line.order_id.company_id.currency_id, round=False)
                                curr_cost = current_extra_cost_line.unit_cost*extra.currency_rate
                            cost += curr_cost
            line.cost_unit = cost

    @api.multi
    def _get_price_unit_with_calculated_extra_cost(self):
        self.ensure_one()
        return self.cost_unit
