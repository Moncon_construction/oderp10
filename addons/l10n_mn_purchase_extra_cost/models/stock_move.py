# -*- coding: utf-8 -*-

from odoo import api, models


class StockMove(models.Model):
    _inherit = 'stock.move'

    @api.multi
    def get_price_unit(self):
        if self.purchase_line_id:
            if self.purchase_line_id.order_id.cost_ok:
                price_unit = self.purchase_line_id.sudo()._get_price_unit_with_calculated_extra_cost()
                self.write({'price_unit': price_unit})
                return self.price_unit
        return super(StockMove, self).get_price_unit()
