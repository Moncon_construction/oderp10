# -*- coding: utf-8 -*-

from odoo import api, models
from odoo.exceptions import ValidationError
from odoo.tools.translate import _


class Picking(models.Model):
    _inherit = 'stock.picking'

    @api.multi
    def do_new_transfer(self):
        for pick in self:
            if pick.picking_type_id.code == 'incoming' and pick.purchase_id and pick.purchase_id.extra_cost_ids and not pick.purchase_id.cost_ok:
                raise ValidationError(_("You cannot process incoming shipment until purchase order cost approved!"))
        return super(Picking, self).do_new_transfer()
