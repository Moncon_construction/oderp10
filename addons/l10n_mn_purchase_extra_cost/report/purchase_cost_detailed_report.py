# -*- encoding: utf-8 -*-

from xlsxwriter.utility import xl_rowcol_to_cell

from odoo import models, _


class PurchaseCostDetailedReport(models.TransientModel):
    """Худалдан Авалтын Өртгийн Дэлгэрэнгүй Тайлан"""
    _inherit = "purchase.detailed.report"

    def get_header(self, sheet, colx, rowx, format_title, num_of_extra_cost):
        sheet, colx, num_of_extra_cost = super(PurchaseCostDetailedReport, self).get_header(sheet, colx, rowx, format_title, num_of_extra_cost)
        extra_costs = self.env['extra.cost.item'].search([], order='id')
        for obj in extra_costs:
            sheet.set_column(colx, colx + 1, 6)
            sheet.set_column(colx + 2, colx + 3, 9)
            sheet.merge_range(rowx, colx, rowx, colx + 3, '%s' % obj.name, format_title)
            sheet.write(rowx + 1, colx, _('Currency'), format_title)
            sheet.write(rowx + 1, colx + 1, _('Rate'), format_title)
            sheet.write(rowx + 1, colx + 2, _('Unit cost (Currency)'), format_title)
            sheet.write(rowx + 1, colx + 3, _('Total cost (Currency)'), format_title)
            num_of_extra_cost += 1
            colx += 4
        sheet.set_column(colx, colx + 3, 9)
        sheet.merge_range(rowx, colx, rowx + 1, colx, _('Total Extra Cost Amount Cost'), format_title)
        colx += 1
        sheet.merge_range(rowx, colx, rowx + 1, colx, _('Total Extra Cost Amount Non Cost'), format_title)
        colx += 1
        sheet.merge_range(rowx, colx, rowx + 1, colx, _('Total Extra Cost Amount'), format_title)
        colx += 1
        return sheet, colx, num_of_extra_cost

    def get_value(self, sheet, order_line, rowx, purchase, index, format_content_text, format_content_float, format_content_float_amount):

        sheet = super(PurchaseCostDetailedReport, self).get_value(sheet, order_line, rowx, purchase, index, format_content_text, format_content_float, format_content_float_amount)
        extra_costs = self.env['extra.cost.item'].search([], order='id')
        total_cost, total_non_cost = 0, 0
        colx = 9
        for obj in extra_costs:
            cost_line = self.env['purchase.extra.cost.line'].search([('purchase_line_id', '=', order_line.id), ('extra_cost_id.item_id', '=', obj.id)])
            unit_cost = 0
            total_cost_amount = 0
            for cost_id in cost_line:
                unit_cost += cost_id.unit_cost * cost_id.extra_cost_id.currency_rate
                total_cost_amount += cost_id.total_cost * cost_id.extra_cost_id.currency_rate

            line = cost_line[0] if cost_line and len(cost_line) > 0 else cost_line
            extra_cost = line.extra_cost_id

            if cost_line:
                sheet.write(rowx, colx, extra_cost.currency_id.name, format_content_text)
                sheet.write(rowx, colx + 1, extra_cost.currency_rate, format_content_float)
                sheet.write(rowx, colx + 2, unit_cost, format_content_float)
                sheet.write(rowx, colx + 3, total_cost_amount, format_content_float)
            else:
                sheet.write(rowx, colx, '', format_content_text)
                sheet.write(rowx, colx + 1, 0, format_content_float)
                sheet.write(rowx, colx + 2, 0, format_content_float)
                sheet.write(rowx, colx + 3, 0, format_content_float)
                
            if not obj.non_cost_calc:
                total_cost += total_cost_amount
            else:
                total_non_cost += total_cost_amount
                
            colx += 4
        sheet.write(rowx, colx, total_cost, format_content_float)
        sheet.write(rowx, colx + 1, total_non_cost, format_content_float)
        sheet.write_formula(rowx, colx + 2, '{=SUM(' + xl_rowcol_to_cell(rowx, colx) + '+' + xl_rowcol_to_cell(rowx, colx + 1) + ')}', format_content_float)

        return sheet

    def get_footer(self, sheet, rowx, format_group_right, format_content_float_amount, order_amount_rows, num, num_of_extra_cost):

        sheet, order_amount_rows = super(PurchaseCostDetailedReport, self).get_footer(sheet, rowx, format_group_right, format_content_float_amount, order_amount_rows, num, num_of_extra_cost)
        colx = 9
        
        for i in range(num_of_extra_cost):  # @UnusedVariable
            sheet.write(rowx, colx, '', format_group_right)
            sheet.write(rowx, colx + 1, '', format_group_right)
            sheet.write_formula(rowx, colx + 2, '{=SUM(' + xl_rowcol_to_cell(rowx - num, colx + 2) + ':' + xl_rowcol_to_cell(rowx - 1, colx + 2) + ')}', format_content_float_amount)
            sheet.write_formula(rowx, colx + 3, '{=SUM(' + xl_rowcol_to_cell(rowx - num, colx + 3) + ':' + xl_rowcol_to_cell(rowx - 1, colx + 3) + ')}', format_content_float_amount)
            colx += 4
            
        sheet.write_formula(rowx, colx, '{=SUM(' + xl_rowcol_to_cell(rowx - num, colx) + ':' + xl_rowcol_to_cell(rowx - 1, colx) + ')}', format_content_float_amount)
        colx += 1
        sheet.write_formula(rowx, colx, '{=SUM(' + xl_rowcol_to_cell(rowx - num, colx) + ':' + xl_rowcol_to_cell(rowx - 1, colx) + ')}', format_content_float_amount)
        colx += 1
        sheet.write_formula(rowx, colx, '{=SUM(' + xl_rowcol_to_cell(rowx - num, colx) + ':' + xl_rowcol_to_cell(rowx - 1, colx) + ')}', format_content_float_amount)

        return sheet, order_amount_rows

    def get_footer1(self, sheet, rowx, order_amount_rows, format_title_right, num_of_extra_cost):
        sheet, order_amount_rows = super(PurchaseCostDetailedReport, self).get_footer1(sheet, rowx, order_amount_rows, format_title_right, num_of_extra_cost)
        
        colx = 9
        for i in range(num_of_extra_cost):  # @UnusedVariable
            sheet.write(rowx, colx, '', format_title_right)
            sheet.write(rowx, colx + 1, '', format_title_right)
            rowcol = self.row_column(order_amount_rows, colx + 2)
            sheet.write_formula(rowx, colx + 2, '{=SUM(%s0)}' % rowcol, format_title_right)
            rowcol = self.row_column(order_amount_rows, colx + 3)
            sheet.write_formula(rowx, colx + 3, '{=SUM(%s0)}' % rowcol, format_title_right)
            colx += 4
            
        rowcol = self.row_column(order_amount_rows, colx)
        sheet.write_formula(rowx, colx, '{=SUM(%s0)}' % rowcol, format_title_right)
        
        colx += 1
        rowcol = self.row_column(order_amount_rows, colx)
        sheet.write_formula(rowx, colx, '{=SUM(%s0)}' % rowcol, format_title_right)
        
        colx += 1
        rowcol = self.row_column(order_amount_rows, colx)
        sheet.write_formula(rowx, colx, '{=SUM(%s0)}' % rowcol, format_title_right)
        
        return sheet, order_amount_rows
