# -*- encoding: utf-8 -*-
##############################################################################
import calendar
from io import BytesIO
import time
from odoo.exceptions import UserError
from datetime import date, timedelta
from operator import itemgetter
import datetime
from odoo import api, fields, models, _
from xlsxwriter.utility import xl_rowcol_to_cell

class PurchaseDetailedDisplayReport(models.Model):
    """
        Худалдан авалтын дэлгэрэнгүй тайлан /дэлгэцээр/
    """
    _inherit = "purchase.detailed.report.display"

    @api.multi
    def create_lines(self, data, line):
        extra_costs = self.env['extra.cost.item'].search([], order='id')
        total_cost, total_non_cost = 0, 0
        for obj in extra_costs:
            cost_line = self.env['purchase.extra.cost.line'].search(
                [('purchase_line_id', '=', line['line_id']), ('extra_cost_id.item_id', '=', obj.id)])
            cost_line = cost_line[0] if cost_line and len(cost_line) > 0 else cost_line
            extra_cost = cost_line.extra_cost_id
            cost_amount = 0
            if cost_line:
                cost_amount = cost_line.total_cost * extra_cost.currency_rate
            if not obj.non_cost_calc:
                total_cost += cost_amount
            else:
                total_non_cost += cost_amount
        data[0].update({
            'amount_of_total_extra_costs_included': total_cost,
            'amount_of_total_extra_costs_not_included': total_non_cost,
            'amount_of_total_extra_costs_sum': total_cost + total_non_cost,
        })
        return super(PurchaseDetailedDisplayReport, self).create_lines(data, line)

    def get_header(self, sheet, colx, rowx, format_title, num_of_extra_cost):
        sheet, colx, num_of_extra_cost = super(PurchaseDetailedDisplayReport, self).get_header(sheet, colx, rowx, format_title, num_of_extra_cost)
        extra_costs = self.env['extra.cost.item'].search([], order='id')
        for obj in extra_costs:
            sheet.set_column(colx, colx + 1, 9)
            sheet.set_column(colx + 2, colx + 3, 12)
            sheet.merge_range(rowx, colx, rowx, colx + 3, '%s' % obj.name, format_title)
            sheet.write(rowx + 1, colx, _('Currency (Display Report Extra)'), format_title)
            sheet.write(rowx + 1, colx + 1, _('Rate (Display Report Extra)'), format_title)
            sheet.write(rowx + 1, colx + 2, _('Unit cost (Currency Display Report Extra)'), format_title)
            sheet.write(rowx + 1, colx + 3, _('Total cost (Currency Display Report Extra)'), format_title)
            num_of_extra_cost += 1
            colx += 4
        sheet.set_row(rowx + 1, 30)
        sheet.set_column(colx, colx + 3, 15)
        sheet.merge_range(rowx, colx, rowx + 1, colx, _('Total Extra Cost Amount Cost (Display Report Extra)'), format_title)
        colx += 1
        sheet.merge_range(rowx, colx, rowx + 1, colx, _('Total Extra Cost Amount Non Cost (Display Report Extra)'), format_title)
        colx += 1
        sheet.merge_range(rowx, colx, rowx + 1, colx, _('Total Extra Cost Amount (Display Report Extra)'), format_title)
        colx += 1
        return sheet, colx, num_of_extra_cost

    def get_value(self, sheet, order_line, purchase, rowx, format_content_text, format_content_float):

        sheet = super(PurchaseDetailedDisplayReport, self).get_value(sheet, order_line, purchase, rowx,
                                                                  format_content_text, format_content_float)
        extra_costs = self.env['extra.cost.item'].search([], order='id')
        total_cost, total_non_cost = 0, 0
        is_shipment_model_exist = self.env.get('purchase.shipment')
        if is_shipment_model_exist is not None:
            colx = 13
        else:
            colx = 12
        for obj in extra_costs:
            cost_line = self.env['purchase.extra.cost.line'].search(
                [('purchase_line_id', '=', order_line.id), ('extra_cost_id.item_id', '=', obj.id)])
            cost_line = cost_line[0] if cost_line and len(cost_line) > 0 else cost_line
            extra_cost = cost_line.extra_cost_id
            cost_amount = 0

            if cost_line:
                cost_amount = cost_line.total_cost * extra_cost.currency_rate
                sheet.write(rowx, colx, extra_cost.currency_id.name, format_content_text)
                sheet.write(rowx, colx + 1, extra_cost.currency_rate, format_content_float)
                sheet.write(rowx, colx + 2, cost_line.unit_cost * extra_cost.currency_rate, format_content_float)
                sheet.write(rowx, colx + 3, cost_amount, format_content_float)
            else:
                sheet.write(rowx, colx, '', format_content_text)
                sheet.write(rowx, colx + 1, 0, format_content_float)
                sheet.write(rowx, colx + 2, 0, format_content_float)
                sheet.write(rowx, colx + 3, 0, format_content_float)

            if not obj.non_cost_calc:
                total_cost += cost_amount
            else:
                total_non_cost += cost_amount

            colx += 4
        sheet.write(rowx, colx, total_cost, format_content_float)
        sheet.write(rowx, colx + 1, total_non_cost, format_content_float)
        sheet.write_formula(rowx, colx + 2,
                            '{=SUM(' + xl_rowcol_to_cell(rowx, colx) + '+' + xl_rowcol_to_cell(rowx, colx + 1) + ')}',
                            format_content_float)
        return sheet

    def get_footer(self, sheet, rowx, start_row,format_group_right,format_group_float, num_of_extra_cost):
        sheet, rowx = super(PurchaseDetailedDisplayReport, self).get_footer(sheet, rowx, start_row,format_group_right,format_group_float,num_of_extra_cost)
        is_shipment_model_exist = self.env.get('purchase.shipment')
        if is_shipment_model_exist is not None:
            colx = 13
        else:
            colx = 12

        for i in range(num_of_extra_cost):  # @UnusedVariable
            sheet.write(rowx, colx, '', format_group_right)
            sheet.write(rowx, colx + 1, '', format_group_right)
            sheet.write(rowx, colx + 2, '', format_group_right)
            sheet.write_formula(rowx, colx +3, '{=SUM(' + xl_rowcol_to_cell(start_row, colx+3) + ':' + xl_rowcol_to_cell(rowx - 1, colx+3) + ')}',
                                format_group_float)
            colx += 4

        sheet.write_formula(rowx, colx,'{=SUM(' + xl_rowcol_to_cell(start_row, colx) + ':' + xl_rowcol_to_cell(rowx - 1, colx) + ')}',
                            format_group_float)

        colx += 1
        sheet.write_formula(rowx, colx,'{=SUM(' + xl_rowcol_to_cell(start_row, colx) + ':' + xl_rowcol_to_cell(rowx - 1, colx) + ')}',
                            format_group_float)

        colx += 1
        sheet.write_formula(rowx, colx,'{=SUM(' + xl_rowcol_to_cell(start_row, colx) + ':' + xl_rowcol_to_cell(rowx - 1, colx) + ')}',
                            format_group_float)

        return sheet, rowx

    def get_footer1(self, sheet, rowx, order_amount_rows, format_group_right,format_group_float, num_of_extra_cost):
        sheet, order_amount_rows = super(PurchaseDetailedDisplayReport, self).get_footer1(sheet, rowx, order_amount_rows, format_group_right,format_group_float, num_of_extra_cost)

        is_shipment_model_exist = self.env.get('purchase.shipment')
        if is_shipment_model_exist is not None:
            colx = 13
        else:
            colx = 12

        for i in range(num_of_extra_cost):  # @UnusedVariable
            sheet.write(rowx, colx, '', format_group_right)
            sheet.write(rowx, colx + 1, '', format_group_right)
            sheet.write(rowx, colx + 2, '', format_group_right)
            rowcol = self.row_column(order_amount_rows, colx + 3)
            sheet.write_formula(rowx, colx + 3, '{=SUM(%s0)}' % rowcol, format_group_float)
            colx += 4

        rowcol = self.row_column(order_amount_rows, colx)
        sheet.write_formula(rowx, colx, '{=SUM(%s0)}' % rowcol, format_group_float)

        colx += 1
        rowcol = self.row_column(order_amount_rows, colx)
        sheet.write_formula(rowx, colx, '{=SUM(%s0)}' % rowcol, format_group_float)

        colx += 1
        rowcol = self.row_column(order_amount_rows, colx)
        sheet.write_formula(rowx, colx, '{=SUM(%s0)}' % rowcol, format_group_float)

        return sheet, order_amount_rows

    def get_header_sub(self,sheet, rowx, rows, order_amount_rows, format_group_right, format_group_float, num_of_extra_cost, num):
        sheet, order_amount_rows = super(PurchaseDetailedDisplayReport, self).get_header_sub(sheet, rowx, rows, order_amount_rows,format_group_right, format_group_float,num_of_extra_cost, num)
        is_shipment_model_exist = self.env.get('purchase.shipment')
        if is_shipment_model_exist is not None:
            coly = 13
        else:
            coly = 12
        for i in range(num_of_extra_cost):  # @UnusedVariable
            sheet.write_formula(rows-1, coly +3, '{=SUM(' + xl_rowcol_to_cell(rows, coly+3) + ':' + xl_rowcol_to_cell(rowx - 1, coly+3) + ')}',
                                format_group_float)
            coly += 4

        sheet.write_formula(rows-1, coly,'{=SUM(' + xl_rowcol_to_cell(rows, coly) + ':' + xl_rowcol_to_cell(rowx - 1, coly) + ')}',
                            format_group_float)

        coly += 1
        sheet.write_formula(rows-1, coly,'{=SUM(' + xl_rowcol_to_cell(rows, coly) + ':' + xl_rowcol_to_cell(rowx - 1, coly) + ')}',
                            format_group_float)

        coly += 1
        sheet.write_formula(rows-1, coly,'{=SUM(' + xl_rowcol_to_cell(rows, coly) + ':' + xl_rowcol_to_cell(rowx - 1, coly) + ')}',
                            format_group_float)

        return sheet, order_amount_rows


class PurchaseDetailedDisplayReportLine(models.Model):
    """
        Худалдан авалтын дэлгэрэнгүй тайлан /дэлгэцээр/
    """
    _inherit = "purchase.detailed.display.report.line"

    amount_of_total_extra_costs_included = fields.Float(string='Amount of total extra costs included')
    amount_of_total_extra_costs_not_included = fields.Float(string='Amount of total extra costs not included')
    amount_of_total_extra_costs_sum = fields.Float(string='Amount of total extra costs sum')


