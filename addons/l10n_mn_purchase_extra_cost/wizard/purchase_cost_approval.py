# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _


class PurchaseCostApproval(models.TransientModel):
    _name = 'purchase.cost.approval'
    _description = 'Extra Cost Approval'

    picking = fields.Many2one('purchase.order')

    @api.model
    def default_get(self, fields):
        res = super(PurchaseCostApproval, self).default_get(fields)
        if not res.get('picking') and self._context.get('active_id'):
            res['picking'] = self._context['active_id']
        return res

    @api.multi
    def process(self):
        for obj in self:
            for line in obj.picking.order_line:
                stock_move = self.env['stock.move'].search([('purchase_line_id', '=', line.id)])
                
                line_ids = []
                for move in stock_move:
                    move.accounts_dict = None
                    #Нэмэлт зардлуудын данс, дүнг stock.move руу хадгалах
                    accounts_dic = {}
                    for line_cost in line.po_line_cost_ids:
                        if line_cost.extra_cost_id.account_id.id not in accounts_dic.keys():
                            accounts_dic[line_cost.extra_cost_id.account_id.id] = line_cost.unit_cost*line_cost.extra_cost_id.currency_rate
                        else:
                            accounts_dic[line_cost.extra_cost_id.account_id.id] += line_cost.unit_cost*line_cost.extra_cost_id.currency_rate
                    
                    for account_id, amount in accounts_dic.iteritems():
                        line_ids.append((0,0,{'stock_move_id': move.id, 'account_id': account_id, 'amount': amount}))
                    
                    #Барааны ЗЯБ данс, дүнг stock.move руу нэмж хадгалах
                    
                    if line.is_module_installed('l10n_mn_stock_account_cost_for_each_wh'):
                        if line.order_id.picking_type_id.warehouse_id.stock_account_input_id.id:
                            line_ids.append((0,0,{'stock_move_id': move.id, 'account_id': line.order_id.picking_type_id.warehouse_id.stock_account_input_id.id, 'amount': line.price_unit_mnt})) 
                        else:
                            raise ValidationError(_("Select stock input account of the warehouse!"))
                    else:
                        if line.product_id.categ_id.property_stock_account_input_categ_id.id:
                            line_ids.append((0,0,{'stock_move_id': move.id, 'account_id': line.product_id.categ_id.property_stock_account_input_categ_id.id, 'amount': line.price_unit_mnt}))
                        else:
                            raise ValidationError(_("Select stock input account of the product category!"))
                    move.accounts_dict = line_ids
            
            obj.picking.cost_ok = True
            obj.picking.extra_cost_status = 'cost_ok'
            obj.picking.message_post(body=_('Purchase Cost approved.'))
