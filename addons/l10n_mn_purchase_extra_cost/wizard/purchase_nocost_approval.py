# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _


class PurchaseNoCostApproval(models.TransientModel):
    _name = 'purchase.nocost.approval'
    _description = 'No Cost Approval'

    picking = fields.Many2one('purchase.order')

    @api.model
    def default_get(self, fields):
        res = super(PurchaseNoCostApproval, self).default_get(fields)
        if not res.get('picking') and self._context.get('active_id'):
            res['picking'] = self._context['active_id']
        return res

    @api.multi
    def process(self):
        for obj in self:
            obj.picking.cost_ok = True
            obj.picking.extra_cost_status = 'cost_ok'
            obj.picking.message_post(body=_('Purchase Cost'
                                            ' approved with'
                                            ' no extra cost record.'))
