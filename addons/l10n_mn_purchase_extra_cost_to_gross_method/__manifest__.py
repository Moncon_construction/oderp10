# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Linker Module For Purchase Extra Cost & Gross Method",
    'version': '1.0',
    'depends': [
        'l10n_mn_purchase_extra_cost',
        'l10n_mn_purchase_gross_method',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
        Purchase Additional Features
    """,
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}
