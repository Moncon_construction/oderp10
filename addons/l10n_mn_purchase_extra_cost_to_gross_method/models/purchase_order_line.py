# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError

class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'
    
    @api.multi
    def _get_price_unit_with_calculated_extra_cost(self):
        """ created For the use of Purchase order line
        model within the purchase extra
        cost module 2017.10.27"""
        self.ensure_one()
        line = self[0]
        line.order_id.button_compute_extra_cost()

        """ Begin: Field Cost Unit Previously
        used by OdERP(mn) was deprecated and
        replaced by this calculation section """
        cost = line.taxes_id.compute_all(line.price_unit,
                                         line.currency_id,
                                         line.product_qty)['total_excluded']
        if line.order_id.currency_id != line.order_id.company_id.currency_id:
            cost = line.order_id.currency_id.with_context(date=line.order_id.currency_cal_date).compute(cost, line.order_id.company_id.currency_id)
        if line.product_qty:
            cost /= line.product_qty
        if line.order_id.extra_cost_ids:
            for extra in line.order_id.extra_cost_ids:
                current_extra_cost_line = extra.adjustment_lines.search(
                    [('purchase_line_id', '=', line.id),
                     ('extra_cost_id', '=', extra.id)])[0]
                if not extra.item_id.non_cost_calc:
                    curr_cost = current_extra_cost_line.unit_cost
                    if line.order_id.company_id.currency_id.id != extra.currency_id.id:
                        curr_cost = extra.currency_id.with_context(date=extra.date_invoice).compute(current_extra_cost_line.unit_cost, line.order_id.company_id.currency_id, round=False)
                    cost += curr_cost
        price_unit = cost
        
        ############### START - Цэвэр дүнгийн аргаар хөнгөлөлт тооцох бол агуулахын журналын мөрийн дүнг хөнгөлөлт тооцсон дүнгээс авна ####################
        has_discount = False
        for line in self:
            if line.discount > 0:
                has_discount = True
                break
            
        if not self.order_id.company_id.purchase_amount_method or self.order_id.company_id.purchase_amount_method != 0 or not has_discount:
            return price_unit
        else:
            return self.get_price_unit_with_discount(price_unit)
        ############### END - Цэвэр дүнгийн аргаар хөнгөлөлт тооцох бол агуулахын журналын мөрийн дүнг хөнгөлөлт тооцсон дүнгээс авна ####################
        
        return price_unit
        """ End: """
        
