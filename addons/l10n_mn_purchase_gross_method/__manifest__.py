# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Purchase - Gross Method",
    'version': '1.0',
    'depends': [
        'l10n_mn_purchase',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
        Purchase Additional Features
    """,
    'data': [
            'views/stock_warehouse_views.xml',
            'views/product_category_views.xml',
            'views/account_invoice_views.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
