# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError

class ProductCategory(models.Model):
    _inherit = "product.category"

    @api.multi
    def compute_cost_config(self):
        self._cr.execute(
            "SELECT name as name, state as state FROM ir_module_module "
            "WHERE name = 'l10n_mn_stock_account_cost_for_each_wh' AND state = 'uninstalled' ")
        config_cost_each_wh = self._cr.fetchall()

        if config_cost_each_wh:
            for obj in self:
                obj.is_checked_product_each_warehouse = True

    is_checked_product_each_warehouse = fields.Boolean('Is Cost Checked', compute='compute_cost_config')
    purchase_discount_account = fields.Many2one('account.account')
