# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError

class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'
    
    @api.depends('product_qty', 'price_unit', 'taxes_id','discount')
    def _compute_amount(self):
        for line in self:
            taxes = line.taxes_id.compute_all(line.price_unit * (1 - (line.discount or 0.0) / 100.0), line.order_id.currency_id, line.product_qty, product=line.product_id, partner=line.order_id.partner_id)
            line.update({
                'price_tax': taxes['total_included'] - taxes['total_excluded'],
                'price_total': taxes['total_included'] ,
                'price_subtotal': taxes['total_excluded'],
            })
            
    @api.multi
    def get_price_unit_with_discount(self, price_unit):
        # Хөнгөлөлттэй үед худалдан авалтаас хүргэлт үүсгэхэд ашиглах нэгжийн үнийг буцаах функц
        # Хэрвээ цэвэр дүнгийн аргаар худалдан авалт тооцох бол агуулахын журналын мөрийн дүнг хөнгөлөлт тооцсон дүнгээс авна.
        self.ensure_one()
        
        line = self[0]
        order = line.order_id
        price_unit = price_unit or line.price_unit
     
        if self.order_id.company_id.purchase_amount_method == 0:
            discount_price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            if line.taxes_id:
                price_unit = line.taxes_id.with_context(round=False).compute_all(
                    discount_price, currency=line.order_id.currency_id, quantity=1.0, product=line.product_id, partner=line.order_id.partner_id
                )['total_excluded']
            if line.product_uom.id != line.product_id.uom_id.id:
                price_unit *= line.product_uom.factor / line.product_id.uom_id.factor
            if order.currency_id != order.company_id.currency_id:
                price_unit = order.currency_id.compute(price_unit, order.company_id.currency_id, round=False)
        
        return price_unit
        
    @api.multi
    def _get_stock_move_price_unit(self):
        # Дахин тодорхойлсон: Хөнгөлөлттэй үед худалдан авалтаас хүргэлт үүсгэхэд ашиглах нэгжийн үнийг буцаах функц
        # Хэрвээ цэвэр дүнгийн аргаар худалдан авалт тооцох бол агуулахын журналын мөрийн дүнг хөнгөлөлт тооцсон дүнгээс авна.
         
        self.ensure_one()
 
        # ХА-ын мөрд хөнгөлөлт байгаа эсэхийг шалгана
        has_discount = False
        for line in self:
            if line.discount > 0:
                has_discount = True
                break
            
        if not self.order_id.company_id.purchase_amount_method or self.order_id.company_id.purchase_amount_method != 0 or not has_discount:
            return super(PurchaseOrderLine, self)._get_stock_move_price_unit()
        elif has_discount and self.order_id.company_id.purchase_amount_method == 0:
            line = self[0]
            order = line.order_id
            price_unit = line.price_unit
            return self.get_price_unit_with_discount(price_unit)
         
        