# -*- coding: utf-8 -*-

from odoo import api, models

class StockMove(models.Model):
    _inherit = 'stock.move'

    @api.multi
    def get_price_unit(self):
        # @Override: Хөнгөлөлттэй үед худалдан авалтаас хүргэлт үүсгэхэд ашиглах нэгжийн үнийг буцаах функц
        # Хэрвээ цэвэр дүнгийн аргаар худалдан авалт тооцох бол агуулахын журналын мөрийн дүнг хөнгөлөлт тооцсон дүнгээс авна.
        
        """ Returns the unit price to store on the quant """
        if self.purchase_line_id:
            order = self.purchase_line_id.order_id
            #if the currency of the PO is different than the company one, the price_unit on the move must be reevaluated
            #(was created at the rate of the PO confirmation, but must be valuated at the rate of stock move execution)
            if order.currency_id != self.company_id.currency_id:
                #we don't pass the move.date in the compute() for the currency rate on purpose because
                # 1) get_price_unit() is supposed to be called only through move.action_done(),
                # 2) the move hasn't yet the correct date (currently it is the expected date, after
                #    completion of action_done() it will be now() )
                price_unit = self.purchase_line_id._get_stock_move_price_unit()
                self.write({'price_unit': price_unit})
                return price_unit
            
            ################### BEGIN: Цэвэр дүнгийн аргаар хөнгөлөлт тооцох бол нэгжийн үнийг хөнгөлөгдсөн дүнгээс авна  ######################
            line = self.purchase_line_id
            has_discount = True if line.discount else False
            
            if not order.company_id.purchase_amount_method or order.company_id.purchase_amount_method != 0 or not has_discount:
                return super(StockMove, self).get_price_unit()
            elif has_discount and order.company_id.purchase_amount_method == 0: 
                discount_price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
                price_unit = line.taxes_id.with_context(round=False).compute_all(
                    discount_price, currency=line.order_id.currency_id, quantity=1.0, product=line.product_id, partner=line.order_id.partner_id
                )['total_excluded']
                if line.product_uom and line.product_id.uom_id and line.product_uom.id != line.product_id.uom_id.id:
                    price_unit *= line.product_uom.factor / line.product_id.uom_id.factor
                return price_unit
            ################### END: Цэвэр дүнгийн аргаар хөнгөлөлт тооцох бол нэгжийн үнийг хөнгөлөгдсөн дүнгээс авна  ######################
        return super(StockMove, self).get_price_unit()
