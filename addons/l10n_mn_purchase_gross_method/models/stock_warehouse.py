# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api

class StockWarehouse(models.Model):
    _inherit = 'stock.warehouse'

    def compute_cost_config(self):
        self._cr.execute(
            "SELECT name as name, state as state FROM ir_module_module "
            "WHERE name = 'l10n_mn_stock_account_cost_for_each_wh' AND state ='installed'")
        config_cost_each_wh = self._cr.fetchall()
        if config_cost_each_wh:
            for obj in self:
                obj.is_checked_product_each_company = True

    is_checked_product_each_company = fields.Boolean('Is Cost Checked', default=False, compute='compute_cost_config')
    purchase_discount_account = fields.Many2one('account.account')
