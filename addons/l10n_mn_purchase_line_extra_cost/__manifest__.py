# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

{
    'name': "Mongolian Purchase Line Extra Cost",
    'version': '1.0',
    'depends': [
        'l10n_mn_purchase_extra_cost',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
        Purchase Additional Features
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/purchase_order_views.xml',
        'views/stock_picking_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
