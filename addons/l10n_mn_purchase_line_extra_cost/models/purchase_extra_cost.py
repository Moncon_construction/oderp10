# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.tools.translate import _
from odoo.addons.l10n_mn_extra_cost_item.models.extra_cost_item import SPLIT_METHOD_EC as ec_item  # @UnresolvedImport
import time
from odoo.exceptions import ValidationError


class PurchaseOrder(models.Model):
    _inherit = ['purchase.order']

    @api.depends('extra_cost_ids', 'order_line')
    def _compute_ec_state(self):
        return True


class StockMove(models.Model):
    _inherit = ['stock.move']

    cost_ok = fields.Boolean('Cost Approved', readonly=True, copy=False)


class PurchaseExtraCost(models.Model):
    _inherit = 'purchase.extra.cost'

    @api.multi
    def _get_default_po_lines(self):
        line_ids = []
        if 'default_order_id' in self._context:
            if self.env['purchase.order'].browse(self._context['default_order_id']):
                purchase = self.env['purchase.order'].browse(self._context['default_order_id'])[0]
                for line in purchase.order_line:
                    if not line.cost_ok:
                        line_ids.append((0,0,{'order_id': purchase.id,'purchase_line_id':line.id, 'product_qty':line.product_qty, 'extra_cost_id': self.id}))
        return line_ids
    
    po_lines = fields.One2many('purchase.lines.adding.extra.cost', 'extra_cost_id', 'PO Lines Adding Extra Cost', default=_get_default_po_lines)