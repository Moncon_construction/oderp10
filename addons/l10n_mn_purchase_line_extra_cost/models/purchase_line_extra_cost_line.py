# -*- coding: utf-8 -*-

from odoo import api, fields, models
import odoo.addons.decimal_precision as dp  # @UnresolvedImport

class PurchaseLinesAddingExtraCost(models.Model):
    _name = 'purchase.lines.adding.extra.cost'
    _description = 'Purchase Lines Adding Extra Cost'

    extra_cost_id = fields.Many2one('purchase.extra.cost', 'Purchase Extra Cost', required=True, ondelete='cascade')
    order_id = fields.Many2one('purchase.order', 'Purchase Order')
    purchase_line_id = fields.Many2one('purchase.order.line', 'Purchase Line', required=True, domain="[('cost_ok', '=', False),('order_id','=',order_id)]")
    product_qty = fields.Float('Quantity', digits=dp.get_precision('Extra Cost Item Amount'))
    
    @api.multi
    @api.onchange('purchase_line_id')
    def onchange_item_id(self):
        for this in self:
            this.product_qty = this.purchase_line_id.product_qty
