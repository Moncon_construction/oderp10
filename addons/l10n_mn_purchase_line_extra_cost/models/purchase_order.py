# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from odoo.tools.float_utils import float_compare


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    @api.multi
    def _compute_lines_cost_approval(self):
        for record in self:
            if record.order_line.filtered(lambda l: not l.cost_ok).ids:
                record.all_lines_cost_approved = False
            else:
                record.all_lines_cost_approved = True

    all_lines_cost_approved = fields.Boolean(compute='_compute_lines_cost_approval')

    @api.multi
    def button_confirm_cost(self):
        for record in self:
            if record.state != 'purchase':
                raise ValidationError(_("Please confirm purchase order first!"))
            else:
                for line in record.order_line:
                    if not line.cost_ok:
                        line.button_approve_line_cost()

    def split_purchase_line(self, cost_line):
        stock_move_obj = self.env['stock.move']
        for line in cost_line.po_lines:
            stock_move_id = stock_move_obj.search([('purchase_line_id', '=', line.purchase_line_id.id)])
            rounding = line.purchase_line_id.product_id.uom_id.rounding
            if float_compare(line.purchase_line_id.product_qty, line.product_qty, precision_rounding=rounding) != 0:
                new_line_id = line.purchase_line_id.copy({'product_qty': line.purchase_line_id.product_qty - line.product_qty})
                line.purchase_line_id.write({'product_qty': line.product_qty})
                if stock_move_id:
                    stock_move = stock_move_id[0]
                    new_stock_move_id = stock_move.copy({'product_uom_qty': stock_move.product_qty - line.product_qty})
                    stock_move.write({'product_uom_qty': line.product_qty})
        return True

    @api.multi
    def button_compute_extra_cost(self):
        ''' Тухайн худалдан авалт дээр оруулсан нэмэлт зардлуудыг
            захиалгын мөр бүрт шингээнэ
        '''
        for order in self:
            costed_line_ids = []
            costed_order_ids = []
            for cost_line in order.extra_cost_ids:
                self.split_purchase_line(cost_line)
                #  [Өртөг тооцох] товч дарахад татваргүй дүнг барааны өртөгт нэмж тооцоолно
                if cost_line.currency_id.id != cost_line.env.user.company_id.currency_id.id and cost_line.state == 'draft':
                    if cost_line.taxes_id:
                        amount = cost_line.currency_id.with_context(date=cost_line.date_invoice).compute(cost_line.amount, cost_line.env.user.company_id.currency_id)
                        cost_line.currency_amount = cost_line.taxes_id.with_context(round=False, date=cost_line.date_invoice).compute_all(amount, currency=cost_line.currency_id, quantity=1.0, partner=cost_line.partner_id)['total_excluded']
                    else:
                        cost_line.currency_amount = cost_line.currency_id.with_context(date=cost_line.date_invoice).compute(cost_line.amount, cost_line.env.user.company_id.currency_id)
                elif cost_line.state == 'draft':
                    amount = cost_line.currency_id.with_context(date=cost_line.date_invoice).compute(cost_line.amount, cost_line.env.user.company_id.currency_id)
                    cost_line.currency_amount = cost_line.taxes_id.with_context(round=False, date=cost_line.date_invoice).compute_all(amount, currency=cost_line.currency_id, quantity=1.0, partner=cost_line.partner_id)['total_excluded']
                cost_line.calculate_cost([l.purchase_line_id for l in cost_line.po_lines])
                for line in cost_line.po_lines:
                    if line.order_id.id not in costed_order_ids:
                        costed_order_ids.append(line.order_id.id)
                    costed_line_ids.append(line.purchase_line_id.id)
                    line.purchase_line_id.write({'is_started_costing': True})
            costed_orders = self.env['purchase.order'].browse(costed_order_ids)
            cancel_costed_line_ids = []
            cancel_costing = False
            for order in costed_orders:
                cancel_costed_line_ids += list(set(order.order_line.ids) - set(costed_line_ids))
            cancel_lines = self.env['purchase.order.line'].browse(cancel_costed_line_ids)
            cancel_lines.write({'is_started_costing': False})

        if self.extra_cost_status == 'new_cost':
            self.extra_cost_status = 'cost_calculated'
            self.message_post(body=_("The total extra cost " +
                                     "calculated for adjustment."))
        return True