# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
import time
from odoo.exceptions import ValidationError
from datetime import date, datetime

class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    is_started_costing = fields.Boolean('Is Started Costing')
    cost_ok = fields.Boolean('Cost Approved', readonly=True, copy=False)
    cost_approved_by = fields.Many2one('res.users', 'User', readonly=True, copy=False)
    cost_approved_date = fields.Datetime('Cost Approved Date', readonly=True, copy=False)

    @api.multi
    def button_approve_line_cost(self):
        for line in self:
            stock_move = self.env['stock.move'].search([('purchase_line_id', '=', line.id)])
            
            line_ids = []
            for move in stock_move:
                move.accounts_dict = None
                #Нэмэлт зардлуудын данс, дүнг stock.move руу хадгалах
                accounts_dic = {}
                for line_cost in line.po_line_cost_ids:
                    if line_cost.extra_cost_id.account_id.id not in accounts_dic.keys():
                        accounts_dic[line_cost.extra_cost_id.account_id.id] = line_cost.unit_cost*line_cost.extra_cost_id.currency_rate
                    else:
                        accounts_dic[line_cost.extra_cost_id.account_id.id] += line_cost.unit_cost*line_cost.extra_cost_id.currency_rate
                
                for account_id, amount in accounts_dic.iteritems():
                    line_ids.append((0,0,{'stock_move_id': move.id, 'account_id': account_id, 'amount': amount}))
                
                #Барааны ЗЯБ данс, дүнг stock.move руу нэмж хадгалах
                
                if self.is_module_installed('l10n_mn_stock_account_cost_for_each_wh'):
                    if line.order_id.picking_type_id.warehouse_id.stock_account_input_id.id:
                        line_ids.append((0,0,{'stock_move_id': move.id, 'account_id': line.order_id.picking_type_id.warehouse_id.stock_account_input_id.id, 'amount': line.price_unit_mnt})) 
                    else:
                        raise ValidationError(_("Select stock input account of the warehouse!"))
                else:
                    if line.product_id.categ_id.property_stock_account_input_categ_id.id:
                        line_ids.append((0,0,{'stock_move_id': move.id, 'account_id': line.product_id.categ_id.property_stock_account_input_categ_id.id, 'amount': line.price_unit_mnt}))
                    else:
                        raise ValidationError(_("Select stock input account of the product category!"))
                move.accounts_dict = line_ids
                move.cost_ok = True
            line.cost_ok = True
            line.cost_approved_by = self.env.uid
            line.cost_approved_date = time.strftime('%Y-%m-%d %H:%M:%S')
            line.order_id.cost_ok = True  # PO-ийн cost_ok талбар ашиглагдахгүй тул шууд True болгов.

    @api.multi
    def button_cancel_line_cost(self):
        for line in self:
            stock_move = self.env['stock.move'].search([('purchase_line_id', '=', line.id)])
            for move in stock_move:
                if move.state != 'done':
                    move.cost_ok = False
                else:
                    raise ValidationError(_("Related stock move is done!"))
            line.cost_ok = False
            line.cost_approved_by = None
            line.cost_approved_date = None

    @api.multi
    def name_get(self):
        result = []
        name = ''
        for line in self:
            name = '%s %s %s' % (line.order_id.name, line.name, (u' [Зардал шингээж эхэлсэн]' if line.is_started_costing else ''))
            result.append((line.id, name))
        return result
    
    @api.multi
    def _compute_cost_unit(self):
        for line in self:
            price_unit = line.price_unit
            # extra cost тооцох үед худалдан авалтын мөрд хөнгөлөлт байвал хөнгөлөттэй үнэ тооцно
            if line.discount and line.discount > 0:
                price_unit = price_unit * (1 - (line.discount / 100))
            cost = line.taxes_id.compute_all(price_unit, line.currency_id, line.product_qty)['total_excluded']
            if line.order_id.currency_id != line.order_id.company_id.currency_id:
                cost = line.order_id.currency_id.with_context(date=line.order_id.currency_cal_date).compute(cost, line.order_id.company_id.currency_id)

                # Хэрэв гараас валютын ханшыг өөрчилсөн бол тухайн өөрчилсөн утгаар нэгж өртгийг бодож гаргав.
                currency_rate = 1
                currency_obj = self.env['res.currency'].search([('name', '=', line.order_id.currency_id.name)])
                date_time = datetime.strptime(line.order_id.currency_cal_date   , '%Y-%m-%d %H:%M:%S')
                day = date_time.replace(hour=0, minute=0, second=0)
                day = str(day)
                if currency_obj:
                    for currency in currency_obj:
                        for rate in currency.rate_ids:
                            if rate.name == day:
                                currency_rate = rate.alter_rate
                if currency_rate != line.order_id.currency_rate:
                    cost = line.order_id.currency_rate * line.taxes_id.compute_all(price_unit, line.currency_id, line.product_qty)['total_excluded']
            if line.product_qty:
                cost /= line.product_qty
            if line.po_line_cost_ids:
                for extra in line.po_line_cost_ids:
                    if not extra.extra_cost_id.item_id.non_cost_calc and extra.extra_cost_id.state == 'completed' and not (self.is_module_installed('l10n_mn_purchase_shipment') and extra.extra_cost_id.shipment_id):
                        current_extra_cost_line = extra
                        curr_cost = current_extra_cost_line.unit_cost
                        if line.order_id.company_id.currency_id.id != extra.extra_cost_id.currency_id.id:
                            curr_cost = current_extra_cost_line.unit_cost*extra.extra_cost_id.currency_rate
                        cost += curr_cost
            line.cost_unit = cost