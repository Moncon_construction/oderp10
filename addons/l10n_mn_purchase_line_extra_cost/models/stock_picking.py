# -*- coding: utf-8 -*-

from odoo import api, models
from odoo.exceptions import ValidationError
from odoo.tools.float_utils import float_compare
from odoo.tools.translate import _

class Picking(models.Model):
    _inherit = 'stock.picking'

    @api.multi
    def do_new_transfer(self):
        if self.picking_type_id.code == 'incoming' and self.purchase_id:
            allowed = True
            all_move_cost_not_ok = True
            if self.state == 'draft' or all([x.qty_done == 0.0 for x in self.pack_operation_ids]):
                #Бүх барааг хүлээж авах гэж байна.
                for move in self.move_lines:
                    move.cost_ok = move.purchase_line_id.cost_ok
                    if move.cost_ok:
                        all_move_cost_not_ok = False
                if all_move_cost_not_ok:
                    #Бүх барааны хөдөлгөөний өртгийг батлаагүй бол хориг тавих
                    raise ValidationError(_("You cannot process incoming shipment until purchase order line cost approved!"))
                else:
                    allowed = True
                    
            if not allowed:
                #Хэсэгчилж хүлээн авах гэж байна. Тиймээс хүлээн авах гэж байгаа барааны хөдөлгөөнүүдийн өртөг батлагдсан эсэхийг шалгах шаардлагатай.
                for pack in self.pack_operation_product_ids:
                    if pack.qty_done != 0: #Зөвхөн хүлээн авах гэж байгаа барааны хөдөлгөөний өртөг батлагдсан эсэхийг шалгая
                        total_cost_ok_qty = 0
                        for link in pack.linked_move_operation_ids:
                            link.move_id.cost_ok = link.move_id.purchase_line_id.cost_ok
                            if not link.move_id.cost_ok:
                                raise ValidationError(_("You cannot process incoming shipment until purchase order line cost approved! [%s] %s") % (pack.product_id.default_code, pack.product_id.name))
                        
        return super(Picking, self).do_new_transfer()
    
    def check_backorder(self):
        need_rereserve, all_op_processed = self.picking_recompute_remaining_quantities(done_qtys=True)
        for move in self.move_lines:
            if float_compare(move.remaining_qty, 0, precision_rounding=move.product_id.uom_id.rounding) != 0:
                return True
        return False