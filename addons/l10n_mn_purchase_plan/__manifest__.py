# -*- coding: utf-8 -*-
{
    "name" : "l10n_mn - Purchase Plan Module",
    "version" : "1.0",
    "author" : "Asterisk Technologies LLC",
    "description": """
    * Depending purchase, product, l10n_mn_base
    """,
    "website" : False,
    "depends" : ['l10n_mn_base', 'l10n_mn_workflow_config','l10n_mn_account_period','product','purchase_requisition','account'],
    "data" : [

       'security/purchase_plan_security.xml',
       'security/ir.model.access.csv',
       'data/plan_sequence.xml',
       'data/plan_notif_cron_email_template.xml',
       'data/follower_notification_cron.xml',
       'wizard/import_plan_lines_view.xml',
       'wizard/purchase_plan_reject_note.xml',
       'wizard/purchase_plan_cancel_note.xml',
       'views/purchase_plan_view.xml',

    ],
    "active": False,
    "installable": True,
}
