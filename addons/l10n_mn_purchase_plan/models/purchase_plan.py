# -*- coding: utf-8 -*-

from odoo.exceptions import UserError, AccessError
from odoo import models, fields, api 
from odoo import _
from odoo.exceptions import except_orm
import time
from odoo.http import request


class PurchaseMonthPlan(models.Model):
    _name = "purchase.month.plan"
    _description = "Purchase Month Plan"
    _rec_name = "period_id"

    period_id = fields.Many2one('account.period', string='Period')
    line_ids = fields.One2many('purchase.plan.line','month_id', string='Purchase Plan Lines')
    plan_id = fields.Many2one('purchase.plan', string='Purchase Plan')
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)


class PurchasePlanLine(models.Model):
    _name = "purchase.plan.line"
    _description = "Purchase Plan Lines"
    _rec_name = "product_qty"

    @api.model
    def create(self,  values):
        if 'product_id' in values:  
            prod = self.env['product.product'].sudo().browse(values['product_id'])
            values['product_uom_id'] = prod.uom_id.id
        return super(PurchasePlanLine, self).create(values)

    @api.multi
    def write(self, values):
        line = self
        if isinstance(line, list):
            line = line[0]

        if 'product_id' in values:
            prod = self.env['product.product'].browse(values['product_id'])
            values['product_uom_id'] = prod.uom_id.id

        if 'product_qty' in values:
            values['qty_performance'] = values['product_qty']

        return super(PurchasePlanLine, self).write(values)
    
    @api.onchange('product_id')
    def onchange_product_id(self):
        value = {'uom_id': ''}
        if self.product_id:
            value = {'uom_id': self.product_id.uom_id.id}
        return {'value': value}

    def _set_department(self):
        user = self.env.user
        employee_ids = self.env['hr.employee'].search([('user_id', '=', user.id)])
        if employee_ids:
            employee = self.env['hr.employee'].browse(employee_ids)[0]
            return employee.department_id.id
        else:
            raise except_orm(_('Warning!'), _('You don\'t have related employee. Please contact administrator.'))
        return None

    product_id = fields.Many2one('product.product', string='Product',required=True)
    descrip = fields.Char(string='Description')
    uom_id = fields.Many2one('product.uom', string='Unit Of Measure')
    product_qty = fields.Integer(string='Product Quantity',required=True)
    qty_performance = fields.Float(string='Product Quantity Performance', readonly=True)
    product_unit_price = fields.Float(string='Product Unit Price', required=True)
    month_id = fields.Many2one('purchase.month.plan', string='Month',required=True)
    period_id = fields.Many2one(related="month_id.period_id", string='Department', store=True)
    department_id = fields.Many2one(related="month_id.plan_id.department_id", string='Department', store=True)
    state = fields.Selection([('draft', 'Draft'),
        ('cancel', 'Cancelled'),
        ('validate', 'Validate'),
        ('done', 'Done')
        ], 'Status', default='draft', index=True, required=True, readonly=True, copy=False, track_visibility='always')
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)

class PurchasePlan(models.Model):
    _name = "purchase.plan"
    _rec_name = "department_id"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = "Purchase Plan"

    @api.model
    def _set_department(self):
        user = self.env.user
        employee_ids = self.env['hr.employee'].search([('user_id', '=', user.id)])
        if employee_ids:
            employee = self.env['hr.employee'].browse(employee_ids)[0]
            return employee.department_id.id
        else:
            raise except_orm(_('Warning!'), _('You don\'t have related employee. Please contact administrator.'))
        return None

    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    department_id = fields.Many2one('hr.department', string='Department', required=True,)
    planner_id = fields.Many2one('res.users', string='Planner', required=True, readonly=True, default=lambda self: self.env.uid)
    state = fields.Selection([('draft', 'Draft'),
        ('cancel', 'Cancelled'),
        ('validate', 'Validated'),
        ('done', 'Done')
        ], 'Status', default='draft', index=True, required=True, readonly=True, copy=False, track_visibility='always')
    name = fields.Char(string='Reference', size=32, required=True, help="Unique number of the plan, computed automatically when the plan is created.", default='/')
    month_plan_ids = fields.One2many('purchase.month.plan','plan_id', string='Purchase Month Plan Lines',states={'validate': [('readonly', True)],
                                                                                                 'done':[('readonly', True)],
                                                                                                 'cancel': [('readonly', True)],
                                                                                                })
    is_planner_pdm = fields.Boolean(compute="_compute_planner_pdm")
    is_pdm = fields.Boolean(default="_is_purchase_dep_mngr", compute="_is_purchase_dep_mngr")
    workflow_id = fields.Many2one('workflow.config', 'Workflow', required=True, domain=[('model_id.model', '=', 'purchase.plan')])
    check_sequence = fields.Integer(string='Workflow Step', default=0)
    
    @api.one
    def _is_purchase_dep_mngr(self):
        self.is_pdm = self.env.user.has_group('l10n_mn_purchase_plan.group_purchase_plan_user')
        return self.env.user.has_group('l10n_mn_purchase_plan.group_purchase_plan_user')
        
    def _get_department(self):
        return "[('company_id', '=', " + str(self.env.user.company_id.id) + ")]"

    def send_notif_to_followers(self, signal):
        return self.sudo().send_notif_to_followers_by_super_user(signal)
    
    def send_notif_to_followers_by_super_user(self, signal):
        states = {
            'draft': u'Ноорог',
            'sent_to_checker': u'Худалдан авалтын менежерт илгээгдсэн',
            'sent_to_financial_adviser': u'Санхүүгийн зөвлөхд илгээгдсэн',
            'sent_to_ceo': u'Гүйцэтгэх захиралд илгээгдсэн',
            'reject': u'Татгалзсан',
            'cancel': u'Цуцлагдсан',
            'done_by_ceo': u'Батлагдсан',
        }
        user = self.env.user
        mail_obj = self.env['mail.followers']
        template_id = self.env['ir.model.data'].get_object_reference('l10n_mn_purchase_plan', 'follower_notif_email_template')[1]

        followers = mail_obj.search([('res_model','=','purchase.plan'),('res_id','in', self.ids)])
        
        if followers:
            users = self.env['res.users'].sudo().search([('partner_id','in',[follower.partner_id.id for follower in followers])])
            plan = self
            data = {
                'subject': 'Purchase Plan Follower Notification',
                'model': 'purchase.plan',
                'department': plan[0].department_id.name,
                'base_url': self.env['ir.config_parameter'].get_param('web.base.url'),
                'action_id': self.env['ir.model.data'].get_object_reference('l10n_mn_purchase_plan', 'action_l10n_mn_purchase_plan')[1],
                'id': plan[0].id,
                'db_name': self.env.cr.dbname,
                'state': states[signal],
                'sender': self.env.user.name,
                'menu_path': u'Худалдан авалт / Худалдан авалтын төлөвлөгөө / Худалдан авалтын төлөвлөгөө',
            }
            
            for this_user in users:
                if this_user != user:
                    template = self.env['mail.template'].browse(template_id)
                    template.with_context(data).send_mail(this_user.id, force_send=True)
        return True

    def send_notification(self, signal):
        return self.sudo().send_notification_by_super_user(signal)
        
    def send_notification_by_super_user(self, signal):
        model_obj = self.env['ir.model.data']
        groups = {'send_notif_to_checker':'group_purchase_plan_user',
                  'send_notif_to_fin_analyst':'group_purchase_plan_manager',
                  'send_notif_to_ceo':'group_purchase_finance_adviser'}
        states = {'send_notif_to_checker':u'Худалдан авалтын менежерт илгээгдсэн',
                  'send_notif_to_fin_analyst': u'Санхүүгийн зөвлөхд илгээгдсэн',
                  'send_notif_to_ceo': u'Гүйцэтгэх захиралд илгээгдсэн'}
        notif_groups = False
        if signal == 'send_notif_to_fin_analyst':
            notif_groups = model_obj.get_object_reference('l10n_mn_purchase_plan', groups[signal])
        else:
            notif_groups = model_obj.get_object_reference('l10n_mn_purchase_plan', groups[signal])

        plan = self
        if plan:
            sel_users = self.env['res.users'].sudo().search([('groups_id','in',[notif_groups[1]])])
            sel_user_ids = []
            if sel_users:
                sel_user_ids = sel_users.ids
            #Нэвтэрсэн хэрэглэгчийн хэлтсийн, chief группын ажилчдын жагсаалт

            group_user_ids = self.env['res.users'].sudo().search([('id','in',sel_user_ids)])
            data = {
                'department': plan[0].department_id.name,
                'base_url': self.env['ir.config_parameter'].get_param('web.base.url'),
                'id': plan[0].id,
                'db_name': self.env.cr.dbname,
                'state': states[signal],
                'buyer': plan[0].planner_id.name,
                'sender': self.env.user.name,
            }
            template_id = self.env['ir.model.data'].get_object_reference('l10n_mn_purchase_plan', 'plan_notif_cron_email_template')[1]
            if group_user_ids:
                users = group_user_ids
                user_emails = []
                for user in users:
                    user_emails.append(user.login)
                    template = self.env['mail.template'].browse(template_id)
                    template.with_context(data).send_mail(user.id, force_send=True)
                email = u'Төлөв: → ' + states[signal]
                self.message_post(body= email + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ', '.join(user_emails))
            else:
                raise UserError(_('Not found receiver. Please contact administrator.'))
        return True

    def send_to_checker(self):
        self.send_notification('send_notif_to_checker')
        self.send_notif_to_followers('sent_to_checker')
        self.add_follower()
        return self.write({'state': 'sent_to_checker'})

    def send_to_financial_analyst(self):
        self.send_notification('send_notif_to_fin_analyst')
        self.send_notif_to_followers('sent_to_financial_adviser')
        self.add_follower()
        return self.write({'state': 'sent_to_financial_adviser'})

    def send_to_ceo(self):
        self.send_notification('send_notif_to_ceo')
        self.send_notif_to_followers('sent_to_ceo')
        self.add_follower()
        return self.write({'state': 'sent_to_ceo'})

    def cancel(self):
        plan = self[0]
        line_ids = []
        if plan.month_plan_ids:
            for month_plan in plan.month_plan_ids:
                if month_plan.line_ids:
                    for line in month_plan.line_ids:
                        line_ids.append(line.id)
        if line_ids:
            lines = self.env['purchase.plan.line'].browse(line_ids)
            lines.write({'state': 'draft'})
        return self.write({'state': 'cancel'})

    def done(self):
        plan = self[0]
        line_ids = []
        if plan.month_plan_ids:
            for month_plan in plan.month_plan_ids:
                if month_plan.line_ids:
                    for line in month_plan.line_ids:
                        line_ids.append(line.id)
        if line_ids:
            lines = self.env['purchase.plan.line'].browse(line_ids)
            lines.write({'state': 'validate'})
        return self.write({'state': 'done'})
    
    def reject(self):
        return self.write({'state': 'draft'})
    
    def reset(self):
        return self.write({'state': 'draft'})

class WorkflowHistory(models.Model):
    _inherit = 'workflow.history'

    purchase_plan_id = fields.Many2one('purchase.plan', 'Purchase Plan')

class PurchasePlan(models.Model):
    _inherit = "purchase.plan"
    
    history_lines = fields.One2many('workflow.history', 'purchase_plan_id', 'Workflow History', copy=False)

    @api.multi
    def name_get(self):
        res = []
        for obj in self:
            name = obj.name
            res.append((obj.id, name))
        return res

    @api.model
    def create(self, vals):
        context = self.env.context.copy()
        vals['name'] = self.env['ir.sequence'].get( 'purchase.plan')
        context.update({'mail_create_nolog': True})
        plan = super(PurchasePlan, self).create(vals)
        plan.message_post(body=_("Plan created"))
        return plan

    def unlink(self):
        stat = self.read(['state'])
        unlink_ids = []
        for t in stat:
            if t['state'] in ('draft','cancel'):
                unlink_ids.append(t['id'])
            else:
                raise UserError( _('In order to delete a purchase plan line, you must first cancel it'))
        to_delete = self.env['purchase.plan'].browse(unlink_ids)
        return super(PurchasePlan, to_delete).unlink()

    def add_follower(self):
        user = self.env.user
        mail_obj = self.env['mail.followers']
        is_follower = mail_obj.search([
            ('partner_id', '=', user.partner_id.id),
            ('res_model', '=', 'purchase.plan'),
            ('res_id', '=', self[0].id)
        ])
        if not is_follower:
            return self.env['mail.followers'].create({
                'res_model': 'purchase.plan',
                'res_id': self[0].id,
                'partner_id': user.partner_id.id
            })
        return True       
        
    def message_unsubscribe(self, partner_ids):
        """ Remove partners from the records followers. """
        # not necessary for computation, but saves an access right check
        if not partner_ids:
            return True
        user_pid = self.env.user.partner_id
        if set(partner_ids) != set([user_pid]):
            raise UserError( _('You can\'t remove other followers.'))
            self.check_access_rights('read')
            self.check_access_rule('read')
        else:
            self.check_access_rights('write')
            self.check_access_rule('write')    
        fol_obj = self.pool['mail.followers']
        followers = fol_obj.search([
            ('res_model', '=', self._name),
            ('res_id', 'in', self.ids),
            ('partner_id', 'in', partner_ids)
        ])
        return followers.unlink()
    
    #Динамик ажлын урсгал
    @api.multi
    def action_validate(self):
        for obj in self:
            if obj.workflow_id:
                workflow_obj = self.env['workflow.config']
                success, current_sequence = workflow_obj.send('workflow.history', 'purchase_plan_id', obj, self.env.user.id)
                is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(obj.workflow_id.id,
                                                                                                 obj, self.env.user.id,
                                                                                                 obj.check_sequence + 1,
                                                                                                 'next')
                if is_next:
                    obj.check_users = [(6, 0, next_user_ids)]
                if success:
                    obj.check_sequence = current_sequence
                    obj.ensure_one()
                    obj.state = 'validate'
        return True

    @api.multi
    def action_done(self):
        self.ensure_one()
        workflow_obj = self.env['workflow.config']
        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve(
                'workflow.history', 'purchase_plan_id', self, self.env.user.id)
            is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(self.workflow_id.id, self,
                                                                                             self.env.user.id,
                                                                                             self.check_sequence + 1,
                                                                                             'next')
            if is_next:
                self.check_users = [(6, 0, next_user_ids)]
            if success:
                if sub_success:
                    self.state = 'done'
                    self.step_is_final = True
                else:
                    self.check_sequence = current_sequence
                    self.show_approve_button = True
        return True
    
class PurchaseRequisitionLine(models.Model):
    _inherit = 'purchase.requisition.line'

    plan_line_id = fields.Many2one('purchase.plan.line', string='Plan Line')
    plan_month = fields.Many2one(related="plan_line_id.month_id")
    plan = fields.Many2one(related="plan_month.plan_id", string="Plan")
    perf_count = fields.Float(compute="_compute_balance", string="Product Quantitiy Balance")
    plan_name = fields.Char(string='Plan')

    @api.onchange('product_id')
    @api.depends('product_id')
    def _onchange_purchase_plan(self):
        requisition_line = self
        for plan_line in self.env['purchase.plan.line'].search([]):
            if requisition_line.product_id:
                if requisition_line.product_id == plan_line.product_id:
                    requisition_line.plan = plan_line.month_id.plan_id
                    requisition_line.plan_line_id = plan_line



    @api.model
    def create(self, values):
        requisition_line = super(PurchaseRequisitionLine, self).create(values)
        if len(requisition_line) >= 1:
            requisition_line = requisition_line[0]
        if 'product_id' in values:
            prod = self.env['product.product'].sudo().browse(values['product_id'])
            values['product_uom_id'] = prod.product_tmpl_id.uom_id.id
        if requisition_line.plan_line_id.id:
            plan = self.env['purchase.plan.line'].browse(requisition_line.plan_line_id.id)[0]
            if 'product_qty' in values:
                plan.qty_performance += values['product_qty']
                if plan.qty_performance > plan.product_qty:
                    raise UserError(_(u'Төлөвлөгдcөн хэмжээнээс хэтэрсэн байна.'))

        return requisition_line

    @api.multi
    def write(self, values):
        requisition_line = self
        if len(requisition_line) > 1:
            requisition_line = requisition_line[0]
        if 'product_id' in values:
            prod = self.env['product.product'].sudo().browse(values['product_id'])
            values['product_uom_id'] = prod.product_tmpl_id.uom_id.id
        if requisition_line.plan_line_id.id:
            plan_line = self.env['purchase.plan.line'].browse(requisition_line.plan_line_id.id)[0]
            plan_line.write({'qty_performance':plan_line.qty_performance + requisition_line.product_qty})
            if 'product_qty' in values:
                if values['product_qty'] - requisition_line.product_qty + plan_line.qty_performance:
                    plan_line.write({'qty_performance':values['product_qty']})
                if plan_line.qty_performance > plan_line.product_qty:
                    alert = u"Та \'%s\' төлөвлөгөөнөөс \'%s\' барааг  %s хэмжээгээр захиалахыг хүссэн байна.\n\
                                \t\t\t\t\t\tУг барааны:\n\
                                \t\t\t\t\t\tТөлөвлөгөөнд заасан хэмжээ:\t\t\t%s\n\
                                \t\t\t\t\t\tНийт захигдсан хэмжээ:\t\t\t%s\n\
                                \t\t\t\t\t\tТөлөвлөгөөнд зааснаас хэтэрсэн хэмжээ:\t\t\t%s" % (requisition_line.plan_line_id.month_id.plan_id.name,requisition_line.product_id.name,requisition_line.product_qty,\
                                                                         plan_line.product_qty,plan_line.qty_performance,(plan_line.qty_performance-requisition_line.product_qty))

                    raise UserError( _(alert))


        return super(PurchaseRequisitionLine, self).write(values)

    def unlink(self):
        requisition_line = self
        for line in requisition_line:
            plan = self.env['purchase.plan.line'].browse(line.plan_line_id.id)
            if isinstance(plan, list):
                plan = plan[0]
            plan.write({'qty_performance':(plan.qty_performance - line.product_qty) if (plan.qty_performance - line.product_qty) > 0 else 0})
        return super(PurchaseRequisitionLine,  self).unlink()

    def _compute_balance(self):
        for obj in self:
            obj.perf_count = obj.plan_line_id.product_qty - obj.plan_line_id.qty_performance

class PurchaseRequisition(models.Model):
    _inherit = 'purchase.requisition'

    def unlink(self):
        requisition = self
        if isinstance(requisition, list):
            requisition = requisition[0]
        for obj in requisition:
            for line in obj.line_ids:
                if line.plan_line_id.id:
                    plan = self.env['purchase.plan.line'].browse( line.plan_line_id.id)
                    if isinstance(plan, list):
                        plan = plan[0]
                    plan.write({'qty_performance':(plan.qty_performance - line.product_qty)})

        return super(PurchaseRequisition, self).unlink()