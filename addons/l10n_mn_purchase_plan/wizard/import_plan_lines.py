# -*- coding: utf-8 -*-

##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2019 Asterisk Technologies LLC Co.,ltd (<http://asterisk-tech.mn/>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : + 976 77228080 
#
##############################################################################

from odoo import fields, models
from odoo import _

class PurchasePlanLineImport(models.TransientModel):
    _name = "purchase.plan.line.import"
    _description = "Purchase Plan Line Import"

    department_id = fields.Many2one('hr.department')
    plan_line_ids = fields.Many2many('purchase.plan.line', 'import_purch_plan_rel', 'wizard_id', 'line_id', string='Purchase Plan Lines', domain=[('state', '=', 'approved')], required=True)
    
    def import_plan_lines(self):
        active_ids = self.env.context.get('active_ids', [])
        data =  self[0]
        requisition = self.env['purchase.requisition'].browse(active_ids)
        for line in data.plan_line_ids:
            self.env['purchase.requisition.line'].create({
                 'product_id': line.product_id.id,
                 'product_uom_id': line.uom_id.id,
                 'product_qty': 1,
                 'plan_name':line.product_id.name,
                 'market_price': line.product_id.product_tmpl_id.market_price,
                 'price_unit': line.product_unit_price,
                 'plan_line_id': line.id,
                 'requisition_id': requisition[0].id
            })
        return {'type': 'ir.actions.act_window_close'}
