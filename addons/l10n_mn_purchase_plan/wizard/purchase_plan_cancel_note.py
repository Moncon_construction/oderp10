# -*- coding: utf-8 -*-

from odoo import fields, models
from odoo import _

class PurchasePlanCancelNote(models.Model):
    _name = "purchase.plan.cancel.note"
    _description = "Purchase plan cancel note"

    note = fields.Text(string='Note', required=False)

    def reject1(self):
        active_ids = self._context.get('active_ids', False) or []
        plan_obj = self.env['purchase.plan']
        for plan in plan_obj.browse(active_ids):
            message_txt = u"Худалдан авалтын төлөвлөгөөг батлахаас татгалзлаа:\t" + self.note
            plan.message_post(body=message_txt)
            return plan.cancel()
