# -*- coding: utf-8 -*-

from odoo.exceptions import except_orm

from odoo import fields, models
from odoo import _

class PurchasePlanRejectNote(models.Model):
    _name = "purchase.plan.reject.note"
    _description = "Purchase Plan Reject Note"

    note = fields.Text(string='Note', required=False)
    
    def reject1(self):
        active_ids = self._context.get('active_ids', False) or []
        plan_obj = self.env['purchase.plan']
        for plan in plan_obj.browse(active_ids):
            plan.message_post(body=self.note)
            return plan.reject()
