# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Purchase Report",
    'version': '1.0',
    'depends': ['l10n_mn_purchase', 'l10n_mn_stock_return_type', 'l10n_mn_report'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Purchase Modules',
    'description': """
        Худалдан авалтын дэлгэрэнгүй тайлан
        Худалдан авалтын буцаалтын тайлан
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/purchase_refund_report_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}