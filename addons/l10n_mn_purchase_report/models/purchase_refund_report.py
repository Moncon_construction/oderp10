# -*- encoding: utf-8 -*-
##############################################################################
import time
from datetime import date, datetime
from operator import itemgetter

from odoo.exceptions import ValidationError, UserError
from odoo import api, fields, models, _


class PurchaseRefundReport(models.Model):
    _name = 'purchase.refund.report'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = 'Purchase Refund Report Display'

    name = fields.Char('Name', required=True, track_visibility='onchange', states={'approved': [('readonly', True)]})
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env['res.company']._company_default_get('purchase.refund.report'),
                                 states={'approved': [('readonly', True)]})
    date_from = fields.Date('Start Date', required=True, default=lambda *a: time.strftime('%Y-%m-01'),  track_visibility='onchange', states={'approved': [('readonly', True)]})
    date_to = fields.Date('End Date', required=True, default=lambda *a: time.strftime('%Y-%m-%d'), track_visibility='onchange', states={'approved': [('readonly', True)]})
    warehouse_ids = fields.Many2many('stock.warehouse', string='Warehouses', states={'approved': [('readonly', True)]})
    supplier_ids = fields.Many2many('res.partner', string='Suppliers', states={'approved': [('readonly', True)]})
    category_ids = fields.Many2many('product.category', string='Product Categories', states={'approved': [('readonly', True)]})
    product_ids = fields.Many2many('product.product', string='Products', states={'approved': [('readonly', True)]})
    return_type_ids = fields.Many2many('stock.return.type', string='Return Types', states={'approved': [('readonly', True)]})
    purchase_order_ids = fields.Many2many('purchase.order', string='Purchase Orders', states={'approved': [('readonly', True)]})
    purchase_order_type = fields.Selection([('all', 'All'),
                                            ('foreign', 'Foreign'),
                                            ('local', 'By Local')], default='all', required=True,
                                           string='Purchase Type', track_visibility='onchange', states={'approved': [('readonly', True)]})
    group_by = fields.Selection([('by_warehouse', 'By Warehouse'),
                                 ('by_supplier', 'By Supplier'),
                                 ('by_product_categ', 'By Product Category'),
                                 ('by_return_type', 'By Return Type'),
                                 ('by_purchase', 'By Purchase')], string="Group By", track_visibility='onchange', states={'approved': [('readonly', True)]})
    second_group_by = fields.Selection([('by_supplier', 'By Supplier'),
                                        ('by_product_categ', 'By Product Category'),
                                        ('by_return_type', 'By Return Type'),
                                        ('by_purchase', 'By Purchase')], string="Group By 2", track_visibility='onchange', states={'approved': [('readonly', True)]})
    third_group_by = fields.Selection([('by_warehouse', 'By Warehouse'),
                                       ('by_return_type', 'By Return Type'),
                                       ('by_purchase', 'By Purchase')], string="Group By 2", track_visibility='onchange',states={'approved': [('readonly', True)]})
    forth_group_by = fields.Selection([('by_warehouse', 'By Warehouse'),
                                       ('by_supplier', 'By Supplier'),
                                       ('by_return_type', 'By Return Type')], string="Group By 2", track_visibility='onchange', states={'approved': [('readonly', True)]})
    fifth_group_by = fields.Selection([('by_warehouse', 'By Warehouse'),
                                       ('by_supplier', 'By Supplier'),
                                       ('by_product_categ', 'By Product Category')], string="Group By 2", track_visibility='onchange', states={'approved': [('readonly', True)]})
    is_second_group = fields.Boolean(string="Is Second Group?", default=True)
    is_product = fields.Boolean(string="Is Product?", default=True)
    line_ids = fields.One2many('purchase.refund.report.line', 'report_id', string='Lines', readonly=True, copy=False)
    description = fields.Text('Description')
    state = fields.Selection([('draft', 'Draft'),
                              ('approved', 'Approved')], string='State', default='draft', required=True, track_visibility='onchange')

    @api.onchange('company_id')
    # Компаниар many2many талбаруудад domain тавих
    def onchange_domain(self):
        domain = {}
        for report in self:
            _warehouses = []
            for warehouse in self.env.user.allowed_warehouses:
                _warehouses.append(warehouse.id)
            if _warehouses:
                domain['warehouse_ids'] = [('id', 'in', _warehouses), ('company_id', '=', report.company_id.id)]
            else:
                domain['warehouse_ids'] = []
            domain['supplier_ids'] = [('company_id', '=', report.company_id.id)]
            domain['category_ids'] = [('company_id', '=', report.company_id.id)]
            domain['product_ids'] = [('product_tmpl_id.company_id', '=', report.company_id.id)]
            domain['return_type_ids'] = [('company_id', '=', report.company_id.id)]
            domain['purchase_order_ids'] = [('company_id', '=', report.company_id.id), ('purchase_returned', '=', True), ('state', 'in', ('purchase', 'done')),
                                            ('date_order', '>=', report.date_from), ('date_order', '<=', report.date_to)]
            return {'domain': domain}

    @api.onchange('purchase_order_type', 'supplier_ids', 'warehouse_ids', 'date_from', 'date_to')
    def _onchange_name_purchase_domain(self):
        # Компани, эхлэх болон дуусах огноогоор нэр талбар бөглөгдөх
        # Худалдан авалтын захиалга дээрх domain: нийлүүлэгчид болон агуулахаар
        domain = {}
        for report in self:
            name = ''
            if report.company_id:
                name += _('%s is ') % report.company_id.name
            if report.date_from:
                date_from = report.date_from
                name += _('[%s - ') % report.date_from
            else:
                raise UserError(_("Please select date from field"))
            if report.date_to:
                date_to = report.date_to
                name += _('%s] duration purchase return report') % report.date_to
            else:
                date_to = datetime.today().strftime('%Y-%m-%d')
            if (report.supplier_ids and len(report.supplier_ids) > 0) and (report.warehouse_ids and len(report.warehouse_ids) > 0) and report.purchase_order_type == 'all':
                domain['purchase_order_ids'] = [('company_id', '=', report.company_id.id), ('purchase_returned', '=', True), ('state', 'in', ('purchase', 'done')),
                                                ('warehouse_id', 'in', report.warehouse_ids.ids), ('partner_id', 'in', report.supplier_ids.ids),
                                                ('date_order', '>=', date_from), ('date_order', '<=', date_to)]
            elif (report.supplier_ids and len(report.supplier_ids) > 0) and (report.warehouse_ids and len(report.warehouse_ids) > 0) and report.purchase_order_type != 'all':
                domain['purchase_order_ids'] = [('company_id', '=', report.company_id.id), ('purchase_returned', '=', True), ('state', 'in', ('purchase', 'done')),
                                                ('warehouse_id', 'in', report.warehouse_ids.ids), ('partner_id', 'in', report.supplier_ids.ids),
                                                ('date_order', '>=', date_from), ('date_order', '<=', date_to), ('purchase_order_type', '<=', report.purchase_order_type)]
            elif report.supplier_ids and len(report.supplier_ids) > 0 and report.purchase_order_type == 'all':
                domain['purchase_order_ids'] = [('company_id', '=', report.company_id.id),('purchase_returned', '=', True),
                                                ('state', 'in', ('purchase', 'done')), ('partner_id', 'in', report.supplier_ids.ids),
                                                ('date_order', '>=', date_from), ('date_order', '<=', date_to)]
            elif report.supplier_ids and len(report.supplier_ids) > 0 and report.purchase_order_type != 'all':
                domain['purchase_order_ids'] = [('company_id', '=', report.company_id.id),('purchase_returned', '=', True),
                                                ('state', 'in', ('purchase', 'done')), ('partner_id', 'in', report.supplier_ids.ids),
                                                ('date_order', '>=', date_from), ('date_order', '<=', date_to), ('purchase_order_type', '<=', report.purchase_order_type)]
            elif report.warehouse_ids and len(report.warehouse_ids) > 0 and report.purchase_order_type == 'all':
                domain['purchase_order_ids'] = [('company_id', '=', report.company_id.id), ('purchase_returned', '=', True),
                                                ('state', 'in', ('purchase', 'done')), ('warehouse_id', 'in', report.warehouse_ids.ids),
                                                ('date_order', '>=', date_from), ('date_order', '<=', date_to)]
            elif report.warehouse_ids and len(report.warehouse_ids) > 0 and report.purchase_order_type != 'all':
                domain['purchase_order_ids'] = [('company_id', '=', report.company_id.id), ('purchase_returned', '=', True),
                                                ('state', 'in', ('purchase', 'done')), ('warehouse_id', 'in', report.warehouse_ids.ids),
                                                ('date_order', '>=', date_from), ('date_order', '<=', date_to), ('purchase_order_type', '<=', report.purchase_order_type)]
            elif report.purchase_order_type == 'all':
                domain['purchase_order_ids'] = [('company_id', '=', report.company_id.id), ('purchase_returned', '=', True), ('state', 'in', ('purchase', 'done')),
                                                ('date_order', '>=', date_from), ('date_order', '<=', date_to)]
            elif report.purchase_order_type != 'all':
                domain['purchase_order_ids'] = [('company_id', '=', report.company_id.id), ('purchase_returned', '=', True), ('state', 'in', ('purchase', 'done')),
                                                ('date_order', '>=', date_from), ('date_order', '<=', date_to), ('purchase_order_type', '<=', report.purchase_order_type)]
            report.name = name
            return {'domain': domain}

    @api.onchange('category_ids')
    def _get_product_domain(self):
        # Барааны ангилал сонгосон үед тухайн ангиллын бараагаар domain тавих
        domain = {}
        for report in self:
            if report.category_ids and len(report.category_ids) > 0:
                domain['product_ids'] = [('product_tmpl_id.categ_id', 'in', report.category_ids.ids)]
                report.is_product = True
            elif report.product_ids and len(report.product_ids) > 0:
                report.is_product = True
            else:
                report.is_product = False
            return {'domain': domain}

    @api.onchange('product_ids')
    def _get_product(self):
        for report in self:
            if report.product_ids and len(report.product_ids) > 0 or report.category_ids and len(report.category_ids) > 0:
                report.is_product = True
            else:
                report.is_product = False

    @api.onchange("group_by")
    def _get_group_by(self):
        for report in self:
            if report.group_by == 'by_purchase':
                report.is_second_group = False

    @api.onchange("second_group_by")
    def _get_second_group_by(self):
        for report in self:
            if report.second_group_by:
                report.is_second_group = True
            else:
                report.is_second_group = False

    @api.onchange("third_group_by")
    def _get_third_group_by(self):
        for report in self:
            if report.third_group_by:
                report.is_second_group = True
            else:
                report.is_second_group = False

    @api.onchange("forth_group_by")
    def _get_forth_group_by(self):
        for report in self:
            if report.forth_group_by:
                report.is_second_group = True
            else:
                report.is_second_group = False

    @api.onchange("fifth_group_by")
    def _get_fifth_group_by(self):
        for report in self:
            if report.fifth_group_by:
                report.is_second_group = True
            else:
                report.is_second_group = False

    @api.multi
    def unlink(self):
        for report in self:
            if report.state != 'draft':
                raise UserError(_('Delete only draft in state'))
        return super(PurchaseRefundReport, self).unlink()

    @api.multi
    def validate(self):
        # Батлах
        self.write({'state': 'approved'})

    @api.multi
    def action_to_draft(self):
        # Ноороглох
        self.write({'state': 'draft'})

    @api.multi
    def compute(self):
        # Тооцоолол хийх функц
        self.line_ids = None
        line_obj = self.env['purchase.refund.report.line']
        left = True
        left_join = " "
        # Нөхцөл шалгаж буй хэсэг
        where = ' AND p.company_id = ' + str(self.company_id.id) + ' '
        where += " AND p.date_order BETWEEN '" + self.date_from + "' AND '" + self.date_to + "' "
        if self.purchase_order_type != 'all':
            where += "AND p.purchase_order_type = '" + self.purchase_order_type + "' "
        if self.purchase_order_ids:
            where += ' AND p.id IN (' + ','.join(map(str, self.purchase_order_ids.ids)) + ') '
        else:
            where += "AND p.id in (SELECT DISTINCT(l.order_id) "
            where += "FROM purchase_order_line l "
            where += "JOIN stock_move m ON (l.id = m.purchase_line_id) "
            where += "JOIN stock_move m1 ON (m.id = m1.origin_returned_move_id) "
            where += "WHERE m1.state = 'done') "
        if self.warehouse_ids:
            where += ' AND p.warehouse_id IN (' + ','.join(map(str, self.warehouse_ids.ids)) + ') '
        if self.supplier_ids:
            where += ' AND p.partner_id IN (' + ','.join(map(str, self.supplier_ids.ids)) + ') '
        if self.category_ids:
            where += ' AND pt.categ_id IN (' + ','.join(map(str, self.category_ids.ids)) + ') '
            left = False
        if self.product_ids:
            where += ' AND pl.product_id IN (' + ','.join(map(str, self.product_ids.ids)) + ') '
            left = False
        if self.return_type_ids:
            where += ' AND sp.return_type_id IN (' + ','.join(map(str, self.return_type_ids.ids)) + ') '

        order_by = 'p.date_order, p.name, pt.name, pl.product_qty, move.mqty, inv.iqty '
        join = ''
        select = ''

        if left == True:
            left_join = " LEFT "
        # Бүлэглэлтээс хамааран join, select, order_by хийж буй хэсэг
        if self.group_by:
            if self.group_by == 'by_warehouse': # Бүлэглэлт 1: Агуулах
                join += 'LEFT JOIN stock_warehouse sw ON (sw.id = p.warehouse_id) '
                select = "COALESCE(sw.name, '') AS group, "
                order_by = 'sw.name '
                if self.second_group_by and self.second_group_by == 'by_supplier': # Бүлэглэлт 2: Нийлүүлэгч
                    join += 'LEFT JOIN res_partner rp ON (rp.id = p.partner_id) '
                    select += "COALESCE(rp.name, '') AS sgroup, "
                    order_by += ', rp.name, p.date_order, p.name, pt.name, pl.product_qty, move.mqty, inv.iqty '
                elif self.second_group_by and self.second_group_by == 'by_product_categ': # Бүлэглэлт 2: Барааны ангилал
                    join += 'LEFT JOIN product_category pc ON (pc.id = pt.categ_id) '
                    select += "COALESCE(pc.name, '') AS sgroup, "
                    order_by += ', pc.name, p.date_order, p.name, pt.name, pl.product_qty, move.mqty, inv.iqty '
                elif self.second_group_by and self.second_group_by == 'by_return_type': # Бүлэглэлт 2: Буцаалтын төрөл
                    join += 'LEFT JOIN stock_return_type sr ON (sr.id = sp.return_type_id) '
                    select += "COALESCE(sr.name, '') AS sgroup, "
                    order_by += ', sr.name, p.date_order, p.name, pt.name, pl.product_qty, move.mqty, inv.iqty '
                elif self.second_group_by and self.second_group_by == 'by_purchase': # Бүлэглэлт 2: Худалдан авалтын захиалга
                    select += "COALESCE(p.name, '') AS sgroup, "
                    order_by += ', p.date_order, p.name, pt.name, pl.product_qty, move.mqty, inv.iqty '
            elif self.group_by == 'by_supplier': # Бүлэглэлт 1: Нийлүүлэгч
                join += 'LEFT JOIN res_partner rp ON (rp.id = p.partner_id) '
                select = "COALESCE(rp.name, '') AS group, "
                order_by = 'rp.name '
                if self.third_group_by and self.third_group_by == 'by_warehouse': # Бүлэглэлт 2: Агуулах
                    join += 'LEFT JOIN stock_warehouse sw ON (sw.id = p.warehouse_id) '
                    select += "COALESCE(sw.name, '') AS sgroup, "
                    order_by += ', sw.name, p.date_order, p.name, pt.name, pl.product_qty, move.mqty, inv.iqty '
                elif self.third_group_by and self.third_group_by == 'by_return_type': # Бүлэглэлт 2: Буцаалтын төрөл
                    join += 'LEFT JOIN stock_return_type sr ON (sr.id = sp.return_type_id) '
                    select += "COALESCE(sr.name, '') AS sgroup, "
                    order_by += ', sr.name, p.date_order, p.name, pt.name, pl.product_qty, move.mqty, inv.iqty '
                elif self.third_group_by and self.third_group_by == 'by_purchase': # Бүлэглэлт 2: Худалдан авалтын захиалга
                    select += "COALESCE(p.name, '') AS sgroup, "
                    order_by += ', p.date_order, p.name, pt.name, pl.product_qty, move.mqty, inv.iqty '
            elif self.group_by == 'by_product_categ': # Бүлэглэлт 1: Барааны ангилал
                join += 'LEFT JOIN product_category pc ON (pc.id = pt.categ_id) '
                select = "COALESCE(pc.name, '') AS group, "
                order_by = 'pc.name '
                if self.forth_group_by and self.forth_group_by == 'by_warehouse': # Бүлэглэлт 2: Агуулах
                    join += 'LEFT JOIN stock_warehouse sw ON (sw.id = p.warehouse_id) '
                    select += "COALESCE(sw.name, '') AS sgroup, "
                    order_by += ', sw.name, p.date_order, p.name, pt.name, pl.product_qty, move.mqty, inv.iqty '
                elif self.forth_group_by and self.forth_group_by == 'by_supplier': # Бүлэглэлт 2: Нийлүүлэгч
                    join += 'LEFT JOIN res_partner rp ON (rp.id = p.partner_id) '
                    select += "COALESCE(rp.name, '') AS sgroup, "
                    order_by += ', rp.name, p.date_order, p.name, pt.name, pl.product_qty, move.mqty, inv.iqty '
                elif self.forth_group_by and self.forth_group_by == 'by_return_type': # Бүлэглэлт 2: Буцаалтын төрөл
                    join += 'LEFT JOIN stock_return_type sr ON (sr.id = sp.return_type_id) '
                    select += "COALESCE(sr.name, '') AS sgroup, "
                    order_by += ', sr.name, p.date_order, p.name, pt.name, pl.product_qty, move.mqty, inv.iqty '
            elif self.group_by == 'by_return_type': # Бүлэглэлт 1: Буцаалтын төрөл
                join += 'LEFT JOIN stock_return_type sr ON (sr.id = sp.return_type_id) '
                select = "COALESCE(sr.name, '') AS group, "
                order_by = 'sr.name '
                if self.fifth_group_by and self.fifth_group_by == 'by_warehouse': # Бүлэглэлт 2: Агуулах
                    join += 'LEFT JOIN stock_warehouse sw ON (sw.id = p.warehouse_id) '
                    select += "COALESCE(sw.name, '') AS sgroup, "
                    order_by += ', sw.name, p.date_order, p.name, pt.name, pl.product_qty, move.mqty, inv.iqty '
                elif self.fifth_group_by and self.fifth_group_by == 'by_supplier': # Бүлэглэлт 2: Нийлүүлэгч
                    join += 'LEFT JOIN res_partner rp ON (rp.id = p.partner_id) '
                    select += "COALESCE(rp.name, '') AS sgroup, "
                    order_by += ', rp.name, p.date_order, p.name, pt.name, pl.product_qty, move.mqty, inv.iqty '
                elif self.fifth_group_by and self.fifth_group_by == 'by_product_categ': # Бүлэглэлт 2: Барааны ангилал
                    join += 'LEFT JOIN product_category pc ON (pc.id = pt.categ_id) '
                    select += "COALESCE(pc.name, '') AS sgroup, "
                    order_by += ', pc.name, p.date_order, p.name, pt.name, pl.product_qty, move.mqty, inv.iqty '
            elif self.group_by == 'by_purchase': # Бүлэглэлт 1: Худалдан авалтын захиалга
                select = "COALESCE(p.name, '') AS group, "
                order_by = 'p.date_order, p.name, pt.name, pl.product_qty, move.mqty, inv.iqty '
        self.env.cr.execute("SELECT  pl.id AS id, pl.product_id AS pid, pl.order_id AS oid, p.name AS oname, p.date_order AS date, "
                                   "pt.name AS name, pp.barcode AS barcode, pt.default_code AS default_code, " + select + " "
                                   "COALESCE(pl.product_qty, 0) AS pqty, COALESCE(pl.price_subtotal, 0) AS ptotal, "
                                   "COALESCE(move.mqty, 0) AS mqty, COALESCE(move.mtotal, 0) AS mtotal, "
                                   "COALESCE(inv.iqty, 0) AS iqty, COALESCE(inv.itotal, 0) AS itotal "
                           "FROM purchase_order_line pl "
                           "LEFT JOIN purchase_order p ON (p.id = pl.order_id) "
                           "LEFT JOIN stock_move sm ON (pl.id = sm.purchase_line_id) "
                           "LEFT JOIN stock_picking sp ON (sp.id = sm.picking_id) "
                           " " + left_join + "JOIN (SELECT m1.origin_returned_move_id AS mid, SUM(COALESCE(m1.product_qty, 0)) AS mqty, "
                                                   "SUM(COALESCE(m1.price_unit * m1.product_qty, 0)) AS mtotal "
                                                   "FROM stock_move m "
                                                   "LEFT JOIN stock_move m1 ON (m.id = m1.origin_returned_move_id) "
                                                   "WHERE m1.origin_returned_move_id IS NOT NULL AND m.state = 'done' AND m1.state = 'done' "
                                                            "AND m.purchase_line_id IS NOT NULL AND m1.product_qty > 0 "
                                                   "GROUP BY m1.origin_returned_move_id) AS move ON (sm.id = move.mid) "
                           " " + left_join + "JOIN (SELECT il.purchase_line_id AS plid, SUM(COALESCE(il.quantity, 0)) AS iqty, SUM(COALESCE(il.price_subtotal, 0)) AS itotal "
                                                   "FROM account_invoice_line il "
                                                   "LEFT JOIN account_invoice i ON (i.id = il.invoice_id) "
                                                   "WHERE il.purchase_line_id IS NOT NULL AND i.state in ('open', 'paid') AND i.type = 'in_refund' "
                                                   "GROUP BY il.purchase_line_id) AS inv ON (pl.id = inv.plid) "
                           "LEFT JOIN product_product pp ON (pp.id = pl.product_id) "
                           "LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id) " + join + " "
                           "WHERE p.state in ('purchase', 'done') " + where + " "
                           "ORDER BY " + order_by + " " )
        lines = self.env.cr.dictfetchall()
        for line in lines:
            group = sgroup = ''
            is_group = is_second_group = False
            pcost = mcost = icost = 0
            if self.group_by:
                group = line['group']
                is_group = True
                if self.is_second_group:
                    sgroup = line['sgroup']
                    is_second_group = True
            if line['pqty'] > 0 and line['ptotal'] > 0:
                pcost = line['ptotal'] / line['pqty']
            if line['mqty'] > 0 and line['mtotal'] > 0:
                mcost = line['mtotal'] / line['mqty']
            if line['iqty'] > 0 and line['itotal'] > 0:
                icost = line['itotal'] / line['iqty']
            line_obj.create({'date': line['date'],
                             'document_number':  line['oname'],
                             'purchase_line_id': line['id'],
                             'group': group,
                             'is_group': is_group,
                             'second_group': sgroup,
                             'is_second_group': is_second_group,
                             'product_id': line['pid'],
                             'name': line['name'],
                             'default_code': line['default_code'],
                             'barcode': line['barcode'],
                             'report_id': self.id,
                             'purchase_qty': line['pqty'] or 0.0,
                             'purchase_cost': pcost,
                             'purchase_total': line['ptotal'] or 0.0,
                             'picking_qty': line['mqty'] or 0.0,
                             'picking_cost': mcost,
                             'picking_total': line['mtotal'] or 0.0,
                             'invoice_qty': line['iqty'] or 0.0,
                             'invoice_cost': icost,
                             'invoice_total': line['itotal'] or 0.0,
                             })
        return True


class PurchaseRefundReportLine(models.Model):
    _name = 'purchase.refund.report.line'
    _description = 'Purchase Refund Report Lines'

    date = fields.Date('Date')
    document_number = fields.Char(string="Document Number")
    purchase_line_id = fields.Many2one('purchase.order.line', string="Purchase Line")
    group = fields.Char(string="Group")
    second_group = fields.Char(string="Second Group")
    is_group = fields.Boolean(string="Is Group?", default=True)
    is_second_group = fields.Boolean(string="Is Second Group?", default=True)
    product_id = fields.Many2one('product.product', string="Product")
    name = fields.Char('Product Name')
    default_code = fields.Char('Default Code')
    barcode = fields.Char('Barcode')
    report_id = fields.Many2one('purchase.refund.report', string='Purchase Refund Report')
    purchase_qty = fields.Float(string='Purchase Quantity', digits=(16, 2), default=0)
    purchase_cost = fields.Float(string='Purchase Cost', digits=(16, 2), default=0)
    purchase_total = fields.Float(string='Purchase Total', digits=(16, 2), default=0)
    picking_qty = fields.Float(string='Picking Quantity', digits=(16, 2), default=0)
    picking_cost = fields.Float(string='Picking Cost', digits=(16, 2), default=0)
    picking_total = fields.Float(string='Picking Total', digits=(16, 2), default=0)
    invoice_qty = fields.Float(string='Invoice Quantity', digits=(16, 2), default=0)
    invoice_cost = fields.Float(string='Invoice Cost', digits=(16, 2), default=0)
    invoice_total = fields.Float(string='Invoice Total', digits=(16, 2), default=0)