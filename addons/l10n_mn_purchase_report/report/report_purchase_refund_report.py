# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell

from odoo import api, models, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class PurchaseRefundReport(models.Model):
    _inherit = 'purchase.refund.report'

    # Энэ функцыг l10n_mn_account_analytic_report модуль дотор override хийсэн
    @api.multi
    def export_report(self):

        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Purchase Refund Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_title_float = book.add_format(ReportExcelCellStyles.format_title_float)
        format_group_left = book.add_format(ReportExcelCellStyles.format_group_left)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_center = book.add_format(ReportExcelCellStyles.format_content_center)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)

        seq = 1
        total_pqty = total_pcost = ptotal = total_mqty = total_mcost = mtotal = total_iqty = total_icost = itotal = 0
        sub_pqty = sub_pcost = psub = sub_mqty = sub_mcost = msub = sub_iqty = sub_icost = isub = 0

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('purchase_refund'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 1
        # compute column
        colx_number = 14
        sheet.set_column('A:A', 4)
        sheet.set_column('B:B', 10)
        if self.group_by == 'by_purchase' or self.second_group_by == 'by_purchase' or self.third_group_by == 'by_purchase':
            colx_number -= 1
            sheet.set_column('C:C', 10)
            sheet.set_column('D:D', 13)
            sheet.set_column('E:E', 30)
            sheet.set_column('F:F', 10)
            sheet.set_column('G:H', 12)
            sheet.set_column('I:I', 10)
            sheet.set_column('J:K', 12)
            sheet.set_column('L:L', 10)
            sheet.set_column('M:P', 12)
        else:
            sheet.set_column('C:D', 10)
            sheet.set_column('E:E', 13)
            sheet.set_column('F:F', 30)
            sheet.set_column('G:G', 10)
            sheet.set_column('H:I', 12)
            sheet.set_column('J:J', 10)
            sheet.set_column('K:L', 12)
            sheet.set_column('M:M', 10)
            sheet.set_column('N:P', 12)

        # create name
        sheet.merge_range(rowx, 0, rowx, 3, '%s: %s' % (_('Company Name'), self.company_id.name), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 0, rowx + 1, colx_number, report_name.upper(), format_name)
        rowx += 2

        # create duration
        sheet.merge_range(rowx, 0, rowx, 3, '%s: %s - %s' % (_('Report Duration'), self.date_from, self.date_to), format_filter)
        rowx += 1

        # create date
        sheet.merge_range(rowx, 0, rowx, 3, '%s: %s' % (_('Created on'), time.strftime('%Y-%m-%d')), format_filter)
        rowx += 2

        # Table title
        sheet.merge_range(rowx, 0, rowx + 1, 0, _('Seq'), format_title)
        sheet.merge_range(rowx, 1, rowx + 1, 1, _('Date'), format_title)
        col = 1
        if not (self.group_by == 'by_purchase' or self.second_group_by == 'by_purchase' or self.third_group_by == 'by_purchase'):
            sheet.merge_range(rowx, 2, rowx + 1, 2, _('Document Number'), format_title)
            col += 1
        sheet.merge_range(rowx, col + 1, rowx + 1, col + 1, _('Default Code'), format_title)
        sheet.merge_range(rowx, col + 2, rowx + 1, col + 2, _('Barcode'), format_title)
        sheet.merge_range(rowx, col + 3, rowx + 1, col + 3, _('Product Name'), format_title)
        sheet.merge_range(rowx, col + 4, rowx, col + 6, _('Purchase'), format_title)
        sheet.merge_range(rowx, col + 7, rowx, col + 9, _('Picking Refund'), format_title)
        sheet.merge_range(rowx, col + 10, rowx, col + 12, _('Invoice Refund'), format_title)
        sheet.write(rowx + 1, col + 4, _('QTY'), format_title)
        sheet.write(rowx + 1, col + 5, _('Price Unit'), format_title)
        sheet.write(rowx + 1, col + 6, _('Total Unit'), format_title)
        sheet.write(rowx + 1, col + 7, _('QTY'), format_title)
        sheet.write(rowx + 1, col + 8, _('Cost'), format_title)
        sheet.write(rowx + 1, col + 9, _('Total Cost'), format_title)
        sheet.write(rowx + 1, col + 10, _('QTY'), format_title)
        sheet.write(rowx + 1, col + 11, _('Price Unit'), format_title)
        sheet.write(rowx + 1, col + 12, _('Total Unit'), format_title)
        rowx += 2
        row2 = rowx
        row = rowx
        col += 4
        group = sgroup = False
        # Lines
        for line in self.line_ids:
            if line.is_group:
                total_pqty += line.purchase_qty
                total_pcost += line.purchase_cost
                ptotal += line.purchase_total
                total_mqty += line.picking_qty
                total_mcost += line.purchase_cost
                mtotal += line.purchase_total
                total_iqty += line.invoice_qty
                total_icost += line.invoice_cost
                itotal += line.invoice_total
                if not group:
                    sheet.merge_range(rowx, 0, rowx, col - 1, line.group or '', format_group_left)
                    rowx += 1
                    row = rowx
                    if not sgroup and line.is_second_group:
                        sheet.write(rowx, 0, '', format_group_left)
                        sheet.merge_range(rowx, 1, rowx, col - 1, line.second_group or '', format_group_left)
                        rowx += 1
                        row2 = rowx
                elif group and group != line.group:
                    if sgroup:
                        sheet.write_formula(row2 - 1, col, '{=SUM(' + xl_rowcol_to_cell(row2, col) + ':' + xl_rowcol_to_cell(rowx - 1, col) + ')}', format_group_float)
                        sheet.write_formula(row2 - 1, col + 1, '{=SUM(' + xl_rowcol_to_cell(row2, col + 1) + ':' + xl_rowcol_to_cell(rowx - 1, col + 1) + ')}', format_group_float)
                        sheet.write_formula(row2 - 1, col + 2, '{=SUM(' + xl_rowcol_to_cell(row2, col + 2) + ':' + xl_rowcol_to_cell(rowx - 1, col + 2) + ')}', format_group_float)
                        sheet.write_formula(row2 - 1, col + 3, '{=SUM(' + xl_rowcol_to_cell(row2, col + 3) + ':' + xl_rowcol_to_cell(rowx - 1, col + 3) + ')}', format_group_float)
                        sheet.write_formula(row2 - 1, col + 4, '{=SUM(' + xl_rowcol_to_cell(row2, col + 4) + ':' + xl_rowcol_to_cell(rowx - 1, col + 4) + ')}', format_group_float)
                        sheet.write_formula(row2 - 1, col + 5, '{=SUM(' + xl_rowcol_to_cell(row2, col + 5) + ':' + xl_rowcol_to_cell(rowx - 1, col + 5) + ')}', format_group_float)
                        sheet.write_formula(row2 - 1, col + 6, '{=SUM(' + xl_rowcol_to_cell(row2, col + 6) + ':' + xl_rowcol_to_cell(rowx - 1, col + 6) + ')}', format_group_float)
                        sheet.write_formula(row2 - 1, col + 7, '{=SUM(' + xl_rowcol_to_cell(row2, col + 7) + ':' + xl_rowcol_to_cell(rowx - 1, col + 7) + ')}', format_group_float)
                        sheet.write_formula(row2 - 1, col + 8, '{=SUM(' + xl_rowcol_to_cell(row2, col + 8) + ':' + xl_rowcol_to_cell(rowx - 1, col + 8) + ')}', format_group_float)
                    sheet.write(row - 1, col, sub_pqty, format_group_float)
                    sheet.write(row - 1, col + 1, sub_pcost, format_group_float)
                    sheet.write(row - 1, col + 2, psub, format_group_float)
                    sheet.write(row - 1, col + 3, sub_mqty, format_group_float)
                    sheet.write(row - 1, col + 4, sub_mcost, format_group_float)
                    sheet.write(row - 1, col + 5, msub, format_group_float)
                    sheet.write(row - 1, col + 6, sub_iqty, format_group_float)
                    sheet.write(row - 1, col + 7, sub_icost, format_group_float)
                    sheet.write(row - 1, col + 8, isub, format_group_float)
                    sheet.merge_range(rowx, 0, rowx, col - 1, line.group or '', format_group_left)
                    sub_pqty = sub_pcost = psub = sub_mqty = sub_mcost = msub = sub_iqty = sub_icost = isub = 0
                    rowx += 1
                    row = rowx
                    if sgroup:
                        sheet.write(rowx, 0, '', format_group_left)
                        sheet.merge_range(rowx, 1, rowx,  col - 1, line.second_group or '', format_group_left)
                        rowx += 1
                        row2 = rowx
                elif sgroup and sgroup != line.second_group:
                    sheet.write_formula(row2 - 1, col, '{=SUM(' + xl_rowcol_to_cell(row2, col) + ':' + xl_rowcol_to_cell(rowx - 1, col) + ')}', format_group_float)
                    sheet.write_formula(row2 - 1, col + 1, '{=SUM(' + xl_rowcol_to_cell(row2, col + 1) + ':' + xl_rowcol_to_cell(rowx - 1, col + 1) + ')}', format_group_float)
                    sheet.write_formula(row2 - 1, col + 2, '{=SUM(' + xl_rowcol_to_cell(row2, col + 2) + ':' + xl_rowcol_to_cell(rowx - 1, col + 2) + ')}', format_group_float)
                    sheet.write_formula(row2 - 1, col + 3, '{=SUM(' + xl_rowcol_to_cell(row2, col + 3) + ':' + xl_rowcol_to_cell(rowx - 1, col + 3) + ')}', format_group_float)
                    sheet.write_formula(row2 - 1, col + 4, '{=SUM(' + xl_rowcol_to_cell(row2, col + 4) + ':' + xl_rowcol_to_cell(rowx - 1, col + 4) + ')}', format_group_float)
                    sheet.write_formula(row2 - 1, col + 5, '{=SUM(' + xl_rowcol_to_cell(row2, col + 5) + ':' + xl_rowcol_to_cell(rowx - 1, col + 5) + ')}', format_group_float)
                    sheet.write_formula(row2 - 1, col + 6, '{=SUM(' + xl_rowcol_to_cell(row2, col + 6) + ':' + xl_rowcol_to_cell(rowx - 1, col + 6) + ')}', format_group_float)
                    sheet.write_formula(row2 - 1, col + 7, '{=SUM(' + xl_rowcol_to_cell(row2, col + 7) + ':' + xl_rowcol_to_cell(rowx - 1, col + 7) + ')}', format_group_float)
                    sheet.write_formula(row2 - 1, col + 8, '{=SUM(' + xl_rowcol_to_cell(row2, col + 8) + ':' + xl_rowcol_to_cell(rowx - 1, col + 8) + ')}', format_group_float)
                    sheet.write(rowx, 0, '', format_group_left)
                    sheet.merge_range(rowx, 1, rowx, col - 1, line.second_group or '', format_group_left)
                    rowx += 1
                    row2 = rowx
                    seq = 1
                sub_pqty += line.purchase_qty
                sub_pcost += line.purchase_cost
                psub += line.purchase_total
                sub_mqty += line.picking_qty
                sub_mcost += line.purchase_cost
                msub += line.purchase_total
                sub_iqty += line.invoice_qty
                sub_icost += line.invoice_cost
                isub += line.invoice_total
                group = line.group
                if self.is_second_group:
                    sgroup = line.second_group

            # Table row
            sheet.write(rowx, 0, seq, format_content_number)
            sheet.write(rowx, 1, line.date, format_content_center)
            if not (self.group_by == 'by_purchase' or self.second_group_by == 'by_purchase' or self.third_group_by == 'by_purchase'):
                sheet.write(rowx, 2, line.document_number or line.purchase_id and line.purchase_id.name, format_content_center)
            sheet.write(rowx, col - 3, line.default_code, format_content_center)
            sheet.write(rowx, col - 2, line.barcode, format_content_center)
            sheet.write(rowx, col - 1, line.name, format_content_text)
            sheet.write(rowx, col, line.purchase_qty, format_content_float)
            sheet.write(rowx, col + 1, line.purchase_cost, format_content_float)
            sheet.write(rowx, col + 2, line.purchase_total, format_content_float)
            sheet.write(rowx, col + 3, line.picking_qty, format_content_float)
            sheet.write(rowx, col + 4, line.picking_cost, format_content_float)
            sheet.write(rowx, col + 5, line.picking_total, format_content_float)
            sheet.write(rowx, col + 6, line.invoice_qty, format_content_float)
            sheet.write(rowx, col + 7, line.invoice_cost, format_content_float)
            sheet.write(rowx, col + 8, line.invoice_total, format_content_float)
            rowx += 1
            seq += 1

        if self.is_second_group and len(self.line_ids) > 0:
            sheet.write_formula(row2 - 1, col, '{=SUM(' + xl_rowcol_to_cell(row2, col) + ':' + xl_rowcol_to_cell(rowx - 1, col) + ')}', format_group_float)
            sheet.write_formula(row2 - 1, col + 1, '{=SUM(' + xl_rowcol_to_cell(row2, col + 1) + ':' + xl_rowcol_to_cell(rowx - 1, col + 1) + ')}', format_group_float)
            sheet.write_formula(row2 - 1, col + 2, '{=SUM(' + xl_rowcol_to_cell(row2, col + 2) + ':' + xl_rowcol_to_cell(rowx - 1, col + 2) + ')}', format_group_float)
            sheet.write_formula(row2 - 1, col + 3, '{=SUM(' + xl_rowcol_to_cell(row2, col + 3) + ':' + xl_rowcol_to_cell(rowx - 1, col + 3) + ')}', format_group_float)
            sheet.write_formula(row2 - 1, col + 4, '{=SUM(' + xl_rowcol_to_cell(row2, col + 4) + ':' + xl_rowcol_to_cell(rowx - 1, col + 4) + ')}', format_group_float)
            sheet.write_formula(row2 - 1, col + 5, '{=SUM(' + xl_rowcol_to_cell(row2, col + 5) + ':' + xl_rowcol_to_cell(rowx - 1, col + 5) + ')}', format_group_float)
            sheet.write_formula(row2 - 1, col + 6, '{=SUM(' + xl_rowcol_to_cell(row2, col + 6) + ':' + xl_rowcol_to_cell(rowx - 1, col + 6) + ')}', format_group_float)
            sheet.write_formula(row2 - 1, col + 7,  '{=SUM(' + xl_rowcol_to_cell(row2, col + 7) + ':' + xl_rowcol_to_cell(rowx - 1, col + 7) + ')}', format_group_float)
            sheet.write_formula(row2 - 1, col + 8, '{=SUM(' + xl_rowcol_to_cell(row2, col + 8) + ':' + xl_rowcol_to_cell(rowx - 1, col + 8) + ')}', format_group_float)
        if self.group_by and len(self.line_ids) > 0:
            sheet.write(row - 1, col, sub_pqty, format_group_float)
            sheet.write(row - 1, col + 1, sub_pcost, format_group_float)
            sheet.write(row - 1, col + 2, psub, format_group_float)
            sheet.write(row - 1, col + 3, sub_mqty, format_group_float)
            sheet.write(row - 1, col + 4, sub_mcost, format_group_float)
            sheet.write(row - 1, col + 5, msub, format_group_float)
            sheet.write(row - 1, col + 6, sub_iqty, format_group_float)
            sheet.write(row - 1, col + 7, sub_icost, format_group_float)
            sheet.write(row - 1, col + 8, isub, format_group_float)

            sheet.merge_range(rowx, 0, rowx, col - 1, _('TOTAL'), format_title)
            sheet.write(rowx, col, total_pqty, format_title_float)
            sheet.write(rowx, col + 1, total_pcost, format_title_float)
            sheet.write(rowx, col + 2, ptotal, format_title_float)
            sheet.write(rowx, col + 3, total_mqty, format_title_float)
            sheet.write(rowx, col + 4, total_mcost, format_title_float)
            sheet.write(rowx, col + 5, mtotal, format_title_float)
            sheet.write(rowx, col + 6, total_iqty, format_title_float)
            sheet.write(rowx, col + 7, total_icost, format_title_float)
            sheet.write(rowx, col + 8, itotal, format_title_float)
        elif not self.group_by and len(self.line_ids) > 0:
            sheet.merge_range(rowx, 0, rowx, col - 1, _('TOTAL'), format_title)
            sheet.write_formula(rowx, col, '{=SUM(' + xl_rowcol_to_cell(row2, col) + ':' + xl_rowcol_to_cell(rowx - 1, col) + ')}', format_title_float)
            sheet.write_formula(rowx, col + 1, '{=SUM(' + xl_rowcol_to_cell(row2, col + 1) + ':' + xl_rowcol_to_cell(rowx - 1, col + 1) + ')}', format_title_float)
            sheet.write_formula(rowx, col + 2, '{=SUM(' + xl_rowcol_to_cell(row2, col + 2) + ':' + xl_rowcol_to_cell(rowx - 1, col + 2) + ')}', format_title_float)
            sheet.write_formula(rowx, col + 3, '{=SUM(' + xl_rowcol_to_cell(row2, col + 3) + ':' + xl_rowcol_to_cell(rowx - 1, col + 3) + ')}', format_title_float)
            sheet.write_formula(rowx, col + 4, '{=SUM(' + xl_rowcol_to_cell(row2, col + 4) + ':' + xl_rowcol_to_cell(rowx - 1, col + 4) + ')}', format_title_float)
            sheet.write_formula(rowx, col + 5, '{=SUM(' + xl_rowcol_to_cell(row2, col + 5) + ':' + xl_rowcol_to_cell(rowx - 1, col + 5) + ')}', format_title_float)
            sheet.write_formula(rowx, col + 6, '{=SUM(' + xl_rowcol_to_cell(row2, col + 6) + ':' + xl_rowcol_to_cell(rowx - 1, col + 6) + ')}', format_title_float)
            sheet.write_formula(rowx, col + 7, '{=SUM(' + xl_rowcol_to_cell(row2, col + 7) + ':' + xl_rowcol_to_cell(rowx - 1, col + 7) + ')}', format_title_float)
            sheet.write_formula(rowx, col + 8, '{=SUM(' + xl_rowcol_to_cell(row2, col + 8) + ':' + xl_rowcol_to_cell(rowx - 1, col + 8) + ')}', format_title_float)

        rowx += 3
        sheet.merge_range(rowx, 2, rowx, 5, '%s: ........................................... (                          )' % _('Executive Director'), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 2, rowx, 5, '%s: ........................................... (                          )' % _('General Accountant'), format_filter)

        book.close()

        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()