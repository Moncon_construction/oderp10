# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Purchase Requisition",
    'version': '1.0',
    'depends': ['purchase_requisition'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
        Purchase Additional Features
    """,
    'data': [
        'views/purchase_requisition_views.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
