# -*- encoding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
import odoo.addons.decimal_precision as dp
from odoo.exceptions import UserError
from datetime import datetime

class PurchaseRequisitionLine(models.Model):
    _inherit = "purchase.requisition.line"

    @api.multi
    def compute_theoretical_quantity(self):
        for obj in self:
            total = 0.0
            warehouse_id = self.env['stock.warehouse'].search([('company_id', '=', obj.company_id.id)])
            for warehouse in warehouse_id:
                if obj.product_id:
                    product = obj.product_id
                    locations = self.env['stock.location'].search([('usage', '=', 'internal'), ('location_id', 'child_of', [warehouse.view_location_id.id])])
                    loc_ids = [loc.id for loc in locations]
                    if obj.requisition_id.date_end:
                        date_start = obj.requisition_id.date_end
                    elif obj.requisition_id.ordering_date:
                        date_start = obj.requisition_id.ordering_date
                    else:
                        date_start = datetime.now()
                    self._cr.execute("SELECT sum(quantity)::decimal(16,2) AS product_qty from stock_history "
                                     "where date <= %s "
                                     "and location_id in %s and product_id = %s ", (date_start, tuple(loc_ids), product.id))
                    fetched = self._cr.fetchall()
                    total += sum([qty[0] if qty[0] is not None else 0 for qty in fetched if fetched is not list])
            obj.theoretical_quantity = total
        
    theoretical_quantity = fields.Float('Theoretical Quantity', compute=compute_theoretical_quantity)
