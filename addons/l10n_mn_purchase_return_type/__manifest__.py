# -*- coding: utf-8 -*-

{
    'name': 'Linker Module for Purchase with Stock Return Type',
    'version': '1.0',
    'depends': [
        'l10n_mn_stock_return_type',
        'l10n_mn_purchase',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """'l10n_mn_purchase', 'l10n_mn_stock_return_type' модулиуд зэрэг суусан үед худалдан авалтын буцаалт дээр "Буцаалтын төрөл" сонгох боломжтой болгов.""",
    'data': [
        'views/purchase_refund_views.xml',
        'wizard/purchase_refund_wizard.xml',
    ],
    "auto_install": True,
    "installable": True,
}

