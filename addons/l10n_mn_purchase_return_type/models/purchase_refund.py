# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class PurchaseRefund(models.Model):
    _inherit = "purchase.refund"

    return_type_id = fields.Many2one('stock.return.type', string='Return Type')
