# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class StockReturnPicking(models.TransientModel):
    _inherit = 'stock.return.picking'

    purchase_refund_id = fields.Many2one('purchase.refund', ondelete='cascade', string='Purchase Refund')
    
    @api.multi
    def _create_returns(self):
        # Буцаалтын төрөл дамжуулав
        res = super(StockReturnPicking, self)._create_returns()
        
        if self.purchase_refund_id and self.return_type_id:
           self.purchase_refund_id.write({'return_type_id': self.return_type_id.id}) 
        
        return res