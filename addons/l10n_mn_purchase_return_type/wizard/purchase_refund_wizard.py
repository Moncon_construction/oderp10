# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError
from docx.opc import part


class PurchasePickingRefund(models.TransientModel):
    _inherit = 'purchase.picking.refund'

    return_type_id = fields.Many2one('stock.return.type', string='Return Type')

    @api.multi
    def _create_returns(self):
        self.ensure_one()
        res = super(PurchasePickingRefund, self)._create_returns()
        
        if self.return_type_id:
            refund_id =  self.env['purchase.refund'].browse(res)
            for picking_id in refund_id.picking_ids:
                picking_id.write({'return_type_id': self.return_type_id.id})
            refund_id.write({'return_type_id': self.return_type_id.id})
                
        return res