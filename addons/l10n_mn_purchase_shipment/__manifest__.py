# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-Today Asterisk Technologies LLC Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

{
    'name': "Purchase Shipment",
    'version': '1.0',
    'depends': ['l10n_mn_purchase', 'l10n_mn_purchase_line_extra_cost'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules/Product',
    'description': """
       Худалдан авалтын дахин ачилт
    """,
    'data': [
        'security/ir.model.access.csv',
        'data/purchase_shipment_sequence.xml',
        'views/purchase_shipment_view.xml',
        'wizard/purchase_shipment_wizard_view.xml',
        'report/purchase_detailed_report_by_display.xml',
        'views/purchase_order_view.xml',
        'views/purchase_shipment_line_view.xml',
        'views/stock_picking_view.xml',
        'views/stock_move_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
