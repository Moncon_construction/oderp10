# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.tools.translate import _
from odoo.addons.l10n_mn_extra_cost_item.models.extra_cost_item import SPLIT_METHOD_EC as ec_item  # @UnresolvedImport


class PurchaseExtraCost(models.Model):
    _inherit = 'purchase.extra.cost'

    shipment_id = fields.Many2one('purchase.shipment', 'Purchase Shipment', ondelete='cascade')

    @api.multi
    def calculate_shipment_cost(self):
        cost = self
        line_cost_obj = self.env['purchase.extra.cost.line']
        StockMove = self.env['stock.move']
        self.mapped('adjustment_lines').unlink()

        shipment = cost.shipment_id
        total_cost_plus = 0

        cost_line = cost

        total_qty = 0.0
        total_amount = 0.0
        total_price = 0.0
        total_weight = 0.0
        total_volume = 0.0
        total_line = 0.0

        lines = shipment.line_ids

        for line in lines:
            if line.purchase_line_id.product_id:
                total_weight += line.purchase_line_id.product_id.weight * line.purchase_line_id.product_qty
                total_volume += line.purchase_line_id.product_id.volume * line.purchase_line_id.product_qty
            total_amount += line.cost_unit * line.quantity
            total_price += line.cost_unit
            total_qty += line.quantity
            total_line += 1

        per_unit = 0
        costlineamount = cost_line.amount
        if cost_line.allocmeth == 'by_quantity':
            per_unit = (costlineamount / total_qty)
        elif cost_line.allocmeth == 'by_weight' and total_weight:
            per_unit = (costlineamount / total_weight)
        elif cost_line.allocmeth == 'by_volume' and total_volume:
            per_unit = (costlineamount / total_volume)
        elif cost_line.allocmeth == 'equal':
            per_unit = (costlineamount / total_line)
        elif cost_line.allocmeth == 'by_subtotal' and total_amount:
            per_unit = (costlineamount / total_amount)

        for line in lines:
            _cost_plus = 0
            if cost_line.allocmeth == 'by_quantity':
                _cost_plus = per_unit
            elif cost_line.allocmeth == 'by_weight':
                _cost_plus = per_unit * line.purchase_line_id.product_id.weight
            elif cost_line.allocmeth == 'by_volume':
                _cost_plus = per_unit * line.purchase_line_id.product_id.volume
            elif cost_line.allocmeth == 'equal' and per_unit:
                _cost_plus = per_unit / line.quantity
            elif cost_line.allocmeth == 'by_subtotal':
                _cost_plus = per_unit * line.cost_unit

            line_cost_obj.create({
                'purchase_line_id': line.purchase_line_id.id,
                'extra_cost_id': cost_line.id,
                'unit_cost': _cost_plus,
                'total_cost': _cost_plus * line.quantity})
            StockMove.sudo().search([('purchase_line_id', '=', line.purchase_line_id.id)]).write({'price_unit': line._compute_cost_unit_shipment()})

        self.write({'state': 'completed'})
        return True

    @api.multi
    def make_invoice(self):
        res = super(PurchaseExtraCost, self).make_invoice()
        product_uom_unit = self.env['product.uom']
        inv_obj = self.env['account.invoice']
        purchase_orders = self.env['purchase.order']
        invoice_line_ids = []
        for extra in self:
            if not extra.order_id and extra.shipment_id:
                journal_ids = self.env['account.journal'].search(
                    [('type', '=', 'purchase'),
                     ('company_id', '=', self.env.user.company_id.id)],
                    limit=1
                )
                if not journal_ids:
                    raise ValidationError(_("Define purchase journal for this company! %s (id:%d).") % (self.env.user.company_id.name, self.env.user.company_id.id))

                account = extra.partner_id.property_account_payable_id
                
                invoice_line_ids.append((0, 0, {
                    'name': extra.name,
                    'account_id': extra.account_id.id,
                    'price_unit': extra.amount or 0.0,
                    'quantity': 1.0,
                    'product_id': False,
                    'uom_id': product_uom_unit,
                    'invoice_line_tax_ids': [(6, 0, extra.taxes_id.ids)],
                    'account_analytic_id': extra.account_analytic_id.id or False}))
                inv_vals = {
                    'name': extra.shipment_id.name,
                    'reference': extra.shipment_id.name,
                    'account_id': account.id,
                    'type': 'in_invoice',
                    'partner_id': extra.partner_id.id,
                    'currency_id': extra.currency_id.id,
                    'journal_id': len(journal_ids) and journal_ids[0].id or False,
                    'invoice_line_ids': invoice_line_ids,
                    'origin': extra.shipment_id.name,
                    'payment_term_id': False,
                    'company_id': self.env.user.company_id.id,
                    'date_invoice': extra.date_invoice
                }

                inv_id = inv_obj.create(inv_vals)
                for order in purchase_orders:
                    order.invoice_ids = inv_id
                    order.write({'invoice_ids': [(4, inv_id.id)]})
                extra.write({'invoice_id': inv_id.id})
        return res


class PurchaseShipment(models.Model):
    _inherit = "purchase.shipment"

    extra_cost_ids = fields.One2many('purchase.extra.cost', 'shipment_id', string='Purchase Shipment Lines')

    @api.multi
    def button_compute_extra_cost(self):
        for shipment in self:
            for cost_line in shipment.extra_cost_ids:
                cost_line.calculate_shipment_cost()
        return True
