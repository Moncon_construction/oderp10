from odoo import models, fields, api, _


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    @api.multi
    def _compute_count_shipment(self):
        for record in self:
            record.count_shipment = len(set([line.shipment_line_id.shipment_id.id for line in record.order_line if line.shipment_line_id]))

    count_shipment = fields.Integer(compute='_compute_count_shipment')

    @api.multi
    def action_view_shipment(self):
        tree_view = self.env.ref('l10n_mn_purchase_shipment.purchase_shipment_tree_view')
        form_view = self.env.ref('l10n_mn_purchase_shipment.purchase_shipment_form_view')
        return {
            'name': _('Shipments'),
            'res_model': 'purchase.shipment',
            'views': [(tree_view.id, 'tree'), (form_view.id, 'form')],
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', [line.shipment_line_id.shipment_id.id for line in self.order_line if line.shipment_line_id])],
            'context': {
                'create': False
            }
        }

    @api.depends('order_line.invoice_lines.invoice_id.state', 'extra_cost_ids.invoice_id.state')
    def _compute_invoice(self):
        for order in self:
            invoices = self.env['account.invoice']
            for line in order.order_line:
                invoices |= line.invoice_lines.mapped('invoice_id')
                if line.shipment_id and line.shipment_id.extra_cost_ids:
                    for extra in line.shipment_id.extra_cost_ids:
                        invoices |= extra.mapped('invoice_id')
            for extra in order.extra_cost_ids:
                invoices |= extra.mapped('invoice_id')
            order.invoice_ids = invoices
            order.invoice_count = len(invoices)
