from odoo import models, fields, api
from odoo.exceptions import ValidationError


class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    shipment_line_id = fields.Many2one('purchase.shipment.line', 'Shipment Line')
    shipment_id = fields.Many2one(related='shipment_line_id.shipment_id', string='Shipment', store=True)
    cost_unit_shipment = fields.Float(related='shipment_line_id.cost_unit_shipment', string='Shipment Cost Unit')

    @api.multi
    def button_approve_line_cost(self):
        res = super(PurchaseOrderLine, self).button_approve_line_cost()
        for line in self.filtered(lambda l: l.shipment_line_id):
            line.shipment_line_id.cost_ok = True

    @api.multi
    def button_cancel_line_cost(self):
        res = super(PurchaseOrderLine, self).button_cancel_line_cost()
        for line in self.filtered(lambda l: l.shipment_line_id):
            stock_move = self.env['stock.move'].search([('purchase_line_id', '=', line.id)])
            line.shipment_line_id.cost_ok = False
