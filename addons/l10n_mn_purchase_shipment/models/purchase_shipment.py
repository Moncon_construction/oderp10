# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2016 Asterisk Technologies LLC Co.,ltd
#    (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError


class PurchaseShipment(models.Model):
    _name = "purchase.shipment"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    
    @api.multi
    def name_get(self):
        return [(record.id, record.car_id.name + ' ' + record.name) if record.car_id else (record.id, ' ' + record.name) for record in self]

    @api.multi
    def _compute_count_purchase_order(self):
        for record in self:
            record.count_purchase_order = len(set([line.purchase_line_id.order_id.id for line in record.line_ids if line.purchase_line_id]))

    name = fields.Char(string='Number', required=True, copy=False, default='New')
    date = fields.Date('Date')
    car_id = fields.Many2one('purchase.shipment.car', 'Car')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id.id)
    state = fields.Selection(
        [('loading', 'Loading'),
         ('done', 'Loaded'),
         ('cost_approved', 'Cost Approved')], string='Status', copy=False, readonly=True, track_visibility='onchange', default='loading')
    line_ids = fields.One2many('purchase.shipment.line', 'shipment_id', string='Purchase Shipment Lines', readonly=True)
    count_purchase_order = fields.Integer(compute='_compute_count_purchase_order')

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('purchase.shipment') or 'New'
        shipment = super(PurchaseShipment, self).create(vals)
        return shipment

    def unlink(self):
        # Өртөг баталсан төлөвтэй ачилтыг утгахгүй.
        for shipment in self:
            if shipment.state == 'cost_approved':
                raise UserError(_('You can not  delete cost approved shipment.'))
            else:
                super(PurchaseShipment, self).unlink()

    @api.multi
    def load_done(self):
        for obj in self:
            if not obj.line_ids:
                raise ValidationError(_("There is no shipment product!"))

            self.state = 'done'
        return {}

    @api.multi
    def load_again(self):
        self.state = 'loading'

    @api.multi
    def button_confirm_cost(self):
        self.button_compute_extra_cost()
        self.state = 'cost_approved'

    @api.multi
    def load_cancel(self):
        if self.line_ids:
            for line in self.line_ids:
                if line.purchase_line_id.qty_received!=0:
                    raise ValidationError(_("You can't cancel shipment with received products!"))
        self.state = 'loading'

    @api.multi
    def action_view_purchase_order(self):
        tree_view = self.env.ref('purchase.purchase_order_tree')
        form_view = self.env.ref('purchase.purchase_order_form')
        return {
            'name': _('Purchase orders'),
            'res_model': 'purchase.order',
            'views': [(tree_view.id, 'tree'), (form_view.id, 'form')],
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', [line.purchase_line_id.order_id.id for line in self.line_ids if line.purchase_line_id])],
            'context': {
                'create': False
            }
        }
