from odoo import models, fields


class PurchaseShipmentCar(models.Model):
    _name = "purchase.shipment.car"

    name = fields.Char('Car', size=256, required=True)
