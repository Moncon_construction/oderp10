from odoo import api, fields, models


class PurchaseShipmentLine(models.Model):
    _name = "purchase.shipment.line"

    product_box = fields.Float('Box')
    quantity = fields.Float('Quantity')
    total_qty = fields.Float('Total Quantity')
    cost_ok = fields.Boolean('Cost Approved')
    purchase_line_id = fields.Many2one('purchase.order.line', 'Purchase Order Line', domain=[('shipment_line_id', '=', None)])
    cost_unit = fields.Float(related='purchase_line_id.cost_unit', string='Calculated unit cost')
    shipment_id = fields.Many2one('purchase.shipment', 'Shipment', ondelete="cascade")
    cost_unit_shipment = fields.Float('Shipment Cost Unit', readonly=True, compute='_get_cost_unit_shipment')

    @api.multi
    @api.onchange('purchase_line_id')
    def onchange_purchase_line_id(self):
        for this in self:
            this.quantity = this.purchase_line_id.product_qty

    @api.multi
    def _get_cost_unit_shipment(self):
        for line in self:
            line.cost_unit_shipment = line._compute_cost_unit_shipment()

    @api.multi
    def _compute_cost_unit_shipment(self):
        self.ensure_one()
        line = self[0]
        cost = line.cost_unit
        if line.shipment_id.extra_cost_ids:
            for extra in line.shipment_id.extra_cost_ids:
                current_extra_cost_line = extra.adjustment_lines.filtered(lambda l: l.purchase_line_id == line.purchase_line_id)
                if current_extra_cost_line:
                    current_extra_cost_line = current_extra_cost_line[0]
                    if not extra.item_id.non_cost_calc:
                        curr_cost = current_extra_cost_line.unit_cost
                        if line.shipment_id.company_id.currency_id.id != extra.currency_id.id:
                            curr_cost = extra.currency_id.with_context(date=extra.date_invoice).compute(current_extra_cost_line.unit_cost, line.shipment_id.company_id.currency_id, round=False)
                        cost += curr_cost
        return cost

    @api.multi
    def name_get(self):
        return [(record.id, record.purchase_line_id.name) for record in self]
