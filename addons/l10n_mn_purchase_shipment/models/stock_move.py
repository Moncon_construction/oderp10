# -*- coding: utf-8 -*-
from odoo import api, models, fields, _
import logging

_logger = logging.getLogger(__name__)


class StockMove(models.Model):
    _inherit = 'stock.move'

    @api.multi
    def get_price_unit(self):
        if self.purchase_line_id and self.purchase_line_id.order_id.cost_ok and self.purchase_line_id.shipment_line_id:
            price_unit = self.purchase_line_id.shipment_line_id.cost_unit_shipment
            self.write({'price_unit': price_unit})
            return self.price_unit
        return super(StockMove, self).get_price_unit()

    shipment_line_id = fields.Many2one(related='purchase_line_id.shipment_line_id')
    shipment_id = fields.Many2one(related='shipment_line_id.shipment_id')

    @api.multi
    def check_recompute_pack_op(self):
        _logger.info(_('Function oderp: check_recompute_pack_op'))
        pickings = self.mapped('picking_id').filtered(lambda picking: picking.state not in ('waiting', 'confirmed'))  # In case of 'all at once' delivery method it should not prepare pack operations
        # Check if someone was treating the picking already
        pickings_partial = pickings.filtered(lambda picking: not any(operation.qty_done for operation in picking.pack_operation_ids))
        count_shipment = 0
        if pickings_partial:
            for picking_partial in pickings_partial:
                count_shipment += len(set([move.shipment_line_id.shipment_id.id for move in picking_partial.move_lines if move.shipment_line_id]))
        if count_shipment > 0:
            pickings_partial.do_prepare_partial_by_move()
        else:
            pickings_partial.do_prepare_partial()
        (pickings - pickings_partial).write({'recompute_pack_op': True})