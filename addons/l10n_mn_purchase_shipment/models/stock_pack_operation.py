from odoo import models, fields, api


class StockPackOperation(models.Model):
    _inherit = 'stock.pack.operation'

    @api.multi
    def _compute_shipment_line(self):
        StockMoveOperationLink = self.env['stock.move.operation.link']
        for record in self:
            link = StockMoveOperationLink.search([('operation_id', '=', record.id)])
            if len(link) == 1:
                record.shipment_line_id = link.move_id.shipment_line_id.id

    shipment_line_id = fields.Many2one('purchase.shipment.line', compute='_compute_shipment_line')
    shipment_id = fields.Many2one(related='shipment_line_id.shipment_id')
