from odoo import models, api, _
from odoo.tools.float_utils import float_compare
import logging

_logger = logging.getLogger(__name__)

class StockPicking(models.Model):
    _inherit = 'stock.picking'

    @api.multi
    def do_prepare_partial_by_move(self):
        _logger.info(_('Function Oderp: do_prepare_partial_by_move'))
        ''' Function to unlink and then create stock pack operations for each stock moves of the picking '''
        StockPackOperation = self.env['stock.pack.operation']
        StockMoveOperationLink = self.env['stock.move.operation.link']
        for record in self:
            if record.state in ('draft', 'assigned'):
                # Retreiving the old pack operations
                pack_operations_to_unlink = record.pack_operation_product_ids
                # Deleting the old pack operations
                pack_operations_to_unlink.unlink()
                # Creating pack operations for each stock move
                for move in record.move_lines:
                    new_stock_operation = StockPackOperation.create({
                        'product_qty': move.product_qty,
                        'location_id': move.location_id.id,
                        'ordered_qty': move.product_qty,
                        'date': move.date_expected,
                        'product_id': move.product_id.id,
                        'product_uom_id': move.product_uom.id,
                        'location_dest_id': move.location_dest_id.id,
                        'picking_id': move.picking_id.id,
                        'fresh_record': False
                    })
                    links = StockMoveOperationLink.search([('move_id', '=', move.id)])
                    if links:
                        for link in links:
                            link.operation_id = new_stock_operation.id
                    else:
                        StockMoveOperationLink.create({'move_id': move.id, 'operation_id': new_stock_operation.id, 'qty': move.product_qty})


    def picking_recompute_remaining_quantities(self, done_qtys=False):
        _logger.info(_('Function Oderp: picking_recompute_remaining_quantities'))
        need_rereserve = False
        all_op_processed = True
        count_shipment = len(set([move.shipment_line_id.shipment_id.id for move in self.move_lines if move.shipment_line_id]))
        if self.pack_operation_ids:
            if count_shipment > 0:
                need_rereserve, all_op_processed = self.recompute_remaining_qty_shipment(done_qtys=done_qtys)
            else:
                need_rereserve, all_op_processed = self.recompute_remaining_qty(done_qtys=done_qtys)
        return need_rereserve, all_op_processed

    def do_recompute_remaining_quantities(self, done_qtys=False):
        _logger.info(_('Function Oderp: do_recompute_remaining_quantities'))
        tmp = self.filtered(lambda picking: picking.pack_operation_ids)
        if tmp:
            for pick in tmp:
                count_shipment = len(set([move.shipment_line_id.shipment_id.id for move in pick.move_lines if move.shipment_line_id]))
                if count_shipment > 0:
                    pick.recompute_remaining_qty_shipment(done_qtys=done_qtys)
                else:
                    pick.recompute_remaining_qty(done_qtys=done_qtys)

    def recompute_remaining_qty_shipment(self, done_qtys=False):
        _logger.info(_('Function: recompute_remaining_qty_shipment'))

        # TDE CLEANME: oh dear ...
        Uom = self.env['product.uom']
        QuantPackage = self.env['stock.quant.package']
        OperationLink = self.env['stock.move.operation.link']

        quants_in_package_done = set()
        still_to_do = []
        need_rereserve = False
        # sort the operations in order to give higher priority to those with a package, then a lot/serial number
        operations = self.pack_operation_ids
        operations = sorted(operations, key=lambda x: ((x.package_id and not x.product_id) and -4 or 0) + (x.package_id and -2 or 0) + (x.pack_lot_ids and -1 or 0))
        # 1) first, try to create links when quants can be identified without any doubt
        for ops in operations:
            lot_qty = {}
            for packlot in ops.pack_lot_ids:
                lot_qty[packlot.lot_id.id] = ops.product_uom_id._compute_quantity(packlot.qty, ops.product_id.uom_id)
            # for each operation, create the links with the stock move by seeking on the matching reserved quants,
            # and deffer the operation if there is some ambiguity on the move to select
            if ops.package_id and not ops.product_id and (not done_qtys or ops.qty_done):
                # entire package
                for quant in ops.package_id.get_content():
                    remaining_qty_on_quant = quant.qty
                    if quant.reservation_id:
                        # avoid quants being counted twice
                        quants_in_package_done.add(quant.id)
                        qty_on_link = _create_link_for_quant(ops.id, quant, quant.qty)
                        remaining_qty_on_quant -= qty_on_link
                    if remaining_qty_on_quant:
                        still_to_do.append((ops, quant.product_id.id, remaining_qty_on_quant))
                        need_rereserve = True
            elif ops.product_id.id:
                # Check moves with same product
                product_qty = ops.qty_done if done_qtys else ops.product_qty
                qty_to_assign = ops.product_uom_id._compute_quantity(product_qty, ops.product_id.uom_id)
                precision_rounding = ops.product_id.uom_id.rounding
                links = OperationLink.search([('operation_id', '=', ops.id)])
                for link in links:
                    move = link.move_id
                    if qty_to_assign == 0:
                        link.qty = 0
                    for quant in move.reserved_quant_ids:
                        if float_compare(qty_to_assign, 0, precision_rounding=precision_rounding) != 1:
                            break
                        if quant.id in quants_in_package_done:
                            continue
                        # check if the quant is matching the operation details
                        if ops.package_id:
                            flag = quant.package_id == ops.package_id
                        else:
                            flag = not quant.package_id.id
                        flag = flag and (ops.owner_id.id == quant.owner_id.id) and (ops.location_id.id == quant.location_id.id)
                        if flag:
                            if not lot_qty:
                                max_qty_on_link = min(quant.qty, qty_to_assign)
                                qty_on_link = min(move.remaining_qty , max_qty_on_link)
                                qty_to_assign -= qty_on_link
                            else:
                                if lot_qty.get(quant.lot_id.id):  # if there is still some qty left
                                    max_qty_on_link = min(quant.qty, qty_to_assign, lot_qty[quant.lot_id.id])
                                    qty_on_link = min(move.remaining_qty , max_qty_on_link)
                                    qty_to_assign -= qty_on_link
                                    lot_qty[quant.lot_id.id] -= qty_on_link

                    qty_assign_cmp = float_compare(qty_to_assign, 0, precision_rounding=precision_rounding)
                    if qty_assign_cmp > 0:
                        # qty reserved is less than qty put in operations. We need to create a link but it's deferred after we processed
                        # all the quants (because they leave no choice on their related move and needs to be processed with higher priority)
                        still_to_do += [(ops, move, qty_to_assign)]
                        need_rereserve = True

        # 2) then, process the remaining part
        all_op_processed = True
        for ops, move, remaining_qty in still_to_do:
            links = OperationLink.search([('operation_id', '=', ops.id)])
            if links:
                links.unlink()
            qty_to_assign = remaining_qty
            rounding = move.product_id.uom_id.rounding
            qtyassign_cmp = float_compare(qty_to_assign, 0.0, precision_rounding=rounding)
            if qtyassign_cmp > 0:
                self.env['stock.move.operation.link'].create({'move_id': move.id, 'operation_id': ops.id, 'qty': qty_to_assign})
            all_op_processed = 0
        return (need_rereserve, all_op_processed)

    # def check_backorder(self):
    #     need_rereserve, all_op_processed = self.picking_recompute_remaining_quantities(done_qtys=True)
    #     for move in self.move_lines:
    #         if move.shipment_line_id and need_rereserve:
    #             return True
    #         if float_compare(move.remaining_qty, 0, precision_rounding=move.product_id.uom_id.rounding) != 0:
    #             return True
    #     return False