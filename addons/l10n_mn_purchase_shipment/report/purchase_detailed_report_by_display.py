# -*- encoding: utf-8 -*-
##############################################################################
import calendar
from io import BytesIO
import time
from odoo.exceptions import UserError
from datetime import date, timedelta
from operator import itemgetter
import datetime
from odoo import api, fields, models, _


class PurchaseDetailedDisplayReport(models.Model):
    """
        Худалдан авалтын дэлгэрэнгүй тайлан /дэлгэцээр/
    """
    _inherit = "purchase.detailed.report.display"

    shipment_ids = fields.Many2many('purchase.shipment', string='Shipments')

    @api.multi
    def get_select(self):
        select = super(PurchaseDetailedDisplayReport, self).get_select()
        select += ', sh.id as shipment_id '
        return select

    @api.multi
    def get_join(self):
        join = super(PurchaseDetailedDisplayReport, self).get_join()
        join += ' LEFT JOIN purchase_shipment sh ON l.shipment_id = sh.id '
        return join

    @api.multi
    def get_filter(self):
        where = super(PurchaseDetailedDisplayReport, self).get_filter()
        if self.shipment_ids:
            where += ' AND l.shipment_id IN (' + ','.join(map(str, self.shipment_ids.ids)) + ') '
        return where

    @api.multi
    def create_lines(self, data, line):
        data[0].update({
            'shipment_id': line['shipment_id']
        })
        return super(PurchaseDetailedDisplayReport, self).create_lines(data, line)


class PurchaseDetailedDisplayReportLine(models.Model):
    _inherit = 'purchase.detailed.display.report.line'

    shipment_id = fields.Many2one('purchase.shipment', string='Purchase Shipment')