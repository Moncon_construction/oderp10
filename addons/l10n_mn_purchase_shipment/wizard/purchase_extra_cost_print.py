# -*- coding: utf-8 -*-

import base64
import time
from io import BytesIO

import xlsxwriter

from odoo import _, api, fields, models
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class PurchaseExtraCostPrint(models.TransientModel):
    _inherit = "purchase.extra.cost.print"

    @api.multi
    def export(self):
        purchase = self.purchase_id
        sheetname_1 = 'Adjusted_Costs_%s' % purchase.id

        now = time.strftime('%Y-%m-%d')
        output = BytesIO()

        workbook = xlsxwriter.Workbook(output)
        worksheet = workbook.add_worksheet(sheetname_1)

        # Begin: Copied from hour_balance_print
        format_name = workbook.add_format(ReportExcelCellStyles.format_name)
        format_filter = workbook.add_format(ReportExcelCellStyles.format_filter)
        format_title = workbook.add_format(ReportExcelCellStyles.format_title)
        format_content_text = workbook.add_format(ReportExcelCellStyles.format_content_text)
        format_content_float = workbook.add_format(ReportExcelCellStyles.format_content_float)
        format_content_bold_text = workbook.add_format(ReportExcelCellStyles.format_content_bold_text)
        format_footer_float = workbook.add_format(ReportExcelCellStyles.format_footer_float)
        worksheet.set_column('A:A', 5)
        worksheet.set_column('B:B', 11)
        worksheet.set_column('C:C', 12)
        worksheet.set_column('D:D', 10)
        worksheet.set_column('E:E', 11)
        worksheet.set_column('F:F', 10)
        worksheet.set_column('G:G', 10)
        worksheet.set_column('H:H', 10)
        worksheet.set_column('I:I', 10)
        worksheet.set_column('J:J', 10)
        worksheet.set_column('K:K', 10)
        worksheet.set_column('L:L', 10)
        worksheet.set_column('M:M', 10)
        worksheet.set_column('N:N', 10)
        worksheet.set_column('O:O', 10)
        worksheet.set_column('P:P', 10)
        worksheet.set_column('Q:Q', 10)
        worksheet.set_column('R:R', 10)
        worksheet.set_column('S:S', 10)
        worksheet.set_column('T:T', 10)
        worksheet.set_column('U:U', 10)
        worksheet.set_column('V:V', 10)
        worksheet.set_column('W:W', 10)
        worksheet.set_column('X:X', 10)
        worksheet.set_column('Y:Y', 10)
        worksheet.set_column('Z:Z', 10)

        row = 1
        col = 0
        # Flag for crosscurrency
        fromcurrency = purchase.currency_id
        tocurrency = fromcurrency
        crosscurrency = False
        if purchase.company_id.currency_id.id != purchase.currency_id.id:
            crosscurrency = True
            fromcurrency = purchase.currency_id
            tocurrency = purchase.company_id.currency_id

        # create name
        worksheet.hide_gridlines('True')
        worksheet.merge_range(row - 1, 0, row - 1, 13, _('Company: %s') % (purchase.company_id.name), format_filter)
        row += 1
        worksheet.merge_range(row, 0, row, 13, _('%s Adjustment Line Document') % purchase.name, format_name)
        row += 1
        #company_currency_rate = fromcurrency.with_context(date=purchase.currency_cal_date).compute(1, tocurrency)
        company_currency_rate = purchase.currency_rate

        title_text = [_('Supplier : %s') % purchase.partner_id.name,
                      _('Order ID : %s') % purchase.name,
                      _('Order Date : %s') % purchase.date_order,
                      _('Order Currency : %s') % fromcurrency.name,
                      _('Order Currency Rate : %s') % company_currency_rate]

        # Begin: WRITE TITLE data into Excel File
        for atitle in title_text:
            worksheet.write(row + 1, col, atitle, format_filter)
            row += 1
        # End: WRITE TITLE data into Excel File

        # Begin: Setting Header for list data
        # List subcategory
        subheader_list = []
        if crosscurrency:
            subheader_list = [fromcurrency.name,
                              tocurrency.name]
        else:
            subheader_list = [fromcurrency.name]
        
        allocmeths = []
        extra_cost_ids = []
        for extra in purchase.extra_cost_ids:
            extra_cost_ids.append(extra)
            if extra.allocmeth == 'by_weight' and 'by_weight' not in allocmeths:
                allocmeths.append('by_weight')
            elif extra.allocmeth == 'by_volume' and 'by_volume' not in allocmeths:
                allocmeths.append('by_volume')

        data_dict = {}
        allocmeth_column = {}
        
        for alloc in allocmeths:
            if alloc == 'by_weight':
                print 'by_weight..1'
                data_dict[_('Weight')] = {}
                allocmeth_column = {_('Weight'): None}
            elif alloc == 'by_volume':
                print 'by_volume..1'
                data_dict[_('Volume')] = {}
                allocmeth_column = {_('Volume'): None}

        header_list = [{_('OrderNum'): None},
                       {_('Barcode'): None},
                       {_('Product'): None},
                       allocmeth_column,
                       {_('Unit Price'): subheader_list},
                       {_('Quantity'): None},
                       {_('Total Price'): subheader_list}]

        data_dict[_('OrderNum')] = {}
        data_dict[_('Barcode')] = {}
        data_dict[_('Product')] = {}
        
        data_dict[_('Quantity')] = {}

        for asub in subheader_list:
            data_dict[_('Unit Price') + asub] = {}
            data_dict[_('Total Price') + asub] = {}

        # List subcategory
        subheader_list = [_('Unit Extra Cost'),
                          _('Total Extra Cost')]

        line_record_number = 0
        # BEGIN: LOOP THROUGH ORDER LINE
        ttl_NonCostCalc = _('Non Cost Calculated (%s)') % tocurrency.name
        ttl_NonCostCalc0 = _('%s%s') % (ttl_NonCostCalc, subheader_list[0])
        ttl_NonCostCalc1 = _('%s%s') % (ttl_NonCostCalc, subheader_list[1])

        data_dict[ttl_NonCostCalc0] = {}
        data_dict[ttl_NonCostCalc1] = {}
        data_dict[_('Cost') + subheader_list[0]] = {}
        data_dict[_('Cost') + subheader_list[1]] = {}

        for line in purchase.order_line:
            for cost in line.po_line_cost_ids:
                if cost.extra_cost_id.shipment_id:
                    for extra in cost.extra_cost_id.shipment_id.extra_cost_ids:
                        if extra not in extra_cost_ids:
                            extra_cost_ids.append(extra)


        for line in purchase.order_line:
            line_record_number += 1
            # BEGIN: Tax Calculation
            curr_taxes = line.taxes_id.compute_all(line.price_unit, line.currency_id, line.product_qty)
            price_unit = (line.product_qty > 0 and curr_taxes['total_excluded'] / line.product_qty) or 0
            price_total = price_unit * line.product_qty
            currunitprice = price_unit
            currtotalprice = price_total
            # ----------
            taxed = curr_taxes['total_included'] / line.product_qty
            tax = taxed - price_unit
            # END: Tax Calculation

            # BEGIN: setting blue header data
            data_dict[_('OrderNum')][line_record_number] = line_record_number
            data_dict[_('Barcode')][line_record_number] = line.product_id.barcode
            data_dict[_('Product')][line_record_number] = line.name
            
            for alloc in allocmeths:
                if alloc == 'by_weight':
                    print 'by_weight..2'
                    data_dict[_('Weight')][line_record_number] = line.product_id.weight
                elif alloc == 'by_volume':
                    print 'by_volume..2'
                    data_dict[_('Volume')][line_record_number] = line.product_id.volume
            
            data_dict[_('Quantity')][line_record_number] = line.product_qty
            data_dict[_('Unit Price') + fromcurrency.name][line_record_number] = price_unit
            data_dict[_('Total Price') + fromcurrency.name][line_record_number] = price_total
            if crosscurrency:
                #tax = fromcurrency.with_context(date=purchase.currency_cal_date).compute(tax, tocurrency)
                tax = tax * company_currency_rate
                #currunitprice = fromcurrency.with_context(date=purchase.currency_cal_date).compute(price_unit, tocurrency)
                currunitprice = price_unit * company_currency_rate
                #currtotalprice = fromcurrency.with_context(date=purchase.currency_cal_date).compute(price_total, tocurrency)
                currtotalprice = price_total * company_currency_rate
                data_dict[_('Unit Price') + tocurrency.name][line_record_number] = currunitprice
                data_dict[_('Total Price') + tocurrency.name][line_record_number] = currtotalprice
            # END: setting blue header data
            cost_unit = line.cost_unit
            data_dict[_('Cost') + subheader_list[0]][line_record_number] = cost_unit
            data_dict[_('Cost') + subheader_list[1]][line_record_number] = cost_unit * line.product_qty
            data_dict[ttl_NonCostCalc0][line_record_number] = tax
            data_dict[ttl_NonCostCalc1][line_record_number] = tax * line.product_qty

                
        liner = 0
        # Begin: Set Pivot data Header for each type of extra cost recorded
        for extra in extra_cost_ids:
            liner = cline = 1
            if not extra.item_id.non_cost_calc:
                header_fieldname = _('%s (%s)') % (extra.name, extra.currency_id.name)
                header_list.append({header_fieldname: subheader_list})
                data_dict[_('%s%s') % (header_fieldname, subheader_list[0])] = {}
                data_dict[_('%s%s') % (header_fieldname, subheader_list[1])] = {}
                liner2 = 0
                # Begin: Calculate Data for this Column
                for line in extra.adjustment_lines:
                    data_dict[_('%s%s') % (header_fieldname, subheader_list[0])][liner + liner2] = line.unit_cost
                    data_dict[_('%s%s') % (header_fieldname, subheader_list[1])][liner + liner2] = line.total_cost
                    liner2 += 1
                    # End: Calculate Data for this Column
                cline2 = 0
                if tocurrency.id != extra.currency_id.id:
                    #curr_currency_rate = extra.currency_id.with_context(date=extra.date_invoice).compute(1, extra.order_id.company_id.currency_id)
                    curr_currency_rate = extra.currency_rate

                    header_fieldname = _('%s (%s Rate for %s: %s)') % (extra.name, tocurrency.name, extra.date_invoice, curr_currency_rate)
                    header_list.append({header_fieldname: subheader_list})
                    data_dict[_('%s%s') % (header_fieldname, subheader_list[0])] = {}
                    data_dict[_('%s%s') % (header_fieldname, subheader_list[1])] = {}
                    
                    for line in extra.adjustment_lines:
                        # Begin: Calculate Data for this Column
                        currextralineunitcost = line.unit_cost * curr_currency_rate
                        currextralinetotalcost = line.total_cost * curr_currency_rate
                        data_dict[_('%s%s') % (header_fieldname, subheader_list[0])][cline + cline2] = currextralineunitcost
                        data_dict[_('%s%s') % (header_fieldname, subheader_list[1])][cline + cline2] = currextralinetotalcost
                        cline2 += 1
                # End: Calculate Data for this Column
                liner = 0
            else:
                for line in extra.adjustment_lines:
                    data_dict[ttl_NonCostCalc0][line_record_number] += line.unit_cost
                    data_dict[ttl_NonCostCalc1][line_record_number] += line.total_cost
        
        # End: Set Pivot data Header for each type of extra cost recorded
        header_list.append({ttl_NonCostCalc: subheader_list})
        header_list.append({_('Cost'): subheader_list})
    # END: LOOP THROUGH ORDER LINE

        # Begin: WRITE DATA INTO EXCEL FILE
        row += 2
        totrow = headrow = 0
        for headeritem in header_list:
            for keyheader, aheader in headeritem.iteritems():
                subcol = 0
                if aheader is None:
                    # Print single header with merge row
                    column_total = 0
                    worksheet.merge_range(row, col, row + 1, col, keyheader, format_title)
                    # BEGIN: PRINT OUT DATA
                    # Further Notice: See towards elimination
                    currheader_name = keyheader
                    for currrownum, currrowdata in data_dict[currheader_name].iteritems():
                        row_for_data = row + 1 + currrownum
                        worksheet.write(row_for_data, col, currrowdata, format_content_text)
                        totrow += 1
                        if currheader_name == _('Quantity'):
                            column_total += currrowdata
                    headrow += 1
                    footer_data = _('-')
                    if currheader_name == _('Product'):
                        footer_data = _('Total')
                    if currheader_name == _('Quantity'):
                        footer_data = _(column_total)
                    worksheet.write(row_for_data + 1, col, footer_data, format_footer_float)
                    # END: PRINT OUT DATA
                else:
                    for subcatitem in aheader:
                        # Print header
                        worksheet.write(row + 1, col + subcol, subcatitem, format_title)
                        subcol += 1
                        column_total = 0
                        # BEGIN: PRINT OUT DATA
                        currheader_name = _('%s%s') % (keyheader, subcatitem)
                        for currrownum, currrowdata in data_dict[currheader_name].iteritems():
                            row_for_data = row + 1 + currrownum
                            col_for_data = col + subcol - 1
                            worksheet.write(row_for_data, col_for_data, currrowdata, format_content_float)
                            column_total += currrowdata
                        # -------Set Footer-----------
                        worksheet.write(row_for_data + 1, col_for_data, column_total, format_footer_float)
                        # END: PRINT OUT DATA
                    subcol = subcol - 1
                    if subcol > 0:
                        # Print header
                        worksheet.merge_range(row, col, row, col + subcol, keyheader, format_title)
                    else:
                        # Print header
                        worksheet.write(row, col, keyheader, format_title)
                col += subcol + 1
        # ----Set Signature
        totrow = totrow / headrow + 5
        worksheet.write(row + totrow, 1, _("Cost Adjusted By: .......................................\nAccountant: ................."), format_filter)
        worksheet.write(row + totrow, 6, _("Cost Supervised By: .......................................\nChief Accountant: ..................."), format_filter)
        # End: WRITE DATA INTO EXCEL FILE
        # Formatting: Enlarge Header rows
        worksheet.set_row(row + totrow, 25)
        worksheet.set_row(row, 30)
        worksheet.set_row(row + 1, 40)
        # End: Setting Header for list data

        # Section Amended towards Workbook Export Change
        workbook.close()
        out = base64.encodestring(output.getvalue())
        file_name = 'purchase_extra_cost_print_%s' % now

        excel_id = self.env['oderp.report.excel.output'].create({
            'filedata': out,
            'filename': file_name + '.xlsx'
        })

        return {
            'name': 'Export Result',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'oderp.report.excel.output',
            'res_id': excel_id.id,
            'view_id': False,
            'context': self._context,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'nodestroy': True,
        }
