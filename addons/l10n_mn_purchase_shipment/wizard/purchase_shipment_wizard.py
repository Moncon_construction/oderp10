# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-Today Asterisk Technologies LLC Co.,ltd
#    (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class PurchaseShipmentWizard(models.TransientModel):
    _name = "purchase.shipment.wizard"
    _description = "Purchase Shipment Wizard"

    date = fields.Date('Date')
    car_id = fields.Many2one('purchase.shipment.car', 'Car')
    add_to_shipment = fields.Boolean('Add to Shipment')
    shipment_id = fields.Many2one('purchase.shipment', 'Shipment', domain=[('state', '=', 'loading')])

    line_ids = fields.One2many('purchase.shipment.wizard.line', 'wizard_id', string='Shipment Lines')

    @api.multi
    def to_ship(self):
        StockMove = self.env['stock.move']
        purchase_line_obj = self.env['purchase.order.line']
        purchase_obj = self.env['purchase.order']
        shipment_line_obj = self.env['purchase.shipment.line']
        shipment_obj = self.env['purchase.shipment']
        StockMoveOperationLink = self.env['stock.move.operation.link']
        StockPackOperation = self.env['stock.pack.operation']

        form = self

        if form.add_to_shipment and not form.shipment_id:
            raise ValidationError(_("Please select shipment!"))
        elif not form.add_to_shipment and (not form.car_id or not form.date):
            raise ValidationError(_("Please select car or date!"))

        shipment = None
        if not form.add_to_shipment:
            shipment = shipment_obj.create({'car_id': form.car_id.id,
                                            'date': form.date})
        else:
            shipment = form.shipment_id

        selected_lines = {}

        for line in form.line_ids:

            stock_move = StockMove.search([('purchase_line_id', '=', line.purchase_line_id.id)])
            if stock_move:
                stock_move = stock_move[0]
            else:
                raise ValidationError(_("Please confirm purchase order first!"))

            if line.purchase_line_id.product_qty != line.quantity:
                if line.purchase_line_id.product_qty < line.quantity:
                    raise ValidationError(_("Quantity of shipment exceeds product quantity of purchase! [%s] %s") % (line.purchase_line_id.product_id.default_code, line.purchase_line_id.product_id.name))

                # Ачиж буй тоо нь худалдан авалтын мөрийн тооноос бага бол мөрийг 2 салгана.
                purchase_line_vals = {
                    'order_id': line.purchase_line_id.order_id.id,
                    'product_id': line.purchase_line_id.product_id.id,
                    'product_uom': line.purchase_line_id.product_uom.id,
                    'price_unit': line.purchase_line_id.price_unit,
                    'product_qty': line.purchase_line_id.product_qty - line.quantity,
                    'partner_id': line.purchase_line_id.partner_id.id,
                    'name': line.purchase_line_id.name,
                    'date_planned': line.purchase_line_id.date_planned,
                    'company_id': line.purchase_line_id.company_id.id,
                    'state': line.purchase_line_id.state,
                    'account_analytic_id': line.purchase_line_id.account_analytic_id.id,
                    'shipment_line_id': line.purchase_line_id.shipment_line_id.id,
                    'shipment_id': line.purchase_line_id.shipment_id.id,
                    'cost_ok': line.purchase_line_id.cost_ok,
                    'cost_approved_by': line.purchase_line_id.cost_approved_by.id if line.purchase_line_id.cost_approved_by else None,
                    'cost_approved_date': line.purchase_line_id.cost_approved_date,
                    'taxes_id': [(6, 0, line.purchase_line_id.taxes_id.ids)]
                }
                if 'vat_indication_id' in line.purchase_line_id:
                    purchase_line_vals['vat_indication_id'] = line.purchase_line_id.vat_indication_id.id
                stock_move.product_uom_qty = line.quantity
                line.purchase_line_id.product_qty = line.quantity
                new_line_id = purchase_line_obj.create(purchase_line_vals)  # Энэ ажилснаар stock pack operation-ийг бүхэлд нь шинээр үүсгэж байгаа.
                stock_move.picking_id.do_prepare_partial_by_move()
            new_shipment_line = shipment_line_obj.create({'shipment_id': shipment.id,
                                                          'purchase_line_id': line.purchase_line_id.id,
                                                          'product_box': line.product_box,
                                                          'quantity': line.quantity,
                                                          'total_qty': line.total_qty,
                                                          })

            line.purchase_line_id.shipment_line_id = new_shipment_line.id
        return True

    @api.model
    def default_get(self, fields):
        res = super(PurchaseShipmentWizard, self).default_get(fields)
        lines = []
        if 'active_model' in self._context and self._context['active_model'] == 'purchase.order' and 'active_ids' in self._context:
            for line in self.env['purchase.order'].browse(self._context['active_ids']).mapped('order_line'):
                if not line.cost_ok and not line.shipment_line_id:
                    lines.append((0, 0, {
                        'purchase_line_id': line.id,
                        'quantity': line.product_qty
                    }))
            res['line_ids'] = lines
        return res
