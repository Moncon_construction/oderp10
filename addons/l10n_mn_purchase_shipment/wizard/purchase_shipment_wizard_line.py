from odoo import models, fields, api


class PurchaseShipmentWizardLine(models.TransientModel):
    _name = "purchase.shipment.wizard.line"
    _description = "Purchase Shipment Wizard Line"

    @api.multi
    def _domain_purchase_order_line(self):
        ids = []
        if self._context and 'active_ids' in self._context:
            for order in self.env['purchase.order'].browse(self._context['active_ids']):
                for line in order.order_line:
                    if not line.cost_ok:
                        ids.append(line.id)
        return [('id', 'in', ids)]

    purchase_line_id = fields.Many2one('purchase.order.line', 'Purchase Order Line', domain=_domain_purchase_order_line, required=True)
    product_box = fields.Float('Box')
    quantity = fields.Float('Quantity')
    total_qty = fields.Float(compute='_compute_total_qty', string='Total Quantity')
    wizard_id = fields.Many2one('purchase.shipment.wizard', 'Shipment Wizard')

    @api.one
    @api.depends('product_box', 'quantity')
    def _compute_total_qty(self):
        for obj in self:
            obj.total_qty = obj.quantity * obj.product_box

    @api.multi
    @api.onchange('purchase_line_id')
    def onchange_purchase_line_id(self):
        for obj in self:
            obj.quantity = obj.purchase_line_id.product_qty
