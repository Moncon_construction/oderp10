# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Purchase Stock Report",
    'version': '1.0',
    'depends': ['l10n_mn_purchase_extra_cost'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Stock Modules',
    'description': """
        Замд яваа барааны тайлан
        Худалдан авалтын ханшийн зөрүүний тайлан
        Орлогын товчоо тайлан
    """,
    'data': [
        'security/ir.model.access.csv',
        'wizard/moving_materials_report.xml',
        'views/report_purchase_exchange_rate_wizard.xml',
    ]
}
