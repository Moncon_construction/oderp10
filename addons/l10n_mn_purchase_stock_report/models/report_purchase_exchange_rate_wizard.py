# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from operator import itemgetter
from odoo.exceptions import ValidationError, UserError

from odoo import api, fields, models, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles

from datetime import datetime


class ReportPurchaseExchangeRate(models.TransientModel):
    """
        Замд яваа барааны тайлан-Худалдан авалтын ханшийн зөрүүний тайлан.
    """
    
    _name = 'report.purchase.exchange.rate'
    _description = "Purchase exchange rate difference report."

    date_from = fields.Date("Start Date", required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    date_to = fields.Date("End Date", required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse')
    partner_id = fields.Many2one('res.partner', string='Partner')
    product_ids = fields.Many2many(comodel_name='product.product', string='Product')
    type = fields.Selection([('summary', 'Summary'),
                            ('detailed', 'Detailed')], string='Report Type', required=True, default='summary')

    @api.multi
    def export_report(self):
        #create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        #create name
        report_name = _('Purchase exchange rate difference report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        
        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_filter_right = book.add_format(ReportExcelCellStyles.format_filter_right)
        format_title_float = book.add_format({
                                                'font_name': 'Times New Roman',
                                                'font_size': 9,
                                                'align': 'right',
                                                'valign': 'vcenter',
                                                'border': 1,
                                                'bg_color': '#83CAFF',
                                                'num_format': '#,##0.00'
                                            })
        format_title_small_left = book.add_format({
                                                'font_name': 'Times New Roman',
                                                'font_size': 10,
                                                'bold': True,
                                                'align': 'left',
                                                'valign': 'vcenter',
                                                'border': 1,
                                                'text_wrap':1,
                                                'bg_color': '#83CAFF'
                                            })
        format_group = book.add_format(ReportExcelCellStyles.format_group)
        format_group_number = book.add_format(ReportExcelCellStyles.format_group_number)
        format_group_left = book.add_format(ReportExcelCellStyles.format_group_left)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_center = book.add_format(ReportExcelCellStyles.format_content_center)
        format_title_small = book.add_format(ReportExcelCellStyles.format_title_small)
        report_type = ''
        if self.type == 'summary':
            report_type = ' (Summary)'
        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('purchase_exchange_rate' + report_type), form_title=file_name).create({})

        # execute query
        where = ''
        if self.warehouse_id:
            where += ' AND m.warehouse_id = %s' % self.warehouse_id.id
        if self.partner_id:
            where += ' AND o.partner_id = %s' % self.partner_id.id
        if self.product_ids:
            where += ' AND m.product_id IN (' + ','.join(str(elem['id']) for elem in self.product_ids) + ')'
        # if self.type :
        self.env.cr.execute("""SELECT\
                                       r.id as partner_id,\
                                       o.id as order_id,\
                                       r.name as partner,\
                                       o.date_order as date_order,\
                                       o.name as order_name,\
                                       w.name as warehouse,\
                                       m.name as product_name,\
                                       l.product_qty as purchase_qty,\
                                       l.qty_received as received_qty,\
                                       l.id as purchase_price,\
                                       m.date as move_date,\
                                       p.name as move_name,\
                                       m.price_unit as move_price,\
                                       m.product_qty as move_qty,\
                                       m.product_id as product_id\
                                   FROM purchase_order_line l\
                                   LEFT JOIN purchase_order o ON l.order_id = o.id\
                                   LEFT JOIN stock_move m ON m.purchase_line_id = l.id\
                                   LEFT JOIN stock_picking p ON m.picking_id = p.id\
                                   LEFT JOIN stock_warehouse w ON m.warehouse_id = w.id\
                                   LEFT JOIN res_partner r ON o.partner_id = r.id\
                                   LEFT JOIN res_currency c ON l.currency_id = c.id\
                                   WHERE m.state = \'done\' 
                                       AND c.name != \'MNT\' %s \
                                       AND o.date_order >=%s AND o.date_order<=%s\
                                   ORDER BY r.id,o.id,m.name desc""" % (where, '\'' + self.date_from + ' 00:00:00\'', '\'' + self.date_to + ' 23:59:59\''))

        result = self.env.cr.dictfetchall()
        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 1
        colx_number = 13

        # create name
        company = self.env.user.company_id
        sheet.merge_range(rowx, 0, rowx, 3, '%s: %s' % (_('Company'), company.name), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 0, rowx + 1, colx_number, report_name.upper(), format_name)
        rowx += 2

        # create duration
        sheet.merge_range(rowx, 0, rowx, 3, '%s: %s - %s' % (_('Duration'), self.date_from, self.date_to), format_filter)

        # create printed user
        if self.env.user.parent_name:
            user_name = '%s.%s' % (self.env.user.name, self.env.user.parent_name[:1])
        else:
            user_name = '%s' % self.env.user.name
        sheet.merge_range(rowx, 11, rowx, 13, '%s: %s' % (_('Printed by'), user_name), format_filter_right)
        rowx += 2

        seq = 1
        seq_order = 1
        seq_product = 1
        total_diff_price = 0
        product_diff = 0
        diff_price = 0
        product_diff_qty = 0
        current_partner_id = 0
        current_order_id = 0
        current_product_id = 0
        if self.type == 'summary':
            # compute column
            colx_number = 5
            sheet.set_column('A:A', 4)
            sheet.set_column('B:B', 9)
            sheet.set_column('C:C', 7)
            sheet.set_column('D:D', 7)
            sheet.set_column('E:E', 15)
            sheet.set_column('F:F', 12)

            sheet.write(rowx, 0, _('Seq'), format_title_small)
            sheet.write(rowx, 1, _('Partner'), format_title_small)
            sheet.write(rowx, 2, _('Order'), format_title_small)
            sheet.write(rowx, 3, _('Date'), format_title_small)
            sheet.write(rowx, 4, _('Warehouse'), format_title_small)
            sheet.write(rowx, 5, _('Total Difference'), format_title_small)
            sheet.set_row(rowx, 40)
            rowx += 1

            for rs in result:
                if current_partner_id != rs['partner_id']:
                    if current_partner_id != 0:
                        total_diff_price += product_diff
                        diff_price += product_diff
                        sheet.write(rowx-1, colx_number, diff_price, format_group_float)
                        product_diff = 0
                        diff_price = 0
                        seq_order += 1

                        sheet.write(rowx, 0, '', format_title_small_left)
                        sheet.merge_range(rowx, 1, rowx, colx_number - 1, self.env['res.partner'].search([('id','=',current_partner_id)]).name, format_title_small_left)
                        sheet.write(rowx, colx_number, total_diff_price, format_title_float)
                        rowx += 1

                    sheet.write(rowx, 0, seq, format_title_small)
                    sheet.merge_range(rowx, 1, rowx, colx_number, rs['partner'], format_title_small_left)

                    current_partner_id = rs['partner_id']
                    current_order_id = 0
                    total_diff_price = 0

                    seq_order = 1
                    seq += 1
                    rowx += 1
                if current_order_id != rs['order_id']:
                    if current_order_id != 0:
                        total_diff_price += product_diff
                        diff_price += product_diff
                        sheet.write(rowx-1, colx_number, diff_price, format_group_float)
                        product_diff = 0
                        diff_price = 0

                    current_order_id = rs['order_id']
                    current_product_id = 0

                    sheet.write(rowx, 0, '', format_group)
                    sheet.write(rowx, 1, '%s.%s' % (seq-1, seq_order), format_group)
                    sheet.write(rowx, 2, rs['order_name'], format_group)
                    date = datetime.strptime(rs['date_order'], '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d')
                    sheet.write(rowx, 3, date, format_group)
                    sheet.write(rowx, 4, rs['warehouse'], format_group)
                    seq_order += 1
                    rowx += 1

                if current_product_id != rs['product_id']:
                    current_product_id = rs['product_id']
                    product_diff_qty = rs['purchase_qty']
                    diff_price += product_diff
                    po_line = self.env['purchase.order.line'].search([('id','=',rs['purchase_price'])])
                    rs['purchase_price'] = po_line._get_price_unit_with_calculated_extra_cost()
                    product_diff = rs['purchase_price'] * rs['purchase_qty']

                product_diff_qty -= abs(rs['move_qty'])
                product_diff -= abs(rs['move_price'] * rs['move_qty'])
                if result.index(rs) == len(result)-1:
                    total_diff_price += product_diff
                    product_diff = 0

                    sheet.write(rowx-1, colx_number, diff_price, format_group_float)
                    diff_price = 0

                    sheet.write(rowx, 0, '', format_title_small_left)
                    sheet.merge_range(rowx, 1, rowx, colx_number - 1, rs['partner'], format_title_small_left)
                    sheet.write(rowx, colx_number, total_diff_price, format_title_float)
                    rowx += 1
        else:
            # compute column
            sheet.set_column('A:A', 4)
            sheet.set_column('B:B', 7)
            sheet.set_column('C:C', 7)
            sheet.set_column('D:D', 15)
            sheet.set_column('E:E', 5)
            sheet.set_column('F:F', 10)
            sheet.set_column('G:G', 10)
            sheet.set_column('H:H', 10)
            sheet.set_column('I:I', 10)
            sheet.set_column('J:J', 5)
            sheet.set_column('K:K', 10)
            sheet.set_column('L:L', 10)
            sheet.set_column('M:M', 5)
            sheet.set_column('N:N', 10)

            sheet.merge_range(rowx, 0, rowx+1, 0, _('Seq'), format_title_small)
            sheet.merge_range(rowx, 1, rowx, 5, _('Purchase'), format_title)
            sheet.merge_range(rowx, 6, rowx, 11, _('Withdrawal'), format_title)
            sheet.merge_range(rowx, 12, rowx, 13, _('Difference'), format_title)
            rowx += 1
            sheet.set_row(rowx, 40)
            sheet.write(rowx, 1, _('Partner'), format_title_small)
            sheet.write(rowx, 2, _('Order'), format_title_small)
            sheet.write(rowx, 3, _('Product'), format_title_small)
            sheet.write(rowx, 4,  _('Quantity'), format_title_small)
            sheet.write(rowx, 5, _('Unit Price'), format_title_small)
            sheet.write(rowx, 6, _('Total Price'), format_title_small)
            sheet.write(rowx, 7,  _('Date'), format_title_small)
            sheet.write(rowx, 8,  _('Withdrawal Receipt'), format_title_small)
            sheet.write(rowx, 9, _('Quantity'), format_title_small)
            sheet.write(rowx, 10, _('Unit Price'), format_title_small)
            sheet.write(rowx, 11, _('Total Price'), format_title_small)
            sheet.write(rowx, 12, _('Quantity'), format_title_small)
            sheet.write(rowx, 13, _('Total Difference'), format_title_small)
            rowx += 1

            for rs in result:
                if current_partner_id != rs['partner_id']:
                    if current_partner_id != 0:
                        total_diff_price += product_diff
                        product_diff = 0

                        sheet.write(rowx, 0, '', format_group)
                        sheet.write(rowx, 1, '', format_group)
                        sheet.write(rowx, 2, self.env['purchase.order'].search([('id','=',current_order_id)]).name, format_group)
                        sheet.merge_range(rowx, 3, rowx, 6, _('Amount'), format_group)
                        sheet.merge_range(rowx, 7, rowx, 12, '', format_group)
                        sheet.write(rowx, 13, diff_price, format_group_float)
                        diff_price = 0
                        rowx += 1

                        sheet.write(rowx, 0, '', format_title_small_left)
                        sheet.merge_range(rowx, 1, rowx, 12, self.env['res.partner'].search([('id','=',current_partner_id)]).name, format_title_small_left)
                        sheet.write(rowx, 13, total_diff_price, format_title_float)
                        rowx += 1

                    sheet.write(rowx, 0, seq, format_title_small)
                    sheet.merge_range(rowx, 1, rowx, colx_number, rs['partner'], format_title_small_left)

                    current_partner_id = rs['partner_id']
                    current_order_id = 0
                    total_diff_price = 0

                    seq_order = 1
                    seq += 1
                    rowx += 1
                if current_order_id != rs['order_id']:
                    if current_order_id != 0:
                        total_diff_price += product_diff
                        product_diff = 0

                        sheet.write(rowx, 0, '', format_group)
                        sheet.write(rowx, 1, '', format_group)
                        sheet.write(rowx, 2, self.env['purchase.order'].search([('id','=',current_order_id)]).name, format_group)
                        sheet.merge_range(rowx, 3, rowx, 6, _('Amount'), format_group)
                        sheet.merge_range(rowx, 7, rowx, 12, '', format_group)
                        sheet.write(rowx, 13, product_diff, format_group_float)
                        diff_price = 0
                        rowx += 1

                    current_order_id = rs['order_id']
                    current_product_id = 0

                    sheet.write(rowx, 0, '', format_group)
                    sheet.write(rowx, 1, '%s.%s' % (seq-1, seq_order), format_group_number)
                    date = datetime.strptime(rs['date_order'], '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d')
                    sheet.merge_range(rowx, 2, rowx, colx_number, '%s \t %s \t %s' % (date, rs['order_name'], rs['warehouse']), format_group_left)

                    seq_product = 1
                    seq_order += 1
                    rowx += 1

                sheet.write(rowx, 0, '', format_content_text)
                sheet.write(rowx, 1, '', format_content_text)
                if current_product_id != rs['product_id']:
                    current_product_id = rs['product_id']
                    sheet.write(rowx, 2, '%s.%s.%s' % (seq-1, seq_order-1, seq_product), format_content_number)
                    sheet.set_row(rowx, 40)
                    sheet.write(rowx, 3, rs['product_name'], format_content_text)
                    sheet.write(rowx, 4, rs['purchase_qty'], format_content_number)
                    po_line = self.env['purchase.order.line'].search([('id', '=', rs['purchase_price'])])
                    rs['purchase_price'] = po_line._get_price_unit_with_calculated_extra_cost()
                    sheet.write(rowx, 5, rs['purchase_price'], format_content_float)
                    sheet.write(rowx, 6, rs['purchase_price'] * rs['purchase_qty'], format_content_float)

                    diff_price += product_diff

                    product_diff_qty = rs['purchase_qty']
                    product_diff = rs['purchase_price'] * rs['purchase_qty']

                    seq_product += 1
                else:
                    sheet.write(rowx, 2, '', format_content_text)
                    sheet.write(rowx, 3, '', format_content_text)
                    sheet.write(rowx, 4, '', format_content_text)
                    sheet.write(rowx, 5, '', format_content_text)
                    sheet.write(rowx, 6, '', format_content_text)
                date = datetime.strptime(rs['move_date'], '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d')
                sheet.write(rowx, 7, date, format_content_center)
                sheet.write(rowx, 8, rs['move_name'], format_content_center)
                sheet.write(rowx, 9, rs['move_qty'], format_content_number)
                sheet.write(rowx, 10, rs['move_price'], format_content_float)
                sheet.write(rowx, 11, rs['move_price'] * rs['move_qty'], format_content_float)
                product_diff_qty -= abs(rs['move_qty'])
                sheet.write(rowx, 12, product_diff_qty, format_content_number)
                product_diff -= abs(rs['move_price'] * rs['move_qty'])
                sheet.write(rowx, 13, product_diff, format_content_float)

                rowx += 1
                if result.index(rs) == len(result)-1:
                    total_diff_price += product_diff
                    product_diff = 0

                    sheet.write(rowx, 0, '', format_group)
                    sheet.write(rowx, 1, '', format_group)
                    sheet.write(rowx, 2, rs['order_name'], format_group)
                    sheet.merge_range(rowx, 3, rowx, 6, _('Amount'), format_group)
                    sheet.merge_range(rowx, 7, rowx, 12, '', format_group)
                    sheet.write(rowx, 13, diff_price, format_group_float)
                    diff_price = 0
                    rowx += 1

                    sheet.write(rowx, 0, '', format_title_small_left)
                    sheet.merge_range(rowx, 1, rowx, 12, rs['partner'], format_title_small_left)
                    sheet.write(rowx, 13, total_diff_price, format_title_float)
                    rowx += 1
        book.close()

        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        
        # call export function
        return report_excel_output_obj.export_report()    
    

