# -*- coding: utf-8 -*-
import base64
from datetime import datetime
from io import BytesIO
import math
import time
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell

from odoo import _
from odoo import fields, models, api
from odoo.exceptions import UserError


class MovingMaterialsReport(models.TransientModel):
    """
         Moving Materials Report - wizard
    """
    _name = "moving.materials.report"
    _description = "Moving Materials Report"

    date_start = fields.Date(string='Date start', required=True, help="Purchase Order Ordered Date")
    date_end = fields.Date(string='Date end', required=True, help="Purchase Order Ordered Date")
    company = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    
    warehouses = fields.Many2many('stock.warehouse', 'moving_materials_report_to_warehouse', 'report_id', 'warehouse_id', string="Warehouses")
    categories = fields.Many2many('product.category', 'moving_materials_report_to_category', 'report_id', 'category_id', string="Category")
    products = fields.Many2many('product.product', 'moving_materials_report_to_product_product', 'report_id', 'product_id', string="Products")
    partners = fields.Many2many('res.partner', 'moving_materials_report_to_partner', 'report_id', 'partner_id', string="Partners")
    
    @api.onchange("company")
    def _get_department_domain(self):
        domain = {}
        if self.company:
            # SET warehouse domain
            domain['warehouses'] = [('company_id','=', self.company.id)]
            _warehouses = []
            for warehouse in self.env.user.allowed_warehouses:
                _warehouses.append(warehouse.id)
            if _warehouses:
                domain['warehouses'] = [('id', 'in', _warehouses), ('company_id','=', self.company.id)]

            # SET others domain
            domain['categories'] = [('company_id','=', self.company.id)]
            domain['products'] = [('product_tmpl_id.company_id','=', self.company.id)]
        return {'domain': domain}
    
    @api.onchange("categories")
    def _get_product_domain(self):
        domain = {}
        if self.categories and len(self.categories) > 0:
            domain['products'] = [('product_tmpl_id.categ_id', 'in', self.categories.ids)]
        return {'domain': domain}
    
    @api.onchange('date_start', 'date_end')
    def validate_date_range_start(self):
        if self.date_start and self.date_end:
            if self.date_start > self.date_end:
                raise UserError('Invalid date range, Please enter a date start less than or equal to date end')

    def  get_xsl_column_name(self, index):
        alphabet = {'0': 'A',  '1':'B',  '2':'C',  '3':'D',  '4':'E',
                    '5': 'F',  '6':'G',  '7':'H',  '8':'I',  '9':'J',
                    '10':'K',  '11':'L', '12':'M', '13':'N', '14':'O',
                    '15':'P',  '16':'Q', '17':'R', '18':'S', '19':'T',
                    '20':'U',  '21':'V', '22':'W', '23':'X', '24':'Y', '25':'Z'}
        
        if index <= 25:
            return (alphabet[str(index)] + ":" + alphabet[str(index)])
        else:
            return (alphabet[str(index/26-1)] + alphabet[str(index%26)] + ":" + alphabet[str(index/26-1)] + alphabet[str(index%26)])
    
    def get_column_name_for_calculate(self, index):
        column_name = self.get_xsl_column_name(index).split(':')[0]
        return column_name
    
    def get_sum_formula_from_list(self, col, list):
        formula = "="
        if list:
            for i in range(len(list)):
                formula += ("+" + self.get_column_name_for_calculate(col) + str(list[i]))
        return formula
    
    def get_arithmetic_formula(self, f_coly, f_rowx, s_coly, s_rowx, oprtr):
        f_cell_index = self.get_column_name_for_calculate(f_coly) + str(f_rowx+1)
        s_cell_index = self.get_column_name_for_calculate(s_coly) + str(s_rowx+1)
        return f_cell_index + oprtr + s_cell_index
    
    def is_module_installed(self, module_name):
        self._cr.execute("SELECT id FROM ir_module_module WHERE name = '%s' AND state IN ('installed', 'to upgrade')" %module_name)
        results = self._cr.dictfetchall()
        
        if results and len(results) > 0:
            return True
        else:
            return False
        
    def get_lines(self):
             
        where_qry = ""
        wh_join_qry = ""
        
        if self.warehouses:
            warehouse_ids = self.warehouses.ids
            where_qry += ' AND wh.id in (' + ','.join(map(str, warehouse_ids)) + ') '
            
        if self.is_module_installed("l10n_mn_stock_account_cost_for_each_wh"):
            wh_join_qry = """
                    LEFT JOIN stock_location sl ON sl.id = sm.location_dest_id
                    LEFT JOIN stock_location view_loc ON (view_loc.parent_left <= sl.parent_left AND view_loc.parent_right >= sl.parent_left)
                    LEFT JOIN stock_warehouse wh ON wh.view_location_id = view_loc.id AND wh.id IS NOT NULL
            """
        else:
            wh_join_qry = """
                    LEFT JOIN stock_location sl ON sl.id = sm.location_dest_id
                    LEFT JOIN stock_warehouse wh ON wh.lot_stock_id = sm.location_dest_id AND wh.id IS NOT NULL
            """
        if self.categories:
            category_ids = self.categories.ids
            where_qry += ' AND pro_cat.id in (' + ','.join(map(str, category_ids)) + ') '
        if self.products:
            product_ids = self.products.ids
            where_qry += ' AND pp.id in (' + ','.join(map(str, product_ids)) + ') '
        if self.partners:
            partner_ids = self.partners.ids
            where_qry += ' AND po.partner_id in (' + ','.join(map(str, partner_ids)) + ') '
            
        query = """
            SELECT wh.id AS wh_id, wh.name AS wh_name, partner.name AS partner_name, partner.id AS partner_id, po.name AS po_name, po.id AS po_id, 
                    po.date_order AS date_order, pol.id AS pol_id, CONCAT('[ ', pp.barcode, ' ][ ', pt.default_code, ' ][ ', pro_cat.name, ' ] ', pt.name) AS pro_name, pt.id AS pro_id, 
                    uom.name AS uom_name, pol.price_unit AS unit_price, pol.product_qty AS pro_qty, pol.name AS description,
                    CASE 
                        WHEN tax.id IS NOT NULL 
                        THEN 
                            CASE 
                                WHEN tax.price_include = TRUE
                                THEN ROUND(sm.price_unit*(100+tax.amount)/100)
                                ELSE sm.price_unit 
                            END 
                        ELSE sm.price_unit 
                    END AS unit_cost 
                    
                FROM purchase_order_line pol
                LEFT JOIN account_tax_purchase_order_line_rel tax_rel on tax_rel.purchase_order_line_id = pol.id
                LEFT JOIN account_tax tax ON tax.id = tax_rel.account_tax_id
                LEFT JOIN purchase_order po ON po.id = pol.order_id
                LEFT JOIN res_partner partner ON partner.id = po.partner_id
                LEFT JOIN product_product pp ON pp.id = pol.product_id
                LEFT JOIN product_template pt ON pt.id = pp.product_tmpl_id
                LEFT JOIN product_uom uom ON uom.id = pt.uom_id
                LEFT JOIN product_category pro_cat ON pt.categ_id = pro_cat.id
                LEFT JOIN stock_move sm ON sm.purchase_line_id = pol.id
                %s
                LEFT JOIN stock_picking picking ON picking.id = sm.picking_id
                WHERE wh.id IS NOT NULL AND po.company_id = %s AND po.state IN ('done', 'purchase') AND picking.state NOT IN ('done', 'cancel') 
                    AND po.date_order BETWEEN '%s' AND '%s' 
                    %s
                ORDER BY wh.name, wh.id, partner.name, partner.id, po.name, po.id, po.date_order DESC, pp.barcode, pt.default_code, pro_cat.name
        """ %(wh_join_qry, self.company.id, str(self.date_start) + " 00:00:00", str(self.date_end) + " 23:59:59", where_qry)
         
        self._cr.execute(query)
        results = self._cr.dictfetchall()
        return results if results else False
    
    @api.multi
    def export(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        
        # create name
        report_name = _('Moving Materials Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('moving_materials_report'), form_title=file_name).create({})
        
        # create formats
        format_text_left = {
            'font_name': 'Times New Roman',
            'font_size': 9,
            'align': 'left',
            'valign': 'top',
            'text_wrap': 1
        }
        
        format_title_left = format_text_left.copy()
        format_title_left['font_size'] = '10'
        format_title_left['bold'] = True
        
        format_title_right = format_title_left.copy()
        format_title_right['align'] = 'right'
        
        format_title_center = format_title_left.copy()
        format_title_center['align'] = 'center'
        format_title_center['font_size'] = '18'
        
        format_text_left_bordered = format_text_left.copy()
        format_text_left_bordered['border'] = 1
        
        format_number_right_bordered = format_text_left_bordered.copy()
        format_number_right_bordered['num_format'] = '_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)'
        
        format_dd_center_bordered = format_number_right_bordered.copy()
        format_dd_center_bordered['align'] = 'center'
        format_dd_center_bordered['num_format'] = '#,##0_);(#,##0)'
        
        format_text_center_bordered = format_text_left_bordered.copy()
        format_text_center_bordered['align'] = 'center'
        
        format_subh_text_bordered = format_text_left_bordered.copy()
        format_subh_text_bordered['bold'] = True
        format_subh_text_bordered['bg_color'] = '#ccffff'
        
        format_subh_number_bordered = format_subh_text_bordered.copy()
        format_subh_number_bordered['align'] = 'right'
        format_subh_number_bordered['num_format'] = '_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)'
        
        format_subh_dd_text_bordered = format_subh_number_bordered.copy()
        format_subh_dd_text_bordered['num_format'] = '#,##0_);(#,##0)'
        
        format_h_text_bordered = format_text_center_bordered.copy()
        format_h_text_bordered['bold'] = True
        format_h_text_bordered['text_wrap'] = 1
        format_h_text_bordered['bg_color'] = '#99ccff'
        format_h_text_bordered['align'] = 'center'
        format_h_text_bordered['font_size'] = '10'
        
        format_h_number_bordered = format_h_text_bordered.copy()
        format_h_number_bordered['num_format'] = '_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)'
        format_h_number_bordered['align'] = 'right'
        
        # register formats
        format_text_left = book.add_format(format_text_left)
        format_title_left = book.add_format(format_title_left)
        format_title_right = book.add_format(format_title_right)
        format_title_center = book.add_format(format_title_center)
        format_text_left_bordered = book.add_format(format_text_left_bordered)
        format_number_right_bordered = book.add_format(format_number_right_bordered)
        format_dd_center_bordered = book.add_format(format_dd_center_bordered)
        format_text_center_bordered = book.add_format(format_text_center_bordered)
        format_subh_text_bordered = book.add_format(format_subh_text_bordered)
        format_subh_number_bordered = book.add_format(format_subh_number_bordered)
        format_subh_dd_text_bordered = book.add_format(format_subh_dd_text_bordered)
        format_h_text_bordered = book.add_format(format_h_text_bordered)
        format_h_text_bordered.set_align('vcenter')
        format_h_number_bordered = book.add_format(format_h_number_bordered)
        
        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 0
        
        # compute column
        sheet.set_column('A:A', 4)
        sheet.set_column('B:B', 60)
        sheet.set_column('C:C', 10)
        sheet.set_column('D:D', 10)
        sheet.set_column('E:E', 10)
        sheet.set_column('F:F', 20)
        sheet.set_column('G:G', 10)
        sheet.set_column('H:H', 20)
        sheet.set_column('I:I', 25)
        
        # create contents
        sheet.merge_range(rowx, 0, rowx, 1, _(u'Company') + u": %s" % unicode(self.company.name), format_title_left) 
        sheet.merge_range(rowx+2, 0, rowx+3, 2, report_name, format_title_center) 
        sheet.merge_range(rowx+5, 0, rowx+5, 1, _(u'Report Date') + u": %s ~ %s" % (unicode(self.date_start), unicode(self.date_end)), format_title_left) 
        sheet.merge_range(rowx+5, 7, rowx+5, 8, _(u'Printed Date') + u": %s" % unicode(datetime.now().strftime('%Y-%m-%d')), format_title_right) 
        
        rowx += 6
        coly = 0
        sheet.merge_range(rowx, coly, rowx+2, coly, u'№', format_h_text_bordered)
        sheet.merge_range(rowx, coly+1, rowx+2, coly+1, _(u'Product'), format_h_text_bordered)
        sheet.merge_range(rowx, coly+2, rowx+2, coly+2, _(u'Unit of measure'), format_h_text_bordered)
        sheet.merge_range(rowx, coly+3, rowx+2, coly+3, _(u'Quantity'), format_h_text_bordered)
        sheet.merge_range(rowx, coly+4, rowx+2, coly+4, _(u'Unit Amount'), format_h_text_bordered)
        sheet.merge_range(rowx, coly+5, rowx+2, coly+5, _(u'Total Amount'), format_h_text_bordered)
        sheet.merge_range(rowx, coly+6, rowx+2, coly+6, _(u'Unit Cost'), format_h_text_bordered)
        sheet.merge_range(rowx, coly+7, rowx+2, coly+7, _(u'Total Cost'), format_h_text_bordered)
        sheet.merge_range(rowx, coly+8, rowx+2, coly+8, _(u'Description'), format_h_text_bordered)
                
        results = self.get_lines()
        index = 1
        rowx += 3
        
        writen_wh_ids, writen_partner_ids, writen_po_ids, writen_pol_ids = [], [], [], []
        wh_rows, partner_rows, po_rows, pol_rows = [], [], [], []
        current_wh_rowx, current_partner_rowx, current_porder_rowx = -1, -1, -1
        
        all_pol_line_count = 0
        
        all_col_count = 9
        main_col_count = 3
        
        if results:
            for result in results:
                if result['wh_id'] not in writen_wh_ids:
                    writen_wh_ids.append(result['wh_id'])
                    wh_rows.append(rowx+1)
                    
                    sheet.merge_range(rowx, 0, rowx, main_col_count - 1, "%s: %s" %(_("Warehouse"), result['wh_name']), format_subh_text_bordered)
                    sheet.write(rowx, all_col_count-1, "", format_subh_text_bordered)
                    
                    if current_wh_rowx != -1:
                        if partner_rows:
                            for i in range(main_col_count, all_col_count - 1):
                                if i%2 == 1:
                                    sheet.write_formula(current_wh_rowx, i, self.get_sum_formula_from_list(i, partner_rows), format_subh_number_bordered)
                                else:
                                    sheet.write(current_wh_rowx, i, "", format_subh_number_bordered)
                    if current_partner_rowx != -1:
                        if po_rows:
                            for i in range(main_col_count, all_col_count - 1):
                                if i%2 == 1:
                                    sheet.write_formula(current_partner_rowx, i, self.get_sum_formula_from_list(i, po_rows), format_subh_number_bordered)
                                else:
                                    sheet.write(current_partner_rowx, i, "", format_subh_number_bordered)
                    if current_porder_rowx != -1:
                        if pol_rows:
                            for i in range(main_col_count, all_col_count - 1):
                                if i%2 == 1:
                                    sheet.write_formula(current_porder_rowx, i, self.get_sum_formula_from_list(i, pol_rows), format_subh_number_bordered)
                                else:
                                    sheet.write(current_porder_rowx, i, "", format_subh_number_bordered)
                    partner_rows = []
                    po_rows = []
                    pol_rows = []
                    writen_partner_ids = []
                    writen_po_ids = []
                    writen_pol_ids = []
                    current_wh_rowx = rowx
                    rowx += 1
                    
                if result['partner_id'] not in writen_partner_ids:
                    writen_partner_ids.append(result['partner_id'])
                    partner_rows.append(rowx+1)
                    
                    sheet.merge_range(rowx, 0, rowx, main_col_count - 1, "%s: %s" %(_("Partner"), result['partner_name']), format_subh_text_bordered)
                    sheet.write(rowx, all_col_count-1, "", format_subh_text_bordered)
                    
                    if current_partner_rowx != -1:
                        if po_rows:
                            for i in range(main_col_count, all_col_count - 1):
                                if i%2 == 1:
                                    sheet.write_formula(current_partner_rowx, i, self.get_sum_formula_from_list(i, po_rows), format_subh_number_bordered)
                                else:
                                    sheet.write(current_partner_rowx, i, "", format_subh_number_bordered)
                    
                    if current_porder_rowx != -1:
                        if pol_rows:
                            for i in range(main_col_count, all_col_count - 1):
                                if i%2 == 1:
                                    sheet.write_formula(current_porder_rowx, i, self.get_sum_formula_from_list(i, pol_rows), format_subh_number_bordered)
                                else:
                                    sheet.write(current_porder_rowx, i, "", format_subh_number_bordered)
                    po_rows = []
                    pol_rows = []
                    writen_po_ids = []
                    writen_pol_ids = []
                    current_partner_rowx = rowx
                    rowx += 1
                    
                if result['po_id'] not in writen_po_ids:
                    writen_po_ids.append(result['po_id'])
                    po_rows.append(rowx+1)
                    
                    sheet.write(rowx, 0, "", format_subh_dd_text_bordered)
                    sheet.merge_range(rowx, 1, rowx, main_col_count - 1, "%s: %s" %(result['po_name'], result['date_order']), format_subh_text_bordered)
                    sheet.write(rowx, all_col_count-1, "", format_subh_text_bordered)
                    
                    if current_porder_rowx != -1:
                        if pol_rows:
                            for i in range(main_col_count, all_col_count - 1):
                                if i%2 == 1:
                                    sheet.write_formula(current_porder_rowx, i, self.get_sum_formula_from_list(i, pol_rows), format_subh_number_bordered)
                                else:
                                    sheet.write(current_porder_rowx, i, "", format_subh_number_bordered)
                         
                    pol_rows = []
                    writen_pol_ids = []
                    current_porder_rowx = rowx
                    rowx += 1
                    
                if result['pol_id'] not in writen_pol_ids:
                    writen_pol_ids.append(rowx+1)
                pol_rows.append(rowx+1)
                
                all_pol_line_count += 1
                sheet.write(rowx, 0, all_pol_line_count, format_dd_center_bordered)
                sheet.write(rowx, 1, " " + result.get("pro_name") or "", format_text_left_bordered)
                sheet.write(rowx, 2, result.get("uom_name") or "", format_text_center_bordered)
                sheet.write(rowx, 3, result.get("pro_qty") or 0, format_number_right_bordered)
                sheet.write(rowx, 4, result.get("unit_price") or 0, format_number_right_bordered)
                sheet.write_formula(rowx, 5, self.get_arithmetic_formula(3, rowx, 4, rowx, '*'), format_number_right_bordered)
                sheet.write(rowx, 6, result.get("unit_cost") or 0, format_number_right_bordered)
                sheet.write_formula(rowx, 7, self.get_arithmetic_formula(3, rowx, 6, rowx, '*'), format_number_right_bordered)
                sheet.write(rowx, 8, " " + result.get("description") or "", format_text_left_bordered)
                
                rowx += 1
                
                if results[-1] == result:
                    if partner_rows:
                        for i in range(main_col_count, all_col_count - 1):
                            if i%2 == 1:
                                sheet.write_formula(current_wh_rowx, i, self.get_sum_formula_from_list(i, partner_rows), format_subh_number_bordered)
                            else:
                                sheet.write(current_wh_rowx, i, "", format_subh_number_bordered)
                    if po_rows:
                        for i in range(main_col_count, all_col_count - 1):
                            if i%2 == 1:
                                sheet.write_formula(current_partner_rowx, i, self.get_sum_formula_from_list(i, po_rows), format_subh_number_bordered)
                            else:
                                sheet.write(current_partner_rowx, i, "", format_subh_number_bordered)
                    if pol_rows:
                        for i in range(main_col_count, all_col_count - 1):
                            if i%2 == 1:
                                sheet.write_formula(current_porder_rowx, i, self.get_sum_formula_from_list(i, pol_rows), format_subh_number_bordered)
                            else:
                                sheet.write(current_porder_rowx, i, "", format_subh_number_bordered)
        
            # build content footer
            sheet.write(rowx, 0, "", format_h_number_bordered)
            sheet.write(rowx, 1, _("TOTAL"), format_h_text_bordered)
            sheet.write(rowx, 2, "", format_h_text_bordered)
            for i in range(main_col_count, all_col_count - 1):
                if i%2 == 1:
                    sheet.write_formula(rowx, i, self.get_sum_formula_from_list(i, wh_rows), format_h_number_bordered)
                else:
                    sheet.write(rowx, i, "", format_h_number_bordered)
            sheet.write(rowx, all_col_count - 1, "", format_h_text_bordered)
                   
            rowx += 3
            sheet.write(rowx, 1,  _(u"Printed By") + u": ........................................................... /                                                /", format_title_left)
            sheet.write(rowx+1, 1,  _(u"Checked By") + u":............................................ .............../                                                /", format_title_left)
            
        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report() 
    
        