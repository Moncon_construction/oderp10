# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Quality Control",
    'version': '10.0.1.0',
    'depends': ['stock', 'decimal_precision', 'l10n_mn_report'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian MRP Modules',
    'description': """
         Quality control
    """,
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'data/data_view.xml',
        'views/quality_team_view.xml',
        'views/quality_alert_code_view.xml',
        'views/quality_indication_view.xml',
        'views/quality_stage_view.xml',
        'views/quality_repair_view.xml',
        'views/quality_alert_view.xml',
        'views/quality_check_view.xml',
        'views/quality_point_view.xml',
        'views/quality_stock_view.xml',
        'views/quality_indication_report_view.xml',
        'data/quality_demo.xml',
        
    ],
        
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False,
    'application': True,
}