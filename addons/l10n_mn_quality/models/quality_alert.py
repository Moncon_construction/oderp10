# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, RedirectWarning, ValidationError
import datetime
import dateutil.parser
from dateutil.relativedelta import relativedelta
import pytz

class QualityControlAlerts(models.Model):
    _name = 'quality.control.alerts'
    _description = 'Quality Control Alerts'
    _inherit = ['mail.thread']

    @api.multi
    def _get_default_stage_id(self):
        min_sorted = False
        minimum = self.env['quality.alert.stage'].search([])
        if minimum:
            min_sorted = minimum.sorted(key=lambda r: r.id)
        return min_sorted[0].id if min_sorted else False

    name = fields.Char('Name', copy=False, readonly=True, default=lambda self: _('New'))
    product_tmpl_id = fields.Many2one('product.template', string='Product Varient')
    product_id = fields.Many2one('product.product', string='Product')
    team_id = fields.Many2one('quality.alert.team', string='Team')
    user_id = fields.Many2one('res.users', 'Owner of the Error', default=lambda self: self.env.user, track_visibility='onchange')
    tag_ids = fields.Many2many('quality.tag', string='Tags', oldname='categ_ids')
    reason_id = fields.Many2one('quality.reason', 'Root Cause')
    color = fields.Integer(string='Color Index')
    priority = fields.Selection([
        ('0', 'Normal'),
        ('1', 'Low'),
        ('2', 'High'),
        ('3', 'Very High')], default='0', string='Priority', copy=False)
    notes = fields.Text('Notes')
    action_resolve = fields.Text('Corrective Actions')
    action_preventive = fields.Text('Preventive Actions')
    miscellaneous = fields.Text('Miscellaneous')
    partner_id = fields.Many2one('res.partner', 'Customer')
    error_date = fields.Datetime('Error Date', default=fields.Datetime.now)
    deadline = fields.Datetime('Dead line', readonly=1, track_visibility='onchange')
    end_date = fields.Datetime('End Date', readonly=1, store=True)
    operation_id = fields.Many2one('stock.picking.type', 'Operation')
    stage_id = fields.Many2one('quality.alert.stage', string='Stage', track_visibility='onchange', copy=False, default=_get_default_stage_id)
    check_id = fields.Many2one('quality.checks', string='Check')
    date_action = fields.Date('Next Activity Date', index=True)
    picking_id = fields.Many2one('stock.picking', 'Operation')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    alert_code_ids = fields.One2many('quality.alert.code.line', 'alerts_code_id', 'Alert Code Ids')
    department_id = fields.Many2one('hr.department', string='Department', track_visibility='onchange')
    error_department_id = fields.Many2one('hr.department', string='Error of Department', track_visibility='onchange')
    resolved_department_id = fields.Many2one('hr.department', string='Resolved Department')
    alert_code_id = fields.Many2one('quality.alert.code', string='Alert Code')
    repair_ids = fields.Many2many('quality.repair', string='Quality Report')
    cost = fields.Integer('Cost')
    resolved_hour = fields.Integer('Resolved Hour')
    work_drawings = fields.One2many('quality.work.drawings', 'alerts_code_id', 'Quality Work Drawing')
    origin = fields.Char('Origin', readonly=True, copy=False)
    repair_things = fields.Char('Repair')

    @api.onchange('alert_code_id')
    def item_delivered_ids_onchange(self):
        return {'domain': {'repair_ids': [('alert_code_id', '=', self.alert_code_id.id)]}}

    @api.multi
    def unlink(self):
        if any(stage.stage_id.id != stage._get_default_stage_id() for stage in self):
            raise UserError(_('Cannot delete a quality alert not in draft state'))
        return super(QualityControlAlerts, self).unlink()

    @api.multi
    def action_see_check(self):
        form_view = self.env.ref('l10n_mn_quality.view_quality_control_check_form').id
        return {
            'name': _('Quality Check'),
            'res_model': 'quality.checks',
            'view_mode': 'form',
            'view_type': 'form',
            'type': 'ir.actions.act_window'
        }

    @api.multi
    def write(self, vals):
        if 'stage_id' in vals:
            stage_obj = self.env['quality.alert.stage'].browse(vals.get('stage_id'))
            if stage_obj and stage_obj.done is True:
                vals.update({
                    'end_date': fields.Datetime.now(),
                })
        if 'alert_code_id' in vals:
            repair_timex = 0
            alert_code_obj = self.env['quality.alert.code'].browse(vals.get('alert_code_id'))
            if alert_code_obj:
                date = self.create_date if self.create_date else fields.Datetime.now()
                datex = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
                repair_timex = alert_code_obj.repair_time
                repair_timey = repair_timex
                while repair_timey > 0:
                    if repair_timey > 23:
                        repair_timex = 24
                        repair_timey = repair_timey - repair_timex
                        datex = datetime.datetime.strptime(str(datex), '%Y-%m-%d %H:%M:%S') + relativedelta(days=int(1))
                    else:
                        repair_timex = repair_timey
                        repair_timey = 0
                if repair_timex == 24:
                    vals.update({
                            'deadline': datetime.datetime.strptime(str(datex), '%Y-%m-%d %H:%M:%S')+ relativedelta(hour=int(0)),
                        })
                else:
                    vals.update({
                            'deadline': datetime.datetime.strptime(str(datex), '%Y-%m-%d %H:%M:%S') + relativedelta(hour=int(repair_timex)),
                        })
                    
        res = super(QualityControlAlerts, self).write(vals)
        return res

    @api.model
    def create(self, vals):
        if 'name' not in vals or vals['name'] == _('New'):
            vals['name'] = self.env['ir.sequence'].next_by_code('quality.control.alerts') or _('New')
        if 'alert_code_id' in vals:
            repair_timex = 0
            alert_code_obj = self.env['quality.alert.code'].browse(vals.get('alert_code_id'))
            if alert_code_obj:
                date = self.create_date if self.create_date else fields.Datetime.now()
                datex = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
                repair_timex = alert_code_obj.repair_time
                repair_timey = repair_timex
                while repair_timey > 0:
                    if repair_timey > 23:
                        repair_timex = 24
                        repair_timey = repair_timey - repair_timex
                        datex = datetime.datetime.strptime(str(datex), '%Y-%m-%d %H:%M:%S') + relativedelta(days=int(1))
                    else:
                        repair_timex = repair_timey
                        repair_timey = 0
                if repair_timex == 24:
                    vals.update({
                            'deadline': datetime.datetime.strptime(str(datex), '%Y-%m-%d %H:%M:%S')+ relativedelta(hour=int(0)),
                        })
                else:
                    vals.update({
                            'deadline': datetime.datetime.strptime(str(datex), '%Y-%m-%d %H:%M:%S') + relativedelta(hour=int(repair_timex)),
                        })
        return super(QualityControlAlerts, self).create(vals)

    @api.onchange('product_tmpl_id')
    def onchange_product_tmpl_id(self):
        if self.product_tmpl_id:
            productID = self.env['product.product'].search([('product_tmpl_id', '=', self.product_tmpl_id.id)]).id
            self.product_id = productID

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.product_tmpl_id = self.product_id.product_tmpl_id.id


class QualityAlertCodeLine(models.Model):
    _name = 'quality.alert.code.line'

    alert_code_id = fields.Many2many('quality.alert.code', string='Alert Code Id')
    alerts_code_id = fields.Many2one('quality.control.alerts', 'Check Code')
    product_qty = fields.Float('Product Quantity')
    quality_indication_id = fields.Many2one('quality.indication', string='Quality Indication')

class QualityWorkDrawings(models.Model):
    _name = 'quality.work.drawings'
    _description = 'Quality Work Drawings'

    alerts_code_id = fields.Many2one('quality.control.alerts', 'Check Code')
    file_name = fields.Char('File name')
    file_attach = fields.Binary('File', attachment=True)