from odoo import api, fields, models, _

class QualityAlertCode(models.Model):
    _name = 'quality.alert.code'
    _description = 'Quality Alert Code'
    _inherit = ['mail.thread']
    
    name = fields.Char('Code Name', required=True)
    alert_code = fields.Char('Alert Code', required=True)
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    user_id = fields.Many2one('res.users', 'Responsible', default=lambda self: self.env.user)
    repair_time = fields.Float('Repair Time')
