from odoo import api, fields, models, _

class QualityControlCheck(models.Model):
    _name = 'quality.checks'
    _description = 'Quality Control Check'
    _inherit = ['mail.thread']
    
    name = fields.Char('Name',copy=False, readonly=True, default=lambda self: _('New'))
    product_id =  fields.Many2one('product.product','Product', default=lambda self: self.is_check)
    measure = fields.Float('Measure')
    point_id = fields.Many2one('quality.point', 'Control Point')
    is_quality_points = fields.Boolean('Is Quality Points', compute = "_compute_quality_points")  
    picking_id = fields.Many2one('stock.picking','Operation')
    test_type = fields.Selection([
        ('pass-fail','Pass-Fail'),
        ('measure', 'Measure')
    ], string='Test Type', default='pass-fail', required=True)
    team_id = fields.Many2one('quality.alert.team','Team', required = True)
    notes = fields.Html('Notes')
    color = fields.Integer(string='Color Index')
    state = fields.Selection([
        ('none', 'To do'),
        ('pass', 'Pass'),
        ('fail', 'Fail')], string='States',
        copy=False, default='none', required = True)
    alert_count = fields.Integer(compute='_compute_alert_count',string='Alert Counts')
    alert_ids = fields.One2many('quality.control.alerts', 'check_id', string='Alert')
    check_id = fields.Many2one('quality.point', string = 'Check')
    passed = fields.Float('Passed')
    failed = fields.Float('Failed')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.user_id.company_id.id)
    user_id = fields.Many2one('res.users', 'Responsible', default=lambda self: self.env.user)
    indication_ids = fields.One2many('quality.indication.line', 'check_id', 'Indicatons')
    is_indication = fields.Integer('Is Indication', compute = '_compute_indication_ids') 
    
    @api.depends('alert_ids')
    def _compute_alert_count(self):
        data = self.env['quality.control.alerts'].read_group([('check_id', 'in', self.ids)], ['check_id'], ['check_id'])
        count_data = dict((item['check_id'][0], item['check_id_count']) for item in data)
        for order in self:
            order.alert_count = count_data.get(order.id, 0)
            
    @api.depends('indication_ids')
    def _compute_indication_ids(self):
        if self.indication_ids:
            for a in self.indication_ids:
                if a.is_pass_fail == True:
                    self.write({'state': 'fail'})
                    break
                else:
                    self.write({'state': 'pass'})
    
    @api.multi
    def button_refresh(self):
        return True
                    
    @api.multi
    def button_alert(self):
        
        self.ensure_one()
        val = {
            'product_tmpl_id':self.product_id.product_tmpl_id.id,
            'product_id':self.product_id.id,
            'team_id':self.team_id.id,
            'check_id': self.id}
        newId = self.env['quality.control.alerts'].create(val)
        
    @api.multi 
    def do_measure(self):
        if self.measure > self.point_id.tolerance_max:
            self.state = "fail"
            self.test_type = "pass-fail"
        else:
            self.state = "pass"
            self.test_type = "pass-fail"
        
    @api.onchange('point_id')
    def onchange_point_id(self):
        
        self.notes = self.point_id.notes
        self.product_id = self.point_id.product_id
        self.team_id = self.point_id.team_id
        if self.point_id.test_type == "measure":
            self.test_type = "measure"
        else:
            self.test_type = "pass-fail"
    
    @api.onchange('failed')
    def onchange_failed(self):
        self.passed = self.passed - self.failed
        
    @api.depends('point_id')
    def _compute_quality_points(self):
        
        if self.point_id.test_type == 'measure':   
            self.is_quality_points = True
        else:
            self.is_quality_points = False
            
    @api.depends('product_id') 
    def is_check(self):
        if quality.point.product_id.id != "":
            return self.product_id.id
            
    @api.model
    def create(self, vals):  
        if 'name' not in vals or vals['name'] == _('New'):
            vals['name'] = self.env['ir.sequence'].next_by_code('quality.checks') or _('New')
        return super(QualityControlCheck, self).create(vals)