from odoo import api, fields, models, _

class QualityIndication(models.Model):
    _name = 'quality.indication'
    _description = 'Quality Indication'
    
    name = fields.Char('Indication Name', required=True)
    is_workcenter = fields.Selection([
                ('before','Before Workcenter'),
                ('after', 'After Workcenter')
                ], string='Is Workcenter', default='before', required=True)
    check_id = fields.Many2one('quality.checks', 'Quality Checks')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.user_id.company_id.id)
    user_id = fields.Many2one('res.users', 'Responsible', default=lambda self: self.env.user)