from odoo import api, fields, models, _
 
class QualityControlPoints(models.Model):
    _name = 'quality.point'
    _description = 'Quality Control Point'
    _inherit = ['mail.thread']
         
    name = fields.Char('Name', copy=False, readonly=True, default=lambda self: _('New'),required=True)
    title = fields.Char('Title')
    product_tmpl_id = fields.Many2one('product.template', string='Product Template')
    product_id = fields.Many2one('product.product', string='Product')
    picking_type_id = fields.Many2one('stock.picking.type', 'Operation', required = True)
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.user_id.company_id.id)
    measure_frequency_type = fields.Selection([
        ('all operations','All Operations')
    ], string='Control Type', default='all operations')
    control_frequency = fields.Char('Control Frequency')
    every_randomly = fields.Integer("Every")
    every_periodically = fields.Integer("Every")
    days = fields.Selection([
        ('day','Day(s)'),
        ('month', 'Month(s)'),
        ('year', 'Year(s)')
    ], string='Days', default="day")
    test_type = fields.Selection([
        ('pass-fail','Pass-Fail')
    ], string='Test Type', default='pass-fail', required=True)
    norm = fields.Char('Norm', default="0.00")
    norm_unit = fields.Char('Norm Unit', default="mm")
    tolerance_min = fields.Char('Tolerance', default="0.00")
    tolerance_max = fields.Char("0.00", default="0.00")
    team_id = fields.Many2one('quality.alert.team','Team', required=True)
    user_id = fields.Many2one('res.users', 'Responsible', default=lambda self: self.env.user)
    notes = fields.Html('Notes')
    reasons = fields.Html('Reasons')
    active = fields.Boolean('Active', default=True)
    point_count = fields.Integer(compute='_compute_point_count',string='Point Counts')
    point_avg = fields.Float(compute='_compute_point_count', strin='Point AVG')
    point_total = fields.Float('Point Total')
    point_ids = fields.One2many('quality.checks', 'check_id', string='Point')
    picking_id = fields.Many2one('stock.picking','Operation')
    indication_ids = fields.One2many('quality.indication.line', 'point_id', 'Indicatons')
    
    @api.depends('point_ids')
    def _compute_point_count(self):
        data = self.env['quality.checks'].read_group([('point_id', 'in', self.ids)], ['point_id'], ['point_id'])
        check = self.env['quality.checks'].search([('point_id', '=', self.id)])
        count_data = dict((item['point_id'][0], item['point_id_count']) for item in data)
        for order in self:
            order.point_count = count_data.get(order.id, 0)
            
        for checks in check:
            order.point_total = order.point_total + checks.measure
          
        if order.point_total!=0:
            self.point_avg = order.point_total / order.point_count
        
    @api.onchange('user_id')
    def onchange_user_id(self):
         
        self.company_id = self.user_id.company_id.id
     
    @api.onchange('norm')
    def onchange_norm(self):
       
        self.tolerance_max = self.norm
        
    @api.onchange('product_tmpl_id')
    def onchange_product_tmpl_id(self):
        if self.product_tmpl_id:
            productID = self.env['product.product'].search([('product_tmpl_id', '=', self.product_tmpl_id.id)]).id
            self.product_id = productID
            
    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.product_tmpl_id = self.product_id.product_tmpl_id.id
        
    @api.model
    def create(self, vals): 
        if 'name' not in vals or vals['name'] == _('New'):
            vals['name'] = self.env['ir.sequence'].next_by_code('quality.point') or _('New')  
        return super(QualityControlPoints, self).create(vals)
    
class QualityIndicationLine(models.Model):
    _name = 'quality.indication.line'
    
    indication_id = fields.Many2one('quality.indication', 'Alert Indication')
    point_id = fields.Many2one('quality.point', 'Quality Point')
    check_id = fields.Many2one('quality.checks', 'Quality Checks')
    is_pass_fail = fields.Boolean('Is Pass Fail')
        