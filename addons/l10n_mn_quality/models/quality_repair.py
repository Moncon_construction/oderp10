# -*- encoding: utf-8 -*-
from odoo import api, fields, models, _


class QualityRepair(models.Model):
    _name = 'quality.repair'
    _description = 'Repair Of Quality'

    alert_code_id = fields.Many2one('quality.alert.code', string='Alert Code', required=True)
    description = fields.Text('Description', required=True)

    @api.multi
    def name_get(self):
        result = []
        for line in self:
            name = line.alert_code_id.name if line.alert_code_id else ''
            result.append((line.id, name))
        return result