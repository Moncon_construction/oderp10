from odoo import api, fields, models, _

class QualityControlStage(models.Model):
    _name = 'quality.alert.stage'
    _description = 'Quality Control Stage'
    _inherit = ['mail.thread']
    
    name = fields.Char('Stage Name', required=True, translate=True)
    sequence = fields.Integer('Stage Sequence', default=1)
    folded = fields.Boolean('Folded')
    done = fields.Boolean('Alert Processed')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.user_id.company_id.id)
    user_id = fields.Many2one('res.users', 'Responsible', default=lambda self: self.env.user)
    
class QualityControlTag(models.Model):
    _name = 'quality.tag'
    _description = 'Quality Control Tag'
    _inherit = ['mail.thread']
    
    name = fields.Char('Tag Name', required=True)
    color = fields.Integer('Color')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.user_id.company_id.id)
    user_id = fields.Many2one('res.users', 'Responsible', default=lambda self: self.env.user)
    
class QualityRootReason(models.Model):
    _name = 'quality.reason'
    _description = 'Quality Root Reason'
    
    name = fields.Char('Reason', required=True)