from odoo import api, fields, models, _

class MrpWorkorderQuality(models.Model):
    _inherit = 'stock.picking'
    _description = 'Stock Picking Quality'
    
    check_ids = fields.One2many('quality.checks', 'picking_id', string='Point')
    check_count = fields.Integer('Check Count', compute='_compute_check_count')
    alert_ids = fields.One2many('quality.control.alerts', 'picking_id', 'Alerts')
    quality_alert_count = fields.Integer('Alert Count', compute='_compute_alert_count')
    point_ids = fields.One2many('quality.point', 'picking_id', 'Alerts')
    quality_state_todo = fields.Boolean('Quality State', default=True)
    product_id = fields.Many2one('product.product', 'Product')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.user_id.company_id.id)
    user_id = fields.Many2one('res.users', 'Responsible', default=lambda self: self.env.user)
    
    @api.depends('check_ids')
    def _compute_check_count(self):
        data = self.env['quality.checks'].read_group([('picking_id', 'in', self.ids)], ['picking_id'], ['picking_id'])
        count_data = dict((item['picking_id'][0], item['picking_id_count']) for item in data)
        for order in self:
            order.check_count = count_data.get(order.id, 0)
                
    @api.depends('alert_ids')
    def _compute_alert_count(self):
        data = self.env['quality.control.alerts'].read_group([('picking_id', 'in', self.ids)], ['picking_id'], ['picking_id'])
        count_data = dict((item['picking_id'][0], item['picking_id_count']) for item in data)
        for order in self:
            order.quality_alert_count = count_data.get(order.id, 0)
    
    @api.multi   
    def check_quality(self):
        view_id = self.env.ref('l10n_mn_quality.view_quality_control_check_form').id
        return {
        'type': 'ir.actions.act_window',
        'name': _('Quality Check'),
        'res_model': 'quality.checks',
        'view_type': 'form',
        'view_mode': 'tree,form',
        'views': [(view_id, 'form')],
        'view_id': view_id,
        'context' : {'default_picking_id': self.id}
        }
        
    @api.multi
    def button_quality_alert(self):
        view_id = self.env.ref('l10n_mn_quality.view_quality_control_alerts_form').id
        return {
        'type': 'ir.actions.act_window',
        'name': _('Quality Alert'),
        'res_model': 'quality.control.alerts',
        'view_type': 'form',
        'view_mode': 'tree,form',
        'views': [(view_id, 'form')],
        'view_id': view_id,
        'context' : {'default_picking_id': self.id,
                     'default_product_id': self.product_id.id}
        }