from odoo import api, fields, models, _
    
class QualityControlTeam(models.Model):
    _name = 'quality.alert.team'
    _description = 'Quality Control Team'
    _inherit = ['mail.thread']
         
    name = fields.Char('Team Name', required=True)
    alias_name = fields.Char('Email Alias')
    alias_contact = fields.Selection([
                                    ('everyone','Everyone'),
                                    ('active customer', 'Active Selection'),
                                    ('followers', 'Followers')], string='Accept Emails From' , default='everyone')
    
    employee_ids = fields.One2many('quality.team.line', 'team_id', 'Team Members')
    quality_alert_count = fields.Integer('Quality Alert Count', compute='_compute_alert_count')
    check_count = fields.Integer('Check count', compute='_compute_point_count')
    color = fields.Integer('Color')
    alert_ids = fields.One2many('quality.control.alerts', 'team_id', string='Alert')
    point_ids = fields.One2many('quality.checks', 'team_id', string='Point')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.user_id.company_id.id)
    user_id = fields.Many2one('res.users', 'Responsible', default=lambda self: self.env.user)
    active = fields.Boolean('Active', default=True)
    
    @api.depends('alert_ids')
    def _compute_alert_count(self):
        data = self.env['quality.control.alerts'].read_group([('team_id', 'in', self.ids)], ['team_id'], ['team_id'])
        count_data = dict((item['team_id'][0], item['team_id_count']) for item in data)
        for order in self:
            order.quality_alert_count = count_data.get(order.id, 0)
            
    @api.depends('point_ids')
    def _compute_point_count(self):
        data = self.env['quality.checks'].read_group([('team_id', 'in', self.ids)], ['team_id'], ['team_id'])
        count_data = dict((item['team_id'][0], item['team_id_count']) for item in data)
        for order in self:
            order.check_count = count_data.get(order.id, 0)
            
    @api.multi
    def check_team(self):
        return {
        'type': 'ir.actions.act_window',
        'name': _('Quality Check'),
        'res_model': 'quality.checks',
        'view_type': 'form',
        'view_mode': 'tree,form,pivot,graph,kanban',
        'domain' : [('team_id', '=', self.id)]
        }
        
    @api.multi
    def alert_team(self):
        return {
        'type': 'ir.actions.act_window',
        'name': _('Quality Alert'),
        'res_model': 'quality.control.alerts',
        'view_type': 'form',
        'view_mode': 'pivot,kanban,tree,form,graph,calendar',
        'domain' : [('team_id', '=', self.id)]
        }
    
class QuilityTeamLine(models.Model):
    _name = 'quality.team.line'

    team_id = fields.Many2one('quality.alert.team', 'Employee')
    employee_id = fields.Many2one('res.users', 'Employee')