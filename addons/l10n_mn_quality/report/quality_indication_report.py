# -*- coding: utf-8 -*-

from io import BytesIO
import base64
import time
from datetime import date,datetime,timedelta
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from operator import itemgetter
from odoo.exceptions import ValidationError, UserError

from odoo import api, fields, models, _
from openerp.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles

# ---------------------------------------------------------
# Quality Indication Report
# ---------------------------------------------------------


class QualityIndicationReport(models.Model):
    _name = "quality.indication.report"

    date_from = fields.Date('Date from')
    date_to = fields.Date('Date to')
    user_id = fields.Many2one('res.users', 'Responsible', default=lambda self: self.env.user)
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.user_id.company_id.id)
    quality_point_type = fields.Selection([
        ('product_point','Product Quality Point'),
        ('product_categ_point','Product Category Quality Point')
    ], string='Quality Point Type', default='product_point', required=True)
    
    def export_report(self, reports):
        #create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        #create name
        report_name = _('Quality Indication Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        
        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_title_small = book.add_format(ReportExcelCellStyles.format_title_small)
        format_group_right = book.add_format(ReportExcelCellStyles.format_group_right)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_bold_text = book.add_format(ReportExcelCellStyles.format_content_bold_text)
        format_content_bold_float = book.add_format(ReportExcelCellStyles.format_content_bold_float)
        
        sub_title = book.add_format({
                'font_name': 'Times New Roman',
                'font_size': 9,
                'align': 'center',
                'valign': 'vcenter',
                'border': 1,
                'num_format': '#,##0.00',
                'text_wrap': True
                })
        
        quality_indication_obj = self.env['quality.control.alerts'].search([])
        indication_line = self.env['quality.indication'].search([])
        seq = 1
        data_dict = {}
        
        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('quality_indication'), form_title=file_name, date_to=self.date_to, date_from=self.date_from).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 1

        # compute column
        colx_number = 5
        sheet.set_column('A:A', 15)
        sheet.set_column('B:B', 15)
        sheet.set_column('C:C', 15)
        sheet.set_column('D:D', 15)
        sheet.set_column('E:E', 35)
        sheet.set_column('F:F', 35)
        sheet.set_column('G:G', 15)
        sheet.set_column('H:H', 15)
        sheet.set_column('I:I', 15)
        
        # create name
        sheet.write(rowx, 0, _('Company:'), format_filter)
        sheet.write(rowx, 1, '%s' % (self.user_id.company_id.name), format_filter)
        # create duration
        sheet.merge_range(rowx, 4, rowx, 6, '%s: %s - %s' % (_('Duration'), self.date_from, self.date_to), format_filter)
        rowx += 1
        
        # create date
        sheet.merge_range(rowx, 4, rowx, 6, '%s: %s' % (_('Created On'), time.strftime('%Y-%m-%d')), format_filter)
        rowx += 2

        sheet.merge_range(rowx, 0, rowx+1, colx_number, report_name.upper(), format_name)
        rowx += 3
        # Тайлангийн хүснэгтийн толгойг зурж байна.
        sheet.merge_range(rowx+1, 0, rowx, 0, _('Indication Name'), format_title)
        sheet.merge_range(rowx+1, 1, rowx, 1, _('Product'), format_title)
        sheet.merge_range(rowx+1, 2, rowx, 2, _('Workcenter'), format_title)
        sheet.merge_range(rowx+1, 3, rowx, 3, _('Alert Owner'), format_title)
        sheet.merge_range(rowx+1, 4, rowx, 4, _('Note'), format_title)
        sheet.merge_range(rowx+1, 5, rowx, 5, _('Action corrective'), format_title)
        
        
        rowx += 2
        is_alert = False
        workcenter_name = ''
        product_name = ''
        responsibler = ''
        workorder_name = ''
        action_corrective = ''
        for quality_indication in indication_line:
            indication = self.env['quality.alert.code.line'].search([('quality_indication_id', '=', quality_indication.id), ('alerts_code_id', '!=', False)]).sorted(key=lambda l: l.quality_indication_id)
            for a in indication:
                if a:
                    alert = self.env['quality.control.alerts'].search([('id', '=', a.alerts_code_id.id)])
                    if alert:
                        if (datetime.strptime(alert.create_date, '%Y-%m-%d %H:%M:%S').date() >= datetime.strptime(self.date_from, '%Y-%m-%d').date() and datetime.strptime(alert.create_date, '%Y-%m-%d %H:%M:%S').date() <= datetime.strptime(self.date_to, '%Y-%m-%d').date()):
                            sheet.set_row(rowx, 20)
                            is_alert = True
                            if 'workcenter_id' in alert._fields:
                                if alert.workcenter_id:
                                    workcenter_name = str(alert.workcenter_id.name)
                                else:
                                    workcenter_name = ''
                            else:
                                workcenter_name = ''
                            if 'workorder_id' in alert._fields:
                                if alert.workorder_id:
                                    workorder_name = str(alert.workorder_id.name) + ': '
                                else:
                                    workorder_name = ''
                            else:
                                workorder_name = ''
                            if alert.product_id:
                                product_name = str(alert.product_id.name)
                            else:
                                product_name = ''
                            if alert.user_id:
                                responsibler = str(alert.user_id.name)
                            else:
                                responsibler = ''
                            if alert.action_corrective:
                                action_corrective = alert.action_corrective
                            else:
                                action_corrective = ''
                            sheet.write(rowx, 1, workorder_name + product_name, sub_title)
                            sheet.write(rowx, 2, workcenter_name, sub_title)
                            sheet.write(rowx, 3, responsibler, sub_title)
                            sheet.write(rowx, 4, alert.notes, sub_title)
                            sheet.write(rowx, 5, action_corrective, sub_title)
                            workcenter_name = ''
                            workorder_name = ''
                            product_name = ''
                            responsibler = ''
                            department_name = ''
                            action_corrective = ''
                            rowx += 1
            if is_alert == True:
                if len(indication) == 1:
                    sheet.write(rowx-1, 0, quality_indication.name, sub_title)
                else:
                    sheet.merge_range(rowx-len(indication), 0, rowx - 1, 0, quality_indication.name, sub_title)
                
        rowx += 2        
        sheet.merge_range(rowx, 5, rowx, 6, '%s: ........................................... (                          )' % _('Quality Manager'), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 5, rowx, 6, '%s: ........................................... (                          )' % _('Manufactoring Master'), format_filter)
        
        book.close()

        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        
        # call export function
        return report_excel_output_obj.export_report()
        