# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "MRP features for Quality Control",
    'version': '10.0.1.0',
    'depends': ['l10n_mn_quality', 'l10n_mn_mrp'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Manufacturing',
    'description': """
         Quality control
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/mrp_workorder_quality.xml',
        'views/mrp_production_view.xml',
    ],
        
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False,
    'application': True,
}