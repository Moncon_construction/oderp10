# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class MrpProduction(models.Model):
    _inherit = 'mrp.production'

    def _quality_control_count(self):
        for obj in self:
            obj.quality_control_count = len(self.env['quality.control.alerts'].search([('related_production_id', '=', obj.id)]))

    quality_control_count = fields.Integer('Quality Control Count', compute='_quality_control_count')

    @api.multi
    def new_quality_control_alert(self):
        # Чанарын доголдол үүснэ.
        related_sale_order_id = False
        partner_id = False
        if 'related_sale_order_id' in self.env['mrp.production']._fields:
            related_sale_order_id = self.related_sale_order_id.id
            partner_id = self.related_sale_order_id.partner_id.id
        view_id = self.env.ref('l10n_mn_quality.view_quality_control_alerts_form').id
        return {
            'type': 'ir.actions.act_window',
            'name': _('Quality Alert'),
            'res_model': 'quality.control.alerts',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'views': [(view_id, 'form')],
            'view_id': view_id,
            'context': {'default_product_id': self.product_id.id,
                        'default_partner_id': partner_id,
                        'default_origin': self.name,
                        'default_related_production_id': self.id or False,
                        'default_related_order_id': related_sale_order_id,
                        }
        }

    @api.multi
    def stat_button_quality(self):
        #   ухаалаг даруул дээр дарахад дуудагдах функц
        res = self.env['quality.control.alerts'].search([('related_production_id', '=', self.id)])
        return {
            'type': 'ir.actions.act_window',
            'name': _('Quality Control Alert'),
            'res_model': 'quality.control.alerts',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'view_id': False,
            'domain': [('id', 'in', res.ids if res else False)],
        }