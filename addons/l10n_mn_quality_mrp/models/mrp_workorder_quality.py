from datetime import datetime
from odoo import api, fields, models, _
from odoo.exceptions import UserError, RedirectWarning, ValidationError
import math

class MrpWorkorderQuality(models.Model):
    _inherit = 'mrp.workorder'
    _description = 'MRP Workorder Quality'
    
    check_ids = fields.One2many('quality.checks', 'workorder_id', string='Point')
    check_count = fields.Integer('Check Count', compute='_compute_check_count')
    alert_ids = fields.One2many('quality.control.alerts', 'workorder_id', 'Alerts')
    quality_alert_count = fields.Integer('Alert Count', compute='_compute_alert_count')
    point_ids = fields.One2many('quality.point', 'workorder_id', 'Alerts')
    quality_state_todo = fields.Boolean('Quality State', default=True)
    is_pass_fail = fields.Boolean('Is Pass Fail', default=True)
    
    @api.depends('check_ids')
    def _compute_check_count(self):
        data = self.env['quality.checks'].read_group([('workorder_id', 'in', self.ids)], ['workorder_id'], ['workorder_id'])
        count_data = dict((item['workorder_id'][0], item['workorder_id_count']) for item in data)
        for order in self:
            order.check_count = count_data.get(order.id, 0)
        for workcenter in self:
            point_id = self.env['quality.point'].search([('workcenter_id', '=', workcenter.workcenter_id.id), ('product_id', '=', workcenter.product_id.id)])
            for points in point_id:
                if points.id:
                    self.write({'quality_state_todo': True})
                else:
                    self.write({'quality_state_todo': False})
    @api.depends('alert_ids')
    def _compute_alert_count(self):
        data = self.env['quality.control.alerts'].read_group([('workorder_id', 'in', self.ids)], ['workorder_id'], ['workorder_id'])
        count_data = dict((item['workorder_id'][0], item['workorder_id_count']) for item in data)
        for order in self:
            order.quality_alert_count = count_data.get(order.id, 0)
        
    @api.multi   
    def check_quality(self):
        self.ensure_one()
        self.write({'is_pass_fail': False})
        point_id = self.env['quality.point'].search([('workcenter_id', '=', self.workcenter_id.id),('product_id', '=', self.product_id.id)])
        indications = self.env['quality.indication.line'].search([('point_id', '=', point_id.id)])
        indication_id = self.env['quality.indication.line']
        view_id = self.env.ref('l10n_mn_quality_mrp.view_quality_check_small_form').id
        if point_id:
            qualitychecks = self.env['quality.checks']
            qualitycheck = qualitychecks.create({
                                                'product_id': self.product_id.id,
                                                'team_id': point_id.team_id.id,
                                                'workorder_id': self.id,
                                                'workcenter_id': self.workcenter_id.id,
                                                'point_id': point_id.id})
            for a in indications:
                indication_id.create({
                                    'indication_id': a.indication_id.id,
                                    'check_id': qualitycheck.id})
            
            for points in point_id:
                if point_id:
                    return {
                    'type': 'ir.actions.act_window',
                    'name': _('Quality Check'),
                    'res_model': 'quality.checks',
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'views': [(view_id, 'form')],
                    'view_id': view_id,
                    'res_id': qualitycheck.id,
                    'target': 'new'
                    }
        else:
            raise UserError(_('There is no quality point'))
    @api.multi
    def button_quality_alert(self):
        view_id = self.env.ref('l10n_mn_quality.view_quality_control_alerts_form').id
        related_sale_order_id = False
        partner_id = False
        if 'related_sale_order_id' in self.env['mrp.production']._fields:
            related_sale_order_id = self.production_id.related_sale_order_id.id
            partner_id = self.production_id.related_sale_order_id.partner_id.id
        return {
        'type': 'ir.actions.act_window',
        'name': _('Quality Alert'),
        'res_model': 'quality.control.alerts',
        'view_type': 'form',
        'view_mode': 'tree,form',
        'views': [(view_id, 'form')],
        'view_id': view_id,
        'context' : {'default_product_id' : self.product_id.id,
                     'default_workcenter_id' : self.workcenter_id.id,
                     'default_workorder_id': self.id,
                     'default_partner_id': partner_id,
                     'default_origin': self.name,
                     'default_related_production_id': self.production_id.id or False,
                     'default_related_order_id': related_sale_order_id,
                     }
        }
    
class MrpQualityCheck(models.Model):
    _inherit = 'quality.checks'
    
    workorder_id = fields.Many2one('mrp.workorder', string='Workorder')
    workcenter_id= fields.Many2one('mrp.workcenter', 'Workcenter')
    
    @api.multi
    def button_alert(self):
        if self.workorder_id:
            val = {
            'product_tmpl_id':self.product_id.product_tmpl_id.id,
            'product_id':self.product_id.id,
            'team_id':self.team_id.id,
            'check_id': self.id,
            'workorder_id': self.workorder_id} 
            newId = self.env['quality.control.alerts'].create(val)
        else:
            super(MrpQualityCheck, self).button_alert()
    
    @api.multi   
    def do_fail_workorder(self):
        alert_code = self.env['quality.alert.code.line']
        alert = self.env['quality.control.alerts']
        alert_create = []
        for a in self.indication_ids:
            if a.is_pass_fail == True:
                alert_create = alert.create({'product_id' : self.product_id.id,
                             'product_tmpl_id': self.product_id.product_tmpl_id.id,
                             'workcenter_id' : self.workcenter_id.id,
                             'workorder_id': self.workorder_id.id,
                             'team_id': self.team_id.id,
                             'point_id': self.point_id.id,
                             'check_id': self.id})
                break
        for b in self.indication_ids:
            if b.is_pass_fail == True:
                quality_indication_ids = self.env['quality.indication.line'].search([('id', '=', b.id)])
                alert_code.create({'quality_indication_id':quality_indication_ids.indication_id.id,
                              'alerts_code_id': alert_create.id})
        for b in self.indication_ids:
            if b.is_pass_fail == True:
                view_ref = self.env['ir.model.data'].get_object_reference('l10n_mn_quality', 'view_quality_control_alerts_form')
                view_id = view_ref and view_ref[1] or False,
                return {
                'type': 'ir.actions.act_window',
                'name': _('Quality Alert'),
                'res_model': 'quality.control.alerts',
                'view_type': 'form',
                'view_mode': 'form',
                'view_id': [view_id],
                'target': 'current',
                'nodestroy': True,
                'res_id': alert_create.id
                }
    
class MrpQualityIndication(models.Model):
    _inherit = 'quality.indication'
    
    workorder_id = fields.Many2one('mrp.workorder', string='Workorder')
               
class QualityControlAlerts(models.Model):
    _inherit = 'quality.control.alerts'
    
    workorder_id = fields.Many2one('mrp.workorder', string='Workorder')
    workcenter_id= fields.Many2one('mrp.workcenter', 'Alert Workcenter')
    related_production_id = fields.Many2one('mrp.production', string='Related Mrp Production')
    
    @api.multi
    def action_create_message(self):
        view_id = self.env.ref('mrp.mrp_message_view_form').id
        return {
        'type': 'ir.actions.act_window',
        'name': _('Quality Alert Message'),
        'res_model': 'mrp.message',
        'view_type': 'form',
        'view_mode': 'form',
        'views': [(view_id, 'form')],
        'view_id': view_id,
        'context' : {'default_product_id' : self.product_id.id,
                     'default_workcenter_id' : self.workcenter_id.id,
                     'default_message' : _('Quality Alert For Product: ') + self.product_id.name},
        'target': 'new'
        }
    
class QualityControlPoint(models.Model):
    _inherit = 'quality.point'
    
    workorder_id = fields.Many2one('mrp.workorder', string='Workorder')
    workcenter_id= fields.Many2one('mrp.workcenter', 'Workcenter')
    is_operation = fields.Boolean(string = 'Is Operation')
    
    @api.onchange('picking_type_id')
    def onchange_picking_type(self):
         
        if self.picking_type_id.code == 'mrp_operation':         
            self.is_operation = True
        else:
            self.is_operation = False