# -*- coding: utf-8 -*-
{
    'name': "Repair Contract",
    'description': """
        Гэрээгээр засварын ажил хийдэг бол энэ модулийг суулгана.
    """,
    'author': "Asterisk Technologies LLC",
    'website': "http://asterisk-tech.mn",
    'category': 'Repair, Mongolian Modules',
    'version': '1.0',
    'depends': ['l10n_mn_repair_wo_technic','l10n_mn_contract'],
    'data': [
        'views/work_order_view.xml',
        'views/contract_view.xml',
    ],
    'contributors': ['Asterisk Technologies LLC <info@asterisk-tech.mn>'],
}
