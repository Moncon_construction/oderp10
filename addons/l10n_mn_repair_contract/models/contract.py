# -*- coding: utf-8 -*-

from odoo import models, fields, api

class ContractManagement(models.Model):
    _inherit = 'contract.management'

    @api.multi
    def _count_work_orders(self):
        wo_obj = self.env['work.order']
        for obj in self:
            wo_ids = wo_obj.search([('contract_id','=',obj.id)])
            if wo_ids:
                obj.work_order_count = len(wo_ids.ids)
            else:
                obj.work_order_count = 0
        
    work_order_count = fields.Float(compute='_count_work_orders', string='Work Order Count')

    @api.multi
    def open_work_orders(self):
        mod_obj = self.env['ir.model.data']
        dummy, action_id = tuple(mod_obj.get_object_reference(self.env.cr, self.env.user.id, 'l10n_mn_repair_work_order', 'work_order_action'))
        action = self.env['ir.actions.act_window'].read(self.env.cr, self.env.user.id, action_id)

        order_ids = self.env['work.order'].search(self.env.cr, self.env.user.id, [('contract_id','=',self.id)])

        if len(order_ids) > 1:
            action['domain'] = "[('id','in',[" + ','.join(map(str, order_ids)) + "])]"
        else:
            res = mod_obj.get_object_reference(self.env.cr, self.env.user.id, 'l10n_mn_repair_work_order', 'work_order_view_form')
            action['views'] = [(res and res[1] or False, 'form')]
            action['res_id'] = order_ids and order_ids[0] or False
        return action
    
ContractManagement()