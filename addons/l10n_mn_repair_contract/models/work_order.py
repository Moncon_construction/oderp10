# -*- coding: utf-8 -*-
import math

from odoo import models, fields

class WorkOrder(models.Model):
    _inherit = 'work.order'


    contract_id = fields.Many2one('contract.management', 'Contract')
