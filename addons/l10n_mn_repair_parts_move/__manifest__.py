# -*- coding: utf-8 -*-
{
    'name': "Сэлбэг шилжүүлэх",
    'version': '1.0',
    'depends': ['base', 'l10n_mn_technic_parts', 'l10n_mn_repair_wo_technic'],
    'author': "Asterisk Technologies LLC",
    'category': 'Repair, Mongolian Modules',
    'description': """
        Сэлбэг шилжүүлэх захиалга үүсгэх, уг захиалгаас засварын ажилбар үүсгэх.
    """,
    'data': [
        'security/repair_parts_move_security.xml',
        'security/ir.model.access.csv',
        'views/repair_work_order_views.xml',
        'views/repair_parts_move_views.xml',
        'views/technic_parts_view.xml',
        'report/report.xml',
        'report/print_repair_parts_move_act.xml',
    ],

    'license': 'GPL-3',
    'installable': True,
    'auto_install': False,
}
