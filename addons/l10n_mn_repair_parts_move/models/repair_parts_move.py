# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import Warning
from odoo import exceptions

class RepairPartsMove(models.Model):
    _name = 'repair.parts.move'
    _description = 'Move spare parts'
    _order = 'create_date DESC'

    def name_get(self):
        result = []
        for record in self:
            name = '%s  %s -> [%s]' % (record.create_date, record.from_technic.name, record.to_technic.name)
            result.append((record.id, name))
        return result
    
    from_technic = fields.Many2one('technic', string = 'From Technic', domain="[('state', '!=', 'draft'), ('in_scrap', '=', False)]", required=True)
    to_technic = fields.Many2one('technic', string = 'To Technic', domain="[('state', '!=', 'draft'), ('in_scrap', '=', False)]", required=True)
    parts = fields.One2many('repair.parts.move.parts', 'repair_parts_move', string = 'Moving Spare Parts')
    state = fields.Selection([('new', 'New'),
                              ('transferred', 'Transferred'),
                              ('completed', 'Completed'),
                              ('cancelled', 'Cancelled')], string = 'State', default="new")
    wo_count = fields.Integer(string = 'WO count', default=0)
    assigned_to = fields.Many2one('res.users', string = 'Assigned to')
    planned_date = fields.Date(string = 'Planned Date')
    end_date = fields.Date('End Date')
    planned_hours = fields.Float(string = 'Planned Hours', default=0.5)
    work_orders = fields.One2many('work.order', 'repair_parts_move', string = 'Related Work Orders')
    project = fields.Many2one('project.project', string = 'Project')
    description = fields.Text('Description')

    @api.onchange('from_technic')
    def _onchange_from_technic(self):
        self.to_technic = None
        if self.from_technic:
            return {
                'domain': {
                    'to_technic': [('id', 'not in', [self.from_technic.id]), ('state', '!=', 'draft'), ('in_scrap', '=', False)]
                }
            }
        else:
            return {
                'domain': {
                    'to_technic': [('id', 'not in', [self.from_technic.id]), ('state', '!=', 'draft'), ('in_scrap', '=', False)]
                }
            }

    @api.model
    def create(self, vals):
        from_technic = self.env['technic'].browse([vals.get('from_technic')])
        vals['project'] = from_technic.project.id
        return super(RepairPartsMove, self).create(vals)

    @api.multi
    def action_transfer(self):
        for transit_part in self.parts:
            if transit_part.part.technic:
                transit_part.part.write({'technic': self.to_technic.id})
                parts_history = self.env['technic.parts.history'].search([('part', '=', transit_part.part.id), ('change_technic', '=', self.from_technic.id)], order='create_date desc', limit=1)
                parts_history.write({'repair_parts_move_id': self.id})
                parts_history = self.env['technic.parts.history'].search(
                    [('part', '=', transit_part.part.id),
                     ('change_technic', '=', self.to_technic.id)],
                    order='create_date desc', limit=1)
                parts_history.write({'description': u'%s->Сэлбэг шилжүүлэн тависан' %self.from_technic.name,
                                     'repair_parts_move_id': self.id})
        self.state = 'transferred'

    @api.multi
    def action_cancel(self):
        self.ensure_one()
        self.state = 'cancelled'

    @api.multi
    def print_act(self):
        form = self.read()
        data = {
            'ids': self.ids,
            'model': 'repair.parts.move',
            'form': form
        }
        ret = self.env['report'].get_action(self, 'l10n_mn_repair_parts_move.print_repair_parts_move_act', data=data)
        return ret

class RepairPartsMoveParts(models.Model):
    _name = 'repair.parts.move.parts'
    _description = 'Moving spare parts'

    repair_parts_move = fields.Many2one('repair.parts.move')
    part = fields.Many2one('technic.parts', string = 'Spare part')

    _sql_constraints = [
        ('r_p_m_part_unique',
         'UNIQUE(repair_parts_move, part)',
         "So many occurrences of certain spare part is not available for a moving!"),
    ]

    @api.model
    def create(self, values):
        line = super(RepairPartsMoveParts, self).create(values)
        if line.repair_parts_move.from_technic != line.part.technic:
            raise exceptions.ValidationError(_('Moving spare part "%s" is invalid. Please select correct spare parts.') % line.part.name_get()[0][1])
        return line
