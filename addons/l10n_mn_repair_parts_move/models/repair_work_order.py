# -*- coding: utf-8 -*-

from odoo import models, fields

class RepairWorkOrder(models.Model):
    _inherit = 'work.order'

    repair_parts_move = fields.Many2one('repair.parts.move', 'Spare parts moving order', ondelete='cascade')
