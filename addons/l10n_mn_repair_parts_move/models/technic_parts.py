
from odoo import models, fields, api


class TechnicPartsHistory(models.Model):
    _inherit = 'technic.parts.history'

    repair_parts_move_id = fields.Many2one('repair.parts.move', string='Repair parts move')