# -*- coding: utf-8 -*-

from odoo import models, fields, api

class PrintRepairPartsMoveAct(models.TransientModel):
    _name = 'report.l10n_mn_repair_parts_move.print_repair_parts_move_act'

    @api.multi
    def render_html(self, docargs, data):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('l10n_mn_repair_parts_move.print_repair_parts_move_act')
        repair_parts_move = self.env['repair.parts.move'].browse(self._context.get('active_ids'))
        docargs = {
            'doc_ids': data['ids'],
            'docs': repair_parts_move,
        }
        return report_obj.render('l10n_mn_repair_parts_move.print_repair_parts_move_act', docargs)
