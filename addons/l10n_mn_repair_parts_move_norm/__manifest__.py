# -*- coding: utf-8 -*-
{
    'name': "Сэлбэг шилжүүлэх, Засварын ажилбарын норм",
    'version': '1.0',
    'depends': ['base', 'l10n_mn_repair_parts_move', 'l10n_mn_repair_wo_norm'],
    'author': "Asterisk Technologies LLC",
    'category': 'Repair, Mongolian Modules',
    'description': """
        Ажилбарын норм суусан үед энэ модуль автоматаар сууж, сэлбэг шилжүүлэх модулийг нормын мэдээллээр өргөтгөнө.
    """,
    'data': [
        'views/repair_parts_move_views.xml',
    ],

    'license': 'GPL-3',
    'installable': True,
    'auto_install': True,
}
