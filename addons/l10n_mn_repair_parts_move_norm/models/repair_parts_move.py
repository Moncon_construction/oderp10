# -*- coding: utf-8 -*-

from odoo import models, fields, api, _, exceptions

class RepairPartsMove(models.Model):
    _inherit = 'repair.parts.move'

    remove_wo_norm = fields.Many2one('work.order.norm', 'Remove WO norm')
    install_wo_norm = fields.Many2one('work.order.norm', 'Install WO norm')
    remove_employee = fields.Many2many('hr.employee', 'hr_remove_employee_rel', string='Remove employee')
    install_employee = fields.Many2many('hr.employee','hr_install_employee_rel', string='Install employee')

    @api.onchange('remove_wo_norm', 'install_wo_norm')
    def onchange_norms(self):
        planned_hours = 0
        if self.remove_wo_norm:
            planned_hours = planned_hours + self.remove_wo_norm.working_hours
        if self.install_wo_norm:
            planned_hours = planned_hours + self.install_wo_norm.working_hours
        self.planned_hours = planned_hours

