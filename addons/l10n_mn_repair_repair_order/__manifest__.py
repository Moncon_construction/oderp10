# -*- coding: utf-8 -*-
{
    'name' : 'Repair - Repair Order',
    'version' : '1.0',
    'depends' : ['l10n_mn_repair_work_order', 'l10n_mn_repair_wo_norm', 'l10n_mn_technic'],
    'author' : 'Asterisk Technologies LLC',
    'category' : 'Mongolian Modules',
    'description' : """
        Repair Order
    """,
    'data':[
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/repair_order_view.xml',
        'views/sequence.xml',
    ],

    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
