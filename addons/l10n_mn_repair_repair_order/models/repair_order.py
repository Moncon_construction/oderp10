# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class RepairOrderType(models.Model):
    _name = 'repair.order.state'
    _description = 'RO Stage'
    _order = 'sequence'

    name = fields.Char(String='Stage name', required=True, translate=True)
    state_type = fields.Selection(
        [('draft', 'New'),
         ('planned', 'Planned'),
         ('open', 'Open'),
         ('parts_wait', 'Parts wait'),
         ('in_progress', 'In Progress'),
         ('cancel', 'Cancelled'),
         ('done', 'Done'), ], 'State')
    description = fields.Text(String='Description')
    sequence = fields.Integer(String='Sequence')
    fold = fields.Boolean(String='Folded in Kanban View',
                          help='This stage is folded in the kanban view when'
                          'there are no records in that stage to display.')


class RepairLocation(models.Model):
    _name = 'repair.order.location'

    name = fields.Char(String='Location name', required=True)


class RepairOrder(models.Model):
    _name = 'repair.order'
    _description = 'Repair Order'
    _order = 'sequence'

    @api.multi
    def stage_find(self, domain=[], order='sequence'):
        search_domain = []
        search_domain += list(domain)
        stage_ids = self.env['repair.order.state'].search(search_domain, order=order)
        if stage_ids:
            return stage_ids[0]
        return False

    @api.multi
    def _get_default_stage_id(self):
        """ Gives default stage_id """
        return self.stage_find([])

    @api.multi
    def _set_default_id(self):
        self.env['res.company']._company_default_get('work.order')

    name = fields.Char(String='Name', track_visibility='onchange', size=128, required=True, index=True)
    origin = fields.Char(String='Order Reference', readonly=True, copy=False)
    description = fields.Text(String='Description')
    priority = fields.Selection([('0', 'Low'), ('1', 'Normal'), ('2', 'High')], String='Priority', default=0)
    stage_id = fields.Many2one('repair.order.state', String='Stage', track_visibility='onchange', index=True, default=_get_default_stage_id)
    create_date = fields.Datetime(String='Create Date', readonly=True)
    write_date = fields.Datetime(String='Last Modification Date', readonly=True)  # not displayed in the view but it might be useful with base_action_rule module (and it needs to be defined first for that)
    date_start = fields.Datetime(String='Starting Date', copy=False)
    date_end = fields.Datetime(String='Ending Date', copy=False)
    notes = fields.Text(String='Deduction')
    user_id = fields.Many2one('res.users', String='Create user', track_visibility='onchange', readonly=True)
    company_id = fields.Many2one('res.company', String='Company', compute=_set_default_id)
    assigned_to = fields.Many2one('hr.employee', String='Assigned to', index=True)
    technic_id = fields.Many2one('technic', String='Technic', index=True)
    project_id = fields.Many2one('project.project', String = 'Project')
    location = fields.Many2one('repair.order.location', String='Location')
    repair_team_id = fields.One2many('repair.team', 'repair_id', String='Repair team')
    delay_cause_id = fields.One2many('delay.causes', 'repair_id', String='Delay causes')
    part_line_ids = fields.One2many('resource.use', 'repair_id', String='Part lines', readonly=True)
    work_orders = fields.Many2many('work.order', 'ro_wo_rel', 'ro_id', 'wo_ids', String='Work orders')

    _order = "priority desc, date_start, name, id"

    @api.onchange('technic_id')
    def onchange_technic_id(self):
        if self.technic_id:
            technic_obj = self.env['technic'].browse(self.technic_id.id)
            return {'domain': {'ro_norm_id': [('technic_configuration', '=', technic_obj.technic_norm_id.id)]}}
        else:
            return False

    @api.model
    def create(self, vals):
        if vals.get('origin', '/') == '/':
            vals['origin'] = self.env['ir.sequence'].next_by_code('repair.order') or '/'
        context = dict(self._context or {}, mail_create_nolog=True)
        order = super(RepairOrder, self).create(vals)
        self_obj = self.browse(order)
        if vals.has_key('ro_norm_id'):
            for ro_norm in self_obj.ro_norm_id.wo_norms:
                """Нэг ажлыг 2 хийж байвал 2 ажилбар үүсгэж байна."""
                for i in range(int(ro_norm.quantity)):
                    val = {
                        'name': ro_norm.wo_norm_id.name,
                        'wo_norm_id': ro_norm.wo_norm_id.id,
                        'repair_order_id': order[0],
                        'ro_norm_id': self_obj.ro_norm_id.id,
                        'technic': self_obj.technic_id.id,
                    }
                    self.env['work.order'].create(val)
        if vals.has_key('work_orders'):
            if vals['work_orders']:
                wo_ids = vals['work_orders'][0][2]
            else:
                wo_ids = []
            for wo in self.env['work.order'].browse(wo_ids):
                self.env['work.order'].write({'repair_order_id': order[0]})
        ro_w_orders = self.env['work.order'].search([('repair_order_id', '=', order.id)])
        self.write({'work_orders': [(6, 0, ro_w_orders)]})
        return order

    @api.multi
    def write(self, vals):
        self_obj = self.browse(self._ids)
        order = super(RepairOrder, self).write(vals)
        if vals.has_key('ro_norm_id'):
            wo_obj = self.env['work.order']
            '''Засварын захиалга дээр Засварын захиалгын норм сонгоход 
                тухайн нормын ажилбарын нормоор Засварын ажилбар 
                үүссэн бол тэдгээр ажитлбаруудын ID-ийг гаргаж ирнэ'''
            ro_w_orders = wo_obj.search([('repair_order_id', '=', self._ids), ('ro_norm_id', '=', self_obj.ro_norm_id.id)])
            if ro_w_orders:
                '''ro_w_orders хувьсагч утгатай үед Засварын захиалгын норм өөрчлөгдөөгүй байна гэж үзээд хуучин утгыг буцаан онооно'''
                ro_w_orders = wo_obj.search([('repair_order_id', '=', self._ids)])
                self.write(self._ids, {'work_orders': [(6, 0, ro_w_orders)]})
            else:
                '''ro_w_orders уг хувьсагч хоосон үед Засварын захиалгын нормгүйгээр үүсэн Засварын ажилбаруудаас бусад ажилбаруудыг устгана '''
                ro_w_orders = wo_obj.search([('repair_order_id', '=', self._ids), ('ro_norm_id', '!=', False)])
                wo_obj.unlink(ro_w_orders)
                '''Шинэ сонгосон Засварын захиалгын норм дээрх ажилбарын нормоор Ажилбар үүсгэнэ.'''
                for ro_norm in self_obj.ro_norm_id.wo_norms:
                    """Нэг ажлыг 2 хийж байвал 2 ажилбар үүсгэж байна."""
                    for i in range(int(ro_norm.quantity)):
                        val = {
                            'name': ro_norm.wo_norm_id.name,
                            'wo_norm_id': ro_norm.wo_norm_id.id,
                            'repair_order_id': self._ids[0],
                            'ro_norm_id': self_obj.ro_norm_id.id,
                            'technic': self_obj.technic_id.id,
                        }
                        wo_obj.create(val)
                '''Тухайн засварын захиалга дээр бүртгэлтэй байгаа бүх засварын ажилбарын ID-ийг олоод many2many талбар дээрээ replace хийнэ.'''
                ro_w_orders = wo_obj.search([('repair_order_id', '=', self._ids)])
                self.write(self._ids, {'work_orders': [(6, 0, ro_w_orders)]})
        if vals.has_key('work_orders'):
            resourse_id = self.env['resource.use'].search([('repair_id', '=', self._ids)])
            resourse_id.unlink()
            for wo_obj in self_obj.work_orders:
                self.env['work.order'].write({'repair_order_id': self._ids[0]})
                for part_obj in wo_obj.products:
                    val = {
                        "repair_id": self._ids[0],
                        'product_id': part_obj.product_id.id,
                        'product_qty': part_obj.product_qty,
                        'work_order': part_obj.order_id.id,
                    }
                    self.env['resource.use'].create(val)
        return order


class RepairTeam(models.Model):
    _name = 'repair.team'

    repair_id = fields.Many2one('repair.order', String='Repair id')
    worker_id = fields.Many2one('res.users', String='Worker', required=True)
    work_time = fields.Float(String='Work time')


class DelayCauses(models.Model):
    _name = 'delay.causes'

    repair_id = fields.Many2one('repair.order', String='Repair id')
    cause = fields.Char(String='Cause', required=True)
    cause_date = fields.Date(String='Cause date')


class ResourceUse(models.Model):
    _name = 'resource.use'

    repair_id = fields.Many2one('repair.order', String='Repair id', readonly=True, ondelete='cascade')
    product_id = fields.Many2one('product.product', String='Product', readonly=True)
    product_qty = fields.Float(String='Quantity', digits=(12, 1), readonly=True)
    work_order = fields.Many2one('work.order', String='Work order', readonly=True, ondelete='cascade')


class WorkOrderInherit(models.Model):
    _inherit = 'work.order'

    repair_order_id = fields.Many2one('repair.order', String='Repair Order')
    # ro_norm_id = fields.Many2one('ro.norm')

    # @api.onchange('technic_id')
    # def onchange_technic_id(self):
    #     technic_obj = self.env['technic'].browse(self.technic_id)
    #     return {'domain': {'wo_norm_id': [('technic_configuration', '=', technic_obj.technic_norm_id.id)]}}


class TechnicPassportInherit(models.Model):
    _inherit = 'technic'

    @api.multi
    def _planned_ro_count(self):
        repair_id = self.env['repair.order'].search([('technic_id', '=', self._ids)])
        return {self._ids[0]: len(repair_id)}

    @api.multi
    def _planned_wo_count(self):
        wo_id = self.env['work.order'].search([('technic', '=', self._ids)])
        return {self._ids[0]: len(wo_id)}

    ro_count = fields.Integer(compute=_planned_ro_count, string="Repair orders", stored=False)
    wo_count = fields.Integer(compute=_planned_wo_count, string="Work orders", stored=False)
