# -*- coding: utf-8 -*-
{
    'name': "Sales of Repair Service",
    'description': """
        Засвараас борлуулалт үүсгэх бол энэ модулийг суулгана
    """,
    'author': "Asterisk Technologies LLC",
    'website': "http://asterisk-tech.mn",
    'category': 'Repair, Mongolian Modules',
    'version': '1.0',
    'depends': ['l10n_mn_repair_wo_norm', 'l10n_mn_sale', 'l10n_mn_repair_repair_order'],
    'data': [
        'views/work_order_view.xml',
        'views/repair_order_view.xml',
        'wizard/wo_create_sales_order_view.xml',
    ],
    'contributors': ['Asterisk Technologies LLC <info@asterisk-tech.mn>'],
}
