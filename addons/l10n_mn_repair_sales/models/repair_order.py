# -*- coding: utf-8 -*-
import math

from odoo import api, fields, models, _

class RepairOrder(models.Model):
    _inherit = 'repair.order'
    
    def _compute_total_sales_amount(self):
        res = {}
        for order in self:
            amount = 0
            if order.work_orders:
                for wo in order.work_orders:
                    amount += wo.sale_order_amount
            res[order.id] = amount
        return res
    
    
    total_sales_amount = fields.Float(compute = '_compute_total_sales_amount', string='Total Sales Amount')
    
            