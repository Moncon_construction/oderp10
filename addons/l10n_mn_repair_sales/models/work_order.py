# -*- coding: utf-8 -*-
import math

from odoo import api, fields, models, _
from odoo.exceptions import UserError

class WorkOrder(models.Model):
    _inherit = 'work.order'

    @api.multi
    def _count_sale_order(self):
        res = {}
        for order in self:
            res[order.id] = 1 if order.sale_order_id else 0
        return res

    @api.multi
    def _compute_sale_order_amount(self):
        res = {}
        for order in self:
            amount = 0
            if order.products:
                for line in order.products:
                    amount += line.product_id.lst_price * line.product_qty
            res[order.id] = amount
        return res

    sale_order_id = fields.Many2one('sale.order', 'Sale Order')
    sale_order_count = fields.Integer(compute='_count_sale_order',  string='Sale Order Count')
    sale_order_amount = fields.Float(compute='_compute_sale_order_amount', string='Sales Amount')

    @api.multi
    def view_sale_order(self):

        mod_obj = self.env['ir.model.data']
        dummy, action_id = tuple(mod_obj.get_object_reference('sale', 'action_orders'))
        action = self.env['ir.actions.act_window'].browse(action_id).read([])[0]
        order_id = []
        for order in self:
            order_id = order.sale_order_id.id
        action['domain'] = "[('id','='," + str(order_id) + ")]"
        return action

    def action_create_sale_order(self):
        mod_obj = self.env['ir.model.data']

        obj = self
        if not obj.partner_id:
            raise UserError(_('Please select partner of this work order.'))
        if not obj.contract_id:
            raise UserError(_('Please select contract of this work order.'))
        if not obj.norm:
            raise UserError(_('Please select norm of this work order.'))
        if not obj.norm.product_id:
            raise UserError(_('Please select product of work order\'s norm.'))
        if obj.sale_order_id:
            raise UserError(_('One of selected work orders (%s) already has sales order.') % obj.origin)

        vals = {
                'partner_id': obj.partner_id.id,
                 'project_id': obj.contract_id.id,
                 'date_order': obj.planned_date,
                 'origin': obj.origin,
            }
        sale_order_id = self.env['sale.order'].create(vals)

        self.env['sale.order.line'].create({'order_id': sale_order_id,
                                              'product_id': obj.norm.product_id.id,
                                              'name': obj.norm.product_id.name,
                                              'product_uom_qty': 1,
                                              'price_display': obj.norm.product_id.lst_price,
                                              'product_uom': obj.norm.product_id.uom_id.id,
                                              'price_unit': obj.norm.product_id.lst_price,
                                              'tax_id': [(6, 0, [x.id for x in obj.norm.product_id.taxes_id])]
                                            })

        self.env['work.order'].write({'sale_order_id': sale_order_id})

        res = mod_obj.get_object_reference('sale', 'view_order_form')
        res_id =  res and res[1] or False,

        return {
            'name': _('Sale Order'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': [res_id],
            'res_model': 'sale.order',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            'res_id': sale_order_id,
        }

    def unlink(self):
        for wo in self:
            if wo.sale_order_id:
                raise Warning(_('Inappropriate Operation'), _('Delete related sale order first.'))
        return super(WorkOrder, self).unlink()