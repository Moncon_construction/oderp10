# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import api, models, fields
from odoo.exceptions import UserError

class work_order_create_sales(models.Model):
    _name = "work.order.create.sales"
    _description = "Create Sales Order"

    def create_sales_order(self):
        active_ids = self.env['active_ids'] or []

        work_order = self.env['work.order']
        mod_obj = self.env['ir.model.data']
        
        partner_ids = []
        contract_ids = []
        months = []
        
        rec = None
        records = work_order.browse(self)
        origin = ''
        for record in records:
            if not record.partner_id:
                raise UserError(_('Please select partner of this work order.'))
            if not record.contract_id:
                raise UserError(_('Please select contract of this work order.'))
            if not record.norm:
                raise UserError(_('Please select norm of this work order.'))
            if not record.norm.product_id:
                raise UserError(_('Please select product of work order\'s norm.'))
            if record.sale_order_id:
                raise UserError(_('One of selected work orders (%s) already has sales order.') % record.origin)
            
            if record.partner_id.id not in partner_ids:
                partner_ids.append(record.partner_id.id)
            if record.contract_id.id not in contract_ids:
                contract_ids.append(record.contract_id.id)
            if record.planned_date[:7] not in months:
                months.append(record.planned_date[:7])
            origin += record.origin + ', '
            rec = record
                
        if len(partner_ids) > 1:
            raise UserError(_('You can only create sales of one partner\'s work order.'))
        if len(contract_ids) > 1:
            raise UserError(_('You can only create sales of one contract\'s work order.'))
        if len(months) > 1:
            raise UserError(_('You can only create sales of one month\'s work order.'))
        
        day = rec.planned_date[:7]+'-25'
        
        sale_order_id = self.self['sale.order'].create({'partner_id': rec.partner_id.id,
                                                             'project_id': rec.contract_id.id,
                                                             'date_order': datetime.strptime(day,"%Y-%m-%d"),
                                                             'origin': origin
                                                             })
        so_line_vals = {}
        for record in records:
            if record.norm.product_id.id not in so_line_vals.keys():
                so_line_vals[record.norm.product_id.id] = {'order_id': sale_order_id,
                                                          'product_id': record.norm.product_id.id,
                                                          'name': record.norm.product_id.name,
                                                          'product_uom_qty': 1,
                                                          'product_uos_qty': 1,
                                                          'price_display': record.norm.product_id.lst_price,
                                                          'product_uom': record.norm.product_id.uom_id.id,
                                                          'price_unit': record.norm.product_id.lst_price,
                                                          'tax_id': [(6, 0, [x.id for x in record.norm.product_id.taxes_id])]
                                                          }
            else:
                so_line_vals[record.norm.product_id.id]['product_uom_qty'] += 1
                so_line_vals[record.norm.product_id.id]['product_uos_qty'] += 1
            if record.products:
                for line in record.products:
                    if line.product_id.id not in so_line_vals.keys():
                        so_line_vals[line.product_id.id] = {'order_id': sale_order_id,
                                                              'product_id': line.product_id.id,
                                                              'name': line.product_id.name,
                                                              'product_uom_qty': line.product_qty,
                                                              'product_uos_qty': line.product_qty,
                                                              'price_display': line.product_id.lst_price,
                                                              'product_uom': line.product_id.uom_id.id,
                                                              'price_unit': line.product_id.lst_price,
                                                              'tax_id': [(6, 0, [x.id for x in line.product_id.taxes_id])]
                                                              }
                    else:
                        so_line_vals[line.product_id.id]['product_uom_qty'] += line.product_qty
                        so_line_vals[line.product_id.id]['product_uos_qty'] += line.product_qty
                    
        for val in so_line_vals.values():
            self.env['sale.order.line'].create(self)
        
        self.env['work.order'].write(self, {'sale_order_id': sale_order_id})
        
        res = mod_obj.get_object_reference(self, 'sale', 'view_order_form')
        res_id = res and res[1] or False,

        return {
            'name': _('Sale Order'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': [res_id],
            'res_model': 'sale.order',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            'res_id': sale_order_id,
        }            

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
