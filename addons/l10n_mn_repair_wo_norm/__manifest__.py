# -*- coding: utf-8 -*-
{
    'name': "Repair - Work Order Norm",
    'version': '1.0',
    'summary': """
        Work Order Norm""",
    'depends': ['analytic', 'l10n_mn_repair_work_order'],
    'website': "http://www.asterisk-tech.mn",
    'author': "Asterisk Technologies LLC",
    'category': 'Repair, Mongolian Modules',
    'description': """
        Засварын ажилбарт нормын мэдээллийг бүртгэх боломжтой болно.
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/work_order_norm_views.xml',
        'views/work_order_views.xml',
        'views/hr_employee_view.xml',
        'views/repairman_rating_view.xml',
        'views/work_order_compute_view.xml'
    ],
}
