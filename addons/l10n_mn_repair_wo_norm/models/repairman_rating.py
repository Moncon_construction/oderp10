# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

from odoo import fields, models, api  # @UnresolvedImport

class RepairmanRating(models.Model):
    _name = 'repairman.rating'
    _description = 'Repairman Rating'

    name = fields.Char('Name')
    description = fields.Text('Description')