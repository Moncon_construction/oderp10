# -*- encoding: utf-8 -*-
from odoo import api, fields, models, _

class WorkOrder(models.Model):
    _inherit = 'work.order'

    norm = fields.Many2one('work.order.norm', string='Work order norm')

    @api.onchange('norm')
    def onchange_norm(self):
        if self.norm:
            for norm in self.norm:
                norm_obj = self.env['work.order.norm'].browse(norm.id)
                if norm:
                    if norm.working_hours:
                        return {
                            'value': {
                                'planned_hours': norm.working_hours,
                                'name': norm.name,
                            }
                        }
        return False

    @api.model
    def create(self, vals):
        def create_products(self, vals, order_id):
            work_order_norm_products = self.env['work.order.norm.products']
            if 'norm' in vals:
                wo_norm_products = work_order_norm_products.search([('norm_id', '=', vals['norm'])])
                for wo_norm_product in wo_norm_products:
                    for wo_norm in work_order_norm_products.browse(wo_norm_product.id):
                        val = {
                            'order_id': order_id.id,
                            'product_id': wo_norm.product.id,
                            'product_qty': wo_norm.quantity,
                        }
                        self.env['work.order.products'].create(val)

        norm_id = vals.get('norm')
        if norm_id:
            norm_obj = self.env['work.order.norm'].browse(norm_id)
            if norm_obj.is_parent:
                vals['is_parent'] = True
            else:
                vals['is_parent'] = False
        order_id = super(WorkOrder, self).create(vals)

        # create products
        if not vals.get('products'):
            create_products(self, vals, order_id)

        # create child work orders
        if norm_id:
            norm_obj = self.env['work.order.norm'].browse(norm_id)
            if norm_obj.is_parent:
                for norm in norm_obj.wo_norms:
                    vals.update({'norm': norm.id, 'name': norm.name, 'planned_hours': norm.working_hours, 'parent_wo': order_id.id})
                    vals.pop('origin', None)
                    vals.pop('is_parent', None)
                    order_child_id = super(WorkOrder, self).create(vals)
                    create_products(self, vals, order_child_id)

        return order_id

    @api.multi
    def write(self, vals):
        res = super(WorkOrder, self).write(vals)
        if res:
            for wo in self:
                # create products from new selected norm
                if not wo.products:
                    if wo.norm:
                        work_order_norm_products = self.env['work.order.norm.products']
                        wo_norm_product_ids = work_order_norm_products.search([('norm_id', '=', wo.norm.id)])
                        for wo_norm_product in wo_norm_product_ids:
                            for wo_norm in work_order_norm_products.browse(wo_norm_product.id):
                                val = {
                                    "order_id": wo.id,
                                    'product_id': wo_norm.product.id,
                                    'product_qty': wo_norm.quantity,
                                }
                                self.env['work.order.products'].create(val)
        return res
