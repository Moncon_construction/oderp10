# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
import datetime
from odoo import fields, models, api  # @UnresolvedImport
from odoo.exceptions import ValidationError


class WorkOrder(models.Model):
    _inherit = 'work.order'

    def _set_team_cost(self):
        for rec in self:
            team_cost = 0
            for employee_rate in rec.repair_team:
                for norm_rate in rec.norm.workers:
                    if employee_rate.rating == norm_rate.rating_id:
                        team_cost += norm_rate.rating_cost
            rec.team_cost = team_cost / len(rec.repair_team) if team_cost > 0 else team_cost

    team_cost = fields.Float(compute=_set_team_cost, strint='Team cost')


class WorkOrderWork(models.Model):
    _inherit = "work.order.work"

    def _set_owner_cost(self):
        for rec in self:
            owner_cost = 0
            if rec.employee in rec.order_id.repair_team:
                for norm_wo in rec.order_id.norm.workers:
                    if norm_wo.rating_id == rec.employee.rating:
                        owner_cost = norm_wo.rating_cost * rec.percent / 100 if norm_wo.rating_cost > 0 else 0
            rec.owner_cost = owner_cost

    def _set_man_hour(self):
        for rec in self:
            rec.man_hour = rec.hours * rec.unit1

    def _set_planned_hour(self):
        for rec in self:
            rec.planned_hour = rec.order_id.norm.working_hours * rec.percent / 100

    def _set_productivity_cost(self):
        for rec in self:
            rec.productivity_cost = rec.owner_cost * rec.planned_hour / rec.hours if rec.hours > rec.planned_hour else rec.owner_cost

    def _set_pay_owner_cost(self):
        for rec in self:
            rec.pay_owner_cost = rec.productivity_cost * rec.unit1

    rating_rel = fields.Many2one('repairman.rating', 'Rating', related='employee.rating', store=True)
    percent = fields.Float('Percent')
    unit1 = fields.Float('Work Number')
    planned_hour = fields.Float(compute=_set_planned_hour, string='Planned Hour')
    man_hour = fields.Float(compute=_set_man_hour, string='Man Hour')
    productivity_cost = fields.Float(compute=_set_productivity_cost, string='Productivity Cost')
    owner_cost = fields.Float(compute=_set_owner_cost, string='Owner cost')
    pay_owner_cost = fields.Float(compute=_set_pay_owner_cost, string='Pay owner cost')

    @api.model
    def create(self, values):
        order = super(WorkOrderWork, self).create(values)
        values.update({'account': order.order_id.project.analytic_account_id.id, 'wow_id': order.id})
        # self._creation_analytic_entries(values) Цагийн хуудасгүй компанид засварчны ажлын тойм бүртгэхэд заавал цагийн хуудас нэхдэг байх шаардлагагүй гэж үзэж тайлбар болгов. Өнөржаргал
        return order

    @api.multi
    def write(self, values):
        data = {}
        order = super(WorkOrderWork, self).write(values)
        update_sheet = self.env['account.analytic.line'].search([('wow_id', '=', self.id)])

        if 'employee' in values:
            update_sheet.unlink()
            values = {
                'date': self.date,
                'name': self.name,
                'hours': self.hours,
                'employee': self.employee.id,
            }
            values.update({'account': self.order_id.project.analytic_account_id.id, 'wow_id': self.id})
            # self._creation_analytic_entries(values) Цагийн хуудасгүй компанид засварчны ажлын тойм бүртгэхэд заавал цагийн хуудас нэхдэг байх шаардлагагүй гэж үзэж тайлбар болгов. Өнөржаргал
        else:
            if 'name' in values:
                data['name'] = values['name']
            if 'hours' in values:
                data['unit_amount'] = values['hours']
            if 'date' in values:
                data['date'] = values['date']
            update_sheet.write(data)

        return order

    @api.multi
    def _creation_analytic_entries(self, vals):
        timesheet = self.env['hr_timesheet_sheet.sheet']
        analytic_timesheet = self.env['account.analytic.line']
        employee = self.env['hr.employee'].browse(vals['employee'])
        employee_timesheet = timesheet.search([('employee_id', '=', vals['employee']),
                                               ('date_from', '<=', vals['date']),
                                               ('date_to', '>=', vals['date'])])
        if employee.journal_id:
            journal = employee.journal_id.id
        else:
            raise ValidationError("Сонгосон ажилчинд шинжилгээний журнал тодорхойлогдоогүй байна !")

        if employee_timesheet:
            data = {
                'sheet_id': employee_timesheet.id,
                'date': vals['date'],
                'account_id': vals['account'],
                'journal_id': journal,
                'name': vals['name'],
                'unit_amount': vals['hours'],
                'wow_id': vals['wow_id'],
            }
            self.env['account.analytic.line'].create(self._cr, employee.user_id.id, data)
        else:
            raise ValidationError(
                "Ажилтанд цаг бүртгэл үүсгэнэ үү ! \n Үүний дараа тухайн ажилтанд цаг бүртгэлтэй нь холбоотой ажлын тойм үүсгэх боломжтой")


class HrAnalyticTimesheet(models.Model):
    _inherit = "account.analytic.line"

    wow_id = fields.Many2one('work.order.work', 'Work order work')