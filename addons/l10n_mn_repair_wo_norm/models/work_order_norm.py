# -*- encoding: utf-8 -*-

from odoo import api, fields, models , _
from odoo.exceptions import Warning, ValidationError
from odoo import exceptions
class WorkOrderNorm(models.Model):
    _name = 'work.order.norm'

    name = fields.Char(string='Name', required=True,)
    working_hours = fields.Float(string='Working hours', required=True)
    products = fields.One2many('work.order.norm.products', 'norm_id')
    workers = fields.One2many('work.order.norm.workers', 'norm_id')
    wo_norms = fields.Many2many('work.order.norm', 'wo_norm_self_rel', 'parent_norm_id', 'child_norm_id', string='Sub norms')
    is_parent = fields.Boolean(string = 'Is sub norm')
    product_id = fields.Many2one('product.product', 'Service', domain=[('type', '=', 'service')])

    _sql_constraints = [
        ('hour_check',
         'CHECK(working_hours > 0.0)',
         "Working hours must be greater than 0!"),
    ]

    @api.onchange('wo_norms')
    def onchange_norms(self):
        sum_hour = 0
        if self.wo_norms:
            for norm in self.wo_norms:
                norm_obj = self.env['work.order.norm'].browse(norm.id)
                if norm_obj:
                    for norm in norm_obj:
                        sum_hour += norm.working_hours
                    return {
                        'value': {'working_hours': sum_hour}
                        }
        return False

    @api.multi
    def write(self, vals):
        if 'wo_norms' in vals:
            time = self.env['work.order.norm']
            vals.update({'working_hours': time.working_hours})
        if 'is_parent' in vals:
            if not vals.get('is_parent'):
                if 'wo_norms' in vals:
                    if vals.get('wo_norms')[0][2]:
                        raise Warning(_('Must be deleted sub norms!!!'))
                else:
                    if self.browse(self._ids).wo_norms.ids:
                        raise Warning(_('Must be deleted sub norms!!!'))
            else:
                products = self.browse(self._ids).products
                if products:
                    products.unlink()
        return super(WorkOrderNorm, self).write(vals)

    @api.model
    def create(self, vals):
        if vals.get('wo_norms') and vals.get('wo_norms')[0][2][0]:
            time = self.env['work.order.norm'].browse(vals.get('wo_norms')[0][2][0])
            vals.update({'working_hours': time.working_hours})

        # is_parent нь True утгатай байвал vals-аас products-ийг устгана.
        if vals.get('is_parent'):
            if len(vals.get('products')) > 0:
                vals.pop('products')
        plan = super(WorkOrderNorm, self).create(vals)
        return plan

    @api.multi
    def unlink(self):
        if self.env['work.order'].search([('norm', '=', self.id)]):
            raise exceptions.ValidationError(_("Can't delete work order norm. Used in Work Order"))
        ret = super(WorkOrderNorm, self).unlink()
        return ret

class WorkOrderNormProducts(models.Model):
    _name = 'work.order.norm.products'

    norm_id = fields.Many2one('work.order.norm', string='Work Order Norm')
    product = fields.Many2one('product.product', string='Product')
    quantity = fields.Float(string='Quantity', required=True,default = 1)
    product_uom = fields.Many2one('product.uom', string='Unit of Measure')
    description = fields.Char(string='Description')

    _sql_constraints = [
        ('w_o_n_product_uniq',
         'unique(norm_id, product)',
         u'Барааны мэдээлэл давхацсан байна!!!'),
    ]

    @api.onchange('product')
    def onchange_product(self):
        product_obj = self.env['product.product'].browse(self.product.id)
        return {
            'value': {
                'product_uom': product_obj.uom_id.id
            }
        }

class WorkOrderNormWorkers(models.Model):
    _name = 'work.order.norm.workers'


    norm_id = fields.Many2one('work.order.norm', 'Work Order Norm')
    rating_id = fields.Many2one('repairman.rating', 'Rating')
    rating_cost = fields.Float('Rating cost', required=True)
    man_hours = fields.Float('Man hours', required=True)