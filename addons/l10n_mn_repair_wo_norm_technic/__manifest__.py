# -*- coding: utf-8 -*-
{

    'name': "Repair - Work Order, Work Order Norm and Technic",
    'version': '1.0',
    'depends': ['base', 'l10n_mn_repair_wo_norm', 'l10n_mn_repair_wo_technic'],
    'author': "Asterisk Technologies LLC",
    'website': "http://www.asterisk-tech.mn",
    'category': 'Repair, Technic, Mongolian Modules',
    'description': """
    Засварын ажилбарын норм болон техникийн мэдээллийг холбогч модуль.
""",
    'summary': """
        Work Order, Work Order Norm and Technic""",
    'data': [
        'views/work_order_norm_views.xml',
        'views/work_order_views.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True,
}