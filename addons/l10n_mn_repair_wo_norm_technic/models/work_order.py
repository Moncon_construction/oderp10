# -*- encoding: utf-8 -*-

from odoo import models, fields, api

class work_order(models.Model):
    _inherit = 'work.order'

    @api.onchange('technic')
    def onchange_technic(self):
        if self.technic:
            wo_norm_ids = self.env['work.order.norm'].search([('technic_norm', '=', self.technic.technic_norm_id.id)])
            return {
                'domain': {
                    'norm': [('id', 'in', wo_norm_ids.ids)]
                },
                'value': {
                    'norm': False,
                },
            }
