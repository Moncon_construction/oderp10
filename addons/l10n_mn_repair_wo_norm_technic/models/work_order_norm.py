# -*- encoding: utf-8 -*-

from odoo import fields, models

class WorkOrderNorm(models.Model):
    _inherit = 'work.order.norm'

    technic_type = fields.Many2one('technic.type', string='Technic Type')
    technic_norm = fields.Many2one('technic.norm', string='Technic Norm')

