# -*- coding: utf-8 -*-
{
    'name': "Repair - Work Order Plan",
    'version': '1.0',
    'depends': ['l10n_mn_repair_work_order'],
    'author': "Asterisk Technologies LLC",
    'website': "http://www.asterisk-tech.mn",
    'category': 'Repair, Mongolian Modules',
    'description': """
        Засварын төлөвлөгөө бүртгэх модуль.
    """,
    'summary': """
        Work Order Plan""",

    'data': [
        'security/wo_plan_security.xml',
        'security/ir.model.access.csv',
        'views/sequence.xml',
        'views/work_order_views.xml',
        'views/work_order_plan_views.xml',
        'views/menu.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False,
}
