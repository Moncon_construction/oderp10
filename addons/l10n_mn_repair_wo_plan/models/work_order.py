# -*- coding: utf-8 -*-
from odoo import models, fields

class WorkOrder(models.Model):
    _inherit = 'work.order'

    plan = fields.Many2one('work.order.plan', 'Work Order Plan', ondelete='cascade')
