# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import Warning

class WorkOrderPlan(models.Model):
    _name = 'work.order.plan'
    _description = 'Work order plan'
    _order = "create_date DESC"

    name = fields.Char('Name', required=True)
    origin = fields.Char('Plan reference', readonly=True, copy=False)
    date_start = fields.Date('Starting Date', index=True, required=True)
    date_end = fields.Date('Ending Date', index=True,)
    project = fields.Many2one('project.project', 'Project', required=True)
    wo_count = fields.Integer(compute='_planned_wo_count', string="Planned WO")
    tasks = fields.One2many('work.order.plan.task', 'plan', 'Tasks')
    progress = fields.Float(compute='_plan_progress', string='Progress')
    work_orders = fields.One2many('work.order', 'plan', 'Related work orders')
    state = fields.Selection([('draft', 'New'),
                              ('confirm', 'Confirmed')], 'State', default='draft', readonly=True)

    @api.multi
    def _planned_wo_count(self):
        for plan in self:
            plan.wo_count = len(plan.work_orders)

    @api.multi
    def _plan_progress(self):
        for self_obj in self:
            sum_hour = 0
            do_hour = 0
            wo_obj = self.env['work.order'].search([('plan', '=', self_obj.id)])
            for wo in wo_obj:
                wo_do_hour = 0
                sum_hour += wo.planned_hours
                for work in wo.works:
                    wo_do_hour += work.hours
                if wo_do_hour > wo.planned_hours:
                    do_hour += wo.planned_hours
                else:
                    do_hour += wo_do_hour
            if sum_hour == 0:
                sum_hour = 1
            self_obj.progress = (do_hour * 100) / sum_hour

    @api.onchange('name')
    def onchange_name(self):
        projects = self.env['project.project'].search([('use_repair', '=', True)])
        if len(projects) == 1:
            self.project = projects
        return {
            'domain': {
                'project': [('id', 'in', projects.ids)]
            }
        }

    @api.model
    def create(self, vals):
        if vals.get('origin', '-') == '-':
            vals['origin'] = self.env['ir.sequence'].next_by_code('work.order.plan')

        return super(WorkOrderPlan, self).create(vals)

    @api.multi
    def unlink(self):
        for plan in self:
            for wo in plan.work_orders:
                if wo.stage.state_type != 'draft':
                    raise Warning(_("Can't delete plan with started work order: %s") % wo.name)
        return super(WorkOrderPlan, self).unlink()

    @api.multi
    def action_confirm(self):
        self.ensure_one()

        for task in self.tasks:
            vals = {
                'project': task.plan.project.id,
                'name': task.name,
                'planned_date': task.plan.date_start,
                'end_date': task.plan.date_end,
                'planned_hours': task.hours,
                'description': task.description,
                'plan': task.plan.id
            }
            task.create_work_order(vals)

        self.write({'state': 'confirm'})

    @api.multi
    def action_cancel(self):
        self.ensure_one()
        self.work_orders.unlink()
        self.write({'state': 'draft'})

class WorkOrderPlanTask(models.Model):
    _name = 'work.order.plan.task'

    plan = fields.Many2one('work.order.plan', 'Work Order Plan')
    name = fields.Char('Task name', required=True)
    hours = fields.Float('Planned hours', required=True)
    description = fields.Text('Description')
    work_order = fields.Many2one('work.order')

    _sql_constraints = [
        ('hour_check',
         'CHECK(hours > 0.0)',
         "Planned hours must be greater than 0!"),
    ]

    @api.multi
    def unlink(self):
        for task in self:
            task.work_order.unlink()
        return super(WorkOrderPlanTask, self).unlink()

    @api.multi
    def create_work_order(self, vals):
        self.ensure_one()
        wo = self.env['work.order'].create(vals)
        self.work_order = wo
