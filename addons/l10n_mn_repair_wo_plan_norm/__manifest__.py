# -*- coding: utf-8 -*-
{
    'name': "Repair - Work Order, Work Order Norm and Work Order Plan",
    'version': '1.0',
    'depends': ['base', 'l10n_mn_repair_wo_norm', 'l10n_mn_repair_wo_plan'],
    'author': "Asterisk Technologies LLC",
    'website': "http://www.asterisk-tech.mn",
    'category': 'Repair, Mongolian Modules',
    'description': """
        Засварын ажилбарын норм болон Засварын ажилбарын төлөвлөгөө модулиудыг холбогч модуль.
    """,
    'summary': """
        Work Order Norm and Work Order Plan""",
    'data': [
        'views/work_order_plan_views.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True,
}
