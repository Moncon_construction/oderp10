# -*- encoding: utf-8 -*-

from odoo import models, fields, api

class WorkOrderPlanTask(models.Model):
    _inherit = 'work.order.plan.task'

    norm = fields.Many2one('work.order.norm', 'Work order norm')

    @api.onchange('norm')
    def onchange_norm(self):
        self.name = self.norm.name
        self.hours = self.norm.working_hours

    @api.multi
    def create_work_order(self, vals):
        self.ensure_one()
        vals['norm'] = self.norm.id
        super(WorkOrderPlanTask, self).create_work_order(vals)
