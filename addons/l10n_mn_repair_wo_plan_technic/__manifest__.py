# -*- coding: utf-8 -*-
{
    'name': "Repair - Work Order Plan and Technic",
    'version': '1.0',
    'depends': ['base', 'l10n_mn_repair_wo_plan', 'l10n_mn_repair_wo_technic'],
    'author': "Asterisk Technologies LLC",
    'website': "http://www.asterisk-tech.mn",
    'category': 'Repair, Mongolian Modules',
    'description': """
        Засварын ажилбарын төлөвлөгөө болон Техник модулиудыг холбогч модуль.
    """,
    'summary': """
        Work Order Plan and Technic""",

    'data': [
        'views/work_order_plan_views.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True,
}
