# -*- encoding: utf-8 -*-
from odoo import models, fields, api

class WorkOrderPlanTask(models.Model):
    _inherit = 'work.order.plan.task'

    technic = fields.Many2one('technic')

    @api.multi
    def create_work_order(self, vals):
        self.ensure_one()
        vals['technic'] = self.technic.id
        super(WorkOrderPlanTask, self).create_work_order(vals)
