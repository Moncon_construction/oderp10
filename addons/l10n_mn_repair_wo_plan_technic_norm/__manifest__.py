# -*- coding: utf-8 -*-
{
    'name': "Repair - Work order plan, Norm, Technic",
    'version': '1.0',
    'depends': ['l10n_mn_repair_wo_plan_norm', 'l10n_mn_repair_wo_plan_technic'],
    'author': "Asterisk Technologies LLC",
    'website': "http://www.asterisk-tech.mn",
    'category': 'Repair, Mongolian Modules',
    'description': """
        Засварын төлөвлөгөө, норм, төлөвлөгөөг холбох модуль.
    """,
    'summary': """
        Work Order Plan Installed Work Order Norm domain for Technic""",

    'data': [],

    'license': 'GPL-3',
    'installable': True,
    'auto_install': True,
}
