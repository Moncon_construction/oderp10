# -*- encoding: utf-8 -*-
from odoo import models, api

class WorkOrderPlanTask(models.Model):
    _inherit = 'work.order.plan.task'

    @api.onchange('technic')
    def onchange_technic(self):
        norms = self.env['work.order.norm'].search(['|', ('technic_norm', '=', self.technic.technic_norm_id.id), ('technic_norm', '=', False)])
        # return {
        #     'domain': {
        #         'norm': [('id', '=', norms.id)]
        #     }
        # }
