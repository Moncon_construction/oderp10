# -*- coding: utf-8 -*-
{
    'name': "Repair - Work Order and Technic",
    'version': '1.0',
    'depends': ['base', 'l10n_mn_repair_work_order', 'l10n_mn_technic'],
    'author': "Asterisk Technologies LLC",
    'category': 'Repair, Technic, Mongolian Modules',
    'description': """
        Засварын ажилбарт техникийн мэдээллийг бүртгэх боломжтой болно.
    """,

    'data': [
        'views/technic_views.xml',
        'views/work_order_views.xml',
        'security/ir.model.access.csv',
        'views/project_views.xml',

        #report
        'report/repair_performance_wizard.xml',
        'report/planned_repair_wizard.xml',
        'report/technic_readiness_plan_wizard.xml',
        #'report/report_plans_and_performance_tbb_wizard.xml',
        'report/menu.xml',
    ],

    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
