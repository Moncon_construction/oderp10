# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import Warning

class Project(models.Model):
    _inherit = 'project.project'

    technic_day_work_hours = fields.Integer('Technic day work hours', help='This describe work hours of technic in a day for this project.', default=0)

    @api.multi
    def get_technic_day_work_hours(self):
        self.ensure_one()
        day_work_hours = self.technic_day_work_hours
        if not day_work_hours:
            raise Warning(_('Please configure work hours of technic in a day on this project.'))
        return day_work_hours
