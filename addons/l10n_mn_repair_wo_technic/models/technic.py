# -*- coding: utf-8 -*-
from odoo import models, fields, api

class Technic(models.Model):
    _inherit = 'technic'

    work_orders = fields.One2many('work.order', 'technic', "Work orders")
    wo_count = fields.Integer('Work orders count', compute='_compute_wo_count')

    @api.multi
    @api.depends('work_orders')
    def _compute_wo_count(self):
        for rec in self:
            rec.wo_count = len(rec.work_orders)
