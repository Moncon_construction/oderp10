# -*- encoding: utf-8 -*-
from odoo import fields, osv, models, api, _
from odoo.exceptions import Warning

class WorkOrder(models.Model):
    _inherit = 'work.order'

    technic = fields.Many2one('technic', string = 'Technic')
    usages = fields.One2many('work.order.usage', 'order_id', string = 'Technic usage of work order')
    component_registered = fields.Boolean(string = 'Components registered', default = False)

    @api.multi
    def write(self, vals):
        # if work order closing
        if len(vals) == 1 and vals.get('stage'):
            stage = self.env['work.order.stage'].browse(vals.get('stage'))
            for wo in self:
                if stage.state_type == 'done' and wo.stage.state_type != "review" and not wo.closed and not wo.is_parent:
                    raise Warning(_("Please use 'Close work order' button on work order."))
        return super(WorkOrder, self).write(vals)

    @api.multi
    def create_default_calendar(self):
        res = super(WorkOrder, self).create_default_calendar()
        if res:
            for wo in self:
                for calendar in wo.repair_calendars:
                    calendar.technic = wo.technic

    @api.multi
    def action_register_components(self):
        wo = self

        # open wizard
        mod_obj = self.env['ir.model.data']
        form_res = mod_obj.get_object_reference('l10n_mn_repair_wo_technic', 'work_order_view_form_register_components')
        form_id = form_res and form_res[1] or False
        return {
            'name': _('Register components'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'work.order',
            'res_id': wo.id,
            'views': [(form_id, 'form')],
            'context': None,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }

    @api.multi
    def action_register_components_save(self):
        # save component registered value
        self.write({'component_registered': True})

    @api.multi
    def action_close(self):
        wo = self

        # create usages
        usage = self.env['work.order.usage'].search([('order_id', '=', wo.id)])
        usage.unlink()
        norm_usage_measurements = self.env['usage.uom.line'].search([('technic_norm_id', '=', wo.technic.technic_norm_id.id)])
        if norm_usage_measurements:
            for measurement in norm_usage_measurements:
                usage_value = 0.0
                technic_usages = self.env['technic.usage'].search([('technic_id', '=', wo.technic.id), ('usage_uom_id', '=', measurement.usage_uom_id.id)])
                if technic_usages:
                    usage_value = technic_usages.usage_value
                data = {
                    'order_id': wo.id,
                    'usage_uom_id': measurement.usage_uom_id.id,
                    'product_uom_id': measurement.product_uom_id.id,
                    'usage_value': usage_value,
                }
                self.env['work.order.usage'].create(data)

        # open wizard
        mod_obj = self.env['ir.model.data']
        form_res = mod_obj.get_object_reference('l10n_mn_repair_wo_technic', 'work_order_view_form_close')
        form_id = form_res and form_res[1] or False
        return {
            'name': _('Close work order'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'work.order',
            'res_id': wo.id,
            'views': [(form_id, 'form')],
            'context': None,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }

    @api.multi
    def action_close_save(self):
        wo = self

        # save closed value
        self.write({'closed': True})

        # update stage
        close_stage_id = self.env['work.order.stage'].search([('state_type', '=', 'done')], limit=1)
        try:
            close_stage_id = close_stage_id[0]
        except Exception:
            raise Warning(_("There is no work order stage with 'done' main type."))
        res = super(WorkOrder, self).write({'stage': close_stage_id.id})

        # update technic usage information
        for usage in wo.usages:
            technic_usage = self.env['technic.usage'].search([('technic_id', '=', wo.technic.id), ('usage_uom_id', '=', usage.usage_uom_id.id)])

            data = {
                'technic_id': wo.technic.id,
                'usage_uom_id': usage.usage_uom_id.id,
                'product_uom_id': usage.usage_uom_id.usage_uom_id.id,
                'usage_value': usage.usage_value,
                'description': wo.name
            }

            # check usage is exist
            if technic_usage:
                technic_usage.write(data)
            else:
                technic_usage = self.env['technic.usage'].create(data)

            # update auto time usages of inspection
            if usage.auto_time:
                usage.usage_value = technic_usage.usage_value

        return res

class RepairCalendar(models.Model):
    _inherit = "repair.calendar"

    technic = fields.Many2one('technic', string = 'Work order')

class WorkOrderUsage(models.Model):
    _name = 'work.order.usage'
    _description = 'Technic usage of work order'

    order_id = fields.Many2one('work.order', string = 'Work Order', ondelete='cascade')
    usage_uom_id = fields.Many2one('usage.uom', string = 'Usage measurement', readonly=True)
    product_uom_id = fields.Many2one('product.uom', string ='Unit of measure', readonly=True)
    usage_value = fields.Float(string = 'Usage value')
    auto_time = fields.Boolean(related = 'usage_uom_id.auto_time', string='Auto update')
