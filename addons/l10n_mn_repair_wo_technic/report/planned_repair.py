# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles  # @UnresolvedImport

class PlannedReportRepairWizard(models.TransientModel):
    _inherit = 'planned.report.repair.wizard'

    @api.multi
    def _get_groups(self):
        groups = super(PlannedReportRepairWizard, self)._get_groups()
        groups.append(('technic', _('Technic')))
        groups.append(('technic_type', _('Technic type')))
        return groups

    technic_type = fields.Many2one('technic.type')
    technic = fields.Many2one('technic')

    @api.onchange('technic_type')
    def onchange_technic_type(self):
        types = self.get_type_descendants(self.technic_type)
        if self.technic_type:
            technics = self.env['technic'].search([('technic_norm_id.technic_type_id.id', '=', types.ids)])
        else:
            technics = self.env['technic'].search([])
        return {
            'domain': {
                'technic': [('id', '=', technics.ids)]
            }
        }

    @api.multi
    def get_type_descendants(self, technic_type):
        ret = technic_type
        for child_type in technic_type.type_ids:
            if child_type.type_ids:
                ret = ret | self.get_type_descendants(child_type)
            else:
                ret = ret | child_type
        return ret

    @api.multi
    def load_data(self):
        datas = super(PlannedReportRepairWizard, self).load_data()

        if self.technic:
            datas = datas.filtered(lambda r: r.technic == self.technic)
        
        if self.technic_type:
            types = self.get_type_descendants(self.technic_type)
            print types
            datas = datas.filtered(lambda r: r.technic.technic_norm_id.technic_type_id in types)
        return datas

    @api.multi
    def compute_column_number(self):
        colx_number = super(PlannedReportRepairWizard, self).compute_column_number()

        colx_number += 1
        if self.technic or self.group_by == 'technic':
            colx_number -= 1

        return colx_number

    @api.multi
    def create_filters(self, book, sheet, rowx, output_obj):
        sheet, rowx = super(PlannedReportRepairWizard, self).create_filters(book, sheet, rowx, output_obj)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)

        if self.technic:
            sheet.merge_range(rowx, 0, rowx, self.compute_column_number() / 2, '%s: %s' % (_('Technic'), self.technic.name), format_filter)
            rowx += 1

        if self.technic_type:
            sheet.merge_range(rowx, 0, rowx, self.compute_column_number() / 2, '%s: %s' % (_('Technic type'), self.technic_type.name), format_filter)
            rowx += 1

        return sheet, rowx

    @api.multi
    def create_addional_titles(self, book, sheet, rowx, colx):
        book, sheet, rowx, colx = super(PlannedReportRepairWizard, self).create_addional_titles(book, sheet, rowx, colx)
        format_title = book.add_format(ReportExcelCellStyles.format_title)

        if not self.technic and self.group_by != 'technic':
            sheet.merge_range(rowx, colx, rowx + 1, colx, _('Technic'), format_title)
            colx += 1

        return book, sheet, rowx, colx

    @api.multi
    def group_data(self):
        datas = super(PlannedReportRepairWizard, self).group_data()

        if self.group_by == 'technic':
            datas = datas.sorted(key=lambda r: r.technic.name)
        elif self.group_by == 'technic_type':
            datas = datas.sorted(key=lambda r: r.technic_norm_id.technic_type_id.name)

        return datas

    @api.multi
    def write_groups(self, book, sheet, rowx, colx, line, current_group, num):
        sheet, rowx, colx, current_group, num = super(PlannedReportRepairWizard, self).write_groups(book, sheet, rowx, colx, line, current_group, num)
        format_group = book.add_format(ReportExcelCellStyles.format_group)

        if self.group_by == 'technic':
            if current_group != line.technic:
                current_group = line.technic
                num = 1
                current_group_value = line.technic.name or _('Undefined')
                sheet.merge_range(rowx, colx, rowx, self.compute_column_number() - 1, current_group_value, format_group)
                rowx += 1

        if self.group_by == 'technic_type':
            if current_group != line.technic_norm_id.technic_type_id:
                current_group = line.technic_norm_id.technic_type_id
                num = 1
                current_group_value = line.technic_norm_id.technic_type_id.name or _('Undefined')
                sheet.merge_range(rowx, colx, rowx, self.compute_column_number() - 1, current_group_value, format_group)
                rowx += 1

        return sheet, rowx, colx, current_group, num

    @api.multi
    def create_addional_lines(self, book, sheet, rowx, colx, line):
        book, sheet, rowx, colx = super(PlannedReportRepairWizard, self).create_addional_lines(book, sheet, rowx, colx, line)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)

        if not self.technic and self.group_by != 'technic':
            sheet.write_string(rowx, colx, line.technic.name if line.technic else '', format_content_text)
            colx += 1

        return book, sheet, rowx, colx
