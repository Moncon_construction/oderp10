# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles  # @UnresolvedImport

class RepairReportPerformanceWizard(models.TransientModel):
    _inherit = 'repair.report.performance.wizard'

    def _get_groups(self):
        groups = super(RepairReportPerformanceWizard, self)._get_groups()
        groups.append(('technic', _('Technic')))
        groups.append(('technic_type', _('Technic type')))
        return groups

    technic = fields.Many2one('technic')
    technic_type = fields.Many2one('technic.type')

    def load_data(self):
        datas = super(RepairReportPerformanceWizard, self).load_data()

        if self.technic:
            datas = datas.filtered(lambda r: r.technic == self.technic)

        if self.technic_type:
            if not self.technic_type.type_ids:
                datas = datas.filtered(lambda r: r.technic.technic_type_id == self.technic_type)
            else:
                new_datas = datas.filtered(lambda r: r.technic.technic_type_id == self.technic_type)
                for child_type in self.technic_type.type_ids:
                    new_datas = new_datas | datas.filtered(lambda r: r.technic.technic_type_id == child_type)
                datas = new_datas

        return datas

    def compute_column_number(self):
        colx_number = super(RepairReportPerformanceWizard, self).compute_column_number()

        colx_number += 1
        if self.technic or self.group_by == 'technic':
            colx_number -= 1

        return colx_number

    def create_filters(self, book, sheet, rowx, output_obj):
        sheet, rowx = super(RepairReportPerformanceWizard, self).create_filters(book, sheet, rowx, output_obj)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)

        if self.technic:
            sheet.merge_range(rowx, 0, rowx, self.compute_column_number() / 2, '%s: %s' % (_('Technic'), self.technic.name), format_filter)
            rowx += 1

        if self.technic_type:
            sheet.merge_range(rowx, 0, rowx, self.compute_column_number() / 2, '%s: %s' % (_('Technic type'), self.technic_type.name), format_filter)
            rowx += 1

        return sheet, rowx

    def create_addional_titles(self, book, sheet, rowx, colx):
        book, sheet, rowx, colx = super(RepairReportPerformanceWizard, self).create_addional_titles(book, sheet, rowx, colx)
        format_title = book.add_format(ReportExcelCellStyles.format_title)

        if not self.technic and self.group_by != 'technic':
            sheet.merge_range(rowx, colx, rowx + 1, colx, _('Technic'), format_title)
            colx += 1

        return book, sheet, rowx, colx

    def group_data(self):
        datas = super(RepairReportPerformanceWizard, self).group_data()

        if self.group_by == 'technic':
            datas = datas.sorted(key=lambda r: r.technic.name)
        elif self.group_by == 'technic_type':
            datas = datas.sorted(key=lambda r: r.technic.technic_type_id.name)

        return datas

    def write_groups(self, book, sheet, rowx, colx, line, current_group, num):
        sheet, rowx, colx, current_group, num = super(RepairReportPerformanceWizard, self).write_groups(book, sheet, rowx, colx, line, current_group, num)
        format_group = book.add_format(ReportExcelCellStyles.format_group)

        if self.group_by == 'technic':
            if current_group != line.technic:
                current_group = line.technic
                num = 1
                current_group_value = line.technic.name or _('Undefined')
                sheet.merge_range(rowx, colx, rowx, self.compute_column_number() - 1, current_group_value, format_group)
                rowx += 1

        if self.group_by == 'technic_type':
            if current_group != line.technic.technic_type_id:
                current_group = line.technic.technic_type_id
                num = 1
                current_group_value = line.technic.technic_type_id.name or _('Undefined')
                sheet.merge_range(rowx, colx, rowx, self.compute_column_number() - 1, current_group_value, format_group)
                rowx += 1

        return sheet, rowx, colx, current_group, num

    def create_addional_lines(self, book, sheet, rowx, colx, line):
        book, sheet, rowx, colx = super(RepairReportPerformanceWizard, self).create_addional_lines(book, sheet, rowx, colx, line)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)

        if not self.technic and self.group_by != 'technic':
            sheet.write_string(rowx, colx, line.technic.name if line.technic else '', format_content_text)
            colx += 1

        return book, sheet, rowx, colx
