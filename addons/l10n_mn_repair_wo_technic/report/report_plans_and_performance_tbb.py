# -*- coding: utf-8 -*-
from io import BytesIO
import base64
from datetime import datetime
import time
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from odoo import fields
from odoo import models, api, _
from odoo.addons.l10n_mn_repair_work_order.models import work_order  # @UnresolvedImport
from operator import itemgetter

groups = [('technic_type_id', 'Technic Type')]

class ReportTbbWizard(models.TransientModel):
    _name = 'report.tbb.wizard'

    date_from = fields.Date(string = 'Date from', required=True)
    date_to = fields.Date(string = 'Date to', required=True)
    group_by = fields.Selection(groups, string = 'Group by')

    @api.multi
    def export_report(self):

        #create workbook
        output = BytesIO()
        workbook = xlsxwriter.Workbook(output)

        #create name
        report_name = _('Tbb')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        #Create a  worksheet
        worksheet = workbook.add_worksheet(report_name)

        name_xf = workbook.add_format({'font': 'Times New Roman', 'align': 'center', 'font_size': 30, 'bold': True, })
        number_xf = workbook.add_format({'font': 'Times New Roman', 'align': 'right', 'bold': True, })
        filter_xf = workbook.add_format({'font': 'Times New Roman', 'align': 'left', 'bold': True, })
        title_xf = workbook.add_format({'font': 'Times New Roman', 'align': 'center', 'bold': True, 'border': 1, 'bg_color': '#00bfff', 'text_wrap': 'on'})
        group_xf = workbook.add_format({'font': 'Times New Roman', 'align': 'center', 'bold': True, 'border': 1, 'bg_color': '#add8e6', 'text_wrap': 'on'})
        group_text_xf = workbook.add_format({'font': 'Times New Roman', 'align': 'right', 'bold': True, 'border': 1, 'bg_color': '#add8e6', 'num_format': '0.00%'})
        content_number_xf = workbook.add_format({'font': 'Times New Roman', 'align': 'center', 'bold': False, 'border': 1, })
        content_text_xf = workbook.add_format({'font': 'Times New Roman', 'align': 'right', 'bold': False, 'border': 1, 'num_format': '0.00%'})
        all_xf = workbook.add_format({'font': 'Times New Roman', 'align': 'right', 'bold': True, 'border': 1, 'bg_color': '#00bfff', 'text_wrap': 'on', 'num_format': '0.00%'})

        # compute column
        colx_number = 4
        worksheet.set_column('A:A', 2)
        worksheet.set_column('B:B', 35)
        worksheet.set_column('C:C', 12)
        worksheet.set_column('D:D', 9)
        worksheet.set_column('E:E', 9)
        worksheet.hide_gridlines('True')

        # create report number
        rowx = 1
        report_number = _('Report №%s') % 1
        worksheet.merge_range(rowx, 1, rowx, colx_number, report_number, number_xf)
        rowx += 1

        # create name
        worksheet.set_row(2, 70)
        worksheet.merge_range(rowx, 0, rowx, colx_number, report_name.upper(), name_xf)
        rowx += 1

        # create filter xls
        technic_filters = []
        worksheet.merge_range(rowx, 0, rowx, colx_number, '%s: %s' % (_('Date from'), self.date_from), filter_xf)
        rowx += 1
        worksheet.merge_range(rowx, 0, rowx, colx_number, '%s: %s' % (_('Date to'), self.date_to), filter_xf)
        rowx += 1

        if self.group_by:
            technic_filters.append(('technic_type_id', '=', self.group_by))
            worksheet.merge_range(rowx, 0, rowx, colx_number, '%s: %s' % (_('Group By'), _(dict(groups)[self.group_by])), filter_xf)
            rowx += 1

        # create date time
        report_datetime = '%s: %s' % (_('Created On'), time.strftime('%Y.%m.%d %H:%M:%S'))
        worksheet.merge_range(rowx, 1, rowx, colx_number, report_datetime, number_xf)
        rowx += 1

        #calculate date
        datetimeFormat = '%Y-%m-%d'
        date_start = datetime.strptime(str(self.date_from), datetimeFormat)
        date_end = datetime.strptime(str(self.date_to), datetimeFormat)
        timedelta = date_end - date_start
        total_work_time = ((timedelta.days + 1) * work_order.DAY_WORK_TIME)

        # create header
        colx = 0
        worksheet.merge_range(rowx, colx, rowx + 1, colx, _('№'), title_xf)
        colx += 1
        worksheet.merge_range(rowx, colx, rowx + 1, colx, 'Technic name', title_xf)
        colx += 1
        worksheet.merge_range(rowx, colx, rowx + 1, colx, 'TBB percent', title_xf)
        colx += 1
        worksheet.merge_range(rowx, colx, rowx, colx + 1, 'Cause', title_xf)
        worksheet.write(rowx + 1, colx, 'Graphical repair', title_xf)
        colx += 1
        worksheet.write(rowx + 1, colx, 'Surprise repair ', title_xf)
        colx += 1
        rowx += 2

        #create line
        self.env.cr.execute("""
        select wo.name as work_name, tt.name as type_name, tech.name as tech_name, rc.planned_hours as planned_hour, rc.date as repair_date, wot.wo_main_type as work_main_type
        from work_order wo
        left join technic tech on tech.id = wo.technic
        left join technic_type tt on tt.id = tech.technic_type_id
        left join repair_calendar rc on rc.wo_id = wo.id
        left join work_order_type wot on wot.id = wo.work_order_type
        """)

        dictfetch = self.env.cr.dictfetchall()
        group_tech_dict = {}
        for line in dictfetch:
                group = line['type_name']
                if group not in group_tech_dict:
                    group_tech_dict[group] = {
                        'technic_type': '',
                        'technic_names': {},

                        }
                group_tech_dict[group]['technic_type'] = group

                group1 = line['tech_name']
                if group1 not in group_tech_dict[group]['technic_names']:
                    group_tech_dict[group]['technic_names'][group1] = {
                        'technic_name': '',
                        'wo_type': {}
                    }
                group_tech_dict[group]['technic_names'][group1]['technic_name'] = line['tech_name']

                group2 = line['work_main_type']
                if group2 not in group_tech_dict[group]['technic_names'][group1]['wo_type']:
                    group_tech_dict[group]['technic_names'][group1]['wo_type'][group2] = {
                        'work_main_type': '',
                        'planned_hour': 0.00,
                        'repair_date': '',
                        'planned_rapair': 0.00,
                        'not_planned_repair': 0.00
                        }
                group_tech_dict[group]['technic_names'][group1]['wo_type'][group2]['work_main_type'] = line['work_main_type']
                group_tech_dict[group]['technic_names'][group1]['wo_type'][group2]['planned_hour'] += line['planned_hour']
                group_tech_dict[group]['technic_names'][group1]['wo_type'][group2]['repair_date'] = line['repair_date']

        count = 1
        start_row = rowx
        for line in sorted(group_tech_dict.values(), key=itemgetter('technic_type')):
            rowl = rowx
            if self.group_by:
                worksheet.merge_range(rowx, 0, rowx, 1, line['technic_type'], group_xf)
                rowx += 1
                count = 1
            for tech in sorted(line['technic_names'].values(), key=itemgetter('technic_name')):
                worksheet.write(rowx, 0, count, content_number_xf)
                worksheet.write(rowx, 1, tech['technic_name'], content_text_xf)
                total_hour = 0
                is_planned = True
                is_not_planned = True
                for hour in sorted(tech['wo_type'].values(), key=itemgetter('work_main_type')):
                    current_date = datetime.strptime(str(hour['repair_date']), datetimeFormat)

                    if date_start <= current_date <= date_end:
                        if hour['work_main_type'] == 'planned':
                            worksheet.write(rowx, 3, (hour['planned_hour']) / total_work_time, content_text_xf)
                            is_planned = False
                        else:
                            worksheet.write(rowx, 4, (hour['planned_hour']) / total_work_time, content_text_xf)
                            is_not_planned = False
                        total_hour += hour['planned_hour']
                    else:
                        worksheet.write(rowx, 3, 0, content_text_xf)
                        worksheet.write(rowx, 4, 0, content_text_xf)

                worksheet.write(rowx, 2, (total_work_time - total_hour) / total_work_time, content_text_xf)
                if is_planned:
                            worksheet.write(rowx, 3, 0, content_text_xf)
                if is_not_planned:
                            worksheet.write(rowx, 4, 0, content_text_xf)
                count += 1
                rowx += 1

            if self.group_by:
                worksheet.write_formula(rowl, 2, '{=AVERAGE(' + xl_rowcol_to_cell(rowx - count + 1, 2) + ':' + xl_rowcol_to_cell(rowx - 1, 2) + ')}', group_text_xf)
                worksheet.write_formula(rowl, 3, '{=AVERAGE(' + xl_rowcol_to_cell(rowx - count + 1, 3) + ':' + xl_rowcol_to_cell(rowx - 1, 3) + ')}', group_text_xf)
                worksheet.write_formula(rowl, 4, '{=AVERAGE(' + xl_rowcol_to_cell(rowx - count + 1, 4) + ':' + xl_rowcol_to_cell(rowx - 1, 4) + ')}', group_text_xf)
            else:
                worksheet.write(rowx, 1, 'All', title_xf)
                worksheet.write_formula(rowx, 2, '{=AVERAGE(' + xl_rowcol_to_cell(rowx - count + 1, 2) + ':' + xl_rowcol_to_cell(rowx - 1, 2) + ')}', all_xf)
                worksheet.write_formula(rowx, 3, '{=AVERAGE(' + xl_rowcol_to_cell(rowx - count + 1, 3) + ':' + xl_rowcol_to_cell(rowx - 1, 3) + ')}', all_xf)
                worksheet.write_formula(rowx, 4, '{=AVERAGE(' + xl_rowcol_to_cell(rowx - count + 1, 4) + ':' + xl_rowcol_to_cell(rowx - 1, 4) + ')}', all_xf)

        end_row = rowx

        chart = workbook.add_chart({'type': 'column'})
        chart.set_title({'name': 'TBB plan'})

        chart.add_series({
                        'values': ['Tbb', start_row, 2, end_row - 1, 2],
                        'categories': ['Tbb', start_row, 1, end_row, 1],
                        'color': '#00bfff',
                        'name': 'Planned',
        })
        chart.set_size({'width': 1000, 'height': 400})
        chart.set_legend({'none': True})
#         chart.set_style(11)
#         chart.set_size({'x_scale': 0.5, 'y_scale': 1})
        worksheet.insert_chart(xl_rowcol_to_cell(rowx + 2, 0), chart)
        workbook.close()

        out = base64.encodestring(output.getvalue())
        excel_id_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix='technic_passport', form_title=report_name).create({'filedata': out, 'filename': file_name},)

        return {
            'name': 'Export Result',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'oderp.report.excel.output',
            'res_id': excel_id_obj.id,
            'view_id': False,
            'context': '',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'nodestroy': True,
        }
