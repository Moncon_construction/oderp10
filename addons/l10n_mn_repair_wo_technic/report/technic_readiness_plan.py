# -*- coding: utf-8 -*-
from io import BytesIO
import base64
from datetime import datetime
from datetime import timedelta
import pytz
import time

import xlsxwriter

from odoo import models, fields, api, _
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.exceptions import Warning
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles  # @UnresolvedImport

class ReadinessPlanWizard(models.TransientModel):
    _name = 'repair.report.technic.readiness.plan.wizard'

    def _default_end_date(self):
        now = datetime.now()
        month = timedelta(days=30)
        return now + month

    def _get_groups(self):
        return [('repair_type', _('Repair type'))]

    project = fields.Many2one('project.project', 'Project', required=True)
    group_by = fields.Selection(lambda self: self._get_groups(), 'Group By')
    start_date = fields.Date('Start date', default=lambda *a: datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT), required=True)
    end_date = fields.Date('End date', default=_default_end_date, required=True)

    @api.onchange('start_date')
    def onchange_start_date(self):
        projects = self.env['project.project'].search([('use_repair', '=', True), ('members', 'in', self.env.user.id)])
        if len(projects) == 1:
            self.project = projects
        return {
            'domain': {
                'project': [('id', '=', projects.ids)]
            }
        }

    def validate_fields(self):
        end_date = datetime.strptime("{0}".format(self.end_date), DEFAULT_SERVER_DATE_FORMAT)
        start_date = datetime.strptime("{0}".format(self.start_date), DEFAULT_SERVER_DATE_FORMAT)
        if start_date > end_date:
            raise Warning(_('Start date must be less than end date.'))

    def load_data(self):
        datas = self.env['work.order'].search([('planned_date', '>=', self.start_date), ('planned_date', '<=', self.end_date), ('technic', '!=', False)])
        return datas

    def get_repair_types(self):
        datas = self.load_data()
        datas = datas.sorted(key=lambda r: r.work_order_type.name)
        types = []
        current_type = ''
        for wo in datas:
            if wo.work_order_type and wo.work_order_type != current_type:
                types.append(wo.work_order_type)
                current_type = wo.work_order_type
        return types

    def compute_column_number(self):
        colx_number = 5

        if self.group_by == 'repair_type':
            colx_number = colx_number + len(self.get_repair_types())

        start_date = datetime.strptime("{0}".format(self.start_date), DEFAULT_SERVER_DATE_FORMAT)
        end_date = datetime.strptime("{0}".format(self.end_date), DEFAULT_SERVER_DATE_FORMAT)
        days = (end_date - start_date).days
        colx_number = colx_number + days

        return colx_number

    def create_filters(self, book, sheet, rowx, output_obj):
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        if self.group_by:
            sheet.merge_range(rowx, 0, rowx, self.compute_column_number() / 2, '%s: %s' % (_('Group By'), output_obj._(dict(self._get_groups())[self.group_by])), format_filter)
            rowx += 1
        return sheet, rowx

    def group_data(self):
        datas = self.load_data()
        datas = datas.sorted(key=lambda r: r.technic.technic_norm_id.technic_type_id.name)
        new_datas = []
        current_technic_type = None
        for wo in datas:
            this_technic_type = wo.technic.technic_norm_id.technic_type_id
            if current_technic_type != this_technic_type:
                current_data_set = datas.filtered(lambda r: r.technic.technic_norm_id.technic_type_id == this_technic_type)
                current_data_set = current_data_set.sorted(key=lambda r: r.technic.name)
                for current_wo in current_data_set:
                    new_datas.append(current_wo)
                current_technic_type = this_technic_type
        return new_datas

    def write_groups(self, book, sheet, rowx, colx, line, current_group, num):
        format_group = book.add_format(ReportExcelCellStyles.format_group)

        if current_group != line.technic.technic_norm_id.technic_type_id:
            current_group = line.technic.technic_norm_id.technic_type_id
            num = 1
            current_group_value = line.technic.technic_norm_id.technic_type_id.name or _('Undefined')
            sheet.merge_range(rowx, colx, rowx, colx + 1, current_group_value, format_group)
            rowx += 1

        return sheet, rowx, colx, current_group, num

    def get_day_repair_hours(self, technic, date, by_type=False, total=False):
        hours = 0
        datas = self.load_data()
        if total:
            datas = datas.filtered(lambda r: r.planned_date == date.date().strftime('%Y-%m-%d'))
        else:
            if not by_type:
                datas = datas.filtered(lambda r: r.technic == technic and r.planned_date == date.date().strftime('%Y-%m-%d'))
            else:
                datas = datas.filtered(lambda r: r.technic.technic_norm_id.technic_type_id == technic and r.planned_date == date.date().strftime('%Y-%m-%d'))
        for wo in datas:
            hours += wo.planned_hours
        return hours

    def get_repair_type_hours(self, technic, repair_type, by_type=False, total=False):
        hours = 0
        datas = self.load_data()
        if total:
            datas = datas.filtered(lambda r: r.work_order_type == repair_type)
        else:
            if not by_type:
                datas = datas.filtered(lambda r: r.technic == technic and r.work_order_type == repair_type)
            else:
                datas = datas.filtered(lambda r: r.technic.technic_norm_id.technic_type_id == technic and r.work_order_type == repair_type)
        for wo in datas:
            hours += wo.planned_hours
        return hours

    def get_repair_total_hours(self, technic, by_type=False, total=False):
        hours = 0
        datas = self.load_data()
        if not total:
            if not by_type:
                datas = datas.filtered(lambda r: r.technic == technic)
            else:
                datas = datas.filtered(lambda r: r.technic.technic_norm_id.technic_type_id == technic)
        for wo in datas:
            hours += wo.planned_hours
        return hours

    def get_total_hours(self, total=False):
        technic_day_work_hours = self.project.get_technic_day_work_hours()
        start_date = datetime.strptime("{0}".format(self.start_date), DEFAULT_SERVER_DATE_FORMAT)
        end_date = datetime.strptime("{0}".format(self.end_date), DEFAULT_SERVER_DATE_FORMAT)
        days = (end_date - start_date).days
        total_hours = technic_day_work_hours * days

        if total:
            datas = self.load_data()
            total_hours = total_hours * len(datas)
        return total_hours

    def get_readiness_percent(self, technic, by_type=False, total=False):
        if total:
            return "{0:.2f}".format((self.get_total_hours(total=True) - self.get_repair_total_hours('', total=True)) * 100 / self.get_total_hours(total=True))
        else:
            if not by_type:
                return "{0:.2f}".format((self.get_total_hours() - self.get_repair_total_hours(technic)) * 100 / self.get_total_hours())
            else:
                return "{0:.2f}".format((self.get_total_hours() - self.get_repair_total_hours(technic, by_type=True)) * 100 / self.get_total_hours())

    @api.multi
    def export_report(self):
        # validate fields
        self.validate_fields()

        # load data
        datas = self.load_data()

        if len(datas) == 0:
            raise Warning(_('No result is found.'))

        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_date = book.add_format(ReportExcelCellStyles.format_date)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_group = book.add_format(ReportExcelCellStyles.format_group)
        format_group_number = book.add_format(ReportExcelCellStyles.format_group_number)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)

        # create name
        report_name = _('Technic readiness plan')
        file_name = "%s_%s.xlsx" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix='technic_readiness_plan', form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name if len(report_name) <= 31 else report_name[:31])
        sheet.set_landscape()
        sheet.set_paper(8)  # A3
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.hide_gridlines(2)
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})

        rowx = 0

        # compute column number
        colx_number = self.compute_column_number()

        # create name
        sheet.merge_range(rowx, 0, rowx, colx_number, report_name.upper(), format_name)
        sheet.set_row(rowx, 30, format_name)
        rowx += 1

        previous_rowx = rowx

        # create filters
        sheet, rowx = self.create_filters(book, sheet, rowx, report_excel_output_obj)

        # create date time
        if self.env.user.partner_id.tz:
            tz = pytz.timezone(self.env.user.partner_id.tz)
        else:
            tz = pytz.utc
        now_utc = datetime.now(pytz.timezone('UTC'))
        now_user_zone = now_utc.astimezone(tz)
        report_datetime = '%s: %s' % (_('Created On'), now_user_zone.strftime('%Y-%m-%d %H:%M:%S'))
        if previous_rowx != rowx:
            sheet.merge_range(rowx - 1, colx_number / 2 + 2, rowx - 1, colx_number, report_datetime, format_date)
        else:
            sheet.merge_range(rowx, colx_number / 2 + 2, rowx, colx_number, report_datetime, format_date)
            rowx += 1

        # create titles
        colx = 0

        sheet.merge_range(rowx, colx, rowx + 1, colx, u'№', format_title)
        colx += 1

        sheet.merge_range(rowx, colx, rowx + 1, colx, _('Technic'), format_title)
        colx += 1

        start_date = datetime.strptime("{0}".format(self.start_date), DEFAULT_SERVER_DATE_FORMAT)
        end_date = datetime.strptime("{0}".format(self.end_date), DEFAULT_SERVER_DATE_FORMAT)
        days = (end_date - start_date).days
        sheet.merge_range(rowx, colx, rowx, colx + days, _('Repair hours'), format_title)

        for i in range(days + 1):
            current_date = start_date + timedelta(days=i)
            sheet.write(rowx + 1, colx, '%s/%s' % (current_date.month, current_date.day), format_title)
            colx += 1

        repair_type_count = len(self.get_repair_types())
        if repair_type_count > 0 and self.group_by:
            sheet.merge_range(rowx, colx, rowx, colx + repair_type_count - 1, _('Repair type'), format_title)

            for repair_type in self.get_repair_types():
                sheet.write(rowx + 1, colx, repair_type.name, format_title)
                colx += 1

        sheet.merge_range(rowx, colx, rowx + 1, colx, _('Total repair hours'), format_title)
        sheet.set_column(colx,colx, 20)
        colx += 1

        sheet.merge_range(rowx, colx, rowx + 1, colx, _('Total hours'), format_title)
        sheet.set_column(colx,colx, 20)
        colx += 1

        sheet.merge_range(rowx, colx, rowx + 1, colx, _('Technic readiness plan'), format_title)
        sheet.set_column(colx,colx, 20)
        colx += 1

        rowx += 2

        sheet.repeat_rows(0, rowx - 1)
        sheet.freeze_panes(rowx, 0)

        # group data
        datas = self.group_data()

        # CREATE LINES
        num = 1
        current_group = ''
        current_technic = ''

        # write lines
        for line in datas:
            colx = 0

            rowx_previous = rowx

            # write group
            sheet, rowx, colx, current_group, num = self.write_groups(book, sheet, rowx, colx, line, current_group, num)

            if rowx != rowx_previous:
                colx += 2
                rowx -= 1

                for i in range(days + 1):
                    current_date = start_date + timedelta(days=i)
                    sheet.write(rowx, colx, self.get_day_repair_hours(line.technic.technic_norm_id.technic_type_id, current_date, by_type=True), format_group_number)
                    colx += 1

                repair_type_count = len(self.get_repair_types())
                if repair_type_count > 0 and self.group_by:
                    for repair_type in self.get_repair_types():
                        sheet.write(rowx, colx, self.get_repair_type_hours(line.technic.technic_norm_id.technic_type_id, repair_type, by_type=True), format_group_number)
                        colx += 1

                sheet.write(rowx, colx, self.get_repair_total_hours(line.technic.technic_norm_id.technic_type_id, by_type=True), format_group_number)
                colx += 1

                sheet.write(rowx, colx, self.get_total_hours(), format_group_number)
                colx += 1

                sheet.write(rowx, colx, self.get_readiness_percent(line.technic.technic_norm_id.technic_type_id, by_type=True), format_group_number)
                colx += 1

                rowx += 1

            colx = 0

            # write line
            if line.technic != current_technic:
                current_technic = line.technic

                sheet.set_row(rowx, None, None, {'level': 1})

                sheet.write_number(rowx, colx, num, format_content_number)
                colx += 1

                sheet.write(rowx, colx, line.technic.name, format_content_text)
                colx += 1

                for i in range(days + 1):
                    current_date = start_date + timedelta(days=i)
                    sheet.write(rowx, colx, self.get_day_repair_hours(line.technic, current_date), format_content_number)
                    colx += 1

                repair_type_count = len(self.get_repair_types())
                if repair_type_count > 0 and self.group_by:
                    for repair_type in self.get_repair_types():
                        sheet.write(rowx, colx, self.get_repair_type_hours(line.technic, repair_type), format_content_number)
                        colx += 1

                sheet.write(rowx, colx, self.get_repair_total_hours(line.technic), format_content_number)
                colx += 1

                sheet.write(rowx, colx, self.get_total_hours(), format_content_number)
                colx += 1

                sheet.write(rowx, colx, self.get_readiness_percent(line.technic), format_content_number)
                colx += 1

                num += 1
                sheet.set_row(rowx, 25)
                rowx += 1

        # write footer
        colx = 0

        sheet.merge_range(rowx, colx, rowx, colx + 1, _('Total'), format_group)
        colx += 2

        for i in range(days + 1):
            current_date = start_date + timedelta(days=i)
            sheet.write(rowx, colx, self.get_day_repair_hours('', current_date, total=True), format_group_number)
            colx += 1

        repair_type_count = len(self.get_repair_types())
        if repair_type_count > 0 and self.group_by:
            for repair_type in self.get_repair_types():
                sheet.write(rowx, colx, self.get_repair_type_hours('', repair_type, total=True), format_group_number)
                colx += 1

        sheet.write(rowx, colx, self.get_repair_total_hours('', total=True), format_group_number)
        colx += 1

        sheet.write(rowx, colx, self.get_total_hours(total=True), format_group_number)
        colx += 1

        sheet.write(rowx, colx, self.get_readiness_percent('', total=True), format_group_number)
        colx += 1

        book.close()

        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()
