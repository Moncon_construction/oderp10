# -*- coding: utf-8 -*-
{
    'name': "Repair - Work Order, Technic and Account",
    'version': '1.0',
    'depends': ['l10n_mn_repair_work_order'],
    'author': "Asterisk Technologies LLC",
    'website': "http://www.asterisk-tech.mn",
    'category': 'Repair, Mongolian Modules',
    'description': """
        Засвар, Техник, Санхүү.
    """,
        'data': [
    ],
    'summary': """
        Work Order, Technic and Account""",
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
