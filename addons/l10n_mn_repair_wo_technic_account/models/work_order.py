# -*- coding: utf-8 -*-
from odoo import models, fields, api 

class WorkOrder(models.Model):
    _inherit = 'work.order'

    def create_expense_line(self, line_value_dict):
        for wo in self:
            if wo.technic.account_analytic_id:
                line_value_dict.update({'analytic_account_id': wo.technic.account_analytic_id.id})
        return super(WorkOrder, self).create_expense_line(line_value_dict)
