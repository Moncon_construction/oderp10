# -*- coding: utf-8 -*-
{
    'name': "Repair - Work Order Expense",
    'version': '1.0',
    'depends': ['l10n_mn_repair_wo_technic', 'l10n_mn_technic_expense'],
    'author': "Asterisk Technologies LLC",
    'category': 'Repair, Mongolian Modules',
    'description': """
        Засвараас шаардах хуудас үүсгэнэ.
    """,
    'data': [
        'views/product_expense_view.xml'
    ],

    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
