# -*- coding: utf-8 -*-
import math

from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.exceptions import Warning, UserError


class WorkOrderExpense(models.Model):
    _inherit = 'work.order'
    _description = 'Work Order Expense'

    @api.multi
    def _default_warehouse_id(self):
        default_warehouse = self.env.user.allowed_warehouses.ids
        warehouse_ids = self.env['stock.warehouse'].search(
            [('id', 'in', default_warehouse), ('company_id', 'child_of', [self.env.user.company_id.id])], limit=1)
        return warehouse_ids

    def _domain_warehouses(self):
        return [
            ('id', 'in', self.env.user.allowed_warehouses.ids),
            '|', ('company_id', '=', False), ('company_id', 'child_of', [self.env.user.company_id.id])
        ]

    warehouse_id = fields.Many2one('stock.warehouse', 'Warehouse', default=_default_warehouse_id,
                                   domain=_domain_warehouses)

    @api.multi
    def create_work_expense(self):
        employee = self.env['hr.employee'].search([('user_id', '=', self._uid)])
        if not employee:
            raise UserError(_('You have no employee'))
        else:
            if not employee.department_id:
                raise UserError(_('Your employee have no department'))
        if not self.technic:
            raise UserError(_("You must select technic"))
        if not self.warehouse_id:
            raise UserError(_("Select warehouse"))
        if not self.warehouse_id.out_type_id.default_location_src_id:
            raise UserError(_("You must select default source location"))
        work_order_expense = self.env['product.expense'].search([('work_order_id', '=', self.id)])
        line_vals = []
        if work_order_expense:
            for line in self.products:
                equal_qty = 0
                expense_qty = 0
                expense_lines = self.env['product.expense.line'].search(
                    [('expense', 'in', work_order_expense.ids), ('product', '=', line.product_id.id)])
                for expense_line in expense_lines:
                    expense_qty += expense_line.quantity
                if expense_qty < line.product_qty:
                    equal_qty = line.product_qty - expense_qty
                    line_vals.append(line._prepare_expense_line(equal_qty))
            if line_vals:
                self._create_work_expense(employee, line_vals)
        else:
            for line in self.products:
                line_vals.append(line._prepare_expense_line(line.product_qty))
            self._create_work_expense(employee, line_vals)

    def _create_work_expense(self, employee, line_vals):
        vals = {
            'date': datetime.now(),
            'expense_line': line_vals if line_vals else [],
            'is_technic_expense': True,
            'technic_id': self.technic.id,
            'warehouse': self.warehouse_id.id,
            'stock_picking_type': self.warehouse_id.out_type_id.id,
            'company': self.env.user.company_id.id,
            'department': employee.department_id.id,
            'employee': employee.id,
            'name': self.name,
            'account': self.warehouse_id.expense_account_id.id,
            'work_order_id': self.id
        }
        expense = self.env['product.expense'].create(vals)
        return expense
