# -*- coding: utf-8 -*-
import math

from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.exceptions import Warning, UserError


class WorkOrderProductsExpense(models.Model):
    _inherit = 'work.order.products'

    def _prepare_expense_line(self, qty):
        self.ensure_one()
        return (0, 0, {
            'product': self.product_id.id,
            'name': self.product_id.name,
            'quantity': qty,
            'account': self.order_id.warehouse_id.expense_account_id.id})