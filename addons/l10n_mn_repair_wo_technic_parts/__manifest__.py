    # -*- coding: utf-8 -*-
{

    'name': "Repair - Work order and Technic parts",
    'version': '1.0',
    'depends': ['base', 'l10n_mn_repair_wo_technic', 'l10n_mn_technic_parts'],
    'author': "Asterisk Technologies LLC",
    'website': "http://www.asterisk-tech.mn",
    'category': 'Repair, Technic, Mongolian Modules',
    'description': """
        Засвар ын ажилбарт сэлбэгийн мэдээллийг бүртгэх боломжтой болно.
    """,
    'summary': """    
        Work Order and Technic parts""",

    'data': [
        'views/work_order_views.xml',
    ],

    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
