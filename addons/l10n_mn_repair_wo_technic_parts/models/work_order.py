# -*- encoding: utf-8 -*-
from odoo import models, fields, api

class WorkOrder(models.Model):
    _inherit = 'work.order'

    @api.multi
    def _count_register_parts(self):
        res = {}
        for order in self:
            res[order.id] = len(order.register_parts)
        return res

    @api.multi
    def _count_remove_parts(self):
        res = {}
        for order in self:
            res[order.id] = len(order.remove_parts)
        return res

    @api.multi
    def _count_install_parts(self):
        res = {}
        for order in self:
            res[order.id] = len(order.install_parts)
        return res

    register_parts = fields.One2many('technic.parts', 'register_work_order', string='Register work order')
    register_parts_count = fields.Integer(compute = _count_register_parts, string='Register spare parts count')
    remove_parts = fields.Many2many('technic.parts', 'work_order_technic_parts_remove_rel', 'work_order', 'part', string='Remove spare parts')
    remove_parts_count = fields.Integer(compute = _count_remove_parts, string='Remove spare parts count')
    install_parts =  fields.Many2many('technic.parts', 'work_order_technic_parts_install_rel', 'work_order', 'part', string='Install spare parts')
    install_parts_count = fields.Integer(compute = _count_install_parts, string='Install spare parts count')

    @api.multi
    def action_close(self):

        # create install spare parts
        register_parts_ids = []
        for part in self:
            register_parts_ids.append(part.id)
        self.write({'install_parts': register_parts_ids})
        return super(WorkOrder, self).action_close()

    @api.multi
    def action_close_save(self):
        wo = self

        res = super(WorkOrder, self).action_close_save()
        # update spare parts information
        self.env['technic.parts'].write({'technic': wo.technic.id})
        self.env['technic.parts'].write({'technic': False})
        return res

class TechnicParts(models.Model):
    _inherit = 'technic.parts'

    register_work_order = fields.Many2one('work.order', string='Register work order', ondelete='set null')
    remove_work_orders = fields.Many2many('work.order', 'work_order_technic_parts_remove_rel', 'part', 'work_order', string='Remove work orders')
    install_s = fields.Many2many('work.order', 'work_order_technic_parts_install_rel', 'part', 'work_order', string='Install work orders')
