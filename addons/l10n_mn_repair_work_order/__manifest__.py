# -*- coding: utf-8 -*-
{
    'name': "Repair - Work Order",
    'version': '1.0',
    'depends': ['base', 'l10n_mn_hr', 'l10n_mn_report', 'project', 'l10n_mn_product_expense'],
    'author': "Asterisk Technologies LLC",
    'category': 'Repair, Mongolian Modules',
    'description': """
        Засварын үндсэн модуль бөгөөд засварын ажилбар бүртгэх боломжийг олгоно.
    """,

    'data': [
        'security/repair_security.xml',
        'security/ir.model.access.csv',
        'views/work_order_views.xml',
        'views/breakdown_views.xml',
        'views/product_expense_view.xml',
        'views/sequence.xml',
        'views/menu.xml',
        'views/project_views.xml',
        'report/report.xml',
        'report/print_work_order.xml',

        # report
        'report/repair_performance_wizard.xml',
        'report/planned_repair_wizard.xml',
        'report/menu.xml',
    ],

    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
