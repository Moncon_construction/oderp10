# -*- coding: utf-8 -*-
from datetime import datetime
from odoo import api, fields, models, _

class RepairBreakdownCause(models.Model):
    _name = 'repair.breakdown.cause'
    _description = 'Breakdown Cause'

    name = fields.Char(String = 'Name', required=True, translate=True)
    description = fields.Text(String = 'Description')

class RepairBreakdownType(models.Model):
    _name = 'repair.breakdown.type'
    _description = 'Breakdown Type'

    def _get_child_ids(self):
        result = {}
        for record in self:
            if record.type_ids:
                result[record.id] = [x.id for x in record.type_ids]
            else:
                result[record.id] = []

        return result

    name = fields.Char(String = 'Name', required=True, translate=True)
    code = fields.Char(String = 'Code')
    description = fields.Text(String = 'Description')
    parent_id = fields.Many2one('repair.breakdown.type', String = 'Parent')
    type_ids = fields.One2many('repair.breakdown.type', 'parent_id', String = 'Types')
    child_id = fields.Many2many(compute=_get_child_ids, relation = "repair.breakdown.type", string="Child breakdown types")

class RepairBreakdown(models.Model):
    _name = 'repair.breakdown'
    _description = 'Breakdown'

    name = fields.Char('Name')
    work_order_id = fields.Many2one('work.order', 'Work Order')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id, required=True, track_visibility='onchange')
    product_id = fields.Many2one('product.product', 'Product')
    user_id = fields.Many2one('res.users', 'User', required=True)
    cause_id = fields.Many2one('repair.breakdown.cause', 'Breakdown Cause', required=True)
    type_id = fields.Many2one('repair.breakdown.type', 'Breakdown Type', required=True)
    date = fields.Date(string='Date', default=datetime.today(), track_visibility='onchange', required=True)
    breakdown_date = fields.Date(string='Breakdown Date', default=datetime.today(), track_visibility='onchange')
    description = fields.Text('Description')
    repair = fields.Text('Repair')
    
    @api.onchange('product_id')
    def onchange_product_id(self):
        for obj in self:
            obj.name = obj.product_id.name if obj.product_id.name else '/'