# -*- coding: utf-8 -*-
from odoo import fields, models


class ProductExpense(models.Model):
    _inherit = 'product.expense'

    work_order_id = fields.Many2one('work.order', string='Related work order')
