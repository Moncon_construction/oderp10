# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.exceptions import Warning

class Project(models.Model):
    _inherit = 'project.project'

    use_repair = fields.Boolean(string = 'Use Repair Management', help='Specify if the project use repair management.', default=False)
    repair_day_work_hours = fields.Integer(string = 'Repair day work hours', help='This describe work hours of repair in a day for this project.', default=0)

    @api.multi
    def get_repair_day_work_hours(self):
        self.ensure_one()
        day_work_hours = self.repair_day_work_hours
        if not day_work_hours:
            raise Warning(_('Please configure work hours of repair in a day on this project.'))
        return day_work_hours
