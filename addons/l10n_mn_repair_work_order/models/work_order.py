# -*- coding: utf-8 -*-
import math

from odoo import api, fields, models, _  # @UnresolvedImport
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT  # @UnresolvedImport
from odoo.exceptions import Warning  # @UnresolvedImport


class WorkOrderType(models.Model):
    _name = 'work.order.type'
    _description = 'Work Order Type'

    name = fields.Char(String='Type Name', required=True)
    wo_main_type = fields.Selection([('planned', 'Planned'),
                                     ('not_planned', 'Not Planned')], String='Work Order Main Type', required=True)
    description = fields.Text(String='Description')


class WorkOrderStage(models.Model):
    _name = 'work.order.stage'
    _description = 'Work Order Stage'
    _order = 'sequence'

    name = fields.Char(String='Stage Name', required=True, translate=True)
    state_type = fields.Selection(
        [('draft', 'New'),
         ('open', 'In Progress'),
         ('pending', 'Pending'),
         ('done', 'Closed'),
         ('review', 'Reviewed'),
         ('cancel', 'Cancelled')], String='State')
    description = fields.Text(String='Description')
    sequence = fields.Integer(String='Sequence')
    fold = fields.Boolean(String='Folded in Kanban View', help='This stage is folded in the kanban view when there are no records in that stage to display.')
    show_issue = fields.Boolean(String='Show Issue')


class WorkOrder(models.Model):
    _name = 'work.order'
    _description = 'Work Order'
    _order = "priority DESC, planned_date, name, id"

    @api.multi
    def stage_find(self, domain=[], order='sequence'):
        search_domain = []
        search_domain += list(domain)
        stage_ids = self.env['work.order.stage'].search(search_domain, order=order)
        if stage_ids:
            return stage_ids[0]
        return False

    @api.multi
    def _get_default_stage_id(self):
        return self.stage_find([])

    @api.multi
    def _count_expenses(self):
        count = 0
        for order in self:
            if order.is_parent:
                for sub_order in order.sub_work_orders:
                    count += len(sub_order.expenses)
            else:
                count = len(order.expenses)
        self.expense_count = count

    @api.multi
    def _compute_is_reviewer(self):
        res = {}
        for order in self:
            if self.env['res.users'].has_group('l10n_mn_repair_work_order.group_manager'):
                res[order.id] = True
            else:
                res[order.id] = False
        return res

    @api.multi
    def _count_sub_work_orders(self):
        res = {}
        for order in self:
            order.sub_count = len(order.sub_work_orders)

    @api.multi
    def get_progress(self, sub_wo):
        progress = 0.0
        for wo in self.browse(sub_wo):
            spent_hours = 0.0
            for work in wo.works:
                spent_hours += work.hours
                if wo.planned_hours > 0:
                    progress = (spent_hours / wo.planned_hours) * 100
        if progress > 100:
            progress = 100
        return progress

    @api.multi
    def _compute_progress(self):
        res = {}
        for wo in self:
            progress = 0.0
            if not wo.is_parent:
                progress = self.get_progress(wo.id)
            else:
                for sub_wo in wo.sub_work_orders:
                    progress += (sub_wo.planned_hours / wo.planned_hours) * self.get_progress(sub_wo.id)
            wo.progress = progress

    @api.multi
    def _stage_state_type(self):
        for state in self:
            state.state_type = state.stage.state_type

    name = fields.Char(String='Task Summary', size=128, required=True)
    origin = fields.Char(String='Order Reference', readonly=True, copy=False)
    description = fields.Text(String='Description')
    priority = fields.Selection([('0', 'Low'), ('1', 'Normal'), ('2', 'High')], default='0', String='Priority')
    stage = fields.Many2one('work.order.stage', default=_get_default_stage_id, String='Stage')
    products = fields.One2many('work.order.products', 'order_id', String='Products')
    works = fields.One2many('work.order.work', 'order_id', String='Works')
    planned_date = fields.Date(String='Planned Date', copy=False, required=True)
    end_date = fields.Date(String='End Date', copy=False, required=True)
    conclusion = fields.Text(String='Conclusion')
    assigned_to = fields.Many2one('res.users', String='Assigned to')
    breakdown_types = fields.Many2many('repair.breakdown.type', 'order_breakdown_type_rel', 'order_id', 'breakdown_type_id', String='Breakdown Types')
    breakdown_causes = fields.Many2many('repair.breakdown.cause', 'order_breakdown_cause_rel', 'order_id', 'breakdown_cuase_id', String='Breakdown causes')
    work_order_type = fields.Many2one('work.order.type', String='Work Order Type')
    expenses = fields.One2many('product.expense', 'work_order_id', 'Work Order Expenses', readonly=True)
    expense_count = fields.Integer(compute=_count_expenses, type='integer', string='Expense')
    repair_calendars = fields.One2many('repair.calendar', 'wo_id', String='Calendars')
    planned_hours = fields.Float(String='Planned hours', required=True)
    issues = fields.Text(String='Issues')
    repair_team = fields.Many2many('hr.employee', 'repair_team_employee_rel', 'repair_id', 'employee_id', String='Repair team')
    color = fields.Integer(String='Color Index')
    show_issue = fields.Boolean(related='stage.show_issue', string='Show issue')
    state_type = fields.Char(compute=_stage_state_type, string='State type')
    project = fields.Many2one('project.project', String='Project', domain="[('use_repair','=',True)]")
    is_reviewer = fields.Boolean(compute=_compute_is_reviewer, string='Is Reviewer')
    closed = fields.Boolean(String='Closed', default=False)
    is_parent = fields.Boolean(String='Is parent', default=False)
    parent_wo = fields.Many2one('work.order', String='Parent work order')
    sub_work_orders = fields.One2many('work.order', 'parent_wo', String='Sub work orders')
    sub_count = fields.Integer(compute=_count_sub_work_orders, string='Sub work order count')
    progress = fields.Integer(compute=_compute_progress, string='Progress')
    partner_id = fields.Many2one('res.partner', string='Partner')
    company_id = fields.Many2one('res.company', string='Company')
    repair_breakdown_ids = fields.One2many('repair.breakdown', 'work_order_id', 'Repair Breakdown')

    _sql_constraints = [
        ('hour_check',
         'CHECK(planned_hours > 0.0)',
         "Planned hours must be greater than 0!"),
    ]

    @api.onchange('name')
    def onchange_name(self):
        projects = self.env['project.project'].search([('use_repair', '=', True)])
        if len(projects) == 1:
            return {
                'domain': {
                    'project': [('id', 'in', projects.ids)]
                },
                'value': {
                    'project': projects[0]
                }
            }
        else:
            return {
                'domain': {
                    'project': [('id', 'in', projects.ids)]
                }
            }

    @api.onchange('state_type')
    def onchange_stage(self):
        self.state_type = self.stage.state_type
        return False

    @api.model
    def create(self, vals):
        if vals.get('origin', '-') == '-':
            vals['origin'] = self.env['ir.sequence'].next_by_code('work.order') or '-'
            vals['name'] = vals['name'].split('-')[0] + '-' + vals['origin']
        if not vals.get('repair_calendars'):
            order = super(WorkOrder, self).create(vals)
            self.env['work.order'].create_default_calendar()
        else:
            planned_date = datetime.strptime(vals['repair_calendars'][0][2]['date'], DEFAULT_SERVER_DATE_FORMAT)
            for triplet in vals['repair_calendars']:
                triplet_date = datetime.strptime(triplet[2]['date'], DEFAULT_SERVER_DATE_FORMAT)
                if triplet_date < planned_date:
                    planned_date = triplet_date
            vals['planned_date'] = planned_date
            order = super(WorkOrder, self).create(vals)
        return order

    @api.multi
    def write(self, vals):
        # if stage changed
        if len(vals) == 1 and vals.get('stage'):
            stage = self.env['work.order.stage'].browse(vals.get('stage'))
            for wo in self:
                # check reviewed and done
                if wo.state_type in ['done', 'review'] and not self.env['res.users'].has_group('l10n_mn_repair_work_order.group_manager'):
                    raise Warning(_('You can\'t change done or reviewed work order.'))

                # check to review
                if stage.state_type == 'review' and not self.env['res.users'].has_group('l10n_mn_repair_work_order.group_manager'):
                    raise Warning(_('You can\'t review work order.'))
                if stage.state_type == 'review' and wo.state_type != 'done' and not wo.is_parent:
                    raise Warning(_('Only done work order could review.'))

        # create calendars
        if 'repair_calendars' in vals and vals['repair_calendars'] != [(6, 0, [])]:
            for wo in self:
                # get remove calendar ids
                remove_calendar_ids = []
                for triplet in vals['repair_calendars']:
                    if triplet[0] == 2:
                        remove_calendar_ids.append(triplet[1])

                # get modified calendar ids
                date_modified_calendar_ids = []
                hour_modified_calendar_ids = []
                for triplet in vals['repair_calendars']:
                    if triplet[0] == 1:
                        if 'date' in triplet[2]:
                            date_modified_calendar_ids.append(triplet[1])
                        if 'planned_hours' in triplet[2]:
                            hour_modified_calendar_ids.append(triplet[1])

                # calculate date and hours
                new_dates = []
                new_hours = 0.0
                for calendar in wo.repair_calendars:
                    if calendar.id not in remove_calendar_ids:
                        if calendar.id not in date_modified_calendar_ids:
                            new_dates.append(datetime.strptime(calendar.date, DEFAULT_SERVER_DATE_FORMAT))
                        if calendar.id not in hour_modified_calendar_ids:
                            new_hours += calendar.planned_hours
                for triplet in vals['repair_calendars']:
                    if triplet[0] in (0, 1):
                        if 'date' in triplet[2]:
                            new_dates.append(datetime.strptime(triplet[2]['date'], DEFAULT_SERVER_DATE_FORMAT))
                        if 'planned_hours' in triplet[2]:
                            new_hours += triplet[2]['planned_hours']
                if new_dates:
                    vals['planned_date'] = min(new_dates)
                    vals['planned_hours'] = new_hours
                res = super(WorkOrder, self).write(vals)
                if not new_dates:
                    self.env['work.order'].create_default_calendar()
        else:
            res = super(WorkOrder, self).write(vals)
            if 'planned_date' in vals or 'planned_hours' in vals:
                if (self.ids):
                    self.env['work.order'].create_default_calendar()

        # calculate planned date
        if vals.get('planned_date'):
            for wo in self:
                this_planned_date = vals.get('planned_date')
                # set sub work order dates
                if wo.is_parent:
                    for sub_wo in wo.sub_work_orders:
                        if sub_wo.planned_date < this_planned_date:
                            self.env['work.order'].write([sub_wo.id], {'planned_date': vals.get('planned_date')})
                # update parent work order date
                else:
                    if wo.parent_wo and wo.parent_wo.planned_date > this_planned_date:
                        self.env['work.order'].write([wo.parent_wo.id], {'planned_date': this_planned_date})

        # calculate parent work order planned hours
        if vals.get('planned_hours'):
            for wo in self:
                if not wo.is_parent:
                    total_hours = 0
                    for sub_wo in wo.parent_wo.sub_work_orders:
                        total_hours += sub_wo.planned_hours
                    self.env['work.order'].write({'planned_hours': total_hours})
        return res

    @api.multi
    def unlink(self):
        for wo in self:
            if wo.stage.state_type in ['done', 'review']:
                raise Warning(_('Can\'t delete done or reviewed work order.'))
        self.works.unlink()
        return super(WorkOrder, self).unlink()

    @api.multi
    def create_default_calendar(self):
        # day_work_hours = self.project.get_repair_day_work_hours()
        day_work_hours = None
        for this in self:
            day_work_hours = this.project.get_repair_day_work_hours()

            if this.is_parent:
                break
            planned_hours = this.planned_hours
            days = math.ceil(planned_hours / day_work_hours)
            hours = day_work_hours
            current_date = datetime.strptime("{0}".format(this.planned_date), DEFAULT_SERVER_DATE_FORMAT)

            # if update:
            #     this.repair_calendars.unlink()

            while days > 0:
                if planned_hours >= day_work_hours:
                    hours = day_work_hours
                else:
                    hours = planned_hours
                self.env['repair.calendar'].create({
                    'date': current_date,
                    'wo_id': this.id,
                    'planned_hours': hours
                })
                current_date += timedelta(days=1)
                planned_hours = planned_hours - day_work_hours
                days = days - 1
        return True

    @api.multi
    def view_product_expense(self):
        mod_obj = self.env['ir.model.data']
        dummy, action_id = tuple(mod_obj.get_object_reference('l10n_mn_technic_expense', 'action_technic_expense'))
        action = self.env['ir.actions.act_window'].browse(action_id).read([])[0]
        ex_ids = []
        for order in self:
            ex_ids += [expense.id for expense in order.expenses]
            if order.is_parent:
                for sub_order in order.sub_work_orders:
                    ex_ids += [expense.id for expense in sub_order.expenses]
        action['domain'] = "[('id','in',[" + ','.join(map(str, ex_ids)) + "])]"
        return action

    @api.multi
    def print_work_order(self):
        form = self.read()[0]
        data = {
            'ids': self.ids,
            'model': 'work.order',
            'form': form
        }
        ret = self.env["report"].get_action([], 'l10n_mn_repair_work_order.print_work_order', data=data)
        return ret


class RepairCalendar(models.Model):
    _name = "repair.calendar"
    _description = "Repair calendar"
    _order = 'date'

    wo_id = fields.Many2one('work.order', String='Work order', ondelete='cascade')
    date = fields.Date(String='Date', copy=False)
    planned_hours = fields.Float(String='Planned hours')
    notes = fields.Text(String='Notes')

    _sql_constraints = [
        ('hour_check',
         'CHECK(planned_hours > 0.0)',
         "Planned hours on the '%s' must be greater than 0!" % (_description)),

        ('date_uniq',
         'UNIQUE(wo_id, date)',
         "Date on the '%s' can not be duplicated. Please select different dates." % (_description)),
    ]

    @api.multi
    def _check_planned_hours(self):
        obj = self.browse(self.ids[0])
        day_work_hours = obj.wo_id.project.get_repair_day_work_hours()
        if obj.planned_hours > day_work_hours:
            return False
        return True

    _constraints = [
        (_check_planned_hours, "Planned hours on the '%s' must be less than day work time." % (_description), ['planned_hours']),
    ]


class WorkOrderProducts(models.Model):
    _name = "work.order.products"

    _description = "Work order spare part and materials"
    _order = "product_id"

    order_id = fields.Many2one('work.order', String='Work order', ondelete='cascade')
    product_id = fields.Many2one('product.product', String='Product')
    product_qty = fields.Float(String='Quantity', digits=(12, 1), required=True, default=1)
    notes = fields.Text(String='Description')
    product_expense_id = fields.Many2one('product.expense', 'Expense', readonly=True)


class WorkOrderWork(models.Model):
    _name = "work.order.work"

    _description = "Work"
    _order = 'date DESC'

    @api.onchange('employee')
    def _get_skill_level(self):
        skill = self.env['hr.employee'].sudo().search([('id', '=', self.employee.id)])
        if skill:
            skill_level = skill.skill_level.code
            self.repairman_rating = skill_level

    @api.multi
    def _get_defualt_rating(self):
        self.env['work.order.work'].search([('employee', '=', self.employee.id)])

    date = fields.Date(String='Date')
    order_id = fields.Many2one('work.order', String='Work order')
    hours = fields.Float(String='Time spent')
    employee = fields.Many2one('hr.employee', String='Employee', required=True)
    name = fields.Text(String='Work summary')
    repairman_rating = fields.Selection([
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5')
    ], default=_get_defualt_rating, String='Rating')

    @api.model
    def create(self, vals):
        res = super(WorkOrderWork, self).create(vals)
        return res

    @api.multi
    def write(self, vals):
        res = super(WorkOrderWork, self).write(vals)
        return res


class StockWarehouse(models.TransientModel):
    _name = 'stock.expense.wizard'

    warehouse = fields.Many2one('stock.warehouse', string='Warehouse', required=1)

    @api.model
    def create_expense_line(self, line_value_dict):
        self.env['product.expense.line'].create(line_value_dict)

    @api.multi
    def action_approve(self):
        active_ids = self._context['active_ids']
        wo_order = self.env['work.order'].search([('id', '=', active_ids[0])])
        wo_products = wo_order.products
        employee_id = self.env['hr.employee'].search([('user_id', '=', self._uid)])[0]
        value = {
            'employee': employee_id.id,
            'company': employee_id.company_id.id,
            'department': employee_id.department_id.id,
            'name': wo_order.name + ' (spare parts expense)',
            # 'description': wo_order.name + ' (spare parts expense)',
            'warehouse': self.warehouse.id,
            'work_order_id': wo_order.ids[0]
        }
        product_expense_id = self.env['product.expense'].create(value)
        is_created_line = False
        for wo_product in wo_products:
            if not wo_product.product_expense_id:
                wo_product.write({'product_expense_id': product_expense_id.id})
                val = {
                    'quantity': wo_product.product_qty,
                    'product': wo_product.product_id.id,
                    'name': wo_product.product_id.name,
                    'expense': product_expense_id.id,
                }
                self.create_expense_line(val)
                is_created_line = True
        if not is_created_line:
            product_expense_id.unlink()
        return True
