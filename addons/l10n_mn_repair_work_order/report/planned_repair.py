# -*- coding: utf-8 -*-
from io import BytesIO
import base64
from datetime import datetime
from datetime import timedelta
import pytz
import time

import xlsxwriter

from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.exceptions import Warning

from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles  # @UnresolvedImport

class PlannedReportRepairWizard(models.TransientModel):
    _name = 'planned.report.repair.wizard'

    @api.multi
    def _default_start_date(self):
        now = datetime.now()
        month = timedelta(days=30)
        return now - month

    @api.multi
    def _get_groups(self):
        return [('assigned_to', _('Assignee')),
                ('week', _('Week')),
                ('work_order_type', _('Repair type'))]

    group_by = fields.Selection(lambda self: self._get_groups(), 'Group By')
    start_date = fields.Date(String = 'Start date', default=_default_start_date, required=True)
    end_date = fields.Date(String = 'End date', default=lambda *a: datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT), required=True)
    is_parent = fields.Boolean(String = 'Only parent work order', default=False)

    @api.multi
    def validate_fields(self):
        end_date = datetime.strptime("{0}".format(self.end_date), DEFAULT_SERVER_DATE_FORMAT)
        if end_date > datetime.now():
            raise Warning(_('End date is invalid.'))
        start_date = datetime.strptime("{0}".format(self.start_date), DEFAULT_SERVER_DATE_FORMAT)
        if start_date > end_date:
            raise Warning(_('Start date must be less than end date.'))

    @api.multi
    def load_data(self):
        datas = None
        if self.is_parent:
            datas = self.env['work.order'].search([('planned_date', '>=', self.start_date), ('planned_date', '<=', self.end_date), ('is_parent', '=', True)])
        else:
            datas = self.env['work.order'].search([('planned_date', '>=', self.start_date), ('planned_date', '<=', self.end_date), ('is_parent', '=', False)])

        return datas

    @api.multi
    def compute_column_number(self):
        colx_number = 8
        if self.group_by in ['assigned_to', 'work_order_type']:
                colx_number -= 1
        return colx_number

    @api.multi
    def create_filters(self, book, sheet, rowx, output_obj):
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        if self.group_by:
            sheet.merge_range(rowx, 0, rowx, self.compute_column_number() / 2, '%s: %s' % (_('Group By'), output_obj._(dict(self._get_groups())[self.group_by])), format_filter)
            rowx += 1
        return sheet, rowx

    @api.multi
    def create_addional_titles(self, book, sheet, rowx, colx):
        return book, sheet, rowx, colx

    @api.multi
    def group_data(self):
        datas = self.load_data()
        if self.group_by == 'assigned_to':
            datas = datas.sorted(key=lambda r: r.assigned_to.name)
        elif self.group_by == 'work_order_type':
            datas = datas.sorted(key=lambda r: r.work_order_type.name)
        elif self.group_by == 'week':
            datas = datas.sorted(key=lambda r: r.planned_date)

        return datas

    @api.multi
    def write_groups(self, book, sheet, rowx, colx, line, current_group, num):
        format_group = book.add_format(ReportExcelCellStyles.format_group)
        if self.group_by in ['assigned_to', 'work_order_type']:
            if current_group != line[self.group_by]:
                current_group = line[self.group_by]
                num = 1
                current_group_value = line[self.group_by].name or _('Undefined')
                sheet.merge_range(rowx, colx, rowx, self.compute_column_number() - 1, current_group_value, format_group)
                rowx += 1

        return sheet, rowx, colx, current_group, num

    @api.multi
    def create_addional_lines(self, book, sheet, rowx, colx, line):
        return book, sheet, rowx, colx

    @api.multi
    def export_report(self):
        # validate fields
        self.validate_fields()

        # load data
        datas = self.load_data()

        if len(datas) == 0:
            raise Warning(_('No result is found.'))

        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_date = book.add_format(ReportExcelCellStyles.format_date)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_group = book.add_format(ReportExcelCellStyles.format_group)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_date = book.add_format(ReportExcelCellStyles.format_content_date)

        # create name
        report_name = _('Planned Repair')
        file_name = "%s_%s.xlsx" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix='planned_repair', form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(8)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.hide_gridlines(2)
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})

        rowx = 0

        # compute column number
        colx_number = self.compute_column_number()

        # create name
        sheet.merge_range(rowx, 0, rowx, colx_number - 1, report_name.upper(), format_name)
        sheet.set_row(rowx, 30, format_name)
        rowx += 1

        # create filters
        sheet, rowx = self.create_filters(book, sheet, rowx, report_excel_output_obj)

        # create date time
        if self.env.user.partner_id.tz:
            tz = pytz.timezone(self.env.user.partner_id.tz)
        else:
            tz = pytz.utc
        now_utc = datetime.now(pytz.timezone('UTC'))
        now_user_zone = now_utc.astimezone(tz)
        report_datetime = '%s: %s' % (_('Created On'), now_user_zone.strftime('%Y-%m-%d %H:%M:%S'))
        sheet.merge_range(rowx - 1, colx_number / 2 + 2, rowx - 1, colx_number - 1, report_datetime, format_date)

        # create titles
        colx = 0
        sheet.merge_range(rowx, colx, rowx + 1, colx, u'№', format_title)
        colx += 1
        sheet.merge_range(rowx, colx, rowx + 1, colx, _('Reference'), format_title)
        colx += 1
        if self.group_by != "work_order_type":
            sheet.merge_range(rowx, colx, rowx + 1, colx, _('Repair type'), format_title)
            colx += 1

        sheet.merge_range(rowx, colx, rowx + 1, colx, _('Name'), format_title)
        colx += 1
        # additional title
        book, sheet, rowx, colx = self.create_addional_titles(book, sheet, rowx, colx)
        sheet.merge_range(rowx, colx, rowx, colx + 1, _('Duration'), format_title)
        sheet.write(rowx + 1, colx, _('Start date'), format_title)
        sheet.set_column(colx,colx, 20)
        sheet.write(rowx + 1, colx + 1, _('End date'), format_title)
        sheet.set_column(colx + 1,colx + 1, 20)
        colx += 2
        if self.group_by != "assigned_to":
            sheet.merge_range(rowx, colx, rowx + 1, colx, _('Assignee'), format_title)
            sheet.set_column(colx,colx, 25)
            colx += 1

        sheet.merge_range(rowx, colx, rowx + 1, colx, _('Description'), format_title)
        sheet.set_column(colx,colx, 30)
        colx += 1

        rowx += 2

        sheet.repeat_rows(0, rowx - 1)
        sheet.freeze_panes(rowx, 0)

        # group data
        datas = self.group_data()

        # CREATE LINES
        num = 1
        current_group = ''

        # # prepare week group
        # end_date = datetime.strptime("{0}".format(self.end_date), DEFAULT_SERVER_DATE_FORMAT)
        # start_date = datetime.strptime("{0}".format(self.start_date), DEFAULT_SERVER_DATE_FORMAT)
        # current_week = 1
        # wrote_first_week = False
        # current_sunday = end_date
        # for i in range(0, (end_date - start_date).days):
        #     current_day = start_date + timedelta(days=i)
        #     if current_day.weekday() == 6:  # sunday
        #         current_sunday = current_day
        #         break

        # write lines
        for line in datas:
            colx = 0

            # write group
            sheet, rowx, colx, current_group, num = self.write_groups(book, sheet, rowx, colx, line, current_group, num)

            # # write week group
            # if self.group_by == 'week':
            #     if current_week == 1 and not wrote_first_week:
            #         sheet.merge_range(rowx, colx, rowx, colx_number - 1, _('Week %s') % current_week, format_group)
            #         rowx += 1
            #         wrote_first_week = True
            #     if current_sunday < datetime.strptime("{0}".format(line['planned_date']), DEFAULT_SERVER_DATE_FORMAT):
            #         current_sunday = current_sunday + timedelta(days=7)
            #         current_week += 1
            #         num = 1
            #         sheet.merge_range(rowx, colx, rowx, colx_number - 1, _('Week %s') % current_week, format_group)
            #         rowx += 1

            # write line
            sheet.write_number(rowx, colx, num, format_content_number)
            colx += 1
            sheet.write_string(rowx, colx, line.origin, format_content_text)
            colx += 1
            if self.group_by != "work_order_type":
                sheet.write_string(rowx, colx, line.work_order_type.name if line.work_order_type else '', format_content_text)
                colx += 1
            sheet.write_string(rowx, colx, line.name if line.name else '', format_content_text)
            colx += 1
            # additional line
            book, sheet, rowx, colx = self.create_addional_lines(book, sheet, rowx, colx, line)
            sheet.write_datetime(rowx, colx, datetime.strptime("{0}".format(line['planned_date']), DEFAULT_SERVER_DATE_FORMAT), format_content_date)
            colx += 1
            # if not line.is_parent:
                # print '\n\n LINE', line.repair_calendars.date
                # finish_date = datetime.strptime("{0}".format(line.repair_calendars[-1].date), DEFAULT_SERVER_DATE_FORMAT)
            # else:
            finish_date = datetime.strptime("{0}".format(line.planned_date), DEFAULT_SERVER_DATE_FORMAT)
            # for sub_wo in line.sub_work_orders:
            #     sub_finish_date = datetime.strptime("{0}".format(sub_wo.repair_calendars[-1].date), DEFAULT_SERVER_DATE_FORMAT)
            #     if sub_finish_date > finish_date:
            #         finish_date = sub_finish_date
            sheet.write_datetime(rowx, colx, finish_date, format_content_date)
            colx += 1
            if self.group_by != "assigned_to":
                sheet.write_string(rowx, colx, line.assigned_to.name if line.assigned_to else '', format_content_text)
                colx += 1

            sheet.write_string(rowx, colx, line.description if line.description else '', format_content_text)
            colx += 1

            sheet.set_row(rowx, 35)
            num += 1
            rowx += 1

        book.close()

        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()
