# -*- coding: utf-8 -*-
##############################################################################
#
##############################################################################

from odoo import api, fields, models, _

class PrintWorkOrder(models.AbstractModel):
    _name = 'report.l10n_mn_repair_work_order.print_work_order'

    @api.multi
    def render_html(self, docargs, data=None):
        report_obj = self.env['report']
        work_order_obj = self.env['work.order']
        report_obj._get_report_from_name('l10n_mn_repair_work_order.print_work_order')
        context = self._context or {}
        work_order = work_order_obj.browse(context.get('active_ids'))

        docargs = {
            'doc_ids': data['ids'],
            'docs': None,
            'work_order': work_order
        }
        return report_obj.render('l10n_mn_repair_work_order.print_work_order', docargs)
