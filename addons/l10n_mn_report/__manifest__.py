# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Report Base",
    'version': '1.0',
    'depends': ['base', 'report'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
        - Эксел тайлан гаргах үндсэн харагдац болон функц
        - OdERP-ийн тайлангийн үндсэн хэлбэржүүлэлтүүдийн эксел хувилбар
        - Эксел тайлангийн агуулгаас хамаарч нүдний хэмжээг автоматаар тохируулах класс
        - PDF тайлангийн хэлбэржүүлэлт
    """,
    'data': [
        'views/report_excel_views.xml',
        'views/report_html_viewer_views.xml',
        'views/l10n_mn_report.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False,
}
