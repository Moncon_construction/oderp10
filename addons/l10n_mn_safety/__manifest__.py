# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

{
    'name': 'Mongolian HSE',
    'version': '1.0',
    'depends': ['hr_holidays', 'l10n_mn_hr', 'l10n_mn_project', 'l10n_mn_stock'],
    'summary': 'Health, Safety and Environment',
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
Health, Safety and Environment
    """,
    'data': [
        'data/accident_data.xml',
        'data/health_data.xml',
        'security/safety_security.xml',
        'security/ir.model.access.csv',
        'email_templates/email_template.xml',
        'views/safety_view.xml',
        'views/accident_view.xml',
        'views/health_view.xml',
        'views/hazard_report_view.xml',
        'views/hr_rule_view.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
