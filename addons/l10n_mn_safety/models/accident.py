# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from odoo.exceptions import UserError


class AccidentAccident(models.Model):
    _name = 'accident.accident'
    _description = "Accident"
    _inherit = "mail.thread"

    @api.one
    def _show_approve_button(self):
        if (self.env.uid == self.employee_id.id) or (self.env.uid == self.create_uid.id):
            self.show_approve_button = True
        else:
            self.show_approve_button = False

    name = fields.Char(string="Name")
    state = fields.Selection([('draft', 'Open'), ('sent', 'Sent'), ('done', 'Decided')], 'Status', default="draft", copy=False, track_visibility='onchange')
    accident_type_id = fields.Many2one('accident.type', string="Accident type")
    accident_level = fields.Selection([('easy', 'Easy'), ('hard', 'Hard'), ('seriously', 'Seriously'), ('low', 'Low'), ('medium', 'Medium'), ('high', 'High')], 'Accident level', copy=False)
    rate = fields.Integer('Rate')
    impact_id = fields.Many2one('accident.impact', string="Accident impact")
    property_type_id = fields.Many2one('accident.property.type', string="The type of accident property")
    reason_id = fields.Many2one('accident.reason', string="Accident reason")
    consequence_id = fields.Many2one('accident.consequence', string="Accident consequence")
    location_id = fields.Many2one('accident.location', string="Accident location")
    created_date = fields.Date(string='Created Date', default=fields.Date.today)
    accident_date = fields.Date(string='Accident Date')
    employee_id = fields.Many2one('res.users', string="Decider")
    employee_ids = fields.Many2many('res.users', 'accident_employee_rel', 'accident_id', 'employee_id', 'Inform Workers', readonly=True, states={'draft': [('readonly', False)]})
    description = fields.Text(string="Description")
    decision = fields.Text(string="Decision")
    tasks = fields.One2many('project.task', 'accident_id', string='Accident')
    # adding show_approve_button field to limit access to the button
    show_approve_button = fields.Boolean(compute=_show_approve_button, string='Show approve button?')

    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Reference must be unique per Name!'),
    ]

    def sent_mail(self, only_inform=False):
        if self.employee_ids:
            partner_ids = [emp.partner_id.id for emp in self.employee_ids]
            partner_ids.append(self.employee_id.partner_id.id)
            mail_invite = self.env['mail.wizard.invite'].with_context({
                'default_res_model': 'accident.accident',
                'default_res_id': self.ids[0]}).create({
                'partner_ids': [(6, 0, partner_ids)], 'send_mail': True
            })
            mail_invite.add_followers()
        if not only_inform:
            template_id = self.env.ref('l10n_mn_safety.accident_mail_template')
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            dbname = self.env.cr.dbname
            action_id = self.env.ref('l10n_mn_safety.action_accident_accident').id
            menu_id = self.env['ir.ui.menu'].search([('action', 'like', action_id)])

            ctx = {
                'base_url': base_url,
                'db_name': dbname,
                'id': self.ids[0],
                'menu_id': menu_id.id,
                'action_id': action_id,
            }
            template_id.with_context(ctx).send_mail(self.ids[0])

    @api.multi
    def write(self, vals):
        if 'employee_id' in vals:
            self.sent_mail()
        if 'employee_ids' in vals:
            self.sent_mail(only_inform=True)
        return super(AccidentAccident, self).write(vals)

    @api.multi
    def action_sent(self):
        ''' Бүртгэлийн хянагч болон мэдэгдэл хүлээн авах ажилчдад имэйл илгээх.
        '''
        self.ensure_one()
        self.write({'state': "sent"})
        self.sent_mail()

    @api.multi
    def action_done(self):
        self.write({'state': "done"})

    @api.multi
    def action_cancel(self):
        self.write({'state': "draft"})

    @api.multi
    def unlink(self):
        for accident in self:
            if accident.state != 'draft':
                raise UserError(_('You can only delete draft accident.'))
            if accident.tasks:
                raise UserError(_('You cannot delete a accident containing tasks. You can either delete all the project\'s tasks and then delete the project.'))
        return super(AccidentAccident, self).unlink()


class ProjectTask(models.Model):
    _inherit = 'project.task'

    accident_id = fields.Many2one('accident.accident', string='Accident')

class AccidentType(models.Model):
    _name = 'accident.type'
    _description = "Accident type"

    name = fields.Char(string="Name")

    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Reference must be unique per Name!'),
    ]


class AccidentImpact(models.Model):
    _name = 'accident.impact'
    _description = "Accident impact"

    name = fields.Char(string="Name")

    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Reference must be unique per Name!'),
    ]


class AccidentPropertyType(models.Model):
    _name = 'accident.property.type'
    _description = "The type of accident property"

    name = fields.Char(string="Name")

    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Reference must be unique per Name!'),
    ]


class AccidentReason(models.Model):
    _name = 'accident.reason'
    _description = "Reason for accident"

    name = fields.Char(string="Name")

    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Reference must be unique per Name!'),
    ]


class AccidentConsequence(models.Model):
    _name = 'accident.consequence'
    _description = "Accident consequence"

    name = fields.Char(string="Name")

    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Reference must be unique per Name!'),
    ]


class AccidentLocation(models.Model):
    _name = 'accident.location'
    _description = "Accident location"

    name = fields.Char(string="Name")

    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Reference must be unique per Name!'),
    ]


class AccidentCategory(models.Model):
    _name = 'accident.category'
    _description = "Accident Category"

    name = fields.Char(string="Name")

    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Reference must be unique per Name!'),
    ]
