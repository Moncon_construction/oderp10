# -*- coding: utf-8 -*-
from datetime import datetime

from odoo import _, api, fields, models
from odoo.exceptions import UserError


class hse_hazard_report(models.Model):
    _name = 'hse.hazard.report'
    _description = 'Hazard report'
    _inherit = "mail.thread"
    _order = 'datetime desc'

    name = fields.Char(string='Name', required=True)
    datetime = fields.Datetime('Datetime', required=True, default=datetime.today(), states={
                               'draft': [('readonly', False)]})
    state = fields.Selection([
        ('draft', 'Draft'),
        ('sent', 'Sent'),
        ('assigned', 'Assigned'),
        ('resolved', 'Resolved'),
        ('cancelled', 'Cancelled')
    ], default='draft', required=True, track_visibility='onchange')

    project_id = fields.Many2one('project.project', 'Project')
    location_id = fields.Many2one(
        'accident.location', string="Accident location")
    responsible = fields.Many2one('res.users', 'responsible', required=True)
    employee_id = fields.Many2one('res.users', string="Decider", copy=False)

    hazard_type = fields.Selection([('minor', 'Minor'), ('medium', 'Medium'), ('serious', 'Serious')], 'Hazard type', required=True)
    hazard_identification = fields.Text('Hazard identification', required=True, copy=False)
    corrective_action_to_be_taken = fields.Text('Corrective action to be taken', copy=False)
    resolved_date = fields.Datetime('Resolved date', copy=False)
    assigned_date = fields.Datetime('Assigned date', copy=False)
    corrective_action_taken = fields.Text('Corrective action taken', copy=False)

    def action_to_draft(self):
        self.write({'state': "draft"})

    def sent_mail(self, user=None, action=None):
        #if action == 'sent':
        template_id = self.env.ref('l10n_mn_safety.hazard_report_mail_template_responsible')
        if action == 'assigned':
            template_id = self.env.ref('l10n_mn_safety.hazard_report_mail_template_decide')
        elif action == 'cancelled':
            template_id = self.env.ref('l10n_mn_safety.hazard_report_mail_template_cancelled')
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        dbname = self.env.cr.dbname
        action_id = self.env.ref('l10n_mn_safety.action_hazar_report').id
        menu_id = self.env['ir.ui.menu'].search([('action', 'like', action_id)])

        ctx = {
            'base_url': base_url,
            'db_name': dbname,
            'id': self.ids[0],
            'menu_id': menu_id.id,
            'action_id': action_id,
        }
        template_id.with_context(ctx).send_mail(self.ids[0])

    @api.multi
    def unlink(self):
        if self.state == 'draft':
            return super(hse_hazard_report, self).unlink()
        else:
            raise UserError(_('Can\'t delete state is not draft'))

    @api.multi
    def write(self, vals):
        result = super(hse_hazard_report, self).write(vals)
        if vals.has_key('responsible'):
            self.sent_mail(user=self.responsible)
        return result

    @api.multi
    def action_to_sent(self):
        self.ensure_one()
        self.write({'state': 'sent'})
        self.sent_mail(user=self.responsible, action='sent')

    def action_to_assigned(self):
        ''' 
            Бүртгэлийн шийдвэрлэх ажилтанд имэйл илгээх.
        '''
        self.ensure_one()
        self.write({'state': 'assigned'})
        self.sent_mail(user=self.employee_id, action='assigned')


    def action_to_resolved(self):
        self.write({'state': "resolved"})

    def action_to_cancelled(self):
        self.write({'state': "cancelled"})
        self.sent_mail(action='cancelled')

