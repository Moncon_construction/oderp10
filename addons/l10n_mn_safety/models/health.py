# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from odoo.exceptions import UserError


class HealthAppointment(models.Model):
    _name = 'health.appointment'
    _description = "Health Appointment"
    _inherit = "mail.thread"

    name = fields.Char(string="Name", required=True)
    state = fields.Selection([('draft', 'Open'), ('done', 'Done')], 'Status', default="draft", copy=False, required=True)
    partner_id = fields.Many2one('res.partner', string="Partner")
    employee_id = fields.Many2one('res.partner', string="Employee", required=True)
    user_id = fields.Many2one('res.users', string='User', default=lambda self: self.env.user)

    created_date = fields.Date(string='Date', default=fields.Date.today, required=True)
    appointment_type_id = fields.Many2one('health.appointment.type', string="The type of appointment", required=True)
    reason_id = fields.Many2one('help.reason', string="Reason", required=True)
    sickness_id = fields.Many2one('health.sickness', string="Sickness")
    doctor_id = fields.Many2one('hr.employee', string="Inspector", required=True)
    warehouse_id = fields.Many2one('stock.warehouse', string="Warehouse")
    description = fields.Text(string="Description", required=True)
    leaves = fields.One2many('hr.holidays', 'health_appointment_id', string="Leaves")

    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Reference must be unique per Name!'),
    ]

    @api.multi
    def check_leaves(self):
        if self.leaves:
            raise UserError(_('You cannot delete a accident containing tasks. You can either delete all the project\'s tasks and then delete the project.'))

    @api.multi
    def action_done(self):
        self.write({'state': "done"})

    @api.onchange('partner_id')
    def onchange_partner(self):
        self.employee_id = False
        if self.partner_id:
            emp_ids = self.env['res.partner'].search([('parent_id', '=', self.partner_id.id)])
            return {'domain': {'employee_id': [('id', 'in', emp_ids.ids)]}}
        else:
            partner_person = []
            for partner in self.env['res.partner'].search([]):
                if partner.company_type == 'person':
                    partner_person.append(partner.id)
            return {'domain': {'employee_id': [('id', 'in', partner_person)]}}

    @api.onchange('user_id')
    def onchange_user(self):
        partner_company = []
        partner_person = []
        for partner in self.env['res.partner'].search([]):
            if partner.company_type == 'company':
                partner_company.append(partner.id)
            else:
                partner_person.append(partner.id)
        return {'domain': {'partner_id': [('id', 'in', partner_company)], 'employee_id': [('id', 'in', partner_person)]}}

    @api.multi
    def unlink(self):
        if self.state == 'done':
            raise UserError(_('You can\'t delete if state is done'))
        else:
            return super(HealthAppointment, self).unlink()

class HealthAppointmentType(models.Model):
    _name = 'health.appointment.type'
    _description = "Appointment type"

    name = fields.Char(string="Name")

    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Reference must be unique per Name!'),
    ]


class HelpReason(models.Model):
    _name = 'help.reason'
    _description = "Reason for Help"

    name = fields.Char(string="Name")

    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Reference must be unique per Name!'),
    ]


class HealthMaterial(models.Model):
    _inherit = 'hr.holidays'
    _description = "Leaves"

    health_appointment_id = fields.Many2one('health.appointment', string="Health Appointment")


class HealthSickness(models.Model):
    _name = 'health.sickness'
    _description = "Health Sickness"

    name = fields.Char(string="Name")

    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Reference must be unique per Name!'),
    ]
