# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

from odoo import models, fields

class HrRule(models.Model):
    _name = 'hr.rule'
    _description = "Hr Rule"
    _inherit = "mail.thread"

    name = fields.Char(string="Name", track_visibility='onchange')
    rule_type_id = fields.Many2one('hr.rule.type', string="Rule type", track_visibility='onchange')
    description = fields.Text(string="Description")
    date_start = fields.Date(string='Start Date')
    active = fields.Boolean('Active', default=True)

    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Reference must be unique per Name!'),
    ]


class HrRuleType(models.Model):
    _name = 'hr.rule.type'
    _description = "Hr Rule Type"

    name = fields.Char(string="Name")

    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Reference must be unique per Name!'),
    ]
