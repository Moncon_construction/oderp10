# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

{
    'name': 'Mongolian Safety Healthy',
    'version': '1.0',
    'depends': ['l10n_mn_hr', 'l10n_mn_project', 'l10n_mn_stock', 'l10n_mn_safety'],
    'summary': 'Health, Safety and Environment',
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'data': [
        'data/safety_healthy_type_data.xml',
        'security/safety_healthy_security.xml',
        'security/ir.model.access.csv',
        'email_templates/email_template.xml',
        'views/safety_healthy_type_view.xml',
        'views/safety_healthy_view.xml',
        'views/project_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}
