# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class Project(models.Model):
    _inherit = 'project.project'
    
    use_safethy = fields.Boolean(string='Use Safety', default=False)
    
class ProjectTask(models.Model):
    _inherit = 'project.task'

    safety_healthy_id = fields.Many2one('safety.healthy', 'Safety Healthy')