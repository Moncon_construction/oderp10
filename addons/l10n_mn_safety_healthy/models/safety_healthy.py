# -*- coding: utf-8 -*-

# Part of OdERP110. See LICENSE file for full copyright and licensing details.

from odoo import api, models, fields, tools, _
from odoo.exceptions import UserError

class SafetyHealthyType(models.Model):
    _name = 'safety.healthy.type'
    
    type = fields.Char('Safety Healthy Type', required=True, translate=True)
    description = fields.Char('Description')
    
    _sql_constraints = [
            ('name_uniq', 'unique (type)', "Safety Healthy Type already exists !"),
    ]
    
    @api.multi
    def name_get(self):
        res = []
        for healthy_type in self:
            name = healthy_type.type
            res.append((healthy_type.id, name))
        return res

class SafetyHealthy(models.Model):
    _name = 'safety.healthy'
    _inherit = "mail.thread"
    
    name = fields.Char(string="Name", required = True)
    project_id = fields.Many2one('project.project', 'Project', domain = "[('use_safethy','=',True)]")
    type = fields.Many2one('safety.healthy.type', string = 'Type', required = True)
    respondent = fields.Many2one('hr.employee', string = "Respondent", required = True)
    start_date = fields.Date(string ='Start Date', required = True)
    end_date = fields.Date(string ='End Date', required = True)
    location = fields.Many2one('accident.location', string = 'Accident location')
    state = fields.Selection([('draft', 'Draft'),
                              ('sent', 'Sent'),
                              ('solved','Solved'),
                              ('canceled','Canceled')],
                             'State', default='draft')
    memo = fields.Text(string = "Memo")
    performance_note = fields.Text(string = "Performance Note")
    tasks = fields.One2many('project.task', 'safety_healthy_id', string='Task')
                        
    @api.multi
    def action_draft(self):
        self.write({'state': 'draft'})
        
    @api.multi
    def action_sent(self):
        emp_ids = []
        for employee in self.respondent:
            if employee.id != self.respondent.id:
                emp_ids += [employee.id]
        emp_ids += [self.respondent.id]
        
        part_ids = []
        if emp_ids:
            for emp_id in self.env['hr.employee'].search([('id', 'in', emp_ids)]):
                part_ids += [emp_id.user_id.partner_id.id]
             
        status = 'sent'
        
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('l10n_mn_safety_healthy', 'safety_healthy_mail_template')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict()
        ctx.update({
            'default_model': 'safety.healthy',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'employees': emp_ids,
            'partners': str(part_ids).replace('[', '').replace(']', ''),
            'mark_so_as_sent': True,
            'status': status
        })
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }
        
    @api.multi
    def action_solved(self):
        self.write({'state': 'solved'})
        
    @api.multi
    def action_canceled(self):
        self.write({'state': 'canceled'})

    @api.multi
    def unlink(self):
        for healthy in self:
            if healthy.state != 'draft':
                if healthy.state != 'canceled':
                    raise UserError(_('You can only delete draft safety healthy.'))
            if healthy.tasks:
                raise UserError(_('You cannot delete a safety healthy containing tasks. You can either delete all the project\'s tasks and then delete the project.'))
        return super(SafetyHealthy, self).unlink()
    
class MailComposer(models.TransientModel):
    _inherit = 'mail.compose.message'

    def send_mail_action(self):
        if self.env.context.get('active_model', 'ir.ui.menu') == 'safety.healthy':
            healthy = self.env['safety.healthy'].browse(self.env.context.get('active_id'))
            status = self.env.context.get('status')
            for respondent in self.env['hr.employee'].search([('id', 'in', self.env.context.get('employees'))]):
                email = u'\n Дараах хэрэглэгч рүү имэйл илгээгдэв : ' + ('<b>' + ('</b>'.join(respondent.name)) + '</b>')
                healthy.message_post(email)

            healthy.write({'state': status})
        return super(MailComposer, self).send_mail_action() 
    
    
    