# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Sale",
    'version': '1.0',
    'depends': [
        'sale',
        'l10n_mn_contacts',
        'l10n_mn_account'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Sale Additional Features
    """,
    'data': [
        'data/sale_category_demo.xml',
        'security/sale_security.xml',
        'security/ir.model.access.csv',
        'views/sale_order_views.xml',
        'views/res_users_view.xml',
        'views/res_partner_view.xml',
        'views/res_company_view.xml',
        'views/sale_order_views.xml',
        'wizard/product_pricelist_item_many_view.xml',
        'views/product_pricelist_views.xml',
        'wizard/sale_order_confirmation_views.xml',
        'wizard/sale_order_select_cancel_reason_view.xml',
        'views/res_partner_hierarchy.xml',
        'views/sale_order_cancel_reason_view.xml',
        'views/config_settings_view.xml',
        'views/sales_team_views.xml',
        'views/account_invoice_view.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
