# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError


class AccountMove(models.Model):
    _inherit = 'account.move'

    is_downpayment_invoice_reconciliation = fields.Boolean(string='Created from Down Payment Invoice Reconciliation', default=False)


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    downpayment_invoice_ids = fields.Many2many('account.invoice', 'invoice_downpayment_invoice_rel', 'invoice_id', 'downpayment_invoice_id',
                                               string="Down Payment Invoices", domain="[('is_prepayment','=',True),('partner_id','=',partner_id),('state','=','paid')]")

    def action_invoice_open(self):
        res = super(AccountInvoice, self).action_invoice_open()
        self.user_id.partner_id._compute_sales_amount()

        if self.downpayment_invoice_ids:
            move_line_obj = self.env['account.move.line']
            if not self.move_id:
                raise UserError(_('Account Move of this invoice is not created.'))
            lines_to_reconcile = []
            for line in self.move_id.line_ids:
                if line.account_id.id == self.account_id.id:
                    lines_to_reconcile.append(line.id)

            for invoice in self.downpayment_invoice_ids:
                if not invoice.move_id:
                    raise UserError(_('Account Move of Down Payment Invoice is not created.'))
                if not invoice.invoice_line_ids:
                    raise UserError(_('There is no line in Down Payment Invoice.'))

                down_payment_account_id = invoice.invoice_line_ids[0].account_id.id
                for l in invoice.move_id.line_ids:
                    if l.account_id.id == down_payment_account_id and l.full_reconcile_id.id is False:
                        lines_to_reconcile.append(l.id)
            if self.invoice_line_ids:
                for invoice_line in self.invoice_line_ids[0]:
                    self._cr.execute("""SELECT order_line_id 
                                                    FROM sale_order_line_invoice_rel 
                                                    WHERE invoice_line_id IN (%s)
                                                                """ % invoice_line.id)
                    results = self._cr.fetchall()
                    if results:
                        sale_order = self.env['sale.order.line'].browse(results[0]).order_id
                        if sale_order:
                            is_prepayment = False
                            for invoice in sale_order.invoice_ids:
                                if self.id != invoice.id and invoice.is_prepayment:
                                    is_prepayment = True
                                    break
                            if is_prepayment:
                                self.write({'date_invoice': self.date_invoice})

            lines = move_line_obj.browse(lines_to_reconcile).reconcile_part_line(self.date_invoice or False, self.journal_id and self.journal_id.id or False)
            if len(lines) > 0:
                for line in self.env['account.move.line'].browse(lines):
                    if line.reconcile_move_id:
                        line.move_id.is_downpayment_invoice_reconciliation = True
                        break

        return res
