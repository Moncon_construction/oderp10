# -*- coding: utf-8 -*-

from odoo import api, models


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    @api.multi
    def post(self):
        res = super(AccountPayment, self).post()
        for obj in self:
            for invoice in obj.invoice_ids:
                invoice.user_id.partner_id._compute_sales_amount()
        return res
