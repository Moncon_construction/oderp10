# -*- coding: utf-8 -*-

from odoo import api, fields, models


class SaleConfigSettings(models.TransientModel):
    _inherit = 'sale.config.settings'

    sale_cancel_reason_setting = fields.Selection([
        (0, "Sale Order subscription will be direct canceled immediately"),
        (1, "Select cancel reason")
    ], "Select a cause for Sale Order")

    sales_team_structure = fields.Selection([
        ('use_single_sales_team', "Partner included just one sales "),
        ('use_multi_sales_team', "Partner can be included many sales team")
    ], "Sales Team Structure", default='use_single_sales_team', related='company_id.sales_team_structure')

    module_l10n_mn_sale_gross_method = fields.Selection([
        (0, 'On Net Method'),
        (1, 'On Gross Method')], "Sale Amount Method",
        help="""Install the module that allows to calculate sales discount with GROSS METHOD.""", related='company_id.sale_method', default=0)

    @api.multi
    def set_sale_cancel_reason(self):
        return self.env['ir.values'].sudo().set_default(
            'sale.config.settings', 'sale_cancel_reason_setting', self.sale_cancel_reason_setting)
