# -*- coding: utf-8 -*-

from itertools import chain

from odoo import api, fields, models, tools, _
from odoo.exceptions import ValidationError, UserError
from datetime import datetime, timedelta


class Pricelist(models.Model):
    _inherit = "product.pricelist"
    # Хэрэглэгч үнийн хүснэгтийг үүсгэхдээ алдаатай мэдээлэл оруулсан бол анхааруулга өгнө.

    @api.model
    def create(self, values):
        res = super(Pricelist, self).create(values)
        for item in res.item_ids:
            if item.adjust_day:
                if not item.is_monday and not item.is_tuesday and not item.is_wednesday and not item.is_thursday and not item.is_friday and not item.is_saturday and not item.is_sunday:
                    raise ValidationError(_("Please must select day! %s" % item.name))

                if item.start_hour > item.end_hour:
                    raise ValidationError(_('Please set the start and end time right! %s' % item.name))

        return res
    # Хэрэглэгч үнийн хүснэгтийг засахдаа алдаатай мэдээлэл оруулсан бол анхааруулга өгнө.

    @api.multi
    def write(self, vals):
        res = super(Pricelist, self).write(vals)
        for line in self:
            for item in line.item_ids:
                if item.adjust_day:
                    if not item.is_monday and not item.is_tuesday and not item.is_wednesday and not item.is_thursday and not item.is_friday and not item.is_saturday and not item.is_sunday:
                        raise ValidationError(_("Please must select day! %s" % item.name))

                if item.start_hour > item.end_hour:
                    raise ValidationError(_('Please set the start and end time right! %s' % item.name))

        return res

    def get_product_price_rule(self, product, quantity, partner, date=False, uom_id=False, order_date=False):
        """ For a given pricelist, return price and rule for a given product """
        self.ensure_one()
        return self._compute_price_rule([(product, quantity, partner)], date=date, uom_id=uom_id, order_date=order_date)[product.id]

    def get_products_price(self, products, quantities, partners, date=False, uom_id=False, order_date=False):
        """ For a given pricelist, return price for products
        Returns: dict{product_id: product price}, in the given pricelist """
        self.ensure_one()
        return dict((product_id, res_tuple[0]) for product_id, res_tuple in self._compute_price_rule(zip(products, quantities, partners), date=date, uom_id=uom_id, order_date=order_date).iteritems())

    def get_product_price(self, product, quantity, partner, date=False, uom_id=False, order_date=False):
        """ For a given pricelist, return price for a given product """
        self.ensure_one()
        return self._compute_price_rule([(product, quantity, partner)], date=date, uom_id=uom_id, order_date=order_date)[product.id][0]

    @api.multi
    def _compute_price_rule(self, products_qty_partner, date=False, uom_id=False, order_date=False):
        """ Low-level method - Mono pricelist, multi products
        Returns: dict{product_id: (price, suitable_rule) for the given pricelist}

        If date in context: Date of the pricelist (%Y-%m-%d)

            :param products_qty_partner: list of typles products, quantity, partner
            :param datetime date: validity date
            :param ID uom_id: intermediate unit of measure
        """
        self.ensure_one()
        if not date:
            date = self._context.get('date') or fields.Date.context_today(self)
        if not uom_id and self._context.get('uom'):
            uom_id = self._context['uom']
        if uom_id:
            # rebrowse with uom if given
            products = [item[0].with_context(uom=uom_id) for item in products_qty_partner]
            products_qty_partner = [(products[index], data_struct[1], data_struct[2]) for index, data_struct in enumerate(products_qty_partner)]
        else:
            products = [item[0] for item in products_qty_partner]

        if not products:
            return {}

        categ_ids = {}
        for p in products:
            categ = p.categ_id
            while categ:
                categ_ids[categ.id] = True
                categ = categ.parent_id
        categ_ids = categ_ids.keys()

        is_product_template = products[0]._name == "product.template"
        if is_product_template:
            prod_tmpl_ids = [tmpl.id for tmpl in products]
            # all variants of all products
            prod_ids = [p.id for p in
                        list(chain.from_iterable([t.product_variant_ids for t in products]))]
        else:
            prod_ids = [product.id for product in products]
            prod_tmpl_ids = [product.product_tmpl_id.id for product in products]

        # Load all rules
        self._cr.execute(
            'SELECT item.id '
            'FROM product_pricelist_item AS item '
            'LEFT JOIN product_category AS categ '
            'ON item.categ_id = categ.id '
            'WHERE (item.product_tmpl_id IS NULL OR item.product_tmpl_id = any(%s))'
            'AND (item.product_id IS NULL OR item.product_id = any(%s))'
            'AND (item.categ_id IS NULL OR item.categ_id = any(%s)) '
            'AND (item.pricelist_id = %s) '
            'AND (item.date_start IS NULL OR item.date_start<=%s) '
            'AND (item.date_end IS NULL OR item.date_end>=%s)'
            'ORDER BY item.applied_on, item.min_quantity desc, categ.parent_left desc',
            (prod_tmpl_ids, prod_ids, categ_ids, self.id, date, date))

        item_ids = [x[0] for x in self._cr.fetchall()]
        items = self.env['product.pricelist.item'].browse(item_ids)
        results = {}
        for product, qty, partner in products_qty_partner:
            results[product.id] = 0.0
            suitable_rule = False

            # Final unit price is computed according to `qty` in the `qty_uom_id` UoM.
            # An intermediary unit price may be computed according to a different UoM, in
            # which case the price_uom_id contains that UoM.
            # The final price will be converted to match `qty_uom_id`.
            qty_uom_id = self._context.get('uom') or product.uom_id.id
            price_uom_id = product.uom_id.id
            qty_in_product_uom = qty
            if qty_uom_id != product.uom_id.id:
                try:
                    qty_in_product_uom = self.env['product.uom'].browse([self._context['uom']])._compute_quantity(qty, product.uom_id)
                except UserError:
                    # Ignored - incompatible UoM in context, use default product UoM
                    pass

            # if Public user try to access standard price from website sale, need to call price_compute.
            # TDE SURPRISE: product can actually be a template
            price = product.price_compute('list_price')[product.id]
            price_uom = self.env['product.uom'].browse([qty_uom_id])
            for rule in items:
                if rule.min_quantity and qty_in_product_uom < rule.min_quantity:
                    continue
                if is_product_template:
                    if rule.product_tmpl_id and product.id != rule.product_tmpl_id.id:
                        continue
                    if rule.product_id and not (product.product_variant_count == 1 and product.product_variant_id.id == rule.product_id.id):
                        # product rule acceptable on template if has only one variant
                        continue
                else:
                    if rule.product_tmpl_id and product.product_tmpl_id.id != rule.product_tmpl_id.id:
                        continue
                    if rule.product_id and product.id != rule.product_id.id:
                        continue

                if rule.categ_id:
                    cat = product.categ_id
                    while cat:
                        if cat.id == rule.categ_id.id:
                            break
                        cat = cat.parent_id
                    if not cat:
                        continue

                if rule.base == 'pricelist' and rule.base_pricelist_id:
                    price_tmp = rule.base_pricelist_id._compute_price_rule([(product, qty, partner)])[product.id][0]  # TDE: 0 = price, 1 = rule
                    price = rule.base_pricelist_id.currency_id.compute(price_tmp, self.currency_id, round=False)
                else:
                    # if base option is public price take sale price else cost price of product
                    # price_compute returns the price in the context UoM, i.e. qty_uom_id
                    price = product.price_compute(rule.base)[product.id]
                convert_to_price_uom = (lambda price: product.uom_id._compute_price(price, price_uom))
                if price is not False:
                    if order_date:
                        orders_date = datetime.strptime(order_date, "%Y-%m-%d %H:%M:%S")
                        day = []
                        if rule.adjust_day is True:
                            if rule.is_monday is True:
                                day.append(0)
                            if rule.is_tuesday is True:
                                day.append(1)
                            if rule.is_wednesday is True:
                                day.append(2)
                            if rule.is_thursday is True:
                                day.append(3)
                            if rule.is_friday is True:
                                day.append(4)
                            if rule.is_saturday is True:
                                day.append(5)
                            if rule.is_sunday is True:
                                day.append(6)
                            if orders_date.weekday() in day:
                                if rule.adjust_hour:
                                    now_delta = timedelta(hours=orders_date.hour + 8, minutes=orders_date.minute, seconds=orders_date.second)
                                    if now_delta > timedelta(hours=rule.start_hour) and now_delta < timedelta(hours=rule.end_hour):
                                        if rule.compute_price == 'fixed':
                                            price = convert_to_price_uom(rule.fixed_price)
                                        elif rule.compute_price == 'percentage':
                                            price = (price - (price * (rule.percent_price / 100))) or 0.0
                                        else:
                                            # complete formula
                                            price_limit = price
                                            price = (price - (price * (rule.price_discount / 100))) or 0.0
                                            if rule.price_round:
                                                price = tools.float_round(price, precision_rounding=rule.price_round)

                                            if rule.price_surcharge:
                                                price_surcharge = convert_to_price_uom(rule.price_surcharge)
                                                price += price_surcharge

                                            if rule.price_min_margin:
                                                price_min_margin = convert_to_price_uom(rule.price_min_margin)
                                                price = max(price, price_limit + price_min_margin)

                                            if rule.price_max_margin:
                                                price_max_margin = convert_to_price_uom(rule.price_max_margin)
                                                price = min(price, price_limit + price_max_margin)
                                        suitable_rule = rule
                                else:
                                    if rule.compute_price == 'fixed':
                                        price = convert_to_price_uom(rule.fixed_price)
                                    elif rule.compute_price == 'percentage':
                                        price = (price - (price * (rule.percent_price / 100))) or 0.0
                                    else:
                                        # complete formula
                                        price_limit = price
                                        price = (price - (price * (rule.price_discount / 100))) or 0.0
                                        if rule.price_round:
                                            price = tools.float_round(price, precision_rounding=rule.price_round)

                                        if rule.price_surcharge:
                                            price_surcharge = convert_to_price_uom(rule.price_surcharge)
                                            price += price_surcharge

                                        if rule.price_min_margin:
                                            price_min_margin = convert_to_price_uom(rule.price_min_margin)
                                            price = max(price, price_limit + price_min_margin)

                                        if rule.price_max_margin:
                                            price_max_margin = convert_to_price_uom(rule.price_max_margin)
                                            price = min(price, price_limit + price_max_margin)
                                    suitable_rule = rule
                        elif rule.adjust_hour:
                            now_delta = timedelta(hours=orders_date.hour + 8, minutes=orders_date.minute, seconds=orders_date.second)
                            if now_delta > timedelta(hours=rule.start_hour) and now_delta < timedelta(hours=rule.end_hour):
                                if rule.compute_price == 'fixed':
                                    price = convert_to_price_uom(rule.fixed_price)
                                elif rule.compute_price == 'percentage':
                                    price = (price - (price * (rule.percent_price / 100))) or 0.0
                                else:
                                    # complete formula
                                    price_limit = price
                                    price = (price - (price * (rule.price_discount / 100))) or 0.0
                                    if rule.price_round:
                                        price = tools.float_round(price, precision_rounding=rule.price_round)

                                    if rule.price_surcharge:
                                        price_surcharge = convert_to_price_uom(rule.price_surcharge)
                                        price += price_surcharge

                                    if rule.price_min_margin:
                                        price_min_margin = convert_to_price_uom(rule.price_min_margin)
                                        price = max(price, price_limit + price_min_margin)

                                    if rule.price_max_margin:
                                        price_max_margin = convert_to_price_uom(rule.price_max_margin)
                                        price = min(price, price_limit + price_max_margin)
                                suitable_rule = rule
                        else:
                            if rule.compute_price == 'fixed':
                                price = convert_to_price_uom(rule.fixed_price)
                            elif rule.compute_price == 'percentage':
                                price = (price - (price * (rule.percent_price / 100))) or 0.0
                            else:
                                # complete formula
                                price_limit = price
                                price = (price - (price * (rule.price_discount / 100))) or 0.0
                                if rule.price_round:
                                    price = tools.float_round(price, precision_rounding=rule.price_round)

                                if rule.price_surcharge:
                                    price_surcharge = convert_to_price_uom(rule.price_surcharge)
                                    price += price_surcharge

                                if rule.price_min_margin:
                                    price_min_margin = convert_to_price_uom(rule.price_min_margin)
                                    price = max(price, price_limit + price_min_margin)

                                if rule.price_max_margin:
                                    price_max_margin = convert_to_price_uom(rule.price_max_margin)
                                    price = min(price, price_limit + price_max_margin)
                            suitable_rule = rule
                    else:
                        if rule.compute_price == 'fixed':
                            price = convert_to_price_uom(rule.fixed_price)
                        elif rule.compute_price == 'percentage':
                            price = (price - (price * (rule.percent_price / 100))) or 0.0
                        else:
                            # complete formula
                            price_limit = price
                            price = (price - (price * (rule.price_discount / 100))) or 0.0
                            if rule.price_round:
                                price = tools.float_round(price, precision_rounding=rule.price_round)

                            if rule.price_surcharge:
                                price_surcharge = convert_to_price_uom(rule.price_surcharge)
                                price += price_surcharge

                            if rule.price_min_margin:
                                price_min_margin = convert_to_price_uom(rule.price_min_margin)
                                price = max(price, price_limit + price_min_margin)

                            if rule.price_max_margin:
                                price_max_margin = convert_to_price_uom(rule.price_max_margin)
                                price = min(price, price_limit + price_max_margin)
                        suitable_rule = rule
                break
            # Final price conversion into pricelist currency
            if suitable_rule and suitable_rule.compute_price != 'fixed' and suitable_rule.base != 'pricelist':
                price = product.currency_id.compute(price, self.currency_id, round=False)
            results[product.id] = (price, suitable_rule and suitable_rule.id or False)
        return results

    # New methods: product based


class PricelistItem(models.Model):
    _inherit = "product.pricelist.item"

    adjust_day = fields.Boolean(string='Adjust Day', default=False)
    adjust_hour = fields.Boolean(string='Adjust Hour', default=False)
    start_hour = fields.Float(string='Start Hour', default=0)
    end_hour = fields.Float(string='End Hour', default=0)
    is_monday = fields.Boolean(string="Mon", default=False)
    is_tuesday = fields.Boolean(string="Tue", default=False)
    is_wednesday = fields.Boolean(string="Wed", default=False)
    is_thursday = fields.Boolean(string="Thu", default=False)
    is_friday = fields.Boolean(string="Fri", default=False)
    is_saturday = fields.Boolean(string="Sat", default=False)
    is_sunday = fields.Boolean(string="Sun", default=False)

    @api.onchange('adjust_day')
    def _onchange_adjust_day(self):
        for obj in self:
            if obj.adjust_day is False:
                obj.is_monday = False
                obj.is_tuesday = False
                obj.is_wednesday = False
                obj.is_thursday = False
                obj.is_friday = False
                obj.is_saturday = False
                obj.is_sunday = False

    @api.onchange('adjust_hour')
    def _onchange_adjust_hour(self):
        for obj in self:
            if obj.adjust_hour is False:
                obj.start_hour = 0
                obj.end_hour = 0

    @api.onchange('end_hour')
    def _onchange_end_hour(self):
        for obj in self:
            if obj.adjust_hour:
                if obj.start_hour > obj.end_hour or obj.start_hour > 24 or obj.end_hour > 24 or obj.start_hour < 0 or obj.end_hour < 0:
                    raise ValidationError(_("Please set the start and end time right!"))
            if obj.adjust_hour is False:
                obj.start_hour = 0
                obj.end_hour = 0