# -*- coding: utf-8 -*-

from odoo import models
import datetime


class ProductProduct(models.Model):
    _inherit = "product.product"

    def _compute_product_price(self):
        prices = {}
        pricelist_id_or_name = self._context.get('pricelist')
        order_date = self._context.get('date')
        if pricelist_id_or_name:
            pricelist = None
            partner = self._context.get('partner', False)
            quantity = self._context.get('quantity', 1.0)

            # Support context pricelists specified as display_name or ID for compatibility
            if isinstance(pricelist_id_or_name, basestring):
                pricelist_name_search = self.env['product.pricelist'].name_search(pricelist_id_or_name, operator='=', limit=1)
                if pricelist_name_search:
                    pricelist = self.env['product.pricelist'].browse([pricelist_name_search[0][0]])
            elif isinstance(pricelist_id_or_name, (int, long)):
                pricelist = self.env['product.pricelist'].browse(pricelist_id_or_name)
            if order_date is None:
                date_order = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            else:
                date_order = order_date
#             date_order = datetime.strptime(date_order, '%Y-%m-%d %H:%M:%S')
            if pricelist:
                quantities = [quantity] * len(self)
                partners = [partner] * len(self)
                prices = pricelist.get_products_price(self, quantities, partners, order_date=date_order)

        for product in self:
            product.price = prices.get(product.id, 0.0)
