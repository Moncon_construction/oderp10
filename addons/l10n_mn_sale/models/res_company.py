# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class InheritedResUsers(models.Model):
    _inherit = 'res.company'

    mobile_sale_validate = fields.Selection([('partner_location', 'Only confirmed by customer s location'),
                                             ('anywhere', 'It can be guaranteed anywhere')], string='Mobile sale validate')
    check_location_radius = fields.Float('Check location radius')

    sales_team_structure = fields.Selection([
        ('use_single_sales_team', "Partner included just one sales "),
        ('use_multi_sales_team', "Partner can be included many sales team")
    ], "Sales Team Structure", default='use_single_sales_team')

    sale_method = fields.Selection([
        (0, 'On Net Method'),
        (1, 'On Gross Method'),
    ], string="Sale Method", default=0)
