# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api


class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.multi
    def _default_invisible(self):
        for partner in self:
            if self.env.user.company_id.sales_team_structure == 'use_single_sales_team':
                partner.is_check_sales_team_config = True
            else:
                partner.is_check_sales_team_config = False

    @api.multi
    def _get_salesman_value(self):
        for obj in self:
            res_users = self.env['res.users'].search([('partner_id', '=', obj.id)])
            for user in res_users:
                if user.has_group('l10n_mn_sale.sale_sales_man'):
                    obj.is_salesman = True

    @api.multi
    def _compute_is_not_manager(self):
        for obj in self:
            if not (self.env.user.has_group('sales_team.group_sale_manager') or self.env.user.has_group('account.group_account_manager')):
                obj.is_not_manager = True

    @api.multi
    @api.depends('is_limit_loan_sale')
    def _compute_sales_amount(self):
        open_invoices = self.env['account.invoice'].search([('state', '=', 'open')])
        for obj in self:
            if obj.is_limit_loan_sale:
                sum_amount_total = 0.0
                for open_invoice in open_invoices:
                    if open_invoice.user_id.partner_id.id == obj.id:
                        sum_amount_total += open_invoice.residual
                obj.loan_sales_amount = sum_amount_total

    is_limit_loan_sale = fields.Boolean("Is limit loan sale", default=False)
    is_salesman = fields.Boolean("Is salesman", compute='_get_salesman_value', default=False)
    limit_of_credit_sales = fields.Float("Limit of credit sales", default=0.0)
    loan_sales_amount = fields.Float("Loan Sales Amount", compute='_compute_sales_amount', store=True, default=0.0, readonly=1)
    is_not_manager = fields.Boolean("Is not manager", compute='_compute_is_not_manager')
    is_check_sales_team_config = fields.Boolean("Is not single", compute='_default_invisible')
    allowed_sales_teams = fields.Many2many('crm.team', 'partner_sales_team_rel', 'partner_id', 'team_id', string="Allowed Sales Team")

    @api.onchange('user_id')
    def onchange_user_id(self):
        team_ids = self.env['crm.team'].search([])
        if team_ids:
            for line in team_ids:
                if self.user_id in line.member_ids or self.user_id == line.user_id:
                    self.team_id = line.id
