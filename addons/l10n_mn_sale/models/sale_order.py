# -*- coding: utf-8 -*-


from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from pyfcm import FCMNotification
from datetime import datetime, timedelta
from odoo import exceptions
import odoo.addons.decimal_precision as dp
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare


class SaleCategory(models.Model):
    _name = 'sale.category'
    _description = 'Sale Category'

    name = fields.Char('Name', translate=True, required=True)
    description = fields.Html('Description')
    sequence = fields.Integer('Sequence')
    is_loan = fields.Boolean('Is Loan')


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    invoice_policy = fields.Selection(track_visibility='onchange')


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    # Борлуулалтын баг солигдоход түүнд харгалзах шинжилгээний данс борлуулалтын шинжилгээний дансанд сонгогдоно
    @api.onchange('team_id')
    def onchange_team_id(self):
        if self.company_id.cost_center == 'sales_team':
            for order in self:
                order.project_id = order.team_id.account_analytic_id.id
                if order.order_line:
                    for line in order.order_line:
                        line.analytic_account_id = order.team_id.account_analytic_id.id

    @api.depends('state', 'order_line.invoice_status')
    def _compute_check_invoiced(self):
        for sale in self:
            invoice_ids = sale.order_line.mapped('invoice_lines').mapped('invoice_id').filtered(lambda r: r.type in ['out_invoice', 'out_refund'])
            not_paid_inv = []
            for invoice in invoice_ids:
                if invoice.state != 'paid':
                    not_paid_inv.append(invoice.id)
                    break
            if len(invoice_ids) >= 1:
                if not_paid_inv:
                    sale.is_invoiced = False
                    sale.is_not_invoiced = True
                else:
                    sale.is_invoiced = True
                    sale.is_not_invoiced = False

    @api.depends('invoice_ids')
    def _compute_all_prepaid_invoice(self):
        for sale in self:
            if sale.invoice_ids:
                count_is_prepayment = 0
                for invoice in sale.invoice_ids:
                    if invoice.is_prepayment:
                        count_is_prepayment += 1
                if count_is_prepayment == len(sale.invoice_ids):
                    sale.is_all_inv_is_prepayment_checked = True
                else:
                    sale.is_all_inv_is_prepayment_checked = False
            else:
                sale.is_all_inv_is_prepayment_checked = False

    @api.depends('invoice_ids', 'state')
    def _product_refund(self):
        tax_obj = self.env['account.tax']
        for sale in self:
            sale.refund = 0.0
            if sale.procurement_group_id:
                picking_ids = []
                picking_ids = self.env['stock.picking'].search([('name','like','%OUT%'),('group_id', '=', sale.procurement_group_id.id)])
                if picking_ids:
                    origin_move_ids = self.env['stock.move'].search([('picking_id', 'in', picking_ids.ids)])
                    for origin_move_id in origin_move_ids:
                        # Буцаалтын тоо хэмжээг олж байна.
                        refund_qty = 0.0
                        refund_move_ids = self.env['stock.move'].search([('state','=','done'),('origin_returned_move_id', '=', origin_move_id.id)])
                        for refund_move_id in refund_move_ids:
                            refund_qty += refund_move_id.product_uom_qty
                            
                        # Буцаалтын дүнг тооцоолж байна.
                        taxes = False
                        real_price = 0.0
                        sale_line = False
                        if origin_move_id.procurement_id:
                            if origin_move_id.procurement_id.sale_line_id:
                                sale_line = origin_move_id.procurement_id.sale_line_id
                                if sale_line:
                                    # Бодит зарсан үнийг тооцоолж байна.
                                    real_price = sale_line.price_unit * (1 - (sale_line.discount or 0.0) / 100.0)
                                    qty = sale_line.product_uom_qty
                                    if sale_line.tax_id:
                                        taxes = sale_line.tax_id.compute_all(real_price, sale_line.order_id.currency_id, origin_move_id.product_qty, product=sale_line.product_id, partner=sale_line.order_id.partner_shipping_id)
                                    else:
                                        taxes = tax_obj.compute_all(real_price, sale_line.currency_id, origin_move_id.product_qty,
                                                                    sale_line.product_id,
                                                                    sale_line.order_id.partner_shipping_id)
                        if taxes:
                            if taxes['base']:
                                sale.refund += (taxes['base'] / origin_move_id.product_qty) * refund_qty
                            else:
                                if sale_line:
                                    sale.refund += refund_qty * sale_line.price_unit

    sale_category_id = fields.Many2one('sale.category', string='Sale Category', readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]})
    invoice_policy = fields.Selection([
        ('to_product', 'To Product'),
        ('order', 'Order'),
        ('delivery', 'Delivery')], string='Invoice Policy', default='to_product')
    cancel_reason_id = fields.Many2one('sale.order.cancel.reason', 'Cancel Reason')
    project_id = fields.Many2one('account.analytic.account', 'Analytic Account', readonly=True,
                                 states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
                                 help="The analytic account related to a sales order.", copy=False,
                                 domain="[('type','=','normal')]")
    is_invoiced = fields.Boolean(compute='_compute_check_invoiced', string='Is Invoiced', search='_sale_invoiced_search', readonly=True, default=False)
    is_not_invoiced = fields.Boolean(compute='_compute_check_invoiced', string='Is Not Invoiced', search='_sale_not_invoiced_search', readonly=True, default=False)
    refund = fields.Float(compute='_product_refund', string='Refund', digits= dp.get_precision('Account'))
    residual_invoice = fields.Monetary(string='Invoice amount')
    is_all_inv_is_prepayment_checked = fields.Boolean(compute='_compute_all_prepaid_invoice', string='Is all invoice is prepayment checked', search='_all_prepayment_invoiced_search', readonly=True, default=False)
    is_delivered = fields.Boolean(string='Is delivered', default=False, store=True)
    move_assign_date = fields.Datetime(string='Move assign date', default=False)

    @api.multi
    def _sale_invoiced_search(self, operator, value):
        recs = self.search([]).filtered(lambda x: x.is_invoiced)
        if recs:
            return [('id', 'in', [x.id for x in recs])]
        else:
            return [('id', 'in', [])]
        
    @api.multi
    def _sale_not_invoiced_search(self, operator, value):
        recs = self.search([]).filtered(lambda x: not x.is_invoiced)
        if recs:
            return [('id', 'in', [x.id for x in recs])]
        else:
            return [('id', 'in', [])]

    @api.multi
    def _all_prepayment_invoiced_search(self, operator, value):
        recs = self.search([]).filtered(lambda x: x.is_all_inv_is_prepayment_checked)
        if recs:
            return [('id', 'in', [x.id for x in recs])]
        else:
            return [('id', 'in', [])]

    @api.onchange('user_id')
    def onchange_user_id(self):
        team_ids = self.env['crm.team'].search([])
        if team_ids:
            for line in team_ids:
                if self.user_id in line.member_ids or self.user_id == line.user_id:
                    self.team_id = line.id

    @api.multi
    def action_invoice_create(self, grouped=False, final=False):

        for order in self:
            for line in order.order_line:
                if order.invoice_policy == 'order':
                    line.qty_to_invoice = line.product_uom_qty - line.qty_invoiced
                elif order.invoice_policy == "delivery":
                    line.qty_to_invoice = line.qty_delivered - line.qty_invoiced

        invoice = super(SaleOrder, self).action_invoice_create(grouped=False, final=final)

        invoice_obj = self.env['account.invoice'].search([('id', 'in', invoice)])
        for order in self:
            if order.user_id:
                invoice_obj.write({'user_id': order.user_id.id})
        
        if final:
            for order in self:
                if order.invoice_ids:
                    downpayment_invoices = []
                    for inv in order.invoice_ids:
                        if inv.id != invoice_obj.id and inv.is_prepayment and inv.state=='paid':
                            downpayment_invoices.append(inv)
                    invoice_obj.downpayment_invoice_ids = [i.id for i in downpayment_invoices]
                
        return invoice

    @api.multi
    def action_cancel(self):
        for selfer in self:
            # Борлуулалтын тохиргооноос хамааран захиалга цуцлах шалтгаан сонгох wizard гардаг болгосон.
            if self.env['ir.values'].get_default('sale.config.settings', 'sale_cancel_reason_setting'):
                view = self.env.ref('l10n_mn_sale.view_select_sale_order_cancel_reason')
                wiz = self.env['select.sale.order.cancel.reason'].create({'order_id': selfer.id})
                return {
                    'name': _('Select sale order'),
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'select.sale.order.cancel.reason',
                    'views': [(view.id, 'form')],
                    'view_id': view.id,
                    'target': 'new',
                    'res_id': wiz.id,
                    'context': self.env.context,
                }
            self._action_cancel()

    @api.multi
    def _action_cancel(self):
        for selfer in self:
            if selfer.invoice_ids:
                if len(selfer.invoice_ids) == 1:
                    invoice = selfer.invoice_ids[0]
                    if invoice.state == 'open' or invoice.state == 'draft':
                        invoice.action_invoice_cancel()
                    elif invoice.state == 'paid':
                        raise UserError(_('Order can not be canceled because invoice paid!'))
                else:
                    for invoice in selfer.invoice_ids:
                        if invoice.state == 'open' or invoice.state == 'draft':
                            invoice.action_invoice_cancel()
            if selfer.picking_ids:
                self.mapped('order_line').mapped('procurement_ids').cancel()
                if len(selfer.picking_ids) == 1:
                    picking = selfer.picking_ids[0]
                    if picking.state != 'done':
                        for move in picking.move_lines:
                            move.write({'state': 'cancel'})
                        picking.write({'state': 'cancel'})
                    elif picking.state == 'done':
                        raise UserError(_('Order can not be canceled because picking is done!'))
                else:
                    for picking in selfer.picking_ids:
                        if picking.state != 'done':
                            for move in picking.move_lines:
                                move.write({'state': 'cancel'})
                            picking.write({'state': 'cancel'})
                        if picking.state == 'done':
                            raise UserError(_('Order can not be canceled because picking is done!'))
            selfer.write({'state': 'cancel'})

    @api.constrains('product_uom_qty')
    def _check_product_uom_qty_value(self):
        default_deposit_product = self.env['ir.values'].get_default('sale.config.settings', 'deposit_product_id_setting', company_id=self.env.user.company_id.id)
        for rec in self:
            # Урьдчилгаа төлбөрийн бараа биш бол 0-с бага байж болохгүй.
            if rec.product_id.id != default_deposit_product and rec.product_uom_qty <= 0:
                raise ValidationError(_('Error!\nProduct quantity must be greater than Zero.'))

    @api.constrains('name')
    def _check_order_reference(self):
        # Захиалгын дугаар давхардах үед гарах алдааны мсж
        for sale_order in self:
            if sale_order.name:
                other_orders = self.env['sale.order'].search([
                    ('name', '=', sale_order.name),
                    ('id', '!=', sale_order.id)])
            for other_order in other_orders:
                if other_order:
                    exception = _('Sale order duplicated: ') + other_order.name
                    raise exceptions.ValidationError(exception)
    
    @api.multi            
    def action_cancel_lock(self):
        self.ensure_one()
        return self.write({'state': 'sale'})

    @api.depends('state', 'order_line.invoice_status')
    def _get_invoiced(self):
        res = super(SaleOrder, self)._get_invoiced()
        for obj in self:
            if obj.invoice_ids:
                total = 0
                for invoice in obj.invoice_ids:
                    if invoice.state == 'open' or invoice.state == 'paid':
                        total += invoice.amount_total
                obj.residual_invoice = total
        return res


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    @api.multi
    def _is_edit_def(self):
        has_group = self.env.user.has_group('l10n_mn_sale.group_edit_price_unit')
        return True if has_group else False

    @api.multi
    def _is_edit(self):
        for obj in self:
            has_group = self.env.user.has_group('l10n_mn_sale.group_edit_price_unit')
            obj.is_edit = True if has_group else False

    is_edit = fields.Boolean('Is Edit', compute=_is_edit)
    is_edit_def = fields.Boolean('Is Edit def', default=_is_edit_def)
    price_unit_values = fields.Float(string='Price Unit Values', digits=dp.get_precision('Product Price'),
                                     required=False)

    @api.onchange('price_unit')
    def _onchange_price_unit(self):
        for rec in self:
            if rec.price_unit:
                rec.price_unit_values = rec.price_unit

    @api.multi
    def write(self, vals):
        if 'price_unit_values' in vals:
            vals['price_unit'] = vals['price_unit_values']
        return super(SaleOrderLine, self).write(vals)

    @api.model
    def create(self, vals):
        if 'price_unit_values' in vals:
            vals['price_unit'] = vals['price_unit_values']
        if 'is_edit_def' in vals:
            vals['is_edit_def'] = False
        return super(SaleOrderLine, self).create(vals)

    @api.multi
    def _prepare_invoice_line(self, qty):
        self.ensure_one()
        res = super(SaleOrderLine, self)._prepare_invoice_line(qty)
        # Хэрвээ борлуулалтын мөр дээр шинжилгээний данс сонгогдсон байвал тухайн шинжилгээний дансыг нэхэмжлэхийн мөр дээр авна.
        if self.analytic_account_id:
            res.update({'account_analytic_id': self.analytic_account_id.id})
        return res

    @api.multi
    def _get_display_price(self, product):
        # TO DO: move me in master/saas-16 on sale.order
        if self.order_id.pricelist_id.discount_policy == 'with_discount':
            return product.with_context(pricelist=self.order_id.pricelist_id.id,
                                        date=self.order_id.date_order).price
        product_context = dict(self.env.context, partner_id=self.order_id.partner_id.id, date=self.order_id.date_order, uom=self.product_uom.id)
        final_price, rule_id = self.order_id.pricelist_id.with_context(product_context).get_product_price_rule(self.product_id, self.product_uom_qty or 1.0, self.order_id.partner_id, order_date=self.order_id.date_order)
        base_price, currency_id = self.with_context(product_context)._get_real_price_currency(product, rule_id, self.product_uom_qty, self.product_uom, self.order_id.pricelist_id.id)
        if currency_id != self.order_id.pricelist_id.currency_id.id:
            base_price = self.env['res.currency'].browse(currency_id).with_context(product_context).compute(base_price, self.order_id.pricelist_id.currency_id)
        # negative discounts (= surcharge) are included in the display price
        return max(base_price, final_price)

    @api.onchange('product_id', 'price_unit', 'product_uom', 'product_uom_qty', 'tax_id')
    def _onchange_discount(self):
        self.discount = 0.0
        if not (self.product_id and self.product_uom and
                self.order_id.partner_id and self.order_id.pricelist_id and
                self.order_id.pricelist_id.discount_policy == 'without_discount' and
                self.env.user.has_group('sale.group_discount_per_so_line')):
            return

        product = self.product_id.with_context(
            lang=self.order_id.partner_id.lang,
            partner=self.order_id.partner_id.id,
            quantity=self.product_uom_qty,
            date=self.order_id.date_order,
            pricelist=self.order_id.pricelist_id.id,
            uom=self.product_uom.id,
            fiscal_position=self.env.context.get('fiscal_position')
        )

        product_context = dict(self.env.context, partner_id=self.order_id.partner_id.id, date=self.order_id.date_order, uom=self.product_uom.id)

        price, rule_id = self.order_id.pricelist_id.with_context(product_context).get_product_price_rule(self.product_id, self.product_uom_qty or 1.0, self.order_id.partner_id, order_date=self.order_id.date_order)
        new_list_price, currency_id = self.with_context(product_context)._get_real_price_currency(product, rule_id, self.product_uom_qty, self.product_uom, self.order_id.pricelist_id.id)

        if new_list_price != 0:
            if self.order_id.pricelist_id.currency_id.id != currency_id:
                # we need new_list_price in the same currency as price, which is in the SO's pricelist's currency
                new_list_price = self.env['res.currency'].browse(currency_id).with_context(product_context).compute(new_list_price, self.order_id.pricelist_id.currency_id)
            discount = (new_list_price - price) / new_list_price * 100
            if discount > 0:
                self.discount = discount



    @api.onchange('product_uom_qty', 'product_uom', 'route_id')
    def _onchange_product_id_check_availability(self):
        if not self.product_id or not self.product_uom_qty or not self.product_uom:
            self.product_packaging = False
            return {}
        if self.product_id.type == 'product':
            precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
            product_qty = self.product_uom._compute_quantity(self.product_uom_qty, self.product_id.uom_id)
            if self.company_id.availability_compute_method == 'stock_move':
                for obj in self:
                    warehouse = obj.order_id.warehouse_id
                    if not warehouse:
                        raise UserError(_('Warning!\nYou must select supply warehouse before add order line!'))
                    if obj.product_id:
                        locations = self.env['stock.location'].search(
                            [('usage', '=', 'internal'), ('location_id', 'child_of', [warehouse.view_location_id.id])])
                        loc_ids = [loc.id for loc in locations or []]
                        available_qty = obj.product_id.get_qty_availability(loc_ids, obj.order_id.date_order, state=['done','assigned'])
                        if product_qty > available_qty:
                            warning_mess = {
                                'title': _('Not enough inventory!'),
                                'message': _('You plan to sell %s %s but you only have %s %s available!') % \
                                           (self.product_uom_qty, self.product_uom.name,
                                            available_qty,
                                            self.product_id.uom_id.name)
                            }
                            return {'warning': warning_mess}
            else:
                if float_compare(self.product_id.virtual_available, product_qty, precision_digits=precision) == -1:
                    is_available = self._check_routing()
                    if not is_available:
                        warning_mess = {
                            'title': _('Not enough inventory!'),
                            'message': _('You plan to sell %s %s but you only have %s %s available!\nThe stock on hand is %s %s.') % \
                                       (self.product_uom_qty, self.product_uom.name, self.product_id.virtual_available,
                                        self.product_id.uom_id.name, self.product_id.qty_available,
                                        self.product_id.uom_id.name)
                        }
                        return {'warning': warning_mess}
        return {}
    
    def get_account_for_invoice_line(self):
        # Нэхэмжлэлийн мөрийн данс бэлдэх /Бараа, түүний ангилалаас хайх/
        self.ensure_one()
        account = self.product_id.property_account_income_id or self.product_id.categ_id.property_account_income_categ_id
        if not account:
            raise UserError(_('Please define income account for this product: "%s" (id:%d) - or for its category: "%s".') %
                (self.product_id.name, self.product_id.id, self.product_id.categ_id.name))
        return account
        
    @api.multi
    def _prepare_invoice_line(self, qty):
        """
            @Override: Данс авах хэсгийг дахин тодорхойлохоор бүтнээр нь дарав.
            @todo: Core дахь event_sale, account_analytic_default модулиуд дээр удамшуулж ашигласан байсан.
                1. event_sale: name-г өөрчилж дамжуулсан байсан. Энэ модулийг ашиглах бол тэр үед нь засаад энэ комментийг авна уу.
                2. account_analytic_default: account_analytic_id-г дамжуулсан байсан. l10n_mn_analytic_balance_sale модуль дээр тодорхойлсон тул энийг чухал биш гэж үзээд дарав. Хэрэгтэй болох үед засах.
            
            @Note: Prepare the dict of values to create the new invoice line for a sales order line.
            :param qty: float quantity to invoice
        """
        self.ensure_one()
        res = {}
        ######### START CHANGE: Данс авах хэсгийг дахин тодорхойлохоор бүтнээр нь дарав. ##########
        account = self.get_account_for_invoice_line()
        ######### END CHANGE: Данс авах хэсгийг дахин тодорхойлохоор бүтнээр нь дарав. ##########
        fpos = self.order_id.fiscal_position_id or self.order_id.partner_id.property_account_position_id
        if fpos:
            account = fpos.map_account(account)

        res = {
            'name': self.name,
            'sequence': self.sequence,
            'origin': self.order_id.name,
            'account_id': account.id,
            'price_unit': self.price_unit,
            'quantity': qty,
            'discount': self.discount,
            'uom_id': self.product_uom.id,
            'product_id': self.product_id.id or False,
            'layout_category_id': self.layout_category_id and self.layout_category_id.id or False,
            'invoice_line_tax_ids': [(6, 0, self.tax_id.ids)],
            'account_analytic_id': self.order_id.project_id.id,
            'analytic_tag_ids': [(6, 0, self.analytic_tag_ids.ids)],
        }
        return res
