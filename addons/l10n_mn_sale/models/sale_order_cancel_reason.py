# -*- coding: utf-8 -*-

from odoo import fields, models


class SaleOrderCancelReason(models.Model):
    _name = 'sale.order.cancel.reason'

    name = fields.Char("Name", required=True)
    code = fields.Char("Code", required=True)
