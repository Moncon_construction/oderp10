# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
import logging
import ast
from math import sin, cos, sqrt, atan2, radians

_logger = logging.getLogger(__name__)


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def action_confirm_mobile(self, latitude, longitude):
        for order in self:
            return_value = 0
            if self.env.user.company_id.mobile_sale_validate == 'partner_location':
                # approximate radius of earth in km
                R = 6373.0

                lat1 = radians(round(latitude, 6))
                lon1 = radians(round(longitude, 6))
                lon2 = radians(round(order.partner_id.partner_longitude, 6))
                lat2 = radians(round(order.partner_id.partner_latitude, 6))

                dlon = lon2 - lon1
                dlat = lat2 - lat1

                a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
                c = 2 * atan2(sqrt(a), sqrt(1 - a))

                distance = R * c  # km
                if distance > self.env.user.company_id.check_location_radius:
                    return -2  # Борлуулалтын үнийн саналыг батлах боломжгүй. Харилцагчийн байршилд очиж борлуулалтын үнийн саналыг батална уу.

            if order.user_id.partner_id.is_limit_loan_sale:
                limit = order.user_id.partner_id.limit_of_credit_sales
                loan_amount = order.user_id.partner_id.loan_sales_amount
                amount_total = order.amount_total
                check_value = limit - loan_amount - amount_total if limit != 0 else 0
                if check_value < 0:
                    return 1  # Лимит хэтэрсэн тул борлуулалтын захиалга батлахыг хориглох
                limit_check_value = (loan_amount + amount_total) / limit if limit != 0 else 0
                if limit_check_value >= 0.8:
                    return_value = 2  # Лимит 80% хүрсэн тул анхааруулга өгнө
                order.user_id.partner_id._compute_sales_amount()
                order.action_confirm()
                return return_value  # Шууд батлах
            order.action_confirm()
            return 0
        return -1  # Ямар ч үнийн санал батлагдахгүй

    @api.multi
    def get_sale_order_list_mobile(self, date_limit, not_synced_ids):
        """
            Борлуулалтын захиалгыг татах мобайл функц
        """
        not_synced_ids = ast.literal_eval(not_synced_ids)
        result = []
        if len(not_synced_ids) == 0:
            not_synced_ids.append(0)
        self.env.cr.execute("""SELECT so.id, so.name, so.partner_id, rp.name AS partner_name, so.date_order,
                                    so.requested_date, so.validity_date, so.confirmation_date, so.state,
                                    so.payment_term_id, so.user_id, so.amount_total, so.amount_untaxed, so.amount_tax,
                                    so.sale_category_id, so.pricelist_id, rc.id AS currency_id,
                                    rc.symbol AS currency_symbol, so.warehouse_id,
                                    so.invoice_policy, so.project_id, so.team_id
                                    FROM sale_order so
                                    LEFT JOIN res_partner rp ON so.partner_id = rp.id
                                    LEFT JOIN product_pricelist ppl ON so.pricelist_id = ppl.id
                                    LEFT JOIN res_currency rc ON ppl.currency_id = rc.id
                                    WHERE so.date_order > '%s' AND so.user_id = %s AND so.team_id = %s
                                    AND so.id NOT IN (%s)""" %
                            (date_limit, self.env.user.id, self.env.user.sale_team_id.id,
                             ','.join(str(a) for a in tuple(not_synced_ids))))
        sale_order_objects = self.env.cr.dictfetchall()
        for sale_order in sale_order_objects:
            self.env.cr.execute("""SELECT id, order_id, product_id, name, product_uom_qty, price_unit, price_tax,
                    price_subtotal, price_total, product_uom
                    FROM sale_order_line
                    WHERE order_id = %s""" % (sale_order['id']))
            order_line = self.env.cr.dictfetchall()
            dic = {'id': sale_order['id'], 'name': sale_order['name'], 'partner_id': sale_order['partner_id'],
                   'partner_name': sale_order['partner_name'], 'date_order': sale_order['date_order'],
                   'requested_date': sale_order['requested_date'], 'validity_date': sale_order['validity_date'],
                   'confirmation_date': sale_order['confirmation_date'], 'state': sale_order['state'],
                   'payment_term_id': sale_order['payment_term_id'], 'user_id': sale_order['user_id'],
                   'amount_total': sale_order['amount_total'], 'amount_untaxed': sale_order['amount_untaxed'],
                   'amount_tax': sale_order['amount_tax'], 'sale_category_id': sale_order['sale_category_id'],
                   'pricelist_id': sale_order['pricelist_id'], 'currency_id': sale_order['currency_id'],
                   'currency_symbol': sale_order['currency_symbol'], 'warehouse_id': sale_order['warehouse_id'],
                   'invoice_policy': sale_order['invoice_policy'], 'order_line': order_line, 
                   'project_id': sale_order['project_id'], 'team_id': sale_order['team_id']}
            result.append(dic)
        return result


class IrSequence(models.Model):
    _inherit = "ir.sequence"
    _order = 'name'

    @api.model
    def next_by_code_mobile(self, sequence_code, row_size):
        self.check_access_rights('read')
        company_ids = self.env['res.company'].search([]).ids + [False]
        seq_ids = self.search(['&', ('code', '=', sequence_code), ('company_id', 'in', company_ids)])
        if not seq_ids:
            _logger.debug("No ir.sequence has been found for code '%s'. Please make sure a sequence is set for current company." % sequence_code)
            return False
        force_company = self._context.get('force_company')
        if not force_company:
            force_company = self.env.user.company_id.id
        preferred_sequences = [s for s in seq_ids if s.company_id and s.company_id.id == force_company]
        seq_id = preferred_sequences[0] if preferred_sequences else seq_ids[0]
        code_collection = []
        for x in xrange(row_size):
            code_collection.append(seq_id._next())
        return code_collection


class ResUsers(models.Model):
    _inherit = 'res.users'

    @api.multi
    def get_sale_user_role_mobile(self):
        """
            Групийн дагуу нэвтэрсэн хэрэглэгч Борлуулалтанд хандах эрхтэй эсэх, үүргийг тодорхойлно
        """
        user_groups_ids = self.env.user.groups_id.ids
        group_sale_salesman = self.env.ref('sales_team.group_sale_salesman').id
        group_sale_salesman_all_leads = self.env.ref('sales_team.group_sale_salesman_all_leads').id
        group_sale_manager = self.env.ref('sales_team.group_sale_manager').id
        if group_sale_salesman in user_groups_ids or group_sale_salesman_all_leads in user_groups_ids or group_sale_manager in user_groups_ids:
            group_id_manager = self.env.ref('l10n_mn_sale.sale_sales_man').id
            group_id_user = self.env.ref('l10n_mn_sale.sale_delivery_worker').id
            if group_id_manager in user_groups_ids:
                if group_id_user in user_groups_ids:
                    return "both"
                return "sale_sales_man"
            if group_id_user in user_groups_ids:
                return "sale_delivery_worker"
            return ""
        else:
            return ""

    @api.multi
    def get_geoengine_user_role_mobile(self):
        """
            Групийн дагуу харилцагчын байршил тогтоох эрхтэй эсэх, үүргийг тодорхойлно.
        """
        user_groups_ids = self.env.user.groups_id.ids
        geoengine_user = self.env.ref('base_geoengine.group_geoengine_user').id
        geoengine_manager = self.env.ref('base_geoengine.group_geoengine_admin').id
        if geoengine_manager in user_groups_ids or geoengine_user in user_groups_ids:
            if geoengine_manager in user_groups_ids:
                return "geoengine_manager"
            elif geoengine_user in user_groups_ids:
                return "geoengine_user"
        else:
            return "False"

    @api.multi
    def get_sale_document_user_role_mobile(self):
        """
            Групийн дагуу нэвтэрсэн Борлуулалтын баримтанд хандах эрхтэй эсэх, үүргийг тодорхойлно.
        """
        user_group_ids = self.env.user.groups_id.ids
        group_sale_salesman = self.env.ref('sales_team.group_sale_salesman').id
        group_sale_salesman_all_leads = self.env.ref('sales_team.group_sale_salesman_all_leads').id
        group_sale_manager = self.env.ref('sales_team.group_sale_manager').id
        if group_sale_manager in user_group_ids:
            return "group_sale_manager"
        elif group_sale_salesman_all_leads in user_group_ids:
            return "group_sale_salesman_all_leads"
        elif group_sale_salesman in user_group_ids:
            return "group_sale_salesman"
        else:
            return "False"

    @api.multi
    def get_create_contact_access_mobile(self):
        """
            Мобайл хэрэглэгч шинээр харилцагч үүсгэх, засах эрхтэй эсэхийг шалгах функц
        """
        user_groups_ids = self.env.user.groups_id.ids
        group_create_contact = self.env.ref('base.group_partner_manager').id
        if group_create_contact in user_groups_ids:
            return True
        else:
            return False


class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.multi
    def check_module_installed_mobile(self, module_name):
        self.env.cr.execute("select name, state from ir_module_module where name = '%s'" % module_name)
        lines = self.env.cr.dictfetchall()
        for line in lines:
            if line['state'] != 'installed':
                raise False

    @api.multi
    def get_res_partner_list(self, local_partners):
        """
            Энэ функцийг Мобайл FORCE UPDATE дуустал энэ чигээр ашиглана
        """
        update_partner = []
        insert_partner = []
        local_ids = []
        result = []
        write_date = ''
        partner_latitude = 0
        partner_longitude = 0
        # Модуль суусан эсэхийг шалгах
        check_module = self.check_module_installed_mobile('base_geolocalize')
        for partner in local_partners:
            local_ids.append(partner.get('id'))
            if not partner.get('write_date'):
                write_date = '2000-01-01 00:00:00.999999'
            else:
                write_date = partner.get('write_date') + '.999999'
            res_partner = self.env['res.partner'].search([('id', '=', partner.get('id')), ('write_date', '>', write_date)])
            # Харилцагчийн мэдээлэл өөрчлөгдсөн байвал update хийнэ.
            if check_module:
                partner_latitude = res_partner.partner_latitude
                partner_longitude = res_partner.partner_longitude
            if res_partner:
                update_partner.append({'id': res_partner.id, 'name': res_partner.name,
                                       'is_company': res_partner.is_company,
                                       'street': res_partner.street, 'street2': res_partner.street2,
                                       'city': res_partner.city, 'zip': res_partner.zip,
                                       'website': res_partner.website, 'phone': res_partner.phone,
                                       'mobile': res_partner.mobile, 'email': res_partner.email,
                                       'company_id': res_partner.company_id.id, 'parent_id': res_partner.parent_id.id,
                                       'state_id': res_partner.state_id.id, 'country_id': res_partner.country_id.id,
                                       'customer': res_partner.customer, 'supplier': res_partner.supplier,
                                       'company_name': res_partner.company_name, 'property_product_price_list': res_partner.property_product_pricelist.id,
                                       'property_payment_term_id': res_partner.property_payment_term_id.id, 'property_product_pricelist': res_partner.property_product_pricelist.id,
                                       'limit_of_credit_sales': res_partner.limit_of_credit_sales, 'loan_sales_amount': res_partner.loan_sales_amount,
                                       'is_limit_loan_sale': res_partner.is_limit_loan_sale, 'team_id': res_partner.team_id.id,
                                       'write_date': res_partner.write_date,
                                       'partner_latitude': partner_latitude,
                                       'partner_longitude': partner_longitude})

        if self.env.user.sale_team_id.id:
            # Шинээр тохируулагдсан харилцагчдыг дуудах
            if len(local_partners) > 0:
                if not self.env.user.company_id.sales_team_structure == 'use_single_sales_team':
                    insert_partners = self.env['res.partner'].search([('id', 'not in', tuple(local_ids)),
                                                                      ('customer', '=', True),
                                                                      ('active', '=', True),
                                                                      '|',
                                                                      ('allowed_sales_teams', '=', self.env.user.sale_team_id.id),
                                                                      ('team_id', '=', self.env.user.sale_team_id.id)
                                                                      ])
                else:
                    insert_partners = self.env['res.partner'].search([('id', 'not in', tuple(local_ids)),
                                                                      ('customer', '=', True),
                                                                      ('active', '=', True),
                                                                      ('team_id', '=', self.env.user.sale_team_id.id)
                                                                      ])

                for partner in insert_partners:

                    if check_module:
                        partner_latitude = partner.partner_latitude
                        partner_longitude = partner.partner_longitude
                    insert_partner.append({'id': partner.id, 'name': partner.name,
                                           'is_company': partner.is_company,
                                           'street': partner.street, 'street2': partner.street2,
                                           'city': partner.city, 'zip': partner.zip,
                                           'website': partner.website, 'phone': partner.phone,
                                           'mobile': partner.mobile or '', 'email': partner.email,
                                           'company_id': partner.company_id.id, 'parent_id': partner.parent_id.id,
                                           'state_id': partner.state_id.id, 'country_id': partner.country_id.id,
                                           'customer': partner.customer, 'supplier': partner.supplier,
                                           'company_name': partner.company_name,
                                           'property_payment_term_id': partner.property_payment_term_id.id,
                                           'property_product_pricelist': partner.property_product_pricelist.id,
                                           'limit_of_credit_sales': partner.limit_of_credit_sales,
                                           'loan_sales_amount': partner.loan_sales_amount,
                                           'is_limit_loan_sale': partner.is_limit_loan_sale,
                                           'team_id': partner.team_id.id,
                                           'write_date': partner.write_date,
                                           'partner_latitude': partner_latitude,
                                           'partner_longitude': partner_longitude})
            else:
                if not self.env.user.company_id.sales_team_structure == 'use_single_sales_team':
                    insert_partners = self.env['res.partner'].search(
                        ['|', ('allowed_sales_teams', '=', res_partner.team_id.id),
                         ('team_id', '=', res_partner.team_id.id),
                         ('customer', '=', True),
                         ('active', '=', True)])
                else:
                    insert_partners = self.env['res.partner'].search([('customer', '=', True),
                                                                      ('active', '=', True),
                                                                      ('team_id', '=', self.env.user.sale_team_id.id)
                                                                      ])

                for partner in insert_partners:
                    if check_module:
                        partner_latitude = partner.partner_latitude
                        partner_longitude = partner.partner_longitude

                    insert_partner.append({'id': partner.id, 'name': partner.name,
                                           'is_company': partner.is_company,
                                           'street': partner.street, 'street2': partner.street2,
                                           'city': partner.city, 'zip': partner.zip,
                                           'website': partner.website, 'phone': partner.phone,
                                           'mobile': partner.mobile or '', 'email': partner.email,
                                           'company_id': partner.company_id.id, 'parent_id': partner.parent_id.id,
                                           'state_id': partner.state_id.id, 'country_id': partner.country_id.id,
                                           'customer': partner.customer, 'supplier': partner.supplier,
                                           'company_name': partner.company_name,
                                           'property_payment_term_id': partner.property_payment_term_id.id,
                                           'property_product_pricelist': partner.property_product_pricelist.id,
                                           'limit_of_credit_sales': partner.limit_of_credit_sales,
                                           'loan_sales_amount': partner.loan_sales_amount,
                                           'is_limit_loan_sale': partner.is_limit_loan_sale,
                                           'team_id': partner.team_id.id,
                                           'write_date': partner.write_date,
                                           'partner_latitude': partner_latitude,
                                           'partner_longitude': partner_longitude})

        result.append({'update': update_partner})
        result.append({'insert': insert_partner})
        return result

    @api.multi
    def get_res_partner_list_sale_mobile(self, local_partners):
        """
        Мобайл төхөөрөмжөөс ResPartner-ийн жагсаалт Борлуулалтын багаар татахад дуудагдах функц Борлуулалтын програмаас
        :param local_partners: Мобайл төхөөрөмжид байгаа харилцагчийн жагсаалтаас id,write_date
        утгууд дамжиж орж ирнэ
        :return: JSON бүтэцтэй харилцагчийн мэдээлэл буцаана
        """
        update_partner = []
        insert_partner = []
        local_ids = []
        result = []
        partner_latitude = 0
        partner_longitude = 0
        # Модуль суусан эсэхийг шалгах
        check_module = self.check_module_installed_mobile('base_geolocalize')
        for partner in local_partners:
            local_ids.append(partner.get('id'))
            if not partner.get('write_date'):
                write_date = '2000-01-01 00:00:00.999999'
            else:
                write_date = partner.get('write_date') + '.999999'
            res_partner = self.env['res.partner'].search(
                [('id', '=', partner.get('id')), ('write_date', '>', write_date)])
            # Харилцагчийн мэдээлэл өөрчлөгдсөн байвал update хийнэ.
            if check_module:
                partner_latitude = res_partner.partner_latitude
                partner_longitude = res_partner.partner_longitude
            if res_partner:
                update_partner.append({'id': res_partner.id,
                                       'name': res_partner.name,
                                       'is_company': res_partner.is_company,
                                       'street': res_partner.street,
                                       'street2': res_partner.street2,
                                       'city': res_partner.city,
                                       'zip': res_partner.zip,
                                       'website': res_partner.website,
                                       'phone': res_partner.phone,
                                       'mobile': res_partner.mobile,
                                       'email': res_partner.email,
                                       'company_id': res_partner.company_id.id,
                                       'parent_id': res_partner.parent_id.id,
                                       'state_id': res_partner.state_id.id,
                                       'country_id': res_partner.country_id.id,
                                       'customer': res_partner.customer,
                                       'supplier': res_partner.supplier,
                                       'company_name': res_partner.company_name,
                                       'property_product_price_list': res_partner.property_product_pricelist.id,
                                       'property_payment_term_id': res_partner.property_payment_term_id.id,
                                       'property_product_pricelist': res_partner.property_product_pricelist.id,
                                       'limit_of_credit_sales': res_partner.limit_of_credit_sales,
                                       'loan_sales_amount': res_partner.loan_sales_amount,
                                       'is_limit_loan_sale': res_partner.is_limit_loan_sale,
                                       'team_id': res_partner.team_id.id,
                                       'write_date': res_partner.write_date,
                                       'partner_latitude': partner_latitude,
                                       'partner_longitude': partner_longitude})

        if self.env.user.sale_team_id.id:
            # Мобайл төхөөрөмжид огт Sync хийгдээгүй шинээр INSERT хийх харилцагчийн жагсаалтыг үүсгэж байна
            if len(local_partners) > 0:
                if not self.env.user.company_id.sales_team_structure == 'use_single_sales_team':
                    # Борлуулалтын багийн хэлбэр - Харилцагч нь олон борлуулалтын багт харьяалагдах тохиргоотой үед.
                    insert_partners = self.env['res.partner'].search([('id', 'not in', tuple(local_ids)),
                                                                      ('customer', '=', True),
                                                                      ('active', '=', True),
                                                                      '|',
                                                                      ('allowed_sales_teams', '=',
                                                                       self.env.user.sale_team_id.id),
                                                                      ('team_id', '=', self.env.user.sale_team_id.id)
                                                                      ])
                else:
                    # Борлуулалтын багийн хэлбэр - Харилцагч нь зөвхөн 1 борлуулалтын багт харьяалагдах тохиргоотой үед.
                    insert_partners = self.env['res.partner'].search([('id', 'not in', tuple(local_ids)),
                                                                      ('customer', '=', True),
                                                                      ('active', '=', True),
                                                                      ('team_id', '=', self.env.user.sale_team_id.id)
                                                                      ])

                for partner in insert_partners:
                    if check_module:
                        partner_latitude = partner.partner_latitude
                        partner_longitude = partner.partner_longitude
                    insert_partner.append({'id': partner.id,
                                           'name': partner.name,
                                           'is_company': partner.is_company,
                                           'street': partner.street,
                                           'street2': partner.street2,
                                           'city': partner.city,
                                           'zip': partner.zip,
                                           'website': partner.website,
                                           'phone': partner.phone,
                                           'mobile': partner.mobile or '',
                                           'email': partner.email,
                                           'company_id': partner.company_id.id,
                                           'parent_id': partner.parent_id.id,
                                           'state_id': partner.state_id.id,
                                           'country_id': partner.country_id.id,
                                           'customer': partner.customer,
                                           'supplier': partner.supplier,
                                           'company_name': partner.company_name,
                                           'property_payment_term_id': partner.property_payment_term_id.id,
                                           'property_product_pricelist': partner.property_product_pricelist.id,
                                           'limit_of_credit_sales': partner.limit_of_credit_sales,
                                           'loan_sales_amount': partner.loan_sales_amount,
                                           'is_limit_loan_sale': partner.is_limit_loan_sale,
                                           'team_id': partner.team_id.id,
                                           'write_date': partner.write_date,
                                           'partner_latitude': partner_latitude,
                                           'partner_longitude': partner_longitude})
            else:
                if not self.env.user.company_id.sales_team_structure == 'use_single_sales_team':
                    # Борлуулалтын багийн хэлбэр - Харилцагч нь олон борлуулалтын багт харьяалагдах тохиргоотой үед.
                    insert_partners = self.env['res.partner'].search(
                        ['|', ('allowed_sales_teams', '=', res_partner.team_id.id),
                         ('team_id', '=', res_partner.team_id.id),
                         ('customer', '=', True),
                         ('active', '=', True)])
                else:
                    # Борлуулалтын багийн хэлбэр - Харилцагч нь зөвхөн 1 борлуулалтын багт харьяалагдах тохиргоотой үед.
                    insert_partners = self.env['res.partner'].search([('customer', '=', True),
                                                                      ('active', '=', True),
                                                                      ('team_id', '=', self.env.user.sale_team_id.id)
                                                                      ])

                for partner in insert_partners:
                    if check_module:
                        partner_latitude = partner.partner_latitude
                        partner_longitude = partner.partner_longitude

                    insert_partner.append({'id': partner.id,
                                           'name': partner.name,
                                           'is_company': partner.is_company,
                                           'street': partner.street,
                                           'street2': partner.street2,
                                           'city': partner.city,
                                           'zip': partner.zip,
                                           'website': partner.website,
                                           'phone': partner.phone,
                                           'mobile': partner.mobile or '',
                                           'email': partner.email,
                                           'company_id': partner.company_id.id,
                                           'parent_id': partner.parent_id.id,
                                           'state_id': partner.state_id.id,
                                           'country_id': partner.country_id.id,
                                           'customer': partner.customer,
                                           'supplier': partner.supplier,
                                           'company_name': partner.company_name,
                                           'property_payment_term_id': partner.property_payment_term_id.id,
                                           'property_product_pricelist': partner.property_product_pricelist.id,
                                           'limit_of_credit_sales': partner.limit_of_credit_sales,
                                           'loan_sales_amount': partner.loan_sales_amount,
                                           'is_limit_loan_sale': partner.is_limit_loan_sale,
                                           'team_id': partner.team_id.id,
                                           'write_date': partner.write_date,
                                           'partner_latitude': partner_latitude,
                                           'partner_longitude': partner_longitude})

        result.append({'update': update_partner})
        result.append({'insert': insert_partner})
        return result
