# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
from odoo.exceptions import UserError


class CrmTeam(models.Model):
    _inherit = 'crm.team'

    account_analytic_id = fields.Many2one('account.analytic.account', 'Analytic Account')

    @api.multi
    def unlink(self):
        for obj in self:
            sale_order = self.env['sale.order'].search([('team_id', '=', obj.id)])
            res_partner = self.env['res.partner'].search([('team_id', '=', obj.id)])
            if sale_order:
                raise UserError(_('It is out of the question to delete this sellers’ team because of its registerarion on the booking list. '))
            elif res_partner:
                raise UserError(_('The marketing team is not be abolished. Because it is registered on interrelation.'))
        return super(CrmTeam, self).unlink()

    @api.model
    def create(self, vals):
        ResUsers = self.env['res.users']
        for record in self:
            if 'member_ids' in vals:
                for member in vals['member_ids']:
                    if member[0] == 6:
                        for user_id in member[2]:
                            user = ResUsers.browse(user_id)
                            if user and user.sale_team_id and user.sale_team_id.id != record.id:
                                raise UserError(_('This employee %s is working on a team of marketing. After crossing out her/him from the team, you have a possibility to recruit') % user.sale_team_id.name)
        return super(CrmTeam, self).create(vals)

    @api.multi
    def write(self, vals):
        ResUsers = self.env['res.users']
        for record in self:
            if 'member_ids' in vals:
                for member in vals['member_ids']:
                    if member[0] == 6:
                        for user_id in member[2]:
                            user = ResUsers.browse(user_id)
                            if user and user.sale_team_id and user.sale_team_id.id != record.id:
                                raise UserError(_('This employee %s is working on a team of marketing. After crossing out her/him from the team, you have a possibility to recruit') % user.sale_team_id.name)
        return super(CrmTeam, self).write(vals)
