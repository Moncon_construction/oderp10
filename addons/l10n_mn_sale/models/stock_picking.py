# -*- coding: utf-8 -*-

from odoo import models, fields, api


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    @api.multi
    def check_sale_delivered(self):
        for picking in self:
            # Борлуулалтаас үүссэн орох хөдөлгөөн эсэхийг шалгах
            if picking.picking_type_id.code == 'outgoing':
                self.env.cr.execute("SELECT "
                                    "so.id as id, so.name as name, sp.id as picking_id "
                                    "FROM stock_move sm "
                                    "JOIN stock_picking sp on sm.picking_id = sp.id "
                                    "JOIN procurement_order po on po.id = sm.procurement_id "
                                    "JOIN sale_order_line sol on sol.id = po.sale_line_id "
                                    "JOIN sale_order so on so.id = sol.order_id "
                                    "WHERE "
                                    "sm.origin_returned_move_id is NULL and sp.id = %s " % picking.id)
                lines = self.env.cr.dictfetchall()
                line_ids = []
                for line in lines:
                    line_ids.append(line['id'])
                sale_order = self.env['sale.order'].search([('id', 'in', line_ids)])
                if sale_order:
                    count = 0
                    count_outgoing_pickings = 0
                    for p in sale_order.picking_ids:
                        if p.state == 'done' and p.picking_type_id.code == 'outgoing':
                            count += 1
                        if p.picking_type_id.code == 'outgoing':
                            count_outgoing_pickings += 1
                    if count > 0 and count_outgoing_pickings > 0:
                        if count == count_outgoing_pickings:
                            sale_order.is_delivered = True
                        else:
                            sale_order.is_delivered = False

    @api.multi
    def do_transfer(self):
        res = super(StockPicking, self).do_transfer()
        self.check_sale_delivered()
        return res
