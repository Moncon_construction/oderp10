# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, _
import odoo.addons.decimal_precision as dp


class Productpricelistitem(models.TransientModel):
    _name = 'product.price.list'
    _description = 'Product price list'

    def _get_default_currency_id(self):
        return self.env.user.company_id.currency_id.id

    product_price_list = fields.Many2many('product.product')
    currency_id = fields.Many2one('res.currency', 'Currency', default=_get_default_currency_id, required=True)
    company_id = fields.Many2one('res.company', 'Company')

    min_quantity = fields.Integer(
        'Min. Quantity', default=0,
        help="For the rule to apply, bought/sold quantity must be greater "
             "than or equal to the minimum quantity specified in this field.\n"
             "Expressed in the default unit of measure of the product.")
    date_start = fields.Date('Start Date', help="Starting date for the pricelist item validation")
    date_end = fields.Date('End Date', help="Ending valid for the pricelist item validation")
    adjust_day = fields.Boolean(string='Adjust Day', default=False)
    adjust_hour = fields.Boolean(string='Adjust Hour', default=False)
    start_hour = fields.Float(string='Start Hour', default=0)
    end_hour = fields.Float(string='End Hour', default=0)
    is_monday = fields.Boolean(string="Mon", default=False)
    is_tuesday = fields.Boolean(string="Tue", default=False)
    is_wednesday = fields.Boolean(string="Wed", default=False)
    is_thursday = fields.Boolean(string="Thu", default=False)
    is_friday = fields.Boolean(string="Fri", default=False)
    is_saturday = fields.Boolean(string="Sat", default=False)
    is_sunday = fields.Boolean(string="Sun", default=False)
    compute_price = fields.Selection([
        ('fixed', 'Fix Price'),
        ('percentage', 'Percentage (discount)'),
        ('formula', 'Formula')], index=True, default='fixed')
    fixed_price = fields.Float('Fixed Price', digits=dp.get_precision('Product Price'))
    percent_price = fields.Float('Percentage Price')

    sequence = fields.Integer(
        'Sequence', default=5, required=True,
        help="Gives the order in which the pricelist items will be checked. The evaluation gives highest priority to lowest sequence and stops as soon as a matching item is found.")
    base = fields.Selection([
        ('list_price', 'Public Price'),
        ('standard_price', 'Cost'),
        ('pricelist', 'Other Pricelist')], "Based on",
        default='list_price', required=True,
        help='Base price for computation.\n'
             'Public Price: The base price will be the Sale/public Price.\n'
             'Cost Price : The base price will be the cost price.\n'
             'Other Pricelist : Computation of the base price based on another Pricelist.')
    base_pricelist_id = fields.Many2one('product.pricelist', 'Other Pricelist')
    pricelist_id = fields.Many2one('product.pricelist', 'Pricelist', index=True, ondelete='cascade')
    price_surcharge = fields.Float(
        'Price Surcharge', digits=dp.get_precision('Product Price'),
        help='Specify the fixed amount to add or substract(if negative) to the amount calculated with the discount.')
    price_discount = fields.Float('Price Discount', default=0, digits=(16, 2))
    price_round = fields.Float(
        'Price Rounding', digits=dp.get_precision('Product Price'),
        help="Sets the price so that it is a multiple of this value.\n"
             "Rounding is applied after the discount and before the surcharge.\n"
             "To have prices that end in 9.99, set rounding 10, surcharge -0.01")
    price_min_margin = fields.Float(
        'Min. Price Margin', digits=dp.get_precision('Product Price'),
        help='Specify the minimum amount of margin over the base price.')
    price_max_margin = fields.Float(
        'Max. Price Margin', digits=dp.get_precision('Product Price'),
        help='Specify the maximum amount of margin over the base price.')

    def save(self):
        for product in self.product_price_list:
            product_pricelist_active = self.env['product.pricelist'].browse(self._context['active_ids'])[0]
            product_pricelist = self.env['product.pricelist.item']
            product_pricelist.create({
                'product_id': product.id,
                'compute_price': self.compute_price,
                'fixed_price': self.fixed_price,
                'min_quantity': self.min_quantity,
                'percent_price': self.percent_price,
                'pricelist_id': product_pricelist_active.id,
                'date_start': self.date_start,
                'date_end': self.date_end,
                'adjust_day': self.adjust_day,
                'adjust_hour': self.adjust_hour,
                'start_hour': self.start_hour,
                'end_hour': self.end_hour,
                'is_monday': self.is_monday,
                'is_tuesday': self.is_tuesday,
                'is_wednesday': self.is_wednesday,
                'is_thursday': self.is_thursday,
                'is_friday': self.is_friday,
                'is_saturday': self.is_saturday,
                'is_sunday': self.is_sunday,
                'base': self.base,
                'base_pricelist_id': product_pricelist_active.id,
                'price_discount': self.price_discount,
                'price_round': self.price_round,
                'price_surcharge': self.price_surcharge,
                'price_min_margin': self.price_min_margin,
                'price_max_margin': self.price_max_margin,
            })