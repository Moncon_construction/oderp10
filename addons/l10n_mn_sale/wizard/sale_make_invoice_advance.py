# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import logging
import time

from odoo import api, fields, models, _
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)
class SaleAdvancePaymentInv(models.TransientModel):
    _inherit = "sale.advance.payment.inv"

    @api.multi
    def _create_invoice(self, order, so_line, amount):
        _logger.info("CALLED _create_invoice() OVERRIDEN INTERNAL FUNCTION %s " % self.advance_payment_method)
        invoice =  super(SaleAdvancePaymentInv, self)._create_invoice(order, so_line, amount)

        # Урьдчилгаа нэхэмжлэл эсэхийг ялгаж байна.
        if self.advance_payment_method == 'percentage' or self.advance_payment_method == 'fixed':
            invoice.write({'is_prepayment' : True})

        return invoice
