# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _


class SaleOrderConfirmation(models.TransientModel):
    _name = 'sale.order.confirmation'
    _description = 'Sale Order Confirmation'

    order_id = fields.Many2one('sale.order')

    @api.model
    def default_get(self, fields):
        res = super(SaleOrderConfirmation, self).default_get(fields)
        if 'order_id' in fields and self._context.get('active_id') and not res.get('order_id'):
            res = {'order_id': self._context['active_id']}
        return res