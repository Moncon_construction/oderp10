# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError

class SelectSaleOrderCancelReason(models.TransientModel):
    _name = 'select.sale.order.cancel.reason'
    
    reason_id = fields.Many2one('sale.order.cancel.reason', 'Reason')
    order_id = fields.Many2one('sale.order', 'Order')

    @api.model
    def default_get(self, fields):
        res = super(SelectSaleOrderCancelReason, self).default_get(fields)
        if 'order_id' in fields and self._context.get('active_id') and not res.get('order_id'):
            res = {'order_id': self._context['active_id']}
        return res

    @api.multi
    def approve(self):
        for order in self.order_id:
            if self.reason_id:
                order.write({'cancel_reason_id':self.reason_id.id})
                order._action_cancel()
            else:
                raise UserError(_('Please select cancel reason'))
