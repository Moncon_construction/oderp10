# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Sale Contract",
    'version': '1.0',
    'depends': [
        'l10n_mn_contract',
        'l10n_mn_sale_stock'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Sale Additional Features
    """,
    'data': [
        'data/sale_contract_sequence.xml',
        'views/contract_category_view.xml',
        'views/contract_participation_view.xml',
        'views/contract_management.xml',
        'views/sale_contract_view.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
