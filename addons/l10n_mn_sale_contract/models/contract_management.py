# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError

class ContractManagement(models.Model):
    _inherit = 'contract.category'

    type = fields.Selection([('sale_contract', 'Sale Contract'), ('purchase_contract', 'Purchase Contract'), ('other', 'Other')], String='Type')

    @api.multi
    @api.constrains('type')
    def _check_type(self):
        type_contract = self.env['contract.category'].search([('type', '=', 'sale_contract')])
        if len(type_contract) > 1:
            raise ValidationError(_("Sale Contract Category already exists !"))

class ContractParticipation(models.Model):
    _inherit = 'contract.participation'

    is_check_sale_contract = fields.Boolean('Is check sale contract')

    _sql_constraints = [
        ('name_uniq', 'unique (is_check_sale_contract)', _("Is check Sale Contract Participation already exists !")),
    ]

class ContractManagement(models.Model):
    _inherit = 'contract.management'

    contract_sale = fields.Many2one('sale.order', 'Contract Sale', readonly=True)