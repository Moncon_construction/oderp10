# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from datetime import date, datetime, timedelta

from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    contract_link = fields.Many2one('contract.management', readonly=True)

    @api.multi
    def copy(self, default):
        default = {'contract_link': False}
        return super(SaleOrder, self).copy(default)

    @api.multi
    def sale_create_contract(self):
        contract_management = self.env['contract.management']
        contract_category = self.env['contract.category'].search([('type', '=', 'sale_contract')], limit=1)
        if not contract_category:
            raise ValidationError(_("Not Sale Contract Category "))
        contract_participation = self.env['contract.participation'].search([('is_check_sale_contract', '=', True)], limit=1)
        if not contract_participation:
            raise ValidationError(_("Not Participation Contract"))
        workflow_id = self.env['workflow.config'].search([('model_id.model', '=', 'contract.management')], limit=1)
        if not workflow_id:
            raise ValidationError(_("Not Contract workflow"))
        type_code = self.name
        number = str(type_code) + _('contract')

        if not self.project_id.id:
            raise ValidationError(_("Not Analytic Account"))
        sale_contract = {
            'name': number,
            'category_id': contract_category.id,
            'analytic_account_id': self.project_id.id,
            'partner_id': self.partner_id.id,
            'participation': contract_participation.id,
            'contract_user_id': self.user_id.id,
            'start_date': datetime.now(),
            'total_amount': self.amount_total,
            'workflow_id': workflow_id.id,
            'contract_sale': self.id
        }
        sale_contract_id = contract_management.create(sale_contract)
        if 'sale_warehouse_id' in contract_management:
            sale_contract_id.sale_warehouse_id = self.warehouse_id.id
        if 'sale_picking_type_id' in contract_management:
            sale_contract_id.sale_picking_type_id = self.stock_picking_type.id
        for line in self.order_line:
            if self.validity_date:
                end_date = self.validity_date
            else:
                end_date = datetime.strptime(self.date_order, DEFAULT_SERVER_DATETIME_FORMAT) + timedelta(days=int(line.product_id.sale_delay))

            sale_contract_term_ids = {
                'contract_id': sale_contract_id.id,
                'product_id': line.product_id.id,
                'name': line.name,
                'invoice_date': self.date_order,
                'end_date': end_date,
                'amount_type': 'money',
                'quantity': line.product_uom_qty,
                'amount': line.price_unit,
                'discount': line.discount,
                'sale_id': line.order_id.id
            }
            self.env['contract.term'].create(sale_contract_term_ids)
        self.contract_link = sale_contract_id.id
