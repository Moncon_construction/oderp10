# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Sale Customer Bonus",
    'version': '1.0',
    'depends': ['l10n_mn_sale'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Sale Customer Bonus
    """,
    'data': [
        'views/sale_customer_bonus_views.xml',
        'views/sale_customer_bonus_settings_views.xml',
        'data/sale_customer_bonus_sequence.xml',
        'wizard/sale_customer_bonus_report.xml',
        'security/ir.model.access.csv'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}