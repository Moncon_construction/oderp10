# -*- coding: utf-8 -*-

from itertools import groupby
from datetime import datetime, timedelta
from ast import literal_eval
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.misc import formatLang

import odoo.addons.decimal_precision as dp


class SaleCustomerBonus(models.Model):
    _name = "sale.customer.bonus"
    _order = ' start_date DESC'

    def _compute_in_balance(self):
        if self.partner_id:
            account_ids = self.env['account.account'].search([('user_type_id.type', '=', 'receivable')]).ids
            if account_ids:

                # in balance
                initials = self.env['account.move.line'].with_context(
                    {'partner_ids': [self.partner_id.id]}).get_initial_balance(self.company_id.id, account_ids,
                                                                               self.start_date, 'posted')
                if initials:
                    in_balance = 0
                    for account_balance in initials:
                        in_balance += account_balance['start_debit'] - account_balance['start_credit']
                    self.inbalance = in_balance

    def _compute_out_balance(self):
        if self.partner_id:
            account_ids = self.env['account.account'].search([('user_type_id.type', '=', 'receivable')]).ids
            if account_ids:
                # out balance
                outtials = self.env['account.move.line'].with_context(
                    {'partner_ids': [self.partner_id.id]}).get_initial_balance(self.company_id.id, account_ids,
                                                                               self.end_date, 'posted')
                if outtials:
                    out_balance = 0
                    for account_balance in outtials:
                        out_balance += account_balance['start_debit'] - account_balance['start_credit']
                    self.outbalance = out_balance

    partner_id = fields.Many2one('res.partner', string="Partner", required=True)
    inbalance = fields.Float('In Balance', readonly=True, compute='_compute_in_balance')
    outbalance = fields.Integer('Out Balance', readonly=True, compute='_compute_out_balance')
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    state = fields.Selection([('draft', 'Draft'),
                              ('posted', 'Posted'),
                              ('reviewed', 'Reviewed'),
                              ('approved', 'Approved')], readonly=True, states={'done': [('readonly', True)]},
                             string='Status', default='draft')
    sale_bonus_type = fields.Many2one('sale.reward.settings', string="Sale reward settings", required=True)
    company_id = fields.Many2one('res.company', string='Company', readonly=True, required=True,
                                 default=lambda self: self.env['res.company']._company_default_get(
                                     'hr.salary.rule.category.report'))

    name = fields.Char(string='Order Reference', readonly=True)
    total_amount_of_sales = fields.One2many(
        'sale.customer.bonus.line', 'bonus_line', 'Sale Order List')
    reward_amount_of_sales = fields.One2many(
        'sale.customer.bonus.reward.line', 'bonus_reward_line', 'Sale Order Reward List')
    # sales_rewards = fields.One2many('rewards.settings', 'partner_id', 'Sale Rewards List', readonly=True)

    one = fields.Char('In Balance')
    two = fields.Char('In Balance')
    invoice_ids = fields.Many2many("account.invoice", 'account_invoice_sale_customer_bonus_rel', string='Invoices',
                                   readonly=True,
                                   copy=False)

    @api.model
    def create(self, vals):
        seq = self.env['ir.sequence'].get('mongolian.sale.customer.bonus.sequence') or '/'
        vals['name'] = 'BNS/' + seq
        return super(SaleCustomerBonus, self).create(vals)

    @api.multi
    def account_invoice(self):
        invoices = self.mapped('invoice_ids')
        action = self.env.ref('account.action_invoice_tree1').read()[0]
        if len(invoices) > 1:
            action['domain'] = [('id', 'in', invoices.ids)]
        elif len(invoices) == 1:
            action['views'] = [(self.env.ref('account.invoice_form').id, 'form')]
            action['res_id'] = invoices.ids[0]
        else:
            action = {'type': 'ir.actions.act_window_close'}
        return action

    @api.multi
    def monitor(self):
        self.write({'state': 'reviewed'})

    @api.multi
    def send(self):
        self.write({'state': 'posted'})

    @api.multi
    def done(self):

        "Батлах товч дарахад Борлуулалтын захиалгын урамшуулаллаас нийллүүлэгчийн нэхэмжлэх үүсгэж байгаа"
        invoice_obj = self.env['account.invoice']
        invoice_lines = self.env['account.invoice.line']
        for obj in self:
            obj.write({'state': 'approved'})

            invoice_id = self.env['account.invoice'].create({'state': 'draft',
                                                             'partner_id': obj.partner_id.id,
                                                             'type': 'in_invoice',
                                                             'account_id': obj.sale_bonus_type.sale_reward_debit_account.id,
                                                             'journal_id': obj.sale_bonus_type.sale_reward_journal.id})
            price_unit = self.reward_calculate()
            invoice_line = self.env['account.invoice.line'].create({
                'account_id': obj.sale_bonus_type.sale_reward_expense_account.id,
                'price_unit': price_unit,
                'name': self.name,
                'invoice_id': invoice_id.id
            })
            invoice_lines = invoice_lines | invoice_line
            invoice_obj = invoice_obj | invoice_id

            obj.invoice_ids = invoice_obj

    @api.multi
    def cancel(self):
        self.write({'state': 'draft'})

    @api.multi
    def reward_calculate(self):

        "Тухайн харилцагчийн тодорхой хугааний Борлууллалтын захиалгуудыг шүүж гаргаж ирэх"

        bonus_lines = self.env['sale.customer.bonus.line']
        reward_lines = self.env['sale.customer.bonus.reward.line']
        query = (
                "SELECT so.id as id, so.name as name, so.amount_total as amount_total, so.date_order as date_order, "
                "SUM(CASE WHEN (sol.qty_delivered>0) THEN ((sol.qty_delivered * purchase_price )) ELSE 0 END) AS delivery "
                "FROM   sale_order so "
                "LEFT JOIN sale_order_line sol on so.id = sol.order_id "
                "WHERE so.state = 'sale' AND so.partner_id = %s AND so.date_order BETWEEN '%s' AND '%s'"
                "GROUP BY so.id, so.name, so.date_order, so.amount_total" % (
                    self.partner_id.id, self.start_date, self.end_date))
        self.env.cr.execute(query)
        sale_order_lines = self.env.cr.dictfetchall()
        sale_amount_total = 0
        for sale_order_line in sale_order_lines:
            sale_amount_total += sale_order_line['amount_total']

            bonus_line = self.env['sale.customer.bonus.line'].create({
                'date': sale_order_line['date_order'],
                'number': sale_order_line['name'],
                'sales_amount': sale_order_line['amount_total'],
                'sales_amount_delivery': sale_order_line['delivery']
            })

            invoice_amount = (""

                              "SELECT ai.residual as residual, ai.amount_total as amount_total "
                              "FROM account_invoice ai "
                              "LEFT JOIN account_invoice_line ail on ail.invoice_id = ai.id "
                              "LEFT JOIN sale_order_line_invoice_rel solir on solir.invoice_line_id = ail.id "
                              "LEFT JOIN sale_order_line sol on  sol.id = solir.order_line_id "
                              "LEFT JOIN sale_order so on so.id =  sol.order_id "

                              "WHERE so.id = %s "
                              "GROUP BY ai.id " % (sale_order_line['id']))
            self.env.cr.execute(invoice_amount)
            sale_order_invoice_lines = self.env.cr.dictfetchall()

            for sale_order_invoice_line in sale_order_invoice_lines:
                bonus_line.write({
                    'sales_amount_paid': sale_order_invoice_line['amount_total'] - sale_order_invoice_line['residual'],
                    'sales_amount_last': sale_order_invoice_line['residual']
                })

            bonus_lines = bonus_lines | bonus_line


        self.total_amount_of_sales = bonus_lines

        "Тухайн харилцагчийн тодорхой хугааний Борлууллалтын захиалгын нийт дүнгээс урамшуулал тооцох бодож байна "


        rewards_amount = 0
        if self.sale_bonus_type.sale_order_amount:

            for purchase_reward_line in self.sale_bonus_type.sale_amount_line:

                if purchase_reward_line.sale_amount_min < sale_amount_total and purchase_reward_line.sale_amount_max > sale_amount_total:
                    if purchase_reward_line.sale_percent_price:
                        rewards_amount += purchase_reward_line.sale_percent_price * sale_amount_total / 100

                        reward_line = self.env['sale.customer.bonus.reward.line'].create({
                            'reward_rate': 'Нийт дүн',
                            'reward_sale_amount': sale_amount_total,
                            'reward_percent': purchase_reward_line.sale_percent_price,
                            'rewards_amount': purchase_reward_line.sale_percent_price * sale_amount_total / 100
                        })

                        reward_lines = reward_lines | reward_line

                    elif purchase_reward_line.sale_fixed_price:
                        rewards_amount += purchase_reward_line.sale_fixed_price
                        reward_line = self.env['sale.customer.bonus.reward.line'].create({
                            'reward_rate': 'Нийт дүн',
                            'reward_sale_amount': sale_amount_total,
                            'reward_percent': purchase_reward_line.sale_percent_price,
                            'rewards_amount': purchase_reward_line.sale_fixed_price

                        })
                        reward_lines = reward_lines | reward_line

                if not purchase_reward_line.sale_amount_max:

                    if purchase_reward_line.sale_amount_min < sale_amount_total:
                        if purchase_reward_line.sale_percent_price:
                            rewards_amount += purchase_reward_line.sale_percent_price * sale_amount_total / 100

                            reward_line = self.env['sale.customer.bonus.reward.line'].create({
                                'reward_rate': 'Нийт дүн',
                                'reward_sale_amount': sale_amount_total,
                                'reward_percent': purchase_reward_line.sale_percent_price,
                                'rewards_amount': purchase_reward_line.sale_percent_price * sale_amount_total / 100
                            })

                            reward_lines = reward_lines | reward_line

                        elif purchase_reward_line.sale_fixed_price:
                            rewards_amount += purchase_reward_line.sale_fixed_price
                            reward_line = self.env['sale.customer.bonus.reward.line'].create({
                                'reward_rate': 'Нийт дүн',
                                'reward_sale_amount': sale_amount_total,
                                'reward_percent': purchase_reward_line.sale_percent_price,
                                'rewards_amount': purchase_reward_line.sale_fixed_price

                            })
                            reward_lines = reward_lines | reward_line

        "Тухайн харилцагчийн тодорхой хугааний Борлууллалтын захиалгын Барааны дүнгээс тооцоолох урамшуулал бодож байна "

        if self.sale_bonus_type.product_amount:

            for product_reward_line in self.sale_bonus_type.product_amount_line:

                if product_reward_line.product_tmpl_id:
                    self.env.cr.execute(
                        "select so.date_order as date_order, so.name as name, rp.name as partner_name, sol.product_id as product_id, sol.price_total as price_total, "
                        "so.amount_total as amount_total, pt.name as name "
                        "from "
                        "sale_order so "
                        "join sale_order_line sol on so.id = sol.order_id "
                        "join product_product pp on sol.product_id=pp.id "
                        "join product_template pt on pp.product_tmpl_id = pt.id "
                        " join res_partner rp on rp.id = so.partner_id "
                        "WHERE so.state = 'sale' AND so.partner_id = %s AND so.date_order BETWEEN '%s' AND '%s' AND sol.product_id = %s" % (
                            self.partner_id.id, self.start_date, self.end_date, product_reward_line.product_tmpl_id.id))

                    product_product_lines = self.env.cr.dictfetchall()

                    product_price_total = 0
                    for product_product_line in product_product_lines:
                        product_price_total += product_product_line['price_total']

                    if product_reward_line.product_reward_min < product_price_total and product_reward_line.product_reward_max > product_price_total:
                        if product_reward_line.product_percent_price:
                            rewards_amount += product_reward_line.product_percent_price * product_price_total / 100
                            reward_line = self.env['sale.customer.bonus.reward.line'].create({
                                'reward_rate': product_product_line['name'],
                                'reward_sale_amount': product_price_total,
                                'reward_percent': product_reward_line.product_percent_price,
                                'rewards_amount': product_reward_line.product_percent_price * product_price_total / 100

                            })
                            reward_lines = reward_lines | reward_line

                        elif product_reward_line.product_fixed_price:
                            rewards_amount += product_reward_line.product_fixed_price
                            reward_line = self.env['sale.customer.bonus.reward.line'].create({
                                'reward_rate': product_product_line['name'],
                                'reward_sale_amount': product_price_total,
                                'reward_percent': product_reward_line.product_percent_price,
                                'rewards_amount': product_reward_line.product_fixed_price

                            })
                            reward_lines = reward_lines | reward_line

                        if not product_reward_line.product_reward_max:
                            if product_reward_line.product_percent_price:
                                rewards_amount += product_reward_line.product_percent_price * product_price_total / 100
                                reward_line = self.env['sale.customer.bonus.reward.line'].create({
                                    'reward_rate': product_product_line['name'],
                                    'reward_sale_amount': product_price_total,
                                    'reward_percent': product_reward_line.product_percent_price,
                                    'rewards_amount': product_reward_line.product_percent_price * product_price_total / 100

                                })
                                reward_lines = reward_lines | reward_line

                            elif product_reward_line.product_fixed_price:
                                rewards_amount += product_reward_line.product_fixed_price
                                reward_line = self.env['sale.customer.bonus.reward.line'].create({
                                    'reward_rate': product_product_line['name'],
                                    'reward_sale_amount': product_price_total,
                                    'reward_percent': product_reward_line.product_percent_price,
                                    'rewards_amount': product_reward_line.product_fixed_price

                                })
                                reward_lines = reward_lines | reward_line

                if product_reward_line.product_brand:
                    self.env.cr.execute(
                        "select pt.brand_name as brand_name ,sol.price_total as price_total, pb.brand_name as brand_name "
                        "from "
                        "sale_order so "
                        "join sale_order_line sol on so.id = sol.order_id"
                        " join res_partner rp on rp.id = so.partner_id "
                        "join product_product pp on sol.product_id=pp.id "
                        "join product_template pt on pp.product_tmpl_id = pt.id "
                        "join product_brand pb on pb.id = pt.brand_name "
                        "WHERE so.state = 'sale' AND so.partner_id = %s AND so.date_order BETWEEN '%s' AND '%s' AND pt.brand_name = %s" % (
                            self.partner_id.id, self.start_date, self.end_date, product_reward_line.product_brand.id))

                    product_brand_lines = self.env.cr.dictfetchall()

                    product_brand_total = 0
                    for product_brand_line in product_brand_lines:
                        product_brand_total += product_brand_line['price_total']
                    if product_reward_line.product_reward_min < product_brand_total and product_reward_line.product_reward_max > product_brand_total:
                        if product_reward_line.product_percent_price:
                            rewards_amount += product_reward_line.product_percent_price * product_brand_total / 100
                            reward_line = self.env['sale.customer.bonus.reward.line'].create({
                                'reward_rate': product_brand_line['brand_name'],
                                'reward_sale_amount': product_brand_total,
                                'reward_percent': product_reward_line.product_percent_price,
                                'rewards_amount': product_reward_line.product_percent_price * product_brand_total / 100

                            })
                            reward_lines = reward_lines | reward_line
                        elif product_reward_line.product_fixed_price:
                            rewards_amount += product_reward_line.product_fixed_price
                            reward_line = self.env['sale.customer.bonus.reward.line'].create({
                                'reward_rate': product_brand_line['brand_name'],
                                'reward_sale_amount': product_brand_total,
                                'reward_percent': product_reward_line.product_percent_price,
                                'rewards_amount': product_reward_line.product_fixed_price

                            })
                            reward_lines = reward_lines | reward_line
                    if not product_reward_line.product_reward_max:
                        if product_reward_line.product_percent_price:
                            rewards_amount += product_reward_line.product_percent_price * product_brand_total / 100
                            reward_line = self.env['sale.customer.bonus.reward.line'].create({
                                'reward_rate': product_brand_line['brand_name'],
                                'reward_sale_amount': product_brand_total,
                                'reward_percent': product_reward_line.product_percent_price,
                                'rewards_amount': product_reward_line.product_percent_price * product_brand_total / 100

                            })
                            reward_lines = reward_lines | reward_line
                        elif product_reward_line.product_fixed_price:
                            rewards_amount += product_reward_line.product_fixed_price
                            reward_line = self.env['sale.customer.bonus.reward.line'].create({
                                'reward_rate': product_brand_line['brand_name'],
                                'reward_sale_amount': product_brand_total,
                                'reward_percent': product_reward_line.product_percent_price,
                                'rewards_amount': product_reward_line.product_fixed_price

                            })
                            reward_lines = reward_lines | reward_line

                if product_reward_line.categ_id:
                    self.env.cr.execute(
                        "select pt.categ_id as categ_id ,sol.price_total as price_total, pc.name as categ_name , so.name as name "
                        "from "
                        "sale_order so "
                        "join sale_order_line sol on so.id = sol.order_id"
                        " join res_partner rp on rp.id = so.partner_id "
                        "join product_product pp on sol.product_id=pp.id "
                        "join product_template pt on pp.product_tmpl_id = pt.id "
                        "join product_category pc on pt.categ_id =pc.id "
                        "WHERE so.state = 'sale' AND so.partner_id = %s AND so.date_order BETWEEN '%s' AND '%s' AND pt.categ_id = %s" % (
                            self.partner_id.id, self.start_date, self.end_date, product_reward_line.categ_id.id))

                    product_categ_lines = self.env.cr.dictfetchall()

                    product_categ_total = 0
                    for product_categ_line in product_categ_lines:
                        product_categ_total += product_categ_line['price_total']

                    if product_reward_line.product_reward_min < product_categ_total and product_reward_line.product_reward_max > product_categ_total:

                        if product_reward_line.product_percent_price:
                            rewards_amount += product_reward_line.product_percent_price * product_categ_total / 100
                            reward_line = self.env['sale.customer.bonus.reward.line'].create({
                                'reward_rate': product_categ_line['categ_name'],
                                'reward_sale_amount': product_categ_total,
                                'reward_percent': product_reward_line.product_percent_price,
                                'rewards_amount': product_reward_line.product_percent_price * product_categ_total / 100

                            })
                            reward_lines = reward_lines | reward_line
                        elif product_reward_line.product_fixed_price:
                            rewards_amount += product_reward_line.product_fixed_price
                            reward_line = self.env['sale.customer.bonus.reward.line'].create({
                                'reward_rate': product_categ_line['categ_name'],
                                'reward_sale_amount': product_categ_total,
                                'reward_percent': product_reward_line.product_percent_price,
                                'rewards_amount': product_reward_line.product_fixed_price

                            })

                            reward_lines = reward_lines | reward_line

                        if not product_reward_line.product_reward_max:
                            if product_reward_line.product_percent_price:
                                rewards_amount += product_reward_line.product_percent_price * product_categ_total / 100
                                reward_line = self.env['sale.customer.bonus.reward.line'].create({
                                    'reward_rate': product_categ_line['categ_name'],
                                    'reward_sale_amount': product_categ_total,
                                    'reward_percent': product_reward_line.product_percent_price,
                                    'rewards_amount': product_reward_line.product_percent_price * product_categ_total / 100

                                })
                                reward_lines = reward_lines | reward_line
                            elif product_reward_line.product_fixed_price:
                                rewards_amount += product_reward_line.product_fixed_price
                                reward_line = self.env['sale.customer.bonus.reward.line'].create({
                                    'reward_rate': product_categ_line['categ_name'],
                                    'reward_sale_amount': product_categ_total,
                                    'reward_percent': product_reward_line.product_percent_price,
                                    'rewards_amount': product_reward_line.product_fixed_price

                                })

                                reward_lines = reward_lines | reward_line

        self.reward_amount_of_sales = reward_lines

        return rewards_amount


class SaleCustomerBonusLine(models.Model):
    _name = "sale.customer.bonus.line"
    bonus_line = fields.Many2one('sale.customer.bonus')
    date = fields.Date(string="Date")
    number = fields.Char(string="Number")
    sales_amount = fields.Integer(string="Sales Amount")
    sales_amount_delivery = fields.Integer(string=" Sales Amount Delivery")
    sales_amount_paid = fields.Integer(string="Sales Amount Paid")
    sales_amount_last = fields.Integer(string="Sales Amount Residual")


class SaleCustomerBonusRewardLine(models.Model):
    _name = "sale.customer.bonus.reward.line"

    bonus_reward_line = fields.Many2one('sale.customer.bonus')

    reward_rate = fields.Char(string="Reward Rate")
    reward_sale_amount = fields.Float(string="Reward Sale Amount")
    reward_percent = fields.Float(string="Reward Percent")
    rewards_amount = fields.Float(string="Reward Amount")
