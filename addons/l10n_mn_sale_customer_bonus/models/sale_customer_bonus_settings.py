# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError, RedirectWarning, ValidationError
import odoo.addons.decimal_precision as dp
from odoo.exceptions import UserError, Warning


class RewardPointsSettings(models.Model):
    """Create two fields to the Reward setting form 'Reward Percentage' and 'Redeem Percentage'"""
    _name = 'sale.reward.settings'
    _order = 'date_from DESC'

    sale_order_amount = fields.Boolean('Calculate total sales order')
    product_amount = fields.Boolean('Estimated amount of product')
    company_id = fields.Many2one('res.company', string='Company', readonly="True", required=True,
                                 default=lambda self: self.env['res.company']._company_default_get(
                                     'hr.salary.rule.category.report'))
    date_from = fields.Date('Start Date', required=True)
    date_to = fields.Date('Stop Date', required=True)
    name = fields.Char(string="Name",required=True)
    sales_team = fields.Many2one('crm.team')
    sale_reward_debit_account = fields.Many2one('account.account', 'Sale reward debit account',required=True)
    sale_reward_expense_account = fields.Many2one('account.account', 'Sale reward expense account',required=True)
    sale_reward_journal = fields.Many2one('account.journal','Sale reward journal', required=True)

    sale_amount_line = fields.One2many(
        'sale.reward.sale.amount.line', 'settings', string='Sale Order List')
    product_amount_line = fields.One2many('sale.reward.product.amount.line', 'settings',
                                         'Sale Order Product list')


    @api.model
    def create(self,vals):

        self.onchange_sale_amount_line()
        self.onchange_product_amount_line()
        return super(RewardPointsSettings, self).create(vals)

    @api.multi
    def write(self, vals):


        self.onchange_sale_amount_line()
        self.onchange_product_amount_line()
        return super(RewardPointsSettings, self).write(vals)

    @api.onchange('sale_amount_line')
    def onchange_sale_amount_line(self):
        if self.sale_amount_line:
            lines = self.sale_amount_line
            lines = lines.sorted('sale_amount_min')
            previous_line = lines[0]

            for line in lines:
                if line.sale_amount_max:
                    if line.sale_amount_min > line.sale_amount_max:
                        raise UserError(_('The minimum amount of sales is higher than the maximum'))
                if line != previous_line and line.sale_amount_min < previous_line.sale_amount_max:
                    raise UserError(_('Sale Amount exits.'))
                previous_line = line




    @api.onchange('product_amount_line')
    def onchange_product_amount_line(self):
        if self.product_amount_line:
            lines = self.product_amount_line
            lines = lines.sorted('product_reward_min')

            for line in lines:
                if line.product_reward_max:
                    if line.product_reward_min > line.product_reward_max:
                        raise UserError(_('The minimum amount of product is higher than the maximum'))



    @api.depends('date_from', 'date_to')
    def _check_duration_date(self):
        for obj_fy in self:
            if obj_fy.date_from < obj_fy.date_to:
                return True

    _constraints = [
        (_check_duration_date, 'Error!\nThe start date of a fiscal year must precede its end date.',
         ['date_from', 'date_to'])
    ]


class SaleRewardSaleAmountLine(models.Model):
    _name = 'sale.reward.sale.amount.line'

    settings = fields.Many2one('sale.reward.settings')

    reward_compute_type = fields.Selection([
        ('fixed', 'Fix Price'),
        ('percentage', 'Percentage (discount)')], index=True, default='fixed')
    sale_amount_min = fields.Integer('Sale Amount Min', required=True)
    sale_amount_max = fields.Integer('Sale Amount Max', required=True)

    sale_fixed_price = fields.Integer('Sale Fixed Price', required=True)
    sale_percent_price = fields.Integer('Sale Percentage Price', required=True)








class SaleRewardProductAmountLine(models.Model):
    _name = 'sale.reward.product.amount.line'

    settings = fields.Many2one('sale.reward.settings')
    product_reward_name = fields.Selection([
        ('2_product_category', ' Product Category'),
        ('1_product_brand', 'Product Brand'),
        ('0_product', 'Product')], "Apply On",
        default='0_product', required=True,
        help='Pricelist Item applicable on selected option')
    categ_id = fields.Many2one(
        'product.category', 'Product Category', ondelete='cascade',
        help="Specify a product category if this rule only applies to products belonging to this category or its children categories. Keep empty otherwise.")
    product_tmpl_id = fields.Many2one(
        'product.product', 'Product Template', ondelete='cascade',
        help="Specify a template if this rule only applies to one product template. Keep empty otherwise.")
    product_brand = fields.Many2one(
        'product.brand', 'Product', ondelete='cascade',
        help="Specify a product if this rule only applies to one product. Keep empty otherwise.")

    reward_compute_type = fields.Selection([
        ('fixed', 'Fix Price'),
        ('percentage', 'Percentage (discount)')], index=True, default='fixed')
    product_reward_min = fields.Integer('Product Reward Min', required=True)
    product_reward_max = fields.Integer('Product Reward Max', required=True)
    product_fixed_price = fields.Integer('Fixed Price', required=True)
    product_percent_price = fields.Integer('Percentage Price', required=True)





