# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, models, fields
from odoo.tools.translate import _
from odoo.exceptions import UserError
import base64
from io import BytesIO
import time
import xlsxwriter
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from datetime import datetime, date
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import pytz


class SaleCustomerBonusReport(models.TransientModel):
    _name = 'sale.customer.bonus.wizard'

    @api.multi
    def _default_products(self):
        active_id = self.env.context.get('active_ids', []) or []
        record = self.env['sale.customer.bonus'].browse(active_id)
        return record

    sale_customer_bonus = fields.Many2one('sale.customer.bonus', string='Sale Customer Bonus', default=_default_products,
                                          invisible=True)

    @api.multi
    def export_report(self):



        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Partner Sales Bonus')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # parameters
        rowx = 0

        # FORMAT
        header_text = {
            'font_name': 'Times New Roman',
            'font_size': 12,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter',
            'border': 1,
            'text_wrap': 1,
            'bg_color': '#83CAFF',
            'text_wrap': True
        }

        text_integer = {
            'font_name': 'Times New Roman',
            'font_size': 12,
            'align': 'right',
            'valign': 'vcenter',
            'border': 1,
        }
        text_font = {
            'font_name': 'Times New Roman',
            'font_size': 12,
            'bold': True,
            'align': 'left',
            'valign': 'vcenter'
        }

        text_float_total = {
            'font_name': 'Times New Roman',
            'font_size': 12,
            'bold': True,
            'align': 'right',
            'valign': 'vcenter',
            'border': 1,
            'num_format': '#,##0.00',
            'bg_color': '#83CAFF'
        }


        text_format_name = {
            'font_name': 'Times New Roman',
            'font_size': 20,
            'align': 'vcenter',
            'valign': 'vcenter',
            'align': 'center',
            'bold': True
        }
        format_group_header = book.add_format(header_text)
        format_group_text_integer = book.add_format(text_integer)
        format_group_text_font = book.add_format(text_font)
        format_group_text_total_float = book.add_format(text_float_total)

        format_name = book.add_format(text_format_name)

        # parameters
        rowx = 0

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(
            filename_prefix=('stock_inventory_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(time.strftime('%Y-%m-%d'))
        sheet.set_portrait()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.20, 0.20, 0.20, 1.5)  # 1cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)

        # compute column
        sheet.set_column('A:A', 4)
        sheet.set_column('B:B', 10)
        sheet.set_column('C:C', 25)
        sheet.set_column('D:D', 10)
        sheet.set_column('E:E', 10)
        sheet.set_column('F:F', 13)
        sheet.set_column('G:G', 10)
        sheet.set_column('H:H', 13)
        sheet.set_column('I:I', 10)
        sheet.set_column('J:J', 13)
        sheet.set_column('K:K', 10)
        sheet.set_column('L:L', 13)
        sheet.set_column('M:M', 10)
        sheet.set_column('N:N', 13)
        sheet.set_column('O:O', 10)
        sheet.set_column('P:P', 13)
        sheet.set_column('Q:Q', 13)
        sheet.set_row(2, 20)
        sheet.set_row(5, 30)
        sheet.set_row(6, 30)

        for obj in self:

            sheet.merge_range(rowx + 1, 0, rowx, 16, report_name.upper(), format_name)

            rowx += 2
            sheet.merge_range(rowx, 3, rowx, 5, obj.sale_customer_bonus.name, format_name)

            rowx += 1

            # create name
            sheet.merge_range(rowx, 1, rowx, 3, _('Company name: %s') % obj.sale_customer_bonus.company_id.name,
                              format_group_text_font)
            bonus_id = obj.sale_customer_bonus.id
            bonus_obj = self.env['sale.customer.bonus'].browse(bonus_id)
            sheet.merge_range(rowx, 4, rowx, 6, _('Partner: %s') % obj.sale_customer_bonus.partner_id.name,
                              format_group_text_font)

            rowx += 1
            sheet.merge_range(rowx, 1, rowx, 3, _('Print Date: %s') % obj.create_date, format_group_text_font)

            sheet.merge_range(rowx, 4, rowx, 6, _('Init Balance: %s') % obj.sale_customer_bonus.inbalance,
                              format_group_text_font)

            rowx += 1
            sheet.merge_range(rowx, 1, rowx, 3, _('Count Date: %s-%s') % (
            obj.sale_customer_bonus.start_date, obj.sale_customer_bonus.end_date),
                              format_group_text_font)
            sheet.merge_range(rowx, 4, rowx, 6, _('Out Balance: %s') % obj.sale_customer_bonus.outbalance,
                              format_group_text_font)

            rowx += 1
            # table header
            sheet.merge_range(rowx, 0, rowx + 1, 0, _('№'), format_group_header)
            sheet.merge_range(rowx, 1, rowx + 1, 1, _('Date'), format_group_header)
            sheet.merge_range(rowx, 2, rowx + 1, 2, _('Number'), format_group_header)
            sheet.merge_range(rowx, 3, rowx + 1, 3, _('Sales Amount'), format_group_header)
            sheet.merge_range(rowx, 4, rowx + 1, 4, _('Picking Amount'), format_group_header)
            sheet.merge_range(rowx, 5, rowx + 1, 5, _('Invoice Amount'), format_group_header)
            sheet.merge_range(rowx, 6, rowx + 1, 6, _('Residuals'), format_group_header)

            rowx += 2

            sum_amoumt_total = 0
            sum_invoice_amoumt_total = 0
            sum_residual_amoumt_total = 0
            sum_delivery_total = 0
            for bonus_line in obj.sale_customer_bonus.total_amount_of_sales:
                sum_amoumt_total += bonus_line.sales_amount
                sum_invoice_amoumt_total += bonus_line.sales_amount_paid
                sum_residual_amoumt_total += bonus_line.sales_amount_last
                sum_delivery_total += bonus_line.sales_amount_delivery

                sheet.write(rowx, 1, bonus_line.date, format_group_text_integer)
                sheet.write(rowx, 2, bonus_line.number, format_group_text_integer)
                sheet.write(rowx, 3, bonus_line.sales_amount, format_group_text_integer)
                sheet.write(rowx, 4, bonus_line.sales_amount_delivery, format_group_text_integer)
                sheet.write(rowx, 5, bonus_line.sales_amount_paid, format_group_text_integer)
                sheet.write(rowx, 6, bonus_line.sales_amount_last, format_group_text_integer)
                rowx += 1

            sheet.merge_range(rowx, 0, rowx, 2, _('TOTAL'), format_group_text_total_float)
            sheet.write(rowx, 3, sum_amoumt_total, format_group_text_total_float)
            sheet.write(rowx, 4, sum_delivery_total, format_group_text_total_float)
            sheet.write(rowx, 5, sum_invoice_amoumt_total, format_group_text_total_float)
            sheet.write(rowx, 6, sum_residual_amoumt_total, format_group_text_total_float)

            rowx += 5
            sheet.merge_range(rowx, 2, rowx + 1, 2, _('Indicator'), format_group_header)
            sheet.merge_range(rowx, 3, rowx + 1, 3, _('Sales Amount'), format_group_header)
            sheet.merge_range(rowx, 4, rowx + 1, 4, _('Reward Rate'), format_group_header)
            sheet.merge_range(rowx, 5, rowx + 1, 5, _('Reward Amount'), format_group_header)
            rowx += 2
            sum_reward_sale_amount = 0
            sum_reward_amount = 0

            for reward_line in obj.sale_customer_bonus.reward_amount_of_sales:
                sum_reward_sale_amount += reward_line.reward_sale_amount
                sum_reward_amount += reward_line.rewards_amount

                sheet.write(rowx, 2, reward_line.reward_rate, format_group_text_integer)
                sheet.write(rowx, 3, reward_line.reward_sale_amount, format_group_text_integer)
                sheet.write(rowx, 4, reward_line.reward_percent, format_group_text_integer)
                sheet.write(rowx, 5, reward_line.rewards_amount, format_group_text_integer)

                rowx += 1
                sheet.write(rowx, 2,  _('TOTAL'), format_group_text_total_float)
                sheet.write(rowx, 3, sum_reward_sale_amount, format_group_text_total_float)
                sheet.write(rowx, 4  , ''   , format_group_text_total_float)
                sheet.write(rowx, 5, sum_reward_amount, format_group_text_total_float)

            # END OF THE REPORT

            rowx += 2
            sheet.merge_range(rowx, 2, rowx, 6,
                              _('Partner:/ ........................../%s') % obj.sale_customer_bonus.partner_id.name,
                              format_group_text_font)
            rowx += 2
            sheet.merge_range(rowx, 2, rowx, 6,_('Accountant ……………………………./                         /') , format_group_text_font)

            rowx += 2

            sheet.merge_range(rowx, 2, rowx, 6, _('Sales Manager ……………………………./                         /'), format_group_text_font)


            rowx +=2

        sheet.set_zoom(75)
        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()
