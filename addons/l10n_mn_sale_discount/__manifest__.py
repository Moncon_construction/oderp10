# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

{
    'name': "Mongolian Sales Discount",
    'version': '1.0',
    'depends': [
        'sale',
        'l10n_mn_sale'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       - Sale Order Line discount type (percent, amount)
       - Add amount discount type
       - Add discount total on Sale order
       - Discount type and amount type discount on the reports
    """,
    'data': [
        'views/sale_order_views.xml',
        'views/account_invoice_views.xml',
        'report/sale_report_templates.xml',
        'report/invoice_report_templates.xml'
    ]
}
