from odoo import models, fields
from odoo.addons import decimal_precision as dp

class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"

    # extend digits
    discount = fields.Float(string='Discount (%)', default=0.0, digits=dp.get_precision('Discount'))
