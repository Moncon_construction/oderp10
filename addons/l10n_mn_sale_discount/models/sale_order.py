# -*- coding: utf-8 -*-


from odoo import models, fields, api

from odoo.addons import decimal_precision as dp  # @UnresolvedImport


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.depends('order_line.price_total')
    def _amount_all(self):
        """
        Compute the total amounts of the SO.
        """
        for order in self:
            # BEGIN CHANGE
            amount_untaxed = amount_tax = amount_discount = 0.0
            # END CHANGE
            for line in order.order_line:
                amount_untaxed += line.price_subtotal
                # FORWARDPORT UP TO 10.0
                if order.company_id.tax_calculation_rounding_method == 'round_globally':
                    price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
                    taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty, product=line.product_id, partner=order.partner_shipping_id)
                    amount_tax += sum(t.get('amount', 0.0) for t in taxes.get('taxes', []))
                else:
                    amount_tax += line.price_tax
                # BEGIN CHANGE
                amount_discount += (line.product_uom_qty * line.price_unit * line.discount) / 100
                # END CHANGE
            order.update({
                'amount_untaxed': order.pricelist_id.currency_id.round(amount_untaxed),
                'amount_tax': order.pricelist_id.currency_id.round(amount_tax),
                # BEGIN CHANGE
                'amount_discount': order.pricelist_id.currency_id.round(amount_discount),
                # END CHANGE
                'amount_total': amount_untaxed + amount_tax,
            })

    amount_discount = fields.Monetary(string='Total Discount', store=True, readonly=True, compute='_amount_all', track_visibility='always')


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    discount_type = fields.Selection([('percent', 'Percentage'), ('amount', 'Amount')], string='Discount type', required=True, default='percent')
    discount_amount = fields.Float('Discount Amount', digits=dp.get_precision('Product Price'), default=0.0)
    # extend digits
    discount = fields.Float(string='Discount (%)', digits=dp.get_precision('Discount'), default=0.0)

    @api.onchange('discount_type')
    def onchange_discount_type(self):
        # clear discounts
        self.discount_amount = 0
        self.discount = 0

    @api.onchange('discount_amount')
    def onchange_discount_amount(self):
        sub_total = round((self.product_uom_qty * self.price_unit))

        # update discount amount
        if self.discount_amount > sub_total:
            self.discount_amount = sub_total

        # update discount
        if sub_total:
            self.discount = (self.discount_amount * 100) / sub_total
        else:
            self.discount = 0

    @api.onchange('product_id', 'price_unit', 'product_uom', 'product_uom_qty', 'tax_id')
    def _onchange_discount(self):
        # call super
        super(SaleOrderLine, self)._onchange_discount()

        # update discount on amount type
        if self.discount_type == 'amount':
            sub_total = round((self.product_uom_qty * self.price_unit))

            # update discount amount
            if self.discount_amount > sub_total:
                self.discount_amount = sub_total

            # update discount
            if sub_total:
                self.discount = (self.discount_amount * 100) / sub_total
            else:
                self.discount = 0

    @api.model
    def create(self, vals):
        # save discount
        discount_type = vals.get('discount_type')
        discount_amount = vals.get('discount_amount')
        product_uom_qty = vals.get('product_uom_qty')
        price_unit = vals.get('price_unit')

        if discount_type == 'percent':
            vals['discount_amount'] = 0
        elif discount_type == 'amount':
            sub_total = round((product_uom_qty * price_unit)) if price_unit is not None else 0
            if sub_total:
                vals['discount'] = (discount_amount * 100) / sub_total
            else:
                vals['discount'] = 0

        # call super
        return super(SaleOrderLine, self).create(vals)

    @api.multi
    def write(self, vals):
        # save discount
        if any(v in vals for v in ('discount_type', 'discount_amount', 'discount', 'product_uom_qty', 'price_unit')):
            discount_type = vals.get('discount_type')
            discount_amount = vals.get('discount_amount')
            product_uom_qty = vals.get('product_uom_qty')
            price_unit = vals.get('price_unit')
            for line in self:
                if discount_type is None:
                    discount_type = line.discount_type
                if discount_amount is None:
                    discount_amount = line.discount_amount
                if product_uom_qty is None:
                    product_uom_qty = line.product_uom_qty
                if price_unit is None:
                    price_unit = line.price_unit

                if discount_type == 'percent':
                    vals['discount_amount'] = 0
                elif discount_type == 'amount':
                    sub_total = round((product_uom_qty * price_unit))
                    if sub_total:
                        vals['discount'] = (discount_amount * 100) / round((product_uom_qty * price_unit))
                    else:
                        vals['discount'] = 0

        # call super
        return super(SaleOrderLine, self).write(vals)
