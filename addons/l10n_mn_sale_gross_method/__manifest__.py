# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Sale Gross Method",
    'version': '1.0',
    'depends': ['l10n_mn_sale','l10n_mn_stock'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Sale Discount Additional Features
    """,
    'data': [
        'views/product_category_views.xml',
        'views/stock_warehouse_views.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
