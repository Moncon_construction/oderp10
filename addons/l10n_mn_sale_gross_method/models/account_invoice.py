# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError
import odoo.addons.decimal_precision as dp

class AccountInvoiceTax(models.Model):
    _inherit = "account.invoice.tax"
    
    # Борлуулалтын хөнгөлөлт тооцоход ашиглах хөнгөлөгдөөгүй дүнгээс тооцсон татварын дүнг хадгалах талбар
    sale_gross_amount = fields.Monetary()
    
class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"
    
    # Борлуулалтын хөнгөлөлт тооцоход ашиглах хөнгөлөлтийн дүнг хадгалах талбар
    net_price_unit = fields.Float(digits=dp.get_precision('Product Price'))
    
class AccountInvoice(models.Model):
    _inherit = "account.invoice"
    
    @api.multi
    def get_taxes_values(self):
        # Дахин тодорхойлсон: Хэрвээ тухайн компанид Борлуулалтын тохиргоо байгаад уг нэхэмжлэл Борлуулалтаас үүсэх нэхэмжлэл бол доорхи функц ажиллана.
        # Хөнгөлөлтийг НИЙТ ДҮН-гийн аргаар тооцох үед журналын мөр үүсгэхдээ нэмэлт нэг татвар тооцоолох ба тухайн татварыг
        # тухайн мөрд харъяалагдах нэхэмжлэлийн мөрийн amount-с бус sale_gross_amount-с тооцох болсон тул уг талбарын утгыг дамжуулдаг болгов. 
        
        ##################  Борлуулалтын хөнгөлөлт тооцох хөгжүүлэлт - Тухайн Борлуулалтыг хөнгөлөлтөөр тооцохгүй бол core-н функциар ажиллах ЭХЛЭЛ #####################
        has_discount = False
        for line in self.invoice_line_ids:
            if line.discount > 0:
                has_discount = True
                break
              
        if not(self.company_id.sale_method and has_discount):
            if not self.type in ['out_invoice','out_refund']:
                return super(AccountInvoice, self).get_taxes_values()
        ##################  Борлуулалтын хөнгөлөлт тооцох хөгжүүлэлт - Тухайн Борлуулалтыг хөнгөлөлтөөр тооцож бол core-н функциар ажиллах ТӨГСГӨЛ #####################
        
        tax_grouped = {}
        for line in self.invoice_line_ids:
            taxes = line.invoice_line_tax_ids.compute_all(line.price_unit * (1 - (line.discount or 0.0) / 100.0), self.currency_id, line.quantity, line.product_id, self.partner_id)['taxes']
            ##################  Борлуулалтын хөнгөлөлт тооцох хөгжүүлэлт - НИЙТ ДҮН-н аргаар тооцож байгаа бол нэхэмжлэлээ журналын бичилт үүсгэхэд sale_gross_amount-г ашиглах тул нэмэж дамжуулав ЭХЛЭЛ #####################
            # gross_amount-г хөнгөлөгдөөгүй дүнгээс тооцно.
            taxes1 = line.invoice_line_tax_ids.compute_all(line.price_unit, self.currency_id, line.quantity, line.product_id, self.partner_id)['taxes']
            index = -1
            for tax in taxes:
                index += 1
                val = self._prepare_tax_line_vals(line, tax)
                val1 = self._prepare_tax_line_vals(line, taxes1[index])
                key = self.env['account.tax'].browse(tax['id']).get_grouping_key(val)
                
                tax['sale_gross_amount'] = val1['amount']
                
                if key not in tax_grouped:
                    tax_grouped[key] = val
                else:
                    tax_grouped[key]['amount'] += val['amount']
                    tax_grouped[key]['base'] += val['base']
                
                # sale_gross_amount-г дамжуулж байна.'
                if 'sale_gross_amount' in tax_grouped[key].keys():
                    tax_grouped[key]['sale_gross_amount'] += val1['amount']
                else:
                    tax_grouped[key]['sale_gross_amount'] = val1['amount']
            ##################  Борлуулалтын хөнгөлөлт тооцох хөгжүүлэлт - НИЙТ ДҮН-н аргаар тооцож байгаа бол нэхэмжлэлээ журналын бичилт үүсгэхэд sale_gross_amount-г ашиглах тул нэмэж дамжуулав ТӨГСГӨЛ #####################
                   
        return tax_grouped
        
    @api.model
    def tax_line_move_line_get(self):
        # Дахин тодорхойлсон: Хэрвээ тухайн компанид Болуулалтын хяналтын тохиргоо байгаад уг нэхэмжлэл болуулалтын үүсэх нэхэмжлэл бол доорхи функц ажиллана.
        # Нэхэмжлэлээс татварын журналын мөр үүсгэхэд ашиглана.
        
        # Хэрвээ нэхэмжлэлийн нэг л мөр дээр хөнгөлөлт байвал тухайн нэхэмжлэлийг нийтэд нь хөнгөлөлттэй гэж үзнэ
        has_discount = False
        for line in self.invoice_line_ids:
            if line.discount > 0:
                has_discount = True
                break
        
        if not (self.company_id.sale_method and has_discount) or not self.type in ['out_invoice','out_refund']:
            return super(AccountInvoice, self).tax_line_move_line_get()

        ################### ӨӨРЧЛӨЛТИЙН ЭХЛЭЛ #####################
        sale_method = self.company_id.sale_method
                
        res = []
        # keep track of taxes already processed
        done_taxes = []
        # loop the invoice.tax.line in reversal sequence
        
        credit_amount = 0
        for line in self.invoice_line_ids:
            discount_amount = line.price_unit * (line.discount or 0.0) / 100.0
            for line_tax in line.invoice_line_tax_ids:
                taxes = line_tax.compute_all(discount_amount, line.invoice_id.currency_id, 1, product=line.product_id, partner=line.invoice_id.partner_id)
                if 'taxes' in taxes.keys():
                    if taxes['taxes'] and len(taxes['taxes']) > 0:
                        for tax in taxes['taxes']:
                            credit_amount += tax['amount']*line.quantity
                                        
        for tax_line in sorted(self.tax_line_ids, key=lambda x: -x.sequence):
            if tax_line.amount:
                tax = tax_line.tax_id
                if tax.amount_type == "group":
                    for child_tax in tax.children_tax_ids:
                        done_taxes.append(child_tax.id)
                tax_price = tax_line.sale_gross_amount - credit_amount if sale_method != 0 and tax_line.sale_gross_amount else tax_line.amount
                res.append({
                    'invoice_tax_line_id': tax_line.id,
                    'tax_line_id': tax_line.tax_id.id,
                    'type': 'tax',
                    'name': tax_line.name,
                    # Хэрвээ цэвэр дүнгийн аргаар тооцох бол хөнгөлөгдсөн дүнг дамжуулна.
                    # Нийт дүнгийн аргаар бол кредит талдаа гарах НӨАТ-н дүнг Дебит талаас хасч тооцно
                    'net_price_unit': tax_line.sale_gross_amount - credit_amount,
                    'quantity': 1,
                    'price': tax_price ,
                    'account_id': tax_line.account_id.id,
                    'account_analytic_id': tax_line.account_analytic_id.id,
                    'invoice_id': self.id,
                    'tax_ids': [(6, 0, list(done_taxes))] if tax_line.tax_id.include_base_amount else [],
                    'partner_id': tax_line.partner_id.id if tax_line.partner_id else False
                })
                done_taxes.append(tax.id)
        return res
        ################### ӨӨРЧЛӨЛТИЙН ТӨГСГӨЛ #####################

    @api.model
    def invoice_line_move_line_get(self):
        # Нэхэмжлэлийн нийт татваргүй дүнгээс үүсэх журналын мөрийн тооцоололд ашиглана.

        ##################  Борлуулалтын хөнгөлөлт тооцох хөгжүүлэлт - Тухайн борлуулалтыг хөнгөлөлтөөр тооцохгүй бол core-н функциар ажиллах ЭХЛЭЛ #####################
        # Хэрвээ нэхэмжлэлийн нэг л мөр дээр хөнгөлөлт байвал тухайн нэхэмжлэлийг нийтэд нь хөнгөлөлттэй гэж үзнэ
        has_discount = False
        for line in self.invoice_line_ids:
            if line.discount > 0:
                has_discount = True
        
        sale_method = self.company_id.sale_method
        if not (sale_method and has_discount):
            if not self.type in ['out_invoice','out_refund']:
                return super(AccountInvoice, self).invoice_line_move_line_get()
        elif has_discount and not sale_method:
            raise UserError(_(u"Sorry. You cannot continue. Because you haven't proc setting for this company.\nPlease configure proc setting and set sale discount type!!!"))
        res = []
        for line in self.invoice_line_ids:
            if line.quantity==0:
                continue
            tax_ids = []
            for tax in line.invoice_line_tax_ids:
                tax_ids.append((4, tax.id, None))
                for child in tax.children_tax_ids:
                    if child.type_tax_use != 'none':
                        tax_ids.append((4, child.id, None))
            analytic_tag_ids = [(4, analytic_tag.id, None) for analytic_tag in line.analytic_tag_ids]

            price_unit, price = 0, 0
        ##################  Борлуулалтын хөнгөлөлт тооцох хөгжүүлэлт - Тухайн борлуулалтын хөнгөлөлтөөр тооцохгүй бол core-н функциар ажиллах ТӨГСГӨЛ #####################
            
        ################## Болуулалтын хөнгөлөлт тооцох хөгжүүлэлт - НИЙТ ДҮН-н аргаар тооцох бол хөнгөлөгдөөгүй дүнг авах ЭХЛЭЛ #####################
            if sale_method and sale_method == 0:
                # Цэвэр дүнгийн аргаар бодох бол хөнгөлөгдсөн дүнгээс нэгжийн үнийг тооцоолно
                price_unit = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            else:
                # Нийт дүнгийн аргаар бодох бол
                price_unit = line.price_unit
            
            price_subtotal = line.quantity * price_unit
            if line.invoice_line_tax_ids and line.discount != 100:
                currency = line.invoice_id and line.invoice_id.currency_id or None
                partner = line.invoice_id and line.invoice_id.partner_id or None
                taxes = line.invoice_line_tax_ids.compute_all(price_unit, currency, line.quantity, product=line.product_id, partner=partner)
                price_subtotal = taxes['total_excluded'] if taxes else price_subtotal
            # Кт-БОРЛУУЛАЛТЫН ОРЛОГО дансанд бичигдэнэ
            # эсвэл буцаалт бол Дт-БОРЛУУЛАЛТЫН БУЦААЛТ дансанд бичигдэнэ
            
            price = price_subtotal
            account = False
            if line.invoice_id.type == 'out_refund':
                # price = -price_subtotal
                # Барааны өртөг агуулах бүрт тусдаа дундаа хөтлөгдөх бол агуулахаас данс авах
                if self.is_module_installed("l10n_mn_stock_account_cost_for_each_wh"):
                    ref_inv_id = self.env['account.invoice'].search([('number', '=', self.origin)], limit=1)
                    sale_order_id = self.env['sale.order.line'].search([('invoice_lines', 'in', ref_inv_id.invoice_line_ids.ids)], limit=1).order_id
                    stock_picking_type = sale_order_id.stock_picking_type if sale_order_id else False
                    account = stock_picking_type.warehouse_id.sale_return_account if stock_picking_type and stock_picking_type.warehouse_id and stock_picking_type.warehouse_id.sale_return_account else False
                    if not account:
                        raise UserError(_(u"Please configure sale return account for warehouse %s!!!") %stock_picking_type.warehouse_id.name)
                else:
                    if not line.product_id.categ_id.sale_return_account:
                        raise UserError(_(u"Please configure sale return account in product %s's category!!!") %line.product_id)
                    else:
                        account = line.product_id.categ_id.sale_return_account
            move_line_dict = {
                'invl_id': line.id,
                'type': 'src',
                'name': line.name.split('\n')[0][:64],
                'price_unit': price_unit,
                'net_price_unit': line.price_unit * (1 - (line.discount or 0.0) / 100.0),
                'quantity': line.quantity,
                'price': price,
                'account_id': account.id if account else line.account_id.id,
                'product_id': line.product_id.id,
                'uom_id': line.uom_id.id,
                'account_analytic_id': line.account_analytic_id.id,
                'tax_ids': tax_ids,
                'invoice_id': self.id,
                'analytic_tag_ids': analytic_tag_ids
            }
            res.append(move_line_dict)
        return res
        ##################  Болуулалтын хөнгөлөлт тооцох хөгжүүлэлт - НИЙТ ДҮН-н аргаар тооцох бол хөнгөлөгдөөгүй дүнг авах ТӨГСГӨЛ #####################
        
    @api.multi
    def action_move_create(self):
        # Дахин тодорхойлсон
        # Хэрвээ Борлуулалтын хөнгөлөлтийг цэвэр/нийт дүнгийн аргаар тооцоолох бол журналын бичилт өөрөөр үүсэх тул дахин тодорхойлов.
         
        account_move = self.env['account.move']

        for inv in self:
            if not inv.journal_id.sequence_id:
                raise UserError(_('Please define sequence on the journal related to this invoice.'))
            if not inv.invoice_line_ids:
                raise UserError(_('Please create some invoice lines.'))
            if inv.move_id:
                continue

            ctx = dict(self._context, lang=inv.partner_id.lang)

            if not inv.date_invoice:
                inv.with_context(ctx).write({'date_invoice': fields.Date.context_today(self)})
            company_currency = inv.company_id.currency_id

            # create move lines (one per invoice line + eventual taxes and analytic lines)
            iml = inv.invoice_line_move_line_get()
            iml += inv.tax_line_move_line_get()
            diff_currency = inv.currency_id != company_currency
            # create one move line for the total and possibly adjust the other lines amount
            
            total, total_currency, iml = inv.with_context(ctx).compute_invoice_totals(company_currency, iml)
            
            ##################################### Дахин тодорхойлолтод нэмэлтээр шалгав. ЭХЛЭЛ #####################################
            # Хэрвээ хөнгөлөлтийн тохиргоо байгаад хөнгөлөлттэйгөөр тооцох бол total атрибутын утга нь нийт хөнгөлөгдсөн дүн байна.
            has_discount = False
            for line in self.invoice_line_ids:
                if line.discount > 0:
                    has_discount = True
                    break
                
            if self.company_id.sale_method and has_discount and self.type in ['out_invoice','out_refund']:
                total = 0
                for line in inv.invoice_line_move_line_get():
                    if self.type == 'out_invoice':
                        total -= (line['net_price_unit'] * line['quantity'])
                    else: 
                        total += (line['net_price_unit'] * line['quantity'])
            ##################################### Дахин тодорхойлолтод нэмэлтээр шалгав. ТӨГСГӨЛ #####################################
            
            # Дт - ДАНСНЫ АВЛАГА журналын бичилт хийх хэсэг
            name = inv.name or '/'
            if inv.payment_term_id:
                totlines = inv.with_context(ctx).payment_term_id.with_context(currency_id=company_currency.id).compute(total, inv.date_invoice)[0]
                res_amount_currency = total_currency
                ctx['date'] = inv._get_currency_rate_date()
                for i, t in enumerate(totlines):
                    if inv.currency_id != company_currency:
                        amount_currency = company_currency.with_context(ctx).compute(t[1], inv.currency_id)
                    else:
                        amount_currency = False

                    # last line: add the diff
                    res_amount_currency -= amount_currency or 0
                    if i + 1 == len(totlines):
                        amount_currency += res_amount_currency
                    price = -t[1]
                    if inv.type == 'out_refund':
                        price = t[1]
                    iml.append({
                        'type': 'dest',
                        'name': name,
                        'price': price,
                        'account_id': inv.account_id.id,
                        'date_maturity': t[0],
                        'amount_currency': diff_currency and amount_currency,
                        'currency_id': diff_currency and inv.currency_id.id,
                        'invoice_id': inv.id
                    })
            elif not has_discount:
                price = total
                iml.append({
                    'type': 'dest',
                    'name': name,
                    'price': price,
                    'account_id': inv.account_id.id,
                    'date_maturity': inv.date_due,
                    'amount_currency': diff_currency and total_currency,
                    'currency_id': diff_currency and inv.currency_id.id,
                    'invoice_id': inv.id
                })
            elif has_discount:
                price = -total
                iml.append({
                    'type': 'dest',
                    'name': name,
                    'price': price,
                    'account_id': inv.account_id.id,
                    'date_maturity': inv.date_due,
                    'amount_currency': diff_currency and total_currency,
                    'currency_id': diff_currency and inv.currency_id.id,
                    'invoice_id': inv.id
                })
                    
            ##################################### Дахин тодорхойлолтод нэмэлтээр журналын мөр нэмэв. ЭХЛЭЛ #####################################
            # Хөнгөлөлтийг НИЙТ ДҮН-гийн аргаар тооцох бол дараахи шалгалтын дагуу нэмэлт 2 журналын мөр үүснэ.
            if has_discount:
                #Борлуулалтын захиалгын хөнгөлөлт
                if self.company_id.sale_method == 1:
                    if self.type in ['out_refund','out_invoice']:
                        sale_method = self.company_id.sale_method
                        
                        if not sale_method:
                            raise UserError(_(u"Please configure sale discount type for proc setting!!!"))
                    
                        if sale_method != 0:
                            account = False
                            # Барааны өртөг агуулах бүрт тусдаа дундаа хөтлөгдөх бол агуулахаас данс авах
                            if self.is_module_installed("l10n_mn_stock_account_cost_for_each_wh"):
                                if self.type =='out_refund':
                                    ref_inv_id = self.env['account.invoice'].search([('number', '=', self.origin)], limit=1)
                                    sale_order_id = self.env['sale.order.line'].search([('invoice_lines', 'in', ref_inv_id.invoice_line_ids.ids)], limit=1).order_id
                                elif self.type == 'out_invoice':
                                    sale_order_id = self.env['sale.order.line'].search([('invoice_lines', 'in', self.invoice_line_ids.ids)], limit=1).order_id
                                stock_picking_type = sale_order_id.stock_picking_type if sale_order_id else False
                                account = stock_picking_type.warehouse_id.sale_discount_account if stock_picking_type and stock_picking_type.warehouse_id and stock_picking_type.warehouse_id.sale_discount_account else False
                                if not account:
                                    raise UserError(_(u"Please configure sale discount account for warehouse %s!!!") %stock_picking_type.warehouse_id.name)

                            for line in self.invoice_line_ids:
                                if not self.is_module_installed("l10n_mn_stock_account_cost_for_each_wh"):
                                    # Барааны өртөг агуулах дундаа хөтлөгдөх бол барааны ангилалаас данс авах
                                    if not line.product_id.categ_id.sale_discount_account:
                                        raise UserError(_(u"Please configure sale discount account in product %s's category!!!") %line.product_id)
                                    else:
                                        account = line.product_id.categ_id.sale_discount_account  

                                if line.quantity==0:
                                    continue        
                                tax_ids = []
                                for tax in line.invoice_line_tax_ids:
                                    tax_ids.append((4, tax.id, None))
                                    for child in tax.children_tax_ids:
                                        if child.type_tax_use != 'none':
                                            tax_ids.append((4, child.id, None))
                                analytic_tag_ids = [(4, analytic_tag.id, None) for analytic_tag in line.analytic_tag_ids]
                    
                                discount_amount = line.price_unit * (line.discount or 0.0) / 100.0
                            
                                tax_amount = 0
                                sub_amount = 0
                                # Дт-Борлуулалтын хөнгөлөлт дансны бичилт
                                # Татвартай үеийн хөнгөлөлт
                                if line.invoice_line_tax_ids and line.discount != 100:
                                    for line_tax in line.invoice_line_tax_ids:
                                        taxes = line_tax.compute_all(discount_amount, line.invoice_id.currency_id, 1, product=line.product_id, partner=line.invoice_id.partner_id)
                                        if line_tax.price_include:
                                            sub_amount += taxes['total_excluded']
                                        else:
                                            sub_amount += taxes['total_included']    
                                        # Хөнгөлөлтийн данс байна
                                        iml.append({
                                            'type': 'dest',
                                            'name': account.name,
                                            'price_unit': -taxes['total_excluded'],
                                            'quantity': line.quantity,
                                            'price': sub_amount*line.quantity if self.type == 'out_invoice' else -sub_amount*line.quantity,
                                            'account_id': account.id,
                                            'product_id': line.product_id.id,
                                            'uom_id': line.uom_id.id,
                                            'account_analytic_id': line.account_analytic_id.id,
                                            'date_maturity': line.invoice_id.date_due,
                                            'tax_ids': tax_ids,
                                            'invoice_id': line.invoice_id.id
                                        })
                                # Татваргүй үеийн хөнгөлөлт
                                else:
                                    if self.type == 'out_invoice':
                                        price = line.quantity * line.price_unit - line.quantity*(line.price_unit * (1 - (line.discount or 0.0) / 100.0))
                                    elif self.type == 'out_refund':
                                        price = -((line.quantity * line.price_unit) - line.quantity*(line.price_unit * (1 - (line.discount or 0.0) / 100.0)))
                                    # Хөнгөлөлтийн данс байна
                                    iml.append({
                                        'type': 'dest',
                                        'name': account.name,
                                        'price_unit': line.price_unit * (1 - (line.discount or 0.0) / 100.0),
                                        'quantity': line.quantity,
                                        'price':  price,
                                        'account_id': account.id,
                                        'product_id': line.product_id.id,
                                        'uom_id': line.uom_id.id,
                                        'account_analytic_id': line.account_analytic_id.id,
                                        'date_maturity': line.invoice_id.date_due,
                                        'tax_ids': False,
                                        'invoice_id': line.invoice_id.id
                                    })
                ##################################### Дахин тодорхойлолтод нэмэлтээр журналын мөр нэмэв. ТӨГСГӨЛ #####################################
                        
            part = self.env['res.partner']._find_accounting_partner(inv.partner_id)
            lines = []
            for iml_line in iml:
                move_line = self.line_get_convert(iml_line, part.id)
                if not (not move_line['debit'] and not move_line['credit']):
                    lines.append(move_line)

            line = [(0, 0, l) for l in lines]
            line = inv.group_lines(iml, line)
            journal = inv.journal_id.with_context(ctx)
#             line = inv.finalize_invoice_move_lines(line)
            date = inv.date or inv.date_invoice
            move_vals = {
                'ref': inv.reference,
                'line_ids': line,
                'journal_id': journal.id,
                'date': date,
                'narration': inv.comment,
            }
            ctx['company_id'] = inv.company_id.id
            ctx['invoice'] = inv
            ctx_nolang = ctx.copy()
            ctx_nolang.pop('lang', None)
            move = account_move.with_context(ctx_nolang).create(move_vals)
            # Pass invoice in context in method post: used if you want to get the same
            # account move reference when creating the same invoice after a cancelled one:
            move.post()
            # make the invoice point to that move
            vals = {
                'move_id': move.id,
                'date': date,
                'move_name': move.name,
                'old_number': move.name,
            }
            inv.with_context(ctx).write(vals)
            # l10n_mn_purchase_asset модуль суусан бол хөрөнгө үүсгэхгүй
            if not self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_purchase_asset'), ('state', 'in', ('installed', 'to upgrade'))]):
                context = dict(self.env.context)
                # Энэхүү context нь хөрөнгийх биш нэхэмжлэлийх тул устгагдах ёстой.
                # Үгүй бол нэхэмжлэлийн төрлөөр хөрөнгө үүсгэх гэж оролдох болно.
                context.pop('default_type', None)
                inv.invoice_line_ids.with_context(context).asset_create()
        return True
            
    def is_module_installed(self, module_name):
        self._cr.execute("SELECT id FROM ir_module_module WHERE name = '%s' AND state IN ('installed', 'to upgrade')" %module_name)
        results = self._cr.dictfetchall()
        
        if results and len(results) > 0:
            return True
        else:
            return False