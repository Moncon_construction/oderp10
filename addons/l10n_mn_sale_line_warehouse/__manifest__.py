# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Sale line Warehouse",
    'version': '1.0',
    'depends': ['l10n_mn_sale', 'l10n_mn_sale_stock', 'l10n_mn_account_tax_report_sale'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Sale Discount Additional Features
    """,
    'data': [
        'views/sale_order_view.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
