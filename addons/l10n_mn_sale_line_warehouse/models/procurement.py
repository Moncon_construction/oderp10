# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, registry, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare, float_round

import logging

_logger = logging.getLogger(__name__)


class ProcurementOrder(models.Model):
    _inherit = "procurement.order"

    def _get_stock_move_values(self):
        res = super(ProcurementOrder, self)._get_stock_move_values()
        res.update({'picking_type_id': self.sale_line_id.line_picking_type_id.id if self.sale_line_id else self.rule_id.picking_type_id.id,
                    'location_id': self.sale_line_id.line_picking_type_id.default_location_src_id.id if self.sale_line_id else self.rule_id.picking_type_id.default_location_src_id.id,
                    'warehouse_id': self.sale_line_id.line_warehouse.id if self.sale_line_id else self.rule_id.propagate_warehouse_id.id or self.rule_id.warehouse_id.id})
        return res

