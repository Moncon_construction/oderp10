# -*- coding: utf-8 -*-


from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import pytz

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'



    # Борлуулалтын захилага дээр сонгосон агуулахыг тогтмолоор авна

    line_warehouse = fields.Many2one('stock.warehouse', string='Warehouse')
    line_picking_type_id = fields.Many2one('stock.picking.type', string='Гарах байрлал',
                                           domain="[('code', '=', 'outgoing'),('warehouse_id','=',line_warehouse)]")


    @api.onchange('line_warehouse')
    def _onchange_line_warehouse(self):
        domain = {}
        _warehouses = []
        if self.line_warehouse:
            line_picking_type_id = self.env['stock.picking.type'].search([('warehouse_id', '=', self.line_warehouse.id), ('code', '=', 'outgoing')],
                                                                         limit=1)
            if line_picking_type_id:
                self.line_picking_type_id = line_picking_type_id.id

        for warehouse in self.env.user.allowed_warehouses:
            _warehouses.append(warehouse.id)
        if _warehouses:
            domain['line_warehouse'] = [('id', 'in', _warehouses)]
        return {'domain': domain}


    def _check_routing(self):
        """ Verify the route of the product based on the warehouse
            return True if the product availibility in stock does not need to be verified,
            which is the case in MTO, Cross-Dock or Drop-Shipping
        """
        is_available = False
        product_routes = self.route_id or (self.product_id.route_ids + self.product_id.categ_id.total_route_ids)

        # Check MTO
        wh_mto_route = self.line_warehouse.mto_pull_id.route_id
        if wh_mto_route and wh_mto_route <= product_routes:
            is_available = True
        else:
            mto_route = False
            try:
                mto_route = self.env['stock.warehouse']._get_mto_route()
            except UserError:
                # if route MTO not found in ir_model_data, we treat the product as in MTS
                pass
            if mto_route and mto_route in product_routes:
                is_available = True

        # Check Drop-Shipping
        if not is_available:
            for pull_rule in product_routes.mapped('pull_ids'):
                if pull_rule.picking_type_id.sudo().default_location_src_id.usage == 'supplier' and \
                        pull_rule.picking_type_id.sudo().default_location_dest_id.usage == 'customer':
                    is_available = True
                    break

        return is_available

    @api.depends('product_id', 'line_warehouse')
    def _compute_available_qty(self):
        move_qty = 0
        qty = 0
        for obj in self:
            warehouse = obj.line_warehouse or obj.order_id.warehouse_id
            if not warehouse:
                raise UserError(_('Warning!\nYou must select supply warehouse before add order line!'))
            if obj.product_id:
                locations = self.env['stock.location'].search([('usage', '=', 'internal'), ('location_id', 'child_of', [warehouse.view_location_id.id])])
                loc_ids = [loc.id for loc in locations or []]
                obj.available_qty = obj.product_id.get_qty_availability(loc_ids, obj.order_id.date_order, state=['done'])

                #Борлуулалт өөрийнхөө нөөцөлснйг давхар нөөцөлсөн гэж харуулаад байсан тул өөрийнх нь нөөцлөлтийг тооцдоггүй болгов
                self_assigned_qty = 0
                if obj.id:
                    date = ("AND m.date <= '%s'" % obj.order_id.date_order) if self.env.user.company_id.availability_check_date == 'record_date' else ''
                    self._cr.execute("""select m.product_qty as qty from stock_move m, procurement_order o where m.procurement_id=o.id and m.state='assigned' %s and o.sale_line_id=%s""" % (date, obj.id))
                    results = self._cr.dictfetchall()
                    if results and len(results) > 0 and results[0]['qty']:
                        self_assigned_qty = results[0]['qty']
                    
                obj.assigned_qty = obj.product_id.get_qty_availability(loc_ids, obj.order_id.date_order, state=['assigned']) + self_assigned_qty
            else:
                obj.available_qty = 0
                obj.assigned_qty = 0

    available_qty = fields.Float(compute='_compute_available_qty', string='Available quantity', readonly=True)
