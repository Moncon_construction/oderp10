# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Sale Loyalty Card",
    'version': '1.0',
    'depends': ['l10n_mn_sale'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Loyalty Card Features
    """,
    'data': [
        'security/loyalty_card_security.xml',
        'security/ir.model.access.csv',
        'views/sale_loyalty_card_config_view.xml',
        'views/sale_loyalty_card_view.xml',
        'data/loyalty_card_sequence.xml',
        'data/loyalty_card_account_template.xml',
        'data/loyalty_card_journal.xml',
        'views/res_partner_view.xml',
        'views/loyalty_card_create_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
