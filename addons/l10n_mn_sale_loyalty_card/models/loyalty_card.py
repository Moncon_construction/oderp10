# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime
from dateutil.relativedelta import *


class LoyaltyCardType(models.Model):
    _name = "loyalty.card.type"

    name = fields.Char(string="Name", required=True, help="Name of the Card type")
    calculation = fields.Selection([], required=True, string='Calculation')
    is_owner_changeable = fields.Boolean(string='Is Owner Changeable', default=False)
    has_co_owner = fields.Boolean(string='Has Co-Owner', default=False)
    continue_date = fields.Integer(string="Continue date(by months)", default=0, required=True)
    card_date = fields.Selection([('by_active_date', 'Active Date'),
                                  ('last_purchase_date', 'By last purchase date'),
                                  ('end_date', 'By End Date')], default="by_active_date", required=True)
    sequence = fields.Char(string='Sequence', readonly=True)
    type_sequence = fields.Many2one('ir.sequence', readonly=True, store=True, string="Type Sequence")
    maximum_amount_percentage = fields.Integer(string="maximum amount %",
                                               help="The percentage of amount that can be paid by this card if \n its 0 than card can pay 100% of amount")

    @api.model
    def create(self, vals):
        # sequence талбарт үндсэн төрлийн код оноож, дэд төрлийн sequence үүсгэж оноож өгч байна

        vals['sequence'] = self.env["ir.sequence"].next_by_code("mongolian.sale.loyalty.card.sequence")
        if vals['maximum_amount_percentage'] == 0:
            vals['maximum_amount_percentage'] = 100
        seq_obj = self.env['ir.sequence']
        seq_vals = {
            'padding': 8,
            'code': "mongolian.sale.loyalty.card.sequence.type." + vals['name'],
            'name': ('Loyalty_card(%s)' % vals['name']),
            'implementation': 'standard',
            'company_id': 1,
            'use_date_range': False,
            'number_increment': 1,
            'prefix': vals['sequence'],
            'date_range_ids': [],
            'number_next_actual': 0,
            'active': True,
            'suffix': False
        }
        if vals['maximum_amount_percentage']>100:
            raise UserError(_("Maximum percentage cannot be higher than 100!"))

        seq_id = seq_obj.sudo().create(seq_vals)
        vals.update({'type_sequence': seq_id.id})

        #continue_date 0 байх ёсгүй
        if vals['continue_date'] <= 0:
            raise UserError(_('Continue date cannot be 0!'))
        return super(LoyaltyCardType, self).create(vals)

    @api.multi
    def write(self, vals):
        #continue_date 0 байх ёсгүй
        if 'continue_date' in vals:
            if vals['continue_date'] <= 0:
                raise UserError(_('Continue date cannot be 0!'))
        if self.maximum_amount_percentage == 0:
            vals['maximum_amount_percentage'] = 100

        if 'maximum_amount_percentage' in vals:
            if vals['maximum_amount_percentage'] > 100:
                raise UserError(_("Maximum percentage cannot be higher than 100!"))
            if vals['maximum_amount_percentage'] == 0:
                vals['maximum_amount_percentage'] = 100
        if 'name' in vals:
            seq_vals = {
                'code' : "mongolian.sale.loyalty.card.sequence.type." + vals['name'],
                'name': ('Loyalty_card(%s)' % vals['name'])}
            self.type_sequence.write(seq_vals)
        return super(LoyaltyCardType, self).write(vals)

    @api.multi
    def unlink(self):
        self.type_sequence.sudo().unlink()
        return super(LoyaltyCardType, self).unlink()


class LoyaltyCard(models.Model):
    _name = "loyalty.card.card"
    _rec_name = 'card_number'

    card_type = fields.Many2one('loyalty.card.type', string='Loyalty Card Type', required=True)
    active_from = fields.Date(string='Active from')
    active_to = fields.Date(string='Active to', compute="_compute_active_to", store=True)
    active_to_date = fields.Date()
    issue_date = fields.Date(string='Issued date')
    cancelled_date = fields.Date(string='Cancelled date')
    cancelled_reason = fields.Char(string="Cancelled reason")
    card_number = fields.Char(string="Card number", required=True)
    password = fields.Char(string="password", help="last 4 digit of holder's phone number by default")
    card_card_date = fields.Boolean(compute='_compute_card_date')
    primary_holder = fields.Many2one('res.partner', required=True, string="Card holder")
    other_holders = fields.Many2many('res.partner', string='Other holders')
    state = fields.Selection(
        [('draft', 'Open'), ('active', 'Active'), ('usable', 'Usable'), ('cancelled', 'Cancelled')], readonly=True,
        required=True, default='draft')
    is_created = fields.Boolean(default=False)
    type_owner_changeable = fields.Boolean(compute="check_owner_changeable")
    type_co_owner = fields.Boolean(compute="check_co_owner")
    card_type_string = fields.Char(string='Card Balance', compute="_compute_card_type")
    history_ids = fields.One2many('loyalty.card.history', 'loyalty_card_id', string='History')

    def _compute_card_type(self):
        for obj in self:
            obj.card_type_string = obj.card_type.calculation

    def buy(self, order_name=None, total_amount=None, sale_percentage=None, sale_amount=None, pay_amount=None, bonus_added=None, bonus_spent=None):
        history = self.env['loyalty.card.history']
        vals = {
            'loyalty_card_id': self.id,
            'transaction': order_name,
            'total_amount': total_amount,
            'sale_percentage': sale_percentage,
            'sale_amount': sale_amount,
            'pay_amount': pay_amount,
            'bonus_added': bonus_added,
            'bonus_spent': bonus_spent,
            'history_date': datetime.now()
        }
        history = history.create(vals)
        return history.id

    @api.depends('card_type')
    def check_co_owner(self):
        for obj in self:
            if obj.card_type:
                obj.type_co_owner = obj.card_type.has_co_owner

    @api.model
    def create(self, vals):
        #үүссэн эсэхийг мэдэх
        vals['is_created'] = True
        # password дотор үсэг байгаа эсэхийг шалгаж байна.
        if 'password' in vals:
            if not vals['password'].isnumeric():
                raise UserError(_('Please insert only four digit numbers in password!'))
            if len(vals['password']) != 4:
                raise UserError(_('Please insert only four digit numbers in password!'))
        return super(LoyaltyCard, self).create(vals)

    @api.depends('card_type')
    def check_owner_changeable(self):
        for obj in self:
            if obj.card_type.is_owner_changeable:
                self.type_owner_changeable = True
            else:
                self.type_owner_changeable = False

    @api.multi
    def write(self, vals):
        #card_owner өөрчлөгдсөн эсэхийг шалгаж байна
        for obj in self:
            if 'active_to' in vals:
                if obj.active_from >= vals['active_to']:
                    raise UserError(_('Active to date cannot be lower than active from'))
                else:
                    vals['active_to_date'] = vals['active_to']

            if not obj.card_type.is_owner_changeable:
                vals['is_created'] = False
            else:
                vals['is_created'] = True

        # password дотор үсэг орсон эсэхийг шалгаж байна
        if 'password' in vals:
            if not vals['password'].isnumeric():
                raise UserError(_('Please insert only four digit numbers in password!'))
            if len(vals['password']) != 4:
                raise UserError(_('Please insert only four digit numbers in password!'))
        return super(LoyaltyCard, self).write(vals)

    @api.onchange('card_type')
    def card_number_change(self):
        for obj in self:
            if obj.card_type:
                obj.card_number = obj.env["ir.sequence"].next_by_code(self.card_type.type_sequence.code)

    @api.onchange('primary_holder')
    def onchange_cardholder(self):
        for obj in self:
            if obj.primary_holder:
                if obj.primary_holder.mobile:
                    obj.password = obj.primary_holder.mobile[-4:]


    @api.multi
    def set_to_active(self):
        active_from = datetime.today()
        self.active_to_date = None
        self.primary_holder.is_card_partner = True
        if self.other_holders:
            for obj in self.other_holders:
                obj.is_card_partner = True
        self.write({'state': 'active', 'active_from': active_from})

    @api.multi
    def set_to_usable(self):
        if not self.active_to:
            raise UserError(_('Active to field cannot be empty!!'))
        else:
            self.write({'state': 'usable', 'issue_date': datetime.today()})

    def check_is_partner(self):
        card_partner = False
        cards = self.env['loyalty.card.card'].search([('primary_holder', '=', self.primary_holder.id)])
        if cards:
            for card in cards:
                if card != self and card.state in ["active", "usable"]:
                    card_partner = True
                    break

        cards = self.env['loyalty.card.card'].search([('other_holders', '=', self.primary_holder.id)])
        if cards:
            for card in cards:
                if card != self and card.state in ["active", "usable"]:
                    card_partner = True
                    break
        self.primary_holder.is_card_partner = card_partner

        for obj in self.other_holders:
            card_partner = False
            cards = self.env['loyalty.card.card'].search([('primary_holder', '=', obj.id)])
            if cards:
                for card in cards:
                    if card != self and card.state in ["active", "usable"]:
                        card_partner = True
                        break

            cards = self.env['loyalty.card.card'].search([('other_holders', '=', obj.id)])
            if cards:
                for card in cards:
                    if card != self and card.state in ["active", "usable"]:
                        card_partner = True
                        break
            obj.is_card_partner = card_partner


    @api.multi
    def set_to_cancelled(self):
        for obj in self:
            obj.check_is_partner()
            view = obj.env.ref('l10n_mn_sale_loyalty_card.loyalty_card_cancel_view')
            return {
                'name': _('Cancelled Reason'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'loyalty.card.cancel',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new'
            }

    def expire_card(self):
        self.check_is_partner()
        self.write({'state': 'cancelled', 'cancelled_reason': 'Хугацаа дууссан'})


    @api.multi
    def set_to_draft(self):
        self.check_is_partner()
        self.write({'state': 'draft', 'cancelled_date': False})

    @api.multi
    def unlink(self):
        for obj in self:
            if obj.state not in ['draft', 'cancelled']:
                raise UserError(_('You can only delete when card is in Draft or Cancelled state'))
        return super(LoyaltyCard, self).unlink()

    @api.multi
    @api.depends("active_from")
    def _compute_active_to(self):
        for obj in self:
            if obj.active_from:
                if obj.card_type.card_date == "by_active_date":
                    obj.active_to = (datetime.strptime(obj.active_from, '%Y-%m-%d')
                                     + relativedelta(months=+obj.card_type.continue_date))
                elif obj.card_type.card_date == "last_purchase_date":
                    last_purchase = obj.env['loyalty.card.history'].search([('loyalty_card_id', '=', obj.id)]
                                                                           , order='create_date DESC', limit=1)
                    if last_purchase:
                        obj.active_to = (datetime.strptime(last_purchase.create_date, '%Y-%m-%d %H:%M:%S')
                                     + relativedelta(months=+obj.card_type.continue_date))
                    else:
                        obj.active_to = (datetime.strptime(obj.active_from, '%Y-%m-%d')
                                         + relativedelta(months=+obj.card_type.continue_date))
                elif obj.card_type.card_date == "end_date":
                    if obj.active_to_date:
                        obj.active_to = obj.active_to_date
                if obj.active_to and datetime.strptime(obj.active_to, '%Y-%m-%d') < datetime.now():
                    obj.expire_card()  # Картны хугацаа дууссан эсэхийг шалгаж байна

    def _compute_card_date(self):
        for obj in self:
            if obj.card_type.card_date == "end_date":
                obj.card_card_date = True


class LoyaltyCardCancel(models.Model):
    _name = "loyalty.card.cancel"

    cancelled_reason = fields.Char(string="Cancelled reason", required=True)

    def set_to_cancelled(self):
        card_id = self.env['loyalty.card.card'].search([('id', '=',self.env.context.get('active_ids'))])
        card_id.cancelled_reason = self.cancelled_reason
        card_id.write({'state': 'cancelled', 'cancelled_date': datetime.today()})