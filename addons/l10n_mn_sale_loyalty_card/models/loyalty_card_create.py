# -*- coding: utf-8 -*-

import logging
import base64
import xlrd
from odoo import fields, models, _  # @UnresolvedImport
from tempfile import NamedTemporaryFile
from odoo.exceptions import UserError, ValidationError  # @UnresolvedImport

_logger = logging.getLogger(__name__)

class LoyaltyCardCreateTool(models.TransientModel):
    _name = 'loyalty.card.create.tool'

    data_file = fields.Binary('Excel File')
    date = fields.Datetime('Date', default=fields.Date.context_today)
    loyalty_card_type_id = fields.Many2one('loyalty.card.type')
    loyalty_card_type = fields.Selection('Loyalty Card Type', related="loyalty_card_type_id.calculation")

    def process(self):
        _logger = logging.getLogger('jacara')
        wiz = self.browse(self.id)
        try:
            fileobj = NamedTemporaryFile('w+', delete=False)
            fileobj.write(base64.decodestring(wiz.data_file))
            fileobj.seek(0)
            file_name = fileobj.name
            book = xlrd.open_workbook(file_name)
        except:
            raise UserError(_('Error loading data file. Please try again!'))
        return book