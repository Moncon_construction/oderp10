# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime
from dateutil.relativedelta import *


class LoyaltyCardHistory(models.Model):
    _name = "loyalty.card.history"
    _order = 'history_date desc'

    loyalty_card_id = fields.Many2one('loyalty.card.card', string="Loyalty Card")
    transaction = fields.Char(string="Transaction", readonly=True)
    total_amount = fields.Float(string="Total Amount")
    sale_percentage = fields.Float(string="Sale Percentage")
    sale_amount = fields.Float(string="Sale Amount")
    pay_amount = fields.Float(string="Pay Amount")
    bonus_spent = fields.Float(string="Bonus spent")
    bonus_added = fields.Float(string="Bonus added")
    history_date = fields.Datetime(string='History date', default=datetime.now())
