# -*- coding: utf-8 -*-
from odoo import fields, models, _

class ResCompany(models.Model):
    _inherit = "res.company"

    module_l10n_mn_sale_loyalty_card_by_buy_amount = fields.Boolean(string="Bonus Card", default = False)
    module_l10n_mn_sale_loyalty_card_discount = fields.Boolean(string="Discount Card", default = False)
    module_l10n_mn_sale_loyalty_card_gift = fields.Boolean(string="Gift Card", default = False)
    module_l10n_mn_sale_loyalty_card_reward_point = fields.Boolean(string="Reward Point Card", default = False)
    module_l10n_mn_sale_loyalty_card_debit = fields.Boolean(string="Loyalty Debit Card", default = False)