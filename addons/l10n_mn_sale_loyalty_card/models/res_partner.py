# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime
from dateutil.relativedelta import *


class ResPartner(models.Model):
    _inherit = 'res.partner'

    card_count = fields.Integer(string="Card Count", compute="_compute_card_count")
    is_card_partner = fields.Boolean(string="Card Partner")

    @api.multi
    def _compute_card_count(self):
        for obj in self:
            obj.card_count = obj.env['loyalty.card.card'].search_count([('primary_holder', '=', obj.id)])
            obj.card_count += obj.env['loyalty.card.card'].search_count([('other_holders', '=', obj.id)])

    @api.multi
    def button_is_partner(self):

        #        ухаалаг даруул дээр дарахад дуудагдах функц
        for obj in self:
            return {
                'type': 'ir.actions.act_window',
                'name': _('Loyalty Cards'),
                'res_model': 'loyalty.card.card',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'view_id': False,
                'domain': ['|', ('primary_holder', '=', obj.id), ('other_holders', '=', obj.name)],
            }