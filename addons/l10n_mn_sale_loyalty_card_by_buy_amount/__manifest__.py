# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Sale Loyalty Bonus Card",
    'version': '1.0',
    'depends': ['l10n_mn_sale_loyalty_card', 'web_readonly_bypass'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Loyalty Card Features
    """,
    'data': [
        'views/res_company.xml',
        'views/loyalty_card_by_buy_amount.xml',
        'views/loyalty_card_create_view.xml',
        'views/sale_view.xml',
        'views/account_invoice_view.xml',
        'views/account_journal_view.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}