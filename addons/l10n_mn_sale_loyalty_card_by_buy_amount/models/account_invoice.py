# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.tools import float_is_zero
from datetime import datetime

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    loyalty_card_id = fields.Many2one('loyalty.card.card', 'Loyalty Card')
    used_bonus = fields.Float('Used Bonus', compute='_compute_bonus')
    added_bonus = fields.Float('Added Bonus', compute='_compute_bonus')
    card_percent = fields.Float('Card Percent')

    @api.multi
    def _compute_bonus(self):
        for obj in self:
            round_curr = obj.currency_id.round
            obj.added_bonus = sum(round_curr(line.added_bonus) for line in obj.invoice_line_ids)
            obj.used_bonus = sum(round_curr(line.used_bonus) for line in obj.invoice_line_ids)

    @api.multi
    def get_taxes_values(self):
        tax_grouped = {}
        round_curr = self.currency_id.round
        for line in self.invoice_line_ids:
            # BEGIN
            price_unit = line.price_unit * (1 - (line.discount or 0.0) / 100.0) - line._get_per_add_bonus(bonus_type='add')
            # END
            taxes = line.invoice_line_tax_ids.compute_all(price_unit, self.currency_id, line.quantity, line.product_id,
                                                          self.partner_id)['taxes']
            for tax in taxes:
                val = self._prepare_tax_line_vals(line, tax)
                key = self.env['account.tax'].browse(tax['id']).get_grouping_key(val)

                if key not in tax_grouped:
                    tax_grouped[key] = val
                    tax_grouped[key]['base'] = round_curr(val['base'])
                else:
                    tax_grouped[key]['amount'] += val['amount']
                    tax_grouped[key]['base'] += round_curr(val['base'])
        return tax_grouped

    @api.model
    def invoice_line_move_line_get(self):
        res = super(AccountInvoice, self).invoice_line_move_line_get()
        for line in self.invoice_line_ids:
            used_bonus = line.used_bonus
            # Бонус карттай харилцагчдад борлуулалт хийгдсэн бол бонусын дүнг дансанд бичнэ.
            added_bonus = line.added_bonus
            if line.invoice_id.loyalty_card_id and added_bonus > 0.0 and line.product_id:
                b = line.invoice_id.company_id.property_credit_account_id
                if not b:
                    raise UserError(
                        _("There is no defined sales bonus credit account for this card type: '%s' (id:%d)") %
                        (line.invoice_id.loyalty_card_id.card_type.name,
                         line.invoice_id.loyalty_card_id.card_type,))
                bonus_add_line = line.move_line_added_item(added_bonus, b, line)
                if bonus_add_line:
                    res.append(bonus_add_line)

                if used_bonus > 0.0:
                    c = line.invoice_id.company_id.property_debit_account_id
                    if not c:
                        raise UserError(
                            _("There is no defined sales bonus debit account for this card type: '%s' (id:%d)") %
                            (line.invoice_id.loyalty_card_id.card_type.name,
                             line.invoice_id.loyalty_card_id.card_type,))
                    bonus_use_line = line.move_line_used_item(used_bonus, c, line)
                    if bonus_use_line:
                        res.append(bonus_use_line)
        return res

    @api.one
    @api.depends(
        'state', 'currency_id', 'invoice_line_ids.price_subtotal',
        'move_id.line_ids.amount_residual',
        'move_id.line_ids.currency_id')
    def _compute_residual(self):
        residual = 0.0
        residual_company_signed = 0.0
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        # OVERRIDE BEGINS
        # Нэхэмжлэлийн үлдэгдэл тооцох үед Бонус картны өглөг дансны бичилт тооцохгүй болгох
        bonus_card_account = self.company_id.property_debit_account_id or self.env.ref('l10n_mn_sale_loyalty_card.account_account_3201_0000')
        # Нэхэмжлэлийн үлдэгдэл тооцох үед борлуулалтын тохиргооны Хадгаламжийн Бүтээгдэхүүнд тохируулсан дансны бичилт тооцохгүй болгох
        product_id = self.env['ir.values'].get_default('sale.config.settings', 'deposit_product_id_setting', company_id=self.env.user.company_id.id)
        account_income_id = self.env['product.product'].browse(product_id).product_tmpl_id.property_account_income_id
        for line in self.sudo().move_id.line_ids:
            if line.account_id.internal_type in ('receivable', 'payable') and line.account_id.id != bonus_card_account.id and line.account_id.id != account_income_id.id:
                residual_company_signed += line.amount_residual
                if line.currency_id == self.currency_id:
                    residual += line.amount_residual_currency if line.currency_id else line.amount_residual
                else:
                    from_currency = (line.currency_id and line.currency_id.with_context(
                        date=line.date)) or line.company_id.currency_id.with_context(date=line.date)
                    residual += from_currency.compute(line.amount_residual, self.currency_id)
        self.residual_company_signed = abs(residual_company_signed) * sign
        self.residual_signed = abs(residual) * sign
        self.residual = abs(residual)
        digits_rounding_precision = self.currency_id.rounding
        if float_is_zero(self.residual, precision_rounding=digits_rounding_precision):
            self.reconciled = True
        else:
            self.reconciled = False
            
    @api.model
    def _prepare_refund(self, invoice, date_invoice=None, date=None, description=None, journal_id=None):
        res = super(AccountInvoice, self)._prepare_refund(invoice, date_invoice=None, date=None, description=None, journal_id=None)
        res['loyalty_card_id'] = invoice.loyalty_card_id.id
        return res
        
    @api.multi 
    def action_invoice_open(self):
        bonus_history_obj = self.env['loyalty.card.history']
        res = super(AccountInvoice, self).action_invoice_open()
        if self.loyalty_card_id and self.type == 'out_invoice':
            name = self.number
            total_amount = self.amount_total
            percent = self.loyalty_card_id.card_type.sale_percentage
            bonus_added = self.added_bonus
            sale_amount = total_amount - self.used_bonus
            pay_amount = total_amount - self.used_bonus
            bonus_spent = self.used_bonus
            self.loyalty_card_id.buy(name, total_amount, sale_amount, pay_amount, percent, bonus_added, bonus_spent)
        elif self.loyalty_card_id and self.type == 'out_refund':
            name = self.number
            total_amount = self.amount_total
            percent = self.loyalty_card_id.card_type.sale_percentage
            bonus_added = -self.added_bonus
            sale_amount = total_amount - self.used_bonus
            pay_amount = total_amount - self.used_bonus
            bonus_spent = -self.used_bonus
            self.loyalty_card_id.buy(name, total_amount, sale_amount, pay_amount, percent, bonus_added, bonus_spent)
        return res
    
    @api.multi
    def action_cancel(self):
        bonus_history_obj = self.env['loyalty.card.history']
        if self.state == 'open':
            if self.loyalty_card_id and self.type == 'out_invoice':
                vals = {
                    'transaction': self.number + u' Цуцлав',
                    'loyalty_card_id': self.loyalty_card_id.id,
                    'total_amount': self.amount_total,
                    'sale_percentage': self.card_percent,
                    'bonus_added': -self.added_bonus,
                    'bonus_spent': -self.used_bonus,
                    'history_date': datetime.now(),
                }
                bonus_history_obj.create(vals)
            elif self.loyalty_card_id and self.type == 'out_refund':
                vals = {
                    'transaction': self.number + u' Цуцлав',
                    'loyalty_card_id': self.loyalty_card_id.id,
                    'total_amount': self.amount_total,
                    'sale_percentage': self.card_percent,
                    'bonus_added': self.added_bonus,
                    'bonus_spent': self.used_bonus,
                    'history_date': datetime.now(),
                }
                bonus_history_obj.create(vals)
        res = super(AccountInvoice, self).action_cancel()
        return res

class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'

    def _get_per_add_bonus(self, bonus_type="use"):
        if bonus_type == "use":
            return self.used_bonus / self.quantity if self.quantity else 0
        if bonus_type == "add":
            return self.added_bonus / self.quantity if self.quantity else 0

    # Түр тайлбар болгов.
    @api.one
    @api.depends('price_unit', 'discount', 'invoice_line_tax_ids', 'quantity', 'product_id','invoice_id.partner_id', 'invoice_id.currency_id')
    def _compute_price(self):
        per_add_bonus = 0.0
        if self.invoice_id.loyalty_card_id and self.added_bonus > 0.0:
            per_add_bonus = self._get_per_add_bonus()
        price = self.price_unit * (1 - (self.discount or 0.0) / 100.0) - self._get_per_add_bonus("add")
        taxes = self.invoice_line_tax_ids.compute_all(price, self.currency_id, quantity=self.quantity, product=self.product_id, partner=self.invoice_id.partner_id)
        self.price_subtotal = taxes['total_excluded']
        if self.invoice_id:
            self.price_subtotal = self.invoice_id.currency_id.round(self.price_subtotal)

    added_bonus = fields.Float('Add Bonus')
    used_bonus = fields.Float('Use Bonus')
    
    @api.model
    def move_line_added_item(self, added_bonus, b, line):
        return {'type': 'dest',
                'name': line.name[:64],
                'price_unit': added_bonus / line.quantity,
                'quantity': line.quantity,
                'price': added_bonus,
                'account_id': b.id,
                'product_id': line.product_id.id,
                'account_analytic_id': line.account_analytic_id.id or False,
                'taxes': False,
                'invoice_id': line.invoice_id.id
                }
        
    @api.model
    def move_line_used_item(self, used_bonus, c, line):
        return {'type': 'dest',
                'name': line.name[:64],
                'price_unit': -used_bonus / line.quantity,
                'quantity': line.quantity,
                'price': -used_bonus,
                'account_id': c.id,
                'product_id': line.product_id.id,
                'account_analytic_id': line.account_analytic_id.id or False,
                'taxes': False,
                'invoice_id': line.invoice_id.id
                }

class AccountInvoiceTax(models.Model):
    """ Татварын журналын бичилтийг үндсэн харилцагчаас өөр харилцагчид бүртгэхийн тулд
        татварын мэдээллээс харилцагч талбарын утгыг авч ашиглана.
    """
    _inherit = 'account.invoice.tax'

    @api.v8
    def compute(self, invoice):
        """ Татварын журналын бичилтийг үндсэн харилцагчаас өөр харилцагчид бүртгэхийн тулд
            татварын мэдээллээс харилцагч талбарын утгыг авч ашиглана.
        """
        tax_grouped = {}
        currency = invoice.currency_id.with_context(date=invoice.date_invoice or fields.Date.context_today(invoice))
        company_currency = invoice.company_id.currency_id
        for line in invoice.invoice_line:
            per_add_bonus = 0.0
            if line.added_bonus > 0.0:
                per_add_bonus = line._get_per_add_bonus()
            taxes = line.invoice_line_tax_ids.compute_all(
                (line.price_unit - per_add_bonus) * (1 - (line.discount or 0.0) / 100.0),
                line.quantity, line.product_id, invoice.partner_id)['taxes']
            for tax in taxes:
                val = {
                    'invoice_id': invoice.id,
                    'name': tax['name'],
                    'amount': tax['amount'],
                    'manual': False,
                    'sequence': tax['sequence'],
                    'base': currency.round(tax['price_unit'] * line['quantity']),
                    'partner_id': tax.get('partner_id', False)
                }
                if invoice.type in ('out_invoice', 'in_invoice'):
                    val['base_code_id'] = tax['base_code_id']
                    val['tax_code_id'] = tax['tax_code_id']
                    val['base_amount'] = currency.compute(val['base'] * tax['base_sign'], company_currency, round=False)
                    val['tax_amount'] = currency.compute(val['amount'] * tax['tax_sign'], company_currency, round=False)
                    val['account_id'] = tax['account_collected_id'] or line.account_id.id
                    val['account_analytic_id'] = tax['account_analytic_collected_id']
                else:
                    val['base_code_id'] = tax['ref_base_code_id']
                    val['tax_code_id'] = tax['ref_tax_code_id']
                    val['base_amount'] = currency.compute(val['base'] * tax['ref_base_sign'], company_currency, round=False)
                    val['tax_amount'] = currency.compute(val['amount'] * tax['ref_tax_sign'], company_currency, round=False)
                    val['account_id'] = tax['account_paid_id'] or line.account_id.id
                    val['account_analytic_id'] = tax['account_analytic_paid_id']

                # If the taxes generate moves on the same financial account as the invoice line
                # and no default analytic account is defined at the tax level, propagate the
                # analytic account from the invoice line to the tax line. This is necessary
                # in situations were (part of) the taxes cannot be reclaimed,
                # to ensure the tax move is allocated to the proper analytic account.
                if not val.get('account_analytic_id') and line.account_analytic_id and val['account_id'] == line.account_id.id:
                    val['account_analytic_id'] = line.account_analytic_id.id

                key = (val['tax_code_id'], val['base_code_id'], val['account_id'], val['partner_id'])
                if key not in tax_grouped:
                    tax_grouped[key] = val
                else:
                    tax_grouped[key]['base'] += val['base']
                    tax_grouped[key]['amount'] += val['amount']
                    tax_grouped[key]['base_amount'] += val['base_amount']
                    tax_grouped[key]['tax_amount'] += val['tax_amount']

        for t in tax_grouped.values():
            t['base'] = currency.round(t['base'])
            t['amount'] = currency.round(t['amount'])
            t['base_amount'] = currency.round(t['base_amount'])
            t['tax_amount'] = currency.round(t['tax_amount'])
        return tax_grouped
