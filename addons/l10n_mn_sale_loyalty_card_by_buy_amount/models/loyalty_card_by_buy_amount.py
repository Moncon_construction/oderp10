# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.osv import expression


class LoyaltyCard(models.Model):
    _inherit = "loyalty.card.card"

    card_balance = fields.Float(string='Card Balance', default="0")
    sale_percentage = fields.Float(string="Card discount %", related="card_type.sale_percentage")
    maximum_amount_percentage = fields.Integer(string='Maximum amount %', related='card_type.maximum_amount_percentage')
    
    # Доорх функцууд картны доод, дээд үнийн интервалыг картны балансд өгч байгаа
    def change_card_balance(self, amount_spent=0, amount_added=0):
        for obj in self:
            obj.card_balance += amount_added - amount_spent
            
    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('primary_holder', '=ilike', name + '%'), ('card_number', operator, name)]
            if operator in expression.NEGATIVE_TERM_OPERATORS:
                domain = ['&', '!'] + domain[1:]
        cards = self.search(domain + args, limit=limit)
        return cards.name_get()

    @api.multi
    @api.depends('primary_holder', 'card_number')
    def name_get(self):
        result = []
        for card in self:
            name = card.primary_holder.name + ' [' + card.card_number + ']'
            result.append((card.id, name))
        return result

class LoyaltyCardType(models.Model):
    _inherit = "loyalty.card.type"

    calculation = fields.Selection(selection_add=[('Collects sale amount', _('Collects Sale Amount'))])
    minimum_interval = fields.Integer(string="Minimum Interval")
    maximum_interval = fields.Integer(string="Maximum Interval")
    has_time_interval = fields.Boolean(string="Has time interval")
    time_interval = fields.Integer(string="Time interval(by month)")
    sale_percentage = fields.Float(string='Percentage to collect from amount')

    @api.model
    def create(self, vals):
        if 'sale_percentage' in vals:
            if vals['sale_percentage'] > 100 or vals['sale_percentage'] < 0:
                raise UserError("Sale percentage must be between 0 to 100")

        if vals['time_interval'] > 99:
            raise UserError(_("Time interval must be 2 digit number"))
        return super(LoyaltyCardType, self).create(vals)

    @api.onchange('minimum_interval', 'maximum_interval')
    def check_interval(self):
        for obj in self:
            if obj.minimum_interval and obj.maximum_interval:
                if obj.maximum_interval < obj.minimum_interval:
                    raise UserError(_("Maximum interval must be higher than minimum interval"))

    @api.multi
    def write(self, vals):
        if 'time_interval' in vals:
            if vals['time_interval'] > 99:
                raise UserError(_("Time interval must be 2 digit number"))
        if 'sale_percentage' in vals:
            if vals['sale_percentage'] > 100 or vals['sale_percentage'] < 0:
                raise UserError("Sale percentage must be between 0 to 100")
        return super(LoyaltyCardType, self).write(vals)

    # Картны үлдэгдэл рүү бичилт хийх функц
    def calculate_point(self, adding_points=None, subtracting_point=None):
        if subtracting_point > self.card_balance:
            raise UserError(_("The card balance is not enough!"))
        else:
            self.card_balance += subtracting_point - adding_points


class LoyaltyCardHistory(models.Model):
    _inherit = "loyalty.card.history"

    @api.model
    def create(self, vals):
        res = super(LoyaltyCardHistory, self).create(vals)
        if res.loyalty_card_id:
            res.loyalty_card_id.change_card_balance(amount_added=float(vals.get('bonus_added') or 0), amount_spent=float(vals.get('bonus_spent') or 0))
        return res