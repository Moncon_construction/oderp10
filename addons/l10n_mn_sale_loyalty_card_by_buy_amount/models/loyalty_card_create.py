# -*- coding: utf-8 -*-

import logging
import base64
import xlrd
from odoo import models, _  # @UnresolvedImport
from tempfile import NamedTemporaryFile
from odoo.exceptions import UserError, ValidationError  # @UnresolvedImport
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

_logger = logging.getLogger(__name__)

class LoyaltyCardCreateTool(models.TransientModel):
    _inherit = 'loyalty.card.create.tool'

    def process(self):
        _logger = logging.getLogger('jacara')
        book = super(LoyaltyCardCreateTool, self).process()

        sheet = book.sheet_by_index(0)
        nrows = sheet.nrows
        rowi = 1
        data = []
        exists = []
        loyalty_card_obj = self.env['loyalty.card.card']

        errors = []

        while rowi < nrows:
            row = sheet.row(rowi)
            primary_holder = row[0].value or False
            card_number = str(row[2].value)
            bonus_balance = row[3].value
            date = str(row[4].value)

            rowi += 1
            exists = loyalty_card_obj.search([('card_number', '=', card_number)], limit=1)
            if not card_number:
                errors.append(_('Data error %s row! \n Card Number columns must have a value!') % (rowi))
            if not primary_holder:
                errors.append(_('Data error %s row! \n Primary holder columns must have a value!') % (rowi))
            if exists and primary_holder:
                # exists.buy(bonus_added=bonus_balance)
                self._cr.execute('SELECT loyalty_card_id FROM loyalty_card_history WHERE loyalty_card_id = %s' % (exists.id))
                if not self._cr.fetchall():
                    history = self.env['loyalty.card.history']
                    vals = {
                        'loyalty_card_id': exists.id,
                        'bonus_added': bonus_balance,
                        'history_date': date,
                        'bonus_spent':0
                    }
                    history.create(vals)
                else:
                    errors.append(_('Data error %s row! \n %s with card_number %s loyalty card history is already exists!') % (rowi, card_number, primary_holder))
            else:
                errors.append(_('Data error %s row! \n %s with card_number %s loyalty card not exists!') % (rowi, card_number, primary_holder))

        if len(errors) > 0:
            error_message = _('There are %s errors found.\n') % len(errors)
            for index in range(len(errors)):
                error_message += (str(index + 1) + ":\t" + errors[index] + "\n")
            raise ValidationError(error_message)

        return True