# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class ResCompany(models.Model):
    _inherit = "res.company"

    def _get_default_account(self):
        card_account = self.env.ref('l10n_mn_sale_loyalty_card.account_account_3201_0000')
        if card_account:
            return card_account
        return False

    property_debit_account_id = fields.Many2one('account.account',  string="Debit Account", default=_get_default_account)
    property_credit_account_id = fields.Many2one('account.account', string="Credit Account", default=_get_default_account)