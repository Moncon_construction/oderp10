# -*- coding: utf-8 -*-

from datetime import datetime
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
import odoo.addons.decimal_precision as dp

class SaleOrder(models.Model):
    _inherit = "sale.order"
    
    @api.depends('order_line')
    def _compute_added_bonus(self):
        for sale in self:
            total = 0
            for line in sale.order_line:
                total += line.add_bonus
            sale.add_bonus = sum(line.add_bonus for line in sale.order_line)
            
    @api.depends('order_line', 'order_line.price_unit', 'order_line.tax_id', 'order_line.discount', 'order_line.product_uom_qty')
    def _amount_all_wrapper(self):
        """ Wrapper because of direct method passing as parameter for function fields """
        return self._amount_all()

    loyalty_card_id = fields.Many2one('loyalty.card.card', string='Loyalty card', copy=False, track_visibility='onchange')
    card_percent = fields.Float(string='Loyalty card percent', copy=False, compute='_compute_card_percent', store=True)
    card_balance = fields.Float(string='Loyalty card balance')
    add_bonus = fields.Float(string='Add bonus', compute='_compute_added_bonus')
    use_bonus = fields.Float(string='Use bonus', copy=False)
    undiscount_total_amount = fields.Monetary(compute='_amount_all_wrapper', digits=dp.get_precision('Account'), string='Total Amount', help="The total amount.")
    
    @api.depends('loyalty_card_id')
    @api.onchange('loyalty_card_id')
    def _compute_card_percent(self):
        for obj in self:
            if obj.loyalty_card_id:
                obj.card_percent = obj.loyalty_card_id.sale_percentage

    @api.onchange('partner_id')
    def change_loyalty_card(self):
        for obj in self:
            obj.loyalty_card_id = False
            obj.card_balance = 0

    @api.multi
    def _get_approve_use_bonus(self):
        ''' Хасагдах бонус дүнд картын үлдэгдэл хүрэлцэж байгаа эсэхийг шалгаж байна'''
        balance = self.loyalty_card_id.card_balance
        curr_percent = self.loyalty_card_id.card_type.maximum_amount_percentage
        curr_use_bonus = self.amount_total * ( (curr_percent or 0.0) / 100.0)
        if balance >= curr_use_bonus:
            if self.use_bonus > curr_use_bonus:
                raise UserError(_('Use bonus over current use bonus !'))
        elif balance < curr_use_bonus:
            if balance < self.use_bonus:
                raise UserError(_('Loyalty card balance over use bonus!'))
        return True

    @api.depends('order_line', 'order_line.price_unit', 'order_line.tax_id', 'order_line.discount', 'order_line.product_uom_qty')
    def _amount_all(self):
        super(SaleOrder, self)._amount_all()

        for order in self:
            order.undiscount_total_amount = order.amount_total + order.add_bonus - order.use_bonus

    @api.multi
    def _get_use_bonus(self):
        ''' Хасагдах бонус дүнг мөрийн хувьд тооцоолж байна'''
        total_amount = 0
        for line in self.order_line:
            total_amount += line.price_unit * line.product_uom_qty
        if self.loyalty_card_id and self.use_bonus > 0.0 and total_amount > 0.0:
            per_unit = self.use_bonus / total_amount
            for use_line in self.order_line:
                use_bonus = use_line.price_unit * per_unit * use_line.product_uom_qty
                use_line.use_bonus = use_bonus
        else:
            for use_line in self.order_line:
                use_line.use_bonus = 0.0
        return True
    
    @api.onchange('loyalty_card_id')
    def onchange_loyalty_card_id(self):
        for sale in self:
            if sale.loyalty_card_id:
                sale._get_use_bonus()
                sale._get_approve_use_bonus()
                for line in sale.order_line:
                    line.loyalty_card_id = sale.loyalty_card_id
                sale.card_balance = sale.loyalty_card_id.card_balance
            else:
                sale.use_bonus = 0.0
                for line in sale.order_line:
                    line.use_bonus = 0.0
    
    @api.onchange('use_bonus')
    def onchange_bonus_use(self):
        for order in self:
            if order.loyalty_card_id:
                if order.use_bonus < 0.0:
                    raise UserError(_(u'Хасагдах бонус 0-ээс бага утга оруулсан байна. Бонус бодогдохгүй тул эерэг тоон утга оруулна уу !'))
                else:
                    order._get_approve_use_bonus()
                    order._get_use_bonus()

    @api.multi
    def write(self, vals):
        res = super(SaleOrder, self).write(vals)
        for so in self:
            if so.use_bonus < 0.0:
                for line in so.order_line:
                    line.use_bonus = 0.0
        return res

    def _prepare_invoice(self):
        res = super(SaleOrder, self)._prepare_invoice()
        if self.loyalty_card_id:
            res.update({'loyalty_card_id': self.loyalty_card_id.id})
            res.update({'card_percent': self.card_percent})
        return res
    
    @api.multi
    def action_confirm(self):
        res = super(SaleOrder, self).action_confirm()
        for obj in self:
            obj._get_approve_use_bonus()
        return res
    
class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"
    
    def _get_per_add_bonus(self):
        return (self.use_bonus / self.product_uom_qty)

    @api.one
    @api.depends('order_id', 'order_id.use_bonus', 'loyalty_card_id', 'use_bonus', 'price_unit', 'discount')
    def _compute_add_bonus(self):
        for line in self:
            if line.loyalty_card_id and line.card_percent > 0.0:
                line.add_bonus = (line.price_unit * (1 - (line.discount or 0.0) / 100.0)) * (line.card_percent / 100) * line.product_uom_qty
    
    loyalty_card_id = fields.Many2one('loyalty.card.card', string='Loyalty card', copy=False, related='order_id.loyalty_card_id')
    card_percent = fields.Float(string='Loyalty card percent', copy=False, related='order_id.card_percent')
    add_bonus = fields.Float(string='Add bonus', compute='_compute_add_bonus')
    use_bonus = fields.Float(string='Use bonus', copy=False)

    @api.multi
    def get_loyalty_percent(self):
        for order_line in self:
            if order_line.order_id.loyalty_card_id:
                if order_line.order_id.use_bonus > 0.0:
                    order_line.order_id._get_use_bonus()
        return True

    @api.model
    def create(self, values):
        sale_order_line_id = super(SaleOrderLine, self).create(values)
        sale_order_line_id.get_loyalty_percent()
        if sale_order_line_id.order_id.use_bonus < 0.0:
            sale_order_line_id.use_bonus = 0.0
        return sale_order_line_id

    @api.multi
    def _prepare_invoice_line(self, qty):
        self.ensure_one()
        res = super(SaleOrderLine, self)._prepare_invoice_line(qty)
        res.update({
            'added_bonus': self.add_bonus,
            'used_bonus': self.use_bonus,
        })
        return res

    @api.depends('product_uom_qty', 'discount', 'price_unit', 'tax_id')
    def _compute_amount(self):
        """
        Compute the amounts of the SO line.
        """
        super(SaleOrderLine, self)._compute_amount()
        per_add_bonus = 0.0
        for line in self:
            # BEGIN CHANGE
            if line.loyalty_card_id and line.card_percent:
                per_add_bonus = (line.add_bonus / line.product_uom_qty)
            price = (line.price_unit * (1 - (line.discount or 0.0) / 100.0)) - per_add_bonus
            taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty,
                                            product=line.product_id, partner=line.order_id.partner_shipping_id)

            # END CHANGE
            line.update({
                'price_tax': taxes['total_included'] - taxes['total_excluded'],
                'price_total': taxes['total_included'],
                'price_subtotal': taxes['total_excluded'],
            })