# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError


class LoyaltyCardType(models.Model):
    _inherit = "loyalty.card.type"

    calculation = fields.Selection(selection_add=[('Percentage', 'Percentage')])
    discount_percentage = fields.Integer(string='Sale percentage')


class LoyaltyCard(models.Model):
    _inherit = "loyalty.card.card"

    discount_percentage = fields.Integer(string="Card discount %", related="card_type.discount_percentage")
