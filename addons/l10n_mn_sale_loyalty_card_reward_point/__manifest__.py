# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Sale Loyalty Reward Point Card",
    'version': '1.0',
    'depends': ['l10n_mn_sale_loyalty_card'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Loyalty Card Features
    """,
    'data': [

    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}