# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError

class LoyaltyCard(models.Model):
    _name = "loyalty.card.gift"
    _inherit = "loyalty.card.card"

class LoyaltyCardType(models.Model):
    _inherit = "loyalty.card.type"

    calculation = fields.Selection(selection_add=[('Gift', 'Gift')])



