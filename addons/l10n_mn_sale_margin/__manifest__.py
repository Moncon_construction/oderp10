# -*- coding: utf-8 -*-
{
    'name': 'Mongolian Margins in Sales Orders',
    'version': '1.0',
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'depends': ['sale_margin', 'l10n_mn_sale', 'l10n_mn_sales_report'],
    'data': [
        'views/sale_margin_view.xml',
    ],
    'installable': True,
    'auto_install': True
}
