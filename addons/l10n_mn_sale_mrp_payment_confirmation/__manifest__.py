# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Mongolian Sales MRP Payment Confirmation',
    'version': '1.0',
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'depends': ['l10n_mn_sale', 'l10n_mn_mrp'],
    'data': [
        'views/sale_view.xml',
        'security/security.xml'
    ],
    "installable": True,
}
