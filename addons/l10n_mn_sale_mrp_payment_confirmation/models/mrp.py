# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError

class ResCompany(models.Model):
    _inherit = 'res.company'

    confirm_paid_percent = fields.Float('Confirm Percent')

class MrpConfigSetttings(models.TransientModel):
    _inherit = 'mrp.config.settings'

    confirm_paid_percent = fields.Float('Confirm Percent', related='company_id.confirm_paid_percent')

class MrpProduction(models.Model):
    _inherit = 'mrp.production'

    @api.multi
    def button_confirm(self):
        res = super(MrpProduction, self).button_confirm()
        if not self.env.user.has_group('l10n_mn_sale_mrp_payment_confirmation.mrp_confirm_group'):
            if self.sale_order_id:
                paid_percent = 0
                paid_amount = 0
                amount = 0
                prepaid_amount = 0
                main_invoice = False
                for invoice in self.sale_order_id.invoice_ids:
                    if invoice.is_prepayment:
                        if invoice.state != 'draft':
                            if invoice.state == 'paid':
                                prepaid_amount += invoice.amount_total
                            else:
                                prepaid_amount += (invoice.amount_total - invoice.residual)
                    else:
                        main_invoice = True
                        if invoice.state != 'draft':
                            if invoice.state == 'paid':
                                paid_amount = invoice.amount_total
                            else:
                                paid_amount = invoice.amount_total - invoice.residual
                    if main_invoice:
                        amount = paid_amount
                    else:
                        amount = prepaid_amount

                if self.sale_order_id.amount_total > 0:
                    paid_percent = (amount / self.sale_order_id.amount_total) * 100

                if self.company_id.confirm_paid_percent > paid_percent:
                    raise UserError(_('In order to confirm manufacturing order %s percent of payment must be paid'%self.company_id.confirm_paid_percent))
        return res
