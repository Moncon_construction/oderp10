# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.one
    def _paid_amount(self):
        invoices = self.mapped('invoice_ids')
        paid_amount = 0
        prepaid_amount = 0
        is_prepaid = False
        main_invoice = False
        for invoice in invoices:
            if invoice.is_prepayment:
                if invoice.state != 'draft':
                    if invoice.state == 'paid':
                        prepaid_amount += invoice.amount_total
                    elif invoice.state == 'cancel':
                        prepaid_amount += invoice.residual
                    else:
                        prepaid_amount += (invoice.amount_total - invoice.residual)
            else:
                main_invoice = True
                if invoice.state != 'draft':
                    if invoice.state == 'paid':
                        paid_amount = invoice.amount_total
                    elif invoice.state == 'cancel':
                        prepaid_amount += invoice.residual
                    else:
                        paid_amount = invoice.amount_total - invoice.residual
            if main_invoice:
                self.paid_amount = paid_amount
            else:
                self.paid_amount = prepaid_amount

    @api.depends('amount_total', 'paid_amount')
    def _residual_amount(self):
        for obj in self:
            obj.residual_amount = obj.amount_total - obj.paid_amount

    @api.depends('amount_total', 'paid_amount')
    def _paid_percent(self):
        for obj in self:
            if obj.amount_total > 0:
                obj.paid_percent = (obj.paid_amount/obj.amount_total)*100

    def _mrp_count(self):
        for obj in self:
            obj.mrp_count = len(self.env['mrp.production'].search([('sale_order_id', '=', obj.id)]))

    paid_amount = fields.Float(string='Paid Amount', compute=_paid_amount)
    residual_amount = fields.Float(string='Residual Amount', compute=_residual_amount)
    paid_percent = fields.Float(string='Paid Percent', compute=_paid_percent)
    mrp_count = fields.Float(string='MRP count', compute=_mrp_count)
