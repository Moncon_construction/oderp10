# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Sale Order Dates",
    'version': '1.0',
    'depends': [
        'sale_order_dates',
        'l10n_mn_sale_stock'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Sale Stock Additional Features
    """,
    'data': [
        'views/sale_order_views.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}
