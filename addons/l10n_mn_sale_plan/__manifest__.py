# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Sale Plan",
    'version': '10.0.1.0',
    'depends': ['sale', 'l10n_mn_account'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Sale Modules',
    'description': """
         Sale Plan
    """,
    'data': [
        'security/sale_plan_security.xml',
        'security/ir.model.access.csv',
        'views/sale_plan_views.xml',
        'views/sale_plan_period_views.xml',
        'views/sale_plan_summary_views.xml',
        'views/sale_plan_line_views.xml'
    ],
        
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False,
    'application': True,
}