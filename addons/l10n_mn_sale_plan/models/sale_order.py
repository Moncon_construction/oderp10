# -*- coding: utf-8 -*-
from odoo import fields, models


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    is_plan_performance = fields.Boolean('Is Plan Performance', copy=False)
