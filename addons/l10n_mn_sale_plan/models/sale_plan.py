# -*- coding: utf-8 -*-
from odoo import api, fields, models, _, exceptions
from dateutil.relativedelta import relativedelta
from datetime import datetime
from odoo.exceptions import UserError


class SalePlan(models.Model):
    _name = 'sale.plan'
    _description = 'Sale Plan'
    _inherit = ['mail.thread']

    #Борлуулалтын төлөвлөгөө
    name = fields.Char('Sale Name', required=True)
    sale_team = fields.Many2one('crm.team', 'Sale Team')
    parent_plan = fields.Many2one('sale.plan', 'Parent Plan')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id, readonly=True)
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    sale_plan_summary_id = fields.Many2one('sale.plan.summary', string='Sale Plan Summary')
    period = fields.Selection([
        ('day', 'Day'),
        ('week', 'Week'),
        ('month', 'Month'),
        ('quarter', 'Quarter')], string='Period', default='month')
    is_holiday_plan = fields.Boolean('Holiday Plan', default=False)
    plan_type = fields.Selection([
                                ('saler', 'Saler'),
                                ('branch', 'Branch/Sale Team'),
                                ('product_category', 'Product Category'),
                                ('product', 'Product')], string='Plan Type', default='branch')
    plan_format = fields.Selection([
        ('quantity', 'Quantity'),
        ('money', 'Money'),
        ('mixed', 'Quantity and Money')], string='Plan Format', default='quantity')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('sent', 'Sent'),
        ('confirmed', 'Confirmed'),
        ('cancel', 'Cancel')], readonly=True, string='Status', default='draft')
    period_ids = fields.One2many('sale.plan.line', 'sale_plan_id', 'Period', )
    number = fields.Char('Number', default=lambda self: _('New'), readonly=True)
    is_product_id = fields.Boolean('Is Product Template Id')
    is_product_categ = fields.Boolean('Is Product Id')
    is_sale_team = fields.Boolean('Is Sale Team')
    is_saler_id = fields.Boolean(string='Is Saler', translate=True)
    plan_year = fields.Many2one('sale.plan.period', 'Plan Year', required=True)
    plan_period = fields.Many2one('sale.plan.period.line', 'Period')
    copy_name = fields.Char('Copy Number', store=True)
    user_id = fields.Many2one('res.users', 'Responsible', default=lambda self: self.env.user)
    product_categ = fields.Many2one('product.category', 'Product Category')
    total_plan_money = fields.Float('Total Plan Money', compute='_compute_period_ids')
    total_performance_money = fields.Float('Total Performance Money', compute='_compute_period_ids')
    total_plan_quantity = fields.Float('Total Plan Quantity', compute='_compute_period_ids')
    total_performance_quantity = fields.Float('Total Performance Quantity', compute='_compute_period_ids')

    @api.multi
    def unlink(self):
        if any(plan.state != 'draft' for plan in self):
            raise UserError(_('Cannot delete a sale plan not in draft state'))
        return super(SalePlan, self).unlink()

    @api.onchange('parent_plan')
    def onchange_parent_plan(self):
        if self.parent_plan:
            self.period = self.parent_plan.period
            self.start_date = self.parent_plan.start_date
            self.end_date = self.parent_plan.end_date
            self.plan_type = self.parent_plan.plan_type
            self.plan_format = self.parent_plan.plan_format
            self.plan_year = self.parent_plan.plan_year
            self.company_id = self.parent_plan.company_id
            self.period_ids = self.parent_plan.period_ids
            if self.parent_plan.sale_team:
                self.sale_team = self.parent_plan.sale_team

    @api.onchange('is_holiday_plan')
    def onchange_is_holiday_plan(self):
        if self.name:
            if self.is_holiday_plan:
                self.name = _("Holiday Plan: ") + self.name
            else:
                if self.name:
                    self.copy_name = self.name.split(' ')[2]
                    self.name = self.copy_name
        else:
            if self.is_holiday_plan:
                raise UserError(_('PLease set name.'))

    @api.depends('period_ids')
    def _compute_period_ids(self):
        for q in self:
            for sale_period in q.period_ids:
                if sale_period.id:
                    q.total_plan_quantity += sale_period.plan_quantity
                    q.total_plan_money += sale_period.plan_money
                    q.total_performance_quantity += sale_period.performance_quantity
                    q.total_performance_money += sale_period.performance_money

    @api.onchange('period')
    def onchange_period(self):
        for period in self.period_ids:
            if period:
                for sale_period in period.sale_period_ids:
                    if sale_period:
                        self.period_ids.sale_period_ids.period = self.period

    @api.onchange('plan_year')
    def onchange_plan_year(self):
        for y in self.plan_year.period_ids:
            self.plan_period = y.id

        self.period = self.plan_year.period
        self.start_date = self.plan_year.date_start
        self.end_date = self.plan_year.date_stop

    @api.onchange('plan_format')
    def onchange_plan_format(self):
        for y in self.period_ids:
            y.plan_format = self.plan_format

    @api.onchange('plan_type')
    def onchange_plan_type(self):
        if self.plan_type == "product":
            self.is_product_id = True
            self.is_product_categ = False
            self.is_sale_team = False
            self.is_saler_id = False
            self.plan_format = "quantity"
        elif self.plan_type == "product_category":
            self.is_product_categ = True
            self.is_product_id = False
            self.is_sale_team = False
            self.is_saler_id = False
            self.plan_format = "money"
        elif self.plan_type == "saler":
            self.is_product_categ = False
            self.is_product_id = False
            self.is_sale_team = False
            self.is_designer_id = False
            self.is_saler_id = True
            self.plan_format = "money"
        else:
            self.is_product_categ = False
            self.is_product_id = False
            self.is_sale_team = True
            self.is_saler_id = False
            self.plan_format = "money"

    @api.multi
    def action_sent(self):
        model_obj = self.env['ir.model.data']
        manager_group = model_obj.get_object_reference('l10n_mn_sale_plan', 'group_sale_plan_manager')
        for users in self.env['res.users'].search([('groups_id', '=', manager_group[1])]):
            if users.email:
                outgoing_email_ids = self.env['ir.mail_server'].search([])
                if not outgoing_email_ids:
                    raise exceptions.ValidationError(_('There is no configuration for outgoing mail server. Please contact system administrator.'))
                else:
                    outgoing_email = self.env['ir.mail_server'].search([('id', '=', outgoing_email_ids[0].id)])
                    you = users.email
                    email_ids = []
                    if self.sale_team:
                        vals = {
                            'state': 'outgoing',
                            'subject': 'Танд борлуулалтын төлөвлөгөө батлах урсгал ирлээ.',
                            'body_html': '<p>Сайн байна уу? %s,</br></p><p>Танд борлуулалтын төлөвлөгөө батлах урсгал ирлээ. </br></p><p><strong>Баримтын дугаар:</strong> %s</br></p><p><strong>Борлуулалтын төлөвлөгөөний нэр:</strong> %s</br></p><p><strong>Илгээгч:</strong> %s</br></p><p><strong>Салбар/Борлуулалтын баг:</strong> %s</br></p><p><strong>Төлөвлөгөө хамрах хугацаа:</strong> %s - %s</br></p><p> Баярлалаа,</p><pre>--<br>ERP Автомат Имэйл </pre>' % (users.name.encode(encoding='UTF-8'), self.number.encode(encoding='UTF-8'), self.name.encode(encoding='UTF-8'), self.user_id.name.encode(encoding='UTF-8'), self.sale_team.name.encode(encoding='UTF-8'), self.start_date.encode(encoding='UTF-8'), self.end_date.encode(encoding='UTF-8')),
                            'email_to': you,
                            'email_from': outgoing_email.smtp_user,
                        }
                    else:
                        vals = {
                            'state': 'outgoing',
                            'subject': 'Танд борлуулалтын төлөвлөгөө батлах урсгал ирлээ.',
                            'body_html': '<p>Сайн байна уу? %s,</br></p><p>Танд борлуулалтын төлөвлөгөө батлах урсгал ирлээ. </br></p><p><strong>Баримтын дугаар:</strong> %s</br></p><p><strong>Борлуулалтын төлөвлөгөөний нэр:</strong> %s</br></p><p><strong>Илгээгч:</strong> %s</br></p><p><strong>Төлөвлөгөө хамрах хугацаа:</strong> %s - %s</br></p><p> Баярлалаа,</p><pre>--<br>ERP Автомат Имэйл </pre>' % (users.name.encode(encoding='UTF-8'), self.number.encode(encoding='UTF-8'), self.name.encode(encoding='UTF-8'), self.user_id.name.encode(encoding='UTF-8'), self.start_date.encode(encoding='UTF-8'), self.end_date.encode(encoding='UTF-8')),
                            'email_to': you,
                            'email_from': outgoing_email.smtp_user,
                        }
                    email_ids.append(self.env['mail.mail'].create(vals))
                    if email_ids != []:
                        self.env['mail.mail'].send(email_ids)
        self.write({'state': 'sent'})
        return True

    @api.multi
    def action_confirmed(self):
        self.write({'state': 'confirmed'})

    @api.multi
    def action_cancel(self):
        self.write({'state': 'cancel'})

    @api.multi
    def action_draft(self):
        self.write({'state': 'draft'})

    #Борлуулалтын захиалгаас тухайн барааны хүргэгдсэн болон нэхэмжилсэн тоо хэмжээг авах.
    @api.multi
    def action_get(self):
        perf_money = 0
        perf_quantity = 0
        order_ids = self.env['sale.order'].search([('team_id', '=', self.sale_team.id), ('state', '=', 'sale'), ('company_id', '=', self.company_id.id)])
        if self.plan_type == "product":
            for order_id in order_ids:
                if not order_id.is_plan_performance:
                    for period in self.period_ids:
                        for a in period.sale_period_ids:
                            if order_id.confirmation_date:
                                confirmation_date = datetime.strptime(order_id.confirmation_date, '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d')
                                if confirmation_date >= a.period_line.start_date and confirmation_date <= a.period_line.end_date:
                                    for order_line in order_id.order_line:
                                        if order_line.product_id.id == period.product_id.id:
                                            if order_line.product_id.type in ('product', 'consu'):
                                                perf_quantity += order_line.qty_delivered
                                                perf_money += order_line.price_unit * order_line.qty_delivered
                                            elif order_line.product_id.type == 'service':
                                                perf_quantity += order_line.qty_invoiced
                                                perf_money += order_line.price_unit * order_line.qty_invoiced
                            a.performance_quantity += perf_quantity
                            a.performance_money += perf_money
                    order_id.write({'is_plan_performance': True})
        elif self.plan_type == "product_category":
            for order_id in order_ids:
                if not order_id.is_plan_performance:
                    for period in self.period_ids:
                        for a in period.sale_period_ids:
                            confirmation_date = datetime.strptime(order_id.confirmation_date, '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d')
                            if confirmation_date >= a.period_line.start_date and confirmation_date <= a.period_line.end_date:
                                for order_line in order_id.order_line:
                                    if order_line.product_id.categ_id.id == period.product_categ.id:
                                        perf_quantity += order_line.qty_delivered
                                        perf_money += order_line.price_unit * order_line.qty_delivered
                            a.performance_quantity += perf_quantity
                            a.performance_money += perf_money
                    order_id.write({'is_plan_performance': True})
        elif self.plan_type == "saler":
            for order_id in order_ids:
                if not order_id.is_plan_performance:
                    for period in self.period_ids:
                        for a in period.sale_period_ids:
                            confirmation_date = datetime.strptime(order_id.confirmation_date, '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d')
                            if order_id.user_id.id == period.saler_id.id and confirmation_date >= a.period_line.start_date and confirmation_date <= a.period_line.end_date:
                                for order_line in order_id.order_line:
                                    perf_quantity += order_line.qty_delivered
                                    perf_money += order_line.price_unit * order_line.qty_delivered
                            a.performance_quantity += perf_quantity
                            a.performance_money += perf_money
                    order_id.write({'is_plan_performance': True})
        elif self.plan_type == "branch":
            for a in self:
                for period_id in a.period_ids:
                    for sale_orders in self.env['sale.order'].search([('team_id', '=', period_id.sale_team.id)]):
                        for sale_order in sale_orders:
                            for line in sale_order.order_line:
                                perf_quantity += line.qty_delivered
                                perf_money += line.price_unit * line.qty_delivered
                                period_id.write({'performance_quantity': perf_quantity,
                                                 'performance_money': perf_money})

    def button_dummy(self):
        return True


class SalePlanLine(models.Model):
    _name = 'sale.plan.line'
    _description = 'Sale Plan Line'

    #Борлуулалтын төлөвлөгөөний мөрүүд
    product_id = fields.Many2one('product.product', 'Product')
    is_product_id = fields.Boolean('Is Product Template Id')
    product_categ = fields.Many2one('product.category', 'Product Category')
    is_product_categ = fields.Boolean('Is Product Id')
    sale_team = fields.Many2one('crm.team', 'Sale Team')
    is_sale_team = fields.Boolean('Is Sale Team')
    is_saler_id = fields.Boolean(string='Is Saler', translate=True)
    saler_id = fields.Many2one('res.users', string='Saler', translate=True)
    sale_plan_id = fields.Many2one('sale.plan', 'Sale Plan')
    plan_quantity = fields.Float('Plan Quantity', compute='_compute_sale_period')
    plan_money = fields.Float('Plan Money', compute='_compute_sale_period')
    performance_quantity = fields.Float('Performance Quantity', compute='_compute_sale_period')
    performance_money = fields.Float('Performance Money', compute='_compute_sale_period')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('sent', 'Sent'),
        ('confirmed', 'Confirmed'),
        ('cancel', 'Cancel')], readonly=True, string='Status', default='draft')
    sale_period_ids = fields.One2many('sale.period.line', 'sale_period_id', 'Sale Period', copy=False)
    plan_year = fields.Many2one('sale.plan.period', 'Period')
    period_name = fields.Char('Period Name')
    plan_format = fields.Selection([
        ('quantity', 'Quantity'),
        ('money', 'Money'),
        ('mixed', 'Quantity and Money')], string='Plan Format')
    sale_cost = fields.Float('Sale Money')
    sale_plan_summary_id = fields.Many2one('sale.plan.summary', related='sale_plan_id.sale_plan_summary_id', string='Sale Plan Summary')

    @api.depends('sale_period_ids')
    def _compute_sale_period(self):
        for q in self:
            for sale_period in q.sale_period_ids:
                if sale_period.id:
                    q.plan_quantity += sale_period.plan_quantity
                    q.plan_money += sale_period.plan_money
                    q.performance_quantity += sale_period.performance_quantity
                    q.performance_money += sale_period.performance_money

    @api.onchange('product_id')
    def _onchange_product_id(self):
        if self.product_id:
            self.period_name = self.product_id.name
            self.sale_cost = self.product_id.product_tmpl_id.list_price

    @api.onchange('product_categ')
    def _onchange_product_category_id(self):
        if self.product_categ:
            self.period_name = self.product_categ.name

    @api.onchange('sale_team')
    def _onchange_sale_team(self):
        if self.sale_team:
            self.period_name = self.sale_team.name
                
    @api.onchange('saler_id')
    def _onchange_saler(self):
        if self.saler_id:
            self.period_name = self.saler_id.name

    def button_dummy(self):
        return True


class SalePeriodLine(models.Model):
    _name = 'sale.period.line'
    _description = 'Sale Period Line'

    #Борлуулалтын төлөвлөгөөний мөчлөгийн мөрүүд
    sale_period_id = fields.Many2one('sale.plan.line', 'Sale Period Line')
    plan_quantity = fields.Float('Plan Quantity')
    plan_money = fields.Float('Plan Money')
    compute_plan_money = fields.Float('Compute Plan Money', compute='_compute_money')
    sale_cost = fields.Float('Sale Money')
    performance_quantity = fields.Float('Performance Quantity')
    performance_money = fields.Float('Performance Money')
    period_line = fields.Many2one('sale.plan.period.line', 'Period Line', required=True)
    plan_year = fields.Many2one('sale.plan.period', 'Period')
    plan_format = fields.Selection([
        ('quantity', 'Quantity'),
        ('money', 'Money'),
        ('mixed', 'Quantity and Money')], string='Plan Format')

    @api.depends('plan_quantity')
    def _compute_money(self):
        for q in self:
            if q.plan_format == 'quantity':
                if q.plan_quantity:
                    q.write({'plan_money': q.plan_quantity * q.sale_cost})


class SalePlanPeriod(models.Model):
    _name = "sale.plan.period"
    _description = "Sale Plan Period"

    #Борлуулалтын төлөвлөгөөний жилийн мөрүүд
    name = fields.Char('Code', copy=False, required=True)
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id)
    date_start = fields.Date('Start Date', required=True)
    date_stop = fields.Date('End Date', required=True)
    period_ids = fields.One2many('sale.plan.period.line', 'sale_plan_period_id', 'Periods')
    state = fields.Selection([
                            ('draft', 'Draft'),
                            ('confirmed', 'Confirmed')], 'Status', default="draft", copy=False)
    plan_year = fields.Char('Plan Year', required=True)
    period = fields.Selection([
        ('day', 'Day'),
        ('week', 'Week'),
        ('month', 'Month'),
        ('quarter', 'Quarter')], string='Period', default='month', required=True)

    _order = "date_start"
    _sql_constraints = [
        ('name_company_uniq', 'unique(name, company_id)', 'The name of the sale plan period must be unique per company!'),
    ]

    @api.multi
    def _check_duration(self):
        for obj_fy in self:
            if obj_fy.date_stop < obj_fy.date_start:
                return False
        return True

    _constraints = [
        (_check_duration, 'Error!\nThe start date of a fiscal year must precede its end date.', ['date_start', 'date_stop'])
    ]

    @api.multi
    def unlink(self):
        if any(plan.state != 'draft' for plan in self):
            raise UserError(_('Cannot delete a sale plan period not in draft state'))
        return super(SalePlanPeriod, self).unlink()

    @api.multi
    def action_confirm(self):
        self.write({'state': 'confirmed'})

    @api.multi
    def action_draft(self):
        self.write({'state': 'draft'})

    @api.multi
    def create_period3(self):
        if self.period == 'month':
            return self.create_period(1)
        elif self.period == "quarter":
            return self.create_period(3)
        elif self.period == "day":
            return self.create_period_day(1)
        elif self.period == "week":
            return self.create_period_day(7)

    @api.multi
    def create_period(self, interval=1):
        period_obj = self.env['sale.plan.period.line'].search([('sale_plan_period_id', '=', self.id)])
        self.env.cr.execute("DELETE FROM sale_plan_period_line WHERE sale_plan_period_id = %s", (self.id,))
        month = interval
        seq = 0
        if type(interval) == dict:
            month = interval['interval']
        for fy in self:
            ds = datetime.strptime(fy.date_start, '%Y-%m-%d')
            while ds.strftime('%Y-%m-%d') < fy.date_stop:
                seq += 1
                de = ds + relativedelta(months=month, days=-1)

                if de.strftime('%Y-%m-%d') > fy.date_stop:
                    de = datetime.strptime(fy.date_stop, '%Y-%m-%d')

                period_obj.create({
                    'code': ds.strftime('%m/%Y'),
                    'start_date': ds.strftime('%Y-%m-%d'),
                    'end_date': de.strftime('%Y-%m-%d'),
                    'company_id': self.company_id.id,
                    'name': str(seq) + ds.strftime('/%Y'),
                    'sale_plan_period_id': fy.id
                })
                ds = ds + relativedelta(months=month)
        return True

    @api.multi
    def create_period_day(self, interval=1):
        period_obj = self.env['sale.plan.period.line'].search([('sale_plan_period_id', '=', self.id)])
        self.env.cr.execute("DELETE FROM sale_plan_period_line WHERE sale_plan_period_id = %s", (self.id,))
        day = interval
        seq = 0
        if type(interval) == dict:
            day = interval['interval']
        for fy in self:
            ds = datetime.strptime(fy.date_start, '%Y-%m-%d')
            while ds.strftime('%Y-%m-%d') < fy.date_stop:
                seq += 1
                de = ds + relativedelta(days=day)
                if de.strftime('%Y-%m-%d') > fy.date_stop:
                    de = datetime.strptime(fy.date_stop, '%Y-%m-%d')

                period_obj.create({
                    'code': ds.strftime('%m/%Y'),
                    'start_date': ds.strftime('%Y-%m-%d'),
                    'end_date': de.strftime('%Y-%m-%d'),
                    'company_id': fy.company_id.id,
                    'name': str(seq) + ds.strftime('/%Y'),
                    'sale_plan_period_id': fy.id
                })
                ds = ds + relativedelta(days=day)
        return True,


class SalePlanPeriodLine(models.Model):
    _name = "sale.plan.period.line"
    _description = "Sale Plan Period Line"

    #Борлуулалтын төлөвлөгөөний жилийн мөчлөгийн мөрүүд
    sale_plan_period_id = fields.Many2one('sale.plan.period', 'Sale Plan')
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    name = fields.Char('Period Name', required=True)
    code = fields.Char('Code', size=6, required=True)

    @api.multi
    def _check_duration(self):
        if self.end_date < self.start_date:
            return False
        return True

    @api.multi
    def _check_year_limit(self):
        for obj_period in self:
            if obj_period.sale_plan_period_id.date_stop < obj_period.end_date or \
               obj_period.sale_plan_period_id.date_stop < obj_period.start_date or \
               obj_period.sale_plan_period_id.date_start > obj_period.start_date or \
               obj_period.sale_plan_period_id.date_start > obj_period.end_date:
                return False

        return True

    @api.multi
    def _check_limit(self):

        date_format = "%Y-%m-%d"
        a = datetime.strptime(self.end_date, date_format)
        b = datetime.strptime(self.start_date, date_format)
        calculate_day = a - b
        if self.sale_plan_period_id.period == "day":
            if int(calculate_day.days) > 1:
                return False
        elif self.sale_plan_period_id.period == "week":
            if int(calculate_day.days) > 7:
                return False
        elif self.sale_plan_period_id.period == "month":
            if int(calculate_day.days) > 31:
                return False
        elif self.sale_plan_period_id.period == "quarter":
            if int(calculate_day.days) > 88:
                return False
        return True

    _constraints = [
        (_check_duration, 'Error!\nThe duration of the Period(s) is/are invalid.', ['end_date']),
        (_check_year_limit, 'Error!\nThe period is invalid. Either some periods are overlapping or the period\'s dates are not matching the scope of the fiscal year.', ['end_date']),
        (_check_limit, 'Error!\nThe period is invalid. Either some periods are overlapping or the period\'s dates are not matching the.', ['end_date'])
    ]
