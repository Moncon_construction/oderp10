# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
from odoo.exceptions import UserError


class SalePlanSummary(models.Model):
    _name = "sale.plan.summary"
    _description = "Sale Plan Summary"

    @api.depends('sale_plan_line_ids')
    def _compute_plan_line_ids(self):
        for q in self:
            for sale_period in q.sale_plan_line_ids:
                if sale_period.id:
                    q.total_plan_quantity += sale_period.plan_quantity
                    q.total_plan_money += sale_period.plan_money
                    q.total_performance_quantity += sale_period.performance_quantity
                    q.total_performance_money += sale_period.performance_money

    # Борлуулалтын төлөвлөгөөний нэгтгэлийн мөрүүд
    name = fields.Char('Sale Name', required=True)
    sale_team = fields.Many2one('crm.team', 'Sale Team')
    parent_plan = fields.Many2one('sale.plan', 'Parent Plan')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id,
                                 readonly=True)
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    period = fields.Selection([
        ('day', 'Day'),
        ('week', 'Week'),
        ('month', 'Month'),
        ('quarter', 'Quarter')], string='Period', default='month')
    is_holiday_plan = fields.Boolean('Holiday Plan', default=False)
    plan_type = fields.Selection([
        ('saler', 'Saler'),
        ('branch', 'Branch/Sale Team'),
        ('product_category', 'Product Category'),
        ('product', 'Product')], string='Plan Type', default='branch')
    sale_plan_ids = fields.One2many('sale.plan', 'sale_plan_summary_id', 'Sale Plan')
    sale_plan_line_ids = fields.One2many('sale.plan.line', 'sale_team', 'Sale Plan Line', readonly=True)
    plan_format = fields.Selection([
        ('quantity', 'Quantity'),
        ('money', 'Money')], string='Plan Format', default='quantity')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('sent', 'Sent'),
        ('confirmed', 'Confirmed'),
        ('cancel', 'Cancel')], readonly=True, string='Status', default='draft')
    number = fields.Char('Number', default=lambda self: _('New'), readonly=True)
    is_product_id = fields.Boolean('Is Product Template Id')
    is_product_categ = fields.Boolean('Is Product Id')
    is_sale_team = fields.Boolean('Is Sale Team')
    is_saler_id = fields.Boolean(string='Is Saler', translate=True)
    plan_year = fields.Many2one('sale.plan.period', 'Plan Year', required=True)
    total_plan_money = fields.Float('Total Plan Money', compute='_compute_plan_line_ids')
    total_performance_money = fields.Float('Total Performance Money', compute='_compute_plan_line_ids')
    total_plan_quantity = fields.Float('Total Plan Quantity', compute='_compute_plan_line_ids')
    total_performance_quantity = fields.Float('Total Performance Quantity', compute='_compute_plan_line_ids')
    period_ids = fields.One2many('sale.plan.line', 'sale_plan_id', 'Period')

    @api.multi
    def action_sent(self):
        for record in self:
            record.state = 'sent'
            for plan in record.sale_plan_ids:
                plan.action_sent()
        self.write({'state': 'sent'})
        return True

    @api.multi
    def action_confirmed(self):
        for record in self:
            record.state = 'confirmed'
            for plan in record.sale_plan_ids:
                plan.action_confirmed()
        self.write({'state': 'confirmed'})
        return True

    @api.multi
    def action_cancel(self):
        for record in self:
            record.state = 'cancel'
            for plan in record.sale_plan_ids:
                plan.action_cancel()
        self.write({'state': 'cancel'})
        return True

    @api.multi
    def action_draft(self):
        for record in self:
            record.state = 'draft'
            for plan in record.sale_plan_ids:
                plan.action_draft()
        self.write({'state': 'draft'})
        return True


