# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
from odoo.exceptions import UserError


class CrmTeam(models.Model):
    _inherit = 'crm.team'

    @api.multi
    def unlink(self):
        for obj in self:
            sale_plan = self.env['sale.plan.line'].search([('sale_team', '=', obj.id)])
            if sale_plan:
                raise UserError(_('It is impossible to be deleted due to a registeration of the planing of marketing. '))
        return super(CrmTeam, self).unlink()