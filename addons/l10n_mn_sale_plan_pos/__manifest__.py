# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Sale Plan POS",
    'version': '1.0',
    'depends': ['l10n_mn_sale_plan', 'l10n_mn_point_of_sale'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Mongolian Point of Sale
    """,
    'data': [
        'views/sale_plan_view.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}