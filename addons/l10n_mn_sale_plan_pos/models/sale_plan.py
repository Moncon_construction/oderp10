# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError

class SalePlan(models.Model):
    _inherit = 'sale.plan'
    
    @api.depends('period_ids')
    def _compute_period_ids(self):
        for obj in self:
            for sale_period in obj.period_ids:
                if sale_period.id:
                    obj.total_plan_quantity += sale_period.plan_quantity
                    obj.total_plan_money += sale_period.plan_money
                    obj.total_performance_quantity += sale_period.performance_quantity + sale_period.performance_quantity_pos
                    obj.total_performance_money += sale_period.performance_money + sale_period.performance_money_pos

    #Борлуулалтын захиалгаас тухайн барааны хүргэгдсэн болон нэхэмжилсэн тоо хэмжээг авах.
    @api.multi
    def action_get(self):
        perf_money = 0
        perf_quantity = 0
        if self.plan_type == "product":
            for period in self.period_ids:
                for a in period.sale_period_ids:
                    date_where = "'" + a.period_line.start_date + "' and " + "'" + a.period_line.end_date + "'"
                    self.env.cr.execute("SELECT SUM(CASE WHEN d.type in ('product', 'consu') THEN b.product_uom_qty ELSE b.qty_invoiced END) as total_perf_qty, "
                                        "SUM(CASE WHEN d.type in ('product', 'consu') THEN b.product_uom_qty * b.price_unit ELSE b.qty_invoiced * b.price_unit END) as total_perf_money FROM "
                                        "sale_order a left join sale_order_line b on a.id = b.order_id "
                                        "left join product_product c on b.product_id = c.id "
                                        "left join product_template d on c.product_tmpl_id = d.id "
                                        " WHERE a.team_id = " 
                                        + str(self.sale_team.id) + " AND a.state in ('sale', 'done') AND a.company_id = " 
                                        + str(self.company_id.id) +
                                        " AND " 
                                        "a.confirmation_date between " + date_where + " AND b.product_id = " + str(period.product_id.id))
                    perfs = self.env.cr.dictfetchall()
                    if perfs[0]['total_perf_qty']:
                        a.performance_quantity = 0
                        a.performance_quantity += perfs[0]['total_perf_qty']
                    if perfs[0]['total_perf_money']:
                        a.performance_money = 0
                        a.performance_money += perfs[0]['total_perf_money']
                    if self.sale_team.member_ids and len(self.sale_team.member_ids) > 1:
                        self.env.cr.execute("SELECT SUM(b.qty) as total_perf_qty, SUM(b.qty * b.price_unit) as total_perf_money " 
                                            "FROM pos_order a left join pos_order_line b on a.id = b.order_id "
                                            "left join product_product c on b.product_id = c.id "
                                            "WHERE a.state in ('paid', 'invoiced') AND a.user_id in " + str(tuple(self.sale_team.member_ids.ids)) + " AND " 
                                            "a.date_order between " + date_where + " AND b.product_id = " + str(period.product_id.id))
                        perfs_pos = self.env.cr.dictfetchall()
                    elif self.sale_team.member_ids and len(self.sale_team.member_ids) == 1:
                        self.env.cr.execute("SELECT SUM(b.qty) as total_perf_qty, SUM(b.qty * b.price_unit) as total_perf_money " 
                                            "FROM pos_order a left join pos_order_line b on a.id = b.order_id "
                                            "left join product_product c on b.product_id = c.id "
                                            "WHERE a.state in ('paid', 'invoiced') AND a.user_id = " + str(self.sale_team.member_ids.id) + " AND " 
                                            "a.date_order between " + date_where + " AND b.product_id = " + str(period.product_id.id))
                        perfs_pos = self.env.cr.dictfetchall()
                    else:
                        raise UserError(_('%s sale team has no member') % period_id.sale_team.name)
                    if perfs_pos[0]['total_perf_qty']:
                        a.performance_quantity_pos = 0
                        a.performance_quantity_pos += perfs_pos[0]['total_perf_qty']
                    if perfs_pos[0]['total_perf_money']:
                        a.performance_money_pos = 0
                        a.performance_money_pos += perfs_pos[0]['total_perf_money']
        elif self.plan_type == "product_category":
            for period in self.period_ids:
                for a in period.sale_period_ids:
                    date_where = "'" + a.period_line.start_date + "' and " + "'" + a.period_line.end_date + "'"
                    self.env.cr.execute("SELECT SUM(CASE WHEN d.type in ('product', 'consu') THEN b.product_uom_qty ELSE b.qty_invoiced END) as total_perf_qty, "
                                        "SUM(CASE WHEN d.type in ('product', 'consu') THEN b.product_uom_qty * b.price_unit ELSE b.qty_invoiced * b.price_unit END) as total_perf_money FROM "
                                        "sale_order a left join sale_order_line b on a.id = b.order_id "
                                        "left join product_product c on b.product_id = c.id "
                                        "left join product_template d on c.product_tmpl_id = d.id "
                                        "left join product_category e on d.categ_id = e.id "
                                        " WHERE a.team_id = " 
                                        + str(self.sale_team.id) + " AND a.state in ('sale', 'done') AND a.company_id = " 
                                        + str(self.company_id.id) +
                                        " AND " 
                                        "a.confirmation_date between " + date_where + " AND e.id = " + str(period.product_categ.id))
                    perfs = self.env.cr.dictfetchall()
                    if perfs[0]['total_perf_qty']:
                        a.performance_quantity = 0
                        a.performance_quantity += perfs[0]['total_perf_qty']
                    if perfs[0]['total_perf_money']:
                        a.performance_money = 0
                        a.performance_money += perfs[0]['total_perf_money']
                    if self.sale_team.member_ids and len(self.sale_team.member_ids) > 1:
                        self.env.cr.execute("SELECT SUM(b.qty) as total_perf_qty, SUM(b.qty * b.price_unit) as total_perf_money " 
                                            "FROM pos_order a left join pos_order_line b on a.id = b.order_id "
                                            "left join product_product c on b.product_id = c.id "
                                            "left join product_template d on c.product_tmpl_id = d.id "
                                            "left join product_category e on d.categ_id = e.id "
                                            "WHERE a.state in ('paid', 'invoiced') AND a.user_id in " + str(tuple(self.sale_team.member_ids.ids)) + " AND "
                                            "a.date_order between " + date_where + " AND e.id = " + str(period.product_categ.id))
                        perfs_pos = self.env.cr.dictfetchall()
                    elif self.sale_team.member_ids and len(self.sale_team.member_ids) == 1:
                        self.env.cr.execute("SELECT SUM(b.qty) as total_perf_qty, SUM(b.qty * b.price_unit) as total_perf_money " 
                                            "FROM pos_order a left join pos_order_line b on a.id = b.order_id "
                                            "left join product_product c on b.product_id = c.id "
                                            "left join product_template d on c.product_tmpl_id = d.id "
                                            "left join product_category e on d.categ_id = e.id "
                                            "WHERE a.state in ('paid', 'invoiced') AND a.user_id = " + str(self.sale_team.member_ids.id) + " AND "
                                            "a.date_order between " + date_where + " AND e.id = " + str(period.product_categ.id))
                        perfs_pos = self.env.cr.dictfetchall()
                    else:
                        raise UserError(_('%s sale team has no member') % period_id.sale_team.name)
                    if perfs_pos[0]['total_perf_qty']:
                        a.performance_quantity_pos = 0
                        a.performance_quantity_pos += perfs_pos[0]['total_perf_qty']
                    if perfs_pos[0]['total_perf_money']:
                        a.performance_money_pos = 0
                        a.performance_money_pos += perfs_pos[0]['total_perf_money']
        elif self.plan_type == "branch":
            for period_id in self.period_ids:
                for a in period_id.sale_period_ids:
                    date_where = "'" + a.period_line.start_date + "' and " + "'" + a.period_line.end_date + "'"
                    self.env.cr.execute("SELECT SUM(CASE WHEN d.type in ('product', 'consu') THEN b.product_uom_qty ELSE b.qty_invoiced END) as total_perf_qty, "
                                        "SUM(CASE WHEN d.type in ('product', 'consu') THEN b.product_uom_qty * b.price_unit ELSE b.qty_invoiced * b.price_unit END) as total_perf_money FROM "
                                        "sale_order a left join sale_order_line b on a.id = b.order_id "
                                        "left join product_product c on b.product_id = c.id "
                                        "left join product_template d on c.product_tmpl_id = d.id "
                                        "left join product_category e on d.categ_id = e.id "
                                        " WHERE a.team_id = " 
                                        + str(period_id.sale_team.id) + " AND a.state in ('sale', 'done') AND a.company_id = " 
                                        + str(self.company_id.id) +
                                        " AND " 
                                        "a.confirmation_date between " + date_where)
                    perfs = self.env.cr.dictfetchall()
                    if perfs[0]['total_perf_qty']:
                        a.performance_quantity = 0
                        a.performance_quantity += perfs[0]['total_perf_qty']
                    if perfs[0]['total_perf_money']:
                        a.performance_money = 0
                        a.performance_money += perfs[0]['total_perf_money']
                    if period_id.sale_team.member_ids and len(period_id.sale_team.member_ids) > 1:
                        self.env.cr.execute("SELECT SUM(b.qty) as total_perf_qty, SUM(b.qty * b.price_unit) as total_perf_money " 
                                            "FROM pos_order a left join pos_order_line b on a.id = b.order_id "
                                            "left join product_product c on b.product_id = c.id "
                                            "left join product_template d on c.product_tmpl_id = d.id "
                                            "left join product_category e on d.categ_id = e.id "
                                            "WHERE a.state in ('paid', 'invoiced') AND a.user_id in " + str(tuple(period_id.sale_team.member_ids.ids)) + " AND "
                                            "a.date_order between " + date_where)
                        perfs_pos = self.env.cr.dictfetchall()
                    elif period_id.sale_team.member_ids and len(period_id.sale_team.member_ids) == 1:
                        self.env.cr.execute("SELECT SUM(b.qty) as total_perf_qty, SUM(b.qty * b.price_unit) as total_perf_money " 
                                            "FROM pos_order a left join pos_order_line b on a.id = b.order_id "
                                            "left join product_product c on b.product_id = c.id "
                                            "left join product_template d on c.product_tmpl_id = d.id "
                                            "left join product_category e on d.categ_id = e.id "
                                            "WHERE a.state in ('paid', 'invoiced') AND a.user_id = " + str(period_id.sale_team.member_ids.id) + " AND "
                                            "a.date_order between " + date_where)
                        perfs_pos = self.env.cr.dictfetchall()
                    else:
                        raise UserError(_('%s sale team has no member') % period_id.sale_team.name)
                    if perfs_pos[0]['total_perf_qty']:
                        a.performance_quantity_pos = 0
                        a.performance_quantity_pos += perfs_pos[0]['total_perf_qty']
                    if perfs_pos[0]['total_perf_money']:
                        a.performance_money_pos = 0
                        a.performance_money_pos += perfs_pos[0]['total_perf_money']

class SalePlanLine(models.Model):
    _inherit = 'sale.plan.line'
    
    performance_quantity_pos = fields.Float('Performance Quantity POS', compute='_compute_sale_period')
    performance_money_pos = fields.Float('Performance Money POS', compute='_compute_sale_period')
    
    @api.depends('sale_period_ids')
    def _compute_sale_period(self):
        for q in self:
            for sale_period in q.sale_period_ids:
                if sale_period.id:
                    q.plan_quantity += sale_period.plan_quantity
                    q.plan_money += sale_period.plan_money
                    q.performance_quantity += sale_period.performance_quantity
                    q.performance_money += sale_period.performance_money
                    q.performance_quantity_pos += sale_period.performance_quantity_pos
                    q.performance_money_pos += sale_period.performance_money_pos
                    
class SalePeriodLine(models.Model):
    _inherit = 'sale.period.line'
    
    performance_quantity_pos = fields.Float('Performance Quantity POS')
    performance_money_pos = fields.Float('Performance Money POS')