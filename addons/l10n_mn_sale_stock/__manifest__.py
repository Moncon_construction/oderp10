# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Sale Stock",
    'version': '1.0',
    'depends': ['sale_stock', 'l10n_mn_sale', 'l10n_mn_stock', 'l10n_mn_stock_account'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Sale Stock Additional Features
    """,
    'data': [
        'views/sale_order_views.xml',
        'views/sale_config_view.xml',
        'views/product_views.xml',
        'wizard/sales_report_by_size_view.xml',
        'views/product_brand_view.xml',
        'views/product_category_view.xml',
        'views/res_company_view.xml',
        'views/stock_warehouse_view.xml',
        'views/stock_picking.xml',
        'views/stock_config_settings_view.xml',
        'report/report_picking_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}