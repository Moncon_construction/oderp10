# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models

class AccountInvoice(models.Model):
    _inherit = "account.invoice"
    
    @api.onchange('partner_id', 'company_id')
    def _onchange_partner_id(self):
        # @Override: Авлага, өглөгийн дансыг агуулахаас авдаг болгов
        res = super(AccountInvoice, self)._onchange_partner_id()

        partner = self.partner_id
        company = self.company_id
        account = self.account_id
        
        if partner:
            if self.type in ('out_invoice', 'out_refund'):
                so = False
                lines = self.env['account.invoice.line'].search([('invoice_id', '=', self.id)])
                if lines and len(lines) > 0:
                    line_ids = str(lines.ids)[1:len(str(lines.ids))-1]
                    self._cr.execute("""
                        SELECT DISTINCT(order.id) AS id
                        FROM sale_order order
                        LEFT JOIN sale_order_line line ON line.order_id = order.id
                        WHERE line.id IN 
                            (
                                SELECT order_line_id 
                                FROM sale_order_line_invoice_rel 
                                WHERE invoice_line_id IN (%s)
                            )
                    """ %(line_ids))
                    results = self._cr.dictfetchall()
                    so = self.env['sale.order'].browse(results[0]['id']) if results else False
                elif self.origin:
                    so = self.env['sale.order'].search([('name', '=', self.origin)])
                
                so = so[0] if so and len(so) > 0 else False
                if so:
                    receivable_account = so.picking_type_id.warehouse_id.receivable_account if not so.warehouse_id else so.warehouse_id.receivable_account
                    account = receivable_account if (company.use_wh_rec_pay_account and receivable_account) else account
                    
                if not account:
                    account = partner.property_account_receivable_id
                    
        self.account_id = account[0] if account else self.account_id

        return res
    
