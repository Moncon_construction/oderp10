# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, _

class ProductCategory(models.Model):
    _inherit = 'product.category'
    
    @api.one
    def _is_analytic_group(self):
        if self.company_id.cost_center == 'product_categ':
            self.is_analytic_group = True
        else:
            self.is_analytic_group = False
    
    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic Account')
    is_analytic_group = fields.Boolean('Is Analytic Group', compute='_is_analytic_group')