# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _


class ResCompany(models.Model):
    _inherit = 'res.company'

    show_delivery_address = fields.Boolean(help='Show Delivery Address on Inventory Expense Report', string='Show Delivery Address')
    check_qty_on_sale_order_approve = fields.Selection([
        ('check', _('Check quantity of sale order upon approving')),
        ('not_check', _('Will not Check quantity of sale order upon approving'))
    ], _('Check Product Quantities'), default='not_check')