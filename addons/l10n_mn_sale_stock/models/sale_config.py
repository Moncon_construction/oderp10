# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _


class SaleConfig(models.TransientModel):
    _inherit = 'sale.config.settings'

    check_qty_on_sale_order_approve = fields.Selection([
        ('check', _('Check quantity of sale order upon approving')),
        ('not_check', _('Will not Check quantity of sale order upon approving'))
    ], _('Product Quantities'), default='not_check', related='company_id.check_qty_on_sale_order_approve')
    