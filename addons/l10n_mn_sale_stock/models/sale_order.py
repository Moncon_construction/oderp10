# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError
import time
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT


class StockMove(models.Model):
    _inherit = 'stock.move'

    @api.multi
    def check_delivery_date(self):
        for obj in self:
            if obj.procurement_id and obj.procurement_id.sale_line_id:
                sale_id = obj.procurement_id.sale_line_id.order_id
                if sale_id and not sale_id.delivered_date:
                    sale_id.write({'delivered_date': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)})

    @api.multi
    def action_done(self):
        res = super(StockMove, self).action_done()
        self.check_delivery_date()
        return res


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.onchange('warehouse_id')
    def _onchange_warehouse_id(self):
        if self.company_id.cost_center == 'warehouse':
            self.project_id = self.warehouse_id.analytic_account_id.id
            if self.order_line:
                for line in self.order_line:
                    line.analytic_account_id = self.project_id

    @api.depends('order_line.product_id', 'state')
    def _compute_check_delivered(self):
        for order in self:
            order.picking_ids = self.env['stock.picking'].search([('group_id', '=', order.procurement_group_id.id)]) if order.procurement_group_id else []
            pick_count = len(order.picking_ids)
            not_done_picks = []
            for pick in order.picking_ids:
                if pick.state != 'done':
                    not_done_picks.append(pick.id)
                    break
            if pick_count >= 1:
                if not not_done_picks:
                    order.is_delivered = True
                    order.is_not_delivered = False
                else:
                    order.is_delivered = False
                    order.is_not_delivered = True

    @api.multi
    def _default_warehouse_id(self):
        default_warehouse = self.env.user.allowed_warehouses.ids
        warehouse_ids = self.env['stock.warehouse'].search([('id', 'in', default_warehouse), ('company_id', 'child_of', [self.env.user.company_id.id])], limit=1)
        return warehouse_ids

    def _domain_warehouses(self):
        return [
            ('id', 'in', self.env.user.allowed_warehouses.ids),
            '|', ('company_id', '=', False), ('company_id', 'child_of', [self.env.user.company_id.id])
        ]

    stock_picking_type = fields.Many2one('stock.picking.type', string='Outgoing location',
                                         domain="[('code', '=', 'outgoing'),('warehouse_id','=',warehouse_id)]")
    sale_returned = fields.Boolean(compute='_compute_sale_returned', string='Sales returned', default=False, search='_sale_returned_search')
    is_delivered = fields.Boolean(compute='_compute_check_delivered', string='Is Delivered', search='_sale_delivered_search', readonly=True, default=False)
    is_not_delivered = fields.Boolean(compute='_compute_check_delivered', string='Is Not Delivered', search='_sale_not_delivered_search', readonly=True, default=False)
    delivered_date = fields.Date(string='Delivered Date')
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse',
                                   required=True, readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]},
                                   default=_default_warehouse_id, domain=_domain_warehouses)

    @api.onchange('warehouse_id')
    def _onchange_warehouse_id(self):
        domain = {}
        _warehouses = []
        if self.warehouse_id:
            stock_picking_type = self.env['stock.picking.type'].search([('warehouse_id', '=', self.warehouse_id.id), ('code', '=', 'outgoing')],
                                                                       limit=1)
            if stock_picking_type:
                self.stock_picking_type = stock_picking_type.id
        for warehouse in self.env.user.allowed_warehouses:
            _warehouses.append(warehouse.id)
        if _warehouses:
            domain['warehouse_id'] = [('id', 'in', _warehouses)]
        return {'domain': domain}

    @api.multi
    def _sale_delivered_search(self, operator, value):
        recs = self.search([]).filtered(lambda x: x.is_delivered)
        if recs:
            return [('id', 'in', [x.id for x in recs])]
        else:
            return [('id', 'in', [])]

    @api.multi
    def _sale_not_delivered_search(self, operator, value):
        recs = self.search([]).filtered(lambda x: not x.is_delivered)
        if recs:
            return [('id', 'in', [x.id for x in recs])]
        else:
            return [('id', 'in', [])]

    @api.multi
    def action_confirm(self):
        # бараануудын id-г хадгалах хувьсагч зарлаж байна (нэг барааг олон удаа оруулах эсэхийг шалгахын тулд)
        product_ids = []
        # бараануудын нөөц-г хадгалах хувьсагч зарлаж байна(нэг бараа олон орсон үед тухайн барааг нэмэхийн тулд)
        product_qtys = []
        # тохиргоо цэсний check_qty_on_sale_order_approve талбар check утгатай үед энэхүү тооцооллыг хийнэ
        if self.company_id.check_qty_on_sale_order_approve == 'check':
            for obj in self.order_line:
                if obj.product_id.id not in product_ids:  # нэг бараа хоёр удаа орсон эсэхийг шалгаж байна
                    product_ids.append(obj.product_id.id)
                    product_qtys.append(obj.product_uom_qty)
                else:  # нэг бараа хоёр удаа орсон үед барааны тоог нэмж үлдэгдэлтэй жишч байна
                    product_qtys[product_ids.index(obj.product_id.id)] += obj.product_uom_qty
                    if obj.product_id.type != 'service' and product_qtys[product_ids.index(obj.product_id.id)] > obj.available_qty:
                        raise UserError(_("%s product doesn't have enough quantity." % obj.product_id.name))
                if obj.product_id.type != 'service' and obj.product_uom_qty > obj.available_qty:
                    raise UserError(_("%s product doesn't have enough quantity." % obj.product_id.name))
        res = super(SaleOrder, self).action_confirm()
        # Хүргэлтийн захиалга нь борлуулалтын захиалга дээрх борлуулалтын багийг авч үүсдэг байна.
        for sale in self:
            sale.picking_ids.write({'team_id': sale.team_id.id})
            sale.picking_ids.write({'related_sale_order_id': sale.id})
            for picking in sale.picking_ids:
                for line in picking.move_lines:
                    if line.procurement_id and line.procurement_id.sale_line_id and line.procurement_id.sale_line_id.analytic_account_id:
                        line.analytic_account_id = line.procurement_id.sale_line_id.analytic_account_id.id
                    elif sale.related_project_id:
                        line.analytic_account_id = sale.related_project_id.id
        # Батлагдахад агуулах дээрх батлахад борлуулалтын хүргэлт автоматаар хийгдэнэ гэдэг чекээс хамаарч ажиллана
        if self.warehouse_id.do_transfer_on_sale_order_confirmation:
            for sale in self:
                for picking in sale.picking_ids:
                    # stock.immediate.transfer ийг хуулж авчирсан
                    if picking.state == 'draft':
                        picking.action_confirm()
                        if picking.state != 'assigned':
                            picking.action_assign()
                            if picking.state != 'assigned':
                                raise UserError(_("Could not reserve all requested products. Please use the \'Mark as Todo\' button to handle the reservation manually."))
                    for pack in picking.pack_operation_ids:
                        if pack.product_qty > 0:
                            pack.write({'qty_done': pack.product_qty})
                        else:
                            pack.unlink()
                    if not picking.picking_type_id.warehouse_id.lock_move or (picking.picking_type_id.warehouse_id.lock_move and self.confirmation_date >= picking.picking_type_id.warehouse_id.lock_move_until):
                        return picking.with_context(force_date=self.confirmation_date).do_transfer()
                    else:
                        raise UserError(_('Sorry, this warehouse is locked until %s') % picking.picking_type_id.warehouse_id.lock_move_until)
        return res

    @api.multi
    def _prepare_invoice(self):
        # @Override: Авлагын дансыг агуулахаас авдаг болгов
        self.ensure_one()
        invoice_vals = super(SaleOrder, self)._prepare_invoice()

        receivable_account = self.picking_type_id.warehouse_id.receivable_account if not self.warehouse_id else self.warehouse_id.receivable_account
        if self.company_id.use_wh_rec_pay_account and receivable_account:
            invoice_vals['account_id'] = receivable_account.id

        return invoice_vals

    @api.multi
    def _compute_sale_returned(self):
        for order in self:
            for picking_id in order.picking_ids:
                if picking_id.picking_type_id.code == 'incoming':
                    order.sale_returned = True
                    break

    @api.multi
    def _sale_returned_search(self, operator, value):
        recs = self.search([]).filtered(lambda x: x.sale_returned is True)
        if recs:
            return [('id', 'in', [x.id for x in recs])]
        else:
            return [('id', 'in', [])]