# -*- coding: utf-8 -*-


from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    @api.depends('product_id')
    def _compute_available_qty(self):
        for obj in self.filtered(lambda x: x.product_id):
            warehouse = obj.order_id.warehouse_id
            if not warehouse:
                raise UserError(_('Warning!\nYou must select supply warehouse before add order line!'))
            locations = self.env['stock.location'].search([('usage', '=', 'internal'), ('location_id', 'child_of', [warehouse.view_location_id.id])])
            loc_ids = [loc.id for loc in locations] if locations else []
            obj.available_qty = obj.product_id.get_qty_availability(loc_ids, obj.order_id.date_order, state=['done'])
            
            #Борлуулалт өөрийнхөө нөөцөлснйг давхар нөөцөлсөн гэж харуулаад байсан тул өөрийнх нь нөөцлөлтийг тооцдоггүй болгов
            self_assigned_qty = 0
            if obj.id:
                date = ("AND m.date <= '%s'" % obj.order_id.date_order) if self.env.user.company_id.availability_check_date == 'record_date' else ''
                self._cr.execute("""select m.product_qty as qty from stock_move m, procurement_order o where m.procurement_id=o.id and m.state='assigned' %s and o.sale_line_id=%s""" % (date, obj.id))
                results = self._cr.dictfetchall()
                if results and len(results) > 0 and results[0]['qty']:
                    self_assigned_qty = results[0]['qty']
            obj.assigned_qty = obj.product_id.get_qty_availability(loc_ids, obj.order_id.date_order, state=['assigned']) + self_assigned_qty
            
        for obj in self.filtered(lambda x: not x.product_id):
            obj.available_qty = 0
            obj.assigned_qty = 0

    available_qty = fields.Float(compute='_compute_available_qty', string='Available quantity', readonly=True)
    assigned_qty = fields.Float(compute='_compute_available_qty', string='Assigned quantity', readonly=True)
    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic Account')
