# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
import logging
from math import sin, cos, sqrt, atan2, radians

_logger = logging.getLogger(__name__)

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def action_confirm_mobile(self,latitude,longitude):
        # бараануудын id-г хадгалах хувьсагч зарлаж байна (нэг барааг олон удаа оруулах эсэхийг шалгахын тулд)
        product_ids = []
        # бараануудын нөөц-г хадгалах хувьсагч зарлаж байна(нэг бараа олон орсон үед тухайн барааг нэмэхийн тулд)
        product_qtys = []
        # тохиргоо цэсний check_qty_on_sale_order_approve талбар check утгатай үед энэхүү тооцооллыг хийнэ
        if self.company_id.check_qty_on_sale_order_approve == 'check':
            for obj in self.order_line:
                if obj.product_id.id not in product_ids: #нэг бараа хоёр удаа орсон эсэхийг шалгаж байна
                    product_ids.append(obj.product_id.id)
                    product_qtys.append(obj.product_uom_qty)
                    if obj.product_uom_qty > obj.available_qty:
                        return_string = '-3:%s:%s' % (obj.product_id.id, obj.available_qty)
                        return return_string
                else: #нэг бараа хоёр удаа орсон үед барааны тоог нэмж үлдэгдэлтэй жишч байна
                    product_qtys[product_ids.index(obj.product_id.id)] += obj.product_uom_qty
                    if product_qtys[product_ids.index(obj.product_id.id)] > obj.available_qty:
                        return_string = '-3:%s:%s' % (obj.product_id.id, obj.available_qty)
                        return return_string
        return super(SaleOrder, self).action_confirm_mobile(latitude, longitude)