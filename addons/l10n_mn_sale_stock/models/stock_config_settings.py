# -*- coding: utf-8 -*-

from odoo import fields, models, api, _


class StockSettings(models.TransientModel):
    _inherit = 'stock.config.settings'

    show_delivery_address = fields.Boolean(related='company_id.show_delivery_address', help='Show Delivery Address on Inventory Expense Report', string='Show Delivery Address')
