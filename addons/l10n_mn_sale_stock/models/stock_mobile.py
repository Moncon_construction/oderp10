# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, _
import time
from datetime import datetime
from odoo.exceptions import UserError
from pytz import timezone
import ast


class StockBackorderConfirmation(models.TransientModel):
    _inherit = 'stock.backorder.confirmation'

    @api.multi
    def process_mobile(self, id):
        """
            Picking батлах товч дархад дутагдлын захиалга үүсэхээр бол уг функцийг дуудна
        """
        self = self.with_env(self.env(cr=self.pool.cursor()))
        try:
            wiz = self.env['stock.backorder.confirmation'].create({'pick_id': id})
            wiz._process()
            picking = self.env['stock.picking'].search([('backorder_id', '=', wiz.pick_id.id)]);
            self._cr.commit()
            if picking:
                return picking.id
            else:
                return -1
        except Exception as message:
            self._cr.rollback()
            return message[0].encode('utf8')
        finally:
            self._cr.close()

    @api.multi
    def process_sale_mobile(self, id):
        """
            Picking батлах товч дархад дутагдлын захиалга үүсгэхгүйгээр батлах функц Борлуулалтын програмаас
        """
        self = self.with_env(self.env(cr=self.pool.cursor()))
        try:
            wiz = self.env['stock.backorder.confirmation'].create({'pick_id': id})
            wiz._process()
            picking = self.env['stock.picking'].search([('backorder_id', '=', wiz.pick_id.id)]);
            if picking:
                if picking.sale_id.amount_total > 0.0:
                    self.create_invoice_from_mobile(picking.id)
                self._cr.commit()
                return picking.id
            else:
                return -1
        except Exception as message:
            self._cr.rollback()
            return message[0].encode('utf8')
        finally:
            self._cr.close()


    @api.multi
    def process_cancel_backorder_mobile(self, id):
        """
            Picking -г Дутагдлын захиалга үүсгэхгүйгээр батлах бол уг функцийг дуудна
        """
        self = self.with_env(self.env(cr=self.pool.cursor()))
        try:
            wiz = self.env['stock.backorder.confirmation'].create({'pick_id': id})
            wiz.process_cancel_backorder()
            picking = self.env['stock.picking'].search([('id', '=', id)])
            self._cr.commit()
            if picking.state == 'done':
                return True
            else:
                return False
        except Exception as message:
            self._cr.rollback()
            return message[0].encode('utf8')
        finally:
            self._cr.close()

    @api.multi
    def process_cancel_backorder_sale_mobile(self, id):
        """
            Picking -г Дутагдлын захиалга үүсгэхгүйгээр батлах функц Борлуулалтын програмаас
        """
        self = self.with_env(self.env(cr=self.pool.cursor()))
        try:
            wiz = self.env['stock.backorder.confirmation'].create({'pick_id': id})
            wiz.process_cancel_backorder()
            picking = self.env['stock.picking'].search([('id', '=', id)])
            if picking.state == 'done':
                if picking.sale_id.amount_total > 0.0:
                    self.create_invoice_from_mobile(picking.id)
                self._cr.commit()
                return True
            else:
                return False
        except Exception as message:
            self._cr.rollback()
            return message[0].encode('utf8')
        finally:
            self._cr.close()

    @api.model
    def create_invoice_from_mobile(self, picking_id):
        self.env.cr.execute("select "
                            "so.id as id, so.name as name, sp.id as picking_id "
                            "from stock_move sm "
                            "JOIN stock_picking sp on sm.picking_id = sp.id "
                            "JOIN procurement_order po on po.id = sm.procurement_id "
                            "JOIN sale_order_line sol on sol.id = po.sale_line_id "
                            "JOIN sale_order so on so.id = sol.order_id "
                            "where "
                            "sm.origin_returned_move_id is NULL and sp.id = %s " % (picking_id))
        lines = self.env.cr.dictfetchall()
        line_ids = []
        for line in lines:
            line_ids.append(line['id'])
        sale_order_obj = self.env['sale.order'].search([('id', 'in', line_ids)])
        invoice_ids = sale_order_obj.action_invoice_create()
        invoice_objs = self.env['account.invoice'].search([('id', 'in', invoice_ids)])
        invoice_objs.action_invoice_open()
        sale_order_obj.write({'invoice_status': 'invoiced'})


class ResUsers(models.Model):
    _inherit = 'res.users'

    @api.multi
    def get_stock_user_role_mobile(self):
        """
            Групийн дагуу нэвтэрсэн хэрэглэгч Агуулахад хандах эрхтэй эсэх, үүргийг тодорхойлно
        """
        user_groups_ids = self.env.user.groups_id.ids
        group_id_manager = self.env.ref('stock.group_stock_manager').id
        if group_id_manager in user_groups_ids:
            return "group_stock_manager"
        group_id_user = self.env.ref('stock.group_stock_user').id
        if group_id_user in user_groups_ids:
            return "group_stock_user"
        return ""


class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.multi
    def get_res_partner_list_mobile(self, local_partners):
        """
        Мобайл төхөөрөмжөөс ResPartner-ийн жагсаалтыг татахад дуудагдах функц Агуулахын програмаас
        :param local_partners: Мобайл төхөөрөмжид байгаа харилцагчийн жагсаалтаас id,write_date
        утгууд дамжиж орж ирнэ
        :return: JSON бүтэцтэй харилцагчийн мэдээлэл буцаана
        """
        update_partner = []
        insert_partner = []
        local_ids = []
        result = []
        # Мобайл төхөөрөмжид байгаа мэдээлэл шинэчлэгдсэн эсэхийг write_date талбараар шалгаж
        # UPDATE хийх жагсаалтыг үүсгэж байна
        for partner in local_partners:
            local_ids.append(partner.get('id'))
            if not partner.get('write_date'):
                write_date = '2000-01-01 00:00:00.999999'
            else:
                write_date = partner.get('write_date') + '.999999'
            res_partner = self.env['res.partner'].search(
                [('id', '=', partner.get('id')), ('write_date', '>', write_date)])
            if res_partner:
                update_partner.append({'id': res_partner.id, 'name': res_partner.name,
                                       'is_company': res_partner.is_company,
                                       'street': res_partner.street,
                                       'street2': res_partner.street2,
                                       'city': res_partner.city,
                                       'zip': res_partner.zip,
                                       'website': res_partner.website,
                                       'phone': res_partner.phone,
                                       'mobile': res_partner.mobile,
                                       'email': res_partner.email,
                                       'company_id': res_partner.company_id.id,
                                       'parent_id': res_partner.parent_id.id,
                                       'state_id': res_partner.state_id.id,
                                       'country_id': res_partner.country_id.id,
                                       'customer': res_partner.customer,
                                       'supplier': res_partner.supplier,
                                       'company_name': res_partner.company_name,
                                       'write_date': res_partner.write_date})
        # Мобайл төхөөрөмжид огт Sync хийгдээгүй шинээр INSERT хийх харилцагчийн
        # жагсаалтыг үүсгэж байна
        insert_partners = self.env['res.partner'].search([('id', 'not in', tuple(local_ids))])
        for partner in insert_partners:
            insert_partner.append({'id': partner.id,
                                   'name': partner.name,
                                   'is_company': partner.is_company,
                                   'street': partner.street,
                                   'street2': partner.street2,
                                   'city': partner.city,
                                   'zip': partner.zip,
                                   'website': partner.website,
                                   'phone': partner.phone,
                                   'mobile': partner.mobile or '',
                                   'email': partner.email,
                                   'company_id': partner.company_id.id,
                                   'parent_id': partner.parent_id.id,
                                   'state_id': partner.state_id.id,
                                   'country_id': partner.country_id.id,
                                   'customer': partner.customer,
                                   'supplier': partner.supplier,
                                   'company_name': partner.company_name,
                                   'write_date': partner.write_date})
        result.append({'update': update_partner})
        result.append({'insert': insert_partner})
        return result


class StockPicking(models.Model):
    _inherit = 'stock.picking'
    transit_order_id = fields.Many2one(
        'stock.transit.order', string='Transit Order')

    @api.multi
    def do_new_transfer_mobile(self):
        """
            Гар утаснаас Picking-ийг батлах товч дарахад дуудагдах функц Агуулахын програмаас
        """
        self = self.with_env(self.env(cr=self.pool.cursor()))
        try:
            confirm_state = ["assigned", "partially_available"]
            if self.state == 'done':
                # Хөдөлгөөн хийхэд хэдийн батлагдсан байх үед
                return 'done'
            if self.state not in confirm_state:
                # Хөдөлгөөн хийхэд төлөв нь ялгаатай үед
                return 'different'
            if self.check_backorder():
                # Тоо ширхэгийн дутагдалтай хөдөлгөөн батлах үед
                return 'backorder'
            immediate = super(StockPicking, self).do_new_transfer()
            self.env['stock.immediate.transfer'].browse(immediate['res_id']).process()
            self._cr.commit()
            for picking in self:
                if picking.state == 'done':
                    return True
                else:
                    return ""
        except Exception as message:
            self._cr.rollback()
            return message[0].encode('utf8')
        finally:
            self._cr.close()

    @api.multi
    def do_new_transfer_sale_mobile(self, not_send=False):
        """
            Гар утаснаас Picking-ийг батлах товч дарахад дуудагдах функц Борлуулалтын програмаас
        """
        self = self.with_env(self.env(cr=self.pool.cursor()))
        try:
            confirm_state = ["assigned", "partially_available"]
            if self.state == 'done':
                # Хөдөлгөөн хийхэд хэдийн батлагдсан байх үед
                return 'done'
            if self.state not in confirm_state:
                # Хөдөлгөөн хийхэд төлөв нь ялгаатай үед
                return 'different'
            if self.check_backorder():
                # Тоо ширхэгийн дутагдалтай хөдөлгөөн батлах үед
                return 'backorder'
            immediate = super(StockPicking, self).do_new_transfer()
            self.env['stock.immediate.transfer'].browse(immediate['res_id']).process()
            for picking in self:
                if picking.state == 'done':
                    if picking.sale_id.amount_total > 0.0:
                        # Борлуулалтын үнийн дүн 0 - с их тохиолдолд нэхэмжлэх үүсгэж батална
                        self.env.cr.execute("select "
                                            "so.id as id, so.name as name, sp.id as picking_id "
                                            "from stock_move sm "
                                            "JOIN stock_picking sp on sm.picking_id = sp.id "
                                            "JOIN procurement_order po on po.id = sm.procurement_id "
                                            "JOIN sale_order_line sol on sol.id = po.sale_line_id "
                                            "JOIN sale_order so on so.id = sol.order_id "
                                            "where "
                                            "sm.origin_returned_move_id is NULL and sp.id = %s " % picking.id)
                        lines = self.env.cr.dictfetchall()
                        line_ids = []
                        for line in lines:
                            line_ids.append(line['id'])
                        sale_order_obj = self.env['sale.order'].search([('id', 'in', line_ids)])
                        invoice_ids = sale_order_obj.action_invoice_create()
                        invoice_objs = self.env['account.invoice'].search([('id', 'in', invoice_ids)])
                        """
                            НӨАТ-н VATPSP модул суусан тохиолдолд нэхэмжлэхийн not_send талбарын утгыг тодорхойлж өгнө  
                        """
                        self.env.cr.execute(
                            "select id as id, name as name, state as state "
                            "from ir_module_module "
                            "where name = '%s' limit 1" % ("l10n_mn_vatpsp"))
                        result = self.env.cr.dictfetchall()
                        if result[0]['state'] == 'installed':
                            invoice_objs.write({'not_send': not_send})
                        invoice_objs.action_invoice_open()
                        sale_order_obj.write({'invoice_status': 'invoiced'})
                    self._cr.commit()
                    return True
                else:
                    return False
        except Exception as message:
            self._cr.rollback()
            return message[0].encode('utf8')
        finally:
            self._cr.close()

    @api.multi
    def check_picking_state_mobile(self):
        """
            Picking - г батлахаас өмнө төлөвийг шалгах функц
        """
        for picking in self:
            return picking.state

    @api.multi
    def get_invoice_mobile(self):
        for picking in self:
            invoice_ids = -1
            if picking.state == 'done':
                self.env.cr.execute("select "
                                    "ai.id as id, ai.name as name, sp.id as picking_id "
                                    "from stock_move sm "
                                    "JOIN stock_picking sp on sm.picking_id = sp.id "
                                    "JOIN procurement_order po on po.id = sm.procurement_id "
                                    "JOIN sale_order_line sol on sol.id = po.sale_line_id "
                                    "JOIN sale_order so on so.id = sol.order_id "
                                    "JOIN sale_order_line_invoice_rel solir on solir.order_line_id = sol.id "
                                    "JOIN account_invoice_line as ail on ail.id = solir.invoice_line_id "
                                    "JOIN account_invoice ai on ai.id = ail.invoice_id "
                                    "where "
                                    "sm.origin_returned_move_id is NULL and ai.state = 'open' and sp.id = %s " %
                                    picking.id)
                lines = self.env.cr.dictfetchall()
                for line in lines:
                    invoice_ids = line['id']
                return invoice_ids
            else:
                return False

    @api.multi
    def get_stock_picking_list_mobile(self, date_limit, picking_type_ids, internal_id_list, not_synced_ids):
        """
            Бэлэн төлөвтэй болон Дотоод шилжүүлэг төрөлтэй Picking-үүдийг татна
        """
        picking_type_ids = ast.literal_eval(picking_type_ids)
        internal_id_list = ast.literal_eval(internal_id_list)
        not_synced_ids = ast.literal_eval(not_synced_ids)
        """
            1: Мобайл АПП дээр тохируулсан өгөгдөл татах хугацааны хязгаараар 
            2: Өөрт зөвшөөрөгдсөн агуулахуудын хөдөлгөөнөөр
            3: Хийгдсэн болон цуцлагдсан - с бусад хөдөлгөөн
            4: Дотоод хөдөлгөөн бол төлөв хамаарахгүй байх ЭСВЭЛ
            5: Гарах ирэх барааны хөдөлгөөн бол бэлэн болон зарим нь бэлэн хөдөлгөөнүүд
        """
        picking_objs = self.env['stock.picking'].search([('create_date', '>', date_limit),
                                                         ('picking_type_id', 'in', tuple(picking_type_ids)),
                                                         ('id', 'not in', tuple(not_synced_ids)),
                                                         ('state', 'not in', ['done', 'cancel']), '|',
                                                         ('picking_type_id', 'in', tuple(internal_id_list)),
                                                         ('state', 'in', ['assigned', 'partially_available'])])
        stock_pickings = []
        for line in picking_objs:
            move_line_data = []
            pack_operation_data = []
            """ 
                Хэрэв 'бэлэн' болон 'зарим хэсэг нь бэлэн' төлөвтэй хөдөлгөөн бол StockPackOperation,
                Бусад тохиолдолд тухайн хөдөлгөөнд хамааралтай StockMove татагдана
            """
            if line.state != 'assigned' and line.state != 'partially_available':
                move_line_obj = self.env['stock.move'].search([('id', 'in', line.move_lines.ids)])
                for m_line in move_line_obj:
                    move_line_data.append({'id': m_line.id, 'name': m_line.name, 'picking_id': m_line.picking_id.id,
                                           'product_id': m_line.product_id.id,
                                           'product_uom_qty': m_line.product_uom_qty,
                                           'product_uom': m_line.product_uom.id, 'location_id': m_line.location_id.id,
                                           'location_dest_id': m_line.location_dest_id.id, 'state': m_line.state,
                                           'company_id': m_line.company_id.id})
            else:
                pack_operation_obj = self.env['stock.pack.operation'].search(
                    [('id', 'in', line.pack_operation_product_ids.ids)])
                for p_line in pack_operation_obj:
                    pack_operation_lot_data = []
                    pack_operation_lot = self.env['stock.pack.operation.lot'].search(
                        [('id', 'in', p_line.pack_lot_ids.ids)])
                    for lotline in pack_operation_lot:
                        pack_operation_lot_data.append(
                            {'id': lotline.id, 'operation_id': lotline.operation_id.id, 'qty': lotline.qty,
                             'lot_id': lotline.lot_id.id, 'lot_name': lotline.lot_name, 'qty_todo': lotline.qty_todo})
                    pack_operation_data.append(
                        {'id': p_line.id, 'picking_id': p_line.picking_id.id, 'product_id': p_line.product_id.id,
                         'product_uom_id': p_line.product_uom_id.id, 'ordered_qty': p_line.ordered_qty,
                         'product_qty': p_line.product_qty, 'qty_done': p_line.qty_done,
                         'product_name': p_line.product_id.name,
                         'product_uom_name': p_line.product_uom_id.name, 'pack_lot_ids': pack_operation_lot_data})
            dic = {'id': line.id, 'name': line.name, 'origin': line.origin, 'partner_id': line.partner_id.id,
                   'location_id': line.location_id.id, 'location_dest_id': line.location_dest_id.id,
                   'picking_type_id': line.picking_type_id.id, 'min_date': line.min_date,
                   'move_lines': move_line_data, 'pack_operation_product_ids': pack_operation_data,
                   'state': line.state, 'partner_name': line.partner_id.name, 'backorder_name': line.backorder_id.name}
            stock_pickings.append(dic)
        return stock_pickings

    @api.multi
    def get_stock_picking_sale_mobile(self, date_limit, picking_type_ids):
        """
            Бэлэн төлөвтэй болон Дотоод шилжүүлэг төрөлтэй Picking-үүдийг татна

        """
        picking_type_ids = ast.literal_eval(picking_type_ids)
        """
            1: Мобайл АПП дээр тохируулсан өгөгдөл татах хугацааны хязгаараар 
            2: Өөрт зөвшөөрөгдсөн агуулахуудын хөдөлгөөнөөр
            3: Хийгдсэн болон цуцлагдсан - с бусад хөдөлгөөн
            4: Дотоод хөдөлгөөн бол төлөв хамаарахгүй байх ЭСВЭЛ
            5: Гарах ирэх барааны хөдөлгөөн бол бэлэн болон зарим нь бэлэн хөдөлгөөнүүд
        """
        picking_objs = self.env['stock.picking'].search([('create_date', '>', date_limit),
                                                         ('picking_type_id', 'in', tuple(picking_type_ids)),
                                                         ('state', 'in', ['assigned', 'partially_available']),
                                                         ('team_id', '=', self.env.user.sale_team_id.id)])
        stock_pickings = []
        for line in picking_objs:
            move_line_data = []
            pack_operation_data = []
            """ 
                Хэрэв 'бэлэн' болон 'зарим хэсэг нь бэлэн' төлөвтэй хөдөлгөөн бол StockPackOperation,
                Бусад тохиолдолд тухайн хөдөлгөөнд хамааралтай StockMove татагдана
            """
            if line.state != 'assigned' and line.state != 'partially_available':
                move_line_obj = self.env['stock.move'].search([('id', 'in', line.move_lines.ids)])
                for m_line in move_line_obj:
                    move_line_data.append({'id': m_line.id, 'name': m_line.name, 'picking_id': m_line.picking_id.id,
                                           'product_id': m_line.product_id.id,
                                           'product_uom_qty': m_line.product_uom_qty,
                                           'product_uom': m_line.product_uom.id, 'location_id': m_line.location_id.id,
                                           'location_dest_id': m_line.location_dest_id.id, 'state': m_line.state,
                                           'company_id': m_line.company_id.id})
            else:
                pack_operation_obj = self.env['stock.pack.operation'].search(
                    [('id', 'in', line.pack_operation_product_ids.ids)])
                for p_line in pack_operation_obj:
                    pack_operation_data.append(
                        {'id': p_line.id, 'picking_id': p_line.picking_id.id, 'product_id': p_line.product_id.id,
                         'product_uom_id': p_line.product_uom_id.id, 'ordered_qty': p_line.ordered_qty,
                         'product_qty': p_line.product_qty, 'qty_done': p_line.qty_done,
                         'product_name': p_line.product_id.name,
                         'product_uom_name': p_line.product_uom_id.name})
            dic = {'id': line.id, 'name': line.name, 'origin': line.origin, 'partner_id': line.partner_id.id,
                   'location_id': line.location_id.id, 'location_dest_id': line.location_dest_id.id,
                   'picking_type_id': line.picking_type_id.id, 'min_date': line.min_date,
                   'move_lines': move_line_data, 'pack_operation_product_ids': pack_operation_data,
                   'state': line.state, 'partner_name': line.partner_id.name}
            stock_pickings.append(dic)
        return stock_pickings


class StockInventory(models.Model):
    _inherit = 'stock.inventory'

    @api.multi
    def get_inventory_mobile(self, date_limit):
        inventory_objects = self.env['stock.inventory'].search([('create_date', '>', date_limit),
                                                                ('state', 'in', ['draft', 'confirm'])])
        inventory_data = []

        for row in inventory_objects:
            inventory_line_data = []
            inventory_line_obj = self.env['stock.inventory.line'].search([('id', 'in', row.line_ids.ids)])
            for line in inventory_line_obj:
                inventory_line_data.append({'id': line.id, 'inventory_id': line.inventory_id.id,
                                            'product_id': line.product_id.id, 'location_id': line.location_id.id,
                                            'product_qty': line.product_qty, 'theoretical_qty': line.theoretical_qty})
            dic = {'id': row.id, 'name': row.name, 'location_id': row.location_id.id, 'filter': row.filter,
                   'state': row.state, 'exhausted': row.exhausted, 'date': row.date, 'company_id': row.company_id.id,
                   'product_id': row.product_id.id, 'category_id': row.category_id.id, 'line_ids': inventory_line_data}
            inventory_data.append(dic)
        return inventory_data


class ProductProduct(models.Model):
    _inherit = 'product.product'

    @api.multi
    def get_product_ids_all_mobile(self, local_products):
        """
        :param local_products: Төхөөрөмжид байгаа барааны id болон write_date утгууд
        :return: Insert болон Update хийх барааны id - г илгээнэ
        """
        update_product = []
        local_ids = []
        result = []
        for product in local_products:
            local_ids.append(product.get('id'))
            self.env.cr.execute("""SELECT pp.id, pt.write_date
                            FROM product_product pp 
                            LEFT JOIN product_template pt ON (pt.id=pp.product_tmpl_id) 
                            WHERE pp.id = %s AND pt.write_date > '%s' 
                            AND company_id = %s AND pt.active = True""" % (str(product.get('id')),
                                                                           product.get('write_date'),
                                                                           self.env.user.company_id.id))
            product_query = self.env.cr.dictfetchall()
            if len(product_query) > 0:
                update_product.append({'id': product.get('id')})
        if len(local_products) > 0:
            self.env.cr.execute("SELECT pp.id "
                                "FROM product_product pp "
                                "LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id) "
                                "WHERE pt.active = True AND pp.id NOT IN " + str(tuple(local_ids)) + " AND "
                                                                                                     "company_id = " + str(
                self.env.user.company_id.id))
        else:
            self.env.cr.execute("SELECT pp.id "
                                "FROM product_product pp "
                                "LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id) "
                                "WHERE pt.active = True AND company_id = " + str(self.env.user.company_id.id))
        insert_product = self.env.cr.dictfetchall()
        self.env.cr.execute("SELECT COUNT(pp.id) AS total "
                            "FROM product_product pp "
                            "LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id) "
                            "WHERE pt.active = True AND pt.company_id = " + str(self.env.user.company_id.id))
        total_count = self.env.cr.dictfetchall()
        result.append({'update': update_product})
        result.append({'insert': insert_product})
        result.append({'total': total_count})
        return result

    @api.multi
    def get_product_mobile(self, id, lang):
        """
            Барааны мэдээлэлийг тухайн хэрэглэгчийн хэлээр татах функц (Зөвхөн нэг бараа)
        """
        self.env.context = {'lang': lang}
        res = []
        line = self.env['product.product'].browse(id)
        dic = {'id': line.id, 'name': line.name, 'default_code': line.default_code, 'barcode': line.barcode,
               'product_tmpl_id': line.product_tmpl_id.id, 'standard_price': line.standard_price,
               'uom_id': line.uom_id.id, 'write_date': line.product_tmpl_id.write_date,
               'tracking': line.product_tmpl_id.tracking}
        res.append(dic)
        return res

    @api.multi
    def get_product_list_mobile(self, ids, lang):
        """
            Барааны мэдээлэлийг тухайн хэрэглэгчийн хэлээр татах функц (Олон бараагаар)
        """
        self.env.context = {'lang': lang}
        ids = ast.literal_eval(ids)
        res = []

        self.env.cr.execute("SELECT pp.id, pt.name, pp.default_code, pp.barcode, pp.product_tmpl_id, pt.uom_id, "
                            "pt.write_date, pt.tracking, pt.list_price, pt.categ_id, pt.brand_name, pt.type, "
                            "pt.available_in_mobile, tr.tax_id "
                            "FROM product_product pp "
                            "LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id) "
                            "LEFT JOIN product_taxes_rel tr ON (tr.prod_id = pp.id) "
                            "WHERE pp.id in (%s) ORDER BY pp.id " % ','.join(str(a) for a in tuple(ids)))

        product = self.env.cr.dictfetchall()
        last_product = None
        for line in product:
            tax_ids = []
            if line.get('tax_id'):
                tax_ids.append(line.get('tax_id'))

            current_product = {'id': line.get('id'), 'name': line.get('name'), 'default_code': line.get('default_code'),
                            'barcode': line.get('barcode'), 'product_tmpl_id': line.get('product_tmpl_id'),
                            'standard_price': line.get('list_price'), 'uom_id': line.get('uom_id'),
                            'write_date': line.get('write_date'), 'tracking': line.get('tracking'),
                            'categ_id': line.get('categ_id'), 'brand_name': line.get('brand_name'),
                            'available_in_mobile': line.get('available_in_mobile'), 'type': line.get('type'),
                            'taxes_id': tax_ids}

            if last_product is None:
                    last_product = current_product
            else:
                if last_product.get('id') != current_product.get('id'):
                    res.append(last_product)
                    last_product = current_product
                else:
                    if last_product.get('taxes_id'):
                        tax_ids = last_product.get('taxes_id')
                    if current_product.get('taxes_id'):
                        tax_ids.extend(current_product.get('taxes_id'))
                    last_product.__setitem__('taxes_id', tax_ids)
        if last_product is not None:
            res.append(last_product)
        return res


class StockProductionLot(models.Model):
    _inherit = 'stock.production.lot'

    @api.multi
    def get_stock_production_lot_list(self, local_lots):
        update_lot = []
        local_ids = []
        result = []
        for lot in local_lots:
            local_ids.append(lot.get('id'))
            self.env.cr.execute("""SELECT spl.id, spl.name, spl.product_id, spl.write_date
                                        FROM stock_production_lot spl
                                        LEFT JOIN product_product pp ON (spl.product_id = pp.id) 
                                        LEFT JOIN product_template pt ON (pt.id=pp.product_tmpl_id) 
                                        WHERE spl.id = %s AND spl.write_date > '%s' 
                                        AND pt.company_id = %s""" % (str(lot.get('id')), lot.get('write_date'),
                                                                     self.env.user.company_id.id))
            lot_query = self.env.cr.dictfetchall()
            if len(lot_query) > 0:
                update_lot.append({'id': lot_query[0].get('id'), 'name': lot_query[0].get('name'),
                                   'product_id': lot_query[0].get('product_id'),
                                   'write_date': lot_query[0].get('write_date')})
        if len(local_lots) > 0:
            self.env.cr.execute("SELECT spl.id, spl.name, spl.product_id, spl.write_date "
                                "FROM stock_production_lot spl "
                                "LEFT JOIN product_product pp ON pp.id = spl.product_id "
                                "LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id) "
                                "WHERE pt.active = True AND spl.id NOT IN " + str(tuple(local_ids)) + " AND "
                                                                                                      "company_id = " +
                                str(self.env.user.company_id.id))
        else:
            self.env.cr.execute("SELECT spl.id, spl.name, spl.product_id, spl.write_date "
                                "FROM stock_production_lot spl "
                                "LEFT JOIN product_product pp ON pp.id = spl.product_id "
                                "LEFT JOIN product_template pt ON pt.id = pp.product_tmpl_id "
                                "WHERE pt.active = True AND company_id = " + str(self.env.user.company_id.id))
        insert_lot = self.env.cr.dictfetchall()
        result.append({'update': update_lot})
        result.append({'insert': insert_lot})
        return result


class InheritedStockTransitOrderLine(models.Model):
    _inherit = 'stock.transit.order.line'

    @api.multi
    def get_availability_product_qty_mobile(self, warehouse_id, ids):
        """
            Гар утсанд тухайн агуулахын барааны бэлэн байгаа тоо хэмжээг татах функц
        """
        ids = ast.literal_eval(ids)
        warehouse = self.env['stock.warehouse'].search([('id', '=', warehouse_id)])
        product_obj = self.env['product.product'].search([('id', 'in', ids)])
        locations = self.env['stock.location'].search(
            [('usage', '=', 'internal'), ('location_id', 'child_of', [warehouse.view_location_id.id])])
        loc_ids = []
        for loc in locations:
            loc_ids.append(loc.id)
        date_start = datetime.now(timezone('UTC')).strftime("%Y-%m-%d %H:%M:%S")
        availability_qty_list = []
        for product in product_obj:
            self._cr.execute("SELECT sum(quantity)::decimal(16,2) AS product_qty from stock_history "
                             "where date <= %s "
                             "and location_id in %s and product_id = %s ", (date_start, tuple(loc_ids), product.id))
            fetched = self._cr.fetchall()
            availability_qty_sum = 0.0
            if fetched != []:
                for qty in fetched:
                    if qty[0] != None:
                        availability_qty_sum += qty[0]
            case = {'id': product.id, 'qty': availability_qty_sum}
            availability_qty_list.append(case)
        return availability_qty_list


class StockQuant(models.Model):
    _inherit = 'stock.quant'

    @api.multi
    def get_product_availability_qty_sale_mobile(self, warehouse_id, ids):
        """
            Гар утсанд тухайн агуулахын барааны бэлэн байгаа тоо хэмжээг татах функц Борлуулалт
        """
        ids = ast.literal_eval(ids)
        warehouse = self.env['stock.warehouse'].search([('id', '=', warehouse_id)])
        product_obj = self.env['product.product'].search([('id', 'in', ids)])
        locations = self.env['stock.location'].search(
            [('usage', '=', 'internal'), ('location_id', 'child_of', [warehouse.view_location_id.id])])
        loc_ids = []
        for loc in locations:
            loc_ids.append(loc.id)
        availability_qty_list = []
        for product in product_obj:
            """
            Барааны боломжит нөөц = Гарт байгаа тоо  - Бусад хөдөлгөөнд нөөцлөгдсөн тоо хэмжээ
            """
            self._cr.execute("SELECT available.product_qty - assigned.assigned_qty AS forecast_qty "
                             "FROM ((SELECT CASE WHEN SUM(qty)::DECIMAL(16,2) > 0 "
                             "THEN SUM(qty)::DECIMAL(16,2) ELSE 0 END AS product_qty "
                             "FROM stock_quant "
                             "WHERE location_id IN %s AND product_id = %s) AS available "
                             "CROSS JOIN "
                             "(SELECT CASE WHEN SUM(product_qty)::DECIMAL(16,2) > 0 "
                             "THEN SUM(product_qty)::DECIMAL(16,2) ELSE 0 END AS assigned_qty "
                             "FROM stock_move "
                             "WHERE location_id IN %s AND product_id = %s AND state = 'assigned') AS assigned)",
                             (tuple(loc_ids), product.id, tuple(loc_ids), product.id))
            fetched = self._cr.fetchall()
            availability_qty_sum = 0.0
            if fetched != []:
                for qty in fetched:
                    if qty[0] != None:
                        availability_qty_sum += qty[0]
            case = {'id': product.id, 'qty': availability_qty_sum}
            availability_qty_list.append(case)
        return availability_qty_list

    @api.multi
    def get_stock_quant_list_mobile(self):
        """
            Өөрт зөвшөөрөгдсөн агуулахуудын барааны нөөц татах мобайлын функц
        """
        lot_stock_ids = []
        result = []
        for warehouses in self.env.user.allowed_warehouses:
            lot_stock_ids.append(warehouses.lot_stock_id.id)
        if len(lot_stock_ids) > 0:
            self.env.cr.execute("SELECT id, product_id, location_id, qty, lot_id "
                                "FROM stock_quant "
                                "WHERE location_id in (%s) AND company_id = %s" % (
                                    ','.join(str(a) for a in tuple(lot_stock_ids)), self.env.user.company_id.id))
            result = self.env.cr.dictfetchall()
        return result

class StockMove(models.Model):
    _inherit = 'stock.move'

    @api.multi
    def get_stock_move_list_mobile(self):
        """
            Өөрт зөвшөөрөгдсөн агуулахуудын барааны нөөц татах мобайлын функц
        """
        lot_stock_ids = []
        result = []

        for warehouses in self.env.user.allowed_warehouses:
            lot_stock_ids.append(warehouses.lot_stock_id.id)
            loc_id = warehouses.lot_stock_id.id
            query = """
                SELECT table1.id AS product_id, %s AS location_id,  ROUND(COALESCE(SUM(table1.in_qty) - SUM(table1.out_qty))::DECIMAL, 2) AS product_qty 
                FROM
                (
                    SELECT pp.id,
                        SUM(CASE WHEN m.location_dest_id = %s AND m.state = 'done' 
                        THEN COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0) ELSE 0 END) AS in_qty, 
                        SUM(CASE WHEN m.location_id = %s AND m.state = 'done' 
                        THEN COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0) ELSE 0 END) AS out_qty
                    FROM stock_move m
                    LEFT JOIN product_product pp ON (pp.id = m.product_id)
                    LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id)
                    LEFT JOIN product_uom uom_m ON (uom_m.id = m.product_uom)
                    LEFT JOIN product_uom uom_t ON (uom_t.id = pt.uom_id)
                    WHERE (m.location_id = %s OR m.location_dest_id = %s) AND m.location_id != m.location_dest_id
                    AND m.state = 'done' AND m.date <= '%s' GROUP BY pp.id
                ) table1 
                GROUP BY table1.id 
                HAVING ROUND(COALESCE(SUM(table1.in_qty) - SUM(table1.out_qty))::DECIMAL, 2) != 0 
            """ % (loc_id, loc_id, loc_id, loc_id, loc_id, datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
            self._cr.execute(query)
            results = self._cr.dictfetchall()
            for val in results:
                result.append({
                    'product_id': val.get("product_id"),
                    'location_id': val.get("location_id"),
                    'product_qty': val.get("product_qty"),
                })
        return result

class InheritedStockTransitOrder(models.Model):
    _inherit = 'stock.transit.order'

    @api.multi
    def get_stock_transit_order_list_mobile(self, date_limit, not_synced_ids):
        """
            Мобайлаас өөрт зөвшөөрөгдсөн агуулахуудын нөхөн дүүргэлтийн захиалгууд татах функц
        """
        not_synced_ids = ast.literal_eval(not_synced_ids)
        transit_orders = []
        user_obj = self.env.user
        if user_obj.allowed_warehouses.ids:
            transit_order_objs = self.env["stock.transit.order"].search([('create_date', '>', date_limit),
                                                                         ('warehouse_id', 'in',
                                                                          user_obj.allowed_warehouses.ids),
                                                                         ('state', '!=', 'done'),
                                                                         ('id', 'not in', tuple(not_synced_ids))])
        else:
            return transit_orders

        for line in transit_order_objs:
            order_line_data = []
            order_line_obj = self.env['stock.transit.order.line'].search([('id', 'in', line.order_line.ids)])
            for oline in order_line_obj:
                order_line_data.append({'id': oline.id,
                                        'name': oline.name,
                                        'product_id': oline.product_id.id,
                                        'product_qty': oline.product_qty,
                                        'product_uom': oline.product_uom.id,
                                        'state': oline.state,
                                        'price_unit': oline.price_unit,
                                        'availability': oline.availability,
                                        'date_planned': oline.date_planned,
                                        'company_id': oline.company_id.id,
                                        'transit_order_id': oline.transit_order_id.id})
            dic = {'id': line.id,
                   'name': line.name,
                   'warehouse_id': line.warehouse_id.id,
                   'receive_picking_type_id': line.receive_picking_type_id.id,
                   'supply_warehouse_id': line.supply_warehouse_id.id,
                   'supply_picking_type_id': line.supply_picking_type_id.id,
                   'user_id': line.user_id.id,
                   'order_line': order_line_data,
                   'company_id': line.company_id.id,
                   'date_order': line.date_order,
                   'state': line.state}
            transit_orders.append(dic)
        return transit_orders

    @api.multi
    def send_supply_chain_manager_mobile(self):
        """
            Гар утаснаас нөхөн дүүргэлтийн захиалга үүсгэхэд Supply Chain Manager үүдийг
            баримтын дагагчаар нэмж баримтыг илгээгдсэн төлөвт оруулна.
            Захиалгын дугаарыг шинээр авч буцаана
        """
        self = self.with_env(self.env(cr=self.pool.cursor()))
        try:
            if self.state == 'send':
                # Нөхөн дүүргэлтийн захиалга хэдийн илгээгдсэн төлөвт байх үед буцаах утга
                return self.state
            if self.state != 'draft':
                # Нөхөн дүүргэлтийн захиалга илгээхээс өмнөх төлөвт биш байх үед буцаах утга
                return 'different'
            if not self.order_line:
                raise UserError(
                    _('You cannot confirm replenishment order without any order lines.'))
            if self.warehouse_id.id == self.supply_warehouse_id.id:
                raise UserError(
                    _('You cannot confirm replenishment order with same warehouses !'))
            for order in self:
                for line in order.order_line:
                    if not line.product_qty:
                        raise UserError(_(
                            'Cannot confirm replenishment order !\n Because there are zero quantitied product in this replenishment order.'))

            order_name = self.env['ir.sequence'].next_by_code('stock.transit.order')

            group_supply_chain_manager_id = self.env.ref(
                'l10n_mn_stock.group_supply_chain_manager').id
            user_ids = self.env['res.users'].search(
                [('groups_id', 'in', [group_supply_chain_manager_id])])
            partner_ids = []
            for user in user_ids:
                partner_ids.append(user.partner_id)
            if not partner_ids:
                raise UserError(
                    _('System not defined supply chain manager'))
            if partner_ids != []:
                mail_invite = self.env['mail.wizard.invite'].with_context({
                    'default_res_model': 'stock.transit.order',
                    'default_res_id': self.id}).create({
                    'partner_ids': [(6, 0, [p.id for p in partner_ids])], 'send_mail': True})
                # Энэ функц нөхөн дүүргэлтийн үйлдлийг маш удааж байгаа тул тайлбар болгов. Хурдан ажилладаг болгож кодыг засах хэрэгтэй. Өнөржаргал
                # mail_invite.add_followers()
            self.write({'state': 'send', 'name': order_name})
            self._cr.commit()
            return order_name
        except Exception as message:
            self._cr.rollback()
            return message[0].encode('utf8')
        finally:
            self._cr.close()

    @api.multi
    def wkf_confirm_transit_mobile(self):
        """
            Гар утаснаас нөхөн дүүргэлтийн захиалга батлахад
            нөхөн дүүргэх дүүргэх дүрмийг шалган
            гарах бараа ирэх бараа үүсгэж төлөвт шилжсэн эсэх утга буцаана
        """
        self = self.with_env(self.env(cr=self.pool.cursor()))
        try:
            if self.state == 'confirmed':
                # Нөхөн дүүргэлтийн захиалга хэдийн батлагдсан байх үед буцаах утга
                return self.state
            if self.state != 'send':
                # Нөхөн дүүргэлтийн захиалга батлахаас өмнөх send төлөвт биш байх үед буцаах утга
                return 'different'
            group_obj = self.env["procurement.group"]
            for order in self:
                vals = self._prepare_procurement_group()
                if not order.procurement_group_id:
                    group_id = group_obj.create(vals)
                else:
                    group_id = order.procurement_group_id
                order.write({'procurement_group_id': group_id.id})
                procrement_order = order.order_line._action_procurement_create()
                if len(procrement_order) > 1:
                    procrement_order = procrement_order[0]
                if not procrement_order.rule_id or not procrement_order.rule_id.location_src_id:
                    return False
                move = self.env['stock.move'].search(
                    [('procurement_id', 'in', [procrement_order.id])])
                self._create_picking()
                if move.picking_id:
                    move.picking_id.write({'transit_order_id': order.id})
                order.write({'state': 'confirmed'})

                # Эхлээд гарах барааны хөдөлгөөнийг нөөцлөөд батална
                for picking in self.picking_ids:
                    if picking.picking_type_id.code == 'outgoing':
                        picking.action_assign()
                        if picking.state == 'assigned':
                            immediate = super(StockPicking, picking).do_new_transfer()
                            self.env['stock.immediate.transfer'].browse(immediate['res_id']).process()
                            if picking.state == 'done':
                                # Хүлээн авах хөдөлгөөнийг бэлэн төлөвт шилжүүлнэ
                                for picking_income in self.picking_ids:
                                    if picking_income.picking_type_id.code == 'incoming':
                                        picking_income.action_assign()
            self._cr.commit()
            return True
        except Exception as message:
            self._cr.rollback()
            return message[0].encode('utf8')
        finally:
            self._cr.close()
