# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from datetime import datetime
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    team_id = fields.Many2one('crm.team', string='Sales Team', store=True, readonly=True)
    related_sale_order_id = fields.Many2one('sale.order', 'Related Sale Order')