# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
import xlsxwriter
from odoo import api, fields, models, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from odoo.exceptions import UserError
import pytz
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

class SalesReportBySize(models.TransientModel):
    """
        Борлуулалтын размераарх тайлан
    """

    _name = 'sales.report.by.size'
    _description = "Sales Report By Size"

    def _default_warehouse(self):
        _warehouse_ids = []
        for warehouse in self.env.user.allowed_warehouses:
            _warehouse_ids.append(warehouse.id)
        if _warehouse_ids:
           return [('id', 'in', _warehouse_ids)]
        else:
            return False
        
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env['res.company']._company_default_get('report.sales'))
    report_type = fields.Selection([('sales_order', 'Sales Order'),
                                     ('by_invoice', 'Invoice'),
                                     ('shipment', 'Shipment'),
                                     ('done', 'Done'), ], string='Report Type', help='Choose type of sales report.\
                                        The report based on only sale order\'Sale Order\'.\
                                        The report based on invoice of sale orders. \'Invoice\' .\
                                        The report based on picking of sale orders. \'Shipment\'.\
                                        The report based on invoice, picking, sale order. \'Done\' ', default='shipment', required=True)
    date_from = fields.Date("Start Date", required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    date_to = fields.Date("End Date", required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    see_pos_order = fields.Boolean(string="See Pos Order", default=False)
    warehouses = fields.Many2many('stock.warehouse', 'sales_report_by_size_to_warehouse', 'report_id', 'warehouse_id', string="Warehouses", domain=_default_warehouse)
    categories = fields.Many2many('product.category', 'sales_report_by_size_to_category', 'report_id', 'category_id', string="Category")
    products = fields.Many2many('product.product', 'sales_report_by_size_to_product', 'report_id', 'product_id', string="Products")
    sizes = fields.Many2many('product.size', 'sales_report_by_size_to_size', 'report_id', 'size_id', string="Product Size")

    @api.onchange('warehouses')
    def onchange_warehouse_ids(self):
        loc_ids = []
        stock_setting = []
        """
        Нэвтэрсэн хэрэглэгчийн зөвшөөрөгдсөн агуулах шалгах
        """
        if self.env.user.allowed_warehouses:
            self.env.cr.execute("select * from stock_config_settings where company_id = %s order by id DESC LIMIT 1" % self.company_id.id)
            stock_setting = self.env.cr.dictfetchall()
        # Сонгосон агуулахууд
        if self.warehouses:
            for warehouse_id in self.warehouses:
                # Сонгосон агуулах болгоноос байрлалын хаяг авах
                loc_ids.append(warehouse_id.lot_stock_id.id)
            # location_ids талбарлуу домейн буцаах
            return {'domain': {'location_ids': [('id', 'in', loc_ids)]}}
        else:
            return {'domain': {'location_ids': [('id', 'in', False)]}}

    def _get_lines_from_stock_picking(self):
        warehouse_qry =  " AND ptype.warehouse_id IN (" + str(self.warehouses.ids)[1:len(str(self.warehouses.ids))-1] + ") " if str(self.warehouses.ids) != "[]" else ""
        pro_category_qry = " AND templ.categ_id IN (" + str(self.categories.ids)[1:len(str(self.categories.ids))-1] + ") " if str(self.categories.ids) != "[]" else ""
        product_qry = " AND pro.id IN (" + str(self.products.ids)[1:len(str(self.products.ids))-1] + ") " if str(self.products.ids) != "[]" else ""
        product_size_qry = " AND size.id IN (" + str(self.sizes.ids)[1:len(str(self.sizes.ids))-1] + ") " if str(self.sizes.ids) != "[]" else ""
        
        query = " \
                select size.id as size_id, size.name as size_name, \
                       cat.id as cat_id, cat.name as cat_name, spack.qty_done as qty\
                from stock_pack_operation spack \
                left join stock_picking sp \
                    on sp.id = spack.picking_id \
                left join stock_picking_type ptype \
                    on ptype.id = sp.picking_type_id \
                left join product_product pro \
                    on pro.id = spack.product_id \
                left join product_template templ \
                    on templ.id = pro.product_tmpl_id \
                left join product_size size \
                    on size.id = templ.size_id \
                left join product_category cat \
                    on cat.id = templ.categ_id \
                WHERE min_date BETWEEN '"  + str(self.date_from) + "' AND '"  + str(self.date_to) + "' " + warehouse_qry + pro_category_qry + product_qry + product_size_qry  
        
        self._cr.execute(query)
        results = self._cr.dictfetchall()
        return results if results else False
    
    def _get_lines_from_sale_order(self):
        warehouse_qry =  " AND so.warehouse_id IN (" + str(self.warehouses.ids)[1:len(str(self.warehouses.ids))-1] + ") " if str(self.warehouses.ids) != "[]" else ""
        pro_category_qry = " AND templ.categ_id IN (" + str(self.categories.ids)[1:len(str(self.categories.ids))-1] + ") " if str(self.categories.ids) != "[]" else ""
        product_qry = " AND pro.id IN (" + str(self.products.ids)[1:len(str(self.products.ids))-1] + ") " if str(self.products.ids) != "[]" else ""
        product_size_qry = " AND size.id IN (" + str(self.sizes.ids)[1:len(str(self.sizes.ids))-1] + ") " if str(self.sizes.ids) != "[]" else ""
        
        qty_qry = ""
        state_qry = ""
        if self.report_type == 'sales_order':
            qty_qry = ", product_uom_qty as qty "
        elif self.report_type == 'by_invoice':
            qty_qry = ", qty_invoiced as qty "
            state_qry = " AND sline.invoice_status = 'invoiced' "
        elif self.report_type == 'shipment':
            qty_qry = ", qty_delivered as qty "
            state_qry = " AND picking.state = 'done' "
        elif self.report_type == 'done':
            qty_qry = ", qty_delivered as qty "
            state_qry = " AND picking.state = 'done' AND sline.invoice_status = 'invoiced' "
        query = " \
                SELECT size.id as size_id, size.name as size_name, \
                       cat.id as cat_id, cat.name as cat_name \
                       " + str(qty_qry) + " \
                FROM sale_order_line sline \
                LEFT JOIN sale_order so \
                    ON so.id = sline.order_id \
                LEFT JOIN product_product pro \
                    ON pro.id = sline.product_id \
                LEFT JOIN product_template templ \
                    ON templ.id = pro.product_tmpl_id \
                LEFT JOIN product_size size \
                    ON size.id = templ.size_id \
                LEFT JOIN product_category cat \
                    ON cat.id = templ.categ_id \
                LEFT JOIN stock_picking picking \
                    ON picking.group_id = so.procurement_group_id \
                WHERE confirmation_date BETWEEN '"  + str(self.date_from) + "' AND '"  + str(self.date_to) + "' "\
                      " AND so.company_id = " + str(self.company_id.id) + state_qry + warehouse_qry + pro_category_qry + product_qry + product_size_qry  \
        
        self._cr.execute(query)
        results = self._cr.dictfetchall()
        return results if results else False
   
    def get_sum_formula(self, start_rowx, end_rowx):
        sheet.write_formula(rowx, 6, '{=SUM(' + xl_rowcol_to_cell(rowx - num, 6) + ':' + xl_rowcol_to_cell(rowx - 1, 6) + ')}', format_content_float_amount)
        formula = "="
        for i in range(6+j, 6*12+6+j, 6):
            formula += self.get_column_name_for_calculate(i)+str(rowx)
            if i != 6*12+j:
                formula += "+"
        
        return formula
    
    @api.multi
    def export_report(self):
        # create workbook
        report_obj = self
        user = self.env.user
        company = user.company_id
        
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        
        # create name
        report_name = _('Борлуулалтын размераарх тайлан')
        file_name = "%s_%s.xlsx" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('sale_report_by_product_size'), form_title=file_name).create({})
        
        # create formats
        format_text_left = {
            'font_name': 'Times New Roman',
            'font_size': 11,
            'align': 'left',
        }
        
        format_text_left_header = format_text_left.copy()
        format_text_left_header['bold'] = True
        
        format_text_center_header = format_text_left_header.copy()
        format_text_center_header['font_size'] = '12'
        format_text_center_header['align'] = 'center'
        
        format_text_left_bordered = format_text_left.copy()
        format_text_left_bordered['border'] = 1
        
        format_text_ghost = format_text_left.copy()
        format_text_ghost['font_color'] = '#ffffff'
        
        format_number_right_ghost = format_text_left.copy()
        format_number_right_ghost['num_format'] = '#,##0.00'
        format_number_right_ghost['font_color'] = '#ffffff'
        
        format_number_right_bordered = format_text_left_bordered.copy()
        format_number_right_bordered['num_format'] = '#,##0.00'
        format_number_right_bordered['align'] = 'right'
        
        format_number_right_bordered_colored = format_number_right_bordered.copy()
        format_number_right_bordered_colored['bg_color'] = '#ccffff'
        
        format_number_percent_right_bordered = format_number_right_bordered.copy()
        format_number_percent_right_bordered['num_format'] = '0.00%'
        
        format_number_percent_right_bordered_colored = format_number_percent_right_bordered.copy()
        format_number_percent_right_bordered_colored['bg_color'] = '#ccffff'
        
        format_column_header = format_text_left_bordered.copy()
        format_column_header['bold'] = True
        format_column_header['align'] = 'center'
        format_column_header['valign'] = 'center'
        format_column_header['bg_color'] = '#99ccff'
        format_column_header['text_wrap'] = 1
        
        format_text_center_bordered_colored = format_text_left_bordered.copy()
        format_text_center_bordered_colored['align'] = 'center'
        format_text_center_bordered_colored['valign'] = 'center'
        format_text_center_bordered_colored['bg_color'] = '#ccffff'
        
        format_column_footer_number = format_column_header.copy()
        format_column_footer_number['align'] = 'right'
        format_column_footer_number['num_format'] = '#,##0.00'
        
        format_column_footer_number_percent = format_column_footer_number.copy()
        format_column_footer_number_percent['num_format'] = '0.00%'

        # register formats
        format_text_left = book.add_format(format_text_left)
        format_text_left_header = book.add_format(format_text_left_header)
        format_text_center_header = book.add_format(format_text_center_header)
        format_text_left_bordered = book.add_format(format_text_left_bordered)
        format_number_right_bordered = book.add_format(format_number_right_bordered)
        format_number_percent_right_bordered = book.add_format(format_number_percent_right_bordered)
        format_column_header = book.add_format(format_column_header)
        format_column_header.set_text_wrap()
        format_text_center_bordered_colored = book.add_format(format_text_center_bordered_colored)
        format_column_footer_number = book.add_format(format_column_footer_number)
        format_column_footer_number_percent = book.add_format(format_column_footer_number_percent)
        format_number_right_bordered_colored = book.add_format(format_number_right_bordered_colored)
        format_number_percent_right_bordered_colored = book.add_format(format_number_percent_right_bordered_colored)
        format_number_right_ghost = book.add_format(format_number_right_ghost)
        format_text_ghost = book.add_format(format_text_ghost)
        
        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        
        # compute column
        sheet.set_column('A:A', 15)
        sheet.set_column('B:B', 25)
        sheet.set_column('C:C', 15)
        sheet.set_column('D:D', 15)
        sheet.set_column('E:E', 15)
        sheet.set_column('F:F', 15)
        sheet.set_row(7, 20)
        
        rowx = 0
        
        # create contents
        sheet.write(rowx, 0, _(u'Company name:'), format_text_left_header)
        sheet.merge_range(rowx, 1, rowx, 3, u'%s' % (company.name), format_text_left) 
        sheet.merge_range(rowx+2, 0, rowx+2, 3, '%s' % report_name, format_text_center_header) 
        sheet.write(rowx+4, 0, _(u'Warehouse:'), format_text_left_header)
        sheet.write(rowx+4, 1, u'%s' % (', '.join([wh.name for wh in self.warehouses])), format_text_left) 
        sheet.write(rowx+5, 0, _(u'Duration:'), format_text_left_header)
        sheet.write(rowx+5, 1, u'%s~%s' % (self.date_from, self.date_to), format_text_left) 
        
        lines_list = []
        sales_list = self._get_lines_from_sale_order()
        if sales_list:
            lines_list += sales_list
        if self.see_pos_order:
            pos_list = self._get_lines_from_stock_picking()
            if pos_list:
                lines_list += self._get_lines_from_stock_picking()
            
        '''
            @note: Доорхи 3 for давталтын тусламжтай query-дэж авсан lines_list-н мөрүүдийг дараахи загвар бүхий болгов.
            [
                'size_name': '12', 
                'size_id': 
                        [
                            'cat_name': 'cat', 
                            'cat_id': 5578,
                            'cat_info':
                                    [
                                    'qty': 12
                                    ]
                        ]
            ]
        '''
        
        size_dict_list = []
        if lines_list:
            for line in lines_list:
                new_size_dict = {}
                new_size_dict['size_name'] = line['size_name']
                new_size_dict['size_id'] = line['size_id']
                new_size_dict['size_info'] = []
                if new_size_dict not in size_dict_list:
                    size_dict_list.append(new_size_dict)
                    
            for line in lines_list:
                new_cat_dict = {}
                new_cat_dict['cat_name'] = line['cat_name']
                new_cat_dict['cat_id'] = line['cat_id']
                new_cat_dict['cat_info'] = []
                if size_dict_list:
                    for size_line in size_dict_list:
                        if size_line['size_id'] == line['size_id'] and (new_cat_dict not in size_line['size_info']):
                            size_line['size_info'].append(new_cat_dict)
                            
            for line in lines_list:
                new_pro_dict = {}
                new_pro_dict['qty'] = line['qty']
                if size_dict_list:
                    for size_line in size_dict_list:
                        for cat_line in size_line['size_info']:
                            if cat_line['cat_id'] == line['cat_id'] and size_line['size_id'] == line['size_id']:
                                cat_line['cat_info'].append(new_pro_dict)
          
        rowx += 6
        sheet.merge_range(rowx, 0, rowx+1, 0, _(u"Size"), format_column_header) 
        sheet.merge_range(rowx, 1, rowx+1, 1, _(u"Product Category"), format_column_header) 
        sheet.merge_range(rowx, 2, rowx+1, 2, _(u"Product QTY"), format_column_header) 
        sheet.merge_range(rowx, 3, rowx+1, 3, _(u"Percentage in Sale"), format_column_header) 
        
        rowx += 2
        
        footer_rowx = rowx    
        temp_rowx = rowx
        footer_sum_rowxs = [] 
        normal_row_indexes = []
        my_series = []
        
        ghost_number = []
        ghost_name = []
        cat_count = 1
        if size_dict_list:
            size_line_count = 0
            for size_line in size_dict_list:
                my_series.append({
                    'start': rowx+size_line_count+2,
                    'end': rowx+size_line_count+len(size_line['size_info'])+1
                })
                sheet.write(rowx+size_line_count, 0, size_line['size_name'], format_text_center_bordered_colored)
                sheet.write(rowx+size_line_count, 1, '', format_text_center_bordered_colored)
                cat_line_count = 0
                
                for cat_line in size_line['size_info']:
                    cat_line_count += 1
                    normal_row_indexes.append(rowx+size_line_count+cat_line_count)
                    sheet.write(rowx+size_line_count+cat_line_count, 0, '', format_text_left_bordered)
                    cat_count += 1
                    sheet.write(rowx+size_line_count+cat_line_count, 1, cat_line['cat_name'], format_text_left_bordered)
                    cat_total_count = 0
                    for pro_line in cat_line['cat_info']:
                        cat_total_count += pro_line['qty']
                    sheet.write(rowx+size_line_count+cat_line_count, 2, cat_total_count, format_number_right_bordered)
                    ghost_number.append(cat_total_count)
                    ghost_name.append(cat_line['cat_name'])
                
                sheet.write_formula(rowx+size_line_count, 2, '{=SUM(C' + str(rowx+size_line_count+2) + ':C' + str(rowx+size_line_count+cat_line_count+1) + ')}', format_number_right_bordered_colored)
                sheet.write_formula(rowx+size_line_count, 3, '{=SUM(D' + str(rowx+size_line_count+2) + ':D' + str(rowx+size_line_count+cat_line_count+1) + ')}', format_number_percent_right_bordered_colored)
                size_line_count += 1
                footer_sum_rowxs.append(rowx+size_line_count)
                rowx += cat_line_count
                
            rowx += size_line_count
            footer_rowx = rowx
            
            for i in range(len(normal_row_indexes)):
                formula = 'C' + str(normal_row_indexes[i]+1) + "/C" + str(footer_rowx+1)
                sheet.write_formula(normal_row_indexes[i], 3, formula, format_number_percent_right_bordered)
            
            sheet.merge_range(footer_rowx, 0, footer_rowx, 1, _(u"Total"), format_column_header)
            formula = ""
            for i in range(len(footer_sum_rowxs)):
                if i != len(footer_sum_rowxs)-1:
                    formula += ("C" + str(footer_sum_rowxs[i]) + "+")
                else:
                    formula += ("C" + str(footer_sum_rowxs[i]))
            sheet.write_formula(footer_rowx, 2, formula, format_column_footer_number)
            formula = 'C' + str(footer_rowx+1) + "/C" + str(footer_rowx+1)
            sheet.write_formula(footer_rowx, 3, formula, format_column_footer_number_percent)
            
        # Add Pie Chart
        if len(ghost_number) > 0:
            total_ghost = sum(ghost_number) if sum(ghost_number) > 0 else 0
            for i in range(len(ghost_number)):
                sheet.write(9+i, 6, ghost_name[i], format_text_ghost)
                sheet.write(9+i, 7, ghost_number[i], format_number_right_ghost)
                
            chart1 = book.add_chart({'type': 'pie'})
            chart1.set_title({'name': u'Борлуулалтын тайлан (размер)'})
            chart1.set_style(10)
            
            chart1.add_series({
                'categories': '=%s!$G$10:$G$%s' %(report_name, (len(ghost_number)+9)),
                'values':     '=%s!$H$10:$H$%s' %(report_name, (len(ghost_number)+9)),
            })
            chart1.add_series({
                'categories': '=%s!$G$10:$G$%s' %(report_name, (len(ghost_number)+9)),
                'values':     '=%s!$H$10:$H$%s' %(report_name, (len(ghost_number)+9)),
            })
            chart1.set_legend({'font': {'size': 8, 'font_name': 'Times New Roman'}})
            chart1.set_legend({'position': 'overlay_right'})
            chart1.set_size({'width': 800 + cat_count*2, 'height': 800 + cat_count*5})
            sheet.insert_chart('F7', chart1)
        
        sheet.set_zoom(70)
        book.close()
        
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report()

        
