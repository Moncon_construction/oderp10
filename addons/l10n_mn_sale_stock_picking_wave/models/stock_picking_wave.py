# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError

class StockPickingWave(models.Model):
    _inherit = "stock.picking.wave"
    
    team_id = fields.Many2one('crm.team', 'Sale Team')
    
    @api.onchange('team_id')
    def onchange_team_id(self):
        self.ensure_one()
        domain = {}
        picking_ids = False
        module = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_distribution_team_stock_picking_wave'),('state', 'in', ('installed', 'to upgrade'))])
        if module:
            if self.team_id and self.distribution_id:
                picking_ids = self.env['stock.picking'].search([('team_id', '=', self.team_id.id),('distribution_section_id', '=', self.distribution_id.id),('state', 'not in', ('cancel', 'done')), ('company_id', '=', self.company_id.id),('picking_type_id', '=', self.picking_type_id.id)])
            elif self.team_id:
                picking_ids = self.env['stock.picking'].search([('team_id', '=', self.team_id.id),('state', 'not in', ('cancel', 'done')), ('company_id', '=', self.company_id.id),('picking_type_id', '=', self.picking_type_id.id)])
            elif self.distribution_id:
                picking_ids = self.env['stock.picking'].search([('distribution_section_id', '=', self.distribution_id.id),('state', 'not in', ('cancel', 'done')), ('company_id', '=', self.company_id.id),('picking_type_id', '=', self.picking_type_id.id)])
            else:
                picking_ids = self.env['stock.picking'].search([('state', 'not in', ('cancel', 'done')), ('company_id', '=', self.company_id.id),('picking_type_id', '=', self.picking_type_id.id)])
        else:
            if self.team_id:
                picking_ids = self.env['stock.picking'].search([('team_id', '=', self.team_id.id),('state', 'not in', ('cancel', 'done')), ('company_id', '=', self.company_id.id),('picking_type_id', '=', self.picking_type_id.id)])
            else:
                picking_ids = self.env['stock.picking'].search([('state', 'not in', ('cancel', 'done')), ('company_id', '=', self.company_id.id),('picking_type_id', '=', self.picking_type_id.id)])
        domain['picking_ids'] = [('id', 'in', picking_ids.ids)]
        return {'domain': domain}