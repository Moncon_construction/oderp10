# -*- coding: utf-8 -*-
{
    'name': 'Sales Meeting',
    'version': '1.0',
    'category': 'GeoBI',
    'author': "Asterisk Technologies LLC",
    'license': 'AGPL-3',
    'website': 'http://asterisk-tech.mn',
    'depends': [
        'geoengine_calender'
    ],
    'data': [
        'views/sale_fail_reason_view.xml',
        'security/ir.model.access.csv',
        'wizard/sales_meeting_button_wizard_view.xml',
        'views/geo_calendar_view.xml',
        'views/res_partner_view.xml',
        'views/sales_team_view.xml',
        'views/sale_order_view.xml',
        'views/geo_calendar_event_view.xml'
    ],
    'installable': True,
    'description': 'Sales meeting'
}
