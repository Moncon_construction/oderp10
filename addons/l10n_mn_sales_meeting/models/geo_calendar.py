# coding=utf-8
from odoo import api, fields, models, _

import json
from pyproj import Proj, transform
from datetime import datetime

class CalendarEvent(models.Model):
    """Add geo_point to calendar.event"""
    _inherit = "calendar.event"

    state = fields.Selection(selection_add=[('open', _('Validate')),('cancel', _('Cancel')), ('done',_('Sale order created'))])
    sale_orders = fields.Many2many('sale.order','sale_order_calendar_event_rel', string='Sale Order', readonly=1)
    sale_orders_amount = fields.Float('Sale Order Amount', readonly=1)
    sale_fail_reason = fields.Many2one('sale.fail.reason', 'Sale order reason')
    sale_order_count = fields.Integer('Sale Orders', compute='_sale_order_count')
    event_latitude = fields.Float('Latitude')
    event_longitude = fields.Float('Longitude')
    
    @api.model
    def create(self, vals):
        # Уулзалт үүсэх үед газарзүйн байршил авах хэсэг
        res = super(CalendarEvent, self).create(vals)
        for event in res:
            if event.geo_point:
                coordinates_dict=json.loads(vals['geo_point'])
                x=coordinates_dict['coordinates'][0]
                y=coordinates_dict['coordinates'][1]
                inProj=Proj(init='epsg:3857')
                outProj=Proj(init='epsg:4326')
                lon, lat=transform(inProj, outProj, x, y)
                if coordinates_dict:
                    event.write({
                        'event_latitude':lat,
                        'event_longitude':lon
                    })
            else:
                event.geo_point = event.client_partner_id.geo_point
                event.event_latitude = event.client_partner_id.partner_latitude
                event.event_longitude = event.client_partner_id.partner_longitude
        return res

    @api.multi
    def write(self, vals):
        # Уулзалт засах үед газарзүйн байршил авах хэсэг
        for event in self:
            if 'geo_point' in vals.keys():
                if vals['geo_point']:
                    coordinates_dict = json.loads(vals['geo_point'])
                    x = coordinates_dict['coordinates'][0]
                    y = coordinates_dict['coordinates'][1]
                    inProj = Proj(init='epsg:3857')
                    outProj = Proj(init='epsg:4326')
                    lon, lat=transform(inProj, outProj, x, y)
                    if coordinates_dict:
                        vals.update({
                            'event_latitude': lat,
                            'event_longitude': lon
                        })
                else:
                    if event.client_partner_id:
                        vals.update({'geo_point': event.client_partner_id.geo_point,
                                     'event_latitude': event.client_partner_id.partner_latitude,
                                     'event_longitude': event.client_partner_id.partner_longitude})
        return super(CalendarEvent, self).write(vals)

    @api.depends('sale_orders')
    def _sale_order_count(self):
        # Хэдэн борлуулалтын захиалга үүссэн байгааг тоолох
        sale_orders = self.sale_orders
        counter = 0
        for sale_order in sale_orders:
            counter += 1
        self.sale_order_count = counter

    @api.onchange('sale_orders')
    def onchange_user_id(self):
        # Борлуулалтын захиалга нэмэгдэхэд төлбөр талбарын утга шинчлэх
        sum_amount = 0.0
        if self.sale_orders:
            for sale_order in self.sale_orders:
                sum_amount += sale_order.amount_total
            self.sale_orders_amount = sum_amount

    @api.onchange('state')
    def onchange_user_id(self):
        if self.state == 'draft':
            self.color_partner_id = 1
        elif self.state == 'cancel':
            self.color_partner_id = 2
        elif self.state == 'open':
            self.color_partner_id = 3
        elif self.state == 'done':
            self.color_partner_id = 4

    @api.multi
    def action_view_sale_orders(self):
        # Уулзалтаас борлуулалтын захиалга үүсгэх функц
        sale_orders = self.mapped('sale_orders')
        action = self.env.ref('sale.action_orders').read()[0]
        if len(sale_orders) > 1:
            action['domain'] = [('id', 'in', sale_orders.ids)]
        elif len(sale_orders) == 1:
            action['views'] = [(self.env.ref('sale.view_order_form').id, 'form')]
            action['res_id'] = sale_orders.ids[0]
        else:
            action = {'type': 'ir.actions.act_window_close'}
        return action

    @api.multi
    def cancel(self):
        self.write({'state': 'cancel'})

    @api.multi
    def draft(self):
        self.write({'state': 'draft'})

    @api.onchange('client_partner_id')
    def onchange_client_partner_id(self):
        super(CalendarEvent, self).onchange_client_partner_id()
        if self.client_partner_id:
            self.update({'section_id':self.client_partner_id.team_id.id,
                         'user_id': self.client_partner_id.user_id.id})

    @api.multi
    @api.depends('allday', 'start', 'stop')
    def _compute_dates(self):
        """ Adapt the value of start_date(time)/stop_date(time) according to start/stop fields and allday. Also, compute
            the duration for not allday meeting ; otherwise the duration is set to zero, since the meeting last all the day.
        """
        for meeting in self:
            if meeting.allday:
                meeting.start_date = meeting.start
                meeting.start_datetime = False
                meeting.stop_date = meeting.stop
                meeting.stop_datetime = False
                meeting.duration = 0.0
            else:
                meeting.start_date = datetime.strptime(meeting.start, '%Y-%m-%d %H:%M:%S').date() if meeting.start else False
                meeting.start_datetime = meeting.start
                meeting.stop_date = datetime.strptime(meeting.stop, '%Y-%m-%d %H:%M:%S').date() if meeting.stop else False
                meeting.stop_datetime = meeting.stop
                meeting.duration = self._get_duration(meeting.start, meeting.stop)