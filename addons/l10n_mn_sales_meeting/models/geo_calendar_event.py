from odoo import fields, tools, models, api
from odoo.tools.translate import _
from odoo.exceptions import Warning


class OpenCalendarEventAnalyzeReport(models.TransientModel):

    _inherit = "open.calendar.event.analyze.report"


class CalendarEventAnalyzeReport(models.Model):
    """ Calendar event Analysis """
    _inherit = "calendar.event.analyze.report"

    sale_orders_amount = fields.Integer('Sale Order Amount', readonly=True)
    sale_order_count = fields.Float('Sale Order Count', readonly=True)

    def init(self):
        """
            Calendar event Report
            @param cr: the current row, from the database cursor
        """
        tools.drop_view_if_exists(self._cr, 'calendar_event_analyze_report')
        self._cr.execute("""
            CREATE OR REPLACE VIEW calendar_event_analyze_report AS (
                SELECT
                    id,
                    c.start_date as start_date,
                    c.stop_date as stop_date,
                    c.sale_orders_amount as sale_orders_amount,
                    count(c.sale_orders_amount) as sale_order_count,
                    c.user_of_employee_id as user_of_employee_id,
                    c.section_id as section_id,
                    c.client_partner_id as client_partner_id,
                    c.manager_id as manager_id,
                    c.name as event_name,
                    count(id) as nbr_events,
                    c.done_datetime_char as done_datetime_char,

                    (select count(c2.id) from calendar_event c2 
                        where c2.is_geo_event = 'true' and 
                            c2.state='draft' and 
                            c2.user_of_employee_id=c.user_of_employee_id and  
                            c2.start_date >= c.start_date and c2.stop_date <= c.stop_date and
                            c2.client_partner_id = c.client_partner_id
                            ) as draft_events,

                    (select count(c3.id) from calendar_event c3 
                        where c3.is_geo_event = 'true' and 
                            c3.state='open' and  c3.start_date >= c.start_date and c3.stop_date <= c.stop_date and
                            c3.client_partner_id = c.client_partner_id and 
                            c3.user_of_employee_id=c.user_of_employee_id) as done_events
                FROM
                    calendar_event c
                WHERE c.active = 'true' and c.is_geo_event = 'true'
                GROUP BY c.id
            )""")

