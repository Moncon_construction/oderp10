from odoo import api, fields, models, _

class CalendarEvent(models.Model):
    """Add geo_point to calendar.event"""
    _inherit = "calendar.event"

    @api.multi
    def action_draft(self):
        self.draft()

    @api.multi
    def action_cancel(self):
        self.cancel()