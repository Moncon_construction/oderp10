# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api
import json
from pyproj import Proj, transform

class ResPartner(models.Model):
    _inherit = 'res.partner'


    def _counter_meeting(self):
        counter = 0
        meetings = ''
        for partner in self:
            meetings = self.env['calendar.event'].search([('client_partner_id','=',partner.id)])
            for meeting in meetings:
                counter += 1
            partner.counter_meeting = counter

    def _counter_meeting_salesman(self):
        counter_salesman = 0
        for partner in self:
            meetings = self.env['calendar.event'].search([('user_id','=',partner.user_id.id)])
            if partner.is_salesman:
                for meeting in meetings:
                    counter_salesman += 1
                partner.counter_meeting_salesman = counter_salesman

    counter_meeting = fields.Integer('Meetings', compute='_counter_meeting')
    counter_meeting_salesman = fields.Integer('Salesman meetings', compute='_counter_meeting_salesman')

    @api.multi
    def sale_order_partner(self):
        action = self.env.ref('geoengine_calender.call_geo_action_calendar_event').read()[0]
        action['domain'] = [('client_partner_id', 'in', self.ids)]
        return action

    @api.multi
    def sale_order_partner_salesman(self):
        action = self.env.ref('geoengine_calender.call_geo_action_calendar_event').read()[0]
        action['domain'] = [('user_id', '=', self.user_id.id)]
        return action

    @api.multi
    def write(self, vals):
        # Харилцагч дээр газрын зураг байршил бүртгэхэд координатын утга шинчлэх
        res = super(ResPartner, self).write(vals)
        for partner in self:
            if 'geo_point' in vals.keys():
                coordinates_dict = json.loads(vals['geo_point'])
                x = coordinates_dict['coordinates'][0]
                y = coordinates_dict['coordinates'][1]
                inProj = Proj(init='epsg:3857')
                outProj = Proj(init='epsg:4326')
                lon, lat=transform(inProj, outProj, x, y)
                if coordinates_dict:
                    partner.write({
                        'partner_latitude': lat,
                        'partner_longitude': lon,
                        'date_localization':fields.Date.context_today(partner)
                    })
        return res