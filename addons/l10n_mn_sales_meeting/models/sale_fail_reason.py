# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class SaleFailReason(models.Model):
    _name = 'sale.fail.reason'
    _description = u'Борлуулалтын захиалга үүсгээгүй шалтгаан'

    name = fields.Char('Sale order reason', required=True)
    reason = fields.Text('Reason explain', required=True)