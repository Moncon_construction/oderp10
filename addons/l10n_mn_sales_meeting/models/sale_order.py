# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    def _compute_sales_meeting(self):
        event_calendars = self.env['calendar.event'].search([('state','=','done')])
        for event_calendar in event_calendars:
            for order in event_calendar.sale_orders:
                if order.id == self.id:
                    self.sales_meeting = event_calendar.id

    sales_meeting = fields.Many2one('calendar.event','Sales Meeting', compute=_compute_sales_meeting)

    @api.model
    def create(self, vals):
        # Уулзалтаас борлуулалт үүсгэхэд борлуулалтын захиалга талбарлуу утга хийж байна.
        result = super(SaleOrder, self).create(vals)
        event_calendar = self.env['calendar.event'].browse(self._context.get('active_ids', []))
        total_ammount = event_calendar.sale_orders_amount
        for obj in result:
            total_ammount += obj.amount_total
            event_calendar.write({'sale_orders': [(4, obj.id)], 'sale_orders_amount': total_ammount, 'state': 'done'})
            event_calendar.message_post_with_view('mail.message_origin_link',
                                           values = {'self': event_calendar,'origin': obj},
                                       subtype_id = self.env.ref('mail.mt_note').id)
        return result

    @api.multi
    def unlink(self):
        # Борлуулалтын захиалга устгахад холбоотой уулзалтын төлөв солино
        res = super(SaleOrder, self).unlink()
        event_calendar = self.env['calendar.event'].browse(self._context.get('active_ids', []))
        event_calendar.write({'state': 'draft'})
        return res