# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api
import json

class CrmTeam(models.Model):
    _inherit = 'crm.team'

    def _counter_meeting(self):
        counter = 0
        meetings = ''
        for sales_team in self:
            meetings = self.env['calendar.event'].search([('section_id','=',sales_team.id)])
            for meeting in meetings:
                counter += 1
            sales_team.counter_meeting = counter

    counter_meeting = fields.Integer('Meetings', compute='_counter_meeting')

    @api.multi
    def sales_team_meeting(self):
        action = self.env.ref('geoengine_calender.call_geo_action_calendar_event').read()[0]
        action['domain'] = [('section_id', 'in', self.ids)]
        return action
