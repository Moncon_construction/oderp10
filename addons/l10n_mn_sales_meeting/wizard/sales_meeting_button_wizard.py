# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class SalesMeetingWizard(models.Model):
    _name = 'sales.meeting.register.wizard'

    sale_fail_reason = fields.Many2one('sale.fail.reason', 'Sale order reason')
    reason_for_sale = fields.Char('Explain reason for not sale')

    @api.multi
    def validate(self):
        event_calendar = self.env['calendar.event'].browse(self._context.get('active_ids', []))
        reason = ''
        if not event_calendar.description:
            reason = self.reason_for_sale
        else:
            reason = event_calendar.description + '\n ' + self.reason_for_sale

        done_datetime_char=str(fields.Datetime.now())
        event_calendar.write({'description': reason, 'state':'open', 'sale_fail_reason': self.sale_fail_reason.id, 'done_datetime':fields.Datetime.now(), 'done_datetime_char':done_datetime_char})