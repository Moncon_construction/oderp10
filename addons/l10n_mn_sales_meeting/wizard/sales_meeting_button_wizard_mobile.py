# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class SalesMeetingWizardMobile(models.Model):
    _inherit = 'sales.meeting.register.wizard'

    @api.multi
    def validate_mobile(self, event_id):
        # Мобайл аас хийгдсэн төлөвтэй болгох функц
        event_calendar = self.env['calendar.event'].browse(event_id)
        reason = ''
        if not event_calendar.description:
            reason = self.reason_for_sale
        else:
            reason = event_calendar.description + '\n ' + self.reason_for_sale

        done_datetime_char = str(fields.Datetime.now())
        event_calendar.write({'description': reason, 'state':'open', 'sale_fail_reason': self.sale_fail_reason.id, 'done_datetime':fields.Datetime.now(), 'done_datetime_char':done_datetime_char})
        return True