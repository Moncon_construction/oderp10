# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Sales Pos Distribution Report",
    'version': '1.0',
    'depends': ['l10n_mn_sales_pos_report','l10n_mn_distribution_team'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Report',
    'description': """
        Посын хүргэлтээр бүлэглэх нэмэгдсэн борлуулалтын дэлгэрэнгүй тайлан
    """,
    'data': [],
    'auto_install': True,
}
