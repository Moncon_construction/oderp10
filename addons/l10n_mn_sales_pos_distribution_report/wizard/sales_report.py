# -*- encoding: utf-8 -*-
##############################################################################
from odoo import api, models

class ReportSalesReport(models.TransientModel):
    """
        Борлуулалтын дэлгэрэнгүй тайлан
    """

    _inherit = 'report.sales'
    
    # query - гээр мэдээлэл татах хэсэгт түгээлтийн багаар бүлэглэх боломж нэмэв
    def _get_query(self, select, _from, where, order_by,  select_two, group_by, is_pos ):
        
        distribution_team_ids = []
        # Түгээлтийн баг сонгосон үед
        if self.distribution_team_ids:
            for distribution_team in self.distribution_team_ids:
                distribution_team_ids.append(distribution_team.id)
        
        if self.see_pos_order == 'pos_order' or is_pos == True:
            select += ', -1 as distribution_section_id'
        else:
            select += ', so.distribution_section_id as distribution_section_id'
        if self.stage_one == 'distribution':
            order_by += ' order by distribution_section_id'
            select_two += ", sales_order_report.distribution_section_id as distribution_section_id"
            group_by += ", sales_order_report.distribution_section_id"
            if distribution_team_ids and is_pos == False:
                where += 'AND so.distribution_section_id in (' + ','.join(map(str, distribution_team_ids)) + ')'
        elif self.stage_two == 'distribution':
            order_by += ', distribution_section_id'
            select_two += ", sales_order_report.distribution_section_id as distribution_section_id"
            group_by += ", sales_order_report.distribution_section_id"
            if distribution_team_ids and is_pos == False:
                where += 'AND so.distribution_section_id in (' + ','.join(map(str, distribution_team_ids)) + ')'
        elif self.stage_three == 'distribution':
            order_by += ', distribution_section_id'
            select_two += ", sales_order_report.distribution_section_id as distribution_section_id"
            group_by += ", sales_order_report.distribution_section_id"
            if distribution_team_ids and is_pos == False:
                where += 'AND so.distribution_section_id in (' + ','.join(map(str, distribution_team_ids)) + ')'
        res = super(ReportSalesReport, self)._get_query(select, _from, where, order_by,  select_two, group_by, is_pos)
        return res
    
    # Тайлангийн бүлэглэлтээс хамааран өгөгдлийг эрэмбэлэх хэсэг
    def _get_order_by_pos_and_sale_data(self):
        # Бүлэглэх
        order_by, select_two, group_by = super(ReportSalesReport, self)._get_order_by_pos_and_sale_data()
        if self.stage_one == 'distribution':
            order_by += ' order by distribution_section_id'
            select_two += ", sales_order_report1.distribution_section_id as distribution_section_id"
            group_by += ", sales_order_report1.distribution_section_id"
        elif self.stage_two == 'distribution':
            order_by += ', distribution_section_id'
            select_two += ", sales_order_report1.distribution_section_id as distribution_section_id"
            group_by += ", sales_order_report1.distribution_section_id"
        elif self.stage_three == 'distribution':
            order_by += ', distribution_section_id'
            select_two += ", sales_order_report1.distribution_section_id as distribution_section_id"
            group_by += ", sales_order_report1.distribution_section_id"
        return order_by, select_two, group_by
    