# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Sales Pos Report",
    'version': '1.0',
    'depends': ['l10n_mn_sales_report'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Report',
    'description': """
        Посын борлуулалтын дэлгэрэнгүй тайлан
    """,
    'data': [
        'wizard/sales_report_view.xml',
    ]
}
