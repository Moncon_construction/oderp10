# -*- encoding: utf-8 -*-
import base64
import time
import xlsxwriter
from io import BytesIO
from odoo import api, fields, models, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
import pytz
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from multiprocessing import Condition

class ReportSalesReport(models.TransientModel):
    """
        Борлуулалтын дэлгэрэнгүй тайланд Посын борлуулалтыг гаргах
    """
    _inherit = 'report.sales'
    
    @api.onchange('report_type')
    def onchange_report_type(self):
        self.invis_profit = True if self.report_type not in ('shipment','done') else False
        if self.report_type == 'loan':
            self.see_pos_order = 'sale_order' 

    see_pos_order = fields.Selection([('sale_order', 'Only Sale Order'),
                                  ('pos_order', 'Only POS Order'),
                                  ('pos_and_sale_order', 'POS + Sale Order')], default='sale_order',string='See Pos Order')

    # query - гээр мэдээлэл татах хэсгийг посын мэдээлэл авахаар дарж бичив
    def _get_query(self, select, _from, where, order_by,  select_two, group_by, is_pos ):
        warehouse_ids = []
        location_ids = []
        category_ids = []
        product_ids = []
        partner_ids = []
        salesman_ids = []
        lot_ids = []
        brand_ids = []
        salesteam_ids = []
        query = brand_qry = supplier_qry = wheree = serial = " "
        warehouse_ids, location_ids, category_ids, product_ids, partner_ids, salesman_ids, lot_ids, brand_ids, salesteam_ids, supplier_ids, serial  = self._get_wizard_data()
        if self.see_pos_order == 'pos_order' or is_pos == True:
            wheree = """ AND sw.id in %s AND pt.categ_id in %s AND sw.lot_stock_id in %s """ % (tuple(warehouse_ids), tuple(category_ids), tuple(location_ids))
            query += """SELECT
                    pt.default_code as code, pt.name as product_name, pt.type as pt_type, pos.user_id as salesman_id, 
                    posl.product_id as product_id, posl.standard_price as standard_price, pu.name as unitname, pt.supplier_id as supplier_id, 
                    spl.name as lot_name, spl.id as lot_id, pt.brand_name as brand,sw.lot_stock_id as location_id, sw.id as warehouse_id, pt.categ_id as cat_id  %s 
                FROM  %s 
                    LEFT JOIN product_product pp on pp.id = posl.product_id
                    LEFT JOIN product_template pt on pt.id = pp.product_tmpl_id
                    LEFT JOIN product_uom pu on pu.id = pt.uom_id
                    LEFT JOIN stock_warehouse sw on sw.lot_stock_id = pos.location_id
                    LEFT JOIN stock_production_lot spl on spl.product_id = pp.id
                WHERE
                       posl.company_id = %s  
                       %s AND posl.product_id in %s %s %s """ % (select, _from, self.company_id.id, where, tuple(product_ids), serial, wheree)
        if self.see_pos_order != 'pos_order' and is_pos == False:
            wheree = """ AND so.warehouse_id in %s AND pt.categ_id in %s AND sw.lot_stock_id in %s """ % (
                        tuple(warehouse_ids), tuple(category_ids), tuple(location_ids)) 
            if partner_ids:        
                wheree += ' AND sol.order_partner_id in (' + ','.join(map(str, partner_ids)) + ')'
            if salesman_ids:        
                wheree += ' AND sol.salesman_id in (' + ','.join(map(str, salesman_ids)) + ')'
            if salesteam_ids:        
                wheree += ' AND so.team_id in (' + ','.join(map(str, salesteam_ids)) + ')'
            if brand_ids:        
                wheree += ' AND pt.brand_name in (' + ','.join(map(str, brand_ids)) + ')'
            if supplier_ids:        
                wheree += ' AND pt.supplier_id in (' + ','.join(map(str, supplier_ids)) + ')'
            query += """SELECT
                               pt.default_code as code,pt.type as pt_type,sol.product_id as product_id, pt.name as product_name, 
                               sol.purchase_price as standard_price, pu.name as unitname,
                               pt.brand_name as brand, pt.supplier_id as supplier_id,
                               spl.name as lot_name ,spl.id as lot_id , so.team_id as salesteam_id,
                               so.partner_id as customer_id, sol.salesman_id as salesman_id, sol.order_partner_id as partner_id,
                           sw.lot_stock_id as location_id, sw.id as warehouse_id, pt.categ_id as cat_id %s 
                           FROM %s
                               LEFT JOIN product_product pp on pp.id = sol.product_id
                               LEFT JOIN product_template pt on pt.id = pp.product_tmpl_id
                               LEFT JOIN product_uom pu on pu.id = sol.product_uom
                               JOIN sale_order so on so.id = sol.order_id 
                               LEFT JOIN sale_category sc on sc.id = so.sale_category_id
                               LEFT JOIN stock_warehouse sw on sw.id = so.warehouse_id
                               LEFT JOIN stock_production_lot spl on spl.product_id = pp.id
                           WHERE
                               sol.company_id = %s  
                               %s AND sol.product_id in %s %s %s """ % (select, _from, self.company_id.id, where, tuple(product_ids), serial, wheree )
            query = query
        if self.see_pos_order == 'pos_and_sale_order':
            order_by = ""
        balanced_query = """SELECT 
                               sales_order_report.code as code,  sales_order_report.pt_type as pt_type, sales_order_report.product_name as product_name, SUM(sales_order_report.qty) as qty, 
                               SUM(sales_order_report.without_tax_discount) as without_tax_discount, SUM(sales_order_report.tax_discount) as tax_discount, 
                               sales_order_report.product_id as product_id, SUM(sales_order_report.standard_price) as standard_price, sales_order_report.unitname as unitname, 
                               sales_order_report.lot_name as lot_name,sales_order_report.lot_id as lot_id,
                               SUM(sales_order_report.price) as price, SUM(sales_order_report.tax) as tax, SUM(sales_order_report.price_unit) as price_unit,
                                SUM(CASE WHEN (sales_order_report.ref_price <> 0) THEN ((sales_order_report.ref_price)) ELSE 0 END) AS rev_price,
                                SUM(CASE WHEN (sales_order_report.ref_qty <> 0) THEN ((sales_order_report.ref_qty)) ELSE 0 END) AS rev_qty,
                                SUM(CASE WHEN (sales_order_report.ref_tax <> 0) THEN ((sales_order_report.ref_tax)) ELSE 0 END) AS rev_tax  %s
                                          FROM
                                              (%s) as sales_order_report
                                          GROUP BY
                                                sales_order_report.code,
                                                sales_order_report.product_id,
                                                sales_order_report.pt_type,
                                                sales_order_report.product_name,
                                                sales_order_report.lot_name,
                                                sales_order_report.lot_id,
                                                sales_order_report.unitname 
                                                %s  %s 
                                    """ % (select_two, query, group_by, order_by)
        return balanced_query
    
    # Посын өгөгдөл татах хэсэг
    def get_pos_data(self, select, _from, order_by, select_two, salesteam_ids):
        select = ", posl.price_unit as price_unit, usr.sale_team_id as salesteam_id, " \
                "CASE WHEN (1-posl.discount/100 <> 0) THEN ((posl.price_subtotal / (1-posl.discount/100) - posl.price_subtotal)) ELSE 0 END AS without_tax_discount, " \
                "CASE WHEN (1-posl.discount/100 <> 0) THEN (((posl.price_subtotal_incl - posl.price_subtotal) * posl.discount/100) / (1-posl.discount/100)) ELSE 0 END as tax_discount, " \
                " -1 as customer_id, "\
                "CASE WHEN (posl.qty > 0) THEN (posl.qty) ELSE 0 END AS qty," \
                "CASE WHEN (posl.qty > 0) THEN (CASE WHEN (1-posl.discount/100 <> 0) THEN (posl.price_subtotal/(1-posl.discount/100)) ELSE 0 END) ELSE 0 END AS price," \
                "CASE WHEN (posl.qty > 0) THEN (CASE WHEN (1-posl.discount/100 <> 0) THEN (((posl.price_subtotal_incl - posl.price_subtotal) / (1 - posl.discount/100))) ELSE 0 END) ELSE 0 END AS tax," \
                "CASE WHEN (posl.qty < 0) THEN (posl.qty * (-1)) ELSE 0 END AS ref_qty, " \
                "CASE WHEN (posl.qty < 0) THEN (CASE WHEN (1-posl.discount/100 <> 0) THEN ((posl.price_subtotal * (-1)/(1-posl.discount/100))) ELSE 0 END) ELSE 0 END AS ref_price, " \
                "CASE WHEN (posl.qty < 0) THEN (CASE WHEN (1-posl.discount/100 <> 0) THEN ((((posl.price_subtotal_incl - posl.price_subtotal) / (1 - posl.discount/100)) * (-1))) ELSE 0 END) ELSE 0 END AS ref_tax " 
        if self.see_pos_order == 'pos_order':
            select_two += ", SUM(sales_order_report.sub_total_price) as sub_total_price"
        _from = " pos_order_line posl  " \
                "JOIN pos_order pos on posl.order_id = pos.id " \
                "LEFT JOIN res_users usr on usr.id = pos.user_id "
        return select, select_two, _from
    
    
        # Посын өгөгдөл татах хэсэг
    def get_pos_and_sale_data(self, balanced_query1, balanced_query2, select_two, group_by, order_by):
        balanced_query =  """SELECT 
                      sales_order_report1.code as code,  sales_order_report1.pt_type as pt_type, sales_order_report1.product_name as product_name, SUM(sales_order_report1.qty) as qty, 
                      SUM(sales_order_report1.without_tax_discount) as without_tax_discount, SUM(sales_order_report1.tax_discount) as tax_discount, 
                      sales_order_report1.product_id as product_id, SUM(sales_order_report1.standard_price) as standard_price, sales_order_report1.unitname as unitname, 
                      sales_order_report1.lot_name as lot_name,sales_order_report1.lot_id as lot_id,
                      SUM(sales_order_report1.price) as price, SUM(sales_order_report1.tax) as tax, SUM(sales_order_report1.price_unit) as price_unit, 
                       SUM(sales_order_report1.rev_price) AS rev_price,
                       SUM(sales_order_report1.rev_qty) AS rev_qty,
                       SUM(sales_order_report1.rev_tax) AS rev_tax  , SUM(sales_order_report1.sub_total_price) as sub_total_price """ +  select_two + """
                     FROM (""" + balanced_query1 + " UNION " + balanced_query2 + """ ) as sales_order_report1 GROUP BY
                                       sales_order_report1.code,
                                       sales_order_report1.pt_type,
                                       sales_order_report1.product_id,
                                       sales_order_report1.product_name,
                                       sales_order_report1.lot_name,
                                       sales_order_report1.lot_id,
                                       sales_order_report1.unitname %s %s """ % (group_by, order_by)
        return balanced_query
    
    # Тайлангийн бүлэглэлтээс хамааран өгөгдлийг эрэмбэлэх хэсэг
    def _get_order_by_pos_and_sale_data(self):
        # Бүлэглэх
        order_by = ''
        select_two = ''
        group_by = ''
        if self.stage_one == 'warehouse':
            order_by += ' order by warehouse_id'
            select_two += ", sales_order_report1.warehouse_id as warehouse_id"
            group_by += ", sales_order_report1.warehouse_id"
        elif self.stage_one == 'location':
            order_by += ' order by location_id'
            select_two += ", sales_order_report1.location_id as location_id"
            group_by += ", sales_order_report1.location_id"
        elif self.stage_one == 'categ':
            order_by += ' order by cat_id'
            select_two += ", sales_order_report1.cat_id as cat_id"
            group_by += ", sales_order_report1.cat_id"
        elif self.stage_one == 'brand':
            select_two += ", sales_order_report1.brand as brand"
            order_by += ' order by brand'
            group_by += ", sales_order_report1.brand"
        elif self.stage_one == 'salesman':
            order_by += ' order by salesman_id'
            select_two += ", sales_order_report1.salesman_id as salesman_id"
            group_by += ", sales_order_report1.salesman_id"
        elif self.stage_one == 'salesteam':
            order_by += ' order by salesteam_id'
            select_two += ", sales_order_report1.salesteam_id as salesteam_id"
            group_by += ", sales_order_report1.salesteam_id"
        elif self.stage_one == 'customer':
            order_by += ' order by customer_id'
            select_two += ", sales_order_report1.customer_id as customer_id"
            group_by += ", sales_order_report1.customer_id"
        elif self.stage_one == 'supplier':
            order_by += ' order by supplier_id'
            select_two += ", sales_order_report1.supplier_id as supplier_id"
            group_by += ", sales_order_report1.supplier_id"
        if self.stage_two == 'warehouse':
            order_by += ', warehouse_id'
            select_two += ", sales_order_report1.warehouse_id as warehouse_id"
            group_by += ", sales_order_report1.warehouse_id"
        elif self.stage_two == 'location' and not self.invis_location:
            order_by += ', location_id'
            select_two += ", sales_order_report1.warehouse_id as location_id"
            group_by += ", sales_order_report1.location_id"
        elif self.stage_two == 'categ':
            order_by += ', cat_id'
            select_two += ", sales_order_report1.cat_id as cat_id"
            group_by += ", sales_order_report1.cat_id"
        elif self.stage_two == 'brand':
            order_by += ', brand'
            select_two += ", sales_order_report1.brand as brand"
            group_by += ", sales_order_report1.brand"
        elif self.stage_two == 'salesman':
            order_by += ', salesman_id'
            select_two += ", sales_order_report1.salesman_id as salesman_id"
            group_by += ", sales_order_report1.salesman_id"
        elif self.stage_two == 'salesteam':
            order_by += ', salesteam_id'
            select_two += ", sales_order_report1.salesteam_id as salesteam_id"
            group_by += ", sales_order_report1.salesteam_id"
        elif self.stage_two == 'customer':
            order_by += ', customer_id'
            select_two += ", sales_order_report1.customer_id as customer_id"
            group_by += ", sales_order_report1.customer_id"
        elif self.stage_two == 'supplier':
            order_by += ', supplier_id'
            select_two += ", sales_order_report1.supplier_id as supplier_id"
            group_by += ", sales_order_report1.supplier_id"
        if self.stage_three == 'warehouse':
            order_by += ', warehouse_id'
            select_two += ", sales_order_report1.warehouse_id as warehouse_id"
            group_by += ", sales_order_report1.warehouse_id"
        elif self.stage_three == 'location' and not self.invis_location:
            order_by += ', location_id'
            select_two += ", sales_order_report1.location_id as location_id"
            group_by += ", sales_order_report1.location_id"
        elif self.stage_three == 'categ':
            order_by += ', cat_id'
            select_two += ", sales_order_report1.cat_id as cat_id"
            group_by += ", sales_order_report1.cat_id"
        elif self.stage_three == 'brand':
            order_by += ', brand'
            select_two += ", sales_order_report1.brand as brand"
            group_by += ", sales_order_report1.brand"
        elif self.stage_three == 'salesman':
            order_by += ', salesman_id'
            select_two += ", sales_order_report1.salesman_id as salesman_id"
            group_by += ", sales_order_report1.salesman_id"
        elif self.stage_three == 'salesteam':
            order_by += ', salesteam_id'
            select_two += ", sales_order_report1.salesteam_id as salesteam_id"
            group_by += ", sales_order_report1.salesteam_id"
        elif self.stage_three == 'customer':
            order_by += ', customer_id'
            select_two += ", sales_order_report1.customer_id as customer_id"
            group_by += ", sales_order_report1.customer_id"
        elif self.stage_three == 'supplier':
            order_by += ', supplier_id'
            select_two += ", sales_order_report1.supplier_id as supplier_id"
            group_by += ", sales_order_report1.supplier_id"
        return order_by, select_two, group_by
                                                
    # Тайлан дээр гарах өгөгдөл татах хэсэгт посын өгөгдлийг авах хэсгийг нэмэн дарж бичив
    def get_data(self):
        
        _from = ''
        select = ''
        select_two = ''
        order_by_sale_pos = select_two_sale_pos = group_by_sale_pos = ""
        data = []
        is_pos = False
        order_by, select, select_two, where, group_by = self._get_order_by_data()
        warehouse_ids, location_ids, category_ids, product_ids, partner_ids, salesman_ids, lot_ids, brand_ids, salesteam_ids, supplier_ids, serial = self._get_wizard_data()
        
        order_by_sale_pos, select_two_sale_pos, group_by_sale_pos = self._get_order_by_pos_and_sale_data()
        # Хэрэглэгчийн цагийн бүст тохируулж цагаа сонгоно
        datetime_from = datetime.strptime(self.date_from + ' 00:00:00', DEFAULT_SERVER_DATETIME_FORMAT)
        datetime_to = datetime.strptime(self.date_to + ' 23:59:59', DEFAULT_SERVER_DATETIME_FORMAT)
        user = self.env['res.users'].browse(self._uid)
        user_time_zone = pytz.UTC
        if user.partner_id.tz:
            user_time_zone = pytz.timezone(user.partner_id.tz)
        datetime_to_str = user_time_zone.localize(datetime.strptime(str(datetime_to), DEFAULT_SERVER_DATETIME_FORMAT))
        datetime_from_str = user_time_zone.localize(datetime.strptime(str(datetime_from), DEFAULT_SERVER_DATETIME_FORMAT))
        datetime_to_str = str(datetime_to_str)
        datetime_from_str = str(datetime_from_str)
        # Тайлангын сонгосон төрлөөс хамааруулан өгөгдлүүдийг тооцно
        if self.report_type == 'sales order' or self.report_type == 'loan': # Тайлангийн төрөл нь Борлуулалтын захиалга эсвэл Зээлээр төрөлтэй тайлангийн тооцоолол
            """Үйлчилгээ төрөлтэй бараа үед нэхэмжлэлээс буцаалтыг тооцно"""
            if self.see_pos_order == 'sale_order':        
                select = ", sol.price_unit as price_unit, so.picking_policy as type, sol.product_uom_qty as qty, sol.price_subtotal / (CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal as without_tax_discount, " \
                            "CASE WHEN (sol.price_subtotal <> 0) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal) * sol.price_tax/sol.price_subtotal) ELSE 0 END as tax_discount, 0 AS sub_total_price , " \
                            "CASE WHEN (sol.price_subtotal <> 0) THEN (sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)*sol.price_tax/sol.price_subtotal) ELSE 0 END AS tax, " \
                            "sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) AS price, " \
                            "(((CASE WHEN (sol.price_subtotal <> 0) THEN (CASE WHEN (sol.product_uom_qty <> 0) THEN ((sol.price_subtotal/(1-sol.discount/100)*sol.price_tax/sol.price_subtotal)/sol.product_uom_qty *  ail.quantity) ELSE 0 END) ELSE 0 END) - " \
                            "(CASE WHEN (sol.price_subtotal <> 0) THEN (CASE WHEN (sol.product_uom_qty <> 0) THEN (((sol.price_subtotal/(1-sol.discount/100) - sol.price_subtotal) * sol.price_tax/sol.price_subtotal)/sol.product_uom_qty * ail.quantity) ELSE 0 END) ELSE 0 END))) / ail.quantity * table1.qty AS ref_tax, " \
                        "table1.price_unit as ref_price, table1.qty as ref_qty " 
                select_two += ", SUM(sales_order_report.sub_total_price) as sub_total_price"
                _from = " sale_order_line sol " \
                        "INNER JOIN sale_order_line_invoice_rel solir on solir.order_line_id = sol.id " \
                        "INNER JOIN account_invoice_line ail on ail.id = solir.invoice_line_id " \
                        "LEFT JOIN account_invoice ai on ai.id = ail.invoice_id " \
                        "LEFT JOIN " \
                        "(SELECT ref_ai.refund_invoice_id as refund_invoice_id, ref_ail.price_unit as price_unit,  ref_ail.quantity as qty, ref_ail.product_id as product_id, ref_ail.price_subtotal as ref_price_subtotal " \
                        "FROM account_invoice ref_ai " \
                        "LEFT JOIN account_invoice_line ref_ail on ref_ail.invoice_id = ref_ai.id " \
                        "WHERE ref_ai.state in ('open', 'paid'))  table1 on table1.refund_invoice_id = ai.id and ai.refund_invoice_id is null and sol.product_id = table1.product_id " 
                where = " AND sol.state in ('done','sale')  AND pt.type='service'  AND so.date_order BETWEEN '" + datetime_from_str + "' AND '" + datetime_to_str +"' "
                if self.report_type == 'loan':
                    where += " AND sc.is_loan = 't'" 
                balanced_query1 = self._get_query(select, _from, where, order_by, select_two, group_by, is_pos)
                self.env.cr.execute(balanced_query1)
                data1 = self.env.cr.dictfetchall()
                """Хадгалах төрөлтэй бараа үед агуулахын хөдөлгөөнөөс буцаалтыг тооцно"""
                select = ", sol.price_unit as price_unit, sol.product_uom_qty as qty, sol.price_subtotal / (CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal as without_tax_discount, " \
                            "CASE WHEN (sol.price_subtotal <> 0) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal) * sol.price_tax/sol.price_subtotal) ELSE 0 END as tax_discount, 0 AS sub_total_price , " \
                            "CASE WHEN (sol.price_subtotal <> 0) THEN (sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)*sol.price_tax/sol.price_subtotal) ELSE 0 END AS tax, " \
                            "sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) AS price, " \
                            "(CASE WHEN (sol.price_subtotal <> 0) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)*sol.price_tax/sol.price_subtotal)/sol.product_uom_qty *table1.qty) ELSE 0 END)- (CASE WHEN (sol.price_subtotal <> 0) THEN (((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal) * sol.price_tax/sol.price_subtotal)/sol.product_uom_qty *table1.qty) ELSE 0 END) AS ref_tax, " \
                            "((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)) - (sol.price_subtotal / (CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal))/sol.product_uom_qty *table1.qty  AS ref_price,  " \
                            "table1.qty as ref_qty  " 
                _from = " sale_order_line sol "\
                        "INNER JOIN procurement_order po on po.sale_line_id = sol.id " \
                        "LEFT JOIN " \
                        "(SELECT sm1.procurement_id as procurement_id, sm1.product_uom_qty as qty, sm1.product_id as product_id " \
                        "FROM stock_move sm1 " \
                        "WHERE sm1.state = 'done' and sm1.origin_returned_move_id is not null) table1 on table1.procurement_id = po.id and sol.product_id = table1.product_id "
                where = " AND sol.state in ('done','sale')  AND pt.type <> 'service' AND so.date_order BETWEEN '" + datetime_from_str + "' AND '" + datetime_to_str +"'"
                if self.report_type == 'loan':
                    where += " AND sc.is_loan = 't'" 
                balanced_query2 = self._get_query(select, _from, where, order_by, select_two, group_by, is_pos)
                self.env.cr.execute(balanced_query2)
                data2 = self.env.cr.dictfetchall()
                data = data1 + data2
            elif self.see_pos_order == 'pos_order':
                select, select_two, _from = self.get_pos_data(select, _from, order_by, select_two, salesteam_ids)
                select += ", 0 AS sub_total_price "
                where = " AND pos.state in ('paid','done') AND pos.date_order BETWEEN '" + datetime_from_str + "' AND '" + datetime_to_str + "' "
                balanced_query = self._get_query(select, _from, where, order_by, select_two, group_by, is_pos)
                self.env.cr.execute(balanced_query)
                data = self.env.cr.dictfetchall()
            else:
                select = ", sol.price_unit as price_unit, so.picking_policy as type, sol.product_uom_qty as qty, sol.price_subtotal / (CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal as without_tax_discount, " \
                            "CASE WHEN (sol.price_subtotal <> 0) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal) * sol.price_tax/sol.price_subtotal) ELSE 0 END as tax_discount, 0 AS sub_total_price , " \
                            "CASE WHEN (sol.price_subtotal <> 0) THEN (sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)*sol.price_tax/sol.price_subtotal) ELSE 0 END AS tax, " \
                            "sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) AS price, " \
                            "(((CASE WHEN (sol.price_subtotal <> 0) THEN (CASE WHEN (sol.product_uom_qty <> 0) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END))*sol.price_tax/sol.price_subtotal)/sol.product_uom_qty *  ail.quantity) ELSE 0 END) ELSE 0 END) - " \
                            "(CASE WHEN (sol.price_subtotal <> 0) THEN (CASE WHEN (sol.product_uom_qty <> 0) THEN (((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal) * sol.price_tax/sol.price_subtotal)/sol.product_uom_qty * ail.quantity) ELSE 0 END) ELSE 0 END))) / ail.quantity * table1.qty AS ref_tax, " \
                        "table1.price_unit as ref_price, table1.qty as ref_qty " 
                select_two += ", SUM(sales_order_report.sub_total_price) as sub_total_price"
                _from = " sale_order_line sol " \
                        "INNER JOIN sale_order_line_invoice_rel solir on solir.order_line_id = sol.id " \
                        "INNER JOIN account_invoice_line ail on ail.id = solir.invoice_line_id " \
                        "LEFT JOIN account_invoice ai on ai.id = ail.invoice_id " \
                        "LEFT JOIN " \
                        "(SELECT ref_ai.refund_invoice_id as refund_invoice_id, ref_ail.price_unit as price_unit,  ref_ail.quantity as qty, ref_ail.product_id as product_id, ref_ail.price_subtotal as ref_price_subtotal " \
                        "FROM account_invoice ref_ai " \
                        "LEFT JOIN account_invoice_line ref_ail on ref_ail.invoice_id = ref_ai.id " \
                        "WHERE ref_ai.state in ('open', 'paid'))  table1 on table1.refund_invoice_id = ai.id and ai.refund_invoice_id is null and sol.product_id = table1.product_id " 
                where = " AND sol.state in ('done','sale')  AND pt.type='service'  AND so.date_order BETWEEN '" + datetime_from_str + "' AND '" + datetime_to_str +"' "
                if self.report_type == 'loan':
                    where += " AND sc.is_loan = 't'" 
                balanced_query1 = self._get_query(select, _from, where, order_by, select_two, group_by, is_pos)
                self.env.cr.execute(balanced_query1)
                data1 = self.env.cr.dictfetchall()
                """Хадгалах төрөлтэй бараа үед агуулахын хөдөлгөөнөөс буцаалтыг тооцно"""
                select = ", sol.price_unit as price_unit, sol.product_uom_qty as qty, sol.price_subtotal / (CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal as without_tax_discount, " \
                            "CASE WHEN (sol.price_subtotal <> 0) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal) * sol.price_tax/sol.price_subtotal) ELSE 0 END as tax_discount, 0 AS sub_total_price , " \
                            "CASE WHEN (sol.price_subtotal <> 0) THEN (sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)*sol.price_tax/sol.price_subtotal) ELSE 0 END AS tax, " \
                            "sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) AS price, " \
                            "(CASE WHEN (sol.price_subtotal <> 0) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)*sol.price_tax/sol.price_subtotal)/sol.product_uom_qty *table1.qty) ELSE 0 END)- (CASE WHEN (sol.price_subtotal <> 0) THEN (((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal) * sol.price_tax/sol.price_subtotal)/sol.product_uom_qty *table1.qty) ELSE 0 END) AS ref_tax, " \
                            "((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)) - (sol.price_subtotal / (CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal))/sol.product_uom_qty *table1.qty  AS ref_price,  " \
                            "table1.qty as ref_qty  " 
                _from = " sale_order_line sol "\
                        "INNER JOIN procurement_order po on po.sale_line_id = sol.id " \
                        "LEFT JOIN " \
                        "(SELECT sm1.procurement_id as procurement_id, sm1.product_uom_qty as qty, sm1.product_id as product_id " \
                        "FROM stock_move sm1 " \
                        "WHERE sm1.state = 'done' and sm1.origin_returned_move_id is not null) table1 on table1.procurement_id = po.id and sol.product_id = table1.product_id "
                where = " AND sol.state in ('done','sale')  AND pt.type <> 'service' AND so.date_order BETWEEN '" + datetime_from_str + "' AND '" + datetime_to_str +"'"
                if self.report_type == 'loan':
                    where += " AND sc.is_loan = 't'" 
                balanced_query2 = self._get_query(select, _from, where, order_by, select_two, group_by, is_pos)
                is_pos = True
                select, select_two, _from = self.get_pos_data(select, _from, order_by, select_two, salesteam_ids)
                select += ", 0 AS sub_total_price "
                where = " AND pos.state in ('paid','done') AND pos.date_order BETWEEN '" + datetime_from_str + "' AND '" + datetime_to_str + "' "
                balanced_query3 = self._get_query(select, _from, where, order_by, select_two, group_by, is_pos)
                balanced_query = self.get_pos_and_sale_data(balanced_query2, balanced_query3, select_two_sale_pos, group_by_sale_pos, order_by_sale_pos)
                self.env.cr.execute(balanced_query)
                data2 = self.env.cr.dictfetchall()
                data = data1 + data2
        elif self.report_type == 'invoice': # Тайлангийн төрөл нь Нэхэмжлэлээр төрөлтэй тайлангийн тооцоолол
            if self.see_pos_order == 'sale_order':
                select = ",ail.price_unit as price_unit,  ail.quantity as qty, 0 AS sub_total_price , " \
                        "CASE WHEN (ail.quantity <> 0) THEN ((ail.price_subtotal /(CASE WHEN (1-ail.discount/100) <> 0 THEN (1-ail.discount/100) ELSE 1 END) - ail.price_subtotal)/ail.quantity * ail.quantity) ELSE 0 END AS without_tax_discount, " \
                        "CASE WHEN (sol.price_subtotal <> 0) THEN (CASE WHEN (sol.product_uom_qty <> 0) THEN (((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal) * sol.price_tax/sol.price_subtotal)/sol.product_uom_qty * ail.quantity) ELSE 0 END) ELSE 0 END AS tax_discount, " \
                        "CASE WHEN (ail.quantity <> 0) THEN ((ail.price_subtotal/(CASE WHEN (1-ail.discount/100) <> 0 THEN (1-ail.discount/100) ELSE 1 END))/ail.quantity * ail.quantity) ELSE 0 END AS price, " \
                        "CASE WHEN (sol.price_subtotal <> 0) THEN (CASE WHEN (sol.product_uom_qty <> 0) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)*sol.price_tax/sol.price_subtotal)/sol.product_uom_qty * ail.quantity) ELSE 0 END) ELSE 0 END AS tax, " \
                        "table1.ref_price_subtotal AS ref_price, table1.qty as ref_qty, " \
                        "(((CASE WHEN (sol.price_subtotal <> 0) THEN (CASE WHEN (sol.product_uom_qty <> 0) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)*sol.price_tax/sol.price_subtotal)/sol.product_uom_qty *  ail.quantity) ELSE 0 END) ELSE 0 END) - " \
                        "(CASE WHEN (sol.price_subtotal <> 0) THEN (CASE WHEN (sol.product_uom_qty <> 0) THEN (((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal) * sol.price_tax/sol.price_subtotal)/sol.product_uom_qty * ail.quantity) ELSE 0 END) ELSE 0 END))) / ail.quantity * table1.qty AS ref_tax "
                select_two += ", SUM(sales_order_report.sub_total_price) as sub_total_price"
                _from = " account_invoice_line ail " \
                        "INNER JOIN sale_order_line_invoice_rel solir on solir.invoice_line_id = ail.id " \
                        "INNER JOIN sale_order_line sol on sol.id = solir.order_line_id " \
                        "LEFT JOIN account_invoice ai on ai.id = ail.invoice_id " \
                        "LEFT JOIN " \
                        "(SELECT ref_ai.refund_invoice_id as refund_invoice_id, ref_ail.price_subtotal/(1-ref_ail.discount/100) as ref_price, ref_ail.price_subtotal as ref_price_subtotal,"\
                        " ref_ail.discount as ref_discount,ref_ail.quantity as qty, ref_ail.product_id as product_id " \
                        "FROM account_invoice ref_ai " \
                        "LEFT JOIN account_invoice_line ref_ail on ref_ail.invoice_id = ref_ai.id " \
                        "WHERE ref_ai.state in ('open', 'paid'))  table1 on table1.refund_invoice_id = ai.id and ai.refund_invoice_id is null and sol.product_id = table1.product_id " 
                where = " AND ai.state in ('open','paid') AND ai.date_invoice BETWEEN '" + str(datetime_from) + "' AND '" + str(datetime_to) +"'"
                balanced_query = self._get_query(select, _from, where, order_by, select_two, group_by, is_pos)
                self.env.cr.execute(balanced_query)
                data = self.env.cr.dictfetchall()
            elif self.see_pos_order == 'pos_order':
                select, select_two, _from = self.get_pos_data(select, _from, order_by, select_two, salesteam_ids)
                select += ", 0 AS sub_total_price  "
                _from += "LEFT JOIN account_move_line aml on pos.account_move = aml.move_id " 
                where = " AND pos.state in ('paid','done') AND  aml.date BETWEEN '" + str(datetime_from) + "' AND '" + str(datetime_to) + "' "
                balanced_query = self._get_query(select, _from, where, order_by, select_two, group_by, is_pos)
                self.env.cr.execute(balanced_query)
                data = self.env.cr.dictfetchall()
            else:
                select = ",ail.price_unit as price_unit,  ail.quantity as qty, 0 AS sub_total_price , " \
                        "CASE WHEN (ail.quantity <> 0) THEN ((ail.price_subtotal / (CASE WHEN (1-ail.discount/100) <> 0 THEN (1-ail.discount/100) ELSE 1 END) - ail.price_subtotal)/ail.quantity * ail.quantity) ELSE 0 END AS without_tax_discount, " \
                        "CASE WHEN (sol.price_subtotal <> 0) THEN (CASE WHEN (sol.product_uom_qty <> 0) THEN (((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal) * sol.price_tax/sol.price_subtotal)/sol.product_uom_qty * ail.quantity) ELSE 0 END) ELSE 0 END AS tax_discount, " \
                        "CASE WHEN (ail.quantity <> 0) THEN ((ail.price_subtotal/(CASE WHEN (1-ail.discount/100) <> 0 THEN (1-ail.discount/100) ELSE 1 END))/ail.quantity * ail.quantity) ELSE 0 END AS price, " \
                        "CASE WHEN (sol.price_subtotal <> 0) THEN (CASE WHEN (sol.product_uom_qty <> 0) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)*sol.price_tax/sol.price_subtotal)/sol.product_uom_qty * ail.quantity) ELSE 0 END) ELSE 0 END AS tax, " \
                        "table1.ref_price_subtotal AS ref_price, table1.qty as ref_qty, " \
                        "(((CASE WHEN (sol.price_subtotal <> 0) THEN (CASE WHEN (sol.product_uom_qty <> 0) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)*sol.price_tax/sol.price_subtotal)/sol.product_uom_qty *  ail.quantity) ELSE 0 END) ELSE 0 END) - " \
                        "(CASE WHEN (sol.price_subtotal <> 0) THEN (CASE WHEN (sol.product_uom_qty <> 0) THEN (((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal) * sol.price_tax/sol.price_subtotal)/sol.product_uom_qty * ail.quantity) ELSE 0 END) ELSE 0 END))) / ail.quantity * table1.qty AS ref_tax "
                select_two += ", SUM(sales_order_report.sub_total_price) as sub_total_price"
                _from = " account_invoice_line ail " \
                        "INNER JOIN sale_order_line_invoice_rel solir on solir.invoice_line_id = ail.id " \
                        "INNER JOIN sale_order_line sol on sol.id = solir.order_line_id " \
                        "LEFT JOIN account_invoice ai on ai.id = ail.invoice_id " \
                        "LEFT JOIN " \
                        "(SELECT ref_ai.refund_invoice_id as refund_invoice_id, ref_ail.price_subtotal/(1-ref_ail.discount/100) as ref_price, ref_ail.price_subtotal as ref_price_subtotal,"\
                        " ref_ail.discount as ref_discount,ref_ail.quantity as qty, ref_ail.product_id as product_id " \
                        "FROM account_invoice ref_ai " \
                        "LEFT JOIN account_invoice_line ref_ail on ref_ail.invoice_id = ref_ai.id " \
                        "WHERE ref_ai.state in ('open', 'paid'))  table1 on table1.refund_invoice_id = ai.id and ai.refund_invoice_id is null and sol.product_id = table1.product_id " 
                where = " AND ai.state in ('open','paid') AND ai.date_invoice BETWEEN '" + str(datetime_from) + "' AND '" + str(datetime_to) +"'"
                balanced_query1 = self._get_query(select, _from, where, order_by, select_two, group_by, is_pos)

                is_pos = True
                select, select_two, _from = self.get_pos_data(select, _from, order_by, select_two, salesteam_ids)
                select += ", 0 AS sub_total_price "
                _from += "LEFT JOIN account_move_line aml on pos.account_move = aml.move_id " 
                where = " AND pos.state in ('paid','done') AND pos.date_order BETWEEN '" + datetime_from_str + "' AND '" + datetime_to_str + "' "
                balanced_query2 = self._get_query(select, _from, where, order_by, select_two, group_by, is_pos)
                balanced_query = self.get_pos_and_sale_data(balanced_query1,balanced_query2, select_two_sale_pos, group_by_sale_pos, order_by_sale_pos)
                self.env.cr.execute(balanced_query)
                data = self.env.cr.dictfetchall()
        elif self.report_type == 'shipment': # Тайлангийн төрөл нь Тээвэрлэлтээр төрөлтэй тайлангийн тооцоолол
            if self.see_pos_order == 'sale_order':
                select += ", sol.price_unit as price_unit, sm.product_uom_qty as qty," \
                        "CASE WHEN ((sol.product_uom_qty <> 0 or table1.qty <> 0) and sm.origin_returned_move_id is null and sol.product_uom_qty > 0) THEN ((sol.price_subtotal / (CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)- sol.price_subtotal)/sol.product_uom_qty*sm.product_uom_qty) ELSE 0 END AS without_tax_discount, " \
                        "CASE WHEN ((sol.product_uom_qty <> 0 or table1.qty <> 0) and sm.origin_returned_move_id is null and sol.product_uom_qty > 0) THEN (CASE WHEN (sol.product_uom_qty <> 0 and sol.price_subtotal <> 0) THEN (((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal) * sol.price_tax/sol.price_subtotal)/sol.product_uom_qty*sm.product_uom_qty) ELSE 0 END) ELSE 0 END AS tax_discount, " \
                        "CASE WHEN (sol.product_uom_qty <> 0 and sm.origin_returned_move_id is null) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END))/sol.product_uom_qty * sm.product_uom_qty) ELSE 0 END AS price, " \
                        "CASE WHEN (sol.price_subtotal <> 0) THEN (CASE WHEN (sol.price_subtotal <> 0) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)*sol.price_tax/sol.price_subtotal)/sol.product_uom_qty * sm.product_uom_qty) ELSE 0 END) ELSE 0 END AS tax, " \
                        "(CASE WHEN (sol.price_subtotal <> 0) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)*sol.price_tax/sol.price_subtotal)/sol.product_uom_qty *table1.qty) ELSE 0 END)- (CASE WHEN (sol.price_subtotal <> 0) THEN (((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal) * sol.price_tax/sol.price_subtotal)/sol.product_uom_qty *table1.qty) ELSE 0 END) AS ref_tax, " \
                        "CASE WHEN (sol.product_uom_qty <>0) THEN (((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)) - (sol.price_subtotal / (CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal))/sol.product_uom_qty *table1.qty) ELSE 0 END AS ref_price, " \
                        "table1.price_unit as ref_price_unit, table1.qty as ref_qty " 
                if self.see_profit:
                    select += ", CASE WHEN (table1.qty > 0) THEN ((sm.price_unit * sm.product_uom_qty) - (table1.price_unit * table1.qty)) ELSE sm.price_unit * sm.product_uom_qty END AS sub_total_price  "
                    select_two += ", SUM(sales_order_report.sub_total_price) as sub_total_price"
                _from = " stock_move sm LEFT JOIN procurement_order po on po.id = sm.procurement_id LEFT JOIN sale_order_line sol on sol.id = po.sale_line_id " \
                        "LEFT JOIN " \
                        "(SELECT sm1.origin_returned_move_id as origin_returned_move_id, sm1.product_uom_qty as qty, sm1.price_unit as price_unit, sm1.product_id as product_id " \
                        "FROM stock_move sm1 " \
                        "WHERE sm1.state = 'done')  table1 on table1.origin_returned_move_id = sm.id and sm.origin_returned_move_id is null and sm.product_id = table1.product_id " 
                where = " AND sm.origin_returned_move_id is null AND sm.state = 'done' AND sm.date BETWEEN '" + datetime_from_str + "' AND '" + datetime_to_str +"'"
                balanced_query = self._get_query(select, _from, where, order_by, select_two, group_by, is_pos)
                self.env.cr.execute(balanced_query)
                data = self.env.cr.dictfetchall()
            elif self.see_pos_order == 'pos_order':
                select, select_two, _from = self.get_pos_data(select, _from, order_by, select_two, salesteam_ids)
                _from += "LEFT JOIN stock_move sm on sm.order_line_id = posl.id " 
                select += ", sm.price_unit * posl.qty AS sub_total_price " 
                if self.see_profit:
                    select_two += ", SUM(sales_order_report.sub_total_price) as sub_total_price"
                where = " AND  sm.state = 'done' AND pos.state in ('paid','done') AND sm.date BETWEEN '" + datetime_from_str + "' AND '" + datetime_to_str + "' "
                balanced_query = self._get_query(select, _from, where, order_by, select_two, group_by, is_pos)
                self.env.cr.execute(balanced_query)
                data = self.env.cr.dictfetchall()
            else:                
                select += ", sol.price_unit as price_unit, sm.product_uom_qty as qty," \
                        "CASE WHEN ((sol.product_uom_qty <> 0 or table1.qty <> 0) and sm.origin_returned_move_id is null and sol.product_uom_qty > 0) THEN ((sol.price_subtotal / (CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)- sol.price_subtotal)/sol.product_uom_qty*sm.product_uom_qty) ELSE 0 END AS without_tax_discount, " \
                        "CASE WHEN ((sol.product_uom_qty <> 0 or table1.qty <> 0) and sm.origin_returned_move_id is null and sol.product_uom_qty > 0) THEN (CASE WHEN (sol.product_uom_qty <> 0 AND sol.price_subtotal <>0) THEN (((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal) * sol.price_tax/sol.price_subtotal)/sol.product_uom_qty*sm.product_uom_qty) ELSE 0 END) ELSE 0 END AS tax_discount, " \
                        "CASE WHEN (sol.product_uom_qty <> 0 and sm.origin_returned_move_id is null) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END))/sol.product_uom_qty * sm.product_uom_qty) ELSE 0 END AS price, " \
                        "CASE WHEN (sol.price_subtotal <> 0) THEN (CASE WHEN (sol.price_subtotal <> 0) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)*sol.price_tax/sol.price_subtotal)/sol.product_uom_qty * sm.product_uom_qty) ELSE 0 END) ELSE 0 END AS tax, " \
                        "(CASE WHEN (sol.price_subtotal <> 0) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)*sol.price_tax/sol.price_subtotal)/sol.product_uom_qty *table1.qty) ELSE 0 END)- (CASE WHEN (sol.price_subtotal <> 0) THEN (((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal) * sol.price_tax/sol.price_subtotal)/sol.product_uom_qty *table1.qty) ELSE 0 END) AS ref_tax, " \
                        "((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)) - (sol.price_subtotal / (CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal))/sol.product_uom_qty *table1.qty  AS ref_price, " \
                        "table1.price_unit as ref_price_unit, table1.qty as ref_qty " 
                select += ", CASE WHEN (table1.qty > 0) THEN ((sm.price_unit * sm.product_uom_qty) - (table1.price_unit * table1.qty)) ELSE sm.price_unit * sm.product_uom_qty END AS sub_total_price  "
                select_two += ", SUM(sales_order_report.sub_total_price) as sub_total_price"
                _from = " stock_move sm LEFT JOIN procurement_order po on po.id = sm.procurement_id LEFT JOIN sale_order_line sol on sol.id = po.sale_line_id " \
                        "LEFT JOIN " \
                        "(SELECT sm1.origin_returned_move_id as origin_returned_move_id, sm1.product_uom_qty as qty, sm1.price_unit as price_unit, sm1.product_id as product_id " \
                        "FROM stock_move sm1 " \
                        "WHERE sm1.state = 'done')  table1 on table1.origin_returned_move_id = sm.id and sm.origin_returned_move_id is null and sm.product_id = table1.product_id " 
                where = " AND sm.origin_returned_move_id is null AND sm.state = 'done' AND sm.date BETWEEN '" + datetime_from_str + "' AND '" + datetime_to_str +"'"
                balanced_query1 = self._get_query(select, _from, where, order_by, select_two, group_by, is_pos)
                is_pos = True
                select, select_two, _from = self.get_pos_data(select, _from, order_by, select_two, salesteam_ids)
                _from += "LEFT JOIN stock_move sm on sm.order_line_id = posl.id " 
                select += ", sm.price_unit * posl.qty AS sub_total_price  " 
                where = " AND sm.state = 'done' AND pos.state = 'done' AND sm.date BETWEEN '" + datetime_from_str + "' AND '" + datetime_to_str + "' "
                balanced_query2 = self._get_query(select, _from, where, order_by, select_two, group_by, is_pos)
                balanced_query = self.get_pos_and_sale_data(balanced_query1,balanced_query2, select_two_sale_pos, group_by_sale_pos, order_by_sale_pos)
                self.env.cr.execute(balanced_query)
                data = self.env.cr.dictfetchall()
                
        else: #Тайлангийн төрөл дууссанг сонгоход нэхэмжлэл төлөгдсөн болон агуулахын хөдөлгөөн хийгдсэн төлөвтэй борлуулалтын захиалгуудыг харуулна.
            if self.see_pos_order == 'sale_order':
                """Үйлчилгээ төрөлтэй бараа үед нэхэмжлэлээс буцаалтыг тооцно"""
                select = ",ail.price_unit as price_unit,  ail.quantity as qty, 0 AS sub_total_price , " \
                        "CASE WHEN (ail.quantity <> 0) THEN ((ail.price_subtotal / (CASE WHEN (1-ail.discount/100) <> 0 THEN (1-ail.discount/100) ELSE 1 END) - ail.price_subtotal)/ail.quantity * ail.quantity) ELSE 0 END AS without_tax_discount, " \
                        "CASE WHEN (sol.price_subtotal <> 0) THEN (CASE WHEN (sol.product_uom_qty <> 0 and sol.price_subtotal <> 0) THEN (((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal) * sol.price_tax/sol.price_subtotal)/sol.product_uom_qty * ail.quantity) ELSE 0 END) ELSE 0 END AS tax_discount, " \
                        "CASE WHEN (ail.quantity <> 0) THEN ((ail.price_subtotal/(CASE WHEN (1-ail.discount/100) <> 0 THEN (1-ail.discount/100) ELSE 1 END))/ail.quantity * ail.quantity) ELSE 0 END AS price, " \
                        "CASE WHEN (sol.price_subtotal <> 0) THEN (CASE WHEN (sol.product_uom_qty <> 0) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)*sol.price_tax/sol.price_subtotal)/sol.product_uom_qty * ail.quantity) ELSE 0 END) ELSE 0 END AS tax, " \
                        "table1.ref_price_subtotal AS ref_price, table1.qty as ref_qty, " \
                        "(((CASE WHEN (sol.price_subtotal <> 0) THEN (CASE WHEN (sol.product_uom_qty <> 0) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)*sol.price_tax/sol.price_subtotal)/sol.product_uom_qty *  ail.quantity) ELSE 0 END) ELSE 0 END) - " \
                        "(CASE WHEN (sol.price_subtotal <> 0) THEN (CASE WHEN (sol.product_uom_qty <> 0) THEN (((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal) * sol.price_tax/sol.price_subtotal)/sol.product_uom_qty * ail.quantity) ELSE 0 END) ELSE 0 END))) / ail.quantity * table1.qty AS ref_tax " 
                if self.see_profit:
                    select_two += ", SUM(sales_order_report.sub_total_price) as sub_total_price" 
                _from = " sale_order_line sol " \
                        "INNER JOIN sale_order_line_invoice_rel solir on solir.order_line_id = sol.id " \
                        "INNER JOIN account_invoice_line ail on ail.id = solir.invoice_line_id " \
                        "LEFT JOIN account_invoice ai on ai.id = ail.invoice_id " \
                        "LEFT JOIN " \
                        "(SELECT ref_ai.refund_invoice_id as refund_invoice_id, ref_ail.price_subtotal/(1-ref_ail.discount/100) as ref_price, ref_ail.price_subtotal as ref_price_subtotal,"\
                        " ref_ail.discount as ref_discount,ref_ail.quantity as qty, ref_ail.product_id as product_id " \
                        "FROM account_invoice ref_ai " \
                        "LEFT JOIN account_invoice_line ref_ail on ref_ail.invoice_id = ref_ai.id " \
                        "WHERE ref_ai.state in ('paid'))  table1 on table1.refund_invoice_id = ai.id and ai.refund_invoice_id is null and sol.product_id = table1.product_id " 
                where = " AND ai.state = 'paid' AND pt.type='service' AND so.date_order BETWEEN '" + datetime_from_str + "' AND '" + datetime_to_str +"' "
                balanced_query1 = self._get_query(select, _from, where, order_by, select_two, group_by, is_pos)
                self.env.cr.execute(balanced_query1)
                data1 = self.env.cr.dictfetchall()
                """Хадгалах төрөлтэй бараа үед агуулахын хөдөлгөөнөөс буцаалтыг тооцно"""
                select = ", sol.price_unit as price_unit, sm.product_uom_qty as qty," \
                        "CASE WHEN (sol.product_uom_qty <> 0 or table1.qty <> 0) THEN ((sol.price_subtotal / (CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal)/sol.product_uom_qty*sm.product_uom_qty) ELSE 0 END AS without_tax_discount, " \
                        "CASE WHEN (sol.price_subtotal <> 0 or table1.qty <> 0) THEN (CASE WHEN (sol.product_uom_qty <> 0 AND sol.price_subtotal <>0) THEN (((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal) * sol.price_tax/sol.price_subtotal)/sol.product_uom_qty*sm.product_uom_qty) ELSE 0 END) ELSE 0 END AS tax_discount, " \
                        "CASE WHEN (sol.product_uom_qty <> 0) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END))/sol.product_uom_qty * sm.product_uom_qty) ELSE 0 END AS price, " \
                        "CASE WHEN (sol.price_subtotal <> 0 and sol.product_uom_qty <> 0) THEN (CASE WHEN (sol.product_uom_qty <> 0) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)*sol.price_tax/sol.price_subtotal)/sol.product_uom_qty * sm.product_uom_qty) ELSE 0 END) ELSE 0 END AS tax, " \
                        "(CASE WHEN (sol.price_subtotal <> 0 and sol.product_uom_qty <>0 ) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)*sol.price_tax/sol.price_subtotal)/sol.product_uom_qty *table1.qty) ELSE 0 END)- (CASE WHEN (sol.price_subtotal <> 0 and sol.product_uom_qty <>0 ) THEN (((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal) * sol.price_tax/sol.price_subtotal)/sol.product_uom_qty *table1.qty) ELSE 0 END) AS ref_tax, " \
                        "CASE WHEN (sol.product_uom_qty <>0) THEN (((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)) - (sol.price_subtotal / (CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal))/sol.product_uom_qty *table1.qty) ELSE 0 END AS ref_price, " \
                        "table1.price_unit as ref_price_unit, table1.qty as ref_qty, CASE WHEN (table1.qty > 0) THEN ((sm.price_unit * sm.product_uom_qty) - (table1.price_unit * table1.qty)) ELSE sm.price_unit * sm.product_uom_qty END AS sub_total_price "  
                if self.see_profit:
                    select_two += ", SUM(sales_order_report.sub_total_price) as sub_total_price"
                _from = " stock_move sm LEFT JOIN procurement_order po on po.id = sm.procurement_id LEFT JOIN sale_order_line sol on sol.id = po.sale_line_id " \
                        "LEFT JOIN " \
                        "(SELECT sm1.origin_returned_move_id as origin_returned_move_id, sm1.product_uom_qty as qty, sm1.price_unit as price_unit, sm1.product_id as product_id " \
                        "FROM stock_move sm1 " \
                        "WHERE sm1.state = 'done')  table1 on table1.origin_returned_move_id = sm.id and sm.origin_returned_move_id is null and sm.product_id = table1.product_id " 
                where = " AND sm.origin_returned_move_id is null AND sm.state = 'done' AND sm.date BETWEEN '" + datetime_from_str + "' AND '" + datetime_to_str +"'"
                balanced_query2 = self._get_query(select, _from, where, order_by, select_two, group_by, is_pos)
                self.env.cr.execute(balanced_query2)
                data2 = self.env.cr.dictfetchall()
                data = data1 + data2
            elif self.see_pos_order == 'pos_order':
                select, select_two, _from = self.get_pos_data(select, _from, order_by, select_two, salesteam_ids)
                _from += "LEFT JOIN stock_move sm on sm.picking_id = pos.picking_id " 
                select += ",sm.price_unit * sm.product_uom_qty AS sub_total_price " 
                if self.see_profit:
                    select_two += ", SUM(sales_order_report.sub_total_price) as sub_total_price"
                where = " AND sm.origin_returned_move_id is null AND pos.state = 'done' AND posl.product_id = sm.product_id AND pos.date_order BETWEEN '" + datetime_from_str + "' AND '" + datetime_to_str + "' "
                
                balanced_query = self._get_query(select, _from, where, order_by, select_two, group_by, is_pos)
                self.env.cr.execute(balanced_query)
                data = self.env.cr.dictfetchall()
            else:                
                """Үйлчилгээ төрөлтэй бараа үед нэхэмжлэлээс буцаалтыг тооцно"""
                select = ",ail.price_unit as price_unit,  ail.quantity as qty, 0 AS sub_total_price , " \
                        "CASE WHEN (ail.quantity <> 0) THEN ((ail.price_subtotal / (CASE WHEN (1-ail.discount/100) <> 0 THEN (1-ail.discount/100) ELSE 1 END) - ail.price_subtotal)/ail.quantity * ail.quantity) ELSE 0 END AS without_tax_discount, " \
                        "CASE WHEN (sol.price_subtotal <> 0) THEN (CASE WHEN (sol.product_uom_qty <> 0 AND sol.price_subtotal <>0) THEN (((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal) * sol.price_tax/sol.price_subtotal)/sol.product_uom_qty * ail.quantity) ELSE 0 END) ELSE 0 END AS tax_discount, " \
                        "CASE WHEN (ail.quantity <> 0) THEN ((ail.price_subtotal/(CASE WHEN (1-ail.discount/100) <> 0 THEN (1-ail.discount/100) ELSE 1 END))/ail.quantity * ail.quantity) ELSE 0 END AS price, " \
                        "CASE WHEN (sol.price_subtotal <> 0) THEN (CASE WHEN (sol.product_uom_qty <> 0) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)*sol.price_tax/sol.price_subtotal)/sol.product_uom_qty * ail.quantity) ELSE 0 END) ELSE 0 END AS tax, " \
                        "table1.ref_price_subtotal AS ref_price, table1.qty as ref_qty, " \
                        "(((CASE WHEN (sol.price_subtotal <> 0) THEN (CASE WHEN (sol.product_uom_qty <> 0) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)*sol.price_tax/sol.price_subtotal)/sol.product_uom_qty *  ail.quantity) ELSE 0 END) ELSE 0 END) - " \
                        "(CASE WHEN (sol.price_subtotal <> 0) THEN (CASE WHEN (sol.product_uom_qty <> 0) THEN (((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal) * sol.price_tax/sol.price_subtotal)/sol.product_uom_qty * ail.quantity) ELSE 0 END) ELSE 0 END))) / ail.quantity * table1.qty AS ref_tax "
                if self.see_profit:
                    select_two += ", SUM(sales_order_report.sub_total_price) as sub_total_price" 
                _from = " sale_order_line sol " \
                        "INNER JOIN sale_order_line_invoice_rel solir on solir.order_line_id = sol.id " \
                        "INNER JOIN account_invoice_line ail on ail.id = solir.invoice_line_id " \
                        "LEFT JOIN account_invoice ai on ai.id = ail.invoice_id " \
                        "LEFT JOIN " \
                        "(SELECT ref_ai.refund_invoice_id as refund_invoice_id, ref_ail.price_subtotal/(1-ref_ail.discount/100) as ref_price, ref_ail.price_subtotal as ref_price_subtotal,"\
                        " ref_ail.discount as ref_discount,ref_ail.quantity as qty, ref_ail.product_id as product_id " \
                        "FROM account_invoice ref_ai " \
                        "LEFT JOIN account_invoice_line ref_ail on ref_ail.invoice_id = ref_ai.id " \
                        "WHERE ref_ai.state in ('paid'))  table1 on table1.refund_invoice_id = ai.id and ai.refund_invoice_id is null and sol.product_id = table1.product_id " 
                where = " AND ai.state = 'paid' AND pt.type='service' AND so.date_order BETWEEN '" + datetime_from_str + "' AND '" + datetime_to_str +"' "
                
                balanced_query1 = self._get_query(select, _from, where, order_by, select_two, group_by, is_pos)
                self.env.cr.execute(balanced_query1)
                data1 = self.env.cr.dictfetchall()
                """Хадгалах төрөлтэй бараа үед агуулахын хөдөлгөөнөөс буцаалтыг тооцно"""
                select = ", sol.price_unit as price_unit, sm.product_uom_qty as qty," \
                        "CASE WHEN (sol.product_uom_qty <> 0 or table1.qty <> 0) THEN ((sol.price_subtotal / (CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal)/sol.product_uom_qty*sm.product_uom_qty) ELSE 0 END AS without_tax_discount, " \
                        "CASE WHEN (sol.price_subtotal <> 0 or table1.qty <> 0) THEN (CASE WHEN (sol.product_uom_qty <> 0 AND sol.price_subtotal <>0) THEN (((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal) * sol.price_tax/sol.price_subtotal)/sol.product_uom_qty*sm.product_uom_qty) ELSE 0 END) ELSE 0 END AS tax_discount, " \
                        "CASE WHEN (sol.product_uom_qty <> 0) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END))/sol.product_uom_qty * sm.product_uom_qty) ELSE 0 END AS price, " \
                        "CASE WHEN (sol.price_subtotal <> 0) THEN (CASE WHEN (sol.price_subtotal <> 0) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)*sol.price_tax/sol.price_subtotal)/sol.product_uom_qty * sm.product_uom_qty) ELSE 0 END) ELSE 0 END AS tax, " \
                        "(CASE WHEN (sol.price_subtotal <> 0) THEN ((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)*sol.price_tax/sol.price_subtotal)/sol.product_uom_qty *table1.qty) ELSE 0 END)- (CASE WHEN (sol.price_subtotal <> 0) THEN (((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal) * sol.price_tax/sol.price_subtotal)/sol.product_uom_qty *table1.qty) ELSE 0 END) AS ref_tax, " \
                        "((sol.price_subtotal/(CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END)) - (sol.price_subtotal / (CASE WHEN (1-sol.discount/100) <> 0 THEN (1-sol.discount/100) ELSE 1 END) - sol.price_subtotal))/sol.product_uom_qty *table1.qty  AS ref_price,  " \
                        "table1.price_unit as ref_price_unit, table1.qty as ref_qty, CASE WHEN (table1.qty > 0) THEN ((sm.price_unit * sm.product_uom_qty) - (table1.price_unit * table1.qty)) ELSE sm.price_unit * sm.product_uom_qty END AS sub_total_price "
                _from = " stock_move sm LEFT JOIN procurement_order po on po.id = sm.procurement_id LEFT JOIN sale_order_line sol on sol.id = po.sale_line_id " \
                        "LEFT JOIN " \
                        "(SELECT sm1.origin_returned_move_id as origin_returned_move_id, sm1.product_uom_qty as qty, sm1.price_unit as price_unit, sm1.product_id as product_id " \
                        "FROM stock_move sm1 " \
                        "WHERE sm1.state = 'done')  table1 on table1.origin_returned_move_id = sm.id and sm.origin_returned_move_id is null and sm.product_id = table1.product_id " 
                where = " AND sm.origin_returned_move_id is null  AND sm.state = 'done' AND sm.date BETWEEN '" + datetime_from_str + "' AND '" + datetime_to_str +"'"
                balanced_query2 = self._get_query(select, _from, where, order_by, select_two, group_by, is_pos)
                is_pos = True
                select, select_two, _from = self.get_pos_data(select, _from, order_by, select_two, salesteam_ids)
                _from += "LEFT JOIN stock_move sm on sm.picking_id = pos.picking_id " 
                select += ",sm.price_unit * sm.product_uom_qty AS sub_total_price " 
                where = " AND sm.origin_returned_move_id is null AND pos.state = 'done' AND posl.product_id = sm.product_id AND pos.date_order BETWEEN '" + datetime_from_str + "' AND '" + datetime_to_str + "' "
                
                balanced_query3 = self._get_query(select, _from, where, order_by, select_two, group_by, is_pos)
                balanced_query = self.get_pos_and_sale_data(balanced_query2,balanced_query3, select_two_sale_pos, group_by_sale_pos, order_by_sale_pos)
                self.env.cr.execute(balanced_query)
                data2 = self.env.cr.dictfetchall()
                data = data1 + data2
        return data