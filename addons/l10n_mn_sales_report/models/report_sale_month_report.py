# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
import pytz
from datetime import datetime
from odoo import api, models, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT


class SalesMonthReport(models.Model):
    _inherit = 'sale.month.report'

    @api.model
    def get_lines(self):
        date_from = pytz.timezone(self.env.user.tz).localize(datetime.strptime(self.date_from, DEFAULT_SERVER_DATE_FORMAT).replace(second=0, hour=0, minute=0)).astimezone(pytz.UTC).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        select = ''
        group_by = ''
        order_by = ''
        date_to = pytz.timezone(self.env.user.tz).localize(datetime.strptime(self.date_to, DEFAULT_SERVER_DATE_FORMAT).replace(second=59, hour=23, minute=59)).astimezone(pytz.UTC).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        if self.group_by_1:
            if self.group_by_1 == 'warehouse':
                select += ', so.warehouse_id'
                group_by += ', so.warehouse_id'
                order_by += 'so.warehouse_id, '
            elif self.group_by_1 == 'location':
                select += ', spt.default_location_src_id'
                group_by += ', spt.default_location_src_id'
                order_by += 'spt.default_location_src_id, '
            elif self.group_by_1 == 'product_categ':
                select += ', pt.categ_id'
                group_by += ', pt.categ_id'
                order_by += 'pt.categ_id, '
            elif self.group_by_1 == 'brand_name':
                select += ', pt.brand_name'
                group_by += ', pt.brand_name'
                order_by += 'pt.brand_name, '
            elif self.group_by_1 == 'salesman':
                select += ', so.user_id'
                group_by += ', so.user_id'
                order_by += 'so.user_id, '
            elif self.group_by_1 == 'salesteam':
                select += ', so.team_id'
                group_by += ', so.team_id'
                order_by += 'so.team_id, '
            elif self.group_by_1 == 'customer':
                select += ', so.partner_id'
                group_by += ', so.partner_id'
                order_by += 'so.partner_id, '
            elif self.group_by_1 == 'supplier':
                select += ', pt.supplier_id'
                group_by += ', pt.supplier_id'
                order_by += 'pt.supplier_id, '
            if self.group_by_2:
                if self.group_by_2 == 'warehouse':
                    select += ', so.warehouse_id'
                    group_by += ', so.warehouse_id'
                    order_by += 'so.warehouse_id, '
                elif self.group_by_2 == 'location':
                    select += ', spt.default_location_src_id'
                    group_by += ', spt.default_location_src_id'
                    order_by += 'spt.default_location_src_id, '
                elif self.group_by_2 == 'product_categ':
                    select += ', pt.categ_id'
                    group_by += ', pt.categ_id'
                    order_by += 'pt.categ_id, '
                elif self.group_by_2 == 'brand_name':
                    select += ', pt.brand_name'
                    group_by += ', pt.brand_name'
                    order_by += 'pt.brand_name, '
                elif self.group_by_2 == 'salesman':
                    select += ', so.user_id'
                    group_by += ', so.user_id'
                    order_by += 'so.user_id, '
                elif self.group_by_2 == 'salesteam':
                    select += ', so.team_id'
                    group_by += ', so.team_id'
                    order_by += 'so.team_id, '
                elif self.group_by_2 == 'customer':
                    select += ', so.partner_id'
                    group_by += ', so.partner_id'
                    order_by += 'so.partner_id, '
                elif self.group_by_2 == 'supplier':
                    select += ', pt.supplier_id'
                    group_by += ', pt.supplier_id'
                    order_by += 'pt.supplier_id, '
        if self.type == 'sale_order':
            query = '''
                select
                    array_agg(sol.id) as sol_ids,
                    pp.default_code,
                    pt.name as product_name,
                    pu.name as uom_name,
                    sum(sol.product_uom_qty) as qty,
                    sum(sol.price_unit * sol.product_uom_qty) as total,
                    sum(
                        (sol.price_unit * sol.product_uom_qty) * sol.discount / 100
                    ) as discount %s
                from
                    sale_order_line sol
                    left join sale_order so on so.id = sol.order_id
                    left join product_product pp on pp.id = sol.product_id
                    left join product_template pt on pt.id = pp.product_tmpl_id
                    left join product_uom pu on pu.id = sol.product_uom
                    left join stock_picking_type spt on so.stock_picking_type = spt.id
                where
                    so.state in ('sale', 'done') and
                    so.date_order between \'''' + date_from + '\' AND \'' + date_to + '''\'
                group by
                    pu.name,
                    pt.name,
                    pp.default_code,
                    sol.product_id %s
                order by
                    %s
                    pp.default_code
            '''
        elif self.type == 'invoice':
            query = '''
                select
                    array_agg(sol.id) as sol_ids,
                    pp.default_code,
                    pt.name as product_name,
                    pu.name as uom_name,
                    sum(sol.product_uom_qty) as qty,
                    sum(sol.price_unit * sol.product_uom_qty) as total,
                    sum(
                        (sol.price_unit * sol.product_uom_qty) * sol.discount / 100
                    ) as discount %s
                from
                    sale_order_line sol
                    left join sale_order_line_invoice_rel solir on solir.order_line_id = sol.id
                    left join account_invoice_line ail on ail.id = solir.invoice_line_id
                    left join account_invoice ai on ai.id = ail.invoice_id
                    left join sale_order so on so.id = sol.order_id
                    left join product_product pp on pp.id = sol.product_id
                    left join product_template pt on pt.id = pp.product_tmpl_id
                    left join product_uom pu on pu.id = sol.product_uom
                    left join stock_picking_type spt on so.stock_picking_type = spt.id
                where
                    ai.state in ('open', 'paid') and
                    so.date_order between \'''' + date_from + '\' AND \'' + date_to + '''\'
                group by
                    pu.name,
                    pt.name,
                    pp.default_code,
                    sol.product_id %s
                order by
                    %s
                    pp.default_code
            '''
        elif self.type == 'picking':
            query = '''
                select
                    array_agg(sol.id) as sol_ids,
                    pp.default_code,
                    pt.name as product_name,
                    pu.name as uom_name,
                    sum(sol.product_uom_qty) as qty,
                    sum(sol.price_unit * sol.product_uom_qty) as total,
                    sum(
                        (sol.price_unit * sol.product_uom_qty) * sol.discount / 100
                    ) as discount %s
                from
                    sale_order_line sol
                    left join sale_order so on so.id = sol.order_id
                    left join stock_picking sp on (so.procurement_group_id = sp.group_id and sp.picking_type_id = so.stock_picking_type)
                    left join product_product pp on pp.id = sol.product_id
                    left join product_template pt on pt.id = pp.product_tmpl_id
                    left join product_uom pu on pu.id = sol.product_uom
                    left join stock_picking_type spt on so.stock_picking_type = spt.id
                where
                    sp.state in ('done') and
                    so.date_order between \'''' + date_from + '\' AND \'' + date_to + '''\'
                group by
                    pu.name,
                    pt.name,
                    pp.default_code,
                    sol.product_id %s
                order by
                    %s
                    pp.default_code
            '''
        elif self.type == 'finished':
            query = '''
                select
                    array_agg(sol.id) as sol_ids,
                    pp.default_code,
                    pt.name as product_name,
                    pu.name as uom_name,
                    sum(sol.product_uom_qty) as qty,
                    sum(sol.price_unit * sol.product_uom_qty) as total,
                    sum(
                        (sol.price_unit * sol.product_uom_qty) * sol.discount / 100
                    ) as discount %s
                from
                    sale_order_line sol
                    left join sale_order_line_invoice_rel solir on solir.order_line_id = sol.id
                    left join account_invoice_line ail on ail.id = solir.invoice_line_id
                    left join account_invoice ai on ai.id = ail.invoice_id
                    left join sale_order so on so.id = sol.order_id
                    left join product_product pp on pp.id = sol.product_id
                    left join product_template pt on pt.id = pp.product_tmpl_id
                    left join product_uom pu on pu.id = sol.product_uom
                    left join stock_picking_type spt on so.stock_picking_type = spt.id
                where
                    ai.state in ('paid') and
                    so.date_order between \'''' + date_from + '\' AND \'' + date_to + '''\'
                group by
                    pu.name,
                    pt.name,
                    pp.default_code,
                    sol.product_id %s
                order by
                    %s
                    pp.default_code
            '''
        self._cr.execute(query % (select, group_by, order_by))
        return self.env.cr.dictfetchall()

    # Борлуулалтын буцаалт
    @api.multi
    def _get_refund(self, sol_ids):
        SaleOrderLine = self.env['sale.order.line']
        refund = 0
        sale_order_lines = SaleOrderLine.browse(sol_ids)
        for line in sale_order_lines:
            for picking in line.order_id.picking_ids:
                for move in picking.move_lines:
                    if move.move_type == 'sale_return' and move.product_id == line.product_id:
                        refund += (move.product_uom_qty * line.price_unit)
        return refund

    @api.multi
    def draw_group_by(self, sheet, line, rowx, format_content_center_border, group_by_1, group_by_2, group_by_1_dict, group_by_2_dict, refund, pure, group_by_1_rowx, group_by_2_rowx):
        SaleOrderLine = self.env['sale.order.line']
        sale_order_line = SaleOrderLine.browse(line['sol_ids'])[0]
        old_group_by_1 = group_by_1
        old_group_by_2 = group_by_2
        if self.group_by_1:
            if self.group_by_1 == 'warehouse':
                group_by_1 = sale_order_line.order_id.warehouse_id.name
            elif self.group_by_1 == 'location':
                group_by_1 = sale_order_line.order_id.stock_picking_type.default_location_src_id.name
            elif self.group_by_1 == 'product_categ':
                group_by_1 = sale_order_line.product_id.categ_id.name
            elif self.group_by_1 == 'brand_name':
                group_by_1 = sale_order_line.product_id.brand_name.brand_name
            elif self.group_by_1 == 'salesman':
                group_by_1 = sale_order_line.order_id.user_id.name
            elif self.group_by_1 == 'salesteam':
                group_by_1 = sale_order_line.order_id.team_id.name
            elif self.group_by_1 == 'customer':
                group_by_1 = sale_order_line.order_id.partner_id.name
            elif self.group_by_1 == 'supplier':
                group_by_1 = sale_order_line.product_id.supplier_id.name
            if old_group_by_1 != group_by_1:
                sheet.merge_range(rowx, 0, rowx, 3, group_by_1, format_content_center_border)
                group_by_1_rowx = rowx
                rowx += 1
                group_by_1_dict = self._reset_dict()
                group_by_1_dict['total']['qty'] = group_by_1_dict['total']['qty'] + line.get('qty')
                group_by_1_dict['total']['total'] = group_by_1_dict['total']['total'] + line.get('total')
                group_by_1_dict['total']['discount'] = group_by_1_dict['total']['discount'] + line.get('discount')
                group_by_1_dict['total']['refund'] = group_by_1_dict['total']['refund'] + refund
                group_by_1_dict['total']['pure'] = group_by_1_dict['total']['pure'] + pure
                sheet.write(group_by_1_rowx, 4, group_by_1_dict['total']['qty'], format_content_center_border)
                sheet.write(group_by_1_rowx, 5, group_by_1_dict['total']['total'], format_content_center_border)
                sheet.write(group_by_1_rowx, 6, group_by_1_dict['total']['discount'], format_content_center_border)
                sheet.write(group_by_1_rowx, 7, group_by_1_dict['total']['refund'], format_content_center_border)
                sheet.write(group_by_1_rowx, 8, group_by_1_dict['total']['pure'], format_content_center_border)
                group_by_1_dict = self._draw_group_by_month(sheet, line, group_by_1_rowx, group_by_1_dict, format_content_center_border)
        if self.group_by_2:
            if self.group_by_2 == 'warehouse':
                group_by_2 = sale_order_line.order_id.warehouse_id.name
            elif self.group_by_2 == 'location':
                group_by_2 = sale_order_line.order_id.stock_picking_type.default_location_src_id.name
            elif self.group_by_2 == 'product_categ':
                group_by_2 = sale_order_line.product_id.categ_id.name
            elif self.group_by_2 == 'brand_name':
                group_by_2 = sale_order_line.product_id.brand_name.brand_name
            elif self.group_by_2 == 'salesman':
                group_by_2 = sale_order_line.order_id.user_id.name
            elif self.group_by_2 == 'salesteam':
                group_by_2 = sale_order_line.order_id.team_id.name
            elif self.group_by_2 == 'customer':
                group_by_2 = sale_order_line.order_id.partner_id.name
            elif self.group_by_2 == 'supplier':
                group_by_2 = sale_order_line.product_id.supplier_id.name
        if self.group_by_1:
            if old_group_by_1 != group_by_1:
                if self.group_by_2:
                    sheet.merge_range(rowx, 1, rowx, 3, group_by_2, format_content_center_border)
                    group_by_2_rowx = rowx
                    rowx += 1
                    group_by_2_dict = self._reset_dict()
                    group_by_2_dict['total']['qty'] = group_by_2_dict['total']['qty'] + line.get('qty')
                    group_by_2_dict['total']['total'] = group_by_2_dict['total']['total'] + line.get('total')
                    group_by_2_dict['total']['discount'] = group_by_2_dict['total']['discount'] + line.get('discount')
                    group_by_2_dict['total']['refund'] = group_by_2_dict['total']['refund'] + refund
                    group_by_2_dict['total']['pure'] = group_by_2_dict['total']['pure'] + pure
                    sheet.write(group_by_2_rowx, 4, group_by_2_dict['total']['qty'], format_content_center_border)
                    sheet.write(group_by_2_rowx, 5, group_by_2_dict['total']['total'], format_content_center_border)
                    sheet.write(group_by_2_rowx, 6, group_by_2_dict['total']['discount'], format_content_center_border)
                    sheet.write(group_by_2_rowx, 7, group_by_2_dict['total']['refund'], format_content_center_border)
                    sheet.write(group_by_2_rowx, 8, group_by_2_dict['total']['pure'], format_content_center_border)
                    group_by_2_dict = self._draw_group_by_month(sheet, line, group_by_2_rowx, group_by_2_dict, format_content_center_border)
            else:
                group_by_1_dict['total']['qty'] = group_by_1_dict['total']['qty'] + line.get('qty')
                group_by_1_dict['total']['total'] = group_by_1_dict['total']['total'] + line.get('total')
                group_by_1_dict['total']['discount'] = group_by_1_dict['total']['discount'] + line.get('discount')
                group_by_1_dict['total']['refund'] = group_by_1_dict['total']['refund'] + refund
                group_by_1_dict['total']['pure'] = group_by_1_dict['total']['pure'] + pure
                sheet.write(group_by_1_rowx, 4, group_by_1_dict['total']['qty'], format_content_center_border)
                sheet.write(group_by_1_rowx, 5, group_by_1_dict['total']['total'], format_content_center_border)
                sheet.write(group_by_1_rowx, 6, group_by_1_dict['total']['discount'], format_content_center_border)
                sheet.write(group_by_1_rowx, 7, group_by_1_dict['total']['refund'], format_content_center_border)
                sheet.write(group_by_1_rowx, 8, group_by_1_dict['total']['pure'], format_content_center_border)
                group_by_1_dict = self._draw_group_by_month(sheet, line, group_by_1_rowx, group_by_1_dict, format_content_center_border)
                if self.group_by_2:
                    if old_group_by_2 != group_by_2:
                        sheet.merge_range(rowx, 1, rowx, 3, group_by_2, format_content_center_border)
                        group_by_2_rowx = rowx
                        rowx += 1
                        group_by_2_dict = self._reset_dict()
                        group_by_2_dict['total']['qty'] = group_by_2_dict['total']['qty'] + line.get('qty')
                        group_by_2_dict['total']['total'] = group_by_2_dict['total']['total'] + line.get('total')
                        group_by_2_dict['total']['discount'] = group_by_2_dict['total']['discount'] + line.get('discount')
                        group_by_2_dict['total']['refund'] = group_by_2_dict['total']['refund'] + refund
                        group_by_2_dict['total']['pure'] = group_by_2_dict['total']['pure'] + pure
                        sheet.write(group_by_2_rowx, 4, group_by_2_dict['total']['qty'], format_content_center_border)
                        sheet.write(group_by_2_rowx, 5, group_by_2_dict['total']['total'], format_content_center_border)
                        sheet.write(group_by_2_rowx, 6, group_by_2_dict['total']['discount'], format_content_center_border)
                        sheet.write(group_by_2_rowx, 7, group_by_2_dict['total']['refund'], format_content_center_border)
                        sheet.write(group_by_2_rowx, 8, group_by_2_dict['total']['pure'], format_content_center_border)
                        group_by_2_dict = self._draw_group_by_month(sheet, line, group_by_2_rowx, group_by_2_dict, format_content_center_border)
                    else:
                        group_by_2_dict['total']['qty'] = group_by_2_dict['total']['qty'] + line.get('qty')
                        group_by_2_dict['total']['total'] = group_by_2_dict['total']['total'] + line.get('total')
                        group_by_2_dict['total']['discount'] = group_by_2_dict['total']['discount'] + line.get('discount')
                        group_by_2_dict['total']['refund'] = group_by_2_dict['total']['refund'] + refund
                        group_by_2_dict['total']['pure'] = group_by_2_dict['total']['pure'] + pure
                        sheet.write(group_by_2_rowx, 4, group_by_2_dict['total']['qty'], format_content_center_border)
                        sheet.write(group_by_2_rowx, 5, group_by_2_dict['total']['total'], format_content_center_border)
                        sheet.write(group_by_2_rowx, 6, group_by_2_dict['total']['discount'], format_content_center_border)
                        sheet.write(group_by_2_rowx, 7, group_by_2_dict['total']['refund'], format_content_center_border)
                        sheet.write(group_by_2_rowx, 8, group_by_2_dict['total']['pure'], format_content_center_border)
                        group_by_2_dict = self._draw_group_by_month(sheet, line, group_by_2_rowx, group_by_2_dict, format_content_center_border)
        return rowx, group_by_1, group_by_2, group_by_1_dict, group_by_2_dict, group_by_1_rowx, group_by_2_rowx

    @api.model
    def _reset_dict(self):
        return {
            'total': {
                'qty': 0,
                'total': 0,
                'discount': 0,
                'refund': 0,
                'pure': 0,
            },
            '1': {
                'qty': 0,
                'total': 0,
                'discount': 0,
                'refund': 0,
                'pure': 0,
            },
            '2': {
                'qty': 0,
                'total': 0,
                'discount': 0,
                'refund': 0,
                'pure': 0,
            },
            '3': {
                'qty': 0,
                'total': 0,
                'discount': 0,
                'refund': 0,
                'pure': 0,
            },
            '4': {
                'qty': 0,
                'total': 0,
                'discount': 0,
                'refund': 0,
                'pure': 0,
            },
            '5': {
                'qty': 0,
                'total': 0,
                'discount': 0,
                'refund': 0,
                'pure': 0,
            },
            '6': {
                'qty': 0,
                'total': 0,
                'discount': 0,
                'refund': 0,
                'pure': 0,
            },
            '7': {
                'qty': 0,
                'total': 0,
                'discount': 0,
                'refund': 0,
                'pure': 0,
            },
            '8': {
                'qty': 0,
                'total': 0,
                'discount': 0,
                'refund': 0,
                'pure': 0,
            },
            '9': {
                'qty': 0,
                'total': 0,
                'discount': 0,
                'refund': 0,
                'pure': 0,
            },
            '10': {
                'qty': 0,
                'total': 0,
                'discount': 0,
                'refund': 0,
                'pure': 0,
            },
            '11': {
                'qty': 0,
                'total': 0,
                'discount': 0,
                'refund': 0,
                'pure': 0,
            },
            '12': {
                'qty': 0,
                'total': 0,
                'discount': 0,
                'refund': 0,
                'pure': 0,
            }
        }

    @api.model
    def _draw_group_by_month(self, sheet, line, rowx, group_by_1_dict, format_content_center_border):
        SaleOrderLine = self.env['sale.order.line']
        colx = 8
        for month in range(1, 13):
            sale_order_lines = SaleOrderLine.browse(line['sol_ids']).filtered(lambda l: pytz.timezone('UTC').localize(datetime.strptime(l.order_id.date_order, DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(pytz.timezone(self.env.user.tz)).month == month)
            if sale_order_lines:
                self._cr.execute('''
                    select
                        array_agg(sol.id) as sol_ids,
                        pp.default_code,
                        pt.name as product_name,
                        pu.name as uom_name,
                        coalesce(sum(sol.product_uom_qty), 0) as qty,
                        coalesce(sum(sol.price_unit * sol.product_uom_qty), 0) as total,
                        coalesce(sum(
                            (sol.price_unit * sol.product_uom_qty) * sol.discount / 100
                        ), 0) as discount
                    from
                        sale_order_line sol
                        left join sale_order so on so.id = sol.order_id
                        left join product_product pp on pp.id = sol.product_id
                        left join product_template pt on pt.id = pp.product_tmpl_id
                        left join product_uom pu on pu.id = sol.product_uom
                        left join stock_picking_type spt on so.stock_picking_type = spt.id
                    where
                        sol.id in (''' + str(sale_order_lines.ids)[1:-1] + ''')
                    group by
                        pp.default_code,
                        pt.name,
                        pu.name;
                ''')
                result = self._cr.dictfetchall()[0]
                refund = self._get_refund(sale_order_lines.ids)
                pure = result['total'] - result['discount'] - refund
                group_by_1_dict[str(month)]['qty'] = group_by_1_dict[str(month)]['qty'] + result['qty']
                group_by_1_dict[str(month)]['total'] = group_by_1_dict[str(month)]['total'] + result['total']
                group_by_1_dict[str(month)]['discount'] = group_by_1_dict[str(month)]['discount'] + result['discount']
                group_by_1_dict[str(month)]['refund'] = group_by_1_dict[str(month)]['refund'] + refund
                group_by_1_dict[str(month)]['pure'] = group_by_1_dict[str(month)]['pure'] + pure
            sheet.write(rowx, colx + 1, group_by_1_dict[str(month)]['qty'], format_content_center_border)
            sheet.write(rowx, colx + 2, group_by_1_dict[str(month)]['total'], format_content_center_border)
            sheet.write(rowx, colx + 3, group_by_1_dict[str(month)]['discount'], format_content_center_border)
            sheet.write(rowx, colx + 4, group_by_1_dict[str(month)]['refund'], format_content_center_border)
            sheet.write(rowx, colx + 5, group_by_1_dict[str(month)]['pure'], format_content_center_border)
            colx += 5
        return group_by_1_dict

    @api.model
    def _draw_total(self, total_dict, sheet, format_title_float_border, rowx):
        sheet.merge_range(rowx, 0, rowx, 3, _('TOTAL'), format_title_float_border)
        sheet.write(rowx, 4, total_dict['total']['qty'], format_title_float_border)
        sheet.write(rowx, 5, total_dict['total']['total'], format_title_float_border)
        sheet.write(rowx, 6, total_dict['total']['discount'], format_title_float_border)
        sheet.write(rowx, 7, total_dict['total']['refund'], format_title_float_border)
        sheet.write(rowx, 8, total_dict['total']['pure'], format_title_float_border)
        colx = 8
        for month in range(1, 13):
            sheet.write(rowx, colx + 1, total_dict[str(month)]['qty'], format_title_float_border)
            sheet.write(rowx, colx + 2, total_dict[str(month)]['total'], format_title_float_border)
            sheet.write(rowx, colx + 3, total_dict[str(month)]['discount'], format_title_float_border)
            sheet.write(rowx, colx + 4, total_dict[str(month)]['refund'], format_title_float_border)
            sheet.write(rowx, colx + 5, total_dict[str(month)]['pure'], format_title_float_border)
            colx += 5

    @api.model
    def draw_month(self, line, sheet, rowx, format_content_center_border, total_dict):
        SaleOrderLine = self.env['sale.order.line']
        colx = 8
        for month in range(1, 13):
            sale_order_lines = SaleOrderLine.browse(line['sol_ids']).filtered(lambda l: pytz.timezone('UTC').localize(datetime.strptime(l.order_id.date_order, DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(pytz.timezone(self.env.user.tz)).month == month)
            if sale_order_lines:
                self._cr.execute('''
                    select
                        array_agg(sol.id) as sol_ids,
                        pp.default_code,
                        pt.name as product_name,
                        pu.name as uom_name,
                        coalesce(sum(sol.product_uom_qty), 0) as qty,
                        coalesce(sum(sol.price_unit * sol.product_uom_qty), 0) as total,
                        coalesce(sum(
                            (sol.price_unit * sol.product_uom_qty) * sol.discount / 100
                        ), 0) as discount
                    from
                        sale_order_line sol
                        left join sale_order so on so.id = sol.order_id
                        left join product_product pp on pp.id = sol.product_id
                        left join product_template pt on pt.id = pp.product_tmpl_id
                        left join product_uom pu on pu.id = sol.product_uom
                        left join stock_picking_type spt on so.stock_picking_type = spt.id
                    where
                        sol.id in (''' + str(sale_order_lines.ids)[1:-1] + ''')
                    group by
                        pp.default_code,
                        pt.name,
                        pu.name;
                ''')
                result = self._cr.dictfetchall()[0]
                refund = self._get_refund(sale_order_lines.ids)
                pure = result['total'] - result['discount'] - refund
                sheet.write(rowx, colx + 1, result['qty'], format_content_center_border)
                sheet.write(rowx, colx + 2, result['total'], format_content_center_border)
                sheet.write(rowx, colx + 3, result['discount'], format_content_center_border)
                sheet.write(rowx, colx + 4, refund, format_content_center_border)
                sheet.write(rowx, colx + 5, pure, format_content_center_border)
                total_dict[str(month)]['qty'] = total_dict[str(month)]['qty'] + result['qty']
                total_dict[str(month)]['total'] = total_dict[str(month)]['total'] + result['total']
                total_dict[str(month)]['discount'] = total_dict[str(month)]['discount'] + result['discount']
                total_dict[str(month)]['refund'] = total_dict[str(month)]['refund'] + refund
                total_dict[str(month)]['pure'] = total_dict[str(month)]['pure'] + pure
            else:
                sheet.write(rowx, colx + 1, 0, format_content_center_border)
                sheet.write(rowx, colx + 2, 0, format_content_center_border)
                sheet.write(rowx, colx + 3, 0, format_content_center_border)
                sheet.write(rowx, colx + 4, 0, format_content_center_border)
                sheet.write(rowx, colx + 5, 0, format_content_center_border)

            colx += 5
        return total_dict

    @api.multi
    def export_report(self):

        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Sale Month Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # Тайлангийн формат хэсэг
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_content_center_border = book.add_format(ReportExcelCellStyles.format_content_center_border)
        format_title_small = book.add_format(ReportExcelCellStyles.format_title_small)
        format_title_float_border = book.add_format(ReportExcelCellStyles.format_title_float_border)
        format_group_left_border = book.add_format(ReportExcelCellStyles.format_group_left_border)
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title_color = book.add_format(ReportExcelCellStyles.format_title_color)

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('sales month_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 1
        sequence = 1
        sheet.hide_gridlines(2)
        colx_number = 14
        sheet.set_column('A:A', 4)
        sheet.set_column('B:B', 10)
        sheet.set_column('C:C', 10)
        sheet.set_column('D:D', 13)
        sheet.set_column('E:E', 10)
        sheet.set_column('F:F', 15)
        sheet.set_column('G:H', 12)
        sheet.set_column('I:I', 10)
        sheet.set_column('J:K', 12)
        sheet.set_column('L:L', 10)
        sheet.set_column('M:P', 12)

        # create name
        sheet.merge_range(rowx, 0, rowx, 3, '%s: %s' % (_('Company Name'), self.company_id.name), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 0, rowx + 1, colx_number, report_name.upper(), format_name)
        rowx += 2

        # create duration
        sheet.merge_range(rowx, 0, rowx, 3, '%s: %s - %s' % (_('Report Duration'), self.date_from, self.date_to), format_filter)
        rowx += 1

        # create date
        sheet.merge_range(rowx, 0, rowx, 3, '%s: %s' % (_('Created On'), time.strftime('%Y-%m-%d')), format_filter)
        rowx += 2

        # Table title
        sheet.merge_range(rowx, 0, rowx + 1, 0, _('№'), format_title_small)
        sheet.merge_range(rowx, 1, rowx + 1, 1, _('Default code'), format_title_small)
        col = 1
        sheet.merge_range(rowx, 2, rowx + 1, 2, _('Product name'), format_title_small)
        col += 1
        sheet.merge_range(rowx, col + 1, rowx + 1, col + 1, _('Product UOM'), format_title_small)
        sheet.merge_range(rowx, col + 2, rowx, col + 6, _('Amount total'), format_title_small)
        sheet.merge_range(rowx, col + 7, rowx, col + 11, _('1 month'), format_title_small)
        sheet.merge_range(rowx, col + 12, rowx, col + 16, _('2 month'), format_title_small)
        sheet.merge_range(rowx, col + 17, rowx, col + 21, _('3 month'), format_title_small)
        sheet.merge_range(rowx, col + 22, rowx, col + 26, _('4 month'), format_title_small)
        sheet.merge_range(rowx, col + 27, rowx, col + 31, _('5 month'), format_title_small)
        sheet.merge_range(rowx, col + 32, rowx, col + 36, _('6 month'), format_title_small)
        sheet.merge_range(rowx, col + 37, rowx, col + 41, _('7 month'), format_title_small)
        sheet.merge_range(rowx, col + 42, rowx, col + 46, _('8 month'), format_title_small)
        sheet.merge_range(rowx, col + 47, rowx, col + 51, _('9 month'), format_title_small)
        sheet.merge_range(rowx, col + 52, rowx, col + 56, _('10 month'), format_title_small)
        sheet.merge_range(rowx, col + 57, rowx, col + 61, _('11 month'), format_title_small)
        sheet.merge_range(rowx, col + 62, rowx, col + 66, _('12 month'), format_title_small)
        sheet.write(rowx + 1, col + 2, _('Quantity'), format_title_small)
        sheet.write(rowx + 1, col + 3, _('Sales Total'), format_title_small)
        sheet.write(rowx + 1, col + 4, _('Discount'), format_title_small)
        sheet.write(rowx + 1, col + 5, _('Return'), format_title_small)
        sheet.write(rowx + 1, col + 6, _('Pure'), format_title_small)
        sheet.write(rowx + 1, col + 7, _('Quantity'), format_title_small)
        sheet.write(rowx + 1, col + 8, _('Sales Total'), format_title_small)
        sheet.write(rowx + 1, col + 9, _('Discount'), format_title_small)
        sheet.write(rowx + 1, col + 10, _('Return'), format_title_small)
        sheet.write(rowx + 1, col + 11, _('Pure'), format_title_small)
        sheet.write(rowx + 1, col + 12, _('Quantity'), format_title_small)
        sheet.write(rowx + 1, col + 13, _('Sales Total'), format_title_small)
        sheet.write(rowx + 1, col + 14, _('Discount'), format_title_small)
        sheet.write(rowx + 1, col + 15, _('Return'), format_title_small)
        sheet.write(rowx + 1, col + 16, _('Pure'), format_title_small)
        sheet.write(rowx + 1, col + 12, _('Quantity'), format_title_small)
        sheet.write(rowx + 1, col + 13, _('Sales Total'), format_title_small)
        sheet.write(rowx + 1, col + 14, _('Discount'), format_title_small)
        sheet.write(rowx + 1, col + 15, _('Return'), format_title_small)
        sheet.write(rowx + 1, col + 16, _('Pure'), format_title_small)
        sheet.write(rowx + 1, col + 17, _('Quantity'), format_title_small)
        sheet.write(rowx + 1, col + 18, _('Sales Total'), format_title_small)
        sheet.write(rowx + 1, col + 19, _('Discount'), format_title_small)
        sheet.write(rowx + 1, col + 20, _('Return'), format_title_small)
        sheet.write(rowx + 1, col + 21, _('Pure'), format_title_small)
        sheet.write(rowx + 1, col + 22, _('Quantity'), format_title_small)
        sheet.write(rowx + 1, col + 23, _('Sales Total'), format_title_small)
        sheet.write(rowx + 1, col + 24, _('Discount'), format_title_small)
        sheet.write(rowx + 1, col + 25, _('Return'), format_title_small)
        sheet.write(rowx + 1, col + 26, _('Pure'), format_title_small)
        sheet.write(rowx + 1, col + 27, _('Quantity'), format_title_small)
        sheet.write(rowx + 1, col + 28, _('Sales Total'), format_title_small)
        sheet.write(rowx + 1, col + 29, _('Discount'), format_title_small)
        sheet.write(rowx + 1, col + 30, _('Return'), format_title_small)
        sheet.write(rowx + 1, col + 31, _('Pure'), format_title_small)
        sheet.write(rowx + 1, col + 32, _('Quantity'), format_title_small)
        sheet.write(rowx + 1, col + 33, _('Sales Total'), format_title_small)
        sheet.write(rowx + 1, col + 34, _('Discount'), format_title_small)
        sheet.write(rowx + 1, col + 35, _('Return'), format_title_small)
        sheet.write(rowx + 1, col + 36, _('Pure'), format_title_small)
        sheet.write(rowx + 1, col + 37, _('Quantity'), format_title_small)
        sheet.write(rowx + 1, col + 38, _('Sales Total'), format_title_small)
        sheet.write(rowx + 1, col + 39, _('Discount'), format_title_small)
        sheet.write(rowx + 1, col + 40, _('Return'), format_title_small)
        sheet.write(rowx + 1, col + 41, _('Pure'), format_title_small)
        sheet.write(rowx + 1, col + 42, _('Quantity'), format_title_small)
        sheet.write(rowx + 1, col + 43, _('Sales Total'), format_title_small)
        sheet.write(rowx + 1, col + 44, _('Discount'), format_title_small)
        sheet.write(rowx + 1, col + 45, _('Return'), format_title_small)
        sheet.write(rowx + 1, col + 46, _('Pure'), format_title_small)
        sheet.write(rowx + 1, col + 47, _('Quantity'), format_title_small)
        sheet.write(rowx + 1, col + 48, _('Sales Total'), format_title_small)
        sheet.write(rowx + 1, col + 49, _('Discount'), format_title_small)
        sheet.write(rowx + 1, col + 50, _('Return'), format_title_small)
        sheet.write(rowx + 1, col + 51, _('Pure'), format_title_small)
        sheet.write(rowx + 1, col + 52, _('Quantity'), format_title_small)
        sheet.write(rowx + 1, col + 53, _('Sales Total'), format_title_small)
        sheet.write(rowx + 1, col + 54, _('Discount'), format_title_small)
        sheet.write(rowx + 1, col + 55, _('Return'), format_title_small)
        sheet.write(rowx + 1, col + 56, _('Pure'), format_title_small)
        sheet.write(rowx + 1, col + 57, _('Quantity'), format_title_small)
        sheet.write(rowx + 1, col + 58, _('Sales Total'), format_title_small)
        sheet.write(rowx + 1, col + 59, _('Discount'), format_title_small)
        sheet.write(rowx + 1, col + 60, _('Return'), format_title_small)
        sheet.write(rowx + 1, col + 61, _('Pure'), format_title_small)
        sheet.write(rowx + 1, col + 62, _('Quantity'), format_title_small)
        sheet.write(rowx + 1, col + 63, _('Sales Total'), format_title_small)
        sheet.write(rowx + 1, col + 64, _('Discount'), format_title_small)
        sheet.write(rowx + 1, col + 65, _('Return'), format_title_small)
        sheet.write(rowx + 1, col + 66, _('Pure'), format_title_small)
        rowx += 2
        sequence = 1
        colx = 0
        lines = self.get_lines()
        group_by_1 = False
        group_by_2 = False
        group_by_1_rowx = 0
        group_by_2_rowx = 0
        group_by_1_dict = self._reset_dict()
        group_by_2_dict = self._reset_dict()
        total_dict = self._reset_dict()
        for line in lines:
            refund = self._get_refund(line['sol_ids'])
            pure = line.get('total') - line.get('discount') - refund
            rowx, group_by_1, group_by_2, group_by_1_dict, group_by_2_dict, group_by_1_rowx, group_by_2_rowx = self.draw_group_by(sheet, line, rowx, format_group_left_border, group_by_1, group_by_2, group_by_1_dict, group_by_2_dict, refund, pure, group_by_1_rowx, group_by_2_rowx)
            sheet.write(rowx, colx + 0, sequence, format_content_center_border)
            sheet.write(rowx, colx + 1, line.get('default_code'), format_content_center_border)
            sheet.write(rowx, colx + 2, line.get('product_name'), format_content_center_border)
            sheet.write(rowx, colx + 3, line.get('uom_name'), format_content_center_border)
            sheet.write(rowx, colx + 4, line.get('qty'), format_content_center_border)
            sheet.write(rowx, colx + 5, line.get('total'), format_content_center_border)
            sheet.write(rowx, colx + 6, line.get('discount'), format_content_center_border)
            sheet.write(rowx, colx + 7, refund, format_content_center_border)
            sheet.write(rowx, colx + 8, pure, format_content_center_border)
            total_dict['total']['qty'] = total_dict['total']['qty'] + line.get('qty')
            total_dict['total']['total'] = total_dict['total']['total'] + line.get('total')
            total_dict['total']['discount'] = total_dict['total']['discount'] + line.get('discount')
            total_dict['total']['refund'] = total_dict['total']['refund'] + refund
            total_dict['total']['pure'] = total_dict['total']['pure'] + pure
            total_dict = self.draw_month(line, sheet, rowx, format_content_center_border, total_dict)
            rowx += 1
            sequence += 1
        self._draw_total(total_dict, sheet, format_title_float_border, rowx)

        book.close()
        # Файлын өгөгдлийг тохируулах
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # Экспортын функц дуудаж байна
        return report_excel_output_obj.export_report()
