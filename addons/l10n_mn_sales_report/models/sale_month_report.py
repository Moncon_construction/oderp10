# -*- encoding: utf-8 -*-
##############################################################################
import time
from operator import itemgetter

from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError


class SaleMonthReport(models.Model):
    _name = 'sale.month.report'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = 'Sale Month Report Display'

    name = fields.Char('Name', required=True, track_visibility='onchange', states={'approved': [('readonly', True)]})
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env['res.company']._company_default_get('sale.month.report'))
    date_from = fields.Date('Start Date', required=True, default=lambda *a: time.strftime('%Y-%m-01'), track_visibility='onchange', states={'approved': [('readonly', True)]})
    date_to = fields.Date('End Date', required=True, default=lambda *a: time.strftime('%Y-%m-%d'), track_visibility='onchange', tates={'approved': [('readonly', True)]})
    warehouse_ids = fields.Many2many('stock.warehouse', string='Warehouses', states={'approved': [('readonly', True)]})
    group_by_1 = fields.Selection([('warehouse', 'By Warehouse'),
                                   ('location', 'By Location'),
                                   ('product_categ', 'By Product Category'),
                                   ('brand_name', 'Product Brand'),
                                   ('salesman', 'Salesman'),
                                   ('salesteam', 'Salesteam'),
                                   ('customer', 'Customer'),
                                   ('supplier', 'By Supplier')], string="Group", defualt=False, track_visibility='onchange', states={'approved': [('readonly', True)]})
    group_by_2 = fields.Selection([('warehouse', 'By Warehouse'),
                                   ('location', 'By Location'),
                                   ('product_categ', 'By Product Category'),
                                   ('brand_name', 'Product Brand'),
                                   ('salesman', 'Salesman'),
                                   ('salesteam', 'Salesteam'),
                                   ('customer', 'Customer'),
                                   ('supplier', 'By Supplier')], string="Group By", track_visibility='onchange', states={'approved': [('readonly', True)]})
    pos_sales = fields.Boolean('Pos sales', defualt=False)
    description = fields.Text('Description')
    state = fields.Selection([('draft', 'Draft'),
                              ('approved', 'Approved')], string='State', default='draft', required=True, track_visibility='onchange')
    type = fields.Selection([
        ('sale_order', 'Sale order'),
        ('invoice', 'Invoice'),
        ('picking', 'Picking'),
        ('finished', 'Finished')
    ], default='sale_order', required=True)

    @api.onchange('company_id')
    def onchange_domain(self):
        domain = {}
        for report in self:
            domain['warehouses'] = [('company_id', '=', report.company_id.id)]
            _warehouses = []
            for warehouse in self.env.user.allowed_warehouses:
                _warehouses.append(warehouse.id)
            if _warehouses:
                domain['warehouse_ids'] = [('id', 'in', _warehouses), ('company_id', '=', report.company_id.id)]
            domain['invoice_ids'] = [('company_id', '=', report.company_id.id)]
            domain['stock_picking_ids'] = [('product_tmpl_id.company_id', '=', report.company_id.id)]
            domain['invoice_paid_ids'] = [('company_id', '=', report.company_id.id)]
            domain['sale_order_ids'] = [('company_id', '=', report.company_id.id), ('state', 'in', ('sale', 'done'))]
            return {'domain': domain}

    @api.multi
    def unlink(self):
        for report in self:
            if report.state != 'draft':
                raise UserError(_('Delete only draft in state'))
        return super(SaleMonthReport, self).unlink()

    @api.multi
    def validate(self):
        # Батлах
        self.write({'state': 'approved'})

    @api.multi
    def action_to_draft(self):
        # Ноороглох
        self.write({'state': 'draft'})
