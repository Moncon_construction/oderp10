# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class SaleOrder(models.Model):
    _inherit = "sale.order"

    @api.one
    @api.depends('date_order')
    def _compute_month_and_day(self):
        if self.date_order:
            self.month = int(self.date_order[5:7])
            self.day = int(self.date_order[8:10])

    month = fields.Integer(compute=_compute_month_and_day, store=True)
    day = fields.Integer(compute=_compute_month_and_day, store=True)
