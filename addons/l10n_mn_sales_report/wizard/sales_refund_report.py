# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
import xlsxwriter
from odoo import api, fields, models, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from odoo.exceptions import UserError
import pytz
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openpyxl import formula
from xlrd import sheet
from odoo.exceptions import UserError
from operator import itemgetter

class SalesRetrievalReport(models.TransientModel):
    """
        Борлуулалтын буцаалтын тайлан
    """

    _name = 'sales.refund.report'
    _description = "Sales Refund Report"

    GROUP_SELECTION = [
        ('no_group', 'No Group'),
        ('by_warehouse', 'Group by Warehouse'),
        ('by_partner', 'Group by Partner'),
        ('by_category', 'Group by Product Category')
    ]
    
    company = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    group_type = fields.Selection(GROUP_SELECTION, default='no_group', required=True)
    
    date_start = fields.Date("Start Date", required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    date_end = fields.Date("End Date", required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    
    show_barcode = fields.Boolean(string='See Product Bar Code', default=False) 
    group_by_sale_order = fields.Boolean(string='Group By Sale Order', default=False) 
    
    warehouses = fields.Many2many('stock.warehouse', 'sales_refund_report_to_warehouse', 'report_id', 'warehouse_id', string="Warehouses")
    categories = fields.Many2many('product.category', 'sales_refund_report_to_category', 'report_id', 'category_id', string="Category")
    products = fields.Many2many('product.product', 'sales_refund_report_to_product_product', 'report_id', 'product_id', string="Products")
    partners = fields.Many2many('res.partner', 'sales_refund_report_to_partner', 'report_id', 'so_id', string="Partners")

    @api.onchange("company")
    def _get_warehouse_and_categ_domain(self):
        domain = {}
        domain['warehouses'] = [('company_id','=', self.company.id)]
        _warehouses = []
        for warehouse in self.env.user.allowed_warehouses:
            _warehouses.append(warehouse.id)
        if _warehouses:
            domain['warehouses'] = [('id', 'in', _warehouses), ('company_id','=', self.company.id)]
        domain['categories'] = [('company_id','=', self.company.id)]
        domain['products'] = [('product_tmpl_id.company_id','=', self.company.id)]
        return {'domain': domain}
    
    @api.onchange("categories")
    def _get_product_domain(self):
        domain = {}
        if self.categories and len(self.categories) > 0:
            domain['products'] = [('product_tmpl_id.categ_id','in', self.categories.ids)]
        return {'domain': domain}
    
    @api.onchange('group_type')
    def set_group_by_sale_order(self):
        for obj in self:
            if ((obj.group_type and obj.group_type == 'no_group') or not obj.group_type) and not obj.group_by_sale_order:
                obj.group_by_sale_order = True
    
    @api.onchange('date_start', 'date_end')
    def validate_date_range_start(self):
        if self.date_start and self.date_end:
            if self.date_start > self.date_end:
                raise UserError('Invalid date range, Please enter a date start less than or equal to date end')

    def  get_xsl_column_name(self, index):
        alphabet = {'0': 'A',  '1':'B',  '2':'C',  '3':'D',  '4':'E',
                    '5': 'F',  '6':'G',  '7':'H',  '8':'I',  '9':'J',
                    '10':'K',  '11':'L', '12':'M', '13':'N', '14':'O',
                    '15':'P',  '16':'Q', '17':'R', '18':'S', '19':'T',
                    '20':'U',  '21':'V', '22':'W', '23':'X', '24':'Y', '25':'Z'}
        
        if index <= 25:
            return (alphabet[str(index)] + ":" + alphabet[str(index)])
        else:
            return (alphabet[str(index/26-1)] + alphabet[str(index%26)] + ":" + alphabet[str(index/26-1)] + alphabet[str(index%26)])
    
    def get_column_name_for_calculate(self, index):
        column_name = self.get_xsl_column_name(index).split(':')[0]
        return column_name
    
    def get_sum_formula_from_list(self, col, list):
        formula = "="
        if list:
            for i in range(len(list)):
                formula += ("+" + self.get_column_name_for_calculate(col) + str(list[i]))
        return formula
    
    def get_arithmetic_formula(self, f_col, f_rowx, s_col, s_rowx, oprtr):
        f_cell_index = self.get_column_name_for_calculate(f_col) + str(f_rowx+1)
        s_cell_index = self.get_column_name_for_calculate(s_col) + str(s_rowx+1)
        return f_cell_index + oprtr + s_cell_index
    
    def get_sum_formula(self, startx, endx, col):
        return "{=SUM(" + self.get_column_name_for_calculate(col) + str(startx) + ":" + self.get_column_name_for_calculate(col) + str(endx) + ")}"
    
    def is_module_installed(self, module_name):
        self._cr.execute("SELECT id FROM ir_module_module WHERE name = '%s' AND state IN ('installed', 'to upgrade')" %module_name)
        results = self._cr.dictfetchall()
        
        if results and len(results) > 0:
            return True
        else:
            return False
        
    def get_all_col_count(self):
        all_col_count = 15
        all_col_count += 1 if self.show_barcode else 0
        return all_col_count

    def get_group_name(self):
        if self.group_type and self.group_type == 'by_warehouse':
            return _("Warehouse")
        elif self.group_type and self.group_type == 'by_category':
            return _("Product Category")
        elif self.group_type and self.group_type == 'by_partner':
            return _("Partner")
        return False
    
    def get_lines(self):
        
        order_by_qry = ""
        main_select = ""
        if self.group_type and self.group_type == 'by_warehouse':
            order_by_qry = ' wh.name, wh.id, '
            main_select = 'wh.id AS group_id, wh.name AS group_name, '
        elif self.group_type and self.group_type == 'by_category':
            order_by_qry = ' pro_cat.name, pro_cat.id, '
            main_select = 'pro_cat.id AS group_id, pro_cat.name AS group_name, '
        elif self.group_type and self.group_type == 'by_partner':
            order_by_qry = ' pt.name, pp.id, '
            main_select = 'partner.id AS group_id, partner.name AS group_name, '
        if self.group_by_sale_order:
            order_by_qry += ' so.name, so.id, so.confirmation_date, '    
        
        where_qry = ""
        if self.warehouses:
            warehouse_ids = self.warehouses.ids
            where_qry += ' AND wh.id in (' + ','.join(map(str, warehouse_ids)) + ') '
        if self.categories:
            category_ids = self.categories.ids
            where_qry += ' AND pro_cat.id in (' + ','.join(map(str, category_ids)) + ') '
        if self.products:
            product_ids = self.products.ids
            where_qry += ' AND pp.id in (' + ','.join(map(str, product_ids)) + ') '
        if self.partners:
            so_ids = self.partners.ids
            where_qry += ' AND partner.id in (' + ','.join(map(str, so_ids)) + ') '


        qry = """
            SELECT %s
                so.id AS so_id, so.name AS so_name, so.confirmation_date, pp.id AS pro_id, pp.barcode AS barcode, pt.default_code AS default_code, pt.name AS pt_name, uom.name AS uom_name,
                all_sale_order.unit_price_after_discount AS so_unit_price,
                all_sale_order.sum_quantity AS so_total_quantity, 
                refund_from_picking.unit_price AS refund_picking_unit_price, 
                refund_from_picking.sum_quantity AS refund_picking_total_quantity, 
                refund_from_invoice.unit_price_after_discount AS refund_invoice_unit_price, 
                refund_from_invoice.sum_quantity AS refund_invoice_total_quantity
            FROM 
            
            /* TABLE all_sale_order: Бүх борлуулалтын захиалга дахь барааны НИЙТ ТОО ХЭМЖЭЭ, НЭГЖ ҮНЭ -г олох. (БЗ > Бараа > Нэгжийн үнэ)-р бүлэглэх*/
            (
                SELECT so_id, pro_id, unit_price_after_discount, sum(quantity) AS sum_quantity
                FROM 
                (
                    SELECT sol.order_id AS so_id, sol.product_id AS pro_id, sol.product_uom_qty AS quantity, (sol.price_unit*(1-(sol.discount)/100)) AS unit_price_after_discount
                     FROM sale_order_line sol
                ) table1
                GROUP BY so_id, pro_id, unit_price_after_discount
            ) all_sale_order
            
            
            LEFT JOIN 
            /* TABLE refund_from_picking: БЗ-ын хүргэлтийн буцаалт дахь НИЙТ ТОО ХЭМЖЭЭ, НЭГЖ ҮНЭ -г олох. (БЗ > Бараа > Нэгжийн үнэ)-р бүлэглэх*/
            (
                SELECT so_id, pro_id, unit_price, abs(sum(quantity)) AS sum_quantity
                FROM 
                (
                    SELECT sol.order_id  AS so_id, sm.product_id AS pro_id, sm.price_unit AS unit_price, 
                    /*Хэрвээ буцаасан хүргэлтээс ахиад буцааж байгаа бол уг буцаалт нь анхны хүргэлтийн хувьд буцаалтынхаа хэмжээгээр хүчингүйд тооцогдох буюу буцаагаагүй гэж үзнэ*/
                    case when dest_loc.usage = 'internal' then COALESCE(-sm.product_uom_qty)
                        else sm.product_uom_qty end AS quantity
                     FROM sale_order_line sol
                    LEFT JOIN procurement_order proc_order ON proc_order.sale_line_id = sol.id
                    LEFT JOIN stock_move sm ON sm.procurement_id = proc_order.id
                    LEFT JOIN stock_location dest_loc ON  dest_loc.id = sm.location_dest_id
                    where sm.state = 'done' AND sm.origin_returned_move_id IS NOT NULL
                ) table1
                GROUP BY so_id, pro_id, unit_price
            ) refund_from_picking ON refund_from_picking.so_id = all_sale_order.so_id AND refund_from_picking.pro_id = all_sale_order.pro_id 
            
            
            LEFT JOIN
            /* TABLE refund_from_invoice: БЗ-ын нэхэмжлэлийн төлбөрийн буцаалт дахь НИЙТ ТОО ХЭМЖЭЭ, НЭГЖ ҮНЭ -г олох. (БЗ > Бараа > Нэгжийн үнэ)-р бүлэглэх*/
            (
                SELECT so_id, pro_id, unit_price_after_discount, sum(quantity) AS sum_quantity
                FROM 
                (
                    SELECT so.id AS so_id, refund_invoice_line.product_id AS pro_id, refund_invoice_line.quantity AS quantity, 
                        (refund_invoice_line.price_unit*(1-(refund_invoice_line.discount)/100)) AS unit_price_after_discount
                    FROM account_invoice refund_inv
                    LEFT JOIN account_invoice_line refund_invoice_line ON refund_invoice_line.invoice_id = refund_inv.id 
                    LEFT JOIN sale_order so ON so.name = refund_invoice_line.origin
                    where refund_inv.state NOT IN ('draft', 'cancel') AND refund_inv.type = 'out_refund' AND refund_inv.origin in 
                    (
                        SELECT out_inv.number
                         FROM account_invoice out_inv 
                        right join sale_order so ON so.name = out_inv.origin
                        where out_inv.type = 'out_invoice' AND out_inv.origin IS NOT NULL
                    ) 
                ) table1
                GROUP BY so_id, pro_id, unit_price_after_discount
            ) refund_from_invoice ON refund_from_invoice.so_id = all_sale_order.so_id AND refund_from_invoice.pro_id = all_sale_order.pro_id 
            
            LEFT JOIN sale_order so ON so.id = all_sale_order.so_id
            LEFT JOIN product_product pp ON pp.id = all_sale_order.pro_id
            LEFT JOIN product_template pt ON pt.id = pp.product_tmpl_id
            LEFT JOIN product_uom uom ON uom.id = pt.uom_id
            LEFT JOIN product_category pro_cat ON pt.categ_id = pro_cat.id
            LEFT JOIN stock_warehouse wh ON wh.id = so.warehouse_id
            LEFT JOIN res_partner partner ON partner.id = so.partner_id
            WHERE so.company_id = %s AND so.state IN ('done', 'sale') AND so.confirmation_date BETWEEN '%s' AND '%s' AND (refund_from_picking.so_id IS NOT NULL OR refund_from_invoice.so_id IS NOT NULL) %s
            ORDER BY %s pp.barcode, pt.default_code, uom.name, all_sale_order.sum_quantity
        """ %(main_select, self.company.id, str(self.date_start) + " 00:00:00", str(self.date_end) + " 23:59:59", where_qry, order_by_qry)
        self._cr.execute(qry)
        results = self._cr.dictfetchall()
        return results if results else False
    
    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        
        # create name
        report_name = _('Sales Refund Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('moving_materials_report'), form_title=file_name).create({})
        
        # create formats
        format_text_left = {
            'font_name': 'Times New Roman',
            'font_size': 9,
            'align': 'left',
            'valign': 'top',
            'text_wrap': 1
        }
        
        format_title_left = format_text_left.copy()
        format_title_left['font_size'] = '10'
        format_title_left['bold'] = True
        
        format_title_right = format_title_left.copy()
        format_title_right['align'] = 'right'
        
        format_title_center = format_title_left.copy()
        format_title_center['align'] = 'center'
        format_title_center['font_size'] = '14'
        
        format_text_left_bordered = format_text_left.copy()
        format_text_left_bordered['border'] = 1
        
        format_number_right_bordered = format_text_left_bordered.copy()
        format_number_right_bordered['num_format'] = '_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)'
        
        format_dd_center_bordered = format_number_right_bordered.copy()
        format_dd_center_bordered['align'] = 'center'
        format_dd_center_bordered['num_format'] = '#,##0_);(#,##0)'
        
        format_text_center_bordered = format_text_left_bordered.copy()
        format_text_center_bordered['align'] = 'center'
        
        format_subh_text_bordered = format_text_left_bordered.copy()
        format_subh_text_bordered['bold'] = True
        format_subh_text_bordered['bg_color'] = '#ccffff'
        
        format_subh_number_bordered = format_subh_text_bordered.copy()
        format_subh_number_bordered['align'] = 'right'
        format_subh_number_bordered['num_format'] = '_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)'
        
        format_subh_dd_text_bordered = format_subh_number_bordered.copy()
        format_subh_dd_text_bordered['num_format'] = '#,##0_);(#,##0)'
        
        format_h_text_bordered = format_text_center_bordered.copy()
        format_h_text_bordered['bold'] = True
        format_h_text_bordered['text_wrap'] = 1
        format_h_text_bordered['bg_color'] = '#99ccff'
        format_h_text_bordered['align'] = 'center'
        format_h_text_bordered['font_size'] = '10'
        
        format_h_number_bordered = format_h_text_bordered.copy()
        format_h_number_bordered['num_format'] = '_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)'
        format_h_number_bordered['align'] = 'right'
        
        # register formats
        format_text_left = book.add_format(format_text_left)
        format_title_left = book.add_format(format_title_left)
        format_title_right = book.add_format(format_title_right)
        format_title_center = book.add_format(format_title_center)
        format_text_left_bordered = book.add_format(format_text_left_bordered)
        format_number_right_bordered = book.add_format(format_number_right_bordered)
        format_dd_center_bordered = book.add_format(format_dd_center_bordered)
        format_text_center_bordered = book.add_format(format_text_center_bordered)
        format_subh_text_bordered = book.add_format(format_subh_text_bordered)
        format_subh_number_bordered = book.add_format(format_subh_number_bordered)
        format_subh_dd_text_bordered = book.add_format(format_subh_dd_text_bordered)
        format_h_text_bordered = book.add_format(format_h_text_bordered)
        format_h_text_bordered.set_align('vcenter')
        format_h_number_bordered = book.add_format(format_h_number_bordered)
        
        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 0
        
        # compute column
        col = 0
        sheet.set_column(self.get_xsl_column_name(col), 4)
        if self.show_barcode:
            col += 1
            sheet.set_column(self.get_xsl_column_name(col), 10)
        sheet.set_column(self.get_xsl_column_name(col+1), 10)
        sheet.set_column(self.get_xsl_column_name(col+2), 10)
        sheet.set_column(self.get_xsl_column_name(col+3), 30)
        sheet.set_column(self.get_xsl_column_name(col+4), 8)
        for i in range(col+5, self.get_all_col_count(), 3):
            sheet.set_column(self.get_xsl_column_name(i), 10)
            sheet.set_column(self.get_xsl_column_name(i+1), 15)
            sheet.set_column(self.get_xsl_column_name(i+2), 15)
        
        # create contents
        sheet.merge_range(rowx, 0, rowx, self.get_all_col_count(), _(u'Company') + u": %s" % unicode(self.company.name), format_title_left) 
        sheet.merge_range(rowx+2, 0, rowx+3, self.get_all_col_count(), report_name, format_title_center) 
        sheet.merge_range(rowx+5, 0, rowx+5, 3, _(u'Report Date') + u": %s ~ %s" % (unicode(self.date_start), unicode(self.date_end)), format_title_left) 
        start_col = 13 if self.show_barcode else 12
        sheet.merge_range(rowx+5, start_col, rowx+5, start_col+1, _(u'Printed Date') + u": %s" % unicode(datetime.now().strftime('%Y-%m-%d')), format_title_right) 
        
        rowx += 6
        col = 0
        sheet.merge_range(rowx, col, rowx+1, col, u'№', format_h_text_bordered)
        if self.show_barcode:
            col += 1
            sheet.merge_range(rowx, col, rowx+1, col, _(u'Product Bar Code'), format_h_text_bordered)
        sheet.merge_range(rowx, col+1, rowx+1, col+1, _(u'Origin Number'), format_h_text_bordered)
        sheet.merge_range(rowx, col+2, rowx+1, col+2, _(u'Internal Reference'), format_h_text_bordered) 
        sheet.merge_range(rowx, col+3, rowx+1, col+3, _(u'Product'), format_h_text_bordered)
        col += 1
        sheet.merge_range(rowx, col+3, rowx+1, col+3, _(u'Unit of measure'), format_h_text_bordered)
        sheet.merge_range(rowx, col+4, rowx, col+6, _(u'Sale'), format_h_text_bordered) 
        sheet.write(rowx+1, col+4, _(u'Quantity'), format_h_text_bordered) 
        sheet.write(rowx+1, col+5, _(u'Unit Price'), format_h_text_bordered)
        sheet.write(rowx+1, col+6, _(u'Total Price'), format_h_text_bordered)
        col += 3
        sheet.merge_range(rowx, col+4, rowx, col+6, _(u'Picking Refund'), format_h_text_bordered) 
        sheet.write(rowx+1, col+4, _(u'Quantity'), format_h_text_bordered) 
        sheet.write(rowx+1, col+5, _(u'Unit Price'), format_h_text_bordered)
        sheet.write(rowx+1, col+6, _(u'Total Price'), format_h_text_bordered)
        col += 3
        sheet.merge_range(rowx, col+4, rowx, col+6, _(u'Invoice Refund'), format_h_text_bordered) 
        sheet.write(rowx+1, col+4, _(u'Quantity'), format_h_text_bordered) 
        sheet.write(rowx+1, col+5, _(u'Unit Price'), format_h_text_bordered)
        sheet.write(rowx+1, col+6, _(u'Total Price'), format_h_text_bordered)
                
        results = self.get_lines()
        index = 1
        rowx += 2
        
        writen_group_ids, writen_so_ids = [], []
        group_rows, so_rows, pp_rows = [], [], []
        current_group_rowx, current_so_rowx = -1, -1
        
        all_pp_line_count = 0
        
        all_col_count = self.get_all_col_count()
        main_col_count = 5 + (1 if self.show_barcode else 0)
        
        startx, endx = -1, -1
        
        if results:
            startx, endx = rowx + 1, rowx + 1
            for result in results:
                if self.group_type and self.group_type != 'no_group' and result['group_id'] not in writen_group_ids:
                    writen_group_ids.append(result['group_id'])
                    group_rows.append(rowx+1)
                    
                    sheet.merge_range(rowx, 0, rowx, main_col_count - 1, "%s: %s" %(self.get_group_name(), result['group_name']), format_subh_text_bordered)
                    
                    if current_group_rowx != -1:
                        if self.group_by_sale_order and so_rows:
                            for i in range(main_col_count, all_col_count - 1, 3):
                                sheet.write_formula(current_group_rowx, i, self.get_sum_formula_from_list(i, so_rows), format_subh_number_bordered)
                                sheet.write(current_group_rowx, i+1, "", format_subh_number_bordered)
                                sheet.write_formula(current_group_rowx, i+2, self.get_sum_formula_from_list(i+2, so_rows), format_subh_number_bordered)
                        else:
                            if pp_rows:
                                for i in range(main_col_count, all_col_count - 1, 3):
                                    sheet.write_formula(current_group_rowx, i, self.get_sum_formula_from_list(i, pp_rows), format_subh_number_bordered)
                                    sheet.write(current_group_rowx, i+1, "", format_subh_number_bordered)
                                    sheet.write_formula(current_group_rowx, i+2, self.get_sum_formula_from_list(i+2, pp_rows), format_subh_number_bordered)
                    if self.group_by_sale_order and current_so_rowx != -1:
                        if pp_rows:
                            for i in range(main_col_count, all_col_count - 1, 3):
                                sheet.write_formula(current_so_rowx, i, self.get_sum_formula_from_list(i, pp_rows), format_subh_number_bordered)
                                sheet.write(current_so_rowx, i+1, "", format_subh_number_bordered)
                                sheet.write_formula(current_so_rowx, i+2, self.get_sum_formula_from_list(i+2, pp_rows), format_subh_number_bordered)
                                
                    so_rows = []
                    pp_rows = []
                    writen_so_ids = []
                    current_group_rowx = rowx
                    rowx += 1
                    
                if self.group_by_sale_order and result['so_id'] not in writen_so_ids:
                    writen_so_ids.append(result['so_id'])
                    so_rows.append(rowx+1)
                    
                    sheet.merge_range(rowx, 0, rowx, main_col_count - 1, "%s: %s" %(result['so_name'], result['confirmation_date']), format_subh_text_bordered)
                    
                    if current_so_rowx != -1:
                        if pp_rows:
                            for i in range(main_col_count, all_col_count - 1, 3):
                                sheet.write_formula(current_so_rowx, i, self.get_sum_formula_from_list(i, pp_rows), format_subh_number_bordered)
                                sheet.write(current_so_rowx, i+1, "", format_subh_number_bordered)
                                sheet.write_formula(current_so_rowx, i+2, self.get_sum_formula_from_list(i+2, pp_rows), format_subh_number_bordered)
                                
                    pp_rows = []
                    current_so_rowx = rowx
                    rowx += 1
                    
                all_pp_line_count += 1
                pp_rows.append(rowx+1)
                col = 0
                sheet.write(rowx, col, all_pp_line_count, format_dd_center_bordered)
                if self.show_barcode:
                    col += 1
                    sheet.write(rowx, col, result.get("barcode") or "", format_text_center_bordered)
                sheet.write(rowx, col+1, result.get("so_name") or "", format_text_center_bordered)
                sheet.write(rowx, col+2, result.get("default_code") or 0, format_text_center_bordered)
                sheet.write(rowx, col+3, result.get("pt_name") or 0, format_text_left_bordered)
                sheet.write(rowx, col+4, result.get("uom_name") or 0, format_text_center_bordered)
                sheet.write(rowx, col+5, result.get("so_total_quantity") or 0, format_number_right_bordered)
                sheet.write(rowx, col+6, result.get("so_unit_price") or 0, format_number_right_bordered)
                sheet.write_formula(rowx, col+7, self.get_arithmetic_formula(col+5, rowx, col+6, rowx, '*'), format_number_right_bordered)
                col += 3
                sheet.write(rowx, col+5, result.get("refund_picking_total_quantity") or 0, format_number_right_bordered)
                sheet.write(rowx, col+6, result.get("refund_picking_unit_price") or 0, format_number_right_bordered)
                sheet.write_formula(rowx, col+7, self.get_arithmetic_formula(col+5, rowx, col+6, rowx, '*'), format_number_right_bordered)
                col += 3
                sheet.write(rowx, col+5, result.get("refund_invoice_total_quantity") or 0, format_number_right_bordered)
                sheet.write(rowx, col+6, result.get("refund_invoice_unit_price") or 0, format_number_right_bordered)
                sheet.write_formula(rowx, col+7, self.get_arithmetic_formula(col+5, rowx, col+6, rowx, '*'), format_number_right_bordered)
                
                endx = rowx
                rowx += 1
                
                if results[-1] == result:
                    if current_group_rowx != -1:
                        if self.group_by_sale_order and so_rows:
                            for i in range(main_col_count, all_col_count - 1, 3):
                                sheet.write_formula(current_group_rowx, i, self.get_sum_formula_from_list(i, so_rows), format_subh_number_bordered)
                                sheet.write(current_group_rowx, i+1, "", format_subh_number_bordered)
                                sheet.write_formula(current_group_rowx, i+2, self.get_sum_formula_from_list(i+2, so_rows), format_subh_number_bordered)
                        else:
                            if pp_rows:
                                for i in range(main_col_count, all_col_count - 1, 3):
                                    sheet.write_formula(current_group_rowx, i, self.get_sum_formula_from_list(i, pp_rows), format_subh_number_bordered)
                                    sheet.write(current_group_rowx, i+1, "", format_subh_number_bordered)
                                    sheet.write_formula(current_group_rowx, i+2, self.get_sum_formula_from_list(i+2, pp_rows), format_subh_number_bordered)
                    if self.group_by_sale_order and current_so_rowx != -1:
                        if pp_rows:
                            for i in range(main_col_count, all_col_count - 1, 3):
                                sheet.write_formula(current_so_rowx, i, self.get_sum_formula_from_list(i, pp_rows), format_subh_number_bordered)
                                sheet.write(current_so_rowx, i+1, "", format_subh_number_bordered)
                                sheet.write_formula(current_so_rowx, i+2, self.get_sum_formula_from_list(i+2, pp_rows), format_subh_number_bordered)
        
            # build content footer
            sheet.merge_range(rowx, 0, rowx, main_col_count - 1, _("TOTAL"), format_h_text_bordered)
            for i in range(main_col_count, all_col_count - 1, 3):
                if self.group_type and self.group_type != 'no_group':
                    sheet.write_formula(rowx, i, self.get_sum_formula_from_list(i, group_rows), format_h_number_bordered)
                    sheet.write_formula(rowx, i+2, self.get_sum_formula_from_list(i+2, group_rows), format_h_number_bordered)
                elif self.group_by_sale_order:
                    sheet.write_formula(rowx, i, self.get_sum_formula_from_list(i, so_rows), format_h_number_bordered)
                    sheet.write_formula(rowx, i+2, self.get_sum_formula_from_list(i+2, so_rows), format_h_number_bordered)
                elif (startx != -1 and endx != -1):
                    sheet.write_formula(rowx, i, self.get_sum_formula(startx, endx, i), format_h_number_bordered)
                    sheet.write_formula(rowx, i+2, self.get_sum_formula(startx, endx, i+2), format_h_number_bordered)
                sheet.write(rowx, i+1, "", format_h_number_bordered)
                   
            rowx += 3
            sheet.merge_range(rowx, 1, rowx, 3, _(u"Checked By") + u": ............................................ ............... /                                                /", format_title_left)
            
        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report() 
    
