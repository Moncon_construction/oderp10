# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
import xlsxwriter
from odoo import api, fields, models, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from odoo.exceptions import UserError
import pytz
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openpyxl import formula
from xlrd import sheet
from odoo.exceptions import UserError
from operator import itemgetter

class SalesReportByTypes(models.TransientModel):
    """
        Борлуулалтын нэр төрлөөрхи тайлан
    """

    _name = 'sales.report.by.types'
    _description = "Sales Report by Types"

    GROUP_SELECTION = [
        ('no_group', 'No Group'),
        ('by_category', 'Group by Product Category')
    ]
    
    company = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env['res.company']._company_default_get('report.sales'))
    group_type = fields.Selection(GROUP_SELECTION, default='no_group', required=True)
    
    date_from = fields.Date("Start Date", required=True, default=lambda *a: time.strftime('%Y-%m-01'), help="Used for check when the sale order is confirmed....")
    date_to = fields.Date("End Date", required=True, default=lambda *a: time.strftime('%Y-%m-%d'), help="Used for check when the sale order is confirmed....")
    
    see_sales_detail = fields.Boolean(string='See Sales Detail', default=False, help="Checked for see warehouse columns at sales debit....") 
    see_residual_detail = fields.Boolean(string='See Residual Detail', default=False, help="Checked for see warehouse columns at lasts....") 
    
    warehouses = fields.Many2many('stock.warehouse', 'sale_report_by_type_to_warehouse', 'report_id', 'warehouse_id', string="Warehouses", help="Used for check the sale orders' products belong to this warehouses' locations....")
    categories = fields.Many2many('product.category', 'sale_report_by_type_to_category', 'report_id', 'category_id', string="Category")
    products = fields.Many2many('product.product', 'sale_report_by_type_to_product_product', 'report_id', 'product_id', string="Products")
    sizes = fields.Many2many('product.size', 'sale_report_by_type_to_size', 'report_id', 'size_id', string="Product Size")

    top = fields.Integer(string="", help="Used to check first %s top sales by sales amount quant...", default=0)
    
    @api.onchange('top')
    def _get_group_type_dom_and_check_value_of_top(self):
        if self.top < 0:
            raise UserError(_(u"Top value must not be negative..."))
        
    @api.onchange("company")
    def _get_warehouse_and_categ_domain(self):
        domain = {}
        domain['warehouses'] = [('company_id','=', self.company.id)]
        _warehouses = []
        for warehouse in self.env.user.allowed_warehouses:
            _warehouses.append(warehouse.id)
        if _warehouses:
            domain['warehouses'] = [('id', 'in', _warehouses), ('company_id','=', self.company.id)]
        domain['categories'] = [('company_id','=', self.company.id)]
        return {'domain': domain}
    
    @api.onchange("categories")
    def _get_product_domain(self):
        domain = {}
        if self.categories and len(self.categories) > 0:
            category_ids = self.env['product.category'].search([('id', 'child_of', self.categories.ids)])
            domain['products'] = [('product_tmpl_id.categ_id','in', category_ids.ids)]
        return {'domain': domain}
    
    def get_warehouses(self):
        _warehouses = []
        for warehouse in self.env.user.allowed_warehouses:
            if warehouse.company_id == self.company:
                _warehouses.append(warehouse)
        return _warehouses
    
    def get_warehouses_ids(self):
        _warehouses_ids = []
        for warehouse in self.env.user.allowed_warehouses:
            if warehouse.company_id == self.company:
                _warehouses_ids.append(warehouse.id)
        return _warehouses_ids
    
    def _get_lines(self):
        # get products
        category_ids = self.env['product.category'].search([('id', 'child_of', self.categories.ids)])
        product_ids = self.products
        if category_ids:
            product_ids = self.env['product.product'].search([('product_tmpl_id.categ_id','in', category_ids.ids)])
        
        product_qry = " AND pp.id IN (" + str(product_ids.ids)[1:len(str(product_ids.ids))-1] + ") " if str(product_ids.ids) != "[]" else ""
                    
        select_by = ""
        select_by1 = "" 
        top_product_qry = ""
        order_by = " pro_name, pro_id "
        order_by1 = " table1.pro_name, table1.pro_id "
        top_product_order_by = "ORDER BY table2.total_sales_debit_quant DESC"

        if self.group_type == "by_category":
            select_by = ", pc.id as group_id, pc.name as group_name " 
        if self.group_type != "no_group":
            order_by = " group_id, group_name, pro_name, pro_id "
            order_by1 = " table1.group_id, table1.group_name, table1.pro_name, table1.pro_id "
            top_product_order_by += ", table1.group_id, table1.group_name, table1.pro_name, table1.pro_id "
            select_by1 = ", table1.group_id as group_id, table1.group_name as group_name "
        
        if order_by != "":
            tmp_order_by = "ORDER BY " + order_by
            order_by = tmp_order_by
            order_by1 = "ORDER BY " + order_by1
            
        pro_category_qry = " AND pc.id IN (" + str(category_ids.ids)[1:len(str(category_ids.ids))-1] + ") AND pc.id IS NOT NULL " if str(category_ids.ids) != "[]" else ""
        pro_size_qry = " AND size.id IN (" + str(self.sizes.ids)[1:len(str(self.sizes.ids))-1] + ") AND size.id IS NOT NULL " if str(self.sizes.ids) != "[]" else ""
        warehouse_qry = " AND wh.id IN (" + str(self.get_warehouses_ids())[1:len(str(self.get_warehouses_ids()))-1] + ") AND wh.id IS NOT NULL " if str(self.get_warehouses_ids()) != "[]" else ""
        if self.warehouses:
            warehouse_qry = " AND wh.id IN (" + str(self.warehouses.ids)[1:len(str(self.warehouses.ids))-1] + ") AND wh.id IS NOT NULL " if str(self.warehouses.ids) != "[]" else ""
        
        date1 = str(self.date_from) + ' 00:00:00'
        date2 = str(self.date_to) + ' 23:59:59'
            
        if self.top > 0:
            order_by1 = ""
            
            top_product_qry = """
                         WHERE table1.pro_id IN 
                        (
                            SELECT table1.pro_id 
                            FROM
                            (
                                SELECT table1.pro_id as pro_id, SUM(table1.sales_debit_qty) AS total_sales_debit_qty
                                FROM
                                (
                                    SELECT m.product_id as pro_id,
                                    CASE 
                                        WHEN m.date BETWEEN '%s' AND '%s' AND m.location_dest_id NOT IN (wh.lot_stock_id) AND m.location_id IN (wh.lot_stock_id) AND src_loc.usage = 'internal' AND dest_loc.usage IN ('customer')
                                            THEN COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0) ELSE 0 END AS sales_debit_qty 
                                    
                                    FROM stock_move m
                                    LEFT JOIN stock_location src_loc ON src_loc.id = m.location_id
                                    LEFT JOIN stock_location dest_loc ON dest_loc.id = m.location_dest_id
                                    
                                    LEFT JOIN product_product pp ON (pp.id = m.product_id)                 
                                    LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id) 
                                    LEFT JOIN product_uom uom_m ON (uom_m.id = m.product_uom)                 
                                    LEFT JOIN product_uom uom_t ON (uom_t.id = pt.uom_id) 
                        
                                    LEFT JOIN stock_warehouse wh ON wh.lot_stock_id = m.location_dest_id OR wh.lot_stock_id = m.location_id
                                    WHERE m.state = 'done' AND COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0) <> 0 %s %s 
                                ) table1
                                GROUP BY table1.pro_id ORDER BY total_sales_debit_qty DESC LIMIT %s 
                            ) table1
                        )
                        %s
            """ %(date1, date2, product_qry, warehouse_qry, self.top, top_product_order_by)
            
        qry = """
            SELECT table1.*,
                    table2.total_init_quant AS total_init_qty,
                    table2.total_credit_quant,
                    table2.total_other_debit_quant,
                    table2.total_sales_debit_quant,
                    table2.total_debit_quant,
                    table2.total_lasts_quant,
                    table2.total_lasts_quant_by_date
                    %s
            FROM
            (    
                SELECT pp.id as pro_id, pt.id as pro_tmpl_id, pt.name as pro_name,
                    pp.default_code as pro_default_code, pp.barcode as pro_barcode, 
                    size.id as size_id, size.name as size_name, 
                    uom_t.id as pro_uom_id, uom_t.name as pro_uom_name, pt.list_price as pro_purchase_price,  
                    wh.name, table1.wh_id, 
                
                
                    table1.init_qty AS wh_init_quant,
                    table1.credit_qty AS wh_credit_quant,
                    table1.other_debit_qty AS wh_other_debit_quant,
                    table1.sales_debit_qty AS wh_sales_debit_quant,
                    table1.debit_qty AS wh_debit_quant,
                    (table1.init_qty + table1.credit_qty - table1.debit_qty) AS wh_lasts_quant,
                    table1.end_qty AS wh_lasts_quant_by_date
                
                    %s
                    
                FROM
                (
                    SELECT table1.pro_id, table1.wh_id AS wh_id, SUM(table1.init_qty) AS init_qty, SUM(table1.credit_qty) AS credit_qty, SUM(table1.other_debit_qty) AS other_debit_qty, SUM(table1.sales_debit_qty) AS sales_debit_qty,
                    COALESCE(SUM(table1.other_debit_qty) + SUM(table1.sales_debit_qty)) as debit_qty, SUM(table1.end_qty) AS end_qty
                        FROM
                        (
                            SELECT wh.id as wh_id, wh.name as wh_name, m.product_id as pro_id,
                                CASE 
                                    WHEN m.date < '%s' AND m.location_id NOT IN (wh.lot_stock_id) AND m.location_dest_id IN (wh.lot_stock_id)
                                        THEN COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0)
                                    WHEN m.date < '%s' AND m.location_id IN (wh.lot_stock_id) AND m.location_dest_id NOT IN (wh.lot_stock_id)
                                        THEN -COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0) ELSE 0 END AS init_qty,
                                CASE 
                                    WHEN m.date > '%s' AND m.location_id NOT IN (wh.lot_stock_id) AND m.location_dest_id IN (wh.lot_stock_id)
                                        THEN COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0)
                                    WHEN m.date > '%s' AND m.location_id IN (wh.lot_stock_id) AND m.location_dest_id NOT IN (wh.lot_stock_id)
                                        THEN -COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0) ELSE 0 END AS end_qty,
                        
                                CASE 
                                    WHEN m.date BETWEEN '%s' AND '%s' AND m.location_id NOT IN (wh.lot_stock_id) AND m.location_dest_id IN (wh.lot_stock_id) AND src_loc.usage IN ('supplier', 'customer', 'transit', 'production', 'inventory') AND dest_loc.usage = 'internal'
                                        THEN COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0) ELSE 0 END AS credit_qty,
                                
                                CASE 
                                    WHEN m.date BETWEEN '%s' AND '%s' AND m.location_id IN (wh.lot_stock_id) AND m.location_dest_id NOT IN (wh.lot_stock_id) AND src_loc.usage = 'internal' AND dest_loc.usage IN ('supplier', 'transit', 'production', 'inventory')
                                        THEN COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0) ELSE 0 END AS other_debit_qty,
                    
                                CASE 
                                    WHEN m.date BETWEEN '%s' AND '%s' AND m.location_dest_id NOT IN (wh.lot_stock_id) AND m.location_id IN (wh.lot_stock_id) AND src_loc.usage = 'internal' AND dest_loc.usage IN ('customer')
                                        THEN COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0) ELSE 0 END AS sales_debit_qty  
                                    
                            FROM stock_move m
                            LEFT JOIN stock_location src_loc ON src_loc.id = m.location_id
                            LEFT JOIN stock_location dest_loc ON dest_loc.id = m.location_dest_id
                            
                            LEFT JOIN product_product pp ON (pp.id = m.product_id)                 
                            LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id) 
                            LEFT JOIN product_uom uom_m ON (uom_m.id = m.product_uom)                 
                            LEFT JOIN product_uom uom_t ON (uom_t.id = pt.uom_id) 
                
                            LEFT JOIN stock_warehouse wh ON wh.lot_stock_id = m.location_dest_id OR wh.lot_stock_id = m.location_id
                            WHERE m.state = 'done' %s %s
                        ) table1
                        GROUP BY table1.pro_id, table1.wh_id
                ) table1
                LEFT JOIN stock_warehouse wh ON (wh.id = table1.wh_id)
                LEFT JOIN product_product pp ON (pp.id = table1.pro_id) 
                LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id)                     
                LEFT JOIN product_size size ON (size.id = pt.size_id)                     
                LEFT JOIN product_uom uom_t ON (uom_t.id = pt.uom_id) 
                LEFT JOIN product_category pc ON (pc.id = pt.categ_id) 
                WHERE 1=1 %s %s %s
                
                %s
    
        ) table1 LEFT JOIN 
        (
            SELECT pp.id as pro_id, 
                
                    SUM(table1.init_qty) AS total_init_quant,
                    SUM(table1.credit_qty) AS total_credit_quant,
                    SUM(table1.other_debit_qty) AS total_other_debit_quant,
                    SUM(table1.sales_debit_qty) AS total_sales_debit_quant,
                    SUM(table1.debit_qty) AS total_debit_quant,
                    SUM(table1.init_qty + table1.credit_qty - table1.debit_qty) AS total_lasts_quant,
                    SUM(table1.end_qty) AS total_lasts_quant_by_date
                    
            FROM
            (
                SELECT table1.pro_id, table1.wh_id AS wh_id, SUM(table1.init_qty) AS init_qty, SUM(table1.credit_qty) AS credit_qty, SUM(table1.other_debit_qty) AS other_debit_qty, SUM(table1.sales_debit_qty) AS sales_debit_qty,
                        COALESCE(SUM(table1.other_debit_qty) + SUM(table1.sales_debit_qty)) as debit_qty, SUM(table1.end_qty) AS end_qty
                FROM
                (
                    SELECT wh.id as wh_id, wh.name as wh_name, m.product_id as pro_id,
                        CASE 
                            WHEN m.date < '%s' AND m.location_id NOT IN (wh.lot_stock_id) AND m.location_dest_id IN (wh.lot_stock_id)
                                THEN COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0)
                            WHEN m.date < '%s' AND m.location_id IN (wh.lot_stock_id) AND m.location_dest_id NOT IN (wh.lot_stock_id)
                                THEN -COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0) ELSE 0 END AS init_qty,
                        
                        CASE 
                            WHEN m.date > '%s' AND m.location_id NOT IN (wh.lot_stock_id) AND m.location_dest_id IN (wh.lot_stock_id)
                                THEN COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0)
                            WHEN m.date > '%s' AND m.location_id IN (wh.lot_stock_id) AND m.location_dest_id NOT IN (wh.lot_stock_id)
                                THEN -COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0) ELSE 0 END AS end_qty,
                
                        CASE 
                            WHEN m.date BETWEEN '%s' AND '%s' AND m.location_id NOT IN (wh.lot_stock_id) AND m.location_dest_id IN (wh.lot_stock_id) AND src_loc.usage IN ('supplier', 'customer', 'transit', 'production', 'inventory') AND dest_loc.usage = 'internal'
                                THEN COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0) ELSE 0 END AS credit_qty,
                        
                        CASE 
                            WHEN m.date BETWEEN '%s' AND '%s' AND m.location_id IN (wh.lot_stock_id) AND m.location_dest_id NOT IN (wh.lot_stock_id) AND src_loc.usage = 'internal' AND dest_loc.usage IN ('supplier', 'transit', 'production', 'inventory')
                                THEN COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0) ELSE 0 END AS other_debit_qty,
            
                        CASE 
                            WHEN m.date BETWEEN '%s' AND '%s' AND m.location_dest_id NOT IN (wh.lot_stock_id) AND m.location_id IN (wh.lot_stock_id) AND src_loc.usage = 'internal' AND dest_loc.usage IN ('customer')
                                THEN COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0) ELSE 0 END AS sales_debit_qty  
                                
                    FROM stock_move m
                    LEFT JOIN stock_location src_loc ON src_loc.id = m.location_id
                    LEFT JOIN stock_location dest_loc ON dest_loc.id = m.location_dest_id
                    
                    LEFT JOIN product_product pp ON (pp.id = m.product_id)                 
                    LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id) 
                    LEFT JOIN product_uom uom_m ON (uom_m.id = m.product_uom)                 
                    LEFT JOIN product_uom uom_t ON (uom_t.id = pt.uom_id) 
        
                    LEFT JOIN stock_warehouse wh ON wh.lot_stock_id = m.location_dest_id OR wh.lot_stock_id = m.location_id
                    WHERE m.state = 'done' %s %s
                ) table1
                GROUP BY table1.pro_id, table1.wh_id 
            ) table1
            LEFT JOIN stock_warehouse wh ON (wh.id = table1.wh_id)
            LEFT JOIN product_product pp ON (pp.id = table1.pro_id) 
            LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id)                     
            LEFT JOIN product_size size ON (size.id = pt.size_id)                     
            LEFT JOIN product_uom uom_t ON (uom_t.id = pt.uom_id) 
            LEFT JOIN product_category pc ON (pc.id = pt.categ_id) 
            WHERE 1=1 %s %s %s 
            GROUP BY pp.id 
        ) table2 ON table2.pro_id = table1.pro_id %s %s
        """ %(select_by1, select_by, 
              date1, date1, date2, date2, date1, date2, date1, date2, date1, date2, 
              product_qry, warehouse_qry, pro_category_qry, pro_size_qry, warehouse_qry, order_by, 
              date1, date1, date2, date2, date1, date2, date1, date2, date1, date2,
              product_qry, warehouse_qry, pro_category_qry, pro_size_qry, warehouse_qry, order_by1, top_product_qry)

        self._cr.execute(qry)
        results = self._cr.dictfetchall()
        return results if results else False
    
    def  get_xsl_column_name(self, index):
        alphabet = {'0': 'A',  '1':'B',  '2':'C',  '3':'D',  '4':'E',
                    '5': 'F',  '6':'G',  '7':'H',  '8':'I',  '9':'J',
                    '10':'K',  '11':'L', '12':'M', '13':'N', '14':'O',
                    '15':'P',  '16':'Q', '17':'R', '18':'S', '19':'T',
                    '20':'U',  '21':'V', '22':'W', '23':'X', '24':'Y', '25':'Z'}
        
        if index <= 25:
            return (alphabet[str(index)] + ":" + alphabet[str(index)])
        else:
            return (alphabet[str(index/26-1)] + alphabet[str(index%26)] + ":" + alphabet[str(index/26-1)] + alphabet[str(index%26)])
    
    def get_column_name_for_calculate(self, index):
        column_name = self.get_xsl_column_name(index).split(':')[0]
        return column_name
      
    def get_arithmetic_formula(self, f_coly, f_rowx, s_coly, s_rowx, oprtr):
        f_cell_index = self.get_column_name_for_calculate(f_coly) + str(f_rowx)
        s_cell_index = self.get_column_name_for_calculate(s_coly) + str(s_rowx)
        return f_cell_index + oprtr + s_cell_index
    
    def get_sum_formula(self, startx, endx, coly):
        return "{=SUM(" + self.get_column_name_for_calculate(coly) + str(startx) + ":" + self.get_column_name_for_calculate(coly) + str(endx) + ")}"
    
    def get_sum_formula_in_not_formula(self, startx, endx, coly):
        formula = self.get_sum_formula(startx, endx, coly)
        return formula[1:(len(formula)-1)]
    
    def get_sum_formula_from_list(self, list):
        formula = "="
        if list:
            for i in range(len(list)):
                formula += ("+" + self.get_column_name_for_calculate(list[i]) + str(list[i]))
        return formula
        
    def set_specific_columns(self, sheet, result, rowx, coly, first_col_value, format, fixed_coly):
        sheet.write(rowx, coly, first_col_value, format)
        sheet.write_formula(rowx, coly+1, self.get_arithmetic_formula(fixed_coly, rowx+1, coly, rowx+1, '*'), format)
        return sheet
    
    def get_specific_column_count(self):
        return 2
                     
    def set_specific_column_headers(self, sheet, rowx, coly, format):
        if 'from_end_balance' in self._context.keys() and self._context['from_end_balance']:
            sheet.merge_range(rowx, coly, rowx+1, coly, _(u'Quant'), format)
            sheet.merge_range(rowx, coly+1, rowx+1, coly+1, _(u'Total Amount'), format)
        else:
            sheet.write(rowx, coly, _(u'Quant'), format)
            sheet.write(rowx, coly+1, _(u'Total Amount By Purchase Price'), format)
        return sheet

    def get_specific_column_header_count(self):
        return 2
       
    def is_debit(self, loc1_usage, loc2_usage):
        if (loc2_usage in ['supplier', 'customer', 'transit', 'production', 'inventory']) and loc1_usage == 'internal':
            # Зарлага  
            return True
        elif (loc1_usage in ['supplier', 'customer', 'transit', 'production', 'inventory']) and loc2_usage == 'internal':
            # Орлого
            return False
    
    def get_group_name(self):
        if self.group_type == 'no_group':
            return u""
        elif self.group_type == 'by_category':
            return u"Ангилал" if self.env.user.lang == 'mn_MN' else u"Product Catgegory"
        
    def set_sub_cols(self, sheet, coly, i):
        sheet.set_column(self.get_xsl_column_name(coly+i*self.get_specific_column_count()), 7)
        sheet.set_column(self.get_xsl_column_name(coly+i*self.get_specific_column_count()+1), 12)
        return sheet
    
    def set_static_cols_width(self, sheet):
        coly = 0
        #    №    Дотоод код    Баркод    Бараа материал    Размер    
        sheet.set_column(self.get_xsl_column_name(coly), 3)
        sheet.set_column(self.get_xsl_column_name(coly+1), 10)
        sheet.set_column(self.get_xsl_column_name(coly+2), 15)
        sheet.set_column(self.get_xsl_column_name(coly+3), 20)
        sheet.set_column(self.get_xsl_column_name(coly+4), 5)
        
        #    Хэмжих нэгж    Худалдах үнэ        
        coly += 5
        sheet.set_column(self.get_xsl_column_name(coly), 8)
        sheet.set_column(self.get_xsl_column_name(coly+1), 12)
        
        #  Эхний үлдэгдэл/Тоо  Эхний үлдэгдэл/Нийт дүн  Нийт орлого/Тоо  Нийт орлого/Нийт дүн
        coly += 2
        sheet.set_column(self.get_xsl_column_name(coly), 7)
        sheet.set_column(self.get_xsl_column_name(coly+1), 12)
        sheet.set_column(self.get_xsl_column_name(coly+2), 7)
        sheet.set_column(self.get_xsl_column_name(coly+3), 12)
        
        coly += 4
        return sheet, coly
    
    def set_half_static_cols_width(self, sheet, coly, tmp_warehouses):
        # 'Нийт борлуулалтын дүн' баганын дэд багануудын өргөнийг оноох
        sheet.set_column(self.get_xsl_column_name(coly), 7)
        sheet.set_column(self.get_xsl_column_name(coly+1), 12)
        # 'Загварын нийт борлогдсон тоо хэмжээ' баганын өргөнийг оноох
        coly += self.get_specific_column_count()
        sheet.set_column(self.get_xsl_column_name(coly), 10)
        # 'Бусад зарлага' баганын дэд багануудын өргөнийг оноох
        coly += 1
        sheet.set_column(self.get_xsl_column_name(coly), 7)
        sheet.set_column(self.get_xsl_column_name(coly+1), 12)
        # 'Эцсийн үлдэгдэл' баганын дэд багануудын өргөнийг оноох
        coly += self.get_specific_column_count()
        sheet.set_column(self.get_xsl_column_name(coly), 7)
        sheet.set_column(self.get_xsl_column_name(coly+1), 12)
        # Хэрвээ 'Үлдэгдлийн задаргааг гаргах эсэх' чеклэгдсэн бол coly-с эхлэх багануудад өргөнийг нь оноох
        coly += self.get_specific_column_count()
        if self.see_residual_detail:
            for i in range(len(tmp_warehouses)):
                sheet = self.set_sub_cols(sheet, coly, i)
        return sheet, coly
    
    def set_static_cols_header(self, sheet, rowx, coly, format_column_header, format_column_header_vertically):
        sheet.merge_range(rowx, coly, rowx+2, coly, _(u'№'), format_column_header)
        sheet.merge_range(rowx, coly+1, rowx+2, coly+1, _(u'Product Default Code'), format_column_header)
        sheet.merge_range(rowx, coly+2, rowx+2, coly+2, _(u'Product Bar Code'), format_column_header)
        sheet.merge_range(rowx, coly+3, rowx+2, coly+3, _(u'Product Name'), format_column_header)
        sheet.merge_range(rowx, coly+4, rowx+2, coly+4, _(u'Product Size'), format_column_header_vertically)
        sheet.merge_range(rowx, coly+5, rowx+2, coly+5, _(u'Product Unit of Measure'), format_column_header)
        sheet.merge_range(rowx, coly+6, rowx+2, coly+6, _(u'Product Purchase Price'), format_column_header)
        coly += 7
        return sheet, coly
    
    def set_static_cols(self, sheet, rowx, coly, result, format_text_center_bordered):
        sheet.write(rowx, coly+1, result['pro_default_code'], format_text_center_bordered)
        sheet.write(rowx, coly+2, result['pro_barcode'], format_text_center_bordered)
        sheet.write(rowx, coly+3, result['pro_name'], format_text_center_bordered)
        sheet.write(rowx, coly+4, result['size_name'], format_text_center_bordered)
        sheet.write(rowx, coly+5, result['pro_uom_name'], format_text_center_bordered)
        sheet.write(rowx, coly+6, result['pro_purchase_price'], format_text_center_bordered)
        return sheet
    
    def build_footer(self, sheet, rowx, format_text_left):
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, 1, _(u"Elaborated By:"), format_text_left)
        sheet.merge_range(rowx, 2, rowx, 3, _(u"%s") %(u"..................................................."), format_text_left)
        sheet.merge_range(rowx, 5, rowx, 6, u"/%s/" %(self.env.user.name), format_text_left)
        
        return sheet
            
    def get_total_fixed_col_count(self):
        return 3;
    
    def set_empty_wh_values(self, sheet, result, rowx, tmp_whs, filled_sales_wh_ids, filled_residual_wh_ids, tmp_warehouses_dict, format, purchase_price_col_index):
        coly = 0
        
        # Үлдсэн борлуулалтын зарлагын агуулахуудыг 0 утгаар дүүргэх
        if self.see_sales_detail:
            for wh in tmp_whs:
                if wh.id not in filled_sales_wh_ids:
                    wh_start_coly_s = coly+self.get_total_fixed_col_count()+self.get_specific_column_count()*2+4
                    sheet = self.with_context({'by_second_uom':True}).set_specific_columns(sheet, result, rowx, wh_start_coly_s + tmp_warehouses_dict["wh_"+str(wh.id)]*self.get_specific_column_count(), 0, format, purchase_price_col_index)
                   
        # Үлдсэн эцсийн үлдэгдлийн агуулахуудыг 0 утгаар дүүргэх
        if self.see_residual_detail:
            for wh in tmp_whs:      
                if wh.id not in filled_residual_wh_ids:
                    wh_start_coly_r = coly + self.get_total_fixed_col_count() + 4 + self.get_specific_column_count()*5+1
                    wh_start_coly_r += len(tmp_whs)*self.get_specific_column_count() if self.see_sales_detail else 0
                    sheet = self.with_context({'by_second_uom':True}).set_specific_columns(sheet, result, rowx, wh_start_coly_r + tmp_warehouses_dict["wh_"+str(wh.id)]*self.get_specific_column_count(), 0, format, purchase_price_col_index)
        return sheet
                   
    def check_already_added_to_sheet(self, result, writen_pro_uom_ids):
        if result['pro_id'] in writen_pro_uom_ids:
            return True
        return False
         
    def add_to_writen_pro_uom_ids(self, result, writen_pro_uom_ids):
        writen_pro_uom_ids.append(result['pro_id'])
        return writen_pro_uom_ids
        
    @api.multi
    def export_report(self):
        if self.top < 0:
            raise UserError(_(u"Top value must not be negative..."))

        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        
        # create name
        report_name = _(u"Sales Report by Types")
        file_name = "%s_%s.xlsx" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('sales_report_by_types'), form_title=file_name).create({})
        
        # create formats
        format_text_left = {
            'font_name': 'Times New Roman',
            'font_size': 8,
            'align': 'left',
        }
        
        format_text_left_bold = format_text_left.copy()
        format_text_left_bold['bold'] = True
        
        format_text_center_header = format_text_left_bold.copy()
        format_text_center_header['font_size'] = '14'
        format_text_center_header['align'] = 'center'
        
        format_text_left_footer = format_text_left.copy()
        format_text_left_footer['font_size'] = '9'
        
        format_text_left_bordered = format_text_left.copy()
        format_text_left_bordered['border'] = 1
        
        format_text_right_bordered = format_text_left_bordered.copy()
        format_text_right_bordered['align'] = 'right'
        
        format_text_center_bordered = format_text_left_bordered.copy()
        format_text_center_bordered['align'] = 'center'
        format_text_center_bordered['text_wrap'] = 1
        
        format_number_right_bordered = format_text_left_bordered.copy()
        format_number_right_bordered['num_format'] = '_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)'
        format_number_right_bordered['align'] = 'right'
        
        format_number_right_bordered_vcentered = format_number_right_bordered.copy()
        
        format_dd_right_bordered = format_number_right_bordered.copy()
        format_dd_right_bordered['num_format'] = '#,##0_);(#,##0)'
        
        format_dd_right_bordered_colored = format_dd_right_bordered.copy()
        format_dd_right_bordered_colored['bg_color'] = '#ccffff'
        
        format_dd_right_bordered_footer = format_dd_right_bordered_colored.copy()
        format_dd_right_bordered_footer['bg_color'] = '#99ccff'
        format_dd_right_bordered_footer['bold'] = True
        format_dd_right_bordered_footer['font_size'] = '10'
        
        format_column_header = format_text_center_bordered.copy()
        format_column_header['bold'] = True
        format_column_header['bg_color'] = '#99ccff'
        format_column_header['text_wrap'] = 1
        
        format_column_footer_number = format_column_header.copy()
        format_column_footer_number['num_format'] = '_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)'
        format_column_footer_number['align'] = 'right'
        format_column_footer_number['font_size'] = '10'
        
        format_column_header_vertically = format_column_header.copy()
        
        format_column_subh_text = format_column_header.copy()
        format_column_subh_text['bg_color'] = '#ccffff'
        format_column_subh_text['align'] = 'left'
        
        format_column_subh_number = format_column_subh_text.copy()
        format_column_subh_number['num_format'] = '_(* #,##0.00_);_(* \(#,##0.00\);_(* -??_);_(@_)'
        format_column_subh_number['align'] = 'right'
        
        format_text_left = book.add_format(format_text_left)
        format_text_left_bold = book.add_format(format_text_left_bold)
        format_text_center_header = book.add_format(format_text_center_header)
        format_text_center_header.set_align('vcenter')
        format_text_left_footer = book.add_format(format_text_left_footer)
        format_text_left_bordered = book.add_format(format_text_left_bordered)
        format_text_right_bordered = book.add_format(format_text_right_bordered)
        format_text_center_bordered = book.add_format(format_text_center_bordered)
        format_text_center_bordered.set_align('vcenter')
        format_number_right_bordered = book.add_format(format_number_right_bordered)
        format_number_right_bordered_vcentered = book.add_format(format_number_right_bordered_vcentered)
        format_number_right_bordered_vcentered.set_align('vcenter')
        format_dd_right_bordered = book.add_format(format_dd_right_bordered)
        format_dd_right_bordered_colored= book.add_format(format_dd_right_bordered_colored)
        format_dd_right_bordered_footer = book.add_format(format_dd_right_bordered_footer)
        format_column_header = book.add_format(format_column_header)
        format_column_header.set_align('vcenter')
        format_column_header_vertically = book.add_format(format_column_header_vertically)
        format_column_header_vertically.set_rotation(90)
        format_column_header_vertically.set_align('vcenter')
        format_column_footer_number = book.add_format(format_column_footer_number)
        format_column_subh_text = book.add_format(format_column_subh_text)
        format_column_subh_number = book.add_format(format_column_subh_number)
        
        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_portrait()
        sheet.set_zoom(100)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        
        # compute column
        sheet, coly = self.set_static_cols_width(sheet)
        
        # Хэрвээ 'Борлуулалтын задаргааг гаргах эсэх' чеклэгдсэн бол coly-с эхлэх багануудад өргөнийг нь оноох
        tmp_warehouses = []
        tmp_whs = []
        if not self.warehouses:
            tmp_whs = self.get_warehouses()
        else:
            for wh in self.warehouses:
                tmp_whs.append(wh)
                
        for wh in tmp_whs:
            new_wh_dict = {}
            new_wh_dict['id'] = wh.id
            new_wh_dict['name'] = wh.name
            tmp_warehouses.append(new_wh_dict)
            
        if self.see_sales_detail:
            for i in range(len(tmp_warehouses)):
                sheet = self.set_sub_cols(sheet, coly, i)
            coly += (len(tmp_warehouses)*self.get_specific_column_count())
            
        tmp_warehouses_dict = {}
        for i in range(len(tmp_warehouses)):
            tmp_warehouses_dict["wh_"+str(tmp_warehouses[i]['id'])] = i
            
        sheet, coly = self.set_half_static_cols_width(sheet, coly, tmp_warehouses)
        
        sheet.set_row(8, 15)
        
        # build xslx header 
        rowx = 0
        company_rich_str = [format_text_left_bold, _(u"Company:"), format_text_left, self.company.name]
        sheet.write_rich_string(rowx, 0, *company_rich_str)
        sheet.merge_range(rowx+2, 0, rowx+2, 12, _(u'Sales Report by Types'), format_text_center_header)
        sheet.merge_range(rowx+4, 0, rowx+4, 1, _(u'Warehouses:'), format_text_left)
        sheet.write(rowx+4, 2, u'%s' % (', '.join([wh_dict['name'] for wh_dict in tmp_warehouses])), format_text_left)
        sheet.merge_range(rowx+5, 0, rowx+5, 1, _(u'Duration:'), format_text_left)
        sheet.write(rowx+5, 2, u'%s~%s' % (self.date_from, self.date_to), format_text_left)
        
         # build column headers
        rowx += 6
        coly = 0
        sheet, coly = self.set_static_cols_header(sheet, rowx, coly, format_column_header, format_column_header_vertically)
        
        # Эхний үлдэгдлээс хойшхи header-үүд
        sheet.merge_range(rowx, coly, rowx+1, coly+self.get_specific_column_count()-1, _(u'Initial Balance.'), format_column_header)
        sheet = self.set_specific_column_headers(sheet, rowx+2, coly, format_column_header)
        coly += self.get_specific_column_count()
        sheet.merge_range(rowx, coly, rowx+1, coly+self.get_specific_column_count()-1, _(u'Total Credit'), format_column_header)
        sheet = self.set_specific_column_headers(sheet, rowx+2, coly, format_column_header)
        wh_col_count = 0
        coly += self.get_specific_column_count()
        if self.see_sales_detail and len(tmp_warehouses) > 0:
            wh_col_count = len(tmp_warehouses)*self.get_specific_column_count()
            sheet.merge_range(rowx, coly, rowx, coly+wh_col_count-1,_(u'Sales Debit'), format_column_header)
            for i in range(len(tmp_warehouses)):
                sheet.merge_range(rowx+1, coly+i*self.get_specific_column_count(), rowx+1, coly+i*self.get_specific_column_count()+self.get_specific_column_count()-1, tmp_warehouses[i]['name'], format_column_header)
                sheet = self.set_specific_column_headers(sheet, rowx+2, coly+i*self.get_specific_column_count(), format_column_header)
            coly += wh_col_count
        sheet.merge_range(rowx, coly, rowx+1, coly+self.get_specific_column_count()-1,_(u'Sales Total Debit'), format_column_header)
        sheet = self.set_specific_column_headers(sheet, rowx+2, coly, format_column_header)
        coly += self.get_specific_column_count()
        sheet.merge_range(rowx, coly, rowx+2, coly,_(u'Total Sales Amount of Template'), format_column_header)
        coly += 1

        sheet.merge_range(rowx, coly, rowx+1, coly+self.get_specific_column_count()-1,_(u'Other Debit'), format_column_header)
        sheet = self.set_specific_column_headers(sheet, rowx+2, coly, format_column_header)
        coly += self.get_specific_column_count()
        if self.see_residual_detail and len(tmp_warehouses) > 0:
            wh_col_count = len(tmp_warehouses)*self.get_specific_column_count() + self.get_specific_column_count() - 1
            sheet.merge_range(rowx, coly, rowx, coly+wh_col_count,_(u'End Balance.'), format_column_header)
            sheet = self.with_context({'from_end_balance': True}).set_specific_column_headers(sheet, rowx+1, coly, format_column_header)
            coly += self.get_specific_column_count()
            for i in range(len(tmp_warehouses)):
                sheet.merge_range(rowx+1, coly+i*self.get_specific_column_count(), rowx+1, coly+i*self.get_specific_column_count()+self.get_specific_column_count()-1, tmp_warehouses[i]['name'], format_column_header)
                sheet = self.set_specific_column_headers(sheet, rowx+2, coly+i*self.get_specific_column_count(), format_column_header)
        else:
            sheet.merge_range(rowx, coly, rowx+1, coly+self.get_specific_column_count()-1,_(u'End Balance.'), format_column_header)
            sheet = self.set_specific_column_headers(sheet, rowx+2, coly, format_column_header)
            
        # Нэг бараанд харгалзах нийт мөр нь = (Тухайн бараа * Уг барааг агуулах агуулахын тоо) ширхэг мөр 
        # бүхий dictionary-н жагсаалт буцаана    
        results = self._get_lines()
        
        writen_category_ids = []
        writen_pro_ids = []
        writen_pro_uom_ids = []
        filled_sales_wh_ids = []
        filled_residual_wh_ids = []
        rowx += 2
        startx = rowx + 1
        pro_count = 0
        coly = 0
        purchase_price_col_index = 0
        total_product_count = 0
        current_category_sub_product_count = 0
        last_category_sub_product_count = 0
        current_category_sub_product_row = -1
        sub_row_indexes = []
        
        current_merge_pro_name = ""
        current_merge_index_s = -1
        current_merge_index_e = -1
        
        current_init_qty = 0
        current_credit_qty = 0
        current_sales_debit_qty = 0
        current_other_debit_qty = 0
        
        if results:
            total_init_qty  = 0
            total_credit_qty = 0
            total_lasts_qty = 0
            for result in results:
                coly = 0
                if self.group_type != 'no_group' and result['group_id'] not in writen_category_ids:
                    coly = 0
                     
                    if current_category_sub_product_row != -1:
                        total_col_count = self.get_total_fixed_col_count() + 5 + self.get_specific_column_count()*5
                        total_col_count += len(tmp_whs)*self.get_specific_column_count() if self.see_sales_detail else 0
                        total_col_count += len(tmp_whs)*self.get_specific_column_count() if self.see_residual_detail else 0   
                        for i in range(4 + self.get_total_fixed_col_count(), total_col_count, 1):
                            sheet.write(current_category_sub_product_row, i, self.get_sum_formula_in_not_formula(current_category_sub_product_row+2, current_category_sub_product_row+1+current_category_sub_product_count, i), format_column_subh_number)
                             
                    if not self.check_already_added_to_sheet(result, writen_pro_uom_ids):
                        if pro_count > 0:
                            # Үлдсэн борлуулалтын зарлага/эцсийн үлдэгдлийн агуулахуудыг 0 утгаар дүүргэх
                            sheet = self.set_empty_wh_values(sheet, result, rowx, tmp_whs, filled_sales_wh_ids, filled_residual_wh_ids, tmp_warehouses_dict, format_number_right_bordered, purchase_price_col_index)
                                    
                    rowx += 1
                    writen_category_ids.append(result['group_id'])
                     
                    sheet.write(rowx, coly, u"%s" %(str(len(writen_category_ids))), format_dd_right_bordered_colored)
                    sheet.merge_range(rowx, coly+1, rowx, coly + self.get_total_fixed_col_count() + 3, u"%s: %s" %(self.get_group_name(),result['group_name']), format_column_subh_text)
                    current_category_sub_product_row = rowx 
                    last_category_sub_product_count = current_category_sub_product_count
                    current_category_sub_product_count = 0
                     
                    sub_row_indexes.append(rowx)
                    pro_count = 0
                     
                    current_init_qty = 0
                    current_credit_qty = 0
                    current_sales_debit_qty = 0
                    current_other_debit_qty = 0
                    sales_col_indexes = []
                
                current_init_qty += result['wh_init_quant']
                current_credit_qty += result['wh_credit_quant']
                current_sales_debit_qty += result['wh_debit_quant']
                current_other_debit_qty += result['wh_other_debit_quant']

                if not self.check_already_added_to_sheet(result, writen_pro_uom_ids):
                    if pro_count > 0:
                        # Үлдсэн борлуулалтын зарлага/эцсийн үлдэгдлийн агуулахуудыг 0 утгаар дүүргэх
                        sheet = self.set_empty_wh_values(sheet, result, rowx, tmp_whs, filled_sales_wh_ids, filled_residual_wh_ids, tmp_warehouses_dict, format_number_right_bordered, purchase_price_col_index)
                                    
                    filled_sales_wh_ids = []
                    filled_residual_wh_ids = []
                    
                    rowx += 1
                    current_category_sub_product_count += 1
                    
                    writen_pro_uom_ids = self.add_to_writen_pro_uom_ids(result, writen_pro_uom_ids)
                    
                    coly = 0
                    pro_count += 1
                    total_product_count += 1
                    total_init_qty  = 0
                    total_credit_qty = 0
                    total_lasts_qty = 0
                    if self.group_type != 'no_group':
                        sheet.write(rowx, coly, u"%s.%s" %(str(len(writen_category_ids)), str(pro_count)), format_dd_right_bordered)
                    else:
                        sheet.write(rowx, coly, pro_count, format_dd_right_bordered)
                    
                    sheet = self.set_static_cols(sheet, rowx, coly, result, format_text_center_bordered)
                    
                    if total_product_count == 1:
                        current_merge_pro_name = result['pro_name']
                        current_merge_index_s, current_merge_index_e = rowx, rowx
                    else:
                        if result['pro_name'] == current_merge_pro_name:
                            current_merge_index_e = rowx
                        else:
                            if current_merge_index_e > current_merge_index_s:
                                prev_coly = 2 + self.get_specific_column_count()*3 + self.get_total_fixed_col_count()
                                prev_coly += len(tmp_whs)*self.get_specific_column_count() if self.see_sales_detail else 0
    
                                sheet.merge_range(current_merge_index_s, coly+3, current_merge_index_e, coly+3, current_merge_pro_name, format_text_center_bordered)
                                sheet.merge_range(current_merge_index_s, prev_coly+2, current_merge_index_e, prev_coly+2, self.get_sum_formula_in_not_formula(current_merge_index_s+1, current_merge_index_e+1, prev_coly - (self.get_specific_column_count() - 2)), format_number_right_bordered_vcentered)
                            current_merge_pro_name = result['pro_name']
                            current_merge_index_s, current_merge_index_e = rowx, rowx
                        
                    # Загварын нийт борлогдсон тоо хэмжээ - г тооцоолоход ашиглана
                    prev_coly = 2 + self.get_specific_column_count()*3 + self.get_total_fixed_col_count()
                    prev_coly += len(tmp_whs)*self.get_specific_column_count() if self.see_sales_detail else 0
                    sheet.write(rowx, prev_coly+2, u"=%s%s" %(self.get_column_name_for_calculate(prev_coly - (self.get_specific_column_count() - 2)), (rowx+1)), format_number_right_bordered)
                    
                    purchase_price_col_index = coly+self.get_total_fixed_col_count()+4-1 
                    initial_balance_col_index = coly+self.get_total_fixed_col_count()+4
                    total_credit_col_index = coly+self.get_total_fixed_col_count()+4+self.get_specific_column_count()
                    total_debit_col_index = coly+self.get_total_fixed_col_count()+4+self.get_specific_column_count()*2
                    if self.see_sales_detail:
                        total_debit_col_index += len(tmp_whs)*self.get_specific_column_count()
                    total_other_debit_col_index = total_debit_col_index + 1 + self.get_specific_column_count()
                    
                    # Эхний үлдэгдэл  /агуулахгүй/
                    sheet = self.with_context({'by_second_uom':True, 'second_uom_key':'total_init_qty_by_second_uom'}).set_specific_columns(sheet, result, rowx, coly+self.get_total_fixed_col_count()+4, result['total_init_qty'], format_number_right_bordered, purchase_price_col_index)
                    # Нийт орлого
                    sheet = self.with_context({'by_second_uom':True, 'second_uom_key':'total_credit_quant_by_second_uom'}).set_specific_columns(sheet, result, rowx, coly+self.get_total_fixed_col_count()+4+self.get_specific_column_count(), result['total_credit_quant'], format_number_right_bordered, purchase_price_col_index)
                    # Нийт борлуулалтын дүн
                    coly += (self.get_total_fixed_col_count()+4+self.get_specific_column_count()*2)
                    if self.see_sales_detail:
                        coly += len(tmp_whs)*self.get_specific_column_count()
                    sheet = self.with_context({'by_second_uom':True, 'second_uom_key':'total_sales_debit_quant_by_second_uom'}).set_specific_columns(sheet, result, rowx, coly, result['total_sales_debit_quant'], format_number_right_bordered, purchase_price_col_index)
                    # Бусад зарлага
                    coly += (self.get_specific_column_count() + 1)
                    sheet = self.with_context({'by_second_uom':True, 'second_uom_key':'total_other_debit_quant_by_second_uom'}).set_specific_columns(sheet, result, rowx, coly, result['total_other_debit_quant'], format_number_right_bordered, purchase_price_col_index)
                    # Эцсийн үлдэгдэл /агуулахгүй/
                    coly += self.get_specific_column_count()
                    quant_formula = "=+" + self.get_column_name_for_calculate(initial_balance_col_index) + str(rowx+1) + \
                                     "+" + self.get_column_name_for_calculate(total_credit_col_index) + str(rowx+1) + \
                                     "-" + self.get_column_name_for_calculate(total_debit_col_index) + str(rowx+1) + \
                                     "-" + self.get_column_name_for_calculate(total_other_debit_col_index) + str(rowx+1)
                    second_uom_formula = "=+" + self.get_column_name_for_calculate(initial_balance_col_index+1) + str(rowx+1) + \
                                     "+" + self.get_column_name_for_calculate(total_credit_col_index+1) + str(rowx+1) + \
                                     "-" + self.get_column_name_for_calculate(total_debit_col_index+1) + str(rowx+1) + \
                                     "-" + self.get_column_name_for_calculate(total_other_debit_col_index+1) + str(rowx+1)
                    
                    sheet = self.with_context({'by_second_uom':True, 'is_residual':True, 'second_uom_formula': second_uom_formula}).set_specific_columns(sheet, result, rowx, coly, quant_formula, format_number_right_bordered, purchase_price_col_index)
#                     sheet = self.set_specific_columns(sheet, result, rowx, coly, result['total_lasts_quant'], format_number_right_bordered, purchase_price_col_index)
                        
                    if total_product_count > 1:
                        current_init_qty = 0
                        current_credit_qty = 0
                        current_sales_debit_qty = 0
                        current_other_debit_qty = 0
                      
                coly = 0
                # Борлуулалтын зарлага /Агуулах бүрээр/
                if self.see_sales_detail:
                    filled_sales_wh_ids.append(result['wh_id'])
                    wh_start_coly_s = coly+self.get_total_fixed_col_count()+4 + self.get_specific_column_count()*2
                    sheet = self.with_context({'by_second_uom':True, 'second_uom_key': 'wh_sales_debit_quant_by_second_uom'}).set_specific_columns(sheet, result, rowx, wh_start_coly_s + tmp_warehouses_dict["wh_"+str(result['wh_id'])]*self.get_specific_column_count(), result['wh_sales_debit_quant'], format_number_right_bordered, purchase_price_col_index)    
                # Эцсийн үлдэгдэл /Агуулах бүрээр/
                if self.see_residual_detail:
                    filled_residual_wh_ids.append(result['wh_id'])
                    wh_start_coly_r = coly + self.get_total_fixed_col_count() + 4 + self.get_specific_column_count()*5 + 1
                    wh_start_coly_r += len(tmp_whs)*self.get_specific_column_count() if self.see_sales_detail else 0
                    sheet = self.with_context({'by_second_uom':True, 'second_uom_key': 'wh_lasts_quant_by_second_uom'}).set_specific_columns(sheet, result, rowx, wh_start_coly_r + tmp_warehouses_dict["wh_"+str(result['wh_id'])]*self.get_specific_column_count(), result['wh_lasts_quant'], format_number_right_bordered, purchase_price_col_index)
                
                if results[-1] == result and self.group_type != 'no_group' and current_category_sub_product_row != -1:
                    total_col_count = self.get_total_fixed_col_count() + 5 + self.get_specific_column_count()*5
                    total_col_count += len(tmp_whs)*self.get_specific_column_count() if self.see_sales_detail else 0
                    total_col_count += len(tmp_whs)*self.get_specific_column_count() if self.see_residual_detail else 0   
                    for i in range(4 + self.get_total_fixed_col_count(), total_col_count, 1):
                        sheet.write(current_category_sub_product_row, i, self.get_sum_formula_in_not_formula(current_category_sub_product_row+2, current_category_sub_product_row+1+current_category_sub_product_count, i), format_column_subh_number)
                        
                if results[-1] == result:
                    # Үлдсэн борлуулалтын зарлага/эцсийн үлдэгдлийн агуулахуудыг 0 утгаар дүүргэх
                    sheet = self.set_empty_wh_values(sheet, result, rowx, tmp_whs, filled_sales_wh_ids, filled_residual_wh_ids, tmp_warehouses_dict, format_number_right_bordered, purchase_price_col_index)
        
        # build content footer
        rowx += 1
        sheet.write(rowx, 0, total_product_count, format_dd_right_bordered_footer)
        sheet.merge_range(rowx, 1, rowx, self.get_total_fixed_col_count()+3, "", format_column_footer_number)
        sum_footer_index = self.get_total_fixed_col_count()+5*self.get_specific_column_count()+5
        if self.see_sales_detail:
            sum_footer_index += len(tmp_whs)*self.get_specific_column_count()
        if self.see_residual_detail:
            sum_footer_index += len(tmp_whs)*self.get_specific_column_count()
        for i in range((self.get_total_fixed_col_count()+4), sum_footer_index, 1):
            if len(sub_row_indexes) == 0:
                sheet.write_formula(rowx, i, self.get_sum_formula(startx+1, rowx, i), format_column_footer_number)
            else:
                sum_formula_row_by_row = "="
                for row_index in sub_row_indexes:
                    sum_formula_row_by_row += ("+" + self.get_column_name_for_calculate(i) + str(row_index+1))
                sheet.write_formula(rowx, i, sum_formula_row_by_row, format_column_footer_number)
        
        # build xlsx footer
        sheet = self.build_footer(sheet, rowx, format_text_left)
        
        book.close()
        
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report() 