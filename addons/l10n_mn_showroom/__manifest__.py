# -*- coding: utf-8 -*-

{
    "name": "Үзүүлэнгийн бүртгэл",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "description": """Үзүүлэнгийн бүртгэл""",
    "website": True,
    "category": "sale",
    "depends": ['l10n_mn_sale', 'l10n_mn_report'],
    "init": [],
    "data": [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/presentation_record_view.xml',
        'reports/showroom_report_views.xml',
    ],
    "demo_xml": [
    ],
    "active": False,
    "installable": True,
}
