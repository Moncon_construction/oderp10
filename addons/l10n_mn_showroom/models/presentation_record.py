# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models

class PresentationRecord(models.Model):
    _name = 'presentation.record'
    _rec_name = "presentation_name"
    _description = "Presentation Code"

    presentation_image = fields.Binary('Presentation Image')
    presentation_name = fields.Char('Presentation Name', required=True)
    presentation_code = fields.Char('Presentation Code', required=True)
    presentation_down_date = fields.Date('Presentation Down Date', required=True)
    date_of_presentation = fields.Date('Date of presentation')
    presentation_warehouse_id = fields.Many2one('stock.warehouse', 'Presentation Warehouse')
    presentation_location = fields.Char('Presentation Location')
    presentation_price = fields.Float('Presentation Price')
    description = fields.Char('Description')
    presentation_product_ids = fields.One2many('presentation.product', 'presentation_id', string='Presentation Product')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)

class PresentationProduct(models.Model):
    _name = 'presentation.product'
    _description = "Product"

    presentation_id = fields.Many2one('presentation.record')
    product_id = fields.Many2one('product.product', 'Product', required=True)
    qty = fields.Integer('Qty')