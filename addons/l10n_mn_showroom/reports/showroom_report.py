# -*- coding: utf-8 -*-

from io import BytesIO
import base64
import time
from datetime import datetime
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from odoo import api, fields, models, _
import pytz
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class ShowroomReport(models.TransientModel):
    _name = 'showroom.report'
    _description = "Showroom Report"

    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env['res.company']._company_default_get('showroom.report'))
    date_from = fields.Date("Start Date", required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    date_to = fields.Date("End Date", required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    warehouse_ids = fields.Many2many('stock.warehouse', 'showroom_warehouse', 'report_id', 'warehouse_id', string="Warehouses")
    categ_ids = fields.Many2many('product.category', 'showroom_category', 'report_id', 'category_id', string="Category")

    def _solve_single_value(self, list):
        if len(list) == 0:
            list.append(-1)
        if len(list) < 2:
            list.append(-1)
        return list

    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Showroom Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        # Custom for standart
        format_name = {
            'font_name': 'Times New Roman',
            'font_size': 14,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter'
        }
        # create formats
        format_content_text_footer = {
            'font_name': 'Times New Roman',
            'font_size': 10,
            'align': 'center',
            'valign': 'vcenter',
        }
        format_group_center = {
            'font_name': 'Times New Roman',
            'font_size': 10,
            'align': 'center',
            'border': 1,
            'valign': 'vcenter',
        }

        format_sub_title = {
            'font_name': 'Times New Roman',
            'font_size': 10,
            'bold': True,
            'align': 'left',
            'valign': 'vcenter',
            'border': 1,
            'bg_color': '#CFE7F5',
        }
        format_sub_title_center = {
            'font_name': 'Times New Roman',
            'font_size': 10,
            'bold': True,
            'align': 'center',
            'valign': 'vcenter',
            'border': 1,
            'bg_color': '#CFE7F5',
        }
        format_group_center = book.add_format(format_group_center)
        format_name = book.add_format(format_name)
        format_sub_title = book.add_format(format_sub_title)
        format_sub_title_center = book.add_format(format_sub_title_center)
        format_content_text_footer = book.add_format(format_content_text_footer)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_title_small = book.add_format(ReportExcelCellStyles.format_title_small)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)


        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('showroom_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 0

        # compute column
        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 25)
        sheet.set_column('C:C', 15)
        sheet.set_column('D:D', 15)
        sheet.set_column('E:E', 20)
        sheet.set_column('F:F', 15)
        sheet.set_column('G:G', 15)
        sheet.set_column('H:H', 15)
        sheet.set_column('I:I', 15)
        sheet.set_column('J:J', 15)
        sheet.set_column('K:K', 15)
        sheet.set_column('L:L', 15)
        sheet.set_column('M:M', 15)
        sheet.set_column('N:N', 15)
        sheet.set_column('O:O', 15)
        sheet.set_column('P:P', 15)
        sheet.set_column('Q:Q', 15)
        sheet.set_column('R:R', 15)
        sheet.set_row(3, 40)

        # rowx += 2
        col = 0
        start_month = int(self.date_from[5:7])
        end_month = int(self.date_to[5:7])
        year = int(self.date_to[0:4])
        ncol = 7 - start_month + end_month

        local = pytz.UTC
        if self.env.user.tz:
            local = pytz.timezone(self.env.user.tz)

        sheet.merge_range(rowx, 0, rowx, 2, '%s' % (self.company_id.name), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 1, rowx, ncol + 1, report_name, format_name)

        rowx += 1
        # create name
        sheet.merge_range(rowx, 0, rowx, 13, _(u'Duration: %s - %s') % (
        pytz.utc.localize(datetime.strptime(self.date_from, '%Y-%m-%d')).astimezone(local).strftime('%Y-%m-%d'),
        pytz.utc.localize(datetime.strptime(self.date_to, '%Y-%m-%d')).astimezone(local).strftime('%Y-%m-%d')),format_filter)
        rowx += 1
        sheet.merge_range(rowx, col, rowx + 1, col, _('№'), format_title_small)
        sheet.merge_range(rowx, col + 1, rowx + 1, col + 1, _('Showroom Name'), format_title_small)
        sheet.merge_range(rowx, col + 2, rowx + 1, col + 2, _('Showroom Warehouse'), format_title_small)
        sheet.merge_range(rowx, col + 3, rowx + 1, col + 3, _('Default Code'), format_title_small)
        sheet.merge_range(rowx, col + 4, rowx + 1, col + 4, _('Product Name'), format_title_small)
        sheet.merge_range(rowx, col + 5, rowx + 1, col + 5, _('Presentation Date'), format_title_small)
        sheet.merge_range(rowx, col + 6, rowx + 1, col + 6, _('Presentation Qty'), format_title_small)
        if start_month == end_month:
            sheet.write(rowx, col + 7, _('Sale'), format_title_small)
        else:
            sheet.merge_range(rowx, col + 7, rowx, ncol, _('Sale'), format_title_small)
        i = 0
        for month in range(ncol-6):
            sheet.write(rowx + 1, 7 + i, str(year) + '/' + str(start_month + i), format_title_small)
            i += 1
        sheet.merge_range(rowx, ncol + 1, rowx + 1, ncol + 1, _('Total'), format_title_small)
        where = ''
        if len(self.warehouse_ids) > 0:
            warehouses = self.warehouse_ids.ids
            where += ' AND p.presentation_warehouse_id in (' + ','.join(map(str, warehouses)) + ') '

        if len(self.categ_ids) > 0:
            categs = self.categ_ids.ids
            where += ' AND pt.categ_id in (' + ','.join(map(str, categs)) + ') '


        self._cr.execute(
            "SELECT p.presentation_name, p.presentation_code, p.presentation_down_date, p.date_of_presentation, w.name as wname, pp.qty, por.id as pid, "
            "pt.default_code, pt.name as pname, pc.id as cid, pc.name as cname  "
            "FROM presentation_product pp "
            "LEFT JOIN presentation_record p ON pp.presentation_id = p.id "
            "LEFT JOIN stock_warehouse w ON p.presentation_warehouse_id = w.id "
            "LEFT JOIN product_product por ON pp.product_id = por.id "
            "LEFT JOIN product_template pt ON por.product_tmpl_id = pt.id "
            "LEFT JOIN product_category pc ON pc.id = pt.categ_id "
            "WHERE p.date_of_presentation >= %s AND p.date_of_presentation <= %s AND p.company_id = %s " + where +
            "GROUP BY pc.id, pc.name,por.id, p.presentation_name, p.presentation_code,  p.presentation_down_date,pt.name, p.date_of_presentation, w.name,pp.qty , pt.default_code "
            "ORDER BY pc.id, pc.name, p.date_of_presentation ", (self.date_from, self.date_to, self.company_id.id))
        lines = self._cr.dictfetchall()

        total_count = total_qty = 0
        rowx += 2
        total_so_qty = 0
        p_name = ''
        warehouse = ''
        rowd = rowx
        rowlist = []
        if lines:
            category_id = False
            product_id = False
            qty = 0
            counter = 1
            for line in lines:
                # Эхний ангиллыг зурна
                if not category_id:
                    sheet.merge_range(rowx, 0, rowx, 5, _(u'Category') + u": %s" % line['cname'], format_sub_title)
                    sheet.write_formula(rowx, 6,'{=SUM(' + xl_rowcol_to_cell(rowx+1, 6) + ':' + xl_rowcol_to_cell(rowx + 1, 6) + ')}',format_sub_title_center)
                    i = 0
                    for month in range(ncol - 6):
                        sheet.write_formula(rowx, 7+i,'{=SUM(' + xl_rowcol_to_cell(rowx + 1, 7+i) + ':' + xl_rowcol_to_cell(rowx + 1, 7+i) + ')}', format_sub_title_center)
                        i += 1
                    sheet.write_formula(rowx, ncol + 1,'{=SUM(' + xl_rowcol_to_cell(rowx, 7) + ':' + xl_rowcol_to_cell(rowx,ncol) + ')}',format_sub_title_center)

                    rowd = rowx
                    rowx += 1
                # Дараагийн ангиллыг зурна
                elif category_id and category_id != line['cid']:
                    sheet.merge_range(rowx, 0, rowx, 5, _(u'Category') + u": %s" % line['cname'], format_sub_title)
                    rowd = rowx
                    rowx += 1
                if product_id != line['pid']:
                    warehouse = line['wname']
                    p_name = line['presentation_name'] + '[' + line['presentation_code'] + ']'
                    qty = int(line['qty'])
                    sheet.write(rowx, 0, counter, format_content_number)
                    sheet.write(rowx, 1, p_name, format_content_text)
                    sheet.write(rowx, 2, warehouse, format_content_text)
                    sheet.write(rowx, 3, line['default_code'], format_group_center)
                    sheet.write(rowx, 4, line['pname'], format_group_center)
                    sheet.write(rowx, 5, line['presentation_down_date'], format_group_center)
                    sheet.write(rowx, 6, qty, format_content_float)
                    i = 0

                    # Батлагдсан борлуулалтын захиалгуудыг тоолно.
                    m = start_month
                    total_count = 0
                    for month in range(ncol - 6):
                        self._cr.execute(
                            "SELECT SUM(sl.qty_delivered) AS qty_delivered "
                            "FROM sale_order_line sl "
                            "LEFT JOIN sale_order s ON sl.order_id = s.id "
                            "WHERE EXTRACT(month FROM s.confirmation_date) = %s AND s.company_id = %s AND s.state in ('done', 'sale') AND sl.product_id = %s ",
                            (m, self.company_id.id, line['pid']))
                        so_qty = self._cr.dictfetchall()
                        m += 1
                        total_count += so_qty[0]['qty_delivered'] if so_qty[0]['qty_delivered'] else 0
                        total_so_qty += so_qty[0]['qty_delivered'] if so_qty[0]['qty_delivered'] else 0
                        sheet.write(rowx, 7 + i, so_qty[0]['qty_delivered'] if so_qty[0]['qty_delivered'] else 0, format_content_float)
                        i += 1
                    sheet.write(rowx, ncol + 1, total_count, format_group_center)

                    total_qty += line['qty']
                    product_id = line['pid']
                    rowq = rowx
                else:
                    if warehouse != line['wname']:
                        warehouse = warehouse + ', ' + line['wname']
                    p_name = p_name + ', ' + line['presentation_name'] + '[' + line['presentation_code'] + ']'
                    qty += line['qty']
                    total_qty += line['qty']
                    sheet.write(rowq, 1, p_name, format_content_text)
                    sheet.write(rowq, 2, warehouse, format_content_text)
                    sheet.write(rowq, 6, qty, format_content_float)
                    rowx = rowq
                sheet.write_formula(rowd, 6,'{=SUM(' + xl_rowcol_to_cell(rowd + 1, 6) + ':' + xl_rowcol_to_cell(rowx , 6) + ')}',format_sub_title_center)
                i = 0
                for month in range(ncol - 6):
                    sheet.write_formula(rowd, 7 + i,'{=SUM(' + xl_rowcol_to_cell(rowd + 1, 7+i) + ':' + xl_rowcol_to_cell(rowx,7+i) + ')}',format_sub_title_center)
                    i += 1
                sheet.write_formula(rowd, ncol + 1,'{=SUM(' + xl_rowcol_to_cell(rowd, 7) + ':' + xl_rowcol_to_cell(rowd,ncol) + ')}',format_sub_title_center)

                rowx += 1
                counter += 1
                category_id = line['cid']
                if rowd not in rowlist:
                    rowlist.append(rowd)

        # Нийт дүнг олох хэсэг
        sheet.merge_range(rowx, 0, rowx, 5, _('Total'), format_title_small)
        sheet.write(rowx, 6, total_qty, format_title_small)

        i = 0
        for month in range(ncol - 6):
            formula = '{=SUM('
            for r in rowlist:
                formula = formula + xl_rowcol_to_cell(r, 7 + i) + ','
            formula = formula[:-1] + ')}'
            sheet.write_formula(rowx, 7 + i, formula, format_title_small)
            i += 1
        sheet.write(rowx, ncol + 1, total_so_qty, format_title_small)

        rowx += 3
        sheet.merge_range(rowx, 1, rowx, ncol + 1, _('Made by:') + '........................' + '/' + self.env.user.name + '/', format_content_text_footer)
        rowx += 1
        sheet.merge_range(rowx, 1, rowx, ncol + 1, _('Check by:') + '........................' + '/' + '........................' + '/', format_content_text_footer)

        sheet.set_zoom(100)
        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report()