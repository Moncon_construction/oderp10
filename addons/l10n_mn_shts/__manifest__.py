# -*- coding: utf-8 -*-
# Part of OdERP. See LICENSE file for full copyright and licensing details.
{
    "name" : "Sinchi 2nd uom",
    "version" : "1.0",
    "author" : "Asterisk Technologies LLC",
    "description": """
        Sinchi 2nd uom
    """,
    "website" : "http://www.asterisk-tech.mn/",
    "category" : "Generic Modules/Others",
    "depends" : ['l10n_mn_sale_stock', 'l10n_mn_purchase', 'l10n_mn_product_expense', 'l10n_mn_sale_plan'],
    "data" : [
        'views/product_create_view.xml',
        'views/sale_order_view.xml',
        'views/product_template_view.xml',
        'views/stock_picking_view.xml',
        'views/purchase_order_view.xml',
        'views/stock_transit_order_view.xml',
        'views/product_expense_view.xml',
        'views/stock_inventory_view.xml',
        'views/sale_plan_view.xml',
    ],
    "installable": True,
}