# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2016 Asterisk Technologies LLC Co.,ltd
#    (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462
#
##############################################################################

import logging
import base64
import xlrd
from odoo import fields, models, _  # @UnresolvedImport
from tempfile import NamedTemporaryFile
from odoo.exceptions import UserError, ValidationError  # @UnresolvedImport

_logger = logging.getLogger(__name__)

class product_create_tool(models.TransientModel):
    _inherit = 'product.create.tool'
    '''
        Бараа материалын эхний үлдэгдлийг Excel файлаас импортлох
    '''

    def process(self):
        _logger = logging.getLogger('jacara')
        wiz = self.browse(self.id)

        try:
            fileobj = NamedTemporaryFile('w+', delete=False)
            fileobj.write(base64.decodestring(wiz.data_file))
            fileobj.seek(0)
            file_name = fileobj.name
            book = xlrd.open_workbook(file_name)
        except:
            raise UserError(_('Error loading data file. \
                                \ Please try again!'))

        sheet = book.sheet_by_index(0)
        nrows = sheet.nrows
        rowi = 1
        data = []
        locations = []
        exists = []
        prod_temp = []
        product_obj = self.env['product.product']
        inventory_obj = self.env['stock.inventory']
        inventory_line_obj = self.env['stock.inventory.line']
        prodlot_obj = self.env['stock.production.lot']
        locatioin_obj = self.env['stock.location']
        history_obj = self.env['product.price.history']
        product_temp = self.env['product.template']

        while rowi < nrows:
            row = sheet.row(rowi)
            location_code = row[0].value or False
            prod_name = row[1].value or False
            prod_code = row[2].value
            qty = row[3].value
            second_qty = row[4].value
            cost = row[5].value
            price = row[6].value
            bar_code = row[7].value
            track_lot = row[8].value

            data.append((location_code, prod_name, prod_code,
                         qty,second_qty, cost, price, track_lot))
            rowi += 1
            tracking_type = 'None'
            if wiz.product_code_type == 'barcode':
                exists = product_obj.search([('barcode', '=', bar_code)])
            elif wiz.product_code_type == 'default_code':
                exists = product_obj.search([('default_code', '=', prod_code)])
            elif wiz.product_code_type == 'product_name':
                prod_temp = product_temp.search([('name', '=', prod_name)])
                exists = product_obj.search([('product_tmpl_id', '=', prod_temp[0].id)]) if prod_temp else False

            if not location_code:
                raise ValidationError(_('Data error %s row! \n \
                        Location columns must have a value!' % (rowi)))
            if exists:
                prod_id = exists[0]
                if location_code not in locations:
                    Location = locatioin_obj.search(
                        [('barcode', '=', location_code)])
                    if Location and Location.usage == 'internal':
                        inventory_id = inventory_obj.create({
                            'date': wiz.date,
                            'accounting_date': wiz.date,
                            'location_id': Location.id,
                            'is_initial' : True,
                            'name': u'%s /%s -%s эхний үлдэгдэл'
                            % (wiz.warehouse_id.name, Location.name, wiz.date)
                        })
                    elif Location and Location.usage != 'internal':
                        raise ValidationError(_('Data error %s row! \n \
                        %s location type not intenal!' % (rowi, Location.name)))
                    else:
                        raise ValidationError(_('Data error %s row! \n %s \
                        barcode with location not exists!' % (rowi, location_code)))
                    locations.append(location_code)
                '''
                Цуварлын дугаарыг үүсгэхдээ Барааны хөтлөлт
                талбараас хамааруулан шалгаж үүсгэх
                '''
                if cost:
                    prod_id.standard_price = cost
                if price:
                    prod_id.list_price = price
                    
                self.set_wh_standart_price(prod_id, cost)
                
                if prod_id.tracking == 'serial':
                    tracking_type = 'serial'
                elif prod_id.tracking == 'lot':
                    tracking_type = 'lot'
                prodlot = False
                if track_lot and tracking_type == 'None':
                    raise ValidationError(_('Data error %s row! \n %s with code %s \
                    product no tracking!' % (rowi, prod_code, prod_name)))
                elif track_lot and tracking_type == 'serial':
                    exists = prodlot_obj.search(
                        [('product_id', '=', prod_id.id), ('name', '!=', track_lot)])
                    if exists:
                        raise ValidationError(_('Data error %s row! \n %s with code %s \
                        product by unique serial number!' % (rowi, prod_code, prod_name)))
                    else:
                        exists = prodlot_obj.search([
                            ('product_id', '=', prod_id.id),
                            ('name', '=', track_lot)])
                        if exists and len(exists) == 1:
                            prodlot_id = exists[0]
                            prodlot = prodlot_id.id
                        elif exists and len(exists) > 1:
                            raise ValidationError(_('Data error %s row! \n %s with code %s \
                            product by unique serial number!' % (rowi, prod_code, prod_name)))
                        else:
                            prodlot_id = prodlot_obj.create({
                                'product_id': prod_id.id,
                                'name': track_lot,
                                'ref': prod_id.default_code,
                                'create_date': wiz.date})
                            prodlot = prodlot_id.id
                elif track_lot and tracking_type == 'lot':
                    exists = prodlot_obj.search([
                        ('product_id', '=', prod_id.id),
                        ('name', '=', track_lot)])
                    if exists:
                        prodlot_id = exists[0]
                        prodlot = prodlot_id.id
                    else:
                        prodlot_id = prodlot_obj.create({
                            'product_id': prod_id.id,
                            'name': track_lot,
                            'ref': prod_id.default_code,
                            'create_date': wiz.date})
                        prodlot = prodlot_id.id
                inventory_line_obj.create({
                    'location_id': Location.id,
                    'product_id': prod_id.id,
                    'product_uom_id': prod_id.uom_id.id,
                    'product_qty': qty,
                    'second_product_qty':second_qty,
                    'second_uom_id':prod_id.second_uom_id.id,
                    'prod_lot_id': prodlot,
                    'inventory_id': inventory_id.id,
                })
                inventory_id.prepare_inventory()
            else:
                raise ValidationError(_('Data error %s row! \n %s with code %s \
                product not exists!' % (rowi, prod_code, prod_name)))
        return True