# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError

class ProductExpenseLine(models.Model):
    _inherit = 'product.expense.line'
    
    second_uom_id = fields.Many2one('product.uom', 'Second Product UOM', ondelete='cascade', index=True, required=False)
    second_product_qty = fields.Float('Second Product Quantity', required=True)
    
    @api.onchange('product')
    def onchange_name(self):
        self.ensure_one()
        self.name = self.product.name
        self.second_uom_id = self.product.second_uom_id.id
        
class ProductExpense(models.Model):
    _inherit = 'product.expense'
    
    @api.multi
    def create_out_picking(self):
        stock_picking = self.env['stock.picking']
        stock_move = self.env['stock.move']
        stock_picking_type = self.env['stock.picking.type']
        outgoing_stock_picking_type = stock_picking_type.search([('code', '=', 'outgoing'),
                                                                 ('warehouse_id', '=', self.warehouse.id)]);
        outgoing_stock_picking_type_id = None
        des_location = []
        des_location = self.env['stock.location'].search([('scrap_location', '=', True)])

        if outgoing_stock_picking_type:
            for ospt in outgoing_stock_picking_type:
                outgoing_stock_picking_type_id = ospt.id

        for object in self:
            # Данс нь авлага болон өглөг төрөлтэй үед хүргэх захиалгаас ажил гүйлгээ үүсэхдээ харилцагч авч үүсдэг болгох. Шаардах хуудас үүсгэсэн ажилтантай холбоотой харилцагчийг авна.
            if str(object.account.user_type_id.type) in ('receivable', 'payable'):
                values = {'expense': object.id,
                          'origin': object.code,
                          'company_id': object.company.id,
                          'picking_type_id': outgoing_stock_picking_type_id,
                          'location_dest_id': des_location[0].id,
                          'location_id': object.warehouse.lot_stock_id.id,
                          'partner_id': object.employee.address_id.id}
            else:
                values = {'expense': object.id,
                          'origin': object.code,
                          'company_id': object.company.id,
                          'picking_type_id': outgoing_stock_picking_type_id,
                          'location_dest_id': des_location[0].id,
                          'location_id': object.warehouse.lot_stock_id.id}
            stock_picking_object = stock_picking.create(values)
            stock_picking_object.message_post_with_view(
                    'l10n_mn_product_expense.message_link_for_picking_expense',
                    values = {
                        'self': stock_picking_object,
                        'origin': object
                    },
                    subtype_id=self.env.ref('mail.mt_note').id
            )
            if object.expense_line:
                for line in object.expense_line:
                    move_lines_values = {'name': line.name,
                                         'picking_id': stock_picking_object.id,
                                         'product_id': line.product.id,
                                         'product_uom': line.product.product_tmpl_id.uom_id.id,
                                         'second_uom_id': line.product.product_tmpl_id.second_uom_id.id,
                                         'location_id': object.warehouse.lot_stock_id.id,
                                         'location_dest_id': des_location[0].id,
                                         'product_uom_qty': line.quantity,
                                         'second_product_qty': line.second_product_qty}
                    stock_move.sudo().create(move_lines_values)