# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError

class ProductTemplate(models.Model):
    _inherit = 'product.template'
    
    second_uom_id = fields.Many2one('product.uom', 'Second Product UOM', ondelete='cascade', index=True, required=False)