# -*- coding: utf-8 -*-
# Part of OdERP. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, _, api
from odoo.exceptions import UserError, ValidationError


class PurchaseOrderLine(models.Model):
    _inherit = "purchase.order.line"

    second_uom_id = fields.Many2one('product.uom', 'Second Product UOM', ondelete='cascade', index=True, required=False)
    second_product_qty = fields.Float('Second Product Quantity', required=False)

    @api.multi
    def _prepare_stock_moves(self, picking):
        self.ensure_one()
        res = super(PurchaseOrderLine, self)._prepare_stock_moves(picking)
        for move in res:
            move.update({'second_product_qty': self.second_product_qty,
                            'second_uom_id': self.second_uom_id.id})
        return res