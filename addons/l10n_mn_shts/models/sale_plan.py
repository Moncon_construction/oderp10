# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError

class SalePlan(models.Model):
    _inherit = 'sale.plan'
    
    total_plan_second_product_qty = fields.Float('Total Plan Second Product Quantity')
    
class SalePlanLine(models.Model):
    _inherit = 'sale.plan.line'
    
    plan_second_product_qty = fields.Float('Plan Second Product Quantity')
    performance_second_product_qty = fields.Float('Performance Second Product Quantity')
    
class SalePeriodLine(models.Model):
    _inherit = 'sale.period.line'
    
    plan_second_product_qty = fields.Float('Plan Second Product Quantity')
    performance_second_product_qty = fields.Float('Performance Second Product Quantity')