# -*- coding: utf-8 -*-
# Part of OdERP. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, _, SUPERUSER_ID
import time
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, float_round, float_compare

class stock_inventory_line(models.Model):
    _inherit = "stock.inventory.line"
    
    second_uom_id = fields.Many2one('product.uom', 'Second Product UOM', ondelete='cascade', index=True)
    second_product_qty = fields.Float('Second Product Quantity')

    def _resolve_inventory_line(self, inventory_line):
        if self._context is None:
            context = {}
        else:
            context = self._context

        stock_move_obj = self.env['stock.move']
        quant_obj = self.env['stock.quant']
        self._fixup_negative_quants(inventory_line)

        # Тооллогоор 2 хэмжих нэгжийн аль нэг нь өөрчлөгдөхөд л агуулахын хөдөлгөөн үүсгэнэ
        if float_compare(inventory_line.theoretical_qty, inventory_line.product_qty, precision_rounding=inventory_line.product_id.uom_id.rounding) == 0 and float_compare(inventory_line.second_theoretical_qty, inventory_line.second_product_qty, precision_rounding=inventory_line.product_id.second_uom_id.rounding) == 0:
            return False
        diff = inventory_line.theoretical_qty - inventory_line.product_qty
        
        #New code 2догч хэмжих нэгжийн хувьд өөрчлөлтийг хадгалж байна
        diff2 = inventory_line.second_theoretical_qty - inventory_line.second_product_qty
        
        #each theorical_lines where difference between theoretical and checked quantities is not 0 is a line for which we need to create a stock move
        inventory_location_id = inventory_line.product_id.property_stock_inventory.id
        if diff < 0:  # found more than expected
            vals = self._get_move_values( inventory_line, abs(diff), inventory_location_id, inventory_line.location_id.id)
        else:
            vals = self._get_move_values( inventory_line, abs(diff), inventory_line.location_id.id, inventory_location_id)

        move_id = stock_move_obj.create( vals, context=context)
        move = stock_move_obj.browse( move_id, context=context)
        if diff > 0:
            domain = [('qty', '>', 0.0), ('package_id', '=', inventory_line.package_id.id), ('lot_id', '=', inventory_line.prod_lot_id.id), ('location_id', '=', inventory_line.location_id.id)]
            preferred_domain_list = [[('reservation_id', '=', False)], [('reservation_id.inventory_id', '!=', inventory_line.inventory_id.id)]]
            quants = quant_obj.quants_get_prefered_domain( move.location_id, move.product_id, move.product_qty, domain=domain, prefered_domain_list=preferred_domain_list, restrict_partner_id=move.restrict_partner_id.id)
            quant_obj.quants_reserve(quants, move)
        elif inventory_line.package_id:
            stock_move_obj.action_done(move_id)
            quants = [x.id for x in move.quant_ids]
            quant_obj.write(cr, SUPERUSER_ID, quants, {'package_id': inventory_line.package_id.id}, context=context)
            res = quant_obj.search( [('qty', '<', 0.0), ('product_id', '=', move.product_id.id),
                                    ('location_id', '=', move.location_dest_id.id), ('package_id', '!=', False)], limit=1)
            if res:
                for quant in move.quant_ids:
                    if quant.location_id.id == move.location_dest_id.id: #To avoid we take a quant that was reconcile already
                        quant_obj._quant_reconcile_negative(quant, move)

        #New code Дээр үүсгэсэн stock_move дээрх second_product_qty ийн утгыг өөрчлөлтөөр хадгалж байна
        #second_product_qty_show нь хэрэглэгчид stock_move харуулахдаа багассан бол сөрөг ихэссэн бол эерэг дүнгээр харуулах зорилготой
        stock_move_obj = self.env['stock.move']
        if move_id:
            # Үндсэн хэмжих нэгжийн хувьд ихэссэн тохиолдолд 2догч хэмжих нэгжийн дүн зөрж тооцоолж байсан учир -1 ээр үржүүлж өгсөн
            if diff < 0:
                stock_move_obj.write( [move_id],
                                     {'second_uom_id': inventory_line.second_uom_id.id,
                                      'second_product_qty': -1*diff2,
                                      'second_product_qty_show': -1*diff2})
            else:
                stock_move_obj.write([move_id],
                                     {'second_uom_id': inventory_line.second_uom_id.id,
                                      'second_product_qty': diff2,
                                      'second_product_qty_show': -1*diff2})
        return move_id


class product_template(models.Model):
    _inherit = "product.template"

    second_uom_id = fields.Many2one('product.uom', 'Second Product UOM', ondelete='cascade', index=True, required=False)

class sale_order_line(models.Model):
    _inherit = "sale.order.line"

    second_uom_id = fields.Many2one('product.uom', 'Second Product UOM', ondelete='cascade', index=True, required=False)
    second_product_qty = fields.Float('Second Product Quantity', required=True)
    second_unit_price = fields.Float('Second Unit Price')
    
    def check_product_availability(self,  product_id, warehouse_id):
        warehouse_obj = self.env['stock.warehouse']
        warehouse = warehouse_obj.browse(warehouse_id)
        locations = self.env['stock.location'].search( [('usage', '=', 'internal'),
                                                                     ('location_id', 'child_of', [warehouse.view_location_id.id])])

        date_start = time.strftime("%Y-%m-%d %H:%M:%S")
        cr.execute("(select m.product_id, coalesce(sum(m.second_product_qty * u.factor), 0) as q, coalesce(sum((m.second_product_qty*u.factor)*m.price_unit), 0) as c "
                   "from stock_move m "
                   "join product_uom u on (u.id=m.product_uom) "
                   "where m.state = 'done' and "
                   "m.date <= %s and "
                   "m.product_id = %s and m.location_dest_id = %s and m.location_id != %s "
                   "group by m.product_id) UNION "
                   "(select m.product_id, -coalesce(sum(m.second_product_qty * u.factor), 0) as q, -coalesce(sum((m.second_product_qty*u.factor)*m.price_unit), 0) as c "
                   "from stock_move m "
                   "join product_uom u on (u.id=m.product_uom) "
                   "where m.state = 'done' and "
                   "m.date <= %s and "
                   "m.product_id = %s and m.location_id = %s and m.location_dest_id != %s "
                   "group by m.product_id)",
                   (date_start, product_id, locations[0], locations[0],
                    date_start, product_id, locations[0], locations[0]))

        fetched = cr.fetchall()
        qty_sum = 0.0
        if fetched != []:
            for pid, qty, amount in fetched:  # @UnusedVariable
                qty_sum += qty

        return qty_sum


    def product_id_change(self,  ids, pricelist, product, qty=0,
                          uom=False, qty_uos=0, uos=False, name='', partner_id=False,
                          lang=False, update_tax=True, date_order=False, packaging=False, fiscal_position=False, flag=False,
                          warehouse_id=False, sale_category_id=False, tax_id=False, expire_range=False, payment_term=False, second_product_qty=False, second_unit_price=False):
        context = self._context or {}
        lang = lang or context.get('lang', False)
        if not partner_id:
            raise except_orm(_('No Customer Defined!'), _('Before choosing a product,\n select a customer in the sales form.'))
        warning = False
        product_uom_obj = self.env['product.uom']
        partner_obj = self.env['res.partner']
        product_obj = self.env['product.product']
        context = {'lang': lang, 'partner_id': partner_id}
        partner = partner_obj.browse( partner_id)
        lang = partner.lang
        context_partner = {'lang': lang, 'partner_id': partner_id}

        if not product:
            return {'value': {'th_weight': 0,
                              'product_uos_qty': qty, 'expire_range': False, 'product_uos': False}, 'domain': {'product_uom': [],
                                                                                                               'product_uos': []}}
        if not date_order:
            date_order = time.strftime(DEFAULT_SERVER_DATE_FORMAT)

        result = {}
        warning_msgs = ''
        product_obj = product_obj.browse(product)

        uom2 = False
        if uom:
            uom2 = product_uom_obj.browse(uom)
            if product_obj.uom_id.category_id.id != uom2.category_id.id:
                uom = False
        if uos:
            if product_obj.uos_id:
                uos2 = product_uom_obj.browse(uos)
                if product_obj.uos_id.category_id.id != uos2.category_id.id:
                    uos = False
            else:
                uos = False

        fpos = False
        if not fiscal_position:
            fpos = partner.property_account_position or False
        else:
            fpos = self.env['account.fiscal.position'].browse(fiscal_position)
        if update_tax:  # The quantity only have changed
            taxes = product_obj.taxes_id
            if not taxes:
                taxes = product_obj.company_id.sales_taxes_id
            result['tax_id'] = self.env['account.fiscal.position'].map_tax(fpos, product_obj.taxes_id)

        if not flag:
            result['name'] = self.env['product.product'].name_get([product_obj.id])[0][1]
            if product_obj.description_sale:
                result['name'] += '\n' + product_obj.description_sale
        domain = {}
        if (not uom) and (not uos):
            result['product_uom'] = product_obj.uom_id.id
            if product_obj.uos_id:
                result['product_uos'] = product_obj.uos_id.id
                result['product_uos_qty'] = qty * product_obj.uos_coeff
                uos_category_id = product_obj.uos_id.category_id.id
            else:
                result['product_uos'] = False
                result['product_uos_qty'] = qty
                uos_category_id = False
            result['th_weight'] = qty * product_obj.weight
            domain = {'product_uom':
                          [('category_id', '=', product_obj.uom_id.category_id.id)],
                      'product_uos':
                          [('category_id', '=', uos_category_id)]}
        elif uos and not uom:  # only happens if uom is False
            result['product_uom'] = product_obj.uom_id and product_obj.uom_id.id
            result['product_uom_qty'] = qty_uos / product_obj.uos_coeff
            result['th_weight'] = result['product_uom_qty'] * product_obj.weight
        elif uom:  # whether uos is set or not
            default_uom = product_obj.uom_id and product_obj.uom_id.id
            q = product_uom_obj._compute_qty( uom, qty, default_uom)
            if product_obj.uos_id:
                result['product_uos'] = product_obj.uos_id.id
                result['product_uos_qty'] = qty * product_obj.uos_coeff
            else:
                result['product_uos'] = False
                result['product_uos_qty'] = qty
            result['th_weight'] = q * product_obj.weight  # Round the quantity up

        if not uom2:
            uom2 = product_obj.uom_id

        if product_obj.second_uom_id:
            result['second_uom_id'] = product_obj.second_uom_id.id

        frm_cur = self.env.user.company_id.currency_id.id
        to_cur = self.env['product.pricelist'].browse([pricelist])[0].currency_id.id
        if product:
            standard_price_id = self.pool['product.warehouse.standard.price'].search( [('product_id', '=', product_obj.product_tmpl_id.id), ('warehouse_id', '=', warehouse_id)])
            standard_price_obj = self.pool['product.warehouse.standard.price'].browse( standard_price_id)
            standard_price = standard_price_obj.standard_price
            to_uom = result.get('product_uom', uom)
            if to_uom != product_obj.uom_id.id:
                standard_price = self.pool['product.uom']._compute_price( product_obj.uom_id.id, standard_price, to_uom)
            ctx = context.copy()
            ctx['date'] = date_order
            price = self.env['res.currency'].compute( frm_cur, to_cur, standard_price, round=False)
            result['purchase_price'] = price
        # get unit price

        if not pricelist:
            warn_msg = _('You have to select a pricelist or a customer in the sales form !\n'
                         'Please set one before choosing a product.')
            warning_msgs += _("No Pricelist ! : ") + warn_msg + "\n\n"
        else:
            price = self.env['product.pricelist'].price_get( [pricelist],
                                                                 product, qty or 1.0, partner_id, dict(context.items(),
                                                                                                       uom=uom or result.get('product_uom'),
                                                                                                       warehouse=warehouse_id,
                                                                                                       sale_category_id=sale_category_id,
                                                                                                       expire_range=expire_range,
                                                                                                       payment_term=payment_term,
                                                                                                       date=date_order,
                                                                                                       return_with_discount=True))[pricelist]
            if price[0] is False:
                warn_msg = _("Cannot find a pricelist line matching this product and quantity.\n"
                             "You have to change either the product, the quantity or the pricelist.")

                warning_msgs += _("No valid pricelist line found ! :") + warn_msg + "\n\n"
            else:
                discount = price[2]
                if discount > 0:
                    base = price[0] / (1.0 - ((discount or 0.0) / 100.0))
                else:
                    base = price[0]
                    discount = 0
                result.update({'price_unit': base, 'price_display': base,
                               'discount': discount, 'discount_display': discount,
                               'discount_amt': ((base > price[0] and base - price[0]) or 0) * qty})
        if warning_msgs:
            warning = {
                'title': _('Configuration Error!'),
                'message': warning_msgs
            }

        result['price_unit'] = second_unit_price * second_product_qty / qty
        return {'value': result, 'domain': domain, 'warning': warning}


class sale_order(models.Model):
    _inherit = "sale.order"

    def _prepare_order_line_procurement(self,  order, line, group_id=False):
        vals = super(sale_order, self)._prepare_order_line_procurement( order, line, group_id)
        if line.second_product_qty:
            vals['second_product_qty'] = line.second_product_qty or False
            vals['second_uom_id'] = line.second_uom_id and line.second_uom_id.id or False

        return vals

    def _prepare_procurement_group(self,  order):
        return {'name': order.name, 'note': order.note, 'note': order.note, 'partner_id': order.partner_shipping_id.id}

    def action_wait(self,  ids):
        ''' Борлуулалтын захиалга батлах үед агуулахын үлдэгдлийг шалгана.
        '''
        context = self._context or {}
        uom_obj = self.env['product.uom']
        location_obj = self.env['stock.location']
        product_obj = self.env['product.product']
        partner_obj = self.env['res.partner']

        self.bonus_line_create()

        for o in self:
            prod_dict = {}
            prod_expire_range_dict = {}
            all_prods = {}
            for line in o.order_line:
                if line.product_id and line.product_id.type == 'product':
                    if not line.expire_range:
                        prod_dict.setdefault(line.product_id.id, 0)
                        qty = line.product_uom_qty
                        if line.product_uom.id <> line.product_id.uom_id.id:
                            qty = uom_obj._compute_qty( line.product_uom.id, qty, line.product_id.uom_id.id)
                        prod_dict[line.product_id.id] += qty
                    else:
                        key = '%s-%s' % line.product_id.id, line.expire_range
                        if key not in prod_expire_range_dict:
                            prod_expire_range_dict[key] = {
                                'expire_range': line.expire_range,
                                'product_id': line.product_id.id,
                                'qty': 0
                            }
                        qty = line.product_uom_qty
                        if line.product_uom.id <> line.product_id.uom_id.id:
                            qty = uom_obj._compute_qty( line.product_uom.id, qty, line.product_id.uom_id.id)
                        prod_expire_range_dict[key]['qty'] += qty
                    all_prods[line.product_id.id] = {
                        'name': u'[%s] %s' % (line.product_id.default_code or '-', line.product_id.name),
                        'rounding': line.product_id.uom_id.rounding,
                        'uom_name': line.product_id.uom_id.name}

            location_ids_tuple = location_obj.search( [('location_id', 'child_of', [o.warehouse_id.view_location_id.id]),
                                                               ('usage', '=', 'internal')])

            prod_data = dict([(x['id'], x) for x in product_obj.read( all_prods.keys(), ['qty_available', 'outgoing_qty'],
                                                                     context=dict(context.items(), location=location_ids_tuple))])

            messages = []
            for prod_id, ordering_qty in prod_dict.iteritems():
                available_qty = float_round(prod_data[prod_id]['qty_available'] - prod_data[prod_id]['outgoing_qty'],
                                            precision_rounding=all_prods[prod_id]['rounding'])

                if (ordering_qty - available_qty) > all_prods[prod_id]['rounding']:
                    messages.append(_('You planned to sell %.2f %s %s but there is only %.2f %s stock available in this sales shop!') % \
                                    (ordering_qty, all_prods[prod_id]['uom_name'], all_prods[prod_id]['name'],
                                     available_qty, all_prods[prod_id]['uom_name']))

            for line_data in prod_expire_range_dict.values():
                prod_id = line_data['product_id']
                ordering_qty = line_data['qty']
                prod_data = product_obj.read( prod_id, ['qty_available', 'outgoing_qty'],
                                             context=dict(context.items(), location=location_ids_tuple, expire_range=line_data['expire_range']))

                available_qty = float_round(prod_data['qty_available'] - prod_data['outgoing_qty'],
                                            precision_rounding=all_prods[prod_id]['rounding'])

                if (ordering_qty - available_qty) > all_prods[prod_id]['rounding']:
                    messages.append(_('You planned to sell %.2f %s %s but there is only %.2f %s stock available in this sales shop!') % \
                                    (ordering_qty, all_prods[prod_id]['uom_name'], all_prods[prod_id]['name'],
                                     available_qty, all_prods[prod_id]['uom_name']))

            for line in o.order_line:

                second_qty_available = self.env['sale.order.line'].check_product_availability( line.product_id.id, o.warehouse_id.id)

                if (line.second_product_qty - second_qty_available) > all_prods[prod_id]['rounding']:
                    messages.append(_('You planned to sell %.2f %s %s but there is only %.2f %s stock available in this sales shop!') % \
                                    (line.second_product_qty, line.second_uom_id.name, line.product_id.name, second_qty_available, line.second_uom_id.name))

            # second_future_qty = float_round(self.product_available(product_id)['outgoing_qty'], precision_rounding=uom_rounding)
            # second_qty_available  -=second_future_qty

            if messages:
                all_messages = '\n'.join(messages[:5])
                if len(messages) > 5:
                    all_messages += '\n...'
                raise except_orm(_('Warning !'), all_messages)

            if o.client_order_ref and not o.partner_id.register_number:
                partner_obj.write(cr, SUPERUSER_ID, [o.partner_id.id],
                                  {'register_number': o.client_order_ref}, context=context)
                partner_obj.message_post( [o.partner_id.id],
                                         u'"%s" борлуулалтын захиалга дээр %s регистрийн дугаар бүртгэгдсэн учир %s харилцагчийн регистрийн дугаарыг шинэчлэв.' % \
                                         (o.name, o.client_order_ref, o.partner_id.name))

        return super(sale_order, self).action_wait( ids, context=context)


class purchase_order_line(models.Model):
    _inherit = "purchase.order.line"

    second_uom_id = fields.Many2one('product.uom', 'Second Product UOM', ondelete='cascade', index=True, required=False)
    second_product_qty = fields.Float('Second Product Quantity', required=True)

class purchase_order(models.Model):
    _inherit = "purchase.order"

    def _prepare_order_line_move(self,  order, order_line, picking_id, group_id):
        ''' Худалдан авалтын захиалга дээр 2дахь хэмжих нэгжийн тоо болон хэмжих нэгжийг ирж буй хүргэлтийн мөр
        дээр гаргана.
        '''
        res = super(purchase_order, self)._prepare_order_line_move( order, order_line, picking_id,
                                                                   group_id)
        if order_line.second_product_qty:
            for move_val in res:
                move_val['second_product_qty'] = order_line.second_product_qty
                move_val['second_uom_id'] = order_line.second_uom_id.id

        return res


class procurement_order(models.Model):
    _inherit = "procurement.order"

    second_uom_id = fields.Many2one('product.uom', 'Second Product UOM', ondelete='cascade', index=True, required=False)
    second_product_qty = fields.Float('Second Product Quantity', required=True)
