# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError, ValidationError

class StockInventoryLine(models.Model):
    _inherit = 'stock.inventory.line'
    
    second_uom_id = fields.Many2one('product.uom', 'Second Product UOM', ondelete='cascade', index=True, required=False, default=lambda self: self.env.ref('product.product_uom_litre', raise_if_not_found=True))
    second_product_qty = fields.Float('Second Product Quantity')
    second_theoretical_qty = fields.Float('Second Product Theoretical Quantity', readonly=True, store=True,
                                                  compute='_compute_second_theoretical_qty', digits=dp.get_precision('Product Unit of Measure'))
    
    @api.onchange('product_id')
    def onchange_product(self):
        self.ensure_one()
        res = super(StockInventoryLine, self).onchange_product()
        self.second_uom_id = self.product_id.second_uom_id.id
        return res
    
    @api.onchange('product_id', 'location_id', 'product_uom_id', 'prod_lot_id', 'partner_id', 'package_id')
    def onchange_quantity_context(self):
        self.ensure_one()
        res = super(StockInventoryLine, self).onchange_quantity_context()
        self._compute_second_theoretical_qty()
        self.second_product_qty = self.second_theoretical_qty
        self.second_uom_id = self.product_id.second_uom_id.id
        return res
    
    def _get_move_values(self, qty, location_id, location_dest_id):
        self.ensure_one()
        diff = self.second_product_qty - self.second_theoretical_qty
        vals = super(StockInventoryLine, self)._get_move_values(qty, location_id, location_dest_id)
        vals.update({'second_uom_id': self.second_uom_id.id,
                     'second_product_qty': diff})
        return vals
    
    @api.one
    @api.depends('location_id', 'product_id', 'package_id', 'second_uom_id', 'company_id', 'prod_lot_id', 'partner_id')
    def _compute_second_theoretical_qty(self):
        
        if not self.product_id:
            self.theoretical_qty = 0
            return
        locations = self.env['stock.location'].search([('id', 'child_of', [self.inventory_id.location_id.id])])
        #Орлого
        self._cr.execute("""SELECT sum(second_product_qty) FROM stock_move 
                                WHERE location_dest_id IN %s 
                                AND location_id NOT IN %s 
                                AND company_id = %s
                                AND product_id = %s
                                AND state = 'done'
                                GROUP BY product_id""", (tuple(locations.ids), tuple(locations.ids), self.company_id.id, self.product_id.id))
        in_qty = self.env.cr.fetchone()
        #Зарлага
        self._cr.execute("""SELECT -sum(second_product_qty) FROM stock_move 
                                    WHERE location_dest_id NOT IN %s 
                                    AND location_id IN %s 
                                    AND company_id = %s
                                    AND product_id = %s
                                    AND state = 'done'
                                    GROUP BY product_id""", (tuple(locations.ids), tuple(locations.ids), self.inventory_id.company_id.id, self.product_id.id))

        out_qty = self.env.cr.fetchone()
        if in_qty:
            if in_qty[0] != None:
                if out_qty:
                    if out_qty[0] != None:
                        second_theoretical_qty = in_qty[0] + out_qty[0]
                        self.second_theoretical_qty = second_theoretical_qty
                    else:
                        self.second_theoretical_qty = in_qty[0]
        else:
            self.second_theoretical_qty = 0
        self.second_uom_id = self.product_id.second_uom_id.id
        