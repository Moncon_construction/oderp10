# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError

class StockMove(models.Model):
    _inherit = 'stock.move'
    
    second_uom_id = fields.Many2one('product.uom', 'Second Product UOM', ondelete='cascade', index=True, required=False)
    second_product_qty = fields.Float('Second Product Quantity', required=True)