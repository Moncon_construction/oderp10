# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError

class StockTransitOrderLine(models.Model):
    _inherit = 'stock.transit.order.line'
    
    second_uom_id = fields.Many2one('product.uom', 'Second Product UOM', ondelete='cascade', index=True, required=False)
    second_product_qty = fields.Float('Second Product Quantity', required=True)
    
    @api.multi
    def _prepare_order_line_procurement(self, group_id=False):
        for line in self:
            res = super(StockTransitOrderLine, self)._prepare_order_line_procurement(group_id=line.transit_order_id.procurement_group_id.id)
            res.update({'second_product_qty': line.second_product_qty,
                        'second_uom_id': line.second_uom_id.id})
        return res
    
    @api.multi
    def _prepare_stock_moves(self, picking):
        self.ensure_one()
        res = super(StockTransitOrderLine, self)._prepare_stock_moves(picking)
        for move in res:
            move.update({'second_product_qty': self.second_product_qty,
                            'second_uom_id': self.second_uom_id.id})
        return res
    
    @api.multi
    def _prepare_out_stock_moves(self, picking):
        self.ensure_one()
        res = super(StockTransitOrderLine, self)._prepare_out_stock_moves(picking)
        for move in res:
            move.update({'second_product_qty': self.second_product_qty,
                            'second_uom_id': self.second_uom_id.id})
        return res