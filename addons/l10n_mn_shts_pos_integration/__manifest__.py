# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

{
    'name' : 'POS integration for Sinchi1',
    'version' : '1.1',
    'author' : 'Asterisk Technologies LLC',
    'category' : 'Sale POS',
    'description' : """
""",
    'website': 'http://www.asterisk-tech.mn',
    'depends' : ['stock', 'l10n_mn_hr', 'l10n_mn_shts'],
    'data':[
        'pos_web.xml',
        'security/pos_security.xml',
        'security/ir.model.access.csv',
        'wizard/import_pos_report_view.xml',
        'pos_view.xml',
        
    ],
    'installable': True,
    'auto_install': False,
}