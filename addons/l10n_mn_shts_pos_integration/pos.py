# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
from odoo import models, api, fields, _
from datetime import datetime
from odoo.exceptions import Warning, UserError
from odoo.osv import expression
import base64
from base64 import encode
from tempfile import NamedTemporaryFile
import openerp.netsvc, decimal, base64, os, time, xlrd

class StockMove(models.Model):
    _inherit = 'stock.move'     

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('origin', '=ilike', name + '%'), ('name', operator, name)]
            if operator in expression.NEGATIVE_TERM_OPERATORS:
                domain = ['&', '!'] + domain[1:]
        transactions = self.search(domain + args, limit=limit)
        return transactions.name_get()

class PosInformation(models.Model): 
    _name = 'pos.information'
    _inherit = ['mail.thread','ir.needaction_mixin']

    def _employee_get(self):
        ids = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)])
        if ids:
            return ids[0]
        return False
    
    date = fields.Datetime('Date', required=True, default=datetime.now())
    employee_id = fields.Many2one('hr.employee','Employee', readonly=True, default=_employee_get)
    line_ids = fields.One2many('pos.information.line', 'pos_information_id', 'Pos Info')
    state = fields.Selection([('draft', 'Draft'),
                             ('approved', 'Approved')], 'State', default='draft')

    def approve(self):
        '''Батлах товч дарснаар посын мэдээлэл батлагдсан төлөвт орох бөгөөд 05 тайлан татах болон импортлох товч алга болно.'''
        for pos_info in self:
            #Мөрүүдийг батлагдсан төлөвт оруулснаар мөрүүдийг хуваарилан Батлах товч харагдана'''
            pos_info.line_ids.write({'state': 'approved'})
        self.write({'state': 'approved'})

    def cancel(self):
        '''Цуцлах товч дарснаар посын мэдээлэл болон 05 тайлангийн мөрүүд ноорог төлөвт орж  05 тайлан дахин татах боломжтой болно. Аль 1 мөрийг нь хуваарилсан 
        баримтыг цуцлах боломжгүй'''

        has_assigned_line = False
        for pos_info in self:
            if 'done' in [line.state for line in pos_info.line_ids]:
                raise UserError(_('You cannot cancel pos information that contain assigned line%'))
            else:
                pos_info.line_ids.write({'state': 'draft'})
        self.write({'state': 'draft'})

    def clear_lines(self):
        '''05 болон sales тайлангуудыг импортлох болон татахад өмнө нь тухайн баримт дээр үүссэн байгаа бүх мөрийг устгах'''
        line_obj = self.env['pos.information.line']
        for pos_info in self:
#             line_obj.unlink([line.id for line in pos_info.line_ids])
            pos_info.line_ids.unlink()

    def create_lines(self, report05, sales):
        '''05 болон sales тайлангуудыг импортлох болон татахад ажиллах функц. 
        :param report05:  05 тайлан
        :param sales: sales тайлан
        '''
        #Бүх мөрөө устгах
        self.clear_lines()

        line_obj = self.env['pos.information.line']
        
        #Файлуудаа унших
        fileobj = NamedTemporaryFile('w+')
        fileobj.write(base64.decodestring(report05))
        fileobj.seek(0)
        
        book = xlrd.open_workbook (fileobj.name)  
        report_sheet = book.sheet_by_index(0)

        fileobj = NamedTemporaryFile('w+')
        fileobj.write(base64.decodestring(sales))
        fileobj.seek(0)
        book = xlrd.open_workbook(fileobj.name)
        sales_sheet = book.sheet_by_index(0)
    
        sales_rows = sales_sheet.nrows
        report_rows = report_sheet.nrows
        report_total_l =report_total_kg = sales_total_l =  sales_total_kg =  0.0 
        
        index=1
        check_sessions = []
        check_products = []
        
        # 2 тайланд байгаа ээлж болон барааны дугаарууд өөр эсэхийг шалгах. Өөр байвал анхааруулга өгөх
        while index < sales_rows:
            sale_row = sales_sheet.row(index)
            if str(sale_row[0].value) not in check_sessions:
                check_sessions.append(str(sale_row[0].value))
            if int(sale_row[3].value) not in check_products:
                check_products.append(int(sale_row[3].value))
            index+=1
        rowi=1
        while rowi < report_rows:
            row = report_sheet.row(rowi)
            if str(row[6].value) in check_sessions:
                check_sessions.remove(str(row[6].value))

            if int(row[2].value) in check_products:
                check_products.remove(int(row[2].value))
            rowi+=1
     
        if check_sessions:
            raise UserError(_('Could not find sessions%s in both reports. Check report data')%check_sessions)
       
        if check_products:
            raise UserError(_('Could not  find products%s in both reports. Check report data')%check_products)
   
        # 05 тайлангын мөрүүдийг үүсгэх. Агуулах, бараа, түгээгүүр болон хошууны мэдээллийг pos_code талбараар нь хайн олно. Байхгүй бол анхааруулга өгөх
        rowi = 1
        while rowi < report_rows:
            index = 1
            row = report_sheet.row(rowi)
            warehouse_ids = self.env['stock.warehouse'].search([('pos_code','=',int(row[1].value))])
            if not warehouse_ids:
                raise UserError(_('There is no warehouse with id %s. Create new warehouse or if warehouse is already created, you should update code')%int(row[1].value))    
            product = self.env['product.product'].search([('pos_code','=',int(row[2].value))])
            if not product:
                raise UserError(_('There is no product with id %s. Create new product or if product is already created, you should update code')%int(row[2].value))
            trk = self.env['fuel.dispenser'].search([('warehouse_id','in',warehouse_ids.ids),('name','=', str(int(row[4].value)))])
            if not trk:
                raise UserError(_('There is no fuel dispenser with id %s. Create new dispenser or if dispenser is already created, you should update code')%int(row[4].value))
            nozzle = self.env['pos.nozzle'].search([('dispenser_id', 'in', [trk.id]),('name','=',str(int(row[5].value)))])
            if not nozzle:
                raise UserError(_('There is no nozzle with id %s. Create new nozzle or if nozzle is already created, you should update code')%int(row[5].value))

            date = row[7].value        
            cash_sales = []
            card_sales = []
            talon_sales = []
            warehouse = warehouse_ids
            session = str(row[6].value)
            product_id = int(row[2].value)
            density_avg = float(row[16].value)
            
            '''sales тайлангуудын мөрийг уншиж борлуулалтын ажилтан бүрээр, ялгаатай нэгж үнээр зарсан борлуулалтын тоогоор борлуулалтын захиалга үүсгэх өгөгдлийг
            cash_sales т хадгална. Одоогоороо посоос талон болон картаар борлуулсан борлуулалтын ажилтан бүрээр салгах боломжгүй учир 
            Энхтуяа нягтлантай яриад бүгдийг нь бэлэн мөнгөний борлуулалт руу үүсгэж байхаар тохирсон. 
            Пос нэвтрээд sales тайлангийн 7-р баганад 'talon', 'card','cash' гээд төрөл нэмэгддэг болчихвол 205-223 мөрийг коммент болгоод доор байгаа кодыг нь ажиллуулна. Мөн constraint-г  идэвхтэй болгоно.
            '''

            # while index < sales_rows:
            #     sale_row = sales_sheet.row(index)
            #     if str(sale_row[0].value)==session and int(sale_row[3].value)==product_id:
            #         if (sale_row[1].value not in [so['employee_id'] for so in cash_sales]) or (so['data']['unit_price']!=float(sale_row[4].value) and sale_row[1].value in [so['employee_id'] for so in cash_sales]):                              
            #             cash_sales.append({'employee_id':sale_row[1].value,
            #                                 'data': {   
            #                                           'partner_id':warehouse.pos_partner_id.id,
            #                                           'unit_price': float(sale_row[4].value),
            #                                           'second_product_qty':float(sale_row[5].value),
            #                                           'product_qty':(float(sale_row[5].value)*density_avg)}
            #                                             })
            #         else:                 
            #             for so in cash_sales:
            #                 if so['employee_id']==sale_row[1].value and so['data']['unit_price']==float(sale_row[4].value) and int(sale_row[3].value)==product_id and str(sale_row[0].value)==session:
            #                     so['data']['second_product_qty'] = so['data']['second_product_qty'] + float(sale_row[5].value)
            #                     so['data']['product_qty']=so['data']['product_qty']+ float(sale_row[5].value)*density_avg
            #     index+=1
            
            while index < sales_rows:
                sale_row = sales_sheet.row(index)
                if str(sale_row[0].value)==session and int(sale_row[3].value)==product_id:
                    if str(sale_row[7].value)=='cash':
                        if (sale_row[1].value not in [so['employee_id'] for so in cash_sales]) or (so['data']['unit_price']!=float(sale_row[4].value) and sale_row[1].value in [so['employee_id'] for so in cash_sales]):                              
                            cash_sales.append({'employee_id':sale_row[1].value,
                                            'data': {   
                                                      'partner_id':warehouse.pos_partner_id.id,
                                                      'unit_price': float(sale_row[4].value),
                                                      'second_product_qty':float(sale_row[5].value),
                                                      'product_qty':(float(sale_row[5].value)*density_avg)}
                                                        })
                        else:                 
                            for so in cash_sales:
                                if so['employee_id']==sale_row[1].value and so['data']['unit_price']==float(sale_row[4].value) and int(sale_row[3].value)==product_id and int(sale_row[0].value)==session:
                                    so['data']['second_product_qty'] = so['data']['second_product_qty'] + float(sale_row[5].value)
                                    so['data']['product_qty']=so['data']['product_qty']+ float(sale_row[5].value)*density_avg
                                     
                    elif str(sale_row[7].value)=='card':
                        if (sale_row[1].value not in [so['employee_id'] for so in card_sales]) or (so['data']['unit_price']!=float(sale_row[4].value) and sale_row[1].value in [so['employee_id'] for so in card_sales]):                              
                            card_sales.append({'employee_id':sale_row[1].value,
                                            'data': {   
                                                      'partner_id':warehouse.pos_partner_id.id,
                                                      'unit_price': float(sale_row[4].value),
                                                      'second_product_qty':float(sale_row[5].value),
                                                      'product_qty':(float(sale_row[5].value)*density_avg)}
                                                        })
                        else:                 
                            for so in card_sales:
                                if so['employee_id']==sale_row[1].value and so['data']['unit_price']==float(sale_row[4].value) and int(sale_row[3].value)==product_id and int(sale_row[0].value)==session:
                                    so['data']['second_product_qty'] = so['data']['second_product_qty'] + float(sale_row[5].value)
                                    so['data']['product_qty']=so['data']['product_qty']+ float(sale_row[5].value)*density_avg
                      

                    elif str(sale_row[7].value=='talon'):
                        if (sale_row[1].value not in [so['employee_id'] for so in talon_sales]) or (so['data']['unit_price']!=float(sale_row[4].value) and sale_row[1].value in [so['employee_id'] for so in talon_sales]):                              
                            talon_sales.append({'employee_id':sale_row[1].value,
                                            'data': {   
                                                      'partner_id':warehouse.pos_partner_id.id,
                                                      'unit_price': float(sale_row[4].value),
                                                      'second_product_qty':float(sale_row[5].value),
                                                      'product_qty':(float(sale_row[5].value)*density_avg)}
                                                        })
                        else:                 
                            for so in talon_sales:
                                if so['employee_id']==sale_row[1].value and so['data']['unit_price']==float(sale_row[4].value) and int(sale_row[3].value)==product_id and int(sale_row[0].value)==session:
                                    so['data']['second_product_qty'] = so['data']['second_product_qty'] + float(sale_row[5].value)
                                    so['data']['product_qty']=so['data']['product_qty']+ float(sale_row[5].value)*density_avg
                index+=1 
                    

            #05 тайлангийн мөр үүсгэх
            _data = {
                'rownum' :int(row[0].value),
                'warehouse_id':warehouse.id,
                'product_id':product.id,
                'tankno' : int(row[3].value),
                'trk' : trk.id,
                'nozzle' : nozzle.id,
                'session' : str(row[6].value),
                # 'date1' :datetime(*xlrd.xldate_as_tuple(date, book.datemode)).strftime('%Y-%m-%d'),
                'opname' : str(row[8].value),
                'amountmm1' : str(row[9].value),
                'density1' : float(row[10].value),
                'temp1' : float(row[11].value),
                'amount1' : float(row[12].value),
                'amountkg' : float(row[13].value),
                'rec_amount' :float(row[14].value),
                'rec_amountkg' : float(row[15].value),
                'density_avg' : float(row[16].value),
                'amount_cash' : float(row[17].value),
                'amountkg_cash' : float(row[18].value),
                'amount_cc' : float(row[19].value),
                'amountkg_cc' : float(row[20].value),
                'amount_other' : float(row[21].value),
                'amountkg_other' : float(row[22].value),
                'amount_talon' : float(row[23].value),
                'amountkg_talon' : float(row[24].value),
                'amount_expected' : float(row[25].value),
                'amountkg_expected' : float(row[26].value),
                'amount_total' : float(row[27].value),
                'amountkg_total' : float(row[28].value),
                'amountmm2' : float(row[29].value),
                'density2' : float(row[30].value),
                'temp2' : float(row[31].value),
                'amount2' : float(row[32].value),
                'amountkg2' : float(row[33].value),
                'amount_diff' : float(row[34].value),
                'amountkg_diff' : float(row[35].value),
                'pos_information_id':self.id,
                # 'cash_sales':cash_orders,
                # 'card_sales':card_sales,
                # 'talon_sales':talon_sales,
                }
            pos_id = line_obj.create(_data)    
            
            #cash_sales -т байгаа утгуудаар Ноорог төлөвтэй Борлуулалтын захиалга үүсгэх
            for order in cash_sales:
                employee_ids = self.env['hr.employee'].search([('pos_code','=',order['employee_id'])])
                if not employee_ids:
                    raise osv.except_osv(u'Configuration Error!','There is no employee with pos code %s!' %order['employee_id'])  
                employee = employee_ids
                self.env['pos.sale.line'].create({
                    'employee_id':employee.id, 
                    'partner_id': order['data']['partner_id'],
                    'unit_price':order['data']['unit_price'],
                    'second_product_qty':order['data']['second_product_qty'],
                    'product_qty': order['data']['product_qty'],
                    'type':'cash',
                    'pos_line_id': pos_id.id
                    })

            for order in card_sales:
                employee_ids = self.env['hr.employee'].search([('pos_code','=',order['employee_id'])])
                if not employee_ids:
                    raise osv.except_osv(u'Configuration Error!','There is no employee with pos code %s!' %order['employee_id'])  
                employee = employee_ids
                self.env['pos.sale.line'].create({
                    'employee_id':employee.id, 
                    'partner_id': order['data']['partner_id'],
                    'unit_price':order['data']['unit_price'],
                    'second_product_qty':order['data']['second_product_qty'],
                    'product_qty': order['data']['product_qty'],
                    'type':'card',
                    'pos_line_id': pos_id.id
                    })

            for order in talon_sales:
                employee_ids = self.env['hr.employee'].search([('pos_code','=',order['employee_id'])])
                if not employee_ids:
                    raise osv.except_osv(u'Configuration Error!','There is no employee with pos code %s!' %order['employee_id'])  
                employee = employee_ids
                self.env['pos.sale.line'].create({
                    'employee_id':employee.id, 
                    'partner_id': order['data']['partner_id'],
                    'unit_price':order['data']['unit_price'],
                    'second_product_qty':order['data']['second_product_qty'],
                    'product_qty': order['data']['product_qty'],
                    'type':'talon',
                    'pos_line_id': pos_id.id
                    })
            rowi+=1

class PosInformationLine(models.Model):
    _name = 'pos.information.line'
    
    pos_information_id =  fields.Many2one('pos.information', 'A5 Report', required=True, select=False)
    rownum = fields.Char('Petrol Station', readonly=True)
    warehouse_id = fields.Many2one('stock.warehouse',string='Warehouse',readonly=True)
    product_id = fields.Many2one('product.product',string ='Product', readonly=True)
    tankno = fields.Integer('Tank ID',readonly=True) 
    trk = fields.Many2one('fuel.dispenser',string='Fuel Dispenser') 
    nozzle = fields.Many2one('pos.nozzle',string='Nozzle', readonly=True)        
    session = fields.Char('Session ID', readonly=True)  
    date1 = fields.Date('Date of Pos',readonly=True)
    opname = fields.Char('Session Name', readonly=True)
    amountmm1 = fields.Char('Initial Height of Tank',readonly=True)
    density1 = fields.Float('Initial Density of Tank',readonly=True)
    temp1 = fields.Float('Initial Temperature of Tank', readonly=True)
    amount1 = fields.Float('Initial Amount Litre ',readonly=True)
    amountkg = fields.Float('Initial Amount Weight',readonly=True)
    rec_amount = fields.Float('Received Liter',readonly=True)
    rec_amountkg = fields.Float('Received Kg',readonly=True)
    rec_details_ids = fields.One2many('pos.detail.line','pos_line_id', string='Detail of Received Amount', domain=[('type','=','incoming')])
    not_assigned_rec_amount = fields.Float('Not Assigned Received Liter')
    not_assigned_rec_amountkg = fields.Float('Not Assigned Received Kg')

    density_avg = fields.Float('Average density',readonly=True)
   
    amount_cash = fields.Float('Amount cash of Litre',readonly=True)
    amountkg_cash = fields.Float('Amount cash of Weight',readonly=True)

    cash_sales = fields.One2many('pos.sale.line','pos_line_id',string='Cash Sale Lines',  readonly=True, domain=[('type','=','cash')])
    
    amount_cc = fields.Float('Amount credit card of Litre',readonly=True)
    amountkg_cc = fields.Float('Amount credit card of Weight',readonly=True)
    card_sales = fields.One2many('pos.sale.line','pos_line_id',string='Card Sale Lines', readonly=True, domain=[('type','=','card')])
   
    amount_other = fields.Float('Amount other of Litre',readonly=True)
    amountkg_other = fields.Float('Amount other of Weight',readonly=True)
    expense_details_ids = fields.One2many('pos.detail.line','pos_line_id', string='Detail of Expenses', readonly=True, states={'approved':[('readonly',False)]},  domain=[('type','=','outgoing')])
    not_assigned_expense_amount = fields.Float('Not Assigned Expense Amount')
    not_assigned_expense_amountkg = fields.Float('Not Assigned Expense Amount Kg')

    amount_talon = fields.Float('Amount talon of Litre',readonly=True)
    amountkg_talon = fields.Float('Amount talon of Weight',readonly=True)
    talon_sales = fields.One2many('pos.sale.line','pos_line_id', string='Talon Sale Lines', readonly=True,  domain=[('type','=','talon')])
    
    amount_expected = fields.Float('Amount expected of Litre',readonly=True)
    amountkg_expected = fields.Float('Amount expected of Weight',readonly=True)
    amount_total = fields.Float('Amount Total of Litre',readonly=True)
    amountkg_total = fields.Float('Amount Total of Weight',readonly=True)
    amountmm2 = fields.Float('Last Height of Tank',readonly=True)
    density2 = fields.Float('Last Density of Tank',readonly=True)
    temp2 = fields.Float('Last Temperature of Tank',readonly=True)
    amount2 = fields.Float('Last Amount Litre ',readonly=True)
    amountkg2 = fields.Float('Last Amount Weight',readonly=True)
    amount_diff = fields.Float('Difference Litre ',readonly=True)
    amountkg_diff = fields.Float('Difference Weight',readonly=True)
    state =  fields.Selection([('draft','Draft'),('approved','Approved'),('done','Done')],string='State', default='draft')
    calculated_total = fields.Float('Total Amount')

    def approve(self):
        ''' Батлах товч дарахад:
            1. cash_sales, card_sales, talon_sales дэх мөрүүдээр борлуулалтын захиалгууд үүсч, батлагдан, бараа нь шилжүүлэгдэнэ. 
            2. rec_details_ids, expense_details_ids  дэх мөрүүдийн бараа нь хуваарилагдсан тоо хэмжээгээр шилжүүлэгдэнэ.
        '''
        sale_obj = self.env['sale.order']
        sale_line_obj = self.env['sale.order.line']
        move_obj = self.env['stock.move']
        
        self._cr.execute('SELECT sum(pdl.product_qty) as qty, sum(pdl.second_product_qty) as second_qty, pdl.type as type '\
                    'FROM pos_detail_line pdl '\
                    'WHERE pdl.pos_line_id = %s GROUP BY pdl.type'
                             % self.id)
        records = self._cr.dictfetchall()
        for line in self:
            
            if not line.rec_details_ids and (line.rec_amount-line.not_assigned_rec_amount>0 or line.rec_amountkg-line.not_assigned_rec_amountkg>0):
                raise UserError(_('Insert detail of received amount%s!')%(line.rec_amount-line.not_assigned_rec_amount))
            if  not line.expense_details_ids and (line.amount_other-line.not_assigned_expense_amount or line.amountkg_other-line.not_assigned_expense_amountkg): 
                raise UserError(_('Insert detail of expense%s!')%(line.amount_other - line.not_assigned_expense_amount))
            
            for record in records:
                if record['type']=='incoming': 
                    if (line.rec_amount-line.not_assigned_rec_amount) != record['second_qty'] or (line.rec_amountkg-line.not_assigned_rec_amountkg) !=  record['qty']:
                        raise UserError(_('Total amount: %s, %s. Assigned amount: %s, %s!')%(line.rec_amount-line.not_assigned_rec_amount,line.rec_amountkg-line.not_assigned_rec_amountkg,record['second_qty'],record['qty']))    

                if record['type']=='outgoing':
                    if (line.amount_other-line.not_assigned_expense_amount) !=  record['second_qty'] or (line.amountkg_other-line.not_assigned_expense_amountkg) !=  record['qty']:
                        raise UserError(_('Total amount: %s, %s. Assigned amount: %s, %s!')%(line.amountkg_other,line.amount_other,record['second_qty'],record['qty']))
            
            for purchase_line in line.rec_details_ids:
                if not purchase_line.move_id.picking_id.pack_operation_product_ids:
                    purchase_line.move_id.picking_id.do_prepare_partial()
                processed_ids = []

                for prod in purchase_line.move_id.picking_id.pack_operation_product_ids:
                    if purchase_line.move_id.picking_id.purchase_id and not purchase_line.move_id.picking_id.purchase_id.cost_ok:
#                         raise osv.except_osv(_('Warning !'), _('You cannot process incoming shipment until purchase order cost approved!'))
                        raise UserError(_('You cannot process incoming shipment until purchase order cost approved!'))
                    if prod.product_id.id==purchase_line.move_id.product_id.id and prod.product_qty==purchase_line.move_id.product_qty:
                        pack_datas = {
                            'product_id': purchase_line.move_id.product_id.id,
                            'product_uom_id': purchase_line.move_id.product_uom.id,
                            'product_qty': purchase_line.product_qty,
                            'package_id': prod.package_id.id,
#                             'lot_id': prod.lot_id.id,
                            'location_id': purchase_line.move_id.location_id.id,
                            'location_dest_id': purchase_line.move_id.location_dest_id.id,
                            'result_package_id': prod.result_package_id.id,
                            'date': purchase_line.move_id.date if purchase_line.move_id.date else datetime.now(),
                            'owner_id': prod.owner_id.id,
                            'second_uom_id':purchase_line.move_id.second_uom_id.id,
                            'second_product_qty':purchase_line.second_product_qty,
                            'picking_id':purchase_line.move_id.picking_id.id
                                }
                        prod.with_context(no_recompute=True).write(pack_datas)
                        processed_ids.append(prod.id)

                        packop_id = self.env['stock.pack.operation'].create(pack_datas)
                        processed_ids.append(prod.id)
                        # Delete the others
                        packops = self.env['stock.pack.operation'].search(['&', ('picking_id', '=',purchase_line.move_id.picking_id.id), '!', ('id', 'in', processed_ids)])
                        packops.unlink()
#                         self.env['stock.pack.operation'].unlink(packops)
                        purchase_line.move_id.picking_id.do_transfer()
                    else:
                        raise UserError(_('You cannot process incoming shipment until purchase order cost approved!'))
            
            for sale_line in line.cash_sales:
                sale_id = sale_obj.create({
                    'date_order': line.pos_information_id.date,
                    'partner_id':sale_line.partner_id.id,
                    'warehouse_id':line.warehouse_id.id,
                    'user_id':sale_line.employee_id.user_id.id
                    })
                sale_line_obj.create({
                        'product_id':line.product_id.id, 
                        'product_uom':line.product_id.uom_id.id, 
                        'price_unit':sale_line.unit_price,
                        'product_uom_qty':sale_line.product_qty,
                        'name':line.product_id.name,
                        'order_id':sale_id.id,
                        'second_product_qty':sale_line.second_product_qty,
                        'second_uom_id':line.product_id.second_uom_id.id,
                        'second_unit_price':sale_line.unit_price,
                    })
                sale_id.signal_workflow('order_confirm') 
            
            for sale_line in line.card_sales:
                sale_id = sale_obj.create({
                    'date_order': line.pos_information_id.date,
                    'partner_id':sale_line.partner_id.id,
                    'warehouse_id':line.warehouse_id.id,
                    'user_id':sale_line.employee_id.user_id.id
                    })
                sale_line_obj.create({
                        'product_id':line.product_id.id, 
                        'product_uom':line.product_id.uom_id.id, 
                        'price_unit':sale_line.unit_price,
                        'product_uom_qty':sale_line.product_qty,
                        'name':line.product_id.name,
                        'order_id':sale_id.id,
                        'second_product_qty':sale_line.second_product_qty,
                        'second_uom_id':line.product_id.second_uom_id.id,
                        'second_unit_price':sale_line.unit_price,
                    })

                sale_id.signal_workflow('order_confirm')
            
            for sale_line in line.talon_sales:
                sale_id = sale_obj.create({
                    'date_order': line.pos_information_id.date,
                    'partner_id':sale_line.partner_id.id,
                    'warehouse_id':line.warehouse_id.id,
                    'user_id':sale_line.employee_id.user_id.id
                    })
                sale_line_obj.create({
                        'product_id':line.product_id.id, 
                        'product_uom':line.product_id.uom_id.id, 
                        'price_unit':sale_line.unit_price,
                        'product_uom_qty':sale_line.product_qty,
                        'name':line.product_id.name,
                        'order_id':sale_id.id,
                        'second_product_qty':sale_line.second_product_qty,
                        'second_uom_id':line.product_id.second_uom_id.id,
                        'second_unit_price':sale_line.unit_price,
                    })

                sale_id.signal_workflow('order_confirm')

                # pick_ids = []
                # for so in self.browse(cr, uid, ids, context=context):
                #     pick_ids += [picking.id for picking in so.picking_ids]


            for expense_line in line.expense_details_ids:
                if not expense_line.move_id.picking_id.pack_operation_ids:
                    expense_line.move_id.picking_id.do_prepare_partial()
                processed_ids = []
                for prod in expense_line.move_id.picking_id.pack_operation_ids:
                    if prod.product_id.id==expense_line.move_id.product_id.id and prod.product_qty==expense_line.move_id.product_qty:
                        pack_datas = {
                            'product_id': expense_line.move_id.product_id.id,
                            'product_uom_id': expense_line.move_id.product_uom.id,
                            'product_qty': expense_line.product_qty,
                            'package_id': prod.package_id.id,
                            'location_id': expense_line.move_id.location_id.id,
                            'location_dest_id': expense_line.move_id.location_dest_id.id,
                            'result_package_id': prod.result_package_id.id,
                            'date': expense_line.move_id.date if expense_line.move_id.date else datetime.now(),
                            'owner_id': prod.owner_id.id,
                            'second_uom_id':expense_line.move_id.second_uom_id.id,
                            'second_product_qty':expense_line.second_product_qty,
                            'picking_id':expense_line.move_id.picking_id.id
                                }
                        prod.with_context(no_recompute=True).write(pack_datas)
                        processed_ids.append(prod.id)
                        packop_id = self.env['stock.pack.operation'].create(pack_datas)
                        processed_ids.append(prod.id)
                        # Delete the others
                        packops = self.env['stock.pack.operation'].search(['&', ('picking_id', '=',expense_line.move_id.picking_id.id), '!', ('id', 'in', processed_ids)])
                        packops.unlink()
#                         self.env['stock.pack.operation'].unlink(cr,uid, packops)
                        expense_line.move_id.picking_id.do_transfer()

        self.write({'state':'done'})

class PosNozzle(models.Model):
    _name = 'pos.nozzle'
   
    name = fields.Char(required=True, string='Nozzle')
    dispenser_id = fields.Many2one('fuel.dispenser',required=True, string='Dispenser')

class FuelDispenser(models.Model):
    _name = 'fuel.dispenser'
    
    @api.multi
    def name_get(self):
        return [(obj.id, '%s:%s' % (obj.warehouse_id.name and '%s/ ' % obj.warehouse_id.name or '', obj.name))
                for obj in self]

    warehouse_id = fields.Many2one('stock.warehouse',required=True, string='Warehouse')
    name = fields.Char(required=True, string='Code')

class PosSaleLine(models.Model):
    _name = 'pos.sale.line'
    
    # product_id = fields.Many2one('product.product')
    pos_line_id = fields.Many2one('pos.information.line',string='Detail',ondelete='cascade')
    partner_id = fields.Many2one('res.partner','Partner')
    employee_id = fields.Many2one('hr.employee','Employee')
    product_qty = fields.Float('Product Quantity')
    second_product_qty = fields.Float('Second Product Quantity')
    unit_price = fields.Float('Price')
    total_amount = fields.Float('Total Amount')
    type = fields.Selection([('cash','Cash'),('talon','talon'),('card','Card')], string='Type')

class PosDetailLine(models.Model):
    _name = 'pos.detail.line'
    
    pos_line_id = fields.Many2one('pos.information.line', string='Detail', ondelete='cascade')
    move_id = fields.Many2one('stock.move', 'Move')
    rel_product_qty = fields.Float('Related Product Qty')
    rel_second_product_qty = fields.Float('Related Second Product Qty')
    product_qty = fields.Float('Product Qty')
    second_product_qty = fields.Float('Second Product Qty')
    type = fields.Selection([('incoming','Incoming'),('outgoing','Outgoing')], 'Type')

    @api.onchange('move_id')
    def onchange_move(self):
        if self.move_id:
            self.rel_product_qty = self.move_id.product_uom_qty
            self.rel_second_product_qty = self.move_id.second_product_qty

    @api.onchange('product_qty', 'second_product_qty')
    def onchange_product_qty(self):
        warnings = {}
        values = {}
        if self.product_qty > self.rel_product_qty:
            warnings.update({
                    'title': _('Warning !'),
                    'message': _('%s greather than %s')%(self.product_qty, self.rel_product_qty)
                        })
            values.update({'product_qty':self.rel_product_qty})
        elif self.second_product_qty > self.rel_second_product_qty:
            warnings.update({
                    'title': _('Warning !'),
                    'message': _('%s greather than %s')%(self.second_product_qty, self.rel_second_product_qty)
                        })
            values.update({'second_product_qty':self.rel_second_product_qty})
        return {'warning':warnings,
                'value':values}

class HrEmployee(models.Model):
    _inherit = 'hr.employee'
    
    pos_code = fields.Char('POS code')

class StockWarehouse(models.Model):
    _inherit='stock.warehouse'
    
    pos_partner_id = fields.Many2one('res.partner','Pos Partner')
    pos_code = fields.Integer('POS code')

class ProductTemplate(models.Model):
    _inherit='product.template'
    
    pos_code = fields.Integer('POS code')

