# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
from odoo import fields, models, _  # @UnresolvedImport
import logging 
_logger = logging.getLogger(__name__)

class ImportPosReport(models.TransientModel):
	_name = 'import.pos.report'
	_description = 'Import POS Report'

	report = fields.Binary('Report 05', required=True)
	sales = fields.Binary('Sales', required=True)

	def import_report(self, context=None):
		if context is None:
			context = {}
		pos_obj = self.env['pos.information']
		active_ids = context['active_ids']
		
		pos_id = context['active_ids']
		pos_report = self.env['pos.information'].browse(pos_id)
		pos_report.create_lines(self.report, self.sales)
		return True
