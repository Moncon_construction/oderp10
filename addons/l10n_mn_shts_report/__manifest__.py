# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

{
    "name": "SHTS Reports",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "description": """
Shts Reports
""",
    "website": "http://www.asterisk-tech.mn/",
    "category": "Generic Modules/Others",
    "depends": ['l10n_mn_shts', 'l10n_mn_stock_base_report', 'l10n_mn_purchase_extra_cost', 'l10n_mn_sales_report'],
    "data": [
        'wizard/purchase_extra_cost_print.xml',
        'report/product_ledger_report_view.xml',
        'report/product_move_check_report_view.xml',
        'report/sales_report_view.xml',
    ],
    "installable": True,
}