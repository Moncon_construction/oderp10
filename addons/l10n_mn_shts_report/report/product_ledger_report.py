# -*- encoding: utf-8 -*-
##############################################################################

import time
import base64
from io import BytesIO

import xlsxwriter
from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles  # @UnresolvedImport
from odoo.addons.l10n_mn_report.tools.tools import str_tuple
from odoo.addons.l10n_mn_report.tools.tools import get_date
from odoo.addons.l10n_mn_report.tools.tools import last_date
from odoo.exceptions import UserError
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import pytz

class ProductLedgerReport(models.TransientModel):
    """
       БАРАА МАТЕРИАЛЫН ТОВЧОО ТАЙЛАН
    """
    _inherit = 'product.legder.report'
    _description = "Product Ledger Report"

    is_liter = fields.Boolean('Is Liter')
    
    def get_categories(self, product_ids):
        res = {}
        if self.is_liter:
            if product_ids and type(product_ids) is not list:
                products = ','.join([str(product.id) for product in product_ids])
                self.env.cr.execute("SELECT\
                    c.id AS categ_id\
                    , c.name AS categ_name\
                    , p.id AS product_id\
                    , p.default_code AS code\
                    , p.barcode AS barcode\
                    , t.name AS product_name\
                    , u.name AS uom_name\
                    , u2.name AS second_uom_name\
                    , t.list_price AS product_price\
                FROM product_product p\
                    , product_template t\
                    , product_category c\
                    , product_uom u \
                    , product_uom u2 \
                WHERE\
                    u.id = t.uom_id\
                    AND u2.id = t.second_uom_id\
                    AND t.id = p.product_tmpl_id\
                    AND c.id = t.categ_id\
                    AND p.id in (" + products + ") ORDER BY c.name, p.default_code, p.barcode")
                
                res = self.env.cr.dictfetchall()
        else:
            res = super(ProductLedgerReport, self).get_categories(product_ids)
 
        return res
    
    def second_get_inventory(self, product_ids, location_ids, date_start, date_end, type, company_id):
        where_sdate = ''
        where_edate = ''

        if date_start:
            where_sdate = "m.date >= '%s ' AND " % date_start
        if date_end:
            where_edate = "m.date <= '%s ' AND " % date_end
        if type == 'c1':
            self._cr.execute("""SELECT query.product_id as id, sum(query.second_product_qty) as second_product_qty, 
                                CASE WHEN abs(sum(query.second_product_qty)) > 0 THEN sum(query.price_unit)/sum(query.second_product_qty) ELSE sum(query.price_unit)/1 END AS price_unit
                                FROM
                                (SELECT m.product_id as product_id, sum(second_product_qty) as second_product_qty, 
                                CASE WHEN abs(sum(m.second_product_qty)) > 0 THEN sum(m.price_unit)/sum(m.second_product_qty) ELSE sum(m.price_unit)/1 END AS price_unit
                                FROM stock_move m
                                WHERE m.date <= %s
                                AND location_dest_id IN %s 
                                AND location_id NOT IN %s 
                                AND company_id = %s
                                AND product_id in %s
                                AND state = 'done'
                                GROUP BY product_id
                                UNION
                                SELECT m.product_id as product_id, -sum(second_product_qty) as second_product_qty, 
                                CASE WHEN abs(sum(m.second_product_qty)) > 0 THEN sum(m.price_unit)/sum(m.second_product_qty) ELSE sum(m.price_unit)/1 END AS price_unit
                                FROM stock_move m
                                WHERE m.date <= %s
                                AND location_dest_id NOT IN %s
                                AND location_id IN %s 
                                AND company_id = %s
                                AND product_id in %s
                                AND state = 'done'
                                GROUP BY product_id) query
                                GROUP BY query.product_id""", (date_end, tuple(location_ids), tuple(location_ids), company_id, tuple(product_ids.ids), date_end, tuple(location_ids), tuple(location_ids), company_id, tuple(product_ids.ids)))
        elif type == 'c2':
            self._cr.execute("""SELECT query.product_id as id, sum(query.second_product_qty) as second_product_qty, 
                                CASE WHEN abs(sum(query.second_product_qty)) > 0 THEN sum(query.price_unit)/sum(query.second_product_qty) ELSE sum(query.price_unit)/1 END AS price_unit
                                FROM 
                                (SELECT m.product_id as product_id, sum(second_product_qty) as second_product_qty, 
                                CASE WHEN abs(sum(m.second_product_qty)) > 0 THEN sum(m.price_unit)/sum(m.second_product_qty) ELSE sum(m.price_unit)/1 END AS price_unit
                                FROM stock_move m
                                WHERE m.date <= %s
                                AND location_dest_id IN %s 
                                AND location_id NOT IN %s 
                                AND company_id = %s
                                AND product_id in %s
                                AND state = 'done'
                                GROUP BY product_id
                                UNION ALL
                                SELECT m.product_id as product_id, -sum(second_product_qty) as second_product_qty, 
                                CASE WHEN abs(sum(m.second_product_qty)) > 0 THEN sum(m.price_unit)/sum(m.second_product_qty) ELSE sum(m.price_unit)/1 END AS price_unit
                                FROM stock_move m
                                WHERE m.date <= %s
                                AND location_dest_id NOT IN %s
                                AND location_id IN %s 
                                AND company_id = %s
                                AND product_id in %s
                                AND state = 'done'
                                GROUP BY product_id
                                UNION ALL
                                SELECT m.product_id as product_id, sum(second_product_qty) as second_product_qty, 
                                CASE WHEN abs(sum(m.second_product_qty)) > 0 THEN sum(m.price_unit)/sum(m.second_product_qty) ELSE sum(m.price_unit)/1 END AS price_unit
                                FROM stock_move m
                                WHERE m.date >= %s 
                                AND m.date <= %s
                                AND location_dest_id IN %s 
                                AND location_id NOT IN %s 
                                AND company_id = %s
                                AND product_id in %s
                                AND state = 'done'
                                GROUP BY product_id
                                UNION ALL
                                SELECT m.product_id as product_id, -sum(second_product_qty) as second_product_qty, 
                                CASE WHEN abs(sum(m.second_product_qty)) > 0 THEN sum(m.price_unit)/sum(m.second_product_qty) ELSE sum(m.price_unit)/1 END AS price_unit
                                FROM stock_move m
                                WHERE m.date >= %s
                                AND m.date <= %s
                                AND location_dest_id NOT IN %s 
                                AND location_id IN %s 
                                AND company_id = %s
                                AND product_id in %s
                                AND state = 'done'
                                GROUP BY product_id) query
                                GROUP BY query.product_id""", (date_end, tuple(location_ids), tuple(location_ids), company_id, tuple(product_ids.ids), date_end, tuple(location_ids), tuple(location_ids), company_id, tuple(product_ids.ids), date_end, date_start, tuple(location_ids), tuple(location_ids), company_id, tuple(product_ids.ids), date_end, date_start, tuple(location_ids), tuple(location_ids), company_id, tuple(product_ids.ids)))
        elif type == 'in':
            self._cr.execute("""SELECT m.product_id as id, sum(second_product_qty) as second_product_qty, 
                                CASE WHEN abs(sum(m.second_product_qty)) > 0 THEN sum(m.price_unit)/sum(m.second_product_qty) ELSE sum(m.price_unit)/1 END AS price_unit
                                FROM stock_move m
                                WHERE m.date >= %s
                                AND m.date <= %s
                                AND location_dest_id IN %s 
                                AND location_id NOT IN %s 
                                AND company_id = %s
                                AND product_id in %s
                                AND state = 'done'
                                GROUP BY product_id""", (date_end, date_start, tuple(location_ids), tuple(location_ids), company_id, tuple(product_ids.ids)))
        elif type == 'out':
            self._cr.execute("""SELECT m.product_id as id, sum(second_product_qty) as second_product_qty,
                                CASE WHEN abs(sum(m.second_product_qty)) > 0 THEN sum(m.price_unit)/sum(m.second_product_qty) ELSE sum(m.price_unit)/1 END AS price_unit
                                FROM stock_move m
                                WHERE m.date >= %s
                                AND m.date <= %s
                                AND location_dest_id NOT IN %s 
                                AND location_id IN %s 
                                AND company_id = %s
                                AND product_id in %s
                                AND state = 'done'
                                GROUP BY product_id""", (date_end, date_start, tuple(location_ids), tuple(location_ids), company_id, tuple(product_ids.ids)))
        records = self.env.cr.dictfetchall()
        results = {}

        for record in records:
            if record['second_product_qty'] != None:
                results[record['id']] = {
                    'second_quantity': record['second_product_qty'],
                    'price_unit': record['price_unit'],
                    'total_cost': record['price_unit'] * record['second_product_qty']
                }

        return results

    @api.multi
    def export_report(self):

        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Product Ledger Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)

        format_title_float = book.add_format(ReportExcelCellStyles.format_title_float)
        format_group = book.add_format(ReportExcelCellStyles.format_group)
        format_group_left = book.add_format(ReportExcelCellStyles.format_group_left)
        format_group_right = book.add_format(ReportExcelCellStyles.format_group_right)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_content_center = book.add_format(ReportExcelCellStyles.format_content_center)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)

        # parameters
        rowx = 0

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('product_ledger_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(time.strftime('%Y-%m-%d'))
        sheet.set_portrait()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.39, 0.39, 0.39, 0.39)  # 1cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})

        # compute column
        if self.show_barcode:
            sheet.set_column('A:A', 4)
            sheet.set_column('B:B', 10)
            sheet.set_column('C:C', 15)
            sheet.set_column('D:D', 27)
            sheet.set_column('E:E', 11)
            sheet.set_column('F:F', 10)
            sheet.set_column('G:G', 15)
            sheet.set_column('H:H', 10)
            sheet.set_column('I:I', 15)
            sheet.set_column('J:J', 10)
            sheet.set_column('K:K', 15)
            sheet.set_column('L:L', 12)
            sheet.set_column('M:M', 10)
            sheet.set_column('N:N', 15)
            sheet.set_row(2, 20)
            sheet.set_row(5, 30)
            sheet.set_row(6, 30)
        else:
            sheet.set_column('A:A', 4)
            sheet.set_column('B:B', 10)
            sheet.set_column('C:C', 27)
            sheet.set_column('D:D', 11)
            sheet.set_column('E:E', 10)
            sheet.set_column('F:F', 15)
            sheet.set_column('G:G', 10)
            sheet.set_column('H:H', 15)
            sheet.set_column('I:I', 10)
            sheet.set_column('J:J', 15)
            sheet.set_column('K:K', 12)
            sheet.set_column('L:L', 10)
            sheet.set_column('M:M', 15)
            sheet.set_row(2, 20)
            sheet.set_row(5, 30)
            sheet.set_row(6, 30)            

        # create name
        sheet.merge_range(rowx, 0, rowx, 12, '%s' % self.company_id.name, format_filter)
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, 12, report_name.upper(), format_name)
        rowx += 2

        warehouse_names = u''
        for warehouse in self.warehouse_ids:
            warehouse_names += u', %s' % (warehouse.name)

        if self.show_barcode:
            sheet.merge_range(rowx - 1, 0, rowx - 1, 13, _(u'Warehouse: %s') % (warehouse_names), format_filter)
            sheet.merge_range(rowx, 0, rowx, 13, _(u'Duration: %s - %s') % (self.date_from, self.date_to), format_filter)
        else:
            sheet.merge_range(rowx - 1, 0, rowx - 1, 12, _(u'Warehouse: %s') % (warehouse_names), format_filter)
            sheet.merge_range(rowx, 0, rowx, 12, _(u'Duration: %s - %s') % (self.date_from, self.date_to), format_filter)
        rowx += 1

        # table header
        col = 0
        sheet.merge_range(rowx, col, rowx + 1, col, _('№'), format_title)
        col += 1
        sheet.merge_range(rowx, col, rowx + 1, col, _('Product code'), format_title)
        if self.show_barcode:
            col += 1
            sheet.merge_range(rowx, col, rowx + 1, col, _('Barcode'), format_title)
        col += 1
        sheet.merge_range(rowx, col, rowx + 1, col, _('Product name'), format_title)
        col += 1
        sheet.merge_range(rowx, col, rowx + 1, col, _('Unit of measure'), format_title)
        col += 1
        if self.is_liter:
            sheet.merge_range(rowx, col, rowx + 1, col, _('Second Unit of measure'), format_title)
            col += 1
            sheet.merge_range(rowx, col, rowx, col+2, _('Initial Balance'), format_title)
            col += 3
            sheet.merge_range(rowx, col, rowx, col+2, _('Income'), format_title)
            col += 3
            sheet.merge_range(rowx, col, rowx, col+2, _('Expense'), format_title)
            col += 3
            sheet.merge_range(rowx, col, rowx, col+3, _('End Balance'), format_title)
            if self.show_barcode:
                col = 7
            else:
                col = 6
            if self.price_type == 'cost':
                sheet.write(rowx + 1, col, _('Cost'), format_title)
                col += 3
                sheet.write(rowx + 1, col, _('Cost'), format_title)
                col += 3
                sheet.write(rowx + 1, col, _('Cost'), format_title)
                col += 2
                sheet.write(rowx + 1, col, _('Unit Cost'), format_title)
                col += 2
                sheet.write(rowx + 1, col, _('Cost'), format_title)
            elif self.price_type == 'sale_price':
                sheet.write(rowx + 1, col, _('Sale Price'), format_title)
                col += 2
                sheet.write(rowx + 1, col, _('Sale Price'), format_title)
                col += 2
                sheet.write(rowx + 1, col, _('Sale Price'), format_title)
                col += 1
                sheet.write(rowx + 1, col, _('Unit Sale Price'), format_title)
                col += 2
                sheet.write(rowx + 1, col, _('Total Price'), format_title)
            if self.show_barcode:
                col = 6
            else:
                col = 5
            sheet.write(rowx + 1, col, _('Quantity'), format_title)
            col += 2
            sheet.write(rowx + 1, col, _('Second Quantity'), format_title)
            col += 1
            sheet.write(rowx + 1, col, _('Quantity'), format_title)
            col += 2
            sheet.write(rowx + 1, col, _('Second Quantity'), format_title)
            col += 1
            sheet.write(rowx + 1, col, _('Quantity'), format_title)
            col += 2
            sheet.write(rowx + 1, col, _('Second Quantity'), format_title)
            col += 2
            sheet.write(rowx + 1, col, _('Quantity'), format_title)
            col += 2
            sheet.write(rowx + 1, col, _('Second Quantity'), format_title)
        else:
            sheet.merge_range(rowx, col, rowx, col+1, _('Initial Balance'), format_title)
            col += 2
            sheet.merge_range(rowx, col, rowx, col+1, _('Income'), format_title)
            col += 2
            sheet.merge_range(rowx, col, rowx, col+1, _('Expense'), format_title)
            col += 2
            sheet.merge_range(rowx, col, rowx, col+2, _('End Balance'), format_title)
            if self.show_barcode:
                col = 6
            else:
                col = 5
            if self.price_type == 'cost':
                sheet.write(rowx + 1, col, _('Cost'), format_title)
                col += 2
                sheet.write(rowx + 1, col, _('Cost'), format_title)
                col += 2
                sheet.write(rowx + 1, col, _('Cost'), format_title)
                col += 1
                sheet.write(rowx + 1, col, _('Unit Cost'), format_title)
                col += 2
                sheet.write(rowx + 1, col, _('Cost'), format_title)
            elif self.price_type == 'sale_price':
                sheet.write(rowx + 1, col, _('Sale Price'), format_title)
                col += 2
                sheet.write(rowx + 1, col, _('Sale Price'), format_title)
                col += 2
                sheet.write(rowx + 1, col, _('Sale Price'), format_title)
                col += 1
                sheet.write(rowx + 1, col, _('Unit Sale Price'), format_title)
                col += 2
                sheet.write(rowx + 1, col, _('Total Price'), format_title)
            if self.show_barcode:
                col = 5
            else:
                col = 4
            sheet.write(rowx + 1, col, _('Quantity'), format_title)
            col += 2
            sheet.write(rowx + 1, col, _('Quantity'), format_title)
            col += 2
            sheet.write(rowx + 1, col, _('Quantity'), format_title)
            col += 3
            sheet.write(rowx + 1, col, _('Quantity'), format_title)

        rowx += 2

        # Тайлан боловсруулалтын үйлдэл эхэлж байна.
        location_ids = self.get_location_ids() or [-1]
        product_ids = self.get_used_product_ids() or [-1]
        records = self.get_categories(product_ids)
        categ_ids = []
        init_subtotal = init_price_subtotal = 0
        in_subtotal = in_price_subtotal = 0
        out_subtotal = out_price_subtotal = 0
        end_subtotal = end_price_subtotal = 0
        end_subprice = 0

        init_total = init_price_total = 0
        in_total = in_price_total = 0
        out_total = out_price_total = 0
        end_total = end_price_total = 0
        end_price = 0
        
        second_init_subtotal = second_init_price_subtotal = 0
        second_in_subtotal = second_in_price_subtotal = 0
        second_out_subtotal = second_out_price_subtotal = 0
        second_end_subtotal = second_end_price_subtotal = 0
        second_end_subprice = 0

        second_init_total = second_init_price_total = 0
        second_in_total = second_in_price_total = 0
        second_out_total = second_out_price_total = 0
        second_end_total = second_end_price_total = 0
        second_end_price = 0

        # Тайлант хугацааны хоорондох үлдэгдлүүдийг боловсруулж байна.
        init_balance = self.get_inventory(product_ids, location_ids, None, get_date(self.date_from, True), ['in', 'out'])
        income_balance = self.get_inventory(product_ids, location_ids, get_date(self.date_from, True), last_date(self.date_to), ['in'])
        out_balance = self.get_inventory(product_ids, location_ids, get_date(self.date_from, True), last_date(self.date_to), ['out'])
        end_balance = self.get_inventory(product_ids, location_ids, None, get_date(self.date_to, False), ['in', 'out'])
        # 2 дох хэмжих нэгжийн эхний үлдэгдэл
        second_init_balance = self.second_get_inventory(product_ids, location_ids, get_date(self.date_to, False), get_date(self.date_from, True), 'c1', self.company_id.id)
        # 2 дох хэмжих нэгжийн орлого
        second_income_balance = self.second_get_inventory(product_ids, location_ids, get_date(self.date_to, False), get_date(self.date_from, True), 'in', self.company_id.id)
        # 2 дох хэмжих нэгжийн зарлага
        second_out_balance = self.second_get_inventory(product_ids, location_ids, get_date(self.date_to, False), get_date(self.date_from, True), 'out', self.company_id.id)
        # 2 дох хэмжих нэгжийн эцсийн үлдэгдэл
        second_end_balance = self.second_get_inventory(product_ids, location_ids, get_date(self.date_to, False), get_date(self.date_from, True), 'c2', self.company_id.id)

        seq = 1
        categ_seq = 1
        is_start = False
        for record in records:
            if record['categ_id'] not in categ_ids:
                if is_start:
                    col = 0
                    sheet.write(rowx, col, '', format_group_left)
                    col += 1
                    if self.show_barcode:
                        sheet.merge_range(rowx, col, rowx, col+3, _('SUBTOTAL'), format_group)
                        col += 4
                    else:
                        sheet.merge_range(rowx, col, rowx, col+2, _('SUBTOTAL'), format_group)
                        col += 3
                    
                    sheet.write(rowx, col, init_subtotal, format_group_float)
                    col += 1
                    sheet.write(rowx, col, init_price_subtotal, format_group_float)

                    col += 1
                    sheet.write(rowx, col, in_subtotal, format_group_float)
                    col += 1
                    sheet.write(rowx, col, in_price_subtotal, format_group_float)

                    col += 1
                    sheet.write(rowx, col, out_subtotal, format_group_float)
                    col += 1
                    sheet.write(rowx, col, out_price_subtotal, format_group_float)

                    col += 1
                    sheet.write(rowx, col, end_price_subtotal / end_subtotal if end_subtotal else 0, format_group_float)
                    col += 1
                    sheet.write(rowx, col, end_subtotal, format_group_float)
                    col += 1
                    sheet.write(rowx, col, end_price_subtotal, format_group_float)

                    init_subtotal = init_price_subtotal = 0
                    in_subtotal = in_price_subtotal = 0
                    out_subtotal = out_price_subtotal = 0
                    end_subtotal = end_price_subtotal = 0
                    rowx += 1
                else:
                    is_start = True
                
                col = 0
                sheet.write(rowx, col, categ_seq, format_group_left)
                col += 1
                if self.show_barcode:
                    sheet.merge_range(rowx, col, rowx, col+3, record['categ_name'], format_group_left)
                    col += 4
                else:
                    sheet.merge_range(rowx, col, rowx, col+2, record['categ_name'], format_group_left)
                    col += 3
                    
                sheet.write(rowx, col, '', format_group_right)
                col += 1
                sheet.write(rowx, col, '', format_group_right)

                col += 1
                sheet.write(rowx, col, '', format_group_right)
                col += 1
                sheet.write(rowx, col, '', format_group_right)

                col += 1
                sheet.write(rowx, col, '', format_group_right)
                col += 1
                sheet.write(rowx, col, '', format_group_right)

                col += 1
                sheet.write(rowx, col, '', format_group_right)
                col += 1
                sheet.write(rowx, col, '', format_group_right)
                col += 1
                sheet.write(rowx, col, '', format_group_right)
                if self.is_liter:
                    col += 1
                    sheet.write(rowx, col, '', format_group_right)
                    col += 1
                    sheet.write(rowx, col, '', format_group_right)
                    col += 1
                    sheet.write(rowx, col, '', format_group_right)
                    col += 1
                    sheet.write(rowx, col, '', format_group_right)
                    col += 1
                    sheet.write(rowx, col, '', format_group_right)

                categ_ids.append(record['categ_id'])
                categ_seq += 1
                seq = 1
                rowx += 1

            # Бараа материалын нэршил
            col = 0
            sheet.write(rowx, col, '%s.%s' % (categ_seq - 1, seq), format_content_center)
            col += 1
            sheet.write(rowx, col, record['code'], format_content_center)
            if self.show_barcode:
                col += 1
                sheet.write(rowx, col, record['barcode'], format_content_center)
            col += 1
            sheet.write(rowx, col, record['product_name'], format_content_text)
            col += 1
            sheet.write(rowx, col, record['uom_name'], format_content_center)
            if self.is_liter:
                col += 1
                sheet.write(rowx, col, record['second_uom_name'], format_content_center)

            # Тайлант хугацааны эхний үлдэгдэл
            if self.is_liter:
                count = init_balance[record['product_id']]['quantity'] if record['product_id'] in init_balance else 0
                second_count = second_init_balance[record['product_id']]['second_quantity'] if record['product_id'] in init_balance else 0
                if self.price_type == 'cost':
                    price = init_balance[record['product_id']]['price_unit'] if record['product_id'] in init_balance else 0
                    total_price = init_balance[record['product_id']]['total_cost'] if record['product_id'] in init_balance else 0
                    second_total_price = second_init_balance[record['product_id']]['total_cost'] if record['product_id'] in init_balance else 0
                elif self.price_type == 'sale_price':
                    total_price = record['product_price'] if record['product_id'] in init_balance else 0
                    total_price = total_price * count
                    second_total_price = total_price * second_count
                    price = record['product_price']
    
                init_subtotal += count if count else 0
                init_price_subtotal += total_price if total_price else 0
                init_total += count if count else 0
                init_price_total += total_price if total_price else 0
                col += 1
                sheet.write(rowx, col, count, format_content_float)
                col += 1
                sheet.write(rowx, col, total_price, format_content_float)
                second_init_subtotal += second_count if second_count else 0
                second_init_total += second_count if second_count else 0
                col += 1
                sheet.write(rowx, col, second_count, format_content_float)
            else:
                count = init_balance[record['product_id']]['quantity'] if record['product_id'] in init_balance else 0
                if self.price_type == 'cost':
                    price = init_balance[record['product_id']]['price_unit'] if record['product_id'] in init_balance else 0
                    total_price = init_balance[record['product_id']]['total_cost'] if record['product_id'] in init_balance else 0
                elif self.price_type == 'sale_price':
                    total_price = record['product_price'] if record['product_id'] in init_balance else 0
                    total_price = total_price * count
                    price = record['product_price']
    
                init_subtotal += count if count else 0
                init_price_subtotal += total_price if total_price else 0
                init_total += count if count else 0
                init_price_total += total_price if total_price else 0
                col += 1
                sheet.write(rowx, col, count, format_content_float)
                col += 1
                sheet.write(rowx, col, total_price, format_content_float)

            # Тайлант хугацааны орлого
            if self.is_liter:
                count = income_balance[record['product_id']]['quantity'] if record['product_id'] in income_balance else 0
                second_count = second_income_balance[record['product_id']]['second_quantity'] if record['product_id'] in second_income_balance else 0
                if self.price_type == 'cost':
                    total_price = income_balance[record['product_id']]['total_cost'] if record['product_id'] in income_balance else 0
                    price = income_balance[record['product_id']]['price_unit'] if record['product_id'] in income_balance else 0
                    second_total_price = second_income_balance[record['product_id']]['total_cost'] if record['product_id'] in income_balance else 0
                    second_price = second_income_balance[record['product_id']]['price_unit'] if record['product_id'] in income_balance else 0
                elif self.price_type == 'sale_price':
                    total_price = record['product_price'] if record['product_id'] in income_balance else 0
                    total_price = total_price * count
                    price = record['product_price']
                in_subtotal += count if count else 0
                in_price_subtotal += total_price if total_price else 0
                in_total += count if count else 0
                in_price_total += total_price if total_price else 0
                second_in_subtotal += second_count if second_count else 0
                second_in_total += second_count if second_count else 0
                col += 1
                sheet.write(rowx, col, count, format_content_float)
                col += 1
                sheet.write(rowx, col, total_price, format_content_float)
                col += 1
                sheet.write(rowx, col, second_count, format_content_float)
            else:
                count = income_balance[record['product_id']]['quantity'] if record['product_id'] in income_balance else 0
                if self.price_type == 'cost':
                    total_price = income_balance[record['product_id']]['total_cost'] if record['product_id'] in income_balance else 0
                    price = income_balance[record['product_id']]['price_unit'] if record['product_id'] in income_balance else 0
                    second_total_price = second_income_balance[record['product_id']]['total_cost'] if record['product_id'] in income_balance else 0
                    second_price = second_income_balance[record['product_id']]['price_unit'] if record['product_id'] in income_balance else 0
                elif self.price_type == 'sale_price':
                    total_price = record['product_price'] if record['product_id'] in income_balance else 0
                    total_price = total_price * count
                    price = record['product_price']

                in_subtotal += count if count else 0
                in_price_subtotal += total_price if total_price else 0
                in_total += count if count else 0
                in_price_total += total_price if total_price else 0
    
                col += 1
                sheet.write(rowx, col, count, format_content_float)
                col += 1
                sheet.write(rowx, col, total_price, format_content_float)

            # Тайлант хугацааны зарлага
            if self.is_liter:
                count = abs(out_balance[record['product_id']]['quantity']) if record['product_id'] in out_balance else 0
                second_count = second_out_balance[record['product_id']]['second_quantity'] if record['product_id'] in second_out_balance else 0
                if self.price_type == 'cost':
                    price = abs(out_balance[record['product_id']]['price_unit'] or 0) if record['product_id'] in out_balance else 0
                    total_price = abs(out_balance[record['product_id']]['total_cost'] or 0) if record['product_id'] in out_balance else 0
                elif self.price_type == 'sale_price':
                    total_price = record['product_price'] if record['product_id'] in out_balance else 0
                    total_price = total_price * count
                    price = record['product_price']
    
                out_subtotal += count if count else 0
                out_price_subtotal += total_price if total_price else 0
                out_total += count if count else 0
                out_price_total += total_price if total_price else 0
                second_out_subtotal += second_count if second_count else 0
                second_out_total += second_count if second_count else 0
                col += 1
                sheet.write(rowx, col, count, format_content_float)
                col += 1
                sheet.write(rowx, col, total_price, format_content_float)
                col += 1
                sheet.write(rowx, col, second_count, format_content_float)
            else:
                count = abs(out_balance[record['product_id']]['quantity']) if record['product_id'] in out_balance else 0
                if self.price_type == 'cost':
                    price = abs(out_balance[record['product_id']]['price_unit'] or 0) if record['product_id'] in out_balance else 0
                    total_price = abs(out_balance[record['product_id']]['total_cost'] or 0) if record['product_id'] in out_balance else 0
                elif self.price_type == 'sale_price':
                    total_price = record['product_price'] if record['product_id'] in out_balance else 0
                    total_price = total_price * count
                    price = record['product_price']
    
                out_subtotal += count if count else 0
                out_price_subtotal += total_price if total_price else 0
                out_total += count if count else 0
                out_price_total += total_price if total_price else 0

                col += 1
                sheet.write(rowx, col, count, format_content_float)
                col += 1
                sheet.write(rowx, col, total_price, format_content_float)

            # Тайлант хугацааны эцсийн үлдэгдэл
            if self.is_liter:
                count = end_balance[record['product_id']]['quantity'] if record['product_id'] in end_balance else 0
                second_count = second_end_balance[record['product_id']]['second_quantity'] if record['product_id'] in second_end_balance else 0
                if self.price_type == 'cost':
                    price = end_balance[record['product_id']]['price_unit'] if record['product_id'] in end_balance else 0
                    total_price = end_balance[record['product_id']]['total_cost'] if record['product_id'] in end_balance else 0
                elif self.price_type == 'sale_price':
                    total_price = record['product_price'] if record['product_id'] in end_balance else 0
                    total_price = total_price * count
                    price = record['product_price']
    
                end_subtotal += count if count else 0   
                end_price_subtotal += total_price if total_price else 0
                end_subprice += price if price else 0
                end_total += count if count else 0
                end_price_total += total_price if total_price else 0
                end_price += price if price else 0
                second_end_subtotal += second_count if second_count else 0
                second_end_total += second_count if second_count else 0
                col += 1
                sheet.write(rowx, col, price, format_content_float)
                col += 1
                sheet.write(rowx, col, count, format_content_float)
                col += 1
                sheet.write(rowx, col, total_price, format_content_float)
                col += 1
                sheet.write(rowx, col, second_count, format_content_float)
            else:
                count = end_balance[record['product_id']]['quantity'] if record['product_id'] in end_balance else 0
                if self.price_type == 'cost':
                    price = end_balance[record['product_id']]['price_unit'] if record['product_id'] in end_balance else 0
                    total_price = end_balance[record['product_id']]['total_cost'] if record['product_id'] in end_balance else 0
                elif self.price_type == 'sale_price':
                    total_price = record['product_price'] if record['product_id'] in end_balance else 0
                    total_price = total_price * count
                    price = record['product_price']
    
                end_subtotal += count if count else 0
                end_price_subtotal += total_price if total_price else 0
                end_subprice += price if price else 0
                end_total += count if count else 0
                end_price_total += total_price if total_price else 0
                end_price += price if price else 0
    
                col += 1
                sheet.write(rowx, col, price, format_content_float)
                col += 1
                sheet.write(rowx, col, count, format_content_float)
                col += 1
                sheet.write(rowx, col, total_price, format_content_float)

            rowx += 1
            seq += 1

        # Ангилалын дүнг тооцож байна.
        col = 0
        sheet.write(rowx, col, '', format_group_right)
        col += 1
        if self.is_liter:
            if self.show_barcode:
                sheet.merge_range(rowx, col, rowx, col+4, _('SUBTOTAL'), format_group)
                col += 5
            else:
                sheet.merge_range(rowx, col, rowx, col+3, _('SUBTOTAL'), format_group)
                col += 4
        else:
            if self.show_barcode:
                sheet.merge_range(rowx, col, rowx, col+3, _('SUBTOTAL'), format_group)
                col += 4
            else:
                sheet.merge_range(rowx, col, rowx, col+2, _('SUBTOTAL'), format_group)
                col += 3
        sheet.write(rowx, col, init_subtotal, format_group_float)
        col += 1
        sheet.write(rowx, col, init_price_subtotal, format_group_float)
        if self.is_liter:
            col += 1
            sheet.write(rowx, col, second_init_subtotal, format_group_float)
        col += 1
        sheet.write(rowx, col, in_subtotal, format_group_float)
        col += 1
        sheet.write(rowx, col, in_price_subtotal, format_group_float)
        if self.is_liter:
            col += 1
            sheet.write(rowx, col, second_in_subtotal, format_group_float)
        col += 1
        sheet.write(rowx, col, out_subtotal, format_group_float)
        col += 1
        sheet.write(rowx, col, out_price_subtotal, format_group_float)
        if self.is_liter:
            col += 1
            sheet.write(rowx, col, second_out_subtotal, format_group_float)
        col += 1
        sheet.write(rowx, col, '', format_group_float)
        col += 1
        sheet.write(rowx, col, end_subtotal, format_group_float)
        col += 1
        sheet.write(rowx, col, end_price_subtotal, format_group_float)
        if self.is_liter:
            col += 1
            sheet.write(rowx, col, second_end_subtotal, format_group_float)
        rowx += 1

        # Нийт дүнг тооцож байна.
        col = 0
        if self.is_liter:
            if self.show_barcode:
                sheet.merge_range(rowx, col, rowx, col+5, _('TOTAL'), format_title)
                col += 6
            else:
                sheet.merge_range(rowx, col, rowx, col+4, _('TOTAL'), format_title)
                col += 5
        else:
            if self.show_barcode:
                sheet.merge_range(rowx, col, rowx, col+4, _('TOTAL'), format_title)
                col += 5
            else:
                sheet.merge_range(rowx, col, rowx, col+3, _('TOTAL'), format_title)
                col += 4
        sheet.write(rowx, col, init_total, format_title_float)
        col += 1
        sheet.write(rowx, col, init_price_total, format_title_float)
        if self.is_liter:
            col += 1
            sheet.write(rowx, col, second_init_total, format_group_float)
            
        col += 1
        sheet.write(rowx, col, in_total, format_title_float)
        col += 1
        sheet.write(rowx, col, in_price_total, format_title_float)
        if self.is_liter:
            col += 1
            sheet.write(rowx, col, second_in_total, format_group_float)
            
        col += 1
        sheet.write(rowx, col, out_total, format_title_float)
        col += 1
        sheet.write(rowx, col, out_price_total, format_title_float)
        if self.is_liter:
            col += 1
            sheet.write(rowx, col, second_out_total, format_group_float)
            
        col += 1
        sheet.write(rowx, col, '' , format_title_float)
        col += 1
        sheet.write(rowx, col, end_total, format_title_float)
        col += 1
        sheet.write(rowx, col, end_price_total, format_title_float)
        if self.is_liter:
            col += 1
            sheet.write(rowx, col, second_end_total, format_group_float)
        sheet.set_zoom(75)
        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()
