# -*- coding: utf-8 -*-
import time
import logging
import base64
import xlsxwriter
from io import BytesIO
from datetime import datetime, timedelta
from lxml import etree
from operator import itemgetter
from odoo import api, fields, models, tools, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from odoo.exceptions import UserError

_logger = logging.getLogger('stock.report')

class ProductMoveCheckReportWizard(models.TransientModel):
    _inherit = 'product.move.check.report.wizard'

    is_liter = fields.Boolean('Is Liter')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id)

    @api.multi
    def get_available(self, wiz):
        ''' Эхний үлдэгдэл тооцож байна '''
        qty = 0
        where = ''
        location_ids = []
        from_date = wiz['from_date'] + ' 23:59:59'
        prod = wiz['prod_id']
        fdate = datetime.strptime(from_date, '%Y-%m-%d %H:%M:%S')
        oneday = timedelta(days=1)
        fdate = fdate - oneday
        self.env.cr.execute("SELECT lot_stock_id FROM stock_warehouse WHERE id = %s" % wiz['warehouse_id'])
        res2 = self.env.cr.dictfetchall()
        if res2:
            location_ids = self.env['stock.location'].search([('location_id', 'child_of', [res2[0]['lot_stock_id']])])
            if location_ids:
                where += " AND r.location_id in " + '(' + ','.join(map(str, location_ids.ids)) + ')'
        else:
            raise UserError(_('Stock Location not found!'))
        if len(location_ids) > 0:
            locations = tuple(location_ids.ids)
        self.env.cr.execute("SELECT sum(quantity) as quantity "
                            "FROM "
                            "(SELECT m.product_id as prod_id, CASE WHEN m.date < %s AND m.location_id NOT IN %s AND m.location_dest_id IN %s "
                            "THEN COALESCE(m.product_uom_qty/u.factor*u2.factor,0) "
                            "WHEN m.date < %s AND m.location_id IN %s AND m.location_dest_id NOT IN %s "
                            "THEN -COALESCE(m.product_uom_qty/u.factor*u2.factor,0) ELSE 0 END AS quantity "
                            "FROM stock_move m "
                            "LEFT JOIN product_product pp ON (pp.id=m.product_id) "
                            "LEFT JOIN product_template pt ON (pt.id=pp.product_tmpl_id) "
                            "LEFT JOIN product_size ps ON (ps.id = pt.size_id)"
                            "LEFT JOIN product_uom u ON (u.id=m.product_uom) "
                            "LEFT JOIN product_uom u2 ON (u2.id=pt.uom_id) "
                            "LEFT JOIN product_category pc ON (pt.categ_id = pc.id) "
                            "WHERE m.date <= %s and m.state = 'done' and m.product_id = %s ) as l "
                            "GROUP BY l.prod_id ",
                            (fdate, locations, locations, fdate, locations, locations, fdate, prod))
        stock = self.env.cr.dictfetchall()
        if stock and stock[0] and stock[0]['quantity']:
            qty = stock[0]['quantity']
        return qty

    @api.multi
    def second_get_available(self, wiz):
        ''' Эхний үлдэгдэл тооцож байна '''
        qty = 0
        where = ''
        location_ids = []
        prod = wiz['prod_id']
        fdate = wiz['from_date'] + ' 00:00:00'
        self.env.cr.execute("SELECT lot_stock_id FROM stock_warehouse WHERE id = %s" % wiz['warehouse_id'])
        res2 = self.env.cr.dictfetchall()
        if res2:
            location_ids = self.env['stock.location'].search([('location_id', 'child_of', [res2[0]['lot_stock_id']])])
            self._cr.execute("""SELECT sum(query.second_start_qty) as second_start_qty 
                                FROM (SELECT m.product_id as product_id, 
                                        CASE WHEN m.date < %s AND m.location_id NOT IN %s AND m.location_dest_id IN %s 
                                            THEN COALESCE(m.second_product_qty/u.factor*u2.factor,0) 
                                        WHEN m.date < %s AND m.location_id IN %s AND m.location_dest_id NOT IN %s 
                                            THEN -COALESCE(m.second_product_qty/u.factor*u2.factor,0) ELSE 0 END AS second_start_qty 
                                FROM stock_move m
                                LEFT JOIN product_product pp ON (pp.id=m.product_id) 
                                LEFT JOIN product_template pt ON (pt.id=pp.product_tmpl_id) 
                                LEFT JOIN product_uom u ON (u.id=m.product_uom) 
                                LEFT JOIN product_uom u2 ON (u2.id=pt.uom_id) 
                                WHERE m.company_id = %s
                                AND m.product_id = %s
                                AND m.state = 'done') as query
                                GROUP BY query.product_id """, (fdate, tuple(location_ids.ids), tuple(location_ids.ids), fdate, tuple(location_ids.ids),
                                                                tuple(location_ids.ids), wiz['company_id'], prod))
            stock = self.env.cr.fetchone()
        if stock and stock[0]:
            qty = stock[0]
        return qty

    @api.multi
    def get_move_data(self, wiz):
        # тухайн барааны хөдөлгөөний тоо, байрлал, үнэ зэрэг мэдээллийн барааны хөдөлгөөнөөс шүүх
        context = self._context or {}
        res = []
        where = where_location = where_not_location = ''
        return_ids = []
        prod_id = wiz['prod_id']
        from_date = wiz['from_date']
        to_date = wiz['to_date'] + ' 23:59:59'
        wid = wiz['warehouse_id']
        company = wiz['company_id']
        select_in = ''
        select_out = ''
        select_coll = ''
        join = ''
        number_query = ''
        group_by = ''
        if not 'sale.order' in self.env or not 'sale.category' in self.env:
            raise UserError(_('Install Mongolian Sale module!'))
        #         if not 'purchase.order' in self.env:
        #             raise UserError(_('Install Purchase Management module!'))
        if 'mrp.production' in self.env:
            select_in = " WHEN m.production_id is not null THEN 'ББ орлого' WHEN m.raw_material_production_id is not null THEN 'ТЭ зарлага' "
            select_out = " WHEN m.production_id is not null THEN 'ББ орлого' WHEN m.raw_material_production_id is not null THEN 'ТЭ зарлага' "
            number_query = " WHEN m.production_id is not null THEN mrp.name "
            join = ' LEFT JOIN mrp_production AS mrp ON (m.production_id = mrp.id OR m.raw_material_production_id = mrp.id) '
            group_by = 'm.production_id,mrp.name,'
        if wiz['draft']:
            where += " AND m.state <> 'cancel' "
        else:
            where += " AND m.state = 'done' "
        #         if wiz['lot_id']:
        #             where += " AND (spo_lot.lot_id = %s OR m.restrict_lot_id = %s)"  %(str(wiz['lot_id']),str(wiz['lot_id']))
        self.env.cr.execute('select view_location_id from stock_warehouse where id=%s', (wiz['warehouse_id'],))
        res2 = self.env.cr.dictfetchall()
        location_ids = self.env['stock.location'].search(
            [('location_id', 'child_of', [res2[0]['view_location_id']]), ('usage', '=', 'internal')])
        if location_ids:
            where_location = "AND m.location_id IN " + '(' + ','.join(
                map(str, location_ids.ids)) + ')' + " AND m.location_dest_id NOT IN " + '(' + ','.join(
                map(str, location_ids.ids)) + ')'
            where_not_location = "AND m.location_id NOT IN " + '(' + ','.join(
                map(str, location_ids.ids)) + ')' + " AND m.location_dest_id IN " + '(' + ','.join(
                map(str, location_ids.ids)) + ')'
        self.env.cr.execute("""SELECT m.date AS date, ('out') AS type, m.origin_returned_move_id AS return_id,
                            (CASE WHEN m.picking_id is not null THEN rp.name WHEN m.warehouse_id is not null
                                and sw.partner_id is not null THEN rp2.name ELSE '' END) AS partner,
                            (CASE WHEN m.procurement_id is not null and so.sale_category_id is not null THEN sc.name||' борлуулалт'
                                WHEN m.picking_id is not null and m.purchase_line_id is not null THEN 'refund_purchase'
                                WHEN m.inventory_id is not null THEN 'inventory' """ + select_out + """ """ + select_coll + """
                                WHEN sl.usage = 'transit' THEN 'procure'
                                WHEN sl.usage = 'consume' THEN 'consume'
                                WHEN m.procurement_id is not null THEN 'swap' WHEN m.origin_returned_move_id is not null THEN 'refund'
                                WHEN m.picking_id is not null and p.picking_type_id is not null and spt.code = 'internal' THEN 'internal'
                                ELSE 'pos' END) AS rep_type, m.state AS state, m.id AS move_id,
                            (CASE WHEN m.picking_id is not null THEN p.name 

                                WHEN m.inventory_id is not null THEN i.name ELSE COALESCE(m.origin, 'pos') END) AS dugaar,
                            (m.price_unit) AS cost, 
                            (pt.list_price) AS price, sl.name AS location,
                            SUM(coalesce(m.product_qty / u.factor * u2.factor)) AS qty,
                            SUM(coalesce(m.second_product_qty / u.factor * u2.factor)) AS second_qty
                        FROM stock_move AS m
                            LEFT JOIN stock_picking AS p ON (m.picking_id = p.id)
                            LEFT JOIN stock_picking_type AS spt ON (p.picking_type_id = spt.id)
                            """ + join + """
                            LEFT JOIN res_partner AS rp ON (p.partner_id = rp.id)""" +
                            # LEFT JOIN purchase_order_line AS pol ON (m.purchase_line_id = pol.id)
                            # LEFT JOIN swap_order_line AS swl ON (m.swap_line_id = swl.id)
                            """LEFT JOIN stock_inventory AS i ON (m.inventory_id = i.id)
                            LEFT JOIN procurement_order AS po ON (m.procurement_id = po.id)
                            LEFT JOIN sale_order_line AS sol ON (po.sale_line_id = sol.id)
                            LEFT JOIN sale_order AS so ON (sol.order_id = so.id)
                            LEFT JOIN sale_category AS sc ON (so.sale_category_id = sc.id)
                            JOIN product_product AS pp ON (m.product_id = pp.id)
                            JOIN product_template AS pt ON (pp.product_tmpl_id = pt.id)
                            JOIN product_uom AS u ON (m.product_uom = u.id)
                            JOIN product_uom AS u2 ON (pt.uom_id = u2.id)
                            JOIN stock_location AS sl ON (m.location_dest_id = sl.id)
                            LEFT JOIN stock_warehouse AS sw ON (m.warehouse_id = sw.id)
                            LEFT JOIN res_partner AS rp2 ON (sw.partner_id = rp2.id)
                        WHERE m.product_id = %s AND m.product_qty is not null
                            AND m.date >= %s AND m.date <= %s """ + where + """ """ + where_location + """
                        GROUP BY m.date,m.picking_id,rp.name,m.warehouse_id,sw.partner_id,rp2.name,sl.usage,
                            m.procurement_id,so.sale_category_id,sc.name,m.purchase_line_id,m.inventory_id,p.picking_type_id,
                            po.transit_line_id,m.origin_returned_move_id,spt.code,""" + group_by + """
                            i.name,pt.list_price,sl.name,m.state,p.name,m.id,m.restrict_lot_id
                    UNION ALL
                        SELECT m.date AS date, ('in') AS type, m.origin_returned_move_id AS return_id,
                            (CASE WHEN m.picking_id is not null THEN rp.name WHEN m.warehouse_id is not null
                                and sw.partner_id is not null THEN rp2.name ELSE '' END) AS partner,
                            (CASE WHEN m.procurement_id is not null and so.sale_category_id is not null THEN sc.name||' буцаалт'
                                WHEN m.picking_id is not null and m.purchase_line_id is not null THEN 'purchase'
                                WHEN m.inventory_id is not null THEN 'inventory' """ + select_in + """  """ + select_coll + """
                                WHEN sl.usage = 'transit' THEN 'procure'
                                WHEN sl.usage = 'consume' THEN 'consume'
                                WHEN m.origin_returned_move_id is not null THEN 'refund'
                                WHEN m.picking_id is not null and p.picking_type_id is not null and spt.code = 'internal' THEN 'internal'
                                ELSE 'pos' END) AS rep_type, m.state AS state, m.id AS move_id,
                            (CASE WHEN m.picking_id is not null THEN p.name 
                                """ + number_query + """ 
                                WHEN m.inventory_id is not null THEN i.name ELSE COALESCE(m.origin, 'pos') END) AS dugaar, (m.price_unit) AS cost, 
                                pt.list_price AS price, sl.name AS location,
                            SUM(coalesce(m.product_qty / u.factor * u2.factor)) AS qty,
                            SUM(coalesce(m.second_product_qty / u.factor * u2.factor)) AS second_qty
                        FROM stock_move AS m
                            LEFT JOIN stock_picking AS p ON (m.picking_id = p.id)
                            LEFT JOIN stock_picking_type AS spt ON (p.picking_type_id = spt.id)
                            """ + join + """
                            LEFT JOIN res_partner AS rp ON (p.partner_id = rp.id)""" +
                            # LEFT JOIN purchase_order_line AS pol ON (m.purchase_line_id = pol.id)
                            # LEFT JOIN swap_order_line AS swl ON (m.swap_line_id = swl.id)
                            """LEFT JOIN stock_inventory AS i ON (m.inventory_id = i.id)
                            LEFT JOIN procurement_order AS po ON (m.procurement_id = po.id)
                            LEFT JOIN sale_order_line AS sol ON (po.sale_line_id = sol.id)
                            LEFT JOIN sale_order AS so ON (sol.order_id = so.id)
                            LEFT JOIN sale_category AS sc ON (so.sale_category_id = sc.id)
                            JOIN product_product AS pp ON (m.product_id = pp.id)
                            JOIN product_template AS pt ON (pp.product_tmpl_id = pt.id)
                            JOIN product_uom AS u ON (m.product_uom = u.id)
                            JOIN product_uom AS u2 ON (pt.uom_id = u2.id)
                            JOIN stock_location AS sl ON (m.location_id = sl.id)
                            LEFT JOIN stock_warehouse AS sw ON (m.warehouse_id = sw.id)
                            LEFT JOIN res_partner AS rp2 ON (sw.partner_id = rp2.id)
                        WHERE m.product_id = %s AND m.product_qty is not null
                            AND m.date >= %s AND m.date <= %s """ + where + """ """ + where_not_location + """
                        GROUP BY m.date,m.picking_id,rp.name,m.warehouse_id,sw.partner_id,rp2.name,sl.usage,
                            m.procurement_id,so.sale_category_id,sc.name,m.purchase_line_id,m.inventory_id,p.picking_type_id,
                            po.transit_line_id,m.origin_returned_move_id,spt.code,""" + group_by + """
                            i.name,pt.list_price,sl.name,m.state,p.name,m.id,m.restrict_lot_id
                    UNION 
                        SELECT h.datetime AS date, ('price') AS type, h.id AS return,
                            (CASE WHEN rc.partner_id is not null THEN rp.name ELSE rc.name END) AS partner, ('price') AS rep_type, ('price') AS state, h.id AS move_id,
                            ('price') AS dugaar, (select 0) AS cost, h.list_price AS price,
                            ('price') AS location,
                            (select 0) AS qty,
                            (select 0) AS second_qty
                        FROM product_price_history AS h
                            JOIN product_template AS t ON (h.product_template_id = t.id)
                            JOIN product_product AS p ON (t.id = p.product_tmpl_id)
                            JOIN res_company AS rc ON (h.company_id = rc.id)
                            LEFT JOIN res_partner AS rp ON (rc.partner_id = rp.id)
                        WHERE h.datetime >= %s AND h.datetime <= %s AND h.company_id = %s
                            AND p.id = %s 
                    ORDER BY date""", (prod_id, from_date, to_date, prod_id, from_date, to_date, from_date, to_date, company,  prod_id))
        result = self.env.cr.dictfetchall()
        for r in result:
            #             lot = r['lot'] or ''
            in_qty = out_qty = 0.0
            second_in_qty = second_out_qty = 0.0
            if r['type'] == 'in':
                in_qty = r['qty']
                second_in_qty = r['second_qty']
            else:
                out_qty = r['qty']
                second_out_qty = r['second_qty']
            date = datetime.strptime(r['date'], '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d %H:%M')
            if wiz['report_type'] != 'price' and r['type'] == 'price':
                continue
            res.append({'date': date,
                        'lot': '',
                        'state': r['state'],
                        'location': r['location'],
                        'dugaar': r['dugaar'],
                        'rep_type': r['rep_type'],
                        'partner': r['partner'],
                        'move_id': r['move_id'],
                        'price': r['price'],
                        'in_qty': in_qty,
                        'out_qty': out_qty,
                        'second_in_qty': second_in_qty,
                        'second_out_qty': second_out_qty,
                        'cost': r['cost'] or 0.0
                        })
        return res

    @api.multi
    def export_report(self):
        # Тайлан хэвлэх процесс
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        report_name = self._name.replace('.', '_')
        filename = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'))

        format_title = book.add_format(ReportExcelCellStyles.format_title)
        # Баримтын толгой
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        # Хүснэгтийн толгой
        format_group = book.add_format(ReportExcelCellStyles.format_group)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_content_bold_float = book.add_format(ReportExcelCellStyles.format_content_bold_float)
        # Хүснэгтийн агуулга
        format_content_bold_text = book.add_format(ReportExcelCellStyles.format_content_bold_text)
        format_content_center = book.add_format(ReportExcelCellStyles.format_content_center)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_float_color = book.add_format(ReportExcelCellStyles.format_content_float_color)
        format_content_center_color = book.add_format(ReportExcelCellStyles.format_content_center_color)
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=(report_name), self_title=filename).create({})

        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Arial"&9&P', {'margin': 0.1})
        rowx = 0

        body = self.get_log_message()
        message = u"[Бүртгэл хяналтын баримт][XLS][PROCESSING] %s" % body
        _logger.info(message)

        wiz = {}
        ctx = self._context.copy()
        warehouse = company = ''
        user = self.env.user
        if 'company' not in ctx:
            ctx.update({'company_id': user.company_id.id})
        wiz.update({'company_id': user.company_id.id})
        lot_name = due_date = '..................'
        from_date = to_date = ''
        max_qty = min_qty = '..................'
        wiz['draft'] = self.draft
        wiz.update({'draft': self.draft,
                    'report_type': self.report_type,
                    'warehouse_id': self.warehouse_id.id,
                    'prod_id': self.product_id.id,
                    'from_date': self.from_date,
                    'to_date': self.to_date})
        wiz.update({'report_type': self.report_type})
        wiz.update({'warehouse_id': self.warehouse_id.id})
        wiz.update({'prod_id': self.product_id.id})
        wiz.update({'lot_id': (self.prodlot_id and self.prodlot_id.id) or False})
        wiz.update({'company_id': self.company_id.id})
        #if self.prodlot_id:
        #    lot = self.prodlot_id
        #    if lot and lot.name: lot_name = lot.name
        #    if lot and lot.life_date: due_date = lot.life_date

        first_avail = self.get_available(wiz) or 0
        second_first_avail = self.second_get_available(wiz) or 0
        result = self.get_move_data(wiz)
        self.env.cr.execute("""SELECT product_max_qty AS max_qty, product_min_qty AS min_qty 
                                FROM stock_warehouse_orderpoint WHERE product_id = %s AND warehouse_id = %s""" % (self.product_id.id, self.warehouse_id.id))
        point = self.env.cr.dictfetchall()
        if point and point[0]['max_qty']: max_qty = str(point[0]['max_qty'])
        if point and point[0]['min_qty']: min_qty = str(point[0]['min_qty'])
        report_name = u''
        mayagt = u''
        if self.report_type == 'price':
            mayagt = _('FORM PT-5-3')
            report_name = _('Product move check report (Price Unit)')
            sheet.merge_range(rowx, 0, rowx, 14, user.company_id.partner_id.name, format_filter)
        elif self.report_type == 'cost':
            mayagt = _('FORM PT-5-2')
            report_name = _('Product move check report (Cost)')
            sheet.merge_range(rowx, 0, rowx, 13, user.company_id.partner_id.name, format_filter)
        else:
            mayagt = _('FORM PT-5-1')
            report_name = _('Product move check report')
            sheet.merge_range(rowx, 0, rowx, 10, user.company_id.partner_id.name, format_filter)
        rowx += 1
        sheet.write(rowx, 2, mayagt, format_filter)
        rowx += 1
        sheet.merge_range(rowx, 2, rowx, 7, report_name, format_title)
        now_date = time.strftime('%Y-%m-%d %H:%M')
        rowx += 2
        sheet.write(rowx, 2, _('Sector name: %s') % (self.warehouse_id.name), format_filter)
        sheet.write(rowx, 6, _('Document Number: -----'), format_filter)
        sheet.write(rowx + 1, 2, _('Product Barcode: %s') % (self.product_id.barcode or ''), format_filter)
        sheet.write(rowx + 1, 6, _('Product Code : %s') % (self.product_id.default_code or ''), format_filter)
        sheet.write(rowx + 2, 2, _('Product Name:   %s') % (self.product_id.name or ''), format_filter)
        sheet.write(rowx + 2, 6, _('Product UoM:   %s') % self.product_id.uom_id.name, format_filter)
        sheet.write(rowx + 3, 2, _('Serial Number: %s') % (lot_name), format_filter)
        sheet.write(rowx + 3, 6, _('End Date: %s') % (due_date), format_filter)
        sheet.write(rowx + 4, 2, _('Safety Stock: %s') % (min_qty), format_filter)
        sheet.write(rowx + 4, 6, _('Max Stock: %s') % (max_qty), format_filter)
        sheet.write(rowx + 5, 2, _('Printed Date: %s') % (now_date), format_filter)
        sheet.write(rowx + 5, 6, _('Check Duration: %s - %s') % (self.from_date, self.to_date), format_filter)
        #         sheet.row(1).height = 400
        rowx += 7
        count = 0
        second_in_total = second_out_total = 0
        sheet.merge_range(rowx, 1, rowx + 1, 1, _('Seq'), format_group)
        sheet.merge_range(rowx, 2, rowx, 6, _('Document'), format_group)
        sheet.write(rowx + 1, 2, _('Date'), format_group)
        sheet.write(rowx + 1, 3, _('Type'), format_group)
        sheet.write(rowx + 1, 4, _('Number'), format_group)
        sheet.write(rowx + 1, 5, _('Move'), format_group)
        sheet.write(rowx + 1, 6, _('Serial'), format_group)
        sheet.merge_range(rowx, 7, rowx + 1, 7, _('Partner'), format_group)
        if self.report_type == 'owner':
            if self.is_liter:
                sheet.merge_range(rowx, 8, rowx, 13, _('QTY'), format_group)
                sheet.write(rowx + 1, 8, _('Income'), format_group)
                sheet.write(rowx + 1, 9, _('Income (liter)'), format_group)
                sheet.write(rowx + 1, 10, _('Expense'), format_group)
                sheet.write(rowx + 1, 11, _('Expense (liter)'), format_group)
                sheet.write(rowx + 1, 12, _('Balance'), format_group)
                sheet.write(rowx + 1, 13, _('Balance (liter)'), format_group)
            else:
                sheet.merge_range(rowx, 8, rowx, 10, _('QTY'), format_group)
                sheet.write(rowx + 1, 8, _('Income'), format_group)
                sheet.write(rowx + 1, 9, _('Expense'), format_group)
                sheet.write(rowx + 1, 10, _('Balance'), format_group)
        elif self.report_type == 'price':
            if self.is_liter:
                sheet.merge_range(rowx, 8, rowx + 1, 8, _('QTY'), format_group)
                sheet.merge_range(rowx, 9, rowx + 1, 9, _('QTY (liter)'), format_group)
                sheet.merge_range(rowx, 10, rowx + 1, 10, _('Price Unit'), format_group)
                sheet.merge_range(rowx, 11, rowx, 16, _('Total /MNT/'), format_group)
                sheet.write(rowx + 1, 11, _('Income'), format_group)
                sheet.write(rowx + 1, 12, _('Income (liter)'), format_group)
                sheet.write(rowx + 1, 13, _('Expense'), format_group)
                sheet.write(rowx + 1, 14, _('Expense (liter)'), format_group)
                sheet.write(rowx + 1, 15, _('Balance'), format_group)
                sheet.write(rowx + 1, 16, _('Balance (liter)'), format_group)
                sheet.merge_range(rowx, 17, rowx + 1, 17, _('Balance Quantity'), format_group)
                sheet.merge_range(rowx, 18, rowx + 1, 18, _('Balance Quantity (liter)'), format_group)
            else:
                sheet.merge_range(rowx, 8, rowx + 1, 8, _('QTY'), format_group)
                sheet.merge_range(rowx, 9, rowx + 1, 9, _('Price Unit'), format_group)
                sheet.merge_range(rowx, 10, rowx, 13, _('Total /MNT/'), format_group)
                sheet.write(rowx + 1, 10, _('Income'), format_group)
                sheet.write(rowx + 1, 11, _('Expense'), format_group)
                sheet.write(rowx + 1, 12, _('Price diff'), format_group)
                sheet.write(rowx + 1, 13, _('Balance'), format_group)
                sheet.merge_range(rowx, 14, rowx + 1, 14, _('Balance Quantity'), format_group)
        else:
            if self.is_liter:
                sheet.merge_range(rowx, 8, rowx + 1, 8, _('QTY'), format_group)
                sheet.merge_range(rowx, 9, rowx + 1, 9, _('QTY (liter)'), format_group)
                sheet.merge_range(rowx, 10, rowx + 1, 10, _('Cost'), format_group)
                sheet.merge_range(rowx, 11, rowx, 16, _('Total /Cost/'), format_group)
                sheet.write(rowx + 1, 11, _('Income'), format_group)
                sheet.write(rowx + 1, 12, _('Income (liter)'), format_group)
                sheet.write(rowx + 1, 13, _('Expense'), format_group)
                sheet.write(rowx + 1, 14, _('Expense (liter)'), format_group)
                sheet.write(rowx + 1, 15, _('Balance'), format_group)
                sheet.write(rowx + 1, 16, _('Balance (liter)'), format_group)
                sheet.merge_range(rowx, 17, rowx + 1, 17, _('Balance Quantity'), format_group)
                sheet.merge_range(rowx, 18, rowx + 1, 18, _('Balance Quantity (liter)'), format_group)
            else:
                sheet.merge_range(rowx, 8, rowx + 1, 8, _('QTY'), format_group)
                sheet.merge_range(rowx, 9, rowx + 1, 9, _('Cost'), format_group)
                sheet.merge_range(rowx, 10, rowx, 12, _('Total /Cost/'), format_group)
                sheet.write(rowx + 1, 10, _('Income'), format_group)
                sheet.write(rowx + 1, 11, _('Expense'), format_group)
                sheet.write(rowx + 1, 12, _('Balance'), format_group)
                sheet.merge_range(rowx, 13, rowx + 1, 13, _('Balance Quantity'), format_group)
        rowx += 2
        sheet.write(rowx, 1, _('X'), format_content_center)
        sheet.write(rowx, 2, _('X'), format_content_center)
        sheet.write(rowx, 3, _('X'), format_content_center)
        sheet.write(rowx, 4, _('X'), format_content_center)
        sheet.write(rowx, 5, _('X'), format_content_center)
        sheet.write(rowx, 6, _('X'), format_content_center)
        sheet.write(rowx, 7, _('Initial Balance'), format_content_center)
        if self.report_type == 'owner':
            sheet.write(rowx, 8, _('X'), format_content_bold_text)
            sheet.write(rowx, 9, _('X'), format_content_center)
            sheet.write(rowx, 10, first_avail, format_content_bold_float)
            if self.is_liter:
                sheet.write(rowx, 11, u'X', format_content_center)
                sheet.write(rowx, 12, u'X', format_content_center)
                sheet.write(rowx, 13, second_first_avail, format_content_bold_float)
        elif self.report_type == 'price':
            first_price = self.get_price(wiz, 'first') or 0.0
            change = 0
            change_total = 0
            if first_price != 0:
                change = first_price
            sheet.write(rowx, 8, first_avail, format_content_bold_float)
            if self.is_liter:
                sheet.write(rowx, 9, second_first_avail, format_content_bold_float)
                sheet.write(rowx, 10, first_price, format_content_bold_float)
                sheet.write(rowx, 11, u'X', format_content_center)
                sheet.write(rowx, 12, u'X', format_content_center)
                sheet.write(rowx, 13, u'X', format_content_center)
                sheet.write(rowx, 14, u'X', format_content_center)
                sheet.write(rowx, 15, (first_avail != 0 and first_price != 0) and (first_avail * first_price) or '', format_content_bold_float)
                sheet.write(rowx, 16,(second_first_avail != 0 and first_price != 0) and (second_first_avail * first_price) or '', format_content_bold_float)
                sheet.write(rowx, 17, first_avail, format_content_bold_float)
                sheet.write(rowx, 18, second_first_avail, format_content_bold_float)
            else:
                sheet.write(rowx, 9, first_price, format_content_bold_float)
                sheet.write(rowx, 10, u'X', format_content_center)
                sheet.write(rowx, 11, u'X', format_content_center)
                sheet.write(rowx, 12, u'X', format_content_center)
                sheet.write(rowx, 13, (first_avail != 0 and first_price != 0) and (first_avail * first_price) or '', format_content_bold_float)
                sheet.write(rowx, 14, first_avail, format_content_bold_float)
        else:
            first_cost = self.get_first_cost(wiz) or 0
            last_cost = first_cost or 0
            balance = (first_avail * first_cost) or 0
            second_balance = (second_first_avail * first_cost) or 0
            sheet.write(rowx, 8, first_avail, format_content_bold_float)
            if self.is_liter:
                sheet.write(rowx, 9, second_first_avail, format_content_bold_float)
                sheet.write(rowx, 10, u'X', format_content_center)
                sheet.write(rowx, 11, u'X', format_content_center)
                sheet.write(rowx, 12, u'X', format_content_center)
                sheet.write(rowx, 13, u'X', format_content_center)
                sheet.write(rowx, 14, u'X', format_content_center)
                sheet.write(rowx, 15, balance, format_content_bold_float)
                sheet.write(rowx, 16,second_balance, format_content_bold_float)
                sheet.write(rowx, 17, first_avail, format_content_bold_float)
                sheet.write(rowx, 18, second_first_avail, format_content_bold_float)
            else:
                sheet.write(rowx, 9, first_cost, format_content_bold_float)
                sheet.write(rowx, 10, u'X', format_content_center)
                sheet.write(rowx, 11, u'X', format_content_center)
                sheet.write(rowx, 12, balance, format_content_bold_float)
                sheet.write(rowx, 13, first_avail, format_content_bold_float)
        rowx += 1
        in_total_qty = second_in_total_qty = in_total = out_total = 0
        qty = second_qty = unit = 0
        if result:
            for r in result:
                price = 0  # self.product_id.lst_price
                second_diff = diff = 0
                count += 1
                rep_type = ''
                dugaar = partner = seri = moves = supply_warehouse_name = ''
                in_qty = out_qty = 0
                second_in_qty = second_out_qty = 0
                if r['rep_type'] == 'procure':
                    move = self.env['stock.move'].browse(r['move_id'])[0]
                    if r['rep_type'] == 'procure':
                        move = self.env['stock.move'].browse(r['move_id'])[0]
                        if move.procurement_id:
                            if move.picking_id.transit_order_id:
                                moves += move.picking_id.transit_order_id.supply_warehouse_id.name
                                moves += ' --> ' + move.picking_id.transit_order_id.warehouse_id.name
                        else:
                            transit_ids = self.env['stock.transit.order'].search([('name', '=', move.origin)])
                            if transit_ids:
                                transit = transit_ids[0]
                                moves += transit.supply_warehouse_id.name
                                moves += ' --> ' + transit.warehouse_id.name
                if r['dugaar']:
                    if r['dugaar'] == 'pos':
                        dugaar = r['location']
                    else:
                        dugaar = r['dugaar']
                if r['partner']: partner = r['partner']
                if r['lot']: seri = r['lot']
                if r['in_qty'] and r['in_qty'] != 0:  # Орлого тооцох хэсэг
                    qty += r['in_qty']
                    in_qty = r['in_qty']
                    first_avail += r['in_qty'] or 0
                    diff = r['in_qty']
                    in_total_qty += r['in_qty']
                    if self.report_type == 'owner':
                        in_total += r['in_qty']
                    elif self.report_type == 'price' and r['price']:
                        in_total += (in_qty * r['price'])
                    elif self.report_type == 'cost' and r['cost']:
                        in_total += (in_qty * r['cost'])
                        balance += (in_qty * r['cost'])
                    if self.report_type == 'cost' and first_avail != 0:
                        last_cost = balance / first_avail
                elif r['out_qty'] and r['out_qty'] != 0:  # Зарлага тооцох хэсэг
                    qty += r['out_qty']
                    out_qty = r['out_qty']
                    first_avail -= r['out_qty'] or 0
                    diff = r['out_qty']
                    in_total_qty -= r['out_qty']
                    if self.report_type == 'owner':
                        out_total += r['out_qty']
                    elif self.report_type == 'price' and r['price']:
                        out_total += (out_qty * r['price'])
                    elif self.report_type == 'cost' and r['cost']:
                        out_total += (out_qty * r['cost'])
                        balance -= (out_qty * r['cost'])
                        last_cost = r['cost']
                if r['second_in_qty'] and r['second_in_qty'] != 0:  # 2дахь ХН орлого тооцох хэсэг
                    second_qty += r['second_in_qty']
                    second_in_qty = r['second_in_qty']
                    second_first_avail += r['second_in_qty'] or 0
                    second_in_total_qty += r['second_in_qty']
                    second_diff = r['second_in_qty']
                    if self.report_type == 'owner':
                        second_in_total += r['second_in_qty']
                    elif self.report_type == 'cost' and r['cost']:
                        second_balance += (second_in_qty * r['cost'])
                elif r['second_out_qty'] and r['second_out_qty'] != 0:  # 2дахь ХН зарлага тооцох хэсэг
                    second_qty += r['second_out_qty']
                    second_out_qty = r['second_out_qty']
                    second_first_avail -= r['second_out_qty'] or 0
                    second_in_total_qty -= r['second_out_qty']
                    second_diff = r['second_out_qty']
                    if self.report_type == 'owner':
                        second_out_total += r['second_out_qty']
                    elif self.report_type == 'cost' and r['cost']:
                        second_balance -= (second_out_qty * r['cost'])
                if self.report_type == 'price' and r['price']:
                    if change == 0:
                        change = r['price']
                    elif change != r['price']:
                        if r['dugaar'] and r['dugaar'] == 'price':
                            unit = (r['price'] - change)
                            change_total += (unit * first_avail)
                    price = r['price']
                if self.report_type == 'cost' and r['cost']:
                    cost = r['cost']
                if r['rep_type']:
                    if r['rep_type'] == 'purchase':
                        rep_type = _('Purchase')
                    elif r['rep_type'] == 'inventory':
                        rep_type = _('Inventory')
                    elif r['rep_type'] == 'swap':
                        rep_type = _('Swap')
                    elif r['rep_type'] == 'consume':
                        rep_type = _('Internal expense')
                    elif r['rep_type'] == 'procure':
                        rep_type = _('Transit')
                    elif r['rep_type'] == 'refund_purchase':
                        rep_type = _('Purchase refund')
                    elif r['rep_type'] == 'refund':
                        rep_type = _('Refund')
                    elif r['rep_type'] == 'internal':
                        rep_type = _('Internal Move')
                    elif r['rep_type'] == 'pos':
                        rep_type = _('POS Sales')
                    elif r['rep_type'] == 'mrp':
                        rep_type = _('Manufacturing')
                    elif r['rep_type'] == 'refund_mrp':
                        rep_type = _('Manufacturing refund')
                    elif r['rep_type'] == 'collection':
                        rep_type = _('Collection')
                    else:
                        rep_type = r['rep_type']
                if r['rep_type'] in ('pos', 'internal') and r['partner'] is None:
                    rep_type = r['location']
                    move = self.env['stock.move'].browse(r['move_id'])[0]
                    partner = move.location_id.name

                if self.draft and r['state'] != 'done':
                    content = format_content_center_color
                    content_float = format_content_float_color
                else:
                    content = format_content_center
                    content_float = format_content_float

                sheet.write(rowx, 1, count, content)
                sheet.write(rowx, 2, r['date'], content)
                sheet.write(rowx, 3, rep_type, content)
                sheet.write(rowx, 4, dugaar, content)
                sheet.write(rowx, 5, moves, content)
                sheet.write(rowx, 6, seri, content)
                sheet.write(rowx, 7, partner, content)
                if self.report_type == 'owner':
                    if self.is_liter:
                        sheet.write(rowx, 8, (in_qty != 0 and in_qty) or '', content_float)
                        sheet.write(rowx, 9, (second_in_qty != 0 and second_in_qty) or '', content_float)
                        sheet.write(rowx, 10, (out_qty != 0 and out_qty) or '', content_float)
                        sheet.write(rowx, 11, (second_out_qty != 0 and second_out_qty) or '', content_float)
                        sheet.write(rowx, 12, first_avail, content_float)
                        sheet.write(rowx, 13, second_first_avail, content_float)
                    else:
                        sheet.write(rowx, 8, (in_qty != 0 and in_qty) or '', content_float)
                        sheet.write(rowx, 9, (out_qty != 0 and out_qty) or '', content_float)
                        sheet.write(rowx, 10, first_avail, content_float)
                elif self.report_type == 'price':
                    sheet.write(rowx, 8, (diff != 0 and diff) or '', content_float)
                    if self.is_liter:
                        sheet.write(rowx, 9, (second_diff != 0 and second_diff) or '', content_float)
                        sheet.write(rowx, 10, (price != 0 and price) or '', content_float)
                        sheet.write(rowx, 11, (in_qty != 0 and in_qty * price) or '', content_float)
                        sheet.write(rowx, 12, (second_in_qty != 0 and second_in_qty * price) or '', content_float)
                        sheet.write(rowx, 13, (out_qty != 0 and out_qty * price) or '', content_float)
                        sheet.write(rowx, 14, (second_out_qty != 0 and second_out_qty * price) or '', content_float)
                        sheet.write(rowx, 15, ((unit > 0 and first_avail > 0 and unit * first_avail) or
                                               (unit < 0 and first_avail > 0 and '(' + str(abs(unit * first_avail)) + ')')) or '', content_float)
                        sheet.write(rowx, 16, ((unit > 0 and second_first_avail > 0 and unit * second_first_avail) or
                                               (unit < 0 and second_first_avail > 0 and '(' + str(abs(unit * second_first_avail)) + ')')) or '', content_float)
                        sheet.write(rowx, 17, first_avail, content_float)
                        sheet.write(rowx, 18, second_first_avail, content_float)
                    else:
                        sheet.write(rowx, 9, (price != 0 and price) or '', content_float)
                        sheet.write(rowx, 10, (in_qty != 0 and in_qty * price) or '', content_float)
                        sheet.write(rowx, 11, (out_qty != 0 and out_qty * price) or '', content_float)
                        sheet.write(rowx, 12, ((unit > 0 and first_avail > 0 and unit * first_avail) or
                                               (unit < 0 and first_avail > 0 and '(' + str(
                                                   abs(unit * first_avail)) + ')')) or '', content_float)
                        sheet.write(rowx, 13, (change != 0 and (change * first_avail)) or '', content_float)
                        sheet.write(rowx, 14, first_avail, content_float)

                else:
                    sheet.write(rowx, 8, (diff != 0 and diff) or '', content_float)
                    if self.is_liter:
                        sheet.write(rowx, 9, (second_diff != 0 and second_diff) or '', content_float)
                        sheet.write(rowx, 10, (cost != 0 and cost) or '', content_float)
                        sheet.write(rowx, 11, (in_qty != 0 and in_qty * cost) or '', content_float)
                        sheet.write(rowx, 12, (second_in_qty != 0 and second_in_qty * cost) or '', content_float)
                        sheet.write(rowx, 13, (out_qty != 0 and out_qty * cost) or '', content_float)
                        sheet.write(rowx, 14, (second_out_qty != 0 and second_out_qty * cost) or '', content_float)
                        sheet.write(rowx, 15, balance, content_float)
                        sheet.write(rowx, 16, second_balance or '', content_float)
                        sheet.write(rowx, 17, first_avail, content_float)
                        sheet.write(rowx, 18, second_first_avail, content_float)
                    else:
                        sheet.write(rowx, 9, (cost != 0 and cost) or '', content_float)
                        sheet.write(rowx, 10, (in_qty != 0 and in_qty * cost) or '', content_float)
                        sheet.write(rowx, 11, (out_qty != 0 and out_qty * cost) or '', content_float)
                        sheet.write(rowx, 12, balance or '', content_float)
                        sheet.write(rowx, 13, first_avail, content_float)
                rowx += 1

        sheet.write(rowx, 1, '', format_content_center)
        sheet.write(rowx, 2, '', format_content_center)
        sheet.write(rowx, 3, '', format_content_bold_text)
        sheet.write(rowx, 4, '', format_content_center)
        sheet.write(rowx, 5, '', format_content_center)
        sheet.write(rowx, 6, '', format_content_center)
        if self.report_type == 'owner':
            sheet.write(rowx, 7, _('Total'), format_content_center)
            if self.is_liter:
                sheet.write(rowx, 8, in_total, format_content_bold_float)
                sheet.write(rowx, 9, second_in_total, format_content_bold_float)
                sheet.write(rowx, 10, out_total, format_content_bold_float)
                sheet.write(rowx, 11, second_out_total, format_content_bold_float)
                sheet.write(rowx, 12, first_avail, format_content_bold_float)
                sheet.write(rowx, 13, second_first_avail, format_content_bold_float)
            else:
                sheet.write(rowx, 8, in_total, format_content_bold_float)
                sheet.write(rowx, 9, out_total, format_content_bold_float)
                sheet.write(rowx, 10, first_avail, format_content_bold_float)
            rowx += 1
            sheet.write(rowx, 2, _('Check: '), format_filter)
            sheet.write(rowx, 8, _('Real balance: '), format_filter)
            sheet.write(rowx, 8, _('Difference'), format_filter)
            sheet.write(rowx, 10, '', format_group)
            sheet.write(rowx, 10, '', format_group)
            rowx += 3
            sheet.write(rowx, 3, _('Product keeper comment: ................................................................................................'), format_filter)
            sheet.write(rowx + 1, 6, _('.........................................................................................................................'), format_filter)
            sheet.write(rowx + 2, 3, _('Conclusions and decisions on checks: ......................................................................................'), format_filter)
            sheet.write(rowx + 3, 6, _('.........................................................................................................................'), format_filter)
            sheet.write(rowx + 4, 3, _('Authorized checker: ...................................work.............................................'), format_filter)
            sheet.write(rowx + 6, 3, _('attended: signature: .............................................................../......................................./'), format_filter)
            sheet.write(rowx + 7, 6, _('signature: ........................................................................./......................................./'), format_filter)
            sheet.write(rowx + 8, 3, _('Product keeper: ......................................................................./......................................./'), format_filter)
            sheet.write(rowx + 9, 6, _('...................................................................................../......................................./'), format_filter)
            sheet.write(rowx + 10, 3, _('Date: .................................'), format_filter)

        elif self.report_type in ('price', 'cost'):
            sheet.write(rowx, 7, _('Income, Expense Total'), format_content_bold_text)
            sheet.write(rowx, 8, '', format_content_center)
            sheet.write(rowx, 9, '', format_content_center)
            if self.is_liter:
                sheet.write(rowx, 10, '', format_content_center)
                sheet.write(rowx, 11, in_total, format_content_bold_float)
                sheet.write(rowx, 12, '', format_content_center)
                sheet.write(rowx, 13, out_total, format_content_bold_float)
                sheet.write(rowx, 14, '', format_content_center)
                sheet.write(rowx, 15, '', format_content_center)
                sheet.write(rowx, 16, '', format_content_center)
                sheet.write(rowx, 17, '', format_content_center)
                sheet.write(rowx, 18, '', format_content_center)
            else:
                sheet.write(rowx, 10, in_total, format_content_bold_float)
                sheet.write(rowx, 11, out_total, format_content_bold_float)
                col = 11
                if self.report_type == 'price':
                    sheet.write(rowx, 12, change_total, format_content_bold_float)
                    col += 1
                sheet.write(rowx, col + 1, '', format_content_center)
                sheet.write(rowx, col + 2, '', format_content_center)
            if self.report_type == 'cost':
                rowx += 1
                last_total = last_total_qty = 0
                sheet.write(rowx, 1, u'X', format_content_center)
                sheet.write(rowx, 2, u'X', format_content_center)
                sheet.write(rowx, 3, u'X', format_content_center)
                sheet.write(rowx, 4, u'X', format_content_center)
                sheet.write(rowx, 5, u'X', format_content_center)
                sheet.write(rowx, 6, u'X', format_content_center)
                sheet.write(rowx, 7, u'', format_content_center)
                sheet.write(rowx, 8, (qty and qty) or '0.0', format_content_bold_float)
                col = 9
                if self.is_liter:
                    sheet.write(rowx, 9, (second_qty and second_qty) or '0.0', format_content_bold_float)
                    col += 1
                sheet.write(rowx, col, (last_cost and last_cost) or '0.0', format_content_bold_float)
                sheet.write(rowx, col+1, u'X', format_content_center)
                sheet.write(rowx, col+2, u'X', format_content_center)
                col += 2
                if self.is_liter:
                    sheet.write(rowx, col + 1, u'X', format_content_center)
                    sheet.write(rowx, col + 2, u'X', format_content_center)
                    col += 2
                sheet.write(rowx, col+1, (last_cost and first_avail and (last_cost * first_avail)) or '0.0', format_content_bold_float)
                if self.is_liter:
                    col += 1
                    sheet.write(rowx, col+1, u'X', format_content_center)
                sheet.write(rowx, col+2, first_avail, format_content_bold_float)
                if self.is_liter:
                    sheet.write(rowx, col+3, second_first_avail, format_content_bold_float)
            rowx += 3
            sheet.write(rowx, 3, _('Accountant: ......................................................................./......................................./'), format_filter)
            rowx += 2
            sheet.write(rowx, 3, _('Date: .................................'), format_filter)

        sheet.set_column(0, 0, 2)
        sheet.set_column(1, 1, 5)
        sheet.set_column(2, 4, 12)
        sheet.set_column(5, 5, 20)
        sheet.set_column(6, 6, 15)
        sheet.set_column(7, 7, 20)
        sheet.set_column(8, 9, 8)
        sheet.set_column(10, 18, 12)

        book.close()
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        return report_excel_output_obj.export_report()
