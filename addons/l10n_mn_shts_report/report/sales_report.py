# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
import xlsxwriter
from odoo import api, fields, models, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
import pytz
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT


class ReportSalesReport(models.TransientModel):
    """
        Борлуулалтын дэлгэрэнгүй тайлан
    """

    _inherit = 'report.sales'

    is_liter = fields.Boolean(string='See liter')

    # query - гээр мэдээлэл татахад 2дахь хэмжих нэгжийн тоог авдаг болгов
    def _get_query(self, select, _from, where, order_by,  select_two, group_by):
        
        if self.is_liter:
            if self.report_type != 'shipment': # Тайлангийн төрөл нь Тээвэрлэлтээс бусад үед борлуулалтын мөрийн 2дахь хэмжих нэгжийн тоог авна.
                select += ',sol.second_product_qty as second_pr_qty'
                select_two += ', SUM(sales_order_report.second_pr_qty) as second_pr_qty'
            else:
                select += ',sm.second_product_qty as second_pr_qty'
                select_two += ', SUM(sales_order_report.second_pr_qty) as second_pr_qty'
        res = super(ReportSalesReport, self)._get_query(select, _from, where, order_by,  select_two, group_by)
        return res
    
    # Тайлангийн дэд гарчигийн нийлбэр олохо 2дах хэмжих нэгжийг оруулахаар дарж бичив
    def get_sum_dict(self, sheet, record, warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, sale_qty, price, tax, price_tax, without_tax_discount, tax_discount, with_tax_discount, \
                        rev_qty, rev_price, rev_tax, rev_price_tax, net_qty, net_price, net_tax, net_price_tax, net_cost_price, cost_price_unit, net_profit, profit_of_unit, percent,  last_ware_rowx, \
                        last_location_rowx, last_cat_rowx, last_brand_rowx, last_salesman_rowx, last_salesteam_rowx, last_customer_rowx, last_supplier_rowx, lot, sale_tax_pay, format_title, format_sub_text_float ):
        second_pr_qty = 0
        if 'second_pr_qty' in record:
            second_pr_qty = record['second_pr_qty'] if record['second_pr_qty'] else 0
        warehouse_sum = self._fill_sum_value(warehouse_sum, sale_qty, price, tax, price_tax, without_tax_discount, tax_discount, with_tax_discount,
                                             rev_qty, rev_price, rev_tax, rev_price_tax, net_qty, net_price,
                                             net_tax, net_price_tax, net_cost_price, cost_price_unit, net_profit, profit_of_unit, percent, second_pr_qty)
        location_sum = self._fill_sum_value(location_sum, sale_qty, price, tax, price_tax, without_tax_discount, tax_discount, with_tax_discount,
                                            rev_qty, rev_price, rev_tax, rev_price_tax, net_qty, net_price,
                                            net_tax, net_price_tax, net_cost_price,  cost_price_unit, net_profit, profit_of_unit, percent, second_pr_qty)
        cat_sum = self._fill_sum_value(cat_sum, sale_qty, price, tax, price_tax, without_tax_discount, tax_discount, with_tax_discount,
                                       rev_qty, rev_price, rev_tax, rev_price_tax, net_qty,
                                       net_price, net_tax, net_price_tax, net_cost_price,  cost_price_unit, net_profit, profit_of_unit, percent, second_pr_qty)
        brand_sum = self._fill_sum_value(brand_sum, sale_qty, price, tax, price_tax, without_tax_discount, tax_discount, with_tax_discount,
                                         rev_qty, rev_price, rev_tax, rev_price_tax, net_qty,
                                         net_price, net_tax, net_price_tax, net_cost_price,  cost_price_unit, net_profit, profit_of_unit, percent, second_pr_qty)
        salesman_sum = self._fill_sum_value(salesman_sum, sale_qty, price, tax, price_tax, without_tax_discount, tax_discount, with_tax_discount,
                                            rev_qty, rev_price, rev_tax, rev_price_tax, net_qty, net_price,
                                            net_tax, net_price_tax, net_cost_price,  cost_price_unit, net_profit, profit_of_unit, percent, second_pr_qty)
        salesteam_sum = self._fill_sum_value(salesteam_sum, sale_qty, price, tax, price_tax, without_tax_discount, tax_discount, with_tax_discount,
                                             rev_qty, rev_price, rev_tax, rev_price_tax, net_qty, net_price,
                                             net_tax, net_price_tax, net_cost_price,  cost_price_unit, net_profit, profit_of_unit, percent, second_pr_qty)
        customer_sum = self._fill_sum_value(customer_sum, sale_qty, price, tax, price_tax, without_tax_discount, tax_discount, with_tax_discount,
                                            rev_qty, rev_price, rev_tax, rev_price_tax, net_qty, net_price,
                                            net_tax, net_price_tax, net_cost_price,  cost_price_unit, net_profit, profit_of_unit, percent, second_pr_qty)
        supplier_sum = self._fill_sum_value(supplier_sum, sale_qty, price, tax, price_tax, without_tax_discount, tax_discount, with_tax_discount,
                                            rev_qty, rev_price, rev_tax, rev_price_tax, net_qty, net_price,
                                            net_tax, net_price_tax, net_cost_price,  cost_price_unit, net_profit, profit_of_unit, percent, second_pr_qty)

        if last_ware_rowx != -1:
            self.get_sub_header(sheet, last_ware_rowx, '', record['lot_name'] if lot != 0 else '', warehouse_sum['qty'],
                                    warehouse_sum['sub_total'], warehouse_sum['tax'], warehouse_sum['total'], warehouse_sum['without_tax_discount'], warehouse_sum['tax_discount'], warehouse_sum['with_tax_discount'],
                                    warehouse_sum['rev_qty'], warehouse_sum['rev_sub_total'], warehouse_sum['rev_tax'], warehouse_sum['rev_total'],
                                    warehouse_sum['net_qty'], warehouse_sum['net_sub_total'], warehouse_sum['net_tax'], warehouse_sum['net_total'], warehouse_sum['net_cost_price'], 
                                    warehouse_sum['unit_price'], warehouse_sum['net_profit'], warehouse_sum['profit_of_unit'], warehouse_sum['percent'], format_title, format_sub_text_float, False, sale_tax_pay, warehouse_sum.get('second_pr_qty', False))
        if last_location_rowx != -1:
            self.get_sub_header(sheet, last_location_rowx, '', record['lot_name'] if lot != 0 else '', location_sum['qty'], 
                                    location_sum['sub_total'], location_sum['tax'],location_sum['total'], location_sum['without_tax_discount'], location_sum['tax_discount'], location_sum['with_tax_discount'],
                                    location_sum['rev_qty'], location_sum['rev_sub_total'], location_sum['rev_tax'], location_sum['rev_total'],
                                    location_sum['net_qty'], location_sum['net_sub_total'], location_sum['net_tax'], location_sum['net_total'], location_sum['net_cost_price'], 
                                    location_sum['unit_price'], location_sum['net_profit'], location_sum['profit_of_unit'], location_sum['percent'], format_title, format_sub_text_float, False, sale_tax_pay, location_sum.get('second_pr_qty', False))
        if last_cat_rowx != -1:
            self.get_sub_header(sheet, last_cat_rowx, '', record['lot_name'] if lot != 0 else '', cat_sum['qty'],
                                    cat_sum['sub_total'], cat_sum['tax'], cat_sum['total'], cat_sum['without_tax_discount'], cat_sum['tax_discount'], cat_sum['with_tax_discount'],
                                    cat_sum['rev_qty'], cat_sum['rev_sub_total'],cat_sum['rev_tax'], cat_sum['rev_total'], 
                                    cat_sum['net_qty'], cat_sum['net_sub_total'], cat_sum['net_tax'], cat_sum['net_total'], cat_sum['net_cost_price'], 
                                    cat_sum['unit_price'], cat_sum['net_profit'], cat_sum['profit_of_unit'], cat_sum['percent'], format_title, format_sub_text_float, False, sale_tax_pay, cat_sum.get('second_pr_qty', False))
        if last_brand_rowx != -1:
            self.get_sub_header(sheet, last_brand_rowx, '', record['lot_name'] if lot != 0 else '', brand_sum['qty'], 
                                    brand_sum['sub_total'], brand_sum['tax'], brand_sum['total'], brand_sum['without_tax_discount'], brand_sum['tax_discount'], brand_sum['with_tax_discount'],
                                    brand_sum['rev_qty'], brand_sum['rev_sub_total'], brand_sum['rev_tax'], brand_sum['rev_total'], 
                                    brand_sum['net_qty'], brand_sum['net_sub_total'], brand_sum['net_tax'], brand_sum['net_total'], brand_sum['net_cost_price'], 
                                    brand_sum['unit_price'], brand_sum['net_profit'], brand_sum['profit_of_unit'], brand_sum['percent'], format_title, format_sub_text_float, False, sale_tax_pay, brand_sum.get('second_pr_qty', False))

        if last_salesman_rowx != -1:
            self.get_sub_header(sheet, last_salesman_rowx, '', record['lot_name'] if lot != 0 else '', salesman_sum['qty'], 
                                    salesman_sum['sub_total'], salesman_sum['tax'], salesman_sum['total'], salesman_sum['without_tax_discount'], salesman_sum['tax_discount'], salesman_sum['with_tax_discount'],
                                    salesman_sum['rev_qty'], salesman_sum['rev_sub_total'], salesman_sum['rev_tax'], salesman_sum['rev_total'],
                                    salesman_sum['net_qty'], salesman_sum['net_sub_total'], salesman_sum['net_tax'], salesman_sum['net_total'], salesman_sum['net_cost_price'], 
                                    salesman_sum['unit_price'], salesman_sum['net_profit'], salesman_sum['profit_of_unit'], salesman_sum['percent'], format_title, format_sub_text_float, False, sale_tax_pay, salesman_sum.get('second_pr_qty', False))
        if last_salesteam_rowx != -1:
            self.get_sub_header(sheet, last_salesteam_rowx, '', record['lot_name'] if lot != 0 else '', salesteam_sum['qty'], 
                                    salesteam_sum['sub_total'], salesteam_sum['tax'], salesteam_sum['total'], salesteam_sum['without_tax_discount'], salesteam_sum['tax_discount'], salesteam_sum['with_tax_discount'],
                                    salesteam_sum['rev_qty'], salesteam_sum['rev_sub_total'], salesteam_sum['rev_tax'], salesteam_sum['rev_total'], 
                                    salesteam_sum['net_qty'], salesteam_sum['net_sub_total'], salesteam_sum['net_tax'], salesteam_sum['net_total'], salesteam_sum['net_cost_price'], 
                                    salesteam_sum['unit_price'], salesteam_sum['net_profit'], salesteam_sum['profit_of_unit'], salesteam_sum['percent'], format_title, format_sub_text_float, False, sale_tax_pay, salesteam_sum.get('second_pr_qty', False))
        if last_customer_rowx != -1:
            self.get_sub_header(sheet, last_customer_rowx, '', record['lot_name'] if lot != 0 else '', customer_sum['qty'], 
                                    customer_sum['sub_total'], customer_sum['tax'], customer_sum['total'], customer_sum['without_tax_discount'], customer_sum['tax_discount'], customer_sum['with_tax_discount'],
                                    customer_sum['rev_qty'], customer_sum['rev_sub_total'], customer_sum['rev_tax'], customer_sum['rev_total'], 
                                    customer_sum['net_qty'], customer_sum['net_sub_total'], customer_sum['net_tax'], customer_sum['net_total'], customer_sum['net_cost_price'], 
                                    customer_sum['unit_price'], customer_sum['net_profit'], customer_sum['profit_of_unit'], customer_sum['percent'], format_title, format_sub_text_float, False, sale_tax_pay, customer_sum.get('second_pr_qty', False))
        if last_supplier_rowx != -1:
            self.get_sub_header(sheet, last_supplier_rowx, '', record['lot_name'] if lot != 0 else '', supplier_sum['qty'], 
                                    supplier_sum['sub_total'], supplier_sum['tax'], supplier_sum['total'], supplier_sum['without_tax_discount'], supplier_sum['tax_discount'], supplier_sum['with_tax_discount'],
                                    supplier_sum['rev_qty'], supplier_sum['rev_sub_total'], supplier_sum['rev_tax'], supplier_sum['rev_total'], 
                                    supplier_sum['net_qty'], supplier_sum['net_sub_total'], supplier_sum['net_tax'], supplier_sum['net_total'], supplier_sum['net_cost_price'], 
                                    supplier_sum['unit_price'], supplier_sum['net_profit'], supplier_sum['profit_of_unit'], supplier_sum['percent'], format_title, format_sub_text_float, False, sale_tax_pay, supplier_sum.get('second_pr_qty', False))
        return sheet
    
    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        # НӨАТ төлдөг эсэх
        sale_tax_pay = False
        self.env.cr.execute("select * from account_config_settings where company_id = %s order by id DESC LIMIT 1" % self.company_id.id)
        account_setting = self.env.cr.dictfetchall()
        if account_setting:
            if not account_setting[0]['default_sale_tax_id']:
                sale_tax_pay = True

        # create name
        report_name = _('Sales Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        
        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_filter_center = book.add_format(ReportExcelCellStyles.format_filter_center)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_title_small = book.add_format(ReportExcelCellStyles.format_title_small)
        format_title_float = book.add_format(ReportExcelCellStyles.format_title_float)
        format_content_center = book.add_format(ReportExcelCellStyles.format_content_center)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_text_color = book.add_format(ReportExcelCellStyles.format_group_number)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_left = book.add_format(ReportExcelCellStyles.format_content_left)
        format_red_text = book.add_format(ReportExcelCellStyles.format_content_float_redcolor)
        format_sub_text_float = book.add_format(ReportExcelCellStyles.format_group_float)

        # Хэрэглэх хувьсагчид
        seq = 1
        rowx = 1
        colx = 0
        lot = 0 if not self.see_serial else 1 if self.lot_ids else 2
        locations = []
        total_tax = total_price_tax = total_qty = total_without_tax_discount = total_tax_discount = total_with_tax_discount = total_price = rev_price = rev_qty = rev_tax = rev_price_tax = 0
        total_rev_qty = total_rev_price = total_rev_tax = total_rev_price_tax = total_purchase_cost_row = total_standard_cost = 0
        net_total_qty = total_price_net = total_tax_net = total_price_tax_net = 0

        check_ids = []
        check_warehouse_ids = []
        check_location_ids = []
        check_cat_ids = []
        check_brand_ids = []
        check_salesman_ids = []
        check_salesteam_ids = []
        check_customer_ids = []
        check_supplier_ids = []
        last_ware_rowx = last_location_rowx = last_cat_rowx = last_brand_rowx = last_salesman_rowx = last_salesteam_rowx = last_customer_rowx = last_supplier_rowx = -1
        warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum = self.get_dict()

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(
            filename_prefix=('sales_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(time.strftime('%Y-%m-%d'))
        sheet.set_portrait()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.39, 0.39, 0.39, 0.39)  # 1cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.freeze_panes(9, 4)
        sheet.set_footer('&C&"Times New Roman"&10&P', {'margin': 0.1})

        # compute column
        sheet, colx_number = self.get_sheet(sheet)

        # create name
        sheet.write(rowx, 0, _(u'Company name: %s') % self.company_id.name, format_filter)
        rowx += 2
        if self.report_type == 'sales order':
            report_type = _('Sale Order')
        elif self.report_type == 'invoice':
            report_type = _('Invoice')
        elif self.report_type == 'shipment':
            report_type = _('Shipment')
        elif self.report_type == 'done':
            report_type = _('Done')
        elif self.report_type == 'loan':
            report_type = _('Loan')
            
        sheet = self.get_title(sheet, rowx, colx_number,report_name, format_name)
        rowx += 2
        sheet.write(rowx, 0, _('Duration: %s - %s') % (self.date_from, self.date_to), format_filter)
        rowx += 1
        sheet.write(rowx, 0, _('Report type: %s') % (report_type), format_filter)
        rowx += 1

        # Толгой хэсэг зурах
        sheet = self.get_header(sheet, rowx, format_title, format_title_small, lot, sale_tax_pay, True)
        rowx += 2
        temp = 1 if self.see_serial else 0
        total_second_product_qty = 0
        # get data
        records = self.get_data()
        # Тайлан өрөлт
        for record in records:
            sub_cat = 0
            second_pr_qty = 0
            if self.is_liter:
                if 'second_pr_qty' in record:
                    second_pr_qty = record['second_pr_qty'] if record['second_pr_qty'] else 0
                    total_second_product_qty += second_pr_qty
            if self.group:
                check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, \
                    warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, \
                    last_ware_rowx, last_location_rowx, last_cat_rowx, last_brand_rowx, last_salesman_rowx, last_salesteam_rowx, last_customer_rowx, last_supplier_rowx, \
                    rowx, sub_cat \
                    = self.stage_check('stage_one', self.stage_one, record,
                                       check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids,
                                       warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum,
                                       sheet, lot, format_content_text_color, format_sub_text_float,
                                       last_ware_rowx, last_location_rowx, last_cat_rowx, last_brand_rowx, last_salesman_rowx, last_salesteam_rowx, last_customer_rowx, last_supplier_rowx,
                                       rowx, sub_cat, sale_tax_pay)
                if self.stage_two:
                    check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, \
                        warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, \
                        last_ware_rowx, last_location_rowx, last_cat_rowx, last_brand_rowx, last_salesman_rowx, last_salesteam_rowx, last_customer_rowx, last_supplier_rowx, \
                        rowx, sub_cat \
                        = self.stage_check('stage_two', self.stage_two, record,
                                           check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids,
                                           warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum,
                                           sheet, lot, format_content_text_color, format_sub_text_float,
                                           last_ware_rowx, last_location_rowx, last_cat_rowx, last_brand_rowx, last_salesman_rowx, last_salesteam_rowx, last_customer_rowx, last_supplier_rowx,
                                           rowx, sub_cat, sale_tax_pay)
                if self.stage_three:
                    check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids, \
                        warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, \
                        last_ware_rowx, last_location_rowx, last_cat_rowx, last_brand_rowx, last_salesman_rowx, last_salesteam_rowx, last_customer_rowx, last_supplier_rowx, \
                        rowx, sub_cat \
                        = self.stage_check('stage_three', self.stage_three, record,
                                           check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids,
                                           warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum,
                                           sheet, lot, format_content_text_color, format_sub_text_float,
                                           last_ware_rowx, last_location_rowx, last_cat_rowx, last_brand_rowx, last_salesman_rowx, last_salesteam_rowx, last_customer_rowx, last_supplier_rowx,
                                           rowx, sub_cat, sale_tax_pay)
                        
            sheet, sale_qty, sale_price, price, tax, sale_tax, price_tax, sale_price_tax, without_tax_discount, tax_discount, with_tax_discount, \
                        rev_qty, rev_price, rev_tax, rev_price_tax, net_qty,net_price, net_tax, net_price_tax, net_cost_price, cost_price_unit, net_profit, profit_of_unit, percent, total_qty, total_price, total_tax, total_price_tax, total_without_tax_discount, total_tax_discount, total_with_tax_discount, total_rev_qty, total_rev_price, total_rev_tax,total_rev_price_tax, net_total_qty, total_price_net, total_tax_net, total_price_tax_net, total_purchase_cost_row, total_standard_cost = \
                        self.compute_data(sheet, rowx, seq, temp, record, format_content_center, format_content_left, format_content_text, format_content_float, format_red_text, lot, sale_tax_pay, check_ids, sub_cat,total_qty, total_price, total_tax, total_price_tax, total_without_tax_discount, total_tax_discount, total_with_tax_discount, total_rev_qty, total_rev_price, total_rev_tax,total_rev_price_tax, net_total_qty, total_price_net, total_tax_net, total_price_tax_net, total_purchase_cost_row, total_standard_cost, True)
            rowx += 1
            seq += 1    
                                
            sheet = self.get_sum_dict(sheet, record, warehouse_sum, location_sum, cat_sum, brand_sum, salesman_sum, salesteam_sum, customer_sum, supplier_sum, sale_qty, sale_price, sale_tax, sale_price_tax, without_tax_discount, tax_discount, with_tax_discount, \
                        rev_qty, rev_price, rev_tax, rev_price_tax, net_qty, net_price, net_tax, net_price_tax, net_cost_price, cost_price_unit, net_profit, profit_of_unit, percent, last_ware_rowx, \
                        last_location_rowx, last_cat_rowx, last_brand_rowx, last_salesman_rowx, last_salesteam_rowx, last_customer_rowx, last_supplier_rowx, lot, sale_tax_pay, format_title, format_sub_text_float )
            
        
            if self.group:
                check_ids, check_warehouse_ids, check_location_ids, check_cat_ids, check_brand_ids, check_salesman_ids, check_salesteam_ids, check_customer_ids, check_supplier_ids = self._get_append_sub_header(record)

        # Тайлангийн хөл дүнгүүдийг зурах
        sheet = self._get_footer_total_amount(sheet, rowx, format_title, format_title_float, total_qty, total_price, total_tax, total_price_tax, total_without_tax_discount, total_tax_discount, total_with_tax_discount, total_rev_qty, total_rev_price, total_rev_tax,\
                                total_rev_price_tax, net_total_qty, total_price_net, total_tax_net, total_price_tax_net, total_purchase_cost_row, total_standard_cost, lot, sale_tax_pay, True,total_second_product_qty)
        rowx += 3
        # END OF THE REPORT
        sheet = self.get_footer(sheet, rowx, colx_number ,(_('Made by')), format_filter_center)   
        rowx += 2
        sheet = self.get_footer(sheet, rowx, colx_number ,(_('Check by')), format_filter_center)  
        sheet.hide_gridlines(2)

        sheet.set_zoom(75)
        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()
    
    # Тайлангийн дэд гарчиг зурахад 2дахь хэмжих нэгж нэмэв
    def get_sub_header(self, sheet, rowx, category, serial_number, qty, sub_total, tax, total, without_tax_discount, tax_discount, with_tax_discount, rev_qty, rev_sub_total, rev_tax, rev_total, net_qty, net_sub_total, net_tax, net_total, net_cost_price, cost_price_unit, net_profit, profit_of_unit, percent, format_title, format_sub_text_float, key, sale_tax_pay, second_product_qty=0, colx_to_start=0):
        if self.is_liter:
            colx_to_start = 5 if self.see_serial else 4
            sheet.write(rowx, colx_to_start, second_product_qty, format_sub_text_float)
            colx_to_start += 1
        return super(ReportSalesReport, self).get_sub_header(sheet, rowx, category, serial_number, qty, sub_total, tax, total, without_tax_discount, tax_discount, with_tax_discount, rev_qty, rev_sub_total, rev_tax, rev_total, net_qty, net_sub_total, net_tax, net_total, net_cost_price, cost_price_unit, net_profit, profit_of_unit, percent, format_title, format_sub_text_float, key, sale_tax_pay, colx_to_start=colx_to_start)

    @api.multi
    def get_header(self, sheet, rowx, format_title, format_title_small, lot, sale_tax_pay, key, colx_to_start=0):
        #Тайлангийн толгой зурахад 2дахь хэмжих нэгж нэмэв
        if key:
            colx = 0
            sheet.merge_range(rowx, colx, rowx + 1, colx, _('№'), format_title)
            colx += 1
            sheet.merge_range(rowx, colx, rowx + 1, colx, _('Product code'), format_title)
            colx += 1
            sheet.merge_range(rowx, colx, rowx + 1, colx, _('Product name'), format_title)
            colx += 1
            sheet.merge_range(rowx, colx, rowx + 1, colx, _('Unit of measure'), format_title)
            colx += 1
            if lot != 0:  # Цувралтай бол цувралын нэрийг нэмнэ
                sheet.merge_range(rowx, colx, rowx + 1, colx, _('Lot Name'), format_title)
                colx += 1
        if colx_to_start:
            colx = colx_to_start
        else:
            colx = 5 if self.see_serial else 4
        sales_colx = colx
        plus = 1 if sale_tax_pay else 3
        is_liter_colx = 0
        if self.is_liter:
            is_liter_colx += 1
        sheet.merge_range(rowx, colx, rowx, colx + plus + is_liter_colx, _('Sales'), format_title)
        colx += 2 if sale_tax_pay else 4
        colx += is_liter_colx
        sheet.merge_range(rowx, colx, rowx, colx + 2, _('Discount'), format_title)
        colx += 2 if sale_tax_pay else 3
        sheet.merge_range(rowx, colx, rowx, colx + plus, _('Reverse'), format_title)
        colx += 2 if sale_tax_pay else 4
        sheet.merge_range(rowx, colx, rowx, colx + plus, _('Net Sales'), format_title)
        colx += 2 if sale_tax_pay else 4
        if self.see_profit:
            sheet.merge_range(rowx, colx, rowx, colx + 4, _('Profit without Tax'), format_title)
        colx += 1
        if self.is_liter:
            sheet.write(rowx + 1, sales_colx, _('Liter Amount'), format_title)
            sales_colx += 1
        sheet.write(rowx + 1, sales_colx, _('Quantity'), format_title)
        sales_colx += 1
        if not sale_tax_pay:
            sheet.write(rowx + 1, sales_colx, _('Cost without Tax'), format_title)
            sales_colx += 1
            sheet.write(rowx + 1, sales_colx, _('Tax'), format_title)
            sales_colx += 1
        sheet.write(rowx + 1, sales_colx, _('Cost with Tax'), format_title)
        sales_colx += 1       
        if not sale_tax_pay:
            sheet.write(rowx + 1, sales_colx, _('Discount without Tax'), format_title)
            sales_colx += 1
            sheet.write(rowx + 1, sales_colx, _('Tax'), format_title)
            sales_colx += 1
        sheet.write(rowx + 1, sales_colx, _('Discount with Tax'), format_title)
        sales_colx += 1
        sheet.write(rowx + 1, sales_colx, _('Quantity'), format_title)
        sales_colx += 1
        if not sale_tax_pay:
            sheet.write(rowx + 1, sales_colx, _('Cost without Tax'), format_title)
            sales_colx += 1
            sheet.write(rowx + 1, sales_colx, _('Tax'), format_title)
            sales_colx += 1
        sheet.write(rowx + 1, sales_colx, _('Cost with Tax'), format_title)
        sales_colx += 1
        sheet.write(rowx + 1, sales_colx, _('Quantity'), format_title)
        sales_colx += 1
        if not sale_tax_pay:
            sheet.write(rowx + 1, sales_colx, _('Cost without Tax'), format_title)
            sales_colx += 1
            sheet.write(rowx + 1, sales_colx, _('Tax'), format_title)
            sales_colx += 1
        sheet.write(rowx + 1, sales_colx, _('Cost with Tax'), format_title)
        sales_colx += 1
        if self.see_profit:
            sheet.write(rowx + 1, sales_colx, _('Total Purchase Cost'), format_title)
            sales_colx += 1
            sheet.write(rowx + 1, sales_colx, _('Purchase Cost'), format_title)
            sales_colx += 1
            sheet.write(rowx + 1, sales_colx, _('Net Profit'), format_title)
            sales_colx += 1
            sheet.write(rowx + 1, sales_colx, _('Profit for unit'), format_title)
            sales_colx += 1
            sheet.write(rowx + 1, sales_colx, _('Percent'), format_title)
        return sheet
    
    # Өгөгдөл зурахад 2дахь хэмжих нэгж нэмэв
    def compute_data(self, sheet, rowx, seq, temp, record, format_content_center, format_content_left, format_content_text, format_content_float, format_red_text, lot, sale_tax_pay, check_ids, sub_cat,total_qty, total_price, total_tax, total_price_tax, total_without_tax_discount, total_tax_discount, total_with_tax_discount, total_rev_qty, total_rev_price, total_rev_tax,total_rev_price_tax, net_total_qty, total_price_net, total_tax_net, total_price_tax_net, total_purchase_cost_row, total_standard_cost, key, colx_to_start=0 ): 
        if self.is_liter:
            colx_to_start = 5 if self.see_serial else 4
            second_pr_qty = 0
            if 'second_pr_qty' in record:
                second_pr_qty = record['second_pr_qty'] if record['second_pr_qty'] else 0
            sheet.write(rowx, colx_to_start, second_pr_qty, format_content_float)
            colx_to_start += 1
            temp += 1
        return super(ReportSalesReport, self).compute_data(sheet, rowx, seq, temp, record, format_content_center, format_content_left, format_content_text, format_content_float, format_red_text, lot, sale_tax_pay, check_ids, sub_cat,total_qty, total_price, total_tax, total_price_tax, total_without_tax_discount, total_tax_discount, total_with_tax_discount, total_rev_qty, total_rev_price, total_rev_tax,total_rev_price_tax, net_total_qty, total_price_net, total_tax_net, total_price_tax_net, total_purchase_cost_row, total_standard_cost, key, colx_to_start=colx_to_start)
    
    #Тайлангийн хөл дүнг зурахад 2дахь хэмжих нэгж нэмэв
    def _get_footer_total_amount(self, sheet, rowx, format_title, format_title_float, total_qty, total_price, total_tax, total_price_tax, total_without_tax_discount, total_tax_discount, total_with_tax_discount, total_rev_qty, total_rev_price, total_rev_tax,\
                                total_rev_price_tax, net_total_qty, total_price_net, total_tax_net, total_price_tax_net, total_purchase_cost_row, total_standard_cost,lot,sale_tax_pay, key, total_second_product_qty, colx_to_start = 0):
        if self.is_liter:
            colx_to_start = 5 if self.see_serial else 4
            sheet.write(rowx, colx_to_start, total_second_product_qty, format_title_float)
            colx_to_start += 1
        return super(ReportSalesReport, self)._get_footer_total_amount(sheet, rowx, format_title, format_title_float, total_qty, total_price, total_tax, total_price_tax, total_without_tax_discount, total_tax_discount, total_with_tax_discount, total_rev_qty, total_rev_price, total_rev_tax,\
                                total_rev_price_tax, net_total_qty, total_price_net, total_tax_net, total_price_tax_net, total_purchase_cost_row, total_standard_cost, lot, sale_tax_pay, key, colx_to_start = colx_to_start)
    
    #Тайлангийн бүлэглэлтийн нийлбэрийг олоход 2дахь хэмжих нэгж нэмэв
    def _fill_sum_value(self, dict, qty, sub_total, tax, total, without_tax_discount, tax_discount, with_tax_discount, rev_qty, rev_sub_total, rev_tax, rev_total, net_qty, net_sub_total, net_tax, net_total, net_cost_price, cost_price_unit, net_profit, profit_of_unit, percent, second_pr_qty=0):
        dict = super(ReportSalesReport, self)._fill_sum_value(dict, qty, sub_total, tax, total, without_tax_discount, tax_discount, with_tax_discount, rev_qty, rev_sub_total, rev_tax, rev_total, net_qty, net_sub_total, net_tax, net_total, net_cost_price, cost_price_unit, net_profit, profit_of_unit, percent)
        if self.is_liter:
            if 'second_pr_qty' in dict:
                dict['second_pr_qty'] += second_pr_qty
            else:
                dict['second_pr_qty'] = second_pr_qty
        return dict
    
    def _fill_zero(self, dict):
        dict = super(ReportSalesReport, self)._fill_zero(dict)
        if self.is_liter:
            if 'second_pr_qty' in dict:
                dict['second_pr_qty'] = 0
        return dict
