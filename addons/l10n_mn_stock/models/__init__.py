# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import stock_transit_order
import res_users
import stock_transit_order_line
import procurement_order
import stock_picking
import stock_warehouse
import stock_inventory
import stock_quant
import product
import res_company
import stock_config_settings
import stock_move
import stock_scrap