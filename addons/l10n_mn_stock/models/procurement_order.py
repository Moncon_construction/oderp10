# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class ProcurementOrder(models.Model):
    _inherit = 'procurement.order'
    transit_line_id = fields.Many2one(
        'stock.transit.order.line', string='Transit Order Line')
