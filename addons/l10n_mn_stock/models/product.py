# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models , api , _
from datetime import datetime, timedelta
import odoo.addons.decimal_precision as dp
from odoo.addons.l10n_mn_web.models.time_helper import *


class ProductTemplate(models.Model):
    _inherit = 'product.template'
    
    size_id = fields.Many2one('product.size', string='Size name', track_visibility='onchange')
    product_quantity_in_box = fields.Integer('Product Quantity in a box', track_visibility='onchange')
    worthy_balance = fields.Integer(string='Worthy Balance of product', default=-1)

    def _compute_quantities(self):
        if self.env.user.company_id.availability_compute_method == 'stock_quant':
            res = super(ProductTemplate, self)._compute_quantities()
        elif self.env.user.company_id.availability_compute_method == 'stock_move':
            res = self._compute_quantities_dict()
            for template in self:
                locations = []
                for warehouse_id in self.env.user.allowed_warehouses.filtered(lambda l: l.company_id.id == self.env.user.company_id.id):
                    locations.append(warehouse_id.lot_stock_id.id)
                
                results = None
                if template.product_variant_id and locations:
                    self._cr.execute("""SELECT SUM(product_qty) as qty FROM stock_by_location_report 
                                            WHERE product_id=%s and location_id in (%s) group by product_id""" % (template.product_variant_id.id, ','.join(str(l_id) for l_id in locations)))
                    results = self._cr.dictfetchall()
                template.qty_available = results[0]['qty'] if results else 0
                template.virtual_available = res[template.id]['virtual_available']
                template.incoming_qty = res[template.id]['incoming_qty']
                template.outgoing_qty = res[template.id]['outgoing_qty']
    
    @api.multi
    def check_product_availability(self):
        mail_users = ''

        worthy_balance_list = []
        user_ids = filter(lambda x: x.has_group('l10n_mn_stock.group_view_available_qty') and x.has_group('stock.group_stock_manager'),
                          self.env['res.users'].sudo().search([]))
        uis = [user.id for user in user_ids]
        
        
        
        employees = self.env['hr.employee'].search([('resource_id.user_id', 'in', uis)])
        for emp in employees:
            
            outgoing_email_ids = self.env['ir.mail_server'].sudo().search([])
            if len(employees) == 1:
                mail_users += emp.work_email
            else :
                mail_users += emp.work_email + " , "
            
     # get products

            products = []
            stocks = []
            datetimes = datetime.now()
            
            for stock in self.env['stock.warehouse'].search([]):
                
                stocks.append(stock.id)
                
                if stock.ids in emp.user_id.allowed_warehouses.ids:
                    self.env.cr.execute(
                        'select sum(qty) from stock_quant where location_id in (select lot_stock_id from stock_warehouse where id = ' + str(
                            stock.id) + ") and in_date <= '" + str(datetime.now()) + "' and product_id = " + str(
                            product.id))

    
                for product in self.env['product.template'].search([]):
                    if product.qty_available != 0 and product.worthy_balance >= product.qty_available and product not in products:
                        products.append(product)
    
           
                outgoing_email = self.env['ir.mail_server'].sudo().search([('id', '=', outgoing_email_ids[0].id)])
        
                if len(mail_users) > 0 and products and outgoing_email:
    
                    worthy_balance_list = ""
                    for product in products:
                        worthy_balance_list += u'<tr>'
                        worthy_balance_list +=u'<td>%s</td>'%_(product.name)
                        worthy_balance_list +=u'<td>%s</td>'%_(product.qty_available)
                        worthy_balance_list +=u'<td>%s</td>'%_(product.worthy_balance)
                        worthy_balance_list +=u'</tr>'
    
                body_html = _('Hello, Remain of following product is running out.')
                body_html += u'<table style="border: 1px solid black; border-collapse: collapse;">' 
                body_html += u'<tr>' 
                body_html += u'<th style="border: 1px solid black; border-collapse: collapse;">%s</th>' %_('Product Name')
                body_html += u'<th style="border: 1px solid black; border-collapse: collapse;">%s</th>'%_('Available Quantity On Hand')
                body_html += u'<th style="border: 1px solid black; border-collapse: collapse;">%s</th>'%_('Worthy Balance')
                body_html += u'</tr> %s'%_(worthy_balance_list)
                body_html += u'</table>' 
                body_html += u'<br/>'
    
                vals = {
    
                    'subject': _('E-mail for informing on product stock run out.'),
                    'body_html': body_html,
                    'email_to': mail_users,
                    'email_from': outgoing_email.smtp_user,
    
                    }
        email_ids = self.env['mail.mail'].create(vals)
        if email_ids != 0:
            email_ids.send()

        return {'type': 'ir.actions.act_window_close'}
    
    @api.multi
    def action_open_quants(self):
        '''
        Зөвшөөрөгдсөн агуулахын байрлалуудаар барааны гарт байгаа тоо хэмжээг харуулдаг болгов.
        '''
        locations = []
        for warehouse_id in self.env.user.allowed_warehouses.filtered(lambda l: l.company_id.id == self.env.user.company_id.id):
            locations.append(warehouse_id.lot_stock_id.id)
        
        if self.env.user.company_id.availability_compute_method == 'stock_quant':
            res = super(ProductTemplate, self).action_open_quants()
            products = self.mapped('product_variant_ids')
            res['domain'] = [('product_id', 'in', products.ids),('location_id', 'in', locations)]
            return res
        
        elif self.env.user.company_id.availability_compute_method == 'stock_move':
            products = self.mapped('product_variant_ids')
            
            action = self.env.ref('l10n_mn_stock.act_product_tmpl_by_location').read()[0]
            action['domain'] = [('product_id', 'in', products.ids),('location_id', 'in', locations)]
            return action

class ProductProduct(models.Model):
    _inherit = "product.product"

    standard_price = fields.Float(
        'Cost', company_dependent=True,
        digits=dp.get_precision('Product Price'),
        groups="l10n_mn_stock.group_stock_view_cost",
        help="Cost of the product template used for standard stock valuation in accounting and used as a base price on purchase orders. "
             "Expressed in the default unit of measure of the product.")
    
    configure_allowed_product_on_wh = fields.Boolean(compute='_compute_configure_allowed_product_on_wh', readonly=True, string="Configure Allowed Product on Warehouse")
    allowed_warehouse_ids = fields.Many2many('stock.warehouse', 'wh_products_rel', 'product_id', 'wh_id', string='Allowed Warehouses')
    
    @api.multi
    def product_open_quants_mn(self):
        locations = []
        for warehouse_id in self.env.user.allowed_warehouses.filtered(lambda l: l.company_id.id == self.env.user.company_id.id):
            locations.append(warehouse_id.lot_stock_id.id)
        if self.env.user.company_id.availability_compute_method == 'stock_quant':
            action = self.env.ref('stock.product_open_quants').read()[0]
            action['domain'] = [('product_id', 'in', self.ids),('location_id', 'in', locations)]
            return action
        elif self.env.user.company_id.availability_compute_method == 'stock_move':
            action = self.env.ref('l10n_mn_stock.act_product_by_location').read()[0]
            action['domain'] = [('product_id', 'in', self.ids),('location_id', 'in', locations)]
            return action
            
    
    @api.depends('stock_quant_ids', 'stock_move_ids')
    def _compute_quantities(self):
        if self.env.user.company_id.availability_compute_method == 'stock_quant':
            res = super(ProductProduct, self)._compute_quantities()
        elif self.env.user.company_id.availability_compute_method == 'stock_move':
            res = self._compute_quantities_dict(self._context.get('lot_id'), self._context.get('owner_id'), self._context.get('package_id'), self._context.get('from_date'), self._context.get('to_date'))
            for product in self:
                
                locations = []
                for warehouse_id in self.env.user.allowed_warehouses.filtered(lambda l: l.company_id.id == self.env.user.company_id.id):
                    locations.append(warehouse_id.lot_stock_id.id)
                
                results = None
                if locations:
                    self._cr.execute("""SELECT SUM(product_qty) as qty FROM stock_by_location_report 
                                            WHERE product_id=%s and location_id in (%s) group by product_id""" % (product.id, ','.join(str(l_id) for l_id in locations)))
                    results = self._cr.dictfetchall()
                    
                product.qty_available = results[0]['qty'] if results else 0
                product.incoming_qty = res[product.id]['incoming_qty']
                product.outgoing_qty = res[product.id]['outgoing_qty']
                product.virtual_available = res[product.id]['virtual_available']
    
    def _compute_configure_allowed_product_on_wh(self):
        for obj in self:
            obj.configure_allowed_product_on_wh = obj.product_tmpl_id.company_id.configure_allowed_product_on_wh if obj.product_tmpl_id and obj.product_tmpl_id.company_id else self.env.user.company_id.configure_allowed_product_on_wh
            
    def get_qty_availability(self, location_ids, date, state = ['done'], qty_dp_digit=False):
        # Тухайн байрлал дээрх нийт боломжит тоо хэмжээг авах функц
        self.ensure_one()
        if location_ids and date:
            if len(str(date)) < 19:
                date = get_display_day_to_user_day('%s 23:59:59' % str(date), self.env.user)
            date = get_day_by_user_timezone(str(date)[:19], self.env.user)
            location_ids = ", ".join(str(id) for id in location_ids)
            qty_dp_digit = (self.env['decimal.precision'].precision_get('Product Unit of Measure') or 2) if not qty_dp_digit else qty_dp_digit
            query_date = ("AND m.date <= '%s'" % date) if self.env.user.company_id.availability_check_date == 'record_date' else ''
            history_date = ("AND date <= '%s'" % date) if self.env.user.company_id.availability_check_date == 'record_date' else ''
            state1 = ','.join("'" + s + "'" for s in state)
            qry = ''
            if self.env.user.company_id.availability_compute_method == 'stock_move' or self._context.get('calculate_from_move'):
                qry = """
                    SELECT ROUND(COALESCE(SUM(table1.in_qty) - SUM(table1.out_qty))::DECIMAL, %s) AS lasts_qty
                    FROM
                    (
                        SELECT 
                            /* Тухайн байрлалд орж ирсэн /done/ хөдөлгөөн буюу орлого */
                            CASE WHEN m.location_dest_id IN (%s) AND m.state = 'done'
                                THEN COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0) ELSE 0 END AS in_qty,
                            /* Тухайн байрлалаас гарсан хөдөлгөөн + гарахаар нөөцөлсөн /done + assigned/ хөдөлгөөн */
                            CASE WHEN m.location_id IN (%s) AND m.state IN (%s) 
                                THEN COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0) ELSE 0 END AS out_qty
                        FROM stock_move m
                        LEFT JOIN product_product pp ON (pp.id = m.product_id)
                        LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id)
                        LEFT JOIN product_uom uom_m ON (uom_m.id = m.product_uom)
                        LEFT JOIN product_uom uom_t ON (uom_t.id = pt.uom_id)
                        WHERE (m.location_id IN (%s) OR m.location_dest_id IN (%s)) AND m.location_id != m.location_dest_id
                        AND m.state IN (%s) %s AND pp.id = %s
                    ) table1
                """ % (qty_dp_digit, location_ids, location_ids, state1, location_ids, location_ids, state1, query_date, self.id)
            else:
                check_assigned_sub_qry = ""
                if state == ['done']:
                    qry = """
                        SELECT ROUND(SUM(quantity)::DECIMAL, %s) AS lasts_qty
                        FROM 
                        (
                            /* Зөвхөн 'done' хөдөлгөөнөөс тооцдог */
                            SELECT SUM(quantity) AS quantity
                            FROM stock_history 
                            WHERE location_id IN (%s) %s AND product_id = %s
                        ) AS table1
                    """ % (qty_dp_digit, location_ids, history_date, self.id)
                elif state == ['assigned']:
                    qry = """
                        /* Тухайн байрлалаас гарахаар нөөцөлсөн хөдөлгөөнүүдийг тооцож хасах */
                        SELECT ROUND(sum(-COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0))) AS lasts_qty
                        FROM stock_move m
                        LEFT JOIN product_product pp ON (pp.id = m.product_id)
                        LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id)
                        LEFT JOIN product_uom uom_m ON (uom_m.id = m.product_uom)
                        LEFT JOIN product_uom uom_t ON (uom_t.id = pt.uom_id)
                        WHERE m.location_id IN (%s) AND m.location_id != m.location_dest_id
                        AND m.state IN (%s) %s AND pp.id = %s
                    """ % (location_ids, state1, query_date, self.id)
                elif state == ['assigned', 'done']:
                    check_assigned_sub_qry = """
                        UNION 
            
                        /* Тухайн байрлалаас гарахаар нөөцөлсөн хөдөлгөөнүүдийг тооцож хасах */
                        SELECT -COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0) AS quantity
                        FROM stock_move m
                        LEFT JOIN product_product pp ON (pp.id = m.product_id)
                        LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id)
                        LEFT JOIN product_uom uom_m ON (uom_m.id = m.product_uom)
                        LEFT JOIN product_uom uom_t ON (uom_t.id = pt.uom_id)
                        WHERE m.location_id IN (%s) AND m.location_id != m.location_dest_id
                        AND m.state = 'assigned' %s AND pp.id = %s
                    """ % (location_ids, query_date, self.id)
                    qry = """
                        SELECT ROUND(SUM(quantity)::DECIMAL, %s) AS lasts_qty
                        FROM 
                        (
                            /* Зөвхөн 'done' хөдөлгөөнөөс тооцдог */
                            SELECT SUM(quantity) AS quantity
                            FROM stock_history 
                            WHERE location_id IN (%s) %s AND product_id = %s
                            
                            %s
                        ) AS table1
                    """ % (qty_dp_digit, location_ids, history_date, self.id, check_assigned_sub_qry)
            self._cr.execute(qry)
            results = self._cr.dictfetchall()
            if results and len(results) > 0 and results[0]['lasts_qty']:
                return results[0]['lasts_qty']

        return 0
    
    def _get_domain_locations(self):
        '''
        Зөвшөөрөгдсөн агуулахын байрлалуудаар барааны гарт байгаа тоо хэмжээ гардаг болгов.
        '''
        Warehouse = self.env['stock.warehouse']
        location_ids = []
        for warehouse_id in self.env.user.allowed_warehouses.filtered(lambda l: l.company_id.id == self.env.user.company_id.id):
            location_ids.append(warehouse_id.view_location_id.id)
        return self._get_domain_locations_new(location_ids, company_id=self.env.context.get('force_company', False), compute_child=self.env.context.get('compute_child', True))
