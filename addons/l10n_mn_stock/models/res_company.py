# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _


class Company(models.Model):
    _inherit = 'res.company'

    show_default_code_on_report = fields.Boolean('Show default code on Report', default=True)
    show_barcode_code_on_report = fields.Boolean('Show barcode on Report')

    show_box_number_on_report = fields.Boolean('Product Quintity in a box')
    show_tax_on_report = fields.Boolean('Show tax on report')
    show_sales_team_on_report = fields.Boolean('Show sales team on report')
    availability_compute_method = fields.Selection([
        ('stock_quant', "Compute from stock quant (quicker)"),
        ('stock_move', "Compute from stock move")
        ], "Availability Computation Method", default='stock_quant')
    show_seller_info = fields.Boolean('Show seller info')
    show_warehouse = fields.Boolean('Show warehouse')
    transit_order_types = fields.Selection([('transit_order_quant', "Transit order quant"),
                                            ('transit_order_not_quant', "Transit order not quant")], default='transit_order_quant', string="Transit order")
    configure_allowed_product_on_wh = fields.Boolean(string="Configure Allowed Product on Warehouse", default=False)
    stock_move_approved_allowed_user = fields.Boolean(string='Approve Allowed User on Stock Move', default=False, help="When this setting is used, a field appears for the system users to select the approved repositories. The customer may not approve the stock move of warehouses other than those warehouses.")

    assign_date = fields.Selection([
        ('now', _('Assign as of today')),
        ('move_date', _('Assign as of the date of the move'))
    ], _('Stock Move Assign Date'), default='move_date')
    
    availability_check_date = fields.Selection([
        ('record_date', _('Check available quantity as of the date of the record')),
        ('all_time', _('Check available quantity for all time'))
    ], _('Availability Check Date'), default='record_date')