# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
import logging
_logger = logging.getLogger(__name__)


class InheritedResUsers(models.Model):
    _inherit = 'res.users'

    allowed_warehouses = fields.Many2many('stock.warehouse', 'res_users_warehouses_rel', 'uid', 'wid', string='Allowed Warehouses')
    approved_warehouses = fields.Many2many('stock.warehouse', 'res_users_approved_warehouses_rel', string='Approved Warehouses')

    @api.multi
    def write(self, vals):
        log_obj = self.env['res.users.change.log']
        for user in self:
            for k, v in vals.items():
                if k == 'allowed_warehouses':
                    old = _('Allowed Warehouses') + ' / '
                    new = _('Allowed Warehouses') + ' / '
                    if self.allowed_warehouses:
                        for warehouse in self.allowed_warehouses:
                            old += warehouse.name + ', '
                    if v[0][2]:
                        warehouses = self.env['stock.warehouse'].search([('id', 'in', v[0][2])])
                        for warehouse in warehouses:
                            new += warehouse.name + ', '
                    log_vals = {
                        'log_user_id': user.id,
                        'old_settings': old,
                        'new_settings': new
                    }
                    log_obj.create(log_vals)
        res = super(InheritedResUsers, self).write(vals)
        return res

