# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _


class StockSettings(models.TransientModel):
    _inherit = 'stock.config.settings'

    show_box_number_on_report = fields.Boolean(related='company_id.show_box_number_on_report', string="Show box number on Report")
    show_barcode_code_on_report = fields.Boolean(related='company_id.show_barcode_code_on_report', string="Show barcode on Report")
    show_default_code_on_report = fields.Boolean(related='company_id.show_default_code_on_report', string="Show default code on Report")
    show_tax_on_report = fields.Boolean(related='company_id.show_tax_on_report', string="Show tax on report")
    show_sales_team_on_report = fields.Boolean(related='company_id.show_sales_team_on_report',string="Show sales team on report")
    return_type_setting = fields.Selection([
        (0, "Return type will not be selected"),
        (1, "Return type will selected")
        ], "Return type")
    module_l10n_mn_stock_return_type = fields.Boolean(string='Stock Return Type')
    show_seller_info = fields.Boolean('Show seller info', related="company_id.show_seller_info")
    show_warehouse = fields.Boolean('Show warehouse', related="company_id.show_warehouse")
    transit_order_type = fields.Selection([('transit_order_not_quant', "Transit order not quant"),
                                           ('transit_order_quant', "Transit order quant")], string="Transit order", related="company_id.transit_order_types")
    configure_allowed_product_on_wh = fields.Boolean(related="company_id.configure_allowed_product_on_wh", string="Configure Allowed Product on Warehouse")
    stock_move_approved_allowed_user = fields.Boolean(related="company_id.stock_move_approved_allowed_user", string="Approve Allowed User on Stock Move")

    assign_date = fields.Selection([
        ('now', _('Assign as of today')),
        ('move_date', _('Assign as of the date of the move'))
    ], _('Stock Move Assign Date'), default='move_date', related="company_id.assign_date")

    availability_check_date = fields.Selection([
        ('record_date', _('Check available quantity as of the date of the record')),
        ('all_time', _('Check available quantity for all time'))
    ], _('Availability Check Date'), default='record_date', related='company_id.availability_check_date')
    
    @api.multi
    def set_sale_cancel_reason(self):
        return self.env['ir.values'].sudo().set_default(
            'stock.config.settings', 'return_type_setting', self.return_type_setting)
        
    @api.onchange('return_type_setting')
    def _onchange_return_type_setting(self):
        for main in self:
            if main.return_type_setting == 1:
                main.module_l10n_mn_stock_return_type = 1
            else:
                main.module_l10n_mn_stock_return_type = 0
                
    @api.model
    def get_default_company_product_code(self, fields):
        company = self.env.user.company_id
        return {
            'show_barcode_code_on_report': company.show_barcode_code_on_report,
            'show_default_code_on_report': company.show_default_code_on_report,
            'show_box_number_on_report': company.show_box_number_on_report,
            'show_tax_on_report':company.show_tax_on_report,
            'show_sales_team_on_report': company.show_sales_team_on_report,

        }