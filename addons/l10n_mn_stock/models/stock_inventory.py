# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models
from odoo.tools.translate import _
import odoo.addons.decimal_precision as dp  # @UnresolvedImport
from odoo.exceptions import UserError, ValidationError

import logging
from pydoc import locate
_logger = logging.getLogger(__name__)

class StockInventory(models.Model):
    _inherit = "stock.inventory"

    @api.multi
    def post_inventory(self):
        res = super(StockInventory, self).post_inventory()
        if self.accounting_date:
            self.mapped('move_ids').filtered(lambda move: move.state == 'done').write({'date': self.accounting_date})
        return res

    @api.onchange('filter')
    def onchange_filter(self):
        super(StockInventory, self).onchange_filter()
        if self.filter in ('product'):
            self.exhausted = False

    @api.depends('state')
    def _get_invoiced(self):
        for inventory in self:
            invoice_ids = inventory.line_ids.mapped('invoice_lines').mapped(
                'invoice_id').filtered(lambda r: r.type in ['out_invoice', 'out_refund'])
            refunds = invoice_ids.search([('origin', 'like', inventory.name)]).filtered(
                lambda r: r.type in ['out_invoice', 'out_refund'])
            invoice_ids |= refunds.filtered(lambda r: inventory.name in [
                                            origin.strip() for origin in r.origin.split(',')])
            refund_ids = self.env['account.invoice'].browse()
            if invoice_ids:
                for inv in invoice_ids:
                    refund_ids += refund_ids.search([('type', '=', 'out_refund'), ('origin', '=',
                                                                                   inv.number), ('origin', '!=', False), ('journal_id', '=', inv.journal_id.id)])

            inventory.update({
                'invoice_count': len(set(invoice_ids.ids + refund_ids.ids)),
                'invoice_ids': invoice_ids.ids + refund_ids.ids,
            })

    @api.multi
    def _show_cost_button(self):
        for inv in self:
            show = False
            if inv.state == 'counted':
                for move in inv.move_ids:
                    if move.location_id.usage == 'inventory':
                        show = True
                        break
                inv.show_cost_button = show
    
    @api.model
    def _default_location_id(self):
        company_user = self.env.user.company_id
        user = self.env['res.users'].browse(self._uid)
        warehouse = self.env['stock.warehouse'].search([('company_id', '=', company_user.id),('id','in', user.allowed_warehouses.ids)], limit=1)
        if warehouse:
            return warehouse.lot_stock_id.id
        else:
            raise UserError(_('You must define a warehouse for the company: %s.') % (company_user.name,))
        
    state = fields.Selection(selection_add=[('counted', 'Counted')])
    invoice_count = fields.Integer(string='# of Invoices', compute='_get_invoiced', readonly=True)
    invoice_ids = fields.Many2many('account.invoice', string='Invoices', compute="_get_invoiced", readonly=True, copy=False)
    invoice_status = fields.Selection([
        ('upselling', 'Upselling Opportunity'),
        ('invoiced', 'Fully Invoiced'),
        ('to invoice', 'To Invoice'),
        ('no', 'Nothing to Invoice')
    ], string='Invoice Status', compute='_get_invoiced', store=True, readonly=True)
    show_cost_button = fields.Boolean(compute=_show_cost_button, store=False)
    is_initial = fields.Boolean('Is Initial Balance')
    is_set_cost_button_clicked = fields.Boolean('Is set cost button clicked?', default=False, copy=False)
    accounting_date = fields.Datetime('Force Accounting Date', readonly=True, states={'draft': [('readonly', False)]},
        help="Choose the accounting date at which you want to value the stock "
             "moves created by the inventory instead of the default one (the "
             "inventory end date)")
    location_id = fields.Many2one(
        'stock.location', 'Inventoried Location',
        readonly=True, required=True, old_name='location_id',
        states={'draft': [('readonly', False)]},
        default=_default_location_id)

    def get_locations_allowed(self):
        domain_locations = []
        for warehouse in self.env.user.allowed_warehouses:
            domain_locations.append(warehouse.lot_stock_id.id)
        return domain_locations

    @api.model
    def search(self, args, offset=0, limit=0, order=None, count=False):
        args = list(args)
        domain_locations = self.get_locations_allowed()
        for index in range(len(args)):
            if (type(args[index]) == list):
                if args[index][2]:
                    if args[index][2] == 'ODERP_WAREHOUSE':
                        args[index] = (args[index][0], args[index][1], domain_locations)
        return super(StockInventory, self).search(args, offset=offset, limit=limit, order=order, count=count)

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        domain = list(domain)
        domain_locations = self.get_locations_allowed()
        for index in range(len(domain)):
            if (type(domain[index]) == list):
                if domain[index][2]:
                    if domain[index][2] == 'ODERP_WAREHOUSE':
                        domain[index] = (domain[index][0], domain[index][1], domain_locations)
        return super(StockInventory, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                             orderby=orderby, lazy=lazy)

    @api.onchange('company_id')
    def onchange_company_id(self):
        if self.company_id:
            return {'domain': {'location_id': [('id', 'in', self.get_locations_allowed()), ('company_id', 'child_of', [self.company_id.id or self.env.user.company_id.id])]}}
            
    @api.multi
    def action_view_invoice(self):
        invoices = self.mapped('invoice_ids')
        action = self.env.ref('account.action_invoice_tree1').read()[0]
        if len(invoices) > 1:
            action['domain'] = [('id', 'in', invoices.ids)]
        elif len(invoices) == 1:
            action['views'] = [(self.env.ref('account.invoice_form').id, 'form')]
            action['res_id'] = invoices.ids[0]
        else:
            action = {'type': 'ir.actions.act_window_close'}
        return action

    @api.multi
    def action_done(self):
        if self.accounting_date:
            self.fix_theoretical_qty()
        self.compute_standard_price()
        for inv in self:
            if not inv.show_cost_button:
                inv.is_set_cost_button_clicked = True
            if not inv.is_set_cost_button_clicked:
                raise UserError(_('Please set inventory cost: %s') % inv.name)
        negative = next((line for line in self.mapped('line_ids') if line.product_qty < 0 and line.product_qty != line.theoretical_qty), False)
        if negative:
            raise UserError(_('You cannot set a negative product quantity in an inventory line:\n\t%s - qty: %s') % (negative.product_id.name, negative.product_qty))
        self.write({'state': 'done'})
        self.post_inventory()
        return True
    
    @api.multi
    def action_cancel_draft(self):
        '''
        Тооллого ноорог төлөвт орох үед
        дахин өртөг тохируулах товч 
        харагдах боломжтой болно.
        '''
        res = super(StockInventory,self).action_cancel_draft()
        self.write({'is_set_cost_button_clicked': False})
        return res

    @api.multi
    def fix_theoretical_qty(self):
        for obj in self:
            if not obj.accounting_date:
                raise UserError(_('Please insert accounting date'))
            
            product_ids = [line.product_id.id for line in obj.line_ids]
            product_start_qty = {}
            where = ''
            join = ''
            if product_ids:
                where += ' AND pp.id in (' + ','.join(map(str, product_ids)) + ') '

            join += "LEFT JOIN stock_location sl ON (l.lid = sl.id)"
            join += "LEFT JOIN stock_location sl1 ON (sl.location_id = sl1.id) "
            group = "sl.id "
            
            accounting_date = obj.accounting_date
            
            self.env.cr.execute("SELECT l.prod_id AS pid, sl.id as lid, SUM(l.start_qty) AS start_qty  "
                                "FROM ( SELECT m.product_id AS prod_id, "
                                "CASE WHEN m.date < %s AND m.location_id != %s AND m.location_dest_id = %s "
                                "THEN COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0) "
                                "WHEN m.date < %s AND m.location_id = %s AND m.location_dest_id != %s "
                                "THEN -COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0) ELSE 0 END AS start_qty, "
                                "CASE WHEN m.location_id != %s AND m.location_dest_id = %s "
                                "THEN m.location_dest_id "
                                "WHEN m.location_id = %s AND m.location_dest_id != %s "
                                "THEN m.location_id ELSE 0 END AS lid "
                                "FROM stock_move m "
                                "LEFT JOIN product_product pp ON (pp.id=m.product_id) "
                                "LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id) "
                                "LEFT JOIN product_uom uom_m ON (uom_m.id = m.product_uom) "
                                "LEFT JOIN product_uom uom_t ON (uom_t.id = pt.uom_id) "
                                "WHERE m.state = 'done' " + where + " ) AS l " + join + " "
                                "GROUP BY l.prod_id, sl.id "
                                "HAVING SUM(l.start_qty)::decimal(16,4) <> 0", (accounting_date, obj.location_id.id, obj.location_id.id, accounting_date,
                                                                                obj.location_id.id, obj.location_id.id,
                                                                                obj.location_id.id, obj.location_id.id, obj.location_id.id, obj.location_id.id))

            lines = self.env.cr.dictfetchall()
            for l in lines:
                if l['pid'] in product_start_qty.keys():
                    product_start_qty[l['pid']] = l['start_qty']
                else:
                    product_start_qty[l['pid']] = {}
                    product_start_qty[l['pid']] = l['start_qty']
            
            for line in obj.line_ids:
                if line.product_id.id in product_start_qty.keys():
                    line.write({'theoretical_qty': product_start_qty[line.product_id.id]})
                else:
                    line.write({'theoretical_qty': 0})
        return True

    @api.multi
    def compute_surplus_deficiency(self):
        for line in self.line_ids:
            result = line.theoretical_qty - line.product_qty
            if result > 0:
                line.write({'qty_deficiency': abs(result),
                            'qty_surplus': 0})
            elif result < 0:
                line.write({'qty_surplus': abs(result),
                            'qty_deficiency': 0,})
            else:
                line.write({'qty_surplus': 0,
                            'qty_deficiency':0})
                
    @api.multi
    def action_check(self):
        if self.accounting_date:
            self.fix_theoretical_qty()
        self.compute_standard_price()
        super(StockInventory,self).action_check()
        return self.write({'state':'counted'}),self._confirm_transit()

    @api.multi
    def _confirm_transit(self):
        product_names = ''
        for line in self.line_ids:
            if line.product_id.tracking == 'lot' and not line.prod_lot_id:
                product_names += ',\n%s' % line.product_id.name
        if len(product_names) > 0:
            raise UserError(_('%s products need serial!') % product_names[2:])


    # Тооллого эхлүүлэхэд тухайн оруулсан шүүлтүүрийн агуулгын дагуу барааг өөрт утга оноох, өртөг, зарах үнийг барьж авна
    @api.multi
    def _get_exhausted_inventory_line(self, products, quant_products):
        vals = super(StockInventory, self)._get_exhausted_inventory_line(products, quant_products)
        new_vals = []
        for line in vals:
            if line.has_key('product_id'):
                product = self.env['product.product'].browse(line['product_id'])[0]
                new_vals.append({
                    'inventory_id': self.ids[0],
                    'product_id': line['product_id'],
                    'location_id': line['location_id'],
                    'standard_price': product.standard_price,
                    'lst_price': product.lst_price,
                })
            else:
                new_vals.append(line)
        return new_vals

    # Тооллогын хуудас хэвлэх
    @api.multi
    def stockInventoryCount(self):
        result = False

        data = {
            'ids': self.ids,
            'model': 'stock.inventory',
        }

        result = self.env['report'].get_action([], 'l10n_mn_stock.print_stock_inventory_count', data=data)
        return result

    # Устгах тохиолдолд Ноорог гэснээс өөр төлөвт байвал усгаж болохгүй

    @api.multi
    def unlink(self):
        for a in self:
            if a.state != 'draft':
                raise UserError(_('You can delete only draft inventory adjustment'))
        return super(StockInventory, self).unlink()

    @api.multi
    def force_cancel(self):
        # Мөчлөг түгжигдсэн эсэхийг шалгах
        for inventory_id in self:
            lock_date = inventory_id.company_id.period_lock_date or False if not self.env.user.has_group('account.group_account_manager') else inventory_id.company_id.fiscalyear_lock_date or False
            if lock_date and lock_date > (inventory_id.accounting_date or inventory_id.date):
                raise ValidationError(_(u"Accounting date is locked until '%s' !!!") % lock_date)
          
        # Батлагдсан тооллогыг хүчээр цуцлах функц  
        for inventory in self:
            
            # Тооллогоос хийгдсэн хөдөлгөөнүүдийг шүүх
            get_moves_qry = "SELECT id FROM stock_move WHERE inventory_id = %s" % (inventory.id)
            self._cr.execute(get_moves_qry)
            move_results = self._cr.dictfetchall()
            
            # ----------------------------Тооллого цуцлалт эхэлж байна.----------------------------
            _logger.info(_(u'\n\t\t\t\t::\tStart FORCE UP the inventory:\t\t%s') %(inventory.name))
            
            if move_results and len(move_results) > 0:
                move_ids = [result['id'] for result in move_results]
                move_ids_str = str(move_ids)[1:len(str(move_ids))-1]
                
                # 1. Тооллогоос хийгдсэн хөдөлгөөнүүд дээрхи stock_quant-уудыг устгах
                del_quant_qry = """DELETE FROM stock_quant WHERE id IN (SELECT quant_id FROM stock_quant_move_rel WHERE move_id in (%s))""" %str(move_ids_str)
                deleted_quant_ids = self._cr.execute(del_quant_qry)
                _logger.info(_(u'\n\t\t\t\t::\tQuants deleted successfully.'))
                
                # 2. Тооллогоос хийгдсэн хөдөлгөөнүүдтэй холбоотой account_move-уудыг устгах
                del_account_move_qry = """DELETE FROM account_move WHERE id IN (SELECT move_id FROM account_move_line WHERE stock_move_id in (%s))""" %str(move_ids_str)
                deleted_account_move_ids = self._cr.execute(del_account_move_qry)
                _logger.info(_(u'\n\t\t\t\t::\tAccount moves deleted successfully.'))
                
                # 3. Тооллогоос хийгдсэн хөдөлгөөнүүдийг устгах
                del_move_qry = """DELETE FROM stock_move WHERE id IN (%s)""" %str(move_ids_str)
                deleted_move_ids = self._cr.execute(del_move_qry)
                _logger.info(_(u'\n\t\t\t\t::\tStock moves deleted successfully.'))
                
                # 4. Тооллогоос үүссэн агуулахын өртгүүдийг устгах
                del_cost_qry = """DELETE FROM stock_inventory_cost WHERE inventory_id = %s""" %str(inventory.id)
                deleted_cost_ids = self._cr.execute(del_cost_qry)
                _logger.info(_(u'\n\t\t\t\t::\tInventory cost deleted successfully.'))
                
            # 5. Тооллогын нэхэмжлэл устгах
            if self.invoice_ids and len(self.invoice_ids) > 0:
                invoice_ids_str = str(inventory.invoice_ids.ids)[1:len(str(inventory.invoice_ids.ids))-1]
                del_invoice_qry = """DELETE FROM account_invoice WHERE id in (%s)""" %str(invoice_ids_str)
                deleted_invoice_ids = self._cr.execute(del_invoice_qry)
                _logger.info(_(u'\n\t\t\t\t::\tInvoice deleted successfully.'))
                
            # 6. Тооллогын төлөвийг өөрчлөх
            inventory.write({'state': 'confirm', 'is_set_cost_button_clicked': False})
                
            # ----------------------------Тооллого цуцлалт дууслаа.----------------------------
            _logger.info(_(u'\n\t\t\t\t::\tEnd FORCE UP the inventory:\t\t%s') %(inventory.name))
        
    @api.multi
    def action_start(self):
        for inventory in self:
            if inventory.accounting_date:
                warehouse_id = self.env['stock.warehouse'].search([('lot_stock_id','=',inventory.location_id.id)])
                if warehouse_id:
                    if warehouse_id.lock_move and inventory.accounting_date <= warehouse_id.lock_move_until:
                        raise UserError(_('Sorry, this warehouse is locked until %s') % warehouse_id.lock_move_until)
        res = super(StockInventory, self).action_start()
        return res
    prepare_inventory = action_start

    @api.multi
    def _get_inventory_lines_values(self):
        res_vals = []
        
        Product = self.env['product.product']
        quant_products = self.env['product.product']
        products_to_filter = self.env['product.product']
        locations = self.env['stock.location'].search([('id', 'child_of', [self.location_id.id])])
        domain1 = ' location_dest_id in %s'
        args1 = (tuple(locations.ids),)
        domain2 = ' location_id in %s'
        args2 = (tuple(locations.ids),)
        
        if self.company_id.availability_compute_method == 'stock_move' and self.accounting_date:
            domain1 += ' AND date < %s'
            args1 += (self.accounting_date,)
            domain2 += ' AND date < %s'
            args2 += (self.accounting_date,)
            product_where = ''
            if self.filter == 'product':
                product_where = ' AND product_id = ' + str(self.product_id.id) + ' '

            self.env.cr.execute('''
                SELECT
                    product_id,
                    location_id,
                    SUM(product_qty) AS product_qty
                FROM
                    (
                        SELECT
                            id,
                            product_id,
                            location_dest_id AS location_id,
                            product_qty
                        FROM
                            stock_move
                        WHERE
                            state='done'
                            and %s %s
                        UNION
                        SELECT
                            id,
                            product_id,
                            location_id,
                            (product_qty*-1) AS product_qty
                        FROM
                            stock_move
                        WHERE
                            state='done'
                            and %s %s
                    ) stock_move_by_location
                GROUP BY
                    product_id,
                    location_id HAVING SUM(product_qty) > 0''' % (domain1, product_where, domain2, product_where), (args1 + args2))
            vals = []
            fetched = self.env.cr.dictfetchall()
            for product_data in fetched:
                # replace the None the dictionary by False, because falsy values are tested later on
                for void_field in [item[0] for item in product_data.items() if item[1] is None]:
                    product_data[void_field] = False
                product_data['theoretical_qty'] = product_data['product_qty']
                if product_data['product_id']:
                    product_data['product_uom_id'] = Product.browse(product_data['product_id']).uom_id.id
                    quant_products |= Product.browse(product_data['product_id'])
                vals.append(product_data)
            if self.exhausted:
                exhausted_vals = self._get_exhausted_inventory_line(products_to_filter, quant_products)
                vals.extend(exhausted_vals)
            res_vals = vals
        elif self.company_id.availability_compute_method == 'stock_quant':
            res_vals = super(StockInventory, self)._get_inventory_lines_values()

        return res_vals

    @api.multi
    def compute_standard_price(self):
        for line in self:
            line.standard_price = line.product_id.standard_price


class StockInventoryLine(models.Model):
    _inherit = "stock.inventory.line"

    invoice_lines = fields.Many2many('account.invoice.line', 'stock_inventory_line_invoice_rel',
                                     'inventory_line_id', 'invoice_line_id', string='Invoice Lines', copy=False)

    standard_price = fields.Float('Product cost', readonly=True)
    theoretical_qty = fields.Float('Theoretical Quantity', digits=dp.get_precision('Product Unit of Measure'),
                                   readonly=True)
    lst_price = fields.Float('Public Price', related='product_id.list_price', digits=dp.get_precision('Product Price'), readonly=True)
    qty_surplus = fields.Float('Surplus', readonly=True)
    qty_deficiency = fields.Float('Deficiency', readonly=True)

    def _get_move_values(self, qty, location_id, location_dest_id):
        self.ensure_one()
        res = super(StockInventoryLine, self)._get_move_values(qty,location_id,location_dest_id)
        if self.inventory_id.accounting_date:
            res['date'] = self.inventory_id.accounting_date
        if self.product_qty == 0:
            res['is_zero_inventory'] = True
        return res

    @api.one
    @api.depends('location_id', 'product_id', 'package_id', 'product_uom_id', 'company_id', 'prod_lot_id', 'partner_id')
    def _compute_theoretical_qty(self):
        # Compute талбарыг болиулж Core дээрх уг функц тооцоолол хийгдээд байсан тул уг дахин тодорхойлсон функцийг ямар нэгэн үйлдэл хийдэггүй болгов
        return True
                    
class StockLocation(models.Model):
    _inherit = "stock.location"
    
    valuation_in_account_id = fields.Many2one('account.account', domain=[('internal_type', 'in', ['other','receivable','income']), ('deprecated', '=', False)])
    valuation_out_account_id = fields.Many2one('account.account',  domain=[('internal_type','in', ['other','payable','expense']), ('deprecated', '=', False)])
           