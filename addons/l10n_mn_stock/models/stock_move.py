# -*- coding: utf-8 -*-
import logging
import time

from odoo import api, fields, models, _
from odoo.tools.float_utils import float_compare

_logger = logging.getLogger(__name__)


class StockMove(models.Model):
    _inherit = "stock.move"
    _description = "Stock Move"

    @api.depends('state', 'write_date')
    def _move_type(self):
        for move in self:
            # борлуулалтын зарлага
            if move.location_id.usage == 'internal' and move.location_dest_id.usage == 'customer':
                if move.picking_id.name:
                    move.move_type = 'sale'
            # борлуулалтын буцаалтын орлого
            elif move.location_id.usage == 'customer' and move.location_dest_id.usage == 'internal':
                if move.picking_id.name:
                    move.move_type = 'sale_return'
            # худалдан авалтын орлого
            elif move.location_id.usage == 'supplier' and move.location_dest_id.usage == 'internal':
                move.move_type = 'purchase'
            # худалдан авалтын буцаалтын зарлага
            elif move.location_id.usage == 'internal' and move.location_dest_id.usage == 'supplier':
                move.move_type = 'purchase_return'
            # нөхөн дүүргэлтийн орлого
            elif move.location_id.usage == 'transit' and move.location_dest_id.usage == 'internal':
                move.move_type = 'transit_income'
            # нөхөн дүүргэлтийн зарлага
            elif move.location_id.usage == 'internal' and move.location_dest_id.usage == 'transit':
                move.move_type = 'transit_expense'
            # тооллогын зарлага
            elif move.location_id.usage == 'internal' and move.location_dest_id.usage == 'inventory':
                if 'expense' in move.picking_id and move.picking_id.expense:
                    move.move_type = 'product_expense_picking'
                else:
                    move.move_type = 'inventory_expense'
            # тооллогын орлого
            elif move.location_id.usage == 'inventory' and move.location_dest_id.usage == 'internal':
                move.move_type = 'inventory_income'
            # үйлдвэрлэлийн зарлага
            elif move.location_id.usage == 'internal' and move.location_dest_id.usage == 'production':
                move.move_type = 'mrp_income'
            # үйлдвэрлэлийн орлого
            elif move.location_id.usage == 'production' and move.location_dest_id.usage == 'internal':
                move.move_type = 'mrp_expense'
                

    move_type = fields.Selection(compute='_move_type', string='Move Type', selection=[('sale', 'Sale'),
                                                                                      ('sale_return', 'Sale Return'),
                                                                                      ('purchase', 'Purchase'),
                                                                                      ('purchase_return', 'Purchase Return'),
                                                                                      ('transit_income', 'Transit Income'),
                                                                                      ('transit_expense', 'Transit Expense'),
                                                                                      ('inventory_income', 'Inventory Income'),
                                                                                      ('inventory_expense', 'Inventory Expense'),
                                                                                      ('mrp_income', 'Production Income'),
                                                                                      ('mrp_expense', 'Production Expense'),
                                                                                      ('product_expense_picking', 'Product Expense Picking')], store=True)
    total_cost = fields.Float(compute='_compute_total_cost', string='Total Cost', store=True)
    is_zero_inventory = fields.Boolean('Is Zero Inventory')

    @api.depends('product_uom_qty', 'price_unit')
    def _compute_total_cost(self):
        for move in self:
            move.total_cost = move.price_unit * move.product_uom_qty

    def get_move_qty(self):
        self._cr.execute("""SELECT ROUND(sum(COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0))::decimal,2) AS product_qty 
                             FROM 
                             stock_move m 
                             LEFT JOIN product_product pp ON (pp.id = m.product_id) 
                             LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id) 
                             LEFT JOIN product_uom uom_m ON (uom_m.id = m.product_uom) 
                             LEFT JOIN product_uom uom_t ON (uom_t.id = pt.uom_id) 
                             WHERE m.id in ( SELECT id FROM stock_move 
                                            where location_id = %s and product_id = %s and state = 'assigned' and id != %s) """ % (
            self.location_id.id, self.product_id.id, self.id))
        moves = self._cr.fetchall()
        move_qty = moves[0][0]
        return move_qty

    def get_qty(self, date, location):
        self._cr.execute("""SELECT ROUND(sum(COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0))::decimal,2) AS product_qty 
                             FROM 
                             stock_move m 
                             LEFT JOIN product_product pp ON (pp.id = m.product_id) 
                             LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id) 
                             LEFT JOIN product_uom uom_m ON (uom_m.id = m.product_uom) 
                             LEFT JOIN product_uom uom_t ON (uom_t.id = pt.uom_id) 
                             WHERE m.id in (SELECT id FROM stock_move 
                                             where date <= '%s' and %s = %s and product_id = %s and state = 'done') """ % (
            date, location, self.location_id.id, self.product_id.id))
        fetched = self._cr.fetchall()
        qty = fetched[0][0]
        return qty

    @api.multi
    def action_assign(self, no_prepare=False):
        company = self.env.user.company_id
        moves_to_assign = self.env['stock.move']
        moves_to_do = self.env['stock.move']
        ancestors_list = {}
        # Хэрэв quant ашиглахгүй тохиргоотой компани бол Бэлэн байдлыг шалгах буюу Нөөцлөх товчийг дарахад stock.move-с барааны үлдэгдэл хүрэлцэж байгаа эсэхийг шалгана
        if company.availability_compute_method == 'stock_move':
            for move in self:
                availability = 0
                assign_date = move.date
                if move.picking_id.transit_order_id and move.picking_id.transit_order_id.move_assign_date:
                    transit_date = move.picking_id.transit_order_id.move_assign_date
                    if move.date <= transit_date:
                        assign_date = transit_date

                if self.env.get('sale.order', False) != False and move.picking_id:
                    self.env.cr.execute("select "
                                        "so.id as id "
                                        "from stock_move sm "
                                        "JOIN stock_picking sp on sm.picking_id = sp.id "
                                        "JOIN procurement_order po on po.id = sm.procurement_id "
                                        "JOIN sale_order_line sol on sol.id = po.sale_line_id "
                                        "JOIN sale_order so on so.id = sol.order_id "
                                        "where "
                                        "sm.origin_returned_move_id is NULL and sp.id = %s " % move.picking_id.id)
                    lines = self.env.cr.fetchall()
                    if len(lines) > 0:
                        ids = [line[0] for line in lines]
                        sale_order_obj = self.env['sale.order'].search([('id', 'in', ids)])
                        if sale_order_obj and sale_order_obj.move_assign_date:
                            sale_date = sale_order_obj.move_assign_date
                            if move.date <= sale_date:
                                assign_date = sale_date

                ancestors = move.find_move_ancestors()
                # Өөрөөс нь бусад тухайн байрлалаас гарах гэж буй БЭЛЭН төлөвт байгаа хөдөлгөөнүүдийн тоо хэмжээг олох
                date = assign_date if self.env.user.company_id.assign_date == 'move_date' else time.strftime('%Y-%m-%d %H:%M:%S')
                move_qty = move.get_move_qty()
                if move.location_id.usage == 'internal' and move.location_dest_id.usage != 'internal':
                    # Тухайн байрлалаас аль хэдийн гарчихсан хөдөлгөөнүүдийн тоо хэмжээг олох
                    qty = move.get_qty(date, 'location_id')
                    # Тухайн байрлалд аль хэдийн орж ирсэн хөдөлгөөнүүдийн тоо хэмжээг олох
                    qty2 = move.get_qty(date, 'location_dest_id')
                    # Тухайн байрлалаас гарах боломжтой тоо хэмжээг олох
                    availability = (qty2 if qty2 else 0) - (qty if qty else 0) - (move_qty if move_qty else 0)
                    # Тухайн байрлалаас гарах боломжтой тоо хэмжээ нь хийх гэж буй хөдөлгөөний тоо хэмжээг хангахуйц бол уг хөдөлгөөнийг БЭЛЭН төлөвт оруулах буюу нөөцлөх
                    if availability > 0 and round(availability,2) >= round(move.product_qty,2):
                        move.write({'state': 'assigned'})
                    else:
                        move.write({'state': 'confirmed'})
                    moves_to_do |= move
                    ancestors_list[move.id] = True if ancestors else False
                # нөхөн дүүргэлтийн ирэх бараа
                elif move.location_id.usage == 'transit' and move.location_dest_id.usage == 'internal':
                    # Тухайн байрлалаас аль хэдийн гарчихсан хөдөлгөөнүүдийн тоо хэмжээг олох
                    qty = move.get_qty(date, 'location_id')
                    # Тухайн байрлалд аль хэдийн орж ирсэн хөдөлгөөнүүдийн тоо хэмжээг олох
                    qty2 = move.get_qty(date, 'location_dest_id')
                    # Тухайн байрлалаас гарах боломжтой тоо хэмжээг олох
                    availability = (qty2 if qty2 else 0) - (qty if qty else 0) - (move_qty if move_qty else 0)
                    # Тухайн байрлалаас гарах боломжтой тоо хэмжээ нь хийх гэж буй хөдөлгөөний тоо хэмжээг хангахуйц бол уг хөдөлгөөнийг БЭЛЭН төлөвт оруулах буюу нөөцлөх
                    if availability > 0 and round(availability,2) >= round(move.product_qty,2):
                        move.write({'state': 'assigned'})
                    else:
                        move.write({'state': 'confirmed'})
                    moves_to_do |= move
                    ancestors_list[move.id] = True if ancestors else False
                    if move.picking_id.state in ['assigned', 'partially_available'] and move.picking_id.pack_operation_ids.ids == []:
                        move.picking_id.prepare_partial_move()
                elif move.location_id.usage == 'internal' and move.location_dest_id.usage == 'internal':
                    # Тухайн байрлалаас аль хэдийн гарчихсан хөдөлгөөнүүдийн тоо хэмжээг олох
                    qty = move.get_qty(date, 'location_id')
                    # Тухайн байрлалд аль хэдийн орж ирсэн хөдөлгөөнүүдийн тоо хэмжээг олох
                    qty2 = move.get_qty(date, 'location_dest_id')
                    # Тухайн байрлалаас гарах боломжтой тоо хэмжээг олох
                    availability = (qty2 if qty2 else 0) - (qty if qty else 0) - (move_qty if move_qty else 0)
                    # Тухайн байрлалаас гарах боломжтой тоо хэмжээ нь хийх гэж буй хөдөлгөөний тоо хэмжээг хангахуйц бол уг хөдөлгөөнийг БЭЛЭН төлөвт оруулах буюу нөөцлөх
                    if availability > 0 and round(availability, 2) >= round(move.product_qty,2):
                        move.write({'state': 'assigned'})
                    else:
                        move.write({'state': 'confirmed'})
                    moves_to_do |= move
                    ancestors_list[move.id] = True if ancestors else False
                else:
                    moves_to_assign |= move
                    if not move.origin_returned_move_id:
                        continue

                if move.product_id.type == 'consu' and not ancestors:
                    moves_to_assign |= move
                    continue
            if moves_to_assign:
                moves_to_assign.write({'state': 'assigned'})
            for move_to_do in sorted(moves_to_do, key=lambda x: -1 if ancestors_list.get(x.id) else 0):
                if move_to_do.state != 'assigned':
                    self.moves_reserve(move_to_do, availability)
            if not no_prepare:
                self.check_recompute_pack_op_move()
        else:
            return super(StockMove, self).action_assign(no_prepare=no_prepare)
    
    def get_product_standard_price(self):
        # Барааны өртөг тооцож буцаах функц
        return self.product_id.standard_price
    
    def update_product_price(self):
        """
            @note: stock.move-н action_done() функцад дуудагдаж журнал бичилт үүсэхээс өмнө price_unit-г шинэчлэхийн тулд дахин тодорхойлж ашиглана.
                   product_price_update_before_done функцыг бүтэн хуулж дахин тодорхойлоход төвөгтэй байсан тул stock.move-н price_unit-г шинэчлэхээр хийсвэр функц тодорхойлов. 
        """
        pass
        
    @api.multi
    def action_done(self):
        self.update_product_price()
        return super(StockMove, self).action_done()
        
    @api.multi
    def check_recompute_pack_op_move(self):
        pickings = self.mapped('picking_id').filtered(lambda picking: picking.state not in ('waiting', 'confirmed'))
        # Check if someone was treating the picking already
        pickings_partial = pickings.filtered(lambda picking: not any(operation.qty_done for operation in picking.pack_operation_ids))
        pickings_partial.prepare_partial_move()
        (pickings - pickings_partial).write({'recompute_pack_op': True})

    @api.multi
    def force_assign(self):
        self.write({'state': 'assigned'})
        company = self.env.user.company_id
        context = self.env.context.copy()
        context['force_assign'] = True
        if company.availability_compute_method == 'stock_move':
            self.with_context(context).check_recompute_pack_op_move()
        else:
            self.with_context(context).check_recompute_pack_op()

    def quants_unreserve(self):
        for move in self:
            if move.company_id.availability_compute_method == 'stock_move':
                move.filtered(lambda x: x.partially_available).write({'partially_available': False})
            else:
                return super(StockMove, self).quants_unreserve()

    # Бэлэн болохыг хүлээж буй төлөвтэй хөдөлгөөнүүдийн нөөцийг шалгаж, боломжит үлдэгдлээс хамааруулж зарим нь бэлэн эсвэл
    # бэлэн эсэхийг шалгав
    def moves_reserve(self, move, availability):
        rounding = move.product_id.uom_id.rounding
        if float_compare(availability, move.product_qty, precision_rounding=rounding) == 0 and move.state in ('confirmed', 'waiting'):
            move.write({'state': 'assigned'})
        elif float_compare(availability, 0, precision_rounding=rounding) > 0 and not move.partially_available:
            move.write({'partially_available': True})

    @api.multi
    def do_prepare_partial_by_move(self):
        """ Нөөцлөгдсөн барааны хөдөлгөөний ажилбаруудыг үүсгэх функц /stock.pack.operation/
            Компани Барааны боломжит нөөц тооцох арга -- > Барааны хөдөлгөөнөөс тооцох
            Энэ функцийг stock.quant-тай адил байдлаар хийн _prepare функцийг мөн нэгтгэв"""

        _logger.info(_('Function: move do_prepare_partial_by_move'))
        StockPackOperation = self.env['stock.pack.operation']
        StockMoveOperationLink = self.env['stock.move.operation.link']
        for move in self:
            # Creating pack operations for each stock move
            new_stock_operation = StockPackOperation.create({
                'product_qty': move.product_qty,
                'location_id': move.location_id.id,
                'ordered_qty': move.product_qty,
                'date': move.date_expected,
                'product_id': move.product_id.id,
                'product_uom_id': move.product_uom.id,
                'location_dest_id': move.location_dest_id.id,
                'picking_id': move.picking_id.id,
                'fresh_record': False,
            })
            link = StockMoveOperationLink.search([('move_id', '=', move.id)])
            if link:
                if link.operation_id:
                    link.operation_id.unlink()
                link.operation_id = new_stock_operation.id
            else:
                StockMoveOperationLink.create({'move_id': move.id, 'operation_id': new_stock_operation.id, 'qty': move.product_qty})