# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from datetime import datetime
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.exceptions import ValidationError
from dateutil.relativedelta import relativedelta
import logging
import time

_logger = logging.getLogger(__name__)


class StockPickingType(models.Model):
    _inherit = 'stock.picking.type'

    def _get_allowed_warehouses(self):
        result = {}
        for obj in self:
            if self.env.user.allowed_warehouses:
                result[obj.id] = [wh.id for wh in self.env.user.allowed_warehouses]
            else:
                result[obj.id] = []
        return result

    allowed_warehouses = fields.Many2many('stock.warehouse', compute=_get_allowed_warehouses, search='_search_allowed_warehouses', string='My Allowed Warehouses')

    @api.multi
    def _search_allowed_warehouses(self, operator, value):
        if value == 'hacking_haha':
            picking_types = self.search([
                '|', ('warehouse_id', '=', False), ('warehouse_id', 'in', self.env.user.allowed_warehouses.ids),
                '|', '|', ('warehouse_id', '=', False), ('warehouse_id.company_id', '=', False), ('warehouse_id.company_id', 'child_of', [self.env.user.company_id.id])
            ])
        else:
            picking_types = self.search([
                ('warehouse_id.name', 'ilike', value),
                '|', ('warehouse_id', '=', False), ('warehouse_id', 'in', self.env.user.allowed_warehouses.ids),
                '|', '|', ('warehouse_id', '=', False), ('warehouse_id.company_id', '=', False), ('warehouse_id.company_id', 'child_of', [self.env.user.company_id.id])
            ])
        if picking_types:
            return [('id', 'in', picking_types.ids)]
        else:
            return [('id', '=', False)]

    @api.model
    def search(self, args, offset=0, limit=0, order=None, count=False):
        args = list(args)
        user = self.env['res.users'].browse(self._uid)
        for index in range(len(args)):
            if (type(args[index]) == list):
                if args[index][2]:
                    if args[index][2] == 'ODERP_WAREHOUSE':
                        args[index] = (args[index][0], args[index][1], user.allowed_warehouses.ids)
        return super(StockPickingType, self).search(args, offset=offset, limit=limit, order=order, count=count)

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        user = self.env['res.users'].browse(self._uid)
        domain = list(domain)
        for index in range(len(domain)):
            if (type(domain[index]) == list):
                if domain[index][2]:
                    if domain[index][2] == 'ODERP_WAREHOUSE':
                        domain[index] = (domain[index][0], domain[index][1], user.allowed_warehouses.ids)
        return super(StockPickingType, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                        orderby=orderby, lazy=lazy)


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    date_done = fields.Datetime('Date of Transfer', copy=False, readonly=True, help="Completion Date of Transfer",
                                track_visibility='onchange')
    picking_type_id = fields.Many2one(
        'stock.picking.type', 'Picking Type',
        required=True,
        readonly=False)

    @api.model
    def search(self, args, offset=0, limit=0, order=None, count=False):
        args = list(args)
        user = self.env['res.users'].browse(self._uid)
        for index in range(len(args)):
            if (type(args[index]) == list):
                if args[index][2]:
                    if args[index][2] == 'ODERP_WAREHOUSE':
                        args[index] = (args[index][0], args[index][1], user.allowed_warehouses.ids)
        return super(StockPicking, self).search(args, offset=offset, limit=limit, order=order, count=count)

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        user = self.env['res.users'].browse(self._uid)
        domain = list(domain)
        for index in range(len(domain)):
            if (type(domain[index]) == list):
                if domain[index][2]:
                    if domain[index][2] == 'ODERP_WAREHOUSE':
                        domain[index] = (domain[index][0], domain[index][1], user.allowed_warehouses.ids)
        return super(StockPicking, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                    orderby=orderby, lazy=lazy)

    transit_order_id = fields.Many2one('stock.transit.order', string='Transit Order')

    @api.multi
    def do_transfer(self):
        res = super(StockPicking, self).do_transfer()
        self.after_done()
        return res

    @api.multi
    def after_done(self):
        ''' Барааны баримт дууссаны дараа тухайн баримтаас
            хамааралтай бусад процессуудыг шалгана.
        '''
        for pick in self:
            if pick.transit_order_id:
                pick.transit_order_id.check_done()
        return True

    @api.multi
    def action_cancel(self):
        for line in self:
            if line.pack_operation_ids:
                for pack_operation in line.pack_operation_ids:
                    pack_operation.unlink()
        return super(StockPicking, self).action_cancel()

    @api.multi
    def do_new_transfer(self):
        """
            Агуулах -> Тохиргоо -> "Агуулахын хөдөлгөөнийг зөвшөөрөгдсөн хэрэглэгч батлана" сонгосон үед
            батлахыг зөвшөөрсөн агуулахын хөдөлгөөн байвал батална.
        """
        user = self.env['res.users'].browse(self._uid)
        for pick in self:
            if pick.company_id.stock_move_approved_allowed_user:
                if pick.picking_type_id.warehouse_id.id not in user.approved_warehouses.ids:
                    raise ValidationError(_("You do not have permission to approve the warehouse. Please, Contact your system administrator!"))
        return super(StockPicking, self).do_new_transfer()

    @api.multi
    def do_prepare_partial_by_move(self):
        _logger.info(_('Function Oderp: do_prepare_partial_by_move'))
        ''' Function to unlink and then create stock pack operations for each stock moves of the picking '''
        StockPackOperation = self.env['stock.pack.operation']
        StockMoveOperationLink = self.env['stock.move.operation.link']
        force_assign = self.env.context.get('force_assign', False)
        for record in self:
            if force_assign or record.state in ('draft', 'assigned'):
                # Retreiving the old pack operations
                pack_operations_to_unlink = record.pack_operation_product_ids
                # Creating pack operations for each stock move
                for move in record.move_lines:
                    new_stock_operation = StockPackOperation.create({
                        'product_qty': move.product_qty,
                        'location_id': move.location_id.id,
                        'ordered_qty': move.product_qty,
                        'date': move.date_expected,
                        'product_id': move.product_id.id,
                        'product_uom_id': move.product_uom.id,
                        'location_dest_id': move.location_dest_id.id,
                        'picking_id': move.picking_id.id,
                        'fresh_record': False
                    })
                    links = StockMoveOperationLink.search([('move_id', '=', move.id)])
                    if links:
                        for link in links:
                            link.operation_id = new_stock_operation.id
                    else:
                        StockMoveOperationLink.create(
                            {'move_id': move.id, 'operation_id': new_stock_operation.id, 'qty': move.product_qty})
                # Deleting the old pack operations
                pack_operations_to_unlink.unlink()

    # Агуулахын хөдөлгөөн цуцлах группын хэрэглэгч биш бол Огноо өөрчлөх цэсийг харуулдаггүй болгов
    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(StockPicking, self).fields_view_get(view_id, view_type, toolbar=toolbar, submenu=submenu)
        if not self.env.user.has_group('l10n_mn_stock.group_picking_to_cancel'):
            if res.get('toolbar'):
                for action in res.get('toolbar').get('action'):
                    if action.get('xml_id'):
                        if action['xml_id'] == 'l10n_mn_stock.action_stock_picking_change_date_1':
                            res['toolbar']['action'].remove(action)
        return res

    @api.one
    def _compute_quant_reserved_exist(self):
        if self.company_id.availability_compute_method == 'stock_move':
            self.quant_reserved_exist = True if self.pack_operation_ids and self.state != 'done' else False
        else:
            return super(StockPicking, self)._compute_quant_reserved_exist()

    @api.multi
    def prepare_partial_move(self):
        _logger.info(_('----------------prepare_partial_move-------------------'))
        PackOperation = self.env['stock.pack.operation']
        StockMoveOperationLink = self.env['stock.move.operation.link']
        force_assign = self.env.context.get('force_assign', False)
        # get list of existing operations and delete them
        existing_packages = PackOperation.search([('picking_id', 'in', self.ids)])
        if existing_packages:
            existing_packages.unlink()
        for picking in self:
            for move in picking.move_lines:
                vals = {}
                p_qty = 0
                assign_date = move.date
                if move.picking_id.transit_order_id and move.picking_id.transit_order_id.move_assign_date:
                    assign_date = move.picking_id.transit_order_id.move_assign_date
                    transit_date = move.picking_id.transit_order_id.move_assign_date
                    if move.date <= transit_date:
                        assign_date = transit_date
                date = assign_date if self.env.user.company_id.assign_date == 'move_date' else time.strftime('%Y-%m-%d %H:%M:%S')
                move_qty = move.get_move_qty()
                if force_assign:
                    move.do_prepare_partial_by_move()
                elif (move.location_id.usage == 'internal' and move.location_dest_id.usage != 'internal') or \
                        (move.location_id.usage == 'transit' and move.location_dest_id.usage == 'internal') or \
                        (move.location_id.usage == 'internal' and move.location_dest_id.usage == 'internal'):
                    # 1. Тухайн байрлалаас аль хэдийн гарчихсан хөдөлгөөнүүдийн тоо хэмжээг олох
                    # 2. Нөхөн дүүргэлтийн ирэх бараа
                    # 3. Тухайн байрлалаас аль хэдийн гарчихсан хөдөлгөөнүүдийн тоо хэмжээг олох
                    qty = move.get_qty(date, 'location_id')
                    # Тухайн байрлалд аль хэдийн орж ирсэн хөдөлгөөнүүдийн тоо хэмжээг олох
                    qty2 = move.get_qty(date, 'location_dest_id')
                    # Тухайн байрлалаас гарах боломжтой тоо хэмжээг олох
                    availability = (qty2 if qty2 else 0) - (qty if qty else 0) - (move_qty if move_qty else 0)
                    if availability > 0:
                        if availability >= move.product_qty:
                            p_qty = move.product_qty
                        else:
                            p_qty = availability
                        vals = self._prepare_pack_op(move, p_qty)
                else:
                    move.do_prepare_partial_by_move()
                if vals:
                    new_stock_operation = PackOperation.create(vals)
                    PackOperation |= new_stock_operation
                    StockMoveOperationLink.create({'move_id': move.id, 'operation_id': new_stock_operation.id, 'qty': p_qty})
        for pack in PackOperation:
            pack.ordered_qty = sum(pack.mapped('linked_move_operation_ids').mapped('move_id').filtered(lambda r: r.state != 'cancel').mapped('ordered_qty'))
        self.write({'recompute_pack_op': False})

    @api.multi
    def recheck_availability(self):
        self.action_assign()
        if self.company_id.availability_compute_method == 'stock_move':
            self.prepare_partial_move()
        else:
            self.do_prepare_partial()

    @api.multi
    def _prepare_pack_op(self, move, qty):
        return {
                'product_qty': qty,
                'location_id': move.location_id.id,
                'ordered_qty': move.product_qty,
                'date': move.date_expected,
                'product_id': move.product_id.id,
                'product_uom_id': move.product_uom.id,
                'location_dest_id': move.location_dest_id.id,
                'picking_id': move.picking_id.id,
                'fresh_record': False
            }
