# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.addons import decimal_precision as dp

class Quant(models.Model):
    _inherit = "stock.quant"
    
    list_price = fields.Float('List Price', compute='_compute_list_price', readonly=True)
    qty = fields.Float(
        'Quantity', digits=dp.get_precision('Product Unit of Measure'),
        index=True, readonly=True, required=True,
        help="Quantity of products in this quant, in the default unit of measure of the product")
    
    @api.multi
    def _compute_list_price(self):
        for quant in self:
            if quant.company_id != self.env.user.company_id:
                quant = quant.with_context(force_company=quant.company_id.id)
            quant.list_price = quant.product_id.list_price * quant.qty
            
    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        " Overwrite the read_group in order to sum the function field 'list_price' in group by "
        res = super(Quant, self).read_group(domain, fields, groupby, offset=offset, limit=limit, orderby=orderby, lazy=lazy)
        if 'list_price' in fields:
            for line in res:
                if '__domain' in line:
                    lines = self.search(line['__domain'])
                    inv_value = 0.0
                    for line2 in lines:
                        inv_value += line2.list_price
                    line['list_price'] = inv_value
        return res

    def _quant_create_from_move(self, qty, move, lot_id=False, owner_id=False, src_package_id=False,
                                dest_package_id=False, force_location_from=False, force_location_to=False):
        quant = super(Quant, self)._quant_create_from_move(qty, move, lot_id=lot_id, owner_id=owner_id,
                                                                src_package_id=src_package_id,
                                                                dest_package_id=dest_package_id,
                                                                force_location_from=force_location_from,
                                                                force_location_to=force_location_to)

        #Quant одоогын огноогоор үүсэж байсныг хөдөлгөөний огноогоор үүсдэг болгов.
        quant.sudo().write({'in_date': move.date})
        return quant