# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class StockScrap(models.Model):
    _inherit = 'stock.scrap'

    def get_locations_allowed(self):
        user = self.env['res.users'].browse(self._uid)
        locations = self.env['stock.location'].search(
            [('company_id', '=', self.env.user.company_id.id), ('usage', 'in', ['view', 'internal'])])
        domain_locations = []
        for location in locations:
            if location.get_warehouse() and location.get_warehouse().is_internal_counting and location.usage == 'view':
                continue
            else:
                if location.get_warehouse().id in user.allowed_warehouses.ids:
                    domain_locations.append(location.id)
        return domain_locations


    @api.model
    def search(self, args, offset=0, limit=0, order=None, count=False):
        args = list(args)
        domain_locations = self.get_locations_allowed()
        for index in range(len(args)):
            if (type(args[index]) == list):
                if args[index][2]:
                    if args[index][2] == 'ODERP_WAREHOUSE':
                        args[index] = (args[index][0], args[index][1], domain_locations)
        return super(StockScrap, self).search(args, offset=offset, limit=limit, order=order, count=count)


    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        domain = list(domain)
        domain_locations = self.get_locations_allowed()
        for index in range(len(domain)):
            if (type(domain[index]) == list):
                if domain[index][2]:
                    if domain[index][2] == 'ODERP_WAREHOUSE':
                        domain[index] = (domain[index][0], domain[index][1], domain_locations)
        return super(StockScrap, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                              orderby=orderby, lazy=lazy)