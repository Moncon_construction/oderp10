# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
import logging
from odoo import netsvc
from datetime import datetime
from dateutil.relativedelta import relativedelta

_logger = logging.getLogger(__name__)


class StockTransitOrder(models.Model):
    _name = 'stock.transit.order'
    _description = 'Replenishment Order'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _order = 'date_order desc'

    name = fields.Char('Name', copy=False)
    warehouse_id = fields.Many2one('stock.warehouse', string='Receive Warehouse', track_visibility='onchange')
    supply_warehouse_id = fields.Many2one('stock.warehouse', string='Supply Warehouse', track_visibility='onchange')
    user_id = fields.Many2one('res.users', string='Responsible', required=False, default=lambda self: self.env.user, track_visibility='onchange')
    date_order = fields.Datetime(string='Date Order', default=fields.Datetime.now, track_visibility='onchange')
    receive_date = fields.Datetime(string='Receive Date', default=fields.Datetime.now, track_visibility='onchange')
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id, track_visibility='onchange')
    receive_picking_type_id = fields.Many2one('stock.picking.type', string="Receive Location",
                                              domain="[('code', '=', 'incoming'),('warehouse_id','=',warehouse_id)]", track_visibility='onchange')
    supply_picking_type_id = fields.Many2one('stock.picking.type', string="Supply Location",
                                             domain="[('code', '=', 'outgoing'), ('warehouse_id','=',supply_warehouse_id)]", track_visibility='onchange')

    state = fields.Selection([('draft', 'Draft'),
                              ('send', 'Send'),
                              ('confirmed', 'Confirmed'),
                              ('cancelled', 'Cancelled'),
                              ('done', 'Done')
                            ], index=True, string='State', readonly=True, default='draft',track_visibility='onchange')
    procurement_group_id = fields.Many2one('procurement.group', string='Procurement group', copy=False)
    note = fields.Text(string='Note')
    procurement_group_id = fields.Many2one('procurement.group', 'Procurement Group', copy=False)

    is_supplied = fields.Boolean('Supplied', default=False, compute='compute_transfer_status', search='_search_is_supplied')
    is_received = fields.Boolean('Received', default=False, compute='compute_transfer_status', search='_search_is_received')
    move_assign_date = fields.Datetime(string='Move assign date', default=False)

    @api.model
    def search(self, args, offset=0, limit=0, order=None, count=False):
        args = list(args)
        user = self.env['res.users'].browse(self._uid)
        for index in range(len(args)):
            if (type(args[index]) == list):
                if args[index][2]:
                    if args[index][2] == 'ODERP_WAREHOUSE':
                        args[index] = (args[index][0], args[index][1], user.allowed_warehouses.ids)
        return super(StockTransitOrder, self).search(args, offset=offset, limit=limit, order=order, count=count)

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        user = self.env['res.users'].browse(self._uid)
        domain = list(domain)
        for index in range(len(domain)):
            if (type(domain[index]) == list):
                if domain[index][2]:
                    if domain[index][2] == 'ODERP_WAREHOUSE':
                        domain[index] = (domain[index][0], domain[index][1], user.allowed_warehouses.ids)
        return super(StockTransitOrder, self).read_group(domain, fields, groupby, offset=offset, limit=limit, orderby=orderby, lazy=lazy)

    @api.model
    def _check_warehouse(self):
        if self.warehouse_id.id == self.supply_warehouse_id.id:
            return False
        return True

    _constraints = [
        (_check_warehouse, 'The supply warehouse must be different for receive warehouse.', [
            'warehouse_id', 'supply_warehouse_id']),
    ]

    @api.multi
    def _search_is_received(self, operator, value):
        orders = self.env['stock.transit.order'].sudo().search([])
        if operator == '=':
            orders = orders.filtered(lambda x: x.is_received)
        else:
            orders = orders.filtered(lambda x: not x.is_received)
        return [('id', 'in', orders.sudo().ids)] if orders else []
    
    @api.multi
    def _search_is_supplied(self, operator, value):
        orders = self.env['stock.transit.order'].sudo().search([])
        if operator == '=':
            orders = orders.filtered(lambda x: x.is_supplied)
        else:
            orders = orders.filtered(lambda x: not x.is_supplied)
        return [('id', 'in', orders.sudo().ids)] if orders else []
    
    @api.multi
    def compute_transfer_status(self):
        for obj in self:
            if obj.picking_ids:
                is_supplied = is_received = False
                for pick in obj.picking_ids:
                    if pick.picking_type_id.code == 'outgoing':
                        is_supplied = True if pick.state == 'done' else False
                    if pick.picking_type_id.code == 'incoming':
                        is_received = True if pick.state == 'done' else False
                if is_supplied:
                    obj.is_supplied = True
                if is_received:
                    obj.is_received = True

    @api.onchange('company_id')
    def onchange_company_id(self):
        if self.company_id:
            return {'domain': {'supply_warehouse_id': [('id', 'in', self.env.user.allowed_warehouses.ids), ('company_id', 'child_of', [self.company_id.id or self.env.user.company_id.id])],
                               'warehouse_id': [('id', 'in', self.env.user.allowed_warehouses.ids), ('company_id', 'child_of', [self.company_id.id or self.env.user.company_id.id])]}}

    @api.onchange('warehouse_id')
    def onchange_warehouse(self):
        if self.warehouse_id:
            stock_picking_type = self.env['stock.picking.type'].search([('warehouse_id', '=', self.warehouse_id.id), ('code', '=', 'incoming')], limit=1)
            if stock_picking_type:
                self.receive_picking_type_id = stock_picking_type.id
            if self.warehouse_id.resupply_wh_ids:
                return {'domain': {'supply_warehouse_id': [('id', 'in', self.warehouse_id.resupply_wh_ids.ids)]}}
        return {'domain': {'supply_warehouse_id': [('id', 'in', self.env.user.allowed_warehouses.ids), ('company_id', 'child_of', [self.company_id.id or self.env.user.company_id.id])]}}

    @api.onchange('supply_warehouse_id')
    def onchange_supply_warehouse(self):
        if self.supply_warehouse_id:
            stock_picking_type = self.env['stock.picking.type'].search([('warehouse_id', '=', self.supply_warehouse_id.id), ('code', '=', 'outgoing')], limit=1)
            if stock_picking_type:
                self.supply_picking_type_id = stock_picking_type.id
            allowed_warehouses = self.env['stock.warehouse'].search([('id', '=', self.supply_warehouse_id.id)]).resupply_wh_ids
            if allowed_warehouses:
                return {'domain': {'warehouse_id': [('id', 'in', allowed_warehouses.ids)]}}


class InheritedStockTransitOrder(models.Model):
    _inherit = 'stock.transit.order'

    @api.depends('picking_ids')
    @api.multi
    def _count_all(self):
        shipment_count = 0
        receipt_count = 0
        for pick in self.picking_ids:
            if pick.picking_type_id.code == 'incoming':
                receipt_count += 1
            elif pick.picking_type_id.code == 'outgoing':
                shipment_count += 1
        self.shipment_count = shipment_count
        self.receipt_count = receipt_count

    picking_ids = fields.One2many('stock.picking', 'transit_order_id', 'Picking List', readonly=True)
    order_line = fields.One2many('stock.transit.order.line', 'transit_order_id', 'Order Lines', readonly=True, states={'draft': [('readonly', False)]}, copy=True)
    product_id = fields.Many2one('product.product', related='order_line.product_id', string='Product')
    shipment_count = fields.Integer(string='Outgoing Shipments', store=True, readonly=True, compute='_count_all')
    receipt_count = fields.Integer(string='Incoming Shipments', store=True, readonly=True, compute='_count_all')
    approved_date = fields.Datetime('Approved date')

    @api.multi
    def send_supply_chain_manager(self):
        ''' Supply Chain Manager үүдийг баримтын дагагчаар нэмж баримтыг
            илгээгдсэн төлөвт оруулна.
        '''
        # # АГУУЛАХАД БАРАА ТОХИРУУЛАХ чек чеклэгдсэн бол барааг шалгах
        self.action_send()
        
        if not self.order_line:
            raise UserError(
                _('You cannot confirm replenishment order without any order lines.'))
        if self.warehouse_id.id == self.supply_warehouse_id.id:
            raise UserError(
                _('You cannot confirm replenishment order with same warehouses !'))

        for order in self:
            for line in order.order_line:
                if not line.product_qty:
                    raise UserError(_(
                        'Cannot confirm replenishment order !\n Because there are zero quantitied product in this replenishment order.'))

        order_name = self.env['ir.sequence'].next_by_code('stock.transit.order')

        group_supply_chain_manager_id = self.env.ref(
            'l10n_mn_stock.group_supply_chain_manager').id
        user_ids = self.env['res.users'].search(
            [('groups_id', 'in', [group_supply_chain_manager_id])])
        partner_ids = []
        for user in user_ids:
            partner_ids.append(user.partner_id)
        if not partner_ids:
            raise UserError(
                _('System not defined supply chain manager'))
        if partner_ids != []:
            mail_invite = self.env['mail.wizard.invite'].with_context({
                'default_res_model': 'stock.transit.order',
                'default_res_id': self.id}).create({
                'partner_ids': [(6, 0, [p.id for p in partner_ids])], 'send_mail': True})
            # Энэ функц нөхөн дүүргэлтийн үйлдлийг маш удааж байгаа тул тайлбар болгов. Хурдан ажилладаг болгож кодыг засах хэрэгтэй. Өнөржаргал
            # mail_invite.add_followers()
        self.write({'state': 'send', 'name': order_name})

    def _prepare_procurement_group(self):
        return {'name': self.name}

    @api.multi
    def _create_out_picking(self):
        StockPicking = self.env['stock.picking']
        for order in self:
            if any([ptype in ['product', 'consu'] for ptype in order.order_line.mapped('product_id.type')]):
                pickings = order.picking_ids.filtered(lambda x: x.state not in ('done', 'cancel') and x.picking_type_id.code == 'outgoing')
                if not pickings:
                    picking = StockPicking.create({
                        'picking_type_id': self.supply_picking_type_id.id,
                        'transit_order_id': self.id,
                        'date': self.receive_date,
                        'origin': self.name,
                        'location_dest_id': self.company_id.internal_transit_location_id.id,
                        'location_id': self.supply_picking_type_id.default_location_src_id.id if self.supply_picking_type_id and self.supply_picking_type_id.default_location_src_id else self.supply_warehouse_id.lot_stock_id.id,
                        'company_id': self.company_id.id,
                    })
                else:
                    picking = pickings[0]
                if picking.transit_order_id and picking.transit_order_id.approved_date:
                    for line in picking.transit_order_id.order_line:
                        line.date_planned = picking.transit_order_id.approved_date
                moves = order.order_line._create_out_stock_moves(picking)
                moves = moves.filtered(lambda x: x.state not in (
                    'done', 'cancel')).action_confirm()
                # moves.force_assign()
                picking.message_post_with_view('mail.message_origin_link',
                                               values={'self': picking,
                                                       'origin': order},
                                               subtype_id=self.env.ref('mail.mt_note').id)
        return True

    @api.multi
    def wkf_confirm_transit(self):
        self.write({
            'state': 'confirmed',
            'approved_date': fields.Datetime.now(),
        })
        self._create_picking()
        self._create_out_picking()

    @api.multi
    def action_send(self):
        
        def get_error_message(transit_order, wh, products):
            products.sort(key=lambda x: x.name)
            product_names = u"\n ".join([u"[" + product.default_code + u"] " + product.name for product in products])
            if wh:
                return _("\n\nBelown products only exist at '%s' warehouse:\n%s") % (wh.name, product_names)
            else:
                return _("\n\nBelown products not exist at neither '%s' nor '%s':\n%s") % (transit_order.supply_warehouse_id.name, transit_order.warehouse_id.name, product_names)
        
        for obj in self:
            # АГУУЛАХАД БАРАА ТОХИРУУЛАХ чек чеклэгдсэн байгаад 2 агуулахад хоёуланд нь бараа тохируулсан бол
            if obj.company_id.configure_allowed_product_on_wh and obj.order_line and obj.supply_warehouse_id and obj.warehouse_id and obj.supply_warehouse_id.allowed_product_ids and obj.warehouse_id.allowed_product_ids:
                intersection = list(set(obj.supply_warehouse_id.allowed_product_ids) & set(obj.warehouse_id.allowed_product_ids)) # 2 агуулахын барааны огтлолцлолыг олох
                strange_lines = obj.order_line.filtered(lambda x: x.product_id not in intersection) # 2 агуулахын барааны огтлолцлолд байхгүй бараа бүхий захиалгын мөрүүдийг олох
                if strange_lines:
                    strange_products = [line.product_id.sudo() for line in strange_lines]
                    
                    # Аль агуулахад байхгүй байгааг шүүх
                    strange_products_at_supply_wh = list(set(obj.supply_warehouse_id.allowed_product_ids) & set(strange_products))
                    strange_products_at_wh = list(set(obj.warehouse_id.allowed_product_ids) & set(strange_products))
                    strange_products = list(set(strange_products) - set(strange_products_at_supply_wh) - set(strange_products_at_wh))
                    
                    # Аль агуулахад байхгүй байгаа талаарх дэлгэрэнгүй raise өгөх
                    validation_error = _("Cannot make transition from '%s' to '%s' !!!") % (obj.supply_warehouse_id.name, obj.warehouse_id.name)
                    if strange_products_at_supply_wh:
                        validation_error += get_error_message(obj, obj.supply_warehouse_id, strange_products_at_supply_wh)
                    if strange_products_at_wh:
                        validation_error += get_error_message(obj, obj.warehouse_id, strange_products_at_wh)
                    if strange_products:
                        validation_error += get_error_message(obj, False, strange_products)
                    
                    raise ValidationError(validation_error)
        
    @api.multi
    def action_cancel_draft(self):
        ''' Цуцласан захиалгыг ноорог болгох
        '''
        wf_service = netsvc.LocalService("workflow")
        for order in self:
            order.write({'state': 'draft',
                         'is_supplied': False,
                        'is_received': False,
                         'move_assign_date': False})
            wf_service.trg_delete(
                self.env.uid, 'stock.transit.order', order.id, self.env.cr)
            wf_service.trg_create(
                self.env.uid, 'stock.transit.order', order.id, self.env.cr)
            message = u"'%s' дугаартай цуцлагдсан нөхөн дүүргэлтийн захиалгыг ноорог төлөвт шилжүүллээ." % order.name
            self.message_post(body=message)
        return True

    @api.multi
    def action_cancel(self):
        ''' Нөхөн дүүргэлтийн захиалгыг цуцална. Агуулахын хөдөлгөөн үүссэн бөгөөд
            шилжсэн тохиолдолд цуцлагдахгүй. Шилжээгүй тохиолдолд үүссэн агуулахын хөдөлгөөнүүдийг
            устгаж нөхөн дүүргэлтийн захиалгыг цуцалсан төлөвт оруулна.
        '''
        for order in self:
            for pick in order.picking_ids:
                if pick.picking_type_id.code == 'incoming':
                    if pick.state in ('done'):
                        raise UserError(
                            _(
                                'Cannot cancel replenishment order !\n Because picking state is Done of reception attached to this replenishment order.'))
                    else:
                        pick.action_cancel()
                        pick.unlink()
                else:
                    if pick.state in ('done'):
                        raise UserError(_(
                            'Cannot cancel replenishment order ! \n Because picking state is Done shipping document attached to this replenishment order.'))
                    else:
                        pick.action_cancel()
                        pick.unlink()
            order.write({'move_assign_date': False})
            self.signal_workflow('action_cancel')

            message = u"'%s' нөхөн дүүргэлтийн захиалгыг цуцаллаа." % order.name
            self.message_post(body=message)
        return True

    @api.multi
    def wkf_procurement_done(self):
        message = _("Replenishment receivement has been complete.")
        self.message_post(body=message)
        return self.write({'state': 'done'})

    @api.multi
    def check_done(self):
        ''' Бараа хүлээн авах захиалгыг дуусгах үед
            нөхөн дүүргэлтийн захиалгыг дууссан төлөвт шилжүүлнэ.
        '''
        ok = True
        for picking in self.picking_ids:
            if picking.picking_type_id.code == 'incoming' and picking.state not in ('done'):
                ok = False
            elif picking.picking_type_id.code == 'outgoing' and picking.state not in ('done'):
                ok = False
        if ok:
            self.signal_workflow('action_done')
        return ok

    @api.model
    def _prepare_picking(self):
        if not self.procurement_group_id:
            self.procurement_group_id = self.procurement_group_id.create({
                'name': self.name,
            })
        return {
            'picking_type_id': self.receive_picking_type_id.id,
            'transit_order_id': self.id,
            'date': self.receive_date,
            'origin': self.name,
            'location_dest_id': self.receive_picking_type_id.default_location_dest_id.id,
            'location_id': self.company_id.internal_transit_location_id.id,
            'company_id': self.company_id.id,
        }

    @api.multi
    def _create_picking(self):
        StockPicking = self.env['stock.picking']
        for order in self:
            if any([ptype in ['product', 'consu'] for ptype in order.order_line.mapped('product_id.type')]):
                pickings = order.picking_ids.filtered(
                    lambda x: x.state not in ('done', 'cancel'))
                if not pickings:
                    res = order._prepare_picking()
                    picking = StockPicking.create(res)
                else:
                    picking = pickings[0]
                if picking.transit_order_id and picking.transit_order_id.approved_date:
                    delta = 1
                    planned_date = datetime.now() + relativedelta(seconds=+delta)
                    for line in picking.transit_order_id.order_line:
                        line.date_planned = planned_date
                moves = order.order_line._create_stock_moves(picking)
                moves = moves.filtered(lambda x: x.state not in (
                    'done', 'cancel')).action_confirm()
                # moves.force_assign()
                picking.message_post_with_view('mail.message_origin_link',
                                               values={'self': picking,
                                                       'origin': order},
                                               subtype_id=self.env.ref('mail.mt_note').id)
        return True

    @api.multi
    def view_picking(self):
        context = self._context or {}
        if context.get('type'):
            type = context.get('type')

            pick_ids = []
            action = self.env.ref('stock.action_picking_tree_all').read()[0]
            for picking in self.picking_ids:
                if picking.picking_type_id.code == type:
                    pick_ids.append(picking.id)
            if len(pick_ids) > 1:
                action['domain'] = [('id', 'in', pick_ids)]
            elif pick_ids:
                action['views'] = [
                    (self.env.ref('stock.view_picking_form').id, 'form')]
                action['res_id'] = pick_ids[0]
        return action

    def unlink(self):
        for order in self:
            if order.state not in ('draft', 'cancelled'):
                raise UserError(
                    _('You can not delete this transit order. You can only be deleted during the drafts!'))
        return super(InheritedStockTransitOrder, self).unlink()

    def action_force_cancel(self):
        receive_picking_ids = self.mapped('picking_ids').filtered(lambda x: x.picking_type_id in self.mapped('receive_picking_type_id'))
        receive_picking_ids.action_force_cancel()
        other_picking_ids = self.mapped('picking_ids') - receive_picking_ids
        other_picking_ids.action_force_cancel()
        self.action_cancel()
        self.write({'state': 'cancelled'})