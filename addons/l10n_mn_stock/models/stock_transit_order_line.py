# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import time
from odoo import api, fields, models, _
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import odoo.addons.decimal_precision as dp
from odoo.exceptions import UserError
import logging

_logger = logging.getLogger(__name__)


class StockTransitOrderLine(models.Model):
    _name = 'stock.transit.order.line'
    _order = 'id'

    @api.depends('product_id')
    def _get_qty(self):
        move_qty = 0
        qty = 0
        for line in self:
            if line.product_id:
                warehouse = line.transit_order_id.supply_warehouse_id
                if not warehouse:
                    raise UserError(_('Warning!\nYou must select supply warehouse before add order line!'))
                product = line.product_id
                if line.transit_order_id.supply_picking_type_id.default_location_src_id:
                    locations = [line.transit_order_id.supply_picking_type_id.default_location_src_id]
                else:
                    locations = self.env['stock.location'].search([('usage', '=', 'internal'), ('location_id', 'child_of', [warehouse.view_location_id.id])])
                loc_ids = [loc.id for loc in locations or []]
                line.availability = product.get_qty_availability(loc_ids, line.transit_order_id.date_order, state=['done'])
                line.assigned_qty = product.get_qty_availability(loc_ids, line.transit_order_id.date_order, state=['assigned'])
                
                line.product_uom = product.uom_id.id
                line.name = product.name

    # @api.model
    def _get_availability(self):
        self.availability = self.product_qty

    name = fields.Text('Description', required=True)
    company_id = fields.Many2one(
        'res.company', 'Company',
        default=lambda self: self.env['res.company']._company_default_get(
            'stock.transit.order.line'),
        required=True)
    date_planned = fields.Datetime(
        'Scheduled Date', default=fields.Datetime.now,
        required=True, index=True, track_visibility='onchange')
    product_id = fields.Many2one(
        'product.product', 'Product',
        readonly=True, required=True,
        states={'draft': [('readonly', False)]}, domain=[('type', '!=', 'service')])
    product_qty = fields.Float(
        'Quantity',
        digits=dp.get_precision('Product Unit of Measure'),
        readonly=True, required=True,
        states={'draft': [('readonly', False)]})
    product_uom = fields.Many2one(
        'product.uom', 'Product Unit of Measure',
        readonly=True, required=True,
        states={'draft': [('readonly', False)]})
    state = fields.Selection(related="transit_order_id.state", store=True, string='Status', default='draft', copy=False, required=True, track_visibility='onchange')
    transit_order_id = fields.Many2one(
        'stock.transit.order', string='Parent Transit Order', ondelete='cascade')
    availability = fields.Float(compute='_get_qty', string='Available Qty')
    assigned_qty = fields.Float(compute='_get_qty', string='Assigned Qty')
    price_unit = fields.Float(compute='_get_unit_cost', string='Price Unit')
    sub_total = fields.Float(compute='_compute_amount', string='Sub Total', readonly=True)
    procurement_ids = fields.One2many(
        'procurement.order', 'transit_line_id', string='Procurements')
    route_id = fields.Many2one('stock.location.route', string='Route', domain=[
        ('sale_selectable', '=', True)])
    supply_warehouse_id = fields.Many2one('stock.warehouse', related='transit_order_id.supply_warehouse_id', store=True, readonly=True, search='_search_supply_warehouse_id', string='Supply Warehouse')
    warehouse_id = fields.Many2one('stock.warehouse', related='transit_order_id.warehouse_id', store=True, readonly=True, search='_search_warehouse_id', string='Receive Warehouse')
    date_order = fields.Datetime(related='transit_order_id.date_order', store=True, readonly=True, string='Date Order')
    configure_allowed_product_on_wh = fields.Boolean(compute='_compute_configure_allowed_product_on_wh', string="Configure Allowed Product on Warehouse")

    @api.model
    def search(self, args, offset=0, limit=0, order=None, count=False):
        args = list(args)
        user = self.env['res.users'].browse(self._uid)
        for index in range(len(args)):
            if (type(args[index]) == list):
                if args[index][2]:
                    if args[index][2] == 'ODERP_WAREHOUSE':
                        args[index] = (args[index][0], args[index][1], user.allowed_warehouses.ids)
        return super(StockTransitOrderLine, self).search(args, offset=offset, limit=limit, order=order, count=count)

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        user = self.env['res.users'].browse(self._uid)
        domain = list(domain)
        for index in range(len(domain)):
            if (type(domain[index]) == list):
                if domain[index][2]:
                    if domain[index][2] == 'ODERP_WAREHOUSE':
                        domain[index] = (domain[index][0], domain[index][1], user.allowed_warehouses.ids)
        return super(StockTransitOrderLine, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                         orderby=orderby, lazy=lazy)

    def _search_supply_warehouse_id(self, operator, value):
        res = []
        if value:
            res = self.env['stock.transit.order.line'].search(['|', ('transit_order_id.supply_warehouse_id.name', 'ilike', value), ('transit_order_id.supply_warehouse_id.code', 'ilike', value)])
        return [('id', operator, res)]
    
    def _search_warehouse_id(self, operator, value):
        res = []
        if value:
            res = self.env['stock.transit.order.line'].search(['|', ('transit_order_id.warehouse_id.name', 'ilike', value), ('transit_order_id.warehouse_id.code', 'ilike', value)])
        return [('id', operator, res)]
        
    @api.onchange('supply_warehouse_id', 'warehouse_id', 'configure_allowed_product_on_wh')
    def set_domain_for_product(self):
        # Компанийн тохиргоонд АГУУЛАХАД БАРАА ТОХИРУУЛАХ чеклэгдсэн бол Нийлүүлэх/Хүргэх агуулахуудын барааны давхцлаар домайндав.
        if self.env.user.company_id.configure_allowed_product_on_wh:
            domain = {}
             
            if self.supply_warehouse_id and self.warehouse_id and self.supply_warehouse_id.allowed_product_ids and self.warehouse_id.allowed_product_ids:
                intersection = list(set(self.supply_warehouse_id.allowed_product_ids) & set(self.warehouse_id.allowed_product_ids))
                domain['product_id'] = [('id', 'in', [pro.id for pro in intersection])]
            else:
                domain['product_id'] = []
                
            return {'domain': domain}

    def _compute_configure_allowed_product_on_wh(self):
        for obj in self:
            obj.configure_allowed_product_on_wh = obj.company_id.sudo().configure_allowed_product_on_wh if obj.company_id else self.env.user.company_id.sudo().configure_allowed_product_on_wh
        
    @api.multi
    def _prepare_order_line_procurement(self, group_id=False):
        self.ensure_one()
        return {
            'name': self.name,
            'origin': self.transit_order_id.name,
            'date_planned': datetime.strptime(self.transit_order_id.date_order, DEFAULT_SERVER_DATETIME_FORMAT),
            'product_id': self.product_id.id,
            'product_qty': self.product_qty,
            'product_uom': self.product_uom.id,
            'company_id': self.transit_order_id.company_id.id,
            'group_id': group_id,
            'transit_line_id': self.id,
            'location_id': self.transit_order_id.company_id.internal_transit_location_id.id,
            'route_ids': self.route_id and [(4, self.route_id.id)] or [],
            'warehouse_id': self.transit_order_id.supply_warehouse_id and self.transit_order_id.supply_warehouse_id.id or False,
            # 'partner_dest_id': self.transit_order_id.partner_shipping_id.id,
        }

    @api.multi
    def _action_procurement_create(self):
        """
        Create procurements based on quantity ordered. If the quantity is increased, new
        procurements are created. If the quantity is decreased, no automated action is taken.
        """
        new_procs = self.env['procurement.order']
        for line in self:
            if not line.transit_order_id.procurement_group_id:
                vals = line.transit_order_id._prepare_procurement_group()
                line.transit_order_id.procurement_group_id = self.env[
                    "procurement.group"].create(vals)

            vals = line._prepare_order_line_procurement(
                group_id=line.transit_order_id.procurement_group_id.id)
            vals['product_qty'] = line.product_qty
            new_proc = self.env["procurement.order"].with_context(
                procurement_autorun_defer=True).create(vals)
            new_proc.message_post_with_view('mail.message_origin_link',
                                            values={
                                                'self': new_proc, 'origin': line.transit_order_id},
                                            subtype_id=self.env.ref('mail.mt_note').id)
            new_procs += new_proc
        new_procs.run()
        return new_procs

    @api.multi
    def _prepare_stock_moves(self, picking):
        """ Prepare the stock moves data for one order line. This function returns a list of
        dictionary ready to be used in stock.move's create()
        """
        self.ensure_one()
        res = []
        if self.product_id.type not in ['product', 'consu']:
            return res
        procurement = self.env['procurement.order'].search([('transit_line_id', '=', self.id)])[0] if self.env['procurement.order'].search([('transit_line_id', '=', self.id)]) else False
        template = {
            'name': self.name or '',
            'product_id': self.product_id.id,
            'product_uom': self.product_uom.id,
            'product_uom_qty': self.product_qty,
            'date': self.transit_order_id.date_order,
            'date_expected': self.date_planned,
            'location_id': self.transit_order_id.company_id.internal_transit_location_id.id,
            'location_dest_id': self.transit_order_id.receive_picking_type_id.default_location_dest_id.id if self.transit_order_id.receive_picking_type_id and self.transit_order_id.receive_picking_type_id.default_location_dest_id else self.transit_order_id.warehouse_id.in_type_id.default_location_dest_id.id,
            'picking_id': picking.id,
            'partner_id': False,
            'move_dest_id': False,
            'state': 'draft',
            'company_id': self.transit_order_id.company_id.id,
            'price_unit': self.price_unit,
            'picking_type_id': self.transit_order_id.warehouse_id.in_type_id.id,
            'group_id': self.transit_order_id.procurement_group_id.id,
            'procurement_id': procurement.id if procurement else False,
            'origin': self.transit_order_id.name,
            'route_ids': self.transit_order_id.warehouse_id.in_type_id.warehouse_id and [(6, 0, [x.id for x in self.transit_order_id.warehouse_id.in_type_id.warehouse_id.route_ids])] or [],
            'warehouse_id': self.transit_order_id.warehouse_id.id,
        }
        res.append(template)
        return res

    @api.multi
    def _create_stock_moves(self, picking):
        moves = self.env['stock.move']
        done = self.env['stock.move'].browse()
        for line in self:
            for val in line._prepare_stock_moves(picking):
                done += moves.create(val)
        return done

    @api.multi
    def _create_out_stock_moves(self, picking):
        moves = self.env['stock.move']
        done = self.env['stock.move'].browse()
        for line in self:
            for val in line._prepare_out_stock_moves(picking):
                done += moves.create(val)
        return done

    @api.multi
    def _prepare_out_stock_moves(self, picking):
        """ Prepare the out stock moves data for one order line. This function returns a list of
        dictionary ready to be used in stock.move's create()
        """
        self.ensure_one()
        res = []
        if self.product_id.type not in ['product', 'consu']:
            return res
        procurement = self.env['procurement.order'].search([('transit_line_id', '=', self.id)])[0] if self.env['procurement.order'].search([('transit_line_id', '=', self.id)]) else False
        template = {
            'name': self.name or '',
            'product_id': self.product_id.id,
            'product_uom': self.product_uom.id,
            'product_uom_qty': self.product_qty,
            'date': self.transit_order_id.date_order,
            'date_expected': self.date_planned,
            'location_id': self.transit_order_id.supply_picking_type_id.default_location_src_id.id if self.transit_order_id.supply_picking_type_id and self.transit_order_id.supply_picking_type_id.default_location_src_id else self.transit_order_id.supply_warehouse_id.out_type_id.default_location_src_id.id,
            'location_dest_id': self.transit_order_id.company_id.internal_transit_location_id.id,
            'picking_id': picking.id,
            'partner_id': False,
            'move_dest_id': False,
            'state': 'draft',
            'company_id': self.transit_order_id.company_id.id,
            'price_unit': self.price_unit,
            'picking_type_id': self.transit_order_id.supply_picking_type_id.id if self.transit_order_id.supply_picking_type_id else self.transit_order_id.warehouse_id.out_type_id.id,
            'group_id': self.transit_order_id.procurement_group_id.id or False,
            'procurement_id': procurement.id if procurement else False,
            'origin': self.transit_order_id.name,
            'route_ids': self.transit_order_id.warehouse_id.out_type_id.warehouse_id and [(6, 0, [x.id for x in self.transit_order_id.warehouse_id.out_type_id.warehouse_id.route_ids])] or [],
            'warehouse_id': self.transit_order_id.supply_warehouse_id.id,
        }
        res.append(template)
        return res

    @api.onchange('product_id', 'product_qty', 'product_uom')
    def onchange_product_id(self):
        result = {}
        if not self.product_id:
            self.product_uom = False
            self.name = False
            self.product_qty = False
        else:
            product = self.product_id
            self.product_uom = product.uom_id.id
            self.name = product.name
            self.price_unit = product.standard_price
            result['domain'] = {'product_uom': [('category_id', '=', product.uom_id.category_id.id)]}
        return result

    @api.onchange('product_id', 'product_qty')
    def onchange_product_qty(self):
        if self.env.user.company_id.transit_order_types == 'transit_order_quant':
            product_qty = self.product_qty
            availability = self.availability
            if availability < product_qty:
                self.product_qty = availability
            else:
                self.product_qty = product_qty

    @api.depends('product_id')
    def _get_unit_cost(self):
        for line in self:
            unit_cost = line.product_id.standard_price
            
            if self.is_module_installed("l10n_mn_stock_account_cost_for_each_wh"):
                wh_price = self.env['product.warehouse.standard.price'].search([('product_id','=',line.product_id.id),('warehouse_id','=',line.transit_order_id.supply_warehouse_id.id)])
                if wh_price:
                    unit_cost = wh_price.standard_price
            line.price_unit = unit_cost
            
    @api.depends('product_qty', 'price_unit')
    def _compute_amount(self):
        """
        Compute the sub_total of the Transit Order line.
        """
        for line in self:
            line.sub_total = line.price_unit * line.product_qty
    
    def is_module_installed(self, module_name):
        self._cr.execute("SELECT id FROM ir_module_module WHERE name = '%s' AND state IN ('installed', 'to upgrade')" %module_name)
        results = self._cr.dictfetchall()
        
        if results and len(results) > 0:
            return True
        else:
            return False