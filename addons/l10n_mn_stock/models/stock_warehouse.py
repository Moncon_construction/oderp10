# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _

class StockWarehouse(models.Model):
    _inherit = 'stock.warehouse'
    
    code = fields.Char('Short Name', required=True, size=15, help="Short name used to identify your warehouse")
    allowed_users = fields.Many2many(
        'res.users', 'res_users_warehouses_rel', 'wid', 'uid', string='Allowed Users')
    lock_move = fields.Boolean('Lock Stock Move', default=False)
    lock_move_until = fields.Datetime('Lock Until') 
    is_internal_counting = fields.Boolean('Is Internal Counting?', default= True)
    configure_allowed_product_on_wh = fields.Boolean(related="company_id.configure_allowed_product_on_wh", default=lambda self: self.company_id.configure_allowed_product_on_wh if self.company_id else self.env.user.company_id.configure_allowed_product_on_wh, string="Configure Allowed Product on Warehouse")
    allowed_product_ids = fields.Many2many('product.product', 'wh_products_rel', 'wh_id', 'product_id', string='Allowed Products')


    def create_resupply_routes(self, supplier_warehouses, default_resupply_wh):
        res = super(StockWarehouse, self).create_resupply_routes(
            supplier_warehouses, default_resupply_wh)
        Route = self.env['stock.location.route']
        for supplier_wh in supplier_warehouses:
            inter_wh_route = Route.create(
                self._get_inter_warehouse_route_values(supplier_wh))
            (self | supplier_wh).write({'route_ids': [(4, inter_wh_route.id)]})
        return res

    @api.multi
    def button_stock_location(self):
        return {
            'name': _('Stock Locations'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'stock.location',
            'type': 'ir.actions.act_window',
            'domain': [('id', 'child_of', self.view_location_id.ids), ('usage', '=', 'internal')],
        }