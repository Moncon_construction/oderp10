# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, models
from odoo.addons.l10n_mn_report.models.report_helper import verbose_numeric, convert_curr


class ReportStockPicking(models.AbstractModel):
    _name = 'report.l10n_mn_stock.report_picking'

    @api.multi
    def render_html(self, docids, data=None, ids=[]):

        report_obj = self.env['report']
        report = report_obj._get_report_from_name('l10n_mn_stock.report_picking')

        picking_ids = self.env['stock.picking'].browse(docids)
        picking_product_lines = {}
        totals = {}
        verbose_total_dict = {}
        with_lot_and_price = {}
        sale_pickings = {}
        purchase_pickings = {}
        wh_pickings = {}
        origin_notes = {}

        # Олон шилжүүлгийн баримтуудыг хэвлэдэг болгов
        for picking in picking_ids:
            with_lot_and_price[picking.id], purchase_pickings[picking.id], sale_pickings[picking.id] = None, None, None
            wh_pickings[picking.id] = {'warehouse': None, 'seller_phone': None, 'seller': None, 'team': None}
            amount_taxed, amount_untaxed, amount_total = 0, 0, 0
            total_qty, total_qty_box = 0, 0
            product_lines = []
            returned_from_po = False

            # Худалдан авалтаас үүссэн ажилбар бол ХА-ын тэмдэглэлийг дамжуулах
            origin_notes[picking.id] = {'created_from_po': False, 'has_note': False, 'note': ''}
            linked_orders = self.env['purchase.order'].sudo()
            linked_orders_moves = picking.move_lines.filtered(lambda x: x.state != 'cancel' and x.purchase_line_id)
            if linked_orders_moves:
                linked_orders |= linked_orders_moves.mapped('purchase_line_id').mapped('order_id')
                if linked_orders:
                    notes = u",\n".join([(u" " + order.notes if order.notes else '') for order in linked_orders])
                    origin_notes[picking.id] = {'created_from_po': True, 'has_note': True if notes else False, 'note': notes}

            # Худалдан авалтаас үүссэн ажилбараас буцаагдсан ажилбар бол ХА-ын тэмдэглэлийг дамжуулах
            if not linked_orders:
                linked_orders_moves = picking.move_lines.filtered(lambda x: x.state != 'cancel' and x.origin_returned_move_id)
                if linked_orders_moves:
                    returned_moves = linked_orders_moves.mapped('origin_returned_move_id')
                    while returned_moves.mapped('origin_returned_move_id'):
                        returned_moves = returned_moves.mapped('origin_returned_move_id')
                    linked_orders |= returned_moves.mapped('purchase_line_id').mapped('order_id')
                    if linked_orders:
                        returned_from_po = True
                        notes = u",\n".join([(u" " + ((order.name + u":\t" + order.notes) if order.notes else order.name)) for order in linked_orders])
                        origin_notes[picking.id] = {'created_from_po': True, 'has_note': True if notes else False, 'note': notes}

            origin_notes[picking.id]['register'] = picking.partner_id.register if picking.partner_id and 'register' in picking.partner_id and picking.partner_id.register else ''

            for move in picking.move_lines:
                if move.state == 'cancel':
                    continue

                product_id = move.product_id
                box_total = round(move.product_uom_qty / product_id.product_quantity_in_box, 2) if product_id.product_quantity_in_box != 0 else 0
                total_qty_box += box_total
                total_qty += move.product_uom_qty

                product_line = {
                    'name': product_id.name,
                    'barcode': product_id.barcode,
                    'default_code': product_id.default_code,
                    'uom': product_id.product_tmpl_id.uom_id.name,
                    'lot': '',
                    'qty': move.product_uom_qty,
                    'box_number': box_total,
                    'cost': move.price_unit,
                    'price': '',
                    'amount': move.price_unit * move.product_uom_qty,
                }

                #  Доорхи 8 мөрийг төслийн custom модульд ашиглаж байгаа тул БҮҮ ХҮР!!!
                if move.purchase_line_id: # Тайлангийн мөр дээр худалдан авалтын мөрийн утгуудыг дуудахын тулд:
                    po_line = move.purchase_line_id
                    product_line.update({
                        'purchase_amount_taxed': po_line.price_tax,
                        'purchase_amount_untaxed': po_line.price_subtotal,
                        'purchase_amount_total': po_line.price_total,
                        'price': po_line.price_unit, # Өмнө хоосон байсан price мөрийг худалдан авалтын мөрийн нэгж үнийг авдаг болгов. Гэхдээ татвар шингэснийг сонгосон тохиолдолд зөв ажиллана.
                    })
                    purchase_pickings[picking.id] = po_line.order_id

                # Зарлагын баримт мөн эсэхийг мөн хэрэглэгч нь серийн дугаар ашигладаг эсэхийг шалгаж байна
                if self.env.user.has_group('stock.group_production_lot') and picking.location_id.usage == 'internal' and picking.location_dest_id.usage != 'internal':
                    lot_id = self.env['stock.production.lot'].sudo().search([('product_id', '=', product_id.id)], limit=1)
                    if lot_id:
                        product_line.update({'lot': lot_id.name if lot_id else '', 'price': product_id.list_price})
                        with_lot_and_price[picking.id] = True

                # Борлуулалтын захиалгын хүргэлтээрх бараа материалын баримт зарах үнээр гарна.
                sale_line_id = move.procurement_id.sale_line_id
                if sale_line_id:
                    sale_pickings[picking.id] = sale_line_id.order_id

                    price = sale_line_id.price_unit * (1 - (sale_line_id.discount or 0.0) / 100.0)
                    taxes = sale_line_id.tax_id.compute_all(price, sale_line_id.order_id.currency_id, 1,
                                                            product=sale_line_id.product_id,
                                                            partner=sale_line_id.order_id.partner_shipping_id)
                    price_unit_tax = taxes['total_included'] - taxes['total_excluded'] # Нэгж барааны татварын дүн
                    price_unit_included = taxes['total_included'] # Нэгж барааны татвартай дүн

                    total_included = price_unit_included * move.product_uom_qty
                    total_tax_amount = price_unit_tax * move.product_uom_qty
                    total_excluded = total_included - total_tax_amount

                    product_line.update({
                        'cost': price_unit_included,
                        'amount': total_included
                    })

                    wh_pickings[picking.id]['warehouse'] = sale_line_id.order_id.warehouse_id.name
                    wh_pickings[picking.id]['seller_phone'] = "%s, %s" % (sale_line_id.order_id.user_id.phone, sale_line_id.order_id.user_id.mobile)
                    wh_pickings[picking.id]['seller'] = sale_line_id.order_id.user_id.name
                    wh_pickings[picking.id]['team'] = sale_line_id.order_id.team_id.name

                    amount_taxed += total_tax_amount
                    amount_untaxed += total_excluded
                    amount_total += total_included
                else:
                    amount_total += (move.price_unit * move.product_uom_qty)

                product_lines.append(product_line)

            ptaxed, puntax, pamount_total, total_cost = 0, 0, 0, 0
            order_id = purchase_pickings[picking.id]
            if order_id:
                ptaxed = order_id.amount_tax
                puntax = order_id.amount_untaxed
                pamount_total = order_id.amount_total
                total_cost = order_id.total_cost
            elif returned_from_po:
                # TODO: Өмнө нь байсан код нь ХА-ын буцаалтын баримт дээр татвар гарахааргүй байсан тул тэрийг хийлгүй алдаатай код засаад орхив.
                puntax = amount_total

            totals[picking.id] = {
                'qty' : total_qty,
                'quintity_box' : total_qty_box,
                'amount_taxed': amount_taxed,
                'amount_untaxed': amount_untaxed,
                'amount_total': amount_total,
                'purchase_amount_taxed': ptaxed,
                'purchase_amount_untaxed': puntax,
                'purchase_amount_total': pamount_total,
                'total_cost': total_cost,
            }
            amt = verbose_numeric(abs(totals[picking.id]['amount_total']))
            verbose_total_dict[picking.id] = {'by_word': convert_curr(amt, u'төгрөг', u'мөнгө')}
            picking_product_lines[picking.id] = product_lines

        docargs = {
            'doc_ids': docids,
            'doc_model': report.model,
            'docs': picking_ids,
            'lines': picking_product_lines,
            'totals': totals,
            'verbose_total': verbose_total_dict,
            'with_lot_and_price': with_lot_and_price,
            'sale_pickings': sale_pickings,
            'purchase_pickings': purchase_pickings,
            'wh_pickings': wh_pickings,
            'origin_notes': origin_notes
        }
        return report_obj.render('l10n_mn_stock.report_picking', docargs)
