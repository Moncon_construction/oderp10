# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, models


class StockInventoryPrint(models.AbstractModel):
    _name = 'report.l10n_mn_stock.print_stock_inventory_count'

    @api.multi
    def render_html(self, docargs, data=None):
        context = self._context.copy() or {}
        inventory_obj = self.env['stock.inventory']

        active_ids = context['active_ids']
        inventories = inventory_obj.browse(active_ids)
        lines = []

        for inv in inventories:
            if inv.line_ids:
                for l in inv.line_ids:
                    name = ''
                    if l.product_code:
                        name += '[' + l.product_code + ']'
                    name += l.product_name

                    line = {
                        'name': name,
                        'barcode': l.product_id and l.product_id.default_code or '',
                        'uom': l.product_uom_id and l.product_uom_id.name or '',
                        'prodlot': (l.prodlot_name or (l.prod_lot_id and l.prod_lot_id.name)) or '',
                        'expire': l.prod_lot_id and l.prod_lot_id.life_date or '',
                        'avail_qty': l.theoretical_qty or 0
                    }
                    lines.append(line)

        docargs = {
            'docs': inventories,
            'lines': lines
        }

        return self.env['report'].render('l10n_mn_stock.print_stock_inventory_count', docargs)
