# -*- coding: utf-8 -*-
from datetime import datetime

from odoo import api, models
from odoo.addons.l10n_mn_report.models.report_helper import comma_me
from odoo.addons.l10n_mn_web.models.time_helper import *


class ReportStockTransitOrder(models.AbstractModel):
    _name = 'report.l10n_mn_stock.report_transit_order'

    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('l10n_mn_stock.report_transit_order')
        orders = {}
        for order in self.env['stock.transit.order'].browse(docids):
            lines = []
            sent_date, received_date = False, False # Агуулахаас гарсан/орсон огноо
            out_total_qty, in_total_qty, price_total_amount, cost_total_amount = 0, 0, 0, 0
            pickings = self.env['stock.picking'].sudo().search([('transit_order_id', '=', order.id)])

            if pickings:
                incoming, outgoing = [], []
                for p in pickings:
                    if p.picking_type_code == 'incoming':
                        received_date = p.date_done
                        outgoing.extend(self.env['stock.move'].sudo().search([('picking_id', '=', p.id)]))
                    elif p.picking_type_code == 'outgoing':
                        sent_date = p.date_done
                        incoming.extend(self.env['stock.move'].sudo().search([('picking_id', '=', p.id)]))

                for o, i in zip(outgoing, incoming):
                    price = o.product_id.list_price
                    out_total_qty += o.product_qty
                    cost_total_amount += (o.price_unit * i.product_qty)
                    in_total_qty += i.product_qty
                    price_total_amount += (price * i.product_qty)
                    lines.append({
                        'name': o.product_id.name,
                        'default_code': o.product_id.default_code,
                        'product_uom': o.product_uom.sudo().name,
                        'ordered_qty': o.product_qty,
                        'received_qty': i.product_qty,
                        'cost': o.price_unit,
                        'cost_amount': o.price_unit * i.product_qty,
                        'price': price,
                        'price_amount': price * i.product_qty
                    })
            else:
                for t in self.env['stock.transit.order.line'].search([('transit_order_id', '=', order.id)]):
                    price = t.product_id.list_price
                    out_total_qty += t.product_qty
                    cost_total_amount += (t.price_unit * t.product_qty)
                    in_total_qty += t.product_qty
                    price_total_amount += (price * t.product_qty)
                    lines.append({
                        'name': t.product_id.name,
                        'default_code': t.product_id.default_code,
                        'product_uom': t.product_uom.name,
                        'ordered_qty': t.product_qty,
                        'received_qty': t.product_qty,
                        'cost': t.price_unit,
                        'cost_amount': t.price_unit * t.product_qty,
                        'price': price,
                        'price_amount': price * t.product_qty
                    })

            orders[order.id] = {
                'ordered_date': get_day_like_display(order.date_order, self.env.user) if order and order.date_order else '',
                'receive_date': get_day_like_display(order.receive_date, self.env.user) if order and order.receive_date else '',
                'sent_date': get_day_like_display(sent_date, self.env.user) if sent_date else '',
                'received_date': get_day_like_display(received_date, self.env.user) if received_date else '',
                'out_qty': out_total_qty,
                'cost_amount': comma_me(cost_total_amount),
                'in_qty': in_total_qty,
                'price_amount': comma_me(price_total_amount),
                'lines': lines
            }

        docargs = {
            'doc_ids': docids,
            'doc_model': report.model,
            'orders': orders,
            'docs': self.env['stock.transit.order'].browse(docids),
            'user_permission': self.env.user.has_group('l10n_mn_stock.group_stock_view_cost')
        }
        return report_obj.render('l10n_mn_stock.report_transit_order', docargs)
