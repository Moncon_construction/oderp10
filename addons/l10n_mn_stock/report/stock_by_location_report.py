# -*- coding: utf-8 -*-

from odoo import tools
from odoo import models, fields, api
import time

class StockByLocationReport(models.Model):
    _name = "stock.by.location.report"
    _description = "Stock by Location Report"
    _auto = False

    location_id = fields.Many2one('stock.location', string='Location', readonly=True)
    product_id = fields.Many2one('product.product', string='Product', readonly=True)
    product_qty = fields.Float(string='Product Quantity', readonly=True)

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        context = dict(self._context or {})
        product_id = context.get('active_ids')
        
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
                SELECT t.location_id::char(8)||t.product_id::char(8) as id, t.location_id as location_id, t.product_id as product_id, t.product_tmpl_id as product_tmpl_id, sum(t.product_qty) as product_qty  
                FROM (SELECT l.id as location_id, m.product_id as product_id, p.product_tmpl_id as product_tmpl_id, sum(product_qty)::decimal(16,2) AS product_qty 
                        FROM stock_move m, stock_location l, stock_location l1, product_product p 
                        WHERE l.id=m.location_dest_id and l.usage='internal' and l1.id=m.location_id and p.id=m.product_id and l1.usage in ('internal','inventory','supplier','customer','production','transit') and m.state = 'done' group by l.id, m.product_id, p.id
                    UNION
                        SELECT l.id as location_id, m.product_id as product_id, p.product_tmpl_id as product_tmpl_id, sum(product_qty)::decimal(16,2)*(-1) AS product_qty 
                        FROM stock_move m, stock_location l, stock_location l1, product_product p  
                        WHERE l.id=m.location_id and l.usage='internal' and l1.id=m.location_dest_id and p.id=m.product_id and l1.usage in ('internal','inventory','supplier','customer','production','transit') and m.state = 'done' group by l.id, m.product_id, p.id) AS t 
                GROUP BY t.location_id, t.product_id, t.product_tmpl_id)""" % (self._table))