# -*- coding: utf-8 -*-
import time
from odoo import fields, models, api, _
from odoo.tools.translate import _
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
from io import BytesIO
import xlsxwriter
import base64


class StockTransitOrderReport(models.TransientModel):
    _name = 'stock.transit.order.report'

    start_date = fields.Date(default=fields.Date.today)
    end_date = fields.Date(default=fields.Date.today)
    transit_order_id = fields.Many2one('stock.transit.order', 'Transit Order')
    transit_order_ids = fields.Many2many('stock.transit.order','stock_transit_order_report_rel','report_id','transit_order_id','Transit Orders')
    warehouse_id = fields.Many2one('stock.warehouse', 'Warehouse')
    warehouse_ids = fields.Many2many('stock.warehouse','stock_transit_order_warehouse_report_rel2','report_id','warehouse_id','Warehouses')
    in_warehouse_id = fields.Many2one('stock.warehouse', 'In Warehouse')
    in_warehouse_ids = fields.Many2many('stock.warehouse','stock_transit_order_in_warehouse_report_rel2','report_id','in_warehouse_id','Warehouses')
    company_id = fields.Many2one('res.company', default=lambda self: self.env.user.company_id)
    is_residual = fields.Boolean('Is Residual', default=True)

    @api.onchange("company_id")
    def _get_warehouse_and_categ_domain(self):
        domain = {}
        domain['warehouse_ids'] = [('company_id', '=', self.company_id.id)]
        _warehouses = []
        for warehouse in self.env.user.allowed_warehouses:
            _warehouses.append(warehouse.id)
        if _warehouses:
            domain['warehouse_ids'] = [('id', 'in', _warehouses), ('company_id', '=', self.company_id.id)]
            domain['warehouse_ids'] = [('id', 'in', _warehouses), ('company_id', '=', self.company_id.id)]
        return {'domain': domain}

    @api.multi
    def get_export_data(self):
        date_from = self.start_date
        date_to = self.end_date
        output = BytesIO()
        
        workbook = xlsxwriter.Workbook(output)
        worksheet = workbook.add_worksheet()

        report_name = _('Transit order report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('transit_order_report'), form_title=file_name, date_to=self.end_date, date_from=self.start_date).create({})

        out_pick_list = []
        in_pick_list = []
        report_type_format = workbook.add_format({'font_size' : '14', 'align': 'center', 'text_wrap': 1,'valign': 'vcenter','font_name':'Times New Roman'})
        format = workbook.add_format({'bold': True, 'font_color': 'black', 'border': True})
        border_head = workbook.add_format(
            {'font_size' : '12','border': 1, 'align': 'center', 'text_wrap': 1,'valign': 'vcenter','font_name':'Times New Roman'})
        border_data = workbook.add_format\
            ({'font_size' : '12', 'bold': 0, 'align': 'center', 'text_wrap': 1,'valign': 'vcenter','font_name':'Times New Roman','border': True})
        border_data1 = workbook.add_format\
            ({'font_size' : '12','bold': 0, 'align': 'center', 'text_wrap': 1,'valign': 'vcenter','font_name':'Times New Roman','border': True})
        border_data2 = workbook.add_format\
            ({'font_size' : '12','bg_color': '#89cee8','bold': 0, 'align': 'center', 'text_wrap': 1,'valign': 'vcenter','font_name':'Times New Roman','border': True})
        format.set_pattern(0)  # This is optional when using a solid fill.
        format.set_bg_color('#fff')
        # create sheet
        file_name =  u'Нөхөн Дүүргэлтийн дэлгэрэнгүй тайлан'+ '.xlsx'

        worksheet.merge_range('B2:D2',u'Нөхөн Дүүргэлтийн дэлгэрэнгүй тайлан',report_type_format)
        worksheet.merge_range('B3:D3',u'Хугацаа %s - %s'%(date_from,date_to),report_type_format)
        worksheet.merge_range('A8:A9',u'№',border_head)
        worksheet.merge_range('B8:B9',u'Захиалгын дугаар',border_head)
        worksheet.merge_range('C8:C9',u'Огноо',border_head)
        worksheet.merge_range('D8:D9',u'Бараа нийлүүлэх агуулах',border_head)
        worksheet.merge_range('E8:E9',u'Бараа хүлээн авах агуулах',border_head)
        worksheet.set_row('A8:A9', 38)
        worksheet.set_row('B8:B9', 38)
        worksheet.set_row('C8:C9', 38)
        worksheet.set_row('D8:D9', 38)
        worksheet.set_row('E8:E9', 38)
        
        where = ''
        if self.transit_order_ids:
            where += ' AND st.id IN ('+','.join(str(elem['id']) for elem in self.transit_order_ids)+')'
        if self.warehouse_ids:
            where += ' AND sws.id IN ('+','.join(str(elem['id']) for elem in self.warehouse_ids)+')'
        if self.in_warehouse_ids:
            where += ' AND sw.id IN ('+','.join(str(elem['id']) for elem in self.in_warehouse_ids)+')'
            
        self.env.cr.execute("SELECT id FROM stock_picking_type WHERE code='outgoing'")
        out_pick_ids = self.env.cr.dictfetchall()
        for out_pick_id in out_pick_ids:
            out_pick_list.append(out_pick_id['id'])
        self.env.cr.execute("select id from stock_picking_type where code='incoming'")
        in_pick_ids = self.env.cr.dictfetchall()
        for in_pick_id in in_pick_ids:
            in_pick_list.append(in_pick_id['id'])
        self.env.cr.execute("SELECT sm.id AS move_id, "
                            "sm.product_id AS move_product_id, "
                            "tem.name AS prod_name, "
                            "sm.product_qty AS qty, "
                            "sm.price_unit AS price_unit, "
                            "st.name AS tr_name, "
                            "st.id AS tr_id, "
                            "st.date_order AS tr_date, "
                            "sw.name AS sw_name, "
                            "sws.name AS sws_name, "
                            "sp.picking_type_id AS pick_id, "
                            "sp.name AS sp_name, "
                            "pp.id AS p_id, "
                            "pp.barcode AS p_barcode "
                            "FROM   stock_move sm "
                               "INNER JOIN stock_picking sp "
                                       "ON sm.picking_id = sp.id "
                               "INNER JOIN stock_transit_order st "
                                       "ON sp.transit_order_id = st.id "
                               "INNER JOIN stock_warehouse sw "
                                       "ON st.warehouse_id = sw.id "
                               "INNER JOIN stock_warehouse sws "
                                       "ON st.supply_warehouse_id = sws.id "
                               "INNER JOIN product_product pp "
                                   "ON sm.product_id = pp.id "
                               "INNER JOIN product_template tem "
                                       "ON tem.id = pp.product_tmpl_id "
                        "WHERE sm.state='done' and st.date_order >=%s and st.date_order<=%s" +where+
                        "ORDER BY st.id, sm.product_id; "
                                ,(self.start_date+' 00:00:00',self.end_date+' 23:59:59'))
        result = self.env.cr.dictfetchall()

        tr_dict = []
        value = []
        result_dict = {}
        qty_dict = {}
        cost_dict = {}
        in_dict = {}
        rowx = 9
        rowx_title = 9 
        rowy = 9
        count = 1
        for rs in result:
            if rs['tr_id'] not in result_dict:
                result_dict[rs['tr_id']] = {
                                            'tr_id': rs['tr_id'],
                                            'name': rs['tr_name'],
                                            'date': rs['tr_date'],
                                            'swh': rs['sws_name'],
                                            'wh': rs['sw_name'],
                                            'p_id': rs['p_id']
                                            }
        for dict in result_dict:
            out_list = []
            in_list = []
            for rs in result:
                if rs['tr_id'] == result_dict[dict]['tr_id']:
                    if rs['tr_id'] not in cost_dict:
                        cost_dict[rs['tr_id']] = {
                                                  'cost': rs['qty']*(rs['price_unit'] or 0.0) if rs['pick_id'] in out_pick_list else (-1)*rs['qty']*(rs['price_unit'] or 0.0)
                                                  }
                    else:
                        if  rs['pick_id'] in out_pick_list:
                            cost_dict[rs['tr_id']]['cost'] += rs['qty']*(rs['price_unit'] or 0.0)
                        else:
                            if rs['pick_id'] in in_pick_list:
                                cost_dict[rs['tr_id']]['cost'] -= rs['qty']*(rs['price_unit'] or 0.0)
                    if (rs['tr_id'],rs['move_product_id']) not in qty_dict:
                        qty_dict[rs['tr_id'],rs['move_product_id']] = {'qty': rs['qty'] if rs['pick_id'] in out_pick_list else -rs['qty'],
                                                                       'name': rs['prod_name']}
                        
                    else:
                        if rs['pick_id'] in out_pick_list:
                            qty_dict[rs['tr_id'],rs['move_product_id']]['qty'] += rs['qty']
                        else:
                            if rs['pick_id'] in in_pick_list:
                                qty_dict[rs['tr_id'],rs['move_product_id']]['qty'] -= rs['qty']
                    if rs['pick_id'] in out_pick_list:
                        # Зарлагын тоо хэмжээг 1 мөрөнд гаргах
                        if rs['p_id'] not in (l['product_id'] for l in out_list):
                            out_list.append({
                                        'barcode': rs['p_barcode'],
                                        'picking_name': rs['sp_name'],
                                        'product_name': rs['prod_name'],
                                        'product_id': rs['p_id'],
                                        'qty': rs['qty'],
                                        'price_unit': (rs['price_unit'] or 0.0),
                                        'sub_total': rs['qty']*(rs['price_unit'] or 0.0)
                                        })
                        else:
                            for l in out_list:
                                l['qty'] += rs['qty'] if l['product_id'] == rs['p_id'] else 0

                    if rs['pick_id'] in in_pick_list:
                        # Орлогын тоо хэмжээг 1 мөрөнд гаргах
                        if rs['p_id'] not in (l['product_id'] for l in in_list):
                            in_list.append({
                                        'picking_name': rs['sp_name'],
                                        'product_name': rs['prod_name'],
                                        'product_id': rs['p_id'],
                                        'qty': rs['qty'],
                                        'price_unit': (rs['price_unit'] or 0.0),
                                        'sub_total': rs['qty'] * (rs['price_unit'] or 0.0)
                            })
                        else:
                            for l in in_list:
                                l['qty'] += rs['qty'] if l['product_id'] == rs['p_id'] else 0
            result_dict[dict]['out']=out_list
            result_dict[dict]['in']=in_list

        for dict in result_dict:
            if not self.is_residual or (self.is_residual and result_dict[dict]['tr_id'] in cost_dict and cost_dict[result_dict[dict]['tr_id']]['cost'] != 0):
                f1_count= 1
                f2_count= 1
                f3_count= 1
                f4_count= 1
                out_count = 1
                in_count = 1

                worksheet.write(rowx,0,str(count),border_data)
                worksheet.write(rowx,1,result_dict[dict]['name'],border_data)
                worksheet.write(rowx,2,result_dict[dict]['date'],border_data)
                worksheet.write(rowx,3,result_dict[dict]['swh'],border_data)
                worksheet.write(rowx,4,result_dict[dict]['wh'],border_data)
                worksheet.set_row(rowx, 45)
                rowx += 1
                rowy = rowx
                col = 0

                sum_out_qty = 0
                sum_out_price_unit = 0
                sum_out_sub_total = 0
                sum_in_qty = 0
                sum_in_price_unit = 0
                sum_in_sub_total = 0
                sum_qty_dict_qty = 0
                worksheet.merge_range(rowy,col,rowy,col+6,u'ГАРСАН',border_data2)
                worksheet.write(rowy+f2_count,col,u'№', border_data2)
                worksheet.write(rowy+f2_count,col+1,u'Бар код',border_data2)
                worksheet.write(rowy+f2_count,col+2,u'Бараа',border_data2)
                worksheet.write(rowy+f2_count,col+3,u'Зарлагын баримтын дугаар',border_data2)
                worksheet.write(rowy+f2_count,col+4,u'Тоо ширхэг',border_data2)
                worksheet.write(rowy+f2_count,col+5,u'Нэгж өртөг',border_data2)
                worksheet.write(rowy+f2_count,col+6,u'Нийт өртөг',border_data2)
                worksheet.set_row(rowy+f2_count, 30)
                f2_count +=1
                for out_dict in result_dict[result_dict[dict]['tr_id']]['out']:
                    worksheet.set_row(rowy+f2_count, 45)
                    worksheet.write(rowy+f2_count,col,str(count) + '/' + str(out_count),border_data1)
                    worksheet.write(rowy+f2_count,col+1,out_dict['barcode'],border_data1)
                    worksheet.write(rowy+f2_count,col+2,out_dict['product_name'],border_data1)
                    worksheet.write(rowy+f2_count,col+3,out_dict['picking_name'],border_data1)
                    worksheet.write(rowy+f2_count,col+4,out_dict['qty'],border_data1)
                    worksheet.write(rowy+f2_count,col+5,out_dict['price_unit'],border_data1)
                    worksheet.write(rowy+f2_count,col+6,out_dict['sub_total'],border_data1)
                    worksheet.write(rowy+f2_count,col+9,out_dict['price_unit'],border_data1)
                    worksheet.write(rowy+f2_count,col+12,out_dict['price_unit'],border_data1)

                    sum_out_price_unit += out_dict['price_unit']
                    sum_out_sub_total += out_dict['sub_total']
                    sum_out_qty += out_dict['qty']

                    f2_count +=1
                    out_count += 1

                worksheet.merge_range(rowy,col+7,rowy,col+10,u'ИРСЭН',border_data2)
                worksheet.write(rowy+f3_count,col+7,u'Орлогын баримтын дугаар',border_data2)
                worksheet.write(rowy+f3_count,col+8,u'Тоо ширхэг',border_data2)
                worksheet.write(rowy+f3_count,col+9,u'Нэгж өртөг',border_data2)
                worksheet.write(rowy+f3_count,col+10,u'Нийт өртөг',border_data2)

                worksheet.merge_range(rowy,col+11,rowy,col+13,u'ЗӨРҮҮ',border_data2)
                worksheet.write(rowy+f4_count,col+11,u'Тоо ширхэг',border_data2)
                worksheet.write(rowy+f4_count,col+12,u'Нэгж өртөг',border_data2)
                worksheet.write(rowy+f4_count,col+13,u'Нийт өртөг',border_data2)

                f4_count +=1
                f3_count +=1
                start_row = rowx+3
                rowxinner = rowx+3
                for in_dict in result_dict[result_dict[dict]['tr_id']]['in']:
                    worksheet.write(rowy+f3_count,col+7,in_dict['picking_name'],border_data1)
                    worksheet.write(rowy+f3_count,col+8,in_dict['qty'],border_data1)
                    # worksheet.write(rowy+f3_count,col+9,in_dict['sub_total'],border_data1)

                    sum_in_qty += in_dict['qty']
                    sum_in_price_unit += in_dict['price_unit']
                    sum_in_sub_total += in_dict['sub_total']

                    worksheet.write_formula('K%s'%rowxinner, '{=(I%s * J%s)}'%(rowxinner,rowxinner), border_data1)
                    worksheet.write_formula('L%s'%rowxinner, '{=(E%s - I%s)}'%(rowxinner,rowxinner), border_data1)
                    worksheet.write_formula('N%s'%rowxinner, '{=(L%s * M%s)}'%(rowxinner,rowxinner), border_data1)
                    f3_count +=1
                    f4_count +=1
                    in_count +=1
                    rowxinner +=1

                f4_count =1
                f4_count +=1
                for q_dict in qty_dict:
                    if result_dict[dict]['tr_id'] in q_dict:
                        # worksheet.write(rowy+f4_count,col+10,qty_dict[q_dict]['qty'],border_data1)
                        sum_qty_dict_qty += qty_dict[q_dict]['qty']
                count += 1
                rowx += max(f1_count,f2_count,f3_count,f4_count)
                last_row = rowxinner
                last_row -=1

                if out_count != in_count:
                    if out_count > in_count:
                        rowxinner +=out_count - in_count

                worksheet.write(rowx,0,u'', border_data2)
                worksheet.write(rowx,1,u'Нийт дүн',border_data2)
                worksheet.write(rowx,2,u'', border_data2)
                worksheet.write(rowx,3,u'', border_data2)
                worksheet.write(rowx,4,sum_out_qty, border_data2)
                worksheet.write(rowx,5,sum_out_price_unit, border_data2)
                worksheet.write(rowx,6,sum_out_sub_total, border_data2)
                worksheet.write(rowx,7,u'', border_data2)
                worksheet.write(rowx,8,sum_in_qty, border_data2)
                worksheet.write(rowx,9,u'', border_data2)
                worksheet.write_formula('K%s'%rowxinner, '{=SUM(K%s:K%s)}'%(start_row,last_row), border_data2)
                worksheet.write(rowx,11,sum_qty_dict_qty, border_data2)
                worksheet.write(rowx,12,u'', border_data2)
                worksheet.write_formula('N%s'%rowxinner, '{=SUM(N%s:N%s)}'%(start_row,last_row), border_data2)
                rowx += 2

        worksheet.set_column(0,0, 4)
        worksheet.set_column(1,1, 18)
        worksheet.set_column(2,2, 18)
        worksheet.set_column(3,3, 13)
        worksheet.set_column(4,4, 24)
        worksheet.set_column(5,5, 13)
        worksheet.set_column(6,6, 8)
        worksheet.set_column(7,7, 18)
        worksheet.set_column(8,8, 5)
        worksheet.set_column(9,9, 8)
        worksheet.set_column(10,10, 8)
        worksheet.set_column(11,11, 5)
        worksheet.set_column(12,12, 8)
        worksheet.set_column(13,13, 8)

        workbook.close()

        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        
        # call export function
        return report_excel_output_obj.export_report()