# -*- coding: utf-8 -*-

from datetime import datetime

from odoo import _, api, fields, models
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT


class TransitOrderLineCreation(models.TransientModel):
    _name = "transit.order.line.creation.wizard"
    _description = "Auto create Transit Order Line"

    transit_order_id = fields.Many2one('stock.transit.order', ondelete='cascade', string='Transit Order')
    supply_warehouse_id = fields.Many2one('stock.warehouse', related='transit_order_id.supply_warehouse_id', string='Supply Warehouse') 
    warehouse_id = fields.Many2one('stock.warehouse', related='transit_order_id.warehouse_id', string='Receive Warehouse')
    product_ids = fields.Many2many('product.product', string='Products')
    
    @api.onchange('supply_warehouse_id', 'warehouse_id')
    def set_domain_for_products(self):
        # Компанийн тохиргоонд АГУУЛАХАД БАРАА ТОХИРУУЛАХ чеклэгдсэн бол Нийлүүлэх/Хүргэх агуулахуудын барааны давхцлаар домайндав.
        if self.env.user.company_id.configure_allowed_product_on_wh:
            domain = {}
             
            if self.supply_warehouse_id and self.warehouse_id and self.supply_warehouse_id.allowed_product_ids and self.warehouse_id.allowed_product_ids:
                intersection = list(set(self.supply_warehouse_id.allowed_product_ids) & set(self.warehouse_id.allowed_product_ids))
                domain['product_ids'] = [('id', 'in', [pro.id for pro in intersection])]
            else:
                domain['product_ids'] = []
                
            return {'domain': domain}
        
    def add_product(self):
        # Нөхөн дүүргэлтэд бөөнөөр нь мөр нэмэх функц
        self.ensure_one() # Зөвхөн FORM харагдац дээр ашиглах

        if not self.transit_order_id or not self.product_ids or (self.product_ids and len(self.product_ids) == 0):
            return

        default_company = self.env['res.company']._company_default_get('stock.transit.order.line')
        company_id = default_company.sudo().id if default_company else self.transit_order_id.company_id.sudo().id
        date_planned = datetime.strftime(datetime.now(), DEFAULT_SERVER_DATETIME_FORMAT)
        create_date = date_planned
        create_uid = self.env.uid

        # Сонгосон бараануудаас захиалгын мөр үүсгэх
        insert_qry = "INSERT INTO stock_transit_order_line (create_date, create_uid, state, product_qty, transit_order_id, product_id, name, product_uom, date_planned, company_id) VALUES "
        for product in self.product_ids:
            product = product.sudo()
            if product == self.product_ids[0]:
                insert_qry += "('%s', %s, 'draft', 1, %s, %s, '%s', %s, '%s', %s)" % (create_date, create_uid, self.transit_order_id.id, product.id, product.name, product.uom_id.id, date_planned, company_id)
            else:
                insert_qry += ", ('%s', %s, 'draft', 1, %s, %s, '%s', %s, '%s', %s)" % (create_date, create_uid, self.transit_order_id.id, product.id, product.name, product.uom_id.id, date_planned, company_id)
        self._cr.execute(insert_qry)
