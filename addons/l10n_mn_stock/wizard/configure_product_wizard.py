# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import AccessError, ValidationError


class ConfigureWh(models.TransientModel):
    _name = 'configure.wh.on.product.wizard'
    
    def _domain_warehouses(self):
        return [('id', 'in', self.env.user.allowed_warehouses.ids or []), '|', ('company_id', '=', False), ('company_id', 'child_of', self.env.user.company_id.id)]
        
    product_ids = fields.Many2many('product.product', 'configure_wh_wizard_to_product', 'wizard_id', 'product_id', ondelete='cascade', required=True, string='Product')
    warehouse_ids = fields.Many2many('stock.warehouse', 'configure_wh_wizard_to_wh', 'wizard_id', 'wh_id', ondelete='cascade', required=True, domain=_domain_warehouses, string='Warehouse')
    
    @api.multi
    def configure(self):
        # Олон бараан дээр олон агуулах зэрэг тохируулах боломжтой болгов
        if not self.env.user.company_id.configure_allowed_product_on_wh:
            raise ValidationError(_("It only available when the 'Configure Allowed Product on Warehouse' configuration is checked !!!"))
        if not self.env.user.has_group("l10n_mn_product.group_product_creator"):
            raise AccessError(_("It only available for the 'Product Creator' group employees !!!"))
        
        qry = "INSERT INTO wh_products_rel (product_id, wh_id) VALUES "
        first = True
        for obj in self:
            for product_id in obj.product_ids:
                configures_whs = product_id.allowed_warehouse_ids.mapped('id')
                nonconfigured_whs = list(set(obj.warehouse_ids.mapped('id')) - set(configures_whs or []))
                for nonconfigured_wh in nonconfigured_whs:
                    if not first:
                        qry += ", "
                    qry += "(%s, %s)" % (product_id.id, nonconfigured_wh)
                    first = False
        if not first:
            self._cr.execute(qry)
