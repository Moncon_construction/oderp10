# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.tools.translate import _
from odoo.tools import float_is_zero
from odoo.exceptions import UserError
import base64
from io import BytesIO
import time
import xlsxwriter
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
import sys
from datetime import datetime, timedelta

class StockInventoryPrint(models.TransientModel):
    _name = 'stock.inventory.print'
     
    stock_inventories = fields.Many2many('stock.inventory', 'print_inventory_wizard_to_inventory', 'wizard_id', 'inventory_id', string='Inventory', required=True)
    print_type = fields.Selection([
                        ('like_import', 'Like Import'),
                        ('like_inventory_by_hand', 'Like Inventory By Hand'),
                        ], string='Print Type', default='like_inventory_by_hand', required=True)
     
    def get_xsl_column_name(self, index):
        alphabet = {'0': 'A',  '1':'B',  '2':'C',  '3':'D',  '4':'E',
                    '5': 'F',  '6':'G',  '7':'H',  '8':'I',  '9':'J',
                    '10':'K',  '11':'L', '12':'M', '13':'N', '14':'O',
                    '15':'P',  '16':'Q', '17':'R', '18':'S', '19':'T',
                    '20':'U',  '21':'V', '22':'W', '23':'X', '24':'Y', '25':'Z'}
        
        if index <= 25:
            return (alphabet[str(index)] + ":" + alphabet[str(index)])
        else:
            return (alphabet[str(index/26-1)] + alphabet[str(index%26)] + ":" + alphabet[str(index/26-1)] + alphabet[str(index%26)])
        
    @api.multi
    def print_inventory(self):
        result = False
    
        if self.print_type == 'like_inventory_by_hand':
            data = {
                'ids': self.stock_inventories.ids,
                'model': 'stock.inventory',
            }
            result = self.env['report'].get_action(self.stock_inventories.ids, 'l10n_mn_stock.print_stock_inventory_count', data=data)
            return result
        else:
            return self.export_report()
    
    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        
        # create name
        report_name = _('Inventory Template like Import')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('inventory_template_like_import'), form_title=file_name).create({})
        
        # create formats
        format_text_left = {
            'font_name': 'Times New Roman',
            'font_size': 9,
            'align': 'left',
            'valign': 'top',
            'text_wrap': 1,
            'border': 1
        }
        
        format_title_center = format_text_left.copy()
        format_title_center['align'] = 'center'
        format_title_center['font_size'] = '12'
        format_title_center['bold'] = True
        format_title_center['bg_color'] = '#99ccff'
        
        format_number_right = format_text_left.copy()
        format_number_right['num_format'] = '0.00'
        format_number_right['align'] = 'right'
        
        format_text_left = book.add_format(format_text_left)
        format_title_center = book.add_format(format_title_center)
        format_title_center.set_align('vcenter')
        format_number_right = book.add_format(format_number_right)
        
        if self.stock_inventories and len(self.stock_inventories) > 0:
            unique_count = 1
            for inventory in self.stock_inventories:
                # create sheet
                name = str(unique_count) + ". "
                if inventory.accounting_date:
                    name += "/" + datetime.strptime(inventory.accounting_date, '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d') + "/ " + inventory.name
                else:
                    name += "/" + datetime.strptime(inventory.date, '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d') + "/ " + inventory.name
                sheet = book.add_worksheet(name)
                unique_count += 1
                
                sheet.set_landscape()
                sheet.set_paper(9)  # A4
                sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
                sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
                
                # compute column
                col = 0
                sheet.set_column(self.get_xsl_column_name(col), 8)
                sheet.set_column(self.get_xsl_column_name(col + 1), 30)
                sheet.set_column(self.get_xsl_column_name(col + 2), 12)
                sheet.set_column(self.get_xsl_column_name(col + 3), 5)
                sheet.set_column(self.get_xsl_column_name(col + 4), 8)
                sheet.set_column(self.get_xsl_column_name(col + 5), 8)
                sheet.set_column(self.get_xsl_column_name(col + 6), 12)
                sheet.set_column(self.get_xsl_column_name(col + 7), 10)
                sheet.set_column(self.get_xsl_column_name(col + 8), 10)
                sheet.set_row(0, 45)
                
                rowx = 0
                col = 0
                sheet.write(rowx, col, _(u'Location BarCode'), format_title_center) 
                sheet.write(rowx, col + 1, _(u'Product Name'), format_title_center) 
                sheet.write(rowx, col + 2, _(u'Default code'), format_title_center) 
                sheet.write(rowx, col + 3, _(u'Product Qty'), format_title_center) 
                sheet.write(rowx, col + 4, _(u'Cost'), format_title_center) 
                sheet.write(rowx, col + 5, _(u'List Price'), format_title_center) 
                sheet.write(rowx, col + 6, _(u'Barcode'), format_title_center) 
                sheet.write(rowx, col + 7, _(u'Lot/Serial Number'), format_title_center) 
                sheet.write(rowx, col + 8, _(u'Serial End Date /Text/'), format_title_center)
                
                rowx += 1
                location_barcode = inventory.location_id.barcode
                for line in inventory.line_ids:
                    col = 0
                    removal_date = line.prod_lot_id.removal_date if line.prod_lot_id and line.prod_lot_id.removal_date else False
                    if removal_date:
                        removal_date = datetime.strptime(removal_date, '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d')
                    sheet.write(rowx, col, location_barcode or '', format_text_left) 
                    sheet.write(rowx, col + 1, line.product_name or '', format_text_left) 
                    sheet.write(rowx, col + 2, line.product_id.default_code or '', format_text_left) 
                    sheet.write(rowx, col + 3, line.product_qty or 0, format_number_right) 
                    sheet.write(rowx, col + 4, line.standard_price or 0, format_number_right) 
                    sheet.write(rowx, col + 5, line.lst_price or 0, format_number_right) 
                    sheet.write(rowx, col + 6, line.product_code or '', format_text_left) 
                    sheet.write(rowx, col + 7, line.prodlot_name or '', format_text_left) 
                    sheet.write(rowx, col + 8, removal_date or '', format_text_left)
                    rowx += 1
            
        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report() 
    
    class StockInventory(models.Model):
        _inherit = 'stock.inventory'
        
        @api.multi
        def print_inventory_count(self):
            print_wizard = self.env['stock.inventory.print'].create({
                'print_type': 'like_inventory_by_hand'
            })
             
            values = ""
            for id in self.ids:
                values += "(%s, %s), " %(print_wizard.id, id)
            values = values[:-2] if values[-2:] == ", " else values
                 
            self._cr.execute("""
                INSERT INTO print_inventory_wizard_to_inventory (wizard_id, inventory_id) 
                VALUES %s
            """ % values)
             
            return {
                'type': 'ir.actions.act_window',
                'name': _('Print Inventory count'),
                'res_model': 'stock.inventory.print',
                'view_mode': 'form',
                'target': 'new',
                'res_id': print_wizard.id,
            }