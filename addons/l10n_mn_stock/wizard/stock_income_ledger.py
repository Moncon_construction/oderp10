# -*- coding: utf-8 -*-
import time
import base64
import xlsxwriter
from io import BytesIO
from odoo import api, models, fields, _
from openerp.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from operator import itemgetter
from odoo.exceptions import UserError
from xlsxwriter.utility import xl_rowcol_to_cell


class PrintStockIncomeLedger(models.TransientModel):
    _inherit = 'oderp.report.html.output'
    _name = "stock.income.ledger.wizard"
    _description = "Print Income Ledger Report"

    def _default_pos_install(self):
        '''
            Пос борлуулалт модуль суусан эсэхийг шалгана.
        '''
        pos_exists = 'pos.order' in self.env
        return pos_exists

    def _default_mrp_install(self):
        '''
            Үйлдвэрлэлийн модуль суусан эсэхийг шалгана.
        '''
        mrp_exists = 'mrp.production' in self.env
        return mrp_exists

    def _default_swap_install(self):
        '''
            Бараа солилцооны модуль суусан эсэхийг шалгана.
        '''
        swap_exists = 'swap.order' in self.env
        return swap_exists

    company_id = fields.Many2one('res.company', 'Company', readonly=True, default=lambda self: self.env['res.company']._company_default_get('stock.inventory'))
    warehouse_ids = fields.Many2many('stock.warehouse', 'report_income_ledger_warehouse_rel',
                            'wizard_id', 'warehouse_id', 'Warehouse')
    product_ids = fields.Many2many('product.product', 'report_income_ledger_product_rel',
                            'wizard_id', 'product_id', 'Product')
    partner_ids = fields.Many2many('res.partner', 'report_income_ledger_partner_rel',
                            'wizard_id', 'partner_id', 'Partner')
    category_ids = fields.Many2many('product.category', 'report_income_ledger_category_rel',
                            'wizard_id', 'category_id', 'Category')
    date_to = fields.Date('To Date', required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    date_from = fields.Date('From Date', required=True, default=lambda *a: time.strftime('%Y-%m-01'))

    purchase = fields.Boolean('Purchase', default=True)
    refund = fields.Boolean('Sales refund', default=True)
    procure = fields.Boolean('Replenishment', default=True)
    pos = fields.Boolean('Pos Refund', default=True)
    inventory = fields.Boolean('Inventory', default=True)
    mrp = fields.Boolean('MRP Production', default=True)
    swap = fields.Boolean('Swap', default=True)
    cost = fields.Boolean('Show Cost Amount?', default=False)
    pos_install = fields.Boolean('Pos Install', default=_default_pos_install)
    mrp_install = fields.Boolean('MRP Install', default=_default_mrp_install)
    swap_install = fields.Boolean('Swap Order Install', default=_default_swap_install)
    all_check = fields.Boolean('Select All', default=True)

    type = fields.Selection([('detail','Detail'),('summary','Summary')], 'Type', required=True, default='summary')

    @api.onchange('date_from')
    def _get_warehouse_and_categ_domain(self):
        domain = {}
        domain['warehouse_ids'] = [('company_id', '=', self.company_id.id)]
        _warehouses = []
        for warehouse in self.env.user.allowed_warehouses:
            _warehouses.append(warehouse.id)
        if _warehouses:
            domain['warehouse_ids'] = [('id', 'in', _warehouses)]
        return {'domain': domain}

    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        report_name = _('Report Income Ledger')
        file_name = "%s %s.xls" % (report_name, time.strftime('%Y-%m-%d %H:%M'))

        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_bold_float = book.add_format(ReportExcelCellStyles.format_content_bold_float)
        format_content_left = book.add_format(ReportExcelCellStyles.format_content_left)
        format_content_bold_text = book.add_format(ReportExcelCellStyles.format_content_bold_text)
        format_content_date = book.add_format(ReportExcelCellStyles.format_content_date)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_group_left = book.add_format(ReportExcelCellStyles.format_group_left)
        format_content_bold_right = book.add_format(ReportExcelCellStyles.format_content_bold_right)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_group_right = book.add_format(ReportExcelCellStyles.format_group_right)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)

        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('Report Income Ledger'), form_title=file_name).create({})
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        sheet.set_column('A:A', 6)# Д/д
        sheet.set_column('B:B', 12) # Огноо
        sheet.set_column('C:C', 18) # Баримтын дугаар
        sheet.set_column('D:D', 18) # Эх баримт
        if self.type == 'detail' and self.env.user.company_id.show_default_code_on_report:
            sheet.set_column('E:E', 15)  # Дотоод код
            sheet.set_column('F:F', 20)  # Харилцагчийн нэр
            sheet.set_column('G:G', 25)  # Хөдөлгөөн / Барааны нэр
            sheet.set_column('H:H', 12)  # Тоо хэмжээ
            sheet.set_column('I:I', 12)  # Нийт дүн
            sheet.set_column('J:J', 12)  # НӨТ
            sheet.set_column('K:K', 15)  # НӨТ-гүй дүн
            if self.cost:
                sheet.set_column('L:L', 15)  # Өртөг дүн
        else:
            sheet.set_column('E:E', 20)  # Харилцагчийн нэр
            sheet.set_column('F:F', 25)  # Хөдөлгөөн
            sheet.set_column('G:G', 12)  # Тоо хэмжээ
            sheet.set_column('H:H', 12)  # Нийт дүн
            sheet.set_column('I:I', 9)  # НӨТ
            sheet.set_column('J:J', 15)  # НӨТ-гүй дүн
            if self.cost:
                sheet.set_column('K:K', 15)  # Өртөг дүн

        rowx = rowd = col = 0
        total_quantity = total_amount = total_tax = total_taxed = total_cost = 0
        rep_type_total_rows = []
        if self.type == 'detail':
            if self.env.user.company_id.show_default_code_on_report:
                total_col = 11 if self.cost else 10
                sub_col = 7
            else:
                total_col = 10 if self.cost else 9
                sub_col = 6
        else:
            total_col = 10 if self.cost else 9
            sub_col = 6

        # Толгой хэсэг зурах
        sheet.set_row(rowx, 15)
        sheet.merge_range(rowx, 1, rowx, total_col, '%s: %s' % (_('Company name'), self.env.user.company_id.name), format_filter)
        rowx += 1
        sheet.set_row(rowx, 25)
        sheet.merge_range(rowx, 1, rowx, total_col, report_name, format_name)
        rowx += 1
        sheet.set_row(rowx, 15)
        sheet.merge_range(rowx, 1, rowx, total_col, '%s: from %s to %s' % (_('Duration'), self.date_from, self.date_to), format_filter)
        rowx += 1
        if not self.warehouse_ids:
            self.warehouse_ids = self.env.user.allowed_warehouses
        location_obj = self.env['stock.location']
        TAX_FACTOR = self.env['ir.config_parameter'].get_param('report.tax') or 0
        wnames = ''
        location_ids = []
        for whouse in self.warehouse_ids:
            wnames += whouse.name
            wnames += ', '
            location_ids.extend(location_obj.search([('location_id', 'child_of', [whouse.view_location_id.id]), ('usage', '=', 'internal')]).ids)
        sheet.set_row(rowx, 20)
        sheet.merge_range(rowx, 1, rowx, total_col, '%s: %s' % (_('Warehouse'), wnames), format_filter)
        rowx += 1
        sheet.set_row(rowx, 20)
        sheet.merge_range(rowx, 1, rowx, total_col, '%s: %s' % (_('Date'), time.strftime('%Y-%m-%d')), format_filter)
        rowx += 3
        col = 0
        sheet.set_row(rowx, 30)
        sheet.write(rowx, col, _('№'), format_title)
        sheet.write(rowx, col + 1, _('Date'), format_title)
        sheet.write(rowx, col + 2, _('Document ref.'), format_title)
        sheet.write(rowx, col + 3, _('Origin'), format_title)
        if self.type == 'detail' and self.env.user.company_id.show_default_code_on_report:
            sheet.write(rowx, col + 4, _('default code'), format_title)
            col += 1
        sheet.write(rowx, col + 4, _('Partner name'), format_title)
        sheet.write(rowx, col + 5, _('Move Income / Product Name') if self.type == 'detail' else _('Move Income'), format_title)
        sheet.write(rowx, col + 6, _('Quantity'), format_title)
        sheet.write(rowx, col + 7, _('Total amount'), format_title)
        sheet.write(rowx, col + 8, _('VAT'), format_title)
        sheet.write(rowx, col + 9, _('Not VAT'), format_title)
        if self.cost:
            sheet.write(rowx, col + 10, _('Total cost'), format_title)
        rowx += 1

        # Хүснэгт дата бэлдэх
        total_dict = {'total': 0,
                      'qty': 0,
                      'taxed': 0,
                      'tax': 0,
                      'cost': 0}
        data = []
        pick_ids = []
        res = {}
        where = ''
        join = ''
        select_type = ''
        if not location_ids:
            raise UserError(_("Stock locations with internal usage not found! First configure internal stock location on warehouse!"))
        location_tuple = tuple(location_ids)
        any_check = False
        total_dict = {'total': 0,
                      'qty': 0,
                      'taxed': 0,
                      'tax': 0,
                      'cost': 0}
        select = ' (select 0) AS tax, '
        select_name = ' p.name AS name, '
        select_origin = ' p.origin AS origin, '
        group_by = ''
        if self.product_ids:
            where += ' AND m.product_id IN (' + ','.join(map(str, self.product_ids.ids)) + ') '
        if self.partner_ids:
            where += ' AND p.partner_id IN (' + ','.join(map(str, self.partner_ids.ids)) + ') '
        if self.category_ids:
            categ_ids = self.env['product.category'].search([('parent_id', 'child_of', self.category_ids.ids)])
            where += ' AND pt.categ_id IN (' + ','.join(map(str, categ_ids.ids)) + ') '
        if self.mrp and self.mrp_install:
            any_check = True
            join += ' LEFT JOIN mrp_production AS mrp ON (m.production_id = mrp.id) '
            select_type += " WHEN m.production_id is not null THEN 'mrp' "
            select_name = '(CASE WHEN m.picking_id is not null THEN p.name \
                            WHEN m.production_id is not null THEN mrp.name END) AS name,'
            group_by += 'm.production_id,mrp.name,'
        if self.purchase:
            any_check = True
            join += ' LEFT JOIN purchase_order_line AS pol ON (m.purchase_line_id = pol.id) \
                        LEFT JOIN account_tax_purchase_order_line_rel AS taxes ON (pol.id = taxes.purchase_order_line_id) '
            select += ' SUM(taxes.account_tax_id) AS tax, pol.price_unit as unit_price, '
            group_by += 'pol.price_unit, '
        if self.refund:
            any_check = True
        if self.pos and self.pos_install:
            any_check = True
            select_type += "WHEN (SELECT pos_order.picking_id FROM stock_move  LEFT JOIN pos_order  ON (stock_move.picking_id = pos_order.picking_id) where stock_move.id = m.origin_returned_move_id) is not null THEN 'pos' "
        if self.procure:
            any_check = True
        if self.inventory:
            any_check = True
            join += ' LEFT JOIN stock_inventory AS i ON (m.inventory_id = i.id) '
            if self.mrp and self.mrp_install:
                select_name = '(CASE WHEN m.picking_id is not null THEN p.name \
                                WHEN m.production_id is not null THEN mrp.name \
                                WHEN m.inventory_id is not null THEN i.name END) AS name,'
                select_origin = '(CASE WHEN m.picking_id is not null THEN p.origin \
                                    WHEN m.production_id is not null THEN mrp.name \
                                    WHEN m.inventory_id is not null THEN i.name END) AS origin, '
            else:
                select_name = '(CASE WHEN m.picking_id is not null THEN p.name \
                                WHEN m.inventory_id is not null THEN i.name END) AS name,'
                select_origin = '(CASE WHEN m.picking_id is not null THEN p.origin \
                                    WHEN m.inventory_id is not null THEN i.name END) AS origin, '
            group_by += 'i.name,'
            """
            Бараа солилцоон дээр татвар сонгодоггүй тул ашиглагдахгүй
            """
        if self.swap and self.swap_install:
            any_check = True
            join += ' LEFT JOIN swap_order_line AS swl ON (m.swap_order_line_id = swl.id) \
                      LEFT JOIN swap_order AS swo ON (swl.order_in_id = swo.id) '
            #                       LEFT JOIN swap_order_in_tax_rel AS stax ON (swo.id = stax.ord_id) '
            #             select += ' SUM(stax.tax_id) AS stax, '
            select_type += "WHEN m.swap_order_line_id is not null THEN 'swap' "
            group_by += 'm.swap_order_line_id,'
        if any_check:
            self.env.cr.execute("SELECT * FROM (SELECT cast(m.date as date) AS date, m.id AS move_id, m.origin_returned_move_id AS return_id, "
                            "(CASE WHEN m.picking_id is not null THEN rp.name WHEN m.warehouse_id is not null "
                                "AND sw.partner_id is not null THEN rp2.name ELSE '' END) AS partner, "
                            "sw1.name AS warehouse_name ,"
                            "sw2.name AS to_warehouse_name ,"
                            "(CASE WHEN m.purchase_line_id is not null THEN 'purchase' "
                                "WHEN m.inventory_id is not null THEN 'inventory' "
                                "WHEN m.picking_id is not null and p.transit_order_id is not null THEN 'procure' "+select_type+" "
                                "WHEN m.origin_returned_move_id is not null THEN 'refund' ELSE 'other_refund' END) AS rep_type, "+select_name+" "
                            "m.product_id AS prod_id, pt.name AS prod_name, pp.default_code AS default_code, "+select_origin+" "
                            "SUM(coalesce((m.product_qty / u.factor * u2.factor),0)) AS qty, "+select+" "
                            "SUM(coalesce((m.price_unit * m.product_qty / u.factor * u2.factor),0)) AS cost, "
                            "SUM(coalesce((m.price_unit * m.product_qty / u.factor * u2.factor),0)) AS amount "
                        "FROM stock_move AS m "
                            "LEFT JOIN stock_picking AS p ON (m.picking_id = p.id) "
                            "LEFT JOIN stock_transit_order sto on sto.id = p.transit_order_id "    
                            "LEFT JOIN stock_warehouse sw1 on sw1.id = sto.supply_warehouse_id "
                            "LEFT JOIN stock_warehouse sw2 on sw2.id = sto.warehouse_id "
                            "LEFT JOIN res_partner AS rp ON (p.partner_id = rp.id) "
                            "JOIN product_product AS pp ON (m.product_id = pp.id) "
                            "JOIN product_template AS pt ON (pp.product_tmpl_id = pt.id) "
                            "JOIN product_uom AS u ON (m.product_uom = u.id) "
                            "JOIN product_uom AS u2 ON (pt.uom_id = u2.id) "
                            "LEFT JOIN stock_warehouse AS sw ON (m.warehouse_id = sw.id) "
                            "LEFT JOIN res_partner AS rp2 ON (sw.partner_id = rp2.id) "
                            "LEFT JOIN procurement_order AS po ON (m.procurement_id = po.id) "+join+""
                        "WHERE m.location_id NOT IN %s AND m.location_dest_id IN %s "
                            "AND m.state = 'done' "
                            "AND m.date >= %s AND m.date <= %s "+where+" "
                        "GROUP BY m.id,m.date,m.origin_returned_move_id,m.picking_id,rp.name,m.warehouse_id,sw.partner_id,m.purchase_line_id, "+group_by+" "
                            "rp2.name,m.inventory_id,p.name,m.product_id, pt.name, pp.default_code, p.origin,p.transit_order_id, sw.name, sw1.name, sw2.name "
                        "ORDER BY m.date) AS m ORDER BY m.rep_type, m.date, m.origin "
                            ,(location_tuple,location_tuple,self.date_from,self.date_to+' 23:59:59'))
            result = self.env.cr.dictfetchall()
            if result:
                if self.type == 'detail':
                    rep_type = False
                    origin = False
                    sub_seq = 1
                    for r in result:
                        if r['rep_type'] == 'mrp' and ((not self.mrp_install) or (self.mrp_install and not self.mrp)):
                            continue
                        if r['rep_type'] == 'pos' and ((not self.pos_install) or (self.pos_install and not self.pos)):
                            continue
                        if r['rep_type'] == 'procure' and not self.procure:
                            continue
                        if r['rep_type'] == 'refund' and not self.refund:
                            continue
                        if r['rep_type'] == 'inventory' and not self.inventory:
                            continue
                        if r['rep_type'] == 'swap' and (
                                (not self.swap_install) or (self.swap_install and not self.swap)):
                            continue
                        if r['rep_type'] == 'purchase' and not self.purchase:
                            continue

                        TAX_FACTOR = tax = taxed = 0
                        if r['warehouse_name'] and r['to_warehouse_name']:
                            move_name = r['warehouse_name'] + '-->' + r['to_warehouse_name']
                        else:
                            move_name = ''
                        # Эхний төрөл болон хөдөлгөөнийг зурна
                        if not rep_type and not origin:
                            name = self.get_type_name(r['rep_type'])
                            sheet.merge_range(rowx, 0, rowx, total_col, name, format_group_left)
                            rowx += 1
                            col = 0
                            sheet.write(rowx, col, sub_seq, format_content_text)
                            col += 1
                            sheet.write(rowx, col, r['date'], format_content_date)
                            col += 1
                            sheet.write(rowx, col, r['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, r['origin'], format_content_text)
                            col += 1
                            if self.env.user.company_id.show_default_code_on_report:
                                sheet.write(rowx, col, '', format_content_text)
                                col += 1
                            sheet.write(rowx, col, r['partner'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, move_name, format_content_text)
                            col += 1
                            sheet.write(rowx, col, '', format_content_text)
                            col += 1
                            sheet.write(rowx, col, '', format_content_text)
                            col += 1
                            sheet.write(rowx, col, '', format_content_text)
                            col += 1
                            sheet.write(rowx, col, '', format_content_text)
                            col += 1
                            if self.cost:
                                sheet.write(rowx, col, '', format_content_text)
                            rowx += 1
                            rowd = rowx
                        # Дараагийн төрөл болон хөдөлгөөнийг зурна
                        elif rep_type and rep_type != r['rep_type']:
                            sub_seq = 1
                            for h in range(sub_col, total_col+1):
                                sheet.write_formula(rowx, h,
                                                    '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(
                                                        rowx - 1, h) + ')}', format_content_bold_float)
                            rep_type_total_rows.append(rowx)
                            rowx += 1
                            sheet.merge_range(rowx, 0, rowx, sub_col-1, _('Sub Total Move'),
                                              format_content_bold_right)
                            for h in range(sub_col, total_col+1):
                                sum_rows = []
                                for t in rep_type_total_rows:
                                    sum_rows.append('' + xl_rowcol_to_cell(t, h) + '')
                                sheet.write_formula(rowx, h,
                                                    '{=SUM(' + ','.join(map(str, sum_rows)) + ')}', format_content_bold_float)
                            rowx += 1
                            name = self.get_type_name(r['rep_type'])
                            sheet.merge_range(rowx, 0, rowx, total_col, name, format_group_left)
                            rep_type_total_rows = []

                            rowx += 1
                            col = 0
                            sheet.write(rowx, col, sub_seq, format_content_text)
                            col += 1
                            sheet.write(rowx, col, r['date'], format_content_date)
                            col += 1
                            sheet.write(rowx, col, r['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, r['origin'], format_content_text)
                            col += 1
                            if self.env.user.company_id.show_default_code_on_report:
                                sheet.write(rowx, col, '', format_content_text)
                                col += 1
                            sheet.write(rowx, col, r['partner'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, move_name, format_content_text)
                            col += 1
                            sheet.write(rowx, col, '', format_content_text)
                            col += 1
                            sheet.write(rowx, col, '', format_content_text)
                            col += 1
                            sheet.write(rowx, col, '', format_content_text)
                            col += 1
                            sheet.write(rowx, col, '', format_content_text)
                            col += 1
                            if self.cost:
                                sheet.write(rowx, col, '', format_content_text)
                            rowx += 1
                            rowd = rowx
                        # Дараагийн хөдөлгөөнийг зурна
                        elif origin and origin != r['origin']:
                            for h in range(sub_col, total_col+1):
                                sheet.write_formula(rowx, h,
                                                    '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(
                                                        rowx - 1, h) + ')}', format_content_bold_float)
                            rep_type_total_rows.append(rowx)
                            rowx += 1
                            sub_seq += 1
                            col = 0
                            sheet.write(rowx, col, sub_seq, format_content_text)
                            col += 1
                            sheet.write(rowx, col, r['date'], format_content_date)
                            col += 1
                            sheet.write(rowx, col, r['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, r['origin'], format_content_text)
                            col += 1
                            if self.env.user.company_id.show_default_code_on_report:
                                sheet.write(rowx, col, '', format_content_text)
                                col += 1
                            sheet.write(rowx, col, r['partner'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, move_name, format_content_text)
                            col += 1
                            sheet.write(rowx, col, '', format_content_text)
                            col += 1
                            sheet.write(rowx, col, '', format_content_text)
                            col += 1
                            sheet.write(rowx, col, '', format_content_text)
                            col += 1
                            sheet.write(rowx, col, '', format_content_text)
                            col += 1
                            if self.cost:
                                sheet.write(rowx, col, '', format_content_text)
                            rowd = rowx
                            rowx += 1

                        tax_id = False
                        taxed = round(r['amount'], 4)
                        if r['tax']:
                            tax_id = self.env['account.tax'].browse(r['tax'])
                            if tax_id.price_include and (r['qty'] * r['unit_price']):
                                TAX_FACTOR = round(1 - ((r['qty'] * r['unit_price'] - r['amount']) * 100 / (
                                            r['qty'] * r['unit_price'])), 4)
                            else:
                                TAX_FACTOR = round(tax_id.amount / 100, 4)
                        if r['rep_type'] in ('purchase', 'swap', 'pos'):
                            if r['rep_type'] == 'pos':
                                if tax_id and tax_id.price_include:
                                    tax = round(r['qty'] * r['unit_price'] - r['amount'], 4)
                                    r['amount'] = round(r['qty'] * r['unit_price'], 4)
                                else:
                                    tax = round(float(r['amount']) * float(float(TAX_FACTOR)), 4)
                                    r['amount'] = taxed + tax
                            elif r['rep_type'] == 'swap':
                                if r['stax'] > 0:
                                    if tax_id and tax_id.price_include:
                                        tax = round(r['qty'] * r['unit_price'] - r['amount'], 4)
                                        r['amount'] = round(r['qty'] * r['unit_price'], 4)
                                    else:
                                        tax = round(float(r['amount']) * float(float(TAX_FACTOR)), 4)
                                        r['amount'] = taxed + tax
                            else:
                                if r['tax'] > 0:
                                    if tax_id and tax_id.price_include:
                                        tax = round(r['qty'] * r['unit_price'] - r['amount'], 4)
                                        r['amount'] = round(r['qty'] * r['unit_price'], 4)
                                    else:
                                        tax = round(float(r['amount']) * float(float(TAX_FACTOR)), 4)
                                        r['amount'] = taxed + tax

                        total_quantity += r['qty']
                        total_amount += r['amount']
                        total_tax += tax
                        total_taxed += taxed
                        if self.cost:
                            total_cost += r['cost']

                        # Барааны мөрийг зурна
                        col = 0
                        sheet.write(rowx, col, '', format_content_left)
                        col += 1
                        sheet.write(rowx, col, '', format_content_left)
                        col += 1
                        sheet.write(rowx, col, '', format_content_left)
                        col += 1
                        sheet.write(rowx, col, '', format_content_left)
                        col += 1
                        if self.env.user.company_id.show_default_code_on_report:
                            sheet.write(rowx, col, r['default_code'], format_content_left)
                            col += 1
                        sheet.write(rowx, col, '', format_content_left)
                        col += 1
                        sheet.write(rowx, col, r['prod_name'], format_content_left)
                        col += 1
                        sheet.write(rowx, col, r['qty'], format_content_float)
                        col += 1
                        sheet.write(rowx, col, r['amount'], format_content_float)
                        col += 1
                        sheet.write(rowx, col, tax, format_content_float)
                        col += 1
                        sheet.write(rowx, col, taxed, format_content_float)
                        col += 1
                        if self.cost:
                            sheet.write(rowx, col, r['cost'], format_content_float)
                            col += 1
                        rowx += 1
                        rep_type = r['rep_type']
                        origin = r['origin']
                    for h in range(sub_col, total_col+1):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_content_bold_float)
                    rep_type_total_rows.append(rowx)
                    rowx += 1
                    sheet.merge_range(rowx, 0, rowx, sub_col - 1, _('Sub Total Move'),
                                      format_content_bold_right)
                    for h in range(sub_col, total_col + 1):
                        sum_rows = []
                        for t in rep_type_total_rows:
                            sum_rows.append('' + xl_rowcol_to_cell(t, h) + '')
                        sheet.write_formula(rowx, h,
                                            '{=SUM(' + ','.join(map(str, sum_rows)) + ')}', format_content_bold_float)
                    rowx += 1
                    sheet.merge_range(rowx, 0, rowx, sub_col - 1, _('Total'),
                                      format_group_right)
                    sheet.write(rowx, sub_col, total_quantity, format_group_float)
                    sheet.write(rowx, sub_col+1, total_amount, format_group_float)
                    sheet.write(rowx, sub_col+2, total_tax, format_group_float)
                    sheet.write(rowx, sub_col+3, total_taxed, format_group_float)
                    if self.cost:
                        sheet.write(rowx, sub_col+4, total_cost, format_group_float)
                else:
                    rep_type = False
                    origin = False
                    sub_seq = 1
                    sum_quantity = sum_amount = sum_tax = sum_taxed = sum_cost = 0
                    for r in result:
                        if r['rep_type'] == 'mrp' and ((not self.mrp_install) or (self.mrp_install and not self.mrp)):
                            continue
                        if r['rep_type'] == 'pos' and ((not self.pos_install) or (self.pos_install and not self.pos)):
                            continue
                        if r['rep_type'] == 'procure' and not self.procure:
                            continue
                        if r['rep_type'] == 'refund' and not self.refund:
                            continue
                        if r['rep_type'] == 'inventory' and not self.inventory:
                            continue
                        if r['rep_type'] == 'swap' and (
                                (not self.swap_install) or (self.swap_install and not self.swap)):
                            continue
                        if r['rep_type'] == 'purchase' and not self.purchase:
                            continue

                        TAX_FACTOR = tax = taxed = 0
                        if r['warehouse_name'] and r['to_warehouse_name']:
                            move_name = r['warehouse_name'] + '-->' + r['to_warehouse_name']
                        else:
                            move_name = ''
                        if not rep_type and not origin:
                            name = self.get_type_name(r['rep_type'])
                            sheet.merge_range(rowx, 0, rowx, total_col, name, format_group_left)
                            rowx += 1
                            col = 0
                            sheet.write(rowx, col, sub_seq, format_content_text)
                            col += 1
                            sheet.write(rowx, col, r['date'], format_content_date)
                            col += 1
                            sheet.write(rowx, col, r['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, r['origin'], format_content_text)
                            col += 1
                            if self.env.user.company_id.show_default_code_on_report:
                                sheet.write(rowx, col, '', format_content_text)
                                col += 1
                            sheet.write(rowx, col, r['partner'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, move_name, format_content_text)
                            col += 1
                            rowx += 1
                        # Дараагийн төрөл болон хөдөлгөөнийг зурна
                        elif rep_type and rep_type != r['rep_type']:
                            sub_seq = 1
                            rowx -= 1
                            sheet.write(rowx, sub_col, sum_quantity, format_content_float)
                            sheet.write(rowx, sub_col + 1, sum_amount, format_content_float)
                            sheet.write(rowx, sub_col + 2, sum_tax, format_content_float)
                            sheet.write(rowx, sub_col + 3, sum_taxed, format_content_float)
                            if self.cost:
                                sheet.write(rowx, sub_col + 4, sum_cost, format_content_float)
                            rep_type_total_rows.append(rowx)
                            rowx += 1
                            sheet.merge_range(rowx, 0, rowx, sub_col - 1, _('Sub Total Move'),
                                              format_content_bold_right)
                            for h in range(sub_col, total_col + 1):
                                sum_rows = []
                                for t in rep_type_total_rows:
                                    sum_rows.append('' + xl_rowcol_to_cell(t, h) + '')
                                sheet.write_formula(rowx, h,
                                                    '{=SUM(' + ','.join(map(str, sum_rows)) + ')}',
                                                    format_content_bold_float)
                            rowx += 1
                            name = self.get_type_name(r['rep_type'])
                            sheet.merge_range(rowx, 0, rowx, total_col, name, format_group_left)
                            rep_type_total_rows = []

                            rowx += 1
                            col = 0
                            sheet.write(rowx, col, sub_seq, format_content_text)
                            col += 1
                            sheet.write(rowx, col, r['date'], format_content_date)
                            col += 1
                            sheet.write(rowx, col, r['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, r['origin'], format_content_text)
                            col += 1
                            if self.env.user.company_id.show_default_code_on_report:
                                sheet.write(rowx, col, '', format_content_text)
                                col += 1
                            sheet.write(rowx, col, r['partner'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, move_name, format_content_text)
                            sum_tax = sum_amount = sum_quantity = sum_taxed = 0
                            rowx += 1
                        # Дараагийн хөдөлгөөнийг зурна
                        elif origin and origin != r['origin']:
                            rowx -= 1
                            sheet.write(rowx, sub_col, sum_quantity, format_content_float)
                            sheet.write(rowx, sub_col + 1, sum_amount, format_content_float)
                            sheet.write(rowx, sub_col + 2, sum_tax, format_content_float)
                            sheet.write(rowx, sub_col + 3, sum_taxed, format_content_float)
                            if self.cost:
                                sheet.write(rowx, sub_col + 4, sum_cost, format_content_float)
                            rep_type_total_rows.append(rowx)
                            rowx += 1
                            sub_seq += 1
                            col = 0
                            sheet.write(rowx, col, sub_seq, format_content_text)
                            col += 1
                            sheet.write(rowx, col, r['date'], format_content_date)
                            col += 1
                            sheet.write(rowx, col, r['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, r['origin'], format_content_text)
                            col += 1
                            if self.env.user.company_id.show_default_code_on_report:
                                sheet.write(rowx, col, '', format_content_text)
                                col += 1
                            sheet.write(rowx, col, r['partner'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, move_name, format_content_text)
                            sum_tax = sum_amount = sum_quantity = sum_taxed = 0
                            rowx += 1

                        tax_id = False
                        taxed = round(r['amount'], 4)
                        if r['tax']:
                            tax_id = self.env['account.tax'].browse(r['tax'])
                            if tax_id.price_include and (r['qty'] * r['unit_price']):
                                TAX_FACTOR = round(1 - ((r['qty'] * r['unit_price'] - r['amount']) * 100 / (
                                        r['qty'] * r['unit_price'])), 4)
                            else:
                                TAX_FACTOR = round(tax_id.amount / 100, 4)
                        if r['rep_type'] in ('purchase', 'swap', 'pos'):
                            if r['rep_type'] == 'pos':
                                if tax_id and tax_id.price_include:
                                    tax = round(r['qty'] * r['unit_price'] - r['amount'], 4)
                                    r['amount'] = round(r['qty'] * r['unit_price'], 4)
                                else:
                                    tax = round(float(r['amount']) * float(float(TAX_FACTOR)), 4)
                                    r['amount'] = taxed + tax
                            elif r['rep_type'] == 'swap':
                                if r['stax'] > 0:
                                    if tax_id and tax_id.price_include:
                                        tax = round(r['qty'] * r['unit_price'] - r['amount'], 4)
                                        r['amount'] = round(r['qty'] * r['unit_price'], 4)
                                    else:
                                        tax = round(float(r['amount']) * float(float(TAX_FACTOR)), 4)
                                        r['amount'] = taxed + tax
                            else:
                                if r['tax'] > 0:
                                    if tax_id and tax_id.price_include:
                                        tax = round(r['qty'] * r['unit_price'] - r['amount'], 4)
                                        r['amount'] = round(r['qty'] * r['unit_price'], 4)
                                    else:
                                        tax = round(float(r['amount']) * float(float(TAX_FACTOR)), 4)
                                        r['amount'] = taxed + tax

                        # Нийт дүнг олно
                        total_quantity += r['qty']
                        total_amount += r['amount']
                        total_tax += tax
                        total_taxed += taxed
                        if self.cost:
                            total_cost += r['cost']
                        rep_type = r['rep_type']
                        origin = r['origin']

                        sum_quantity += r['qty']
                        sum_amount += r['amount']
                        sum_tax += tax
                        sum_taxed += taxed
                        if self.cost:
                            sum_cost += r['cost']
                    rowx -= 1
                    sheet.write(rowx, sub_col, sum_quantity, format_content_float)
                    sheet.write(rowx, sub_col + 1, sum_amount, format_content_float)
                    sheet.write(rowx, sub_col + 2, sum_tax, format_content_float)
                    sheet.write(rowx, sub_col + 3, sum_taxed, format_content_float)
                    if self.cost:
                        sheet.write(rowx, sub_col + 4, sum_cost, format_content_float)
                    if self.cost:
                        sheet.write(rowx, sub_col + 4, sum_cost,
                                    format_content_float)

                    rep_type_total_rows.append(rowx)
                    rowx += 1
                    sheet.merge_range(rowx, 0, rowx, sub_col - 1, _('Sub Total Move'),
                                      format_content_bold_right)
                    for h in range(sub_col, total_col + 1):
                        sum_rows = []
                        for t in rep_type_total_rows:
                            sum_rows.append('' + xl_rowcol_to_cell(t, h) + '')
                        sheet.write_formula(rowx, h,
                                            '{=SUM(' + ','.join(map(str, sum_rows)) + ')}', format_content_bold_float)
                    rowx += 1

                    sheet.merge_range(rowx, 0, rowx, sub_col - 1, _('Total'),
                                      format_group_right)
                    sheet.write(rowx, sub_col, total_quantity, format_group_float)
                    sheet.write(rowx, sub_col + 1, total_amount, format_group_float)
                    sheet.write(rowx, sub_col + 2, total_tax, format_group_float)
                    sheet.write(rowx, sub_col + 3, total_taxed, format_group_float)
                    if self.cost:
                        sheet.write(rowx, sub_col + 4, total_cost, format_group_float)
        rowx += 2
        # Хөл зурах
        sheet.write(rowx, 1,_('Executive Director .................................. (                                   )'), format_filter)
        rowx += 2
        sheet.write(rowx, 1,_('General Accountant .................................. (                               )'), format_filter)
        book.close()
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        return report_excel_output_obj.export_report()

    def get_type_name(self, rep_type):
        name = False
        if rep_type == 'procure':
            name = _('Transit')  # Нөхөн дүүргэлт
        elif rep_type == 'pos':
            name = _('Pos Refund')  # Посын буцаалт
        elif rep_type == 'purchase':
            name = _('Purchase')  # Худалдан авалт
        elif rep_type == 'mrp':
            name = _('MRP Production')  # Үйлдвэрлэл
        elif rep_type == 'swap':
            name = _('Swap')  # Солилцоо
        elif rep_type == 'refund':
            name = _('Sale Refund')  # Буцаалт
        elif rep_type == 'inventory':
            name = _('Inventory')  # Тооллого
        elif rep_type == 'other_refund':
            name = _('Other Refund')  # Бусад буцаалт
        return name
