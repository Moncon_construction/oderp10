# -*- coding: utf-8 -*-

import odoo.addons.decimal_precision as dp  # @UnresolvedImport
from odoo import api, models, fields


class StockInventoryCost(models.TransientModel):
    _name = "stock.inventory.cost"
    _description = "Stock Inventory Income Cost"

    inventory_id = fields.Many2one('stock.inventory', string='Inventory', required=True)
    lines = fields.One2many('stock.inventory.cost.line', 'wizard_id', string='Inventory Incomes')

    @api.model
    def default_get(self, fields):
        res = super(StockInventoryCost, self).default_get(fields)
        product_obj = self.env['product.product']
        inventory = self.env['stock.inventory'].browse(self._context['active_id'])
        if 'inventory_id' in self._fields:
            res['inventory_id'] = inventory.id
        if 'lines' in self._fields and inventory.state == 'counted':
            lines = []
            prods = map(lambda x: x.product_id.id, inventory.move_ids)
            prices = product_obj.search([('id', 'in', prods)]).price_get(ptype='standard_price')
            for move in inventory.move_ids:
                if move.location_id.usage == 'inventory':
                    lines.append((0, 0, {
                        'move_id': move.id,
                        'product_id': move.product_id.id,
                        'product_uom': move.product_uom.id,
                        'prodlot_id': move.restrict_lot_id.id,
                        'product_qty': move.product_uom_qty,
                        'location_id': move.location_id.id,
                        'location_dest_id': move.location_dest_id.id,
                        'price_unit': prices.get(move.product_id.id, 0)
                    }))
            res['lines'] = lines
        return res

    @api.multi
    def process(self):
        inventory = self.env['stock.inventory'].browse(self._context['active_id'])
        inventory.is_set_cost_button_clicked = True
        for wiz in self:
            for line in wiz.lines:
                if line.move_id.state != 'done':
                    line.move_id.write({'price_unit': line.price_unit})
        return True


class StockInventoryCostLine(models.TransientModel):
    _name = 'stock.inventory.cost.line'
    _description = 'Stock Inventory Income Cost Line'

    move_id = fields.Many2one('stock.move', string='Move', required=True, ondelete='cascade')
    product_id = fields.Many2one('product.product', string='Product', readonly=True)
    product_uom = fields.Many2one('product.uom', string='UOM', readonly=True)
    product_qty = fields.Float('Quantity', digits=dp.get_precision('Product Unit of Measure'), readonly=True)
    prodlot_id = fields.Many2one('stock.production.lot', string='Production Lot', readonly=True)
    location_id = fields.Many2one('stock.location', string='Location src', readonly=True)
    location_dest_id = fields.Many2one('stock.location', string='Location dest', readonly=True)
    price_unit = fields.Float(string='Unit Cost', required=True)
    wizard_id = fields.Many2one('stock.inventory.cost', string='Wizard', ondelete='cascade')
