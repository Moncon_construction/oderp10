# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models
from datetime import datetime
from datetime import timedelta
from tempfile import NamedTemporaryFile
from odoo.exceptions import UserError
from odoo.tools.translate import _
import base64, os, xlrd
import logging

_logger = logging.getLogger(__name__)

class StockInventoryImport(models.TransientModel):
    _name = 'stock.inventory.import'
    _description = 'Stock Inventory Import'

    data = fields.Binary('Data File', required=True)
    use_lot = fields.Boolean('Use Stock Production Lot')
    code_type = fields.Selection([
        ('barcode', 'Barcode'),
        ('default_code', 'Default Code')
    ], string='Import Code Type')

    
    @api.multi
    def import_inventory(self):
        context = dict(self._context or {})

        inventory_id = context['active_ids']
        inventory = self.env['stock.inventory'].browse(inventory_id)
        
        for form in self:
            fileobj = NamedTemporaryFile('w+')
            fileobj.write(base64.decodestring(form.data))
            fileobj.seek(0)
        
            if not os.path.isfile(fileobj.name):
                raise UserError(_("Import failed. Errorreason: The file has syntax errors"))

            book = xlrd.open_workbook(fileobj.name)  
            sheet_general = book.sheet_by_index(0)
            company_id = self.env.user.company_id.id
            nrows = sheet_general.nrows
            inventory_line_dic = {}
            rowi = 1
            
            if not form.use_lot:
                while rowi < nrows :
                    row = sheet_general.row(rowi)
                
                    code = row[0].value
                    product_qty = row[1].value
                    
                    product_id = None
                    
                    if form.code_type == 'barcode':
                        product_id = self.env['product.product'].search([('barcode','=',str(code))])
                    elif form.code_type == 'default_code':
                        product_id = self.env['product.product'].search([('default_code','=',str(code))])
                    if len(product_id) > 1:
                        raise UserError(_('Import failed. The product more than 1: %s') % code)
                    line = self.env['stock.inventory.line'].search([('product_id','=',product_id.id),('inventory_id','=',inventory.id)])
                    if len(line) > 1:
                        raise UserError(_('Import failed. This product more than 1 in the inventory line: %s') % code)
                    if line:
                        line.write({'product_qty': product_qty})
                    else:
                        if product_id:
                            if product_id[0] not in inventory_line_dic.keys():
                                inventory_line_dic[product_id[0]] = product_qty
                            else:
                                inventory_line_dic[product_id[0]] += product_qty
                        else:
                            raise UserError(_("Import failed. This product is not found: %s") % code)
                    
                    rowi += 1
                    
                if inventory_line_dic:
                    for product, qty in inventory_line_dic.iteritems():
                        data = {
                            'product_id': product.id,
                            'product_uom_id': product.uom_id.id,
                            'product_name': product.name,
                            'product_qty': qty,
                            'company_id': company_id,
                            'location_id': inventory.location_id.id,
                            'location_name': inventory.location_id.name,
                            'inventory_id': inventory_id[0]
                        }
                        self.env['stock.inventory.line'].create(data)
            else:
                error_lots = {}
                while rowi < nrows :
                    row = sheet_general.row(rowi)
                
                    default_code = int(row[0].value)
                    lot = row[1].value
                    product_qty = row[2].value
                    
                    #Check product default code
                    product_id = self.env['product.product'].search([('default_code','=',str(default_code))])
                    if len(product_id) > 1:
                        raise UserError(_('Import failed. The product more than 1: %s') % code)
                    if not product_id:
                        raise UserError(_('Import failed. This product is not found: %s') % default_code)
                    
                    #Check production lot
                    self.env.cr.execute("select name from stock_production_lot where name = '"+str(lot)+"' and product_id = '"+str(product_id.id)+"'")
                    
                    lot_name = self.env.cr.fetchone()
                    if lot_name == [] or lot_name == None:
                        if default_code not in error_lots.keys():
                            error_lots[default_code] = []
                            error_lots[default_code].append(lot)
                        else:
                            error_lots[default_code].append(lot)
                    rowi += 1
                
                if error_lots:
                    raise UserError(_('Дараах сериуд олдсонгүй ! %s') % (error_lots))
                
                rowi = 1
                while rowi < nrows:
                    row = sheet_general.row(rowi)
                
                    default_code = row[0].value
                    lot = row[1].value
                    product_qty = row[2].value
                    
                    product_id = self.env['product.product'].search([('default_code','=',str(default_code))])
                    
                    if len(product_id) > 1:
                        raise UserError(_('Import failed. The product more than 1: %s') % code)
                    line = self.env['stock.inventory.line'].search([('product_id','=',product_id.id),('inventory_id','=',inventory.id)])
                    if len(line) > 1:
                        raise UserError(_('Import failed. This product more than 1 in the inventory line: %s') % code)
                    if line:
                        line.write({'product_qty': product_qty})
                    else:
                        self.env.cr.execute("select id from stock_production_lot where name = '"+str(lot)+"' and product_id = '"+str(product_id.id)+"'")
                        
                        lot_id = self.env.cr.fetchone()
                        
                        if product_id[0] not in inventory_line_dic.keys():
                            inventory_line_dic[product_id[0]] = {lot_id: product_qty}
                        elif lot_id not in inventory_line_dic[product_id[0]].keys():
                            inventory_line_dic[product_id[0]][lot_id] = product_qty
                        else:
                            inventory_line_dic[product_id[0]][lot_id] += product_qty

                    rowi += 1
                if inventory_line_dic:
                    for product, lot_dic in inventory_line_dic.iteritems():
                        for lot_id, qty in lot_dic.iteritems():
                            data = {
                                'product_id': product.id,
                                'product_uom_id': product.uom_id.id,
                                'product_name': product.name,
                                'product_qty': qty,
                                'prod_lot_id': lot_id,
                                'company_id': company_id,
                                'location_id': inventory.location_id.id,
                                'location_name': inventory.location_id.name,
                                'inventory_id': inventory_id[0]
                            }
                        self.env['stock.inventory.line'].create(data)                
        return True