# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, models, fields
from odoo.tools.translate import _
from odoo.tools import float_is_zero
from odoo.exceptions import UserError
import base64
from io import BytesIO
import time
import xlsxwriter
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from datetime import datetime

class PrintStockInventory(models.TransientModel):
    _name = "print.stock.inventory.wizard"
    _description = "Print Inventory"

    company_id = fields.Many2one('res.company', string='Company', readonly=True, default=lambda self: self.env.user.company_id)
    document_type = fields.Selection([('all', 'All'), ('print_surplus', 'Print Surplus'), ('print_deficiency', 'Print Deficiency')], required=True, string='Document Type', default='all')

    @api.multi
    def export_report(self):
        #create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        #create name
        report_name = _(u'Тооллогын баримт')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_title_small = book.add_format(ReportExcelCellStyles.format_title_small)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_group_right = book.add_format(ReportExcelCellStyles.format_group_right)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_bold_right_no_border = {
        'font_name': 'Times New Roman',
        'font_size': 9,
        'align': 'right',
        'valign': 'vcenter'
        }
        format_content_bold_right_no_border = book.add_format(format_content_bold_right_no_border)
        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=_('stock_inventory'), form_title=file_name).create({})

        stock_inventories = self.env['stock.inventory'].browse(self._context.get('active_ids', []))
        if stock_inventories and len(stock_inventories) > 0:
            unique_count = 1
            for stock_inventory in stock_inventories:
                # create sheet
                name = str(unique_count) + ". "
                if stock_inventory.accounting_date:
                    name += "/" + datetime.strptime(stock_inventory.accounting_date, '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d') + "/ " + stock_inventory.name
                else:
                    name += "/" + datetime.strptime(stock_inventory.date, '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d') + "/ " + stock_inventory.name
                sheet = book.add_worksheet(name)
                unique_count += 1
                sheet.set_landscape()
                sheet.set_paper(9)  # A4
                sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
                sheet.fit_to_pages(1, 0)
                sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
                rowx = 0
        
                # compute column
                colx_number = 3
                sheet.set_column('A:A', 2)
                sheet.set_column('B:B', 8)
                sheet.set_column('C:C', 20)
                sheet.set_column('D:D', 10)
                sheet.set_column('E:E', 10)
                sheet.set_column('F:F', 10)
                sheet.set_column('G:G', 8)
                sheet.set_column('H:H', 10)
                sheet.set_column('I:I', 10)
                sheet.set_column('J:J', 10)
                sheet.set_column('K:K', 10)
                sheet.set_column('L:L', 10)
                sheet.set_column('M:M', 10)
                sheet.set_column('N:N', 10)
                sheet.set_column('O:O', 10)
                sheet.set_column('P:P', 10)
        
                # ** Print out Report Title
                sheet.merge_range(rowx, 0, rowx, 3, (_(u'Company Name: %s') % self.company_id.name), format_filter)
                rowx += 1
                sheet.merge_range(rowx, 2, rowx, 6, report_name, format_name)
                rowx += 1
                sheet.write(rowx, 1, (_('Date:')), format_filter)
                sheet.write(rowx, 2, (_('%s') % time.strftime('%Y-%m-%d')), format_filter)
                sheet.merge_range(rowx, 3, rowx, 6, (_(u'Location: %s') % stock_inventory.location_id.complete_name), format_filter)
                sheet.merge_range(rowx, 7, rowx, 10, (_(u'Stock Inventory Name: %s') % stock_inventory.name), format_filter)
                # **
                # BEGIN: Form non account module table SQL addition+
        
                is_serial_number = False
                if self.env.user.has_group('stock.group_production_lot'):
                    is_serial_number = True
                # Сэрийн дугаар бүртгэж байгаа эсэхийг мэдэж байна
        
                is_manager = False
                if self.env.user.has_group('stock.group_stock_manager'):
                    is_manager = True
                # Агуулахын менежир эсхийг мэдэж байна.
                rowx += 1
        
                print is_serial_number
                if is_serial_number:
                    sheet.merge_range(rowx, 0, rowx+1, 0, _('№'), format_title_small)
                    sheet.merge_range(rowx, 1, rowx+1, 1, _('Default Code'), format_title_small)
                    sheet.merge_range(rowx, 2, rowx+1, 2, _('Name'), format_title_small)
                    sheet.merge_range(rowx, 3, rowx+1, 3, _('Barcode'), format_title_small)
                    sheet.merge_range(rowx, 4, rowx+1, 4, _('Serial Number'), format_title_small)
                    sheet.merge_range(rowx, 5, rowx+1, 5, _('UOM'), format_title_small)
        
                    if is_manager:
                        sheet.merge_range(rowx, 6, rowx+1, 6, _('Price Unit'), format_title_small)
                        sheet.merge_range(rowx, 7, rowx+1, 7, _('List Price'), format_title_small)
                        sheet.merge_range(rowx, 8, rowx, 10, _('Theoretical Quantity'), format_title_small)
                        sheet.merge_range(rowx, 11, rowx, 13, _('Checked Quantity'), format_title_small)
                        sheet.merge_range(rowx, 14, rowx, 16, _('Surplus'), format_title_small)
                        sheet.merge_range(rowx, 17, rowx, 19, _('Deficiency'), format_title_small)
                        sheet.write(rowx+1,8,_('Number'), format_title_small)
                        sheet.write(rowx+1,9,_('Result Price Unit'), format_title_small)
                        sheet.write(rowx+1,10,_('Result'), format_title_small)
                        sheet.write(rowx+1,11,_('Number'), format_title_small)
                        sheet.write(rowx+1,12,_('Result Price Unit'), format_title_small)
                        sheet.write(rowx+1,13,_('Result'), format_title_small)
                        sheet.write(rowx+1,14,_('Number'), format_title_small)
                        sheet.write(rowx+1,15,_('Result Price Unit'), format_title_small)
                        sheet.write(rowx+1,16,_('Result'), format_title_small)
                        sheet.write(rowx+1,17,_('Number'), format_title_small)
                        sheet.write(rowx+1,18,_('Result Price Unit'), format_title_small)
                        sheet.write(rowx+1,19,_('Result'), format_title_small)
                    else:
                        sheet.merge_range(rowx, 6, rowx+1, 6, _('List Price'), format_title_small)
                        sheet.merge_range(rowx, 7, rowx, 8, _('Theoretical Quantity'), format_title_small)
                        sheet.merge_range(rowx, 9, rowx, 10, _('Checked Quantity'), format_title_small)
                        sheet.merge_range(rowx, 11, rowx, 12, _('Surplus'), format_title_small)
                        sheet.merge_range(rowx, 13, rowx, 14, _('Deficiency'), format_title_small)
                        sheet.write(rowx+1,7,_('Number'), format_title_small)
                        sheet.write(rowx+1,8,_('Result'), format_title_small)
                        sheet.write(rowx+1,9,_('Number'), format_title_small)
                        sheet.write(rowx+1,10,_('Result'), format_title_small)
                        sheet.write(rowx+1,11,_('Number'), format_title_small)
                        sheet.write(rowx+1,12,_('Result'), format_title_small)
                        sheet.write(rowx+1,13,_('Number'), format_title_small)
                        sheet.write(rowx+1,14,_('Result'), format_title_small)
                else:
                    sheet.merge_range(rowx, 0, rowx+1, 0, _('№'), format_title_small)
                    sheet.merge_range(rowx, 1, rowx+1, 1, _('Default Code'), format_title_small)
                    sheet.merge_range(rowx, 2, rowx+1, 2, _('Name'), format_title_small)
                    sheet.merge_range(rowx, 3, rowx+1, 3, _('Barcode'), format_title_small)
                    sheet.merge_range(rowx, 4, rowx+1, 4, _('UOM'), format_title_small)
        
                    if is_manager:
                        sheet.merge_range(rowx, 5, rowx+1, 5, _('Price Unit'), format_title_small)
                        sheet.merge_range(rowx, 6, rowx+1, 6, _('List Price'), format_title_small)
                        sheet.merge_range(rowx, 7, rowx, 9, _('Theoretical Quantity'), format_title_small)
                        sheet.merge_range(rowx, 10, rowx, 12, _('Checked Quantity'), format_title_small)
                        sheet.merge_range(rowx, 13, rowx, 15, _('Surplus'), format_title_small)
                        sheet.merge_range(rowx, 16, rowx, 18, _('Deficiency'), format_title_small)
                        sheet.write(rowx+1,7,_('Number'), format_title_small)
                        sheet.write(rowx+1,8,_('Result Price Unit'), format_title_small)
                        sheet.write(rowx+1,9,_('Result'), format_title_small)
                        sheet.write(rowx+1,10,_('Number'), format_title_small)
                        sheet.write(rowx+1,11,_('Result Price Unit'), format_title_small)
                        sheet.write(rowx+1,12,_('Result'), format_title_small)
                        sheet.write(rowx+1,13,_('Number'), format_title_small)
                        sheet.write(rowx+1,14,_('Result Price Unit'), format_title_small)
                        sheet.write(rowx+1,15,_('Result'), format_title_small)
                        sheet.write(rowx+1,16,_('Number'), format_title_small)
                        sheet.write(rowx+1,17,_('Result Price Unit'), format_title_small)
                        sheet.write(rowx+1,18,_('Result'), format_title_small)
                    else:
                        sheet.merge_range(rowx, 5, rowx+1, 5, _('List Price'), format_title_small)
                        sheet.merge_range(rowx, 6, rowx, 7, _('Theoretical Quantity'), format_title_small)
                        sheet.merge_range(rowx, 8, rowx, 9, _('Checked Quantity'), format_title_small)
                        sheet.merge_range(rowx, 10, rowx, 11, _('Surplus'), format_title_small)
                        sheet.merge_range(rowx, 12, rowx, 13, _('Deficiency'), format_title_small)
                        sheet.write(rowx+1,6,_('Number'), format_title_small)
                        sheet.write(rowx+1,7,_('Result'), format_title_small)
                        sheet.write(rowx+1,8,_('Number'), format_title_small)
                        sheet.write(rowx+1,9,_('Result'), format_title_small)
                        sheet.write(rowx+1,10,_('Number'), format_title_small)
                        sheet.write(rowx+1,11,_('Result'), format_title_small)
                        sheet.write(rowx+1,12,_('Number'), format_title_small)
                        sheet.write(rowx+1,13,_('Result'), format_title_small)
        
                if is_manager:
                    man_fields = ('table1.price_unit, '
                                  'CASE WHEN (sil.theoretical_qty>0) THEN CASE WHEN(table1.price_unit is null) THEN 0 ELSE (sil.theoretical_qty*table1.price_unit) END ELSE 0 END AS theoretical_total_price_unit, '
                                  'CASE WHEN (sil.product_qty>0) THEN CASE WHEN(table1.price_unit is null) THEN 0 ELSE (sil.product_qty*table1.price_unit) END ELSE 0 END AS product_total_price_unit, '
                                  'CASE WHEN (sil.theoretical_qty!=sil.product_qty) THEN CASE WHEN(sil.theoretical_qty-sil.product_qty > 0) THEN CASE WHEN(table1.price_unit is null) THEN 0 ELSE ((sil.theoretical_qty-sil.product_qty)*table1.price_unit) END ELSE 0 END ELSE 0 END AS total_price_unit_deficiency, '
                                  'CASE WHEN (sil.theoretical_qty!=sil.product_qty) THEN CASE WHEN(sil.product_qty-sil.theoretical_qty > 0) THEN CASE WHEN(table1.price_unit is null) THEN 0 ELSE ((sil.product_qty-sil.theoretical_qty)*table1.price_unit) END ELSE 0 END ELSE 0 END AS total_price_unit_surplus, '
                                  )
                    man_tables = "LEFT JOIN (select distinct price_unit, si.id as id, sm.product_id as product_id, sm.restrict_lot_id as lot_id from stock_move as sm left join stock_inventory as si on sm.inventory_id = si.id) as table1 on table1.id = sil.inventory_id and table1.product_id = sil.product_id and (table1.lot_id = sil.prod_lot_id or sil.prod_lot_id is null) "
                else:
                    man_fields = ''
                    man_tables = ''
        
                if is_serial_number:
                    sn_fields =  ('spl.name AS serial_name, ')
                    sn_tables =  "LEFT JOIN stock_production_lot spl on spl.product_id=pp.id and sil.prod_lot_id=spl.id "
                else:
                    sn_fields =  ''
                    sn_tables =  ''
        
                query = ('SELECT '
                            'pt.default_code, '
                            'pt.name, '
                            'pt.id AS pt_id, '
                            'pp.barcode, '
                            'pc.name as categ_name, '
                            'pt.categ_id, '
                            +sn_fields+
                            'pu.name AS uom, '
                            'sil.standard_price, '
                            'pt.list_price, '
                            'sil.theoretical_qty, '
                            'CASE WHEN (sil.theoretical_qty>0) THEN (sil.theoretical_qty*pt.list_price) ELSE 0 END AS theoretical_total_price, '
                            'sil.product_qty, '
                            +man_fields+
                            'CASE WHEN (sil.product_qty>0) THEN (sil.product_qty*pt.list_price) ELSE 0 END AS product_total_price, '
                            'CASE WHEN (sil.theoretical_qty!=sil.product_qty) THEN CASE WHEN(sil.theoretical_qty-sil.product_qty > 0) THEN (sil.theoretical_qty-sil.product_qty) ELSE 0 END ELSE 0 END AS qty_deficience, '
                            'CASE WHEN (sil.theoretical_qty!=sil.product_qty) THEN CASE WHEN(sil.theoretical_qty-sil.product_qty > 0) THEN ((sil.theoretical_qty-sil.product_qty)::decimal(16,2)*pt.list_price) ELSE 0 END ELSE 0 END AS total_price_deficiency, '
                            'CASE WHEN (sil.theoretical_qty!=sil.product_qty) THEN CASE WHEN(sil.product_qty-sil.theoretical_qty > 0) THEN (sil.product_qty-sil.theoretical_qty) ELSE 0 END ELSE 0 END AS qty_surplus, '
                            'CASE WHEN (sil.theoretical_qty!=sil.product_qty) THEN CASE WHEN(sil.product_qty-sil.theoretical_qty > 0) THEN ((sil.product_qty-sil.theoretical_qty)::decimal(16,2)*pt.list_price) ELSE 0 END ELSE 0 END AS total_price_surplus, '
                            'CASE WHEN (sil.theoretical_qty!=sil.product_qty) THEN ((sil.product_qty-sil.theoretical_qty)*sil.standard_price) ELSE 0 END AS total_standard_price_difference '
                        'FROM stock_inventory_line sil '
                            'LEFT JOIN product_product pp on sil.product_id=pp.id '
                            'LEFT JOIN product_template pt on pt.id=pp.product_tmpl_id '
                            +sn_tables+
                            'LEFT JOIN product_uom pu on pu.id=sil.product_uom_id '
                            'LEFT JOIN product_category pc on pc.id = pt.categ_id '
                            +man_tables+
                        'WHERE sil.inventory_id = %s order by categ_name asc' % (stock_inventory.id))
        
                self.env.cr.execute(query)
                lines = self.env.cr.dictfetchall()
                ind=1
                rowx += 2
                old_categ = ''
                old_categ = lines[0]['categ_name']
                if is_serial_number:
                    sheet.merge_range(rowx, 0, rowx, 14, (_(u'Category: %s') % old_categ), format_content_text)
                    rowx += 1
                else:
                    sheet.merge_range(rowx, 0, rowx, 13, (_(u'Category: %s') % old_categ), format_content_text)
                    rowx += 1
        
                #Ангилал бүрийн нийт дүн бодох
                theoretical_categ_qty = 0.0
                theoretical_categ_price = 0.0
                theoretical_categ_price_unit = 0.0
                product_categ_qty = 0.0
                product_categ_price = 0.0
                product_categ_price_unit = 0.0
                categ_qty_surplus = 0.0
                categ_price_surplus = 0.0
                categ_price_surplus_unit = 0.0
                categ_qty_deficiency = 0.0
                categ_price_deficiency = 0.0
                categ_price_deficiency_unit = 0.0
        
                #Нийт дүн бодох
                theoretical_total_qty = 0.0
                theoretical_total_price = 0.0
                theoretical_total_price_unit = 0.0
                product_total_qty = 0.0
                product_total_price = 0.0
                product_total_price_unit = 0.0
                total_qty_surplus = 0.0
                total_price_surplus = 0.0
                total_price_surplus_unit = 0.0
                total_qty_deficiency = 0.0
                total_price_deficiency = 0.0
                total_price_deficiency_unit = 0.0
                write_lines = []
                for obj in lines:
                    if self.document_type == 'print_surplus':
                        if obj['qty_surplus'] > 0:
                            write_lines.append(obj)
                    elif self.document_type == 'print_deficiency':
                        if obj['qty_deficience'] > 0:
                            write_lines.append(obj)
                    else:
                        write_lines.append(obj)
                for line in write_lines:
                    if is_serial_number:
                        if is_manager:
                            if old_categ != line['categ_name']:
                                sheet.write(rowx, 8, theoretical_categ_qty, format_content_float)
                                sheet.write(rowx, 10, theoretical_categ_price, format_content_float)
                                sheet.write(rowx, 9, theoretical_categ_price_unit, format_content_float)
                                sheet.write(rowx, 11, product_categ_qty, format_content_float)
                                sheet.write(rowx, 13, product_categ_price, format_content_float)
                                sheet.write(rowx, 12, product_categ_price_unit, format_content_float)
                                sheet.write(rowx, 14, categ_qty_surplus, format_content_float)
                                sheet.write(rowx, 16, categ_price_surplus, format_content_float)
                                sheet.write(rowx, 15, categ_price_surplus_unit, format_content_float)
                                sheet.write(rowx, 17, categ_qty_deficiency, format_content_float)
                                sheet.write(rowx, 19, categ_price_deficiency, format_content_float)
                                sheet.write(rowx, 18, categ_price_deficiency_unit, format_content_float)
                                theoretical_categ_qty = 0.0
                                theoretical_categ_price = 0.0
                                theoretical_categ_price_unit = 0.0
                                product_categ_qty = 0.0
                                product_categ_price = 0.0
                                product_categ_price_unit = 0.0
                                categ_qty_surplus = 0.0
                                categ_price_surplus = 0.0
                                categ_price_surplus_unit = 0.0
                                categ_qty_deficiency = 0.0
                                categ_price_deficiency = 0.0
                                categ_price_deficiency_unit = 0.0
                                rowx += 1
                                sheet.merge_range(rowx, 0, rowx, 14, (_(u'Category: %s') % line['categ_name']), format_content_text)
                                old_categ = line['categ_name']
                                rowx += 1
        
                            sheet.write(rowx, 7, line['list_price'], format_content_number)
                            sheet.write(rowx, 6, line['price_unit'], format_content_float)
                            sheet.write(rowx, 8, line['theoretical_qty'], format_content_float)
                            sheet.write(rowx, 10, line['theoretical_total_price'], format_content_float)
                            sheet.write(rowx, 9, line['theoretical_total_price_unit'], format_content_float)
                            sheet.write(rowx, 11, line['product_qty'], format_content_float)
                            sheet.write(rowx, 13, line['product_total_price'], format_content_float)
                            sheet.write(rowx, 12, line['product_total_price_unit'], format_content_float)
                            sheet.write(rowx, 14, line['qty_surplus'], format_content_float)
                            sheet.write(rowx, 16, line['total_price_surplus'], format_content_float)
                            sheet.write(rowx, 15, line['total_price_unit_surplus'], format_content_float)
                            sheet.write(rowx, 17, line['qty_deficience'], format_content_float)
                            sheet.write(rowx, 19, line['total_price_deficiency'], format_content_float)
                            sheet.write(rowx, 18, line['total_price_unit_deficiency'], format_content_float)
        
                            theoretical_categ_price_unit += line['theoretical_total_price_unit']
                            product_categ_price_unit += line['product_total_price_unit']
                            categ_price_surplus_unit += line['total_price_unit_surplus']
                            categ_price_deficiency_unit += line['total_price_unit_deficiency']
        
                            theoretical_total_price_unit += line['theoretical_total_price_unit']
                            product_total_price_unit += line['product_total_price_unit']
                            total_price_surplus_unit += line['total_price_unit_surplus']
                            total_price_deficiency_unit += line['total_price_unit_deficiency']
                        else:
                            if old_categ != line['categ_name']:
                                sheet.write(rowx, 7, theoretical_categ_qty, format_content_float)
                                sheet.write(rowx, 8, theoretical_categ_price, format_content_float)
                                sheet.write(rowx, 9, product_categ_qty, format_content_float)
                                sheet.write(rowx, 10, product_categ_price, format_content_float)
                                sheet.write(rowx, 11, categ_qty_surplus, format_content_float)
                                sheet.write(rowx, 12, categ_price_surplus, format_content_float)
                                sheet.write(rowx, 13, categ_qty_deficiency, format_content_float)
                                sheet.write(rowx, 14, categ_price_deficiency, format_content_float)
                                theoretical_categ_qty = 0.0
                                theoretical_categ_price = 0.0
                                product_categ_qty = 0.0
                                product_categ_price = 0.0
                                categ_qty_surplus = 0.0
                                categ_price_surplus = 0.0
                                categ_qty_deficiency = 0.0
                                categ_price_deficiency = 0.0
                                rowx += 1
                                sheet.merge_range(rowx, 0, rowx, 14, (_(u'Category: %s') % line['categ_name']), format_content_text)
                                old_categ = line['categ_name']
                                rowx += 1
        
                            sheet.write(rowx, 6, line['list_price'], format_content_number)
                            sheet.write(rowx, 7, line['theoretical_qty'], format_content_float)
                            sheet.write(rowx, 8, line['theoretical_total_price'], format_content_float)
                            sheet.write(rowx, 9, line['product_qty'], format_content_float)
                            sheet.write(rowx, 10, line['product_total_price'], format_content_float)
                            sheet.write(rowx, 11, line['qty_surplus'], format_content_float)
                            sheet.write(rowx, 12, line['total_price_surplus'], format_content_float)
                            sheet.write(rowx, 13, line['qty_deficience'], format_content_float)
                            sheet.write(rowx, 14, line['total_price_deficiency'], format_content_float)
        
                        sheet.write(rowx, 0, ind, format_content_text)
                        sheet.write(rowx, 1, line['default_code'], format_content_text)
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['pt_id'])
                        sheet.write(rowx, 2, pt_with_user_lang.name, format_content_text)
                        sheet.write(rowx, 3, line['barcode'], format_content_text)
                        sheet.write(rowx, 4, line['serial_name'], format_content_text)
                        sheet.write(rowx, 5, line['uom'], format_content_text)
        
                        theoretical_categ_qty += line['theoretical_qty']
                        theoretical_categ_price += line['theoretical_total_price']
                        product_categ_qty += line['product_qty']
                        product_categ_price += line['product_total_price']
                        categ_qty_surplus += line['qty_surplus']
                        categ_price_surplus += line['total_price_surplus']
                        categ_qty_deficiency += line['qty_deficience']
                        categ_price_deficiency += line['total_price_deficiency']
        
                        theoretical_total_qty += line['theoretical_qty']
                        theoretical_total_price += line['theoretical_total_price']
                        product_total_qty += line['product_qty']
                        product_total_price += line['product_total_price']
                        total_qty_surplus += line['qty_surplus']
                        total_price_surplus += line['total_price_surplus']
                        total_qty_deficiency += line['qty_deficience']
                        total_price_deficiency += line['total_price_deficiency']
                    else:
                        if is_manager:
                            if old_categ != line['categ_name']:
                                sheet.write(rowx, 7, theoretical_categ_qty, format_content_float)
                                sheet.write(rowx, 9, theoretical_categ_price, format_content_float)
                                sheet.write(rowx, 8, theoretical_categ_price_unit, format_content_float)
                                sheet.write(rowx, 10, product_categ_qty, format_content_float)
                                sheet.write(rowx, 12, product_categ_price, format_content_float)
                                sheet.write(rowx, 11, product_categ_price_unit, format_content_float)
                                sheet.write(rowx, 13, categ_qty_surplus, format_content_float)
                                sheet.write(rowx, 15, categ_price_surplus, format_content_float)
                                sheet.write(rowx, 14, categ_price_surplus_unit, format_content_float)
                                sheet.write(rowx, 16, categ_qty_deficiency, format_content_float)
                                sheet.write(rowx, 18, categ_price_deficiency, format_content_float)
                                sheet.write(rowx, 17, categ_price_deficiency_unit, format_content_float)
                                theoretical_categ_qty = 0.0
                                theoretical_categ_price = 0.0
                                theoretical_categ_price_unit = 0.0
                                product_categ_qty = 0.0
                                product_categ_price = 0.0
                                product_categ_price_unit = 0.0
                                categ_qty_surplus = 0.0
                                categ_price_surplus = 0.0
                                categ_price_surplus_unit = 0.0
                                categ_qty_deficiency = 0.0
                                categ_price_deficiency = 0.0
                                categ_price_deficiency_unit = 0.0
                                rowx += 1
                                sheet.merge_range(rowx, 0, rowx, 13, (_(u'Category: %s') % line['categ_name']), format_content_text)
                                old_categ = line['categ_name']
                                rowx += 1
        
                            sheet.write(rowx, 6, line['list_price'], format_content_number)
                            sheet.write(rowx, 5, line['price_unit'], format_content_float)
                            sheet.write(rowx, 7, line['theoretical_qty'], format_content_float)
                            sheet.write(rowx, 9, line['theoretical_total_price'], format_content_float)
                            sheet.write(rowx, 8, line['theoretical_total_price_unit'], format_content_float)
                            sheet.write(rowx, 10, line['product_qty'], format_content_float)
                            sheet.write(rowx, 12, line['product_total_price'], format_content_float)
                            sheet.write(rowx, 11, line['product_total_price_unit'], format_content_float)
                            sheet.write(rowx, 13, line['qty_surplus'], format_content_float)
                            sheet.write(rowx, 15, line['total_price_surplus'], format_content_float)
                            sheet.write(rowx, 14, line['total_price_unit_surplus'], format_content_float)
                            sheet.write(rowx, 16, line['qty_deficience'], format_content_float)
                            sheet.write(rowx, 18, line['total_price_deficiency'], format_content_float)
                            sheet.write(rowx, 17, line['total_price_unit_deficiency'], format_content_float)
        
                            theoretical_categ_price_unit += line['theoretical_total_price_unit']
                            product_categ_price_unit += line['product_total_price_unit']
                            categ_price_surplus_unit += line['total_price_unit_surplus']
                            categ_price_deficiency_unit += line['total_price_unit_deficiency']
        
                            theoretical_total_price_unit += line['theoretical_total_price_unit']
                            product_total_price_unit += line['product_total_price_unit']
                            total_price_surplus_unit += line['total_price_unit_surplus']
                            total_price_deficiency_unit += line['total_price_unit_deficiency']
                        else:
                            if old_categ != line['categ_name']:
                                sheet.write(rowx, 6, theoretical_categ_qty, format_content_float)
                                sheet.write(rowx, 7, theoretical_categ_price, format_content_float)
                                sheet.write(rowx, 8, product_categ_qty, format_content_float)
                                sheet.write(rowx, 9, product_categ_price, format_content_float)
                                sheet.write(rowx, 10, categ_qty_surplus, format_content_float)
                                sheet.write(rowx, 11, categ_price_surplus, format_content_float)
                                sheet.write(rowx, 12, categ_qty_deficiency, format_content_float)
                                sheet.write(rowx, 13, categ_price_deficiency, format_content_float)
                                theoretical_categ_qty = 0.0
                                theoretical_categ_price = 0.0
                                product_categ_qty = 0.0
                                product_categ_price = 0.0
                                categ_qty_surplus = 0.0
                                categ_price_surplus = 0.0
                                categ_qty_deficiency = 0.0
                                categ_price_deficiency = 0.0
                                rowx += 1
                                sheet.merge_range(rowx, 0, rowx, 13, (_(u'Category: %s') % line['categ_name']), format_content_text)
                                old_categ = line['categ_name']
                                rowx += 1
        
                            sheet.write(rowx, 5, line['list_price'], format_content_number)
                            sheet.write(rowx, 6, line['theoretical_qty'], format_content_float)
                            sheet.write(rowx, 7, line['theoretical_total_price'], format_content_float)
                            sheet.write(rowx, 8, line['product_qty'], format_content_float)
                            sheet.write(rowx, 9, line['product_total_price'], format_content_float)
                            sheet.write(rowx, 10, line['qty_difference'], format_content_float)
                            sheet.write(rowx, 11, line['total_price_difference'], format_content_float)
                            sheet.write(rowx, 12, line['total_standard_price_difference'], format_content_float)
                            sheet.write(rowx, 13, line['total_standard_price_difference'], format_content_float)
        
                        sheet.write(rowx, 0, ind, format_content_text)
                        sheet.write(rowx, 1, line['default_code'], format_content_text)
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['pt_id'])
                        sheet.write(rowx, 2, pt_with_user_lang.name, format_content_text)
                        sheet.write(rowx, 3, line['barcode'], format_content_text)
                        sheet.write(rowx, 4, line['uom'], format_content_text)
        
                        theoretical_categ_qty += line['theoretical_qty']
                        theoretical_categ_price += line['theoretical_total_price']
                        product_categ_qty += line['product_qty']
                        product_categ_price += line['product_total_price']
                        categ_qty_surplus += line['qty_surplus']
                        categ_price_surplus += line['total_price_surplus']
                        categ_qty_deficiency += line['qty_deficience']
                        categ_price_deficiency += line['total_price_deficiency']
        
                        theoretical_total_qty += line['theoretical_qty']
                        theoretical_total_price += line['theoretical_total_price']
                        product_total_qty += line['product_qty']
                        product_total_price += line['product_total_price']
                        total_qty_surplus += line['qty_surplus']
                        total_price_surplus += line['total_price_surplus']
                        total_qty_deficiency += line['qty_deficience']
                        total_price_deficiency += line['total_price_deficiency']
        
                    ind += 1
                    rowx += 1
        
                if is_serial_number:
                    if is_manager:
                        sheet.write(rowx, 8, theoretical_categ_qty, format_content_float)
                        sheet.write(rowx, 10, theoretical_categ_price, format_content_float)
                        sheet.write(rowx, 9, theoretical_categ_price_unit, format_content_float)
                        sheet.write(rowx, 11, product_categ_qty, format_content_float)
                        sheet.write(rowx, 13, product_categ_price, format_content_float)
                        sheet.write(rowx, 12, product_categ_price_unit, format_content_float)
                        sheet.write(rowx, 14, categ_qty_surplus, format_content_float)
                        sheet.write(rowx, 16, categ_price_surplus, format_content_float)
                        sheet.write(rowx, 15, categ_price_surplus_unit, format_content_float)
                        sheet.write(rowx, 17, categ_qty_deficiency, format_content_float)
                        sheet.write(rowx, 19, categ_price_deficiency, format_content_float)
                        sheet.write(rowx, 18, categ_price_deficiency_unit, format_content_float)
                    else:
                        sheet.write(rowx, 7, theoretical_categ_qty, format_content_float)
                        sheet.write(rowx, 8, theoretical_categ_price, format_content_float)
                        sheet.write(rowx, 9, product_categ_qty, format_content_float)
                        sheet.write(rowx, 10, product_categ_price, format_content_float)
                        sheet.write(rowx, 11, categ_qty_surplus, format_content_float)
                        sheet.write(rowx, 12, categ_price_surplus, format_content_float)
                        sheet.write(rowx, 13, categ_qty_deficiency, format_content_float)
                        sheet.write(rowx, 14, categ_price_deficiency, format_content_float)
                    rowx += 1
                else:
                    if is_manager:
                        sheet.write(rowx, 7, theoretical_categ_qty, format_content_float)
                        sheet.write(rowx, 9, theoretical_categ_price, format_content_float)
                        sheet.write(rowx, 8, theoretical_categ_price_unit, format_content_float)
                        sheet.write(rowx, 10, product_categ_qty, format_content_float)
                        sheet.write(rowx, 12, product_categ_price, format_content_float)
                        sheet.write(rowx, 11, product_categ_price_unit, format_content_float)
                        sheet.write(rowx, 13, categ_qty_surplus, format_content_float)
                        sheet.write(rowx, 15, categ_price_surplus, format_content_float)
                        sheet.write(rowx, 14, categ_price_surplus_unit, format_content_float)
                        sheet.write(rowx, 16, categ_qty_deficiency, format_content_float)
                        sheet.write(rowx, 18, categ_price_deficiency, format_content_float)
                        sheet.write(rowx, 17, categ_price_deficiency_unit, format_content_float)
                    else:
                        sheet.write(rowx, 6, theoretical_categ_qty, format_content_float)
                        sheet.write(rowx, 7, theoretical_categ_price, format_content_float)
                        sheet.write(rowx, 8, product_categ_qty, format_content_float)
                        sheet.write(rowx, 9, product_categ_price, format_content_float)
                        sheet.write(rowx, 10, categ_qty_surplus, format_content_float)
                        sheet.write(rowx, 11, categ_price_surplus, format_content_float)
                        sheet.write(rowx, 12, categ_qty_deficiency, format_content_float)
                        sheet.write(rowx, 13, categ_price_deficiency, format_content_float)
                    rowx += 1
        
                if is_serial_number:
                    if is_manager:
                        sheet.write(rowx, 8, theoretical_total_qty, format_group_float)
                        sheet.write(rowx, 10, theoretical_total_price, format_group_float)
                        sheet.write(rowx, 9, theoretical_total_price_unit, format_group_float)
                        sheet.write(rowx, 11, product_total_qty, format_group_float)
                        sheet.write(rowx, 13, product_total_price, format_group_float)
                        sheet.write(rowx, 12, product_total_price_unit, format_group_float)
                        sheet.write(rowx, 14, total_qty_surplus, format_group_float)
                        sheet.write(rowx, 16, total_price_surplus, format_group_float)
                        sheet.write(rowx, 15, total_price_surplus_unit, format_group_float)
                        sheet.write(rowx, 17, total_qty_deficiency, format_group_float)
                        sheet.write(rowx, 19, total_price_deficiency, format_group_float)
                        sheet.write(rowx, 18, total_price_deficiency_unit, format_group_float)
                    else:
                        sheet.write(rowx, 7, theoretical_total_qty, format_group_float)
                        sheet.write(rowx, 8, theoretical_total_price, format_group_float)
                        sheet.write(rowx, 9, product_total_qty, format_group_float)
                        sheet.write(rowx, 10, product_total_price, format_group_float)
                        sheet.write(rowx, 11, total_qty_surplus, format_group_float)
                        sheet.write(rowx, 12, total_price_surplus, format_group_float)
                        sheet.write(rowx, 13, total_qty_deficiency, format_group_float)
                        sheet.write(rowx, 14, total_price_deficiency, format_group_float)
                else:
                    if is_manager:
                        sheet.write(rowx, 7, theoretical_total_qty, format_group_float)
                        sheet.write(rowx, 9, theoretical_total_price, format_group_float)
                        sheet.write(rowx, 8, theoretical_total_price_unit, format_group_float)
                        sheet.write(rowx, 10, product_total_qty, format_group_float)
                        sheet.write(rowx, 12, product_total_price, format_group_float)
                        sheet.write(rowx, 11, product_total_price_unit, format_group_float)
                        sheet.write(rowx, 13, total_qty_surplus, format_group_float)
                        sheet.write(rowx, 15, total_price_surplus, format_group_float)
                        sheet.write(rowx, 14, total_price_surplus_unit, format_group_float)
                        sheet.write(rowx, 16, total_qty_deficiency, format_group_float)
                        sheet.write(rowx, 18, total_price_deficiency, format_group_float)
                        sheet.write(rowx, 17, total_price_deficiency_unit, format_group_float)
                    else:
                        sheet.write(rowx, 6, theoretical_total_qty, format_group_float)
                        sheet.write(rowx, 7, theoretical_total_price, format_group_float)
                        sheet.write(rowx, 8, product_total_qty, format_group_float)
                        sheet.write(rowx, 9, product_total_price, format_group_float)
                        sheet.write(rowx, 10, total_qty_surplus, format_group_float)
                        sheet.write(rowx, 11, total_price_surplus, format_group_float)
                        sheet.write(rowx, 12, total_qty_deficiency, format_group_float)
                        sheet.write(rowx, 13, total_price_deficiency, format_group_float)
        
                rowx += 3
                sheet.merge_range(rowx, 2, rowx, 10, u'Зөвшөөрсөн...................................../                                             /', format_content_bold_right_no_border)
                rowx += 1
                sheet.merge_range(rowx, 2, rowx, 10, u'Хөтөлсөн....................................../                                            /', format_content_bold_right_no_border)
                rowx += 1
                sheet.merge_range(rowx, 2, rowx, 10, u'Нярав......................................../                                              /', format_content_bold_right_no_border)

        book.close()

        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()


class CreateInvoice(models.TransientModel):
    _name = "create.invoice.stock.wizard"
    _description = "Create Invoice"

    @api.multi
    def _get_defualt_partner_id(self):
        stock_inventories = self.env['stock.inventory'].browse(self._context.get('active_ids', []))
        partner = stock_inventories.location_id.partner_id.id
        if partner:
            return partner
        else:
            raise UserError(_('Configure partner in stock location'))

    @api.multi
    def _get_defualt_account_receivable_id(self):
        stock_inventories = self.env['stock.inventory'].browse(self._context.get('active_ids', []))
        partner = stock_inventories.location_id.partner_id.id

        account_rec = self.env['res.partner'].search([('id', '=', partner)])
        return account_rec.property_account_receivable_id

    calculations = fields.Selection([('0', 'Create calculations of census of lack using selling price'),
                                     ('1', 'Create calculations of census of lack, excess diffrence using selling price')], required=True, String='Choose invoice', default='0')
    partner_id = fields.Many2one('res.partner', default=_get_defualt_partner_id, required=True)
    account_receivable = fields.Many2one('account.account', default=_get_defualt_account_receivable_id, required=True)
    account_sales_revenue = fields.Many2one('account.account', required=True)
    tax = fields.Many2one('account.tax', string='Tax')

    @api.multi
    def create_invoices(self):

        '''
            Нэхэмжлэл үүсгэх товч дархад энэ функц ажиллана.
        '''

        inv_obj = self.env['account.invoice']
        ir_property_obj = self.env['ir.property']

        stock_inventories = self.env['stock.inventory'].browse(self._context.get('active_ids', []))
        lines = self.env['stock.inventory.line'].search([('inventory_id', '=', stock_inventories.id)])

        if self.calculations == '0':
            account_id = False
            name = _("Deficiency")
            line_values = []
            tax_ids = []

            for line in lines:
                qty = line.theoretical_qty - line.product_qty
                if qty > 0:
                    if self.tax:
                        tax_ids = self.tax.ids
                    line = [0, 0, {
                        'name': name,
                        'origin': stock_inventories.name,
                        'account_id': self.account_sales_revenue.id,
                        'price_unit': line.lst_price,
                        'quantity': qty,
                        'discount': 0.0,
                        'uom_id': line.product_id.uom_id.id,
                        'product_id': line.product_id.id,
                        'invoice_line_tax_ids': [(6, 0, tax_ids)],
                    }]
                    line_values.append(line)

        elif self.calculations == '1':
            account_id = False
            name = _("")
            line_values = []

            tax_ids = []
            for line in lines:
                qty = line.theoretical_qty - line.product_qty
                if qty != 0:
                    if self.tax:
                        tax_ids = self.tax.ids

                    if qty > 0:
                        name = _("Deficiency")
                    else:
                        name = _("Surplus")

                    line = [0, 0, {
                        'name': name,
                        'origin': stock_inventories.name,
                        'account_id': self.account_sales_revenue.id,
                        'price_unit': line.lst_price,
                        'quantity': qty,
                        'discount': 0.0,
                        'uom_id': line.product_id.uom_id.id,
                        'product_id': line.product_id.id,
                        'invoice_line_tax_ids': [(6, 0, tax_ids)],
                    }]
                    line_values.append(line)

        if not line_values:
            raise UserError(_('Disable to invoice'))
            return 0
        else:
            invoice = inv_obj.create({
                'name': stock_inventories.name,
                'origin': stock_inventories.name,
                'type': 'out_invoice',
                        'reference': False,
                        'account_id': self.account_receivable.id,
                        'partner_id': stock_inventories.location_id.partner_id.id or False,
                        'invoice_line_ids': line_values
            })

            invoice.compute_taxes()
            price = 0
            for line in self.env['account.invoice.line'].search([('invoice_id', '=', invoice.id)]):
                price = price + line.price_subtotal_signed

            if int(price) > 0:
                invoice.message_post_with_view('mail.message_origin_link',
                                               values={'self': invoice},
                                               subtype_id=self.env.ref('mail.mt_note').id)

                if self._context.get('open_invoices', False):
                    invoices = self.env['account.invoice'].search([('id', '=', invoice.id)])
                    action = self.env.ref('account.action_invoice_tree1').read()[0]
                    if len(invoices) > 1:
                        action['domain'] = [(invoice.id, 'in', invoices.ids)]
                    elif len(invoices) == 1:
                        action['views'] = [(self.env.ref('account.invoice_form').id, 'form')]
                        action['res_id'] = invoices.ids[0]
                    else:
                        action = {'type': 'ir.actions.act_window_close'}
                    return action
                return {'type': 'ir.actions.act_window_close'}
            else:
                raise UserError(_('Cant create invoice'))
                return 0
