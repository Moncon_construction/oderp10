# -*- encoding: utf-8 -*-
##############################################################################
#    
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.     
#
##############################################################################
import logging
import pytz
from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import UserError

_logger = logging.getLogger('STOCK PICKING')

class StockPickingChangeName(models.TransientModel):
    _name = 'stock.picking.change.date'
    _description = "Stock Picking Change Date"

    date_time = fields.Datetime('Change date', required=True)
        
    def action_picking_change_date(self, docids, context=None):
        ''' Агуулахын баримтын огноог засах'''
        if context is None:
            context = {}
        pickings = self.env['stock.picking'].browse(docids['active_ids'])
        move_obj = self.env['stock.move']
        pack_obj = self.env['stock.pack.operation']
        acc_move_obj = self.env['account.move']
        for picking in pickings:
            if picking.state == 'done':
                local = pytz.timezone(self.env.user.tz)
                if not picking.picking_type_id.warehouse_id.lock_move or (
                        picking.picking_type_id.warehouse_id.lock_move and not picking.picking_type_id.warehouse_id.lock_move_until ):
                    until_date = pytz.utc.localize(datetime.strptime(picking.min_date,'%Y-%m-%d %H:%M:%S')).astimezone(local).strftime('%Y-%m-%d %H:%M:%S')
                else:
                    until_date = pytz.utc.localize(datetime.strptime(picking.picking_type_id.warehouse_id.lock_move_until,'%Y-%m-%d %H:%M:%S')).astimezone(local).strftime('%Y-%m-%d %H:%M:%S')

                change_date = pytz.utc.localize(datetime.strptime(self.date_time,'%Y-%m-%d %H:%M:%S')).astimezone(local).strftime('%Y-%m-%d %H:%M:%S')

                lock_date = max(picking.company_id.period_lock_date, picking.company_id.fiscalyear_lock_date)
                if self.user_has_groups('account.group_account_manager'):
                    lock_date = picking.company_id.fiscalyear_lock_date

                if change_date <= lock_date:
                    message = _("The financial date is locked with %s and cannot be confirmed. \n"
                                "After check your date, try changing the date again") % (
                        lock_date)
                    raise UserError(message)

                if not picking.picking_type_id.warehouse_id.lock_move or (picking.picking_type_id.warehouse_id.lock_move and change_date >= until_date):
                    picking.sudo().write({'date_done': self.date_time, 'min_date': self.date_time})
                    if "account_move_id" in picking:
                        if picking.account_move_id:
                            acc_move_obj = picking.account_move_id
                            if acc_move_obj.state == 'posted':
                                acc_move_obj.button_cancel()
                                acc_move_obj.write({'date': self.date_time})
                                acc_move_obj.post()
                            else:
                                acc_move_obj.write({'date': self.date_time})
                    for move in picking.move_lines:
                        move_obj.write({'date': self.date_time, 'date_expected': self.date_time})
                    for pack in picking.pack_operation_ids:
                        pack_obj.write({'date': self.date_time})
                    
                    stock_move = self.env['stock.move'].search([('picking_id','=',picking.id)])
                    if stock_move:
                        for move in stock_move:
                            self.env.cr.execute("UPDATE stock_move SET create_date = '%s' , date = '%s' WHERE id = %s"%(self.date_time, self.date_time, move.id))
                            for line in move.quant_ids:
                                line.sudo().write({"in_date":self.date_time})
                else:
                    raise UserError(_('Sorry, this warehouse is locked until %s') % until_date)
            else:
                raise UserError(_("Зөвхөн хийгдсэн төлөвтэй үед өөрчлөх боломжтой!!"))
        return {'type': 'ir.actions.act_window_close'}
     



