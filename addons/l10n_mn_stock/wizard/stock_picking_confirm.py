# -*- encoding: utf-8 -*-
##############################################################################
#    
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
    #
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import logging
import datetime
import dateutil.parser


from odoo import api, fields, models, _
from time import time
from odoo.exceptions import UserError, ValidationError


_logger = logging.getLogger('STOCK PICKING')

class StockPickingConfirm(models.TransientModel):
    _name = 'stock.picking.confirm.verification'
    time_from = fields.Datetime('Confirm date', required=True)

    @api.multi
    def process(self, docids, context=None):
        checkSer = True
        error_description = ''
        pickings = self.env['stock.picking'].browse(docids['active_ids'])
        for picking in pickings:
            if picking.state == 'assigned':
                checkDate = dateutil.parser.parse(picking[0].min_date).date()
                checkDate1 = dateutil.parser.parse(picking.min_date).date()

                if checkDate != checkDate1:
                        checkSer = False
                        error_description = _('Choose the same date')

                if picking.picking_type_code == 'incoming':
                    if picking.picking_type_code == 'outgoing' or picking.picking_type_code == 'internal':
                        checkSer = False
                        error_description = _('Incoming transaction')

                if picking.picking_type_code == 'outgoing':
                    if picking.picking_type_code == 'incoming' or picking.picking_type_code == 'internal':
                        checkSer = False
                        error_description = _('Outgoing transaction')

                if picking.picking_type_code == 'internal':
                    if picking.picking_type_code == 'incoming' or picking.picking_type_code == 'outgoing':
                        checkSer = False
                        error_description = _('Internal transaction')

            else :
                 checkSer = False
                 error_description = _('Wrong state')

            if checkSer:

                wiz_act = picking.do_new_transfer()
                if wiz_act:
                    wiz = self.env[wiz_act['res_model']].sudo().browse(wiz_act['res_id'])
                    wiz.process()

            else :
                raise UserError(_("%s"% error_description))






