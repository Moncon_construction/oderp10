# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models
from datetime import datetime
from datetime import timedelta
from tempfile import NamedTemporaryFile
from odoo.exceptions import UserError
from odoo.tools.translate import _
import base64, os, xlrd
import logging

_logger = logging.getLogger(__name__)

class StockPickingImport(models.TransientModel):
    _name = 'stock.picking.import'
    _description = 'Stock Picking Import'

    data = fields.Binary('Data File', required=True)
    code_type = fields.Selection([
        ('barcode', 'Barcode'),
        ('default_code', 'Default Code')
    ], string='Import Code Type')

    
    @api.multi
    def import_picking(self):
        context = dict(self._context or {})

        picking_id = context['active_ids']
        picking = self.env['stock.picking'].browse(picking_id)
        if picking.move_lines:
            raise UserError(_("Inventory lines should be empty"))
        if not picking.picking_type_id:
            raise UserError(_("Please select picking type"))
        
        for form in self:
            fileobj = NamedTemporaryFile('w+')
            fileobj.write(base64.decodestring(form.data))
            fileobj.seek(0)
        
            if not os.path.isfile(fileobj.name):
                raise UserError(_("Import failed. Errorreason: The file has syntax errors"))

            book = xlrd.open_workbook(fileobj.name)  
            sheet_general = book.sheet_by_index(0)
            company_id = self.env.user.company_id.id
            nrows = sheet_general.nrows
            stock_move_dic = {}
            rowi = 1
            
            while rowi < nrows :
                row = sheet_general.row(rowi)
            
                code = row[0].value
                product_qty = row[1].value
                
                product_id = None
                
                if form.code_type == 'barcode':
                    product_id = self.env['product.product'].search([('barcode','=',str(code))])
                elif form.code_type == 'default_code':
                    product_id = self.env['product.product'].search([('default_code','=',str(code))])
                    
                if product_id:
                    if product_id[0] not in stock_move_dic.keys():
                        stock_move_dic[product_id[0]] = product_qty
                    else:
                        stock_move_dic[product_id[0]] += product_qty
                else:
                    raise UserError(_("Import failed. This product is not found: %s") % code)
                
                rowi += 1
            
            if stock_move_dic:
                for product, qty in stock_move_dic.iteritems():
                    data = {
                        'product_id': product.id,
                        'product_uom': product.uom_id.id,
                        'name': product.name,
                        'product_uom_qty': qty,
                        'company_id': company_id,
                        'location_id': picking.picking_type_id.default_location_src_id.id,
                        'location_dest_id': picking.picking_type_id.default_location_dest_id.id,
                        'picking_id': picking_id[0]
                    }
                    self.env['stock.move'].create(data)
        return True