# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from datetime import datetime, timedelta

class StockCurrentRemovalProductionLot(models.TransientModel):
    _name = 'stock.current.removal.production.lot'
    
    start_date = fields.Datetime('Start Date')
    end_date = fields.Datetime('End Date')
    time_type = fields.Selection([
                                ('removal_date', 'Removal Date'),
                                ('use_date', 'Use Date'), 
                                ('life_date', 'Life Date'), 
                                ('alert_date', 'Alert Date') 
                                ], 'Type of Time')
    warehouse_ids = fields.Many2many('stock.warehouse', string="Warehouses")
    category_ids = fields.Many2many('product.category', string="Categories")
    product_ids = fields.Many2many('product.product', string="Products")
    supplier_ids = fields.Many2many('res.partner', string='Suppliers')
        
    @api.multi
    def removal_lot_open(self):
        date_where = ""
        where = ""
        if self.time_type == 'removal_date':
            date_where = " where lot.removal_date >= '%s' and lot.removal_date <= '%s' " % (self.start_date, self.end_date)
        elif self.time_type == 'use_date':
            date_where = " where lot.use_date >= '%s' and lot.use_date <= '%s' " % (self.start_date, self.end_date)
        elif self.time_type == 'life_date':
            date_where = " where lot.life_date >= '%s' and lot.life_date <= '%s' " % (self.start_date, self.end_date)
        elif self.time_type == 'alert_date':
            date_where = " where lot.alert_date >= '%s' and lot.alert_date <= '%s' " % (self.start_date, self.end_date)
        if self.warehouse_ids:
            location_ids = self.warehouse_ids.mapped('lot_stock_id').ids
            where += " AND sloc.id in (" + ','.join(map(str, location_ids)) + ") "
        if self.category_ids:
            where += " AND pt.categ_id in (" + ','.join(map(str, self.category_ids.ids)) + ") "
        if self.supplier_ids:
            where += " AND si.name in (" + ','.join(map(str, self.supplier_ids.ids)) + ") "
            
        if self.product_ids:
            where += " AND pp.id in (" + ','.join(map(str, self.product_ids.ids)) + ") "
            
        self.env.cr.execute("""SELECT lot.id from stock_production_lot lot 
                            LEFT JOIN product_product pp ON pp.id=lot.product_id
                            LEFT JOIN product_template pt ON pt.id=pp.product_tmpl_id
                            LEFT JOIN stock_quant sq ON sq.lot_id=lot.id
                            LEFT JOIN stock_location sloc ON sq.location_id=sloc.id
                            LEFT JOIN product_category pc ON pt.categ_id = pc.id 
                            LEFT JOIN product_supplierinfo si ON pt.id = si.product_tmpl_id
                            """ + date_where + """ """ + where + """ """,)
        lot_ids = [lot['id'] for lot in self.env.cr.dictfetchall()]

        return {
                'domain': [('id', 'in', lot_ids)],
                'name': _('Removal Production Lot'),
                'view_mode': 'tree,form',
                'view_type': 'form',
                'context': {'tree_view_ref': 'stock.view_production_lot_tree'},
                'res_model': 'stock.production.lot',
                'type': 'ir.actions.act_window',
                }
    