# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError

class StockReturnPickingLine(models.TransientModel):
    _inherit = 'stock.return.picking.line'
    check_stock_picking = fields.Boolean('Check')

class StockReturnPicking(models.TransientModel):
    _inherit = 'stock.return.picking'

    @api.multi
    def create_returns(self):
        check_type = True
        for line in self:
            stock_ids = self.env['stock.return.picking.line'].search([('wizard_id', '=', line.id)])
            for stock_line in stock_ids:
                if stock_line.check_stock_picking:
                    check_type = False
        if check_type:
                new_picking_id, pick_type_id = self._create_returns()
                ctx = dict(self.env.context)
                ctx.update({
                    'search_default_picking_type_id': pick_type_id,
                    'search_default_draft': False,
                    'search_default_assigned': False,
                    'search_default_confirmed': False,
                    'search_default_ready': False,
                    'search_default_late': False,
                    'search_default_available': False,
                })
                return {
                    'name': _('Returned Picking'),
                    'view_type': 'form',
                    'view_mode': 'form,tree,calendar',
                    'res_model': 'stock.picking',
                    'res_id': new_picking_id,
                    'type': 'ir.actions.act_window',
                    'context': ctx,
                }
        else:
            stock_ids = self.env['stock.return.picking.line'].search([('wizard_id', '=', self.id)])
            for stock_line in stock_ids:
                if not stock_line.check_stock_picking:
                    stock_line.unlink()
            new_picking_id, pick_type_id = self._create_returns()
            ctx = dict(self.env.context)
            ctx.update({
                'search_default_picking_type_id': pick_type_id,
                'search_default_draft': False,
                'search_default_assigned': False,
                'search_default_confirmed': False,
                'search_default_ready': False,
                'search_default_late': False,
                'search_default_available': False,
            })
            return {
                'name': _('Returned Picking'),
                'view_type': 'form',
                'view_mode': 'form,tree,calendar',
                'res_model': 'stock.picking',
                'res_id': new_picking_id,
                'type': 'ir.actions.act_window',
                'context': ctx,
            }

    @api.model
    def default_get(self, fields):

        picking = self.env['stock.picking'].browse(self.env.context.get('active_id'))
        # шаардахаас үүссэн зардлын баримт буцаах үед хөдөлгөөний мөр дээр гологдол False байх үед ажиллах функц
        for move in picking.move_lines:
            if move.scrapped:
                move.scrapped = False
        res = super(StockReturnPicking, self).default_get(fields)
        return res
