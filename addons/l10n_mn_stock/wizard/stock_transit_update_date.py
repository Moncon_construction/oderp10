# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from lxml import etree

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class StockTransitUpdateDate(models.TransientModel):
    _name = 'stock.transit.update.date'
    _description = 'Stock Transit Update Date'

    receive_date = fields.Datetime(string='Receive Date', default=fields.Datetime.now, required=True)
    is_supplied = fields.Boolean('Supplied', default=False)
    is_received = fields.Boolean('Received', default=False)

    @api.model
    def default_get(self, fields):
        res = super(StockTransitUpdateDate, self).default_get(fields)
        data = self.get_value()
        if 'is_supplied' in fields:
            res.update({'is_supplied': data['is_supplied']})
        if 'is_received' in fields:
            res.update({'is_received': data['is_received']})
        return res

    @api.multi
    def get_value(self):
        context = self._context or {}
        is_supplied = is_received = False
        orders = self.env['stock.transit.order'].browse(context.get('active_ids', []))
        for order in orders:
            if order.is_supplied:
                is_supplied = True
            if order.is_received:
                is_received = True
        return {'is_supplied': is_supplied, 'is_received': is_received}

    @api.multi
    def update(self):
        """ Нөхөн дүүргэлтийн хүлээн авах огноо шинэчлэх
        """
        transit_id = self.env.context.get('active_id', False)
        transit = self.env['stock.transit.order'].browse(transit_id)
        if self.is_supplied and self.is_received:
            raise UserError(_('Receive date cannot update, this transit order is done.'))
        elif not self.is_supplied and self.is_received:
            raise UserError(_('Receive date cannot update, this transit order incoming shipment is done.'))
        elif (self.is_supplied and not self.is_received) or not (self.is_supplied and self.is_received):
            date = False
            for picking in transit.picking_ids:
                if picking.picking_type_id.code == 'outgoing' and picking.state == 'done' and (not date or date < picking.date):
                    date = picking.date_done
            if date > self.receive_date:
                raise UserError(_('Receive date cannot update, this transit order receive date is less than date of delivery.'))
            transit.write({'receive_date': self.receive_date})
            for picking in transit.picking_ids:
                if not (picking.picking_type_id.code == 'outgoing' and picking.state in ('done', 'cancelled')):
                    # Хүргэлтийн огноо өөрчлөх
                    picking.write({'date': self.receive_date, 'min_date': self.receive_date, 'date_done': self.receive_date})
                    for move in picking.move_lines:
                        # Агуулахын хөдөлгөөний огноо өөрчлөх
                        move.write({'date': self.receive_date})
                    for operation in picking.pack_operation_ids:
                        # Багцын огноо өөрчлөх
                        operation.write({'date': self.receive_date})
        return {'type': 'ir.actions.act_window_close'}
