# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Stock Account",
    'version': '1.0',
    'depends': [
        'l10n_mn_stock',
        'l10n_mn_account',
        'l10n_mn_product',
        'stock_account'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Stock Account Additional Features
    """,
    'data': [
        'security/security_view.xml',
        'security/ir.model.access.csv',
        'wizard/stock_immediate_transfer.xml',
        'wizard/stock_backorder_cancel.xml',
        'views/stock_menu.xml',
        'views/stock_account_views.xml',
        'views/stock_picking_view.xml',
        'views/product_info_change.xml',
        'views/product_view.xml',
        'views/account_move_line_view.xml',
        'views/stock_config_settings_views.xml',
        'views/stock_warehouse_views.xml',
        'views/stock_inventory_view.xml',
        'views/stock_backorder_confirmation_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}
