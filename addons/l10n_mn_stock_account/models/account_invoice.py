# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models

class AccountInvoice(models.Model):
    _inherit = "account.invoice"
    
    @api.onchange('partner_id', 'company_id')
    def _onchange_partner_id(self):
        # @Override: Авлага, өглөгийн дансыг агуулахаас авдаг болгов
        res = super(AccountInvoice, self)._onchange_partner_id()

        partner = self.partner_id
        company = self.company_id
        account = self.account_id
        
        if partner:
            if self.type in ('in_invoice', 'in_refund'):
                po = False
                for invoice_line in self.invoice_line_ids:
                    if invoice_line.purchase_line_id:
                        po = invoice_line.purchase_line_id.order_id
                        break
                if po:
                    payable_account = po.picking_type_id.warehouse_id.payable_account
                    account = payable_account if (company.use_wh_rec_pay_account and payable_account) else account
                    
                if not account:
                    account = partner.property_account_payable_id
                    
        self.account_id = account[0] if account else self.account_id

        return res

    inventory_id = fields.Many2one('stock.inventory', string='Stock Inventory', copy=False)
