# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, _

class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    stock_move_id = fields.Many2one('stock.move', string='Stock move', copy=False)
    inventory_id = fields.Many2one('stock.inventory', string='Stock Inventory', copy=False)
