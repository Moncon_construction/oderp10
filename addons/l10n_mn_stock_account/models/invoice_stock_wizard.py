# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError

TYPE2JOURNAL = {
    'out_invoice': 'sale',
    'in_invoice': 'purchase',
    'out_refund': 'sale',
    'in_refund': 'purchase',
}


class CreateInvoice(models.TransientModel):
    _inherit = "create.invoice.stock.wizard"

    @api.model
    def _default_journal(self):
        if self._context.get('default_journal_id', False):
            return self.env['account.journal'].browse(self._context.get('default_journal_id'))
        inv_type = self._context.get('type', 'out_invoice')
        inv_types = inv_type if isinstance(inv_type, list) else [inv_type]
        company_id = self._context.get('company_id', self.env.user.company_id.id)
        domain = [
            ('type', 'in', filter(None, map(TYPE2JOURNAL.get, inv_types))),
            ('company_id', '=', company_id),
        ]
        return self.env['account.journal'].search(domain, limit=1)
    journal_id = fields.Many2one('account.journal', string='Journal', default=_default_journal)

    @api.multi
    def create_invoices(self):
        '''
            Нэхэмжлэл үүсгэх товч дархад энэ функц ажиллана.
        '''

        inv_obj = self.env['account.invoice']
        ir_property_obj = self.env['ir.property']

        stock_inventories = self.env['stock.inventory'].browse(self._context.get('active_ids', []))
        lines = self.env['stock.inventory.line'].search([('inventory_id', '=', stock_inventories.id)])

        if self.calculations == '0':
            account_id = False
            name = _("Deficiency")
            line_values = []
            tax_ids = []

            for line in lines:
                qty = line.theoretical_qty - line.product_qty
                if qty > 0:
                    if self.tax:
                        tax_ids = self.tax.ids
                    line = [0, 0, {
                        'name': name,
                        'origin': stock_inventories.name,
                        'account_id': self.account_sales_revenue.id,
                        'price_unit': line.lst_price,
                        'quantity': qty,
                        'discount': 0.0,
                        'uom_id': line.product_id.uom_id.id,
                        'product_id': line.product_id.id,
                        'invoice_line_tax_ids': [(6, 0, tax_ids)],
                    }]
                    line_values.append(line)

        elif self.calculations == '1':
            account_id = False
            name = _("")
            line_values = []

            tax_ids = []
            for line in lines:
                qty = line.theoretical_qty - line.product_qty
                if qty != 0:
                    if self.tax:
                        tax_ids = self.tax.ids

                    if qty > 0:
                        name = _("Deficiency")
                    else:
                        name = _("Surplus")

                    line = [0, 0, {
                        'name': name,
                        'origin': stock_inventories.name,
                        'account_id': self.account_sales_revenue.id,
                        'price_unit': line.lst_price,
                        'quantity': qty,
                        'discount': 0.0,
                        'uom_id': line.product_id.uom_id.id,
                        'product_id': line.product_id.id,
                        'invoice_line_tax_ids': [(6, 0, tax_ids)],
                    }]
                    line_values.append(line)

        if not line_values:
            raise UserError(_('Disable to invoice'))
            return 0
        else:
            # Тооллогын id г дамжуулж өгнө.
            # BEGIN
            journal = self.journal_id
            currency_id = journal.currency_id or journal.company_id.currency_id or self.env.user.company_id.currency_id
            invoice = inv_obj.create({
                'name': stock_inventories.name,
                'origin': stock_inventories.name,
                'type': 'out_invoice',
                'reference': False,
                'account_id': self.account_receivable.id,
                'partner_id': stock_inventories.location_id.partner_id.id or False,
                'invoice_line_ids': line_values,
                'inventory_id': stock_inventories.id,
                'journal_id': self.journal_id.id,
                'currency_id': currency_id.id,
            })
            # END

            invoice.compute_taxes()
            price = 0
            for line in self.env['account.invoice.line'].search([('invoice_id', '=', invoice.id)]):
                price = price + line.price_subtotal_signed

            if int(price) > 0:
                invoice.message_post_with_view('mail.message_origin_link',
                                               values={'self': invoice},
                                               subtype_id=self.env.ref('mail.mt_note').id)

                if self._context.get('open_invoices', False):
                    invoices = self.env['account.invoice'].search([('id', '=', invoice.id)])
                    action = self.env.ref('account.action_invoice_tree1').read()[0]
                    if len(invoices) > 1:
                        action['domain'] = [(invoice.id, 'in', invoices.ids)]
                    elif len(invoices) == 1:
                        action['views'] = [(self.env.ref('account.invoice_form').id, 'form')]
                        action['res_id'] = invoices.ids[0]
                    else:
                        action = {'type': 'ir.actions.act_window_close'}
                    return action
                return {'type': 'ir.actions.act_window_close'}
            else:
                raise UserError(_('Cant create invoice'))
                return 0