# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2016 Asterisk Technologies LLC Co.,ltd
#    (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462
#
##############################################################################

from odoo import api, fields, models, _ 

import odoo.addons.decimal_precision as dp
from odoo.exceptions import UserError

class ProductCategory(models.Model):
    _inherit = "product.category"
    
    @api.model
    def default_get(self, fields):
        rec = super(ProductCategory, self).default_get(fields)
        rec.update({'property_valuation': 'real_time'})
        rec.update({'property_cost_method': 'average'})
        return rec
    