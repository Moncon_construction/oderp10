# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, _

class ProductInfoChange(models.TransientModel):
    _inherit='product.info.change'
    
    property_account_creditor_price_difference = fields.Many2one(
        'account.account', string="Price Difference Account", company_dependent=True,
        help="This account will be used to value price difference between purchase price and cost price.")    
    property_account_expense_id = fields.Many2one('account.account', company_dependent=True,
        string="Expense Account", oldname="property_account_expense",
        domain=[('deprecated', '=', False)],
        help="This account will be used for invoices instead of the default one to value expenses for the current product.")
    property_stock_account_input = fields.Many2one(
        'account.account', 'Stock Input Account',
        company_dependent=True, domain=[('deprecated', '=', False)],
        help="When doing real-time inventory valuation, counterpart journal items for all incoming stock moves will be posted in this account, unless "
             "there is a specific valuation account set on the source location. When not set on the product, the one from the product category is used.")
    property_stock_account_output = fields.Many2one(
        'account.account', 'Stock Output Account',
        company_dependent=True, domain=[('deprecated', '=', False)],
        help="When doing real-time inventory valuation, counterpart journal items for all outgoing stock moves will be posted in this account, unless "
             "there is a specific valuation account set on the destination location. When not set on the product, the one from the product category is used.")
    property_account_income_id = fields.Many2one('account.account', company_dependent=True,
        string="Income Account", oldname="property_account_income",
        domain=[('deprecated', '=', False)],
        help="This account will be used for invoices instead of the default one to value sales for the current product.")
    @api.multi
    def product_info_change(self):
        context = dict(self._context or {})
        product_ids = self.env['product.template'].browse(context.get('active_ids'))
        
        if self.property_account_creditor_price_difference:
            for a in product_ids:    
                a.property_account_creditor_price_difference = self.property_account_creditor_price_difference
        if self.property_account_expense_id:
            for a in product_ids:
                a.property_account_expense_id = self.property_account_expense_id
        if self.property_stock_account_input:
            for a in product_ids:
                a.property_stock_account_input = self.property_stock_account_input
        if self.property_stock_account_output:
            for a in product_ids:
                a.property_stock_account_output = self.property_stock_account_output
        if self.property_account_income_id:
            for a in product_ids:
                a.property_account_income_id = self.property_account_income_id
        
        return super(ProductInfoChange, self).product_info_change()