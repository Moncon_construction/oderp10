from odoo import fields, models


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    property_stock_account_input = fields.Many2one(track_visibility='onchange')
    property_stock_account_output = fields.Many2one(track_visibility='onchange')
