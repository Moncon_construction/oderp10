# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class ResCompany(models.Model):
    _inherit = 'res.company'
    
    use_wh_rec_pay_account = fields.Boolean(string="Use Warehouse's Account", default=False)
    transit_incoming_picking_confirm = fields.Selection([
        ('all_out_done', "When all out picking done"),
        ('some_out_done', "When some out picking done"),
        ('outgoing_done', "Incoming after outgoing done automatically")
    ], "Transit incoming picking confirm", default='all_out_done')
