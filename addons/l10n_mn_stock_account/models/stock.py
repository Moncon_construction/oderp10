# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import logging
from collections import defaultdict
from datetime import datetime

import pytz

from odoo import _, api, fields, models
from odoo.exceptions import UserError
from odoo.exceptions import ValidationError
import odoo.addons.decimal_precision as dp
from odoo.addons.l10n_mn_base.models.tools import *
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.tools import float_compare, float_is_zero

_logger = logging.getLogger(__name__)


class StockWarehouse(models.Model):
    _inherit = "stock.warehouse"

    receivable_account = fields.Many2one('account.account', string="Receivable Account")
    payable_account = fields.Many2one('account.account', string="Payable Account")
    expense_account_id = fields.Many2one('account.account', string="Expense Account")


class StockPicking(models.Model):
    _inherit = "stock.picking"

    account_move_id = fields.Many2one('account.move', string='Move', copy=False)
    is_close_back_order = fields.Boolean('Is Close Back Order')

    @api.multi
    def button_journal_entries(self):
        return {
            'name': _('Journal Entries'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'account.move',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'res_id': self.account_move_id.id
        }

    @api.multi
    def to_expense_backorder(self):
        context = dict(self._context or {})
        context['picking_id'] = self.id
        return {
            'name': 'Note',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'stock.backorder.cancel',
            'context': context,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
        }

    def reconfirm(self):
        todo = []
        for pick in self:
            '''
            if pick.transit_order_id and pick.picking_type_id.code == 'incoming':
                if pick.create_incoming_return:
                    if len(pick.return_picking_ids) > 0:
                        raise UserError(_('Warning !'), _('There is created return picking. You have delete return picking!'))
            '''
            for move in pick.move_lines:
                todo.append(move)
            if pick.is_close_back_order:
                pick.is_close_back_order = False
                if pick.account_move_id:
                    pick.account_move_id.button_cancel()
                    pick.account_move_id.unlink()

            # Хэрэв нэхэмжлэл устаагүй байвал борлуулалтын захиалга төлөвт орно.
            # Үгүй бол нэхэмжлэлийн сондгойрол төлөлвт орж дахин нэхэмжлэл үүсгэх боломжтой болно.
            '''
            sale_id = sale_order_obj.search([('name','=',pick.origin)])
            if sale_id:
                if sale_id[0].invoice_ids:
                    sale_id.write({'state': 'progress'})
                else :
                    sale_id.write({'state': 'invoice_except'})
            '''
        if todo:
            for t in todo:
                t.write({'state': 'draft'})
        self.state = 'draft'
        self.message_post(body='Cancelled picking has re-confirmed.')
        return self.action_confirm()

    def check_backorder_by_move(self):
        for move in self.move_lines:
            if float_compare(move.remaining_qty, 0, precision_rounding=move.product_id.uom_id.rounding) != 0:
                return True
        return False

    @api.multi
    def do_new_transfer(self):
        user = self.env['res.users'].browse(self._uid)
        operations = self.env['stock.pack.operation']
        has_pack_op = False
        for pick in self:
            """ Агуулах -> Тохиргоо -> "Агуулахын хөдөлгөөнийг зөвшөөрөгдсөн хэрэглэгч батлана" сонгосон үед
                батлахыг зөвшөөрсөн агуулахын хөдөлгөөн байвал батална.
            """
            if pick.company_id.stock_move_approved_allowed_user:
                if pick.picking_type_id.warehouse_id.id not in user.approved_warehouses.ids:
                    raise ValidationError(_("You do not have permission to approve the warehouse. Please, Contact your system administrator!"))

            if not pick.pack_operation_product_ids and not pick.pack_operation_pack_ids and not pick.pack_operation_ids:
                raise UserError(_("Hasn't operations! Reset or create operations."))
            ''' Нөхөн дүүргэлтийн гарах бараа гараагүй \n
                байхад ирэх барааг хүлээн авахыг хориглоно.
            '''
            if pick.location_id and pick.location_id.usage == 'transit':
                assigned_product_ids = pick.mapped('move_lines').filtered(lambda x: x.state == 'assigned').mapped('product_id')
                if assigned_product_ids:
                    out_picking_ids = self.env['stock.picking'].search([('id', '!=', pick.id), ('transit_order_id', '=', pick.transit_order_id.id), ('state', '!=', 'done'), ('location_dest_id', '=', pick.location_id.id)])
                    all_out_picking_ids = self.env['stock.picking'].search([('id', '!=', pick.id), ('transit_order_id', '=', pick.transit_order_id.id), ('location_dest_id', '=', pick.location_id.id)])
                    if pick.company_id.transit_incoming_picking_confirm == 'all_out_done':
                        if out_picking_ids:
                            raise UserError(
                                _("Replenishment's OUT picking is not done yet! Confirm OUT pickings first."))
                    else:
                        if len(out_picking_ids) == len(all_out_picking_ids):
                            raise UserError(_("Replenishment's OUT picking is not done yet! Confirm OUT pickings first."))
            """
                Xэрвээ цувралын дугаартай бол нөхөн шивэлтийн огноо авах
            """
            if pick.state == 'assigned' and pick.pack_operation_ids:
                # Check backorder should check for other barcodes
                temp = False
                for opr in pick.pack_operation_ids:
                    if opr.qty_done != 0:
                        temp = True
                if pick.company_id.availability_compute_method == 'stock_move' and \
                        self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_stock_account_create_move'), ('state', 'in', ('installed', 'to upgrade'))]):
                    check_backorder = pick.check_backorder_by_move()
                else:
                    check_backorder = pick.check_backorder()
                if check_backorder and temp:
                    view = self.env.ref('stock.view_backorder_confirmation')
                    wiz = self.env['stock.backorder.confirmation'].create({'pick_id': pick.id})
                    # TDE FIXME: same reamrk as above actually
                    return {
                        'name': _('Create Backorder?'),
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'stock.backorder.confirmation',
                        'views': [(view.id, 'form')],
                        'view_id': view.id,
                        'target': 'new',
                        'res_id': wiz.id,
                        'context': self.env.context,
                    }

                view = self.env.ref('stock.view_immediate_transfer')
                wiz = self.env['stock.immediate.transfer'].create({'pick_id': pick.id})
                has_pack_op = True
                imme_dict = {
                    'name': _('Immediate Transfer?'),
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'stock.immediate.transfer',
                    'views': [(view.id, 'form')],
                    'view_id': view.id,
                    'target': 'new',
                    'res_id': wiz.id,
                    'context': self.env.context
                }
                return imme_dict
            # FIXME: Ийшээ орохгүй байгаа тул нөхөн шивэлтийн огноог дутагдлын дэлгэцэнд харуулж сайжруулах шаардлагатай.
            if has_pack_op:
                imme = self.env['stock.immediate.transfer'].search([('pick_id', '=', pick.id)], limit=1)
                # Агуулахын хөдөлгөөний огноо
                move = self.env['stock.move'].search([('picking_id', '=', pick.id)])
                if imme:
                    move.write({'state': 'done',
                                'date': imme.force_date or datetime.now()})
                # Хүргэлтийн огноо
                pick.write({'date_done': imme.force_date or datetime.now()})
                # Багцын огноо
                operation = operations.search([('picking_id', '=', move.picking_id.id)])
                if operation:
                    operation.write({'date': imme.force_date or datetime.now()})
                # Журналын бичилтын огноо
                account_move_lines = self.account_line_ids
                if account_move_lines:
                    account_move_lines[0].move_id.write({'date': imme.force_date or datetime.now()})
                    # Аналитик бичилтийн огноо
                    for account_move_line in account_move_lines:
                        account_move_line.analytic_line_ids.write({'date': imme.force_date or datetime.now()})
                has_pack_op = False
        return super(StockPicking, self).do_new_transfer()

    @api.multi
    def create_group_line(self):
        # Журнал бичилтийг нэгтгэх
        for picking in self:
            if picking.move_lines:
                account_move_lines = self.env['account.move.line'].search([('stock_move_id', 'in', picking.move_lines.ids)])
                account_moves = account_move_lines.mapped('move_id')
                if account_moves:
                    picking.account_move_id = account_moves[0]
                    tobe_delete_account_moves = account_moves - account_moves[0]
                    if tobe_delete_account_moves:
                        self.env.cr.execute('''UPDATE account_move_line SET move_id = %s WHERE move_id IN %s''', (account_moves[0].id, tuple(tobe_delete_account_moves.ids)))
                        self.env.cr.execute('''DELETE FROM account_move WHERE id IN %s''', (tuple(tobe_delete_account_moves.ids),))
                if 'related_sale_order_id' in picking and picking.related_sale_order_id and picking.related_sale_order_id.warehouse_id:
                    if 'stock_account_output_id' in picking.related_sale_order_id.warehouse_id and picking.related_sale_order_id.warehouse_id.stock_account_output_id:
                        account_move_lines = account_move_lines.filtered(lambda l: l.account_id == picking.related_sale_order_id.warehouse_id.stock_account_output_id)
                    else:
                        account_ids = picking.related_sale_order_id.order_line.mapped('product_id.categ_id.property_stock_account_output_categ_id').ids
                        account_move_lines = account_move_lines.filtered(lambda l: l.account_id.id in account_ids)
                    moves_to_repost = self.env['account.move']
                    for line in account_move_lines:
                        order_line = picking.related_sale_order_id.order_line.filtered(
                            lambda l: l.product_id == line.product_id)
                        if order_line:
                            if len(order_line) > 1:
                                order_line = order_line[0]
                            analytic_account_id = order_line.sudo().analytic_account_id.id
                            if analytic_account_id:
                                self.env.cr.execute('''UPDATE account_move_line
                                                       SET analytic_account_id = ''' + str(analytic_account_id) + '''
                                                       WHERE id = ''' + str(line.id))
                                moves_to_repost |= line.move_id
                    moves_to_repost.button_cancel()
                    moves_to_repost.post()

    @api.multi
    def do_transfer(self):
        res = super(StockPicking, self).do_transfer()
        self.create_group_line()
        return res

    @api.multi
    def write(self, vals):
        if vals.get('min_date'):
            self.mapped('move_lines').filtered(lambda sm: sm.state not in ('done', 'assigned')).write({'date': vals['min_date']})
        return super(StockPicking, self).write(vals)

class StockMoveAccountsDict(models.Model):
    _name = "stock.move.accounts.dict"
    
    stock_move_id = fields.Many2one('stock.move', string='Stock Move', required=True, ondelete="cascade")
    account_id = fields.Many2one('account.account', string='Account', required=True)
    amount = fields.Float(string='Amount', required=True)

class StockMove(models.Model):
    _inherit = "stock.move"

    @api.depends('state')
    def _get_journal_entry_cnt(self):
        for move in self:
            move.update({
                'journal_entry_count': len(move.account_line_ids),
                'symbol': 'none'
                # 'symbol': 'journal' if len(line_ids) > 0 else 'none'
            })

    journal_entry_count = fields.Integer(string='# of Journal Entries', compute='_get_journal_entry_cnt', readonly=True)
    symbol = fields.Selection([('none', '#'),
                               ('journal', '$')], string='Symbol', default='none', compute=_get_journal_entry_cnt, store=True)
    account_line_ids = fields.One2many('account.move.line', "stock_move_id", string='Account Move Lines', readonly=True, copy=False)
    analytic_account_id = fields.Many2one('account.analytic.account', string='Analytic Account', readonly=True)
    is_same_product = fields.Boolean(string='Is Same Product', default=False)  # Ижил бараатай мөрийг тэмдэглэх
    accounts_dict = fields.One2many('stock.move.accounts.dict', "stock_move_id", string='Account Move Account Dictionary', readonly=True, copy=False)

    @api.multi
    def button_journal_entries(self):
        return {
            'name': _('Journal Entry Lines'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move.line',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': [('stock_move_id', '=', self.id)],
        }

    def is_module_installed(self, module_name):                 # Тухайн модуль суусан эсэхийг шалгах
        self._cr.execute(
            "SELECT id FROM ir_module_module WHERE name = '%s' AND state IN ('installed', 'to upgrade')" % module_name)
        results = self._cr.dictfetchall()
        if results and len(results) > 0:
            return True
        else:
            return False

    def enter_account_entry_move(self):
        """ Accounting Valuation Entries """
        for move in self:
            if move.procurement_id:
                if move.procurement_id.transit_line_id:
                    return False

            if move.product_id.type != 'product' or move.product_id.valuation != 'real_time':
                # no stock valuation for consumable products
                return False

            location_from = move.location_id
            location_to = move.location_dest_id  # TDE FIXME: as the accounting is based on this value, should probably check all location_to to be the same
            company_from = location_from.usage == 'internal' and location_from.company_id or False
            company_to = location_to and (location_to.usage == 'internal') and location_to.company_id or False

            # Create Journal Entry for products arriving in the company; in case of routes making the link between several
            # warehouse of the same company, the transit location belongs to this company, so we don't need to create accounting entries
            if hasattr(move.picking_id, 'expense') and move.picking_id.expense:
                account_id = False
                for line in move.picking_id.expense.expense_line:
                    if line.product.id == move.product_id.id:
                        account_id = line.account.id
                if move.picking_id.picking_type_id.code != 'incoming':
                    journal_id, acc_src, acc_dest, acc_valuation = move._get_accounting_data_for_valuation()
                    acc_dest = account_id.id
                    move.with_context(force_company=company_from.id)._create_account_move_line(acc_valuation, acc_dest, journal_id)
                else:
                    journal_id, acc_src, acc_valuation, acc_dest = move._get_accounting_data_for_valuation()
                    acc_valuation = account_id.id
                    move.with_context(force_company=company_to.id)._create_account_move_line(move, acc_valuation, acc_dest, journal_id)
            else:
                # ############ BEGIN CHANGE ##########################
                # Компани дундаа өртөгтэй үед нөхөн дүүргэлт хийхэд журналын бичилт үүсдэггүй болгосон
                if not move.is_module_installed("l10n_mn_stock_account_cost_for_each_wh") and location_from.usage in ('internal', 'transit') \
                        and location_to.usage in ('internal', 'transit') and location_from.company_id == location_to.company_id:
                    return False
                # ############ END CHANGE ############################
                if company_to and (move.location_id.usage not in ('internal', 'transit') and move.location_dest_id.usage == 'internal' or company_from != company_to):
                    journal_id, acc_src, acc_dest, acc_valuation = move._get_accounting_data_for_valuation()
                    if location_from and location_from.usage == 'customer':  # goods returned from customer
                        move.with_context(force_company=company_to.id)._create_account_move_line(acc_dest, acc_valuation, journal_id)
                    else:
                        move.with_context(force_company=company_to.id)._create_account_move_line(acc_src, acc_valuation, journal_id)
                # Create Journal Entry for products leaving the company
                if company_from and (move.location_id.usage == 'internal' and move.location_dest_id.usage not in ('internal', 'transit') or company_from != company_to):
                    journal_id, acc_src, acc_dest, acc_valuation = move._get_accounting_data_for_valuation()
                    if location_to and location_to.usage == 'supplier':  # goods returned to supplier
                        move.with_context(force_company=company_from.id)._create_account_move_line(acc_valuation, acc_src, journal_id)
                    else:
                        move.with_context(force_company=company_from.id)._create_account_move_line(acc_valuation, acc_dest, journal_id)

                if move.company_id.anglo_saxon_accounting and move.location_id.usage == 'supplier' and move.location_dest_id.usage == 'customer':
                    # Creates an account entry from stock_input to stock_output on a dropship move. https://github.com/odoo/odoo/issues/12687
                    journal_id, acc_src, acc_dest, acc_valuation = move._get_accounting_data_for_valuation()
                    move.with_context(force_company=move.company_id.id)._create_account_move_line(acc_src, acc_dest, journal_id)

    @api.multi
    def create_account_move_line(self):
        context = self._context
        ids = context.get('active_ids')
        method = self.env.user.company_id.availability_compute_method
        if self.id:
            if self.journal_entry_count == 0:
                self.enter_account_entry_move()
                if method == 'stock_quant' and self.quant_ids:
                    self.product_price_update_after_done()
            else:
                raise UserError(_("Account move lines are already created. %s" % self.name))
            return True
        elif ids:
            journaled_moves = []
            moves = self.env['stock.move'].browse(ids)
            for move in moves:
                if move.journal_entry_count == 0:
                    move.enter_account_entry_move()
                    if method == 'stock_quant' and move.quant_ids:
                        move.product_price_update_after_done()
                else:
                    journaled_moves.append(move)
            if len(journaled_moves) > 0:
                _logger.warning(_(u"Warning :: Account move lines are already created at belown moves.") + (u"\n::%s" % (", ".join(move.name for move in journaled_moves))))
            return []

    def _create_account_move_line(self, credit_account_id, debit_account_id, journal_id):
        # Энэ функцийг stock.quant-с stock.move рүү оруулав
        # group quants by cost
        for move in self:
            AccountMove = self.env['account.move']
            move_lines = move._prepare_account_move_line(move.product_qty, move.price_unit, credit_account_id, debit_account_id)
            if move_lines:
                date = self._context.get('force_period_date', fields.Date.context_today(self))
                if not move.picking_id.account_move_id:
                    new_account_move = AccountMove.create({
                        'journal_id': journal_id,
                        'line_ids': move_lines,
                        'date': move.date,
                        'ref': move.picking_id.name})
                    new_account_move.post()
                    move.picking_id.write({'account_move_id': new_account_move.id})
                    #  Шинэ үүссэн ажил гүйлгээ нь тооллогоос дамжиж үүсэж байвал тооллоготой холбож өгнө.
                    if not move.picking_id and move.inventory_id:
                        move.inventory_id.write({'account_move_id': new_account_move.id})
                else:
                    move.picking_id.account_move_id.write({'line_ids': move_lines})

    @api.multi
    def _get_accounting_data_for_valuation(self):
        journal_id, acc_src, acc_dest, acc_valuation = super(StockMove, self)._get_accounting_data_for_valuation()
        if self.inventory_id and self.inventory_id.is_initial:
            acc_src = self.company_id.initial_config_account_id.id
        return journal_id, acc_src, acc_dest, acc_valuation

    def _prepare_account_move_line(self, qty, cost, credit_account_id, debit_account_id):
        """
        Generate the account.move.line values to post to track the stock valuation difference due to the
        processing of the given quant.
        """
        self.ensure_one()
        inventory_id = False
        if self._context.get('force_valuation_amount'):
            valuation_amount = self._context.get('force_valuation_amount')
        else:
            if self.product_id.cost_method == 'average':
                # ############ BEGIN CHANGE ##########################
                # Зарлагын өртөг үнийг функцээс авдаг болгож сольсон
                valuation_amount = cost or self.get_product_standard_price()
                # ############ END CHANGE ############################
            else:
                # ############ BEGIN CHANGE ##########################
                # Зарлагын өртөг үнийг функцээс авдаг болгож сольсон
                valuation_amount = cost if self.product_id.cost_method == 'real' else self.get_product_standard_price()
                # ############ END CHANGE ############################
        # the standard_price of the product may be in another decimal precision, or not compatible with the coinage of
        # the company currency... so we need to use round() before creating the accounting entries.
        debit_value = self.company_id.currency_id.round(valuation_amount * qty)
        # check that all data is correct
        if self.company_id.currency_id.is_zero(debit_value):
            if self.product_id.cost_method == 'standard':
                raise UserError(_("The found valuation amount for product %s is zero. Which means there is probably a configuration error. Check the costing method and the standard price") % (self.product_id.name,))
            return []
        credit_value = debit_value

        if self.product_id.cost_method == 'average' and self.company_id.anglo_saxon_accounting:
            # in case of a supplier return in anglo saxon mode, for products in average costing method, the stock_input
            # account books the real purchase price, while the stock account books the average price. The difference is
            # booked in the dedicated price difference account.
            if self.location_dest_id.usage == 'supplier' and self.origin_returned_move_id and self.origin_returned_move_id.purchase_line_id:
                debit_value = self.origin_returned_move_id.price_unit * qty
            # in case of a customer return in anglo saxon mode, for products in average costing method, the stock valuation
            # is made using the original average price to negate the delivery effect.
            if self.location_id.usage == 'customer' and self.origin_returned_move_id:
                debit_value = self.origin_returned_move_id.price_unit * qty
                credit_value = debit_value
        
        partner_id = (self.picking_id.partner_id and self.picking_id.partner_id.id) or self.env['res.partner']._find_accounting_partner(self.picking_id.partner_id).id or False
        
        # Шинжилгээний данс шалгах
        debit_analytic_account_id = False
        credit_analytic_account_id = False
        db_account = self.env['account.account'].browse(debit_account_id)
        ct_account = self.env['account.account'].browse(credit_account_id)
        
        if db_account.req_analytic_account and self.analytic_account_id:
            debit_analytic_account_id = self.analytic_account_id.id
        if ct_account.req_analytic_account and self.analytic_account_id:
            credit_analytic_account_id = self.analytic_account_id.id
        
        debit_require_partner = True if db_account.reconcile else False
        credit_require_partner = True if ct_account.reconcile else False
        
        debit_line_vals = {
            'name': self.name,
            'product_id': self.product_id.id,
            'quantity': qty,
            'product_uom_id': self.product_id.uom_id.id,
            'ref': self.picking_id.name,
            'partner_id': partner_id if debit_require_partner else None,
            'debit': debit_value if debit_value > 0 else 0,
            'credit': -debit_value if debit_value < 0 else 0,
            'account_id': debit_account_id,
            'analytic_account_id': debit_analytic_account_id
        }
        credit_line_vals = {
            'name': self.name,
            'product_id': self.product_id.id,
            'quantity': qty,
            'product_uom_id': self.product_id.uom_id.id,
            'ref': self.picking_id.name,
            'partner_id': partner_id if credit_require_partner else None,
            'credit': credit_value if credit_value > 0 else 0,
            'debit': -credit_value if credit_value < 0 else 0,
            'account_id': credit_account_id,
            'analytic_account_id': credit_analytic_account_id
        }

        # ############### BEGIN CHANGE ######################################
        # Хэрвээ тооллогоос үүссэн ажил гүйлгээний мөр бол ажил гүйлгээний мөр дээр тоолллогыг холбоно.
        if self.inventory_id:
            inventory_id = self.inventory_id.id
        # Ажил гүйлгээний мөрийн өгөгдөлд stock_move_id талбарт утга оноох
        debit_line_vals.update({
            'stock_move_id': self.id,
            'inventory_id': inventory_id
        })
        credit_line_vals.update({
            'stock_move_id': self.id,
            'inventory_id': inventory_id
        })
        # ############### END CHANGE ########################################
        res = [(0, 0, debit_line_vals), (0, 0, credit_line_vals)]
        if credit_value != debit_value:
            # for supplier returns of product in average costing method, in anglo saxon mode
            diff_amount = debit_value - credit_value
            price_diff_account = self.product_id.property_account_creditor_price_difference
            if not price_diff_account:
                price_diff_account = self.product_id.categ_id.property_account_creditor_price_difference_categ
            if not price_diff_account:
                raise UserError(_('Configuration error. Please configure the price difference account on the product or its category to process this operation.'))
            price_diff_line = {
                'name': self.name,
                'product_id': self.product_id.id,
                'quantity': qty,
                'product_uom_id': self.product_id.uom_id.id,
                'ref': self.picking_id.name,
                'partner_id': partner_id,
                'credit': diff_amount > 0 and diff_amount or 0,
                'debit': diff_amount < 0 and -diff_amount or 0,
                'account_id': price_diff_account.id,
                # ############### BEGIN CHANGE ######################################
                # Ажил гүйлгээний мөрийн өгөгдөлд stock_move_id талбарт утга оноох
                'stock_move_id': self.id,
                'inventory_id': inventory_id
                # ############### END CHANGE ########################################
            }
            res.append((0, 0, price_diff_line))
        return res

    @api.multi
    def action_done(self):
        #  Агуулахын хөдөлгөөний огноог өөрчлөн орлого авахад тухайн огноог дамжуулах
        res = super(StockMove, self.with_context(date=self.env.context.get('force_date'))).action_done()
        '''
            Тухайн хөдөлгөөн батлахад сонгосон огноог өгөх, энэ нь нөхөн шивэлт хийхэд ашиглагдана.
            1. Агуулахын хөдөлгөөний огноо dуюу stock move
            2. Багцын огноо буюу pack operation
            3. Журналын бичилтын огноо буюу account move
            4. Хүргэлтийн огноо буюу stock picking г.м
        '''
        operations = self.env['stock.pack.operation']
        description = ''
        for move in self:
            if move.picking_id:
                force_date = self.env['stock.immediate.transfer'].search([('pick_id', '=', move.picking_id.id)], order="id desc", limit=1).force_date
                if not force_date:
                    force_date = self.env['stock.backorder.confirmation'].search([('pick_id', '=', move.picking_id.id)], limit=1).force_date
                # Агуулахын хөдөлгөөний огноо
                if force_date:
                    move.write({'state': 'done',
                                'date': force_date or datetime.now()})
                # Хүргэлтийн огноо
                move.picking_id.write({'date_done': force_date or datetime.now()})
                # Багцын огноо
                operation = operations.search([('picking_id', '=', move.picking_id.id)])
                if operation:
                    operation.write({'date': force_date or datetime.now()})
                # Журналын бичилтын огноо
                account_move_lines = move.account_line_ids

                # Цагийн зөрүүтэй баазад хадгалагдсан огноог буцааж хэрэглэгчийн цагийн бүсийнхээр харуулах
                update_date = False
                if force_date:
                    tz = get_user_timezone(self.env.user)
                    date_time_obj = pytz.utc.localize(datetime.strptime(move.date, '%Y-%m-%d %H:%M:%S')).astimezone(tz)
                    update_date = str(date_time_obj.date())
                if move.purchase_line_id and move.purchase_line_id.order_id:
                    if move.purchase_line_id.order_id.notes:
                        description = move.purchase_line_id.order_id.notes
                    else:
                        description = move.purchase_line_id.order_id.name
                if account_move_lines:
                    self._cr.execute('''UPDATE account_move
                                        SET description = \'''' + str(description) + '''\'
                                        ,date = \'''' + (update_date or datetime.now().strftime('%Y-%m-%d')) + '''\'
                                        WHERE id = ''' + str(account_move_lines[0].move_id.id) + '''
                                     ''')

                    for account_move_line in account_move_lines:
                        #  Журналын мөрийн огноо
                        self._cr.execute('''UPDATE account_move_line
                                            SET date = \'''' + (update_date or datetime.now().strftime('%Y-%m-%d')) + '''\'
                                            WHERE id = ''' + str(account_move_line.id) + '''
                                            ''')
                        # Аналитик бичилтийн огноо
                        account_move_line.analytic_line_ids.write({'date': update_date or datetime.now()})
        return res
            
    def _store_average_cost_price(self):
        """ Store the average price of the move on the move and product form (costing method 'real')"""
        for move in self.filtered(lambda move: move.product_id.cost_method == 'real'):
            # product_obj = self.pool.get('product.product')
            if any(q.qty <= 0 for q in move.quant_ids) or move.product_qty == 0:
                # if there is a negative quant, the standard price shouldn't be updated
                continue
            # Note: here we can't store a quant.cost directly as we may have moved out 2 units
            # (1 unit to 5€ and 1 unit to 7€) and in case of a product return of 1 unit, we can't
            # know which of the 2 costs has to be used (5€ or 7€?). So at that time, thanks to the
            # average valuation price we are storing we will valuate it at 6€
            valuation_price = sum(q.qty * q.cost for q in move.quant_ids)
            average_valuation_price = valuation_price / move.product_qty

            move.product_id.with_context(force_company=move.company_id.id).sudo().write({'standard_price': average_valuation_price})
            move.write({'price_unit': average_valuation_price})

        for move in self.filtered(lambda move: move.product_id.cost_method != 'real' and not move.origin_returned_move_id):
            # Unit price of the move should be the current standard price, taking into account
            # price fluctuations due to products received between move creation (e.g. at SO
            # confirmation) and move set to done (delivery completed).
            if move.is_zero_inventory and move.inventory_id:
                if move.location_dest_id.usage == 'inventory' and move.product_id.cost_method == 'average':
                    move.write({'price_unit': move.get_price_unit()})
                else:
                    #Тооллогоор 0-лэж тоолсон тохиолдолд move-д get_price_unit функциэр оноосон өртгийг хэвээр үлдээнэ
                    pass
            else:
                move.write({'price_unit': move.product_id.standard_price})

    @api.multi
    def get_price_unit(self):
        for obj in self:
            if obj.is_zero_inventory and obj.inventory_id:
                #Хэрэв тооллогоор 0 гэж тоолсон бол тухайн барааны өртөг бас 0 болох ёстой. Тооллогын дараах тайлан дээр бас өртгийн үлдэгдэл 0-лэж харагдах ёстой. 
                #Үүний тулд тооллогын дараа тухайн барааны нийт өртөг 0 болгохын тулд хөдөлгөөний өртгийг тохируулж бичнэ.
                
                if obj.env.user.company_id.availability_compute_method == 'stock_move' and not obj.is_module_installed("l10n_mn_stock_account_cost_for_each_wh"): 
                    #Компани даяар өртөг хөтөлж байгаа, stock.move-р үлдэгдлээ тооцож байгаа тохиолдолд 
                    #(бусад тохиолдлыг аль нэг захиалагч дээр шаардлага гарвал гүйцээж бичиж өгвөл зохино. 
                    #Энэ нь өртөг угаахгүйгээр бүх бараагаа тооллогоор 0-лэж байгаад буцааж тооллогоор орлого авах замаар өртгөө засахаар шийдсэн захиалагчид тохиромжтой)
                    qty_dp_digit = self.env['decimal.precision'].precision_get('Product Unit of Measure') or 2
                    location_ids = obj.location_id.id if obj.location_id.usage == 'internal' else obj.location_dest_id.id
                    qry = """
                        SELECT  ROUND(COALESCE(SUM(table1.in_qty) - SUM(table1.out_qty))::DECIMAL, %s) AS lasts_qty,
                                ROUND(COALESCE(SUM(table1.in_cost) - SUM(table1.out_cost))::DECIMAL, %s) AS lasts_cost
                        FROM
                        (
                            SELECT 
                                /* Тухайн байрлалд орж ирсэн хөдөлгөөн буюу орлогын тоо хэмжээ */
                                CASE WHEN m.location_dest_id IN (%s) AND m.state = 'done' 
                                    THEN COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0) ELSE 0 END AS in_qty,
                                /* Тухайн байрлалд орж ирсэн хөдөлгөөн буюу орлогын өртөг */
                                CASE WHEN m.location_dest_id IN (%s) AND m.state = 'done'
                                    THEN m.product_uom_qty*m.price_unit ELSE 0 END AS in_cost,
                                    
                                /* Тухайн байрлалаас гарсан хөдөлгөөн буюу зарлагын тоо хэмжээ */
                                CASE WHEN m.location_id IN (%s) AND m.state = 'done' 
                                    THEN COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0) ELSE 0 END AS out_qty,
                                /* Тухайн байрлалаас гарсан хөдөлгөөн буюу зарлагын өртөг */
                                CASE WHEN m.location_id IN (%s) AND m.state = 'done' 
                                    THEN m.product_uom_qty*m.price_unit ELSE 0 END AS out_cost
                            FROM stock_location l_id, stock_location l_dest_id, stock_move m    
                            LEFT JOIN product_product pp ON (pp.id = m.product_id)
                            LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id)
                            LEFT JOIN product_uom uom_m ON (uom_m.id = m.product_uom)
                            LEFT JOIN product_uom uom_t ON (uom_t.id = pt.uom_id)
                            WHERE ((m.location_id IN (%s) AND l_dest_id.usage != 'transit') OR (m.location_dest_id IN (%s) AND l_id.usage != 'transit')) 
                            AND m.location_id != m.location_dest_id
                            AND m.location_id = l_id.id AND m.location_dest_id = l_dest_id.id AND m.state = 'done' and m.date <= '%s' AND pp.id = %s AND m.id != %s 
                        ) table1
                    """ % (qty_dp_digit, qty_dp_digit, location_ids, location_ids, location_ids, location_ids, location_ids, location_ids, obj.date, obj.product_id.id, obj.id)        
                    self._cr.execute(qry)
                    results = self._cr.dictfetchall()
                    
                    if results and len(results) > 0 and results[0]['lasts_qty']:
                        price_unit = results[0]['lasts_cost']/results[0]['lasts_qty']
                        obj.write({'price_unit': price_unit})
                        return price_unit
                    else:
                        return 0
                
        return super(StockMove, self).get_price_unit()

    @api.multi
    def product_price_update_before_done(self):
        tmpl_dict = defaultdict(lambda: 0.0)
        # adapt standard price on incomming moves if the product cost_method is 'average'
        std_price_update = {}
        product_tot_qty_available = 0
        total_price = total_qty = 0
        
        for move in self.filtered(lambda move: move.location_id.usage in ('supplier', 'production', 'inventory') and move.product_id.cost_method == 'average'):
            # ############ BEGIN OF CHANGE #########################
            #  Агуулахын хөдөлгөөний огноог өөрчлөн орлого авахад тухайн огноогоорхи дундаж өртгийг тооцож барааны үнийн түүх үрүү бичнэ.
            if move.company_id.availability_compute_method == 'stock_quant':
                self.env.cr.execute("SELECT SUM(r.quantity) AS quantity, SUM(r.quantity * r.price_unit_on_quant) AS total_cost FROM stock_history r "
                    "LEFT JOIN stock_location sl ON sl.id = r.location_id "
                    "WHERE r.date <= %s AND sl.usage = 'internal' AND r.product_id = %s  ", (move.date, move.product_id.id))
            elif move.company_id.availability_compute_method == 'stock_move':
                self.env.cr.execute("""SELECT sum(t.product_qty) as quantity FROM 
                    (SELECT sum(product_qty)::decimal(16,2) AS product_qty FROM stock_move m, stock_location l, stock_location l1, product_product p 
                            WHERE l.id=m.location_dest_id and l.usage='internal' and l1.id=m.location_id and p.id=m.product_id and p.id=%s and m.date <= %s and
                                l1.usage in ('internal','inventory','supplier','customer','production','transit') and m.state = 'done' 
                    UNION SELECT sum(product_qty)::decimal(16,2)*(-1) AS product_qty FROM stock_move m, stock_location l, stock_location l1, product_product p 
                            WHERE l.id=m.location_id and l.usage='internal' and l1.id=m.location_dest_id and p.id=m.product_id and p.id=%s and m.date <= %s and 
                                l1.usage in ('internal','inventory','supplier','customer','production','transit') and m.state = 'done') as t""", (move.product_id.id, move.date, move.product_id.id, move.date))
                
            history = self.env.cr.dictfetchall()
            if history and history[0]:
                product_tot_qty_available = history[0]['quantity']
            # if the incoming move is for a purchase order with foreign currency, need to call this to get the same value that the quant will use.
            if product_tot_qty_available <= 0:
                # Хэрвээ худалдан авалтаас орлого орж байгаа тохиолдолд ижил бараа 2 мөр орсон байвал тухайн барааны өртгийг тооцож байна.
                if move.purchase_line_id:
                    moves = self.env['stock.move'].search([('product_id', '=', move.product_id.id),('picking_id', '=', move.picking_id.id),('id', '!=', move.id)])
                    if moves:
                        for move1 in moves:
                            total_price += move1.get_price_unit() * move1.product_qty
                            total_qty += move1.product_qty
                            move1.is_same_product = True
                        new_std_price = total_price / total_qty
                    else:
                        new_std_price = move.get_price_unit()
                else:
                    new_std_price = move.get_price_unit()
                move.product_id.with_context(force_company=move.company_id.id, force_date=move.date).sudo().write(
                    {'standard_price': new_std_price})
            else:
                amount_unit = move.product_id.standard_price
                if move.is_same_product == False:
                    if move.purchase_line_id:
                        moves = self.env['stock.move'].search(
                            [('product_id', '=', move.product_id.id), ('picking_id', '=', move.picking_id.id),
                             ('id', '!=', move.id)])
                        product_tot_qty = amount_unit * product_tot_qty_available
                        '''Ижил бараатай орлогын мөрийг тэмдэглэж эхний мөрөөр өртгийг шинэчилсэн учир дараагийн мөрөөр дахин 
                            шинэчлэхгүй гэсэн санаагаар ижил бараа эсэх талбарыг чеклэж дахин тооцоолол хийхгүй байхаар тооцсон.'''
                        if moves:
                            for move1 in moves:
                                total_price += move1.get_price_unit() * move1.product_qty
                                total_qty += move1.product_qty
                                move1.write({'is_same_product': True})
                            new_std_price = (product_tot_qty + total_price + (move.get_price_unit() * move.product_qty)) / (product_tot_qty_available + total_qty + move.product_qty)
                        else:
                            new_std_price = ((amount_unit * product_tot_qty_available) + (
                                        move.get_price_unit() * move.product_qty)) / (
                                                        product_tot_qty_available + move.product_qty)
                    else:
                        new_std_price = ((amount_unit * product_tot_qty_available) + (
                                    move.get_price_unit() * move.product_qty)) / (
                                                    product_tot_qty_available + move.product_qty)
                    move.product_id.with_context(force_company=move.company_id.id, force_date=move.date).sudo().write(
                        {'standard_price': new_std_price})
            # ############ END OF CHANGE #########################

            tmpl_dict[move.product_id.id] += move.product_qty

    @api.multi
    def get_initial_balance(self, product, date_from, locations, company_id, type):
        # Хугацааны хооронд тооцоолж байгаа тохиолдолд эхний үлдэгдлийг олно
        select = ''
        group_by = ''
        if type == 'location':
            select = ', l.lid'
            group_by = ', l.lid'
        date = get_display_day_to_user_day('%s 00:00:00' % str(date_from), self.env.user)
        self.env.cr.execute("SELECT SUM(l.cost) AS cost , SUM(l.qty) AS qty " + select + " "
                            "FROM ( SELECT m.product_id AS prod_id, "
                                           "CASE WHEN m.location_id NOT IN (" + locations + ") AND m.location_dest_id IN (" + locations + ") "
                                           "THEN COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0) "
                                           "WHEN m.location_id IN (" + locations + ") AND m.location_dest_id NOT IN (" + locations + ") "
                                           "THEN -COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0) ELSE 0 END AS qty, "
                                           "CASE WHEN m.location_id NOT IN (" + locations + ") AND m.location_dest_id IN (" + locations + ") "
                                           "THEN COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0, 0) * COALESCE(m.price_unit, 0) "
                                           "WHEN m.location_id IN (" + locations + ") AND m.location_dest_id NOT IN (" + locations + ") "
                                           "THEN -COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor,0) * COALESCE(m.price_unit, 0) ELSE 0 END AS cost, "
                                           "CASE WHEN m.location_id NOT IN (" + locations + ") AND m.location_dest_id IN (" + locations + ") "
                                           "THEN m.location_dest_id "
                                           "WHEN m.location_id IN (" + locations + ") AND m.location_dest_id NOT IN (" + locations + ") "
                                           "THEN m.location_id ELSE 0 END AS lid "                                                                                          
                                    "FROM stock_move m "
                                    "LEFT JOIN product_product pp ON (pp.id = m.product_id) "
                                    "LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id) "                                                                                                
                                    "LEFT JOIN product_uom uom_m ON (uom_m.id = m.product_uom) "
                                    "LEFT JOIN product_uom uom_t ON (uom_t.id = pt.uom_id) "                                                                                                 
                                    "WHERE m.date < '" + str(date) + "' AND m.state = 'done' AND m.company_id = " + str(company_id.id) + " "
                                            "AND m.product_id = " + str(product) + ") AS l "
                            "GROUP BY l.prod_id " + group_by + " ")
        return self.env.cr.dictfetchall()

    @api.multi
    def create_account_move_by_qry(self):
        # Журнал бичилтийг quant-с хамааралгүйгээр stock.move-с query ашиглан үүсгэх функц
        values = []
        uid = self.env.uid
        date = fields.Datetime.now()
        description = ''
        
        for move_id in self:
            if round(move_id.product_uom_qty * move_id.price_unit, move_id.company_id.currency_id.decimal_places or 2) == 0:
                continue
            date1 = get_day_like_display(move_id.date, self.env.user)
            company_id = move_id.company_id.id
        
            # Журнал бичилт үүсгэх
            journal_id, acc_src, acc_dest, acc_valuation = move_id._get_accounting_data_for_valuation()
            partner_id = move_id.partner_id.id if move_id.partner_id else 'NULL'
            analytic_account_id = move_id.analytic_account_id.id if move_id.analytic_account_id else 'NULL'
            move_name = False
            if not journal_id:
                raise ValidationError(_("Please check journal !!!"))
            journal_id = self.env['account.journal'].browse(journal_id)
            if journal_id.sequence_id:
                move_name = journal_id.sequence_id.with_context(ir_sequence_date=move_id.date[:10]).next_by_id()
            if not move_name:
                raise ValidationError(_("Please check sequence of journal %s !!!") % journal_id.name)
            if move_id.purchase_line_id and move_id.purchase_line_id.order_id:
                if move_id.purchase_line_id.order_id.notes:
                    description = move_id.purchase_line_id.order_id.notes
                else:
                    description = move_id.purchase_line_id.order_id.name
            self._cr.execute("""
                INSERT INTO account_move (name, date, state, ref, journal_id, company_id, description) 
                VALUES ('%s', '%s', 'posted', '%s', %s, %s, '%s')
                RETURNING id
            """ % (move_name, date1, move_id.name, journal_id[0].id, company_id, description))
            
            # Журнал бичилтийн мөр /account.move.line/ үүсгэх өгөгдөл бэлдэх
            account_move_id = self._cr.fetchall()[0][0]
            if move_id.picking_id:
                move_id.picking_id.write({'account_move_id': account_move_id})
            if move_id.location_id.usage == 'internal':
                acc_debit, acc_credit = acc_dest, acc_valuation
            else:
                acc_debit, acc_credit = acc_valuation, acc_src
                
            aml_values = move_id.with_context({'force_valuation_amount': move_id.price_unit})._prepare_account_move_line(move_id.product_uom_qty, move_id.price_unit, acc_credit, acc_debit)
            for aml_value in aml_values:
                aml_value = aml_value[2]
                values.append("""(
                    %s, '%s', %s, '%s',
                    %s, %s, %s, %s, %s, %s,
                    %s, %s, %s, %s, 
                    '%s', '%s', '%s', '%s', %s,
                    %s, %s
                )""" % (
                    uid, date, uid, date,
                    company_id, account_move_id, journal_id[0].id, aml_value['account_id'], aml_value.get('partner_id', 'NULL') or 'NULL', aml_value.get('analytic_account_id', 'NULL') or 'NULL',
                    move_id.id, aml_value.get('product_id', 'NULL') or 'NULL', aml_value.get('product_uom_id', 'NULL') or 'NULL', aml_value.get('quantity', 'NULL'),
                    get_qry_value(move_id.name), get_qry_value(move_id.product_id.name), date1, date1, move_id.inventory_id.id or 'NULL',
                    aml_value.get('debit', 0), aml_value.get('credit', 0)
                ))
                
        if values:
            # Журнал бичилтийн мөр үүсгэх
            qry = """
                INSERT INTO account_move_line (
                    create_uid, create_date, write_uid, write_date,
                    company_id, move_id, journal_id, account_id, partner_id, analytic_account_id,
                    stock_move_id, product_id, product_uom_id, quantity,
                    name, ref, date, date_maturity, inventory_id,
                    debit, credit
                ) VALUES 
            """
            qry += ", ".join(value for value in values)
            qry += " RETURNING id"
            self._cr.execute(qry)
            
            # Шинжилгээний бичилт үүсгэх
            results = self.env.cr.fetchall()
            aml_ids = [str(result[0]) for result in results]
            if aml_ids:
                qry = """
                    INSERT INTO account_analytic_line(create_uid, create_date, write_uid, write_date, account_id, company_id, amount, unit_amount, partner_id, name, ref, general_account_id, move_id, product_id, product_uom_id, date, currency_id, amount_currency)
                        SELECT %s, '%s', %s, '%s', analytic_account_id AS account_id, aml.company_id AS company_id,
                            aml.credit - aml.debit AS amount, aml.quantity AS unit_amount,
                            aml.partner_id AS partner_id, aml.name AS name, aml.ref AS ref, aml.account_id AS general_account_id, aml.id AS move_id, aml.product_id AS product_id,
                            aml.product_uom_id AS product_uom_id, aml.date AS date,
                            aml.currency_id AS currency_id, aml.amount_currency AS amount_currency
                        FROM account_move_line aml
                        LEFT JOIN account_analytic_line aal ON aml.id = aal.move_id
                        WHERE aml.analytic_account_id IS NOT NULL AND aal.id IS NULL AND aml.id IN (%s)
                        ORDER BY aml.id
                """ % (uid, date, uid, date, ",".join(aml_ids))
                self._cr.execute(qry)
                