from odoo import models, fields, api, _
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from dateutil.relativedelta import relativedelta


class StockBackorderConfirmation(models.TransientModel):
    _inherit = 'stock.backorder.confirmation'

    def _is_assign_date_now(self):
        if self.pick_id.company_id.assign_date == 'now':
            self.is_assign_date_now = True
        else:
            self.is_assign_date_now = False

    force_date = fields.Datetime(
        'Force Date', default=lambda self: fields.Datetime.now())
    is_assign_date_now = fields.Boolean(compute=_is_assign_date_now, string='Is assign date now?')

    # Overrides function to add force date
    @api.one
    def _process(self, cancel_backorder=False):
        operations_to_delete = self.pick_id.pack_operation_ids.filtered(
            lambda o: o.qty_done <= 0)
        for pack in self.pick_id.pack_operation_ids - operations_to_delete:
            pack.product_qty = pack.qty_done
        operations_to_delete.unlink()
        # override starts here
        # self.pick_id.do_transfer() --> self.with_context(force_date=self.force_date).pick_id.do_transfer()
        self.with_context(force_date=self.force_date).pick_id.do_transfer()
        # override ends here
        if cancel_backorder:
            backorder_pick = self.env['stock.picking'].search(
                [('backorder_id', '=', self.pick_id.id)])
            backorder_pick.action_cancel()
            self.pick_id.message_post(
                body=_("Back order <em>%s</em> <b>cancelled</b>.") % (backorder_pick.name))

        if not self.pick_id.picking_type_id.warehouse_id.lock_move or (self.pick_id.picking_type_id.warehouse_id.lock_move and self.force_date >= self.pick_id.picking_type_id.warehouse_id.lock_move_until):
            if self.pick_id.company_id.transit_incoming_picking_confirm == 'outgoing_done':
                if self.pick_id and self.pick_id.transit_order_id and self.pick_id.picking_type_id and self.pick_id.picking_type_id.code == 'outgoing':
                    incoming_picking_ids = self.env['stock.picking'].search(
                        [('transit_order_id', '=', self.pick_id.transit_order_id.id),
                         ('picking_type_id.code', '=', 'incoming'), ('state', '!=', 'cancel')])
                    filtered_incoming_picking_ids = self.env['stock.picking']
                    if self.pick_id.move_lines.mapped('product_id'):
                        for out_picking_id in incoming_picking_ids:
                            if out_picking_id.move_lines.mapped('product_id') and set(self.pick_id.move_lines.mapped('product_id') & out_picking_id.move_lines.mapped('product_id')):
                                filtered_incoming_picking_ids |= out_picking_id
                    if filtered_incoming_picking_ids:
                        filtered_incoming_picking_ids.filtered(lambda x: x.state == 'draft').action_confirm()
                        filtered_incoming_picking_ids.filtered(lambda x: x.state in ('confirmed', 'partially_available', 'assigned')).force_assign()
                        for out_picking_id in filtered_incoming_picking_ids:
                            for p in out_picking_id.pack_operation_product_ids:
                                for outp in self.pick_id.pack_operation_product_ids:
                                    if p.product_id == outp.product_id:
                                        p.qty_done = outp.qty_done
                            context = {"active_model": "stock.picking", "active_ids": [out_picking_id.ids], "active_id": out_picking_id.id}
                            if date:
                                date = datetime.strptime(self.force_date, DEFAULT_SERVER_DATETIME_FORMAT) + relativedelta(seconds=1)
                            else:
                                date = datetime.strptime(self.min_date, DEFAULT_SERVER_DATETIME_FORMAT) + relativedelta(seconds=1)
                            transfer_wizard = self.env['stock.backorder.confirmation'].with_context(context).create({'pick_id': out_picking_id.id, 'force_date': date})
                            transfer_wizard.process()
