# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class StockConfigSettings(models.TransientModel):
    _inherit = 'stock.config.settings'

    module_l10n_mn_stock_account_cost_for_each_wh = fields.Selection([
        (0, 'Control product cost for each company'),
        (1, 'Control product cost for each warehouse')], "Cost controlling",
        help="""Install the module that allows to store product cost for each warehouse.""")
    
    use_wh_rec_pay_account = fields.Boolean(related="company_id.use_wh_rec_pay_account", default=lambda self: self.env.user.company_id.use_wh_rec_pay_account, string="Use Warehouse's Account")
    transit_incoming_picking_confirm = fields.Selection([
        ('all_out_done', "When all out picking done"),
        ('some_out_done', "When some out picking done"),
        ('outgoing_done', "Incoming after outgoing done automatically")
    ], "Transit incoming picking confirm", default='all_out_done', related="company_id.transit_incoming_picking_confirm")