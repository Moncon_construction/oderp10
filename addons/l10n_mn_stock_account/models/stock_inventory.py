# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError

TYPE2JOURNAL = {
    'out_invoice': 'sale',
    'in_invoice': 'purchase',
    'out_refund': 'sale',
    'in_refund': 'purchase',
}

class StockInverntory(models.Model):
    _inherit = 'stock.inventory'
    
    account_move_line_ids = fields.One2many('account.move.line', 'inventory_id', 'Account Entry', readonly=True, copy=False)
    
    @api.multi
    def button_journal_entries(self):
        return {
            'name': _('Journal Entries'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move.line',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', self.account_move_line_ids.ids)],
        }

    @api.depends('state')
    def _get_invoiced(self):
        for inventory in self:
            # id гаар хайдаг болгов.
            # BEGIN
            invoice_ids = inventory.line_ids.mapped('invoice_lines').mapped('invoice_id').filtered(lambda r: r.type in ['out_invoice', 'out_refund'])
            refunds = invoice_ids.search([('inventory_id', '=', inventory.id)]).filtered(lambda r: r.type in ['out_invoice', 'out_refund'])
            invoice_ids |= refunds.filtered(lambda r: inventory in r.inventory_id)
            refund_ids = self.env['account.invoice'].browse()
            if invoice_ids:
                for inv in invoice_ids:
                    refund_ids += refund_ids.search([('type', '=', 'out_refund'), ('inventory_id', '=',inv.id), ('origin', '!=', False), ('journal_id', '=', inv.journal_id.id)])
            # END
            inventory.update({
                'invoice_count': len(set(invoice_ids.ids + refund_ids.ids)),
                'invoice_ids': invoice_ids.ids + refund_ids.ids,
            })
