# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from collections import defaultdict
from datetime import datetime

from odoo import models, fields, api
from odoo.addons.l10n_mn_web.models.time_helper import *


class StockQuant(models.Model):
    _inherit = "stock.quant"

    def is_module_installed(self, module_name):                 # Тухайн модуль суусан эсэхийг шалгах
        self._cr.execute(
            "SELECT id FROM ir_module_module WHERE name = '%s' AND state IN ('installed', 'to upgrade')" % module_name)
        results = self._cr.dictfetchall()
        if results and len(results) > 0:
            return True
        else:
            return False

    def _account_entry_move(self, move):
        """ Accounting Valuation Entries """
        if move.procurement_id:
            if move.procurement_id.transit_line_id:
                return False

        if move.product_id.type != 'product' or move.product_id.valuation != 'real_time':
            # no stock valuation for consumable products
            return False
        if any(quant.owner_id or quant.qty <= 0 for quant in self):
            # if the quant isn't owned by the company, we don't make any valuation en
            # we don't make any stock valuation for negative quants because the valuation is already made for the counterpart.
            # At that time the valuation will be made at the product cost price and afterward there will be new accounting entries
            # to make the adjustments when we know the real cost price.
            return False

        location_from = move.location_id
        location_to = self[0].location_id  # TDE FIXME: as the accounting is based on this value, should probably check all location_to to be the same
        company_from = location_from.usage == 'internal' and location_from.company_id or False
        company_to = location_to and (location_to.usage == 'internal') and location_to.company_id or False

        # Create Journal Entry for products arriving in the company; in case of routes making the link between several
        # warehouse of the same company, the transit location belongs to this company, so we don't need to create accounting entries

        # ############ BEGIN CHANGE ##########################
        # Компани дундаа өртөгтэй үед нөхөн дүүргэлт хийхэд журналын бичилт үүсдэггүй болгосон
        if not self.is_module_installed("l10n_mn_stock_account_cost_for_each_wh") and move.location_id.usage in ('internal', 'transit') \
                and move.location_dest_id.usage in ('internal', 'transit') and move.location_id.company_id == move.location_dest_id.company_id:
            return False
        # ############ END CHANGE ############################

        if hasattr(move.picking_id, 'expense') and move.picking_id.expense:
            account_id = False
            for line in move.picking_id.expense.expense_line:
                if line.product.id == move.product_id.id:
                    account_id = line.account.id
            if move.picking_id.picking_type_id.code != 'incoming':
                journal_id, acc_src, acc_dest, acc_valuation = move._get_accounting_data_for_valuation()
                acc_dest = account_id.id
                self.with_context(force_company=company_from.id)._create_account_move_line(move, acc_valuation, acc_dest, journal_id)
            else:
                journal_id, acc_src, acc_valuation, acc_dest = move._get_accounting_data_for_valuation()
                acc_valuation = account_id.id
                self.with_context(force_company=company_to.id)._create_account_move_line(move, acc_valuation, acc_dest, journal_id)
        else:
            if company_to and (move.location_id.usage not in ('internal', 'transit') and move.location_dest_id.usage == 'internal' or company_from != company_to):
                journal_id, acc_src, acc_dest, acc_valuation = move._get_accounting_data_for_valuation()
                if location_from and location_from.usage == 'customer':  # goods returned from customer
                    self.with_context(force_company=company_to.id)._create_account_move_line(move, acc_dest,
                                                                                             acc_valuation, journal_id)
                else:
                    self.with_context(force_company=company_to.id)._create_account_move_line(move, acc_src,
                                                                                             acc_valuation, journal_id)
            # Create Journal Entry for products leaving the company
            if company_from and (move.location_id.usage == 'internal' and move.location_dest_id.usage not in ('internal', 'transit') or company_from != company_to):
                journal_id, acc_src, acc_dest, acc_valuation = move._get_accounting_data_for_valuation()
                if location_to and location_to.usage == 'supplier':  # goods returned to supplier
                    self.with_context(force_company=company_from.id)._create_account_move_line(move, acc_valuation, acc_src, journal_id)
                else:
                    self.with_context(force_company=company_from.id)._create_account_move_line(move, acc_valuation, acc_dest, journal_id)

            if move.company_id.anglo_saxon_accounting and move.location_id.usage == 'supplier' and move.location_dest_id.usage == 'customer':
                # Creates an account entry from stock_input to stock_output on a dropship move. https://github.com/odoo/odoo/issues/12687
                journal_id, acc_src, acc_dest, acc_valuation = move._get_accounting_data_for_valuation()
                self.with_context(force_company=move.company_id.id)._create_account_move_line(move, acc_src, acc_dest, journal_id)

    def _create_account_move_line(self, move, credit_account_id, debit_account_id, journal_id):
        AccountMove = self.env['account.move']
        # Цуврал хөтлөдөг эсэхийг шалгахдаа өмнө нь Transient моделоос шалгасан байсныг засав.
        non_tracking_product_quants = self.filtered(lambda self: self.product_id.tracking != 'lot')
        tracking_product_quants = self.filtered(lambda self: self.product_id.tracking == 'lot')
        if non_tracking_product_quants:
            # Цуврал хөтөлдөггүй бол stock.move-н price_unit-р ажил гүйлгээг үүсгэнэ. Энэ сонголтыг Od ERP дээр цуврал хөтөлдөггүй харилцагчид зориулж нэмж хийж өгсөн
            move_lines = []
            for obj in non_tracking_product_quants:
                # Журналын бичилтийн мөр дэх тоо хэмжээг quant-с авдаг болгов
                # Үгүй бол 1 move-н тоо хэмжээг 2 салгаж quant үүсгэж буй үед тухайн 2 quant-с үүссэн журналын бичилт буруу дүнтэй үүсч байсан
                if move.accounts_dict:
                    for acc_line in move.accounts_dict:
                        move_lines.extend(move._prepare_account_move_line(obj.qty, acc_line.amount, acc_line.account_id.id, debit_account_id))
                    
                    #БМ дансны дб бичилтийн мөрүүдийг нэгтгэх
                    db_line = None
                    for line in move_lines:
                        if line[2]['account_id'] == debit_account_id:
                            if not db_line:
                                db_line = line
                            else:
                                db_line[2]['debit'] += line[2]['debit']
                                
                            move_lines.remove(line)
                    if db_line:
                        move_lines.append((0, 0, db_line[2]))
                else:
                    move_lines.extend(move._prepare_account_move_line(obj.qty, move.price_unit, credit_account_id, debit_account_id))
            if move_lines:
                date = move.date or self._context.get('force_period_date', fields.Date.context_today(self))
                if not move.picking_id.account_move_id:
                    date = get_day_like_display(date, self.env.user)
                    new_account_move = AccountMove.create({
                        'journal_id': journal_id,
                        'line_ids': move_lines,
                        'date': date,
                        'ref': move.picking_id.name})
                    new_account_move.post()
                    move.picking_id.write({'account_move_id': new_account_move.id})
                    #  Шинэ үүссэн ажил гүйлгээ нь тооллогоос дамжиж үүсэж байвал тооллоготой холбож өгнө.
                    if not move.picking_id and move.inventory_id:
                        move.inventory_id.write({'account_move_id': new_account_move.id})
                else:
                    company_id = self[0].company_id and self[0].company_id.id or self.env.user.company_id.id
                    user_id = move and move.create_uid and move.create_uid.id or self.env.user.id
                    all_creates = True
                    for line in move_lines:
                        if line[0] != 0 or line[1] != 0:
                            all_creates = False
                    if all_creates:
                        total = move.picking_id.account_move_id and move.picking_id.account_move_id.amount or 0
                        for line in move_lines:
                            select = ''
                            value = ''
                            if line[2].get('analytic_2nd_account_id', False):
                                select = '''
                                    analytic_2nd_account_id,
                                '''
                                value = str(line[2]['analytic_2nd_account_id']) + ','
                            self._cr.execute('''
                                INSERT INTO
                                    account_move_line(
                                        create_uid,
                                        write_uid,
                                        journal_id,
                                        date,
                                        register_date,
                                        account_id,
                                        analytic_account_id,
                                        ''' + select + '''
                                        credit,
                                        debit,
                                        inventory_id,
                                        name,
                                        partner_id,
                                        product_id,
                                        product_uom_id,
                                        quantity,
                                        ref,
                                        stock_move_id,
                                        move_id,
                                        date_maturity,
                                        company_id
                                    )
                                VALUES(
                                    ''' + str(user_id) + ''',
                                    ''' + str(user_id) + ''',
                                    ''' + str(journal_id) + ''',
                                    \'''' + date + '''\',
                                    \'''' + date + '''\',
                                    ''' + str(line[2]['account_id']) + ''',
                                    ''' + (str(line[2]['analytic_account_id']) if line[2]['analytic_account_id'] else 'NULL') + ''',
                                    ''' + value + '''
                                    ''' + str(line[2]['credit']) + ''',
                                    ''' + str(line[2]['debit']) + ''',
                                    ''' + (str(line[2]['inventory_id']) if line[2]['inventory_id'] else 'NULL') + ''',
                                    \'''' + str(line[2]['name']).replace('\'', '\'\'') + '''\',
                                    ''' + (str(line[2]['partner_id']) if line[2]['partner_id'] else 'NULL') + ''',
                                    ''' + (str(line[2]['product_id']) if line[2]['product_id'] else 'NULL') + ''',
                                    ''' + (str(line[2]['product_uom_id']) if line[2]['product_uom_id'] else 'NULL') + ''',
                                    ''' + (str(line[2]['quantity']) if line[2]['quantity'] else 'NULL') + ''',
                                    \'''' + str(line[2]['ref']) + '''\',
                                    ''' + (str(line[2]['stock_move_id']) if line[2]['stock_move_id'] else 'NULL') + ''',
                                    ''' + str(move.picking_id.account_move_id.id) + ''',
                                    \'''' + datetime.now().strftime('%Y-%m-%d') + '''\',
                                    ''' + str(company_id) + '''
                                )
                                RETURNING id
                            ''')
                            total += line[2]['debit']
                            newly_added_move_line_id = self.env.cr.fetchone()[0]
                            # Шинжилгээний тархалт харуулах байвал журналын бичилт дээр шинжилгээний тархалт нэмж байна
                            if 'show_analytic_share' in self.env.user.company_id and self.env.user.company_id.show_analytic_share:
                                if line[2].get('analytic_share_ids', False):
                                    for share in line[2]['analytic_share_ids']:
                                        self._cr.execute('''
                                            INSERT INTO
                                                account_analytic_share(
                                                    create_uid,
                                                    write_uid,
                                                    create_date,
                                                    write_date,
                                                    move_line_id,
                                                    analytic_account_id,
                                                    rate)
                                                values(
                                                    ''' + str(user_id) + ''',
                                                    ''' + str(user_id) + ''',
                                                    \'''' + str(fields.Datetime.now()) + '''\',
                                                    \'''' + str(fields.Datetime.now()) + '''\',
                                                    ''' + str(newly_added_move_line_id) + ''',
                                                    ''' + str(share[2]['analytic_account_id']) + ''',
                                                    ''' + str(share[2]['rate']) + ''')
                                        ''')
                                if line[2].get('analytic_share_ids2', False):
                                    for share in line[2]['analytic_share_ids2']:
                                        self._cr.execute('''
                                            INSERT INTO
                                                account_analytic_share(
                                                    create_uid,
                                                    write_uid,
                                                    create_date,
                                                    write_date,
                                                    move_line_id2,
                                                    analytic_account_id,
                                                    rate)
                                                values(
                                                    ''' + str(user_id) + ''',
                                                    ''' + str(user_id) + ''',
                                                    \'''' + str(fields.Datetime.now()) + '''\',
                                                    \'''' + str(fields.Datetime.now()) + '''\',
                                                    ''' + str(newly_added_move_line_id) + ''',
                                                    ''' + str(share[2]['analytic_account_id']) + ''',
                                                    ''' + str(share[2]['rate']) + ''')
                                        ''')
                                # Нэмсэн шинжилгээний тархалттай холбоотой шинжилгээний бичилт үүсгэж байна
                                self.env['account.move.line'].browse(newly_added_move_line_id).create_analytic_lines()
                            else:
                                # Тохиргооноос хамааран үүсдэг болгов
                                # Шинжилгээний бичилт үүсгэх хэсэг. Журналын мөрийг insert query бичин оруулсан тул шинжилгээний бичилт үүсгэх функц нь ажиллахгүй байгаа тул нэмэв
                                if line[2]['analytic_account_id']:
                                    move_line_ids = self.env['account.move.line'].search(
                                        [('move_id', '=', move.picking_id.account_move_id.id),
                                         ('analytic_account_id', '=', line[2]['analytic_account_id']),
                                         ('product_id', '=', line[2]['product_id'])], order='id desc', limit=1)
                                    if move_line_ids:
                                        move_line_id = move_line_ids.ids[0]
                                        amount = (line[2]['credit'] or 0.0) - (line[2]['debit'] or 0.0)
                                        self._cr.execute('''INSERT INTO
                                                                account_analytic_line(
                                                                    create_uid,
                                                                    write_uid,
                                                                    name,
                                                                    date,
                                                                    account_id,
                                                                    unit_amount,
                                                                    product_id,
                                                                    product_uom_id,
                                                                    amount,
                                                                    general_account_id,
                                                                    ref,
                                                                    move_id,
                                                                    company_id
                                                                    )
                                                                VALUES(
                                                                    ''' + str(user_id) + ''',
                                                                    ''' + str(user_id) + ''',
                                                                    \'''' + str(line[2]['name']).replace('\'', '\'\'') + '''\',
                                                                    \'''' + date + '''\',
                                                                    ''' + str(line[2]['analytic_account_id']) + ''',
                                                                    ''' + (str(line[2]['quantity']) if line[2]['quantity'] else 'NULL') + ''',
                                                                    ''' + (str(line[2]['product_id']) if line[2]['product_id'] else 'NULL') + ''',
                                                                    ''' + (str(line[2]['product_uom_id']) if line[2]['product_uom_id'] else 'NULL') + ''',
                                                                    ''' + str(amount) + ''',
                                                                    ''' + str(line[2]['account_id']) + ''',
                                                                    \'''' + str(line[2]['ref']) + '''\',
                                                                    ''' + str(move_line_id) + ''',
                                                                    ''' + str(company_id) + '''
                                                                )
                                                            ''')
                                if line[2].get('analytic_2nd_account_id', False):
                                    move_line_ids = self.env['account.move.line'].search(
                                        [('move_id', '=', move.picking_id.account_move_id.id),
                                         ('analytic_2nd_account_id', '=', line[2]['analytic_2nd_account_id']),
                                         ('product_id', '=', line[2]['product_id'])], order='id desc', limit=1)
                                    if move_line_ids:
                                        move_line_id = move_line_ids.ids[0]
                                        amount = (line[2]['credit'] or 0.0) - (line[2]['debit'] or 0.0)
                                        self._cr.execute('''INSERT INTO
                                                                account_analytic_line(
                                                                    create_uid,
                                                                    write_uid,
                                                                    name,
                                                                    date,
                                                                    account_id,
                                                                    unit_amount,
                                                                    product_id,
                                                                    product_uom_id,
                                                                    amount,
                                                                    general_account_id,
                                                                    ref,
                                                                    move_id,
                                                                    company_id
                                                                    )
                                                                VALUES(
                                                                    ''' + str(user_id) + ''',
                                                                    ''' + str(user_id) + ''',
                                                                    \'''' + str(line[2]['name']).replace('\'', '\'\'') + '''\',
                                                                    \'''' + date + '''\',
                                                                    ''' + str(line[2]['analytic_2nd_account_id']) + ''',
                                                                    ''' + (str(line[2]['quantity']) if line[2]['quantity'] else 'NULL') + ''',
                                                                    ''' + (str(line[2]['product_id']) if line[2]['product_id'] else 'NULL') + ''',
                                                                    ''' + (str(line[2]['product_uom_id']) if line[2]['product_uom_id'] else 'NULL') + ''',
                                                                    ''' + str(amount) + ''',
                                                                    ''' + str(line[2]['account_id']) + ''',
                                                                    \'''' + str(line[2]['ref']) + '''\',
                                                                    ''' + str(move_line_id) + ''',
                                                                    ''' + str(company_id) + '''
                                                                )
                                                            ''')
                        if move.picking_id.account_move_id:
                            move.picking_id.account_move_id.write({'amount': total})
                    else:
                        move.picking_id.account_move_id.write({'line_ids': move_lines})

        if tracking_product_quants:
            # Цуврал хөтөлдөг бол stock.quant-н cost-р ажил гүйлгээг үүсгэнэ. Энэ нь default хувилбар нь
            quant_cost_qty = defaultdict(lambda: 0.0)
            for quant in tracking_product_quants:
                quant_cost_qty[quant.cost] += quant.qty
            for cost, qty in quant_cost_qty.iteritems():
                move_lines = move._prepare_account_move_line(qty, cost, credit_account_id, debit_account_id)
                if move_lines:
                    date = self._context.get('force_period_date', fields.Date.context_today(self))
                    new_account_move = AccountMove.create({
                        'journal_id': journal_id,
                        'line_ids': move_lines,
                        'date': date,
                        'ref': move.picking_id.name})
                    new_account_move.post()
