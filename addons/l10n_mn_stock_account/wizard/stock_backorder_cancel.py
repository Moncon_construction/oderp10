# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import UserError, Warning


class StockBackorderCancel(models.TransientModel):
    _name = 'stock.backorder.cancel'

    picking_id = fields.Many2one('stock.picking', string='Picking')
    account_id = fields.Many2one('account.account', string='Expense Account', required=True, domain="[('journal_entries_selectable','=',False)]")

    @api.model
    def default_get(self, fields):
        result = super(StockBackorderCancel, self).default_get(fields)
        if self._context.get('picking_id'):
            result.update({
                'picking_id': self._context['picking_id']
            })
        return result

    @api.multi
    def cancel_button(self):
        for obj in self:
            line_ids = []
            for move in obj.picking_id.move_lines:
                cost = move.product_qty * move.price_unit
                partner_id = (obj.picking_id.partner_id and self.env['res.partner']._find_accounting_partner(
                    obj.picking_id.partner_id).id) or False
                date = self._context.get('force_period_date', fields.Date.context_today(self))
                journal_id, acc_src, acc_dest, acc_valuation = move._get_accounting_data_for_valuation()
                line_ids.append((0, 0, {
                    'name': obj.picking_id.name,
                    'product_id': move.product_id.id,
                    'quantity': move.product_qty,
                    'product_uom_id': move.product_id.uom_id.id,
                    'ref': obj.picking_id.name,
                    'partner_id': partner_id,
                    'stock_move_id': move.id,
                    'debit': cost,
                    'credit': 0,
                    'account_id': self.account_id.id,
                }))
                line_ids.append((0, 0, {
                    'name': obj.picking_id.name,
                    'product_id': move.product_id.id,
                    'quantity': move.product_qty,
                    'product_uom_id': move.product_id.uom_id.id,
                    'ref': obj.picking_id.name,
                    'partner_id': partner_id,
                    'stock_move_id': move.id,
                    'debit': 0,
                    'credit': cost,
                    'account_id': acc_src,
                }))
            new_account_move = self.env['account.move'].create({
                'journal_id': journal_id,
                'company_id': self.env.user.company_id.id,
                'date': date,
                'line_ids':line_ids,
                'ref': obj.picking_id.name,
            })
            new_account_move.post()
            obj.picking_id.account_move_id = new_account_move.id
            obj.picking_id.state = 'cancel'
            obj.picking_id.is_close_back_order = True

