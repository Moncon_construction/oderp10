# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from dateutil.relativedelta import relativedelta


class StockImmediateTransfer(models.TransientModel):
    _inherit = 'stock.immediate.transfer'

    force_date = fields.Datetime('Force Date', default=lambda self: fields.Datetime.now())
    show_force_date = fields.Boolean(compute='_show_force_date', string='Show Force Date')

    def _show_force_date(self):
        for obj in self:
            obj.show_force_date = True if self.env.user.company_id.assign_date == 'move_date' else False
                
    @api.multi
    def process(self):
        # Санхүүгийн түгжих огнооноос өмнөх огноонд picking батлаж болдоггүй болгов
        lock_date = max(self.pick_id.company_id.period_lock_date, self.pick_id.company_id.fiscalyear_lock_date)
        if self.user_has_groups('account.group_account_manager'):
            lock_date = self.pick_id.company_id.fiscalyear_lock_date
        check_date = self.force_date or datetime.now()
        if str(check_date) <= lock_date:
            message = _("You cannot add/modify entries prior to and inclusive of the lock date %s") % (
                lock_date)
            raise UserError(message)

        if self.pick_id.min_date and str(check_date) < self.pick_id.min_date:
            message = _("The date you entered must be equal to or after the Min Date. "
                        "Once you have verified your date, please confirm again.")
            raise UserError(message)
        if self.pick_id.location_id and self.pick_id.location_id.usage == 'transit':
            out_picking_ids = self.env['stock.picking'].search(
                [('id', '!=', self.pick_id.id), ('transit_order_id', '=', self.pick_id.transit_order_id.id),
                 ('state', '!=', 'done'), ('location_dest_id', '=', self.pick_id.location_id.id)])
            all_out_picking_ids = self.env['stock.picking'].search(
                [('id', '!=', self.pick_id.id), ('transit_order_id', '=', self.pick_id.transit_order_id.id),
                 ('location_dest_id', '=', self.pick_id.location_id.id)])
            if self.pick_id.company_id.transit_incoming_picking_confirm == 'all_out_done':
                if out_picking_ids:
                    raise UserError(
                        _("Replenishment's OUT picking is not done yet! Confirm OUT pickings first."))
            else:
                if len(out_picking_ids) == len(all_out_picking_ids):
                    raise UserError(_("Replenishment's OUT picking is not done yet! Confirm OUT pickings first."))

        if self.pick_id.transit_order_id:
            transit = self.env['stock.transit.order'].browse(self.pick_id.transit_order_id.id)
            change_date = False
            if transit.move_assign_date and check_date:
                change_date = check_date if str(check_date) > transit.move_assign_date else transit.move_assign_date
            else:
                change_date = check_date
            transit.move_assign_date = change_date
            if self.pick_id.picking_type_id.code == 'outgoing':
                for picking in transit.picking_ids:
                    if picking.picking_type_id.code == 'incoming' and picking.state != 'done':
                        in_move_date = datetime.strptime(str(change_date)[:10], '%Y-%m-%d') + timedelta(seconds=1)
                        for in_move in picking.move_lines:
                            in_move.write({'date': in_move_date})
        # Борлуулалтын захиалгаас үүссэн агуулахын хөдөлгөөн эсэхийг шалган, борлуулалт дээр зарласан move_assign_date
        # огноо талбарыг цэнэглэж, тухайн огноогоор нөөцлөлтийг шалгана.
        if self.env.get('sale.order', False) != False:
            if self.pick_id.related_sale_order_id:
                sale_order = self.env['sale.order'].browse(self.pick_id.related_sale_order_id.id)
                change_date = False
                if sale_order.move_assign_date:
                    change_date = check_date if str(check_date) > sale_order.move_assign_date else sale_order.move_assign_date
                else:
                    change_date = check_date
                sale_order.move_assign_date = change_date
            self.env.cr.execute("select "
                                "so.id as id "
                                "from stock_move sm "
                                "JOIN stock_picking sp on sm.picking_id = sp.id "
                                "JOIN procurement_order po on po.id = sm.procurement_id "
                                "JOIN sale_order_line sol on sol.id = po.sale_line_id "
                                "JOIN sale_order so on so.id = sol.order_id "
                                "where "
                                "sm.origin_returned_move_id is NULL and sp.id = %s " % self.pick_id.id)
            lines = self.env.cr.fetchall()
            if len(lines) > 0:
                ids = [line[0] for line in lines]
                sale_order = self.env['sale.order'].search([('id', 'in', ids)])
                if sale_order:
                    change_date = False
                    if sale_order.move_assign_date:
                        change_date = check_date if check_date > sale_order.move_assign_date else sale_order.move_assign_date
                    else:
                        change_date = check_date
                    sale_order.move_assign_date = change_date
        self.ensure_one()
        # If still in draft => confirm and assign
        if self.pick_id.state == 'draft':
            self.pick_id.action_confirm()
            if self.pick_id.state != 'assigned':
                self.pick_id.action_assign()
                if self.pick_id.state != 'assigned':
                    raise UserError(_("Could not reserve all requested products. Please use the \'Mark as Todo\' button to handle the reservation manually."))
        for pack in self.pick_id.pack_operation_ids:
            if pack.product_qty > 0:
                pack.write({'qty_done': pack.product_qty})
            else:
                pack.unlink()
                # ############ BEGIN OF CHANGE #########################
                #  Агуулахын хөдөлгөөний огноог өөрчлөн орлого авахад тухайн огноог дамжуулах
        if not self.pick_id.picking_type_id.warehouse_id.lock_move or (self.pick_id.picking_type_id.warehouse_id.lock_move and self.force_date >= self.pick_id.picking_type_id.warehouse_id.lock_move_until):
            res = self.with_context(force_period_date=self.force_date).pick_id.do_transfer()
            if self.pick_id.company_id.transit_incoming_picking_confirm == 'outgoing_done':
                if self.pick_id and self.pick_id.transit_order_id and self.pick_id.picking_type_id and self.pick_id.picking_type_id.code == 'outgoing':
                    incoming_picking_ids = self.env['stock.picking'].search([('transit_order_id', '=', self.pick_id.transit_order_id.id), ('picking_type_id.code', '=', 'incoming'), ('state', '!=', 'cancel')])
                    filtered_incoming_picking_ids = self.env['stock.picking']
                    if self.pick_id.move_lines.mapped('product_id'):
                        for out_picking_id in incoming_picking_ids:
                            if out_picking_id.move_lines.mapped('product_id') and set(self.pick_id.move_lines.mapped('product_id') & out_picking_id.move_lines.mapped('product_id')):
                                filtered_incoming_picking_ids |= out_picking_id
                    if filtered_incoming_picking_ids:
                        filtered_incoming_picking_ids.filtered(lambda x: x.state == 'draft').action_confirm()
                        filtered_incoming_picking_ids.filtered(lambda x: x.state in ('confirmed', 'partially_available', 'assigned')).force_assign()
                        for out_picking_id in filtered_incoming_picking_ids:
                            context = {"active_model": "stock.picking", "active_ids": [out_picking_id.ids], "active_id": out_picking_id.id}
                            if self.force_date:
                                date = datetime.strptime(self.force_date, DEFAULT_SERVER_DATETIME_FORMAT) + relativedelta(seconds=1)
                            else:
                                date = datetime.strptime(self.pick_id.min_date, DEFAULT_SERVER_DATETIME_FORMAT) + relativedelta(seconds=1)
                            transfer_wizard = self.env['stock.immediate.transfer'].with_context(context).create({'force_date': date})
                            transfer_wizard.process()
            return res
        else:
            raise UserError(_('Sorry, this warehouse is locked until %s') % self.pick_id.picking_type_id.warehouse_id.lock_move_until)
        # ############ END OF CHANGE #########################
