# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Product Average Cost for Each Warehouse (Under development)",
    'version': '1.0',
    'depends': [
        'l10n_mn_stock_account',
        'l10n_mn_sale'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Product Average Cost for Each Warehouse
    """,
    'data': [
        'wizard/stock_change_standard_price_views.xml',
        'views/product_view.xml',
        'views/stock_warehouse_view.xml',
        'views/product_create_view.xml',
        'security/ir.model.access.csv',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
