# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class AccountInvoice(models.Model):
    _inherit = "account.invoice"
    
    def _prepare_invoice_line_from_po_line(self, line):
        res = super(AccountInvoice, self)._prepare_invoice_line_from_po_line(line)
#         ХА-с нэхэмжлэл үүсгэхэд нэхэмжлэлийн мөрөөс хөөж агуулахын дансыг авах
        account_id = line.order_id.picking_type_id.warehouse_id.stock_account_input_id
        if account_id:
            res.update({
                        'account_id':account_id.id
                        })
        return res