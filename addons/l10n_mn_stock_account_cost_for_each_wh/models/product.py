# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, _
from odoo.exceptions import UserError
import odoo.addons.decimal_precision as dp


class ProductWarehouseStandardPrice(models.Model):
    _name = 'product.warehouse.standard.price'
    _description = 'Standard Price on Warehouse'
    '''
    def name_get(self, cr, uid, ids, context=None):
        res = []
        for price in self.browse(cr, uid, ids, context=context):
            res.append( (price.id, u'%s cost on %s' % (price.product_id.name, price.warehouse_id.name)))
        return res
    '''
    product_id = fields.Many2one('product.product', string='Product', required=True, index=True, ondelete='cascade')
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse', required=True, index=True)
    company_id = fields.Many2one('res.company', related='warehouse_id.company_id', string='Company', store=True, default=lambda self: self.env.user.company_id)
    standard_price = fields.Float('Cost Price', dp.get_precision('Product Standard Price'))
    initial_standard_price = fields.Float('Initial Standard Price')
    origin = fields.Char('Origin')

    @api.model
    def create(self, values):
        values.pop('product_name', False)
        existings = self.search([
            ('product_id', '=', values.get('product_id')),
            ('warehouse_id', '=', values.get('warehouse_id'))])

        res = super(ProductWarehouseStandardPrice, self).create(values)
        if existings:
            product = self.env['product.product'].browse(values.get('product_id'))
            warehouse = self.env['stock.warehouse'].browse(values.get('warehouse_id'))

            raise UserError(_("You cannot have two cost with the same product: [%s] %s and warehouse: %s.") %
                            (product.default_code, product.name, warehouse.name))
        return res


class ProductProduct(models.Model):
    _inherit = 'product.product'

    def get_warehouse_qty(self, warehouse, qty_date):
        StockLocation = self.env['stock.location']
        location_ids = str(StockLocation.search([('id', 'child_of', warehouse.view_location_id.id), ('usage', '=', 'internal')]).ids)[1:-1]
        self.env.cr.execute("SELECT l.prod_id AS pid, SUM(l.start_qty) AS qty  "
                            "FROM ( SELECT m.product_id AS prod_id, "
                            "CASE WHEN m.date <= %s AND m.location_id not in (" + location_ids + ") AND m.location_dest_id in (" + location_ids + ") "
                            "THEN m.product_qty "
                            "WHEN m.date <= %s AND m.location_id in (" + location_ids + ") AND m.location_dest_id not in (" + location_ids + ") "
                            "THEN -m.product_qty ELSE 0 END AS start_qty, "
                            "CASE WHEN m.location_id not in (" + location_ids + ") AND m.location_dest_id in (" + location_ids + ") "
                            "THEN m.location_dest_id "
                            "WHEN m.location_id in (" + location_ids + ") AND m.location_dest_id not in (" + location_ids + ") "
                            "THEN location_id ELSE 0 END AS lid "
                            "FROM stock_move m "
                            "LEFT JOIN product_product pp ON (pp.id=m.product_id) "
                            "WHERE m.state = 'done' AND pp.id = %s) AS l "
                            "LEFT JOIN stock_location sl ON (l.lid = sl.id) "
                            "LEFT JOIN stock_location sl1 ON (sl.location_id = sl1.id) "
                            "GROUP BY l.prod_id "
                            "HAVING SUM(l.start_qty)::decimal(16,4) <> 0", (qty_date, qty_date, self.id))
        lines = self.env.cr.dictfetchall()
        qty = 0
        for line in lines:
            qty = line['qty']
        return qty

    def get_warehouse_initial_standard_price(self, warehouse_id):
        wh_price = self.env['product.warehouse.standard.price'].search([('product_id', '=', self.id), ('warehouse_id', '=', warehouse_id)])
        return wh_price.initial_standard_price if wh_price else 0

    def get_warehouse_standard_price(self, warehouse_id):
        domain = [('product_id', '=', self.id)]
        if warehouse_id:
            domain += [('warehouse_id', '=', warehouse_id)]
        wh_price = 0
        for rec in self.env['product.warehouse.standard.price'].search(domain):
            wh_price = rec.standard_price
        return wh_price

    def set_warehouse_standard_price(self, warehouse_id, new_price, origin):
        price_obj = self.env['product.warehouse.standard.price']
        wh_price = price_obj.search([('product_id', '=', self.id), ('warehouse_id', '=', warehouse_id)])
        if wh_price:
            wh_price.standard_price = new_price
            wh_price.origin = origin
        else:
            price_obj.create({'product_id': self.id,
                              'initial_standard_price': 0,
                              'warehouse_id': warehouse_id,
                              'standard_price': new_price,
                              'origin':origin
                              })
        return True

    @api.multi
    def do_change_warehouse_standard_price(self, new_price, warehouse_id, account_id):
        """ Changes the Warehouse Standard Price of Product and (creates an account move accordingly - this code will be added soon)."""
        wh_standard_price_obj = self.env['product.warehouse.standard.price']
        price_ids = wh_standard_price_obj.search([('product_id', '=', self.id), ('warehouse_id', '=', warehouse_id)])
        if price_ids:
            price_ids.write({'standard_price': new_price})
        else:
            wh_standard_price_obj.create({'product_id': self.id, 'warehouse_id': warehouse_id, 'standard_price': new_price})
        return True


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    @api.multi
    def check_categ(self, categ_id):
        return True


class ProductCategory(models.Model):
    _inherit = 'product.category'

    @api.model
    def default_get(self, fields):
        # Агуулах бүрээр өртөг хөтөлдөг тохиолдолд барааны ангилал дээр default-раа утга нь оноогддог дараахи талбарын утгуудыг хоослов.
        # 1. Бараа материалын борлуулалтын данс  - property_account_income_categ_id
        # 2. Бараа материалын зардлын данс       - property_account_expense_categ_id
        # 3. Бараа материалын журнал             - property_stock_journal
        res = super(ProductCategory, self).default_get(fields)
        if 'property_stock_journal' in fields and 'property_stock_journal' in res:
            res['property_stock_journal'] = False
        if 'property_account_income_categ_id' in fields and 'property_account_income_categ_id' in res:
            res['property_account_income_categ_id'] = False
        if 'property_account_expense_categ_id' in fields and 'property_account_expense_categ_id' in res:
            res['property_account_expense_categ_id'] = False
        return res
