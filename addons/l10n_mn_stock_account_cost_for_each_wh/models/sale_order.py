# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError
import time

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    
    def get_account_for_invoice_line(self):
        # @verride: Нэхэмжлэлийн мөрийн данс бэлдэх /Борлуулалтын захиалга дах агуулахаас хайх/
        self.ensure_one()
        account = self.order_id.warehouse_id.stock_account_income_id
        if not account:
            raise UserError(_('Please define income account for this warehouse: "%s".') % (self.order_id.warehouse_id.name))
        return account
    
    def _compute_margin(self, order_id, product_id, product_uom_id):
        # Дахин тодорхойлов:
        # Агуулах бүрээр өртөг тооцоолдог үед тухайн барааны агуулахаарх өртөгөөр борлуулалт үүсгэх боломжтой болгов
        
        frm_cur = self.env.user.company_id.currency_id
        to_cur = order_id.pricelist_id.currency_id
        
        purchase_price = product_id.standard_price
        
        ############### Барааны агуулахаарх өртөгөөр борлуулалт үүсгэх өөрчлөлт - ЭХЛЭЛ ############### 
        
        self._cr.execute("""
                SELECT standard_price FROM product_warehouse_standard_price 
                WHERE company_id = %s AND warehouse_id = %s AND product_id = %s""" %(order_id.warehouse_id.company_id.id, order_id.warehouse_id.id, product_id.id))
        results = self._cr.dictfetchall()
        if results and len(results) > 0:
            purchase_price = results[0]['standard_price'] if results[0]['standard_price'] else purchase_price
            
        ############### Барааны агуулахаарх өртөгөөр борлуулалт үүсгэх өөрчлөлт - ТӨГСГӨЛ ############### 
        
        if product_uom_id != product_id.uom_id:
            purchase_price = product_id.uom_id._compute_price(purchase_price, product_uom_id)
        ctx = self.env.context.copy()
        ctx['date'] = order_id.date_order
        price = frm_cur.with_context(ctx).compute(purchase_price, to_cur, round=False)
        return price