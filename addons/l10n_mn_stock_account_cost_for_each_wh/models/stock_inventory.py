# -*- coding: utf-8 -*-

from datetime import datetime

from odoo import api, fields, models
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from dateutil.relativedelta import relativedelta


class StockInventory(models.Model):
    _inherit = "stock.inventory"

    @api.multi
    def post_inventory(self):
        res = super(StockInventory, self).post_inventory()
        self.update_warehouse_standard_price()
        return res
        
    @api.multi
    def update_warehouse_standard_price(self):
        # Тооллогын хөдөлгөөнөөс агуулах бүрээрх өртөг шинэчлэх/үүсгэх функц
        new_values, update_values = [], []
        for move in self.mapped('move_ids'):
            if move.location_dest_id.usage == 'internal':
                is_initial = move.inventory_id.is_initial
                origin_name = move.inventory_id.name
                warehouse_id = move.location_dest_id.get_warehouse().id
                price_id = self.env['product.warehouse.standard.price'].search([('product_id', '=', move.product_id.id), ('warehouse_id', '=', warehouse_id)], order="create_date desc", limit=1)
                if price_id:
                    if is_initial:
                        price = move.price_unit
                    else:
                        # Тухайн хөдөлгөөнөөс өмнөх тухайн агуулах дахь нийт үлдэгдлийг олж өмнөх агуулахын өртөгтэй дундажлах
                        product_qty = move.product_id.get_qty_availability([move.location_dest_id.id], datetime.strptime(move.date, DEFAULT_SERVER_DATETIME_FORMAT) - relativedelta(microseconds=1)) or 0
                        price = (product_qty * price_id.standard_price + move.product_uom_qty * move.price_unit) / (product_qty + move.product_uom_qty) if product_qty + move.product_uom_qty > 0 else 0
                    update_values.append((price_id.id, price or 0, origin_name, is_initial))
                else:
                    new_values.append("(%s, %s, %s, %s, '%s')" % (move.product_id.id, warehouse_id, move.price_unit, move.price_unit if is_initial else 0, origin_name))
                    
        if new_values:
            qry = """
                INSERT INTO product_warehouse_standard_price (product_id, warehouse_id, standard_price, initial_standard_price, origin) 
                VALUES 
            """
            qry += ", ".join(value for value in new_values)
            self._cr.execute(qry)
            
        if update_values:
            update_tuple = ", ".join("(%s, %s, '%s', %s)" % (update_value[0], update_value[1], update_value[2], update_value[3]) for update_value in update_values)
            qry = """
                UPDATE product_warehouse_standard_price sprice SET standard_price = u.price, origin = u.origin, initial_standard_price = COALESCE(CASE WHEN u.is_initial THEN u.price ELSE initial_standard_price END, initial_standard_price)
                FROM (VALUES %s) AS u(id, price, origin, is_initial)
                WHERE u.id = sprice.id
            """ % update_tuple
            self._cr.execute(qry)
            
    @api.multi
    def compute_standard_price(self):
        # Тооллогын мөр дээрх өртөг барааны агуулах бүрээрх өртөг цэснээс өртгөө авна.
        warehouse_id = self.location_id.get_warehouse().id
        for line in self.line_ids:
            if line.product_id:
                self.env.cr.execute(
                    "SELECT standard_price "
                    "FROM product_warehouse_standard_price "
                    "WHERE product_id = %s AND warehouse_id = %s "
                    "ORDER BY create_date "
                    "DESC LIMIT 1" % (line.product_id.id, warehouse_id))
                standard_price = self.env.cr.dictfetchone()
                if standard_price:
                    line.standard_price = standard_price['standard_price']
                else:
                    line.standard_price = line.product_id.standard_price