# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import logging

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from collections import defaultdict

_logger = logging.getLogger(__name__)


class StockMove(models.Model):
    _inherit = 'stock.move'

    is_same_product = fields.Boolean(string='Is Same Product', default = False)#Ижил бараатай мөрийг тэмдэглэх

    @api.multi
    def action_done(self):
        res = super(StockMove, self.with_context(date=self.env.context.get('force_date'))).action_done()
        #  Агуулахын тоо хэмжээ 0 болон түүнээс бага бол үниийн түүх дээрх өртгийг 0 болгох
        for move in self:
            if move.location_id.usage == 'internal':
                warehouse = move.location_id.get_warehouse()
                if not warehouse:
                    raise UserError(_("This location has no warehouse. Location: %s" % move.location_id.name))
                product_tot_qty_available = move.product_id.get_warehouse_qty(warehouse, fields.Datetime.now())
                if product_tot_qty_available <= 0:
                    wh_price = self.env['product.warehouse.standard.price'].search([('product_id', '=', move.product_id.id), ('warehouse_id', '=', warehouse.id)])
                    if wh_price:
                        wh_price.standard_price = 0
        return res
    
    @api.multi
    def product_price_update_before_done(self):
        tmpl_dict = defaultdict(lambda: 0.0)
        # adapt standard price on incomming moves if the product cost_method is 'average'
        std_price_update = {}
        origin = ''
        new_std_price = total_price = total_qty = product_tot_qty = 0
        for move in self.filtered(lambda move: move.location_id.usage in ('supplier', 'production', 'transit') and move.product_id.cost_method == 'average'):
            warehouse = move.location_dest_id.get_warehouse()
            if not warehouse:
                raise UserError(_("This location has no warehouse. Location: %s" % (move.location_dest_id.name)))
            product_tot_qty_available = move.product_id.get_warehouse_qty(warehouse, fields.Datetime.now())
            # if the incoming move is for a purchase order with foreign currency, need to call this to get the same value that the quant will use.
            if product_tot_qty_available <= 0:
                #Хэрвээ худалдан авалтаас орлого орж байгаа тохиолдолд ижил бараа 2 мөр орсон байвал тухайн барааны өртгийг тооцож байна.
                if move.purchase_line_id:
                    moves = self.env['stock.move'].search([('product_id', '=', move.product_id.id),('picking_id', '=', move.picking_id.id),('id', '!=', move.id)])
                    if moves:
                        for move1 in moves:
                            total_price += move1.get_price_unit() * move1.product_qty
                            total_qty += move1.product_qty
                            move1.is_same_product = True
                        new_std_price = total_price/total_qty
                    else:
                        new_std_price = move.get_price_unit()
                else:
                    new_std_price = move.get_price_unit()
                if move.picking_id:
                    origin = move.picking_id.name
                move.product_id.set_warehouse_standard_price(warehouse.id, new_std_price, origin)
            else:
                # Get the standard price
                amount_unit = move.product_id.get_warehouse_standard_price(warehouse.id)
                if move.is_same_product == False:
                    #Хэрвээ худалдан авалтаас орлого орж байгаа тохиолдолд ижил бараа 2 мөр орсон байвал тухайн барааны өртгийг тооцож байна.
                    if move.purchase_line_id:
                        moves = self.env['stock.move'].search([('product_id', '=', move.product_id.id),('picking_id', '=', move.picking_id.id),('id', '!=', move.id)])
                        product_tot_qty = amount_unit * product_tot_qty_available
                        '''Ижил бараатай орлогын мөрийг тэмдэглэж эхний мөрөөр өртгийг шинэчилсэн учир дараагийн мөрөөр дахин 
                            шинэчлэхгүй гэсэн санаагаар ижил бараа эсэх талбарыг чеклэж дахин тооцоолол хийхгүй байхаар тооцсон.'''
                        if moves:
                            for move1 in moves:
                                total_price += move1.get_price_unit() * move1.product_qty
                                total_qty += move1.product_qty
                                move1.write({'is_same_product': True})
                            new_std_price = (product_tot_qty + total_price) / (product_tot_qty_available +total_qty)
                        else:
                            new_std_price = ((amount_unit * product_tot_qty_available) + (move.get_price_unit() * move.product_qty)) / (product_tot_qty_available + move.product_qty)
                    else:
                        new_std_price = ((amount_unit * product_tot_qty_available) + (move.get_price_unit() * move.product_qty)) / (product_tot_qty_available + move.product_qty)
                    
                    if move.picking_id:
                        origin = move.picking_id.name
                    move.product_id.set_warehouse_standard_price(warehouse.id, new_std_price, origin) 

    @api.multi
    def get_price_unit(self):
        if self.location_id.usage == 'transit' and self.location_dest_id.usage == 'internal':
            # Нөхөн дүүргэлтийн орлого
            if self.picking_id.transit_order_id:
                source_move = self.env['stock.move'].search([('product_id', '=', self.product_id.id), ('id', '!=', self.id), ('picking_id.transit_order_id', '=', self.picking_id.transit_order_id.id)])
                if not source_move:
                    raise UserError(_("'%s' product's move not found at outcoming products !!!") % self.product_id.name)
                self.price_unit = source_move[0].price_unit
                return self.price_unit
            else:
                raise UserError(_("Transit order not found: %s") % self.picking_id.name)
        return super(StockMove, self).get_price_unit()

    def get_product_standard_price(self):
        _logger.info(_('Function Start: move get_product_standard_price'))
        self.ensure_one()
        if self.location_id.usage == 'internal':
            warehouse = self.env['stock.warehouse'].search([('view_location_id', 'parent_of', self.location_id.id)])
            if warehouse:
                wh_price = self.env['product.warehouse.standard.price'].search([('product_id', '=', self.product_id.id), ('warehouse_id', '=', warehouse.id)])
                if wh_price:
                    return wh_price.standard_price
                else:
                    return self.price_unit
            else:
                raise UserError(_("This location has no warehouse. Location: %s" % (self.location_id.name)))
        elif self.inventory_id and self.inventory_id.is_initial and self.location_id.usage == 'inventory' and self.location_dest_id.usage == 'internal':
            warehouse = self.env['stock.warehouse'].search([('view_location_id', 'parent_of', self.location_dest_id.id)])
            if warehouse:
                wh_price = self.env['product.warehouse.standard.price'].search([('product_id', '=', self.product_id.id), ('warehouse_id', '=', warehouse.id)])
                if wh_price:
                    return wh_price.initial_standard_price
                else:
                    return self.price_unit
            else:
                raise UserError(_("This location has no warehouse. Location: %s" % (self.location_id.name)))
        elif self.location_id.usage == 'inventory' and self.location_dest_id.usage == 'internal':
            warehouse = self.env['stock.warehouse'].search([('view_location_id', 'parent_of', self.location_dest_id.id)])
            if warehouse:
                wh_price = self.env['product.warehouse.standard.price'].search([('product_id', '=', self.product_id.id), ('warehouse_id', '=', warehouse.id)])
                if wh_price:
                    return wh_price.standard_price
                else:
                    return self.price_unit
            else:
                raise UserError(_("This location has no warehouse. Location: %s" % (self.location_id.name)))
        else:
            return self.price_unit

    @api.multi
    def get_warehouse_accounts(self, location):
        accounts_data = {}
        warehouse_id = location.get_warehouse()
        if warehouse_id:
            # ББӨ данс авч байна
            if warehouse_id.stock_account_output_id:
                accounts_data['stock_output'] = warehouse_id.stock_account_output_id.id
            else:
                raise UserError(_('Please configure Stock Output Account on this warehouse: %s') % warehouse_id.name)

            # БМ данс авч байна
            if warehouse_id.stock_valuation_account_id:
                accounts_data['stock_valuation'] = warehouse_id.stock_valuation_account_id.id
            else:
                raise UserError(_('Please configure Stock Valuation Account on this warehouse: %s') % warehouse_id.name)

            # Замд яваа барааны данс авч байна
            if self.location_id.usage == 'transit' or self.location_dest_id.usage == 'transit':
                warehouse = self.picking_id.transit_order_id.warehouse_id
                if warehouse.replenishment_account_on_the_road_id:
                    accounts_data['stock_input'] = warehouse.replenishment_account_on_the_road_id.id
                else:
                    raise UserError(_('Please configure Stock Replenishment account on the road of this warehouse: %s') % warehouse_id.name)
            else:
                if warehouse_id.stock_account_input_id:
                    accounts_data['stock_input'] = warehouse_id.stock_account_input_id.id
                else:
                    raise UserError(_('Please configure Stock Input Account of this warehouse: %s') % warehouse_id.name)

            # БМ-н журнал авч байна
            if warehouse_id.stock_journal:
                accounts_data['stock_journal'] = warehouse_id.stock_journal.id
            else:
                raise UserError(_('Please configure Stock Journal of this warehouse: %s') % warehouse_id.name)
        else:
            raise UserError(_('There is no warehouse on this location: %s') % location.name)
        return accounts_data

    @api.multi
    def _get_accounting_data_for_valuation(self):
        """ Return the accounts and journal to use to post Journal Entries for
        the real-time valuation of the quant. """
        self.ensure_one()

        # Агуулах тус бүртээ өртөг хөтлөх үед дансны тохиргоог агуулах дээрээс авна.
        warehouse_obj = self.env['stock.warehouse']
        accounts_data = {}

        if self.location_id.usage == 'internal' and self.location_dest_id.usage in ['inventory', 'customer', 'supplier', 'production']:
            accounts_data['stock_journal'] = self.get_warehouse_accounts(self.location_id)['stock_journal']
            acc_valuation = self.get_warehouse_accounts(self.location_id)['stock_valuation']
            acc_src = self.get_warehouse_accounts(self.location_id)['stock_input']
            acc_dest = self.get_warehouse_accounts(self.location_id)['stock_output']
        elif self.location_id.usage == 'internal' and self.location_dest_id.usage in ['transit']:
            # Нөхөн дүүргэлтийн зарлага
            accounts_data['stock_journal'] = self.get_warehouse_accounts(self.location_id)['stock_journal']
            acc_valuation = self.get_warehouse_accounts(self.location_id)['stock_valuation']
            acc_src = self.get_warehouse_accounts(self.location_id)['stock_input']
            acc_dest = self.get_warehouse_accounts(self.location_id)['stock_input']

        elif self.location_id.usage in ('supplier', 'inventory', 'production', 'customer', 'transit') and self.location_dest_id.usage == 'internal':
            if self.inventory_id and self.inventory_id.is_initial:
                acc_src = self.company_id.initial_config_account_id.id or self.get_warehouse_accounts(self.location_dest_id)['stock_input']
            else:
                acc_src = self.get_warehouse_accounts(self.location_dest_id)['stock_input']

            accounts_data['stock_journal'] = self.get_warehouse_accounts(self.location_dest_id)['stock_journal']
            acc_valuation = self.get_warehouse_accounts(self.location_dest_id)['stock_valuation']
            acc_dest = self.get_warehouse_accounts(self.location_dest_id)['stock_output']

        elif self.location_id.usage in ('transit') and self.location_dest_id.usage == 'internal':
            # Нөхөн дүүргэлтийн орлого
            accounts_data['stock_journal'] = self.get_warehouse_accounts(self.location_dest_id)['stock_journal']
            acc_valuation = self.get_warehouse_accounts(self.location_dest_id)['stock_valuation']
            acc_src = self.get_warehouse_accounts(self.location_dest_id)['stock_input']
            acc_dest = self.get_warehouse_accounts(self.location_dest_id)['stock_input']

        if self.location_id.valuation_out_account_id:
            acc_src = self.location_id.valuation_out_account_id.id

        if self.location_dest_id.valuation_in_account_id:
            acc_dest = self.location_dest_id.valuation_in_account_id.id

        if not accounts_data.get('stock_journal', False):
            raise UserError(_('You don\'t have any stock journal defined on your product category, check if you have installed a chart of accounts'))
        if not acc_src:
            raise UserError(_('Cannot find a stock input account for the product %s. You must define one on the product category, or on the location, before processing this operation.') % (self.product_id.name))
        if not acc_dest:
            raise UserError(_('Cannot find a stock output account for the product %s. You must define one on the product category, or on the location, before processing this operation.') % (self.product_id.name))
        if not acc_valuation:
            raise UserError(_('You don\'t have any stock valuation account defined on your product category. You must define one before processing this operation.'))
        journal_id = accounts_data['stock_journal']
        return journal_id, acc_src, acc_dest, acc_valuation

    def _store_average_cost_price(self):
        """ Store the average price of the move on the move and product form (costing method 'real')"""
        for move in self.filtered(lambda move: move.product_id.cost_method == 'real'):
            # product_obj = self.pool.get('product.product')
            if any(q.qty <= 0 for q in move.quant_ids) or move.product_qty == 0:
                # if there is a negative quant, the standard price shouldn't be updated
                continue
            # Note: here we can't store a quant.cost directly as we may have moved out 2 units
            # (1 unit to 5€ and 1 unit to 7€) and in case of a product return of 1 unit, we can't
            # know which of the 2 costs has to be used (5€ or 7€?). So at that time, thanks to the
            # average valuation price we are storing we will valuate it at 6€
            valuation_price = sum(q.qty * q.cost for q in move.quant_ids)
            average_valuation_price = valuation_price / move.product_qty

            move.product_id.with_context(force_company=move.company_id.id).sudo().write({'standard_price': average_valuation_price})
            move.write({'price_unit': average_valuation_price})

        for move in self.filtered(lambda move: move.product_id.cost_method != 'real' and not move.origin_returned_move_id):
            # Unit price of the move should be the current standard price, taking into account
            # price fluctuations due to products received between move creation (e.g. at SO
            # confirmation) and move set to done (delivery completed).
            move.write({'price_unit': move.get_product_standard_price()})

    def _create_account_move_line(self, credit_account_id, debit_account_id, journal_id):
        # Энэ функцийг stock.quant-с stock.move рүү оруулав
        # group quants by cost
        for move in self:
            AccountMove = self.env['account.move']
            move_lines = move._prepare_account_move_line(move.product_qty, move.price_unit, credit_account_id, debit_account_id)
            if move_lines:
                date = self._context.get('force_period_date', fields.Date.context_today(self))
                if not move.picking_id.account_move_id:
                    new_account_move = AccountMove.create({
                        'journal_id': journal_id,
                        'line_ids': move_lines,
                        'date': move.date,
                        'ref': move.picking_id.name})
                    new_account_move.post()
                    move.picking_id.write({'account_move_id': new_account_move.id})
                else:
                    move.picking_id.account_move_id.write({'line_ids': move_lines})

    def enter_account_entry_move(self):
        """ Accounting Valuation Entries """
        _logger.info(_('Function Start: move enter_account_entry_move'))
        for move in self:
            if move.product_id.type != 'product' or move.product_id.valuation != 'real_time':
                # no stock valuation for consumable products
                return False
            location_from = move.location_id
            location_to = move.location_dest_id  # TDE FIXME: as the accounting is based on this value, should probably check all location_to to be the same
            company_from = location_from.usage == 'internal' and location_from.company_id or False
            company_to = location_to and (location_to.usage == 'internal') and location_to.company_id or False
            if hasattr(move.picking_id, 'expense') and move.picking_id.expense:
                account_id = False
                for line in move.picking_id.expense.expense_line:
                    if line.product.id == move.product_id.id:
                        account_id = line.account.id
                if move.picking_id.picking_type_id.code != 'incoming':
                    journal_id, acc_src, acc_dest, acc_valuation = move._get_accounting_data_for_valuation()
                    acc_dest = account_id.id
                    move.with_context(force_company=company_from.id)._create_account_move_line(acc_valuation, acc_dest, journal_id)
                else:
                    journal_id, acc_src, acc_valuation, acc_dest = move._get_accounting_data_for_valuation()
                    acc_valuation = account_id.id
                    move.with_context(force_company=company_to.id)._create_account_move_line(acc_valuation, acc_dest, journal_id)
            else:
                # Create Journal Entry for products arriving in the company; in case of routes making the link between several
                # warehouse of the same company, the transit location belongs to this company, so we don't need to create accounting entries
                if company_to and (move.location_id.usage not in ('internal') and move.location_dest_id.usage == 'internal' or company_from != company_to):
                    journal_id, acc_src, acc_dest, acc_valuation = move._get_accounting_data_for_valuation()
                    if location_from and location_from.usage == 'customer':  # goods returned from customer
                        move.with_context(force_company=company_to.id)._create_account_move_line(acc_dest, acc_valuation, journal_id)
                    else:
                        move.with_context(force_company=company_to.id)._create_account_move_line(acc_src, acc_valuation, journal_id)
                # Create Journal Entry for products leaving the company
                if company_from and (move.location_id.usage == 'internal' and move.location_dest_id.usage not in ('internal') or company_from != company_to):
                    journal_id, acc_src, acc_dest, acc_valuation = move._get_accounting_data_for_valuation()
                    if location_to and location_to.usage == 'supplier':  # goods returned to supplier
                        move.with_context(force_company=company_from.id)._create_account_move_line(acc_valuation, acc_src, journal_id)
                    else:
                        move.with_context(force_company=company_from.id)._create_account_move_line(acc_valuation, acc_dest, journal_id)
                if move.company_id.anglo_saxon_accounting and move.location_id.usage == 'supplier' and move.location_dest_id.usage == 'customer':
                    # Creates an account entry from stock_input to stock_output on a dropship move. https://github.com/odoo/odoo/issues/12687
                    journal_id, acc_src, acc_dest, acc_valuation = move._get_accounting_data_for_valuation()
                    move.with_context(force_company=move.company_id.id)._create_account_move_line(acc_src, acc_dest, journal_id)
        _logger.info(_('Function Done: move enter_account_entry_move'))
        return True
                
    def set_default_price_unit_from_product(self):
        # @Override: Агуулах бүрээр өртөг хөтлөх бол 0 өртөгтэй хөдөлгөөний өртгийг олгохдоо БАРААНЫ АГУУЛАХ БҮРЭЭРХ ӨРТӨГ цэснээс авдаг болгов 
        for move in self._set_default_price_moves():
            move.write({'price_unit': move.get_product_standard_price()})
