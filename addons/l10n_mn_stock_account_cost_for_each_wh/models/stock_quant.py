# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api
from collections import defaultdict


class StockQuant(models.Model):
    _inherit = "stock.quant"

    @api.multi
    def _compute_inventory_value(self):
        for quant in self:
            # Дундаж үнийн өртөгөөр тооцоолоогүй үед core функц-р ажиллана
            if quant.product_id.cost_method != 'average':
                return super(StockQuant, self)._compute_inventory_value()
            # Дундаж үнийн өртгөөр тооцоолж байгаа үед stock.move-с өртөгөө авна
            # Stock.move зарлага үед агуулахын өртгөөр, орлого үед захиалга дээрх үнийг авна
            # Дундаж өртгөөр тооцоолж байгаа үед хамгийн сүүлийн stock.move-д байгаа нэгж үнэ нь өнөөгийн өртөг гэж үзнэ
            else:
                if quant.history_ids:
                    move_id = quant.history_ids.sorted(reverse=True)[0]
                    quant.inventory_value = move_id.price_unit * quant.qty

    def _account_entry_move(self, move):
        """ Accounting Valuation Entries """
            
        if move.product_id.type != 'product' or move.product_id.valuation != 'real_time':
            # no stock valuation for consumable products
            return False
        if any(quant.owner_id or quant.qty <= 0 for quant in self):
            # if the quant isn't owned by the company, we don't make any valuation en
            # we don't make any stock valuation for negative quants because the valuation is already made for the counterpart.
            # At that time the valuation will be made at the product cost price and afterward there will be new accounting entries
            # to make the adjustments when we know the real cost price.
            return False
        location_from = move.location_id
        location_to = self[0].location_id  # TDE FIXME: as the accounting is based on this value, should probably check all location_to to be the same
        company_from = location_from.usage == 'internal' and location_from.company_id or False
        company_to = location_to and (location_to.usage == 'internal') and location_to.company_id or False
        if hasattr(move.picking_id, 'expense') and move.picking_id.expense:
            account_id = False
            for line in move.picking_id.expense.expense_line:
                if line.product.id == move.product_id.id:
                    account_id = line.account.id
            if move.picking_id.picking_type_id.code != 'incoming':
                journal_id, acc_src, acc_dest, acc_valuation = move._get_accounting_data_for_valuation()
                acc_dest = account_id
                self.with_context(force_company=company_from.id)._create_account_move_line(move, acc_valuation, acc_dest, journal_id)
            else:
                journal_id, acc_src, acc_valuation, acc_dest = move._get_accounting_data_for_valuation()
                acc_valuation = account_id
                self.with_context(force_company=company_to.id)._create_account_move_line(move, acc_valuation, acc_dest, journal_id)
        else:
            # Create Journal Entry for products arriving in the company; in case of routes making the link between several
            # warehouse of the same company, the transit location belongs to this company, so we don't need to create accounting entries
            if company_to and (move.location_id.usage not in ('internal') and move.location_dest_id.usage == 'internal' or company_from != company_to):
                journal_id, acc_src, acc_dest, acc_valuation = move._get_accounting_data_for_valuation()
                if location_from and location_from.usage == 'customer':  # goods returned from customer
                    self.with_context(force_company=company_to.id)._create_account_move_line(move, acc_dest, acc_valuation, journal_id)
                else:
                    self.with_context(force_company=company_to.id)._create_account_move_line(move, acc_src, acc_valuation, journal_id)

            # Create Journal Entry for products leaving the company
            if company_from and (move.location_id.usage == 'internal' and move.location_dest_id.usage not in ('internal') or company_from != company_to):
                journal_id, acc_src, acc_dest, acc_valuation = move._get_accounting_data_for_valuation()
                if location_to and location_to.usage == 'supplier':  # goods returned to supplier
                    self.with_context(force_company=company_from.id)._create_account_move_line(move, acc_valuation, acc_src, journal_id)
                else:
                    self.with_context(force_company=company_from.id)._create_account_move_line(move, acc_valuation, acc_dest, journal_id)
            if move.company_id.anglo_saxon_accounting and move.location_id.usage == 'supplier' and move.location_dest_id.usage == 'customer':
                # Creates an account entry from stock_input to stock_output on a dropship move. https://github.com/odoo/odoo/issues/12687
                journal_id, acc_src, acc_dest, acc_valuation = move._get_accounting_data_for_valuation()
                self.with_context(force_company=move.company_id.id)._create_account_move_line(move, acc_src, acc_dest, journal_id)