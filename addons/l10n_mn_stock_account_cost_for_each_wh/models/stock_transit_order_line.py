# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import time
from odoo import api, fields, models, _
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import odoo.addons.decimal_precision as dp
from odoo.exceptions import UserError
import logging

_logger = logging.getLogger(__name__)

class StockTransitOrderLine(models.Model):
    _inherit = 'stock.transit.order.line'
    
    @api.onchange('product_id', 'product_qty', 'product_uom')
    def onchange_product_id(self):
        result = {}
        if not self.product_id:
            self.product_uom = False
            self.name = False
            self.product_qty = False
        else:
            product = self.product_id
            self.product_uom = product.uom_id.id
            self.name = product.name
            self.price_unit = product.get_warehouse_standard_price(self.transit_order_id.supply_warehouse_id.id)
            result['domain'] = {'product_uom': [('category_id', '=', product.uom_id.category_id.id)]}
        return result

    @api.depends('product_qty', 'price_unit')
    def _compute_amount(self):
        """
        Compute the sub_total of the Transit Order line.
        """
        for line in self:
            line.sub_total = line.price_unit * line.product_qty