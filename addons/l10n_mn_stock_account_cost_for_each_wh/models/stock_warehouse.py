# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api

class StockWarehouse(models.Model):
    _inherit = 'stock.warehouse'
    
    stock_account_creditor_price_difference = fields.Many2one('account.account', string="Price Difference Account",
        help="This account will be used to value price difference between purchase price and accounting cost.")
    
    stock_account_income_id = fields.Many2one('account.account', string="Income Account",
        domain=[('deprecated', '=', False)],
        help="This account will be used for invoices to value sales.")
    stock_account_expense_id = fields.Many2one('account.account', string="Expense Account",
        domain=[('deprecated', '=', False)],
        help="This account will be used for invoices to value expenses.")
    
    stock_journal = fields.Many2one('account.journal', 'Stock Journal',
        help="When doing real-time inventory valuation, this is the Accounting Journal in which entries will be automatically posted when stock moves are processed.")
    stock_account_input_id = fields.Many2one(
        'account.account', 'Stock Input Account',
        domain=[('deprecated', '=', False)],
        help="When doing real-time inventory valuation, counterpart journal items for all incoming stock moves will be posted in this account, unless "
             "there is a specific valuation account set on the source location. This is the default value for all products in this category. It "
             "can also directly be set on each product")
    stock_account_output_id = fields.Many2one(
        'account.account', 'Stock Output Account', domain=[('deprecated', '=', False)],
        help="When doing real-time inventory valuation, counterpart journal items for all outgoing stock moves will be posted in this account, unless "
             "there is a specific valuation account set on the destination location. This is the default value for all products in this category. It "
             "can also directly be set on each product")
    stock_valuation_account_id = fields.Many2one(
        'account.account', 'Stock Valuation Account', domain=[('deprecated', '=', False)],
        help="When real-time inventory valuation is enabled on a product, this account will hold the current value of the products.",)
    replenishment_account_on_the_road_id = fields.Many2one('account.account', domain=[('deprecated', '=', False)],
                                                           string="Replenishment account on the road")
