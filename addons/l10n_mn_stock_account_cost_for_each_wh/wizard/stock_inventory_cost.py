# -*- coding: utf-8 -*-

import odoo.addons.decimal_precision as dp  # @UnresolvedImport
from odoo import api, models, fields, _
from odoo.exceptions import UserError


class StockInventoryCost(models.TransientModel):
    _inherit = "stock.inventory.cost"

    @api.model
    def default_get(self, fields):
        res = super(StockInventoryCost, self).default_get(fields)

        product_obj = self.env['product.product']
        location_obj = self.env['stock.location']
        warehouse_obj = self.env['stock.warehouse']

        inventory_obj = self.env['stock.inventory']
        inventory = inventory_obj.browse(self._context['active_id'])
        warehouse = warehouse_obj.search([('lot_stock_id', '=', inventory.location_id.id)])
        if not warehouse:
            warehouse = warehouse_obj.search([('view_location_id', 'parent_of', inventory.location_id.id)])
        if 'lines' in res.keys():
            if warehouse:
                for line in res['lines']:
                    product = product_obj.browse(line[2]['product_id'])
                    line[2]['price_unit'] = product.get_warehouse_standard_price(warehouse.id) if not inventory.is_initial else product.get_warehouse_initial_standard_price(warehouse.id)
            else:
                raise UserError(_("Warehouse not found related to this location."))

        return res
