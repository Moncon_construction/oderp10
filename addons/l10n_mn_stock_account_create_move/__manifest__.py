# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Stock move -- account move",
    'version': '1.0',
    'depends': ['l10n_mn_stock_account'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """ Барааны хөдөлгөөнөөс журналын бичилт үүсгэх
    """,
    'data': [],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}