# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import logging
import time
from datetime import datetime

from odoo import api, fields, models, _
from odoo.exceptions import UserError
import odoo.addons.decimal_precision as dp
from odoo.tools import float_compare, float_is_zero
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

_logger = logging.getLogger(__name__)


class StockPicking(models.Model):
    _inherit = "stock.picking"

    @api.multi
    def do_transfer(self):
        company = self.company_id
        if company.availability_compute_method == 'stock_move':
            # Компани Барааны боломжит нөөц тооцох арга -- > Барааны хөдөлгөөнөөс тооцох
            res = self.do_transfer_by_move()
        else:
            # Компани Барааны боломжит нөөц тооцох арга -- > Барааны тооллогоос тооцох
            res = super(StockPicking, self).do_transfer()
        return res

    @api.multi
    def do_transfer_by_move(self):
        """ Шилжүүлэх функц
            Компани Барааны боломжит нөөц тооцох арга -- > Барааны хөдөлгөөнөөс тооцох
            барааны хөдөлгөөнөөс тооцож байхад stock.quant болон lot_ids шаардлагагүй тул эдгээрийг тооцож буй функцуудыг хаслаа """
        _logger.info(_('Function: picking do_transfer_by_move'))
        no_pack_op_pickings = self.filtered(lambda picking: not picking.pack_operation_ids)
        other_pickings = self - no_pack_op_pickings
        for picking in other_pickings:
            todo_moves = self.env['stock.move']
            toassign_moves = self.env['stock.move']
            if not picking.move_lines:
                todo_moves |= picking._create_extra_moves()
            # split move lines if needed
            for move in picking.move_lines:
                rounding = move.product_id.uom_id.rounding
                remaining_qty = move.remaining_qty
                if move.state in ('done', 'cancel'):
                    # ignore stock moves cancelled or already done
                    continue
                elif move.state == 'draft':
                    toassign_moves |= move
                if float_compare(remaining_qty, 0, precision_rounding=rounding) == 0:
                    if move.state in ('draft', 'assigned', 'confirmed'):
                        todo_moves |= move
                elif float_compare(remaining_qty, 0, precision_rounding=rounding) > 0 and float_compare(remaining_qty, move.product_qty, precision_rounding=rounding) < 0:
                    # TDE FIXME: shoudl probably return a move - check for no track key, by the way
                    new_move_id = move.split(remaining_qty)
                    new_move = self.env['stock.move'].with_context(mail_notrack=True).browse(new_move_id)
                    todo_moves |= move
                    # Assign move as it was assigned before
                    toassign_moves |= new_move

            # TDE FIXME: do_only_split does not seem used anymore
            if todo_moves and not self.env.context.get('do_only_split'):
                todo_moves.action_done_by_move()
            elif self.env.context.get('do_only_split'):
                picking = picking.with_context(split=todo_moves.ids)
            picking._create_backorder()
        self.after_done()
        # Барааны шаардах суусан тохиолдолд ажиллах функц
        if self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_product_expense'), ('state', 'in', ('installed', 'to upgrade'))]):
            self.check_product_expense()
        # ХАЗ суусан тохиолдолд ажиллах функц
        if self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_purchase'), ('state', 'in', ('installed', 'to upgrade'))]):
            self.check_purchase_received()
        # Борлуулалт суусан тохиолдолд ажиллах функц
        if self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_sale_stock'), ('state', 'in', ('installed', 'to upgrade'))]):
            self.check_sale_delivered()
        return True

    @api.multi
    def action_done_by_move(self):
        """ Агуулахын хөдөлгөөний үйлдлүүдлийг дуусгах функц
            Компани Барааны боломжит нөөц тооцох арга -- > Барааны хөдөлгөөнөөс тооцох"""
        _logger.info(_('Function: picking action_done_by_move'))
        draft_moves = self.mapped('move_lines').filtered(lambda self: self.state == 'draft')
        todo_moves = self.mapped('move_lines').filtered(lambda self: self.state in ['draft', 'assigned', 'confirmed'])
        draft_moves.action_confirm()
        todo_moves.action_done_by_move()
        return True

    @api.multi
    def force_assign(self):
        _logger.info(_('Function: picking force_assign'))
        company = self.env.user.company_id
        pos = self._context.get('pos', False)
        if company.availability_compute_method == 'stock_move' and pos:
            # Компани Барааны боломжит нөөц тооцох арга -- > Барааны хөдөлгөөнөөс тооцох
            res = self.force_assign_by_move()
        else:
            # Компани Барааны боломжит нөөц тооцох арга -- > Барааны тооллогоос тооцох
            res = super(StockPicking, self).force_assign()
        return res

    def force_assign_by_move(self):
        """ Хүчээр бэлэн болгох функц
            Компани Барааны боломжит нөөц тооцох арга -- > Барааны хөдөлгөөнөөс тооцох
            барааны хөдөлгөөнөөс тооцож байхад stock.quant болон lot_ids шаардлагагүй тул эдгээрийг тооцож буй функцуудыг хаслаа """
        _logger.info(_('Function Start: picking force_assign_by_move'))
        date = self._context.get('force_period_date', time.strftime(DEFAULT_SERVER_DATETIME_FORMAT))
        for picking in self:
            for move in picking.move_lines:
                if float_compare(move.product_qty, 0, precision_rounding=move.product_id.uom_id.rounding) > 0:
                    move.do_prepare_partial_by_move()
                    move.product_price_update_before_done()
                    move.enter_account_entry_move()
                    if not move.price_unit:
                        move.write({'price_unit': move.get_product_standard_price()})
                    self._cr.execute("""UPDATE stock_move SET date = '%s', state='done' WHERE id = %s;""" % (date, move.id))
        self.write({'state': 'done',
                    'date_done': date,
                    'min_date': date})
        _logger.info(_('Function Finished: picking force_assign_by_move'))
        return True


class StockMove(models.Model):
    _inherit = "stock.move"

    @api.depends('state')
    def _get_journal_entry_cnt(self):
        return True

    @api.multi
    def action_done(self):
        company = self.env.user.company_id
        if company.availability_compute_method == 'stock_move':
            # Компани Барааны боломжит нөөц тооцох арга -- > Барааны хөдөлгөөнөөс тооцох
            res = self.action_done_by_move()
        else:
            # Компани Барааны боломжит нөөц тооцох арга -- > Барааны тооллогоос тооцох
            res = super(StockMove, self).action_done()
        return res

    def _store_average_cost_price(self):
        """ Store the average price of the move on the move and product form (costing method 'real')"""
        for move in self:
            move.write({'price_unit': move.get_product_standard_price()})

    @api.multi
    def action_done_by_move(self):
        """ Барааны хөдөлгөөний үйлдлүүдлийг дуусгах функц
            Компани Барааны боломжит нөөц тооцох арга -- > Барааны хөдөлгөөнөөс тооцох"""
        # self.filtered(lambda move: move.state == 'draft').action_confirm()
        _logger.info(_('Function start: move action_done_by_move'))
        pickings = self.env['stock.picking']
        procurements = self.env['procurement.order']
        move_dest_ids = set()
        date = self._context.get('force_period_date', time.strftime(DEFAULT_SERVER_DATETIME_FORMAT))
        for move in self:
            if move.picking_id:
                pickings |= move.picking_id
            if float_compare(move.remaining_qty, 0, precision_rounding=move.product_id.uom_id.rounding) >= 0:
                move.product_price_update_before_done()
                move.enter_account_entry_move()
                if not move.price_unit:
                    move.write({'price_unit': move.get_product_standard_price()})
            # If the move has a destination, add it to the list to reserve
            if move.move_dest_id and move.move_dest_id.state in ('waiting', 'confirmed'):
                move_dest_ids.add(move.move_dest_id.id)
            if move.procurement_id:
                procurements |= move.procurement_id
            # set the move as done
            #self._cr.execute("""UPDATE stock_move SET date = '%s', state = 'done' WHERE id = %s""" % (date, move.id))
            # move.write({'state': 'done',
            #             'date': date})
        self.write({'state': 'done', 'date': date})
        procurements.check()
        # assign destination moves
        if move_dest_ids:
            self.browse(list(move_dest_ids)).action_assign()
        pickings.write({'state': 'done',
                        'date_done': date,
                        'min_date': date})
        self.check_delivery_date()
        _logger.info(_('Function done: move action_done_by_move'))
        return True

    def create_account_move(self, journal_id, date, name):
        # Журналын бичилт үүсгэх
        uid = self.create_uid and self.create_uid.id or self.env.user.id
        company_id = self.company_id and self.company_id.id or self.env.user.company_id.id
        currency_id = self.company_id and self.company_id.currency_id.id or self.env.user.company_id.currency_id.id
        move_name = False
        if not journal_id:
            raise ValidationError(_("Please check journal !!!"))
        journal_id = self.env['account.journal'].browse(journal_id)
        if journal_id.sequence_id:
            move_name = journal_id.sequence_id.with_context(ir_sequence_date=date[:10]).next_by_id()
        if not move_name:
            raise ValidationError(_("Please check sequence of journal %s !!!") % journal_id.name)
        self._cr.execute("""INSERT INTO account_move (create_uid, create_date, write_uid, write_date, name, date, state, ref, journal_id, company_id, currency_id) 
                            VALUES (%s, '%s', %s, '%s', '%s', '%s', 'posted', '%s', %s, %s, %s)
                            RETURNING id
                        """ % (uid, date, uid, date, move_name, date, name, journal_id[0].id, company_id, currency_id))

        # Журнал бичилтийн мөр /account.move.line/ үүсгэх өгөгдөл бэлдэх
        account_move_id = self._cr.fetchall()[0][0]
        return account_move_id

    def get_account_move(self, journal_id, date):
        # Журналын бичилтийн id-г олох функц
        _logger.info(_('Function start: ++++ move get_account_move'))
        if self.picking_id:
            if self.picking_id.account_move_id:
                account_move_id = self.picking_id.account_move_id.id
            else:
                account_move_id = self.create_account_move(journal_id, date, self.picking_id.name)
                self.picking_id.write({'account_move_id': account_move_id})
        else:
            name = self.name
            if self.inventory_id:
                name = self.inventory_id.name
            elif self.sudo().env['ir.module.module'].search([('name', '=', 'mrp'), ('state', 'in', ('installed', 'to upgrade'))]):
                if self.raw_material_production_id:
                    name = self.raw_material_production_id.name
                elif self.production_id:
                    name = self.production_id.name
            account_move_id = self.create_account_move(journal_id, date, name)
        _logger.info(_('Function done: ---- move get_account_move'))
        return account_move_id

    def _create_account_move_line(self, credit_account_id, debit_account_id, journal_id):
        _logger.info(_('Function start: move _create_account_move_line'))
        # Журналын мөр үүсгэх
        values = []
        # Шинжилгээний данс 2, Шинжилгээний тархалт дээр _prepare_account_move_line функцийг ашигласан
        # Шууд insert query бичихэд ШТ үед хэрхэн ашиглах шийдэл байхгүй тул үүнийг ашигласан
        move_lines = self._prepare_account_move_line(self.product_qty, self.price_unit, credit_account_id, debit_account_id)
        if move_lines:
            date = self.date or self._context.get('force_period_date', fields.Date.context_today(self))
            company_id = self.company_id and self.company_id.id or self.env.user.company_id.id
            uid = self.create_uid and self.create_uid.id or self.env.user.id
            account_move_id = self.get_account_move(journal_id, date)
            all_creates = True
            for line in move_lines:
                if line[0] != 0 or line[1] != 0:
                    all_creates = False
                name = line[2]['name'].replace("'", "''")
                ref = name
                if line[2]['ref']:
                    ref = line[2]['ref'].replace("'", "''")
                if all_creates:
                    values.append("""(%s, '%s', %s, '%s',
                                      %s, %s, %s, %s, %s, 
                                      %s, %s, %s, 
                                      %s, %s,
                                      '%s', '%s', '%s', '%s', 
                                      %s, %s, %s)""" % (
                                      uid, date, uid, date,
                                      company_id, account_move_id, journal_id, line[2]['account_id'], line[2]['analytic_account_id'] if line[2]['analytic_account_id'] else 'NULL',
                                      line[2]['partner_id'] if line[2]['partner_id'] else 'NULL', self.id, line[2]['product_id'] if line[2]['product_id'] else 'NULL',
                                      line[2]['product_uom_id'] if line[2]['product_uom_id'] else 'NULL', line[2]['quantity'],
                                      name, ref, date, date,
                                      line[2]['inventory_id'] if line[2]['inventory_id'] else 'NULL', line[2]['debit'], line[2]['credit']
                    ))
            if values:
                # Журнал бичилтийн мөр үүсгэх
                qry = """ INSERT INTO account_move_line (
                                create_uid, create_date, write_uid, write_date,
                                company_id, move_id, journal_id, account_id, analytic_account_id,
                                partner_id, stock_move_id, product_id, 
                                product_uom_id, quantity,
                                name, ref, date, date_maturity, 
                                inventory_id, debit, credit
                            ) VALUES  """
                qry += ", ".join(value for value in values)
                qry += " RETURNING id"
                self._cr.execute(qry)
                # Шинжилгээний бичилт үүсгэх
                results = self.env.cr.fetchall()
                aml_ids = [str(result[0]) for result in results]
                if aml_ids:
                    qry = """ INSERT INTO account_analytic_line (create_uid, create_date, write_uid, write_date, account_id, company_id, 
                                          amount, unit_amount, partner_id, name, ref, 
                                          general_account_id, move_id, product_id, product_uom_id, date, currency_id, amount_currency)
                                        SELECT %s, '%s', %s, '%s', analytic_account_id AS account_id, aml.company_id AS company_id,
                                            aml.credit - aml.debit AS amount, aml.quantity AS unit_amount,
                                            aml.partner_id AS partner_id, aml.name AS name, aml.ref AS ref, 
                                            aml.account_id AS general_account_id, aml.id AS move_id, aml.product_id AS product_id,
                                            aml.product_uom_id AS product_uom_id, aml.date AS date,
                                            aml.currency_id AS currency_id, aml.amount_currency AS amount_currency
                                        FROM account_move_line aml
                                        LEFT JOIN account_analytic_line aal ON aml.id = aal.move_id
                                        WHERE aml.analytic_account_id IS NOT NULL AND aal.id IS NULL AND aml.id IN (%s)
                                        ORDER BY aml.id
                                """ % (uid, date, uid, date, ",".join(aml_ids))
                    self._cr.execute(qry)
        _logger.info(_('Function done: move _create_account_move_line'))
        return True