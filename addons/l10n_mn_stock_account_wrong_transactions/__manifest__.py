# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Stock Wrong Transaction List",
    'version': '1.0',
    'depends': [
        'l10n_mn_stock_account',
        'l10n_mn_account_wrong_transaction',
    ],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Stock Wrong Transaction List
    """,
    'data': [
        'security/stock_account_wrong_transaction_security.xml',
        'security/ir.model.access.csv',
        'views/root_menu.xml',
        'views/fix_stock_move.xml',
        'views/stock_transit_order.xml',
        'views/account_move_views.xml',
        'views/stock_account_date_miss_views.xml',
        'views/stock_quantity_order.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
}