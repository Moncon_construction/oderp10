# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime
from odoo import api, fields, models, _
from odoo.exceptions import UserError

class AccountMove(models.Model):
    _inherit = "account.move"

    def get_account_moves_line(self):
        """
        Дебит болон кредит талдаа хоёуланд нь Бараа материалын дансанд бичигдсэнийг шүүж харуулна.
        """
        ids = []
        credit_move_ids = []
        self.env.cr.execute("""
                            SELECT am.id
                            FROM account_move as am
                            LEFT JOIN account_move_line as al on al.move_id = am.id
                            LEFT JOIN account_account aa on aa.id = al.account_id
                            LEFT JOIN account_account_type as type on aa.user_type_id = type.id and al.debit >= 0
                            LEFT JOIN account_account_type as type_b on aa.user_type_id = type_b.id and al.credit >= 0
                            WHERE type.is_product = True and al.debit>0
                            GROUP BY am.id, al.debit, al.credit 
                            ORDER BY am.id
                                """)
        debit = self.env.cr.fetchall()
        self.env.cr.execute("""
                            SELECT am.id
                            FROM account_move as am
                            LEFT JOIN account_move_line as al on al.move_id = am.id
                            LEFT JOIN account_account aa on aa.id = al.account_id
                            LEFT JOIN account_account_type as type on aa.user_type_id = type.id and al.debit >= 0
                            LEFT JOIN account_account_type as type_b on aa.user_type_id = type_b.id and al.credit >= 0
                            WHERE type.is_product = True and al.credit>0
                            GROUP BY am.id, al.debit, al.credit 
                            ORDER BY am.id
                                """)
        credit = self.env.cr.fetchall()
        if len(credit) > 0:
            credit_move_ids = [line[0] for line in credit]
        if len(debit) > 0:
            for line in debit:
                if line[0] in credit_move_ids:
                    ids.append(line[0])
        tree_id = self.env.ref("account.view_move_tree")
        form_id = self.env.ref("account.view_move_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Account Move'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }