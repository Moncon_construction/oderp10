# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _, tools

class StockAccountDateMiss(models.Model):
    _name = "stock.account.date.miss"
    _order = 'move_date'
    _auto = False

    stock_move_id = fields.Many2one('stock.move', 'Stock Move', readonly=1)
    product_id = fields.Many2one('product.template', 'Product', readonly=1, store=1)
    move_id = fields.Many2one('account.move', 'Account Move', readonly=1)
    picking_date = fields.Date('Picking Date', readonly=1)
    move_date = fields.Date('Move Date', readonly=1)
    picking_name = fields.Char('Picking name')
    quantity = fields.Float('Quantity')
    amount = fields.Float('Amount')
    location_id = fields.Many2one(
        'stock.location', 'Source Location', index=True, readonly=True)
    location_dest_id = fields.Many2one(
        'stock.location', 'Destination Location', index=True, readonly=True)

    @api.multi
    def return_stock_move(self):
        for line in self:
            res = self.env['stock.move'].search([('id', '=', line.stock_move_id.id)])
            return {
                'type': 'ir.actions.act_window',
                'name': _('Stock Picking'),
                'res_model': 'stock.move',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'view_id': False,
                'domain': [('id', 'in', res.ids)],
            }

    @api.multi
    def return_account_move(self):
        for line in self:
            res = self.env['account.move'].search([('id', '=', line.move_id.id)])
            return {
                'type': 'ir.actions.act_window',
                'name': _('Account Move'),
                'res_model': 'account.move',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'view_id': False,
                'domain': [('id', 'in', res.ids)],
            }

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self._cr, 'stock_account_date_miss')
        self._cr.execute("""
            CREATE VIEW stock_account_date_miss AS (
              SELECT MAX(s.id) as id,
                s.id as stock_move_id,
                s.product_id as product_id,
                am.id as move_id,
                s.date as picking_date,
                al.date as move_date,
                s.name as picking_name,
                s.product_uom_qty as quantity,
                SUM(s.price_unit * s.product_uom_qty) AS amount,
                s.location_id as location_id,
                s.location_dest_id as location_dest_id
              FROM stock_move  s
                JOIN account_move_line as al on  s.id  = al.stock_move_id
                JOIN account_move as am on al.move_id = am.id
              WHERE CAST(s.date AS DATE) != al.date
                GROUP BY am.id, s.id, s.location_id, al.id, s.location_dest_id, 
                s.product_id, s.date, al.date, s.product_uom_qty
            )""")
