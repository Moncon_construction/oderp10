# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime
from odoo import api, fields, models, _, tools
from odoo.exceptions import UserError


class StockMove(models.Model):
    _inherit = "stock.move"

    @api.model
    def is_module_installed(self, module_name=None):
        return module_name in self.env['ir.module.module'].sudo()._installed()

    def get_moves_without_acc_move_lines(self):
        ids = []
        self.env.cr.execute("select m.id from stock_move m left join account_move_line l on m.id=l.stock_move_id where l.id is null and m.state = 'done' and m.company_id = " + str(self.env.user.company_id.id))
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]
            for move in self.browse(ids):
                if move.location_id.usage == 'internal' and move.location_dest_id.usage == 'internal':
                    if not self.is_module_installed('l10n_mn_stock_account_cost_for_each_wh'):
                        ids.remove(move.id)
                    elif move.location_id.get_warehouse() == move.location_dest_id.get_warehouse():
                        ids.remove(move.id)
        tree_id = self.env.ref("stock.view_move_tree")
        form_id = self.env.ref("stock.view_move_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Stock Moves Without Account Move Line'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'stock.move',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }

    def get_moves_without_cost(self):
        ids = []
        self.env.cr.execute("select id from stock_move where price_unit=0 and state = 'done' and company_id = " + str(self.env.user.company_id.id) + " order by date")
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]
        tree_id = self.env.ref("stock.view_move_tree")
        form_id = self.env.ref("stock.view_move_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Stock Moves Without Cost'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'stock.move',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }

    def get_moves_difference_purchase_line(self):
        ids, real_ids = [], []

        currency_id = str(self.env.user.company_id.currency_id.id)
        decimal_places = self.env.user.company_id.currency_id.decimal_places
        prec = self.env['decimal.precision'].precision_get('Account')

        qry = "select sm.id from stock_move sm left join purchase_order_line pol on sm.purchase_line_id = pol.id "\
            "where sm.state='done' and ROUND((pol.price_subtotal/pol.product_qty)::numeric, " + str(decimal_places) + ") != ROUND(sm.price_unit::numeric, " + str(decimal_places) + ") and pol.currency_id = " + currency_id + \
            " UNION ALL select sm.id from stock_move sm left join purchase_order_line pol on sm.purchase_line_id = pol.id "\
            "left join purchase_order po on pol.order_id = po.id "\
            "where sm.state='done' and ROUND(((pol.price_subtotal/pol.product_qty) * po.currency_rate)::numeric, " + str(decimal_places) + ") != ROUND(sm.price_unit::numeric, " + str(decimal_places) + ") and pol.currency_id != " + currency_id + " and pol.company_id = " + str(self.env.user.company_id.id)

        self.env.cr.execute(qry)
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]

        moves = self.env['stock.move'].browse(ids)
        for move in moves:
            purchase_line = move.purchase_line_id
            line_amount = purchase_line.price_subtotal / purchase_line.product_qty

            if purchase_line.order_id.currency_id != purchase_line.order_id.company_id.currency_id:
                line_amount *= purchase_line.order_id.currency_rate

            # Нэмэлт зардалтай үеийн тооцооллыг шалгадаг болгов.
            self._cr.execute("SELECT id FROM ir_module_module WHERE name = 'l10n_mn_purchase_extra_cost' AND state IN ('installed', 'to upgrade')")
            results = self._cr.dictfetchall()
            if results and len(results) > 0 and purchase_line:
                extra_costs = self.env['purchase.extra.cost.line'].search([('purchase_line_id', '=', purchase_line.id)])
                for cost in extra_costs:
                    if not cost.extra_cost_id.item_id.non_cost_calc:
                        line_amount += (cost.unit_cost * cost.extra_cost_id.currency_rate)
            if abs(round(line_amount, decimal_places) - round(move.price_unit, decimal_places)) > 10 ** (-prec or -5):
                real_ids.append(move.id)

        tree_id = self.env.ref("stock.view_move_tree")
        form_id = self.env.ref("stock.view_move_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Stock Moves Difference Purchase Line'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'stock.move',
            'domain': [('id', 'in', real_ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }

    @api.multi
    def set_price_unit_from_pol(self):
        context = dict(self._context or {})
        currency_id = self.env.user.company_id.currency_id.id
        decimal_places = self.env.user.company_id.currency_id.decimal_places
        moves = self.env['stock.move'].browse(context.get('active_ids'))
        price_unit = 0
        for move in moves:
            # Худалдан авалтаас үүссэн барааны хөдөлгөөн эсэхийг шалгаж байна.
            if move.purchase_line_id:
                # Гадаад худалдан авалт эсэхийг шалгаж байна.
                if move.purchase_line_id.currency_id.id != currency_id:
                    # Гадаад худалдан авалт байвал дэд дүнг валютын ханшаар үржүүлж нэгж үнийг тооцно.
                    price_unit = round((move.purchase_line_id.price_subtotal / move.purchase_line_id.product_qty * move.purchase_line_id.order_id.currency_rate), decimal_places)
                else:
                    price_unit = round((move.purchase_line_id.price_subtotal / move.purchase_line_id.product_qty), decimal_places)
                move.price_unit = price_unit
                for quant in move.quant_ids:
                    quant.sudo().cost = price_unit
                account_move_lines = self.env['account.move.line'].search([('stock_move_id', '=', move.id)])
                # Холбоотой журналын мөрөөс журналын бичилтийг устгана.
                for account_move_line in account_move_lines:
                    move_line = account_move_line.move_id
                    move_line.button_cancel()
                    break
                # Холбоотой журналын бичилтийг шинэ дүнгээр нь update хийнэ. Ингэж бараа материалын товчоо тайлан, гүйлгээ балансыг тэнцүүлнэ.
                if account_move_lines:
                    for account_move_line in account_move_lines:
                        if account_move_line.debit > 0:
                            account_move_line.debit = move.price_unit * move.product_uom_qty
                        else:
                            account_move_line.credit = move.price_unit * move.product_uom_qty
                    move_line.post()
                else:
                    move.create_account_move_line()

    def get_moves_location_difference_stock_picking_type_location(self):
        ids, real_ids = [], []

        self.env.cr.execute("select a.id from stock_move a left join stock_picking_type b on a.picking_type_id = b.id left join stock_location c on a.location_id = c.id  where a.location_id != b.default_location_src_id and a.company_id = " + str(self.env.user.company_id.id) +
                            "UNION ALL "
                            "select a.id from stock_move a left join stock_picking_type b on a.picking_type_id = b.id left join stock_location c on a.location_dest_id = c.id  where a.location_dest_id != b.default_location_dest_id and a.company_id = " + str(self.env.user.company_id.id))
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]

        moves = self.env['stock.move'].browse(ids)
        for move in moves:
            real_ids.append(move.id)
        tree_id = self.env.ref("stock.view_move_tree")
        form_id = self.env.ref("stock.view_move_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Stock Moves Location Difference Stock Picking Type Location'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'stock.move',
            'domain': [('id', 'in', real_ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }


class StockMoveDiff(models.Model):
    _name = 'stock.move.diff'
    _description = 'Stock Moves Difference Account Move Line'
    _auto = False

    stock_move_id = fields.Many2one('stock.move', string='Stock Move')
    stock_move_amount = fields.Float(string='Stock Move Amount')
    account_move_line_id = fields.Many2one('account.move.line', string='Account Move Line')
    journal_amount = fields.Float(string='Journal Amount')
    diff = fields.Float(string='Difference', digits=(16, 3))
    company_id = fields.Many2one('res.company', 'Company')
    date = fields.Datetime(string="Date")

    @api.model_cr
    def init(self):
        _sql = """SELECT sm.id AS id, sm.id AS stock_move_id, ml.id AS account_move_line_id, sm.date AS date,
                         sm.product_uom_qty * sm.price_unit AS stock_move_amount, 
                         ml.debit AS journal_amount, 
                         sm.product_uom_qty * sm.price_unit - ml.debit AS diff, 
                         sm.company_id AS company_id 
                FROM stock_move sm 
                JOIN account_move_line ml ON (sm.id = ml.stock_move_id)  
                WHERE ml.debit > 0 AND ABS(sm.product_uom_qty * sm.price_unit - ml.debit) > 0.005 
               """
        tools.drop_view_if_exists(self._cr, self._table)
        self._cr.execute(
            """CREATE or REPLACE VIEW %s as (%s)""" %
            (self._table, _sql,))

    @api.multi
    def fix_acount_moves(self):
        account_move_line_obj = self.env['account.move.line']
        amount_editables = []
        to_zero = []

        # 1. 2-с олон мөрөөр журнал бичилт үүссэн байвал эхний 2 бичилтээс бусдын ДТ, КТ дүнг 0 болгох / Холбоотой шинжилгээний бичилтийн дүнг 0-лэх
        for move_diff in self:
            debit_line = account_move_line_obj.search([('stock_move_id', '=', move_diff.stock_move_id.id), ('debit', '>', 0)], limit=1)
            credit_line = account_move_line_obj.search([('stock_move_id', '=', move_diff.stock_move_id.id), ('credit', '>', 0)], limit=1)
            if debit_line and credit_line:
                update_amount_qry = """
                                    UPDATE account_move_line aml SET debit = sm.price_unit * sm.product_uom_qty, quantity = sm.product_uom_qty
                                    FROM stock_move sm
                                    WHERE sm.id = aml.stock_move_id AND sm.id IN (%s) AND debit > 0;
                                    UPDATE account_move_line aml SET credit = sm.price_unit * sm.product_uom_qty, quantity = sm.product_uom_qty
                                    FROM stock_move sm
                                    WHERE sm.id = aml.stock_move_id AND sm.id IN (%s) AND credit > 0;
                                """ % (move_diff.stock_move_id.id, move_diff.stock_move_id.id)
                self._cr.execute(update_amount_qry)
            # ДТ, КТ 2-уулаа 0 бол доорх шалгалт руу орно.
            if not debit_line and not credit_line:
                aml_ids = account_move_line_obj.search([('stock_move_id', '=', move.id)], order='id asc', limit=2)
                debit_line = aml_ids[0]
                credit_line = aml_ids[1]
            if len(move_diff.stock_move_id.account_line_ids) > 2 and debit_line and credit_line:
                for l in move_diff.stock_move_id.account_line_ids:
                    if l.id not in [debit_line.id, credit_line.id]:
                        to_zero.append(l.id)

        # 1. 2-с олон мөрөөр журнал бичилт үүссэн байвал эхний 2 бичилтээс бусдын ДТ, КТ дүнг 0 болгох / Холбоотой шинжилгээний бичилтийн дүнг 0-лэх
        if to_zero:
            aml_ids = ", ".join(str(aml_id) for aml_id in to_zero)
            update_zero_amount_qry = """
                    UPDATE account_move_line SET debit = 0, credit = 0 WHERE id IN (%s);
                    UPDATE account_move_line SET quantity = 0 WHERE move_id IN (%s);
                """ % (aml_ids, aml_ids)
            self._cr.execute(update_zero_amount_qry)