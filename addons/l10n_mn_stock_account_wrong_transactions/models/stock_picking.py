# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class Picking(models.Model):
    _inherit = "stock.picking"

    @api.multi
    def check_picking_internal_locations(self):
        for obj in self:
            if obj.picking_type_id.code == 'internal':
                location_warehouse = self.env['stock.warehouse'].search([('view_location_id', 'parent_of', obj.location_id.id)])
                location_dest_warehouse = self.env['stock.warehouse'].search([('view_location_id', 'parent_of', obj.location_dest_id.id)])
                if location_warehouse != location_dest_warehouse:
                    raise UserError(_('Internal picking must have same warehouse locations.\n Please do replenishment if you want to transfer between different warehouses.'))
                for line in obj.move_lines:
                    location_warehouse = self.env['stock.warehouse'].search([('view_location_id', 'parent_of', line.location_id.id)])
                    location_dest_warehouse = self.env['stock.warehouse'].search([('view_location_id', 'parent_of', line.location_dest_id.id)])
                    if location_warehouse != location_dest_warehouse:
                        raise UserError(_(
                            'Internal picking must have same warehouse locations.\n Please do replenishment if you want to transfer between different warehouses.'))

    @api.multi
    def action_confirm(self):
        self.check_picking_internal_locations()
        return super(Picking, self).action_confirm()

    @api.multi
    def do_new_transfer(self):
        self.check_picking_internal_locations()
        return super(Picking, self).do_new_transfer()
