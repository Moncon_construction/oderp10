# -*- coding: utf-8 -*-
from odoo import fields, models, api, _


class StockQuantityOrder(models.Model):
    _inherit = "stock.transit.order"

    @api.multi
    def get_income_quantity_diff_transit(self):
        ids = []
        self.env.cr.execute(
            """
                select id from (select o.id, m.state as state , 
				                SUM(case when pt.code = 'outgoing' then m.product_uom_qty else 0.0 end ) as outgoing,
				                SUM(case when pt.code = 'incoming' then m.product_uom_qty else 0.0 end ) as incoming
				                from stock_move m
				                join stock_picking p on m.picking_id = p.id
				                join stock_picking_type pt on pt.id = p.picking_type_id
				                join stock_transit_order o on p.transit_order_id = o.id
				                where m.state = 'done' or m.state = 'cancel'
				                group by m.state, o.id
				                order by id) AS st 
                GROUP BY id, incoming, outgoing, state 
                HAVING ABS(SUM(outgoing) - SUM(incoming)) > 0
                order by id
            """)
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]

        tree_id = self.env.ref("l10n_mn_stock.view_stock_transit_order_tree")
        form_id = self.env.ref("l10n_mn_stock.view_stock_transit_order_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Income Quantity Diff Transit'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'stock.transit.order',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }
