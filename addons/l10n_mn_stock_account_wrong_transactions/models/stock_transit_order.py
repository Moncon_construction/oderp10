#-*- coding: utf-8 -*-
from odoo import fields, models, api, _


class StockTransitOrder(models.Model):
    _inherit = "stock.transit.order"

    @api.multi
    def get_income_expense_diff_transit(self):
        ids = []
        self.env.cr.execute(
            """
                SELECT id   
                FROM (SELECT o.id AS id, 
                    CASE WHEN pt.code = 'outgoing' THEN m.price_unit * m.product_uom_qty ELSE 0.00 END AS outgoing,
                    CASE WHEN pt.code = 'incoming' THEN m.price_unit * m.product_uom_qty ELSE 0.00 END AS incoming
                    FROM stock_move m 
                    JOIN stock_picking p ON (m.picking_id = p.id) 
                    JOIN stock_picking_type pt ON (pt.id = p.picking_type_id) 
                    JOIN stock_transit_order o ON (p.transit_order_id = o.id) 
                    WHERE o.state = 'done' ) AS st
                GROUP BY id
                HAVING ABS(SUM(outgoing) - SUM(incoming)) > 0.001
                ORDER BY id
            """)
        fetched = self.env.cr.fetchall()
        if len(fetched) > 0:
            ids = [line[0] for line in fetched]

        tree_id = self.env.ref("l10n_mn_stock.view_stock_transit_order_tree")
        form_id = self.env.ref("l10n_mn_stock.view_stock_transit_order_form")

        return {
            'type': 'ir.actions.act_window',
            'name': _('Income Expense Diff Transit'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'stock.transit.order',
            'domain': [('id', 'in', ids)],
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
            'target': 'current',
        }