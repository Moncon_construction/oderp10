# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Stock Report",
    'version': '1.0',
    'depends': [
        'l10n_mn_stock_account',
        'l10n_mn_report'
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Агуулахын баримтууд
       * Бараа материалын товчоо тайлан
       * Бүртгэл хяналтын баримт
    """,
    'data': [
        'wizard/product_move_check_report_view.xml',
        'wizard/wh_report_by_size_view.xml',
        'views/product_ledger_report_view.xml',
        'views/product_ledger_stock_move_report_view.xml',
        'views/product_ledger_detail_report_view.xml',
        'views/product_move_check_report_view.xml',
        'wizard/report_expense_ledger_view.xml',
        'wizard/report_expense_income_ledger_view.xml',
        'views/product_interrupt_report_view.xml',
        'views/stock_config_settings_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}
