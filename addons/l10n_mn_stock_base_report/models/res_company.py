# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _


class Company(models.Model):
    _inherit = 'res.company'

    zero_qty_zero_cost = fields.Boolean('Show 0 when quantity is 0 on Stock Report', default=True)