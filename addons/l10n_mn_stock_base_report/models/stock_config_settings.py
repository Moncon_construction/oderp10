# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _


class StockSettings(models.TransientModel):
    _inherit = 'stock.config.settings'

    zero_qty_zero_cost = fields.Boolean(related='company_id.zero_qty_zero_cost', string="Show 0 when quantity is 0 on Stock Report")