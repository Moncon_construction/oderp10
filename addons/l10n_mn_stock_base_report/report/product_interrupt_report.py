# -*- coding: utf-8 -*-

from io import BytesIO
import base64
import time
from datetime import datetime, date, timedelta
from calendar import monthrange
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from odoo import api, fields, models, _
import pytz
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class ProductInterruptReport(models.TransientModel):
    _inherit = 'oderp.report.html.output'
    _name = 'product.interrupt.report'
    _description = "Product Interrupt Report"

    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env['res.company']._company_default_get('showroom.report'))
    date_from = fields.Date("Start Date", required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    date_to = fields.Date("End Date", required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    warehouse_ids = fields.Many2many('stock.warehouse', string="Warehouses")
    category_ids = fields.Many2many('product.category', string="Categories")
    product_ids = fields.Many2many('product.product', string="Products")

    @api.onchange("company_id")
    def _get_warehouse_and_categ_domain(self):
        domain = {}
        domain['warehouse_ids'] = [('company_id', '=', self.company_id.id)]
        _warehouses = []
        for warehouse in self.env.user.allowed_warehouses:
            _warehouses.append(warehouse.id)
        if _warehouses:
            domain['warehouse_ids'] = [('id', 'in', _warehouses), ('company_id', '=', self.company_id.id)]
        domain['category_ids'] = [('company_id', '=', self.company_id.id)]
        return {'domain': domain}

    @api.onchange("category_ids")
    def _get_product_domain(self):
        domain = {}
        if self.category_ids and len(self.category_ids) > 0:
            domain['product_ids'] = [('product_tmpl_id.categ_id', 'in', self.category_ids.ids)]
        return {'domain': domain}

    def _solve_single_value(self, list):
        if len(list) == 0:
            list.append(-1)
        if len(list) < 2:
            list.append(-1)
        return list

    def diff_month(self, d1, d2):
        return (d1.year - d2.year) * 12 + d1.month - d2.month

    def months_between(self, date_start, date_end):
        date_start = date_start.date()
        date_end = date_end.date()
        months = []
        if date_start > date_end:
            tmp = date_start
            date_start = date_end
            date_end = tmp

        tmp_date = date_start
        while tmp_date.month <= date_end.month or tmp_date.year < date_end.year:
            months.append(tmp_date)

            if tmp_date.month == 12:
                tmp_date = datetime(tmp_date.year + 1, 1, 1)
            else:
                tmp_date = datetime(tmp_date.year, tmp_date.month + 1, 1)
        return months

    def count_interrupt_day(self, product_id, start_date, end_date, location_id):
        # Огнооны хооронд хэдэн өдөр бараа 0 үлдэгдэлтэй байгааг буцаана.
        interrupt_day = 0
        diff = timedelta(days=1)
        mydate = start_date
        while mydate < end_date:
            qty = self.get_product_available_qty(location_id, mydate, product_id)
            # Барааны тасалдсан өдрийг тоолж байна
            if qty <= 0:
                interrupt_day += 1
            mydate += diff

        return interrupt_day

    def get_product_available_qty(self, location_ids, date_to, product_id):
        # Барааны үлдэгдлийг буцаана.
        available_qty = 0
        if product_id:
            product = product_id
            date_end = date_to
            if location_ids:
                self._cr.execute("SELECT sum(product_qty)::decimal(16,2) AS product_qty from stock_move "
                                 "where location_id = %s and product_id = %s and state = 'assigned'",
                                 (location_ids, product))
                moves = self._cr.fetchall()
                move_qty = sum([qty[0] if qty[0] is not None else 0 for qty in moves if moves is not list])
                if self.env.user.company_id.availability_compute_method == 'stock_quant':
                    self._cr.execute("SELECT sum(quantity)::decimal(16,2) AS product_qty from stock_history "
                                     "where date <= %s "
                                     "and location_id = %s and product_id = %s ",
                                     (date_end, location_ids, product))
                    fetched = self._cr.fetchall()
                    pro_qty = sum([qty[0] if qty[0] is not None else 0 for qty in fetched if fetched is not list])
                    available_qty = pro_qty - move_qty
                elif self.env.user.company_id.availability_compute_method == 'stock_move':
                    self._cr.execute("SELECT sum(product_qty)::decimal(16,2) AS product_qty from stock_move "
                                     "where date <= %s "
                                     "and location_id = %s and product_id = %s and state = 'done'",
                                     (date_end, location_ids, product))
                    fetched = self._cr.fetchall()
                    qty = sum([qty[0] if qty[0] is not None else 0 for qty in fetched if fetched is not list])
                    self._cr.execute("SELECT sum(product_qty)::decimal(16,2) AS product_qty from stock_move "
                                     "where date <= %s  "
                                     "and location_dest_id = %s and product_id = %s and state = 'done'",
                                     (date_end, location_ids, product))
                    in_moves = self._cr.fetchall()
                    qty2 = sum([qty2[0] if qty2[0] is not None else 0 for qty2 in in_moves if in_moves is not list])
                    available_qty = qty2 - qty - move_qty
            else:
                available_qty = 0
        return available_qty


    @api.multi
    def export_report(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Product Interrupt Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title_float = book.add_format(ReportExcelCellStyles.format_sub_title_float)
        # format_title_small = book.add_format(ReportExcelCellStyles.format_title_small)
        format_title_small = book.add_format(ReportExcelCellStyles.format_title_small)
        format_title_small_left = book.add_format(ReportExcelCellStyles.format_title_left)
        format_title_small_right = book.add_format(ReportExcelCellStyles.format_title_float)
        format_content_left = book.add_format(ReportExcelCellStyles.format_content_left)
        format_content_text_footer = book.add_format(ReportExcelCellStyles.format_filter)
        format_content_center = book.add_format(ReportExcelCellStyles.format_content_center)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_sub_title = book.add_format(ReportExcelCellStyles.format_sub_title)
        format_sub_title_center = book.add_format(ReportExcelCellStyles.format_sub_title_center)

        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('product_interrupt_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        rowx = 0

        # compute column
        sheet.set_column('A:A', 3)
        sheet.set_column('B:B', 15)
        sheet.set_column('C:C', 25)
        sheet.set_row(3, 40)

        # rowx += 2
        col = 0
        location_obj = self.env['stock.location']
        start_month = int(self.date_from[5:7])
        end_month = int(self.date_to[5:7])
        date_list = self.months_between(datetime.strptime(self.date_from, '%Y-%m-%d'),datetime.strptime(self.date_to, '%Y-%m-%d'))

        diff_month = self.diff_month(datetime.strptime(self.date_to, '%Y-%m-%d'),datetime.strptime(self.date_from, '%Y-%m-%d')) + 1
        ncol = (diff_month) * 2 + 3
        local = pytz.UTC
        if self.env.user.tz:
            local = pytz.timezone(self.env.user.tz)

        # create name
        sheet.merge_range(rowx, 0, rowx, ncol + 2, _(u'Company Name: %s') % self.company_id.name, format_filter)
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, ncol + 2, report_name.upper(), format_name)
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, 3, _(u'Duration: %s - %s') % (
            pytz.utc.localize(datetime.strptime(self.date_from, '%Y-%m-%d')).astimezone(local).strftime('%Y-%m-%d'),
            pytz.utc.localize(datetime.strptime(self.date_to, '%Y-%m-%d')).astimezone(local).strftime('%Y-%m-%d')), format_filter)

        rowx += 1
        add_row = 2
        sheet.merge_range(rowx, col, rowx + add_row, col, _('№'), format_title_small)
        sheet.merge_range(rowx, col + 1, rowx + add_row, col + 1, _('Default Code'), format_title_small)
        sheet.merge_range(rowx, col + 2, rowx + add_row, col + 2, _('Product Name'), format_title_small)

        total_day = 0
        total_interrupt_day = 0
        # Month
        if start_month == end_month:
            sheet.merge_range(rowx, col + 3, rowx, col + 4, _('Sale'), format_title_small)
        else:
            sheet.merge_range(rowx, col + 3, rowx, ncol - 1, _('Sale'), format_title_small)
        i = 0
        for line in date_list:
            sheet.merge_range(rowx + 1, 3 + i, rowx + 1, 4 + i, str(line.year) + '/' + str(line.month), format_title_small)
            sheet.merge_range(rowx + 2, 3 + i, rowx + 2, 4 + i, monthrange(line.year, line.month)[1], format_title_small)
            total_day += int(monthrange(line.year, line.month)[1])
            i += 2

        sheet.merge_range(rowx, ncol, rowx + add_row, ncol, _('Interrupt Day'), format_title_small)
        sheet.merge_range(rowx, ncol + 1, rowx + add_row, ncol + 1, _('Order Available Day'), format_title_small)
        sheet.merge_range(rowx, ncol + 2, rowx + add_row, ncol + 2, _('Quality Percent'), format_title_small)

        where = ''

        locations = []
        if self.product_ids:
            product_ids = self.product_ids.ids
            where += ' AND pp.id in (' + ','.join(map(str, product_ids)) + ') '
        if self.category_ids:
            category_ids = self.category_ids.ids
            where += ' AND pc.id in (' + ','.join(map(str, category_ids)) + ') '

        warehouse_dic = {}
        if self.warehouse_ids:
            for wh in self.warehouse_ids:
                loc_id = location_obj.search([('usage', '=', 'internal'), ('location_id', 'child_of', [wh.view_location_id.id])]).ids
                locations += loc_id
                if loc_id[0] not in warehouse_dic.keys():
                    warehouse_dic[loc_id[0]] = wh.id
        else:
            locations = self.env['stock.location'].search([('usage', '=', 'internal'), ('company_id', '=', self.env.user.company_id.id)]).ids

        if len(locations) > 0:
            locations = tuple(locations)

        self._cr.execute(
            "SELECT pt.name as pname, pt.default_code as default_code, pc.id AS cid, pc.name AS cname, pp.id as pid, sq.location_id as lid, "
            "CASE WHEN sw.code is null THEN  sw2.code ElSE sw.code END AS wcode, "
            "CASE WHEN sw.name is null THEN  sw2.name ElSE sw.name END AS wname "
            "FROM product_product pp "
            "LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id) "
            "LEFT JOIN product_category pc ON (pt.categ_id = pc.id) "
            "LEFT JOIN stock_quant sq ON (sq.product_id = pp.id) "
            "LEFT JOIN stock_location sl ON (sq.location_id = sl.id) "
            "LEFT JOIN stock_warehouse sw ON (sw.view_location_id = sl.id) "
            "LEFT JOIN stock_warehouse sw2 ON (sw2.lot_stock_id = sl.id) "
            "WHERE pt.active is true and pt.company_id = %s and sl.id in %s" + where +
            "GROUP BY sw2.code, sw2.name, pc.id, pt.name, pt.default_code,sw.id,pc.name, pp.id, sq.location_id, sw.code,sw.name "
            "ORDER BY sw2.code, pc.id, pc.name ", (self.company_id.id, locations))

        rowx += 3
        counter = 1
        lines = self._cr.dictfetchall()
        if lines:
            type_id = False
            categ_id = False

            for line in lines:
                # Эхний агуулах/ангиллыг зурна
                if not type_id and not categ_id:
                    counter = 1
                    sheet.merge_range(rowx, 0, rowx, ncol + 2, _(u'Warehouse') + u": %s" % line['wname'], format_title_small)
                    rowx += 1
                    sheet.merge_range(rowx, 0, rowx, ncol + 2, _(u'Categories') + u": %s" % line['cname'], format_sub_title)
                    rowx += 1
                # Дараагийн агуулах/ангиллыг зурна
                elif type_id and type_id != line['wcode']:
                    rowx += 1
                    sheet.merge_range(rowx, 0, rowx, ncol + 2, _(u'Warehouse') + u": %s" % line['wname'], format_title_small)
                    rowx += 1
                    sheet.merge_range(rowx, 0, rowx, ncol + 2, _(u'Categories') + u": %s" % line['cname'], format_sub_title)
                    rowx += 1
                # Дараагийн агуулах/ангиллыг зурна
                elif categ_id and categ_id != line['cid']:
                    sheet.merge_range(rowx, 0, rowx, ncol + 2, _(u'Categories') + u": %s" % line['cname'], format_sub_title)
                    rowx += 1
                sheet.write(rowx, 0, counter, format_content_center)
                sheet.write(rowx, 1, line['default_code'], format_content_center)
                sheet.write(rowx, 2, line['pname'], format_content_left)
                i = 0
                total_interrupt_day = 0
                for l in date_list:
                    date_str = str(l.year) + '-' + str(l.month) + '-' + str(l.day)
                    date = datetime.strptime(date_str, '%Y-%m-%d')
                    end_date = datetime.strptime(str(l.year) + '-' + str(l.month) + '-' + str(monthrange(l.year, l.month)[1]), '%Y-%m-%d')
                    # Үлдэгдэл 0 байсан өдрийг олно
                    interrupt_day = self.count_interrupt_day(line['pid'], date, end_date, line['lid'])
                    total_interrupt_day += interrupt_day
                    interrupt_percent = float(interrupt_day) / float(monthrange(l.year, l.month)[1]) * 100
                    sheet.write(rowx, 3 + i, interrupt_day, format_content_center)
                    sheet.write(rowx, 4 + i, str(round(interrupt_percent, 2)) + '%', format_content_float)
                    i += 2
                sheet.write(rowx, ncol, total_interrupt_day, format_content_float)
                sheet.write(rowx, ncol + 1, total_day - total_interrupt_day, format_content_float)
                sheet.write(rowx, ncol + 2, str(round(1 - float(float(total_interrupt_day)/float(total_day)), 2)) + '%', format_content_float)

                counter += 1
                rowx += 1
                type_id = line['wcode']
                categ_id = line['cid']

        rowx += 3
        sheet.merge_range(rowx, 1, rowx, ncol + 1, _('Made by:') + '........................' + '/' + self.env.user.name + '/', format_content_text_footer)
        rowx += 1
        sheet.merge_range(rowx, 1, rowx, ncol + 1, _('Check by:') + '........................' + '/' + '........................' + '/', format_content_text_footer)

        sheet.set_zoom(100)
        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report()