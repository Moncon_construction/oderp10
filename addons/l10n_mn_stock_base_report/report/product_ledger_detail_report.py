# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
import pytz
from datetime import datetime

from odoo import models, fields, api, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles  # @UnresolvedImport
from odoo.addons.l10n_mn_report.tools.tools import str_tuple  # @UnresolvedImport


class ProductLedgerDetailReport(models.TransientModel):
    """
       Бараа материалын дэлгэрэнгүй тайлан
    """
    _inherit = 'oderp.report.html.output'
    _name = 'product.ledger.detail.report'
    _description = "Product Ledger Detail Report"

    company_id = fields.Many2one('res.company', string='Company', required=True,
                                 default=lambda self: self.env['res.company']._company_default_get('hr.salary.rule.category.report'))
    date_from = fields.Datetime("Start Date", required=True, default=lambda *a: time.strftime('%Y-%m-01 16:00:00'))
    date_to = fields.Datetime("End Date", required=True, default=lambda *a: time.strftime('%Y-%m-%d 15:59:59'))
    warehouse_ids = fields.Many2many('stock.warehouse', string="Warehouses", required=True)
    category_ids = fields.Many2many('product.category', string="Categories")
    product_ids = fields.Many2many('product.product', string="Products")
    type = fields.Selection([('no_grouping', 'No grouping'), ('warehouse', 'Warehouse'), ('account', 'Account'), ('supplier', 'Supplier'), ('category', 'Category')], string="Type", required=True, default='warehouse')
    group_by = fields.Selection([('by_category', 'By Category'), ('by_supplier', 'By Supplier')], string="Group By", required=True, default='by_category')
    show_cost = fields.Boolean('Show Cost')
    show_purch_price = fields.Boolean('Show Purchase Price')
    show_barcode = fields.Boolean('Show Barcode')
    pricelist_id = fields.Many2one('product.pricelist', string='Price list')

    @api.onchange("company_id")
    def _get_warehouse_and_categ_domain(self):
        domain = {}
        domain['warehouse_ids'] = [('company_id', '=', self.company_id.id)]
        _warehouses = []
        for warehouse in self.env.user.allowed_warehouses:
            _warehouses.append(warehouse.id)
        if _warehouses:
            domain['warehouse_ids'] = [('id', 'in', _warehouses), ('company_id', '=', self.company_id.id)]
        domain['category_ids'] = [('company_id', '=', self.company_id.id)]
        return {'domain': domain}

    @api.multi
    def check_module_installed(self, module_name):
        for record in self:  # @UnusedVariable
            module = self.sudo().env['ir.module.module'].search([
                ('name', '=', module_name)
            ])
            if module and module.state == 'installed' or module.state == 'to upgrade':
                return True
            else:
                return False

    @api.depends('show_purch_price')
    def change_show_cost_value(self):
        for obj in self:
            if obj.show_purch_price:
                obj.show_cost = False

    @api.depends('show_cost')
    def change_show_purch_price_alue(self):
        for obj in self:
            if obj.show_cost:
                obj.show_purch_price = False

    def module_counter(self, boolean_list):
        count = 0
        for item in boolean_list:
            if item:
                count += 1
        return count

    def write_qty_immediately(self, can_write, sheet, rowx, col, qty, format):
        if can_write:
            sheet.write(rowx, col, qty, format)
            col += 1
        return sheet, col

    def write_qty_immediately_from_line(self, can_write, sheet, rowx, col, line, qty, format):
        if can_write:
            sheet.write(rowx, col, line[qty], format)
            col += 1
        return sheet, col

    def write_qty_and_cost_immediately(self, can_write, sheet, rowx, col, qty, cost, format):
        if can_write:
            sheet.write(rowx, col, qty, format)
            sheet.write(rowx, col + 1, cost, format)
            col += 2
        return sheet, col

    def write_qty_and_cost(self, can_write, sheet, rowx, col, line, qty, cost, format):
        if can_write:
            sheet.write(rowx, col, line[qty], format)
            sheet.write(rowx, col + 1, 0 if line[qty] == 0 else line[cost], format)
            col += 2
        return sheet, col

    def build_column(self, can_build, sheet, rowx, col, column_name, format):
        if can_build:
            if self.show_cost or self.show_purch_price:
                sheet.merge_range(rowx + 1, col, rowx + 1, col + 1, column_name, format)
                sheet.write(rowx + 2, col, _('Quantity'), format)
                sheet.write(rowx + 2, col + 1, _('Cost'), format) if self.show_cost else sheet.write(rowx + 2, col + 1, _('Purchase Price'), format)
                col += 2
            else:
                sheet.write(rowx + 1, col, column_name, format)
                sheet.write(rowx + 2, col, _('Quantity'), format)
                col += 1
        return sheet, col

    def build_subtotal_row_with_cost(self, sheet, rowx, col, format_header, format, \
                                     is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                                     subtotal_start_qty, subtotal_start_cost, subtotal_in_supplier_qty, subtotal_in_supplier_cost, \
                                     subtotal_in_swap_qty, subtotal_in_swap_cost, subtotal_in_production_qty, subtotal_in_production_cost, \
                                     subtotal_in_transit_qty, subtotal_in_transit_cost, subtotal_in_customer_qty, subtotal_in_customer_cost, \
                                     subtotal_in_inventory_qty, subtotal_in_inventory_cost, subtotal_other_income, subtotal_other_income_cost, \
                                     subtotal_qty, subtotal_cost, subtotal_out_customer_qty, subtotal_out_customer_cost, \
                                     subtotal_out_swap_qty, subtotal_out_swap_cost, subtotal_out_pos_customer_qty, subtotal_out_pos_customer_cost, \
                                     subtotal_out_transit_qty, subtotal_out_transit_cost, subtotal_out_prod_ex_qty, subtotal_out_prod_ex_cost, \
                                     subtotal_out_supplier_qty, subtotal_out_supplier_cost, subtotal_out_inventory_qty, subtotal_out_inventory_cost, \
                                     subtotal_out_production_qty, subtotal_out_production_cost, subtotal_other_expense, subtotal_other_expense_cost, \
                                     subtotal_ex_qty, subtotal_ex_cost, subtotal_end_qty, subtotal_end_cost):
        sheet.merge_range(rowx, 0, rowx, col, _('Sub Total'), format_header)
        col += 1

        sheet, col = self.write_qty_and_cost_immediately(True, sheet, rowx, col, subtotal_start_qty, subtotal_start_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_purchase, sheet, rowx, col, subtotal_in_supplier_qty, subtotal_in_supplier_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_swap, sheet, rowx, col, subtotal_in_swap_qty, subtotal_in_swap_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_mrp, sheet, rowx, col, subtotal_in_production_qty, subtotal_in_production_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_stock, sheet, rowx, col, subtotal_in_transit_qty, subtotal_in_transit_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_sale, sheet, rowx, col, subtotal_in_customer_qty, subtotal_in_customer_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_stock, sheet, rowx, col, subtotal_in_inventory_qty, subtotal_in_inventory_cost, format)

        sheet, col = self.write_qty_and_cost_immediately(True, sheet, rowx, col, subtotal_other_income, subtotal_other_income_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(True, sheet, rowx, col, subtotal_qty, subtotal_cost, format)

        sheet, col = self.write_qty_and_cost_immediately(is_sale, sheet, rowx, col, subtotal_out_customer_qty, subtotal_out_customer_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_swap, sheet, rowx, col, subtotal_out_swap_qty, subtotal_out_swap_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_pos, sheet, rowx, col, subtotal_out_pos_customer_qty, subtotal_out_pos_customer_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_stock, sheet, rowx, col, subtotal_out_transit_qty, subtotal_out_transit_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_product_expense, sheet, rowx, col, subtotal_out_prod_ex_qty, subtotal_out_prod_ex_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_purchase, sheet, rowx, col, subtotal_out_supplier_qty, subtotal_out_supplier_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_stock, sheet, rowx, col, subtotal_out_inventory_qty, subtotal_out_inventory_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_mrp, sheet, rowx, col, subtotal_out_production_qty, subtotal_out_production_cost, format)

        sheet, col = self.write_qty_and_cost_immediately(True, sheet, rowx, col, subtotal_other_expense, subtotal_other_expense_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(True, sheet, rowx, col, subtotal_ex_qty, subtotal_ex_cost, format)

        sheet.write(rowx, col, 0, format)
        col += 1
        sheet, col = self.write_qty_and_cost_immediately(True, sheet, rowx, col, subtotal_end_qty, subtotal_end_cost, format)

        return sheet, col

    def build_subtotal_row(self, sheet, rowx, col, format_header, format, \
                           is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                           subtotal_start_qty, subtotal_in_supplier_qty, subtotal_in_swap_qty, subtotal_in_production_qty, \
                           subtotal_in_transit_qty, subtotal_in_customer_qty, subtotal_in_inventory_qty, subtotal_other_income, \
                           subtotal_qty, subtotal_out_customer_qty, subtotal_out_swap_qty, subtotal_out_pos_customer_qty, \
                           subtotal_out_transit_qty, subtotal_out_prod_ex_qty, subtotal_out_supplier_qty, subtotal_out_inventory_qty, \
                           subtotal_out_production_qty, subtotal_other_expense, subtotal_ex_qty, subtotal_end_qty):
        sheet.merge_range(rowx, 0, rowx, col, _('Sub Total'), format_header)
        col += 1

        sheet, col = self.write_qty_immediately(True, sheet, rowx, col, subtotal_start_qty, format)
        sheet, col = self.write_qty_immediately(is_purchase, sheet, rowx, col, subtotal_in_supplier_qty, format)
        sheet, col = self.write_qty_immediately(is_swap, sheet, rowx, col, subtotal_in_swap_qty, format)
        sheet, col = self.write_qty_immediately(is_mrp, sheet, rowx, col, subtotal_in_production_qty, format)
        sheet, col = self.write_qty_immediately(is_stock, sheet, rowx, col, subtotal_in_transit_qty, format)
        sheet, col = self.write_qty_immediately(is_sale, sheet, rowx, col, subtotal_in_customer_qty, format)
        sheet, col = self.write_qty_immediately(is_stock, sheet, rowx, col, subtotal_in_inventory_qty, format)

        sheet, col = self.write_qty_immediately(True, sheet, rowx, col, subtotal_other_income, format)
        sheet, col = self.write_qty_immediately(True, sheet, rowx, col, subtotal_qty, format)

        sheet, col = self.write_qty_immediately(is_sale, sheet, rowx, col, subtotal_out_customer_qty, format)
        sheet, col = self.write_qty_immediately(is_swap, sheet, rowx, col, subtotal_out_swap_qty, format)
        sheet, col = self.write_qty_immediately(is_pos, sheet, rowx, col, subtotal_out_pos_customer_qty, format)
        sheet, col = self.write_qty_immediately(is_stock, sheet, rowx, col, subtotal_out_transit_qty, format)
        sheet, col = self.write_qty_immediately(is_product_expense, sheet, rowx, col, subtotal_out_prod_ex_qty, format)
        sheet, col = self.write_qty_immediately(is_purchase, sheet, rowx, col, subtotal_out_supplier_qty, format)
        sheet, col = self.write_qty_immediately(is_stock, sheet, rowx, col, subtotal_out_inventory_qty, format)
        sheet, col = self.write_qty_immediately(is_mrp, sheet, rowx, col, subtotal_out_production_qty, format)

        sheet, col = self.write_qty_immediately(True, sheet, rowx, col, subtotal_other_expense, format)
        sheet, col = self.write_qty_immediately(True, sheet, rowx, col, subtotal_ex_qty, format)
        sheet, col = self.write_qty_immediately(True, sheet, rowx, col, subtotal_end_qty, format)

        return sheet, col

    def build_total_row_with_cost(self, sheet, rowx, col, format_header, format, \
                                  is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                                  total_start_qty, total_start_cost, total_in_supplier_qty, total_in_supplier_cost, \
                                  total_in_swap_qty, total_in_swap_cost, total_in_production_qty, total_in_production_cost, \
                                  total_in_transit_qty, total_in_transit_cost, total_in_customer_qty, total_in_customer_cost, \
                                  total_in_inventory_qty, total_in_inventory_cost, total_other_income, total_other_income_cost, \
                                  total_qty, total_cost, total_out_customer_qty, total_out_customer_cost, \
                                  total_out_swap_qty, total_out_swap_cost, total_out_pos_customer_qty, total_out_pos_customer_cost, \
                                  total_out_transit_qty, total_out_transit_cost, total_out_prod_ex_qty, total_out_prod_ex_cost, \
                                  total_out_supplier_qty, total_out_supplier_cost, total_out_inventory_qty, total_out_inventory_cost, \
                                  total_out_production_qty, total_out_production_cost, total_other_expense, total_other_expense_cost, \
                                  total_ex_qty, total_ex_cost, total_end_qty, total_end_cost):
        sheet.merge_range(rowx, 0, rowx, col, _('Total'), format_header)
        col += 1

        sheet, col = self.write_qty_and_cost_immediately(True, sheet, rowx, col, total_start_qty, total_start_cost, format)

        sheet, col = self.write_qty_and_cost_immediately(is_purchase, sheet, rowx, col, total_in_supplier_qty, total_in_supplier_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_swap, sheet, rowx, col, total_in_swap_qty, total_in_swap_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_mrp, sheet, rowx, col, total_in_production_qty, total_in_production_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_stock, sheet, rowx, col, total_in_transit_qty, total_in_transit_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_sale, sheet, rowx, col, total_in_customer_qty, total_in_customer_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_stock, sheet, rowx, col, total_in_inventory_qty, total_in_inventory_cost, format)

        sheet, col = self.write_qty_and_cost_immediately(True, sheet, rowx, col, total_other_income, total_other_income_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(True, sheet, rowx, col, total_qty, total_cost, format)

        sheet, col = self.write_qty_and_cost_immediately(is_sale, sheet, rowx, col, total_out_customer_qty, total_out_customer_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_swap, sheet, rowx, col, total_out_swap_qty, total_out_swap_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_pos, sheet, rowx, col, total_out_pos_customer_qty, total_out_pos_customer_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_stock, sheet, rowx, col, total_out_transit_qty, total_out_transit_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_product_expense, sheet, rowx, col, total_out_prod_ex_qty, total_out_prod_ex_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_purchase, sheet, rowx, col, total_out_supplier_qty, total_out_supplier_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_stock, sheet, rowx, col, total_out_inventory_qty, total_out_inventory_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(is_mrp, sheet, rowx, col, total_out_production_qty, total_out_production_cost, format)

        sheet, col = self.write_qty_and_cost_immediately(True, sheet, rowx, col, total_other_expense, total_other_expense_cost, format)
        sheet, col = self.write_qty_and_cost_immediately(True, sheet, rowx, col, total_ex_qty, total_ex_cost, format)

        sheet.write(rowx, col, 0, format)
        col += 1
        sheet, col = self.write_qty_and_cost_immediately(True, sheet, rowx, col, total_end_qty, total_end_cost, format)

        return sheet, col

    def build_total_row(self, sheet, rowx, col, format_header, format, \
                        is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                        total_start_qty, total_in_supplier_qty, total_in_swap_qty, total_in_production_qty, \
                        total_in_transit_qty, total_in_customer_qty, total_in_inventory_qty, \
                        total_other_income, total_qty, total_out_pos_customer_qty, \
                        total_out_customer_qty, total_out_swap_qty, total_out_transit_qty, \
                        total_out_prod_ex_qty, total_out_supplier_qty, total_out_inventory_qty, total_out_production_qty, \
                        total_other_expense, total_ex_qty, total_end_qty):
        sheet.merge_range(rowx, 0, rowx, col, _('Total'), format_header)
        col += 1

        sheet, col = self.write_qty_immediately(True, sheet, rowx, col, total_start_qty, format)
        sheet, col = self.write_qty_immediately(is_purchase, sheet, rowx, col, total_in_supplier_qty, format)
        sheet, col = self.write_qty_immediately(is_swap, sheet, rowx, col, total_in_swap_qty, format)
        sheet, col = self.write_qty_immediately(is_mrp, sheet, rowx, col, total_in_production_qty, format)
        sheet, col = self.write_qty_immediately(is_stock, sheet, rowx, col, total_in_transit_qty, format)
        sheet, col = self.write_qty_immediately(is_sale, sheet, rowx, col, total_in_customer_qty, format)
        sheet, col = self.write_qty_immediately(is_stock, sheet, rowx, col, total_in_inventory_qty, format)

        sheet, col = self.write_qty_immediately(True, sheet, rowx, col, total_other_income, format)
        sheet, col = self.write_qty_immediately(True, sheet, rowx, col, total_qty, format)

        sheet, col = self.write_qty_immediately(is_sale, sheet, rowx, col, total_out_customer_qty, format)
        sheet, col = self.write_qty_immediately(is_swap, sheet, rowx, col, total_out_swap_qty, format)
        sheet, col = self.write_qty_immediately(is_pos, sheet, rowx, col, total_out_pos_customer_qty, format)
        sheet, col = self.write_qty_immediately(is_stock, sheet, rowx, col, total_out_transit_qty, format)
        sheet, col = self.write_qty_immediately(is_product_expense, sheet, rowx, col, total_out_prod_ex_qty, format)
        sheet, col = self.write_qty_immediately(is_purchase, sheet, rowx, col, total_out_supplier_qty, format)
        sheet, col = self.write_qty_immediately(is_stock, sheet, rowx, col, total_out_inventory_qty, format)
        sheet, col = self.write_qty_immediately(is_mrp, sheet, rowx, col, total_out_production_qty, format)

        sheet, col = self.write_qty_immediately(True, sheet, rowx, col, total_other_expense, format)
        sheet, col = self.write_qty_immediately(True, sheet, rowx, col, total_ex_qty, format)
        sheet, col = self.write_qty_immediately(True, sheet, rowx, col, total_end_qty, format)

        return sheet, col

    def build_debit_with_cost(self, sheet, col, rowx, format, line, \
                              is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense):
        sheet, col = self.write_qty_and_cost(is_sale, sheet, rowx, col, line, 'out_customer_qty', 'out_customer_cost', format)
        sheet, col = self.write_qty_and_cost(is_swap, sheet, rowx, col, line, 'out_swap_qty', 'out_swap_cost', format)
        sheet, col = self.write_qty_and_cost(is_pos, sheet, rowx, col, line, 'out_pos_customer_qty', 'out_pos_customer_cost', format)
        sheet, col = self.write_qty_and_cost(is_stock, sheet, rowx, col, line, 'out_transit_qty', 'out_transit_cost', format)
        sheet, col = self.write_qty_and_cost(is_product_expense, sheet, rowx, col, line, 'out_prod_ex_qty', 'out_prod_ex_cost', format)
        sheet, col = self.write_qty_and_cost(is_purchase, sheet, rowx, col, line, 'out_supplier_qty', 'out_supplier_cost', format)
        sheet, col = self.write_qty_and_cost(is_stock, sheet, rowx, col, line, 'out_inventory_qty', 'out_inventory_cost', format)
        sheet, col = self.write_qty_and_cost(is_mrp, sheet, rowx, col, line, 'out_production_qty', 'out_production_cost', format)
        return sheet, col

    def build_debit(self, sheet, col, rowx, format, line, \
                    is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense):
        sheet, col = self.write_qty_immediately_from_line(is_sale, sheet, rowx, col, line, 'out_customer_qty', format)
        sheet, col = self.write_qty_immediately_from_line(is_swap, sheet, rowx, col, line, 'out_swap_qty', format)
        sheet, col = self.write_qty_immediately_from_line(is_pos, sheet, rowx, col, line, 'out_pos_customer_qty', format)
        sheet, col = self.write_qty_immediately_from_line(is_stock, sheet, rowx, col, line, 'out_transit_qty', format)
        sheet, col = self.write_qty_immediately_from_line(is_product_expense, sheet, rowx, col, line, 'out_prod_ex_qty', format)
        sheet, col = self.write_qty_immediately_from_line(is_purchase, sheet, rowx, col, line, 'out_supplier_qty', format)
        sheet, col = self.write_qty_immediately_from_line(is_stock, sheet, rowx, col, line, 'out_inventory_qty', format)
        sheet, col = self.write_qty_immediately_from_line(is_mrp, sheet, rowx, col, line, 'out_production_qty', format)
        return sheet, col

    def build_credit_with_cost(self, sheet, col, rowx, format, line, \
                              is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense):
        sheet, col = self.write_qty_and_cost(is_purchase, sheet, rowx, col, line, 'in_supplier_qty', 'in_supplier_cost', format)
        sheet, col = self.write_qty_and_cost(is_swap, sheet, rowx, col, line, 'in_swap_qty', 'in_swap_cost', format)
        sheet, col = self.write_qty_and_cost(is_mrp, sheet, rowx, col, line, 'in_production_qty', 'in_production_cost', format)
        sheet, col = self.write_qty_and_cost(is_stock, sheet, rowx, col, line, 'in_transit_qty', 'in_transit_cost', format)
        sheet, col = self.write_qty_and_cost(is_sale, sheet, rowx, col, line, 'in_customer_qty', 'in_customer_cost', format)
        sheet, col = self.write_qty_and_cost(is_stock, sheet, rowx, col, line, 'in_inventory_qty', 'in_inventory_cost', format)
        return sheet, col

    def build_credit(self, sheet, col, rowx, format, line, \
                    is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense):
        sheet, col = self.write_qty_immediately_from_line(is_sale, sheet, rowx, col, line, 'in_supplier_qty', format)
        sheet, col = self.write_qty_immediately_from_line(is_swap, sheet, rowx, col, line, 'in_swap_qty', format)
        sheet, col = self.write_qty_immediately_from_line(is_pos, sheet, rowx, col, line, 'in_production_qty', format)
        sheet, col = self.write_qty_immediately_from_line(is_stock, sheet, rowx, col, line, 'in_transit_qty', format)
        sheet, col = self.write_qty_immediately_from_line(is_product_expense, sheet, rowx, col, line, 'in_customer_qty', format)
        sheet, col = self.write_qty_immediately_from_line(is_purchase, sheet, rowx, col, line, 'in_inventory_qty', format)
        return sheet, col

    @api.multi
    def export_report(self):
        location_obj = self.env['stock.location']

        is_mrp = self.check_module_installed('mrp')
        is_sale = self.check_module_installed('sale')
        is_pos = self.check_module_installed('point_of_sale')
        is_purchase = self.check_module_installed('purchase')
        is_stock = self.check_module_installed('l10n_mn_stock')
        is_swap = self.check_module_installed('l10n_mn_swap_order')
        is_product_expense = self.check_module_installed('l10n_mn_product_expense')
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        # create name
        report_name = _('Product Ledger Detail Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_filter_right = book.add_format(ReportExcelCellStyles.format_filter_right)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_title_float = book.add_format(ReportExcelCellStyles.format_title_float)

        format_group = book.add_format(ReportExcelCellStyles.format_group)
        format_group_left = book.add_format(ReportExcelCellStyles.format_group_left)
        format_group_right = book.add_format(ReportExcelCellStyles.format_group_right)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)

        format_content_center = book.add_format(ReportExcelCellStyles.format_content_center)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_bold_left = book.add_format(ReportExcelCellStyles.format_content_bold_left)
        format_content_bold_number = book.add_format(ReportExcelCellStyles.format_content_bold_number)
        # parameters
        seq = 1
        rowx = rowd = 0
        locations = []

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('product_ledger_detail_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(time.strftime('%Y-%m-%d'))
        sheet.set_portrait()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.39, 0.39, 0.39, 0.39)  # 1cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
#         A a B b C c D d E e F f G g H h I i J j K k L l M m N n O o P p Q q R r S s T t U u V v W w X x Y y Z z

        # compute column
        sheet.set_column('A:A', 8)
        sheet.set_column('B:B', 10)
        if self.show_barcode:
            sheet.set_column('C:C', 10)
            sheet.set_column('D:D', 27)
            sheet.set_column('E:E', 11)
            sheet.set_column('F:F', 10)
            sheet.set_column('G:AT', 13)
        else:
            sheet.set_column('C:C', 27)
            sheet.set_column('D:D', 11)
            sheet.set_column('E:E', 10)
            sheet.set_column('F:AT', 13)

        local = pytz.UTC
        if self.env.user.tz:
            local = pytz.timezone(self.env.user.tz)

        date_from = pytz.utc.localize(datetime.strptime(self.date_from,'%Y-%m-%d %H:%M:%S')).astimezone(local).strftime('%Y-%m-%d %H:%M:%S')
        date_to = pytz.utc.localize(datetime.strptime(self.date_to,'%Y-%m-%d %H:%M:%S')).astimezone(local).strftime('%Y-%m-%d %H:%M:%S')

        in_counter = self.module_counter([is_purchase, is_swap, is_mrp, is_stock, is_sale, is_stock])
        out_counter = self.module_counter([is_sale, is_swap, is_pos, is_stock, is_product_expense, is_purchase, is_stock, is_mrp])
        total_col_range_with_cost = (in_counter + out_counter) * 2 + (17 if self.show_barcode else 16)
        total_col_range = in_counter + out_counter + (9 if self.show_barcode else 8) + 1
        # create name
        if self.show_cost or self.show_purch_price:
            sheet.merge_range(rowx, 0, rowx, total_col_range_with_cost, '%s' % self.company_id.name, format_filter)
            rowx += 2
            sheet.merge_range(rowx, 0, rowx, total_col_range_with_cost, report_name.upper(), format_name)
            rowx += 2
            sheet.merge_range(rowx, 0, rowx, total_col_range_with_cost, '%s - %s' % (date_from, date_to), format_filter_right)
            rowx += 1
        else:
            sheet.merge_range(rowx, 0, rowx, total_col_range, '%s' % self.company_id.name, format_filter)
            rowx += 2
            sheet.merge_range(rowx, 0, rowx, total_col_range, report_name.upper(), format_name)
            rowx += 2
            sheet.merge_range(rowx, 0, rowx, total_col_range, '%s - %s' % (date_from, date_to), format_filter_right)
            rowx += 1

        # table header
        col = 0
        sheet.merge_range(rowx, col, rowx + 2, col, _('Seq'), format_group)
        col += 1
        sheet.merge_range(rowx, col, rowx + 2, col, _('Product code'), format_group)
        col += 1

        if self.show_barcode:
            sheet.merge_range(rowx, col, rowx + 2, col, _('Barcode'), format_group)
            col += 1
#         sheet.merge_range(rowx, col, rowx + 2, col, _('Product Part Number'), format_group)
#         col += 1
        sheet.merge_range(rowx, col, rowx + 2, col, _('Product name'), format_group)
        col += 1
        sheet.merge_range(rowx, col, rowx + 2, col, _('Unit of measure'), format_group)
        col += 1

        if self.show_cost or self.show_purch_price:
            sheet.merge_range(rowx, col, rowx + 1, col + 1, _('Initial Balance'), format_group)
            sheet.write(rowx + 2, col, _('Quantity'), format_group)
            col += 1
            sheet.write(rowx + 2, col, _('Cost'), format_group) if self.show_cost else sheet.write(rowx + 2, col, _('Purchase Price'), format_group)
            col += 1
        else:
            sheet.merge_range(rowx, col, rowx + 1, col, _('Initial Balance'), format_group)
            sheet.write(rowx + 2, col, _('Quantity'), format_group)
            col += 1

#             ОРЛОГО
#             Худалдан авалт        Бараа солилцоо        Үйлдвэрлэл        Нөхөн дүүргэлт        Борлуулалтын буцаалт        Тоолого        Бусад        Нийт орлого
        if self.show_cost or self.show_purch_price:
            in_counter = in_counter * 2 + 2
        sheet.merge_range(rowx, col, rowx, col + 1 + in_counter, _('Income'), format_group)

        sheet, col = self.build_column(is_purchase, sheet, rowx, col, _('Purchase'), format_group)
        sheet, col = self.build_column(is_swap, sheet, rowx, col, _('Product Swap'), format_group)
        sheet, col = self.build_column(is_mrp, sheet, rowx, col, _('Production'), format_group)
        sheet, col = self.build_column(is_stock, sheet, rowx, col, _('Stock Transit Order'), format_group)
        sheet, col = self.build_column(is_sale, sheet, rowx, col, _('Sale Return'), format_group)
        sheet, col = self.build_column(is_stock, sheet, rowx, col, _('Stock Inventory'), format_group)
        sheet, col = self.build_column(True, sheet, rowx, col, _('Other'), format_group)
        sheet, col = self.build_column(True, sheet, rowx, col, _('Total Income'), format_group)

#             Зардал
# Борлуулалт        Бараа солилцоо        Борлуулалтын цэг        Нөхөн дүүргэлт        Шаардах        Худалдан авалтын буцаалт        Тооллого        Үйлдвэрлэлийн ТЭМ зарлага
        if self.show_cost or self.show_purch_price:
            out_counter = out_counter * 2 + 3
        else:
            out_counter = out_counter + 1
        sheet.merge_range(rowx, col, rowx, col + out_counter, _('Expense'), format_group)

        sheet, col = self.build_column(is_sale, sheet, rowx, col, _('Sale'), format_group)
        sheet, col = self.build_column(is_swap, sheet, rowx, col, _('Product Swap'), format_group)
        sheet, col = self.build_column(is_pos, sheet, rowx, col, _('Point of sale'), format_group)
        sheet, col = self.build_column(is_stock, sheet, rowx, col, _('Stock Transit Order'), format_group)
        sheet, col = self.build_column(is_product_expense, sheet, rowx, col, _('Product Expense'), format_group)
        sheet, col = self.build_column(is_purchase, sheet, rowx, col, _('Purchase return'), format_group)
        sheet, col = self.build_column(is_stock, sheet, rowx, col, _('Stock Inventory'), format_group)
        sheet, col = self.build_column(is_mrp, sheet, rowx, col, _('Raw materials'), format_group)
        sheet, col = self.build_column(True, sheet, rowx, col, _('Other'), format_group)
        sheet, col = self.build_column(True, sheet, rowx, col, _('Total Expense'), format_group)

        if self.show_cost or self.show_purch_price:
            sheet.merge_range(rowx, col, rowx + 1, col + 2, _('End Balance'), format_group)
            sheet.write(rowx + 2, col, _('Cost Unit') if self.show_cost else _('Purchase Price Unit'), format_group)
            sheet.write(rowx + 2, col + 1, _('Quantity'), format_group)
            sheet.write(rowx + 2, col + 2, _('Total amount'), format_group)
            col += 1
        else:
            sheet.merge_range(rowx, col, rowx + 1, col, _('End Balance'), format_group)
            sheet.write(rowx + 2, col, _('Quantity'), format_group)
            col += 1

        rowx += 3

        # нөхцөл шалгах

        sub_select = ''
        select = ''
        sub_join = ''
        join = ''
        where = ''
        account_where = ''
        group = ''
        order = ''
        if self.product_ids:
            product_ids = self.product_ids.ids
            where += ' AND pp.id in (' + ','.join(map(str, product_ids)) + ') '
        if self.category_ids:
            category_ids = self.category_ids.ids
            where += ' AND pc.id in (' + ','.join(map(str, category_ids)) + ') '
        having = "HAVING SUM(l.start_qty)::decimal(16,4) <> 0 OR SUM(l.start_cost)::decimal(16,4) <> 0 OR SUM(l.qty)::decimal(16,4) <> 0 \
                 OR SUM(l.cost)::decimal(16,4) <> 0 OR SUM(l.ex_qty)::decimal(16,4) <> 0 OR SUM(l.ex_cost)::decimal(16,4) <> 0 "

        if self.type == 'warehouse':
            select = "sl.id as lid, sw.code AS wcode, sw.name AS wname, "
            join += "LEFT JOIN stock_location sl ON (l.lid = sl.id)"
            join += "LEFT JOIN stock_location sl1 ON (sl.location_id = sl1.id) "
            join += "LEFT JOIN stock_warehouse sw ON (sw.view_location_id = sl1.id) "
            group = ",sl.id, sw.code, sw.name "
            order += " sw.code, sw.name, "
            if self.group_by == 'by_supplier':
                select += " l.supp_id AS supp_id, l.supp_name as supp_name, "
                sub_select = "rp.id AS supp_id, rp.name AS supp_name, "
                sub_join += "LEFT JOIN res_partner rp ON (pt.supplier_id = rp.id) "
                group += ",l.supp_id, l.supp_name "
                order += " l.supp_name, "
        elif self.type == 'account':
            select = "sl.id as lid, sw.code AS wcode, sw.name AS wname,aa.id AS account_id,aa.name AS account_name, "
            join += "LEFT JOIN stock_location sl ON (l.lid = sl.id)"
            join += "LEFT JOIN stock_location sl1 ON (sl.location_id = sl1.id) "
            join += "LEFT JOIN stock_warehouse sw ON (sw.view_location_id = sl1.id) "
            join += "LEFT JOIN ir_property i ON (i.res_id = 'product.category,'|| l.cid) "
            join += "LEFT JOIN account_account aa ON (aa.id = split_part(i.value_reference, ',', 2)::int) "
            account_where += " WHERE i.name = 'property_stock_valuation_account_id' "
            group = ",sl.id, sw.code, sw.name,aa.id  "
            order += " aa.name, sw.code, sw.name, "
        elif self.type == 'supplier':
            select = " l.supp_id AS supp_id, l.supp_name as supp_name, "
            sub_select = " rp.id AS supp_id, rp.name AS supp_name, "
            sub_join += "LEFT JOIN res_partner rp ON (pt.supplier_id = rp.id) "
            group = ",l.supp_id, l.supp_name "
            order += " l.supp_id, "

        warehouse_dic = {}
        for wh in self.warehouse_ids:
            loc_id = location_obj.search([('usage', '=', 'internal'), ('location_id', 'child_of', [wh.view_location_id.id])]).ids
            locations += loc_id
            if loc_id[0] not in warehouse_dic.keys():
                warehouse_dic[loc_id[0]] = wh.id

        if len(locations) > 0:
            locations = tuple(locations)
        locs = []
        locs.append(loc for loc in locations)

        cost_or_lst_price = "COALESCE(m.price_unit*m.product_qty,0)"
        if self.show_purch_price:
            cost_or_lst_price = "COALESCE(pt.list_price*m.product_qty,0)"

        if is_swap:
            select += " SUM(l.in_swap_qty) AS in_swap_qty, SUM(l.out_swap_qty) AS out_swap_qty, "
            select += " SUM(l.in_swap_cost) AS in_swap_cost, SUM(l.out_swap_cost) AS out_swap_cost, "
            sub_select += "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id IN %s" % ("'" + str(date_from) + "'", "'" + str(date_to) + "'", str_tuple(locations))
            sub_select += " AND  m.swap_order_line_in is not null "
            sub_select += " THEN COALESCE(m.product_qty/u.factor*u2.factor,0) ELSE 0 END AS in_swap_qty,"
            sub_select += "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id IN %s" % ("'" + str(date_from) + "'", "'" + str(date_to) + "'", str_tuple(locations))
            sub_select += " AND  m.swap_order_line_in is not null "
            sub_select += " THEN " + cost_or_lst_price + " ELSE 0 END AS in_swap_cost,"
            sub_select += " CASE WHEN m.date BETWEEN %s AND %s AND m.location_id IN %s " % ("'" + str(date_from) + "'", "'" + str(date_to) + "'", str_tuple(locations))
            sub_select += " AND  m.swap_order_line_out is not null "
            sub_select += " THEN COALESCE(m.product_qty/u.factor*u2.factor,0) ELSE 0 END AS out_swap_qty,"
            sub_select += " CASE WHEN m.date BETWEEN %s AND %s AND m.location_id IN %s " % ("'" + str(date_from) + "'", "'" + str(date_to) + "'", str_tuple(locations))
            sub_select += " AND  m.swap_order_line_out is not null "
            sub_select += " THEN " + cost_or_lst_price + " ELSE 0 END AS out_swap_cost,"

        if is_product_expense:
            select += "SUM(l.out_prod_ex_qty) AS out_prod_ex_qty,"
            select += "SUM(l.out_prod_ex_cost) AS out_prod_ex_cost,"
            sub_select += "CASE WHEN m.date BETWEEN %s AND %s AND  sp.expense is not null " % ("'" + str(date_from) + "'", "'" + str(date_to) + "'")
            sub_select += "THEN COALESCE(m.product_qty/u.factor*u2.factor,0) ELSE 0 END AS out_prod_ex_qty,"
            sub_select += "CASE WHEN m.date BETWEEN %s AND %s AND  sp.expense is not null " % ("'" + str(date_from) + "'", "'" + str(date_to) + "'")
            sub_select += "THEN " + cost_or_lst_price + " ELSE 0 END AS out_prod_ex_cost,"
            sub_join += " LEFT JOIN stock_picking sp ON sp.id = m.picking_id "

        # эхний үлдэгдэл, Орлого, Зарлагыг авах query
        self.env.cr.execute("SELECT l.list_price,l.prod_id AS pid, l.ptid as ptid, l.name AS name, l.code AS code, l.barcode as barcode, l.uom_name AS uom_name, "
                            "l.cid AS cid, l.cname AS cname, " + select + " "
                            "SUM(l.start_qty) AS start_qty, SUM(l.start_cost) AS start_cost, "
                             "SUM(l.qty) AS qty, SUM(l.cost) AS cost, "

                            "SUM(l.ex_qty) AS ex_qty, SUM(l.ex_cost) AS ex_cost, "

                            "SUM(l.start_qty + l.qty - l.ex_qty) AS end_qty,  "
                            "SUM(l.start_cost + l.cost - l.ex_cost) AS end_cost, "

                            "SUM(l.in_supplier_qty) AS in_supplier_qty, "
                            "SUM(l.in_supplier_cost) AS in_supplier_cost, "
                            "SUM(l.out_supplier_qty) AS out_supplier_qty,"
                            "SUM(l.out_supplier_cost) AS out_supplier_cost,"

                            "SUM(l.in_inventory_qty) AS in_inventory_qty,"
                            "SUM(l.in_inventory_cost) AS in_inventory_cost,"
                            "SUM(l.out_inventory_qty) AS out_inventory_qty,"
                            "SUM(l.out_inventory_cost) AS out_inventory_cost,"

                            "SUM(l.in_production_qty) AS in_production_qty,"
                            "SUM(l.in_production_cost) AS in_production_cost,"
                            "SUM(l.out_production_qty) AS out_production_qty,"
                            "SUM(l.out_production_cost) AS out_production_cost,"

                            "SUM(l.in_transit_qty) AS in_transit_qty,"
                            "SUM(l.in_transit_cost) AS in_transit_cost,"
                            "SUM(l.out_transit_qty) AS out_transit_qty,"
                            "SUM(l.out_transit_cost) AS out_transit_cost,"

                            "SUM(l.in_customer_qty) AS in_customer_qty,"
                            "SUM(l.in_customer_cost) AS in_customer_cost,"
                            "SUM(l.out_customer_qty) AS out_customer_qty, "
                            "SUM(l.out_customer_cost) AS out_customer_cost, "

                            "SUM(l.out_pos_customer_qty) AS out_pos_customer_qty, "
                            "SUM(l.out_pos_customer_cost) AS out_pos_customer_cost "

                            "FROM ( SELECT pt.list_price as list_price,m.product_id AS prod_id, pt.id as ptid, pt.name AS name, pp.default_code AS code, pp.barcode as barcode,"
                            "u2.name AS uom_name, pc.id AS cid, pc.name AS cname," + sub_select +
                            #                                     эхний үлдэгдэл авах
                            "CASE WHEN m.date < %s AND m.location_id NOT IN %s AND m.location_dest_id IN %s "
                            "THEN COALESCE(m.product_qty/u.factor*u2.factor,0) "
                            "WHEN m.date < %s AND m.location_id IN %s AND m.location_dest_id NOT IN %s "
                            "THEN -COALESCE(m.product_qty/u.factor*u2.factor,0) ELSE 0 END AS start_qty, "
                            #                                     худалдан авалтын орлого
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id NOT IN %s "
                            "AND m.location_dest_id IN %s AND sl2.usage='internal' AND sl.usage ='supplier' "
                            "THEN COALESCE(m.product_qty/u.factor*u2.factor,0) ELSE 0 END AS in_supplier_qty, "
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id NOT IN %s "
                            "AND m.location_dest_id IN %s AND sl2.usage='internal' AND sl.usage ='supplier' "
                            "THEN " + cost_or_lst_price + " ELSE 0 END AS in_supplier_cost, "
                            #                                     борлуулалтын буцаалтын орлого
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id NOT IN %s "
                            "AND m.location_dest_id IN %s AND sl2.usage='internal' AND sl.usage ='customer' "
                            "THEN COALESCE(m.product_qty/u.factor*u2.factor,0) ELSE 0 END AS in_customer_qty, "
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id NOT IN %s "
                            "AND m.location_dest_id IN %s AND sl2.usage='internal' AND sl.usage ='customer' "
                            "THEN " + cost_or_lst_price + " ELSE 0 END AS in_customer_cost, "
                            #                                     нөхөн дүүргэлтийн орлого
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id NOT IN %s "
                            "AND m.location_dest_id IN %s AND sl2.usage='internal' AND sl.usage ='transit' "
                            "THEN COALESCE(m.product_qty/u.factor*u2.factor,0) ELSE 0 END AS in_transit_qty, "
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id NOT IN %s "
                            "AND m.location_dest_id IN %s AND sl2.usage='internal' AND sl.usage ='transit' "
                            "THEN " + cost_or_lst_price + " ELSE 0 END AS in_transit_cost, "
                            #                                     үйлдвэрлэлийн орлого
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id NOT IN %s "
                            "AND m.location_dest_id IN %s AND sl2.usage='internal' AND sl.usage ='production' "
                            "THEN COALESCE(m.product_qty/u.factor*u2.factor,0) ELSE 0 END AS in_production_qty, "
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id NOT IN %s "
                            "AND m.location_dest_id IN %s AND sl2.usage='internal' AND sl.usage ='production' "
                            "THEN " + cost_or_lst_price + " ELSE 0 END AS in_production_cost, "
                            #                                     тооллогын орлого
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id NOT IN %s "
                            "AND m.location_dest_id IN %s AND sl2.usage='internal' AND sl.usage ='inventory' AND sl.scrap_location = 'f' "
                            "THEN COALESCE(m.product_qty/u.factor*u2.factor,0) ELSE 0 END AS in_inventory_qty, "
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id NOT IN %s "
                            "AND m.location_dest_id IN %s AND sl2.usage='internal' AND sl.usage ='inventory' AND sl.scrap_location = 'f' "
                            "THEN " + cost_or_lst_price + " ELSE 0 END AS in_inventory_cost, "
                            #                                     худалдан авалтын зарлага
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id IN %s "
                            "AND m.location_dest_id NOT IN %s AND sl2.usage='supplier' AND sl.usage ='internal' "
                            "THEN COALESCE(m.product_qty/u.factor*u2.factor,0) ELSE 0 END AS out_supplier_qty, "
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id IN %s "
                            "AND m.location_dest_id NOT IN %s AND sl2.usage='supplier' AND sl.usage ='internal' "
                            "THEN " + cost_or_lst_price + " ELSE 0 END AS out_supplier_cost, "
                            #                                     борлуулалтын зарлага
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id IN %s "
                            "AND m.location_dest_id NOT IN %s AND sl2.usage ='customer' AND sl.usage ='internal' "
                            "AND m.procurement_id is not null AND m.partner_id is not null "
                            "THEN COALESCE(m.product_qty/u.factor*u2.factor,0) ELSE 0 END AS out_customer_qty, "
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id IN %s "
                            "AND m.location_dest_id NOT IN %s AND sl2.usage ='customer' AND sl.usage ='internal' "
                            "AND m.procurement_id is not null AND m.partner_id is not null "
                            "THEN " + cost_or_lst_price + " ELSE 0 END AS out_customer_cost, "
                            #                                     пос-н борлуулалтын зарлага
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id IN %s "
                            "AND m.location_dest_id NOT IN %s AND sl2.usage ='customer' AND sl.usage ='internal' "
                            "AND m.procurement_id is NULL AND m.partner_id is NULL "
                            "THEN COALESCE(m.product_qty/u.factor*u2.factor,0) ELSE 0 END AS out_pos_customer_qty , "
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id IN %s "
                            "AND m.location_dest_id NOT IN %s AND sl2.usage ='customer' AND sl.usage ='internal' "
                            "AND m.procurement_id is NULL AND m.partner_id is NULL "
                            "THEN " + cost_or_lst_price + " ELSE 0 END AS out_pos_customer_cost , "
                            #                                     нөхөн дүүргэлтийн зарлага
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id IN %s "
                            "AND m.location_dest_id NOT IN %s AND sl2.usage='transit' AND sl.usage ='internal' "
                            "THEN COALESCE(m.product_qty/u.factor*u2.factor,0) ELSE 0 END AS out_transit_qty, "
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id IN %s "
                            "AND m.location_dest_id NOT IN %s AND sl2.usage='transit' AND sl.usage ='internal' "
                            "THEN " + cost_or_lst_price + " ELSE 0 END AS out_transit_cost, "
                            #                                     тэм-н зарлага
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id IN %s "
                            "AND m.location_dest_id NOT IN %s AND sl2.usage='production' AND sl.usage ='internal' "
                            "THEN COALESCE(m.product_qty/u.factor*u2.factor,0) ELSE 0 END AS out_production_qty, "
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id IN %s "
                            "AND m.location_dest_id NOT IN %s AND sl2.usage='production' AND sl.usage ='internal' "
                            "THEN " + cost_or_lst_price + " ELSE 0 END AS out_production_cost, "
                            #                                     тооллогын зарлага
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id IN %s "
                            "AND m.location_dest_id NOT IN %s AND sl2.usage='inventory' AND sl.usage ='internal' AND sl2.scrap_location = 'f' "
                            "THEN COALESCE(m.product_qty/u.factor*u2.factor,0) ELSE 0 END AS out_inventory_qty, "
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id IN %s "
                            "AND m.location_dest_id NOT IN %s AND sl2.usage='inventory' AND sl.usage ='internal' AND sl2.scrap_location = 'f' "
                            "THEN " + cost_or_lst_price + " ELSE 0 END AS out_inventory_cost, "
                            #                                     эхний үлдэгдлийн өртөг
                            "CASE WHEN m.date < %s AND m.location_id NOT IN %s AND m.location_dest_id IN %s "
                            "THEN " + cost_or_lst_price + " "
                            "WHEN m.date < %s AND m.location_id IN %s AND m.location_dest_id NOT IN %s "
                            "THEN -" + cost_or_lst_price + " ELSE 0 END AS start_cost, "
                            #                                     нийт орлого
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id NOT IN %s AND m.location_dest_id IN %s "
                            "THEN COALESCE(m.product_qty/u.factor*u2.factor,0) ELSE 0 END AS qty, "
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id NOT IN %s AND m.location_dest_id IN %s "
                            "THEN " + cost_or_lst_price + " ELSE 0 END AS cost, "
                            #                                     нийт зарлага
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id IN %s AND m.location_dest_id NOT IN %s "
                            "THEN COALESCE(m.product_qty/u.factor*u2.factor,0) ELSE 0 END AS ex_qty, "
                            "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id IN %s AND m.location_dest_id NOT IN %s "
                            "THEN " + cost_or_lst_price + " ELSE 0 END AS ex_cost, "
                            "CASE WHEN m.location_id NOT IN %s AND m.location_dest_id IN %s "
                            "THEN m.location_dest_id "
                            "WHEN m.location_id IN %s AND m.location_dest_id NOT IN %s "
                            "THEN m.location_id ELSE 0 END AS lid "
                            "FROM stock_move m "
                            "LEFT JOIN stock_location sl ON m.location_id = sl.id "
                            "LEFT JOIN stock_location sl2 ON m.location_dest_id = sl2.id "
                            "LEFT JOIN product_product pp ON (pp.id=m.product_id) "
                            "LEFT JOIN product_template pt ON (pt.id=pp.product_tmpl_id) "
                            "LEFT JOIN product_uom u ON (u.id=m.product_uom) "
                            "LEFT JOIN product_uom u2 ON (u2.id=pt.uom_id) "
                            "LEFT JOIN product_category pc ON (pt.categ_id = pc.id) " + sub_join +
                            "WHERE m.state = 'done' " + where + " ) AS l " + join + account_where + " "
                            "GROUP BY l.prod_id, l.ptid, l.name, l.list_price, l.code, l.barcode, l.uom_name, l.cid, l.cname " + group + having + " "
                            "ORDER BY " + order + "l.cname, l.name, l.code, l.uom_name ", (date_from, locations, locations, date_from, locations, locations,
                                                                                           date_from, date_to, locations, locations, date_from, date_to, locations, locations, date_from, date_to, locations, locations, date_from, date_to, locations, locations,
                                                                                           date_from, date_to, locations, locations, date_from, date_to, locations, locations, date_from, date_to, locations, locations, date_from, date_to, locations, locations,
                                                                                           date_from, date_to, locations, locations, date_from, date_to, locations, locations, date_from, date_to, locations, locations, date_from, date_to, locations, locations,
                                                                                           date_from, date_to, locations, locations, date_from, date_to, locations, locations, date_from, date_to, locations, locations, date_from, date_to, locations, locations,
                                                                                           date_from, date_to, locations, locations, date_from, date_to, locations, locations, date_from, date_to, locations, locations, date_from, date_to, locations, locations,
                                                                                           date_from, date_to, locations, locations, date_from, date_to, locations, locations, date_from, locations, locations,
                                                                                           date_from, locations, locations, date_from, date_to, locations, locations, date_from, date_to, locations, locations,
                                                                                           date_from, date_to, locations, locations, date_from, date_to, locations, locations, locations, locations, locations, locations))

        lines = self.env.cr.dictfetchall()
        where1 = ''
        if self.product_ids:
            product_ids = self.product_ids.ids
            where1 += ' AND product_id in (' + ','.join(str(p_id) for p_id in product_ids) + ') '
        if lines:
            type_id = False
            category_id = False
            supplier_id = False
            account_id = False
            seq = 1
            sub_seq = 1

            total_start_qty = total_start_cost = 0
            total_qty = total_cost = 0
            total_ex_qty = total_ex_cost = 0
            total_end_qty = total_end_cost = 0

            total_in_customer_qty = total_out_customer_qty = total_out_pos_customer_qty = 0
            total_in_supplier_qty = total_out_supplier_qty = 0
            total_in_inventory_qty = total_out_inventory_qty = 0
            total_in_production_qty = total_out_production_qty = 0
            total_in_transit_qty = total_out_transit_qty = 0

            total_in_customer_cost = total_out_customer_cost = total_out_pos_customer_cost = 0
            total_in_supplier_cost = total_out_supplier_cost = 0
            total_in_inventory_cost = total_out_inventory_cost = 0
            total_in_production_cost = total_out_production_cost = 0
            total_in_transit_cost = total_out_transit_cost = 0
            total_out_prod_ex_cost = 0
            total_other_income = 0
            total_other_expense = 0
            total_other_income_cost = 0
            total_other_expense_cost = 0
            subtotal_start_qty = subtotal_start_cost = 0
            subtotal_qty = subtotal_cost = 0
            subtotal_ex_qty = subtotal_ex_cost = 0
            subtotal_end_qty = subtotal_end_cost = 0
            subtotal_in_customer_qty = subtotal_out_customer_qty = subtotal_out_pos_customer_qty = 0
            subtotal_in_supplier_qty = subtotal_out_supplier_qty = 0
            subtotal_in_inventory_qty = subtotal_out_inventory_qty = 0
            subtotal_in_production_qty = subtotal_out_production_qty = 0
            subtotal_in_transit_qty = subtotal_out_transit_qty = 0
            subtotal_other_income = 0
            subtotal_other_expense = 0
            subtotal_in_customer_cost = subtotal_out_customer_cost = subtotal_out_pos_customer_cost = 0
            subtotal_in_supplier_cost = subtotal_out_supplier_cost = 0
            subtotal_in_inventory_cost = subtotal_out_inventory_cost = 0
            subtotal_in_production_cost = subtotal_out_production_cost = 0
            subtotal_in_transit_cost = subtotal_out_transit_cost = 0
            subtotal_other_income = 0
            subtotal_other_expense = 0
            subtotal_out_prod_ex_cost = 0
            subtotal_other_income_cost = 0
            subtotal_other_expense_cost = 0
            # if is_product_expense:
            subtotal_out_prod_ex_qty = 0
            total_out_prod_ex_qty = 0
            # if is_swap:
            total_in_swap_qty = total_out_swap_qty = 0
            total_in_swap_cost = total_out_swap_cost = 0
            subtotal_in_swap_qty = subtotal_out_swap_qty = 0
            subtotal_in_swap_cost = 0
            subtotal_out_swap_cost = 0
            list_price = 0   
#             ӨРТӨГ ХАРАХ ЭСЭХ
            if self.show_cost or self.show_purch_price:
                #                 Бүлэглэлтгүй үед өртөгтэй
                if self.type == 'no_grouping':
                    for line in lines:
                        product = self.env['product.template'].search([('id','=', line['ptid']),('active','=', True)])
                        if self.pricelist_id:
                            # Зарах үнийг үнийн хүснэгтээс авах
                            if product:
                                list_price = self.pricelist_id.get_product_price(product, line['qty'], False, date_to, False)
                                line['list_price'] = list_price
                                line['start_cost'] = list_price * line['start_qty']
                                line['in_supplier_cost'] = list_price * line['in_supplier_qty']
                                line['in_production_cost'] = list_price * line['in_production_qty']
                                line['in_transit_cost'] = list_price * line['in_transit_qty']
                                line['in_customer_cost'] = list_price * line['in_customer_qty']
                                line['in_inventory_cost'] = list_price * line['in_supplier_qty']
                                line['cost'] = list_price * line['qty']
                                line['ex_cost'] = list_price * line['ex_qty']
                                line['out_customer_cost'] = list_price * line['out_customer_qty']
                                line['out_pos_customer_cost'] = list_price * line['out_pos_customer_qty']
                                line['out_transit_cost'] = list_price * line['out_transit_qty']
                                line['out_prod_ex_cost'] = list_price * line['out_prod_ex_qty']
                                line['out_supplier_cost'] = list_price * line['out_supplier_qty']
                                line['out_inventory_cost'] = list_price * line['out_inventory_qty']
                                line['out_production_cost'] = list_price * line['out_production_qty']
                                line['end_cost'] = list_price * line['end_qty']
                        elif self.show_purch_price:
                            # Зарах үнийг барааны үнийн түүхээс авах
                            list_price_his = self.env['product.price.history'].search([('product_template_id','=', product.id),
                                                                                   ('price_type','=', 'list_price'),
                                                                                   ('datetime', '<', date_to)], limit=1, order='datetime desc') 
                            list_price = list_price_his.list_price 
                            line['list_price'] = list_price
                            line['start_cost'] = list_price * line['start_qty']
                            line['in_supplier_cost'] = list_price * line['in_supplier_qty']
                            line['in_production_cost'] = list_price * line['in_production_qty']
                            line['in_transit_cost'] = list_price * line['in_transit_qty']
                            line['in_customer_cost'] = list_price * line['in_customer_qty']
                            line['in_inventory_cost'] = list_price * line['in_supplier_qty']
                            line['cost'] = list_price * line['qty']
                            line['ex_cost'] = list_price * line['ex_qty']
                            line['out_customer_cost'] = list_price * line['out_customer_qty']
                            line['out_pos_customer_cost'] = list_price * line['out_pos_customer_qty']
                            line['out_transit_cost'] = list_price * line['out_transit_qty']
                            line['out_prod_ex_cost'] = list_price * line['out_prod_ex_qty']
                            line['out_supplier_cost'] = list_price * line['out_supplier_qty']
                            line['out_inventory_cost'] = list_price * line['out_inventory_qty']
                            line['out_production_cost'] = list_price * line['out_production_qty']
                            line['end_cost'] = list_price * line['end_qty']
                        subtotal_start_qty += line['start_qty']
                        subtotal_start_cost += line['start_cost']
                        subtotal_qty += line['qty']
                        subtotal_cost += line['cost']
                        subtotal_ex_qty += line['ex_qty']
                        subtotal_ex_cost += line['ex_cost']
                        subtotal_end_qty += line['end_qty']
                        subtotal_end_cost += line['end_cost']
                        subtotal_in_customer_qty += line['in_customer_qty']
                        subtotal_in_customer_cost += line['in_customer_cost']
                        subtotal_out_customer_qty += line['out_customer_qty']
                        subtotal_out_customer_cost += line['out_customer_cost']
                        subtotal_out_pos_customer_qty += line['out_pos_customer_qty']
                        subtotal_out_pos_customer_cost += line['out_pos_customer_cost']
                        subtotal_in_supplier_qty += line['in_supplier_qty']
                        subtotal_in_supplier_cost += line['in_supplier_cost']
                        subtotal_out_supplier_qty += line['out_supplier_qty']
                        subtotal_out_supplier_cost += line['out_supplier_cost']
                        subtotal_in_inventory_qty += line['in_inventory_qty']
                        subtotal_in_inventory_cost += line['in_inventory_cost']
                        subtotal_out_inventory_qty += line['out_inventory_qty']
                        subtotal_out_inventory_cost += line['out_inventory_cost']
                        subtotal_in_production_qty += line['in_production_qty']
                        subtotal_in_production_cost += line['in_production_cost']
                        subtotal_out_production_qty += line['out_production_qty']
                        subtotal_out_production_cost += line['out_production_cost']
                        subtotal_in_transit_qty += line['in_transit_qty']
                        subtotal_in_transit_cost += line['in_transit_cost']
                        subtotal_out_transit_qty += line['out_transit_qty']
                        subtotal_out_transit_cost += line['out_transit_cost']

                        if is_product_expense:
                            subtotal_out_prod_ex_qty += line.get('out_prod_ex_qty', 0)
                            subtotal_out_prod_ex_cost += line['out_prod_ex_cost']

                        if is_swap:
                            subtotal_in_swap_qty += line['in_swap_qty']
                            subtotal_out_swap_qty += line['out_swap_qty']
                            subtotal_in_swap_cost += line['in_swap_cost']
                            subtotal_out_swap_cost += line['out_swap_cost']
                        # Эцсийн үлдэгдэлийн нэгж өртөг зурах хэсэг
                        unit_cost = 0
                        if line['end_qty'] != 0:
                            unit_cost = abs(line['end_cost']) / abs(line['end_qty'])
                        #                                 Бусад орлогыг тооцох
                        other_income = 0
                        if line['qty'] != 0:
                            other_income = line['qty'] - line['in_customer_qty'] - line['in_supplier_qty'] - line['in_inventory_qty'] - line['in_production_qty'] - line['in_transit_qty']
                            if is_swap:
                                other_income = other_income - line['in_swap_qty']
                        other_income_cost = 0
                        if line['cost'] != 0:
                            other_income_cost = line['cost'] - line['in_customer_cost'] - line['in_supplier_cost'] - line['in_inventory_cost'] - line['in_production_cost'] - line['in_transit_cost']
                            if is_swap:
                                other_income_cost = other_income_cost - line['in_swap_cost']
#                                 Бусад зарлагыг тооцох
                        other_expense = 0
                        if line['ex_qty'] != 0:
                            other_expense = line['ex_qty'] - line['out_customer_qty'] - line['out_pos_customer_qty'] - line['out_supplier_qty'] - line['out_inventory_qty'] - line['out_production_qty'] - line['out_transit_qty']
                            if is_swap:
                                other_expense = other_expense - line['out_swap_qty']
                            if is_product_expense:
                                other_expense = other_expense - line['out_prod_ex_qty']
                        other_expense_cost = 0
                        if line['ex_cost'] != 0:
                            other_expense_cost = line['ex_cost'] - line['out_customer_cost'] - line['out_pos_customer_cost'] - line['out_supplier_cost'] - line['out_inventory_cost'] - line['out_production_cost'] - line['out_transit_cost']
                            if is_swap:
                                other_expense_cost = other_expense_cost - line['out_swap_cost']
                            if is_product_expense:
                                other_expense_cost = other_expense_cost - line['out_prod_ex_cost']

                        subtotal_other_income += line['cost']
                        subtotal_other_expense += other_expense
                        subtotal_other_income_cost += 0 if other_income == 0 else other_income_cost
                        subtotal_other_expense_cost += 0 if other_expense == 0 else other_expense_cost
                        total_other_income += other_income
                        total_other_expense += other_expense
                        total_other_income_cost += 0 if other_income == 0 else other_income_cost
                        total_other_expense_cost += 0 if other_expense == 0 else other_expense_cost

                        # Нийт дүнг олно
                        total_start_qty += line['start_qty']
                        total_start_cost += 0 if line['start_qty'] == 0 else line['start_cost']
                        total_qty += line['qty']
                        total_cost += line['cost']
                        total_ex_qty += line['ex_qty']
                        total_ex_cost += line['ex_cost']
                        total_end_qty += line['end_qty']
                        total_end_cost += line['end_cost']
                        total_in_customer_qty += line['in_customer_qty']
                        total_in_customer_cost += line['in_customer_cost']
                        total_out_customer_qty += line['out_customer_qty']
                        total_out_customer_cost += line['out_customer_cost']
                        total_out_pos_customer_qty += line['out_pos_customer_qty']
                        total_out_pos_customer_cost += line['out_pos_customer_cost']
                        total_in_supplier_qty += line['in_supplier_qty']
                        total_in_supplier_cost += line['in_supplier_cost']
                        total_out_supplier_qty += line['out_supplier_qty']
                        total_out_supplier_cost += line['out_supplier_cost']
                        total_in_inventory_qty += line['in_inventory_qty']
                        total_in_inventory_cost += line['in_inventory_cost']
                        total_out_inventory_qty += line['out_inventory_qty']
                        total_out_inventory_cost += line['out_inventory_cost']
                        total_in_production_qty += line['in_production_qty']
                        total_in_production_cost += line['in_production_cost']
                        total_out_production_qty += line['out_production_qty']
                        total_out_production_cost += line['out_production_cost']
                        total_in_transit_qty += line['in_transit_qty']
                        total_in_transit_cost += line['in_transit_cost']
                        total_out_transit_qty += line['out_transit_qty']
                        total_out_transit_cost += line['out_transit_cost']

                        if is_product_expense:
                            total_out_prod_ex_qty += line.get('out_prod_ex_qty', 0)
                            total_out_prod_ex_cost += line['out_prod_ex_cost']
                        if is_swap:
                            total_in_swap_qty += line['in_swap_qty']
                            total_out_swap_qty += line['out_swap_qty']
                            total_in_swap_cost += line['in_swap_cost']
                            total_out_swap_cost += line['out_swap_cost']

                        # Барааны мөрийг зурна
                        col = 0
                        sheet.write(rowx, col, '%s' % seq, format_content_center)
                        col += 1
                        sheet.write(rowx, col, line['code'], format_content_center)
                        col += 1
                        if self.show_barcode:
                            sheet.write(rowx, col, line['barcode'], format_content_center)
                            col += 1
                        sheet.write(rowx, col, line['name'], format_content_text)
                        col += 1
                        sheet.write(rowx, col, line['uom_name'], format_content_center)
                        col += 1
                        sheet, col = self.write_qty_and_cost(True, sheet, rowx, col, line, 'start_qty', 'start_cost', format_content_float)

                        #                         Орлогын хэсэг
                        sheet, col = self.build_credit_with_cost(sheet, col, rowx, format_content_float, line, \
                                                                 is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense)
                        sheet.write(rowx, col, other_income, format_content_float)
                        col += 1
                        sheet.write(rowx, col, other_income_cost, format_content_float)
                        col += 1
                        sheet, col = self.write_qty_and_cost(True, sheet, rowx, col, line, 'qty', 'cost', format_content_float)

                        #                         Зарлагын хэсэг is_sale, is_swap, is_pos, is_stock, is_product_expense, is_purchase, is_stock, is_mrp
                        sheet, col = self.build_debit_with_cost(sheet, col, rowx, format_content_float, line, \
                                      is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense)
                        sheet.write(rowx, col, other_expense, format_content_float)
                        col += 1
                        sheet.write(rowx, col, other_expense_cost, format_content_float)
                        col += 1
                        sheet.write(rowx, col, line['ex_qty'], format_group_float)
                        col += 1
                        sheet.write(rowx, col, 0 if line['ex_qty'] == 0 else line['ex_cost'], format_group_float)
                        col += 1
                        sheet.write(rowx, col, unit_cost if self.show_purch_price or self.env['product.product'].browse(
                            line['pid']).cost_method != 'real' else '', format_content_float)
                        col += 1
                        sheet, col = self.write_qty_and_cost(True, sheet, rowx, col, line, 'end_qty', 'end_cost', format_content_float)
                        rowx += 1
                        seq += 1

                    col = 4 if self.show_barcode else 3
                    #                     Дэд дүнгийн мөр
                    sheet, col = self.build_subtotal_row_with_cost(sheet, rowx, col, format_group_right, format_group_float, \
                                     is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                                     subtotal_start_qty, subtotal_start_cost, subtotal_in_supplier_qty, subtotal_in_supplier_cost, \
                                     subtotal_in_swap_qty, subtotal_in_swap_cost, subtotal_in_production_qty, subtotal_in_production_cost, \
                                     subtotal_in_transit_qty, subtotal_in_transit_cost, subtotal_in_customer_qty, subtotal_in_customer_cost, \
                                     subtotal_in_inventory_qty, subtotal_in_inventory_cost, subtotal_other_income, subtotal_other_income_cost, \
                                     subtotal_qty, subtotal_cost, subtotal_out_customer_qty, subtotal_out_customer_cost, \
                                     subtotal_out_swap_qty, subtotal_out_swap_cost, subtotal_out_pos_customer_qty, subtotal_out_pos_customer_cost, \
                                     subtotal_out_transit_qty, subtotal_out_transit_cost, subtotal_out_prod_ex_qty, subtotal_out_prod_ex_cost, \
                                     subtotal_out_supplier_qty, subtotal_out_supplier_cost, subtotal_out_inventory_qty, subtotal_out_inventory_cost, \
                                     subtotal_out_production_qty, subtotal_out_production_cost, subtotal_other_expense, subtotal_other_expense_cost, \
                                     subtotal_ex_qty, subtotal_ex_cost, subtotal_end_qty, subtotal_end_cost)

                    #                     Нийт дүнгийн мөр
                    rowx += 1
                    col = 4 if self.show_barcode else 3
                    sheet, col = self.build_total_row_with_cost(sheet, rowx, col, format_title, format_title_float, \
                                  is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                                  total_start_qty, total_start_cost, total_in_supplier_qty, total_in_supplier_cost, \
                                  total_in_swap_qty, total_in_swap_cost, total_in_production_qty, total_in_production_cost, \
                                  total_in_transit_qty, total_in_transit_cost, total_in_customer_qty, total_in_customer_cost, \
                                  total_in_inventory_qty, total_in_inventory_cost, total_other_income, total_other_income_cost, \
                                  total_qty, total_cost, total_out_customer_qty, total_out_customer_cost, \
                                  total_out_swap_qty, total_out_swap_cost, total_out_pos_customer_qty, total_out_pos_customer_cost, \
                                  total_out_transit_qty, total_out_transit_cost, total_out_prod_ex_qty, total_out_prod_ex_cost, \
                                  total_out_supplier_qty, total_out_supplier_cost, total_out_inventory_qty, total_out_inventory_cost, \
                                  total_out_production_qty, total_out_production_cost, total_other_expense, total_other_expense_cost, \
                                  total_ex_qty, total_ex_cost, total_end_qty, total_end_cost)
                    rowx += 1
                #                 АГУУЛАХ ТӨРӨЛТЭЙ
                if self.type == 'warehouse':
                    #                     АНГИЛАЛААР БҮЛЭГЛЭХ
                    if self.group_by == 'by_category':
                        for line in lines:
                            product = self.env['product.template'].search([('id','=', line['ptid']),('active','=', True)])
                            if self.pricelist_id:
                                # Зарах үнийг үнийн хүснэгтээс авах
                                if product:
                                    list_price = self.pricelist_id.get_product_price(product, line['qty'], False, date_to, False)
                                    line['list_price'] = list_price
                                    line['start_cost'] = list_price * line['start_qty']
                                    line['in_supplier_cost'] = list_price * line['in_supplier_qty']
                                    line['in_production_cost'] = list_price * line['in_production_qty']
                                    line['in_transit_cost'] = list_price * line['in_transit_qty']
                                    line['in_customer_cost'] = list_price * line['in_customer_qty']
                                    line['in_inventory_cost'] = list_price * line['in_supplier_qty']
                                    line['cost'] = list_price * line['qty']
                                    line['ex_cost'] = list_price * line['ex_qty']
                                    line['out_customer_cost'] = list_price * line['out_customer_qty']
                                    line['out_pos_customer_cost'] = list_price * line['out_pos_customer_qty']
                                    line['out_transit_cost'] = list_price * line['out_transit_qty']
                                    line['out_prod_ex_cost'] = list_price * line['out_prod_ex_qty']
                                    line['out_supplier_cost'] = list_price * line['out_supplier_qty']
                                    line['out_inventory_cost'] = list_price * line['out_inventory_qty']
                                    line['out_production_cost'] = list_price * line['out_production_qty']
                                    line['end_cost'] = list_price * line['end_qty']
                            elif self.show_purch_price:
                                # Зарах үнийг барааны үнийн түүхээс авах
                                list_price_his = self.env['product.price.history'].search([('product_template_id','=', product.id),
                                                                                       ('price_type','=', 'list_price'),
                                                                                       ('datetime', '<', date_to)], limit=1, order='datetime desc')
                                list_price = list_price_his.list_price
                                line['list_price'] = list_price
                                line['start_cost'] = list_price * line['start_qty']
                                line['in_supplier_cost'] = list_price * line['in_supplier_qty']
                                line['in_production_cost'] = list_price * line['in_production_qty']
                                line['in_transit_cost'] = list_price * line['in_transit_qty']
                                line['in_customer_cost'] = list_price * line['in_customer_qty']
                                line['in_inventory_cost'] = list_price * line['in_supplier_qty']
                                line['cost'] = list_price * line['qty']
                                line['ex_cost'] = list_price * line['ex_qty']
                                line['out_customer_cost'] = list_price * line['out_customer_qty']
                                line['out_pos_customer_cost'] = list_price * line['out_pos_customer_qty']
                                line['out_transit_cost'] = list_price * line['out_transit_qty']
                                line['out_prod_ex_cost'] = list_price * line['out_prod_ex_qty']
                                line['out_supplier_cost'] = list_price * line['out_supplier_qty']
                                line['out_inventory_cost'] = list_price * line['out_inventory_qty']
                                line['out_production_cost'] = list_price * line['out_production_qty']
                                line['end_cost'] = list_price * line['end_qty']
                            # Эхний агуулах/данс болон ангиллыг зурна
                            if not type_id and not category_id:
                                name = (('[' + line['wcode'] + '] ') if line['wcode'] else '') + line['wname'] if line['wname'] else ''
                                col = total_col_range_with_cost
                                sheet.merge_range(rowx, 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 1, rowx, col, line['cname'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн агуулах/дансыг зурна
                            elif type_id and type_id != line['wcode']:
                                sub_seq = 1
                                seq = 1
                                col = 4 if self.show_barcode else 3
                                sheet.merge_range(rowx, 0, rowx, col, _('Sub Total'), format_group_right)
                                for h in range(col, total_col_range_with_cost + 1):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowx += 1
                                name = (('[' + line['wcode'] + '] ') if line['wcode'] else '') + line['wname'] if line['wname'] else ''
                                col = total_col_range_with_cost
                                sheet.merge_range(rowx, 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 0, sub_seq, format_content_number)
                                sheet.merge_range(rowx, 1, rowx, col, line['cname'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн ангиллыг зурна
                            elif category_id and category_id != line['cid']:
                                col = total_col_range_with_cost
                                sub_seq += 1
                                sheet.write(rowx, 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 1, rowx, col, line['cname'], format_content_bold_left)
                                rowx += 1
                                seq = 1

                            subtotal_start_qty += line['start_qty']
                            subtotal_start_cost += line['start_cost']
                            subtotal_qty += line['qty']
                            subtotal_cost += line['cost']
                            subtotal_ex_qty += line['ex_qty']
                            subtotal_ex_cost += line['ex_cost']
                            subtotal_end_qty += line['end_qty']
                            subtotal_end_cost += line['end_cost']
                            subtotal_in_customer_qty += line['in_customer_qty']
                            subtotal_in_customer_cost += line['in_customer_cost']
                            subtotal_out_customer_qty += line['out_customer_qty']
                            subtotal_out_customer_cost += line['out_customer_cost']
                            subtotal_out_pos_customer_qty += line['out_pos_customer_qty']
                            subtotal_out_pos_customer_cost += line['out_pos_customer_cost']
                            subtotal_in_supplier_qty += line['in_supplier_qty']
                            subtotal_in_supplier_cost += line['in_supplier_cost']
                            subtotal_out_supplier_qty += line['out_supplier_qty']
                            subtotal_out_supplier_cost += line['out_supplier_cost']
                            subtotal_in_inventory_qty += line['in_inventory_qty']
                            subtotal_in_inventory_cost += line['in_inventory_cost']
                            subtotal_out_inventory_qty += line['out_inventory_qty']
                            subtotal_out_inventory_cost += line['out_inventory_cost']
                            subtotal_in_production_qty += line['in_production_qty']
                            subtotal_in_production_cost += line['in_production_cost']
                            subtotal_out_production_qty += line['out_production_qty']
                            subtotal_out_production_cost += line['out_production_cost']
                            subtotal_in_transit_qty += line['in_transit_qty']
                            subtotal_in_transit_cost += line['in_transit_cost']
                            subtotal_out_transit_qty += line['out_transit_qty']
                            subtotal_out_transit_cost += line['out_transit_cost']

                            if is_product_expense:
                                subtotal_out_prod_ex_qty += line.get('out_prod_ex_qty', 0)
                                subtotal_out_prod_ex_cost += line['out_prod_ex_cost']

                            if is_swap:
                                subtotal_in_swap_qty += line['in_swap_qty']
                                subtotal_out_swap_qty += line['out_swap_qty']
                                subtotal_in_swap_cost += line['in_swap_cost']
                                subtotal_out_swap_cost += line['out_swap_cost']
                            # Эцсийн үлдэгдэлийн нэгж өртөг зурах хэсэг
                            unit_cost = 0
                            if line['end_qty'] != 0:
                                unit_cost = abs(line['end_cost']) / abs(line['end_qty'])
                            # Бусад орлогыг тооцох
                            other_income = 0
                            if line['qty'] != 0:
                                other_income = line['qty'] - line['in_customer_qty'] - line['in_supplier_qty'] - line['in_inventory_qty'] - line['in_production_qty'] - line['in_transit_qty']
                                if is_swap:
                                    other_income = other_income - line['in_swap_qty']
                            other_income_cost = 0
                            if line['cost'] != 0:
                                other_income_cost = line['cost'] - line['in_customer_cost'] - line['in_supplier_cost'] - line['in_inventory_cost'] - line['in_production_cost'] - line['in_transit_cost']
                                if is_swap:
                                    other_income_cost = other_income_cost - line['in_swap_cost']
#                                 Бусад зарлагыг тооцох
                            other_expense = 0
                            if line['ex_qty'] != 0:
                                other_expense = line['ex_qty'] - line['out_customer_qty'] - line['out_pos_customer_qty'] - line['out_supplier_qty'] - line['out_inventory_qty'] - line['out_production_qty'] - line['out_transit_qty']
                                if is_swap:
                                    other_expense = other_expense - line['out_swap_qty']
                                if is_product_expense:
                                    other_expense = other_expense - line['out_prod_ex_qty']

                            other_expense_cost = 0
                            if line['ex_cost'] != 0:
                                other_expense_cost = line['ex_cost'] - line['out_customer_cost'] - line['out_pos_customer_cost'] - line['out_supplier_cost'] - line['out_inventory_cost'] - line['out_production_cost'] - line['out_transit_cost']
                                if is_swap:
                                    other_expense_cost = other_expense_cost - line['out_swap_cost']
                                if is_product_expense:
                                    other_expense_cost = other_expense_cost - line['out_prod_ex_cost']

                            subtotal_other_income += other_income
                            subtotal_other_expense += other_expense
                            subtotal_other_income_cost += 0 if other_income == 0 else line['out_prod_ex_cost']
                            subtotal_other_expense_cost += 0 if other_expense == 0 else line['out_prod_ex_cost']
                            total_other_income += other_income
                            total_other_expense += other_expense
                            total_other_income_cost += line['out_prod_ex_cost']
                            total_other_expense_cost += line['out_prod_ex_cost']

                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_start_cost += 0 if line['start_qty'] == 0 else line['start_cost']
                            total_qty += line['qty']
                            total_cost += line['cost']
                            total_ex_qty += line['ex_qty']
                            total_ex_cost += line['ex_cost']
                            total_end_qty += line['end_qty']
                            total_end_cost += line['end_cost']
                            total_in_customer_qty += line['in_customer_qty']
                            total_in_customer_cost += line['in_customer_cost']
                            total_out_customer_qty += line['out_customer_qty']
                            total_out_customer_cost += line['out_customer_cost']
                            total_out_pos_customer_qty += line['out_pos_customer_qty']
                            total_out_pos_customer_cost += line['out_pos_customer_cost']
                            total_in_supplier_qty += line['in_supplier_qty']
                            total_in_supplier_cost += line['in_supplier_cost']
                            total_out_supplier_qty += line['out_supplier_qty']
                            total_out_supplier_cost += line['out_supplier_cost']
                            total_in_inventory_qty += line['in_inventory_qty']
                            total_in_inventory_cost += line['in_inventory_cost']
                            total_out_inventory_qty += line['out_inventory_qty']
                            total_out_inventory_cost += line['out_inventory_cost']
                            total_in_production_qty += line['in_production_qty']
                            total_in_production_cost += line['in_production_cost']
                            total_out_production_qty += line['out_production_qty']
                            total_out_production_cost += line['out_production_cost']
                            total_in_transit_qty += line['in_transit_qty']
                            total_in_transit_cost += line['in_transit_cost']
                            total_out_transit_qty += line['out_transit_qty']
                            total_out_transit_cost += line['out_transit_cost']

                            if is_product_expense:
                                total_out_prod_ex_qty += line.get('out_prod_ex_qty', 0)
                                total_out_prod_ex_cost += line['out_prod_ex_cost']
                            if is_swap:
                                total_in_swap_qty += line['in_swap_qty']
                                total_out_swap_qty += line['out_swap_qty']
                                total_in_swap_cost += line['in_swap_cost']
                                total_out_swap_cost += line['out_swap_cost']

                            # Барааны мөрийг зурна
                            col = 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            sheet, col = self.write_qty_and_cost(True, sheet, rowx, col, line, 'start_qty', 'start_cost', format_content_float)

                            #                         Орлогын хэсэг
                            sheet, col = self.build_credit_with_cost(sheet, col, rowx, format_content_float, line, \
                                                                     is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense)
                            sheet.write(rowx, col, other_income, format_content_float)
                            col += 1
                            sheet.write(rowx, col, other_income_cost, format_content_float)
                            col += 1
                            sheet, col = self.write_qty_and_cost(True, sheet, rowx, col, line, 'qty', 'cost', format_group_float)

                            #                         Зарлагын хэсэг is_sale, is_swap, is_pos, is_stock, is_product_expense, is_purchase, is_stock, is_mrp
                            sheet, col = self.build_debit_with_cost(sheet, col, rowx, format_content_float, line, \
                                      is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense)
                            sheet.write(rowx, col, other_expense, format_content_float)
                            col += 1
                            sheet.write(rowx, col, other_expense_cost, format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_group_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['ex_qty'] == 0 else line['ex_cost'], format_group_float)
                            col += 1
                            sheet.write(rowx, col, unit_cost if self.show_purch_price or self.env['product.product'].browse(
                                line['pid']).cost_method != 'real' else '', format_content_float)
                            col += 1
                            sheet, col = self.write_qty_and_cost(is_stock, sheet, rowx, col, line, 'end_qty', 'end_cost', format_content_float)
                            rowx += 1
                            seq += 1
                            category_id = line['cid']
                            type_id = line['wcode']

                        col = 4 if self.show_barcode else 3
                        sheet.merge_range(rowx, 0, rowx, col, _('Sub Total'), format_group_right)
                        for h in range(col, total_col_range_with_cost + 1):
                            sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)

                        #                     Нийт дүнгийн мөр
                        rowx += 1
                        col = 4 if self.show_barcode else 3
                        sheet, col = self.build_total_row_with_cost(sheet, rowx, col, format_title, format_title_float, \
                                  is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                                  total_start_qty, total_start_cost, total_in_supplier_qty, total_in_supplier_cost, \
                                  total_in_swap_qty, total_in_swap_cost, total_in_production_qty, total_in_production_cost, \
                                  total_in_transit_qty, total_in_transit_cost, total_in_customer_qty, total_in_customer_cost, \
                                  total_in_inventory_qty, total_in_inventory_cost, total_other_income, total_other_income_cost, \
                                  total_qty, total_cost, total_out_customer_qty, total_out_customer_cost, \
                                  total_out_swap_qty, total_out_swap_cost, total_out_pos_customer_qty, total_out_pos_customer_cost, \
                                  total_out_transit_qty, total_out_transit_cost, total_out_prod_ex_qty, total_out_prod_ex_cost, \
                                  total_out_supplier_qty, total_out_supplier_cost, total_out_inventory_qty, total_out_inventory_cost, \
                                  total_out_production_qty, total_out_production_cost, total_other_expense, total_other_expense_cost, \
                                  total_ex_qty, total_ex_cost, total_end_qty, total_end_cost)
                        rowx += 1

                                # #                     АГУУЛАХ төрөлтэй ба НИЙЛҮҮЛЭГЧЭЭР АНГИЛНА
                    elif self.group_by == 'by_supplier':
                        sub_seq = 1
                        seq = 1
                        for line in lines:
                            product = self.env['product.template'].search([('id','=', line['ptid']),('active','=', True)])
                            if self.pricelist_id:
                                # Зарах үнийг үнийн хүснэгтээс авах
                                if product:
                                    list_price = self.pricelist_id.get_product_price(product, line['qty'], False, date_to, False)
                                    line['list_price'] = list_price
                                    line['start_cost'] = list_price * line['start_qty']
                                    line['in_supplier_cost'] = list_price * line['in_supplier_qty']
                                    line['in_production_cost'] = list_price * line['in_production_qty']
                                    line['in_transit_cost'] = list_price * line['in_transit_qty']
                                    line['in_customer_cost'] = list_price * line['in_customer_qty']
                                    line['in_inventory_cost'] = list_price * line['in_supplier_qty']
                                    line['cost'] = list_price * line['qty']
                                    line['ex_cost'] = list_price * line['ex_qty']
                                    line['out_customer_cost'] = list_price * line['out_customer_qty']
                                    line['out_pos_customer_cost'] = list_price * line['out_pos_customer_qty']
                                    line['out_transit_cost'] = list_price * line['out_transit_qty']
                                    line['out_prod_ex_cost'] = list_price * line['out_prod_ex_qty']
                                    line['out_supplier_cost'] = list_price * line['out_supplier_qty']
                                    line['out_inventory_cost'] = list_price * line['out_inventory_qty']
                                    line['out_production_cost'] = list_price * line['out_production_qty']
                                    line['end_cost'] = list_price * line['end_qty']
                            elif self.show_purch_price:
                                # Зарах үнийг барааны үнийн түүхээс авах
                                list_price_his = self.env['product.price.history'].search([('product_template_id','=', product.id),
                                                                                       ('price_type','=', 'list_price'),
                                                                                       ('datetime', '<', date_to)], limit=1, order='datetime desc')
                                list_price = list_price_his.list_price 
                                line['list_price'] = list_price
                                line['start_cost'] = list_price * line['start_qty']
                                line['in_supplier_cost'] = list_price * line['in_supplier_qty']
                                line['in_production_cost'] = list_price * line['in_production_qty']
                                line['in_transit_cost'] = list_price * line['in_transit_qty']
                                line['in_customer_cost'] = list_price * line['in_customer_qty']
                                line['in_inventory_cost'] = list_price * line['in_supplier_qty']
                                line['cost'] = list_price * line['qty']
                                line['ex_cost'] = list_price * line['ex_qty']
                                line['out_customer_cost'] = list_price * line['out_customer_qty']
                                line['out_pos_customer_cost'] = list_price * line['out_pos_customer_qty']
                                line['out_transit_cost'] = list_price * line['out_transit_qty']
                                line['out_prod_ex_cost'] = list_price * line['out_prod_ex_qty']
                                line['out_supplier_cost'] = list_price * line['out_supplier_qty']
                                line['out_inventory_cost'] = list_price * line['out_inventory_qty']
                                line['out_production_cost'] = list_price * line['out_production_qty']
                                line['end_cost'] = list_price * line['end_qty']    
                            # Эхний агуулах/данс болон ангиллыг зурна
                            if not type_id and not supplier_id:
                                name = (('[' + line['wcode'] + '] ') if line['wcode'] else '') + line['wname'] if line['wname'] else ''
                                col = total_col_range_with_cost
                                sheet.merge_range(rowx, 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 1, rowx, col, line['supp_name'] or _('Undefined Supplier'),
                                                  format_group_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн агуулах/дансыг зурна
                            elif type_id and type_id != line['wcode']:
                                sub_seq = 1
                                seq = 1
                                col = 4 if self.show_barcode else 3
                                sheet.merge_range(rowx, 0, rowx, col, _('Sub Total'), format_group_right)
                                for h in range(col, total_col_range_with_cost + 1):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowx += 1
                                name = (('[' + line['wcode'] + '] ') if line['wcode'] else '') + line['wname'] if line['wname'] else ''
                                col = total_col_range_with_cost
                                sheet.merge_range(rowx, 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 0, sub_seq, format_content_number)
                                sheet.merge_range(rowx, 1, rowx, col, line['supp_name'] or _('Undefined Supplier'),
                                                  format_group_left)
                                rowx += 1
                                rowd = rowx
#                             # Дараагийн ангиллыг зурна
                            elif supplier_id and supplier_id != line['supp_id']:
                                sub_seq += 1
                                seq = 1
                                col = 4 if self.show_barcode else 3
                                 #                     Дэд дүнгийн мөр
                                sheet, col = self.build_subtotal_row_with_cost(sheet, rowx, col, format_group_right, format_group_float, \
                                     is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                                     subtotal_start_qty, subtotal_start_cost, subtotal_in_supplier_qty, subtotal_in_supplier_cost, \
                                     subtotal_in_swap_qty, subtotal_in_swap_cost, subtotal_in_production_qty, subtotal_in_production_cost, \
                                     subtotal_in_transit_qty, subtotal_in_transit_cost, subtotal_in_customer_qty, subtotal_in_customer_cost, \
                                     subtotal_in_inventory_qty, subtotal_in_inventory_cost, subtotal_other_income, subtotal_other_income_cost, \
                                     subtotal_qty, subtotal_cost, subtotal_out_customer_qty, subtotal_out_customer_cost, \
                                     subtotal_out_swap_qty, subtotal_out_swap_cost, subtotal_out_pos_customer_qty, subtotal_out_pos_customer_cost, \
                                     subtotal_out_transit_qty, subtotal_out_transit_cost, subtotal_out_prod_ex_qty, subtotal_out_prod_ex_cost, \
                                     subtotal_out_supplier_qty, subtotal_out_supplier_cost, subtotal_out_inventory_qty, subtotal_out_inventory_cost, \
                                     subtotal_out_production_qty, subtotal_out_production_cost, subtotal_other_expense, subtotal_other_expense_cost, \
                                     subtotal_ex_qty, subtotal_ex_cost, subtotal_end_qty, subtotal_end_cost)
                                rowx += 1

                                col = total_col_range_with_cost
                                sheet.write(rowx, 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 1, rowx, col, line['supp_name'] or _('Undefined Supplier'), format_group_left)
                                rowx += 1
                                rowd = rowx

                                subtotal_start_qty = subtotal_start_cost = 0
                                subtotal_qty = subtotal_cost = 0
                                subtotal_ex_qty = subtotal_ex_cost = 0
                                subtotal_end_qty = subtotal_end_cost = 0
                                subtotal_in_customer_qty = subtotal_out_customer_qty = subtotal_out_pos_customer_qty = 0
                                subtotal_in_supplier_qty = subtotal_out_supplier_qty = 0
                                subtotal_in_inventory_qty = subtotal_out_inventory_qty = 0
                                subtotal_in_production_qty = subtotal_out_production_qty = 0
                                subtotal_in_transit_qty = subtotal_out_transit_qty = 0
                                subtotal_out_prod_ex_qty = subtotal_out_prod_ex_cost = 0
                                if is_swap:
                                    subtotal_in_swap_qty = subtotal_out_swap_qty = 0

                            subtotal_start_qty += line['start_qty']
                            subtotal_start_cost += line['start_cost']
                            subtotal_qty += line['qty']
                            subtotal_cost += line['cost']
                            subtotal_ex_qty += line['ex_qty']
                            subtotal_ex_cost += line['ex_cost']
                            subtotal_end_qty += line['end_qty']
                            subtotal_end_cost += line['end_cost']
                            subtotal_in_customer_qty += line['in_customer_qty']
                            subtotal_in_customer_cost += line['in_customer_cost']
                            subtotal_out_customer_qty += line['out_customer_qty']
                            subtotal_out_customer_cost += line['out_customer_cost']
                            subtotal_out_pos_customer_qty += line['out_pos_customer_qty']
                            subtotal_out_pos_customer_cost += line['out_pos_customer_cost']
                            subtotal_in_supplier_qty += line['in_supplier_qty']
                            subtotal_in_supplier_cost += line['in_supplier_cost']
                            subtotal_out_supplier_qty += line['out_supplier_qty']
                            subtotal_out_supplier_cost += line['out_supplier_cost']
                            subtotal_in_inventory_qty += line['in_inventory_qty']
                            subtotal_in_inventory_cost += line['in_inventory_cost']
                            subtotal_out_inventory_qty += line['out_inventory_qty']
                            subtotal_out_inventory_cost += line['out_inventory_cost']
                            subtotal_in_production_qty += line['in_production_qty']
                            subtotal_in_production_cost += line['in_production_cost']
                            subtotal_out_production_qty += line['out_production_qty']
                            subtotal_out_production_cost += line['out_production_cost']
                            subtotal_in_transit_qty += line['in_transit_qty']
                            subtotal_in_transit_cost += line['in_transit_cost']
                            subtotal_out_transit_qty += line['out_transit_qty']
                            subtotal_out_transit_cost += line['out_transit_cost']

                            if is_product_expense:
                                subtotal_out_prod_ex_qty += line['out_prod_ex_qty']
                                subtotal_out_prod_ex_cost += line['out_prod_ex_cost']

                            if is_swap:
                                subtotal_in_swap_qty += line['in_swap_qty']
                                subtotal_out_swap_qty += line['out_swap_qty']
                                subtotal_in_swap_cost += line['in_swap_cost']
                                subtotal_out_swap_cost += line['out_swap_cost']
                            # Эцсийн үлдэгдэлийн нэгж өртөг зурах хэсэг
                            unit_cost = 0
                            if line['end_qty'] != 0:
                                unit_cost = abs(line['end_cost']) / abs(line['end_qty'])
                            #                                 Бусад орлогыг тооцох
                            other_income = 0
                            if line['qty'] != 0:
                                other_income = line['qty'] - line['in_customer_qty'] - line['in_supplier_qty'] - line['in_inventory_qty'] - line['in_production_qty'] - line['in_transit_qty']
                                if is_swap:
                                    other_income = other_income - line['in_swap_qty']
                            other_income_cost = 0
                            if line['cost'] != 0:
                                other_income_cost = line['cost'] - line['in_customer_cost'] - line['in_supplier_cost'] - line['in_inventory_cost'] - line['in_production_cost'] - line['in_transit_cost']
                                if is_swap:
                                    other_income_cost = other_income_cost - line['in_swap_cost']
#                                 Бусад зарлагыг тооцох
                            other_expense = 0
                            if line['ex_qty'] != 0:
                                other_expense = line['ex_qty'] - line['out_customer_qty'] - line['out_pos_customer_qty'] - line['out_supplier_qty'] - line['out_inventory_qty'] - line['out_production_qty'] - line['out_transit_qty']
                                if is_swap:
                                    other_expense = other_expense - line['out_swap_qty']
                                if is_product_expense:
                                    other_expense = other_expense - line['out_prod_ex_qty']
                            other_expense_cost = 0
                            if line['ex_cost'] != 0:
                                other_expense_cost = line['ex_cost'] - line['out_customer_cost'] - line['out_pos_customer_cost'] - line['out_supplier_cost'] - line['out_inventory_cost'] - line['out_production_cost'] - line['out_transit_cost']
                                if is_swap:
                                    other_expense_cost = other_expense_cost - line['out_swap_cost']
                                if is_product_expense:
                                    other_expense_cost = other_expense_cost - line['out_prod_ex_cost']

                            subtotal_other_income += other_income
                            subtotal_other_expense += other_expense
                            subtotal_other_income_cost += 0 if other_income == 0 else other_income_cost
                            subtotal_other_expense_cost += 0 if other_expense == 0 else other_expense_cost
                            total_other_income += other_income
                            total_other_expense += other_expense
                            total_other_income_cost += 0 if other_income == 0 else other_income_cost
                            total_other_expense_cost += 0 if other_expense == 0 else other_expense_cost

                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_start_cost += 0 if line['start_qty'] == 0 else line['start_cost']
                            total_qty += line['qty']
                            total_cost += line['cost']
                            total_ex_qty += line['ex_qty']
                            total_ex_cost += line['ex_cost']
                            total_end_qty += line['end_qty']
                            total_end_cost += line['end_cost']
                            total_in_customer_qty += line['in_customer_qty']
                            total_in_customer_cost += line['in_customer_cost']
                            total_out_customer_qty += line['out_customer_qty']
                            total_out_customer_cost += line['out_customer_cost']
                            total_out_pos_customer_qty += line['out_pos_customer_qty']
                            total_out_pos_customer_cost += line['out_pos_customer_cost']
                            total_in_supplier_qty += line['in_supplier_qty']
                            total_in_supplier_cost += line['in_supplier_cost']
                            total_out_supplier_qty += line['out_supplier_qty']
                            total_out_supplier_cost += line['out_supplier_cost']
                            total_in_inventory_qty += line['in_inventory_qty']
                            total_in_inventory_cost += line['in_inventory_cost']
                            total_out_inventory_qty += line['out_inventory_qty']
                            total_out_inventory_cost += line['out_inventory_cost']
                            total_in_production_qty += line['in_production_qty']
                            total_in_production_cost += line['in_production_cost']
                            total_out_production_qty += line['out_production_qty']
                            total_out_production_cost += line['out_production_cost']
                            total_in_transit_qty += line['in_transit_qty']
                            total_in_transit_cost += line['in_transit_cost']
                            total_out_transit_qty += line['out_transit_qty']
                            total_out_transit_cost += line['out_transit_cost']

                            if is_product_expense:
                                total_out_prod_ex_qty += line.get('out_prod_ex_qty', 0)
                                total_out_prod_ex_cost += line['out_prod_ex_cost']
                            if is_swap:
                                total_in_swap_qty += line['in_swap_qty']
                                total_out_swap_qty += line['out_swap_qty']
                                total_in_swap_cost += line['in_swap_cost']
                                total_out_swap_cost += line['out_swap_cost']

                            # Барааны мөрийг зурна
                            col = 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1

                            sheet.write(rowx, col, line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            sheet, col = self.write_qty_and_cost(True, sheet, rowx, col, line, 'start_qty', 'start_cost', format_content_float)

                             #                         Орлогын хэсэг
                            sheet, col = self.build_credit_with_cost(sheet, col, rowx, format_content_float, line, \
                                                                    is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense)
                            sheet.write(rowx, col, other_income, format_content_float)
                            col += 1
                            sheet.write(rowx, col, other_income_cost, format_content_float)
                            col += 1
                            sheet, col = self.write_qty_and_cost(True, sheet, rowx, col, line, 'qty', 'cost', format_group_float)

                            #                         Зарлагын хэсэг is_sale, is_swap, is_pos, is_stock, is_product_expense, is_purchase, is_stock, is_mrp
                            sheet, col = self.build_debit_with_cost(sheet, col, rowx, format_content_float, line, \
                                      is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense)
                            sheet.write(rowx, col, other_expense, format_content_float)
                            col += 1
                            sheet.write(rowx, col, other_expense_cost, format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_group_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['ex_qty'] == 0 else line['ex_cost'], format_group_float)
                            col += 1
                            sheet.write(rowx, col, unit_cost if self.show_purch_price or self.env['product.product'].browse(
                                line['pid']).cost_method != 'real' else '', format_content_float)
                            col += 1
                            sheet, col = self.write_qty_and_cost(True, sheet, rowx, col, line, 'end_qty', 'end_cost', format_content_float)
                            rowx += 1
                            seq += 1
                            supplier_id = line['supp_id']
                            type_id = line['wcode']

                        #                     Дэд дүнгийн мөр
                        col = 4 if self.show_barcode else 3
                        sheet.merge_range(rowx, 0, rowx, col, _('Sub Total'), format_group_right)
                        for h in range(col, total_col_range_with_cost + 1):
                            sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)

                        #                         Нийт дүнгийн мөр
                        rowx += 1
                        col = 4 if self.show_barcode else 3
                        sheet, col = self.build_total_row_with_cost(sheet, rowx, col, format_title, format_title_float, \
                                  is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                                  total_start_qty, total_start_cost, total_in_supplier_qty, total_in_supplier_cost, \
                                  total_in_swap_qty, total_in_swap_cost, total_in_production_qty, total_in_production_cost, \
                                  total_in_transit_qty, total_in_transit_cost, total_in_customer_qty, total_in_customer_cost, \
                                  total_in_inventory_qty, total_in_inventory_cost, total_other_income, total_other_income_cost, \
                                  total_qty, total_cost, total_out_customer_qty, total_out_customer_cost, \
                                  total_out_swap_qty, total_out_swap_cost, total_out_pos_customer_qty, total_out_pos_customer_cost, \
                                  total_out_transit_qty, total_out_transit_cost, total_out_prod_ex_qty, total_out_prod_ex_cost, \
                                  total_out_supplier_qty, total_out_supplier_cost, total_out_inventory_qty, total_out_inventory_cost, \
                                  total_out_production_qty, total_out_production_cost, total_other_expense, total_other_expense_cost, \
                                  total_ex_qty, total_ex_cost, total_end_qty, total_end_cost)
                        rowx += 1

                #                         Ангилал төрлөөр
                elif self.type == 'category':
                    sub_seq = 1
                    seq = 1
                    for line in lines:
                        product = self.env['product.template'].search([('id','=', line['ptid']),('active','=', True)])
                        if self.pricelist_id:
                            # Зарах үнийг үнийн хүснэгтээс авах
                            if product:
                                list_price = self.pricelist_id.get_product_price(product, line['qty'], False, date_to, False)
                                line['list_price'] = list_price
                                line['start_cost'] = list_price * line['start_qty']
                                line['in_supplier_cost'] = list_price * line['in_supplier_qty']
                                line['in_production_cost'] = list_price * line['in_production_qty']
                                line['in_transit_cost'] = list_price * line['in_transit_qty']
                                line['in_customer_cost'] = list_price * line['in_customer_qty']
                                line['in_inventory_cost'] = list_price * line['in_supplier_qty']
                                line['cost'] = list_price * line['qty']
                                line['ex_cost'] = list_price * line['ex_qty']
                                line['out_customer_cost'] = list_price * line['out_customer_qty']
                                line['out_pos_customer_cost'] = list_price * line['out_pos_customer_qty']
                                line['out_transit_cost'] = list_price * line['out_transit_qty']
                                line['out_prod_ex_cost'] = list_price * line['out_prod_ex_qty']
                                line['out_supplier_cost'] = list_price * line['out_supplier_qty']
                                line['out_inventory_cost'] = list_price * line['out_inventory_qty']
                                line['out_production_cost'] = list_price * line['out_production_qty']
                                line['end_cost'] = list_price * line['end_qty']
                        elif self.show_purch_price:
                            # Зарах үнийг барааны үнийн түүхээс авах
                            list_price_his = self.env['product.price.history'].search([('product_template_id','=', product.id),
                                                                                   ('price_type','=', 'list_price'),
                                                                                   ('datetime', '<', date_to)], limit=1, order='datetime desc') 
                            list_price = list_price_his.list_price 
                            line['list_price'] = list_price
                            line['start_cost'] = list_price * line['start_qty']
                            line['in_supplier_cost'] = list_price * line['in_supplier_qty']
                            line['in_production_cost'] = list_price * line['in_production_qty']
                            line['in_transit_cost'] = list_price * line['in_transit_qty']
                            line['in_customer_cost'] = list_price * line['in_customer_qty']
                            line['in_inventory_cost'] = list_price * line['in_supplier_qty']
                            line['cost'] = list_price * line['qty']
                            line['ex_cost'] = list_price * line['ex_qty']
                            line['out_customer_cost'] = list_price * line['out_customer_qty']
                            line['out_pos_customer_cost'] = list_price * line['out_pos_customer_qty']
                            line['out_transit_cost'] = list_price * line['out_transit_qty']
                            line['out_prod_ex_cost'] = list_price * line['out_prod_ex_qty']
                            line['out_supplier_cost'] = list_price * line['out_supplier_qty']
                            line['out_inventory_cost'] = list_price * line['out_inventory_qty']
                            line['out_production_cost'] = list_price * line['out_production_qty']
                            line['end_cost'] = list_price * line['end_qty']
                        # Эхний агуулах/данс болон ангиллыг зурна
                        if not category_id:
                            col = total_col_range_with_cost
                            sheet.write(rowx, 0, sub_seq, format_group_left)
                            sheet.merge_range(rowx, 1, rowx, col, line['cname'], format_group_left)
                            rowx += 1
                            rowd = rowx
                            subtotal_start_qty = subtotal_start_cost = 0
                            subtotal_qty = subtotal_cost = 0
                            subtotal_ex_qty = subtotal_ex_cost = 0
                            subtotal_end_qty = subtotal_end_cost = 0
                            subtotal_in_customer_qty = subtotal_out_customer_qty = subtotal_out_pos_customer_qty = 0
                            subtotal_in_supplier_qty = subtotal_out_supplier_qty = 0
                            subtotal_in_inventory_qty = subtotal_out_inventory_qty = 0
                            subtotal_in_production_qty = subtotal_out_production_qty = 0
                            subtotal_in_transit_qty = subtotal_out_transit_qty = 0
                            subtotal_out_prod_ex_qty = subtotal_out_prod_ex_cost = 0
                            if is_swap:
                                subtotal_in_swap_qty = subtotal_out_swap_qty = 0
                        # Дараагийн ангиллыг зурна
                        elif category_id and category_id != line['cid']:
                            sub_seq += 1
                            seq = 1
                            col = 4 if self.show_barcode else 3
                            #                     Дэд дүнгийн мөр
                            sheet, col = self.build_subtotal_row_with_cost(sheet, rowx, col, format_group_right, format_group_float, \
                                     is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                                     subtotal_start_qty, subtotal_start_cost, subtotal_in_supplier_qty, subtotal_in_supplier_cost, \
                                     subtotal_in_swap_qty, subtotal_in_swap_cost, subtotal_in_production_qty, subtotal_in_production_cost, \
                                     subtotal_in_transit_qty, subtotal_in_transit_cost, subtotal_in_customer_qty, subtotal_in_customer_cost, \
                                     subtotal_in_inventory_qty, subtotal_in_inventory_cost, subtotal_other_income, subtotal_other_income_cost, \
                                     subtotal_qty, subtotal_cost, subtotal_out_customer_qty, subtotal_out_customer_cost, \
                                     subtotal_out_swap_qty, subtotal_out_swap_cost, subtotal_out_pos_customer_qty, subtotal_out_pos_customer_cost, \
                                     subtotal_out_transit_qty, subtotal_out_transit_cost, subtotal_out_prod_ex_qty, subtotal_out_prod_ex_cost, \
                                     subtotal_out_supplier_qty, subtotal_out_supplier_cost, subtotal_out_inventory_qty, subtotal_out_inventory_cost, \
                                     subtotal_out_production_qty, subtotal_out_production_cost, subtotal_other_expense, subtotal_other_expense_cost, \
                                     subtotal_ex_qty, subtotal_ex_cost, subtotal_end_qty, subtotal_end_cost)

                            col = total_col_range_with_cost
                            rowx += 1
                            sheet.write(rowx, 0, sub_seq, format_group_left)
                            sheet.merge_range(rowx, 1, rowx, col, line['cname'], format_group_left)
                            rowx += 1
                            rowd = rowx

                            subtotal_start_qty = subtotal_start_cost = 0
                            subtotal_qty = subtotal_cost = 0
                            subtotal_ex_qty = subtotal_ex_cost = 0
                            subtotal_end_qty = subtotal_end_cost = 0
                            subtotal_in_customer_qty = subtotal_out_customer_qty = subtotal_out_pos_customer_qty = 0
                            subtotal_in_supplier_qty = subtotal_out_supplier_qty = 0
                            subtotal_in_inventory_qty = subtotal_out_inventory_qty = 0
                            subtotal_in_production_qty = subtotal_out_production_qty = 0
                            subtotal_in_transit_qty = subtotal_out_transit_qty = 0
                            subtotal_out_prod_ex_qty = subtotal_out_prod_ex_cost = 0
                            if is_swap:
                                subtotal_in_swap_qty = subtotal_out_swap_qty = 0
                        #
                        subtotal_start_qty += line['start_qty']
                        subtotal_start_cost += line['start_cost']
                        subtotal_qty += line['qty']
                        subtotal_cost += line['cost']
                        subtotal_ex_qty += line['ex_qty']
                        subtotal_ex_cost += line['ex_cost']
                        subtotal_end_qty += line['end_qty']
                        subtotal_end_cost += line['end_cost']
                        subtotal_in_customer_qty += line['in_customer_qty']
                        subtotal_in_customer_cost += line['in_customer_cost']
                        subtotal_out_customer_qty += line['out_customer_qty']
                        subtotal_out_customer_cost += line['out_customer_cost']
                        subtotal_out_pos_customer_qty += line['out_pos_customer_qty']
                        subtotal_out_pos_customer_cost += line['out_pos_customer_cost']
                        subtotal_in_supplier_qty += line['in_supplier_qty']
                        subtotal_in_supplier_cost += line['in_supplier_cost']
                        subtotal_out_supplier_qty += line['out_supplier_qty']
                        subtotal_out_supplier_cost += line['out_supplier_cost']
                        subtotal_in_inventory_qty += line['in_inventory_qty']
                        subtotal_in_inventory_cost += line['in_inventory_cost']
                        subtotal_out_inventory_qty += line['out_inventory_qty']
                        subtotal_out_inventory_cost += line['out_inventory_cost']
                        subtotal_in_production_qty += line['in_production_qty']
                        subtotal_in_production_cost += line['in_production_cost']
                        subtotal_out_production_qty += line['out_production_qty']
                        subtotal_out_production_cost += line['out_production_cost']
                        subtotal_in_transit_qty += line['in_transit_qty']
                        subtotal_in_transit_cost += line['in_transit_cost']
                        subtotal_out_transit_qty += line['out_transit_qty']
                        subtotal_out_transit_cost += line['out_transit_cost']

                        if is_product_expense:
                            subtotal_out_prod_ex_qty += line.get('out_prod_ex_qty', 0)
                            subtotal_out_prod_ex_cost += line['out_prod_ex_cost']

                        if is_swap:
                            subtotal_in_swap_qty += line['in_swap_qty']
                            subtotal_out_swap_qty += line['out_swap_qty']
                            subtotal_in_swap_cost += line['in_swap_cost']
                            subtotal_out_swap_cost += line['out_swap_cost']
                        # Эцсийн үлдэгдэлийн нэгж өртөг зурах хэсэг
                        unit_cost = 0
                        if line['end_qty'] != 0:
                            unit_cost = abs(line['end_cost']) / abs(line['end_qty'])
                        #                                 Бусад орлогыг тооцох
                        other_income = 0
                        if line['qty'] != 0:
                            other_income = line['qty'] - line['in_customer_qty'] - line['in_supplier_qty'] - line['in_inventory_qty'] - line['in_production_qty'] - line['in_transit_qty']
                            if is_swap:
                                other_income = other_income - line['in_swap_qty']
                        other_income_cost = 0
                        if line['cost'] != 0:
                            other_income_cost = line['cost'] - line['in_customer_cost'] - line['in_supplier_cost'] - line['in_inventory_cost'] - line['in_production_cost'] - line['in_transit_cost']
                            if is_swap:
                                other_income_cost = other_income_cost - line['in_swap_cost']
#                                 Бусад зарлагыг тооцох
                        other_expense = 0
                        if line['ex_qty'] != 0:
                            other_expense = line['ex_qty'] - line['out_customer_qty'] - line['out_pos_customer_qty'] - line['out_supplier_qty'] - line['out_inventory_qty'] - line['out_production_qty'] - line['out_transit_qty']
                            if is_swap:
                                other_expense = other_expense - line['out_swap_qty']
                            if is_product_expense:
                                other_expense = other_expense - line['out_prod_ex_qty']
                        other_expense_cost = 0
                        if line['ex_cost'] != 0:
                            other_expense_cost = line['ex_cost'] - line['out_customer_cost'] - line['out_pos_customer_cost'] - line['out_supplier_cost'] - line['out_inventory_cost'] - line['out_production_cost'] - line['out_transit_cost']
                            if is_swap:
                                other_expense_cost = other_expense_cost - line['out_swap_cost']
                            if is_product_expense:
                                other_expense_cost = other_expense_cost - line['out_prod_ex_cost']

                        subtotal_other_income += other_income
                        subtotal_other_expense += other_expense
                        subtotal_other_income_cost += 0 if other_income == 0 else other_income_cost
                        subtotal_other_expense_cost += 0 if other_expense == 0 else other_expense_cost
                        total_other_income += other_income
                        total_other_expense += other_expense
                        total_other_income_cost += 0 if other_income == 0 else other_income_cost
                        total_other_expense_cost += 0 if other_expense == 0 else other_expense_cost

                        # Нийт дүнг олно
                        total_start_qty += line['start_qty']
                        total_start_cost += 0 if line['start_qty'] == 0 else line['start_cost']
                        total_qty += line['qty']
                        total_cost += line['cost']
                        total_ex_qty += line['ex_qty']
                        total_ex_cost += line['ex_cost']
                        total_end_qty += line['end_qty']
                        total_end_cost += line['end_cost']
                        total_in_customer_qty += line['in_customer_qty']
                        total_in_customer_cost += line['in_customer_cost']
                        total_out_customer_qty += line['out_customer_qty']
                        total_out_customer_cost += line['out_customer_cost']
                        total_out_pos_customer_qty += line['out_pos_customer_qty']
                        total_out_pos_customer_cost += line['out_pos_customer_cost']
                        total_in_supplier_qty += line['in_supplier_qty']
                        total_in_supplier_cost += line['in_supplier_cost']
                        total_out_supplier_qty += line['out_supplier_qty']
                        total_out_supplier_cost += line['out_supplier_cost']
                        total_in_inventory_qty += line['in_inventory_qty']
                        total_in_inventory_cost += line['in_inventory_cost']
                        total_out_inventory_qty += line['out_inventory_qty']
                        total_out_inventory_cost += line['out_inventory_cost']
                        total_in_production_qty += line['in_production_qty']
                        total_in_production_cost += line['in_production_cost']
                        total_out_production_qty += line['out_production_qty']
                        total_out_production_cost += line['out_production_cost']
                        total_in_transit_qty += line['in_transit_qty']
                        total_in_transit_cost += line['in_transit_cost']
                        total_out_transit_qty += line['out_transit_qty']
                        total_out_transit_cost += line['out_transit_cost']

                        if is_product_expense:
                            total_out_prod_ex_qty += line.get('out_prod_ex_qty', 0)
                            total_out_prod_ex_cost += line['out_prod_ex_cost']
                        if is_swap:
                            total_in_swap_qty += line['in_swap_qty']
                            total_out_swap_qty += line['out_swap_qty']
                            total_in_swap_cost += line['in_swap_cost']
                            total_out_swap_cost += line['out_swap_cost']

                        # Барааны мөрийг зурна
                        col = 0
                        sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                        col += 1
                        sheet.write(rowx, col, line['code'], format_content_center)
                        col += 1
                        if self.show_barcode:
                            sheet.write(rowx, col, line['barcode'], format_content_center)
                            col += 1
                        sheet.write(rowx, col, line['name'], format_content_text)
                        col += 1
                        sheet.write(rowx, col, line['uom_name'], format_content_center)
                        col += 1
                        sheet, col = self.write_qty_and_cost(True, sheet, rowx, col, line, 'start_qty', 'start_cost', format_content_float)

                        #                         Орлогын хэсэг
                        sheet, col = self.build_credit_with_cost(sheet, col, rowx, format_content_float, line, \
                                                                 is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense)
                        sheet.write(rowx, col, other_income, format_content_float)
                        col += 1
                        sheet.write(rowx, col, other_income_cost, format_content_float)
                        col += 1
                        sheet, col = self.write_qty_and_cost(True, sheet, rowx, col, line, 'qty', 'cost', format_content_float)

                            #                         Зарлагын хэсэг is_sale, is_swap, is_pos, is_stock, is_product_expense, is_purchase, is_stock, is_mrp
                        sheet, col = self.build_debit_with_cost(sheet, col, rowx, format_content_float, line, \
                                      is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense)
                        sheet.write(rowx, col, other_expense, format_content_float)
                        col += 1
                        sheet.write(rowx, col, other_expense_cost, format_content_float)
                        col += 1
                        sheet.write(rowx, col, line['ex_qty'], format_group_float)
                        col += 1
                        sheet.write(rowx, col, 0 if line['ex_qty'] == 0 else line['ex_cost'], format_group_float)
                        col += 1
                        sheet.write(rowx, col, unit_cost if self.show_purch_price or self.env['product.product'].browse(
                            line['pid']).cost_method != 'real' else '', format_content_float)
                        col += 1
                        sheet, col = self.write_qty_and_cost(True, sheet, rowx, col, line, 'end_qty', 'end_cost', format_content_float)
                        rowx += 1
                        seq += 1
                        category_id = line['cid']

                    col = 4 if self.show_barcode else 3
                    #                     Дэд дүнгийн мөр
                    sheet, col = self.build_subtotal_row_with_cost(sheet, rowx, col, format_group_right, format_group_float, \
                                     is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                                     subtotal_start_qty, subtotal_start_cost, subtotal_in_supplier_qty, subtotal_in_supplier_cost, \
                                     subtotal_in_swap_qty, subtotal_in_swap_cost, subtotal_in_production_qty, subtotal_in_production_cost, \
                                     subtotal_in_transit_qty, subtotal_in_transit_cost, subtotal_in_customer_qty, subtotal_in_customer_cost, \
                                     subtotal_in_inventory_qty, subtotal_in_inventory_cost, subtotal_other_income, subtotal_other_income_cost, \
                                     subtotal_qty, subtotal_cost, subtotal_out_customer_qty, subtotal_out_customer_cost, \
                                     subtotal_out_swap_qty, subtotal_out_swap_cost, subtotal_out_pos_customer_qty, subtotal_out_pos_customer_cost, \
                                     subtotal_out_transit_qty, subtotal_out_transit_cost, subtotal_out_prod_ex_qty, subtotal_out_prod_ex_cost, \
                                     subtotal_out_supplier_qty, subtotal_out_supplier_cost, subtotal_out_inventory_qty, subtotal_out_inventory_cost, \
                                     subtotal_out_production_qty, subtotal_out_production_cost, subtotal_other_expense, subtotal_other_expense_cost, \
                                     subtotal_ex_qty, subtotal_ex_cost, subtotal_end_qty, subtotal_end_cost)

                        #                         Нийт дүнгийн мөр
                    rowx += 1
                    col = 4 if self.show_barcode else 3
                    sheet, col = self.build_total_row_with_cost(sheet, rowx, col, format_title, format_title_float, \
                                  is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                                  total_start_qty, total_start_cost, total_in_supplier_qty, total_in_supplier_cost, \
                                  total_in_swap_qty, total_in_swap_cost, total_in_production_qty, total_in_production_cost, \
                                  total_in_transit_qty, total_in_transit_cost, total_in_customer_qty, total_in_customer_cost, \
                                  total_in_inventory_qty, total_in_inventory_cost, total_other_income, total_other_income_cost, \
                                  total_qty, total_cost, total_out_customer_qty, total_out_customer_cost, \
                                  total_out_swap_qty, total_out_swap_cost, total_out_pos_customer_qty, total_out_pos_customer_cost, \
                                  total_out_transit_qty, total_out_transit_cost, total_out_prod_ex_qty, total_out_prod_ex_cost, \
                                  total_out_supplier_qty, total_out_supplier_cost, total_out_inventory_qty, total_out_inventory_cost, \
                                  total_out_production_qty, total_out_production_cost, total_other_expense, total_other_expense_cost, \
                                  total_ex_qty, total_ex_cost, total_end_qty, total_end_cost)
                    rowx += 1

                #                             Нийлүүлэгч төрлөөр
                elif self.type == 'supplier':
                    for line in lines:
                        product = self.env['product.template'].search([('id','=', line['ptid']),('active','=', True)])
                        if self.pricelist_id:
                            # Зарах үнийг үнийн хүснэгтээс авах
                            if product:
                                list_price = self.pricelist_id.get_product_price(product, line['qty'], False, date_to, False)
                                line['list_price'] = list_price
                                line['start_cost'] = list_price * line['start_qty']
                                line['in_supplier_cost'] = list_price * line['in_supplier_qty']
                                line['in_production_cost'] = list_price * line['in_production_qty']
                                line['in_transit_cost'] = list_price * line['in_transit_qty']
                                line['in_customer_cost'] = list_price * line['in_customer_qty']
                                line['in_inventory_cost'] = list_price * line['in_supplier_qty']
                                line['cost'] = list_price * line['qty']
                                line['ex_cost'] = list_price * line['ex_qty']
                                line['out_customer_cost'] = list_price * line['out_customer_qty']
                                line['out_pos_customer_cost'] = list_price * line['out_pos_customer_qty']
                                line['out_transit_cost'] = list_price * line['out_transit_qty']
                                line['out_prod_ex_cost'] = list_price * line['out_prod_ex_qty']
                                line['out_supplier_cost'] = list_price * line['out_supplier_qty']
                                line['out_inventory_cost'] = list_price * line['out_inventory_qty']
                                line['out_production_cost'] = list_price * line['out_production_qty']
                                line['end_cost'] = list_price * line['end_qty']
                        elif self.show_purch_price:
                            # Зарах үнийг барааны үнийн түүхээс авах
                            list_price_his = self.env['product.price.history'].search([('product_template_id','=', product.id),
                                                                                   ('price_type','=', 'list_price'),
                                                                                   ('datetime', '<', date_to)], limit=1, order='datetime desc') 
                            list_price = list_price_his.list_price 
                            line['list_price'] = list_price
                            line['start_cost'] = list_price * line['start_qty']
                            line['in_supplier_cost'] = list_price * line['in_supplier_qty']
                            line['in_production_cost'] = list_price * line['in_production_qty']
                            line['in_transit_cost'] = list_price * line['in_transit_qty']
                            line['in_customer_cost'] = list_price * line['in_customer_qty']
                            line['in_inventory_cost'] = list_price * line['in_supplier_qty']
                            line['cost'] = list_price * line['qty']
                            line['ex_cost'] = list_price * line['ex_qty']
                            line['out_customer_cost'] = list_price * line['out_customer_qty']
                            line['out_pos_customer_cost'] = list_price * line['out_pos_customer_qty']
                            line['out_transit_cost'] = list_price * line['out_transit_qty']
                            line['out_prod_ex_cost'] = list_price * line['out_prod_ex_qty']
                            line['out_supplier_cost'] = list_price * line['out_supplier_qty']
                            line['out_inventory_cost'] = list_price * line['out_inventory_qty']
                            line['out_production_cost'] = list_price * line['out_production_qty']
                            line['end_cost'] = list_price * line['end_qty']
                        # Эхний агуулах/данс болон ангиллыг зурна
                        if not supplier_id and line['supp_id']:
                            col = total_col_range_with_cost
                            sheet.write(rowx, 0, sub_seq, format_group_left)
                            sheet.merge_range(rowx, 1, rowx, col, line['supp_name'] or _('Undefined Supplier'), format_group_left)
                            rowx += 1
                            rowd = rowx
                            subtotal_start_qty = subtotal_start_cost = 0
                            subtotal_qty = subtotal_cost = 0
                            subtotal_ex_qty = subtotal_ex_cost = 0
                            subtotal_end_qty = subtotal_end_cost = 0
                            subtotal_in_customer_qty = subtotal_out_customer_qty = subtotal_out_pos_customer_qty = 0
                            subtotal_in_supplier_qty = subtotal_out_supplier_qty = 0
                            subtotal_in_inventory_qty = subtotal_out_inventory_qty = 0
                            subtotal_in_production_qty = subtotal_out_production_qty = 0
                            subtotal_in_transit_qty = subtotal_out_transit_qty = 0
                            subtotal_out_prod_ex_qty = subtotal_out_prod_ex_cost = 0
                            if is_swap:
                                subtotal_in_swap_qty = subtotal_out_swap_qty = 0
                        # Дараагийн ангиллыг зурна
                        elif supplier_id and supplier_id != line['supp_id']:
                            col = 4 if self.show_barcode else 3
                            #                     Дэд дүнгийн мөр
                            sheet, col = self.build_subtotal_row_with_cost(sheet, rowx, col, format_group_right, format_group_float, \
                                     is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                                     subtotal_start_qty, subtotal_start_cost, subtotal_in_supplier_qty, subtotal_in_supplier_cost, \
                                     subtotal_in_swap_qty, subtotal_in_swap_cost, subtotal_in_production_qty, subtotal_in_production_cost, \
                                     subtotal_in_transit_qty, subtotal_in_transit_cost, subtotal_in_customer_qty, subtotal_in_customer_cost, \
                                     subtotal_in_inventory_qty, subtotal_in_inventory_cost, subtotal_other_income, subtotal_other_income_cost, \
                                     subtotal_qty, subtotal_cost, subtotal_out_customer_qty, subtotal_out_customer_cost, \
                                     subtotal_out_swap_qty, subtotal_out_swap_cost, subtotal_out_pos_customer_qty, subtotal_out_pos_customer_cost, \
                                     subtotal_out_transit_qty, subtotal_out_transit_cost, subtotal_out_prod_ex_qty, subtotal_out_prod_ex_cost, \
                                     subtotal_out_supplier_qty, subtotal_out_supplier_cost, subtotal_out_inventory_qty, subtotal_out_inventory_cost, \
                                     subtotal_out_production_qty, subtotal_out_production_cost, subtotal_other_expense, subtotal_other_expense_cost, \
                                     subtotal_ex_qty, subtotal_ex_cost, subtotal_end_qty, subtotal_end_cost)

                            rowx += 1
                            sub_seq += 1
                            sheet.write(rowx, 0, sub_seq, format_group_left)
                            col = total_col_range_with_cost
                            sheet.merge_range(rowx, 1, rowx, col, line['supp_name'] or _('Undefined Supplier'), format_group_left)
                            rowx += 1
                            seq = 1

                            subtotal_start_qty = subtotal_start_cost = 0
                            subtotal_qty = subtotal_cost = 0
                            subtotal_ex_qty = subtotal_ex_cost = 0
                            subtotal_end_qty = subtotal_end_cost = 0
                            subtotal_in_customer_qty = subtotal_out_customer_qty = subtotal_out_pos_customer_qty = 0
                            subtotal_in_supplier_qty = subtotal_out_supplier_qty = 0
                            subtotal_in_inventory_qty = subtotal_out_inventory_qty = 0
                            subtotal_in_production_qty = subtotal_out_production_qty = 0
                            subtotal_in_transit_qty = subtotal_out_transit_qty = 0
                            subtotal_in_swap_qty = subtotal_out_swap_qty = 0
                            subtotal_out_prod_ex_qty = subtotal_out_prod_ex_cost = 0

                        subtotal_start_qty += line['start_qty']
                        subtotal_start_cost += line['start_cost']
                        subtotal_qty += line['qty']
                        subtotal_cost += line['cost']
                        subtotal_ex_qty += line['ex_qty']
                        subtotal_ex_cost += line['ex_cost']
                        subtotal_end_qty += line['end_qty']
                        subtotal_end_cost += line['end_cost']
                        subtotal_in_customer_qty += line['in_customer_qty']
                        subtotal_in_customer_cost += line['in_customer_cost']
                        subtotal_out_customer_qty += line['out_customer_qty']
                        subtotal_out_customer_cost += line['out_customer_cost']
                        subtotal_out_pos_customer_qty += line['out_pos_customer_qty']
                        subtotal_out_pos_customer_cost += line['out_pos_customer_cost']
                        subtotal_in_supplier_qty += line['in_supplier_qty']
                        subtotal_in_supplier_cost += line['in_supplier_cost']
                        subtotal_out_supplier_qty += line['out_supplier_qty']
                        subtotal_out_supplier_cost += line['out_supplier_cost']
                        subtotal_in_inventory_qty += line['in_inventory_qty']
                        subtotal_in_inventory_cost += line['in_inventory_cost']
                        subtotal_out_inventory_qty += line['out_inventory_qty']
                        subtotal_out_inventory_cost += line['out_inventory_cost']
                        subtotal_in_production_qty += line['in_production_qty']
                        subtotal_in_production_cost += line['in_production_cost']
                        subtotal_out_production_qty += line['out_production_qty']
                        subtotal_out_production_cost += line['out_production_cost']
                        subtotal_in_transit_qty += line['in_transit_qty']
                        subtotal_in_transit_cost += line['in_transit_cost']
                        subtotal_out_transit_qty += line['out_transit_qty']
                        subtotal_out_transit_cost += line['out_transit_cost']

                        if is_product_expense:
                            subtotal_out_prod_ex_qty += line.get('out_prod_ex_qty', 0)
                            subtotal_out_prod_ex_cost += line['out_prod_ex_cost']

                        if is_swap:
                            subtotal_in_swap_qty += line['in_swap_qty']
                            subtotal_out_swap_qty += line['out_swap_qty']
                            subtotal_in_swap_cost += line['in_swap_cost']
                            subtotal_out_swap_cost += line['out_swap_cost']
                        # Эцсийн үлдэгдэлийн нэгж өртөг зурах хэсэг
                        unit_cost = 0
                        if line['end_qty'] != 0:
                            unit_cost = abs(line['end_cost']) / abs(line['end_qty'])
                        #                                 Бусад орлогыг тооцох
                        other_income = 0
                        if line['qty'] != 0:
                            other_income = line['qty'] - line['in_customer_qty'] - line['in_supplier_qty'] - line['in_inventory_qty'] - line['in_production_qty'] - line['in_transit_qty']
                            if is_swap:
                                other_income = other_income - line['in_swap_qty']

                        other_income_cost = 0
                        if line['cost'] != 0:
                            other_income_cost = line['cost'] - line['in_customer_cost'] - line['in_supplier_cost'] - line['in_inventory_cost'] - line['in_production_cost'] - line['in_transit_cost']
                            if is_swap:
                                other_income_cost = other_income_cost - line['in_swap_cost']
#                                 Бусад зарлагыг тооцох
                        other_expense = 0
                        if line['ex_qty'] != 0:
                            other_expense = line['ex_qty'] - line['out_customer_qty'] - line['out_pos_customer_qty'] - line['out_supplier_qty'] - line['out_inventory_qty'] - line['out_production_qty'] - line['out_transit_qty']
                            if is_swap:
                                other_expense = other_expense - line['out_swap_qty']
                            if is_product_expense:
                                other_expense = other_expense - line['out_prod_ex_qty']

                        other_expense_cost = 0
                        if line['ex_cost'] != 0:
                            other_expense_cost = line['ex_cost'] - line['out_customer_cost'] - line['out_pos_customer_cost'] - line['out_supplier_cost'] - line['out_inventory_cost'] - line['out_production_cost'] - line['out_transit_cost']
                            if is_swap:
                                other_expense_cost = other_expense_cost - line['out_swap_cost']
                            if is_product_expense:
                                other_expense_cost = other_expense_cost - line['out_prod_ex_cost']

                        subtotal_other_income += other_income
                        subtotal_other_expense += other_expense
                        subtotal_other_income_cost += 0 if other_income == 0 else other_income_cost
                        subtotal_other_expense_cost += 0 if other_expense == 0 else other_expense_cost
                        total_other_income += other_income
                        total_other_expense += other_expense
                        total_other_income_cost += 0 if other_income == 0 else other_income_cost
                        total_other_expense_cost += 0 if other_expense == 0 else other_expense_cost

                        # Нийт дүнг олно
                        total_start_qty += line['start_qty']
                        total_start_cost += 0 if line['start_qty'] == 0 else line['start_cost']
                        total_qty += line['qty']
                        total_cost += line['cost']
                        total_ex_qty += line['ex_qty']
                        total_ex_cost += line['ex_cost']
                        total_end_qty += line['end_qty']
                        total_end_cost += line['end_cost']
                        total_in_customer_qty += line['in_customer_qty']
                        total_in_customer_cost += line['in_customer_cost']
                        total_out_customer_qty += line['out_customer_qty']
                        total_out_customer_cost += line['out_customer_cost']
                        total_out_pos_customer_qty += line['out_pos_customer_qty']
                        total_out_pos_customer_cost += line['out_pos_customer_cost']
                        total_in_supplier_qty += line['in_supplier_qty']
                        total_in_supplier_cost += line['in_supplier_cost']
                        total_out_supplier_qty += line['out_supplier_qty']
                        total_out_supplier_cost += line['out_supplier_cost']
                        total_in_inventory_qty += line['in_inventory_qty']
                        total_in_inventory_cost += line['in_inventory_cost']
                        total_out_inventory_qty += line['out_inventory_qty']
                        total_out_inventory_cost += line['out_inventory_cost']
                        total_in_production_qty += line['in_production_qty']
                        total_in_production_cost += line['in_production_cost']
                        total_out_production_qty += line['out_production_qty']
                        total_out_production_cost += line['out_production_cost']
                        total_in_transit_qty += line['in_transit_qty']
                        total_in_transit_cost += line['in_transit_cost']
                        total_out_transit_qty += line['out_transit_qty']
                        total_out_transit_cost += line['out_transit_cost']

                        if is_product_expense:
                            total_out_prod_ex_qty += line.get('out_prod_ex_qty', 0)
                            total_out_prod_ex_cost += line['out_prod_ex_cost']
                        if is_swap:
                            total_in_swap_qty += line['in_swap_qty']
                            total_out_swap_qty += line['out_swap_qty']
                            total_in_swap_cost += line['in_swap_cost']
                            total_out_swap_cost += line['out_swap_cost']

                        # Барааны мөрийг зурна
                        col = 0
                        sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                        col += 1
                        sheet.write(rowx, col, line['code'], format_content_center)
                        col += 1
                        if self.show_barcode:
                            sheet.write(rowx, col, line['barcode'], format_content_center)
                            col += 1
                        sheet.write(rowx, col, line['name'], format_content_text)
                        col += 1
                        sheet.write(rowx, col, line['uom_name'], format_content_center)
                        col += 1
                        sheet.write(rowx, col, line['start_qty'], format_content_float)
                        col += 1
                        sheet.write(rowx, col, 0 if line['start_qty'] == 0 else line['start_cost'], format_content_float)
                        col += 1

                            #                         Орлогын хэсэг
                        sheet, col = self.build_credit_with_cost(sheet, col, rowx, format_content_float, line, \
                                                                 is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense)
                        sheet.write(rowx, col, other_income, format_content_float)
                        col += 1
                        sheet.write(rowx, col, other_income_cost, format_content_float)
                        col += 1
                        sheet.write(rowx, col, line['qty'], format_group_float)
                        col += 1
                        sheet.write(rowx, col, 0 if line['qty'] == 0 else line['cost'], format_group_float)
                        col += 1

                        #                         Зарлагын хэсэг is_sale, is_swap, is_pos, is_stock, is_product_expense, is_purchase, is_stock, is_mrp
                        sheet, col = self.build_debit_with_cost(sheet, col, rowx, format_content_float, line, \
                                      is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense)
                        sheet.write(rowx, col, other_expense, format_content_float)
                        col += 1
                        sheet.write(rowx, col, other_expense_cost, format_content_float)
                        col += 1
                        sheet.write(rowx, col, line['ex_qty'], format_group_float)
                        col += 1
                        sheet.write(rowx, col, 0 if line['ex_qty'] == 0 else line['ex_cost'], format_group_float)
                        col += 1
                        sheet.write(rowx, col, unit_cost if self.show_purch_price or self.env['product.product'].browse(
                            line['pid']).cost_method != 'real' else '', format_content_float)
                        col += 1
                        sheet.write(rowx, col, line['end_qty'], format_content_float)
                        col += 1
                        sheet.write(rowx, col, 0 if line['end_qty'] == 0 else line['end_cost'], format_content_float)
                        col += 1
                        rowx += 1
                        seq += 1
                        supplier_id = line['supp_id']

                    #                     Дэд дүнгийн мөр
                    col = 4 if self.show_barcode else 3
                    sheet, col = self.build_subtotal_row_with_cost(sheet, rowx, col, format_group_right, format_group_float, \
                                     is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                                     subtotal_start_qty, subtotal_start_cost, subtotal_in_supplier_qty, subtotal_in_supplier_cost, \
                                     subtotal_in_swap_qty, subtotal_in_swap_cost, subtotal_in_production_qty, subtotal_in_production_cost, \
                                     subtotal_in_transit_qty, subtotal_in_transit_cost, subtotal_in_customer_qty, subtotal_in_customer_cost, \
                                     subtotal_in_inventory_qty, subtotal_in_inventory_cost, subtotal_other_income, subtotal_other_income_cost, \
                                     subtotal_qty, subtotal_cost, subtotal_out_customer_qty, subtotal_out_customer_cost, \
                                     subtotal_out_swap_qty, subtotal_out_swap_cost, subtotal_out_pos_customer_qty, subtotal_out_pos_customer_cost, \
                                     subtotal_out_transit_qty, subtotal_out_transit_cost, subtotal_out_prod_ex_qty, subtotal_out_prod_ex_cost, \
                                     subtotal_out_supplier_qty, subtotal_out_supplier_cost, subtotal_out_inventory_qty, subtotal_out_inventory_cost, \
                                     subtotal_out_production_qty, subtotal_out_production_cost, subtotal_other_expense, subtotal_other_expense_cost, \
                                     subtotal_ex_qty, subtotal_ex_cost, subtotal_end_qty, subtotal_end_cost)

                    #                     Нийт дүнгийн мөр
                    rowx += 1
                    col = 4 if self.show_barcode else 3
                    sheet, col = self.build_total_row_with_cost(sheet, rowx, col, format_title, format_title_float, \
                                  is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                                  total_start_qty, total_start_cost, total_in_supplier_qty, total_in_supplier_cost, \
                                  total_in_swap_qty, total_in_swap_cost, total_in_production_qty, total_in_production_cost, \
                                  total_in_transit_qty, total_in_transit_cost, total_in_customer_qty, total_in_customer_cost, \
                                  total_in_inventory_qty, total_in_inventory_cost, total_other_income, total_other_income_cost, \
                                  total_qty, total_cost, total_out_customer_qty, total_out_customer_cost, \
                                  total_out_swap_qty, total_out_swap_cost, total_out_pos_customer_qty, total_out_pos_customer_cost, \
                                  total_out_transit_qty, total_out_transit_cost, total_out_prod_ex_qty, total_out_prod_ex_cost, \
                                  total_out_supplier_qty, total_out_supplier_cost, total_out_inventory_qty, total_out_inventory_cost, \
                                  total_out_production_qty, total_out_production_cost, total_other_expense, total_other_expense_cost, \
                                  total_ex_qty, total_ex_cost, total_end_qty, total_end_cost)
                    rowx += 1

                #                 Данс төрлөөр өртөгтэй
                elif self.type == 'account':
                    sub_seq = 1
                    seq = 1
                    for line in lines:
                        product = self.env['product.template'].search([('id','=', line['ptid']),('active','=', True)])
                        if self.pricelist_id:
                            # Зарах үнийг үнийн хүснэгтээс авах
                            if product:
                                list_price = self.pricelist_id.get_product_price(product, line['qty'], False, date_to, False)
                                line['list_price'] = list_price
                                line['start_cost'] = list_price * line['start_qty']
                                line['in_supplier_cost'] = list_price * line['in_supplier_qty']
                                line['in_production_cost'] = list_price * line['in_production_qty']
                                line['in_transit_cost'] = list_price * line['in_transit_qty']
                                line['in_customer_cost'] = list_price * line['in_customer_qty']
                                line['in_inventory_cost'] = list_price * line['in_supplier_qty']
                                line['cost'] = list_price * line['qty']
                                line['ex_cost'] = list_price * line['ex_qty']
                                line['out_customer_cost'] = list_price * line['out_customer_qty']
                                line['out_pos_customer_cost'] = list_price * line['out_pos_customer_qty']
                                line['out_transit_cost'] = list_price * line['out_transit_qty']
                                line['out_prod_ex_cost'] = list_price * line['out_prod_ex_qty']
                                line['out_supplier_cost'] = list_price * line['out_supplier_qty']
                                line['out_inventory_cost'] = list_price * line['out_inventory_qty']
                                line['out_production_cost'] = list_price * line['out_production_qty']
                                line['end_cost'] = list_price * line['end_qty']
                        elif self.show_purch_price:
                            # Зарах үнийг барааны үнийн түүхээс авах
                            list_price_his = self.env['product.price.history'].search([('product_template_id','=', product.id),
                                                                                   ('price_type','=', 'list_price'),
                                                                                   ('datetime', '<', date_to)], limit=1, order='datetime desc') 
                            list_price = list_price_his.list_price 
                            line['list_price'] = list_price
                            line['start_cost'] = list_price * line['start_qty']
                            line['in_supplier_cost'] = list_price * line['in_supplier_qty']
                            line['in_production_cost'] = list_price * line['in_production_qty']
                            line['in_transit_cost'] = list_price * line['in_transit_qty']
                            line['in_customer_cost'] = list_price * line['in_customer_qty']
                            line['in_inventory_cost'] = list_price * line['in_supplier_qty']
                            line['cost'] = list_price * line['qty']
                            line['ex_cost'] = list_price * line['ex_qty']
                            line['out_customer_cost'] = list_price * line['out_customer_qty']
                            line['out_pos_customer_cost'] = list_price * line['out_pos_customer_qty']
                            line['out_transit_cost'] = list_price * line['out_transit_qty']
                            line['out_prod_ex_cost'] = list_price * line['out_prod_ex_qty']
                            line['out_supplier_cost'] = list_price * line['out_supplier_qty']
                            line['out_inventory_cost'] = list_price * line['out_inventory_qty']
                            line['out_production_cost'] = list_price * line['out_production_qty']
                            line['end_cost'] = list_price * line['end_qty']
                        # Эхний агуулах/данс болон ангиллыг зурна
                        if not account_id:
                            col = total_col_range_with_cost
                            sheet.write(rowx, 0, sub_seq, format_group_left)
                            sheet.merge_range(rowx, 1, rowx, col, line['account_name'], format_group_left)
                            rowx += 1
                            rowd = rowx
                            subtotal_start_qty = subtotal_start_cost = 0
                            subtotal_qty = subtotal_cost = 0
                            subtotal_ex_qty = subtotal_ex_cost = 0
                            subtotal_end_qty = subtotal_end_cost = 0
                            subtotal_in_customer_qty = subtotal_out_customer_qty = subtotal_out_pos_customer_qty = 0
                            subtotal_in_supplier_qty = subtotal_out_supplier_qty = 0
                            subtotal_in_inventory_qty = subtotal_out_inventory_qty = 0
                            subtotal_in_production_qty = subtotal_out_production_qty = 0
                            subtotal_in_transit_qty = subtotal_out_transit_qty = 0
                            subtotal_in_swap_qty = subtotal_out_swap_qty = 0
                            subtotal_out_prod_ex_qty = subtotal_out_prod_ex_cost = 0
                        # Дараагийн ангиллыг зурна
                        elif account_id and account_id != line['account_id']:
                            sub_seq += 1
                            seq = 1
                            col = 4 if self.show_barcode else 3
                            #                     Дэд дүнгийн мөр
                            sheet, col = self.build_subtotal_row_with_cost(sheet, rowx, col, format_group_right, format_group_float, \
                                     is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                                     subtotal_start_qty, subtotal_start_cost, subtotal_in_supplier_qty, subtotal_in_supplier_cost, \
                                     subtotal_in_swap_qty, subtotal_in_swap_cost, subtotal_in_production_qty, subtotal_in_production_cost, \
                                     subtotal_in_transit_qty, subtotal_in_transit_cost, subtotal_in_customer_qty, subtotal_in_customer_cost, \
                                     subtotal_in_inventory_qty, subtotal_in_inventory_cost, subtotal_other_income, subtotal_other_income_cost, \
                                     subtotal_qty, subtotal_cost, subtotal_out_customer_qty, subtotal_out_customer_cost, \
                                     subtotal_out_swap_qty, subtotal_out_swap_cost, subtotal_out_pos_customer_qty, subtotal_out_pos_customer_cost, \
                                     subtotal_out_transit_qty, subtotal_out_transit_cost, subtotal_out_prod_ex_qty, subtotal_out_prod_ex_cost, \
                                     subtotal_out_supplier_qty, subtotal_out_supplier_cost, subtotal_out_inventory_qty, subtotal_out_inventory_cost, \
                                     subtotal_out_production_qty, subtotal_out_production_cost, subtotal_other_expense, subtotal_other_expense_cost, \
                                     subtotal_ex_qty, subtotal_ex_cost, subtotal_end_qty, subtotal_end_cost)
                            rowx += 1

                            col = total_col_range_with_cost
                            sheet.write(rowx, 0, sub_seq, format_group_left)
                            sheet.merge_range(rowx, 1, rowx, col, line['account_name'], format_group_left)
                            rowx += 1
                            rowd = rowx

                            subtotal_start_qty = subtotal_start_cost = 0
                            subtotal_qty = subtotal_cost = 0
                            subtotal_ex_qty = subtotal_ex_cost = 0
                            subtotal_end_qty = subtotal_end_cost = 0
                            subtotal_in_customer_qty = subtotal_out_customer_qty = subtotal_out_pos_customer_qty = 0
                            subtotal_in_supplier_qty = subtotal_out_supplier_qty = 0
                            subtotal_in_inventory_qty = subtotal_out_inventory_qty = 0
                            subtotal_in_production_qty = subtotal_out_production_qty = 0
                            subtotal_in_transit_qty = subtotal_out_transit_qty = 0
                            subtotal_in_swap_qty = subtotal_out_swap_qty = 0
                            subtotal_out_prod_ex_qty = subtotal_out_prod_ex_cost = 0
                        #
                        subtotal_start_qty += line['start_qty']
                        subtotal_start_cost += line['start_cost']
                        subtotal_qty += line['qty']
                        subtotal_cost += line['cost']
                        subtotal_ex_qty += line['ex_qty']
                        subtotal_ex_cost += line['ex_cost']
                        subtotal_end_qty += line['end_qty']
                        subtotal_end_cost += line['end_cost']
                        subtotal_in_customer_qty += line['in_customer_qty']
                        subtotal_in_customer_cost += line['in_customer_cost']
                        subtotal_out_customer_qty += line['out_customer_qty']
                        subtotal_out_customer_cost += line['out_customer_cost']
                        subtotal_out_pos_customer_qty += line['out_pos_customer_qty']
                        subtotal_out_pos_customer_cost += line['out_pos_customer_cost']
                        subtotal_in_supplier_qty += line['in_supplier_qty']
                        subtotal_in_supplier_cost += line['in_supplier_cost']
                        subtotal_out_supplier_qty += line['out_supplier_qty']
                        subtotal_out_supplier_cost += line['out_supplier_cost']
                        subtotal_in_inventory_qty += line['in_inventory_qty']
                        subtotal_in_inventory_cost += line['in_inventory_cost']
                        subtotal_out_inventory_qty += line['out_inventory_qty']
                        subtotal_out_inventory_cost += line['out_inventory_cost']
                        subtotal_in_production_qty += line['in_production_qty']
                        subtotal_in_production_cost += line['in_production_cost']
                        subtotal_out_production_qty += line['out_production_qty']
                        subtotal_out_production_cost += line['out_production_cost']
                        subtotal_in_transit_qty += line['in_transit_qty']
                        subtotal_in_transit_cost += line['in_transit_cost']
                        subtotal_out_transit_qty += line['out_transit_qty']
                        subtotal_out_transit_cost += line['out_transit_cost']

                        if is_product_expense:
                            subtotal_out_prod_ex_qty += line.get('out_prod_ex_qty', 0)
                            subtotal_out_prod_ex_cost += line['out_prod_ex_cost']

                        if is_swap:
                            subtotal_in_swap_qty += line['in_swap_qty']
                            subtotal_out_swap_qty += line['out_swap_qty']
                            subtotal_in_swap_cost += line['in_swap_cost']
                            subtotal_out_swap_cost += line['out_swap_cost']
                        # Эцсийн үлдэгдэлийн нэгж өртөг зурах хэсэг
                        unit_cost = 0
                        if line['end_qty'] != 0:
                            unit_cost = abs(line['end_cost']) / abs(line['end_qty'])
                        #                                 Бусад орлогыг тооцох
                        other_income = 0
                        if line['qty'] != 0:
                            other_income = line['qty'] - line['in_customer_qty'] - line['in_supplier_qty'] - line['in_inventory_qty'] - line['in_production_qty'] - line['in_transit_qty']
                            if is_swap:
                                other_income = other_income - line['in_swap_qty']

                        other_income_cost = 0
                        if line['cost'] != 0:
                            other_income_cost = line['cost'] - line['in_customer_cost'] - line['in_supplier_cost'] - line['in_inventory_cost'] - line['in_production_cost'] - line['in_transit_cost']
                            if is_swap:
                                other_income_cost = other_income_cost - line['in_swap_cost']

#                                 Бусад зарлагыг тооцох
                        other_expense = 0
                        if line['ex_qty'] != 0:
                            other_expense = line['ex_qty'] - line['out_customer_qty'] - line['out_pos_customer_qty'] - line['out_supplier_qty'] - line['out_inventory_qty'] - line['out_production_qty'] - line['out_transit_qty']
                            if is_swap:
                                other_expense = other_expense - line['out_swap_qty']
                            if is_product_expense:
                                other_expense = other_expense - line['out_prod_ex_qty']

                        other_expense_cost = 0
                        if line['ex_cost'] != 0:
                            other_expense_cost = line['ex_cost'] - line['out_customer_cost'] - line['out_pos_customer_cost'] - line['out_supplier_cost'] - line['out_inventory_cost'] - line['out_production_cost'] - line['out_transit_cost']
                            if is_swap:
                                other_expense_cost = other_expense_cost - line['out_swap_cost']
                            if is_product_expense:
                                other_expense_cost = other_expense_cost - line['out_prod_ex_cost']

                        subtotal_other_income += other_income
                        subtotal_other_expense += other_expense
                        subtotal_other_income_cost += 0 if other_income == 0 else other_income_cost
                        subtotal_other_expense_cost += 0 if other_expense == 0 else other_expense_cost
                        total_other_income += other_income
                        total_other_expense += other_expense
                        total_other_income_cost += 0 if other_income == 0 else other_income_cost
                        total_other_expense_cost += 0 if other_expense == 0 else other_expense_cost

                        # Нийт дүнг олно
                        total_start_qty += line['start_qty']
                        total_start_cost += 0 if line['start_qty'] == 0 else line['start_cost']
                        total_qty += line['qty']
                        total_cost += line['cost']
                        total_ex_qty += line['ex_qty']
                        total_ex_cost += line['ex_cost']
                        total_end_qty += line['end_qty']
                        total_end_cost += line['end_cost']
                        total_in_customer_qty += line['in_customer_qty']
                        total_in_customer_cost += line['in_customer_cost']
                        total_out_customer_qty += line['out_customer_qty']
                        total_out_customer_cost += line['out_customer_cost']
                        total_out_pos_customer_qty += line['out_pos_customer_qty']
                        total_out_pos_customer_cost += line['out_pos_customer_cost']
                        total_in_supplier_qty += line['in_supplier_qty']
                        total_in_supplier_cost += line['in_supplier_cost']
                        total_out_supplier_qty += line['out_supplier_qty']
                        total_out_supplier_cost += line['out_supplier_cost']
                        total_in_inventory_qty += line['in_inventory_qty']
                        total_in_inventory_cost += line['in_inventory_cost']
                        total_out_inventory_qty += line['out_inventory_qty']
                        total_out_inventory_cost += line['out_inventory_cost']
                        total_in_production_qty += line['in_production_qty']
                        total_in_production_cost += line['in_production_cost']
                        total_out_production_qty += line['out_production_qty']
                        total_out_production_cost += line['out_production_cost']
                        total_in_transit_qty += line['in_transit_qty']
                        total_in_transit_cost += line['in_transit_cost']
                        total_out_transit_qty += line['out_transit_qty']
                        total_out_transit_cost += line['out_transit_cost']

                        if is_product_expense:
                            total_out_prod_ex_qty += line.get('out_prod_ex_qty', 0)
                            total_out_prod_ex_cost += line['out_prod_ex_cost']
                        if is_swap:
                            total_in_swap_qty += line['in_swap_qty']
                            total_out_swap_qty += line['out_swap_qty']
                            total_in_swap_cost += line['in_swap_cost']
                            total_out_swap_cost += line['out_swap_cost']

                        # Барааны мөрийг зурна
                        col = 0
                        sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                        col += 1
                        sheet.write(rowx, col, line['code'], format_content_center)
                        col += 1
                        if self.show_barcode:
                            sheet.write(rowx, col, line['barcode'], format_content_center)
                            col += 1
                        sheet.write(rowx, col, line['name'], format_content_text)
                        col += 1
                        sheet.write(rowx, col, line['uom_name'], format_content_center)
                        col += 1
                        sheet.write(rowx, col, line['start_qty'], format_content_float)
                        col += 1
                        sheet.write(rowx, col, 0 if line['start_qty'] == 0 else line['start_cost'], format_content_float)
                        col += 1

                        #                         Орлогын хэсэг
                        sheet, col = self.build_credit_with_cost(sheet, col, rowx, format_content_float, line, \
                                                                 is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense)
                        sheet.write(rowx, col, other_income, format_content_float)
                        col += 1
                        sheet.write(rowx, col, other_income_cost, format_content_float)
                        col += 1
                        sheet.write(rowx, col, line['qty'], format_group_float)
                        col += 1
                        sheet.write(rowx, col, 0 if line['qty'] == 0 else line['cost'], format_group_float)
                        col += 1

                        #                         Зарлагын хэсэг is_sale, is_swap, is_pos, is_stock, is_product_expense, is_purchase, is_stock, is_mrp
                        sheet, col = self.build_debit_with_cost(sheet, col, rowx, format_content_float, line, \
                                      is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense)
                        sheet.write(rowx, col, other_expense, format_content_float)
                        col += 1
                        sheet.write(rowx, col, other_expense_cost, format_content_float)
                        col += 1
                        sheet.write(rowx, col, line['ex_qty'], format_group_float)
                        col += 1
                        sheet.write(rowx, col, 0 if line['ex_qty'] == 0 else line['ex_cost'], format_group_float)
                        col += 1
                        sheet.write(rowx, col, unit_cost if self.show_purch_price or self.env['product.product'].browse(
                            line['pid']).cost_method != 'real' else '', format_content_float)
                        col += 1
                        sheet.write(rowx, col, line['end_qty'], format_content_float)
                        col += 1
                        sheet.write(rowx, col, 0 if line['end_qty'] == 0 else line['end_cost'], format_content_float)
                        col += 1
                        rowx += 1
                        seq += 1
                        account_id = line['account_id']

                    col = 4 if self.show_barcode else 3
                    #                     Дэд дүнгийн мөр
                    sheet, col = self.build_subtotal_row_with_cost(sheet, rowx, col, format_group_right, format_group_float, \
                                     is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                                     subtotal_start_qty, subtotal_start_cost, subtotal_in_supplier_qty, subtotal_in_supplier_cost, \
                                     subtotal_in_swap_qty, subtotal_in_swap_cost, subtotal_in_production_qty, subtotal_in_production_cost, \
                                     subtotal_in_transit_qty, subtotal_in_transit_cost, subtotal_in_customer_qty, subtotal_in_customer_cost, \
                                     subtotal_in_inventory_qty, subtotal_in_inventory_cost, subtotal_other_income, subtotal_other_income_cost, \
                                     subtotal_qty, subtotal_cost, subtotal_out_customer_qty, subtotal_out_customer_cost, \
                                     subtotal_out_swap_qty, subtotal_out_swap_cost, subtotal_out_pos_customer_qty, subtotal_out_pos_customer_cost, \
                                     subtotal_out_transit_qty, subtotal_out_transit_cost, subtotal_out_prod_ex_qty, subtotal_out_prod_ex_cost, \
                                     subtotal_out_supplier_qty, subtotal_out_supplier_cost, subtotal_out_inventory_qty, subtotal_out_inventory_cost, \
                                     subtotal_out_production_qty, subtotal_out_production_cost, subtotal_other_expense, subtotal_other_expense_cost, \
                                     subtotal_ex_qty, subtotal_ex_cost, subtotal_end_qty, subtotal_end_cost)

                    #                         Нийт дүнгийн мөр
                    rowx += 1
                    col = 4 if self.show_barcode else 3
                    sheet, col = self.build_total_row_with_cost(sheet, rowx, col, format_title, format_title_float, \
                                  is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                                  total_start_qty, total_start_cost, total_in_supplier_qty, total_in_supplier_cost, \
                                  total_in_swap_qty, total_in_swap_cost, total_in_production_qty, total_in_production_cost, \
                                  total_in_transit_qty, total_in_transit_cost, total_in_customer_qty, total_in_customer_cost, \
                                  total_in_inventory_qty, total_in_inventory_cost, total_other_income, total_other_income_cost, \
                                  total_qty, total_cost, total_out_customer_qty, total_out_customer_cost, \
                                  total_out_swap_qty, total_out_swap_cost, total_out_pos_customer_qty, total_out_pos_customer_cost, \
                                  total_out_transit_qty, total_out_transit_cost, total_out_prod_ex_qty, total_out_prod_ex_cost, \
                                  total_out_supplier_qty, total_out_supplier_cost, total_out_inventory_qty, total_out_inventory_cost, \
                                  total_out_production_qty, total_out_production_cost, total_other_expense, total_other_expense_cost, \
                                  total_ex_qty, total_ex_cost, total_end_qty, total_end_cost)
                    rowx += 1

                #                     Өртөггүй үед
            else:
                #                 Бүлэглэлтгүй болон өртөггүй
                if self.type == 'no_grouping':
                    for line in lines:
                        subtotal_start_qty += line['start_qty']
                        subtotal_qty += line['qty']
                        subtotal_ex_qty += line['ex_qty']
                        subtotal_end_qty += line['end_qty']
                        subtotal_in_customer_qty += line['in_customer_qty']
                        subtotal_out_customer_qty += line['out_customer_qty']
                        subtotal_out_pos_customer_qty += line['out_pos_customer_qty']
                        subtotal_in_supplier_qty += line['in_supplier_qty']
                        subtotal_out_supplier_qty += line['out_supplier_qty']
                        subtotal_in_inventory_qty += line['in_inventory_qty']
                        subtotal_out_inventory_qty += line['out_inventory_qty']
                        subtotal_in_production_qty += line['in_production_qty']
                        subtotal_out_production_qty += line['out_production_qty']
                        subtotal_in_transit_qty += line['in_transit_qty']
                        subtotal_out_transit_qty += line['out_transit_qty']
                        if is_swap:
                            subtotal_in_swap_qty += line['in_swap_qty']
                            subtotal_out_swap_qty += line['out_swap_qty']
                        if is_product_expense:
                            subtotal_out_prod_ex_qty += line.get('out_prod_ex_qty', 0)

                        #                                   Бусад орлогыг тооцох
                        other_income = 0
                        if line['qty'] > 0:
                            other_income = line['qty'] - line['in_customer_qty'] - line['in_supplier_qty'] - line['in_inventory_qty'] - line['in_production_qty'] - line['in_transit_qty']
                            if is_swap:
                                other_income = other_income - line['in_swap_qty']

                        other_income_cost = 0
                        if line['cost'] != 0:
                            other_income_cost = line['cost'] - line['in_customer_cost'] - line['in_supplier_cost'] - line['in_inventory_cost'] - line['in_production_cost'] - line['in_transit_cost']
                            if is_swap:
                                other_income_cost = other_income_cost - line['in_swap_cost']

#                                 Бусад зарлагыг тооцох
                        other_expense = 0
                        if line['ex_qty'] > 0:
                            other_expense = line['ex_qty'] - line['out_customer_qty'] - line['out_pos_customer_qty'] - line['out_supplier_qty'] - line['out_inventory_qty'] - line['out_production_qty'] - line['out_transit_qty']
                            if is_swap:
                                other_expense = other_expense - line['out_swap_qty']
                            if is_product_expense:
                                other_expense = other_expense - line['out_prod_ex_qty']

                        other_expense_cost = 0
                        if line['ex_cost'] != 0:
                            other_expense_cost = line['ex_cost'] - line['out_customer_cost'] - line['out_pos_customer_cost'] - line['out_supplier_cost'] - line['out_inventory_cost'] - line['out_production_cost'] - line['out_transit_cost']
                            if is_swap:
                                other_expense_cost = other_expense_cost - line['out_swap_cost']
                            if is_product_expense:
                                other_expense_cost = other_expense_cost - line['out_prod_ex_cost']

                        subtotal_other_income += other_income
                        subtotal_other_expense += other_expense

                        # Нийт дүнг олно
                        total_start_qty += line['start_qty']
                        total_qty += line['qty']
                        total_ex_qty += line['ex_qty']
                        total_end_qty += line['end_qty']
                        total_in_customer_qty += line['in_customer_qty']
                        total_out_customer_qty += line['out_customer_qty']
                        if is_pos:
                            total_out_pos_customer_qty += line['out_pos_customer_qty']
                        total_in_supplier_qty += line['in_supplier_qty']
                        total_out_supplier_qty += line['out_supplier_qty']
                        total_in_inventory_qty += line['in_inventory_qty']
                        total_out_inventory_qty += line['out_inventory_qty']
                        if is_mrp:
                            total_in_production_qty += line['in_production_qty']
                            total_out_production_qty += line['out_production_qty']
                        total_in_transit_qty += line['in_transit_qty']
                        total_out_transit_qty += line['out_transit_qty']
                        if is_swap:
                            total_in_swap_qty += line['in_swap_qty']
                            total_out_swap_qty += line['out_swap_qty']
                        if is_product_expense:
                            total_out_prod_ex_qty += line.get('out_prod_ex_qty', 0)
                        # Барааны мөрийг зурна
                        col = 0
                        sheet.write(rowx, col, '%s' % seq, format_content_center)
                        col += 1
                        sheet.write(rowx, col, line['code'], format_content_center)
                        col += 1
                        if self.show_barcode:
                            sheet.write(rowx, col, line['barcode'], format_content_center)
                            col += 1

                        sheet.write(rowx, col, line['name'], format_content_text)
                        col += 1
                        sheet.write(rowx, col, line['uom_name'], format_content_center)
                        col += 1
                        sheet.write(rowx, col, line['start_qty'], format_content_float)
                        col += 1

                        #                         Орлогын хэсэг
                        sheet, col = self.build_credit(sheet, col, rowx, format_content_float, line, \
                                                       is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense)
                        sheet.write(rowx, col, other_income, format_content_float)
                        col += 1
                        sheet.write(rowx, col, line['qty'], format_group_float)
                        col += 1

                        #                         Зарлагын хэсэг is_sale, is_swap, is_pos, is_stock, is_product_expense, is_purchase, is_stock, is_mrp
                        sheet, col = self.build_debit(sheet, col, rowx, format_content_float, line, \
                                                      is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense)
                        sheet.write(rowx, col, other_expense, format_content_float)
                        col += 1
                        sheet.write(rowx, col, line['ex_qty'], format_group_float)
                        col += 1
                        sheet.write(rowx, col, line['end_qty'], format_content_float)
                        col += 1
                        rowx += 1
                        seq += 1

                    col = 4 if self.show_barcode else 3
                    #                     Дэд дүнгийн мөр
                    sheet, col = self.build_subtotal_row(sheet, rowx, col, format_group_right, format_group_float, \
                           is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                           subtotal_start_qty, subtotal_in_supplier_qty, subtotal_in_swap_qty, subtotal_in_production_qty, \
                           subtotal_in_transit_qty, subtotal_in_customer_qty, subtotal_in_inventory_qty, subtotal_other_income, \
                           subtotal_qty, subtotal_out_customer_qty, subtotal_out_swap_qty, subtotal_out_pos_customer_qty, \
                           subtotal_out_transit_qty, subtotal_out_prod_ex_qty, subtotal_out_supplier_qty, subtotal_out_inventory_qty, \
                           subtotal_out_production_qty, subtotal_other_expense, subtotal_ex_qty, subtotal_end_qty)

                    #                     Нийт дүнгийн мөр
                    rowx += 1
                    col = 4 if self.show_barcode else 3
                    sheet, col = self.build_total_row(sheet, rowx, col, format_group, format_title_float, \
                        is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                        total_start_qty, total_in_supplier_qty, total_in_swap_qty, total_in_production_qty, \
                        total_in_transit_qty, total_in_customer_qty, total_in_inventory_qty, \
                        total_other_income, total_qty, total_out_pos_customer_qty, \
                        total_out_customer_qty, total_out_swap_qty, total_out_transit_qty, \
                        total_out_prod_ex_qty, total_out_supplier_qty, total_out_inventory_qty, total_out_production_qty, \
                        total_other_expense, total_ex_qty, total_end_qty)
                    rowx += 1

                            #                 АГУУЛАХ ТӨРӨЛТЭЙ
                if self.type == 'warehouse':
                    if self.group_by == 'by_category':
                        for line in lines:
                            # Эхний агуулах/данс болон ангиллыг зурна
                            if not type_id and not category_id:
                                name = (('[' + line['wcode'] + '] ') if line['wcode'] else '') + line['wname'] if line['wname'] else ''
                                col = total_col_range
                                sheet.merge_range(rowx, 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 1, rowx, col, line['cname'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx

                                subtotal_start_qty = 0
                                subtotal_qty = 0
                                subtotal_ex_qty = 0
                                subtotal_end_qty = 0
                                subtotal_in_customer_qty = subtotal_out_customer_qty = subtotal_out_pos_customer_qty = 0
                                subtotal_in_supplier_qty = subtotal_out_supplier_qty = 0
                                subtotal_in_inventory_qty = subtotal_out_inventory_qty = 0
                                subtotal_in_production_qty = subtotal_out_production_qty = 0
                                subtotal_in_transit_qty = subtotal_out_transit_qty = 0
                                subtotal_in_swap_qty = subtotal_out_swap_qty = 0
                                subtotal_out_prod_ex_qty = 0
                            # Дараагийн агуулах
                            elif type_id and type_id != line['wcode']:
                                sub_seq = 1
                                seq = 1
                                col = 4 if self.show_barcode else 3
                                name = (('[' + line['wcode'] + '] ') if line['wcode'] else '') + line['wname'] if line['wname'] else ''
                                col = total_col_range
                                sheet.merge_range(rowx, 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 0, sub_seq, format_content_number)
                                sheet.merge_range(rowx, 1, rowx, col, line['cname'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн ангиллыг зурна
                            elif category_id and category_id != line['cid']:
                                col = 4 if self.show_barcode else 3
                                # Дэд дүнгийн мөр
                                sheet, col = self.build_subtotal_row(sheet, rowx, col, format_group_right, format_group_float, \
                                                                                       is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                           subtotal_start_qty, subtotal_in_supplier_qty, subtotal_in_swap_qty, subtotal_in_production_qty, \
                           subtotal_in_transit_qty, subtotal_in_customer_qty, subtotal_in_inventory_qty, subtotal_other_income, \
                           subtotal_qty, subtotal_out_customer_qty, subtotal_out_swap_qty, subtotal_out_pos_customer_qty, \
                           subtotal_out_transit_qty, subtotal_out_prod_ex_qty, subtotal_out_supplier_qty, subtotal_out_inventory_qty, \
                           subtotal_out_production_qty, subtotal_other_expense, subtotal_ex_qty, subtotal_end_qty)

                                rowx += 1
                                sub_seq += 1
                                sheet.write(rowx, 0, sub_seq, format_group_left)
                                col = total_col_range
                                sheet.merge_range(rowx, 1, rowx, col, line['cname'], format_group_left)
                                rowx += 1
                                seq = 1

                                subtotal_start_qty = 0
                                subtotal_qty = 0
                                subtotal_ex_qty = 0
                                subtotal_end_qty = 0
                                subtotal_in_customer_qty = subtotal_out_customer_qty = subtotal_out_pos_customer_qty = 0
                                subtotal_in_supplier_qty = subtotal_out_supplier_qty = 0
                                subtotal_in_inventory_qty = subtotal_out_inventory_qty = 0
                                subtotal_in_production_qty = subtotal_out_production_qty = 0
                                subtotal_in_transit_qty = subtotal_out_transit_qty = 0
                                subtotal_in_swap_qty = subtotal_out_swap_qty = 0
                                subtotal_out_prod_ex_qty = 0

                            subtotal_start_qty += line['start_qty']
                            subtotal_qty += line['qty']
                            subtotal_ex_qty += line['ex_qty']
                            subtotal_end_qty += line['end_qty']
                            subtotal_in_customer_qty += line['in_customer_qty']
                            subtotal_out_customer_qty += line['out_customer_qty']
                            subtotal_out_pos_customer_qty += line['out_pos_customer_qty']
                            subtotal_in_supplier_qty += line['in_supplier_qty']
                            subtotal_out_supplier_qty += line['out_supplier_qty']
                            subtotal_in_inventory_qty += line['in_inventory_qty']
                            subtotal_out_inventory_qty += line['out_inventory_qty']
                            subtotal_in_production_qty += line['in_production_qty']
                            subtotal_out_production_qty += line['out_production_qty']
                            subtotal_in_transit_qty += line['in_transit_qty']
                            subtotal_out_transit_qty += line['out_transit_qty']
                            if is_swap:
                                subtotal_in_swap_qty += line['in_swap_qty']
                                subtotal_out_swap_qty += line['out_swap_qty']

                            if is_product_expense:
                                subtotal_out_prod_ex_qty += line.get('out_prod_ex_qty', 0)

                            if is_swap:
                                subtotal_in_swap_qty += line['in_swap_qty']
                                subtotal_out_swap_qty += line['out_swap_qty']

                            #                                   Бусад орлогыг тооцох
                            other_income = 0
                            if line['qty'] > 0:
                                other_income = line['qty'] - line['in_customer_qty'] - line['in_supplier_qty'] - line['in_inventory_qty'] - line['in_production_qty'] - line['in_transit_qty']
                                if is_swap:
                                    other_income = other_income - line['in_swap_qty']
                            other_income_cost = 0
                            if line['cost'] != 0:
                                other_income_cost = line['cost'] - line['in_customer_cost'] - line['in_supplier_cost'] - line['in_inventory_cost'] - line['in_production_cost'] - line['in_transit_cost']
                                if is_swap:
                                    other_income_cost = other_income_cost - line['in_swap_cost']
#                                 Бусад зарлагыг тооцох
                            other_expense = 0
                            if line['ex_qty'] > 0:
                                other_expense = line['ex_qty'] - line['out_customer_qty'] - line['out_pos_customer_qty'] - line['out_supplier_qty'] - line['out_inventory_qty'] - line['out_production_qty'] - line['out_transit_qty']
                                if is_swap:
                                    other_expense = other_expense - line['out_swap_qty']
                                if is_product_expense:
                                    other_expense = other_expense - line['out_prod_ex_qty']
                            other_expense_cost = 0
                            if line['ex_cost'] != 0:
                                other_expense_cost = line['ex_cost'] - line['out_customer_cost'] - line['out_pos_customer_cost'] - line['out_supplier_cost'] - line['out_inventory_cost'] - line['out_production_cost'] - line['out_transit_cost']
                                if is_swap:
                                    other_expense_cost = other_expense_cost - line['out_swap_cost']
                                if is_product_expense:
                                    other_expense_cost = other_expense_cost - line['out_prod_ex_cost']

                            subtotal_other_income += other_income
                            subtotal_other_expense += other_expense

                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_qty += line['qty']
                            total_ex_qty += line['ex_qty']
                            total_end_qty += line['end_qty']
                            total_in_customer_qty += line['in_customer_qty']
                            total_out_customer_qty += line['out_customer_qty']
                            if is_pos:
                                total_out_pos_customer_qty += line['out_pos_customer_qty']
                            total_in_supplier_qty += line['in_supplier_qty']
                            total_out_supplier_qty += line['out_supplier_qty']
                            total_in_inventory_qty += line['in_inventory_qty']
                            total_out_inventory_qty += line['out_inventory_qty']
                            if is_mrp:
                                total_in_production_qty += line['in_production_qty']
                                total_out_production_qty += line['out_production_qty']
                            total_in_transit_qty += line['in_transit_qty']
                            total_out_transit_qty += line['out_transit_qty']
                            if is_swap:
                                total_in_swap_qty += line['in_swap_qty']
                                total_out_swap_qty += line['out_swap_qty']
                            if is_product_expense:
                                total_out_prod_ex_qty += line.get('out_prod_ex_qty', 0)
                            # Барааны мөрийг зурна
                            col = 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1

                            sheet.write(rowx, col, line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1

                            #                         Орлогын хэсэг

                            sheet, col = self.build_credit(sheet, col, rowx, format_content_float, line, \
                                                           is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense)
                            sheet.write(rowx, col, other_income, format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_group_float)
                            col += 1

                            #                         Зарлагын хэсэг is_sale, is_swap, is_pos, is_stock, is_product_expense, is_purchase, is_stock, is_mrp
                            sheet, col = self.build_debit(sheet, col, rowx, format_content_float, line, \
                                                          is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense)
                            sheet.write(rowx, col, other_expense, format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_group_float)
                            col += 1
                            sheet.write(rowx, col, line['end_qty'], format_content_float)
                            col += 1
                            rowx += 1
                            seq += 1
                            category_id = line['cid']
                            type_id = line['wcode']

                        col = 4 if self.show_barcode else 3
                        #                     Дэд дүнгийн мөр
                        sheet, col = self.build_subtotal_row(sheet, rowx, col, format_group_right, format_group_float, \
                                                                               is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                           subtotal_start_qty, subtotal_in_supplier_qty, subtotal_in_swap_qty, subtotal_in_production_qty, \
                           subtotal_in_transit_qty, subtotal_in_customer_qty, subtotal_in_inventory_qty, subtotal_other_income, \
                           subtotal_qty, subtotal_out_customer_qty, subtotal_out_swap_qty, subtotal_out_pos_customer_qty, \
                           subtotal_out_transit_qty, subtotal_out_prod_ex_qty, subtotal_out_supplier_qty, subtotal_out_inventory_qty, \
                           subtotal_out_production_qty, subtotal_other_expense, subtotal_ex_qty, subtotal_end_qty)

                        #                     Нийт дүнгийн мөр
                        rowx += 1
                        col = 4 if self.show_barcode else 3
                        sheet, col = self.build_total_row(sheet, rowx, col, format_group, format_title_float, \
                                                          is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                        total_start_qty, total_in_supplier_qty, total_in_swap_qty, total_in_production_qty, \
                        total_in_transit_qty, total_in_customer_qty, total_in_inventory_qty, \
                        total_other_income, total_qty, total_out_pos_customer_qty, \
                        total_out_customer_qty, total_out_swap_qty, total_out_transit_qty, \
                        total_out_prod_ex_qty, total_out_supplier_qty, total_out_inventory_qty, total_out_production_qty, \
                        total_other_expense, total_ex_qty, total_end_qty)
                        rowx += 1

                    # #                     АГУУЛАХ төрөлтэй ба НИЙЛҮҮЛЭГЧЭЭР бүлэглэнэ
                    elif self.group_by == 'by_supplier':
                        sub_seq = 1
                        seq = 1
                        for line in lines:
                            # Эхний агуулах/данс болон ангиллыг зурна
                            if not type_id and not supplier_id:
                                name = (('[' + line['wcode'] + '] ') if line['wcode'] else '') + line['wname'] if line['wname'] else ''
                                col = total_col_range
                                sheet.merge_range(rowx, 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 1, rowx, col, line['supp_name'] or _('Undefined Supplier'), format_group_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн агуулах/дансыг зурна
                            elif type_id and type_id != line['wcode']:
                                sub_seq = 1
                                seq = 1
                                col = 4 if self.show_barcode else 3
                                sheet.merge_range(rowx, 0, rowx, col, _('Sub Total'), format_group_right)
                                for h in range(col, total_col_range + 1):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowx += 1
                                name = (('[' + line['wcode'] + '] ') if line['wcode'] else '') + line['wname'] if line['wname'] else ''
                                col = total_col_range
                                sheet.merge_range(rowx, 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 0, sub_seq, format_content_number)
                                sheet.merge_range(rowx, 1, rowx, col, line['supp_name'] or _('Undefined Supplier'), format_group_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн ангиллыг зурна
                            elif supplier_id and supplier_id != line['supp_id']:
                                sub_seq += 1
                                seq = 1
                                col = 4 if self.show_barcode else 3
                                sheet, col = self.build_subtotal_row(sheet, rowx, col, format_group_right,
                                                                               format_group_float, \
                                                                               is_purchase, is_swap, is_mrp, is_stock,
                                                                               is_sale, is_pos, is_product_expense, \
                                                                               subtotal_start_qty,
                                                                               subtotal_in_supplier_qty,
                                                                               subtotal_in_swap_qty,
                                                                               subtotal_in_production_qty,
                                                                               subtotal_in_transit_qty,
                                                                               subtotal_in_customer_qty,
                                                                               subtotal_in_inventory_qty,
                                                                               subtotal_other_income,
                                                                               subtotal_qty,
                                                                               subtotal_out_customer_qty,
                                                                               subtotal_out_swap_qty,
                                                                               subtotal_out_pos_customer_qty,
                                                                               subtotal_out_transit_qty,
                                                                               subtotal_out_prod_ex_qty,
                                                                               subtotal_out_supplier_qty,
                                                                               subtotal_out_inventory_qty,
                                                                               subtotal_out_production_qty,
                                                                               subtotal_other_expense,
                                                                               subtotal_ex_qty,
                                                                               subtotal_end_qty)
                                rowx += 1
                                col = total_col_range
                                sheet.write(rowx, 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 1, rowx, col, line['supp_name'] or _('Undefined Supplier'),
                                                  format_group_left)
                                rowx += 1
                                rowd = rowx

                                subtotal_start_qty  = 0
                                subtotal_qty =  0
                                subtotal_ex_qty = 0
                                subtotal_end_qty =0
                                subtotal_in_customer_qty = subtotal_out_customer_qty = subtotal_out_pos_customer_qty = 0
                                subtotal_in_supplier_qty = subtotal_out_supplier_qty = 0
                                subtotal_in_inventory_qty = subtotal_out_inventory_qty = 0
                                subtotal_in_production_qty = subtotal_out_production_qty = 0
                                subtotal_in_transit_qty = subtotal_out_transit_qty = 0
                                subtotal_out_prod_ex_qty =  0
                                if is_swap:
                                    subtotal_in_swap_qty = subtotal_out_swap_qty = 0

                            subtotal_start_qty += line['start_qty']
                            subtotal_qty += line['qty']
                            subtotal_ex_qty += line['ex_qty']
                            subtotal_end_qty += line['end_qty']
                            subtotal_in_customer_qty += line['in_customer_qty']
                            subtotal_out_customer_qty += line['out_customer_qty']
                            subtotal_out_pos_customer_qty += line['out_pos_customer_qty']
                            subtotal_in_supplier_qty += line['in_supplier_qty']
                            subtotal_out_supplier_qty += line['out_supplier_qty']
                            subtotal_in_inventory_qty += line['in_inventory_qty']
                            subtotal_out_inventory_qty += line['out_inventory_qty']
                            subtotal_in_production_qty += line['in_production_qty']
                            subtotal_out_production_qty += line['out_production_qty']
                            subtotal_in_transit_qty += line['in_transit_qty']
                            subtotal_out_transit_qty += line['out_transit_qty']

                            if is_product_expense:
                                subtotal_out_prod_ex_qty += line['out_prod_ex_qty']

                            if is_swap:
                                subtotal_in_swap_qty += line['in_swap_qty']
                                subtotal_out_swap_qty += line['out_swap_qty']

                            #                                   Бусад орлогыг тооцох
                            other_income = 0
                            if line['qty'] > 0:
                                other_income = line['qty'] - line['in_customer_qty'] - line['in_supplier_qty'] - line['in_inventory_qty'] - line['in_production_qty'] - line['in_transit_qty']
                                if is_swap:
                                    other_income = other_income - line['in_swap_qty']

                            other_income_cost = 0
                            if line['cost'] != 0:
                                other_income_cost = line['cost'] - line['in_customer_cost'] - line['in_supplier_cost'] - line['in_inventory_cost'] - line['in_production_cost'] - line['in_transit_cost']
                                if is_swap:
                                    other_income_cost = other_income_cost - line['in_swap_cost']
#                                 Бусад зарлагыг тооцох
                            other_expense = 0
                            if line['ex_qty'] > 0:
                                other_expense = line['ex_qty'] - line['out_customer_qty'] - line['out_pos_customer_qty'] - line['out_supplier_qty'] - line['out_inventory_qty'] - line['out_production_qty'] - line['out_transit_qty']
                                if is_swap:
                                    other_expense = other_expense - line['out_swap_qty']
                                if is_product_expense:
                                    other_expense = other_expense - line['out_prod_ex_qty']
                            other_expense_cost = 0
                            if line['ex_cost'] != 0:
                                other_expense_cost = line['ex_cost'] - line['out_customer_cost'] - line['out_pos_customer_cost'] - line['out_supplier_cost'] - line['out_inventory_cost'] - line['out_production_cost'] - line['out_transit_cost']
                                if is_swap:
                                    other_expense_cost = other_expense_cost - line['out_swap_cost']
                                if is_product_expense:
                                    other_expense_cost = other_expense_cost - line['out_prod_ex_cost']

                            subtotal_other_income += other_income
                            subtotal_other_expense += other_expense

                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_qty += line['qty']
                            total_ex_qty += line['ex_qty']
                            total_end_qty += line['end_qty']
                            total_in_customer_qty += line['in_customer_qty']
                            total_out_customer_qty += line['out_customer_qty']
                            total_out_pos_customer_qty += line['out_pos_customer_qty']
                            total_in_supplier_qty += line['in_supplier_qty']
                            total_out_supplier_qty += line['out_supplier_qty']
                            total_in_inventory_qty += line['in_inventory_qty']
                            total_out_inventory_qty += line['out_inventory_qty']
                            total_in_production_qty += line['in_production_qty']
                            total_out_production_qty += line['out_production_qty']
                            total_in_transit_qty += line['in_transit_qty']
                            total_out_transit_qty += line['out_transit_qty']
                            if is_swap:
                                total_in_swap_qty += line['in_swap_qty']
                                total_out_swap_qty += line['out_swap_qty']
                            if is_product_expense:
                                total_out_prod_ex_qty += line.get('out_prod_ex_qty', 0)
                            # Барааны мөрийг зурна
                            col = 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1

                            sheet.write(rowx, col, line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1

                            #                         Орлогын хэсэг
                            sheet, col = self.build_credit(sheet, col, rowx, format_content_float, line, \
                                                           is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense)
                            sheet.write(rowx, col, other_income, format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_group_float)
                            col += 1

                            #                         Зарлагын хэсэг is_sale, is_swap, is_pos, is_stock, is_product_expense, is_purchase, is_stock, is_mrp
                            sheet, col = self.build_debit(sheet, col, rowx, format_content_float, line, \
                                                          is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense)
                            sheet.write(rowx, col, other_expense, format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_group_float)
                            col += 1
                            sheet.write(rowx, col, line['end_qty'], format_content_float)
                            col += 1

                            rowx += 1
                            seq += 1
                            supplier_id = line['supp_id']
                            type_id = line['wcode']

                        col = 4 if self.show_barcode else 3
                        #                     Дэд дүнгийн мөр
                        sheet, col = self.build_subtotal_row(sheet, rowx, col, format_group_right, format_group_float, \
                                                                               is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                           subtotal_start_qty, subtotal_in_supplier_qty, subtotal_in_swap_qty, subtotal_in_production_qty, \
                           subtotal_in_transit_qty, subtotal_in_customer_qty, subtotal_in_inventory_qty, subtotal_other_income, \
                           subtotal_qty, subtotal_out_customer_qty, subtotal_out_swap_qty, subtotal_out_pos_customer_qty, \
                           subtotal_out_transit_qty, subtotal_out_prod_ex_qty, subtotal_out_supplier_qty, subtotal_out_inventory_qty, \
                           subtotal_out_production_qty, subtotal_other_expense, subtotal_ex_qty, subtotal_end_qty)

                        #                     Нийт дүнгийн мөр
                        rowx += 1
                        col = 4 if self.show_barcode else 3
                        sheet, col = self.build_total_row(sheet, rowx, col, format_group, format_title_float, \
                                                          is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                        total_start_qty, total_in_supplier_qty, total_in_swap_qty, total_in_production_qty, \
                        total_in_transit_qty, total_in_customer_qty, total_in_inventory_qty, \
                        total_other_income, total_qty, total_out_pos_customer_qty, \
                        total_out_customer_qty, total_out_swap_qty, total_out_transit_qty, \
                        total_out_prod_ex_qty, total_out_supplier_qty, total_out_inventory_qty, total_out_production_qty, \
                        total_other_expense, total_ex_qty, total_end_qty)
                        rowx += 1

                elif self.type == 'supplier':
                    for line in lines:
                        # Эхний агуулах/данс болон ангиллыг зурна
                        if not supplier_id and line['supp_id']:
                            col = total_col_range
                            sheet.write(rowx, 0, sub_seq, format_group_left)
                            sheet.merge_range(rowx, 1, rowx, col, line['supp_name'] or _('Undefined Supplier'), format_group_left)
                            rowx += 1
                            rowd = rowx
                            subtotal_start_qty = 0
                            subtotal_qty = 0
                            subtotal_ex_qty = 0
                            subtotal_end_qty = 0
                            subtotal_in_customer_qty = subtotal_out_customer_qty = subtotal_out_pos_customer_qty = 0
                            subtotal_in_supplier_qty = subtotal_out_supplier_qty = 0
                            subtotal_in_inventory_qty = subtotal_out_inventory_qty = 0
                            subtotal_in_production_qty = subtotal_out_production_qty = 0
                            subtotal_in_transit_qty = subtotal_out_transit_qty = 0
                            subtotal_in_swap_qty = subtotal_out_swap_qty = 0
                            subtotal_out_prod_ex_qty = 0
                        # Дараагийн ангиллыг зурна
                        elif supplier_id and supplier_id != line['supp_id']:
                            col = 4 if self.show_barcode else 3 if self.show_barcode else 3
                            #                     Дэд дүнгийн мөр
                            sheet, col = self.build_subtotal_row(sheet, rowx, col, format_group_right, format_group_float, \
                                                                                   is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                           subtotal_start_qty, subtotal_in_supplier_qty, subtotal_in_swap_qty, subtotal_in_production_qty, \
                           subtotal_in_transit_qty, subtotal_in_customer_qty, subtotal_in_inventory_qty, subtotal_other_income, \
                           subtotal_qty, subtotal_out_customer_qty, subtotal_out_swap_qty, subtotal_out_pos_customer_qty, \
                           subtotal_out_transit_qty, subtotal_out_prod_ex_qty, subtotal_out_supplier_qty, subtotal_out_inventory_qty, \
                           subtotal_out_production_qty, subtotal_other_expense, subtotal_ex_qty, subtotal_end_qty)

                            rowx += 1
                            sub_seq += 1
                            sheet.write(rowx, 0, sub_seq, format_group_left)
                            col = total_col_range
                            sheet.merge_range(rowx, 1, rowx, col, line['supp_name'] or _('Undefined Supplier'), format_group_left)
                            rowx += 1
                            seq = 1

                            subtotal_start_qty = 0
                            subtotal_qty = 0
                            subtotal_ex_qty = 0
                            subtotal_end_qty = 0
                            subtotal_in_customer_qty = subtotal_out_customer_qty = subtotal_out_pos_customer_qty = 0
                            subtotal_in_supplier_qty = subtotal_out_supplier_qty = 0
                            subtotal_in_inventory_qty = subtotal_out_inventory_qty = 0
                            subtotal_in_production_qty = subtotal_out_production_qty = 0
                            subtotal_in_transit_qty = subtotal_out_transit_qty = 0
                            subtotal_in_swap_qty = subtotal_out_swap_qty = 0
                            subtotal_out_prod_ex_qty = 0

                        subtotal_start_qty += line['start_qty']
                        subtotal_qty += line['qty']
                        subtotal_ex_qty += line['ex_qty']
                        subtotal_end_qty += line['end_qty']
                        subtotal_in_customer_qty += line['in_customer_qty']
                        subtotal_out_customer_qty += line['out_customer_qty']
                        subtotal_out_pos_customer_qty += line['out_pos_customer_qty']
                        subtotal_in_supplier_qty += line['in_supplier_qty']
                        subtotal_out_supplier_qty += line['out_supplier_qty']
                        subtotal_in_inventory_qty += line['in_inventory_qty']
                        subtotal_out_inventory_qty += line['out_inventory_qty']
                        subtotal_in_production_qty += line['in_production_qty']
                        subtotal_out_production_qty += line['out_production_qty']
                        subtotal_in_transit_qty += line['in_transit_qty']
                        subtotal_out_transit_qty += line['out_transit_qty']
                        if is_swap:
                            subtotal_in_swap_qty += line['in_swap_qty']
                            subtotal_out_swap_qty += line['out_swap_qty']

                        if is_product_expense:
                            subtotal_out_prod_ex_qty += line.get('out_prod_ex_qty', 0)

                        if is_swap:
                            subtotal_in_swap_qty += line['in_swap_qty']
                            subtotal_out_swap_qty += line['out_swap_qty']

                        #                                   Бусад орлогыг тооцох
                        other_income = 0
                        if line['qty'] > 0:
                            other_income = line['qty'] - line['in_customer_qty'] - line['in_supplier_qty'] - line['in_inventory_qty'] - line['in_production_qty'] - line['in_transit_qty']
                            if is_swap:
                                other_income = other_income - line['in_swap_qty']
                        other_income_cost = 0
                        if line['cost'] != 0:
                            other_income_cost = line['cost'] - line['in_customer_cost'] - line['in_supplier_cost'] - line['in_inventory_cost'] - line['in_production_cost'] - line['in_transit_cost']
                            if is_swap:
                                other_income_cost = other_income_cost - line['in_swap_cost']

#                                 Бусад зарлагыг тооцох
                        other_expense = 0
                        if line['ex_qty'] > 0:
                            other_expense = line['ex_qty'] - line['out_customer_qty'] - line['out_pos_customer_qty'] - line['out_supplier_qty'] - line['out_inventory_qty'] - line['out_production_qty'] - line['out_transit_qty']
                            if is_swap:
                                other_expense = other_expense - line['out_swap_qty']
                            if is_product_expense:
                                other_expense = other_expense - line['out_prod_ex_qty']
                        other_expense_cost = 0
                        if line['ex_cost'] != 0:
                            other_expense_cost = line['ex_cost'] - line['out_customer_cost'] - line['out_pos_customer_cost'] - line['out_supplier_cost'] - line['out_inventory_cost'] - line['out_production_cost'] - line['out_transit_cost']
                            if is_swap:
                                other_expense_cost = other_expense_cost - line['out_swap_cost']
                            if is_product_expense:
                                other_expense_cost = other_expense_cost - line['out_prod_ex_cost']

                        subtotal_other_income += other_income
                        subtotal_other_expense += other_expense

                        # Нийт дүнг олно
                        total_start_qty += line['start_qty']
                        total_qty += line['qty']
                        total_ex_qty += line['ex_qty']
                        total_end_qty += line['end_qty']
                        total_in_customer_qty += line['in_customer_qty']
                        total_out_customer_qty += line['out_customer_qty']
                        total_out_pos_customer_qty += line['out_pos_customer_qty']
                        total_in_supplier_qty += line['in_supplier_qty']
                        total_out_supplier_qty += line['out_supplier_qty']
                        total_in_inventory_qty += line['in_inventory_qty']
                        total_out_inventory_qty += line['out_inventory_qty']
                        total_in_production_qty += line['in_production_qty']
                        total_out_production_qty += line['out_production_qty']
                        total_in_transit_qty += line['in_transit_qty']
                        total_out_transit_qty += line['out_transit_qty']
                        if is_swap:
                            total_in_swap_qty += line['in_swap_qty']
                            total_out_swap_qty += line['out_swap_qty']
                        if is_product_expense:
                            total_out_prod_ex_qty += line.get('out_prod_ex_qty', 0)
                        # Барааны мөрийг зурна
                        col = 0
                        sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                        col += 1
                        sheet.write(rowx, col, line['code'], format_content_center)
                        col += 1
                        if self.show_barcode:
                            sheet.write(rowx, col, line['barcode'], format_content_center)
                            col += 1
                        sheet.write(rowx, col, line['name'], format_content_text)
                        col += 1
                        sheet.write(rowx, col, line['uom_name'], format_content_center)
                        col += 1
                        sheet.write(rowx, col, line['start_qty'], format_content_float)
                        col += 1

                        #                         Орлогын хэсэг
                        sheet, col = self.build_credit(sheet, col, rowx, format_content_float, line, \
                                                       is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense)
                        sheet.write(rowx, col, other_income, format_content_float)
                        col += 1
                        sheet.write(rowx, col, line['qty'], format_group_float)
                        col += 1

                        #                         Зарлагын хэсэг is_sale, is_swap, is_pos, is_stock, is_product_expense, is_purchase, is_stock, is_mrp
                        sheet, col = self.build_debit(sheet, col, rowx, format_content_float, line, \
                                                      is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense)
                        sheet.write(rowx, col, other_expense, format_content_float)
                        col += 1
                        sheet.write(rowx, col, line['ex_qty'], format_group_float)
                        col += 1
                        sheet.write(rowx, col, line['end_qty'], format_content_float)
                        col += 1

                        rowx += 1
                        seq += 1
                        supplier_id = line['supp_id']

                    col = 4 if self.show_barcode else 3 if self.show_barcode else 3
                    #                     Дэд дүнгийн мөр
                    sheet, col = self.build_subtotal_row(sheet, rowx, col, format_group_right, format_group_float, \
                                                                           is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                           subtotal_start_qty, subtotal_in_supplier_qty, subtotal_in_swap_qty, subtotal_in_production_qty, \
                           subtotal_in_transit_qty, subtotal_in_customer_qty, subtotal_in_inventory_qty, subtotal_other_income, \
                           subtotal_qty, subtotal_out_customer_qty, subtotal_out_swap_qty, subtotal_out_pos_customer_qty, \
                           subtotal_out_transit_qty, subtotal_out_prod_ex_qty, subtotal_out_supplier_qty, subtotal_out_inventory_qty, \
                           subtotal_out_production_qty, subtotal_other_expense, subtotal_ex_qty, subtotal_end_qty)

                    #                     Нийт дүнгийн мөр
                    rowx += 1
                    col = 4 if self.show_barcode else 3
                    sheet, col = self.build_total_row(sheet, rowx, col, format_group, format_title_float, \
                                                      is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                        total_start_qty, total_in_supplier_qty, total_in_swap_qty, total_in_production_qty, \
                        total_in_transit_qty, total_in_customer_qty, total_in_inventory_qty, \
                        total_other_income, total_qty, total_out_pos_customer_qty, \
                        total_out_customer_qty, total_out_swap_qty, total_out_transit_qty, \
                        total_out_prod_ex_qty, total_out_supplier_qty, total_out_inventory_qty, total_out_production_qty, \
                        total_other_expense, total_ex_qty, total_end_qty)
                    rowx += 1

                elif self.type == 'category':
                    for line in lines:
                        # Эхний агуулах/данс болон ангиллыг зурна
                        if not category_id:
                            col = total_col_range
                            sheet.write(rowx, 0, sub_seq, format_group_float)
                            sheet.merge_range(rowx, 1, rowx, col, line['cname'], format_group_left)
                            rowx += 1
                            rowd = rowx
                            subtotal_start_qty = 0
                            subtotal_qty = 0
                            subtotal_ex_qty = 0
                            subtotal_end_qty = 0
                            subtotal_in_customer_qty = subtotal_out_customer_qty = subtotal_out_pos_customer_qty = 0
                            subtotal_in_supplier_qty = subtotal_out_supplier_qty = 0
                            subtotal_in_inventory_qty = subtotal_out_inventory_qty = 0
                            subtotal_in_production_qty = subtotal_out_production_qty = 0
                            subtotal_in_transit_qty = subtotal_out_transit_qty = 0
                            subtotal_in_swap_qty = subtotal_out_swap_qty = 0
                            subtotal_out_prod_ex_qty = 0
                        # Дараагийн ангиллыг зурна
                        elif category_id and category_id != line['cid']:
                            col = 4 if self.show_barcode else 3
                            #                     Дэд дүнгийн мөр
                            sheet, col = self.build_subtotal_row(sheet, rowx, col, format_group_right, format_group_float, \
                                                                                   is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                           subtotal_start_qty, subtotal_in_supplier_qty, subtotal_in_swap_qty, subtotal_in_production_qty, \
                           subtotal_in_transit_qty, subtotal_in_customer_qty, subtotal_in_inventory_qty, subtotal_other_income, \
                           subtotal_qty, subtotal_out_customer_qty, subtotal_out_swap_qty, subtotal_out_pos_customer_qty, \
                           subtotal_out_transit_qty, subtotal_out_prod_ex_qty, subtotal_out_supplier_qty, subtotal_out_inventory_qty, \
                           subtotal_out_production_qty, subtotal_other_expense, subtotal_ex_qty, subtotal_end_qty)

                            rowx += 1
                            sub_seq += 1
                            sheet.write(rowx, 0, sub_seq, format_group_left)
                            col = total_col_range
                            sheet.merge_range(rowx, 1, rowx, col, line['cname'], format_group_left)
                            rowx += 1
                            seq = 1

                            subtotal_start_qty = 0
                            subtotal_qty = 0
                            subtotal_ex_qty = 0
                            subtotal_end_qty = 0
                            subtotal_in_customer_qty = subtotal_out_customer_qty = subtotal_out_pos_customer_qty = 0
                            subtotal_in_supplier_qty = subtotal_out_supplier_qty = 0
                            subtotal_in_inventory_qty = subtotal_out_inventory_qty = 0
                            subtotal_in_production_qty = subtotal_out_production_qty = 0
                            subtotal_in_transit_qty = subtotal_out_transit_qty = 0
                            subtotal_in_swap_qty = subtotal_out_swap_qty = 0
                            subtotal_out_prod_ex_qty = 0

                        subtotal_start_qty += line['start_qty']
                        subtotal_qty += line['qty']
                        subtotal_ex_qty += line['ex_qty']
                        subtotal_end_qty += line['end_qty']
                        subtotal_in_customer_qty += line['in_customer_qty']
                        subtotal_out_customer_qty += line['out_customer_qty']
                        subtotal_out_pos_customer_qty += line['out_pos_customer_qty']
                        subtotal_in_supplier_qty += line['in_supplier_qty']
                        subtotal_out_supplier_qty += line['out_supplier_qty']
                        subtotal_in_inventory_qty += line['in_inventory_qty']
                        subtotal_out_inventory_qty += line['out_inventory_qty']
                        subtotal_in_production_qty += line['in_production_qty']
                        subtotal_out_production_qty += line['out_production_qty']
                        subtotal_in_transit_qty += line['in_transit_qty']
                        subtotal_out_transit_qty += line['out_transit_qty']
                        if is_swap:
                            subtotal_in_swap_qty += line['in_swap_qty']
                            subtotal_out_swap_qty += line['out_swap_qty']

                        if is_product_expense:
                            subtotal_out_prod_ex_qty += line.get('out_prod_ex_qty', 0)

                        if is_swap:
                            subtotal_in_swap_qty += line['in_swap_qty']
                            subtotal_out_swap_qty += line['out_swap_qty']

                        #                                   Бусад орлогыг тооцох
                        other_income = 0
                        if line['qty'] > 0:
                            other_income = line['qty'] - line['in_customer_qty'] - line['in_supplier_qty'] - line['in_inventory_qty'] - line['in_production_qty'] - line['in_transit_qty']
                            if is_swap:
                                other_income = other_income - line['in_swap_qty']
                        other_income_cost = 0
                        if line['cost'] != 0:
                            other_income_cost = line['cost'] - line['in_customer_cost'] - line['in_supplier_cost'] - line['in_inventory_cost'] - line['in_production_cost'] - line['in_transit_cost']
                            if is_swap:
                                other_income_cost = other_income_cost - line['in_swap_cost']
#                                 Бусад зарлагыг тооцох
                        other_expense = 0
                        if line['ex_qty'] > 0:
                            other_expense = line['ex_qty'] - line['out_customer_qty'] - line['out_pos_customer_qty'] - line['out_supplier_qty'] - line['out_inventory_qty'] - line['out_production_qty'] - line['out_transit_qty']
                            if is_swap:
                                other_expense = other_expense - line['out_swap_qty']
                            if is_product_expense:
                                other_expense = other_expense - line['out_prod_ex_qty']
                        other_expense_cost = 0
                        if line['ex_cost'] != 0:
                            other_expense_cost = line['ex_cost'] - line['out_customer_cost'] - line['out_pos_customer_cost'] - line['out_supplier_cost'] - line['out_inventory_cost'] - line['out_production_cost'] - line['out_transit_cost']
                            if is_swap:
                                other_expense_cost = other_expense_cost - line['out_swap_cost']
                            if is_product_expense:
                                other_expense_cost = other_expense_cost - line['out_prod_ex_cost']

                        subtotal_other_income += other_income
                        subtotal_other_expense += other_expense

                        # Нийт дүнг олно
                        total_start_qty += line['start_qty']
                        total_qty += line['qty']
                        total_ex_qty += line['ex_qty']
                        total_end_qty += line['end_qty']
                        total_in_customer_qty += line['in_customer_qty']
                        total_out_customer_qty += line['out_customer_qty']
                        total_out_pos_customer_qty += line['out_pos_customer_qty']
                        total_in_supplier_qty += line['in_supplier_qty']
                        total_out_supplier_qty += line['out_supplier_qty']
                        total_in_inventory_qty += line['in_inventory_qty']
                        total_out_inventory_qty += line['out_inventory_qty']
                        total_in_production_qty += line['in_production_qty']
                        total_out_production_qty += line['out_production_qty']
                        total_in_transit_qty += line['in_transit_qty']
                        total_out_transit_qty += line['out_transit_qty']
                        if is_swap:
                            total_in_swap_qty += line['in_swap_qty']
                            total_out_swap_qty += line['out_swap_qty']
                        if is_product_expense:
                            total_out_prod_ex_qty += line.get('out_prod_ex_qty', 0)
                        # Барааны мөрийг зурна
                        col = 0
                        sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                        col += 1
                        sheet.write(rowx, col, line['code'], format_content_center)
                        col += 1
                        if self.show_barcode:
                            sheet.write(rowx, col, line['barcode'], format_content_center)
                            col += 1
                        sheet.write(rowx, col, line['name'], format_content_text)
                        col += 1
                        sheet.write(rowx, col, line['uom_name'], format_content_center)
                        col += 1
                        sheet.write(rowx, col, line['start_qty'], format_content_float)
                        col += 1

                        #                         Орлогын хэсэг
                        sheet, col = self.build_credit(sheet, col, rowx, format_content_float, line, \
                                                       is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense)
                        sheet.write(rowx, col, other_income, format_content_float)
                        col += 1
                        sheet.write(rowx, col, line['qty'], format_group_float)
                        col += 1

                        #                         Зарлагын хэсэг is_sale, is_swap, is_pos, is_stock, is_product_expense, is_purchase, is_stock, is_mrp
                        sheet, col = self.build_debit(sheet, col, rowx, format_content_float, line, \
                                                      is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense)
                        sheet.write(rowx, col, other_expense, format_content_float)
                        col += 1
                        sheet.write(rowx, col, line['ex_qty'], format_group_float)
                        col += 1
                        sheet.write(rowx, col, line['end_qty'], format_content_float)
                        col += 1

                        rowx += 1
                        seq += 1
                        category_id = line['cid']

                    col = 4 if self.show_barcode else 3
                        #                     Дэд дүнгийн мөр
                    sheet, col = self.build_subtotal_row(sheet, rowx, col, format_group_right, format_group_float, \
                           is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                           subtotal_start_qty, subtotal_in_supplier_qty, subtotal_in_swap_qty, subtotal_in_production_qty, \
                           subtotal_in_transit_qty, subtotal_in_customer_qty, subtotal_in_inventory_qty, subtotal_other_income, \
                           subtotal_qty, subtotal_out_customer_qty, subtotal_out_swap_qty, subtotal_out_pos_customer_qty, \
                           subtotal_out_transit_qty, subtotal_out_prod_ex_qty, subtotal_out_supplier_qty, subtotal_out_inventory_qty, \
                           subtotal_out_production_qty, subtotal_other_expense, subtotal_ex_qty, subtotal_end_qty)

                        #                     Нийт дүнгийн мөр
                    rowx += 1
                    col = 4 if self.show_barcode else 3
                    sheet, col = self.build_total_row(sheet, rowx, col, format_group, format_title_float, \
                        is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                        total_start_qty, total_in_supplier_qty, total_in_swap_qty, total_in_production_qty, \
                        total_in_transit_qty, total_in_customer_qty, total_in_inventory_qty, \
                        total_other_income, total_qty, total_out_pos_customer_qty, \
                        total_out_customer_qty, total_out_swap_qty, total_out_transit_qty, \
                        total_out_prod_ex_qty, total_out_supplier_qty, total_out_inventory_qty, total_out_production_qty, \
                        total_other_expense, total_ex_qty, total_end_qty)
                    rowx += 1
                        #                 Данс төрлөөр өртөггүй
                elif self.type == 'account':
                    for line in lines:
                        # Эхний агуулах/данс болон ангиллыг зурна
                        if not account_id:
                            col = total_col_range
                            sheet.write(rowx, 0, sub_seq, format_group_left)
                            sheet.merge_range(rowx, 1, rowx, col, line['account_name'], format_group_left)
                            rowx += 1
                            rowd = rowx
                            subtotal_start_qty = 0
                            subtotal_qty = 0
                            subtotal_ex_qty = 0
                            subtotal_end_qty = 0
                            subtotal_in_customer_qty = subtotal_out_customer_qty = subtotal_out_pos_customer_qty = 0
                            subtotal_in_supplier_qty = subtotal_out_supplier_qty = 0
                            subtotal_in_inventory_qty = subtotal_out_inventory_qty = 0
                            subtotal_in_production_qty = subtotal_out_production_qty = 0
                            subtotal_in_transit_qty = subtotal_out_transit_qty = 0
                            subtotal_in_swap_qty = subtotal_out_swap_qty = 0
                            subtotal_out_prod_ex_qty = 0
                        # Дараагийн ангиллыг зурна
                        elif account_id and account_id != line['account_id']:
                            col = 4 if self.show_barcode else 3
                            #                     Дэд дүнгийн мөр
                            sheet, col = self.build_subtotal_row(sheet, rowx, col, format_group_right, format_group_float, \
                               is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                               subtotal_start_qty, subtotal_in_supplier_qty, subtotal_in_swap_qty, subtotal_in_production_qty, \
                               subtotal_in_transit_qty, subtotal_in_customer_qty, subtotal_in_inventory_qty, subtotal_other_income, \
                               subtotal_qty, subtotal_out_customer_qty, subtotal_out_swap_qty, subtotal_out_pos_customer_qty, \
                               subtotal_out_transit_qty, subtotal_out_prod_ex_qty, subtotal_out_supplier_qty, subtotal_out_inventory_qty, \
                               subtotal_out_production_qty, subtotal_other_expense, subtotal_ex_qty, subtotal_end_qty)

                            rowx += 1
                            sub_seq += 1
                            sheet.write(rowx, 0, sub_seq, format_group_left)
                            col = total_col_range
                            sheet.merge_range(rowx, 1, rowx, col, line['account_name'], format_group_left)
                            rowx += 1
                            seq = 1

                            subtotal_start_qty = 0
                            subtotal_qty = 0
                            subtotal_ex_qty = 0
                            subtotal_end_qty = 0
                            subtotal_in_customer_qty = subtotal_out_customer_qty = subtotal_out_pos_customer_qty = 0
                            subtotal_in_supplier_qty = subtotal_out_supplier_qty = 0
                            subtotal_in_inventory_qty = subtotal_out_inventory_qty = 0
                            subtotal_in_production_qty = subtotal_out_production_qty = 0
                            subtotal_in_transit_qty = subtotal_out_transit_qty = 0
                            subtotal_in_swap_qty = subtotal_out_swap_qty = 0
                            subtotal_out_prod_ex_qty = 0

                        subtotal_start_qty += line['start_qty']
                        subtotal_qty += line['qty']
                        subtotal_ex_qty += line['ex_qty']
                        subtotal_end_qty += line['end_qty']
                        subtotal_in_customer_qty += line['in_customer_qty']
                        subtotal_out_customer_qty += line['out_customer_qty']
                        subtotal_out_pos_customer_qty += line['out_pos_customer_qty']
                        subtotal_in_supplier_qty += line['in_supplier_qty']
                        subtotal_out_supplier_qty += line['out_supplier_qty']
                        subtotal_in_inventory_qty += line['in_inventory_qty']
                        subtotal_out_inventory_qty += line['out_inventory_qty']
                        subtotal_in_production_qty += line['in_production_qty']
                        subtotal_out_production_qty += line['out_production_qty']
                        subtotal_in_transit_qty += line['in_transit_qty']
                        subtotal_out_transit_qty += line['out_transit_qty']
                        if is_swap:
                            subtotal_in_swap_qty += line['in_swap_qty']
                            subtotal_out_swap_qty += line['out_swap_qty']
                        if is_product_expense:
                            subtotal_out_prod_ex_qty += line.get('out_prod_ex_qty', 0)

                        #                                   Бусад орлогыг тооцох
                        other_income = 0
                        if line['qty'] > 0:
                            other_income = line['qty'] - line['in_customer_qty'] - line['in_supplier_qty'] - line['in_inventory_qty'] - line['in_production_qty'] - line['in_transit_qty']
                            if is_swap:
                                other_income = other_income - line['in_swap_qty']
                        other_income_cost = 0
                        if line['cost'] != 0:
                            other_income_cost = line['cost'] - line['in_customer_cost'] - line['in_supplier_cost'] - line['in_inventory_cost'] - line['in_production_cost'] - line['in_transit_cost']
                            if is_swap:
                                other_income_cost = other_income_cost - line['in_swap_cost']
#                                 Бусад зарлагыг тооцох
                        other_expense = 0
                        if line['ex_qty'] > 0:
                            other_expense = line['ex_qty'] - line['out_customer_qty'] - line['out_pos_customer_qty'] - line['out_supplier_qty'] - line['out_inventory_qty'] - line['out_production_qty'] - line['out_transit_qty']
                            if is_swap:
                                other_expense = other_expense - line['out_swap_qty']
                            if is_product_expense:
                                other_expense = other_expense - line['out_prod_ex_qty']
                        other_expense_cost = 0
                        if line['ex_cost'] != 0:
                            other_expense_cost = line['ex_cost'] - line['out_customer_cost'] - line['out_pos_customer_cost'] - line['out_supplier_cost'] - line['out_inventory_cost'] - line['out_production_cost'] - line['out_transit_cost']
                            if is_swap:
                                other_expense_cost = other_expense_cost - line['out_swap_cost']
                            if is_product_expense:
                                other_expense_cost = other_expense_cost - line['out_prod_ex_cost']

                        subtotal_other_income += other_income
                        subtotal_other_expense += other_expense

                        # Нийт дүнг олно
                        total_start_qty += line['start_qty']
                        total_qty += line['qty']
                        total_ex_qty += line['ex_qty']
                        total_end_qty += line['end_qty']
                        total_in_customer_qty += line['in_customer_qty']
                        total_out_customer_qty += line['out_customer_qty']
                        total_out_pos_customer_qty += line['out_pos_customer_qty']
                        total_in_supplier_qty += line['in_supplier_qty']
                        total_out_supplier_qty += line['out_supplier_qty']
                        total_in_inventory_qty += line['in_inventory_qty']
                        total_out_inventory_qty += line['out_inventory_qty']
                        total_in_production_qty += line['in_production_qty']
                        total_out_production_qty += line['out_production_qty']
                        total_in_transit_qty += line['in_transit_qty']
                        total_out_transit_qty += line['out_transit_qty']
                        if is_swap:
                            total_in_swap_qty += line['in_swap_qty']
                            total_out_swap_qty += line['out_swap_qty']
                        if is_product_expense:
                            total_out_prod_ex_qty += line.get('out_prod_ex_qty', 0)
                        # Барааны мөрийг зурна
                        col = 0
                        sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                        col += 1
                        sheet.write(rowx, col, line['code'], format_content_center)
                        col += 1
                        if self.show_barcode:
                            sheet.write(rowx, col, line['barcode'], format_content_center)
                            col += 1
                        sheet.write(rowx, col, line['name'], format_content_text)
                        col += 1
                        sheet.write(rowx, col, line['uom_name'], format_content_center)
                        col += 1
                        sheet.write(rowx, col, line['start_qty'], format_content_float)
                        col += 1

                        #                         Орлогын хэсэг
                        sheet, col = self.build_credit(sheet, col, rowx, format_content_float, line, \
                                                       is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense)
                        sheet.write(rowx, col, other_income, format_content_float)
                        col += 1
                        sheet.write(rowx, col, line['qty'], format_group_float)
                        col += 1

                        #                         Зарлагын хэсэг is_sale, is_swap, is_pos, is_stock, is_product_expense, is_purchase, is_stock, is_mrp
                        sheet, col = self.build_debit(sheet, col, rowx, format_content_float, line, \
                                                      is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense)
                        sheet.write(rowx, col, other_expense, format_content_float)
                        col += 1
                        sheet.write(rowx, col, line['ex_qty'], format_group_float)
                        col += 1
                        sheet.write(rowx, col, line['end_qty'], format_content_float)
                        col += 1

                        rowx += 1
                        seq += 1
                        account_id = line['account_id']

                    col = 4 if self.show_barcode else 3
                        #                     Дэд дүнгийн мөр
                    sheet, col = self.build_subtotal_row(sheet, rowx, col, format_group_right, format_group_float, \
                           is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                           subtotal_start_qty, subtotal_in_supplier_qty, subtotal_in_swap_qty, subtotal_in_production_qty, \
                           subtotal_in_transit_qty, subtotal_in_customer_qty, subtotal_in_inventory_qty, subtotal_other_income, \
                           subtotal_qty, subtotal_out_customer_qty, subtotal_out_swap_qty, subtotal_out_pos_customer_qty, \
                           subtotal_out_transit_qty, subtotal_out_prod_ex_qty, subtotal_out_supplier_qty, subtotal_out_inventory_qty, \
                           subtotal_out_production_qty, subtotal_other_expense, subtotal_ex_qty, subtotal_end_qty)

                        #                     Нийт дүнгийн мөр
                    rowx += 1
                    col = 4 if self.show_barcode else 3
                    sheet, col = self.build_total_row(sheet, rowx, col, format_group, format_title_float, \
                        is_purchase, is_swap, is_mrp, is_stock, is_sale, is_pos, is_product_expense, \
                        total_start_qty, total_in_supplier_qty, total_in_swap_qty, total_in_production_qty, \
                        total_in_transit_qty, total_in_customer_qty, total_in_inventory_qty, \
                        total_other_income, total_qty, total_out_pos_customer_qty, \
                        total_out_customer_qty, total_out_swap_qty, total_out_transit_qty, \
                        total_out_prod_ex_qty, total_out_supplier_qty, total_out_inventory_qty, total_out_production_qty, \
                        total_other_expense, total_ex_qty, total_end_qty)
                    rowx += 1
        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()
