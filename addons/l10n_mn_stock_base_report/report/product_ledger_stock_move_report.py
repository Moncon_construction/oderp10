# -*- encoding: utf-8 -*-
import base64
import io
import time
from datetime import date, datetime
from io import BytesIO

import pytz
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell

from odoo import _, api, fields, models
from odoo.addons.l10n_mn_stock_base_report.tools.report_excel_cell_styles import ProductLedgerReportExcelCellStyles
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import UserError


class ProductLedgerStockMoveReport(models.TransientModel):
    """
       Бараа материалын товчоо тайлан (Хөдөлгөөнөөр)
    """

    _inherit = 'oderp.report.html.output'
    _name = 'product.legder.stock.move.report'
    _description = "Product Ledger Stock Move Report"

    def type_selection(self):
        type_selection = [('warehouse', _('Warehouse')), ('supplier', _('Supplier')), ('category', _('Category')), ('size', _('Size'))]
        is_cost_for_each_wh = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_stock_account_cost_for_each_wh')], limit=1)
        if is_cost_for_each_wh and is_cost_for_each_wh.state not in ('installed', 'to upgrade', 'to install'):
            type_selection.append(('account', _('Account')))
        return type_selection

    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env['res.company']._company_default_get('hr.salary.rule.category.report'))
    date_from = fields.Datetime("Start Date", required=True, default=lambda *a: time.strftime('%Y-%m-01 16:00:00'))
    date_to = fields.Datetime("End Date", required=True, default=lambda *a: time.strftime('%Y-%m-%d 15:59:59'))
    warehouse_ids = fields.Many2many('stock.warehouse', string="Warehouses", required=True)
    category_ids = fields.Many2many('product.category', string="Categories")
    product_ids = fields.Many2many('product.product', string="Products")
    product_size_ids = fields.Many2many('product.size', string="Product size")
    account_ids = fields.Many2many('account.account', string="Accounts")
    product_brands = fields.Many2many('product.brand', 'product_ledger_move_report_to_brand', 'report_id', 'brand_id', string="Product Brand")
    supplier_ids = fields.Many2many('res.partner', string="Supplier")
    location_ids = fields.Many2many('stock.location', 'product_ledger_move_report_location', 'report_id', 'location_id', string="Locations")
    type = fields.Selection(type_selection, string="Type", required=True, default='warehouse')
    group_by = fields.Selection([('by_category', 'By Category'), ('by_supplier', 'By Supplier'), ('by_size', 'By Size'), ('by_brand', 'By Brand'), ('by_account', 'By Account'), ('by_location', 'By Location')], string="Group By", required=True, default='by_category')
    end_balance_non_zero = fields.Boolean("End balance non zero", default=False)
    hide_type = fields.Boolean("Hide Type")
    qty_on_hand = fields.Boolean("Quantity on Hand")
    show_cost = fields.Boolean('Show Cost')
    show_barcode = fields.Boolean('Show Barcode', default=True)
    show_current_cost = fields.Boolean('Show Current Cost')
    show_price = fields.Boolean('Show Current Price', default=False)
    show_size = fields.Boolean('Show Size', default=False)
    show_only_minus = fields.Boolean('Show Only Minus Residuals', default=False)
    group_by_parent = fields.Boolean('Group by Parent', default=False)
    show_image = fields.Boolean('Show Image', default=False)
    show_reserved = fields.Boolean('Нөөцөлсөн тоо хэмжээ харах')
    show_residual = fields.Boolean('Ирээдүйн боломжит үлдэгдэл харах')
    show_order = fields.Boolean('Show order', default=False)
    show_product_age = fields.Boolean('Show product age', default=False)
    column_selection = fields.Selection([('initial_balance', 'Initial Balance'),
                                         ('income', 'Income'),
                                         ('expense', 'Expense'),
                                         ('end_balance', 'End Balance')], string='Column')
    amount_selection = fields.Selection([('quantity', 'Quantity'),
                                         ('cost', 'Cost'),
                                         ('price', 'Price')], string='Selection')
    order_by = fields.Selection([('asc', 'Ascending'),
                                 ('desc', 'Descending')], string='Order By', default='asc')
    price_view_form = fields.Selection([('with_cost_price', 'Look at the cost side by next'),
                                        ('only_sale_price', 'View only for sale price')], string='Price View Form')
    show_worthy_resources = fields.Boolean('Show Worthy Resources', default=False)  # Зохистой нөөц гаргах эсэх

    @api.onchange("company_id")
    def _get_warehouse_and_categ_domain(self):
        domain = {}
        domain['warehouse_ids'] = [('company_id', '=', self.company_id.id)]
        _warehouses = []
        for warehouse in self.env.user.allowed_warehouses:
            _warehouses.append(warehouse.id)
        if _warehouses:
            domain['warehouse_ids'] = [('id', 'in', _warehouses), ('company_id', '=', self.company_id.id)]
        domain['category_ids'] = [('company_id', '=', self.company_id.id)]
        return {'domain': domain}

    @api.onchange("supplier_ids")
    def _get_supplier_domain(self):
        domain = {}
        if self.supplier_ids:
            domain['product_ids'] = [('product_tmpl_id.supplier_id', 'in', self.supplier_ids.ids)]
        return {'domain': domain}

    @api.onchange("warehouse_ids")
    def _get_location_domain(self):
        domain = {}
        location_ids = []
        if self.warehouse_ids:
            for wh in self.warehouse_ids:
                loc_ids = self.env['stock.location'].search([('usage', '=', 'internal'), ('location_id', 'child_of', [wh.view_location_id.id])]).ids
                location_ids += loc_ids
            domain['location_ids'] = [('id', 'in', location_ids)]
        return {'domain': domain}

    @api.onchange("category_ids")
    def _get_product_domain(self):
        domain = {}
        if self.category_ids and len(self.category_ids) > 0:
            domain['product_ids'] = [('product_tmpl_id.categ_id', 'in', self.category_ids.ids)]
        return {'domain': domain}

    @api.onchange('show_cost')
    def onchange_show_cost(self):
        if self.show_cost:
            self.show_price = False
        else:
            self.show_current_cost = False

    @api.onchange('show_price')
    def onchange_show_price(self):
        if self.show_price:
            self.show_current_cost = False
            self.show_cost = False

    @api.onchange('type')
    def onchange_type(self):
        if self.type == 'account':
            self.group_by = 'by_category'
            self.show_only_minus = False
            self.end_balance_non_zero = False
            self.show_reserved = False
            self.show_residual = False
            self.qty_on_hand = False
            self.show_product_age = False
            self.show_worthy_resources = False

    def get_extra_col_count(self):
        count = 0
        count += 2 if self.qty_on_hand else 0
        count += 2 if self.show_current_cost else 0
        count += 1 if self.group_by_parent else 0
        count += 5 if self.show_cost or self.show_price else 0
        count += 1 if self.show_barcode else 0
        count += 1 if self.show_size else 0
        count += 1 if self.show_image else 0
        count += 2 if self.show_reserved else 0
        count += 4 if self.show_residual else 0
        count += 1 if self.show_product_age else 0
        count += 2 if self.show_worthy_resources else 0
        count += 5 if self.show_price and self.price_view_form == 'with_cost_price' else 0
        return count

    def get_all_col_count(self):
        return 7 + self.get_extra_col_count()

    def get_main_col_count(self):
        count = 3
        count += 1 if self.show_barcode else 0
        count += 1 if self.show_size else 0
        count += 1 if self.show_image else 0
        count += 1 if self.group_by_parent else 0
        return count

    def get_add_col_count(self, col):
        count = col
        if self.show_reserved:
            count += 2
        if self.qty_on_hand:
            count += 2
        if self.show_current_cost:
            count += 2
        if self.show_residual:
            count += 4
        if self.show_product_age:
            count += 1
        if self.show_worthy_resources:
            count += 2
        if self.price_view_form == 'with_cost_price':
            count += 5
        return count

    def get_remain_col_count(self):
        return self.get_all_col_count() - self.get_main_col_count() + 1

    def show_reserved_warehouse(self, product_ids, location_ids, type):
        if type == 'c1':
            self._cr.execute("""SELECT sm.product_id as product_id, SUM(sm.product_uom_qty) as reserved_move
                                FROM stock_move sm
                                WHERE sm.state = 'assigned'
                                and sm.location_id = %s
                                and  NOT sm.location_dest_id = %s
                                and sm.product_id = %s
                                GROUP BY sm.product_id""", (location_ids, location_ids, product_ids))
        elif type == 'c2':
            self._cr.execute("""SELECT sm.product_id as product_id, SUM(sm.product_uom_qty) as reserved_move
                                FROM stock_move sm
                                WHERE sm.state = 'assigned'
                                 and NOT sm.location_id = %s AND sm.location_dest_id = %s
                                 and sm.product_id = %s
                                 GROUP BY sm.product_id""", (location_ids, location_ids, product_ids))
        elif type == 'c3':
            self._cr.execute("""SELECT sm.product_id as product_id, SUM(sm.product_uom_qty) as reserved_move
                                FROM stock_move sm
                                WHERE sm.state = 'confirmed'
                                and sm.location_id = %s
                                and NOT sm.location_dest_id = %s
                                and sm.product_id = %s
                                GROUP BY sm.product_id""", (location_ids, location_ids, product_ids))
        records = self.env.cr.dictfetchall()
        results = {}

        for record in records:
            results[record['product_id']] = record['reserved_move']

        return results

    def get_column_name_for_calculate(self, index):
        column_name = self.get_xsl_column_name(index).split(':')[0]
        return column_name

    def get_arithmetic_formula(self, f_coly, f_rowx, s_coly, s_rowx, oprtr):
        f_cell_index = self.get_column_name_for_calculate(f_coly) + str(f_rowx)
        s_cell_index = self.get_column_name_for_calculate(s_coly) + str(s_rowx)
        if oprtr == "/":
            return "=IF(" + s_cell_index + "," + f_cell_index + oprtr + s_cell_index + ",0)"
        return "=" + f_cell_index + oprtr + s_cell_index

    @api.multi
    def export_report(self):

        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Product Ledger Stock Move Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_name = book.add_format(ProductLedgerReportExcelCellStyles.format_name)
        format_filter = book.add_format(ProductLedgerReportExcelCellStyles.format_filter)
        format_filter_right = book.add_format(ProductLedgerReportExcelCellStyles.format_filter_right)
        format_title = book.add_format(ProductLedgerReportExcelCellStyles.format_title)
        format_title_float = book.add_format(ProductLedgerReportExcelCellStyles.format_title_float)
        format_group_left = book.add_format(ProductLedgerReportExcelCellStyles.format_group_left)
        format_filter_bold = book.add_format(ProductLedgerReportExcelCellStyles.format_filter_bold)
        format_group_float = book.add_format(ProductLedgerReportExcelCellStyles.format_group_float)
        format_group_total = book.add_format(ProductLedgerReportExcelCellStyles.format_group_total)
        format_content_center = book.add_format(ProductLedgerReportExcelCellStyles.format_content_center)
        format_content_text = book.add_format(ProductLedgerReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ProductLedgerReportExcelCellStyles.format_content_number)
        format_content_float = book.add_format(ProductLedgerReportExcelCellStyles.format_content_float)
        format_content_bold_left = book.add_format(ProductLedgerReportExcelCellStyles.format_content_bold_left)
        format_content_bold_number = book.add_format(ProductLedgerReportExcelCellStyles.format_content_bold_number)

        location_obj = self.env['stock.location']
        is_cost_for_each_wh = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_stock_account_cost_for_each_wh')], limit=1)

        # parameters
        seq = 1
        rowx = rowd = 0
        locations = []
        warehouse_total_rows = []
        total_start_qty = total_start_cost = total_start_price = total_qty = total_cost = total_price = 0
        total_ex_qty = total_ex_cost = total_ex_price = total_end_qty = total_end_cost = total_end_price = 0
        parent_total_start_qty = parent_total_qty = parent_total_ex_qty = parent_total_end_qty = 0
        parent_total_start_cost = parent_total_cost = parent_total_ex_cost = parent_total_unit_cost = parent_total_end_cost = parent_total_start_price = parent_total_price = parent_total_ex_price = parent_total_end_price = parent_total_unit_price = 0
        total_start_qty_on_hand = total_start_qty_on_hand_diff = 0
        total_warehouse_reserved_qty = total_available_warehouse_qty = total_warehouse_road = total_warehouse_residual_future = total_reserved_qty = total_available_future = 0
        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('product_ledger_stock_move_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(time.strftime('%Y-%m-%d'))
        # windows -с нээх үед алдаа зааж байсан тул коммент болгов
        # sheet.set_portrait()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.39, 0.39, 0.39, 0.39)  # 1cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})

        # compute column
        sheet.set_column('A:A', 4)

        if self.group_by_parent:
            sheet.set_column('B:B', 4)
            if self.show_image:
                sheet.set_column('C:C', 20)
            else:
                sheet.set_column('D:D', 10)
            sheet.set_column('D:D', 10)
            if self.show_image:
                sheet.set_column('E:E', 11)
                sheet.set_column('F:F', 27)
            else:
                sheet.set_column('E:E', 27)
                sheet.set_column('F:F', 11)
        else:
            if self.show_image:
                sheet.set_column('B:B', 20)
            else:
                sheet.set_column('B:B', 10)
            sheet.set_column('C:C', 10)
            if self.show_image:
                sheet.set_column('D:D', 11)
                sheet.set_column('E:E', 27)
            else:
                sheet.set_column('D:D', 27)
                sheet.set_column('E:E', 11)
        sheet.set_column('G:G', 10)
        sheet.set_column('H:H', 15)
        sheet.set_column('I:I', 15)
        sheet.set_column('J:J', 15)
        sheet.set_column('K:K', 15)
        sheet.set_column('L:L', 12)
        sheet.set_column('M:M', 10)
        sheet.set_column('N:N', 15)
        sheet.set_column('O:O', 15)
        sheet.set_column('P:P', 15)
        sheet.set_column('Q:Q', 15)
        sheet.set_column('R:R', 15)
        sheet.set_column('S:S', 15)
        sheet.set_column('T:T', 15)
        sheet.set_column('U:U', 15)
        sheet.set_column('V:V', 15)
        sheet.set_column('W:W', 15)
        sheet.set_column('X:X', 15)
        sheet.set_column('Y:Y', 15)
        sheet.set_column('Z:Z', 15)
        sheet.set_column('AA:AA', 15)
        sheet.set_column('AB:AB', 15)
        sheet.set_column('AC:AC', 15)
        sheet.set_column('AD:AD', 15)
        sheet.set_column('AE:AE', 15)
        sheet.set_column('AF:AF', 15)
        sheet.set_column('AG:AG', 15)
        
        zero_qty_zero_cost = self.env.user.company_id.zero_qty_zero_cost

        local = pytz.UTC
        if self.env.user.tz:
            local = pytz.timezone(self.env.user.tz)

        # create name
        col_name = self.get_all_col_count()
        sheet.merge_range(rowx, 0, rowx, col_name, '%s' % self.company_id.name, format_filter)
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, col_name, report_name.upper(), format_name)
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, col_name, '%s' % ', '.join(map(lambda x: x.name, self.warehouse_ids)), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 0, rowx, col_name, _(u'Duration: %s - %s') % (pytz.utc.localize(datetime.strptime(self.date_from, '%Y-%m-%d %H:%M:%S')).astimezone(local).strftime('%Y-%m-%d %H:%M:%S'),
                                                                              pytz.utc.localize(datetime.strptime(self.date_to, '%Y-%m-%d %H:%M:%S')).astimezone(local).strftime('%Y-%m-%d %H:%M:%S')), format_filter_bold)
        rowx += 1

        # table header
        if self.show_cost:
            col = 1 if self.group_by_parent else 0
            sheet.merge_range(rowx, col, rowx + 1, col, _('Seq'), format_title)
            col += 1
            if self.show_image:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Image'), format_title)
                col += 1
            sheet.merge_range(rowx, col, rowx + 1, col, _('Product code'), format_title)
            col += 1
            if self.show_barcode:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Barcode'), format_title)
                col += 1
            sheet.merge_range(rowx, col, rowx + 1, col, _('Product name'), format_title)
            col += 1
            sheet.merge_range(rowx, col, rowx + 1, col, _('Unit of measure'), format_title)
            col += 1
            if self.show_size:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Size of Product'), format_title)
                col += 1
            sheet.merge_range(rowx, col, rowx, col + 1, _('Initial Balance'), format_title)
            col += 2
            sheet.merge_range(rowx, col, rowx, col + 1, _('Income'), format_title)
            col += 2
            sheet.merge_range(rowx, col, rowx, col + 1, _('Expense'), format_title)
            col += 2
            sheet.merge_range(rowx, col, rowx, col + 2, _('End Balance'), format_title)
            col += 3
            if self.show_worthy_resources:
                sheet.merge_range(rowx, col, rowx + 1, col, _(u'Worthly Resources'), format_title)
                col += 1
                sheet.merge_range(rowx, col, rowx + 1, col, _(u'Difference'), format_title)
                col += 1
            if self.show_product_age:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Aging of the goods'), format_title)
                col += 1
            if self.show_reserved:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Reserved'), format_title)
                col += 1
                sheet.merge_range(rowx, col, rowx + 1, col, _('Available balance'), format_title)
                col += 1
            if self.show_residual:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Commodities on the way'), format_title)
                col += 1
                sheet.merge_range(rowx, col, rowx + 1, col, _('Forecast'), format_title)
                col += 1
                sheet.merge_range(rowx, col, rowx + 1, col, _('Reserved from Transit'), format_title)
                col += 1
                sheet.merge_range(rowx, col, rowx + 1, col, _('Future Forecast'), format_title)
                col += 1

            if self.qty_on_hand:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Quantity on Hand'), format_title)
                col += 1
                sheet.merge_range(rowx, col, rowx + 1, col, _('Quantity on Hand Difference'), format_title)
                col += 1
            if self.show_current_cost:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Current Cost'), format_title)
                col += 1
                sheet.merge_range(rowx, col, rowx + 1, col, _('Cost difference'), format_title)
                col += 1

            col = (7 if self.show_size else 6) if self.group_by_parent else (6 if self.show_size else 5)

            if self.show_image:
                col += 1
            col -= 0 if self.show_barcode else 1
            sheet.write(rowx + 1, col, _('Quantity'), format_title)
            col += 1
            sheet.write(rowx + 1, col, _('Cost'), format_title)
            col += 1
            sheet.write(rowx + 1, col, _('Quantity'), format_title)
            col += 1
            sheet.write(rowx + 1, col, _('Cost'), format_title)
            col += 1
            sheet.write(rowx + 1, col, _('Quantity'), format_title)
            col += 1
            sheet.write(rowx + 1, col, _('Cost'), format_title)
            col += 1
            sheet.write(rowx + 1, col, _('Unit Cost'), format_title)
            col += 1
            sheet.write(rowx + 1, col, _('Quantity'), format_title)
            col += 1
            sheet.write(rowx + 1, col, _('Cost'), format_title)
            col += 1
        elif self.show_price:  # худалдах үнээр харах үед
            col = 1 if self.group_by_parent else 0
            sheet.merge_range(rowx, col, rowx + 1, col, _('Seq'), format_title)
            col += 1
            if self.show_image:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Image'), format_title)
                col += 1
            sheet.merge_range(rowx, col, rowx + 1, col, _('Product code'), format_title)
            col += 1
            if self.show_barcode:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Barcode'), format_title)
                col += 1
            sheet.merge_range(rowx, col, rowx + 1, col, _('Product name'), format_title)
            col += 1
            sheet.merge_range(rowx, col, rowx + 1, col, _('Unit of measure'), format_title)
            col += 1
            if self.show_size:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Size of Product'), format_title)
                col += 1
            if self.price_view_form == 'with_cost_price':
                sheet.merge_range(rowx, col, rowx, col + 2, _('Initial Balance'), format_title)
                col += 3
            else:
                sheet.merge_range(rowx, col, rowx, col + 1, _('Initial Balance'), format_title)
                col += 2
            if self.price_view_form == 'with_cost_price':
                sheet.merge_range(rowx, col, rowx, col + 2, _('Income'), format_title)
                col += 3
            else:
                sheet.merge_range(rowx, col, rowx, col + 1, _('Income'), format_title)
                col += 2
            if self.price_view_form == 'with_cost_price':
                sheet.merge_range(rowx, col, rowx, col + 2, _('Expense'), format_title)
                col += 3
            else:
                sheet.merge_range(rowx, col, rowx, col + 1, _('Expense'), format_title)
                col += 2
            if self.price_view_form == 'with_cost_price':
                sheet.merge_range(rowx, col, rowx, col + 4, _('End Balance'), format_title)
                col += 5
            else:
                sheet.merge_range(rowx, col, rowx, col + 2, _('End Balance'), format_title)
                col += 3
            if self.show_worthy_resources:
                sheet.merge_range(rowx, col, rowx + 1, col, _(u'Worthly Resources'), format_title)
                col += 1
                sheet.merge_range(rowx, col, rowx + 1, col, _(u'Difference'), format_title)
                col += 1
            if self.show_product_age:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Aging of the goods'), format_title)
                col += 1
            if self.show_reserved:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Reserved'), format_title)
                col += 1
                sheet.merge_range(rowx, col, rowx + 1, col, _('Available balance'), format_title)
                col += 1
            if self.show_residual:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Commodities on the way'), format_title)
                col += 1
                sheet.merge_range(rowx, col, rowx + 1, col, _('ForeCast'), format_title)
                col += 1
                sheet.merge_range(rowx, col, rowx + 1, col, _('Reserved from Transit'), format_title)
                col += 1
                sheet.merge_range(rowx, col, rowx + 1, col, _('Future Forecast'), format_title)
                col += 1
            if self.qty_on_hand:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Quantity on Hand'), format_title)
                col += 1
                sheet.merge_range(rowx, col, rowx + 1, col, _('Quantity on Hand Difference'), format_title)
                col += 1
            if self.show_current_cost:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Current Cost'), format_title)
                col += 1
                sheet.merge_range(rowx, col, rowx + 1, col, _('Cost difference'), format_title)
                col += 1

            col = self.get_main_col_count() + 1
            sheet.write(rowx + 1, col, _('Quantity'), format_title)
            col += 1
            if self.price_view_form == 'with_cost_price':
                sheet.write(rowx + 1, col, _('Unit Cost'), format_title)
                col += 1
            sheet.write(rowx + 1, col, _('Price'), format_title)
            col += 1
            sheet.write(rowx + 1, col, _('Quantity'), format_title)
            col += 1
            if self.price_view_form == 'with_cost_price':
                sheet.write(rowx + 1, col, _('Unit Cost'), format_title)
                col += 1
            sheet.write(rowx + 1, col, _('Price'), format_title)
            col += 1
            sheet.write(rowx + 1, col, _('Quantity'), format_title)
            col += 1
            if self.price_view_form == 'with_cost_price':
                sheet.write(rowx + 1, col, _('Unit Cost'), format_title)
                col += 1
            sheet.write(rowx + 1, col, _('Price'), format_title)
            col += 1
            if self.price_view_form == 'with_cost_price':
                sheet.write(rowx + 1, col, _('Quantity'), format_title)
                col += 1
                sheet.write(rowx + 1, col, _('Unit Cost'), format_title)
                col += 1
                sheet.write(rowx + 1, col, _('Unit price'), format_title)
                col += 1
                sheet.write(rowx + 1, col, _('Cost'), format_title)
                col += 1
            else:
                sheet.write(rowx + 1, col, _('Unit price'), format_title)
                col += 1
                sheet.write(rowx + 1, col, _('Quantity'), format_title)
                col += 1
            sheet.write(rowx + 1, col, _('Price'), format_title)
            col += 1
        else:
            col = 1 if self.group_by_parent else 0
            sheet.merge_range(rowx, col, rowx + 1, col, _('Seq'), format_title)
            col += 1
            if self.show_image:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Image'), format_title)
                col += 1
            sheet.merge_range(rowx, col, rowx + 1, col, _('Product code'), format_title)
            col += 1
            if self.show_barcode:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Barcode'), format_title)
                col += 1
            sheet.merge_range(rowx, col, rowx + 1, col, _('Product name'), format_title)
            col += 1
            sheet.merge_range(rowx, col, rowx + 1, col, _('Unit of measure'), format_title)
            col += 1
            if self.show_size:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Size of Product'), format_title)
                col += 1
            sheet.merge_range(rowx, col, rowx + 1, col, _('Initial Balance'), format_title)
            col += 1
            sheet.merge_range(rowx, col, rowx + 1, col, _('Income'), format_title)
            col += 1
            sheet.merge_range(rowx, col, rowx + 1, col, _('Expense Translate'), format_title)
            col += 1
            sheet.merge_range(rowx, col, rowx + 1, col, _('End Balance Translate'), format_title)
            col += 1
            if self.show_worthy_resources:
                sheet.merge_range(rowx, col, rowx + 1, col, _(u'Worthly Resources'), format_title)
                col += 1
                sheet.merge_range(rowx, col, rowx + 1, col, _(u'Difference'), format_title)
                col += 1
            if self.show_product_age:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Aging of the goods'), format_title)
                col += 1
            if self.show_reserved:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Reserved'), format_title)
                col += 1
                sheet.merge_range(rowx, col, rowx + 1, col, _('Available balance'), format_title)
                col += 1
            if self.show_residual:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Commodities on the way'), format_title)
                col += 1
                sheet.merge_range(rowx, col, rowx + 1, col, _('Forecast'), format_title)
                col += 1
                sheet.merge_range(rowx, col, rowx + 1, col, _('Reserved from Transit'), format_title)
                col += 1
                sheet.merge_range(rowx, col, rowx + 1, col, _('Future Forecast'), format_title)
                col += 1
            if self.qty_on_hand:
                sheet.merge_range(rowx, col, rowx + 1, col, _('Quantity on Hand'), format_title)
                col += 1
                sheet.merge_range(rowx, col, rowx + 1, col, _('Quantity on Hand Difference'), format_title)
                col += 1

        rowx += 2

        # нөхцөл шалгах

        sub_select = ''
        select = ''
        sub_join = ''
        join = ''
        where = ''
        where_done = ''
        account_where = ''
        group = ''
        order = ''
        if self.show_residual:
            where_done = " m.state = 'done' AND "
            where = " m.state in ('assigned','done')"
        else:
            where = " m.state = 'done' "
        if self.product_ids:
            product_ids = self.product_ids.ids
            where += ' AND pp.id in (' + ','.join(map(str, product_ids)) + ') '
        if self.product_size_ids:
            size_ids = self.product_size_ids.ids
            where += ' AND ps.id in (' + ','.join(map(str, size_ids)) + ') '
        if self.category_ids:
            category_ids = self.category_ids.ids
            where += ' AND pc.id in (' + ','.join(map(str, category_ids)) + ') '
        if self.product_brands:
            product_brand_ids = self.product_brands.ids
            where += ' AND pb.id in (' + ','.join(map(str, product_brand_ids)) + ') '
        if self.account_ids:
            account_ids = self.account_ids.ids
            where += ' AND aa.id in (' + ','.join(map(str, account_ids)) + ') '
        if self.supplier_ids:
            supplier_ids = self.supplier_ids.ids
            where += ' AND rp.id in (' + ','.join(map(str, supplier_ids)) + ') '
        having = " HAVING ROUND(SUM(l.start_qty)::decimal,4) <> 0 OR ROUND(SUM(l.start_cost)::decimal,4) <> 0 OR ROUND(SUM(l.qty)::decimal,4) <> 0 \
                 OR ROUND(SUM(l.cost)::decimal,4) <> 0 OR ROUND(SUM(l.ex_qty)::decimal,4) <> 0 OR ROUND(SUM(l.ex_cost)::decimal,4) <> 0 "
        if self.end_balance_non_zero:
            having = " HAVING ROUND(SUM(l.start_qty + l.qty - l.ex_qty)::decimal,4) <> 0 "
        if self.show_residual:
            having += ' OR ROUND(SUM(l.on_way_qty)::decimal,4) <> 0 '
        date_from = self.date_from
        date_to = self.date_to

        if self.type == 'warehouse':
            select = "sl.id as lid,sl.name as lname, "
            select += "CASE WHEN sw.code is null THEN  sw1.code ElSE sw.code END AS wcode, "
            select += "CASE WHEN sw.name is null THEN  sw1.name ElSE sw.name END AS wname, "
            join += "LEFT JOIN stock_location sl ON (l.lid = sl.id)"
            join += "LEFT JOIN stock_location sl1 ON (sl.location_id = sl1.id) "
            join += "LEFT JOIN stock_location sl2 ON (sl1.location_id = sl2.id) "
            join += "LEFT JOIN stock_warehouse sw ON (sw.view_location_id = sl1.id) "
            join += "LEFT JOIN stock_warehouse sw1 ON (sw1.view_location_id = sl2.id) "
            group = ",sl.id, sw.code,  sw1.code ,sw.name , sw1.name ,sl.name "
            order += " sw.code, sw.name, sl.name, "
            if self.group_by == 'by_size':
                order += " l.size_id, "
            if self.group_by == 'by_brand':
                select += " l.brand_name as brand_name, l.brand_id AS brand_id, "
                sub_select += " pb.brand_name AS brand_name, pb.id AS brand_id, "
                sub_join += "LEFT JOIN product_brand pb ON (pb.id = pt.brand_name) "
                group += " , l.brand_id, l.brand_name "
                order += " l.brand_name, l.brand_id, "
            if self.group_by == 'by_account':
                order += " l.account_id, "
            if self.group_by == 'by_supplier':
                select += " l.supp_id AS supp_id, l.supp_name as supp_name, "
                sub_select += "rp.id AS supp_id, rp.name AS supp_name, "
                join += """
                                LEFT JOIN product_template pt ON pt.id = l.ptid
                                LEFT JOIN res_partner rp ON (pt.supplier_id = rp.id)
                            """
                group += ",rp.id, rp.name, supp_id, l.supp_name "
                order += " rp.name, "
        elif self.type == 'size':
            order += " l.size_id, "
        elif self.type == 'account':
            select = "sl.id as lid, sw.code AS wcode, sw.name AS wname, "
            join += "LEFT JOIN stock_location sl ON (l.lid = sl.id)"
            join += "LEFT JOIN stock_location sl1 ON (sl.location_id = sl1.id) "
            join += "LEFT JOIN stock_warehouse sw ON (sw.view_location_id = sl1.id) "
            join += "LEFT JOIN ir_property i ON (i.res_id = 'product.category,'|| l.cid) "
            join += "LEFT JOIN account_account aa ON (aa.id = split_part(i.value_reference, ',', 2)::int) "
            account_where += " WHERE i.name = 'property_stock_valuation_account_id' "
            group = ",sl.id, sw.code, sw.name "
            order += " sw.code, sw.name, "
        elif self.type == 'supplier':
            select = " l.supp_id AS supp_id, l.supp_name as supp_name, "
            sub_select = " rp.id AS supp_id, rp.name AS supp_name, "
            group = ",l.supp_id, l.supp_name "
            order += " l.supp_id, "
        elif self.type == 'category':
            group = " "

        warehouse_dic = {}
        liness = []
        for wh in self.warehouse_ids:
            if self.location_ids:
                locations = self.location_ids.ids
            else:
                loc_id = location_obj.search([('usage', '=', 'internal'), ('location_id', 'child_of', [wh.view_location_id.id])]).ids
                if loc_id:
                    locations += loc_id
                    if loc_id[0] not in warehouse_dic.keys():
                        warehouse_dic[loc_id[0]] = wh.id
                else:
                    raise UserError(_('Stock Location not found!'))
        if len(locations) > 0:
            locations = tuple(locations)
        # эхний үлдэгдэл, Орлого, Зарлагыг авах query
        select1 = ""
        group1 = ""
        if self.type == 'account' or self.group_by == 'by_account':
            if self.type == 'warehouse':
                select1 = "table3.lid as lid,table3.lname as lname, table3.wname as wname, table3.wcode as wcode, "
                group1 = "table3.lid,table3.lname, table3.wname , table3.wcode,"
                order1 = "table3.wcode, table3.account_id,"

            if zero_qty_zero_cost:
                zero_qty_zero_cost_qry1 = " CASE WHEN sum(table3.start_qty) = 0 THEN sum(table3.a_cost) - sum(table3.a_ex_cost) ELSE sum(table3.start_cost) + sum(table3.a_cost) - sum(table3.a_ex_cost) END as end_cost,"
                zero_qty_zero_cost_qry2 = " CASE WHEN SUM(l.start_qty) = 0 THEN SUM(l.cost - l.ex_cost) ELSE SUM(l.start_cost + l.cost - l.ex_cost) END AS end_cost,"
                                            
            else:
                zero_qty_zero_cost_qry1 = " sum(table3.start_cost) + sum(table3.a_cost) - sum(table3.a_ex_cost) as end_cost,"
                zero_qty_zero_cost_qry2 = " SUM(l.start_cost + l.cost - l.ex_cost) AS end_cost,"
            
            query = '''
            SELECT table3.list_price, table3.pid, table3.ptid, table3.name, table3.code, table3.barcode, table3.uom_name, 
            table3.cid, table3.cname, table3.size, table3.size_id, table3.worthy_balance, 
            sum(table3.start_qty) as start_qty, sum(table3.start_cost) as start_cost,
            sum(table3.a_qty) as qty, sum(table3.a_ex_qty) as ex_qty, sum(table3.a_cost) as cost,
            sum(table3.start_qty)+sum(table3.a_qty)-sum(table3.a_ex_qty) as end_qty, sum(table3.a_ex_cost) as ex_cost,
            ''' + zero_qty_zero_cost_qry1 + ''' 
            ''' + select1 + '''
            table3.account_id, table3.account_code, table3.account_name 
            FROM (SELECT * FROM (SELECT
                        l.date,
                        sum(l.debit) as debit, sum(l.credit) as credit, sum(l.quantity) as a_qty, sum(l.ex_quantity) as a_ex_qty,
                        sum(l.a_cost) as a_cost, sum(l.a_ex_cost) as a_ex_cost,
                        l.spt_code,
                        l.list_price,
                        l.prod_id AS pid,
                        l.ptid AS ptid,
                        l.name AS name,
                        l.code AS code,
                        l.barcode AS barcode,
                        l.uom_name AS uom_name,
                        l.cid AS cid,
                        l.cname AS cname,
                        l.size_name AS size,
                        l.size_id AS size_id,
                        l.account_id AS account_id,
                        l.account_name AS account_name,
                        l.account_code AS account_code,
                        l.worthy_balance AS worthy_balance, ''' + select + '''
                        SUM(l.start_qty) AS start_qty,
                        SUM(l.start_cost) AS start_cost,
                        SUM(l.qty) AS qty,
                        SUM(l.cost) AS
                        COST,
                        SUM(l.ex_qty) AS ex_qty,
                        SUM(l.ex_cost) AS ex_cost,
                        SUM(l.start_qty + l.qty - l.ex_qty) AS end_qty,
                        ''' + zero_qty_zero_cost_qry2 + '''
                        CASE WHEN l.aml_id != - 1 THEN
                            l.aml_id
                        ELSE
                            l.aml_id1
                        END AS account_ml_id
                    FROM (
                        SELECT
                            m.date as date,
                            spt.code as spt_code,
                            aml.debit, aml.credit, 
                            CASE WHEN aml.debit > 0 AND m.date BETWEEN %s  
                                AND %s THEN aml.quantity ELSE 0 END AS quantity,
                            CASE WHEN aml.credit > 0 AND m.date BETWEEN %s  
                                AND %s THEN aml.quantity ELSE 0 END AS ex_quantity,
                            CASE WHEN aml.debit > 0 AND m.date BETWEEN %s  
                                AND %s THEN aml.debit ELSE 0 END AS a_cost,
                            CASE WHEN aml.credit > 0 AND m.date BETWEEN %s  
                                AND %s THEN aml.credit ELSE 0 END AS a_ex_cost,
                            pt.list_price AS list_price,
                            m.state,
                            ps.name AS size_name,
                            ps.id AS size_id,
                            m.product_id AS prod_id,
                            pt.id AS ptid,
                            pt.name AS name,
                            pp.default_code AS code,
                            pp.barcode AS barcode,
                            u2.name AS uom_name,
                            pc.id AS cid,
                            pc.name AS cname,
                            aa.id AS account_id,
                            aa.name AS account_name,
                            aa.code AS account_code,
                            pt.worthy_balance AS worthy_balance, ''' + sub_select + '''
                             CASE WHEN m.date < %s
                                AND m.location_id NOT IN %s 
                                AND m.location_dest_id IN %s THEN
                                COALESCE(m.product_uom_qty / u.factor * u2.factor, 0)
                            WHEN m.date < %s
                                AND m.location_id IN %s
                                AND m.location_dest_id NOT IN %s THEN
                                - COALESCE(m.product_uom_qty / u.factor * u2.factor, 0)
                            ELSE
                                0
                            END AS start_qty,
                            CASE WHEN m.date < %s
                                AND m.location_id NOT IN %s
                                AND m.location_dest_id IN %s THEN
                                COALESCE(m.price_unit * m.product_uom_qty, 0)
                            WHEN m.date < %s
                                AND m.location_id IN %s
                                AND m.location_dest_id NOT IN %s THEN
                                - COALESCE(m.price_unit * m.product_uom_qty, 0)
                            ELSE
                                0
                            END AS start_cost,
                            CASE WHEN m.date BETWEEN %s
                                AND %s 
                                AND m.location_id NOT IN %s
                                AND m.location_dest_id IN %s THEN
                                COALESCE(m.product_uom_qty / u.factor * u2.factor, 0)
                            ELSE
                                0
                            END AS qty,
                            CASE WHEN m.date BETWEEN %s 
                                AND %s 
                                AND m.location_id NOT IN %s 
                                AND m.location_dest_id IN %s  THEN
                                COALESCE(m.price_unit * m.product_uom_qty, 0)
                            ELSE
                                0
                            END AS
                            COST,
                            CASE WHEN m.date BETWEEN %s 
                                AND %s 
                                AND m.location_id IN %s
                                AND m.location_dest_id NOT IN %s THEN
                                COALESCE(m.product_uom_qty / u.factor * u2.factor, 0)
                            ELSE
                                0
                            END AS ex_qty,
                            CASE WHEN m.date BETWEEN %s 
                                AND %s  
                                AND m.location_id IN %s 
                                AND m.location_dest_id NOT IN %s  THEN
                                COALESCE(m.price_unit * m.product_uom_qty, 0)
                            ELSE
                                0
                            END AS ex_cost,
                            CASE WHEN m.location_id NOT IN %s 
                                AND m.location_dest_id IN %s THEN
                                m.location_dest_id
                            WHEN m.location_id IN %s 
                                AND m.location_dest_id NOT IN %s THEN
                                m.location_id
                            ELSE
                                0
                            END AS lid,
                            CASE WHEN spt.code = 'incoming'
                                AND aml.debit > 0 THEN
                                aml.id
                            ELSE
                                - 1
                            END AS aml_id,
                            CASE WHEN spt.code = 'outgoing'
                                AND aml.credit > 0 THEN
                                aml.id
                            ELSE
                                - 1
                            END AS aml_id1
                        FROM
                            stock_move m
                        LEFT JOIN product_product pp ON (pp.id = m.product_id)
                        LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id)
                        LEFT JOIN product_size ps ON (ps.id = pt.size_id)
                        LEFT JOIN res_partner rp ON (pt.supplier_id = rp.id)
                        LEFT JOIN product_uom u ON (u.id = m.product_uom)
                        LEFT JOIN product_uom u2 ON (u2.id = pt.uom_id)
                        LEFT JOIN stock_picking_type spt ON (spt.id = m.picking_type_id)
                        LEFT JOIN account_move_line aml ON (aml.stock_move_id = m.id)
                        LEFT JOIN account_account aa ON (aa.id = aml.account_id)
                        LEFT JOIN product_category pc ON (pt.categ_id = pc.id) ''' + sub_join + '''
                    WHERE
                        ''' + where + ''' order by m.date asc ) AS l ''' + join + account_where + ''' 
                          GROUP BY 
                l.date,
                l.spt_code,
                    l.prod_id,
                    l.ptid,
                    l.name,
                    l.list_price,
                    l.code,
                    l.barcode,
                    l.uom_name,
                    l.cid,
                    l.cname,
                    l.size_id,
                    l.size_name,
                    l.aml_id,
                    l.account_name,
                    l.aml_id1,
                    l.account_id,
                    l.account_code, 
                    l.worthy_balance ''' + group + having + '''
                      ORDER BY
                     l.date, l.spt_code, ''' + order + '''  l.cname,
                    l.name,
                    l.size_name,
                    l.code,
                    l.worthy_balance,
                    l.uom_name ) as table1 WHERE table1.account_ml_id <> -1 UNION 
                    SELECT * FROM (SELECT
                        l.date,
                        sum(l.debit) as debit, sum(l.credit) as credit, 
                        sum(l.quantity) as a_qty, sum(l.ex_quantity) as a_ex_qty,
                        sum(l.a_cost) as a_cost, sum(l.a_ex_cost) as a_ex_cost,
                        l.spt_code,
                        l.list_price,
                        l.prod_id AS pid,
                        l.ptid AS ptid,
                        l.name AS name,
                        l.code AS code,
                        l.barcode AS barcode,
                        l.uom_name AS uom_name,
                        l.cid AS cid,
                        l.cname AS cname,
                        l.size_name AS size,
                        l.size_id AS size_id,
                        l.account_id AS account_id,
                        l.account_name AS account_name,
                        l.account_code AS account_code,
                        l.worthy_balance AS worthy_balance, ''' + select + ''' 
                        SUM(l.start_qty) AS start_qty,
                        SUM(l.start_cost) AS start_cost,
                        SUM(l.qty) AS qty,
                        SUM(l.cost) AS
                        COST,
                        SUM(l.ex_qty) AS ex_qty,
                        SUM(l.ex_cost) AS ex_cost,
                        SUM(l.start_qty + l.qty - l.ex_qty) AS end_qty,
                        ''' + zero_qty_zero_cost_qry2 + '''
                        CASE WHEN l.aml_id != - 1 THEN
                            l.aml_id
                        ELSE
                            l.aml_id1
                        END AS account_ml_id
                    FROM (
                        SELECT
                            m.date as date,
                            spt.code as spt_code,
                            aml.debit, aml.credit, 
                            CASE WHEN aml.debit > 0 AND m.date BETWEEN %s  
                                AND %s THEN aml.quantity ELSE 0 END AS quantity,
                            CASE WHEN aml.credit > 0 AND m.date BETWEEN %s  
                                AND %s THEN aml.quantity ELSE 0 END AS ex_quantity,
                            CASE WHEN aml.debit > 0 AND m.date BETWEEN %s  
                                AND %s THEN aml.debit ELSE 0 END AS a_cost,
                            CASE WHEN aml.credit > 0 AND m.date BETWEEN %s  
                                AND %s THEN aml.credit ELSE 0 END AS a_ex_cost,
                            pt.list_price AS list_price,
                            m.state,
                            ps.name AS size_name,
                            ps.id AS size_id,
                            m.product_id AS prod_id,
                            pt.id AS ptid,
                            pt.name AS name,
                            pp.default_code AS code,
                            pp.barcode AS barcode,
                            u2.name AS uom_name,
                            pc.id AS cid,
                            pc.name AS cname,
                            aa.id AS account_id,
                            aa.name AS account_name,
                            aa.code AS account_code,
                            pt.worthy_balance AS worthy_balance, ''' + sub_select + '''
                             CASE WHEN m.date < %s 
                                AND m.location_id NOT IN %s 
                                AND m.location_dest_id IN %s THEN
                                COALESCE(m.product_uom_qty / u.factor * u2.factor, 0)
                            WHEN m.date < %s 
                                AND m.location_id IN %s 
                                AND m.location_dest_id NOT IN %s THEN
                                - COALESCE(m.product_uom_qty / u.factor * u2.factor, 0)
                            ELSE
                                0
                            END AS start_qty,
                            CASE WHEN m.date < %s 
                                AND m.location_id NOT IN %s 
                                AND m.location_dest_id IN %s THEN
                                COALESCE(m.price_unit * m.product_uom_qty, 0)
                            WHEN m.date < %s 
                                AND m.location_id IN %s 
                                AND m.location_dest_id NOT IN %s THEN
                                - COALESCE(m.price_unit * m.product_uom_qty, 0)
                            ELSE
                                0
                            END AS start_cost,
                            CASE WHEN m.date BETWEEN %s 
                                AND %s 
                                AND m.location_id NOT IN %s 
                                AND m.location_dest_id IN %s THEN
                                COALESCE(m.product_uom_qty / u.factor * u2.factor, 0)
                            ELSE
                                0
                            END AS qty,
                            CASE WHEN m.date BETWEEN %s 
                                AND %s 
                                AND m.location_id NOT IN %s 
                                AND m.location_dest_id IN %s THEN
                                COALESCE(m.price_unit * m.product_uom_qty, 0)
                            ELSE
                                0
                            END AS
                            COST,
                            CASE WHEN m.date BETWEEN %s 
                                AND %s  
                                AND m.location_id IN %s 
                                AND m.location_dest_id NOT IN %s THEN
                                COALESCE(m.product_uom_qty / u.factor * u2.factor, 0)
                            ELSE
                                0
                            END AS ex_qty,
                            CASE WHEN m.date BETWEEN %s  
                                AND %s  
                                AND m.location_id IN %s 
                                AND m.location_dest_id NOT IN %s THEN
                                COALESCE(m.price_unit * m.product_uom_qty, 0)
                            ELSE
                                0
                            END AS ex_cost,
                            CASE WHEN m.location_id NOT IN %s
                                AND m.location_dest_id IN %s THEN
                                m.location_dest_id
                            WHEN m.location_id IN %s 
                                AND m.location_dest_id NOT IN %s THEN
                                m.location_id
                            ELSE
                                0
                            END AS lid,
                            CASE WHEN spt.code = 'incoming'
                                AND aml.debit > 0 THEN
                                aml.id
                            ELSE
                                - 1
                            END AS aml_id,
                            CASE WHEN spt.code = 'outgoing'
                                AND aml.credit > 0 THEN
                                aml.id
                            ELSE
                                - 1
                            END AS aml_id1
                        FROM
                            stock_move m
                        LEFT JOIN product_product pp ON (pp.id = m.product_id)
                        LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id)
                        LEFT JOIN product_size ps ON (ps.id = pt.size_id)
                        LEFT JOIN res_partner rp ON (pt.supplier_id = rp.id)
                        LEFT JOIN product_uom u ON (u.id = m.product_uom)
                        LEFT JOIN product_uom u2 ON (u2.id = pt.uom_id)
                        LEFT JOIN stock_picking_type spt ON (spt.id = m.picking_type_id)
                        LEFT JOIN account_move_line aml ON (aml.stock_move_id = m.id)
                        LEFT JOIN account_account aa ON (aa.id = aml.account_id)
                        LEFT JOIN product_category pc ON (pt.categ_id = pc.id) ''' + sub_join + '''
                    WHERE
                         ''' + where + ''' order by m.date asc ) AS l ''' + join + account_where + '''   
                        GROUP BY
                l.date,
                l.spt_code,
                    l.prod_id,
                    l.ptid,
                    l.name,
                    l.list_price,
                    l.code,
                    l.barcode,
                    l.uom_name,
                    l.cid,
                    l.cname,
                    l.size_id,
                    l.size_name,
                    l.aml_id,
                    l.account_name,
                    l.aml_id1,
                    l.account_id,
                    l.account_code, 
                    l.worthy_balance ''' + group + having + '''  
                    ORDER BY
                     l.date, l.spt_code, ''' + order + '''  l.cname,
                    l.name,
                    l.size_name,
                    l.code,
                    l.worthy_balance,
                    l.uom_name) as table2 WHERE table2.spt_code is null AND table2.date BETWEEN '2020-01-01 16:00:00' 
                                AND '2020-11-17 15:59:59' AND table2.account_id in (SELECT table2.account_id FROM (SELECT
                        l.date,
                        l.spt_code,
                        l.list_price,
                        l.prod_id AS pid,
                        l.ptid AS ptid,
                        l.name AS name,
                        l.code AS code,
                        l.barcode AS barcode,
                        l.uom_name AS uom_name,
                        l.cid AS cid,
                        l.cname AS cname,
                        l.size_name AS size,
                        l.size_id AS size_id,
                        l.account_id AS account_id,
                        l.account_name AS account_name,
                        l.account_code AS account_code,
                        l.worthy_balance AS worthy_balance, ''' + select + '''
                        SUM(l.start_qty) AS start_qty,
                        SUM(l.start_cost) AS start_cost,
                        SUM(l.qty) AS qty,
                        SUM(l.cost) AS
                        COST,
                        SUM(l.ex_qty) AS ex_qty,
                        SUM(l.ex_cost) AS ex_cost,
                        SUM(l.start_qty + l.qty - l.ex_qty) AS end_qty,
                        ''' + zero_qty_zero_cost_qry2 + '''
                        CASE WHEN l.aml_id != - 1 THEN
                            l.aml_id
                        ELSE
                            l.aml_id1
                        END AS account_ml_id
                    FROM (
                        SELECT
                            m.date as date,
                            spt.code as spt_code,
                            aml.debit, aml.credit, 
                            CASE WHEN aml.debit > 0 AND m.date BETWEEN %s  
                                AND %s THEN aml.quantity ELSE 0 END AS quantity,
                            CASE WHEN aml.credit > 0 AND m.date BETWEEN %s  
                                AND %s THEN aml.quantity ELSE 0 END AS ex_quantity,
                            CASE WHEN aml.debit > 0 AND m.date BETWEEN %s  
                                AND %s THEN aml.debit ELSE 0 END AS a_cost,
                            CASE WHEN aml.credit > 0 AND m.date BETWEEN %s  
                                AND %s THEN aml.credit ELSE 0 END AS a_ex_cost,
                            pt.list_price AS list_price,
                            m.state,
                            ps.name AS size_name,
                            ps.id AS size_id,
                            m.product_id AS prod_id,
                            pt.id AS ptid,
                            pt.name AS name,
                            pp.default_code AS code,
                            pp.barcode AS barcode,
                            u2.name AS uom_name,
                            pc.id AS cid,
                            pc.name AS cname,
                            aa.id AS account_id,
                            aa.name AS account_name,
                            aa.code AS account_code,
                            pt.worthy_balance AS worthy_balance, ''' + sub_select + '''
                             CASE WHEN m.date < %s 
                                AND m.location_id NOT IN %s 
                                AND m.location_dest_id IN %s THEN
                                COALESCE(m.product_uom_qty / u.factor * u2.factor, 0)
                            WHEN m.date < %s 
                                AND m.location_id IN %s 
                                AND m.location_dest_id NOT IN %s THEN
                                - COALESCE(m.product_uom_qty / u.factor * u2.factor, 0)
                            ELSE
                                0
                            END AS start_qty,
                            CASE WHEN m.date < %s 
                                AND m.location_id NOT IN %s 
                                AND m.location_dest_id IN %s THEN
                                COALESCE(m.price_unit * m.product_uom_qty, 0)
                            WHEN m.date < %s 
                                AND m.location_id IN %s 
                                AND m.location_dest_id NOT IN %s THEN
                                - COALESCE(m.price_unit * m.product_uom_qty, 0)
                            ELSE
                                0
                            END AS start_cost,
                            CASE WHEN m.date BETWEEN %s 
                                AND %s 
                                AND m.location_id NOT IN %s 
                                AND m.location_dest_id IN %s THEN
                                COALESCE(m.product_uom_qty / u.factor * u2.factor, 0)
                            ELSE
                                0
                            END AS qty,
                            CASE WHEN m.date BETWEEN %s 
                                AND %s  
                                AND m.location_id NOT IN %s 
                                AND m.location_dest_id IN %s THEN
                                COALESCE(m.price_unit * m.product_uom_qty, 0)
                            ELSE
                                0
                            END AS
                            COST,
                            CASE WHEN m.date BETWEEN %s 
                                AND %s 
                                AND m.location_id IN %s 
                                AND m.location_dest_id NOT IN %s THEN
                                COALESCE(m.product_uom_qty / u.factor * u2.factor, 0)
                            ELSE
                                0
                            END AS ex_qty,
                            CASE WHEN m.date BETWEEN %s 
                                AND %s 
                                AND m.location_id IN %s 
                                AND m.location_dest_id NOT IN %s THEN
                                COALESCE(m.price_unit * m.product_uom_qty, 0)
                            ELSE
                                0
                            END AS ex_cost,
                            CASE WHEN m.location_id NOT IN %s 
                                AND m.location_dest_id IN %s THEN
                                m.location_dest_id
                            WHEN m.location_id IN %s 
                                AND m.location_dest_id NOT IN %s THEN
                                m.location_id
                            ELSE
                                0
                            END AS lid,
                            CASE WHEN spt.code = 'incoming'
                                AND aml.debit > 0 THEN
                                aml.id
                            ELSE
                                - 1
                            END AS aml_id,
                            CASE WHEN spt.code = 'outgoing'
                                AND aml.credit > 0 THEN
                                aml.id
                            ELSE
                                - 1
                            END AS aml_id1
                        FROM
                            stock_move m
                        LEFT JOIN product_product pp ON (pp.id = m.product_id)
                        LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id)
                        LEFT JOIN product_size ps ON (ps.id = pt.size_id)
                        LEFT JOIN res_partner rp ON (pt.supplier_id = rp.id)
                        LEFT JOIN product_uom u ON (u.id = m.product_uom)
                        LEFT JOIN product_uom u2 ON (u2.id = pt.uom_id)
                        LEFT JOIN stock_picking_type spt ON (spt.id = m.picking_type_id)
                        LEFT JOIN account_move_line aml ON (aml.stock_move_id = m.id)
                        LEFT JOIN account_account aa ON (aa.id = aml.account_id)
                        LEFT JOIN product_category pc ON (pt.categ_id = pc.id) ''' + sub_join + ''' 
                    WHERE
                        ''' + where + ''' order by m.date asc ) AS l ''' + join + account_where + '''     
                        GROUP BY
                l.date,
                l.spt_code,
                    l.prod_id,
                    l.ptid,
                    l.name,
                    l.list_price,
                    l.code,
                    l.barcode,
                    l.uom_name,
                    l.cid,
                    l.cname,
                    l.size_id,
                    l.size_name,
                    l.aml_id,
                    l.account_name,
                    l.aml_id1,
                    l.account_id,
                    l.account_code, 
                    l.worthy_balance ''' + group + having + '''  
                    ORDER BY
                     l.date, l.spt_code, ''' + order + '''  l.cname,
                    l.name,
                    l.size_name,
                    l.code,
                    l.worthy_balance,
                    l.uom_name) as table2 WHERE table2.account_ml_id <> -1 GROUP BY table2.account_id) 
                    UNION 
                    SELECT * FROM (SELECT
                        l.date,
                        sum(l.debit) as debit, sum(l.credit) as credit, 
                        sum(l.quantity) as a_qty, sum(l.ex_quantity) as a_ex_qty,
                        sum(l.a_cost) as a_cost, sum(l.a_ex_cost) as a_ex_cost,
                        l.spt_code,
                        l.list_price,
                        l.prod_id AS pid,
                        l.ptid AS ptid,
                        l.name AS name,
                        l.code AS code,
                        l.barcode AS barcode,
                        l.uom_name AS uom_name,
                        l.cid AS cid,
                        l.cname AS cname,
                        l.size_name AS size,
                        l.size_id AS size_id,
                        l.account_id AS account_id,
                        l.account_name AS account_name,
                        l.account_code AS account_code,
                        l.worthy_balance AS worthy_balance, ''' + select + ''' 
                        SUM(l.start_qty) AS start_qty,
                        SUM(l.start_cost) AS start_cost,
                        SUM(l.qty) AS qty,
                        SUM(l.cost) AS
                        COST,
                        SUM(l.ex_qty) AS ex_qty,
                        SUM(l.ex_cost) AS ex_cost,
                        SUM(l.start_qty + l.qty - l.ex_qty) AS end_qty,
                        ''' + zero_qty_zero_cost_qry2 + '''
                        CASE WHEN l.aml_id != - 1 THEN
                            l.aml_id
                        ELSE
                            l.aml_id1
                        END AS account_ml_id
                    FROM (
                        SELECT
                            m.date as date,
                            spt.code as spt_code,
                            aml.debit, aml.credit, 
                            CASE WHEN aml.debit > 0 AND m.date BETWEEN %s  
                                AND %s THEN aml.quantity ELSE 0 END AS quantity,
                            CASE WHEN aml.credit > 0 AND m.date BETWEEN %s  
                                AND %s THEN aml.quantity ELSE 0 END AS ex_quantity,
                            CASE WHEN aml.debit > 0 AND m.date BETWEEN %s  
                                AND %s THEN aml.debit ELSE 0 END AS a_cost,
                            CASE WHEN aml.credit > 0 AND m.date BETWEEN %s  
                                AND %s THEN aml.credit ELSE 0 END AS a_ex_cost,
                            pt.list_price AS list_price,
                            m.state,
                            ps.name AS size_name,
                            ps.id AS size_id,
                            m.product_id AS prod_id,
                            pt.id AS ptid,
                            pt.name AS name,
                            pp.default_code AS code,
                            pp.barcode AS barcode,
                            u2.name AS uom_name,
                            pc.id AS cid,
                            pc.name AS cname,
                            aa.id AS account_id,
                            aa.name AS account_name,
                            aa.code AS account_code,
                            pt.worthy_balance AS worthy_balance, ''' + sub_select + '''
                             CASE WHEN m.date < %s
                                AND m.location_id NOT IN %s 
                                AND m.location_dest_id IN %s THEN
                                COALESCE(m.product_uom_qty / u.factor * u2.factor, 0)
                            WHEN m.date < %s 
                                AND m.location_id IN %s 
                                AND m.location_dest_id NOT IN %s THEN
                                - COALESCE(m.product_uom_qty / u.factor * u2.factor, 0)
                            ELSE
                                0
                            END AS start_qty,
                            CASE WHEN m.date < %s 
                                AND m.location_id NOT IN %s 
                                AND m.location_dest_id IN %s  THEN
                                COALESCE(m.price_unit * m.product_uom_qty, 0)
                            WHEN m.date < %s 
                                AND m.location_id IN %s 
                                AND m.location_dest_id NOT IN %s THEN
                                - COALESCE(m.price_unit * m.product_uom_qty, 0)
                            ELSE
                                0
                            END AS start_cost,
                            CASE WHEN m.date BETWEEN %s 
                                AND %s 
                                AND m.location_id NOT IN %s 
                                AND m.location_dest_id IN %s THEN
                                COALESCE(m.product_uom_qty / u.factor * u2.factor, 0)
                            ELSE
                                0
                            END AS qty,
                            CASE WHEN m.date BETWEEN %s  
                                AND %s  
                                AND m.location_id NOT IN %s 
                                AND m.location_dest_id IN %s THEN
                                COALESCE(m.price_unit * m.product_uom_qty, 0)
                            ELSE
                                0
                            END AS
                            COST,
                            CASE WHEN m.date BETWEEN %s  
                                AND %s 
                                AND m.location_id IN %s 
                                AND m.location_dest_id NOT IN %s THEN
                                COALESCE(m.product_uom_qty / u.factor * u2.factor, 0)
                            ELSE
                                0
                            END AS ex_qty,
                            CASE WHEN m.date BETWEEN %s 
                                AND %s 
                                AND m.location_id IN %s 
                                AND m.location_dest_id NOT IN %s THEN
                                COALESCE(m.price_unit * m.product_uom_qty, 0)
                            ELSE
                                0
                            END AS ex_cost,
                            CASE WHEN m.location_id NOT IN %s 
                                AND m.location_dest_id IN %s THEN
                                m.location_dest_id
                            WHEN m.location_id IN %s 
                                AND m.location_dest_id NOT IN %s THEN
                                m.location_id
                            ELSE
                                0
                            END AS lid,
                            CASE WHEN spt.code = 'incoming'
                                AND aml.debit > 0 THEN
                                aml.id
                            ELSE
                                - 1
                            END AS aml_id,
                            CASE WHEN spt.code = 'outgoing'
                                AND aml.credit > 0 THEN
                                aml.id
                            ELSE
                                - 1
                            END AS aml_id1
                        FROM
                            stock_move m
                        LEFT JOIN product_product pp ON (pp.id = m.product_id)
                        LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id)
                        LEFT JOIN product_size ps ON (ps.id = pt.size_id)
                        LEFT JOIN res_partner rp ON (pt.supplier_id = rp.id)
                        LEFT JOIN product_uom u ON (u.id = m.product_uom)
                        LEFT JOIN product_uom u2 ON (u2.id = pt.uom_id)
                        LEFT JOIN stock_picking_type spt ON (spt.id = m.picking_type_id)
                        LEFT JOIN account_move_line aml ON (aml.stock_move_id = m.id)
                        LEFT JOIN account_account aa ON (aa.id = aml.account_id)
                        LEFT JOIN product_category pc ON (pt.categ_id = pc.id) ''' + sub_join + ''' 
                    WHERE
                        ''' + where + ''' order by m.date asc ) AS l ''' + join + account_where + '''   
                        GROUP BY
                l.date,
                l.spt_code,
                    l.prod_id,
                    l.ptid,
                    l.name,
                    l.list_price,
                    l.code,
                    l.barcode,
                    l.uom_name,
                    l.cid,
                    l.cname,
                    l.size_id,
                    l.size_name,
                    l.aml_id,
                    l.account_name,
                    l.aml_id1,
                    l.account_id,
                    l.account_code, 
                    l.worthy_balance ''' + group + having + '''
                    ORDER BY
                     l.date, l.spt_code, ''' + order + '''  l.cname,
                    l.name,
                    l.size_name,
                    l.code,
                    l.worthy_balance,
                    l.uom_name) as table4 WHERE table4.date < %s AND table4.account_id in (SELECT table2.account_id FROM (SELECT
                        l.date,
                        l.spt_code,
                        l.list_price,
                        l.prod_id AS pid,
                        l.ptid AS ptid,
                        l.name AS name,
                        l.code AS code,
                        l.barcode AS barcode,
                        l.uom_name AS uom_name,
                        l.cid AS cid,
                        l.cname AS cname,
                        l.size_name AS size,
                        l.size_id AS size_id,
                        l.account_id AS account_id,
                        l.account_name AS account_name,
                        l.account_code AS account_code,
                        l.worthy_balance AS worthy_balance, ''' + select + ''' 
                        SUM(l.start_qty) AS start_qty,
                        SUM(l.start_cost) AS start_cost,
                        SUM(l.qty) AS qty,
                        SUM(l.cost) AS
                        COST,
                        SUM(l.ex_qty) AS ex_qty,
                        SUM(l.ex_cost) AS ex_cost,
                        SUM(l.start_qty + l.qty - l.ex_qty) AS end_qty,
                        ''' + zero_qty_zero_cost_qry2 + '''
                        CASE WHEN l.aml_id != - 1 THEN
                            l.aml_id
                        ELSE
                            l.aml_id1
                        END AS account_ml_id
                    FROM (
                        SELECT
                            m.date as date,
                            spt.code as spt_code,
                            aml.debit, aml.credit, 
                            CASE WHEN aml.debit > 0 AND m.date BETWEEN %s  
                                AND %s THEN aml.quantity ELSE 0 END AS quantity,
                            CASE WHEN aml.credit > 0 AND m.date BETWEEN %s  
                                AND %s THEN aml.quantity ELSE 0 END AS ex_quantity,
                            CASE WHEN aml.debit > 0 AND m.date BETWEEN %s  
                                AND %s THEN aml.debit ELSE 0 END AS a_cost,
                            CASE WHEN aml.credit > 0 AND m.date BETWEEN %s  
                                AND %s THEN aml.credit ELSE 0 END AS a_ex_cost,
                            pt.list_price AS list_price,
                            m.state,
                            ps.name AS size_name,
                            ps.id AS size_id,
                            m.product_id AS prod_id,
                            pt.id AS ptid,
                            pt.name AS name,
                            pp.default_code AS code,
                            pp.barcode AS barcode,
                            u2.name AS uom_name,
                            pc.id AS cid,
                            pc.name AS cname,
                            aa.id AS account_id,
                            aa.name AS account_name,
                            aa.code AS account_code,
                            pt.worthy_balance AS worthy_balance, ''' + sub_select + '''
                             CASE WHEN m.date < %s 
                                AND m.location_id NOT IN %s 
                                AND m.location_dest_id IN %s THEN
                                COALESCE(m.product_uom_qty / u.factor * u2.factor, 0)
                            WHEN m.date < %s 
                                AND m.location_id IN %s 
                                AND m.location_dest_id NOT IN %s THEN
                                - COALESCE(m.product_uom_qty / u.factor * u2.factor, 0)
                            ELSE
                                0
                            END AS start_qty,
                            CASE WHEN m.date < %s 
                                AND m.location_id NOT IN %s 
                                AND m.location_dest_id IN %s THEN
                                COALESCE(m.price_unit * m.product_uom_qty, 0)
                            WHEN m.date < %s 
                                AND m.location_id IN %s 
                                AND m.location_dest_id NOT IN %s THEN
                                - COALESCE(m.price_unit * m.product_uom_qty, 0)
                            ELSE
                                0
                            END AS start_cost,
                            CASE WHEN m.date BETWEEN %s 
                                AND %s 
                                AND m.location_id NOT IN %s 
                                AND m.location_dest_id IN %s THEN
                                COALESCE(m.product_uom_qty / u.factor * u2.factor, 0)
                            ELSE
                                0
                            END AS qty,
                            CASE WHEN m.date BETWEEN %s 
                                AND %s 
                                AND m.location_id NOT IN %s 
                                AND m.location_dest_id IN %s THEN
                                COALESCE(m.price_unit * m.product_uom_qty, 0)
                            ELSE
                                0
                            END AS
                            COST,
                            CASE WHEN m.date BETWEEN %s  
                                AND %s  
                                AND m.location_id IN %s 
                                AND m.location_dest_id NOT IN %s THEN
                                COALESCE(m.product_uom_qty / u.factor * u2.factor, 0)
                            ELSE
                                0
                            END AS ex_qty,
                            CASE WHEN m.date BETWEEN %s 
                                AND %s  
                                AND m.location_id IN %s 
                                AND m.location_dest_id NOT IN %s THEN
                                COALESCE(m.price_unit * m.product_uom_qty, 0)
                            ELSE
                                0
                            END AS ex_cost,
                            CASE WHEN m.location_id NOT IN %s 
                                AND m.location_dest_id IN %s THEN
                                m.location_dest_id
                            WHEN m.location_id IN %s 
                                AND m.location_dest_id NOT IN %s THEN
                                m.location_id
                            ELSE
                                0
                            END AS lid,
                            CASE WHEN spt.code = 'incoming'
                                AND aml.debit > 0 THEN
                                aml.id
                            ELSE
                                - 1
                            END AS aml_id,
                            CASE WHEN spt.code = 'outgoing'
                                AND aml.credit > 0 THEN
                                aml.id
                            ELSE
                                - 1
                            END AS aml_id1
                        FROM
                            stock_move m
                        LEFT JOIN product_product pp ON (pp.id = m.product_id)
                        LEFT JOIN product_template pt ON (pt.id = pp.product_tmpl_id)
                        LEFT JOIN product_size ps ON (ps.id = pt.size_id)
                        LEFT JOIN res_partner rp ON (pt.supplier_id = rp.id)
                        LEFT JOIN product_uom u ON (u.id = m.product_uom)
                        LEFT JOIN product_uom u2 ON (u2.id = pt.uom_id)
                        LEFT JOIN stock_picking_type spt ON (spt.id = m.picking_type_id)
                        LEFT JOIN account_move_line aml ON (aml.stock_move_id = m.id)
                        LEFT JOIN account_account aa ON (aa.id = aml.account_id)
                        LEFT JOIN product_category pc ON (pt.categ_id = pc.id) ''' + sub_join + ''' 
                    WHERE
                        ''' + where + ''' order by m.date asc ) AS l ''' + join + account_where + ''' 
                        GROUP BY
                l.date,
                l.spt_code,
                    l.prod_id,
                    l.ptid,
                    l.name,
                    l.list_price,
                    l.code,
                    l.barcode,
                    l.uom_name,
                    l.cid,
                    l.cname,
                    l.size_id,
                    l.size_name,
                    l.aml_id,
                    l.account_name,
                    l.aml_id1,
                    l.account_id,
                    l.account_code, 
                    l.worthy_balance ''' + group + having + '''
                    ORDER BY
                     l.date, l.spt_code, ''' + order + '''  l.cname,
                    l.name,
                    l.size_name,
                    l.code,
                    l.worthy_balance,
                    l.uom_name) as table2 WHERE table2.account_ml_id <> -1 GROUP BY table2.account_id)
                    ) AS table3 
                    GROUP BY table3.account_id,table3.list_price, table3.pid, table3.ptid, table3.name, ''' + group1 + '''
                    table3.code, table3.barcode, table3.uom_name, table3.cid, table3.cname, table3.size, table3.size_id, table3.account_code, table3.account_name, table3.worthy_balance 
                    ORDER BY ''' + order1 + ''' table3.pid, table3.ptid, table3.name
            '''

            self.env.cr.execute(query, (
                date_from, date_to, date_from, date_to, date_from, date_to, date_from, date_to,
                date_from, locations, locations, date_from, locations, locations,
                date_from, locations, locations, date_from, locations, locations,
                date_from, date_to, locations, locations, date_from, date_to, locations, locations,
                date_from, date_to, locations, locations, date_from, date_to, locations, locations,
                locations, locations, locations, locations,
                date_from, date_to, date_from, date_to, date_from, date_to, date_from, date_to,
                date_from, locations, locations, date_from, locations, locations,
                date_from, locations, locations, date_from, locations, locations,
                date_from, date_to, locations, locations, date_from, date_to, locations, locations,
                date_from, date_to, locations, locations, date_from, date_to, locations, locations,
                locations, locations, locations, locations,
                date_from, date_to, date_from, date_to, date_from, date_to, date_from, date_to,
                date_from, locations, locations, date_from, locations, locations,
                date_from, locations, locations, date_from, locations, locations,
                date_from, date_to, locations, locations, date_from, date_to, locations, locations,
                date_from, date_to, locations, locations, date_from, date_to, locations, locations,
                locations, locations, locations, locations,
                date_from, date_to, date_from, date_to, date_from, date_to, date_from, date_to,
                date_from, locations, locations, date_from, locations, locations,
                date_from, locations, locations, date_from, locations, locations,
                date_from, date_to, locations, locations, date_from, date_to, locations, locations,
                date_from, date_to, locations, locations, date_from, date_to, locations, locations,
                locations, locations, locations, locations,
                date_from,
                date_from, date_to, date_from, date_to, date_from, date_to, date_from, date_to,
                date_from, locations, locations, date_from, locations, locations,
                date_from, locations, locations, date_from, locations, locations,
                date_from, date_to, locations, locations, date_from, date_to, locations, locations,
                date_from, date_to, locations, locations, date_from, date_to, locations, locations,
                locations, locations, locations, locations
            ))
            lines = self.env.cr.dictfetchall()
        else:
            i = 1
            j = 0
            select_on_way = ""
            for location in locations:
                if self.group_by == 'by_location':
                    j = len(locations)
                    IN = " = %s"
                    NOT_IN = " != %s"
                else:
                    location = locations
                    IN = " IN %s"
                    NOT_IN = " NOT IN %s"
                    j = 1
                
                if self.show_residual:
                    select_on_way =  "CASE WHEN m.date BETWEEN '"+ str(date_from) +"' AND '"+str(date_to)+"' AND m.state = 'assigned' AND m.location_id NOT IN (" + ','.join(map(str, location)) + ") AND m.location_dest_id IN (" + ','.join(map(str, location)) + ") " \
                                     "THEN COALESCE(m.product_uom_qty/u.factor*u2.factor,0) ELSE 0 END AS on_way_qty, "
                if i <= j:
                    i += 1
                    
                    if zero_qty_zero_cost:
                        zero_qty_zero_cost_qry2 = " CASE WHEN SUM(l.start_qty) = 0 THEN SUM(l.cost - l.ex_cost) ELSE SUM(l.start_cost + l.cost - l.ex_cost) END AS end_cost "
                    else:
                        zero_qty_zero_cost_qry2 = " SUM(l.start_cost + l.cost - l.ex_cost) AS end_cost "
                    
                    self.env.cr.execute(
                        "SELECT l.list_price,l.prod_id AS pid, l.ptid as ptid, l.name AS name, l.code AS code, l.barcode as barcode, l.uom_name AS uom_name, l.worthy_balance AS worthy_balance,"
                        "l.cid AS cid, l.cname AS cname,l.size_name as size,l.size_id AS size_id, " + select + " "
                        "SUM(l.start_qty) AS start_qty, SUM(l.start_cost) AS start_cost, "
                        "SUM(l.qty) AS qty, SUM(l.cost) AS cost, "
                        "SUM(l.ex_qty) AS ex_qty, SUM(l.ex_cost) AS ex_cost, "
                        "SUM(l.start_qty + l.qty - l.ex_qty) AS end_qty,  " + zero_qty_zero_cost_qry2 + 
                        "FROM "
                        "( "
                        "SELECT pt.list_price as list_price,ps.name AS size_name,ps.id AS size_id,m.product_id AS prod_id, pt.id as ptid, pt.name AS name, pp.default_code AS code, pp.barcode as barcode,  "
                        " u2.name AS uom_name, pt.worthy_balance AS worthy_balance, pc.id AS cid, pc.name AS cname, " + sub_select + " "

                        "CASE WHEN " + where_done + " m.date < %s AND m.location_id " + NOT_IN + " AND m.location_dest_id" + IN + " "
                        "THEN COALESCE(m.product_uom_qty/u.factor*u2.factor,0) "
                        "WHEN " + where_done + " m.date < %s AND m.location_id" + IN + " AND m.location_dest_id " + NOT_IN + " "
                        "THEN -COALESCE(m.product_uom_qty/u.factor*u2.factor,0) ELSE 0 END AS start_qty, "
                        "CASE WHEN " + where_done + " m.date < %s AND m.location_id " + NOT_IN + " AND m.location_dest_id" + IN + " "
                        "THEN COALESCE(m.price_unit*m.product_uom_qty,0) "
                        "WHEN " + where_done + " m.date < %s AND m.location_id" + IN + " AND m.location_dest_id " + NOT_IN + " "
                        "THEN -COALESCE(m.price_unit*m.product_uom_qty,0) ELSE 0 END AS start_cost, "

                        "CASE WHEN " + where_done + " m.date BETWEEN %s AND %s AND m.location_id " + NOT_IN + " AND m.location_dest_id" + IN + " "
                        "THEN COALESCE(m.product_uom_qty/u.factor*u2.factor,0) ELSE 0 END AS qty, "
                        "CASE WHEN " + where_done + " m.date BETWEEN %s AND %s AND m.location_id " + NOT_IN + " AND m.location_dest_id" + IN + " "
                        "THEN COALESCE(m.price_unit*m.product_uom_qty,0) ELSE 0 END AS cost, " 
                        + select_on_way + 
                        "CASE WHEN " + where_done + " m.date BETWEEN %s AND %s AND m.location_id" + IN + " AND m.location_dest_id " + NOT_IN + " "
                        "THEN COALESCE(m.product_uom_qty/u.factor*u2.factor,0) ELSE 0 END AS ex_qty, "
                        "CASE WHEN " + where_done + " m.date BETWEEN %s AND %s AND m.location_id" + IN + " AND m.location_dest_id " + NOT_IN + " "
                        "THEN COALESCE(m.price_unit*m.product_uom_qty,0) ELSE 0 END AS ex_cost, "

                        "CASE WHEN m.location_id " + NOT_IN + " AND m.location_dest_id" + IN + " "
                        "THEN m.location_dest_id "
                        "WHEN m.location_id " + IN + " AND m.location_dest_id " + NOT_IN + " "
                        "THEN m.location_id ELSE 0 END AS lid "
                        "FROM stock_move m "
                        "LEFT JOIN product_product pp ON (pp.id=m.product_id) "
                        "LEFT JOIN product_template pt ON (pt.id=pp.product_tmpl_id) "
                        "LEFT JOIN product_size ps ON (ps.id = pt.size_id)"
                        "LEFT JOIN res_partner rp ON (pt.supplier_id = rp.id)"
                        "LEFT JOIN product_uom u ON (u.id=m.product_uom) "
                        "LEFT JOIN product_uom u2 ON (u2.id=pt.uom_id) "
                        "LEFT JOIN product_category pc ON (pt.categ_id = pc.id) " + sub_join + " "
                        "WHERE  " + where + ") AS l " + join + account_where + " "
                        '''GROUP BY
                            l.prod_id,
                            l.ptid,
                            l.name,
                            l.list_price,
                            l.code,
                            l.barcode,
                            l.uom_name,
                            l.cid,
                            l.cname,
                            l.size_id,
                            l.size_name,
                            l.worthy_balance ''' + group + having + " "
                        "ORDER BY " + order + "l.cname, l.name, l.size_name, l.code, l.worthy_balance, l.uom_name ",
                        (date_from, location, location, date_from, location, location,
                            date_from, location, location, date_from, location, location,
                            date_from, date_to, location, location, date_from, date_to, location, location,
                            date_from, date_to, location, location, date_from, date_to, location, location,
                            location, location, location, location))
                line = self.env.cr.dictfetchall()
                liness += line
            lines = liness
        reverse = False
        if self.show_order:
            reverse = False if self.order_by == 'asc' else True
            if self.show_cost:
                if self.column_selection == "initial_balance" and self.amount_selection == "cost":
                    lines = sorted(lines, key=lambda i: i['start_cost'], reverse=reverse)
                elif self.column_selection == "income" and self.amount_selection == "cost":
                    lines = sorted(lines, key=lambda i: i['cost'], reverse=reverse)
                elif self.column_selection == "expense" and self.amount_selection == "cost":
                    lines = sorted(lines, key=lambda i: i['ex_cost'], reverse=reverse)
                elif self.column_selection == "end_balance" and self.amount_selection == "cost":
                    lines = sorted(lines, key=lambda i: i['end_cost'], reverse=reverse)
            elif self.show_price:
                if self.column_selection == "initial_balance" and self.amount_selection == "price":
                    lines = sorted(lines, key=lambda i: i['list_price'] * i['start_qty'], reverse=reverse)
                elif self.column_selection == "income" and self.amount_selection == "price":
                    lines = sorted(lines, key=lambda i: i['list_price'] * i['qty'], reverse=reverse)
                elif self.column_selection == "expense" and self.amount_selection == "price":
                    lines = sorted(lines, key=lambda i: i['list_price'] * i['ex_qty'], reverse=reverse)
                elif self.column_selection == "end_balance" and self.amount_selection == "price":
                    lines = sorted(lines, key=lambda i: i['list_price'] * (i['start_qty'] + i['qty'] - i['ex_qty']), reverse=reverse)
            if self.column_selection == "initial_balance" and self.amount_selection == "quantity":
                lines = sorted(lines, key=lambda i: i['start_qty'], reverse=reverse)
            elif self.column_selection == "income" and self.amount_selection == "quantity":
                lines = sorted(lines, key=lambda i: i['qty'], reverse=reverse)
            elif self.column_selection == "expense" and self.amount_selection == "quantity":
                lines = sorted(lines, key=lambda i: i['ex_qty'], reverse=reverse)
            elif self.column_selection == "end_balance" and self.amount_selection == "quantity":
                lines = sorted(lines, key=lambda i: i['start_qty'] + i['qty'] - i['ex_qty'], reverse=reverse)
        if self.type == 'supplier':
            lines = sorted(lines, key=lambda i: i['supp_id'], reverse=reverse)

        categ_ids = []
        for l in lines:
            if l['cid'] not in categ_ids:
                categ_ids.append(l['cid'])

        categ_obj = self.env['product.category']
        categ_parent_dic = {}

        if self.group_by_parent:
            for c_id in categ_ids:
                categ = categ_obj.browse(c_id)
                while categ.parent_id:
                    categ = categ.parent_id
                categ_parent_dic[c_id] = {'id': categ.id, 'name': categ.name}

        where1 = ''
        if self.product_ids:
            product_ids = self.product_ids.ids
            where1 += ' AND product_id in (' + ','.join(str(p_id) for p_id in product_ids) + ') '

        if self.qty_on_hand:
            self.env.cr.execute("select location_id as location_id, product_id as pid, sum(qty) as total_qty from stock_quant where location_id in (" + ','.join(str(l_id) for l_id in locations) + ") " + where1 + " group by location_id, product_id")
            quant_lines = self.env.cr.dictfetchall()
            quant = {}
            for l in quant_lines:
                if l['location_id'] not in quant.keys():
                    quant[l['location_id']] = {}
                    quant[l['location_id']][l['pid']] = l['total_qty']
                else:
                    quant[l['location_id']][l['pid']] = l['total_qty']

        current_cost = {}
        if self.show_current_cost:
            if is_cost_for_each_wh.state == 'installed':
                # Хэрэв агуулах бүрээр өртөг тооцох модуль суусан бол агуулахын өртгийг одоогийн өртөг болгож хадгална.
                query = ("select product_id as pid, warehouse_id as warehouse_id, standard_price as cost \
                            from product_warehouse_standard_price where warehouse_id in (%s) " + where1) % (','.join(map(str, self.warehouse_ids.ids)))
                self.env.cr.execute(query)

                standard_price = self.env.cr.dictfetchall()

                for l in standard_price:
                    if l['pid'] not in current_cost.keys():
                        current_cost[l['pid']] = {}
                        current_cost[l['pid']][l['warehouse_id']] = l['cost']
                    else:
                        current_cost[l['pid']][l['warehouse_id']] = l['cost']
            else:
                # Хэрэв компани дундаа өртөг хөтөлж байгаа бол барааны өртгийг одоогийн өртөг болгож хадгална.
                query = ("select split_part(res_id, ',', 2)::int as pid, value_float as cost from ir_property where name='standard_price'")
                self.env.cr.execute(query)

                standard_price = self.env.cr.dictfetchall()

                for l in standard_price:
                    current_cost[l['pid']] = l['cost']
        # Барааны насжилт харах талбар сонгоход барааны насжилтыг олно.
        total_liness = []
        currday_of_month = ''
        if self.show_product_age:
            end_date = datetime.strptime(self.date_to, '%Y-%m-%d %H:%M:%S')
            i = 1
            j = 0
            for location in locations:
                if self.group_by == 'by_location':
                    j = len(locations)
                    IN = " = %s"
                    NOT_IN = " != %s"
                else:
                    if len(locations) > 0:
                        location = tuple(locations)
                    IN = " IN %s"
                    NOT_IN = " NOT IN %s"
                    j = 1
                if i <= j:  # Агуулахын байрлалаар бүлэглэхгүй тохиолдолд агуулахын бүх байрлалын хувьд барааг тооцох
                    i += 1
                    self.env.cr.execute("SELECT l.prod_id AS pid, sl.id as location_id, sl.name as lname, "
                                        "CASE WHEN sw.code is null THEN  sw1.code ElSE sw.code END AS wcode, "
                                        "CASE WHEN sw.name is null THEN  sw1.name ElSE sw.name END AS wname, "
                                        "SUM(l.start_qty + l.qty - l.ex_qty) AS end_qty "
                                        "FROM "
                                        "(SELECT m.product_id AS prod_id, "
                                        "CASE WHEN m.date < %s AND m.location_id  " + NOT_IN + "  AND m.location_dest_id " + IN + " THEN "
                                        "COALESCE(m.product_uom_qty/u.factor*u2.factor,0) WHEN m.date < %s AND m.location_id " + IN + " "
                                        "AND m.location_dest_id  " + NOT_IN + "  THEN -COALESCE(m.product_uom_qty/u.factor*u2.factor,0) ELSE 0 END AS start_qty, "
                                        "CASE WHEN m.date BETWEEN %s AND %s "
                                        "AND m.location_id  " + NOT_IN + "  AND m.location_dest_id " + IN + " THEN COALESCE(m.product_uom_qty/u.factor*u2.factor,0) ELSE 0 END AS qty, "
                                        "CASE WHEN m.date BETWEEN %s AND %s "
                                        "AND m.location_id " + IN + " AND m.location_dest_id  " + NOT_IN + "  THEN COALESCE(m.product_uom_qty/u.factor*u2.factor,0) ELSE 0 END AS ex_qty, "
                                        "CASE WHEN m.location_id  " + NOT_IN + "  AND m.location_dest_id " + IN + " THEN m.location_dest_id WHEN m.location_id " + IN + " AND m.location_dest_id  " + NOT_IN + "  THEN m.location_id ELSE 0 END AS lid "
                                        "FROM stock_move m "
                                        "LEFT JOIN product_product pp ON (pp.id=m.product_id) "
                                        "LEFT JOIN product_template pt ON (pt.id=pp.product_tmpl_id)"
                                        "LEFT JOIN product_uom u ON (u.id=m.product_uom) "
                                        "LEFT JOIN product_uom u2 ON (u2.id=pt.uom_id)  WHERE m.state = 'done') AS l "
                                        "LEFT JOIN stock_location sl ON (l.lid = sl.id)"
                                        "LEFT JOIN stock_location sl1 ON (sl.location_id = sl1.id) "
                                        "LEFT JOIN stock_location sl2 ON (sl1.location_id = sl2.id) "
                                        "LEFT JOIN stock_warehouse sw ON (sw.view_location_id = sl1.id) "
                                        "LEFT JOIN stock_warehouse sw1 ON (sw1.view_location_id = sl2.id) "
                                        "GROUP BY l.prod_id, sl.id, sw.code,  sw1.code ,sw.name , sw1.name ,sl.name "
                                        "HAVING ROUND(SUM(l.start_qty + l.qty - l.ex_qty)::decimal,4) <> 0 ",
                                        (date_from, location, location, date_from, location, location,
                                         date_from, date_to, location, location, date_from, date_to, location, location,
                                         location, location, location, location))

                    total_lines = self.env.cr.dictfetchall()
                    total_liness += total_lines
            product_age_line = {}
            for l in total_liness:
                qty = age_month = 0
                age_date = ''
                end_qty = l['end_qty']
                stock_moves = self.env['stock.move'].search([('date', '<=', date_to), ('location_dest_id', '=', l['location_id']), ('state', '=', 'done'), ('product_id', '=', l['pid'])], order='date desc')
                for move in stock_moves:
                    qty += move.product_qty
                    if qty >= end_qty and age_date == '':
                        age_date = move.date
                if age_date:
                    start_date = datetime.strptime(age_date, '%Y-%m-%d %H:%M:%S')
                    age_days = (end_date - start_date).days
                    age_month = round(float(age_days) / 30, 2)
                if l['location_id'] not in product_age_line.keys():
                    product_age_line[l['location_id']] = {}
                    product_age_line[l['location_id']][l['pid']] = age_month
                else:
                    product_age_line[l['location_id']][l['pid']] = age_month
        image_obj = {}
        if self.show_image:
            image_path = 'myimage.png'
            for line in lines:
                product_product = self.env['product.product'].search([('id', '=', line['pid'])])
                if not product_product.product_tmpl_id.image_medium:
                    image_obj[line['pid']] = False
                else:
                    product_image = str(product_product.product_tmpl_id.image_medium)
                    imgdata = base64.b64decode(product_image)
                    image_data = io.BytesIO(imgdata)
                    image_obj[line['pid']] = image_data

        if lines:
            type_id = False
            category_id = False
            location_id = False
            parent_category_id = False
            supplier_id = False
            size_id = False
            brand_id = False
            account_id = False
            supp_id = False
            warehouse_code = False
            seq = 1
            sub_seq = 1

            if self.show_cost:
                if self.type == 'warehouse' and self.group_by == 'by_category':
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            # Эхний агуулах/данс болон ангиллыг зурна
                            if not type_id and not category_id:
                                if self.group_by_parent:
                                    parent_total_start_qty = parent_total_qty = parent_total_ex_qty = parent_total_end_qty = 0
                                    parent_total_start_cost = parent_total_cost = parent_total_ex_cost = parent_total_unit_cost = parent_total_end_cost = 0
                                    col = self.get_main_col_count()
                                    sheet.merge_range(rowx, 0, rowx, col, categ_parent_dic[line['cid']]['name'], format_content_bold_left)
                                    parent_categ_row = rowx
                                    rowx += 1
                                    parent_category_id = categ_parent_dic[line['cid']]['id']
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['cname'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн агуулах/дансыг зурна
                            elif type_id and type_id != line['wcode']:
                                sub_seq = 1
                                seq = 1
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                                coly = self.get_all_col_count()
                                for h in range(col, coly + 1):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                warehouse_total_rows.append(rowx)
                                rowx += 1
                                if self.group_by_parent and parent_category_id and parent_category_id != categ_parent_dic[line['cid']]['id']:
                                    # Эцэг ангиллаар бүлэглэсэн тохиолдолд
                                    col_parent = self.get_main_col_count()
                                    sheet.write(parent_categ_row, col_parent + 1, parent_total_start_qty, format_title_float)
                                    sheet.write(parent_categ_row, col_parent + 2, parent_total_start_cost, format_title_float)
                                    sheet.write(parent_categ_row, col_parent + 3, parent_total_qty, format_title_float)
                                    sheet.write(parent_categ_row, col_parent + 4, parent_total_cost, format_title_float)
                                    sheet.write(parent_categ_row, col_parent + 5, parent_total_ex_qty, format_title_float)
                                    sheet.write(parent_categ_row, col_parent + 6, parent_total_ex_cost, format_title_float)
                                    sheet.write(parent_categ_row, col_parent + 7, parent_total_unit_cost, format_title_float)
                                    sheet.write(parent_categ_row, col_parent + 8, parent_total_end_qty, format_title_float)
                                    sheet.write(parent_categ_row, col_parent + 9, parent_total_end_cost, format_title_float)

                                    parent_total_start_qty = parent_total_qty = parent_total_ex_qty = parent_total_end_qty = 0
                                    parent_total_start_cost = parent_total_cost = parent_total_ex_cost = parent_total_unit_cost = parent_total_end_cost = 0
                                    sheet.merge_range(rowx, 0, rowx, col_parent, categ_parent_dic[line['cid']]['name'], format_content_bold_left)
                                    parent_categ_row = rowx
                                    rowx += 1
                                    parent_category_id = categ_parent_dic[line['cid']]['id']
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Warehouse Total'),
                                                  format_group_total)
                                for h in range(col, coly + 1):
                                    sum_rows = []
                                    for r in warehouse_total_rows:
                                        sum_rows.append('' + xl_rowcol_to_cell(r, h) + '')
                                    sheet.write_formula(rowx, h,
                                                        '{=SUM(' + ','.join(map(str, sum_rows)) + ')}', format_group_float)
                                rowx += 1
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                warehouse_total_rows = []
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['cname'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн ангиллыг зурна
                            elif category_id and category_id != line['cid']:
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                                coly = self.get_all_col_count()
                                for h in range(col, coly + 1):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                warehouse_total_rows.append(rowx)
                                rowx += 1
                                sub_seq += 1
                                if self.group_by_parent and parent_category_id and parent_category_id != categ_parent_dic[line['cid']]['id']:
                                    # Эцэг ангиллаар бүлэглэсэн тохиолдолд
                                    col_parent = self.get_main_col_count()
                                    sheet.write(parent_categ_row, col_parent + 1, parent_total_start_qty, format_title_float)
                                    sheet.write(parent_categ_row, col_parent + 2, parent_total_start_cost, format_title_float)
                                    sheet.write(parent_categ_row, col_parent + 3, parent_total_qty, format_title_float)
                                    sheet.write(parent_categ_row, col_parent + 4, parent_total_cost, format_title_float)
                                    sheet.write(parent_categ_row, col_parent + 5, parent_total_ex_qty, format_title_float)
                                    sheet.write(parent_categ_row, col_parent + 6, parent_total_ex_cost, format_title_float)
                                    sheet.write(parent_categ_row, col_parent + 7, parent_total_unit_cost, format_title_float)
                                    sheet.write(parent_categ_row, col_parent + 8, parent_total_end_qty, format_title_float)
                                    sheet.write(parent_categ_row, col_parent + 9, parent_total_end_cost, format_title_float)

                                    parent_total_start_qty = parent_total_qty = parent_total_ex_qty = parent_total_end_qty = 0
                                    parent_total_start_cost = parent_total_cost = parent_total_ex_cost = parent_total_unit_cost = parent_total_end_cost = 0
                                    sheet.merge_range(rowx, 0, rowx, col_parent, categ_parent_dic[line['cid']]['name'], format_content_bold_left)
                                    parent_categ_row = rowx
                                    rowx += 1
                                    parent_category_id = categ_parent_dic[line['cid']]['id']
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['cname'], format_content_bold_left)
                                rowd = rowx
                                rowx += 1
                                seq = 1

                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_start_cost += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost']
                            total_qty += line['qty']
                            total_cost += 0 if line['qty'] == 0  and zero_qty_zero_cost else line['cost']
                            total_ex_qty += line['ex_qty']
                            total_ex_cost += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost']
                            total_end_qty += line['end_qty']
                            total_end_cost += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost']
                            if self.show_reserved:
                                warehouse_reserved = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                total_warehouse_reserved_qty += warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                line_available_qty = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                total_available_warehouse_qty += line['end_qty'] - line_available_qty
                            if self.show_residual:
                                warehouse_residual = self.show_reserved_warehouse(line['pid'], line['lid'], 'c2')
                                show_reserved_qty_c1 = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                road_reserved_qty = self.show_reserved_warehouse(line['pid'], line['lid'], 'c3')
                                total_reserved_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                total_residual = warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_road += warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_residual_future += line['end_qty'] - total_reserved_c1 + total_residual
                                total_reserved_qty += road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_road_reserved = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_available_future += line['end_qty'] - total_reserved_c1 + total_residual - total_road_reserved

                            # Эцсийн үлдэгдэлийн нэгж өртөг зурах хэсэг
                            unit_cost = 0
                            if line['end_qty'] > 0:
                                unit_cost = abs(line['end_cost']) / abs(line['end_qty'])
                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            parent_total_start_qty += line['start_qty']
                            parent_total_qty += line['qty']
                            parent_total_ex_qty += line['ex_qty']
                            parent_total_end_qty += line['end_qty']
                            parent_total_start_cost += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost']
                            parent_total_cost += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost']
                            parent_total_unit_cost += unit_cost if self.env['product.product'].browse(line['pid']).cost_method != 'real' else 0
                            parent_total_ex_cost += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost']
                            parent_total_end_cost += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost']
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, unit_cost if self.env['product.product'].browse(line['pid']).cost_method != 'real' else 0, format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['end_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost'], format_content_float)
                            col += 1
                            if self.show_worthy_resources:
                                sheet.write(rowx, col, line['worthy_balance'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - line['worthy_balance'], format_content_float)
                                col += 1
                            if self.show_product_age:
                                product_age = product_age_line[line['lid']][line['pid']] if (line['lid'] in product_age_line.keys() and line['pid'] in product_age_line[line['lid']].keys()) else 0
                                sheet.write(rowx, col, product_age, format_content_float)
                                col += 1
                            if self.show_reserved:
                                sheet.write(rowx, col, warehouse_reserved[line['pid']] if warehouse_reserved else 0, format_content_float)
                                col += 1
                                show_reserved_total = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total, format_content_float)
                                col += 1
                            if self.show_residual:
                                sheet.write(rowx, col, warehouse_residual[line['pid']] if warehouse_residual else 0, format_content_float)
                                col += 1
                                show_reserved_total_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                show_residual_total = warehouse_residual[line['pid']] if warehouse_residual else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total, format_content_float)
                                col += 1
                                sheet.write(rowx, col, road_reserved_qty[line['pid']] if road_reserved_qty else 0, format_content_float)
                                col += 1
                                road_reserved_total = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total - road_reserved_total, format_content_float)
                                col += 1

                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                total_start_qty_on_hand += quant_qty
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                total_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                col += 1

                            if self.show_current_cost:
                                if is_cost_for_each_wh.state == 'installed':
                                    wh_id = warehouse_dic[line['lid']] if line['lid'] in warehouse_dic.keys() else None
                                    if wh_id:
                                        cost = current_cost[line['ptid']][wh_id] if (line['ptid'] in current_cost.keys() and wh_id in current_cost[line['ptid']].keys()) else 0
                                        sheet.write(rowx, col, cost if line['end_qty'] != 0 else 0, format_content_float)
                                        col += 1
                                        sheet.write(rowx, col, (unit_cost - cost) if line['end_qty'] != 0 else 0, format_content_float)
                                        col += 1
                                    else:
                                        sheet.write(rowx, col, 0, format_content_float)
                                        col += 1
                                        sheet.write(rowx, col, 0, format_content_float)
                                        col += 1
                                else:
                                    sheet.write(rowx, col, current_cost[line['pid']] if line['end_qty'] != 0 and line['pid'] in current_cost.keys() else 0, format_content_float)
                                    col += 1
                                    sheet.write(rowx, col, (unit_cost - current_cost[line['pid']]) if line['end_qty'] != 0 and line['pid'] in current_cost.keys() else 0, format_content_float)
                                    col += 1

                            rowx += 1
                            seq += 1
                            type_id = line['wcode']
                            category_id = line['cid']
                    col = self.get_main_col_count()
                    if self.group_by_parent:
                        sheet.write(parent_categ_row, col + 1, parent_total_start_qty, format_title_float)
                        sheet.write(parent_categ_row, col + 2, parent_total_start_cost, format_title_float)
                        sheet.write(parent_categ_row, col + 3, parent_total_qty, format_title_float)
                        sheet.write(parent_categ_row, col + 4, parent_total_cost, format_title_float)
                        sheet.write(parent_categ_row, col + 5, parent_total_ex_qty, format_title_float)
                        sheet.write(parent_categ_row, col + 6, parent_total_ex_cost, format_title_float)
                        sheet.write(parent_categ_row, col + 7, parent_total_unit_cost, format_title_float)
                        sheet.write(parent_categ_row, col + 8, parent_total_end_qty, format_title_float)
                        sheet.write(parent_categ_row, col + 9, parent_total_end_cost, format_title_float)
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                    coly = self.get_all_col_count()
                    for h in range(col, coly + 1):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    warehouse_total_rows.append(rowx)
                    rowx += 1
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Warehouse Total'),
                                      format_group_total)
                    for h in range(col, coly + 1):
                        sum_rows = []
                        for r in warehouse_total_rows:
                            sum_rows.append('' + xl_rowcol_to_cell(r, h) + '')
                        sheet.write_formula(rowx, h,
                                            '{=SUM(' + ','.join(map(str, sum_rows)) + ')}', format_group_float)
                    rowx += 1
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_start_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, 0, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_cost, format_title_float)
                    col += 1
                    if self.show_worthy_resources:
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                    if self.show_product_age:
                        sheet.write(rowx, col, ' ', format_title_float)
                        col += 1
                    if self.show_reserved:
                        sheet.write(rowx, col, total_warehouse_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_warehouse_qty, format_title_float)
                        col += 1
                    if self.show_residual:
                        sheet.write(rowx, col, total_warehouse_road, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_warehouse_residual_future, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_future, format_title_float)
                        col += 1
                    if self.qty_on_hand:
                        sheet.write(rowx, col, total_start_qty_on_hand, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_start_qty_on_hand_diff, format_title_float)
                        col += 1
                    if self.show_current_cost:
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    rowx += 1
                elif self.type == 'warehouse' and self.group_by == 'by_supplier':
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            # Эхний агуулах/данс болон ангиллыг зурна
                            if not type_id and not supp_id:
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['supp_name'] or u'Тодорхойгүй', format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн агуулах/дансыг зурна
                            elif type_id and type_id != line['wcode']:
                                sub_seq = 1
                                seq = 1
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                                coly = self.get_all_col_count()
                                for h in range(col, coly + 1):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                warehouse_total_rows.append(rowx)
                                rowx += 1
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col,
                                                  _('Warehouse Total'),
                                                  format_group_total)
                                for h in range(col, coly + 1):
                                    sum_rows = []
                                    for r in warehouse_total_rows:
                                        sum_rows.append('' + xl_rowcol_to_cell(r, h) + '')
                                    sheet.write_formula(rowx, h,
                                                        '{=SUM(' + ','.join(map(str, sum_rows)) + ')}', format_group_float)
                                rowx += 1
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                warehouse_total_rows = []
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['supp_name'] or u'Тодорхойгүй', format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн ангиллыг зурна
                            elif supp_id and supp_id != line['supp_id']:
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                                coly = self.get_all_col_count()
                                for h in range(col, coly + 1):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                warehouse_total_rows.append(rowx)
                                rowx += 1
                                sub_seq += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, coly, line['supp_name'] or u'Тодорхойгүй', format_content_bold_left)
                                rowd = rowx
                                rowx += 1
                                seq = 1
                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_start_cost += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost']
                            total_qty += line['qty']
                            total_cost += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost']
                            total_ex_qty += line['ex_qty']
                            total_ex_cost += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost']
                            total_end_qty += line['end_qty']
                            total_end_cost += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost']
                            if self.show_reserved:
                                warehouse_reserved = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                total_warehouse_reserved_qty += warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                line_available_qty = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                total_available_warehouse_qty += line['end_qty'] - line_available_qty
                            if self.show_residual:
                                warehouse_residual = self.show_reserved_warehouse(line['pid'], line['lid'], 'c2')
                                show_reserved_qty_c1 = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                road_reserved_qty = self.show_reserved_warehouse(line['pid'], line['lid'], 'c3')
                                total_reserved_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                total_residual = warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_road += warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_residual_future += line['end_qty'] - total_reserved_c1 + total_residual
                                total_reserved_qty += road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_road_reserved = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_available_future += line['end_qty'] - total_reserved_c1 + total_residual - total_road_reserved

                            # Эцсийн үлдэгдэлийн нэгж өртөг зурах хэсэг
                            unit_cost = 0
                            if line['end_qty'] > 0:
                                unit_cost = abs(line['end_cost']) / abs(line['end_qty'])
                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, unit_cost if self.env['product.product'].browse(
                                line['pid']).cost_method != 'real' else '', format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['end_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost'], format_content_float)
                            col += 1
                            if self.show_worthy_resources:
                                sheet.write(rowx, col, line['worthy_balance'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - line['worthy_balance'], format_content_float)
                                col += 1
                            if self.show_product_age:
                                product_age = product_age_line[line['lid']][line['pid']] if (line['lid'] in product_age_line.keys() and line['pid'] in product_age_line[line['lid']].keys()) else 0
                                sheet.write(rowx, col, product_age, format_content_float)
                                col += 1
                            if self.show_reserved:
                                sheet.write(rowx, col, warehouse_reserved[line['pid']] if warehouse_reserved else 0, format_content_float)
                                col += 1
                                show_reserved_total = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total, format_content_float)
                                col += 1
                            if self.show_residual:
                                sheet.write(rowx, col, warehouse_residual[line['pid']] if warehouse_residual else 0, format_content_float)
                                col += 1
                                show_reserved_total_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                show_residual_total = warehouse_residual[line['pid']] if warehouse_residual else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total, format_content_float)
                                col += 1
                                sheet.write(rowx, col, road_reserved_qty[line['pid']] if road_reserved_qty else 0, format_content_float)
                                col += 1
                                road_reserved_total = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total - road_reserved_total, format_content_float)
                                col += 1

                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                total_start_qty_on_hand += quant_qty
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                total_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                col += 1

                            if self.show_current_cost:
                                if is_cost_for_each_wh.state == 'installed':
                                    wh_id = warehouse_dic[line['lid']] if line['lid'] in warehouse_dic.keys() else None

                                    if wh_id:
                                        cost = current_cost[line['ptid']][wh_id] if (line['ptid'] in current_cost.keys() and wh_id in current_cost[line['ptid']].keys()) else 0
                                        sheet.write(rowx, col, cost if line['end_qty'] != 0 else 0, format_content_float)
                                        col += 1
                                        sheet.write(rowx, col, (unit_cost - cost) if line['end_qty'] != 0 else 0, format_content_float)
                                        col += 1
                                    else:
                                        sheet.write(rowx, col, 0, format_content_float)
                                        col += 1
                                        sheet.write(rowx, col, 0, format_content_float)
                                        col += 1
                                else:
                                    sheet.write(rowx, col, current_cost[line['pid']] if line['end_qty'] != 0 and line['pid'] in current_cost.keys() else 0, format_content_float)
                                    col += 1
                                    sheet.write(rowx, col, (unit_cost - current_cost[line['pid']]) if line['end_qty'] != 0 and line['pid'] in current_cost.keys() else 0, format_content_float)
                                    col += 1

                            rowx += 1
                            seq += 1
                            type_id = line['wcode']
                            supp_id = line['supp_id']

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                    coly = self.get_all_col_count()
                    for h in range(col, coly + 1):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    warehouse_total_rows.append(rowx)
                    rowx += 1
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Warehouse Total'),
                                      format_group_total)
                    for h in range(col, coly + 1):
                        sum_rows = []
                        for r in warehouse_total_rows:
                            sum_rows.append('' + xl_rowcol_to_cell(r, h) + '')
                        sheet.write_formula(rowx, h,
                                            '{=SUM(' + ','.join(map(str, sum_rows)) + ')}', format_group_float)
                    rowx += 1
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_start_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, 0, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_cost, format_title_float)
                    col += 1
                    if self.show_worthy_resources:
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                    if self.show_product_age:
                        sheet.write(rowx, col, ' ', format_title_float)
                        col += 1
                    if self.show_reserved:
                        sheet.write(rowx, col, total_warehouse_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_warehouse_qty, format_title_float)
                        col += 1
                    if self.show_residual:
                        sheet.write(rowx, col, total_warehouse_road, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_warehouse_residual_future, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_future, format_title_float)
                        col += 1
                    if self.qty_on_hand:
                        sheet.write(rowx, col, total_start_qty_on_hand, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_start_qty_on_hand_diff, format_title_float)
                        col += 1
                    if self.show_current_cost:
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1

                    rowx += 1
                elif self.type == 'category':
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            # Эхний агуулах/данс болон ангиллыг зурна
                            if not category_id:
                                col = (15 if self.show_size else 14) if self.group_by_parent else (14 if self.show_size else 13)
                                if self.show_image:
                                    col += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['cname'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн ангиллыг зурна
                            elif category_id and category_id != line['cid']:
                                col = (15 if self.show_size else 14) if self.group_by_parent else (14 if self.show_size else 13)
                                if self.show_image:
                                    col += 1
                                sub_seq += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['cname'], format_content_bold_left)
                                rowx += 1
                                seq = 1
                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_start_cost += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost']
                            total_qty += line['qty']
                            total_cost += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost']
                            total_ex_qty += line['ex_qty']
                            total_ex_cost += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost']
                            total_end_qty += line['end_qty']
                            total_end_cost += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost']
                            # Эцсийн үлдэгдэлийн нэгж өртөг зурах хэсэг
                            unit_cost = 0
                            if line['end_qty'] > 0:
                                unit_cost = abs(line['end_cost']) / abs(line['end_qty'])
                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, unit_cost if self.env['product.product'].browse(
                                line['pid']).cost_method != 'real' else '', format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['end_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost'], format_content_float)
                            col += 1

                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                total_start_qty_on_hand += quant_qty
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                total_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                col += 1

                            if self.show_current_cost:
                                wh_id = warehouse_dic[line['lid']] if line['lid'] in warehouse_dic.keys() else None

                                if wh_id:
                                    cost = current_cost[line['ptid']][wh_id] if (line['ptid'] in current_cost.keys() and wh_id in current_cost[line['ptid']].keys()) else 0
                                    sheet.write(rowx, col, cost if line['end_qty'] != 0 else 0, format_content_float)
                                    col += 1
                                    sheet.write(rowx, col, (unit_cost - cost) if line['end_qty'] != 0 else 0, format_content_float)
                                    col += 1

                            rowx += 1
                            seq += 1
                            category_id = line['cid']

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                    col = self.get_add_col_count(col)
                    for h in range(col, col + 10):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    rowx += 1

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_start_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, 0, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_cost, format_title_float)
                    col += 1
                    rowx += 1
                    # РАЗМЕРААР БҮЛЭГЛЭХ - өртөгтэй үед
                elif self.type == 'warehouse' and self.group_by == 'by_size':
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            if not type_id and not category_id:
                                # Эхний агуулах/размер болон ангиллыг зурна
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['size'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            elif type_id and type_id != line['wcode']:
                                # Дараагийн агуулах/размер зурна
                                sub_seq = 1
                                seq = 1
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Warehouse Total'), format_group_total)
                                coly = self.get_all_col_count()
                                for h in range(col, coly + 1):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowx += 1
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['size'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн ангиллыг зурна
                            elif size_id and size_id != line['size_id']:
                                col = self.get_all_col_count()
                                sub_seq += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['size'], format_content_bold_left)
                                rowx += 1
                                seq = 1

                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_start_cost += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost']
                            total_qty += line['qty']
                            total_cost += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost']
                            total_ex_qty += line['ex_qty']
                            total_ex_cost += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost']
                            total_end_qty += line['end_qty']
                            total_end_cost += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost']
                            if self.show_reserved:
                                warehouse_reserved = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                total_warehouse_reserved_qty += warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                line_available_qty = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                total_available_warehouse_qty += line['end_qty'] - line_available_qty
                            if self.show_residual:
                                warehouse_residual = self.show_reserved_warehouse(line['pid'], line['lid'], 'c2')
                                show_reserved_qty_c1 = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                road_reserved_qty = self.show_reserved_warehouse(line['pid'], line['lid'], 'c3')
                                total_reserved_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                total_residual = warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_road += warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_residual_future += line['end_qty'] - total_reserved_c1 + total_residual
                                total_reserved_qty += road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_road_reserved = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_available_future += line['end_qty'] - total_reserved_c1 + total_residual - total_road_reserved
                            # Эцсийн үлдэгдэлийн нэгж өртөг зурах хэсэг
                            unit_cost = 0
                            if line['end_qty'] > 0:
                                unit_cost = abs(line['end_cost']) / abs(line['end_qty'])
                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, unit_cost if self.env['product.product'].browse(
                                line['pid']).cost_method != 'real' else '', format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['end_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost'], format_content_float)
                            col += 1
                            if self.show_worthy_resources:
                                sheet.write(rowx, col, line['worthy_balance'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - line['worthy_balance'], format_content_float)
                                col += 1
                            if self.show_product_age:
                                product_age = product_age_line[line['lid']][line['pid']] if (line['lid'] in product_age_line.keys() and line['pid'] in product_age_line[line['lid']].keys()) else 0
                                sheet.write(rowx, col, product_age, format_content_float)
                                col += 1
                            if self.show_reserved:
                                sheet.write(rowx, col, warehouse_reserved[line['pid']] if warehouse_reserved else 0, format_content_float)
                                col += 1
                                show_reserved_total = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total, format_content_float)
                                col += 1
                            if self.show_residual:
                                sheet.write(rowx, col, warehouse_residual[line['pid']] if warehouse_residual else 0, format_content_float)
                                col += 1
                                show_reserved_total_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                show_residual_total = warehouse_residual[line['pid']] if warehouse_residual else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total, format_content_float)
                                col += 1
                                sheet.write(rowx, col, road_reserved_qty[line['pid']] if road_reserved_qty else 0, format_content_float)
                                col += 1
                                road_reserved_total = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total - road_reserved_total, format_content_float)
                                col += 1

                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                total_start_qty_on_hand += quant_qty
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                total_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                col += 1

                            if self.show_current_cost:
                                if is_cost_for_each_wh.state == 'installed':
                                    wh_id = warehouse_dic[line['lid']] if line['lid'] in warehouse_dic.keys() else None

                                    if wh_id:
                                        cost = current_cost[line['ptid']][wh_id] if (line['ptid'] in current_cost.keys() and wh_id in current_cost[line['ptid']].keys()) else 0
                                        sheet.write(rowx, col, cost if line['end_qty'] != 0 else 0, format_content_float)
                                        col += 1
                                        sheet.write(rowx, col, (unit_cost - cost) if line['end_qty'] != 0 else 0, format_content_float)
                                        col += 1
                                    else:
                                        sheet.write(rowx, col, 0, format_content_float)
                                        col += 1
                                        sheet.write(rowx, col, 0, format_content_float)
                                        col += 1
                                else:
                                    sheet.write(rowx, col, current_cost[line['pid']] if line['end_qty'] != 0 and line['pid'] in current_cost.keys() else 0, format_content_float)
                                    col += 1
                                    sheet.write(rowx, col, (unit_cost - current_cost[line['pid']]) if line['end_qty'] != 0 and line['pid'] in current_cost.keys() else 0, format_content_float)
                                    col += 1

                            rowx += 1
                            seq += 1
                            type_id = line['wcode']
                            size_id = line['size_id']

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Warehouse Total'), format_group_total)
                    coly = self.get_all_col_count()
                    for h in range(col, coly + 1):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    rowx += 1
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_start_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, 0, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_cost, format_title_float)
                    col += 1
                    if self.show_worthy_resources:
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                    if self.show_product_age:
                        sheet.write(rowx, col, ' ', format_title_float)
                        col += 1
                    if self.show_reserved:
                        sheet.write(rowx, col, total_warehouse_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_warehouse_qty, format_title_float)
                        col += 1
                    if self.show_residual:
                        sheet.write(rowx, col, total_warehouse_road, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_warehouse_residual_future, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_future, format_title_float)
                        col += 1
                    if self.qty_on_hand:
                        sheet.write(rowx, col, total_start_qty_on_hand, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_start_qty_on_hand_diff, format_title_float)
                        col += 1

                    if self.show_current_cost:
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    rowx += 1
                elif self.type == 'warehouse' and self.group_by == 'by_account':
                    for line in lines:
                        account_name = line['account_code'] + ' ' + line['account_name']
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            # Эхний агуулах/данс болон ангиллыг зурна
                            if not type_id and not account_id:
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = self.get_all_col_count()
                                if self.show_image:
                                    col += 1
                                sheet.merge_range(rowx, 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 1, rowx, col, account_name, format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн агуулах/данс зурна
                            elif type_id and type_id != line['wcode']:
                                sub_seq = 1
                                seq = 1

                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1, rowx, col, _('Subtotal'), format_group_total)
                                coly = self.get_all_col_count()
                                for h in range(col, coly+1):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                warehouse_total_rows.append(rowx)
                                rowx += 1
                                sheet.merge_range(rowx, 1, rowx, col, _('Warehouse Total'), format_group_total)
                                for h in range(col, coly + 1):
                                    sum_rows = []
                                    for r in warehouse_total_rows:
                                        sum_rows.append('' + xl_rowcol_to_cell(r, h) + '')
                                    sheet.write_formula(rowx, h,
                                                        '{=SUM(' + ','.join(map(str, sum_rows)) + ')}',
                                                        format_group_float)
                                rowx += 1
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 1, rowx, col, account_name, format_content_bold_left)

                                rowx += 1
                                rowd = rowx
                            # Дараагийн ангиллыг зурна
                            elif account_id and account_id != line['account_id']:
                                col = self.get_main_col_count()
                                if self.show_image:
                                    col += 1
                                sheet.merge_range(rowx, 1 , rowx, col, _('Subtotal'), format_group_total)
                                coly = self.get_all_col_count()
                                for h in range(col, coly+1):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                warehouse_total_rows.append(rowx)
                                rowx += 1
                                sub_seq += 1
                                sheet.write(rowx, 0, sub_seq, format_content_bold_number)
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1, rowx, col, account_name, format_content_bold_left)
                                rowd = rowx
                                rowx += 1
                                seq = 1

                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_start_cost += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost']
                            total_qty += line['qty']
                            total_cost += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost']
                            total_ex_qty += line['ex_qty']
                            total_ex_cost += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost']
                            total_end_qty += line['end_qty']
                            total_end_cost += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost']

                            if self.show_reserved:
                                warehouse_reserved = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                total_warehouse_reserved_qty += warehouse_reserved[
                                    line['pid']] if warehouse_reserved else 0
                                line_available_qty = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                total_available_warehouse_qty += line['end_qty'] - line_available_qty
                            if self.show_residual:
                                warehouse_residual = self.show_reserved_warehouse(line['pid'], line['lid'], 'c2')
                                show_reserved_qty_c1 = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                road_reserved_qty = self.show_reserved_warehouse(line['pid'], line['lid'], 'c3')
                                total_reserved_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                total_residual = warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_road += warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_residual_future += line['end_qty'] - total_reserved_c1 + total_residual
                                total_reserved_qty += road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_road_reserved = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_available_future += line[
                                                              'end_qty'] - total_reserved_c1 + total_residual - total_road_reserved

                            # Эцсийн үлдэгдэлийн нэгж өртөг зурах хэсэг
                            unit_cost = 0
                            if line['end_qty'] > 0:
                                unit_cost = abs(line['end_cost']) / abs(line['end_qty'])
                            # Барааны мөрийг зурна
                            col = 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, unit_cost if self.env['product.product'].browse(line['pid']).cost_method != 'real' else '', format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['end_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost'], format_content_float)
                            col += 1
                            if self.show_worthy_resources:
                                sheet.write(rowx, col, line['worthy_balance'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - line['worthy_balance'], format_content_float)
                                col += 1
                            if self.show_product_age:
                                product_age = product_age_line[line['lid']][line['pid']] if (
                                            line['lid'] in product_age_line.keys() and line['pid'] in product_age_line[
                                        line['lid']].keys()) else 0
                                sheet.write(rowx, col, product_age, format_content_float)
                                col += 1
                            if self.show_reserved:
                                sheet.write(rowx, col, warehouse_reserved[line['pid']] if warehouse_reserved else 0,
                                            format_content_float)
                                col += 1
                                show_reserved_total = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total, format_content_float)
                                col += 1
                            if self.show_residual:
                                sheet.write(rowx, col, warehouse_residual[line['pid']] if warehouse_residual else 0,
                                            format_content_float)
                                col += 1
                                show_reserved_total_c1 = show_reserved_qty_c1[
                                    line['pid']] if show_reserved_qty_c1 else 0
                                show_residual_total = warehouse_residual[line['pid']] if warehouse_residual else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total,
                                            format_content_float)
                                col += 1
                                sheet.write(rowx, col, road_reserved_qty[line['pid']] if road_reserved_qty else 0,
                                            format_content_float)
                                col += 1
                                road_reserved_total = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                sheet.write(rowx, col, line[
                                    'end_qty'] - show_reserved_total_c1 + show_residual_total - road_reserved_total,
                                            format_content_float)
                                col += 1
                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                total_start_qty_on_hand += quant_qty
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                total_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                col += 1

                            if self.show_current_cost:
                                wh_id = warehouse_dic[line['lid']] if line['lid'] in warehouse_dic.keys() else None

                                if wh_id:
                                    cost = current_cost[line['ptid']][wh_id] if (line['ptid'] in current_cost.keys() and wh_id in current_cost[line['ptid']].keys()) else 0
                                    sheet.write(rowx, col, cost if line['end_qty'] != 0 else 0, format_content_float)
                                    col += 1
                                    sheet.write(rowx, col, (unit_cost - cost) if line['end_qty'] != 0 else 0, format_content_float)
                                    col += 1

                            rowx += 1
                            seq += 1
                            type_id = line['wcode']
                            account_id = line['account_id']

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1, rowx, col, _('Subtotal'), format_group_total)
                    coly = self.get_all_col_count()
                    for h in range(col, coly+1):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    warehouse_total_rows.append(rowx)
                    rowx += 1
                    sheet.merge_range(rowx, 1, rowx, col, _('Warehouse Total'), format_group_total)
                    for h in range(col, coly + 1):
                        sum_rows = []
                        for r in warehouse_total_rows:
                            sum_rows.append('' + xl_rowcol_to_cell(r, h) + '')
                        sheet.write_formula(rowx, h,
                                            '{=SUM(' + ','.join(map(str, sum_rows)) + ')}', format_group_float)
                    rowx += 1
                    sheet.merge_range(rowx, 1 , rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_start_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, 0, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_cost, format_title_float)
                    col += 1
                    if self.show_worthy_resources:
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                    if self.show_product_age:
                        sheet.write(rowx, col, ' ', format_title_float)
                        col += 1
                    if self.show_reserved:
                        sheet.write(rowx, col, total_warehouse_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_warehouse_qty, format_title_float)
                        col += 1
                    if self.show_residual:
                        sheet.write(rowx, col, total_warehouse_road, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_warehouse_residual_future, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_future, format_title_float)
                        col += 1
                    if self.qty_on_hand:
                        sheet.write(rowx, col, total_start_qty_on_hand, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_start_qty_on_hand_diff, format_title_float)
                        col += 1
                    if self.show_current_cost:
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    rowx += 1
                    # АГУУЛАХ -> БАЙРЛАЛААР БҮЛЭГЛЭХ - өртөгтэй үед
                elif self.type == 'warehouse' and self.group_by == 'by_location':
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        sl_with_user_lang = self.env['stock.location'].with_context(lang=self.env.user.lang).browse(line['lid'])
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            if not type_id and not location_id:
                                # Эхний агуулах болон байрлалыг зурна
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 1, rowx, col, sl_with_user_lang.name if pt_with_user_lang else line['lname'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            elif type_id and type_id != line['wcode']:
                                # Дараагийн байрлалыг зурна
                                sub_seq = 1
                                seq = 1
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 0, rowx, col, _('Sub Total'), format_group_total)
                                coly = self.get_all_col_count()
                                for h in range(col, coly + 1):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowx += 1
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 1, rowx, col, sl_with_user_lang.name if pt_with_user_lang else line['lname'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн байрлалыг зурна
                            elif location_id and location_id != line['lid']:
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 0, rowx, col, _('Sub Total'), format_group_total)
                                coly = self.get_all_col_count()
                                for h in range(col, coly + 1):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowx += 1
                                sub_seq += 1
                                sheet.write(rowx, 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 1, rowx, coly, sl_with_user_lang.name if pt_with_user_lang else line['lname'], format_content_bold_left)
                                rowd = rowx
                                rowx += 1
                                seq = 1
                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_start_cost += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost']
                            total_qty += line['qty']
                            total_cost += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost']
                            total_ex_qty += line['ex_qty']
                            total_ex_cost += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost']
                            total_end_qty += line['end_qty']
                            total_end_cost += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost']
                            if self.show_reserved:
                                warehouse_reserved = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                total_warehouse_reserved_qty += warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                line_available_qty = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                total_available_warehouse_qty += line['end_qty'] - line_available_qty
                            if self.show_residual:
                                warehouse_residual = self.show_reserved_warehouse(line['pid'], line['lid'], 'c2')
                                show_reserved_qty_c1 = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                road_reserved_qty = self.show_reserved_warehouse(line['pid'], line['lid'], 'c3')
                                total_reserved_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                total_residual = warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_road += warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_residual_future += line['end_qty'] - total_reserved_c1 + total_residual
                                total_reserved_qty += road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_road_reserved = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_available_future += line['end_qty'] - total_reserved_c1 + total_residual - total_road_reserved

                            # Эцсийн үлдэгдэлийн нэгж өртөг зурах хэсэг
                            unit_cost = 0
                            if line['end_qty'] > 0:
                                unit_cost = abs(line['end_cost']) / abs(line['end_qty'])
                            # Барааны мөрийг зурна
                            col = 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, unit_cost if self.env['product.product'].browse(
                                line['pid']).cost_method != 'real' else '', format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['end_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost'], format_content_float)
                            col += 1
                            if self.show_worthy_resources:
                                sheet.write(rowx, col, line['worthy_balance'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - line['worthy_balance'], format_content_float)
                                col += 1
                            if self.show_product_age:
                                product_age = product_age_line[line['lid']][line['pid']] if (line['lid'] in product_age_line.keys() and line['pid'] in product_age_line[line['lid']].keys()) else 0
                                sheet.write(rowx, col, product_age, format_content_float)
                                col += 1
                            if self.show_reserved:
                                sheet.write(rowx, col, warehouse_reserved[line['pid']] if warehouse_reserved else 0, format_content_float)
                                col += 1
                                show_reserved_total = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total, format_content_float)
                                col += 1
                            if self.show_residual:
                                sheet.write(rowx, col, warehouse_residual[line['pid']] if warehouse_residual else 0, format_content_float)
                                col += 1
                                show_reserved_total_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                show_residual_total = warehouse_residual[line['pid']] if warehouse_residual else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total, format_content_float)
                                col += 1
                                sheet.write(rowx, col, road_reserved_qty[line['pid']] if road_reserved_qty else 0, format_content_float)
                                col += 1
                                road_reserved_total = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total - road_reserved_total, format_content_float)
                                col += 1

                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                total_start_qty_on_hand += quant_qty
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                total_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                col += 1

                            if self.show_current_cost:
                                if is_cost_for_each_wh.state == 'installed':
                                    wh_id = warehouse_dic[line['lid']] if line['lid'] in warehouse_dic.keys() else None

                                    if wh_id:
                                        cost = current_cost[line['ptid']][wh_id] if (line['ptid'] in current_cost.keys() and wh_id in current_cost[line['ptid']].keys()) else 0
                                        sheet.write(rowx, col, cost if line['end_qty'] != 0 else 0, format_content_float)
                                        col += 1
                                        sheet.write(rowx, col, (unit_cost - cost) if line['end_qty'] != 0 else 0, format_content_float)
                                        col += 1
                                    else:
                                        sheet.write(rowx, col, 0, format_content_float)
                                        col += 1
                                        sheet.write(rowx, col, 0, format_content_float)
                                        col += 1
                                else:
                                    sheet.write(rowx, col, current_cost[line['pid']] if line['end_qty'] != 0 and line['pid'] in current_cost.keys() else 0, format_content_float)
                                    col += 1
                                    sheet.write(rowx, col, (unit_cost - current_cost[line['pid']]) if line['end_qty'] != 0 and line['pid'] in current_cost.keys() else 0, format_content_float)
                                    col += 1

                            rowx += 1
                            seq += 1
                            type_id = line['wcode']
                            location_id = line['lid']

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 0, rowx, col, _('Sub Total'), format_group_total)
                    coly = self.get_add_col_count(col)
                    for h in range(col, coly + 10):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    rowx += 1
                    sheet.merge_range(rowx, 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_start_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, 0, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_cost, format_title_float)
                    col += 1
                    if self.show_worthy_resources:
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                    if self.show_product_age:
                        sheet.write(rowx, col, ' ', format_title_float)
                        col += 1
                    if self.show_reserved:
                        sheet.write(rowx, col, total_warehouse_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_warehouse_qty, format_title_float)
                        col += 1
                    if self.show_residual:
                        sheet.write(rowx, col, total_warehouse_road, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_warehouse_residual_future, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_future, format_title_float)
                        col += 1
                    if self.qty_on_hand:
                        sheet.write(rowx, col, total_start_qty_on_hand, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_start_qty_on_hand_diff, format_title_float)
                        col += 1
                    if self.show_current_cost:
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    rowx += 1
                    # ДАНС -> АНГИЛЛААР БҮЛЭГЛЭХ - өртөгтэй үед
                elif self.type == 'account':
                    subtotal_start_qty = 0
                    subtotal_start_cost = 0
                    subtotal_qty = 0
                    subtotal_cost = 0
                    subtotal_ex_qty = 0
                    subtotal_ex_cost = 0
                    subtotal_end_qty = 0
                    subtotal_end_cost = 0
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            # Эхний данс болон ангиллыг зурна
                            if not type_id and not category_id:
                                account_code = '[' + line['account_code'] + '] ' if line['account_code'] else ''
                                account_name = line['account_name'] if line['account_name'] else ''
                                name = account_code + account_name
                                col = (15 if self.show_size else 14) if self.group_by_parent else (14 if self.show_size else 13)
                                if self.show_image:
                                    col += 1
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['account_name'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['cname'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн дансыг зурна
                            elif type_id and type_id != line['account_code']:
                                sub_seq = 1
                                seq = 1

                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Subtotal'), format_group_total)
                                for h in range(col, col + 10):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowx += 1
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Amount'), format_group_total)
                                col += 1
                                sheet.write(rowx, col, subtotal_start_qty, format_title_float)
                                col += 1
                                sheet.write(rowx, col, subtotal_start_cost, format_title_float)
                                col += 1
                                sheet.write(rowx, col, subtotal_qty, format_title_float)
                                col += 1
                                sheet.write(rowx, col, subtotal_cost, format_title_float)
                                col += 1
                                sheet.write(rowx, col, subtotal_ex_qty, format_title_float)
                                col += 1
                                sheet.write(rowx, col, subtotal_ex_cost, format_title_float)
                                col += 1
                                sheet.write(rowx, col, 0, format_title_float)
                                col += 1
                                sheet.write(rowx, col, subtotal_end_qty, format_title_float)
                                col += 1
                                sheet.write(rowx, col, subtotal_end_cost, format_title_float)
                                col += 1
                                rowx += 1
                                account_code = '[' + line['account_code'] + '] ' if line['account_code'] else ''
                                account_name = line['account_name'] if line['account_name'] else ''
                                name = account_code + account_name
                                col = (15 if self.show_size else 14) if self.group_by_parent else (14 if self.show_size else 13)
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['account_name'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['cname'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                                subtotal_start_qty = 0
                                subtotal_start_cost = 0
                                subtotal_qty = 0
                                subtotal_cost = 0
                                subtotal_ex_qty = 0
                                subtotal_ex_cost = 0
                                subtotal_end_qty = 0
                                subtotal_end_cost = 0
                            # Дараагийн ангиллыг зурна
                            elif category_id and category_id != line['cid']:
                                col = (6 if self.show_size else 5) if self.group_by_parent else (5 if self.show_size else 4)
                                if self.show_image:
                                    col += 1
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Subtotal'), format_group_total)
                                for h in range(col, 15 if self.show_size else 14):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowx += 1
                                col = (15 if self.show_size else 14) if self.group_by_parent else (14 if self.show_size else 13)
                                sub_seq += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['cname'], format_content_bold_left)
                                rowd = rowx
                                rowx += 1
                                seq = 1
                            # Дэд дүнг олно
                            subtotal_start_qty += line['start_qty']
                            subtotal_start_cost += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['start_qty']
                            subtotal_qty += line['qty']
                            subtotal_cost += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['qty']
                            subtotal_ex_qty += line['ex_qty']
                            subtotal_ex_cost += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['ex_qty']
                            subtotal_end_qty += line['end_qty']
                            subtotal_end_cost += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty']
                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_start_cost += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost']
                            total_qty += line['qty']
                            total_cost += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost']
                            total_ex_qty += line['ex_qty']
                            total_ex_cost += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost']
                            total_end_qty += line['end_qty']
                            total_end_cost += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost']
                            # Эцсийн үлдэгдэлийн нэгж өртөг зурах хэсэг
                            unit_cost = 0
                            if line['end_qty'] > 0:
                                unit_cost = abs(line['end_cost']) / abs(line['end_qty'])
                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, unit_cost if self.env['product.product'].browse(line['pid']).cost_method != 'real' else '', format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['end_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost'], format_content_float)
                            col += 1

                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                total_start_qty_on_hand += quant_qty
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                total_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                col += 1

                            if self.show_current_cost:
                                if is_cost_for_each_wh.state == 'installed':
                                    wh_id = warehouse_dic[line['lid']] if line['lid'] in warehouse_dic.keys() else None

                                    if wh_id:
                                        cost = current_cost[line['ptid']][wh_id] if (line['ptid'] in current_cost.keys() and wh_id in current_cost[line['ptid']].keys()) else 0
                                        sheet.write(rowx, col, cost if line['end_qty'] != 0 else 0, format_content_float)
                                        col += 1
                                        sheet.write(rowx, col, (unit_cost - cost) if line['end_qty'] != 0 else 0, format_content_float)
                                        col += 1
                                    else:
                                        sheet.write(rowx, col, 0, format_content_float)
                                        col += 1
                                        sheet.write(rowx, col, 0, format_content_float)
                                        col += 1
                                else:
                                    sheet.write(rowx, col, current_cost[line['pid']] if line['end_qty'] != 0 and line['pid'] in current_cost.keys() else 0, format_content_float)
                                    col += 1
                                    sheet.write(rowx, col, (unit_cost - current_cost[line['pid']]) if line['end_qty'] != 0 and line['pid'] in current_cost.keys() else 0, format_content_float)
                                    col += 1

                            rowx += 1
                            seq += 1
                            type_id = line['account_code']
                            category_id = line['cid']

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Subtotal'), format_group_total)
                    for h in range(col, col + 10):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    rowx += 1
                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_start_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, 0, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_cost, format_title_float)
                    col += 1
                    rowx += 1
                elif self.type == 'warehouse' and self.group_by == 'by_brand':
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            # Эхний агуулах болон брэндийг зурна
                            if not type_id and not brand_id:
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                if line['brand_name']:
                                    brand_name = line['brand_name']
                                else:
                                    brand_name = _('Not defined')
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, brand_name, format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн агуулах болон брэндийг зурна
                            elif type_id and type_id != line['wcode']:
                                sub_seq = 1
                                seq = 1

                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                                for h in range(col, col + self.get_remain_col_count()):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                warehouse_total_rows.append(rowx)
                                rowx += 1
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col,
                                                  _('Warehouse Total'),
                                                  format_group_total)
                                for h in range(col, col + self.get_remain_col_count()):
                                    sum_rows = []
                                    for r in warehouse_total_rows:
                                        sum_rows.append('' + xl_rowcol_to_cell(r, h) + '')
                                    sheet.write_formula(rowx, h,
                                                        '{=SUM(' + ','.join(map(str, sum_rows)) + ')}',
                                                        format_group_float)
                                rowx += 1
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                warehouse_total_rows = []
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                if line['brand_name']:
                                    brand_name = line['brand_name']
                                else:
                                    brand_name = _('Not defined')
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, brand_name, format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн брэндийг зурна
                            elif brand_id and brand_id != line['brand_id']:
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                                for h in range(col, col + self.get_remain_col_count()):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                warehouse_total_rows.append(rowx)
                                rowx += 1

                                col = self.get_all_col_count()
                                sub_seq += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                if line['brand_name']:
                                    brand_name = line['brand_name']
                                else:
                                    brand_name = _('Not defined')
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, brand_name, format_content_bold_left)
                                rowd = rowx
                                rowx += 1
                                seq = 1
                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_start_cost += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost']
                            total_qty += line['qty']
                            total_cost += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost']
                            total_ex_qty += line['ex_qty']
                            total_ex_cost += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost']
                            total_end_qty += line['end_qty']
                            total_end_cost += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost']
                            if self.show_reserved:
                                warehouse_reserved = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                total_warehouse_reserved_qty += warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                line_available_qty = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                total_available_warehouse_qty += line['end_qty'] - line_available_qty
                            if self.show_residual:
                                warehouse_residual = self.show_reserved_warehouse(line['pid'], line['lid'], 'c2')
                                show_reserved_qty_c1 = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                road_reserved_qty = self.show_reserved_warehouse(line['pid'], line['lid'], 'c3')
                                total_reserved_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                total_residual = warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_road += warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_residual_future += line['end_qty'] - total_reserved_c1 + total_residual
                                total_reserved_qty += road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_road_reserved = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_available_future += line['end_qty'] - total_reserved_c1 + total_residual - total_road_reserved

                            # Эцсийн үлдэгдэлийн нэгж өртөг зурах хэсэг
                            unit_cost = 0
                            if line['end_qty'] > 0:
                                unit_cost = abs(line['end_cost']) / abs(line['end_qty'])
                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, unit_cost if self.env['product.product'].browse(
                                line['pid']).cost_method != 'real' else '', format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['end_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost'], format_content_float)
                            col += 1
                            if self.show_worthy_resources:
                                sheet.write(rowx, col, line['worthy_balance'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - line['worthy_balance'], format_content_float)
                                col += 1
                            if self.show_product_age:
                                product_age = product_age_line[line['lid']][line['pid']] if (line['lid'] in product_age_line.keys() and line['pid'] in product_age_line[line['lid']].keys()) else 0
                                sheet.write(rowx, col, product_age, format_content_float)
                                col += 1
                            if self.show_reserved:
                                sheet.write(rowx, col, warehouse_reserved[line['pid']] if warehouse_reserved else 0, format_content_float)
                                col += 1
                                show_reserved_total = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total, format_content_float)
                                col += 1
                            if self.show_residual:
                                sheet.write(rowx, col, warehouse_residual[line['pid']] if warehouse_residual else 0, format_content_float)
                                col += 1
                                show_reserved_total_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                show_residual_total = warehouse_residual[line['pid']] if warehouse_residual else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total, format_content_float)
                                col += 1
                                sheet.write(rowx, col, road_reserved_qty[line['pid']] if road_reserved_qty else 0, format_content_float)
                                col += 1
                                road_reserved_total = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total - road_reserved_total, format_content_float)
                                col += 1

                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                total_start_qty_on_hand += quant_qty
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                total_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                col += 1

                            if self.show_current_cost:
                                if is_cost_for_each_wh.state == 'installed':
                                    wh_id = warehouse_dic[line['lid']] if line['lid'] in warehouse_dic.keys() else None

                                    if wh_id:
                                        cost = current_cost[line['ptid']][wh_id] if (line['ptid'] in current_cost.keys() and wh_id in current_cost[line['ptid']].keys()) else 0
                                        sheet.write(rowx, col, cost if line['end_qty'] != 0 else 0, format_content_float)
                                        col += 1
                                        sheet.write(rowx, col, (unit_cost - cost) if line['end_qty'] != 0 else 0, format_content_float)
                                        col += 1
                                    else:
                                        sheet.write(rowx, col, 0, format_content_float)
                                        col += 1
                                        sheet.write(rowx, col, 0, format_content_float)
                                        col += 1
                                else:
                                    sheet.write(rowx, col, current_cost[line['pid']] if line['end_qty'] != 0 and line['pid'] in current_cost.keys() else 0, format_content_float)
                                    col += 1
                                    sheet.write(rowx, col, (unit_cost - current_cost[line['pid']]) if line['end_qty'] != 0 and line['pid'] in current_cost.keys() else 0, format_content_float)
                                    col += 1

                            rowx += 1
                            seq += 1
                            type_id = line['wcode']
                            brand_id = line['brand_id']

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                    for h in range(col, col + self.get_remain_col_count()):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    warehouse_total_rows.append(rowx)
                    rowx += 1
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Warehouse Total'),
                                      format_group_total)
                    for h in range(col, col + self.get_remain_col_count()):
                        sum_rows = []
                        for r in warehouse_total_rows:
                            sum_rows.append('' + xl_rowcol_to_cell(r, h) + '')
                        sheet.write_formula(rowx, h,
                                            '{=SUM(' + ','.join(map(str, sum_rows)) + ')}', format_group_float)
                    rowx += 1
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_start_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, 0, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_cost, format_title_float)
                    col += 1
                    if self.show_worthy_resources:
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                    if self.show_product_age:
                        sheet.write(rowx, col, ' ', format_title_float)
                        col += 1
                    if self.show_reserved:
                        sheet.write(rowx, col, total_warehouse_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_warehouse_qty, format_title_float)
                        col += 1
                    if self.show_residual:
                        sheet.write(rowx, col, total_warehouse_road, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_warehouse_residual_future, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_future, format_title_float)
                        col += 1
                    if self.qty_on_hand:
                        sheet.write(rowx, col, total_start_qty_on_hand, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_start_qty_on_hand_diff, format_title_float)
                        col += 1
                    if self.show_current_cost:
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    rowx += 1
                elif self.type == 'size':
                    # Размер төрлөөр өртөгтэй
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            # Эхний размер зурна
                            if not size_id:
                                col = (15 if self.show_size else 14) if self.group_by_parent else (14 if self.show_size else 13)
                                if self.show_image:
                                    col += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['size'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн ангиллыг зурна
                            elif size_id and size_id != line['size_id']:

                                sub_seq += 1

                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                                col = self.get_add_col_count(col)
                                for h in range(col, 15 if self.show_size else 14):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowx += 1
                                rowd = rowx
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                col = (15 if self.show_size else 14) if self.group_by_parent else (14 if self.show_size else 13)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['size'], format_content_bold_left)
                                rowx += 1
                                seq = 1

                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_start_cost += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost']
                            total_qty += line['qty']
                            total_cost += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost']
                            total_ex_qty += line['ex_qty']
                            total_ex_cost += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost']
                            total_end_qty += line['end_qty']
                            total_end_cost += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost']
                            # Эцсийн үлдэгдэлийн нэгж өртөг зурах хэсэг
                            unit_cost = 0
                            if line['end_qty'] > 0:
                                unit_cost = abs(line['end_cost']) / abs(line['end_qty'])
                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, unit_cost if self.env['product.product'].browse(
                                line['pid']).cost_method != 'real' else '', format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['end_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost'], format_content_float)
                            col += 1

                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                col += 1

                            if self.show_current_cost:
                                wh_id = warehouse_dic[line['lid']] if line['lid'] in warehouse_dic.keys() else None

                                if wh_id:
                                    cost = current_cost[line['ptid']][wh_id] if (line['ptid'] in current_cost.keys() and wh_id in current_cost[line['ptid']].keys()) else 0
                                    sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else cost, format_content_float)
                                    col += 1
                                    sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else (unit_cost - cost), format_content_float)
                                    col += 1

                            rowx += 1
                            seq += 1
                            size_id = line['size_id']

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                    col = self.get_add_col_count(col)
                    for h in range(col, 14 if self.show_size else 13):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    rowx += 1
                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_start_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, 0, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_cost, format_title_float)
                    col += 1
                    rowx += 1
                elif self.type == 'supplier':
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            # Эхний агуулах/данс болон ангиллыг зурна
                            if not supplier_id and line['supp_id']:
                                col = (15 if self.show_size else 14) if self.group_by_parent else (14 if self.show_size else 13)
                                if self.show_image:
                                    col += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['supp_name'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн ангиллыг зурна
                            elif supplier_id and supplier_id != line['supp_id']:
                                col = (15 if self.show_size else 14) if self.group_by_parent else (14 if self.show_size else 13)
                                if self.show_image:
                                    col += 1
                                sub_seq += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['supp_name'] or u'Тодорхойгүй', format_content_bold_left)
                                rowx += 1
                                seq = 1
                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_start_cost += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost']
                            total_qty += line['qty']
                            total_cost += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost']
                            total_ex_qty += line['ex_qty']
                            total_ex_cost += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost']
                            total_end_qty += line['end_qty']
                            total_end_cost += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost']
                            # Эцсийн үлдэгдэлийн нэгж өртөг зурах хэсэг
                            unit_cost = 0
                            if line['end_qty'] > 0:
                                unit_cost = abs(line['end_cost']) / abs(line['end_qty'])
                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, unit_cost if self.env['product.product'].browse(
                                line['pid']).cost_method != 'real' else '', format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['end_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost'], format_content_float)
                            col += 1

                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                col += 1

                            if self.show_current_cost:
                                wh_id = warehouse_dic[line['lid']] if line['lid'] in warehouse_dic.keys() else None

                                if wh_id:
                                    cost = current_cost[line['ptid']][wh_id] if (line['ptid'] in current_cost.keys() and wh_id in current_cost[line['ptid']].keys()) else 0
                                    sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else cost, format_content_float)
                                    col += 1
                                    sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else (unit_cost - cost), format_content_float)
                                    col += 1

                            rowx += 1
                            seq += 1
                            supplier_id = line['supp_id']

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                    col = self.get_add_col_count(col)
                    for h in range(col, col + 10):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    rowx += 1
                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_start_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_cost, format_title_float)
                    col += 1
                    sheet.write(rowx, col, 0, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_cost, format_title_float)
                    col += 1
                    rowx += 1
            elif self.show_price:
                # худалдах үнээр харах бол
                if self.type == 'warehouse' and self.group_by == 'by_category':
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            if not type_id and not category_id:
                                # Эхний агуулах/данс болон ангиллыг зурна
                                if self.group_by_parent:
                                    parent_total_start_qty = parent_total_qty = parent_total_ex_qty = parent_total_end_qty = 0
                                    parent_total_start_cost = parent_total_cost = parent_total_ex_cost = parent_total_unit_cost = parent_total_end_cost = 0
                                    parent_total_start_price = parent_total_price = parent_total_ex_price = parent_total_end_price = parent_total_unit_price = 0
                                    col = (6 if self.show_size else 5) if self.group_by_parent else (5 if self.show_size else 4)
                                    sheet.merge_range(rowx, 0, rowx, col, categ_parent_dic[line['cid']]['name'], format_content_bold_left)
                                    parent_categ_row = rowx
                                    rowx += 1
                                    parent_category_id = categ_parent_dic[line['cid']]['id']
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = (15 if self.show_size else 14) if self.group_by_parent else (14 if self.show_size else 13)
                                col = self.get_add_col_count(col)
                                if self.show_image:
                                    col += 1
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['cname'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            elif type_id and type_id != line['wcode']:
                                # Дараагийн агуулах/дансыг зурна
                                sub_seq = 1
                                seq = 1
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                                coly = self.get_add_col_count(col)
                                for h in range(col, coly + 1):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                warehouse_total_rows.append(rowx)
                                rowx += 1
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col,
                                                  _('Warehouse Total'),
                                                  format_group_total)
                                for h in range(col, coly + 1):
                                    sum_rows = []
                                    for r in warehouse_total_rows:
                                        sum_rows.append('' + xl_rowcol_to_cell(r, h) + '')
                                    sheet.write_formula(rowx, h,
                                                        '{=SUM(' + ','.join(map(str, sum_rows)) + ')}',
                                                        format_group_float)
                                rowx += 1
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                warehouse_total_rows = []
                                col = (15 if self.show_size else 14) if self.group_by_parent else (14 if self.show_size else 13)
                                col = self.get_add_col_count(col)
                                if self.show_image:
                                    col += 1
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['cname'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            elif category_id and category_id != line['cid']:
                                # Дараагийн ангиллыг зурна
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'),
                                                  format_group_total)
                                coly = self.get_all_col_count()
                                for h in range(col, coly + 1):
                                    sheet.write_formula(rowx, h,
                                                        '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(
                                                            rowx - 1, h) + ')}', format_group_float)
                                warehouse_total_rows.append(rowx)
                                rowx += 1
                                sub_seq += 1
                                col = self.get_main_col_count()
                                if self.group_by_parent and parent_category_id and parent_category_id != categ_parent_dic[line['cid']]['id']:
                                    # Эцэг ангиллаар бүлэглэсэн тохиолдолд
                                    if self.price_view_form == 'with_cost_price':
                                        sheet.write(parent_categ_row, col + 1, parent_total_start_qty, format_title_float)
                                        sheet.write(parent_categ_row, col + 2, parent_total_start_cost, format_title_float)
                                        sheet.write(parent_categ_row, col + 3, parent_total_start_price, format_title_float)
                                        sheet.write(parent_categ_row, col + 4, parent_total_qty, format_title_float)
                                        sheet.write(parent_categ_row, col + 5, parent_total_cost, format_title_float)
                                        sheet.write(parent_categ_row, col + 6, parent_total_price, format_title_float)
                                        sheet.write(parent_categ_row, col + 7, parent_total_ex_qty, format_title_float)
                                        sheet.write(parent_categ_row, col + 8, parent_total_ex_cost, format_title_float)
                                        sheet.write(parent_categ_row, col + 9, parent_total_ex_price, format_title_float)
                                        sheet.write(parent_categ_row, col + 10, parent_total_end_qty, format_title_float)
                                        sheet.write(parent_categ_row, col + 11, parent_total_unit_cost, format_title_float)
                                        sheet.write(parent_categ_row, col + 12, parent_total_unit_price, format_title_float)
                                        sheet.write(parent_categ_row, col + 13, parent_total_end_cost, format_title_float)
                                        sheet.write(parent_categ_row, col + 14, parent_total_end_price, format_title_float)
                                    else:
                                        sheet.write(parent_categ_row, col + 1, parent_total_start_qty, format_title_float)
                                        sheet.write(parent_categ_row, col + 2, parent_total_start_price, format_title_float)
                                        sheet.write(parent_categ_row, col + 3, parent_total_qty, format_title_float)
                                        sheet.write(parent_categ_row, col + 4, parent_total_price, format_title_float)
                                        sheet.write(parent_categ_row, col + 5, parent_total_ex_qty, format_title_float)
                                        sheet.write(parent_categ_row, col + 6, parent_total_ex_price, format_title_float)
                                        sheet.write(parent_categ_row, col + 7, parent_total_unit_price, format_title_float)
                                        sheet.write(parent_categ_row, col + 8, parent_total_end_qty, format_title_float)
                                        sheet.write(parent_categ_row, col + 9, parent_total_end_price, format_title_float)
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['cname'], format_content_bold_left)
                                seq = 1
                                if self.group_by_parent:
                                    parent_total_start_qty = parent_total_qty = parent_total_ex_qty = parent_total_end_qty = 0
                                    parent_total_start_cost = parent_total_cost = parent_total_ex_cost = parent_total_unit_cost = parent_total_end_cost = 0
                                    parent_total_start_price = parent_total_price = parent_total_ex_price = parent_total_end_price = parent_total_unit_price = 0
                                    sheet.merge_range(rowx, 0, rowx, col, categ_parent_dic[line['cid']]['name'], format_content_bold_left)
                                    parent_categ_row = rowx
                                    parent_category_id = categ_parent_dic[line['cid']]['id']
                                rowx += 1
                                rowd = rowx
                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_start_price += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['start_qty']
                            total_qty += line['qty']
                            total_price += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['qty']
                            total_ex_qty += line['ex_qty']
                            total_ex_price += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['ex_qty']
                            total_end_qty += line['end_qty']
                            total_end_price += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty']
                            total_start_cost += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost']
                            total_cost += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost']
                            total_ex_cost += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost']
                            total_end_cost += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost']
                            # Нэгж өртөг зурах хэсэг
                            start_unit_cost = unit_cost = ex_unit_cost = end_unit_cost = 0
                            if line['start_qty'] > 0:
                                start_unit_cost = abs(line['start_cost']) / abs(line['start_qty'])
                            if line['qty'] > 0:
                                unit_cost = abs(line['cost']) / abs(line['qty'])
                            if line['ex_qty'] > 0:
                                ex_unit_cost = abs(line['ex_cost']) / abs(line['ex_qty'])
                            if line['end_qty'] > 0:
                                end_unit_cost = abs(line['end_cost']) / abs(line['end_qty'])

                            if self.show_reserved:
                                warehouse_reserved = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                total_warehouse_reserved_qty += warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                line_available_qty = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                total_available_warehouse_qty += line['end_qty'] - line_available_qty
                            if self.show_residual:
                                warehouse_residual = self.show_reserved_warehouse(line['pid'], line['lid'], 'c2')
                                show_reserved_qty_c1 = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                road_reserved_qty = self.show_reserved_warehouse(line['pid'], line['lid'], 'c3')
                                total_reserved_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                total_residual = warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_road += warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_residual_future += line['end_qty'] - total_reserved_c1 + total_residual
                                total_reserved_qty += road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_road_reserved = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_available_future += line['end_qty'] - total_reserved_c1 + total_residual - total_road_reserved

                            parent_total_start_qty += line['start_qty']
                            parent_total_start_cost += start_unit_cost if self.env['product.product'].browse(line['pid']).cost_method != 'real' else 0
                            parent_total_start_price += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['start_qty']

                            parent_total_qty += line['qty']
                            parent_total_cost += unit_cost if self.env['product.product'].browse(line['pid']).cost_method != 'real' else 0
                            parent_total_price += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['qty']

                            parent_total_ex_qty += line['ex_qty']
                            parent_total_ex_cost += ex_unit_cost if self.env['product.product'].browse(line['pid']).cost_method != 'real' else 0
                            parent_total_ex_price += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['ex_qty']

                            parent_total_end_qty += line['end_qty']
                            parent_total_unit_cost += end_unit_cost if self.env['product.product'].browse(line['pid']).cost_method != 'real' else 0
                            parent_total_unit_price += 0 if line['list_price'] == 0 else line['list_price']
                            parent_total_end_cost += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost']
                            parent_total_end_price += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty']

                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, start_unit_cost if self.env['product.product'].browse(line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                            sheet.write(rowx, col, 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                            sheet.write(rowx, col, 0 if line['qty'] == 0 and zero_qty_zero_cost else line['qty'] * line['list_price'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, ex_unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                            sheet.write(rowx, col, 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_qty'] * line['list_price'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, line['end_qty'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, end_unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['list_price'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty'], format_content_float)
                                col += 1
                            else:
                                sheet.write(rowx, col, line['list_price'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty'], format_content_float)
                                col += 1
                            if self.show_worthy_resources:
                                sheet.write(rowx, col, line['worthy_balance'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - line['worthy_balance'], format_content_float)
                                col += 1
                            if self.show_product_age:
                                product_age = product_age_line[line['lid']][line['pid']] if (line['lid'] in product_age_line.keys() and line['pid'] in product_age_line[line['lid']].keys()) else 0
                                sheet.write(rowx, col, product_age, format_content_float)
                                col += 1
                            if self.show_reserved:
                                sheet.write(rowx, col, warehouse_reserved[line['pid']] if warehouse_reserved else 0, format_content_float)
                                col += 1
                                show_reserved_total = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total, format_content_float)
                                col += 1
                            if self.show_residual:
                                sheet.write(rowx, col, warehouse_residual[line['pid']] if warehouse_residual else 0, format_content_float)
                                col += 1
                                show_reserved_total_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                show_residual_total = warehouse_residual[line['pid']] if warehouse_residual else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total, format_content_float)
                                col += 1
                                sheet.write(rowx, col, road_reserved_qty[line['pid']] if road_reserved_qty else 0, format_content_float)
                                col += 1
                                road_reserved_total = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total - road_reserved_total, format_content_float)
                                col += 1

                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                total_start_qty_on_hand += quant_qty
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                total_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                col += 1

                            rowx += 1
                            seq += 1
                            type_id = line['wcode']
                            category_id = line['cid']

                    if self.group_by_parent and parent_category_id:
                        # Эцэг ангиллаар бүлэглэсэн тохиолдолд
                        col_parent = 6 if self.show_size else 5
                        if self.price_view_form == 'with_cost_price':
                            sheet.write(parent_categ_row, col_parent + 1, parent_total_start_qty, format_title_float)
                            sheet.write(parent_categ_row, col_parent + 2, parent_total_start_cost, format_title_float)
                            sheet.write(parent_categ_row, col_parent + 3, parent_total_start_price, format_title_float)
                            sheet.write(parent_categ_row, col_parent + 4, parent_total_qty, format_title_float)
                            sheet.write(parent_categ_row, col_parent + 5, parent_total_cost, format_title_float)
                            sheet.write(parent_categ_row, col_parent + 6, parent_total_price, format_title_float)
                            sheet.write(parent_categ_row, col_parent + 7, parent_total_ex_qty, format_title_float)
                            sheet.write(parent_categ_row, col_parent + 8, parent_total_ex_cost, format_title_float)
                            sheet.write(parent_categ_row, col_parent + 9, parent_total_ex_price, format_title_float)
                            sheet.write(parent_categ_row, col_parent + 10, parent_total_end_qty, format_title_float)
                            sheet.write(parent_categ_row, col_parent + 11, parent_total_unit_cost, format_title_float)
                            sheet.write(parent_categ_row, col_parent + 12, parent_total_unit_price, format_title_float)
                            sheet.write(parent_categ_row, col_parent + 13, parent_total_end_cost, format_title_float)
                            sheet.write(parent_categ_row, col_parent + 14, parent_total_end_price, format_title_float)
                        else:
                            sheet.write(parent_categ_row, col_parent + 1, parent_total_start_qty, format_title_float)
                            sheet.write(parent_categ_row, col_parent + 2, parent_total_start_price, format_title_float)
                            sheet.write(parent_categ_row, col_parent + 3, parent_total_qty, format_title_float)
                            sheet.write(parent_categ_row, col_parent + 4, parent_total_price, format_title_float)
                            sheet.write(parent_categ_row, col_parent + 5, parent_total_ex_qty, format_title_float)
                            sheet.write(parent_categ_row, col_parent + 6, parent_total_ex_price, format_title_float)
                            sheet.write(parent_categ_row, col_parent + 7, parent_total_unit_price, format_title_float)
                            sheet.write(parent_categ_row, col_parent + 8, parent_total_end_qty, format_title_float)
                            sheet.write(parent_categ_row, col_parent + 9, parent_total_end_price, format_title_float)
                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                    coly = self.get_all_col_count()
                    for h in range(col, coly + 1):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    warehouse_total_rows.append(rowx)
                    rowx += 1
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Warehouse Total'),
                                      format_group_total)
                    for h in range(col, coly + 1):
                        sum_rows = []
                        for r in warehouse_total_rows:
                            sum_rows.append('' + xl_rowcol_to_cell(r, h) + '')
                        sheet.write_formula(rowx, h,
                                            '{=SUM(' + ','.join(map(str, sum_rows)) + ')}', format_group_float)
                    rowx += 1
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, total_start_price, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, total_price, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, total_ex_price, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, total_end_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_cost, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_price, format_title_float)
                        col += 1
                    else:
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_price, format_title_float)
                        col += 1
                    if self.show_worthy_resources:
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                    if self.show_product_age:
                        sheet.write(rowx, col, ' ', format_title_float)
                        col += 1
                    if self.show_reserved:
                        sheet.write(rowx, col, total_warehouse_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_warehouse_qty, format_title_float)
                        col += 1
                    if self.show_residual:
                        sheet.write(rowx, col, total_warehouse_road, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_warehouse_residual_future, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_future, format_title_float)
                        col += 1
                    if self.qty_on_hand:
                        sheet.write(rowx, col, total_start_qty_on_hand, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_start_qty_on_hand_diff, format_title_float)
                        col += 1
                    rowx += 1
                elif self.type == 'warehouse' and self.group_by == 'by_brand':
                    # ###################### START: ХУДАЛДАХ ҮНЭЭР ХАРАХ -> (Агуулах > Брэнд) -р бүлэглэх  #######################
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            # Эхний агуулах болон брэндийг зурна
                            if not type_id and not brand_id:
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                if line['brand_name']:
                                    brand_name = line['brand_name']
                                else:
                                    brand_name = _('Not defined')
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, brand_name, format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн агуулахыг зурна
                            elif type_id and type_id != line['wcode']:
                                sub_seq = 1
                                seq = 1
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                                for h in range(col, col + self.get_remain_col_count()):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                warehouse_total_rows.append(rowx)
                                rowx += 1
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col,
                                                  _('Warehouse Total'),
                                                  format_group_total)
                                for h in range(col, col + self.get_remain_col_count()):
                                    sum_rows = []
                                    for r in warehouse_total_rows:
                                        sum_rows.append('' + xl_rowcol_to_cell(r, h) + '')
                                    sheet.write_formula(rowx, h,
                                                        '{=SUM(' + ','.join(map(str, sum_rows)) + ')}',
                                                        format_group_float)
                                rowx += 1
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                warehouse_total_rows = []
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                if line['brand_name']:
                                    brand_name = line['brand_name']
                                else:
                                    brand_name = _('Not defined')
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, brand_name, format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн брэндийг зурна
                            elif brand_id and brand_id != line['brand_id']:
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                                for h in range(col, col + self.get_remain_col_count()):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                warehouse_total_rows.append(rowx)
                                rowx += 1
                                rowd = rowx

                                col = self.get_all_col_count()
                                sub_seq += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                if line['brand_name']:
                                    brand_name = line['brand_name']
                                else:
                                    brand_name = _('Not defined')
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, brand_name, format_content_bold_left)
                                rowx += 1
                                seq = 1
                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_start_price += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['start_qty']
                            total_start_cost += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost']
                            total_qty += line['qty']
                            total_price += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['qty']
                            total_cost += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost']
                            total_ex_qty += line['ex_qty']
                            total_ex_price += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['ex_qty']
                            total_ex_cost += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost']
                            total_end_qty += line['end_qty']
                            total_end_price += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty']
                            total_end_cost += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost']
                            if self.show_reserved:
                                warehouse_reserved = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                total_warehouse_reserved_qty += warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                line_available_qty = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                total_available_warehouse_qty += line['end_qty'] - line_available_qty
                            if self.show_residual:
                                warehouse_residual = self.show_reserved_warehouse(line['pid'], line['lid'], 'c2')
                                show_reserved_qty_c1 = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                road_reserved_qty = self.show_reserved_warehouse(line['pid'], line['lid'], 'c3')
                                total_reserved_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                total_residual = warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_road += warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_residual_future += line['end_qty'] - total_reserved_c1 + total_residual
                                total_reserved_qty += road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_road_reserved = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_available_future += line['end_qty'] - total_reserved_c1 + total_residual - total_road_reserved
                            # Нэгж өртөг зурах хэсэг
                            start_unit_cost = unit_cost = ex_unit_cost = end_unit_cost = 0
                            if line['start_qty'] > 0:
                                start_unit_cost = abs(line['start_cost']) / abs(line['start_qty'])
                            if line['qty'] > 0:
                                unit_cost = abs(line['cost']) / abs(line['qty'])
                            if line['ex_qty'] > 0:
                                ex_unit_cost = abs(line['ex_cost']) / abs(line['ex_qty'])
                            if line['end_qty'] > 0:
                                end_unit_cost = abs(line['end_cost']) / abs(line['end_qty'])

                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, start_unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                            sheet.write(rowx, col, 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                            sheet.write(rowx, col, 0 if line['qty'] == 0 and zero_qty_zero_cost else line['qty'] * line['list_price'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, ex_unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                            sheet.write(rowx, col, 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_qty'] * line['list_price'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, line['end_qty'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, end_unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['list_price'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty'], format_content_float)
                                col += 1
                            else:
                                sheet.write(rowx, col, line['list_price'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty'], format_content_float)
                                col += 1

                            if self.show_worthy_resources:
                                sheet.write(rowx, col, line['worthy_balance'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - line['worthy_balance'], format_content_float)
                                col += 1
                            if self.show_product_age:
                                product_age = product_age_line[line['lid']][line['pid']] if (line['lid'] in product_age_line.keys() and line['pid'] in product_age_line[line['lid']].keys()) else 0
                                sheet.write(rowx, col, product_age, format_content_float)
                                col += 1
                            if self.show_reserved:
                                sheet.write(rowx, col, warehouse_reserved[line['pid']] if warehouse_reserved else 0, format_content_float)
                                col += 1
                                show_reserved_total = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total, format_content_float)
                                col += 1
                            if self.show_residual:
                                sheet.write(rowx, col, warehouse_residual[line['pid']] if warehouse_residual else 0, format_content_float)
                                col += 1
                                show_reserved_total_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                show_residual_total = warehouse_residual[line['pid']] if warehouse_residual else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total, format_content_float)
                                col += 1
                                sheet.write(rowx, col, road_reserved_qty[line['pid']] if road_reserved_qty else 0, format_content_float)
                                col += 1
                                road_reserved_total = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total - road_reserved_total, format_content_float)
                                col += 1

                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                total_start_qty_on_hand += quant_qty
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                total_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                col += 1

                            rowx += 1
                            seq += 1
                            type_id = line['wcode']
                            brand_id = line['brand_id']
                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                    for h in range(col, col + self.get_remain_col_count()):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    warehouse_total_rows.append(rowx)
                    rowx += 1
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Warehouse Total'),
                                      format_group_total)
                    for h in range(col, col + self.get_remain_col_count()):
                        sum_rows = []
                        for r in warehouse_total_rows:
                            sum_rows.append('' + xl_rowcol_to_cell(r, h) + '')
                        sheet.write_formula(rowx, h,
                                            '{=SUM(' + ','.join(map(str, sum_rows)) + ')}', format_group_float)
                    rowx += 1
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, total_start_price, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, total_price, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, total_ex_price, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, total_end_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_cost, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_price, format_title_float)
                        col += 1
                    else:
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_price, format_title_float)
                        col += 1
                    if self.show_worthy_resources:
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                    if self.show_product_age:
                        sheet.write(rowx, col, ' ', format_title_float)
                        col += 1
                    if self.show_reserved:
                        sheet.write(rowx, col, total_warehouse_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_warehouse_qty, format_title_float)
                        col += 1
                    if self.show_residual:
                        sheet.write(rowx, col, total_warehouse_road, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_warehouse_residual_future, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_future, format_title_float)
                        col += 1
                    if self.qty_on_hand:
                        sheet.write(rowx, col, total_start_qty_on_hand, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_start_qty_on_hand_diff, format_title_float)
                        col += 1
                    rowx += 1
                elif self.type == 'warehouse' and self.group_by == 'by_supplier':
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            # Эхний агуулах/данс болон ангиллыг зурна
                            if not type_id and not supp_id:
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = (15 if self.show_size else 14) if self.group_by_parent else (14 if self.show_size else 13)
                                col = self.get_add_col_count(col)
                                if self.show_image:
                                    col += 1
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['supp_name'] or u'Тодорхойгүй', format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн агуулах/дансыг зурна
                            elif type_id and type_id != line['wcode']:
                                sub_seq = 1
                                seq = 1

                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                                coly = self.get_add_col_count(col)
                                for h in range(col, coly + 10):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowx += 1
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = (15 if self.show_size else 14) if self.group_by_parent else (14 if self.show_size else 13)
                                col = self.get_add_col_count(col)
                                if self.show_image:
                                    col += 1
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['supp_name'] or u'Тодорхойгүй', format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн ангиллыг зурна
                            elif supp_id and supp_id != line['supp_id']:
                                col = (15 if self.show_size else 14) if self.group_by_parent else (14 if self.show_size else 13)
                                col = self.get_add_col_count(col)
                                if self.show_image:
                                    col += 1
                                sub_seq += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['supp_name'] or u'Тодорхойгүй', format_content_bold_left)
                                rowx += 1
                                seq = 1
                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_start_price += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['start_qty']
                            total_qty += line['qty']
                            total_price += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['qty']
                            total_ex_qty += line['ex_qty']
                            total_ex_price += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['ex_qty']
                            total_end_qty += line['end_qty']
                            total_end_price += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty']
                            total_start_cost += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost']
                            total_cost += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost']
                            total_ex_cost += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost']
                            total_end_cost += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost']
                            # Нэгж өртөг зурах хэсэг
                            start_unit_cost = unit_cost = ex_unit_cost = end_unit_cost = 0
                            if line['start_qty'] > 0:
                                start_unit_cost = abs(line['start_cost']) / abs(line['start_qty'])
                            if line['qty'] > 0:
                                unit_cost = abs(line['cost']) / abs(line['qty'])
                            if line['ex_qty'] > 0:
                                ex_unit_cost = abs(line['ex_cost']) / abs(line['ex_qty'])
                            if line['end_qty'] > 0:
                                end_unit_cost = abs(line['end_cost']) / abs(line['end_qty'])

                            if self.show_reserved:
                                warehouse_reserved = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                total_warehouse_reserved_qty += warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                line_available_qty = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                total_available_warehouse_qty += line['end_qty'] - line_available_qty
                            if self.show_residual:
                                warehouse_residual = self.show_reserved_warehouse(line['pid'], line['lid'], 'c2')
                                show_reserved_qty_c1 = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                road_reserved_qty = self.show_reserved_warehouse(line['pid'], line['lid'], 'c3')
                                total_reserved_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                total_residual = warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_road += warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_residual_future += line['end_qty'] - total_reserved_c1 + total_residual
                                total_reserved_qty += road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_road_reserved = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_available_future += line['end_qty'] - total_reserved_c1 + total_residual - total_road_reserved
                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, start_unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                            sheet.write(rowx, col, 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                            sheet.write(rowx, col, 0 if line['qty'] == 0 and zero_qty_zero_cost else line['qty'] * line['list_price'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, ex_unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                            sheet.write(rowx, col, 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_qty'] * line['list_price'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, line['end_qty'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, end_unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['list_price'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty'], format_content_float)
                                col += 1
                            else:
                                sheet.write(rowx, col, line['list_price'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty'], format_content_float)
                                col += 1
                            if self.show_worthy_resources:
                                sheet.write(rowx, col, line['worthy_balance'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - line['worthy_balance'], format_content_float)
                                col += 1
                            if self.show_product_age:
                                product_age = product_age_line[line['lid']][line['pid']] if (line['lid'] in product_age_line.keys() and line['pid'] in product_age_line[line['lid']].keys()) else 0
                                sheet.write(rowx, col, product_age, format_content_float)
                                col += 1
                            if self.show_reserved:
                                sheet.write(rowx, col, warehouse_reserved[line['pid']] if warehouse_reserved else 0, format_content_float)
                                col += 1
                                show_reserved_total = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total, format_content_float)
                                col += 1
                            if self.show_residual:
                                sheet.write(rowx, col, warehouse_residual[line['pid']] if warehouse_residual else 0, format_content_float)
                                col += 1
                                show_reserved_total_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                show_residual_total = warehouse_residual[line['pid']] if warehouse_residual else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total, format_content_float)
                                col += 1
                                sheet.write(rowx, col, road_reserved_qty[line['pid']] if road_reserved_qty else 0, format_content_float)
                                col += 1
                                road_reserved_total = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total - road_reserved_total, format_content_float)
                                col += 1

                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                total_start_qty_on_hand += quant_qty
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                total_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                col += 1

                            rowx += 1
                            seq += 1
                            type_id = line['wcode']
                            supp_id = line['supp_id']

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                    coly = self.get_add_col_count(col)
                    for h in range(col, coly + 10):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    rowx += 1
                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, total_start_price, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, total_price, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, total_ex_price, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, total_end_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_cost, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_price, format_title_float)
                        col += 1
                    else:
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_price, format_title_float)
                        col += 1
                    if self.show_worthy_resources:
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                    if self.show_product_age:
                        sheet.write(rowx, col, ' ', format_title_float)
                        col += 1
                    if self.show_reserved:
                        sheet.write(rowx, col, total_warehouse_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_warehouse_qty, format_title_float)
                        col += 1
                    if self.show_residual:
                        sheet.write(rowx, col, total_warehouse_road, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_warehouse_residual_future, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_future, format_title_float)
                        col += 1
                    if self.qty_on_hand:
                        sheet.write(rowx, col, total_start_qty_on_hand, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_start_qty_on_hand_diff, format_title_float)
                        col += 1
                    rowx += 1
                elif self.type == 'warehouse' and self.group_by == 'by_account':
                    subtotal_start_qty = 0
                    subtotal_start_price = 0
                    subtotal_start_cost = 0
                    subtotal_qty = 0
                    subtotal_cost = 0
                    subtotal_price = 0
                    subtotal_ex_qty = 0
                    subtotal_ex_cost = 0
                    subtotal_ex_price = 0
                    subtotal_end_qty = 0
                    subtotal_end_price = 0
                    subtotal_end_cost = 0
                    subtotal_warehouse_reserved_qty = subtotal_available_warehouse_qty = subtotal_warehouse_road = subtotal_warehouse_residual_future = 0
                    subtotal_reserved_qty = subtotal_available_future = 0
                    subtotal_start_qty_on_hand = subtotal_start_qty_on_hand_diff = 0
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        account_name = line['account_code'] + ' ' + line['account_name']
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            # Эхний агуулах болон дансыг зурна
                            if not type_id and not brand_id:
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = self.get_all_col_count()
                                if self.show_image:
                                    col += 1
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, account_name, format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн агуулахыг зурна
                            elif type_id and type_id != line['wcode']:
                                sub_seq = 1
                                seq = 1

                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Subtotal'), format_group_total)
                                for h in range(col, col + self.get_remain_col_count()):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowd = rowx + 2
                                rowx += 1
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Amount'), format_group_total)
                                col += 1
                                sheet.write(rowx, col, subtotal_start_qty, format_title_float)
                                col += 1
                                if self.price_view_form == 'with_cost_price':
                                    sheet.write(rowx, col, subtotal_start_cost, format_title_float)
                                    col += 1
                                sheet.write(rowx, col, subtotal_start_price, format_title_float)
                                col += 1
                                sheet.write(rowx, col, subtotal_qty, format_title_float)
                                col += 1
                                if self.price_view_form == 'with_cost_price':
                                    sheet.write(rowx, col, subtotal_cost, format_title_float)
                                    col += 1
                                sheet.write(rowx, col, subtotal_price, format_title_float)
                                col += 1
                                sheet.write(rowx, col, subtotal_ex_qty, format_title_float)
                                col += 1
                                if self.price_view_form == 'with_cost_price':
                                    sheet.write(rowx, col, subtotal_ex_cost, format_title_float)
                                    col += 1
                                sheet.write(rowx, col, subtotal_ex_price, format_title_float)
                                col += 1
                                if self.price_view_form == 'with_cost_price':
                                    sheet.write(rowx, col, subtotal_end_qty, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, 0, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, 0, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, subtotal_end_cost, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, subtotal_end_price, format_title_float)
                                    col += 1
                                else:
                                    sheet.write(rowx, col, 0, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, subtotal_end_qty, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, subtotal_end_price, format_title_float)
                                    col += 1
                                if self.show_worthy_resources:
                                    sheet.write(rowx, col, ' ', format_content_float)
                                    col += 1
                                    sheet.write(rowx, col, ' ', format_content_float)
                                    col += 1
                                if self.show_product_age:
                                    sheet.write(rowx, col, ' ', format_title_float)
                                    col += 1
                                if self.show_reserved:
                                    sheet.write(rowx, col, subtotal_warehouse_reserved_qty, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, subtotal_available_warehouse_qty, format_title_float)
                                    col += 1
                                if self.show_residual:
                                    sheet.write(rowx, col, subtotal_warehouse_road, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, subtotal_warehouse_residual_future, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, subtotal_reserved_qty, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, subtotal_available_future, format_title_float)
                                    col += 1
                                if self.qty_on_hand:
                                    sheet.write(rowx, col, subtotal_start_qty_on_hand, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, subtotal_start_qty_on_hand_diff, format_title_float)
                                    col += 1
                                rowx += 1

                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, account_name, format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                                subtotal_start_qty = 0
                                subtotal_start_price = 0
                                subtotal_start_cost = 0
                                subtotal_qty = 0
                                subtotal_cost = 0
                                subtotal_price = 0
                                subtotal_ex_qty = 0
                                subtotal_ex_cost = 0
                                subtotal_ex_price = 0
                                subtotal_end_qty = 0
                                subtotal_end_price = 0
                                subtotal_end_cost = 0
                                subtotal_warehouse_reserved_qty = subtotal_available_warehouse_qty = subtotal_warehouse_road = subtotal_warehouse_residual_future = 0
                                subtotal_reserved_qty = subtotal_available_future = 0
                                subtotal_start_qty_on_hand = subtotal_start_qty_on_hand_diff = 0

                            # Дараагийн дансийг зурна
                            elif account_id and account_id != line['account_id']:
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Subtotal'), format_group_total)
                                for h in range(col, col + self.get_remain_col_count()):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowd = rowx + 2
                                rowx += 1

                                col = self.get_all_col_count()
                                sub_seq += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, account_name, format_content_bold_left)
                                rowx += 1
                                seq = 1
                            # Дэд дүнг олно
                            subtotal_start_qty += line['start_qty']
                            subtotal_start_price += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['start_qty']
                            subtotal_start_cost += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost']
                            subtotal_qty += line['qty']
                            subtotal_price += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['qty']
                            subtotal_cost += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost']
                            subtotal_ex_qty += line['ex_qty']
                            subtotal_ex_price += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['ex_qty']
                            subtotal_ex_cost += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost']
                            subtotal_end_qty += line['end_qty']
                            subtotal_end_price += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty']
                            subtotal_end_cost += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost']
                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_start_price += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['start_qty']
                            total_start_cost += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost']
                            total_qty += line['qty']
                            total_price += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['qty']
                            total_cost += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost']
                            total_ex_qty += line['ex_qty']
                            total_ex_price += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['ex_qty']
                            total_ex_cost += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost']
                            total_end_qty += line['end_qty']
                            total_end_price += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty']
                            total_end_cost += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost']
                            if self.show_reserved:
                                warehouse_reserved = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                total_warehouse_reserved_qty += warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                line_available_qty = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                total_available_warehouse_qty += line['end_qty'] - line_available_qty
                                subtotal_warehouse_reserved_qty += warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                subtotal_available_warehouse_qty += line['end_qty'] - line_available_qty
                            if self.show_residual:
                                warehouse_residual = self.show_reserved_warehouse(line['pid'], line['lid'], 'c2')
                                show_reserved_qty_c1 = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                road_reserved_qty = self.show_reserved_warehouse(line['pid'], line['lid'], 'c3')
                                total_reserved_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                total_residual = warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_road += warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_residual_future += line['end_qty'] - total_reserved_c1 + total_residual
                                total_reserved_qty += road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_road_reserved = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_available_future += line['end_qty'] - total_reserved_c1 + total_residual - total_road_reserved

                                subtotal_warehouse_road += warehouse_residual[line['pid']] if warehouse_residual else 0
                                subtotal_warehouse_residual_future += line['end_qty'] - total_reserved_c1 + total_residual
                                subtotal_reserved_qty += road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                subtotal_road_reserved = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                subtotal_available_future += line['end_qty'] - total_reserved_c1 + total_residual - total_road_reserved
                            # Нэгж өртөг зурах хэсэг
                            start_unit_cost = unit_cost = ex_unit_cost = end_unit_cost = 0
                            if line['start_qty'] > 0:
                                start_unit_cost = abs(line['start_cost']) / abs(line['start_qty'])
                            if line['qty'] > 0:
                                unit_cost = abs(line['cost']) / abs(line['qty'])
                            if line['ex_qty'] > 0:
                                ex_unit_cost = abs(line['ex_cost']) / abs(line['ex_qty'])
                            if line['end_qty'] > 0:
                                end_unit_cost = abs(line['end_cost']) / abs(line['end_qty'])
                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, start_unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                            sheet.write(rowx, col, 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                            sheet.write(rowx, col, 0 if line['qty'] == 0 and zero_qty_zero_cost else line['qty'] * line['list_price'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, ex_unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                            sheet.write(rowx, col, 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_qty'] * line['list_price'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, line['end_qty'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, end_unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['list_price'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty'], format_content_float)
                                col += 1
                            else:
                                sheet.write(rowx, col, line['list_price'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty'], format_content_float)
                                col += 1

                            if self.show_worthy_resources:
                                sheet.write(rowx, col, line['worthy_balance'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - line['worthy_balance'], format_content_float)
                                col += 1
                            if self.show_product_age:
                                product_age = product_age_line[line['lid']][line['pid']] if (line['lid'] in product_age_line.keys() and line['pid'] in product_age_line[line['lid']].keys()) else 0
                                sheet.write(rowx, col, product_age, format_content_float)
                                col += 1
                            if self.show_reserved:
                                sheet.write(rowx, col, warehouse_reserved[line['pid']] if warehouse_reserved else 0, format_content_float)
                                col += 1
                                show_reserved_total = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total, format_content_float)
                                col += 1
                            if self.show_residual:
                                sheet.write(rowx, col, warehouse_residual[line['pid']] if warehouse_residual else 0, format_content_float)
                                col += 1
                                show_reserved_total_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                show_residual_total = warehouse_residual[line['pid']] if warehouse_residual else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total, format_content_float)
                                col += 1
                                sheet.write(rowx, col, road_reserved_qty[line['pid']] if road_reserved_qty else 0, format_content_float)
                                col += 1
                                road_reserved_total = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total - road_reserved_total, format_content_float)
                                col += 1

                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                total_start_qty_on_hand += quant_qty
                                subtotal_start_qty_on_hand += quant_qty
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                total_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                subtotal_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                col += 1

                            rowx += 1
                            seq += 1
                            type_id = line['wcode']
                            account_id = line['account_id']

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Subtotal'),
                                      format_group_total)
                    for h in range(col, col + self.get_remain_col_count()):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    rowx += 1
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Amount'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, subtotal_start_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, subtotal_start_price, format_title_float)
                    col += 1
                    sheet.write(rowx, col, subtotal_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, subtotal_price, format_title_float)
                    col += 1
                    sheet.write(rowx, col, subtotal_ex_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, subtotal_ex_price, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, subtotal_end_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, subtotal_end_cost, format_title_float)
                        col += 1
                        sheet.write(rowx, col, subtotal_end_price, format_title_float)
                        col += 1
                    else:
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, subtotal_end_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, subtotal_end_cost, format_title_float)
                        col += 1
                    if self.show_worthy_resources:
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                    if self.show_product_age:
                        sheet.write(rowx, col, ' ', format_title_float)
                        col += 1
                    if self.show_reserved:
                        sheet.write(rowx, col, subtotal_warehouse_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, subtotal_available_warehouse_qty, format_title_float)
                        col += 1
                    if self.show_residual:
                        sheet.write(rowx, col, subtotal_warehouse_road, format_title_float)
                        col += 1
                        sheet.write(rowx, col, subtotal_warehouse_residual_future, format_title_float)
                        col += 1
                        sheet.write(rowx, col, subtotal_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, subtotal_available_future, format_title_float)
                        col += 1
                    if self.qty_on_hand:
                        sheet.write(rowx, col, subtotal_start_qty_on_hand, format_title_float)
                        col += 1
                        sheet.write(rowx, col, subtotal_start_qty_on_hand_diff, format_title_float)
                        col += 1
                    rowx += 1

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, total_start_price, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, total_price, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, total_ex_price, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, total_end_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_cost, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_price, format_title_float)
                        col += 1
                    else:
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_cost, format_title_float)
                        col += 1
                    if self.show_worthy_resources:
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                    if self.show_product_age:
                        sheet.write(rowx, col, ' ', format_title_float)
                        col += 1
                    if self.show_reserved:
                        sheet.write(rowx, col, total_warehouse_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_warehouse_qty, format_title_float)
                        col += 1
                    if self.show_residual:
                        sheet.write(rowx, col, total_warehouse_road, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_warehouse_residual_future, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_future, format_title_float)
                        col += 1
                    if self.qty_on_hand:
                        sheet.write(rowx, col, total_start_qty_on_hand, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_start_qty_on_hand_diff, format_title_float)
                        col += 1
                    rowx += 1
                elif self.type == 'warehouse' and self.group_by == 'by_size':
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            # Эхний агуулах/данс болон ангиллыг зурна
                            if not type_id and not size_id:
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['size'] or u'Тодорхойгүй', format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн агуулах/дансыг зурна
                            elif type_id and type_id != line['wcode']:
                                sub_seq = 1
                                seq = 1
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                                coly = self.get_all_col_count()
                                for h in range(col, coly + 1):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowx += 1
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['size'] or u'Тодорхойгүй', format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн ангиллыг зурна
                            elif size_id and size_id != line['size_id']:
                                col = self.get_all_col_count()
                                sub_seq += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['size'] or u'Тодорхойгүй', format_content_bold_left)
                                rowx += 1
                                seq = 1
                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_start_price += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['start_qty']
                            total_qty += line['qty']
                            total_price += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['qty']
                            total_ex_qty += line['ex_qty']
                            total_ex_price += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['ex_qty']
                            total_end_qty += line['end_qty']
                            total_end_price += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty']
                            total_start_cost += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost']
                            total_cost += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost']
                            total_ex_cost += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost']
                            total_end_cost += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost']
                            # Нэгж өртөг зурах хэсэг
                            start_unit_cost = unit_cost = ex_unit_cost = end_unit_cost = 0
                            if line['start_qty'] > 0:
                                start_unit_cost = abs(line['start_cost']) / abs(line['start_qty'])
                            if line['qty'] > 0:
                                unit_cost = abs(line['cost']) / abs(line['qty'])
                            if line['ex_qty'] > 0:
                                ex_unit_cost = abs(line['ex_cost']) / abs(line['ex_qty'])
                            if line['end_qty'] > 0:
                                end_unit_cost = abs(line['end_cost']) / abs(line['end_qty'])

                            if self.show_reserved:
                                warehouse_reserved = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                total_warehouse_reserved_qty += warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                line_available_qty = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                total_available_warehouse_qty += line['end_qty'] - line_available_qty
                            if self.show_residual:
                                warehouse_residual = self.show_reserved_warehouse(line['pid'], line['lid'], 'c2')
                                show_reserved_qty_c1 = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                road_reserved_qty = self.show_reserved_warehouse(line['pid'], line['lid'], 'c3')
                                total_reserved_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                total_residual = warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_road += warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_residual_future += line['end_qty'] - total_reserved_c1 + total_residual
                                total_reserved_qty += road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_road_reserved = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_available_future += line['end_qty'] - total_reserved_c1 + total_residual - total_road_reserved
                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, start_unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                            sheet.write(rowx, col, 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                            sheet.write(rowx, col, 0 if line['qty'] == 0 and zero_qty_zero_cost else line['qty'] * line['list_price'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, ex_unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                            sheet.write(rowx, col, 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_qty'] * line['list_price'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, line['end_qty'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, end_unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['list_price'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty'], format_content_float)
                                col += 1
                            else:
                                sheet.write(rowx, col, line['list_price'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty'], format_content_float)
                                col += 1
                            if self.show_worthy_resources:
                                sheet.write(rowx, col, line['worthy_balance'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - line['worthy_balance'], format_content_float)
                                col += 1
                            if self.show_product_age:
                                product_age = product_age_line[line['lid']][line['pid']] if (line['lid'] in product_age_line.keys() and line['pid'] in product_age_line[line['lid']].keys()) else 0
                                sheet.write(rowx, col, product_age, format_content_float)
                                col += 1
                            if self.show_reserved:
                                sheet.write(rowx, col, warehouse_reserved[line['pid']] if warehouse_reserved else 0, format_content_float)
                                col += 1
                                show_reserved_total = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total, format_content_float)
                                col += 1
                            if self.show_residual:
                                sheet.write(rowx, col, warehouse_residual[line['pid']] if warehouse_residual else 0, format_content_float)
                                col += 1
                                show_reserved_total_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                show_residual_total = warehouse_residual[line['pid']] if warehouse_residual else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total, format_content_float)
                                col += 1
                                sheet.write(rowx, col, road_reserved_qty[line['pid']] if road_reserved_qty else 0, format_content_float)
                                col += 1
                                road_reserved_total = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total - road_reserved_total, format_content_float)
                                col += 1

                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                total_start_qty_on_hand += quant_qty
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                total_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                col += 1

                            rowx += 1
                            seq += 1
                            type_id = line['wcode']
                            size_id = line['size_id']

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                    coly = self.get_add_col_count(col)
                    for h in range(col, coly + 10):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    rowx += 1
                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, total_start_price, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, total_price, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, total_ex_price, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, total_end_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_cost, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_price, format_title_float)
                        col += 1
                    else:
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_price, format_title_float)
                        col += 1
                    if self.show_worthy_resources:
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                    if self.show_product_age:
                        sheet.write(rowx, col, ' ', format_title_float)
                        col += 1
                    if self.show_reserved:
                        sheet.write(rowx, col, total_warehouse_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_warehouse_qty, format_title_float)
                        col += 1
                    if self.show_residual:
                        sheet.write(rowx, col, total_warehouse_road, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_warehouse_residual_future, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_future, format_title_float)
                        col += 1
                    if self.qty_on_hand:
                        sheet.write(rowx, col, total_start_qty_on_hand, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_start_qty_on_hand_diff, format_title_float)
                        col += 1
                    rowx += 1
                elif self.type == 'category':
                    subtotal_start_qty = 0
                    subtotal_start_price = 0
                    subtotal_start_cost = 0
                    subtotal_qty = 0
                    subtotal_cost = 0
                    subtotal_price = 0
                    subtotal_ex_qty = 0
                    subtotal_ex_cost = 0
                    subtotal_ex_price = 0
                    subtotal_end_qty = 0
                    subtotal_end_price = 0
                    subtotal_end_cost = 0
                    subtotal_warehouse_reserved_qty = subtotal_available_warehouse_qty = subtotal_warehouse_road = subtotal_warehouse_residual_future = 0
                    subtotal_reserved_qty = subtotal_available_future = 0
                    subtotal_start_qty_on_hand = subtotal_start_qty_on_hand_diff = 0
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            # Эхний агуулах/данс болон ангиллыг зурна
                            if not category_id:
                                col = (15 if self.show_size else 14) if self.group_by_parent else (14 if self.show_size else 13)
                                if self.show_image:
                                    col += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['cname'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                                subtotal_start_qty = 0
                                subtotal_start_price = 0
                                subtotal_start_cost = 0
                                subtotal_qty = 0
                                subtotal_cost = 0
                                subtotal_price = 0
                                subtotal_ex_qty = 0
                                subtotal_ex_cost = 0
                                subtotal_ex_price = 0
                                subtotal_end_qty = 0
                                subtotal_end_price = 0
                                subtotal_end_cost = 0
                                subtotal_warehouse_reserved_qty = subtotal_available_warehouse_qty = subtotal_warehouse_road = subtotal_warehouse_residual_future = 0
                                subtotal_reserved_qty = subtotal_available_future = 0
                                subtotal_start_qty_on_hand = subtotal_start_qty_on_hand_diff = 0
                            # Дараагийн ангиллыг зурна
                            elif category_id and category_id != line['cid']:
                                col = (6 if self.show_size else 5) if self.group_by_parent else (5 if self.show_size else 4)
                                if self.show_image:
                                    col += 1
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Subtotal'), format_group_total)
                                col += 1
                                sheet.write(rowx, col, subtotal_start_qty, format_title_float)
                                col += 1
                                if self.price_view_form == 'with_cost_price':
                                    sheet.write(rowx, col, 0, format_title_float)
                                    col += 1
                                sheet.write(rowx, col, subtotal_start_price, format_title_float)
                                col += 1
                                sheet.write(rowx, col, subtotal_qty, format_title_float)
                                col += 1
                                if self.price_view_form == 'with_cost_price':
                                    sheet.write(rowx, col, 0, format_title_float)
                                    col += 1
                                sheet.write(rowx, col, subtotal_price, format_title_float)
                                col += 1
                                sheet.write(rowx, col, subtotal_ex_qty, format_title_float)
                                col += 1
                                if self.price_view_form == 'with_cost_price':
                                    sheet.write(rowx, col, 0, format_title_float)
                                    col += 1
                                sheet.write(rowx, col, subtotal_ex_price, format_title_float)
                                col += 1
                                if self.price_view_form == 'with_cost_price':
                                    sheet.write(rowx, col, subtotal_end_qty, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, 0, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, 0, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, subtotal_end_cost, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, subtotal_end_price, format_title_float)
                                    col += 1
                                else:
                                    sheet.write(rowx, col, 0, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, subtotal_end_qty, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, subtotal_end_cost, format_title_float)
                                    col += 1
                                if self.show_worthy_resources:
                                    sheet.write(rowx, col, ' ', format_content_float)
                                    col += 1
                                    sheet.write(rowx, col, ' ', format_content_float)
                                    col += 1
                                if self.show_product_age:
                                    sheet.write(rowx, col, ' ', format_title_float)
                                    col += 1
                                if self.show_reserved:
                                    sheet.write(rowx, col, subtotal_warehouse_reserved_qty, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, subtotal_available_warehouse_qty, format_title_float)
                                    col += 1
                                if self.show_residual:
                                    sheet.write(rowx, col, subtotal_warehouse_road, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, subtotal_warehouse_residual_future, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, subtotal_reserved_qty, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, subtotal_available_future, format_title_float)
                                    col += 1
                                if self.qty_on_hand:
                                    sheet.write(rowx, col, subtotal_start_qty_on_hand, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, subtotal_start_qty_on_hand_diff, format_title_float)
                                    col += 1

                                rowx += 1
                                col = (15 if self.show_size else 14) if self.group_by_parent else (14 if self.show_size else 13)
                                sub_seq += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['cname'], format_content_bold_left)
                                rowx += 1
                                seq = 1
                                subtotal_start_qty = 0
                                subtotal_start_price = 0
                                subtotal_start_cost = 0
                                subtotal_qty = 0
                                subtotal_cost = 0
                                subtotal_price = 0
                                subtotal_ex_qty = 0
                                subtotal_ex_cost = 0
                                subtotal_ex_price = 0
                                subtotal_end_qty = 0
                                subtotal_end_price = 0
                                subtotal_end_cost = 0
                                subtotal_warehouse_reserved_qty = subtotal_available_warehouse_qty = subtotal_warehouse_road = subtotal_warehouse_residual_future = 0
                                subtotal_reserved_qty = subtotal_available_future = 0
                                subtotal_start_qty_on_hand = subtotal_start_qty_on_hand_diff = 0

                            # Дэд дүнг олно
                            subtotal_start_qty += line['start_qty']
                            subtotal_start_price += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['start_qty']
                            subtotal_start_cost += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost']
                            subtotal_qty += line['qty']
                            subtotal_price += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['qty']
                            subtotal_cost += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost']
                            subtotal_ex_qty += line['ex_qty']
                            subtotal_ex_price += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['ex_qty']
                            subtotal_ex_cost += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost']
                            subtotal_end_qty += line['end_qty']
                            subtotal_end_price += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty']
                            subtotal_end_cost += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost']
                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_start_price += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['start_qty']
                            total_start_cost += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost']
                            total_qty += line['qty']
                            total_price += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['qty']
                            total_cost += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost']
                            total_ex_qty += line['ex_qty']
                            total_ex_price += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['ex_qty']
                            total_ex_cost += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost']
                            total_end_qty += line['end_qty']
                            total_end_price += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty']
                            total_end_cost += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost']
                            if self.show_reserved:
                                warehouse_reserved = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                total_warehouse_reserved_qty += warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                line_available_qty = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                total_available_warehouse_qty += line['end_qty'] - line_available_qty
                                subtotal_warehouse_reserved_qty += warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                subtotal_available_warehouse_qty += line['end_qty'] - line_available_qty
                            if self.show_residual:
                                warehouse_residual = self.show_reserved_warehouse(line['pid'], line['lid'], 'c2')
                                show_reserved_qty_c1 = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                road_reserved_qty = self.show_reserved_warehouse(line['pid'], line['lid'], 'c3')
                                total_reserved_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                total_residual = warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_road += warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_residual_future += line['end_qty'] - total_reserved_c1 + total_residual
                                total_reserved_qty += road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_road_reserved = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_available_future += line['end_qty'] - total_reserved_c1 + total_residual - total_road_reserved

                                subtotal_warehouse_road += warehouse_residual[line['pid']] if warehouse_residual else 0
                                subtotal_warehouse_residual_future += line['end_qty'] - total_reserved_c1 + total_residual
                                subtotal_reserved_qty += road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                subtotal_road_reserved = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                subtotal_available_future += line['end_qty'] - total_reserved_c1 + total_residual - total_road_reserved
                            # Нэгж өртөг зурах хэсэг
                            start_unit_cost = unit_cost = ex_unit_cost = end_unit_cost = 0
                            if line['start_qty'] > 0:
                                start_unit_cost = abs(line['start_cost']) / abs(line['start_qty'])
                            if line['qty'] > 0:
                                unit_cost = abs(line['cost']) / abs(line['qty'])
                            if line['ex_qty'] > 0:
                                ex_unit_cost = abs(line['ex_cost']) / abs(line['ex_qty'])
                            if line['end_qty'] > 0:
                                end_unit_cost = abs(line['end_cost']) / abs(line['end_qty'])
                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, start_unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                            sheet.write(rowx, col, 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_qty'] * line['list_price'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                            sheet.write(rowx, col, 0 if line['qty'] == 0 and zero_qty_zero_cost else line['qty'] * line['list_price'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, ex_unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                            sheet.write(rowx, col, 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_qty'] * line['list_price'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, line['end_qty'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, end_unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['list_price'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty'], format_content_float)
                                col += 1
                            else:
                                sheet.write(rowx, col, line['list_price'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty'], format_content_float)
                                col += 1

                            if self.show_worthy_resources:
                                sheet.write(rowx, col, line['worthy_balance'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - line['worthy_balance'], format_content_float)
                                col += 1
                            if self.show_product_age:
                                product_age = product_age_line[line['lid']][line['pid']] if (line['lid'] in product_age_line.keys() and line['pid'] in product_age_line[line['lid']].keys()) else 0
                                sheet.write(rowx, col, product_age, format_content_float)
                                col += 1
                            if self.show_reserved:
                                sheet.write(rowx, col, warehouse_reserved[line['pid']] if warehouse_reserved else 0, format_content_float)
                                col += 1
                                show_reserved_total = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total, format_content_float)
                                col += 1
                            if self.show_residual:
                                sheet.write(rowx, col, warehouse_residual[line['pid']] if warehouse_residual else 0, format_content_float)
                                col += 1
                                show_reserved_total_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                show_residual_total = warehouse_residual[line['pid']] if warehouse_residual else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total, format_content_float)
                                col += 1
                                sheet.write(rowx, col, road_reserved_qty[line['pid']] if road_reserved_qty else 0, format_content_float)
                                col += 1
                                road_reserved_total = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total - road_reserved_total, format_content_float)
                                col += 1

                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                total_start_qty_on_hand += quant_qty
                                subtotal_start_qty_on_hand += quant_qty
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                total_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                subtotal_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                col += 1
                            rowx += 1
                            seq += 1
                            category_id = line['cid']

                    col = (6 if self.show_size else 5) if self.group_by_parent else (5 if self.show_size else 4)
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Subtotal'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, subtotal_start_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, subtotal_start_price, format_title_float)
                    col += 1
                    sheet.write(rowx, col, subtotal_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, subtotal_price, format_title_float)
                    col += 1
                    sheet.write(rowx, col, subtotal_ex_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, subtotal_ex_price, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, subtotal_end_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, subtotal_end_cost, format_title_float)
                        col += 1
                        sheet.write(rowx, col, subtotal_end_price, format_title_float)
                        col += 1
                    else:
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, subtotal_end_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, subtotal_end_cost, format_title_float)
                        col += 1
                    if self.show_worthy_resources:
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                    if self.show_product_age:
                        sheet.write(rowx, col, ' ', format_title_float)
                        col += 1
                    if self.show_reserved:
                        sheet.write(rowx, col, subtotal_warehouse_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, subtotal_available_warehouse_qty, format_title_float)
                        col += 1
                    if self.show_residual:
                        sheet.write(rowx, col, subtotal_warehouse_road, format_title_float)
                        col += 1
                        sheet.write(rowx, col, subtotal_warehouse_residual_future, format_title_float)
                        col += 1
                        sheet.write(rowx, col, subtotal_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, subtotal_available_future, format_title_float)
                        col += 1
                    if self.qty_on_hand:
                        sheet.write(rowx, col, subtotal_start_qty_on_hand, format_title_float)
                        col += 1
                        sheet.write(rowx, col, subtotal_start_qty_on_hand_diff, format_title_float)
                        col += 1
                    rowx += 1
                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, total_start_price, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, total_price, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, total_ex_price, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, total_end_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_cost, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_price, format_title_float)
                        col += 1
                    else:
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_cost, format_title_float)
                        col += 1
                    if self.show_worthy_resources:
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                    if self.show_product_age:
                        sheet.write(rowx, col, ' ', format_title_float)
                        col += 1
                    if self.show_reserved:
                        sheet.write(rowx, col, total_warehouse_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_warehouse_qty, format_title_float)
                        col += 1
                    if self.show_residual:
                        sheet.write(rowx, col, total_warehouse_road, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_warehouse_residual_future, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_future, format_title_float)
                        col += 1
                    if self.qty_on_hand:
                        sheet.write(rowx, col, total_start_qty_on_hand, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_start_qty_on_hand_diff, format_title_float)
                        col += 1
                    rowx += 1
                elif self.type == 'warehouse' and self.group_by == 'by_location':
                    # АГУУЛАХ -> БАЙРЛАЛААР БҮЛЭГЛЭХ - өртөгтэй үед
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        sl_with_user_lang = self.env['stock.location'].with_context(lang=self.env.user.lang).browse(line['lid'])
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            # Эхний агуулах болон байрлалыг зурна
                            if not type_id and not location_id:
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, sl_with_user_lang.name if pt_with_user_lang else line['lname'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн агуулахыг зурна
                            elif type_id and type_id != line['wcode']:
                                sub_seq = 1
                                seq = 1
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                                for h in range(col, col + self.get_remain_col_count()):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowx += 1

                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname

                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, sl_with_user_lang.name if pt_with_user_lang else line['lname'], format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, sl_with_user_lang.name if pt_with_user_lang else line['lname'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн байрлалыг зурна
                            elif location_id and location_id != line['lid']:
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                                for h in range(col, col + self.get_remain_col_count()):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowx += 1
                                rowd = rowx

                                col = self.get_all_col_count()
                                sub_seq += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, sl_with_user_lang.name if pt_with_user_lang else line['lname'], format_content_bold_left)
                                rowx += 1
                                seq = 1
                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_start_price += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['start_qty']
                            total_start_cost += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost']
                            total_qty += line['qty']
                            total_price += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['qty']
                            total_cost += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost']
                            total_ex_qty += line['ex_qty']
                            total_ex_price += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['ex_qty']
                            total_ex_cost += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost']
                            total_end_qty += line['end_qty']
                            total_end_price += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty']
                            total_end_cost += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost']
                            if self.show_reserved:
                                warehouse_reserved = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                total_warehouse_reserved_qty += warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                line_available_qty = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                total_available_warehouse_qty += line['end_qty'] - line_available_qty
                            if self.show_residual:
                                warehouse_residual = self.show_reserved_warehouse(line['pid'], line['lid'], 'c2')
                                show_reserved_qty_c1 = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                road_reserved_qty = self.show_reserved_warehouse(line['pid'], line['lid'], 'c3')
                                total_reserved_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                total_residual = warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_road += warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_residual_future += line['end_qty'] - total_reserved_c1 + total_residual
                                total_reserved_qty += road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_road_reserved = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_available_future += line['end_qty'] - total_reserved_c1 + total_residual - total_road_reserved
                            # Нэгж өртөг зурах хэсэг
                            start_unit_cost = unit_cost = ex_unit_cost = end_unit_cost = 0
                            if line['start_qty'] > 0:
                                start_unit_cost = abs(line['start_cost']) / abs(line['start_qty'])
                            if line['qty'] > 0:
                                unit_cost = abs(line['cost']) / abs(line['qty'])
                            if line['ex_qty'] > 0:
                                ex_unit_cost = abs(line['ex_cost']) / abs(line['ex_qty'])
                            if line['end_qty'] > 0:
                                end_unit_cost = abs(line['end_cost']) / abs(line['end_qty'])

                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, start_unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                            sheet.write(rowx, col, 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                            sheet.write(rowx, col, 0 if line['qty'] == 0 and zero_qty_zero_cost else line['qty'] * line['list_price'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, ex_unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                            sheet.write(rowx, col, 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_qty'] * line['list_price'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, line['end_qty'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, end_unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['list_price'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty'], format_content_float)
                                col += 1
                            else:
                                sheet.write(rowx, col, line['list_price'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty'], format_content_float)
                                col += 1

                            if self.show_worthy_resources:
                                sheet.write(rowx, col, line['worthy_balance'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - line['worthy_balance'], format_content_float)
                                col += 1
                            if self.show_product_age:
                                product_age = product_age_line[line['lid']][line['pid']] if (line['lid'] in product_age_line.keys() and line['pid'] in product_age_line[line['lid']].keys()) else 0
                                sheet.write(rowx, col, product_age, format_content_float)
                                col += 1
                            if self.show_reserved:
                                sheet.write(rowx, col, warehouse_reserved[line['pid']] if warehouse_reserved else 0, format_content_float)
                                col += 1
                                show_reserved_total = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total, format_content_float)
                                col += 1
                            if self.show_residual:
                                sheet.write(rowx, col, warehouse_residual[line['pid']] if warehouse_residual else 0, format_content_float)
                                col += 1
                                show_reserved_total_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                show_residual_total = warehouse_residual[line['pid']] if warehouse_residual else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total, format_content_float)
                                col += 1
                                sheet.write(rowx, col, road_reserved_qty[line['pid']] if road_reserved_qty else 0, format_content_float)
                                col += 1
                                road_reserved_total = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total - road_reserved_total, format_content_float)
                                col += 1

                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                total_start_qty_on_hand += quant_qty
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                total_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                col += 1

                            rowx += 1
                            seq += 1
                            type_id = line['wcode']
                            location_id = line['lid']
                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                    for h in range(col, col + self.get_remain_col_count()):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    rowx += 1

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, total_start_price, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, total_price, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, total_ex_price, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, total_end_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_cost, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_price, format_title_float)
                        col += 1
                    else:
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_price, format_title_float)
                        col += 1
                    if self.show_worthy_resources:
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                    if self.show_product_age:
                        sheet.write(rowx, col, ' ', format_title_float)
                        col += 1
                    if self.show_reserved:
                        sheet.write(rowx, col, total_warehouse_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_warehouse_qty, format_title_float)
                        col += 1
                    if self.show_residual:
                        sheet.write(rowx, col, total_warehouse_road, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_warehouse_residual_future, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_future, format_title_float)
                        col += 1
                    if self.qty_on_hand:
                        sheet.write(rowx, col, total_start_qty_on_hand, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_start_qty_on_hand_diff, format_title_float)
                        col += 1
                    rowx += 1
                elif self.type == 'account':
                    # ДАНС -> АНГИЛЛААР БҮЛЭГЛЭХ
                    subtotal_start_qty = 0
                    subtotal_start_price = 0
                    subtotal_start_cost = 0
                    subtotal_qty = 0
                    subtotal_cost = 0
                    subtotal_price = 0
                    subtotal_ex_qty = 0
                    subtotal_ex_cost = 0
                    subtotal_ex_price = 0
                    subtotal_end_qty = 0
                    subtotal_end_price = 0
                    subtotal_end_cost = 0
                    subtotal_warehouse_reserved_qty = subtotal_available_warehouse_qty = subtotal_warehouse_road = subtotal_warehouse_residual_future = 0
                    subtotal_reserved_qty = subtotal_available_future = 0
                    subtotal_start_qty_on_hand = subtotal_start_qty_on_hand_diff = 0
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(
                            line['ptid'])

                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            # Эхний агуулах/данс болон ангиллыг зурна
                            if not type_id and not category_id:
                                account_code = '[' + line['account_code'] + '] ' if line['account_code'] else ''
                                account_name = line['account_name'] if line['account_name'] else ''
                                name = account_code + account_name
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['account_name'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['cname'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx

                            # Дараагийн агуулах/дансыг зурна
                            elif type_id and type_id != line['account_code']:
                                sub_seq = 1
                                seq = 1
                                col = self.get_main_col_count()
                                coly = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Subtotal'), format_group_total)
                                for h in range(col, coly + 1):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowx += 1
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Amount'), format_group_total)
                                col += 1
                                sheet.write(rowx, col, subtotal_start_qty, format_title_float)
                                col += 1
                                if self.price_view_form == 'with_cost_price':
                                    sheet.write(rowx, col, subtotal_start_cost, format_title_float)
                                    col += 1
                                sheet.write(rowx, col, subtotal_start_price, format_title_float)
                                col += 1
                                sheet.write(rowx, col, subtotal_qty, format_title_float)
                                col += 1
                                if self.price_view_form == 'with_cost_price':
                                    sheet.write(rowx, col, subtotal_cost, format_title_float)
                                    col += 1
                                sheet.write(rowx, col, subtotal_price, format_title_float)
                                col += 1
                                sheet.write(rowx, col, subtotal_ex_qty, format_title_float)
                                col += 1
                                if self.price_view_form == 'with_cost_price':
                                    sheet.write(rowx, col, subtotal_ex_cost, format_title_float)
                                    col += 1
                                sheet.write(rowx, col, subtotal_ex_price, format_title_float)
                                col += 1
                                if self.price_view_form == 'with_cost_price':
                                    sheet.write(rowx, col, subtotal_end_qty, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, 0, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, 0, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, subtotal_end_cost, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, subtotal_end_price, format_title_float)
                                    col += 1
                                else:
                                    sheet.write(rowx, col, 0, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, subtotal_end_qty, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, subtotal_end_cost, format_title_float)
                                    col += 1
                                rowx += 1

                                account_code = '[' + line['account_code'] + '] ' if line['account_code'] else ''
                                account_name = line['account_name'] if line['account_name'] else ''
                                name = account_code + account_name
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['account_name'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['cname'],
                                                  format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                                subtotal_start_qty = 0
                                subtotal_start_price = 0
                                subtotal_start_cost = 0
                                subtotal_qty = 0
                                subtotal_cost = 0
                                subtotal_price = 0
                                subtotal_ex_qty = 0
                                subtotal_ex_cost = 0
                                subtotal_ex_price = 0
                                subtotal_end_qty = 0
                                subtotal_end_price = 0
                                subtotal_end_cost = 0
                                subtotal_warehouse_reserved_qty = subtotal_available_warehouse_qty = subtotal_warehouse_road = subtotal_warehouse_residual_future = 0
                                subtotal_reserved_qty = subtotal_available_future = 0
                                subtotal_start_qty_on_hand = subtotal_start_qty_on_hand_diff = 0

                            # Дараагийн ангиллыг зурна
                            elif category_id and category_id != line['cid']:
                                sub_seq += 1
                                col = self.get_main_col_count()
                                coly = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Subtotal'), format_group_total)
                                for h in range(col, coly + 1):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, coly, line['cname'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                                seq = 1

                            # Дэд дүнг олно
                            subtotal_start_qty += line['start_qty']
                            subtotal_start_price += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['start_qty']
                            subtotal_start_cost += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost']
                            subtotal_qty += line['qty']
                            subtotal_price += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['qty']
                            subtotal_cost += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost']
                            subtotal_ex_qty += line['ex_qty']
                            subtotal_ex_price += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['ex_qty']
                            subtotal_ex_cost += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost']
                            subtotal_end_qty += line['end_qty']
                            subtotal_end_price += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty']
                            subtotal_end_cost += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost']
                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_start_price += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['start_qty']
                            total_start_cost += 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['start_cost']
                            total_qty += line['qty']
                            total_price += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['qty']
                            total_cost += 0 if line['qty'] == 0 and zero_qty_zero_cost else line['cost']
                            total_ex_qty += line['ex_qty']
                            total_ex_price += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['ex_qty']
                            total_ex_cost += 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_cost']
                            total_end_qty += line['end_qty']
                            total_end_price += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty']
                            total_end_cost += 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost']
                            if self.show_reserved:
                                warehouse_reserved = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                total_warehouse_reserved_qty += warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                line_available_qty = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                total_available_warehouse_qty += line['end_qty'] - line_available_qty
                                subtotal_warehouse_reserved_qty += warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                subtotal_available_warehouse_qty += line['end_qty'] - line_available_qty
                            if self.show_residual:
                                warehouse_residual = self.show_reserved_warehouse(line['pid'], line['lid'], 'c2')
                                show_reserved_qty_c1 = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                road_reserved_qty = self.show_reserved_warehouse(line['pid'], line['lid'], 'c3')
                                total_reserved_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                total_residual = warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_road += warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_residual_future += line['end_qty'] - total_reserved_c1 + total_residual
                                total_reserved_qty += road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_road_reserved = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_available_future += line['end_qty'] - total_reserved_c1 + total_residual - total_road_reserved

                                subtotal_warehouse_road += warehouse_residual[line['pid']] if warehouse_residual else 0
                                subtotal_warehouse_residual_future += line['end_qty'] - total_reserved_c1 + total_residual
                                subtotal_reserved_qty += road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                subtotal_road_reserved = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                subtotal_available_future += line['end_qty'] - total_reserved_c1 + total_residual - total_road_reserved
                            # Нэгж өртөг зурах хэсэг
                            start_unit_cost = unit_cost = ex_unit_cost = end_unit_cost = 0
                            if line['start_qty'] > 0:
                                start_unit_cost = abs(line['start_cost']) / abs(line['start_qty'])
                            if line['qty'] > 0:
                                unit_cost = abs(line['cost']) / abs(line['qty'])
                            if line['ex_qty'] > 0:
                                ex_unit_cost = abs(line['ex_cost']) / abs(line['ex_qty'])
                            if line['end_qty'] > 0:
                                end_unit_cost = abs(line['end_cost']) / abs(line['end_qty'])
                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, start_unit_cost if self.env['product.product'].browse(line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                            sheet.write(rowx, col, 0 if line['start_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, unit_cost if self.env['product.product'].browse(line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                            sheet.write(rowx, col, 0 if line['qty'] == 0 and zero_qty_zero_cost else line['qty'] * line['list_price'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, ex_unit_cost if self.env['product.product'].browse(line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                            sheet.write(rowx, col, 0 if line['ex_qty'] == 0 and zero_qty_zero_cost else line['ex_qty'] * line['list_price'], format_content_float)
                            col += 1
                            if self.price_view_form == 'with_cost_price':
                                sheet.write(rowx, col, line['end_qty'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, end_unit_cost if self.env['product.product'].browse(
                                    line['pid']).cost_method != 'real' else '', format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['list_price'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['end_cost'],
                                            format_content_float)
                                col += 1
                                sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty'],
                                            format_content_float)
                                col += 1
                            else:
                                sheet.write(rowx, col, line['list_price'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else line['list_price'] * line['end_qty'],
                                            format_content_float)
                                col += 1

                            if self.show_worthy_resources:
                                sheet.write(rowx, col, line['worthy_balance'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - line['worthy_balance'], format_content_float)
                                col += 1
                            if self.show_product_age:
                                product_age = product_age_line[line['lid']][line['pid']] if (line['lid'] in product_age_line.keys() and line['pid'] in product_age_line[line['lid']].keys()) else 0
                                sheet.write(rowx, col, product_age, format_content_float)
                                col += 1
                            if self.show_reserved:
                                sheet.write(rowx, col, warehouse_reserved[line['pid']] if warehouse_reserved else 0, format_content_float)
                                col += 1
                                show_reserved_total = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total, format_content_float)
                                col += 1
                            if self.show_residual:
                                sheet.write(rowx, col, warehouse_residual[line['pid']] if warehouse_residual else 0, format_content_float)
                                col += 1
                                show_reserved_total_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                show_residual_total = warehouse_residual[line['pid']] if warehouse_residual else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total, format_content_float)
                                col += 1
                                sheet.write(rowx, col, road_reserved_qty[line['pid']] if road_reserved_qty else 0, format_content_float)
                                col += 1
                                road_reserved_total = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total - road_reserved_total, format_content_float)
                                col += 1

                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                total_start_qty_on_hand += quant_qty
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                total_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                col += 1

                            rowx += 1
                            seq += 1
                            type_id = line['account_code']
                            category_id = line['cid']

                    col = self.get_main_col_count()
                    coly = self.get_all_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Subtotal'), format_group_total)
                    for h in range(col, coly + 1):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    rowx += 1
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, total_start_price, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, total_price, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                    sheet.write(rowx, col, total_ex_price, format_title_float)
                    col += 1
                    if self.price_view_form == 'with_cost_price':
                        sheet.write(rowx, col, total_end_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_cost, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_price, format_title_float)
                        col += 1
                    else:
                        sheet.write(rowx, col, 0, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_end_cost, format_title_float)
                        col += 1
                    if self.show_worthy_resources:
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                    if self.show_product_age:
                        sheet.write(rowx, col, ' ', format_title_float)
                        col += 1
                    if self.show_reserved:
                        sheet.write(rowx, col, total_warehouse_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_warehouse_qty, format_title_float)
                        col += 1
                    if self.show_residual:
                        sheet.write(rowx, col, total_warehouse_road, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_warehouse_residual_future, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_future, format_title_float)
                        col += 1
                    if self.qty_on_hand:
                        sheet.write(rowx, col, total_start_qty_on_hand, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_start_qty_on_hand_diff, format_title_float)
                        col += 1
                    rowx += 1
            else:
                unit_cost = 0
                if self.type == 'size':
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            # Эхний размер зурна
                            if not size_id:
                                col = (10 if self.show_size else 9) if self.group_by_parent else (9 if self.show_size else 8)
                                if self.show_image:
                                    col += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['size'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн ангиллыг зурна
                            elif size_id and size_id != line['size_id']:
                                sub_seq += 1
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                                col = self.get_add_col_count(col)
                                for h in range(col, 10 if self.show_size else 9):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowx += 1
                                rowd = rowx
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                col = (10 if self.show_size else 9) if self.group_by_parent else (9 if self.show_size else 8)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['size'], format_content_bold_left)
                                rowx += 1
                                seq = 1
                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_qty += line['qty']
                            total_ex_qty += line['ex_qty']
                            total_end_qty += line['end_qty']
                            # Эцсийн үлдэгдэлийн нэгж өртөг зурах хэсэг
                            unit_cost = 0
                            if line['end_qty'] > 0:
                                unit_cost = abs(line['end_cost']) / abs(line['end_qty'])
                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['end_qty'], format_content_float)
                            col += 1

                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                col += 1

                            rowx += 1
                            seq += 1
                            size_id = line['size_id']

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                    col = self.get_add_col_count(col)
                    for h in range(col, col + 5):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    rowx += 1
                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_qty, format_title_float)
                    col += 1
                    rowx += 1
                elif self.type == 'warehouse' and self.group_by == 'by_supplier':
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            # Эхний агуулах/данс болон ангиллыг зурна
                            if not type_id and not supplier_id:
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['supp_name'] or u'Тодорхойгүй', format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн агуулах/дансыг зурна
                            elif type_id and type_id != line['wcode']:
                                sub_seq = 1
                                seq = 1
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, 4, _('Sub Total'), format_group_total)
                                coly = self.get_all_col_count()
                                for h in range(col, coly + 1):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowx += 1
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['supp_name'] or u'Тодорхойгүй', format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            elif supplier_id and supplier_id != line['supp_id']:
                                # Дараагийн ангиллыг зурна
                                sub_seq += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['supp_name'] or u'Тодорхойгүй', format_content_bold_left)
                                rowx += 1
                                seq = 1
                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_qty += line['qty']
                            total_ex_qty += line['ex_qty']
                            total_end_qty += line['end_qty']
                            if self.show_reserved:
                                warehouse_reserved = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                total_warehouse_reserved_qty += warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                line_available_qty = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                total_available_warehouse_qty += line['end_qty'] - line_available_qty
                            if self.show_residual:
                                warehouse_residual = self.show_reserved_warehouse(line['pid'], line['lid'], 'c2')
                                show_reserved_qty_c1 = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                road_reserved_qty = self.show_reserved_warehouse(line['pid'], line['lid'], 'c3')
                                total_reserved_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                total_residual = warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_road += warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_residual_future += line['end_qty'] - total_reserved_c1 + total_residual
                                total_reserved_qty += road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_road_reserved = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_available_future += line['end_qty'] - total_reserved_c1 + total_residual - total_road_reserved
                            # Эцсийн үлдэгдэлийн нэгж өртөг зурах хэсэг
                            unit_cost = 0
                            if line['end_qty'] > 0:
                                unit_cost = abs(line['end_cost']) / abs(line['end_qty'])
                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['end_qty'], format_content_float)
                            col += 1
                            if self.show_worthy_resources:
                                sheet.write(rowx, col, line['worthy_balance'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - line['worthy_balance'], format_content_float)
                                col += 1
                            if self.show_product_age:
                                product_age = product_age_line[line['lid']][line['pid']] if (line['lid'] in product_age_line.keys() and line['pid'] in product_age_line[line['lid']].keys()) else 0
                                sheet.write(rowx, col, product_age, format_content_float)
                                col += 1
                            if self.show_reserved:
                                sheet.write(rowx, col, warehouse_reserved[line['pid']] if warehouse_reserved else 0, format_content_float)
                                col += 1
                                show_reserved_total = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total, format_content_float)
                                col += 1
                            if self.show_residual:
                                sheet.write(rowx, col, warehouse_residual[line['pid']] if warehouse_residual else 0, format_content_float)
                                col += 1
                                show_reserved_total_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                show_residual_total = warehouse_residual[line['pid']] if warehouse_residual else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total, format_content_float)
                                col += 1
                                sheet.write(rowx, col, road_reserved_qty[line['pid']] if road_reserved_qty else 0, format_content_float)
                                col += 1
                                road_reserved_total = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total - road_reserved_total, format_content_float)
                                col += 1
                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                total_start_qty_on_hand += quant_qty
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                total_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                col += 1

                            rowx += 1
                            seq += 1
                            type_id = line['wcode']
                            supplier_id = line['supp_id']

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                    coly = self.get_all_col_count()
                    for h in range(col, coly + 1):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    rowx += 1
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_qty, format_title_float)
                    col += 1
                    if self.show_worthy_resources:
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                    if self.show_product_age:
                        sheet.write(rowx, col, ' ', format_title_float)
                        col += 1
                    if self.show_reserved:
                        sheet.write(rowx, col, total_warehouse_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_warehouse_qty, format_title_float)
                        col += 1
                    if self.show_residual:
                        sheet.write(rowx, col, total_warehouse_road, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_warehouse_residual_future, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_future, format_title_float)
                        col += 1
                    if self.qty_on_hand:
                        sheet.write(rowx, col, total_start_qty_on_hand, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_start_qty_on_hand_diff, format_title_float)
                        col += 1
                    rowx += 1
                elif self.type == 'account':
                    subtotal_start_qty = 0
                    subtotal_qty = 0
                    subtotal_ex_qty = 0
                    subtotal_end_qty = 0
                    subtotal_start_qty_on_hand = 0
                    subtotal_start_qty_on_hand_diff = 0
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            # Эхний агуулах болон брэндийг зурна
                            if not type_id and not category_id:
                                account_code = '[' + line['account_code'] + '] ' if line['account_code'] else ''
                                account_name = line['account_name'] if line['account_name'] else ''
                                name = account_code + account_name
                                col = self.get_all_col_count()
                                if self.show_image:
                                    col += 1
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['account_name'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['cname'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн агуулах болон брэндийг зурна
                            elif type_id and type_id != line['account_code']:
                                sub_seq = 1
                                seq = 1

                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Subtotal'), format_group_total)
                                for h in range(col, col + self.get_remain_col_count()):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowx += 1
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Amount'), format_group_total)
                                col += 1
                                sheet.write(rowx, col, subtotal_start_qty, format_title_float)
                                col += 1
                                sheet.write(rowx, col, subtotal_qty, format_title_float)
                                col += 1
                                sheet.write(rowx, col, subtotal_ex_qty, format_title_float)
                                col += 1
                                sheet.write(rowx, col, subtotal_end_qty, format_title_float)
                                col += 1
                                if self.qty_on_hand:
                                    sheet.write(rowx, col, subtotal_start_qty_on_hand, format_title_float)
                                    col += 1
                                    sheet.write(rowx, col, subtotal_start_qty_on_hand_diff, format_title_float)
                                    col += 1
                                rowx += 1

                                account_code = '[' + line['account_code'] + '] ' if line['account_code'] else ''
                                account_name = line['account_name'] if line['account_name'] else ''
                                name = account_code + account_name

                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['account_name'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['cname'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                                subtotal_start_qty = 0
                                subtotal_qty = 0
                                subtotal_ex_qty = 0
                                subtotal_end_qty = 0
                                subtotal_start_qty_on_hand = 0
                                subtotal_start_qty_on_hand_diff = 0
                            # Дараагийн брэндийг зурна
                            elif category_id and category_id != line['cid']:
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Subtotal'), format_group_total)
                                for h in range(col, col + self.get_remain_col_count()):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowx += 1

                                col = self.get_all_col_count()
                                sub_seq += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['cname'], format_content_bold_left)
                                rowd = rowx
                                rowx += 1
                                seq = 1
                            # Дэд дүнг олно
                            subtotal_start_qty += line['start_qty']
                            subtotal_qty += line['qty']
                            subtotal_ex_qty += line['ex_qty']
                            subtotal_end_qty += line['end_qty']
                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_qty += line['qty']
                            total_ex_qty += line['ex_qty']
                            total_end_qty += line['end_qty']
                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['end_qty'], format_content_float)
                            col += 1

                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (
                                    line['lid'] in quant.keys() and line['pid'] in quant[
                                        line['lid']].keys()) else 0
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                total_start_qty_on_hand += quant_qty
                                subtotal_start_qty_on_hand += quant_qty
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                total_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                subtotal_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                col += 1

                            if self.show_current_cost:
                                if is_cost_for_each_wh.state == 'installed':
                                    wh_id = warehouse_dic[line['lid']] if line['lid'] in warehouse_dic.keys() else None

                                    if wh_id:
                                        cost = current_cost[line['ptid']][wh_id] if (line['ptid'] in current_cost.keys() and wh_id in current_cost[line['ptid']].keys()) else 0
                                        sheet.write(rowx, col, cost if line['end_qty'] != 0 else 0, format_content_float)
                                        col += 1
                                        sheet.write(rowx, col, (unit_cost - cost) if line['end_qty'] != 0 else 0, format_content_float)
                                        col += 1
                                    else:
                                        sheet.write(rowx, col, 0, format_content_float)
                                        col += 1
                                        sheet.write(rowx, col, 0, format_content_float)
                                        col += 1
                                else:
                                    sheet.write(rowx, col, current_cost[line['pid']] if line['end_qty'] != 0 and line['pid'] in current_cost.keys() else 0, format_content_float)
                                    col += 1
                                    sheet.write(rowx, col, (unit_cost - current_cost[line['pid']]) if line['end_qty'] != 0 and line['pid'] in current_cost.keys() else 0, format_content_float)
                                    col += 1

                            rowx += 1
                            seq += 1
                            type_id = line['account_code']
                            category_id = line['cid']

                    col = self.get_main_col_count()
                    coly = self.get_all_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Subtotal'), format_group_total)
                    for h in range(col, coly + 1):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    if self.qty_on_hand:
                        sheet.write(rowx, col, subtotal_start_qty_on_hand, format_title_float)
                        col += 1
                        sheet.write(rowx, col, subtotal_start_qty_on_hand_diff, format_title_float)
                        col += 1
                    rowx += 1

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_qty, format_title_float)
                    col += 1
                    if self.qty_on_hand:
                        sheet.write(rowx, col, total_start_qty_on_hand, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_start_qty_on_hand_diff, format_title_float)
                        col += 1
                    rowx += 1
                elif self.type == 'supplier':
                    # НИЙЛҮҮЛЭГЧЭЭР БҮЛЭГЛЭХ
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            # Эхний нийлүүлэгчийг ангиллыг зурна
                            if not supplier_id and line['supp_id']:
                                col = (15 if self.show_size else 14) if self.group_by_parent else (14 if self.show_size else 13)
                                if self.show_image:
                                    col += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['supp_name'] or u'Тодорхойгүй', format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн нийлүүлэгчийг зурна
                            elif supplier_id and supplier_id != line['supp_id']:
                                col = (15 if self.show_size else 14) if self.group_by_parent else (14 if self.show_size else 13)
                                if self.show_image:
                                    col += 1
                                sub_seq += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['supp_name'] or u'Тодорхойгүй', format_content_bold_left)
                                rowx += 1
                                seq = 1
                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_qty += line['qty']
                            total_ex_qty += line['ex_qty']
                            total_end_qty += line['end_qty']
                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['end_qty'], format_content_float)
                            col += 1
                            rowx += 1
                            seq += 1
                            supplier_id = line['supp_id']

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                    col = self.get_add_col_count(col)
                    for h in range(col, col + 5):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    rowx += 1
                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_qty, format_title_float)
                    col += 1
                    rowx += 1
                elif self.type == 'warehouse' and self.group_by == 'by_category':
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            # Эхний ангиллыг зурна
                            if not category_id:
                                if self.group_by_parent:
                                    parent_total_start_qty = parent_total_qty = parent_total_ex_qty = parent_total_end_qty = 0
                                    col = (6 if self.show_size else 5) if self.group_by_parent else (5 if self.show_size else 4)
                                    sheet.merge_range(rowx, 0, rowx, col, categ_parent_dic[line['cid']]['name'], format_content_bold_left)
                                    parent_categ_row = rowx
                                    rowx += 1
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['cname'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx - 1
                            # Дараагийн ангиллыг зурна
                            elif warehouse_code != line['wcode'] or category_id and category_id != line['cid']:
                                sub_seq += 1
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                                coly = self.get_all_col_count()
                                for h in range(col, coly + 1):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd + 1, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                warehouse_total_rows.append(rowx)
                                rowx += 1
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                rowd = rowx
                                col = (6 if self.show_size else 5) if self.group_by_parent else (5 if self.show_size else 4)
                                if self.group_by_parent and parent_category_id and parent_category_id != categ_parent_dic[line['cid']]['id']:
                                    # Эцэг ангиллаар бүлэглэсэн тохиолдолд
                                    col_parent = col
                                    sheet.write(parent_categ_row, col_parent + 1, parent_total_start_qty, format_title_float)
                                    sheet.write(parent_categ_row, col_parent + 2, parent_total_qty, format_title_float)
                                    sheet.write(parent_categ_row, col_parent + 3, parent_total_ex_qty, format_title_float)
                                    sheet.write(parent_categ_row, col_parent + 4, parent_total_end_qty, format_title_float)

                                    parent_total_start_qty = parent_total_qty = parent_total_ex_qty = parent_total_end_qty = 0
                                    sheet.merge_range(rowx, 0, rowx, col, categ_parent_dic[line['cid']]['name'], format_content_bold_left)
                                    parent_categ_row = rowx
                                    rowx += 1
                                if warehouse_code != line['wcode']:
                                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col,
                                                      _('Warehouse Total'),
                                                      format_group_total)
                                    for h in range(col, coly + 1):
                                        sum_rows = []
                                        for r in warehouse_total_rows:
                                            sum_rows.append('' + xl_rowcol_to_cell(r, h) + '')
                                        sheet.write_formula(rowx, h,
                                                            '{=SUM(' + ','.join(map(str, sum_rows)) + ')}',
                                                            format_group_float)
                                    rowx += 1
                                    warehouse_total_rows = []
                                    col = self.get_all_col_count()
                                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                    rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['cname'], format_content_bold_left)
                                rowx += 1
                                seq = 1
                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_qty += line['qty']
                            total_ex_qty += line['ex_qty']
                            total_end_qty += line['end_qty']
                            if self.show_reserved:
                                warehouse_reserved = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                total_warehouse_reserved_qty += warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                line_available_qty = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                total_available_warehouse_qty += line['end_qty'] - line_available_qty
                            if self.show_residual:
                                warehouse_residual = self.show_reserved_warehouse(line['pid'], line['lid'], 'c2')
                                show_reserved_qty_c1 = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                road_reserved_qty = self.show_reserved_warehouse(line['pid'], line['lid'], 'c3')
                                total_reserved_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                total_residual = warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_road += warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_residual_future += line['end_qty'] - total_reserved_c1 + total_residual
                                total_reserved_qty += road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_road_reserved = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_available_future += line['end_qty'] - total_reserved_c1 + total_residual - total_road_reserved
                            # Эцсийн үлдэгдэлийн нэгж өртөг зурах хэсэг
                            unit_cost = 0
                            if line['end_qty'] > 0:
                                unit_cost = abs(line['end_cost']) / abs(line['end_qty'])
                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['end_qty'], format_content_float)
                            col += 1
                            if self.show_worthy_resources:
                                sheet.write(rowx, col, line['worthy_balance'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - line['worthy_balance'], format_content_float)
                                col += 1
                            if self.show_product_age:
                                product_age = product_age_line[line['lid']][line['pid']] if (line['lid'] in product_age_line.keys() and line['pid'] in product_age_line[line['lid']].keys()) else 0
                                sheet.write(rowx, col, product_age, format_content_float)
                                col += 1
                            if self.show_reserved:
                                sheet.write(rowx, col, warehouse_reserved[line['pid']] if warehouse_reserved else 0, format_content_float)
                                col += 1
                                show_reserved_total = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total, format_content_float)
                                col += 1
                            if self.show_residual:
                                sheet.write(rowx, col, warehouse_residual[line['pid']] if warehouse_residual else 0, format_content_float)
                                col += 1
                                show_reserved_total_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                show_residual_total = warehouse_residual[line['pid']] if warehouse_residual else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total, format_content_float)
                                col += 1
                                sheet.write(rowx, col, road_reserved_qty[line['pid']] if road_reserved_qty else 0, format_content_float)
                                col += 1
                                road_reserved_total = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total - road_reserved_total, format_content_float)
                                col += 1
                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                total_start_qty_on_hand += quant_qty
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                total_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                col += 1
                            rowx += 1
                            seq += 1
                            category_id = line['cid']
                            warehouse_code = line['wcode']
                            if self.group_by_parent:
                                parent_category_id = categ_parent_dic[line['cid']]['id']

                            parent_total_start_qty += line['start_qty']
                            parent_total_qty += line['qty']
                            parent_total_ex_qty += line['ex_qty']
                            parent_total_end_qty += line['end_qty']

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                    coly = self.get_all_col_count()
                    for h in range(col, coly + 1):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    warehouse_total_rows.append(rowx)
                    rowx += 1
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Warehouse Total'),
                                      format_group_total)
                    for h in range(col, coly + 1):
                        sum_rows = []
                        for r in warehouse_total_rows:
                            sum_rows.append('' + xl_rowcol_to_cell(r, h) + '')
                        sheet.write_formula(rowx, h,
                                            '{=SUM(' + ','.join(map(str, sum_rows)) + ')}', format_group_float)
                    rowx += 1
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_qty, format_title_float)
                    col += 1
                    if self.show_worthy_resources:
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                    if self.show_product_age:
                        sheet.write(rowx, col, ' ', format_title_float)
                        col += 1
                    if self.show_reserved:
                        sheet.write(rowx, col, total_warehouse_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_warehouse_qty, format_title_float)
                        col += 1
                    if self.show_residual:
                        sheet.write(rowx, col, total_warehouse_road, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_warehouse_residual_future, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_future, format_title_float)
                        col += 1
                    if self.qty_on_hand:
                        sheet.write(rowx, col, total_start_qty_on_hand, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_start_qty_on_hand_diff, format_title_float)
                        col += 1
                    rowx += 1

                    if self.group_by_parent:
                        sheet.write(parent_categ_row, col_parent + 1, parent_total_start_qty, format_title_float)
                        sheet.write(parent_categ_row, col_parent + 2, parent_total_qty, format_title_float)
                        sheet.write(parent_categ_row, col_parent + 3, parent_total_ex_qty, format_title_float)
                        sheet.write(parent_categ_row, col_parent + 4, parent_total_end_qty, format_title_float)
                    parent_total_start_qty += total_start_qty
                    parent_total_qty += total_qty
                    parent_total_ex_qty += total_ex_qty
                    parent_total_end_qty += total_end_qty
                elif self.type == 'warehouse' and self.group_by == 'by_size':
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            # Эхний агуулах/размер болон ангиллыг зурна
                            if not type_id and not category_id:
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['size'] or u'Тодорхойгүй', format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн агуулах/размер зурна
                            elif type_id and type_id != line['wcode']:
                                sub_seq = 1
                                seq = 1
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                                coly = self.get_all_col_count()
                                for h in range(col, coly + 1):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowx += 1
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['size'] or u'Тодорхойгүй', format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн ангиллыг зурна
                            elif size_id and size_id != line['size_id']:
                                sub_seq += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['size'] or u'Тодорхойгүй', format_content_bold_left)
                                rowx += 1
                                seq = 1
                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_qty += line['qty']
                            total_ex_qty += line['ex_qty']
                            total_end_qty += line['end_qty']
                            if self.show_reserved:
                                warehouse_reserved = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                total_warehouse_reserved_qty += warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                line_available_qty = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                total_available_warehouse_qty += line['end_qty'] - line_available_qty
                            if self.show_residual:
                                warehouse_residual = self.show_reserved_warehouse(line['pid'], line['lid'], 'c2')
                                show_reserved_qty_c1 = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                road_reserved_qty = self.show_reserved_warehouse(line['pid'], line['lid'], 'c3')
                                total_reserved_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                total_residual = warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_road += warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_residual_future += line['end_qty'] - total_reserved_c1 + total_residual
                                total_reserved_qty += road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_road_reserved = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_available_future += line['end_qty'] - total_reserved_c1 + total_residual - total_road_reserved
                            # Эцсийн үлдэгдэлийн нэгж өртөг зурах хэсэг
                            unit_cost = 0
                            if line['end_qty'] > 0:
                                unit_cost = abs(line['end_cost']) / abs(line['end_qty'])
                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['end_qty'], format_content_float)
                            col += 1
                            if self.show_worthy_resources:
                                sheet.write(rowx, col, line['worthy_balance'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - line['worthy_balance'], format_content_float)
                                col += 1
                            if self.show_product_age:
                                product_age = product_age_line[line['lid']][line['pid']] if (line['lid'] in product_age_line.keys() and line['pid'] in product_age_line[line['lid']].keys()) else 0
                                sheet.write(rowx, col, product_age, format_content_float)
                                col += 1
                            if self.show_reserved:
                                sheet.write(rowx, col, warehouse_reserved[line['pid']] if warehouse_reserved else 0, format_content_float)
                                col += 1
                                show_reserved_total = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total, format_content_float)
                                col += 1
                            if self.show_residual:
                                sheet.write(rowx, col, warehouse_residual[line['pid']] if warehouse_residual else 0, format_content_float)
                                col += 1
                                show_reserved_total_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                show_residual_total = warehouse_residual[line['pid']] if warehouse_residual else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total, format_content_float)
                                col += 1
                                sheet.write(rowx, col, road_reserved_qty[line['pid']] if road_reserved_qty else 0, format_content_float)
                                col += 1
                                road_reserved_total = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total - road_reserved_total, format_content_float)
                                col += 1
                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                total_start_qty_on_hand += quant_qty
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                total_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                col += 1

                            rowx += 1
                            seq += 1
                            type_id = line['wcode']
                            size_id = line['size_id']

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                    coly = self.get_all_col_count()
                    for h in range(col, coly + 1):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    rowx += 1
                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_qty, format_title_float)
                    col += 1
                    if self.show_worthy_resources:
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                    if self.show_product_age:
                        sheet.write(rowx, col, ' ', format_title_float)
                        col += 1
                    if self.show_reserved:
                        sheet.write(rowx, col, total_warehouse_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_warehouse_qty, format_title_float)
                        col += 1
                    if self.show_residual:
                        sheet.write(rowx, col, total_warehouse_road, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_warehouse_residual_future, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_future, format_title_float)
                        col += 1
                    if self.qty_on_hand:
                        sheet.write(rowx, col, total_start_qty_on_hand, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_start_qty_on_hand_diff, format_title_float)
                        col += 1
                    rowx += 1
                elif self.type == 'warehouse' and self.group_by == 'by_location':
                    # ###################### START: ӨРТӨГ/ХУДАЛДАХ ҮНЭ чеклээгүй -> (Агуулах > Байрлал) -р бүлэглэх  #######################
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        sl_with_user_lang = self.env['stock.location'].with_context(lang=self.env.user.lang).browse(line['lid'])
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            if not type_id and not location_id:
                                # Эхний агуулах болон байрлалыг зурна
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or sl_with_user_lang.name if pt_with_user_lang else line['lname'], format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, sl_with_user_lang.name if pt_with_user_lang else line['lname'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            elif type_id and type_id != line['wcode']:
                                # Дараагийн агуулах болон байрлалыг зурна
                                sub_seq = 1
                                seq = 1
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                                for h in range(col, col + self.get_remain_col_count()):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowx += 1
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname

                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or sl_with_user_lang.name if pt_with_user_lang else line['lname'], format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, sl_with_user_lang.name if pt_with_user_lang else line['lname'], format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            elif location_id and location_id != line['lid']:
                                # Дараагийн байрлалыг зурна
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                                for h in range(col, col + self.get_remain_col_count()):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowx += 1

                                col = self.get_all_col_count()
                                sub_seq += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, sl_with_user_lang.name if pt_with_user_lang else line['lname'], format_content_bold_left)
                                rowd = rowx
                                rowx += 1
                                seq = 1
                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_qty += line['qty']
                            total_ex_qty += line['ex_qty']
                            total_end_qty += line['end_qty']
                            if self.show_reserved:
                                warehouse_reserved = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                total_warehouse_reserved_qty += warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                line_available_qty = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                total_available_warehouse_qty += line['end_qty'] - line_available_qty
                            if self.show_residual:
                                warehouse_residual = self.show_reserved_warehouse(line['pid'], line['lid'], 'c2')
                                show_reserved_qty_c1 = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                road_reserved_qty = self.show_reserved_warehouse(line['pid'], line['lid'], 'c3')
                                total_reserved_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                total_residual = warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_road += warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_residual_future += line['end_qty'] - total_reserved_c1 + total_residual
                                total_reserved_qty += road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_road_reserved = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_available_future += line['end_qty'] - total_reserved_c1 + total_residual - total_road_reserved
                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['end_qty'], format_content_float)
                            col += 1
                            if self.show_worthy_resources:
                                sheet.write(rowx, col, line['worthy_balance'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - line['worthy_balance'], format_content_float)
                                col += 1
                            if self.show_product_age:
                                product_age = product_age_line[line['lid']][line['pid']] if (line['lid'] in product_age_line.keys() and line['pid'] in product_age_line[line['lid']].keys()) else 0
                                sheet.write(rowx, col, product_age, format_content_float)
                                col += 1
                            if self.show_reserved:
                                sheet.write(rowx, col, warehouse_reserved[line['pid']] if warehouse_reserved else 0, format_content_float)
                                col += 1
                                show_reserved_total = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total, format_content_float)
                                col += 1
                            if self.show_residual:
                                sheet.write(rowx, col, warehouse_residual[line['pid']] if warehouse_residual else 0, format_content_float)
                                col += 1
                                show_reserved_total_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                show_residual_total = warehouse_residual[line['pid']] if warehouse_residual else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total, format_content_float)
                                col += 1
                                sheet.write(rowx, col, road_reserved_qty[line['pid']] if road_reserved_qty else 0, format_content_float)
                                col += 1
                                road_reserved_total = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total - road_reserved_total, format_content_float)
                                col += 1

                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                total_start_qty_on_hand += quant_qty
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                total_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                col += 1

                            if self.show_current_cost:
                                if is_cost_for_each_wh.state == 'installed':
                                    wh_id = warehouse_dic[line['lid']] if line['lid'] in warehouse_dic.keys() else None

                                    if wh_id:
                                        cost = current_cost[line['ptid']][wh_id] if (line['ptid'] in current_cost.keys() and wh_id in current_cost[line['ptid']].keys()) else 0
                                        sheet.write(rowx, col, cost if line['end_qty'] != 0 else 0, format_content_float)
                                        col += 1
                                        sheet.write(rowx, col, (unit_cost - cost) if line['end_qty'] != 0 else 0, format_content_float)
                                        col += 1
                                    else:
                                        sheet.write(rowx, col, 0, format_content_float)
                                        col += 1
                                        sheet.write(rowx, col, 0, format_content_float)
                                        col += 1
                                else:
                                    sheet.write(rowx, col, current_cost[line['pid']] if line['end_qty'] != 0 and line['pid'] in current_cost.keys() else 0, format_content_float)
                                    col += 1
                                    sheet.write(rowx, col, (unit_cost - current_cost[line['pid']]) if line['end_qty'] != 0 and line['pid'] in current_cost.keys() else 0, format_content_float)
                                    col += 1

                            rowx += 1
                            seq += 1
                            type_id = line['wcode']
                            location_id = line['lid']

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                    for h in range(col, col + self.get_remain_col_count()):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    rowx += 1

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_qty, format_title_float)
                    col += 1
                    if self.show_worthy_resources:
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                    if self.show_product_age:
                        sheet.write(rowx, col, ' ', format_title_float)
                        col += 1
                    if self.show_reserved:
                        sheet.write(rowx, col, total_warehouse_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_warehouse_qty, format_title_float)
                        col += 1
                    if self.show_residual:
                        sheet.write(rowx, col, total_warehouse_road, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_warehouse_residual_future, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_future, format_title_float)
                        col += 1
                    if self.qty_on_hand:
                        sheet.write(rowx, col, total_start_qty_on_hand, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_start_qty_on_hand_diff, format_title_float)
                        col += 1
                    rowx += 1
                elif self.type == 'warehouse' and self.group_by == 'by_brand':
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            # Эхний агуулах болон брэндийг зурна
                            if not type_id and not brand_id:
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                if line['brand_name']:
                                    brand_name = line['brand_name']
                                else:
                                    brand_name = _('Not defined')
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, brand_name, format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн агуулах болон брэндийг зурна
                            elif type_id and type_id != line['wcode']:
                                sub_seq = 1
                                seq = 1
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                                coly = self.get_all_col_count()
                                for h in range(col, coly + 1):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                warehouse_total_rows.append(rowx)
                                rowx += 1
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col,
                                                  _('Warehouse Total'),
                                                  format_group_total)
                                for h in range(col, coly + 1):
                                    sum_rows = []
                                    for r in warehouse_total_rows:
                                        sum_rows.append('' + xl_rowcol_to_cell(r, h) + '')
                                    sheet.write_formula(rowx, h,
                                                        '{=SUM(' + ','.join(map(str, sum_rows)) + ')}',
                                                        format_group_float)
                                rowx += 1
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                warehouse_total_rows = []
                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                if line['brand_name']:
                                    brand_name = line['brand_name']
                                else:
                                    brand_name = _('Not defined')
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, brand_name, format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            elif brand_id and brand_id != line['brand_id']:
                                # Дараагийн брэндийг зурна
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                                coly = self.get_all_col_count()
                                for h in range(col, coly + 1):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                warehouse_total_rows.append(rowx)
                                rowx += 1
                                col = self.get_all_col_count()
                                sub_seq += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                if line['brand_name']:
                                    brand_name = line['brand_name']
                                else:
                                    brand_name = _('Not defined')
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, brand_name, format_content_bold_left)
                                rowd = rowx
                                rowx += 1
                                seq = 1
                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_qty += line['qty']
                            total_ex_qty += line['ex_qty']
                            total_end_qty += line['end_qty']
                            if self.show_reserved:
                                warehouse_reserved = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                total_warehouse_reserved_qty += warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                line_available_qty = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                total_available_warehouse_qty += line['end_qty'] - line_available_qty
                            if self.show_residual:
                                warehouse_residual = self.show_reserved_warehouse(line['pid'], line['lid'], 'c2')
                                show_reserved_qty_c1 = self.show_reserved_warehouse(line['pid'], line['lid'], 'c1')
                                road_reserved_qty = self.show_reserved_warehouse(line['pid'], line['lid'], 'c3')
                                total_reserved_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                total_residual = warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_road += warehouse_residual[line['pid']] if warehouse_residual else 0
                                total_warehouse_residual_future += line['end_qty'] - total_reserved_c1 + total_residual
                                total_reserved_qty += road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_road_reserved = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                total_available_future += line['end_qty'] - total_reserved_c1 + total_residual - total_road_reserved
                            # Эцсийн үлдэгдэлийн нэгж өртөг зурах хэсэг
                            unit_cost = 0
                            if line['end_qty'] > 0:
                                unit_cost = abs(line['end_cost']) / abs(line['end_qty'])
                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['end_qty'], format_content_float)
                            col += 1
                            if self.show_worthy_resources:
                                sheet.write(rowx, col, line['worthy_balance'], format_content_float)
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - line['worthy_balance'], format_content_float)
                                col += 1
                            if self.show_product_age:
                                product_age = product_age_line[line['lid']][line['pid']] if (line['lid'] in product_age_line.keys() and line['pid'] in product_age_line[line['lid']].keys()) else 0
                                sheet.write(rowx, col, product_age, format_content_float)
                                col += 1
                            if self.show_reserved:
                                sheet.write(rowx, col, warehouse_reserved[line['pid']] if warehouse_reserved else 0, format_content_float)
                                col += 1
                                show_reserved_total = warehouse_reserved[line['pid']] if warehouse_reserved else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total, format_content_float)
                                col += 1
                            if self.show_residual:
                                sheet.write(rowx, col, warehouse_residual[line['pid']] if warehouse_residual else 0, format_content_float)
                                col += 1
                                show_reserved_total_c1 = show_reserved_qty_c1[line['pid']] if show_reserved_qty_c1 else 0
                                show_residual_total = warehouse_residual[line['pid']] if warehouse_residual else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total, format_content_float)
                                col += 1
                                sheet.write(rowx, col, road_reserved_qty[line['pid']] if road_reserved_qty else 0, format_content_float)
                                col += 1
                                road_reserved_total = road_reserved_qty[line['pid']] if road_reserved_qty else 0
                                sheet.write(rowx, col, line['end_qty'] - show_reserved_total_c1 + show_residual_total - road_reserved_total, format_content_float)
                                col += 1
                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                total_start_qty_on_hand += quant_qty
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                total_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                col += 1

                            rowx += 1
                            seq += 1
                            type_id = line['wcode']
                            brand_id = line['brand_id']

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Sub Total'), format_group_total)
                    coly = self.get_all_col_count()
                    for h in range(col, coly + 1):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    warehouse_total_rows.append(rowx)
                    rowx += 1
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Warehouse Total'),
                                      format_group_total)
                    for h in range(col, coly + 1):
                        sum_rows = []
                        for r in warehouse_total_rows:
                            sum_rows.append('' + xl_rowcol_to_cell(r, h) + '')
                        sheet.write_formula(rowx, h,
                                            '{=SUM(' + ','.join(map(str, sum_rows)) + ')}', format_group_float)
                    rowx += 1
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_qty, format_title_float)
                    col += 1
                    if self.show_worthy_resources:
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                        sheet.write(rowx, col, ' ', format_content_float)
                        col += 1
                    if self.show_product_age:
                        sheet.write(rowx, col, ' ', format_title_float)
                        col += 1
                    if self.show_reserved:
                        sheet.write(rowx, col, total_warehouse_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_warehouse_qty, format_title_float)
                        col += 1
                    if self.show_residual:
                        sheet.write(rowx, col, total_warehouse_road, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_warehouse_residual_future, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_reserved_qty, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_available_future, format_title_float)
                        col += 1
                    if self.qty_on_hand:
                        sheet.write(rowx, col, total_start_qty_on_hand, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_start_qty_on_hand_diff, format_title_float)
                        col += 1
                    rowx += 1
                elif self.type == 'warehouse' and self.group_by == 'by_account':
                    subtotal_start_qty = 0
                    subtotal_qty = 0
                    subtotal_ex_qty = 0
                    subtotal_end_qty = 0
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        account_name = line['account_code'] + ' ' + line['account_name']
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            # Эхний агуулах болон данс зурна
                            if not type_id and not account_id:
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname
                                col = self.get_all_col_count()
                                if self.show_image:
                                    col += 1
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, account_name, format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            elif type_id and type_id != line['wcode']:
                                # Дараагийн агуулах болон данс зурна
                                sub_seq = 1
                                seq = 1

                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Subtotal'), format_group_total)
                                for h in range(col, col + self.get_remain_col_count()):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowx += 1
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Amount'), format_group_total)
                                col += 1
                                sheet.write(rowx, col, subtotal_start_qty, format_title_float)
                                col += 1
                                sheet.write(rowx, col, subtotal_qty, format_title_float)
                                col += 1
                                sheet.write(rowx, col, subtotal_ex_qty, format_title_float)
                                col += 1
                                sheet.write(rowx, col, subtotal_end_qty, format_title_float)
                                col += 1
                                rowx += 1
                                rowd += 1
                                wcode = '[' + line['wcode'] + '] ' if line['wcode'] else ''
                                wname = line['wname'] if line['wname'] else ''
                                name = wcode + wname

                                col = self.get_all_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, name or line['wname'] or '', format_group_left)
                                rowx += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, account_name, format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                                subtotal_start_qty = 0
                                subtotal_qty = 0
                                subtotal_ex_qty = 0
                                subtotal_end_qty = 0
                            # Дараагийн дансийг зурна
                            elif account_id and account_id != line['account_id']:
                                col = self.get_main_col_count()
                                sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Subtotal'), format_group_total)
                                for h in range(col, col + self.get_remain_col_count()):
                                    sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                                rowx += 1
                                rowd = rowx + 1

                                col = self.get_all_col_count()
                                sub_seq += 1
                                sheet.write(rowx, 1 if self.group_by_parent else 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, account_name, format_content_bold_left)
                                rowd = rowx
                                rowx += 1
                                seq = 1
                            # Дэд дүнг олно
                            subtotal_start_qty += line['start_qty']
                            subtotal_qty += line['qty']
                            subtotal_ex_qty += line['ex_qty']
                            subtotal_end_qty += line['end_qty']
                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_qty += line['qty']
                            total_ex_qty += line['ex_qty']
                            total_end_qty += line['end_qty']
                            # Барааны мөрийг зурна
                            col = 1 if self.group_by_parent else 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['end_qty'], format_content_float)
                            col += 1

                            if self.qty_on_hand:
                                quant_qty = quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                                sheet.write(rowx, col, quant_qty, format_content_float)
                                total_start_qty_on_hand += quant_qty
                                col += 1
                                sheet.write(rowx, col, line['end_qty'] - quant_qty, format_content_float)
                                total_start_qty_on_hand_diff += (line['end_qty'] - quant_qty)
                                col += 1

                            if self.show_current_cost:
                                if is_cost_for_each_wh.state == 'installed':
                                    wh_id = warehouse_dic[line['lid']] if line['lid'] in warehouse_dic.keys() else None

                                    if wh_id:
                                        cost = current_cost[line['ptid']][wh_id] if (line['ptid'] in current_cost.keys() and wh_id in current_cost[line['ptid']].keys()) else 0
                                        sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else cost, format_content_float)
                                        col += 1
                                        sheet.write(rowx, col, 0 if line['end_qty'] == 0 and zero_qty_zero_cost else (unit_cost - cost), format_content_float)
                                        col += 1
                                    else:
                                        sheet.write(rowx, col, 0, format_content_float)
                                        col += 1
                                        sheet.write(rowx, col, 0, format_content_float)
                                        col += 1
                                else:
                                    sheet.write(rowx, col, current_cost[line['pid']] if line['end_qty'] != 0 and line[
                                        'pid'] in current_cost.keys() else 0, format_content_float)
                                    col += 1
                                    sheet.write(rowx, col,
                                                (unit_cost - current_cost[line['pid']]) if line['end_qty'] != 0 and line['pid'] in current_cost.keys() else 0, format_content_float)
                                    col += 1

                            rowx += 1
                            seq += 1
                            type_id = line['wcode']
                            account_id = line['account_id']

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Subtotal'), format_group_total)
                    for h in range(col, col + self.get_remain_col_count()):
                        sheet.write_formula(rowx, h, '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(rowx - 1, h) + ')}', format_group_float)
                    rowx += 1
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Amount'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, subtotal_start_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, subtotal_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, subtotal_ex_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, subtotal_end_qty, format_title_float)
                    col += 1

                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 1 if self.group_by_parent else 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_qty, format_title_float)
                    col += 1
                    if self.qty_on_hand:
                        sheet.write(rowx, col, total_start_qty_on_hand, format_title_float)
                        col += 1
                        sheet.write(rowx, col, total_start_qty_on_hand_diff, format_title_float)
                        col += 1
                    rowx += 1
                elif self.type == 'category':
                    for line in lines:
                        pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                        if not self.show_only_minus or (self.show_only_minus and line['end_qty'] < 0):
                            # Эхний нийлүүлэгчийг ангиллыг зурна
                            if not category_id and line['cid']:
                                col = self.get_main_col_count()
                                sheet.write(rowx, 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['cname'] or u'Тодорхойгүй', format_content_bold_left)
                                rowx += 1
                                rowd = rowx
                            # Дараагийн нийлүүлэгчийг зурна
                            elif category_id and category_id != line['cid']:
                                col = self.get_main_col_count()
                                sub_seq += 1
                                sheet.write(rowx, 0, sub_seq, format_content_bold_number)
                                sheet.merge_range(rowx, 2 if self.group_by_parent else 1, rowx, col, line['cname'] or u'Тодорхойгүй', format_content_bold_left)
                                rowx += 1
                                seq = 1
                            # Нийт дүнг олно
                            total_start_qty += line['start_qty']
                            total_qty += line['qty']
                            total_ex_qty += line['ex_qty']
                            total_end_qty += line['end_qty']
                            # Барааны мөрийг зурна
                            col = 0
                            sheet.write(rowx, col, '%s.%s' % (sub_seq, seq), format_content_center)
                            col += 1
                            if self.show_image:
                                sheet.insert_image(rowx, col, image_path, {'image_data': image_obj[line['pid']], 'x_scale': 1, 'y_scale': 1, 'locked': True})
                                col += 1
                                if image_obj[line['pid']]:
                                    sheet.set_row(rowx, 95)
                            sheet.write(rowx, col, line['code'], format_content_center)
                            col += 1
                            if self.show_barcode:
                                sheet.write(rowx, col, line['barcode'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, pt_with_user_lang.name if pt_with_user_lang else line['name'], format_content_text)
                            col += 1
                            sheet.write(rowx, col, line['uom_name'], format_content_center)
                            col += 1
                            if self.show_size:
                                sheet.write(rowx, col, line['size'], format_content_center)
                                col += 1
                            sheet.write(rowx, col, line['start_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['ex_qty'], format_content_float)
                            col += 1
                            sheet.write(rowx, col, line['end_qty'], format_content_float)
                            col += 1
                            rowx += 1
                            seq += 1
                            category_id = line['cid']
                    col = self.get_main_col_count()
                    sheet.merge_range(rowx, 0, rowx, col, _('Total'), format_group_total)
                    col += 1
                    sheet.write(rowx, col, total_start_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_ex_qty, format_title_float)
                    col += 1
                    sheet.write(rowx, col, total_end_qty, format_title_float)
                    col += 1
                    rowx += 1

        rowx += 3
        sheet.write(rowx, 3, u'Эд хариуцагч:', format_filter_right)
        sheet.merge_range(rowx, 4, rowx, 5, u'...................................................../', format_filter)
        sheet.write(rowx, 6, u'/', format_filter_right)
        rowx += 1
        sheet.write(rowx, 3, u'Хянасан нягтлан бодогч:', format_filter_right)
        sheet.merge_range(rowx, 4, rowx, 5, u'...................................................../', format_filter)
        sheet.write(rowx, 6, u'/', format_filter_right)

        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report()
