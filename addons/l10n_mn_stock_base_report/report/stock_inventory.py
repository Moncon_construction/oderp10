# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools

class StockInventoryReport(models.AbstractModel):
    _name = 'stock.inventory.report'
    _auto = False
    _order = 'date asc'

    move_id = fields.Many2one('stock.move', 'Stock Move', required=True)
    location_id = fields.Many2one('stock.location', 'Location', required=True)
    company_id = fields.Many2one('res.company', 'Company')
    product_id = fields.Many2one('product.product', 'Product', required=True)
    product_categ_id = fields.Many2one('product.category', 'Product Category', required=True)
    quantity = fields.Float('Product Quantity')
    date = fields.Datetime('Operation Date')
    price_unit_on_quant = fields.Float('Value')
    origin = fields.Char('Origin')
    product_template_id = fields.Many2one('product.template', 'Product Template', required=True)

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self._cr, 'stock_inventory_report')
        self._cr.execute("""
            CREATE VIEW stock_inventory_report AS (
                SELECT MIN(id) AS id
                    , move_id
                    , location_id
                    , company_id
                    , product_id
                    , product_categ_id
                    , product_template_id
                    , SUM(quantity) AS quantity
                    , date
                    , COALESCE(SUM(price_unit_on_quant * quantity) / NULLIF(SUM(quantity), 0), 0) AS price_unit_on_quant
                    , SUM(amount) AS amount
                    , type
                    , origin
                    FROM
                    ((SELECT
                        stock_move.id AS id
                        , stock_move.id AS move_id
                        , dest_location.id AS location_id
                        , dest_location.company_id AS company_id
                        , stock_move.product_id AS product_id
                        , product_template.id AS product_template_id
                        , product_template.categ_id AS product_categ_id
                        , his.quantity AS quantity
                        , stock_move.date AS date
                        , his.price_unit_on_quant AS price_unit_on_quant
                        , his.quantity * his.price_unit_on_quant AS amount
                        , 'in' AS type
                        , stock_move.origin AS origin
                    FROM
                        stock_history AS his
                    JOIN stock_move ON stock_move.id = his.move_id
                    JOIN stock_location dest_location ON stock_move.location_dest_id = dest_location.id
                    JOIN stock_location source_location ON stock_move.location_id = source_location.id
                    JOIN product_product ON product_product.id = stock_move.product_id
                    JOIN product_template ON product_template.id = product_product.product_tmpl_id
                    WHERE his.quantity > 0 AND stock_move.state = 'done' AND dest_location.usage IN ('internal', 'transit')
                    AND (
                        NOT (source_location.company_id IS NULL AND dest_location.company_id IS NULL) OR
                        source_location.company_id != dest_location.company_id OR
                        source_location.usage NOT IN ('internal', 'transit'))
                    ) UNION ALL
                    (SELECT
                        (-1) * stock_move.id AS id
                        , stock_move.id AS move_id
                        , source_location.id AS location_id
                        , source_location.company_id AS company_id
                        , stock_move.product_id AS product_id
                        , product_template.id AS product_template_id
                        , product_template.categ_id AS product_categ_id
                        , - his.quantity AS quantity
                        , stock_move.date AS date
                        , his.price_unit_on_quant AS price_unit_on_quant
                        , -(his.quantity * his.price_unit_on_quant) AS amount
                        , 'out' AS type
                        , stock_move.origin AS origin
                    FROM
                        stock_history AS his
                    JOIN stock_move ON stock_move.id = his.move_id
                    JOIN stock_location source_location ON stock_move.location_id = source_location.id
                    JOIN stock_location dest_location ON stock_move.location_dest_id = dest_location.id
                    JOIN product_product ON product_product.id = stock_move.product_id
                    JOIN product_template ON product_template.id = product_product.product_tmpl_id
                    WHERE his.quantity < 0 AND stock_move.state = 'done' AND source_location.usage IN ('internal', 'transit')
                    AND (
                        NOT (dest_location.company_id IS NULL AND source_location.company_id IS NULL) OR
                        dest_location.company_id != source_location.company_id OR
                        dest_location.usage NOT IN ('internal', 'transit'))
                    ))
                    AS foo
                    GROUP BY move_id
                    , location_id
                    , company_id
                    , product_id
                    , product_categ_id
                    , date
                    , origin
                    , type
                    , product_template_id
            )""")
