# -*- coding: utf-8 -*-


class ProductLedgerReportExcelCellStyles:
    """ProductLedgerReport-т зориулсан форматууд
    """
    format_name = {
        'font_name': 'Times New Roman',
        'font_size': 14,
        'bold': True,
        'align': 'center',
        'valign': 'vcenter'
    }

    format_filter = {
        'font_name': 'Times New Roman',
        'font_size': 10,
        'bold': True,
        'align': 'left',
        'valign': 'vcenter'
    }

    format_filter_right = {
        'font_name': 'Times New Roman',
        'font_size': 10,
        'bold': True,
        'align': 'right',
        'valign': 'vcenter'
    }

    format_filter_bold = {
        'font_name': 'Times New Roman',
        'font_size': 10,
        'bold': False,
        'align': 'right',
        'valign': 'vcenter',
        'num_format': 'YYYY-mm-dd'
    }

    format_title = {
        'font_name': 'Times New Roman',
        'font_size': 10,
        'bold': True,
        'align': 'center',
        'valign': 'vcenter',
        'border': 1,
        'text_wrap': 1,
        'bg_color': '#CFE7F5',
        'border_color': '#95cae9',
    }

    format_group = {
        'font_name': 'Times New Roman',
        'font_size': 10,
        'bold': True,
        'align': 'center',
        'valign': 'vcenter',
        'border': 1,
        'bg_color': '#83CAFF',
        'border_color': '#66bdff',
    }

    format_group_total = {
        'font_name': 'Times New Roman',
        'font_size': 10,
        'bold': True,
        'align': 'center',
        'valign': 'vcenter',
        'top': 1,
        'bottom': 1,
        'border_color': '#66bdff',
    }

    format_group_right = {
        'font_name': 'Times New Roman',
        'font_size': 10,
        'bold': True,
        'align': 'right',
        'valign': 'vcenter',
        'border': 1,
        'bg_color': '#CFE7F5',
        'border_color': '#95cae9',
    }

    format_group_left = {
        'font_name': 'Times New Roman',
        'font_size': 10,
        'bold': True,
        'align': 'left',
        'valign': 'vcenter',
        'border': 1,
        'bg_color': '#CFE7F5',
        'border_color': '#95cae9',
    }

    format_group_float = {
        'font_name': 'Times New Roman',
        'font_size': 10,
        'bold': True,
        'align': 'right',
        'valign': 'vcenter',
        'top': 1,
        'bottom': 1,
        'num_format': '#,##0.00',
        'border_color': '#95cae9',
    }

    format_content_text = {
        'font_name': 'Times New Roman',
        'font_size': 9,
        'align': 'justify',
        'valign': 'vcenter',
        # 'border': 1
    }

    format_content_center = {
        'font_name': 'Times New Roman',
        'font_size': 9,
        'align': 'center',
        'valign': 'vcenter',
        # 'border': 1
    }

    format_content_float = {
        'font_name': 'Times New Roman',
        'font_size': 9,
        'align': 'right',
        'valign': 'vcenter',
        # 'border': 1,
        'num_format': '#,##0.00'
    }
    
    format_no = {
        'font_name': 'Times New Roman',
        'font_size': 9,
        'align': 'right',
        'valign': 'vcenter',
        'num_format': '#,##0_);(#,##0)'

    }

    format_no_colored = {
        'font_name': 'Times New Roman',
        'font_size': 9,
        'align': 'right',
        'valign': 'vcenter',
        'num_format': '#,##0_);(#,##0)',
        'bg_color': '#cfe7f5',
        'border_color': '#66bdff',
        'bold': True
    }

    format_no_total = {
        'font_name': 'Times New Roman',
        'font_size': 9,
        'align': 'right',
        'valign': 'vcenter',
        'num_format': '#,##0_);(#,##0)',
        'border_color': '#66bdff',
        'bold': True,
        'border': 1
    }

    format_content_float_colored = {
        'font_name': 'Times New Roman',
        'font_size': 9,
        'align': 'right',
        'valign': 'vcenter',
        'border': 1,
        'num_format': '#,##0.00',
        'bg_color': '#cfe7f5',
        'border_color': '#66bdff',
        'bold': True
    }

    format_title_float = {
        'font_name': 'Times New Roman',
        'font_size': 10,
        'bold': True,
        'align': 'right',
        'valign': 'vcenter',
        'top': 1,
        'bottom': 1,
        'num_format': '#,##0.00',
        'border_color': '#66bdff',
    }

    format_content_bold_float = {
        'font_name': 'Times New Roman',
        'font_size': 9,
        'bold': True,
        'align': 'right',
        'valign': 'vcenter',
        'border': 1,
        'num_format': '#,##0.00'
    }

    format_content_bold_float_noborder = {
        'font_name': 'Times New Roman',
        'font_size': 9,
        'bold': True,
        'align': 'right',
        'valign': 'vcenter',
        'num_format': '#,##0.00'
    }

    format_content_bold_text = {
        'font_name': 'Times New Roman',
        'font_size': 9,
        'bold': True,
        'align': 'center',
        'valign': 'vcenter',
        'border': 1
    }

    format_content_float_color = {
        'font_name': 'Times New Roman',
        'font_size': 9,
        'font_color': '#0033CC',
        'align': 'right',
        'valign': 'vcenter',
        'bg_color': '#DDDDDD',
        'border': 1,
        'num_format': '#,##0.00'
    }
    format_content_number = {
        'font_name': 'Times New Roman',
        'font_size': 9,
        'align': 'center',
        'valign': 'vcenter',
        'border': 1,
    }

    format_content_bold_left = {
        'font_name': 'Times New Roman',
        'font_size': 9,
        'bold': True,
        'align': 'left',
        'valign': 'vcenter',
        # 'border': 1
    }

    format_content_bold_number = {
        'font_name': 'Times New Roman',
        'font_size': 9,
        'bold': True,
        'align': 'center',
        'valign': 'vcenter',
        # 'border': 1,
    }
