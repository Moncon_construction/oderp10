# -*- encoding: utf-8 -*-
#########################

import time
import base64
from io import BytesIO

import xlsxwriter
from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.addons.l10n_mn_stock_base_report.tools.report_excel_cell_styles import \
    ProductLedgerReportExcelCellStyles
from odoo.addons.l10n_mn_report.tools.tools import str_tuple
from odoo.addons.l10n_mn_report.tools.tools import get_date
from odoo.addons.l10n_mn_report.tools.tools import last_date
from odoo.exceptions import UserError
from datetime import datetime
from xlsxwriter.utility import xl_rowcol_to_cell
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import pytz


class ProductLedgerReport(models.TransientModel):
    """
       БАРАА МАТЕРИАЛЫН ТОВЧОО ТАЙЛАН
    """
    _name = 'product.legder.report'
    _inherit = 'oderp.report.html.output'
    _description = "Product Ledger Report"

    company_id = fields.Many2one('res.company', string='Company', required=True,
                                 default=lambda self: self.env['res.company']._company_default_get(
                                     'hr.salary.rule.category.report'))
    date_from = fields.Date("Start Date", required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    date_to = fields.Date("End Date", required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    warehouse_ids = fields.Many2many('stock.warehouse', string="Warehouses", required=True)
    category_ids = fields.Many2many('product.category', string="Categories")
    product_ids = fields.Many2many('product.product', string="Products")
    price_type = fields.Selection([('cost', 'Cost Price'),
                                   ('sale_price', 'Sale Price')], string='Price Type', default='cost', required=True)
    show_barcode = fields.Boolean('Show Barcode')
    show_lot = fields.Boolean('Show Lot')  # Цувралаар гаргах эсэх
    show_worthy_resources = fields.Boolean('Show Worthy Resources')  # Зохистой нөөц гаргах эсэх
    show_turnover = fields.Boolean('Show Inventory Turnover')
    location_ids = fields.Many2many('stock.location', string='Locations')
    is_grouped_by_warehouse = fields.Boolean("Is grouped by warehouse")
    is_grouped_by_location = fields.Boolean("Is grouped by location")

    @api.onchange("company_id")
    def _get_domains(self):
        domain = {}
        domain['warehouse_ids'] = [('company_id', '=', self.company_id.id)]
        _warehouses = []
        for warehouse in self.env.user.allowed_warehouses:
            _warehouses.append(warehouse.id)
        if _warehouses:
            domain['warehouse_ids'] = [('id', 'in', _warehouses), ('company_id', '=', self.company_id.id)]
        domain['category_ids'] = [('company_id', '=', self.company_id.id)]
        domain['product_ids'] = [('product_tmpl_id.company_id', '=', self.company_id.id)]
        return {'domain': domain}

    @api.onchange("is_grouped_by_warehouse")
    def change_is_grouped_by_location(self):
        if not self.is_grouped_by_warehouse:
            self.is_grouped_by_location = False

    def _get_warehouse_locations(self):
        if self.env.user.has_group("stock.group_stock_multi_locations"):
            locations = self.env['stock.location'].search([('usage', '=', 'internal'), ('id', 'child_of', [wh.view_location_id.id for wh in self.warehouse_ids])])
        else:
            locations = [wh.lot_stock_id for wh in self.warehouse_ids]

        return locations

    @api.onchange("warehouse_ids")
    def _compute_warehouse_locations(self):
        location_obj = self._get_warehouse_locations()
        if location_obj:
            return {
                'domain': {
                    'location_ids': [('id', 'in', [location.id for location in location_obj])]
                }
            }
        else:
            return {
                'domain': {
                    'location_ids': [('usage', '=', 'internal')]
                }
            }

    def get_xsl_column_name(self, index):
        alphabet = {'0': 'A', '1': 'B', '2': 'C', '3': 'D', '4': 'E',
                    '5': 'F', '6': 'G', '7': 'H', '8': 'I', '9': 'J',
                    '10': 'K', '11': 'L', '12': 'M', '13': 'N', '14': 'O',
                    '15': 'P', '16': 'Q', '17': 'R', '18': 'S', '19': 'T',
                    '20': 'U', '21': 'V', '22': 'W', '23': 'X', '24': 'Y', '25': 'Z'}

        if index <= 25:
            return (alphabet[str(index)] + ":" + alphabet[str(index)])
        else:
            return (alphabet[str(index / 26 - 1)] + alphabet[str(index % 26)] + ":" + alphabet[str(index / 26 - 1)] + alphabet[str(index % 26)])

    def get_column_name_for_calculate(self, index):
        column_name = self.get_xsl_column_name(index).split(':')[0]
        return column_name

    def get_sum_formula(self, startx, endx, coly):
        return "{=SUM(" + self.get_column_name_for_calculate(coly) + str(startx) + ":" + self.get_column_name_for_calculate(coly) + str(endx) + ")}"

    def get_sum_formula_from_list(self, list, coly):
        formula = "="
        if list:
            for i in range(len(list)):
                formula += ("+" + self.get_column_name_for_calculate(coly) + str(list[i]))
        return formula

    def get_header(self, sheet, rowx, coly, format_title):
        sheet.merge_range(rowx, coly, rowx + 1, coly, _(u'№'), format_title)
        coly += 1
        sheet.merge_range(rowx, coly, rowx + 1, coly, _('Product code PLR'), format_title)
        if self.show_barcode:
            coly += 1
            sheet.merge_range(rowx, coly, rowx + 1, coly, _('Barcode PLR'), format_title)
        coly += 1
        sheet.merge_range(rowx, coly, rowx + 1, coly, _('Product name PLR'), format_title)
        coly += 1
        if self.show_lot:
            sheet.merge_range(rowx, coly, rowx + 1, coly, _('Serial PLR'), format_title)
            sheet.merge_range(rowx, coly + 1, rowx + 1, coly + 1, _('Serial End Date PLR'), format_title)
            coly += 2
        sheet.merge_range(rowx, coly, rowx + 1, coly, _('Unit of measure PLR'), format_title)
        coly += 1
        sheet.merge_range(rowx, coly, rowx, coly + 1, _('Initial Balance PLR'), format_title)
        coly += 2
        sheet.merge_range(rowx, coly, rowx, coly + 1, _('Income PLR'), format_title)
        coly += 2
        sheet.merge_range(rowx, coly, rowx, coly + 1, _('Expense PLR'), format_title)
        coly += 2
        sheet.merge_range(rowx, coly, rowx, coly + 2, _('End Balance PLR'), format_title)
        if self.show_barcode and self.show_lot:
            coly = 8
        elif self.show_barcode and not self.show_lot:
            coly = 6
        elif not self.show_barcode and self.show_lot:
            coly = 7
        else:
            coly = 5
        if self.price_type == 'cost':
            sheet.write(rowx + 1, coly, _('Cost PLR'), format_title)
            coly += 2
            sheet.write(rowx + 1, coly, _('Cost PLR'), format_title)
            coly += 2
            sheet.write(rowx + 1, coly, _('Cost PLR'), format_title)
            coly += 1
            sheet.write(rowx + 1, coly, _('Unit Cost PLR'), format_title)
            coly += 2
            sheet.write(rowx + 1, coly, _('Cost PLR'), format_title)
        elif self.price_type == 'sale_price':
            sheet.write(rowx + 1, coly, _('Sale Price PLR'), format_title)
            coly += 2
            sheet.write(rowx + 1, coly, _('Sale Price PLR'), format_title)
            coly += 2
            sheet.write(rowx + 1, coly, _('Sale Price PLR'), format_title)
            coly += 1
            sheet.write(rowx + 1, coly, _('Unit Sale Price PLR'), format_title)
            coly += 2
            sheet.write(rowx + 1, coly, _('Total Sale Price PLR'), format_title)
        if self.show_worthy_resources:
            coly += 1
            sheet.merge_range(rowx, coly, rowx + 1, coly, _('Worthly Resources PLR'), format_title)
            coly += 1
            sheet.merge_range(rowx, coly, rowx + 1, coly, _('Difference PLR'), format_title)
        if self.show_turnover:
            coly += 1
            sheet.merge_range(rowx, coly, rowx + 1, coly, _('Turnover PLR'), format_title)
            coly += 1
            sheet.merge_range(rowx, coly, rowx + 1, coly, _('AVG Lifespan PLR'), format_title)

        if self.show_barcode and self.show_lot:
            coly = 7
        elif self.show_barcode and not self.show_lot:
            coly = 5
        elif not self.show_barcode and self.show_lot:
            coly = 6
        else:
            coly = 4
        sheet.write(rowx + 1, coly, _('Init Quantity PLR'), format_title)
        coly += 2
        sheet.write(rowx + 1, coly, _('Income Quantity PLR'), format_title)
        coly += 2
        sheet.write(rowx + 1, coly, _('Out Quantity PLR'), format_title)
        coly += 3
        sheet.write(rowx + 1, coly, _('End Quantity PLR'), format_title)
        return sheet

    def get_data(self):
        date_start = str(self.date_from) + ' 00:00:00'
        date_end = str(self.date_to) + ' 23:59:59'

        locations = []

        if self.location_ids:
            locations = ', '.join([str(loc.id) for loc in self.location_ids])
        else:
            location_ids = self._get_warehouse_locations()
            if location_ids and len(location_ids) > 0:
                locations = ', '.join([str(loc.id) for loc in location_ids])

        location_qry = "AND m.location_id IN (%s) " % locations if locations and len(locations) > 0 else ""

        wh_sub_join_qry, wh_join_qry, wh_sub_selection_qry, wh_selection_qry = "", "", "", ""
        wh_order_qry, wh_group_qry = "", ""
        wh_where = ''
        if self.is_grouped_by_warehouse:
            """
                @note: Belown row we want to get warehouse from location....
                            if there is one location exists for the wh we can find it by wh's lot_stock_id
                            but if there are more than one locations exits for the wh so we can find it by wh's view_location_id
            """
            wh_sub_join_qry = """
                        LEFT JOIN stock_location sl ON sl.id = m.location_id
                        LEFT JOIN stock_location view_loc ON (view_loc.parent_left <= sl.parent_left AND view_loc.parent_right >= sl.parent_left)
                        LEFT JOIN stock_warehouse wh ON wh.view_location_id = view_loc.id AND wh.id IS NOT NULL
            """

            wh_join_qry = "LEFT JOIN stock_warehouse wh ON wh.id = table1.wh_id AND wh.id IS NOT NULL"

            wh_sub_selection_qry = "wh.id AS wh_id, "
            wh_selection_qry = "wh.id AS wh_id, wh.name as wh_name, "
            wh_group_qry = wh_order_qry = "wh.name, wh.id, "
            wh_where = ' AND wh.id is not null '

        pro_category_qry = " pc.id IN (" + str(self.category_ids.ids)[1:len(str(self.category_ids.ids)) - 1] + ") AND pc.id IS NOT NULL " if str(self.category_ids.ids) != "[]" else ""
        product_qry = ""
        if pro_category_qry and str(self.product_ids.ids) != "[]":
            if str(self.product_ids.ids) != "[]":
                product_qry = "AND (p.id IN (" + str(self.product_ids.ids)[1:len(str(self.product_ids.ids)) - 1] + ") AND %s)" % pro_category_qry
        elif pro_category_qry:
            product_qry = "AND " + pro_category_qry
        elif str(self.product_ids.ids) != "[]":
            product_qry = "AND p.id IN (" + str(self.product_ids.ids)[1:len(str(self.product_ids.ids)) - 1] + ") "

        loc_selection_qry, loc_join_qry, loc_group_qry, loc_order_qry = "", "", "", ""
        if self.is_grouped_by_location:
            loc_selection_qry = "loc.id AS loc_id, loc.complete_name AS loc_name, "
            loc_join_qry = "LEFT JOIN stock_location loc ON loc.id = table1.loc_id"
            loc_group_qry = loc_order_qry = "loc.id, loc.name, "

        """
            @note: When user check show lot from wizard before print this report, it means get all quantity and cost
                    from stock_quant object so belown check used for fill queries with quant.
                    REMEMBER!!! It used only when user check 'show lot'. If we use it inside of main query without check
                    it will return wrong result when user unchek 'show lot'...
        """
        lot_selection_qry, lot_join_qry, lot_group_qry, lot_order_qry = "", "", "", ""
        lot_sub_selection_qry, lot_sub_join_qry, lot_where_qry = "", "", ""
        qty_selection = "m.quantity"
        cost_selection = "m.amount"
        if self.show_lot:
            lot_selection_qry = "lot.id AS lot_id, lot.name AS lot_name, lot.removal_date AS lot_end_date, "
            lot_sub_selection_qry = "spl.id AS lot_id,"
            lot_join_qry = "LEFT JOIN stock_production_lot lot ON lot.id = table1.lot_id"
            lot_sub_join_qry = """
                        LEFT JOIN stock_quant sq ON sq.product_id = p.id AND sq.location_id = m.location_id
                        LEFT JOIN stock_production_lot spl ON sq.lot_id = spl.id"""
            lot_group_qry = lot_order_qry = ", lot.id, lot.name, lot.removal_date "
            lot_where_qry = "AND sq.id IS NOT NULL"
            qty_selection = "sq.qty"
            cost_selection = "sq.cost"

        worthy_selection_qry, worthy_join_qry, worthy_group_qry, worthy_order_qry = "", "", "", ""
        if self.show_worthy_resources:
            worthy_selection_qry += "swo.product_min_qty AS worthy_qty, "
            worthy_join_qry += "LEFT JOIN stock_warehouse_orderpoint swo ON swo.product_id = p.id "
            worthy_group_qry = worthy_order_qry = ", swo.product_min_qty "

        """
            @note: This query will return all row of report.
                   At one row we can see: product and its detailed info which used for report,
                                       product's location, location's warehouse
                                       and init, income, out quantity/cost/.
        """
        query = """
            SELECT %s %s pc.id as cat_id, pc.name as cat_name,
                p.id as pro_id, pt.name as pro_name, p.default_code as pro_code, p.barcode as barcode,
                %s uom_t.name as uom_name, %s
                SUM(init_quantity) AS init_quantity, SUM(init_cost) AS init_cost,
                SUM(income_quantity) AS income_quantity, SUM(income_cost) AS income_cost,
                SUM(out_quantity) AS out_quantity, SUM(out_cost) AS out_cost,
                SUM(init_price) AS init_price, SUM(income_price) AS income_price, SUM(out_price) AS out_price
            FROM
            (
                SELECT
                            %s m.location_id AS loc_id, p.id AS product_id, %s

                            /* Belown 3 Case used for get init_quantity, init_cost & init_price */
                            CASE
                                WHEN m.date < '%s' AND m.type IN ('in')
                                    THEN %s
                                WHEN m.date < '%s' AND m.type IN ('out')
                                    THEN -%s ELSE 0 END AS init_quantity,
                            CASE
                                WHEN m.date < '%s' AND m.type IN ('in')
                                    THEN %s
                                WHEN m.date < '%s' AND m.type IN ('out')
                                    THEN -%s ELSE 0 END AS init_cost,
                            CASE
                                WHEN m.date < '%s' AND m.type IN ('in')
                                    THEN t.list_price
                                WHEN m.date < '%s' AND m.type IN ('out')
                                    THEN -t.list_price ELSE 0 END AS init_price,

                            /* Belown 3 Case used for get income_quantity, income_cost & income_price */
                            CASE
                                WHEN m.date BETWEEN '%s' AND '%s' AND m.type = 'in'
                                    THEN %s ELSE 0 END AS income_quantity,
                            CASE
                                WHEN m.date BETWEEN '%s' AND '%s' AND m.type = 'in'
                                    THEN %s ELSE 0 END AS income_cost,
                            CASE
                                WHEN m.date BETWEEN '%s' AND '%s' AND m.type = 'in'
                                    THEN t.list_price ELSE 0 END AS income_price,

                            /* Belown 3 Case used for get out_quantity, out_cost & out_price */
                            CASE
                                WHEN m.date BETWEEN '%s' AND '%s' AND m.type = 'out'
                                    THEN %s ELSE 0 END AS out_quantity,
                            CASE
                                WHEN m.date BETWEEN '%s' AND '%s' AND m.type = 'out'
                                    THEN %s ELSE 0 END AS out_cost,
                            CASE
                                WHEN m.date BETWEEN '%s' AND '%s' AND m.type = 'out'
                                    THEN t.list_price ELSE 0 END AS out_price

                        FROM stock_inventory_report m
                        LEFT JOIN product_product p ON m.product_id = p.id
                        LEFT JOIN product_template t ON t.id = p.product_tmpl_id
                        %s
                        %s
                        WHERE m.company_id = %s
                            %s
                            %s
            ) table1
            LEFT JOIN product_product p ON table1.product_id = p.id
            LEFT JOIN product_template pt ON (pt.id = p.product_tmpl_id)
            LEFT JOIN product_category pc ON (pc.id = pt.categ_id)
            LEFT JOIN product_uom uom_t ON (uom_t.id = pt.uom_id)
            %s
            %s
            %s
            %s
            WHERE 1=1 %s %s
            GROUP BY %s %s pc.id, p.id, pt.name, p.default_code %s , uom_t.id %s
            ORDER BY %s %s pc.name, pc.id, pt.name, p.id %s %s
        """ % (wh_selection_qry, loc_selection_qry, lot_selection_qry, worthy_selection_qry,
               wh_sub_selection_qry, lot_sub_selection_qry,
               date_start, qty_selection, date_start, qty_selection,
               date_start, cost_selection, date_start, cost_selection,
               date_start, date_start,
               date_start, date_end, qty_selection,
               date_start, date_end, cost_selection,
               date_start, date_end,
               date_start, date_end, qty_selection,
               date_start, date_end, cost_selection,
               date_start, date_end,
               lot_sub_join_qry, wh_sub_join_qry, self.company_id.id, lot_where_qry, location_qry,
               wh_join_qry, loc_join_qry, lot_join_qry, worthy_join_qry,
               product_qry, wh_where,
               wh_group_qry, loc_group_qry, lot_group_qry, worthy_group_qry,
               wh_order_qry, loc_order_qry, lot_order_qry, worthy_order_qry)

        self.env.cr.execute(query)

        return self.env.cr.dictfetchall()

    def set_spec_level(self, sheet, rowx, start_lvl):
        """
            @note: Used for levelize all rows for collapse rows.
        """
        if (self.is_grouped_by_warehouse and self.is_grouped_by_location):
            sheet.set_row(rowx, None, None, {'level': start_lvl + 2})
        elif (self.is_grouped_by_warehouse or self.is_grouped_by_location):
            sheet.set_row(rowx, None, None, {'level': start_lvl + 1})
        else:
            sheet.set_row(rowx, None, None, {'level': start_lvl})

        return sheet

    @api.multi
    def export_report(self):

        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Product Ledger Report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_name = book.add_format(ProductLedgerReportExcelCellStyles.format_name)
        format_filter = book.add_format(ProductLedgerReportExcelCellStyles.format_filter)
        format_filter_right = book.add_format(ProductLedgerReportExcelCellStyles.format_filter_right)
        format_title = book.add_format(ProductLedgerReportExcelCellStyles.format_title)
        format_filter_bold = book.add_format(ProductLedgerReportExcelCellStyles.format_filter_bold)
        format_content_bold_float_noborder = book.add_format(ProductLedgerReportExcelCellStyles.format_content_bold_float_noborder)

        format_content_float = book.add_format(ProductLedgerReportExcelCellStyles.format_content_float)
        format_no = book.add_format(ProductLedgerReportExcelCellStyles.format_no)
        format_no_colored = book.add_format(ProductLedgerReportExcelCellStyles.format_no_colored)
        format_content_float_colored = book.add_format(ProductLedgerReportExcelCellStyles.format_content_float_colored)
        format_group_total = book.add_format(ProductLedgerReportExcelCellStyles.format_group_total)
        format_group_left = book.add_format(ProductLedgerReportExcelCellStyles.format_group_left)
        format_content_center = book.add_format(ProductLedgerReportExcelCellStyles.format_content_center)

        # parameters
        rowx = 0
        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(
            filename_prefix=('product_ledger_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(time.strftime('%Y-%m-%d'))
        sheet.set_portrait()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.39, 0.39, 0.39, 0.39)  # 1cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})

        # compute column
        to_init_column_count = 4

        coly = 0
        sheet.set_column(self.get_xsl_column_name(coly), 6)
        sheet.set_column(self.get_xsl_column_name(coly + 1), 10)
        coly += 2
        if self.show_barcode:
            sheet.set_column(self.get_xsl_column_name(coly), 15)
            to_init_column_count += 1
            coly += 1
        sheet.set_column(self.get_xsl_column_name(coly), 27)
        coly += 1
        if self.show_lot:
            to_init_column_count += 2
            sheet.set_column(self.get_xsl_column_name(coly), 11)
            sheet.set_column(self.get_xsl_column_name(coly + 1), 15)
            coly += 2
        sheet.set_column(self.get_xsl_column_name(coly), 10)
        coly += 1
        for i in range(4 * 2 + 1):
            sheet.set_column(self.get_xsl_column_name(coly + i), 15)
        coly += (4 * 2 + 1)
        if self.show_worthy_resources:
            sheet.set_column(self.get_xsl_column_name(coly), 15)
            sheet.set_column(self.get_xsl_column_name(coly + 1), 15)
            coly += 2
        if self.show_turnover:
            sheet.set_column(self.get_xsl_column_name(coly), 15)
            sheet.set_column(self.get_xsl_column_name(coly + 1), 15)
            coly += 2

        total_col_count = coly

        sheet.set_row(2, 20)
        sheet.set_row(5, 30)
        sheet.set_row(6, 30)

        # build xslx header
        rowx = 0
        sheet.merge_range(rowx, 0, rowx, 12, '%s' % self.company_id.name, format_filter)
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, 12, report_name.upper(), format_name)
        rowx += 1

        sheet.merge_range(rowx, 0, rowx, 1, _(u"Warehouse PLR:"), format_filter)
        sheet.merge_range(rowx, 2, rowx, total_col_count - 1, ', '.join([wh.name for wh in self.warehouse_ids]), format_filter)
        rowx += 1

        sheet.merge_range(rowx, 0, rowx, total_col_count - 1, _(u'Duration: %s - %s') % (self.date_from, self.date_to), format_filter_bold)
        rowx += 1

        # build table header
        coly = 0
        sheet = self.get_header(sheet, rowx, coly, format_title)
        rowx += 2

        # get rows
        results = self.get_data()

        # build rows
        if results and len(results) > 0:
            writen_wh_ids, writen_loc_ids, writen_cat_ids, writen_pro_ids, writen_lot_ids = [], [], [], [], []
            wh_seq, loc_seq, cat_seq, pro_seq, lot_seq = 0, 0, 0, 0, 0

            current_product_rowxs, current_loc_rowxs, current_categ_rowxs = [], [], []
            current_wh_rowx, current_loc_rowx, current_categ_rowx = -1, -1, -1

            cats_rowxs, locations_rowxs, whs_rowxs = [], [], []
            all_cats, all_locations, all_whs = [], [], []
            rowd = 0
            warehouse_total_rows = []

            """
                @note: has lot means: user check 'show lot' from wizard.

                       If has lot then count total rows with distinct lot.
                       If has not lot then count total rows with distinct products.
            """
            total_line_count = 0

            for result in results:
                coly = 0
                """
                    @note: Belown check used for build warehouse row.

                           When we first build warehouse row, we only build it's title.
                           But when we second build warehouse row, we build it's title and just before warehouse's detailed information.

                           Warehouse detailed information contains: all products /which belown to this warehouse/ total init and ... blabla columns summary.
                """
                if self.is_grouped_by_warehouse and result['wh_id'] not in writen_wh_ids:
                    coly = 0
                    writen_loc_ids, writen_cat_ids, writen_pro_ids, writen_lot_ids = [], [], [], []
                    wh_seq += 1
                    loc_seq, cat_seq, pro_seq, lot_seq = 0, 0, 0, 0
                    writen_wh_ids.append(result['wh_id'])

                    if current_wh_rowx != -1:
                        for i in range(coly + to_init_column_count, total_col_count, 1):
                            if self.is_grouped_by_location:
                                sheet.write_formula(current_wh_rowx, coly + i, '{=' + self.get_sum_formula_from_list(current_loc_rowxs, coly + i) + '}', format_content_float_colored)
                            else:
                                sheet.write_formula(current_wh_rowx, coly + i, '{=' + self.get_sum_formula_from_list(current_categ_rowxs, coly + i) + '}', format_content_float_colored)
                        current_wh_rowx = -1

                    if self.is_grouped_by_location and current_loc_rowx != -1:
                        for i in range(coly + to_init_column_count, total_col_count, 1):
                            sheet.write_formula(current_loc_rowx, coly + i, '{=' + self.get_sum_formula_from_list(current_categ_rowxs, coly + i) + '}', format_content_float_colored)
                        current_loc_rowx = -1

                    if current_categ_rowx != -1:
                        sheet.merge_range(rowx, coly, rowx, coly + to_init_column_count - 1, _('Category subtotal'), format_group_total)
                        for h in range(coly + to_init_column_count, total_col_count):
                            sheet.write_formula(rowx, h,
                                                '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(
                                                    rowx-1, h) + ')}', format_content_bold_float_noborder)
                            sheet.write_formula(current_categ_rowx, h,
                                                '{=' + self.get_column_name_for_calculate(h) + str(rowx + 1) + '}',
                                                format_content_float_colored)
                        sheet = self.set_spec_level(sheet, rowx, 2)
                        current_categ_rowx = -1
                        rowx += 1

                    sheet.write(rowx, coly, wh_seq, format_no_colored)
                    sheet.merge_range(rowx, coly + 1, rowx, coly + to_init_column_count - 1, _(u"Warehouse PLR:") + u" %s" % (result["wh_name"]), format_group_left)

                    sheet.set_row(rowx, None, None, {'level': 0})

                    current_categ_rowxs, current_loc_rowxs = [], []
                    current_wh_rowx = rowx
                    whs_rowxs.append(rowx + 1)
                    rowx += 1

                """
                    @note: Belown check used for build location row.

                           Building logic is exactly same as building warehouse.
                """
                if self.is_grouped_by_location and result['loc_id'] not in writen_loc_ids:
                    coly = 0
                    writen_cat_ids, writen_pro_ids, writen_lot_ids = [], [], []
                    loc_seq += 1
                    cat_seq, pro_seq, lot_seq = 0, 0, 0
                    writen_loc_ids.append(result['loc_id'])

                    if current_loc_rowx != -1:
                        for i in range(coly + to_init_column_count, total_col_count, 1):
                            sheet.write_formula(current_loc_rowx, coly + i, '{=' + self.get_sum_formula_from_list(current_categ_rowxs, coly + i) + '}', format_content_float_colored)
                        current_loc_rowx = -1

                    locations_rowxs.append(rowx + 1)

                    if current_categ_rowx != -1:
                        sheet.merge_range(rowx, coly, rowx, coly + to_init_column_count - 1, _('Category subtotal'), format_group_total)
                        for h in range(coly + to_init_column_count, total_col_count):
                            sheet.write_formula(rowx, h,
                                                '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(
                                                    rowx-1, h) + ')}', format_content_bold_float_noborder)
                            sheet.write_formula(current_categ_rowx, h,
                                                '{=' + self.get_column_name_for_calculate(h) + str(rowx + 1) + '}',
                                                format_content_float_colored)
                        sheet = self.set_spec_level(sheet, rowx, 2)
                        current_categ_rowx = -1
                        rowx += 1

                    sheet.write(rowx, coly, u"%s.%s" % (wh_seq, loc_seq), format_no_colored)
                    sheet.merge_range(rowx, coly + 1, rowx, coly + to_init_column_count - 1, _(u"Location:") + u" %s" % (result["loc_name"]), format_group_left)

                    sheet.set_row(rowx, None, None, {'level': 0}) if not self.is_grouped_by_warehouse else sheet.set_row(rowx, None, None, {'level': 1})

                    current_categ_rowxs, current_product_rowxs = [], []
                    current_loc_rowx = rowx
                    current_loc_rowxs.append(rowx + 1)
                    rowx += 1

                """
                    @note: Belown check used for build category row.

                           Building logic is exactly same as building warehouse.
                           But we always group by category so we only check this category already writen or not.

                           Already writen mean is: If we group by location then we check category is already writen for that location which just writen before.
                                                   Then category row can duplicately writen in one sheet belown many locations or warehouses.
                """
                if result['cat_id'] not in writen_cat_ids:
                    coly = 0
                    writen_pro_ids, writen_lot_ids = [], []
                    cat_seq += 1
                    pro_seq, lot_seq = 0, 0
                    writen_cat_ids.append(result['cat_id'])

                    if current_categ_rowx != -1:
                        sheet.merge_range(rowx, coly, rowx, coly + to_init_column_count - 1, _('Category subtotal'), format_group_total)
                        for h in range(coly + to_init_column_count, total_col_count):
                            sheet.write_formula(rowx, h,
                                                '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(
                                                    rowx-1, h) + ')}', format_content_bold_float_noborder)
                            sheet.write_formula(current_categ_rowx, h,
                                                '{=' + self.get_column_name_for_calculate(h) + str(rowx + 1) + '}',
                                                format_content_float_colored)
                        sheet = self.set_spec_level(sheet, rowx, 2)
                        current_categ_rowx = -1
                        rowx += 1

                    cat_seq_str = str(cat_seq)
                    if self.is_grouped_by_warehouse and not self.is_grouped_by_location:
                        cat_seq_str = u"%s.%s" % (wh_seq, cat_seq)
                    elif self.is_grouped_by_warehouse and self.is_grouped_by_location:
                        cat_seq_str = u"%s.%s.%s" % (wh_seq, loc_seq, cat_seq)
                    sheet.write(rowx, coly, cat_seq_str, format_no_colored)
                    sheet.merge_range(rowx, coly + 1, rowx, coly + to_init_column_count - 1, _(u"Category:") + u" %s" % (result["cat_name"]), format_group_left)

                    sheet = self.set_spec_level(sheet, rowx, 0)

                    current_categ_rowx = rowx
                    current_categ_rowxs.append(rowx + 1)
                    cats_rowxs.append(rowx + 1)
                    current_product_rowxs = []
                    rowx += 1
                    rowd = rowx

                """
                    @note: Belown if/else check used for build one product row's columns depends on show lot or not.
                            'can_write_other_info' attribute used for write same columns inside if/else check at one group code.
                """
                can_write_other_info = False
                if self.show_lot:
                    if result['pro_id'] not in writen_pro_ids:
                        writen_pro_ids.append(result['pro_id'])
                        writen_lot_ids = []

                    if result['lot_id'] not in writen_lot_ids:
                        coly = 0
                        lot_seq += 1
                        total_line_count += 1
                        writen_lot_ids.append(result['lot_id'])
                        lot_seq_str = str(lot_seq)

                        if self.is_grouped_by_warehouse and not self.is_grouped_by_location:
                            lot_seq_str = u"%s.%s.%s" % (wh_seq, cat_seq, lot_seq)
                        elif self.is_grouped_by_warehouse and self.is_grouped_by_location:
                            lot_seq_str = u"%s.%s.%s.%s" % (wh_seq, loc_seq, cat_seq, lot_seq)
                        else:
                            lot_seq_str = u"%s.%s" % (cat_seq, lot_seq)

                        sheet.write(rowx, coly, lot_seq_str, format_no)
                        sheet.write(rowx, coly + 1, result['pro_code'], format_content_center)
                        coly += 2
                        if self.show_barcode:
                            sheet.write(rowx, coly, result['barcode'], format_content_center)
                            coly += 1
                        sheet.write(rowx, coly, result['pro_name'], format_content_center)
                        coly += 1
                        sheet.write(rowx, coly, result['lot_name'], format_content_center)
                        sheet.write(rowx, coly + 1, result['lot_end_date'], format_content_center)
                        coly += 1

                        can_write_other_info = True
                else:
                    if result['pro_id'] not in writen_pro_ids:
                        coly = 0
                        pro_seq += 1
                        total_line_count += 1
                        writen_pro_ids.append(result['pro_id'])
                        pro_seq_str = str(pro_seq)

                        if self.is_grouped_by_warehouse and not self.is_grouped_by_location:
                            pro_seq_str = u"%s.%s.%s" % (wh_seq, cat_seq, pro_seq)
                        elif self.is_grouped_by_warehouse and self.is_grouped_by_location:
                            pro_seq_str = u"%s.%s.%s.%s" % (wh_seq, loc_seq, cat_seq, pro_seq)
                        else:
                            pro_seq_str = u"%s.%s" % (cat_seq, pro_seq)

                        sheet.write(rowx, coly, pro_seq_str, format_no)
                        sheet.write(rowx, coly + 1, result['pro_code'], format_content_center)
                        coly += 2
                        if self.show_barcode:
                            sheet.write(rowx, coly, result['barcode'], format_content_center)
                            coly += 1
                        sheet.write(rowx, coly, result['pro_name'], format_content_center)

                        can_write_other_info = True

                if can_write_other_info:
                    init_cost = result['init_cost'] or 0 if self.price_type == 'cost' else result['init_price'] or 0
                    out_cost = result['out_cost'] or 0 if self.price_type == 'cost' else result['out_price'] or 0
                    in_cost = result['income_cost'] or 0 if self.price_type == 'cost' else result['income_price'] or 0
                    sheet.write(rowx, coly + 1, result['uom_name'], format_content_center)
                    sheet.write(rowx, coly + 2, result['init_quantity'] or 0, format_content_float)
                    sheet.write(rowx, coly + 3, result['init_cost'] or 0 if self.price_type == 'cost' else result['init_price'] or 0, format_content_float)
                    sheet.write(rowx, coly + 4, result['income_quantity'] or 0, format_content_float)
                    sheet.write(rowx, coly + 5, result['income_cost'] or 0 if self.price_type == 'cost' else result['income_price'] or 0, format_content_float)
                    sheet.write(rowx, coly + 6, result['out_quantity'] or 0, format_content_float)
                    sheet.write(rowx, coly + 7, result['out_cost'] or 0 if self.price_type == 'cost' else result['out_price'] or 0, format_content_float)
                    end_qty = round((result['init_quantity'] + result['income_quantity'] - result['out_quantity']),
                                    2) if result['init_quantity'] or result['income_quantity'] or result[
                        'out_quantity'] else 0
                    end_cost = round((init_cost + in_cost- out_cost), 2)
                    sheet.write(rowx, coly + 8, end_cost/end_qty if end_qty > 0 else 0, format_content_float)
                    sheet.write(rowx, coly + 9, end_qty, format_content_float)
                    sheet.write(rowx, coly + 10, end_cost, format_content_float)

                    if self.show_worthy_resources:
                        sheet.write(rowx, coly + 11, result['worthy_qty'] if result['worthy_qty'] else 0, format_content_float)
                        sheet.write(rowx, coly + 12, end_qty - result['worthy_qty'] if result['worthy_qty'] else 0, format_content_float)
                        coly += 2
                    if self.show_turnover:
                        out_coly = coly + 7 if not self.show_worthy_resources else coly + 5
                        end_amount = result['init_cost'] or 0 + result['income_cost'] or 0 - result['out_cost'] or 0
                        if self.price_type != 'cost':
                            end_amount = result['init_price'] or 0 + result['income_price'] or 0 - result['out_price'] or 0
                        sheet.write(rowx, coly + 11, round(out_cost/end_cost,2) if end_amount != 0 and end_cost != 0 else 0, format_content_float)
                        date_from = datetime.strptime(self.date_from, '%Y-%m-%d')
                        date_to = datetime.strptime(self.date_to, '%Y-%m-%d')
                        daysDiff = str((date_from - date_to).days)
                        bme = result['out_cost'] or 0 / (end_amount or 1)
                        if self.price_type != 'cost':
                            bme = result['out_price'] or 0 / (end_amount or 1)
                        sheet.write(rowx, coly + 12,
                                    ((abs(float(daysDiff)) + 1) / bme) if bme else 0,
                                    format_content_float)

                    sheet = self.set_spec_level(sheet, rowx, 1)

                    current_product_rowxs.append(rowx + 1)
                    rowx += 1

                """
                    @note: We are building total warehouse and location row before its' products.
                           So this building logic, we cannot write last warehouse/location's total summary.
                           Then belown check will check the for loop result is last or not.
                               Then if result is last we must write last result's corresponding warehouse/location's total summary.
                               We use current_wh_rowx, current_loc_rowx, current_categ_rowx attributes for it.

                            And if result is last, we must build footer for it.
                """
                if results[-1] == result:
                    coly = 0
                    # build last warehouse's summary columns' value
                    if self.is_grouped_by_warehouse and current_wh_rowx != -1:
                        for i in range(coly + to_init_column_count, total_col_count, 1):
                            if self.is_grouped_by_location:
                                sheet.write_formula(current_wh_rowx, coly + i, '{=' + self.get_sum_formula_from_list(current_loc_rowxs, coly + i) + '}', format_content_float_colored)
                            else:
                                sheet.write_formula(current_wh_rowx, coly + i, '{=' + self.get_sum_formula_from_list(current_categ_rowxs, coly + i) + '}', format_content_float_colored)

                    # build last location's summary columns' value
                    if self.is_grouped_by_location and current_loc_rowx != -1:
                        for i in range(coly + to_init_column_count, total_col_count, 1):
                            sheet.write_formula(current_loc_rowx, coly + i, '{=' + self.get_sum_formula_from_list(current_categ_rowxs, coly + i) + '}', format_content_float_colored)

                    # build last category's summary columns' value
                    if current_categ_rowx != -1:
                        sheet.merge_range(rowx, coly, rowx, coly + to_init_column_count - 1, _('Category subtotal'), format_group_total)
                        for h in range(coly + to_init_column_count, total_col_count):
                            sheet.write_formula(rowx, h,
                                                '{=SUM(' + xl_rowcol_to_cell(rowd, h) + ':' + xl_rowcol_to_cell(
                                                    rowx-1, h) + ')}', format_content_bold_float_noborder)
                            sheet.write_formula(current_categ_rowx, h,
                                                '{=' + self.get_column_name_for_calculate(h) + str(rowx + 1) + '}',
                                                format_content_float_colored)
                        sheet = self.set_spec_level(sheet, rowx, 2)
                        rowx += 1

                    # build xslx footer
                    sheet.merge_range(rowx, coly + 1, rowx, coly + to_init_column_count - 1, _('Total PLR'), format_group_total)
                    for i in range(coly + to_init_column_count, total_col_count, 1):
                        if self.is_grouped_by_warehouse:
                            sheet.write_formula(rowx, coly + i, '{=' + self.get_sum_formula_from_list(whs_rowxs, coly + i) + '}', format_content_bold_float_noborder)
                        elif not self.is_grouped_by_warehouse and self.is_grouped_by_location:
                            sheet.write_formula(rowx, coly + i, '{=' + self.get_sum_formula_from_list(locations_rowxs, coly + i) + '}', format_content_bold_float_noborder)
                        else:
                            sheet.write_formula(rowx, coly + i, '{=' + self.get_sum_formula_from_list(cats_rowxs, coly + i) + '}', format_content_bold_float_noborder)

                    sheet.set_row(rowx, None, None, {'level': 0})
                    rowx += 1

        rowx += 3
        sheet.merge_range(rowx, 0, rowx, 1, u' Эд хариуцагч:', format_filter)
        sheet.merge_range(rowx, 2, rowx, 3, u'/...................................................../', format_filter_right)
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, 1, u' Хянасан нягтлан бодогч:', format_filter)
        sheet.merge_range(rowx, 2, rowx, 3, u'/...................................................../', format_filter_right)

        sheet.set_zoom(75)

        book.close()
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        is_start = False
        # call export function

        return report_excel_output_obj.export_report()
