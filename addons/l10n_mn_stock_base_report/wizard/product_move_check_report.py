# -*- coding: utf-8 -*-
import base64
import logging
import time
from datetime import datetime, timedelta
from io import BytesIO

import pytz
import xlsxwriter
from lxml import etree

from odoo import _, api, fields, models
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from odoo.exceptions import UserError
from odoo.addons.l10n_mn_web.models.time_helper import *

_logger = logging.getLogger('stock.report')


class ProductMoveCheckReportWizard(models.TransientModel):
    _name = 'product.move.check.report.wizard'
    _description = 'Product Move Check Wizard'

    def _default_product(self):
        context = self._context or {}
        if 'active_id' in context and context['active_id']:
            return context['active_id']
        return False

    def _default_report_type(self):
        if self.env.user.has_group('account.group_account_user'):
            res = 'price'
        else:
            res = 'owner'
        return res

    def _default_warehouse(self):
        res = 0
        if self.env.user and self.env.user.allowed_warehouses:
            res = self.env.user.allowed_warehouses[0].id
        return res

    def _default_level(self):
        if self.env.user.has_group('account.group_account_user'):
            level = 'manager'
        else:
            level = 'owner'
        return level

    def get_report_type(self):
        res = [('owner', _('QTY')),
               ('price', _('List Price'))]
        if self.env.user.has_group('l10n_mn_stock.group_stock_view_cost'):
            res.append(('cost', _('Cost Price')))
        return res

    report_type = fields.Selection(get_report_type, 'Report Type', default=_default_report_type, required=True)
    level = fields.Selection([('owner', 'Owner'),
                              ('manager', 'Manager')], 'Level', default=_default_level, required=True)
    from_date = fields.Date('From Date', default=fields.Date.context_today, required=True)
    to_date = fields.Date('To Date', default=fields.Date.context_today, required=True)
    warehouse_id = fields.Many2one('stock.warehouse', 'Warehouse', default=_default_warehouse, required=True)
    template_id = fields.Many2one('product.template', 'Product Template')
    category_id = fields.Many2one('product.category', 'Category')
    product_id = fields.Many2one('product.product', 'Product', default=_default_product, required=True)
    prodlot_id = fields.Many2one('stock.production.lot', 'Production Lot')
    save = fields.Boolean('Save to document storage')
    draft = fields.Boolean('Draft View')
    is_method_stock_move = fields.Boolean('Is stock move', default=False)

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(ProductMoveCheckReportWizard, self).fields_view_get(
            view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        if view_type == 'form':
            if 'warehouse_id' in res['fields']:
                res['fields']['warehouse_id']['domain'] = [
                    ('id', 'in', map(lambda x: x.id, self.env.user.allowed_warehouses))]
        return res

    @api.onchange('template_id')
    def template_id_change(self):
        res = {}
        if self.template_id:
            self.env.cr.execute(""" SELECT p.id FROM product_product p
                                    JOIN product_template t on p.product_tmpl_id = t.id
                                    WHERE t.id = %s""" % (self.template_id.id))
            prod_ids = [p['id'] for p in self.env.cr.dictfetchall()]
            return ({'domain': {'product_id': [('id', 'in', prod_ids)]}})
        else:
            return res

    @api.onchange('category_id')
    def categ_id_change(self):
        res = {}
        if self.category_id:
            self.env.cr.execute(""" SELECT p.id FROM product_template p
                                    WHERE categ_id = %s""" % (self.category_id.id))
            templ_ids = [p['id'] for p in self.env.cr.dictfetchall()]
            return ({'domain': {'product_id': [('product_tmpl_id', 'in', templ_ids)],
                                'template_id': [('id', 'in', templ_ids)]}})
        else:
            return res
        
    @api.onchange('warehouse_id')
    def onchange_warehouse(self):
        if self.warehouse_id:
            if self.warehouse_id.company_id.availability_compute_method == 'stock_move':
                self.is_method_stock_move = True
            else:
                self.is_method_stock_move = False

    @api.onchange('prodlot_id')
    def prodlot_id_change(self):
        res = {}
        if self.prodlot_id:
            self.env.cr.execute("""SELECT p.id FROM stock_production_lot p WHERE product_id = %s""" % (self.prodlot_id.id))
            prodlot_ids = [p['id'] for p in self.env.cr.dictfetchall()]
            return ({'domain': {'prodlot_id': [('id', 'in', prodlot_ids)]}})
        else:
            return res

    @api.multi
    def get_log_message(self):
        return _('Product move check report (Warehouse ="%s", Product = "%s", Start date = "%s", End date = "%s")') % (self.warehouse_id.name, self.product_id.name, self.from_date, self.to_date)

    @api.multi
    def get_available(self, wiz):
        ''' Эхний үлдэгдэл тооцож байна '''
        qty = 0
        from_date = wiz['from_date'] + ' 23:59:59'
        fdate = datetime.strptime(from_date, '%Y-%m-%d %H:%M:%S') - timedelta(days=1)
        fdate = get_display_day_to_user_day(fdate, self.env.user)
        self.env.cr.execute("SELECT view_location_id FROM stock_warehouse WHERE id = %s" % wiz['warehouse_id'])
        res2 = self.env.cr.dictfetchall()
        if res2:
            location_ids = self.env['stock.location'].search([('location_id', 'child_of', [res2[0]['view_location_id']]), ('usage', '=', 'internal')])
            if location_ids:
                self._cr.execute("SELECT  "
                        "sum(a.product_qty) AS quantity "
                    "FROM ( "
                        "SELECT "
                            "sm.product_id, "
                            "sm.product_qty, "
                            "sm.date "
                        "FROM "
                            "stock_move sm "
                        "WHERE "
                            "sm.date <= %s "
                            "AND sm.product_id = %s "
                            "AND sm.location_dest_id IN ("+str(location_ids.ids)[1:-1] +") "
                            "AND sm.location_id NOT IN ("+str(location_ids.ids)[1:-1]+") "
                            "AND sm.state = 'done' "
                        "UNION ALL "
                        "SELECT "
                            "sm.product_id, "
                            "- sm.product_qty, "
                            "sm.date " 
                        "FROM "
                            "stock_move sm "
                        "WHERE "
                            "sm.date <= %s "
                            "AND sm.product_id = %s "
                            "AND sm.location_dest_id NOT IN ("+ str(location_ids.ids)[1:-1]+") "
                            "AND sm.location_id IN ("+ str(location_ids.ids)[1:-1] +") "
                            "AND sm.state = 'done') AS a ",
                                 (str(fdate), wiz['prod_id'], str(fdate), wiz['prod_id']))
                stock = self.env.cr.dictfetchall()
                if stock and stock[0] and stock[0]['quantity']:
                    qty = stock[0]['quantity']
                return qty
        return 0

    @api.multi
    def get_first_cost(self, wiz):
        ''' Тухайн барааны эхний хугацаандах өртөгүүдийг олно.'''
        res = 0.0
        where = where_not = ''
        prod_id = wiz['prod_id']
        wid = wiz['warehouse_id']
        self.env.cr.execute('select view_location_id from stock_warehouse where id=%s', (wid,))
        res2 = self.env.cr.dictfetchall()
        location_ids = self.env['stock.location'].search([('location_id', 'child_of', [res2[0]['view_location_id']]), ('usage', '=', 'internal')])
        if location_ids:
            where = " AND m.location_id NOT IN " + '(' + ','.join(map(str, location_ids.ids)) + ')' + " AND m.location_dest_id IN " + '(' + ','.join(map(str, location_ids.ids)) + ')'
            where_not = " AND m.location_id IN " + '(' + ','.join(map(str, location_ids.ids)) + ')' + " AND m.location_dest_id NOT IN " + '(' + ','.join(map(str, location_ids.ids)) + ')'
        from_date = wiz['from_date'] + ' 23:59:59'
        fdate = datetime.strptime(from_date, '%Y-%m-%d %H:%M:%S') - timedelta(days=1)
        fdate = get_display_day_to_user_day(fdate, self.env.user)
        self.env.cr.execute("""SELECT (m.price_unit * uom.factor / uom2.factor) AS cost,m.date AS date,
                        (m.product_qty / uom.factor * uom2.factor) AS in_qty,
                        (select 0) AS out_qty
                        FROM stock_move AS m
                          LEFT JOIN stock_picking AS p ON m.picking_id = p.id
                          LEFT JOIN res_partner AS rp ON p.partner_id = rp.id
                          LEFT JOIN product_product AS pp ON m.product_id = pp.id
                          LEFT JOIN product_template AS pt ON pp.product_tmpl_id = pt.id
                          LEFT JOIN product_uom AS uom ON m.product_uom = uom.id
                          LEFT JOIN product_uom AS uom2 ON pt.uom_id = uom2.id
                        WHERE m.product_id = %s
                          AND m.state = 'done'
                          AND m.product_qty is not null
                          AND m.date <= %s """ + where + """
                    UNION ALL
                    SELECT (m.price_unit * uom.factor / uom2.factor) AS cost,m.date AS date,
                        (select 0) AS in_qty,
                        (m.product_qty / uom.factor * uom2.factor) AS out_qty
                        FROM stock_move AS m
                            LEFT JOIN stock_picking AS p ON m.picking_id = p.id
                            LEFT JOIN res_partner AS rp ON p.partner_id = rp.id
                            LEFT JOIN product_product AS pp ON m.product_id = pp.id
                            LEFT JOIN product_template AS pt ON pp.product_tmpl_id = pt.id
                            LEFT JOIN product_uom AS uom ON m.product_uom = uom.id
                            LEFT JOIN product_uom AS uom2 ON pt.uom_id = uom2.id
                        WHERE m.product_id = %s
                          AND m.state = 'done'
                          AND m.product_qty is not null
                          AND m.date <= %s """ + where_not + """
                    ORDER BY date
                    """, (prod_id, fdate, prod_id, fdate,))
        stock = self.env.cr.dictfetchall()
        if stock:
            tmp_cost = 0.0
            qty = 0
            for st in stock:
                if st['in_qty'] > 0:
                    qty += st['in_qty']
                if st['out_qty'] > 0:
                    qty -= st['out_qty']
                tmp_cost = st['cost']
            res = tmp_cost
        return res

    @api.multi
    def get_price(self, wiz, ptype):
        # Үнийн түүхээс аль үнийн түүхийг сонгож авахыг асуух
        context = self._context or {}
        from_date = wiz['from_date'] + ' 23:59:59'
        fdate = datetime.strptime(from_date, '%Y-%m-%d %H:%M:%S') - timedelta(days=1)
        fdate = get_display_day_to_user_day(fdate, self.env.user)
        price = 0.0
        if ptype == 'first':
            self.env.cr.execute("select h.list_price from product_price_history h "
                                "join product_template t on (h.product_template_id = t.id) "
                                "join product_product p on (t.id = p.product_tmpl_id) "
                                "where p.id = %s and h.datetime <= %s "
                                "and h.company_id = %s "
                                "order by h.datetime desc limit 1", (wiz['prod_id'], fdate, wiz['company_id']))  # and h.price_type = wiz['price_type']
            fetched = self.env.cr.dictfetchall()
            if fetched and fetched[0]:
                price = fetched[0]['list_price']

        else:
            price = self.env['product.product'].browse(wiz['prod_id']).price_get('list_price')[wiz['prod_id']]
            self.env.cr.execute("select h.list_price from product_price_history h "
                                "join product_template t on (h.product_template_id = t.id) "
                                "join product_product p on (t.id = p.product_tmpl_id) "
                                "where p.id = %s and h.datetime <= %s "
                                "and h.company_id = %s "
                                "order by h.datetime desc limit 1", (wiz['prod_id'], wiz['to_date'] + ' 23:59:59', wiz['company_id']))  # and h.price_type = wiz['price_type']
            fetched = self.env.cr.dictfetchall()
            if fetched and fetched[0]:
                price = fetched[0]['list_price']
        return price

    @api.multi
    def get_move_data(self, wiz):
        # тухайн барааны хөдөлгөөний тоо, байрлал, үнэ зэрэг мэдээллийн барааны хөдөлгөөнөөс шүүх
        context = self._context or {}
        res = []
        where = where_location = where_not_location = ''
        return_ids = []
        prod_id = wiz['prod_id']
        from_date = get_display_day_to_user_day('%s 00:00:00' % str(wiz['from_date']), self.env.user)
        to_date = get_display_day_to_user_day('%s 23:59:59' % str(wiz['to_date']), self.env.user)
        wid = wiz['warehouse_id']
        company = wiz['company_id']
        is_method_stock_move = self.is_method_stock_move
        select_in = ''
        select_out = ''
        join = ''
        join_lot = ''
        number_query = ''
        group_by = ''
        group_by_lot = ''
        select = ''
        if 'sale.order' not in self.env or 'sale.category' not in self.env:
            raise UserError(_('Install Mongolian Sale module!'))
        if 'purchase.order' not in self.env:
            raise UserError(_('Install Purchase Management module!'))
        if 'mrp.production' in self.env:
            select_in = " WHEN m.production_id is not null THEN 'ББ орлого' WHEN m.raw_material_production_id is not null THEN 'ТЭ зарлага' "
            select_out = " WHEN m.production_id is not null THEN 'ББ орлого' WHEN m.raw_material_production_id is not null THEN 'ТЭ зарлага' "
            number_query = " WHEN m.production_id is not null THEN mrp.name "
            join = ' LEFT JOIN mrp_production AS mrp ON (m.production_id = mrp.id OR m.raw_material_production_id = mrp.id) '
            group_by = 'm.production_id,mrp.name,'
        if wiz['draft']:
            where += " AND m.state <> 'cancel' "
        else:
            where += " AND m.state = 'done' "
        if is_method_stock_move == True:   
            select = """ '' AS lot, SUM(coalesce(m.product_qty / u.factor * u2.factor)) AS qty, 0  AS lot_qty """
        else:
            join_lot =  """ LEFT JOIN stock_move_operation_link AS link ON (m.id = link.move_id)
                    LEFT JOIN stock_pack_operation AS spo ON (link.operation_id = spo.id)
                    LEFT JOIN stock_pack_operation_lot spo_lot ON (spo_lot.operation_id = spo.id)
                    LEFT JOIN stock_production_lot lot ON (spo_lot.lot_id = lot.id)
                    LEFT JOIN stock_production_lot lot2 ON (m.restrict_lot_id = lot2.id) """
            select = """(CASE WHEN m.restrict_lot_id is not null THEN lot2.name  ELSE lot.name END) AS lot, 
                        SUM(coalesce(CASE WHEN link.move_id is not null THEN (link.qty / u.factor * u2.factor)
                                ELSE (m.product_qty / u.factor * u2.factor) END, 0)) AS qty, 
                        SUM(DISTINCT coalesce(CASE WHEN m.restrict_lot_id is not null THEN (m.product_qty / u.factor * u2.factor) ELSE spo_lot.qty  END, 0)) AS lot_qty
                                """
            group_by_lot = """ lot.name,lot2.name, """
            
        if wiz['lot_id']:
            where += " AND (spo_lot.lot_id = %s OR m.restrict_lot_id = %s)" % (str(wiz['lot_id']), str(wiz['lot_id']))
        self.env.cr.execute('select view_location_id from stock_warehouse where id=%s', (wiz['warehouse_id'],))
        res2 = self.env.cr.dictfetchall()
        location_ids = self.env['stock.location'].search([('location_id', 'child_of', [res2[0]['view_location_id']]), ('usage', '=', 'internal')])
        if location_ids:
            where_location = "AND m.location_id IN " + '(' + ','.join(map(str, location_ids.ids)) + ')' + " AND m.location_dest_id NOT IN " + '(' + ','.join(map(str, location_ids.ids)) + ')'
            where_not_location = "AND m.location_id NOT IN " + '(' + ','.join(map(str, location_ids.ids)) + ')' + " AND m.location_dest_id IN " + '(' + ','.join(map(str, location_ids.ids)) + ')'
        self.env.cr.execute("""SELECT (CASE WHEN m.picking_id is not null THEN m.picking_id ELSE 0 END) AS picking, m.date AS date, ('out') AS type,
                        (CASE WHEN m.picking_id is not null THEN rp.name WHEN m.warehouse_id is not null
                            and sw.partner_id is not null THEN rp2.name ELSE '' END) AS partner,
                        (CASE WHEN m.inventory_id is not null THEN 'inventory'  """ + select_out + """
                            ELSE '' END) AS rep_type, m.state AS state,
                        (CASE WHEN m.picking_id is not null THEN p.name
                            WHEN m.inventory_id is not null THEN i.name ELSE COALESCE(m.origin, 'pos') END) AS dugaar,
                        SUM(DISTINCT coalesce(m.price_unit)) AS cost,
                        (pt.list_price) AS price, sl.name AS location,
                        m.origin AS origin,
                        """ + select + """
                        
                    FROM stock_move AS m
                        LEFT JOIN stock_picking AS p ON (m.picking_id = p.id)
                        LEFT JOIN stock_picking_type AS spt ON (p.picking_type_id = spt.id)
                        LEFT JOIN res_partner AS rp ON (p.partner_id = rp.id)
                        """ + join + """
                        LEFT JOIN stock_inventory AS i ON (m.inventory_id = i.id)
                        JOIN product_product AS pp ON (m.product_id = pp.id)
                        JOIN product_template AS pt ON (pp.product_tmpl_id = pt.id)
                        JOIN product_uom AS u ON (m.product_uom = u.id)
                        JOIN product_uom AS u2 ON (pt.uom_id = u2.id)
                        JOIN stock_location AS sl ON (m.location_dest_id = sl.id)
                        LEFT JOIN stock_warehouse AS sw ON (m.warehouse_id = sw.id)
                        LEFT JOIN res_partner AS rp2 ON (sw.partner_id = rp2.id)
                        """ + join_lot + """
                    WHERE m.product_id = %s AND m.product_qty is not null
                        AND m.date >=%s AND m.date <= %s  """ + where + """ """ + where_location + """
                    GROUP BY m.id, m.date, """ + group_by_lot + """ m.picking_id,m.restrict_lot_id,rp.name,pt.list_price,sl.name,m.warehouse_id, m.origin,
                    sw.partner_id,rp2.name, """ + group_by + """ m.state,m.inventory_id,sl.usage,p.picking_type_id,spt.code,p.name,
                    i.name
                UNION ALL
                    SELECT (CASE WHEN m.picking_id is not null THEN m.picking_id ELSE 0 END) AS picking, m.date AS date, ('in') AS type,
                        (CASE WHEN m.picking_id is not null THEN rp.name WHEN m.warehouse_id is not null
                            and sw.partner_id is not null THEN rp2.name ELSE '' END) AS partner,
                        (CASE WHEN m.inventory_id is not null THEN 'inventory' """ + select_in + """
                            ELSE '' END) AS rep_type, m.state AS state,
                        (CASE WHEN m.picking_id is not null THEN p.name
                        """ + number_query + """
                            WHEN m.inventory_id is not null THEN i.name ELSE COALESCE(m.origin, 'pos') END) AS dugaar,
                            SUM(DISTINCT coalesce(m.price_unit)) AS cost,
                            pt.list_price AS price, sl.name AS location,
                        m.origin AS origin,
                        """ + select + """
                    FROM stock_move AS m
                        LEFT JOIN stock_picking AS p ON (m.picking_id = p.id)
                        LEFT JOIN stock_picking_type AS spt ON (p.picking_type_id = spt.id)
                        """ + join + """
                        LEFT JOIN res_partner AS rp ON (p.partner_id = rp.id)
                        LEFT JOIN stock_inventory AS i ON (m.inventory_id = i.id)
                        JOIN product_product AS pp ON (m.product_id = pp.id)
                        JOIN product_template AS pt ON (pp.product_tmpl_id = pt.id)
                        JOIN product_uom AS u ON (m.product_uom = u.id)
                        JOIN product_uom AS u2 ON (pt.uom_id = u2.id)
                        JOIN stock_location AS sl ON (m.location_id = sl.id)
                        LEFT JOIN stock_warehouse AS sw ON (m.warehouse_id = sw.id)
                        LEFT JOIN res_partner AS rp2 ON (sw.partner_id = rp2.id)
                        """ + join_lot + """
                    WHERE m.product_id = %s AND m.product_qty is not null
                        AND m.date >= %s  AND m.date <= %s """ + where + """ """ + where_not_location + """
                    GROUP BY m.id, m.date, """ + group_by_lot + """ m.picking_id,m.restrict_lot_id,rp.name,m.warehouse_id,sw.partner_id,rp2.name,p.name,m.inventory_id,i.name,m.state, m.origin,
                    """ + group_by + """ sl.usage,p.picking_type_id,spt.code,pt.list_price,  sl.name
                ORDER BY date, type""", (prod_id, from_date, to_date, prod_id, from_date, to_date))
        result = self.env.cr.dictfetchall()
        for r in result:
            lot = r['lot'] or ''
            in_qty = out_qty = in_lot_qty = out_lot_qty = 0.0
            if r['type'] == 'in':
                in_qty = r['qty']
                in_lot_qty = r['lot_qty']
            else:
                out_qty = r['qty']
                out_lot_qty = r['lot_qty']
            date = datetime.strptime(r['date'], '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')
            if wiz['report_type'] != 'price' and r['type'] == 'price':
                continue
            res.append({'date': date,
                        'lot': lot,
                        'state': r['state'],
                        'location': r['location'],
                        'dugaar': r['dugaar'],
                        'rep_type': r['rep_type'],
                        'origin': r['origin'] if 'origin' in r.keys() else '',
                        'partner': r['partner'],
                        'price': r['price'],
                        'in_qty': in_qty,
                        'out_qty': out_qty,
                        'in_lot_qty': in_lot_qty,
                        'out_lot_qty': out_lot_qty,
                        'picking': r['picking'],
                        'cost': r['cost'] or 0.0
                        })
        return res

    @api.multi
    def print_report(self):
        # pdf файл хэвлэх процесс
        context = self._context or {}
        user = self.env.user
        wiz = {}
        wiz.update({'company_id': user.company_id.id})
        lot_name = ''
        due_date = ''
        if self.prodlot_id:
            lot = self.prodlot_id
            if lot and lot.name:
                lot_name = lot.name
            if lot and lot.life_date:
                due_date = lot.life_date
        wiz.update({'draft': self.draft,
                    'report_type': self.report_type,
                    'warehouse_id': self.warehouse_id.id,
                    'wname': self.warehouse_id.name,
                    'prod_id': self.product_id.id,
                    'from_date': self.from_date,
                    'to_date': self.to_date})
        wiz.update({'report_type': self.report_type})
        wiz.update({'warehouse_id': self.warehouse_id.id})
        wiz.update({'prod_id': self.product_id.id})
        wiz.update({'lot_id': (self.prodlot_id and self.prodlot_id.id) or False})
        first_avail = self.get_available(wiz)
        first_cost = self.get_first_cost(wiz) or 0
        first_price = self.get_price(wiz, 'first')
        result = self.get_move_data(wiz)
        data = {
            'ids': [],
            'model': 'product.move.check.report.wizard',
            'self': self,
            'wizard': {'from_date': self.from_date,
                       'to_date': self.to_date,
                       'prod_id': self.product_id.id,
                       'company': user.company_id.name,
                       'company_id': user.company_id.id,
                       'lot_name': lot_name,
                       'life_date': due_date,
                       'warehouse_id': self.warehouse_id.id,
                       'wname': self.warehouse_id.name,
                       'report_type': self.report_type,
                       'first_avail': first_avail,
                       'first_cost': first_cost,
                       'first_price': first_price,
                       'draft': self.draft,
                       'is_method_stock_move': self.is_method_stock_move, 
                       'prodlot_id': self.prodlot_id.id}
        }
        if data['self'].report_type == 'owner':
            body = (_('Product move check report (Start date="%s", End date="%s", Product="%s",Warehouse="%s",prodlot_id="%s"')
                    ) % (self.from_date, self.to_date, self.product_id.name, self.warehouse_id.name, self.prodlot_id and self.prodlot_id.name)
            message = _('[Report][PDF][PROCESSING] %s') % body
            _logger.info(message)
        elif data['self'].report_type == 'price':
            body = (_('Product move check report (Price Unit)(Start date="%s", End date="%s", Product="%s",Warehouse="%s",prodlot_id="%s"')
                    ) % (self.from_date, self.to_date, self.product_id.name, self.warehouse_id.name, self.prodlot_id and self.prodlot_id.name)
            message = _('[Report][PDF][PROCESSING] %s') % body
            _logger.info(message)
        elif data['self'].report_type == 'cost':
            body = (_('Product move check report (Cost)(Start date="%s", End date="%s", Product="%s",Warehouse="%s",prodlot_id="%s"')
                    ) % (self.from_date, self.to_date, self.product_id.name, self.warehouse_id.name, self.prodlot_id and self.prodlot_id.name)
            message = _('[Report][PDF][PROCESSING] %s') % body
            _logger.info(message)
        data.update({'self': self.id})
        return self.env.get("report").get_action([], 'l10n_mn_stock_base_report.product_move_check_report', data=data)

    @api.multi
    def export_report(self):
        # Тайлан хэвлэх процесс
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        report_name = self._name.replace('.', '_')
        filename = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'))

        format_title = book.add_format(ReportExcelCellStyles.format_title)
        # Баримтын толгой
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        # Хүснэгтийн толгой
        format_group = book.add_format(ReportExcelCellStyles.format_group)
        format_group_float = book.add_format(ReportExcelCellStyles.format_group_float)
        format_content_bold_float = book.add_format(ReportExcelCellStyles.format_content_bold_float)
        # Хүснэгтийн агуулга
        format_content_bold_text = book.add_format(ReportExcelCellStyles.format_content_bold_text)
        format_content_center = book.add_format(ReportExcelCellStyles.format_content_center)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)
        format_content_float_color = book.add_format(ReportExcelCellStyles.format_content_float_color)
        format_content_center_color = book.add_format(ReportExcelCellStyles.format_content_center_color)
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=(report_name), self_title=filename).create({})

        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Arial"&9&P', {'margin': 0.1})
        rowx = 0

        body = self.get_log_message()
        message = _('[Product move check report][XLS][PROCESSING] %s') % body
        _logger.info(message)

        module = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_product_expense')])

        wiz = {}
        ctx = self._context.copy()
        user = self.env.user
        if 'company' not in ctx:
            ctx.update({'company_id': user.company_id.id})
        wiz.update({'company_id': user.company_id.id})
        lot_name = due_date = '..................'
        max_qty = min_qty = '..................'
        wiz['draft'] = self.draft
        wiz.update({'draft': self.draft,
                    'report_type': self.report_type,
                    'warehouse_id': self.warehouse_id.id,
                    'prod_id': self.product_id.id,
                    'from_date': self.from_date,
                    'to_date': self.to_date})
        wiz.update({'report_type': self.report_type})
        wiz.update({'warehouse_id': self.warehouse_id.id})
        wiz.update({'prod_id': self.product_id.id})
        wiz.update({'lot_id': (self.prodlot_id and self.prodlot_id.id) or False})
        if self.prodlot_id:
            lot = self.prodlot_id
            if lot and lot.name:
                lot_name = lot.name
            if lot and lot.life_date:
                due_date = lot.life_date

        first_avail = self.get_available(wiz) or 0
        first_avail_lot = first_avail
        result = self.get_move_data(wiz)
        self.env.cr.execute("""SELECT product_max_qty AS max_qty, product_min_qty AS min_qty
                        FROM stock_warehouse_orderpoint WHERE product_id = %s AND warehouse_id = %s""" % (self.product_id.id, self.warehouse_id.id))
        point = self.env.cr.dictfetchall()
        if point and point[0]['max_qty']:
            max_qty = str(point[0]['max_qty'])
        if point and point[0]['min_qty']:
            min_qty = str(point[0]['min_qty'])
        report_name = u''
        mayagt = u''
        if self.report_type == 'price':
            mayagt = _('FORM PT-5-3')
            report_name = _('Product move check report (Price Unit)')
            sheet.merge_range(rowx, 0, rowx, 14, user.company_id.partner_id.name, format_filter)
        elif self.report_type == 'cost':
            mayagt = _('FORM PT-5-2')
            report_name = _('Product move check report (Cost)')
            sheet.merge_range(rowx, 0, rowx, 13, user.company_id.partner_id.name, format_filter)
        else:
            mayagt = _('FORM PT-5-1')
            report_name = _('Product move check report')
            sheet.merge_range(rowx, 0, rowx, 10, user.company_id.partner_id.name, format_filter)
        rowx += 1
        sheet.write(rowx, 2, mayagt, format_filter)
        rowx += 1
        sheet.merge_range(rowx, 2, rowx, 7, report_name, format_title)
        now_date = str(get_day_like_display(fields.Datetime.now(), self.env.user))[:16]
        rowx += 2
        sheet.write(rowx, 2, _('Sector name: %s') % (self.warehouse_id.name), format_filter)
        sheet.write(rowx, 6, _('Document Number: -----'), format_filter)
        sheet.write(rowx + 1, 2, _('Product Barcode: %s') % (self.product_id.barcode or ''), format_filter)
        sheet.write(rowx + 1, 6, _('Product Code : %s') % (self.product_id.default_code or ''), format_filter)
        sheet.write(rowx + 2, 2, _('Product Name:   %s') % (self.product_id.name or ''), format_filter)
        sheet.write(rowx + 2, 6, _('Product UoM:   %s') % self.product_id.uom_id.name, format_filter)
        sheet.write(rowx + 3, 2, _('Serial Number: %s') % (lot_name), format_filter)
        sheet.write(rowx + 3, 6, _('End Date: %s') % (due_date), format_filter)
        sheet.write(rowx + 4, 2, _('Safety Stock: %s') % (min_qty), format_filter)
        sheet.write(rowx + 4, 6, _('Max Stock: %s') % (max_qty), format_filter)
        sheet.write(rowx + 5, 2, _('Printed Date: %s') % (now_date), format_filter)
        sheet.write(rowx + 5, 6, _('Check Duration: %s - %s') % (self.from_date, self.to_date), format_filter)
#         sheet.row(1).height = 400
        rowx += 7
        count = 0
        inch = 300

        sheet.merge_range(rowx, 1, rowx + 1, 1, _('Seq'), format_group)
        sheet.merge_range(rowx, 2, rowx, 6, _('Document'), format_group)
        sheet.write(rowx + 1, 2, _('Date'), format_group)
        sheet.write(rowx + 1, 3, _('Type'), format_group)
        sheet.write(rowx + 1, 4, _('Number'), format_group)
        sheet.write(rowx + 1, 5, _('Move'), format_group)
        sheet.write(rowx + 1, 6, _('Serial'), format_group)
        sheet.merge_range(rowx, 7, rowx + 1, 7, _('Partner'), format_group)
        if self.report_type == 'owner':
            sheet.merge_range(rowx, 8, rowx, 10, _('QTY'), format_group)
            sheet.write(rowx + 1, 8, _('Income'), format_group)
            sheet.write(rowx + 1, 9, _('Expense'), format_group)
            sheet.write(rowx + 1, 10, _('Balance'), format_group)
        elif self.report_type == 'price':
            sheet.merge_range(rowx, 8, rowx + 1, 8, _('QTY'), format_group)
            sheet.merge_range(rowx, 9, rowx + 1, 9, _('Price Unit'), format_group)
            sheet.merge_range(rowx, 10, rowx, 13, _('Total /MNT/'), format_group)
            sheet.write(rowx + 1, 10, _('Income'), format_group)
            sheet.write(rowx + 1, 11, _('Expense'), format_group)
            sheet.write(rowx + 1, 12, _('Price diff'), format_group)
            sheet.write(rowx + 1, 13, _('Balance'), format_group)
            sheet.merge_range(rowx, 14, rowx + 1, 14, _('Balance Quantity'), format_group)
        else:
            sheet.merge_range(rowx, 8, rowx + 1, 8, _('QTY'), format_group)
            sheet.merge_range(rowx, 9, rowx + 1, 9, _('Cost'), format_group)
            sheet.merge_range(rowx, 10, rowx, 12, _('Total /Cost/'), format_group)
            sheet.write(rowx + 1, 10, _('Income'), format_group)
            sheet.write(rowx + 1, 11, _('Expense'), format_group)
            sheet.write(rowx + 1, 12, _('Balance'), format_group)
            sheet.merge_range(rowx, 13, rowx + 1, 13, _('Balance Quantity'), format_group)
        rowx += 2

        sheet.write(rowx, 1, _('X'), format_content_center)
        sheet.write(rowx, 2, _('X'), format_content_center)
        sheet.write(rowx, 3, _('X'), format_content_center)
        sheet.write(rowx, 4, _('X'), format_content_center)
        sheet.write(rowx, 5, _('X'), format_content_center)
        sheet.write(rowx, 6, _('X'), format_content_center)
        sheet.write(rowx, 7, _('Initial Balance'), format_content_center)
        if self.report_type == 'owner':
            sheet.write(rowx, 8, _('X'), format_content_bold_text)
            sheet.write(rowx, 9, _('X'), format_content_center)
            sheet.write(rowx, 10, first_avail, format_content_bold_float)
        elif self.report_type == 'price':
            first_price = self.get_price(wiz, 'first') or 0
            change = change_total = change_total_lot = 0
            if first_price != 0:
                change = first_price
            sheet.write(rowx, 8, first_avail, format_content_bold_float)
            sheet.write(rowx, 9, first_price, format_content_bold_float)
            sheet.write(rowx, 10, u'X', format_content_center)
            sheet.write(rowx, 11, u'X', format_content_center)
            sheet.write(rowx, 12, u'X', format_content_center)
            sheet.write(rowx, 13, (first_avail != 0 and first_price != 0) and (first_avail * first_price) or '', format_content_bold_float)
            sheet.write(rowx, 14, first_avail, format_content_bold_float)
        else:
            first_cost = self.get_first_cost(wiz) or 0
            last_cost = first_cost or 0
            balance = (first_avail * first_cost) or 0
            lot_balance = (first_avail * first_cost) or 0
            sheet.write(rowx, 8, first_avail, format_content_bold_float)
            sheet.write(rowx, 9, first_cost, format_content_bold_float)
            sheet.write(rowx, 10, u'X', format_content_center)
            sheet.write(rowx, 11, u'X', format_content_center)
            sheet.write(rowx, 12, balance, format_content_bold_float)
            sheet.write(rowx, 13, first_avail, format_content_bold_float)
        rowx += 1

        in_total = out_total = in_lot_total = out_lot_total = 0
        qty = unit = 0

        seri = ''
        if result:
            for r in result:
                price = 0  # self.product_id.lst_price
                diff = 0
                count += 1
                rep_type = ''
                dugaar = partner = seri = supply_warehouse_name = ''
                in_qty = out_qty = in_lot_qty = out_lot_qty = 0
                if r['dugaar']:
                    if r['dugaar'] == 'pos':
                        dugaar = r['location']
                    elif self.report_type == 'price' and r['dugaar'] == 'price':
                        if r['price']:
                            change = r['price']
                        dugaar = _('Price diff')
                    else:
                        dugaar = r['dugaar']
                if r['partner']:
                    partner = r['partner']
                if r['lot']:
                    seri = r['lot']
                if r['in_qty'] and r['in_qty'] != 0:                        # Орлого тооцох хэсэг
                    qty += r['in_qty']
                    in_qty = r['in_qty']
                    first_avail += r['in_qty'] or 0
                    diff = r['in_qty']
                    if self.report_type == 'owner':
                        in_total += r['in_qty']
                    elif self.report_type == 'price' and r['price']:
                        in_total += (in_qty * r['price'])
                    elif self.report_type == 'cost' and r['cost']:
                        in_total += (in_qty * r['cost'])
                        balance += (in_qty * r['cost'])
                    if self.report_type == 'cost' and first_avail != 0:
                        last_cost = balance / first_avail
                if r['in_lot_qty'] and r['in_lot_qty'] != 0:                # Цувралтай бол цувралын орлого тооцох хэсэг
                    qty += r['in_qty']
                    in_lot_qty = r['in_lot_qty']
                    first_avail_lot += r['in_lot_qty'] or 0
                    diff = r['in_lot_qty']
                    if self.report_type == 'owner':
                        in_total += r['in_qty']
                    elif self.report_type == 'price' and r['price']:
                        in_lot_total += (in_lot_qty * r['price'])
                    elif self.report_type == 'cost' and r['cost']:
                        in_lot_total += (in_lot_qty * r['cost'])
                        lot_balance += (in_lot_qty * r['cost'])
                    if self.report_type == 'cost' and first_avail_lot != 0:
                        last_cost = lot_balance / first_avail_lot
                if r['out_qty'] and r['out_qty'] != 0:                        # Зарлага тооцох хэсэг
                    qty += r['out_qty']
                    out_qty = r['out_qty']
                    first_avail -= r['out_qty'] or 0
                    diff = r['out_qty']
                    if self.report_type == 'owner':
                        out_total += r['out_qty']
                    elif self.report_type == 'price' and r['price']:
                        out_total += (out_qty * r['price'])
                    elif self.report_type == 'cost' and r['cost']:
                        out_total += (out_qty * r['cost'])
                        balance -= (out_qty * r['cost'])
                        last_cost = r['cost']
                if r['out_lot_qty'] and r['out_lot_qty'] != 0:                 # Цувралтай бол цувралын зарлага тооцох хэсэг
                    qty += r['out_qty']
                    out_lot_qty = r['out_lot_qty']
                    first_avail_lot -= r['out_lot_qty'] or 0
                    diff = r['out_lot_qty']
                    if self.report_type == 'owner':
                        out_total += r['out_qty']
                    elif self.report_type == 'price' and r['price']:
                        out_lot_total += (out_lot_qty * r['price'])
                    elif self.report_type == 'cost' and r['cost']:
                        out_lot_total += (out_lot_qty * r['cost'])
                        lot_balance -= (out_lot_qty * r['cost'])
                        last_cost = r['cost']
                if self.report_type == 'price' and r['price']:
                    if change == 0:
                        change = r['price']
                    if change != r['price']:
                        if r['dugaar'] and r['dugaar'] == 'price':
                            unit = (r['price'] - change)
                            change_total += (unit * first_avail)
                            change_total_lot += (unit * first_avail_lot)
                    price = r['price']
                cost = 0.0
                if self.report_type == 'cost' and r['cost']:
                    cost = r['cost']

                picking_id = 0
                if r['picking']:
                    picking_id = r['picking']
                if picking_id > 0:
                    picking = self.env['stock.picking'].search([('id', '=', picking_id)])
                    if picking.transit_order_id:
                        rep_type = _('Transit')
                        supply_warehouse_name += picking.transit_order_id.supply_warehouse_id.name + ' --> ' + picking.transit_order_id.warehouse_id.name
                    if module.state == 'installed' and picking.expense:
                        rep_type = _('Expense')
                    elif picking.picking_type_id.code == 'internal':
                        rep_type = _('Internal Move')
                    elif picking.group_id:
                        sale = self.env['sale.order'].search([('procurement_group_id', '=', picking.group_id.id)])
                        if sale:
                            rep_type = _('Sales / (%s)') % sale.sale_category_id.name or ''
                    for line in picking.move_lines:
                        if line.purchase_line_id:
                            rep_type = _('Purchase')
                        elif line.origin_returned_move_id:
                            rep_type = _('Return')
                if r['rep_type']:
                    if r['rep_type'] == 'inventory':
                        rep_type = _('Inventory')
                    else:
                        rep_type = r['rep_type']
                if rep_type == '':
                    rep_type = _('Point of Sale')

                if self.draft and r['state'] != 'done':
                    content = format_content_center_color
                    content_float = format_content_float_color
                else:
                    content = format_content_center
                    content_float = format_content_float

                date_format = str(get_day_like_display(r['date'], self.env.user))

                sheet.write(rowx, 1, count, content)
                sheet.write(rowx, 2, date_format, content)
                sheet.write(rowx, 3, rep_type, content)
                sheet.write(rowx, 4, dugaar, content)
                sheet.write(rowx, 5, supply_warehouse_name, content)
                sheet.write(rowx, 6, seri, content)
                sheet.write(rowx, 7, partner, content)
                if self.report_type == 'owner':
                    if seri:
                        sheet.write(rowx, 8, (in_lot_qty != 0 and in_lot_qty) or '', content_float)
                        sheet.write(rowx, 9, (out_lot_qty != 0 and out_lot_qty) or '', content_float)
                        sheet.write(rowx, 10, first_avail_lot, content_float)
                    else:
                        sheet.write(rowx, 8, (in_qty != 0 and in_qty) or '', content_float)
                        sheet.write(rowx, 9, (out_qty != 0 and out_qty) or '', content_float)
                        sheet.write(rowx, 10, first_avail, content_float)
                elif self.report_type == 'price':
                    sheet.write(rowx, 8, (diff != 0 and diff) or '', content_float)
                    sheet.write(rowx, 9, (price != 0 and price) or '', content_float)
                    if seri:
                        sheet.write(rowx, 10, (in_lot_qty != 0 and in_lot_qty * price) or '', content_float)
                        sheet.write(rowx, 11, (out_lot_qty != 0 and out_lot_qty * price) or '', content_float)
                        sheet.write(rowx, 14, first_avail_lot, content_float)
                    else:
                        sheet.write(rowx, 10, (in_qty != 0 and in_qty * price) or '', content_float)
                        sheet.write(rowx, 11, (out_qty != 0 and out_qty * price) or '', content_float)
                        sheet.write(rowx, 14, first_avail, content_float)
                    sheet.write(rowx, 12, ((unit > 0 and first_avail > 0 and unit * first_avail) or (unit < 0 and first_avail > 0 and '(' + str(abs(unit * first_avail)) + ')')) or '', content_float)
                    sheet.write(rowx, 13, (change != 0 and (change * first_avail)) or '', content_float)

                else:
                    sheet.write(rowx, 8, (diff != 0 and diff) or '', content_float)
                    sheet.write(rowx, 9, (cost != 0 and cost) or '', content_float)
                    if seri:
                        sheet.write(rowx, 10, (in_qty != 0 and in_qty * cost) or '', content_float)
                        sheet.write(rowx, 11, (out_qty != 0 and out_qty * cost) or '', content_float)
                        sheet.write(rowx, 12, '', content_float)
                        sheet.write(rowx, 13, first_avail_lot, content_float)
                    else:
                        sheet.write(rowx, 10, (in_qty != 0 and in_qty * cost) or '', content_float)
                        sheet.write(rowx, 11, (out_qty != 0 and out_qty * cost) or '', content_float)
                        sheet.write(rowx, 12, balance or '', content_float)
                        sheet.write(rowx, 13, first_avail, content_float)
                rowx += 1

        sheet.write(rowx, 1, '', format_content_center)
        sheet.write(rowx, 2, '', format_content_center)
        sheet.write(rowx, 3, '', format_content_bold_text)
        sheet.write(rowx, 4, '', format_content_center)
        sheet.write(rowx, 5, '', format_content_center)
        sheet.write(rowx, 6, '', format_content_center)
        if self.report_type == 'owner':
            sheet.write(rowx, 7, _('Total'), format_content_center)
            if seri:
                sheet.write(rowx, 8, in_lot_total, format_content_bold_float)
                sheet.write(rowx, 9, out_lot_total, format_content_bold_float)
                sheet.write(rowx, 10, first_avail_lot, format_content_bold_float)
            else:
                sheet.write(rowx, 8, in_total, format_content_bold_float)
                sheet.write(rowx, 9, out_total, format_content_bold_float)
                sheet.write(rowx, 10, first_avail, format_content_bold_float)
            rowx += 1
            sheet.write(rowx, 2, _('Check: '), format_filter)
            sheet.write(rowx, 8, _('Real balance: '), format_filter)
            sheet.write(rowx, 8, _('Difference'), format_filter)
            sheet.write(rowx, 10, '', format_group)
            sheet.write(rowx, 10, '', format_group)
            rowx += 3
            sheet.write(rowx, 3, _('Product keeper comment: ................................................................................................'), format_filter)
            sheet.write(rowx + 1, 6, _('.........................................................................................................................'), format_filter)
            sheet.write(rowx + 2, 3, _('Conclusions and decisions on checks: ......................................................................................'), format_filter)
            sheet.write(rowx + 3, 6, _('.........................................................................................................................'), format_filter)
            sheet.write(rowx + 4, 3, _('Authorized checker: ...................................work.............................................'), format_filter)
            sheet.write(rowx + 6, 3, _('attended: signature: .............................................................../......................................./'), format_filter)
            sheet.write(rowx + 7, 6, _('signature: ........................................................................./......................................./'), format_filter)
            sheet.write(rowx + 8, 3, _('Product keeper: ......................................................................./......................................./'), format_filter)
            sheet.write(rowx + 9, 6, _('...................................................................................../......................................./'), format_filter)
            sheet.write(rowx + 10, 3, _('Date: .................................'), format_filter)

        elif self.report_type in ('price', 'cost'):
            sheet.write(rowx, 7, _('Income, Expense Total'), format_content_bold_text)
            sheet.write(rowx, 8, '', format_content_center)
            sheet.write(rowx, 9, '', format_content_center)
            if seri:
                sheet.write(rowx, 10, in_lot_total, format_content_bold_float)
                sheet.write(rowx, 11, out_lot_total, format_content_bold_float)
                col = 11
                if self.report_type == 'price':
                    sheet.write(rowx, 12, change_total_lot, format_content_bold_float)
                    col += 1
            else:
                sheet.write(rowx, 10, in_total, format_content_bold_float)
                sheet.write(rowx, 11, out_total, format_content_bold_float)
                col = 11
                if self.report_type == 'price':
                    sheet.write(rowx, 12, change_total, format_content_bold_float)
                    col += 1
            sheet.write(rowx, col + 1, '', format_content_center)
            sheet.write(rowx, col + 2, '', format_content_center)
            if self.report_type == 'cost':
                rowx += 1
                last_total = last_total_qty = 0
                sheet.write(rowx, 1, u'X', format_content_center)
                sheet.write(rowx, 2, u'X', format_content_center)
                sheet.write(rowx, 3, u'X', format_content_center)
                sheet.write(rowx, 4, u'X', format_content_center)
                sheet.write(rowx, 5, u'X', format_content_center)
                sheet.write(rowx, 6, u'X', format_content_center)
                sheet.write(rowx, 7, u'', format_content_center)
                sheet.write(rowx, 8, (qty and qty) or '0.0', format_content_bold_float)
                sheet.write(rowx, 9, (last_cost and last_cost) or '0.0', format_content_bold_float)
                sheet.write(rowx, 10, u'X', format_content_center)
                sheet.write(rowx, 11, u'X', format_content_center)
                sheet.write(rowx, 12, (last_cost and first_avail and (last_cost * first_avail)) or '0.0', format_content_bold_float)
                sheet.write(rowx, 13, first_avail, format_content_bold_float)
            rowx += 3
            sheet.write(rowx, 3, _('Accountant: ......................................................................./......................................./'), format_filter)
            rowx += 2
            sheet.write(rowx, 3, _('Date: .................................'), format_filter)

        sheet.set_column(0, 0, 2)
        sheet.set_column(1, 1, 5)
        sheet.set_column(2, 4, 15)
        sheet.set_column(5, 5, 40)
        sheet.set_column(6, 6, 15)
        sheet.set_column(7, 7, 20)
        sheet.set_column(8, 9, 8)
        sheet.set_column(10, 18, 12)

        book.close()
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        return report_excel_output_obj.export_report()
