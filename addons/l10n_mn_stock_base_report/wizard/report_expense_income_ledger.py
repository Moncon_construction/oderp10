# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
import pytz
from datetime import datetime
from odoo import models, fields, api, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from odoo.addons.l10n_mn_report.tools.tools import str_tuple
from odoo.addons.l10n_mn_web.models.time_helper import *


class ReportExpenseIncomeLedger(models.TransientModel):
    """
           БАРАА МАТЕРИАЛЫН ЗАРЛАГА, ОРЛОГЫН ТОВЧОО ТАЙЛАН
    """
    _inherit = 'oderp.report.html.output'
    _name = 'report.expense.income.ledger'
    _description = 'Report Expense Income Ledger'

    company_id = fields.Many2one('res.company', 'Company', readonly=True,
                                 default=lambda self: self.env['res.company']._company_default_get('stock.inventory'))
    warehouse_ids = fields.Many2many('stock.warehouse', string='Warehouse')

    @api.multi
    def default_date(self, date):
        for line in self:
            if line.company_id:
                if date == 'start_date':
                    return line.company_id.start_date
                elif date == 'end_date':
                    return line.company_id.end_date
                return False

    start_date = fields.Date("Start Date", required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    end_date = fields.Date("End Date", required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    expense = fields.Boolean('Expense', defualt=False)
    sales = fields.Boolean('Sales', defualt=False)
    transit = fields.Boolean('Transit', defualt=False)
    purchase = fields.Boolean('Purchase', default=False)

    @api.onchange('start_date')
    def _get_warehouse_and_categ_domain(self):
        domain = {}
        domain['warehouse_ids'] = [('company_id', '=', self.company_id.id)]
        _warehouses = []
        for warehouse in self.env.user.allowed_warehouses:
            _warehouses.append(warehouse.id)
        if _warehouses:
            domain['warehouse_ids'] = [('id', 'in', _warehouses)]
        return {'domain': domain}

    @api.onchange('warehouse_ids')
    @api.model
    def _compute_module_state(self):
        '''
                    модуль суусан эсэхийг шалгана.
        '''
        if 'purchase.order' in self.env and 'discount' in self.env['purchase.order.line']:
            self.purchase_installed = True
        if 'product.expense' in self.env:
            self.expense_install = True
        if 'sale.order' in self.env and 'invoice_status' in self.env['sale.order.line']:
            self.sales_install = True
        module = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_sale_stock')])
        if module:
            if module.state == 'installed':
                self.sales_install = True
            else:
                self.sales_install = False
        else:
            self.sales_install = False
        if 'stock.transit.order' in self.env:
            self.transit_install = True

    expense_install = fields.Boolean('Expense', compute=_compute_module_state, defualt=False)
    purchase_installed = fields.Boolean('Purchase install', compute=_compute_module_state, default=False)
    sales_install = fields.Boolean('Sales install', compute=_compute_module_state, default=False)
    transit_install = fields.Boolean('Transit install', compute=_compute_module_state, default=False)

    def get_sheet_format(self, sheet):
        # Тайлангийн хуудас
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.39, 0.39, 0.39, 0.39)  # 1cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Arial"&9&P', {'margin': 0.1})
        sheet.hide_gridlines(2)
        sheet.set_landscape()
        return sheet

    @api.multi
    def get_header(self, sheet, header_formats, rowx, report_name):
        StockWarehouse = self.env['stock.warehouse']
        warehouse_ids = StockWarehouse.search([]).ids if not self.warehouse_ids else self.warehouse_ids.ids
        warehouse_names = ''
        for warehouse_id in warehouse_ids:
            warehouse_names += (str(StockWarehouse.browse(warehouse_id).name) + ', ')
        sheet.write(rowx, 0, _('Company name: %s') % self.company_id.name, header_formats['format_filter'])
        rowx += 1
        sheet.write(rowx, 0, _('Duration: %s - %s') % (self.start_date, self.end_date), header_formats['format_filter'])
        rowx += 1
        sheet.write(rowx, 0, _('Warehouse: %s') % warehouse_names[:-2], header_formats['format_filter'])
        rowx += 1
        sheet.write(rowx, 0, _('Date: %s') % time.strftime('%Y-%m-%d'), header_formats['format_filter'])
        rowx += 1
        sheet.merge_range(rowx, 2, rowx, 9, report_name.upper(), header_formats['format_name'])
        rowx += 3
        return sheet, rowx

    def get_footer(self, sheet, header_formats, rowx):

        sheet.write(rowx, 1,
                    _('Executive Director .................................. (                                   )'),
                    header_formats['format_filter'])
        sheet.write(rowx + 2, 1,
                    _('General Accountant .................................. (                                   )'),
                    header_formats['format_filter'])
        return sheet

    @api.multi
    def export_report(self):
        ''' Тайлангийн загварыг боловсруулж өгөгдлүүдийг
                    тооцоолж байрлуулна.
        '''
        # Тайлан хэвлэх процесс
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        format_content_left_color_bold = {
            'font_name': 'Times New Roman',
            'font_size': 9,
            'align': 'left',
            'valign': 'vcenter',
            'border': 1,
            'border_color': '#95cae9',
            'bold': True
        }

        # Нэр үүсгэх
        report_name = _('Expense Income ledger report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'))
        # Тайлангийн формат хэсэг
        format_content_header = book.add_format(ReportExcelCellStyles.format_content_header)
        format_content_float_border = book.add_format(ReportExcelCellStyles.format_content_float_border)
        format_content_left_color = book.add_format(ReportExcelCellStyles.format_content_left_color)
        format_title_float_border = book.add_format(ReportExcelCellStyles.format_title_float_border)
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title_color = book.add_format(ReportExcelCellStyles.format_title_color)
        format_content_left_color_bold = book.add_format(format_content_left_color_bold)


        header_formats = {
            'format_filter': format_filter,
            'format_name': format_name,
        }

        content_line_formats = {
            'format_content_float_border': format_content_float_border,
            'format_content_left_color': format_content_left_color,
            'format_title_float_border': format_title_float_border,
            'format_title_color': format_title_color,
            'format_content_left_color_bold': format_content_left_color_bold,
        }

        # тайлангийн обьект
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=report_name,
                                                                                     form_title=file_name,
                                                                                     end_date=self.end_date,
                                                                                     start_date=self.start_date).create(
            {})

        # Тайлангийн хуудас
        sheet = book.add_worksheet(report_name)
        sheet = self.get_sheet_format(sheet)

        # Тооцоолох багана
        sheet.set_column('A:A', 4)
        sheet.set_column('B:B', 30)
        sheet.set_column('B:B', 10)
        sheet.set_column('C:C', 20)
        sheet.set_column('D:D', 10)
        sheet.set_column('E:E', 10)
        sheet.set_column('F:F', 10)
        sheet.set_column('G:G', 10)
        sheet.set_column('H:H', 15)
        sheet.set_column('I:I', 15)
        sheet.set_column('J:J', 15)
        sheet.set_column('K:K', 15)
        sheet.set_column('L:L', 15)
        sheet.set_column('M:M', 12)
        sheet.set_column('N:N', 8)

        # тайлангийн(хүснэгт) толгой хэсэг
        rowx = 0
        sheet, rowx = self.get_header(sheet, header_formats, rowx, report_name)

        sheet.merge_range(rowx, 0, rowx + 1, 0, '№', format_content_header)
        sheet.merge_range(rowx, 1, rowx + 1, 1, _('Code'), format_content_header)
        sheet.merge_range(rowx, 2, rowx + 1, 2, _('Product'), format_content_header)
        sheet.merge_range(rowx, 3, rowx + 1, 3, _('Quantity'), format_content_header)
        sheet.merge_range(rowx, 4, rowx + 1, 4, _('Unit Price'), format_content_header)
        sheet.merge_range(rowx, 5, rowx + 1, 5, _('Product UOM'), format_content_header)
        sheet.merge_range(rowx, 6, rowx + 1, 6, _('Amount without NUAT'), format_content_header)
        sheet.merge_range(rowx, 7, rowx + 1, 7, _('NUAT'), format_content_header)
        sheet.merge_range(rowx, 8, rowx + 1, 8, _('Total'), format_content_header)
        sheet.merge_range(rowx, 9, rowx + 1, 9, _('Account'), format_content_header)
        rowx += 2
        date_from = str(get_day_by_user_timezone(self.start_date + " 00:00:00", self.env.user))
        date_to = str(get_day_by_user_timezone(self.end_date + " 23:59:59", self.env.user))

        # Хүснэгт дата бэлдэх
        stock_picking_obj = self.env['stock.picking'].search([('date_done', '>=', date_from), ('date_done', '<=', date_to)])
        product_product_obj = self.env['product.product']
        product_uom_obj = self.env['product.uom']
        product_template_obj = self.env['product.template']
        product_expense_obj = self.env['product.expense']
        res_partner_obj = self.env['res.partner']
        stock_warehouse_obj = self.env['stock.warehouse']

        sale_order_obj = self.env['sale.order']
        sale_order_line_obj = self.env['sale.order.line']

        stock_transit_order_obj = self.env['stock.transit.order']
        stock_transit_order_line_obj = self.env['stock.transit.order.line']

        purchase_order_obj = self.env['purchase.order']
        purchase_order_line_obj = self.env['purchase.order.line']

        for stock_picking in stock_picking_obj:
            move_type_sale = 1 if self.env['stock.picking'].search([('id', '=', stock_picking.id), ('origin', 'like', '%SO%')]) else 0
            move_type_transit = 1 if stock_picking_obj.search([('id', '=', stock_picking.id), ('origin', 'like', '%TRA%'), ('name', 'like', '%OUT%')]) else 0
            move_type_expense = 0
            if stock_picking_obj.search([('id', '=', stock_picking.id), ('origin', 'like', '%ex%')]):
                move_type_expense = 1
            elif stock_picking_obj.search([('id', '=', stock_picking.id), ('origin', 'like', '%EX%')]):
                move_type_expense = 1
            move_type_purchase = 1 if stock_picking_obj.search([('id', '=', stock_picking.id), ('origin', 'like', '%PO%')]) else 0
            sequence = 0
            if move_type_sale and self.sales:
                sale_order_id = sale_order_obj.search([('name', '=', stock_picking.origin)])
                if sale_order_id.warehouse_id in self.warehouse_ids:
                    sheet.write(rowx, 1, _('Sales'), content_line_formats['format_content_left_color_bold'])
                    sheet.write(rowx, 2, stock_picking.origin, content_line_formats['format_content_left_color_bold'])
                    sheet.write(rowx, 3, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 4, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 5, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 6, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 7, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 8, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 9, '', content_line_formats['format_content_left_color'])
                    rowx += 1
                    sheet.write(rowx, 1, _('Partner'), content_line_formats['format_content_left_color'])
                    res_partner_id = res_partner_obj.search([('id', '=', stock_picking.partner_id.id)])
                    if res_partner_id:
                        sheet.write(rowx, 2, res_partner_id.name, content_line_formats['format_content_left_color'])
                    else:
                        sheet.write(rowx, 2, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 3, _('Date'), content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 4, stock_picking.date_done, content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 5, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 6, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 7, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 8, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 9, '', content_line_formats['format_content_left_color'])
                    rowx += 1
                    for line in stock_picking.move_lines:
                        product = self.env['product.product'].search([('id', '=', line.product_id.id)])
                        sequence += 1
                        sol = sale_order_line_obj.search([('order_id', '=', sale_order_id.id), ('product_id', '=', line.product_id.id)], limit=1)
                        tax = sol.price_tax / sol.product_uom_qty * line.product_uom_qty if sol.price_tax > 0 and sol.product_uom_qty > 0 and line.product_uom_qty > 0 else 0
                        taxed = sol.price_subtotal / sol.product_uom_qty * line.product_uom_qty if sol.price_subtotal > 0 and sol.product_uom_qty > 0 and line.product_uom_qty > 0 else 0
                        uom = product_uom_obj.search([('id', '=', sol.product_uom.id)])
                        total = sol.price_unit * line.product_uom_qty
                        sheet.write(rowx, 0, sequence, content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 1, product['default_code'], content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 2, product['name'], content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 3, line['product_uom_qty'], content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 4, sol.price_unit, content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 5, uom.name, content_line_formats['format_content_float_border'])
                        sheet.write(rowx, 6, taxed, content_line_formats['format_content_float_border'])
                        sheet.write(rowx, 7, tax, content_line_formats['format_content_float_border'])
                        sheet.write(rowx, 8, total, content_line_formats['format_content_float_border'])
                        sheet.write(rowx, 9, '', content_line_formats['format_content_float_border'])
                        rowx += 1
            if move_type_transit and self.transit:
                stock_transit_order_id = stock_transit_order_obj.search([('id', '=', stock_picking.transit_order_id.id)])
                if stock_transit_order_id.supply_warehouse_id in self.warehouse_ids:
                    sheet.write(rowx, 1, _('Transit'), content_line_formats['format_content_left_color_bold'])
                    sheet.write(rowx, 2, stock_picking.origin, content_line_formats['format_content_left_color_bold'])
                    sheet.write(rowx, 3, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 4, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 5, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 6, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 7, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 8, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 9, '', content_line_formats['format_content_left_color'])
                    rowx += 1
                    move = stock_warehouse_obj.search([('id', '=', stock_transit_order_id.supply_warehouse_id.id)])
                    move2 = stock_warehouse_obj.search([('id', '=', stock_transit_order_id.warehouse_id.id)])
                    string = '' + str(move.name) + ' --> ' + str(move2.name)
                    sheet.write(rowx, 1, _('Warehouse'), content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 2, string, content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 3, _('Date'), content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 4, stock_picking.date_done, content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 5, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 6, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 7, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 8, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 9, '', content_line_formats['format_content_left_color'])
                    rowx += 1
                    for line in stock_picking.move_lines:
                        product = product_product_obj.search([('id', '=', line.product_id.id)])
                        sequence += 1
                        transit_order_line = stock_transit_order_line_obj.search([('transit_order_id', '=', stock_transit_order_id.id), ('product_id', '=', line.product_id.id)], limit=1)
                        uom = product_uom_obj.search([('id', '=', transit_order_line.product_uom.id)], limit=1)
                        total = line.product_uom_qty * line.price_unit
                        taxed = line.product_uom_qty * line.price_unit
                        sheet.write(rowx, 0, sequence, content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 1, product['default_code'], content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 2, product['name'], content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 3, line['product_uom_qty'], content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 4, transit_order_line['price_unit'], content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 5, uom.name, content_line_formats['format_content_float_border'])
                        sheet.write(rowx, 6, taxed, content_line_formats['format_content_float_border'])
                        sheet.write(rowx, 7, '0', content_line_formats['format_content_float_border'])
                        sheet.write(rowx, 8, total, content_line_formats['format_content_float_border'])
                        sheet.write(rowx, 9, '', content_line_formats['format_content_float_border'])
                        rowx += 1
            if move_type_expense and self.expense:
                product_expense = product_expense_obj.search([('id', '=', stock_picking.expense.id)])
                if product_expense.warehouse in self.warehouse_ids:
                    sheet.write(rowx, 1, _('Expense'), content_line_formats['format_content_left_color_bold'])
                    sheet.write(rowx, 2, stock_picking.origin, content_line_formats['format_content_left_color_bold'])
                    sheet.write(rowx, 3, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 4, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 5, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 6, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 7, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 8, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 9, '', content_line_formats['format_content_left_color'])
                    rowx += 1
                    hr_employee = self.env['hr.employee'].search([('id', '=', stock_picking.user_id.id)], limit=1)
                    sheet.write(rowx, 1, _('Employee'), content_line_formats['format_content_left_color'])
                    if hr_employee:
                        sheet.write(rowx, 2, hr_employee['name'], content_line_formats['format_content_left_color'])
                    else:
                        sheet.write(rowx, 2, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 3, _('Date'), content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 4, stock_picking.date_done, content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 5, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 6, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 7, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 8, '', content_line_formats['format_content_left_color'])
                    sheet.write(rowx, 9, '', content_line_formats['format_content_left_color'])
                    rowx += 1
                    for line in stock_picking.move_lines:
                        product = product_product_obj.search([('id', '=', line.product_id.id)])
                        sequence += 1
                        total = line.product_uom_qty * line.price_unit
                        taxed = line.product_uom_qty * line.price_unit
                        sheet.write(rowx, 0, sequence, content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 1, product['default_code'], content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 2, product['name'], content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 3, line['product_uom_qty'], content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 4, line['price_unit'], content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 5, '', content_line_formats['format_content_float_border'])
                        sheet.write(rowx, 6, taxed, content_line_formats['format_content_float_border'])
                        sheet.write(rowx, 7, '0', content_line_formats['format_content_float_border'])
                        sheet.write(rowx, 8, total, content_line_formats['format_content_float_border'])
                        sheet.write(rowx, 9, '', content_line_formats['format_content_float_border'])
                        rowx += 1
            if move_type_purchase and self.purchase:
                if stock_picking.move_lines:
                    stock_warehouse = stock_warehouse_obj.search(
                        [('lot_stock_id', '=', stock_picking.move_lines[0].location_dest_id.id)])
                    if stock_warehouse in self.warehouse_ids:
                        sheet.write(rowx, 1, _('Purchase'), content_line_formats['format_content_left_color_bold'])
                        sheet.write(rowx, 2, stock_picking.origin, content_line_formats['format_content_left_color_bold'])
                        sheet.write(rowx, 3, '', content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 4, '', content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 5, '', content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 6, '', content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 7, '', content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 8, '', content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 9, '', content_line_formats['format_content_left_color'])
                        rowx += 1
                        sheet.write(rowx, 1, _('Partner'), content_line_formats['format_content_left_color'])
                        res_partner = None
                        if stock_picking.origin:
                            purchase = self.env['purchase.order'].search([('name', '=', stock_picking.origin)])
                            if purchase:
                                res_partner = purchase.partner_id
                        if res_partner:
                            sheet.write(rowx, 2, res_partner.name, content_line_formats['format_content_left_color'])
                        else:
                            sheet.write(rowx, 2, '', content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 3, _('Date'), content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 4, stock_picking.date_done, content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 5, '', content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 6, '', content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 7, '', content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 8, '', content_line_formats['format_content_left_color'])
                        sheet.write(rowx, 9, '', content_line_formats['format_content_left_color'])
                        rowx += 1
                        for line in stock_picking.move_lines:
                            product = product_product_obj.search([('id', '=', line.product_id.id)])
                            sequence += 1
                            purchase_order_line = purchase_order_line_obj.search([('id', '=', line.purchase_line_id.id)])
                            pt = product_template_obj.search([('id', '=', product.product_tmpl_id.id)])
                            uom = product_uom_obj.search([('id', '=', pt.uom_id.id)])
                            qty = line.product_qty / uom.factor * uom.factor if line.product_qty > 0 and uom.factor > 0 else 0
                            total = purchase_order_line['price_unit'] * line.product_qty / uom.factor * uom.factor if \
                            purchase_order_line['price_unit'] > 0 and line.product_qty > 0 and uom.factor > 0 else 0
                            taxed = purchase_order_line['cost_unit'] * line.product_qty / uom.factor * uom.factor if \
                            purchase_order_line['cost_unit'] > 0 and line.product_qty > 0 and uom.factor > 0 else 0
                            if purchase_order_line.discount:
                                total = total - total * purchase_order_line.discount / 100
                            tax = total - taxed
                            sheet.write(rowx, 0, sequence, content_line_formats['format_content_left_color'])
                            sheet.write(rowx, 1, product['default_code'], content_line_formats['format_content_left_color'])
                            sheet.write(rowx, 2, product['name'], content_line_formats['format_content_left_color'])
                            sheet.write(rowx, 3, qty, content_line_formats['format_content_left_color'])
                            sheet.write(rowx, 4, purchase_order_line['price_unit'],
                                        content_line_formats['format_content_left_color'])
                            sheet.write(rowx, 5, uom.name, content_line_formats['format_content_float_border'])
                            sheet.write(rowx, 6, taxed, content_line_formats['format_content_float_border'])
                            sheet.write(rowx, 7, tax, content_line_formats['format_content_float_border'])
                            sheet.write(rowx, 8, total, content_line_formats['format_content_float_border'])
                            sheet.write(rowx, 9, '', content_line_formats['format_content_float_border'])
                            rowx += 1
        rowx += 2

        sheet = self.get_footer(sheet, header_formats, rowx)
        book.close()
        # Файлын өгөгдлийг тохируулах
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # Экспортын функц дуудаж байна
        return report_excel_output_obj.export_report()
