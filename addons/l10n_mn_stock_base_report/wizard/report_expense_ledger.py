# -*- encoding: utf-8 -*-
##############################################################################
from io import BytesIO
import base64
import time
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
import pytz
from datetime import datetime

from odoo import models, fields, api, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from odoo.addons.l10n_mn_report.tools.tools import str_tuple
from odoo.addons.l10n_mn_web.models.time_helper import *


class ReportExpenseLedger(models.TransientModel):
    """
           БАРАА МАТЕРИАЛЫН ЗАРЛАГЫН ТОВЧОО ТАЙЛАН
    """
    _inherit = 'oderp.report.html.output'
    _name = 'report.expense.ledger'
    _description = 'Report Expense Ledger'
    company_id = fields.Many2one('res.company', 'Company', readonly=True, default=lambda obj: obj.env['res.company']._company_default_get('stock.inventory.report'))
    warehouse_ids = fields.Many2many('stock.warehouse', 'report_expense_ledger_warehouse_rel', 'wizard_id', 'warehouse_id', 'Warehouse')
    products = fields.Many2many('product.product', 'report_expense_ledger_product_rel', 'wizard_id', 'product_id', 'Product')
    partners = fields.Many2many('res.partner', 'report_expense_ledger_partner_rel', 'wizard_id', 'partner_id', 'Partner')
    categories = fields.Many2many('product.category', 'report_expense_ledger_category_rel', 'wizard_id', 'category_id', 'Category')
    report_type = fields.Selection([('detail', 'Detail'), ('summary', 'Summary')], 'Type',
                                   required=True, default='summary')

    @api.multi
    def default_date(self, date):
        for line in self:
            if line.company_id:
                if date == 'start_date':
                    return line.company_id.start_date
                elif date == 'end_date':
                    return line.company_id.end_date
                return False

    start_date = fields.Date("Start Date", required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    end_date = fields.Date("End Date", required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    sales = fields.Boolean('Sales', defualt=False)
    expense = fields.Boolean('Expense', defualt=False)
    refund = fields.Boolean('Stock refund', defualt=False)
    inventory = fields.Boolean('Inventory', defualt=False)
    transit = fields.Boolean('Replenishment', defualt=False)
    pos = fields.Boolean('Pos Sale', defualt=False)
    mrp = fields.Boolean('MRP Production', defualt=False)
    cost = fields.Boolean('Show Cost Amount?', defualt=False)

    @api.onchange('start_date')
    def _get_warehouse_and_categ_domain(self):
        domain = {}
        domain['warehouse_ids'] = [('company_id', '=', self.company_id.id)]
        _warehouses = []
        for warehouse in self.env.user.allowed_warehouses:
            _warehouses.append(warehouse.id)
        if _warehouses:
            domain['warehouse_ids'] = [('id', 'in', _warehouses)]
        return {'domain': domain}

    @api.onchange('warehouse_ids')
    @api.model
    def _compute_module_state(self):
        '''
                    модуль суусан эсэхийг шалгана.
        '''
        if 'purchase.order' in self.env and 'discount' in self.env['purchase.order.line']:
            self.purchase_installed = True
        if 'product.expense' in self.env:
            self.expense_install = True
        if 'pos.order' in self.env:
            self.pos_install = True
        if 'mrp.production' in self.env:
            self.mrp_install = True
        if 'sale.order' in self.env and 'invoice_status' in self.env['sale.order.line']:
            self.sales_install = True
        if 'stock.inventory' in self.env:
            self.inventory_install = True
        if 'stock.transit.order' in self.env:
            self.transit_install = True

    expense_install = fields.Boolean('Expense', compute=_compute_module_state, defualt=False)
    pos_install = fields.Boolean('Pos Install', compute=_compute_module_state, default=False)
    mrp_install = fields.Boolean('MRP Install', compute=_compute_module_state, default=False)
    purchase_installed = fields.Boolean('Purchase install', compute=_compute_module_state, default=False)
    sales_install = fields.Boolean('Sales install', compute=_compute_module_state, default=False)
    inventory_install = fields.Boolean('Inventory install', compute=_compute_module_state, default=False)
    transit_install = fields.Boolean('Transit install', compute=_compute_module_state, default=False)

    @api.model
    def _get_inventory_lines(self):
        where_qry = ''
        if self.partners:
            partner_ids = self.partners.ids
            where_qry += ' AND rp1.id in (' + ','.join(map(str, partner_ids)) + ') '
        if self.products:
            product_ids = self.products.ids
            where_qry += ' AND pp2.id in (' + ','.join(map(str, product_ids)) + ') '
        if self.categories:
            category_ids = self.categories.ids
            where_qry += ' AND pro_cat1.id in (' + ','.join(map(str, category_ids)) + ') '
        if self.warehouse_ids:
            location_ids = self.warehouse_ids.mapped('lot_stock_id').ids
            where_qry += ' AND sl1.id in (' + ','.join(map(str, location_ids)) + ') '

        group = ''

        if self.report_type == 'detail':
            select = '''
                sm.date,
                si.name,
                si.name as origin,
                '' as partner,
                sm.product_uom_qty as qty,
                COALESCE(sm.product_uom_qty * sm.price_unit, 0) as total,
                0 as tax,
                COALESCE(sm.product_uom_qty * sm.price_unit, 0) as taxed,
                COALESCE(sm.product_uom_qty * sm.price_unit, 0) as cost'''
        else:
            select = '''
                si.date,
                si.name,
                si.name as origin,
                '' as partner,
                sum(sm.product_uom_qty) as qty,
                sum(COALESCE(sm.product_uom_qty * sm.price_unit, 0)) as total,
                0 as tax,
                sum(COALESCE(sm.product_uom_qty * sm.price_unit, 0)) as taxed,
                sum(COALESCE(sm.product_uom_qty * sm.price_unit, 0)) as cost'''
            group = ' GROUP BY si.id'

        query = '''
            SELECT
                %s
            FROM
                stock_inventory si
                RIGHT JOIN stock_move sm on sm.inventory_id = si.id
                LEFT JOIN stock_location sl1 on sl1.id = sm.location_id
                LEFT JOIN stock_location sl2 on sl2.id = sm.location_dest_id
                LEFT JOIN product_product pp2 ON sm.product_id = pp2.id
                LEFT JOIN product_template pt2 ON pt2.id = pp2.product_tmpl_id
                LEFT JOIN product_category pro_cat1 ON pt2.categ_id = pro_cat1.id
                LEFT JOIN res_partner rp1 on rp1.id = sm.partner_id
            WHERE
                si.state in ('done', 'counted') AND
                sm.state in ('done') AND
                sl1.usage = 'internal' AND
                sl2.usage = 'inventory' AND
                sm.date BETWEEN '%s' AND '%s'
                %s %s ORDER BY si.date
        ''' % (select, self.start_date, self.end_date, where_qry, group)
        self._cr.execute(query)
        return self._cr.dictfetchall()

    @api.model
    def _get_transit_lines(self):
        where_qry = ''
        if self.partners:
            partner_ids = self.partners.ids
            where_qry += ' AND rp.id in (' + ','.join(map(str, partner_ids)) + ') '
        if self.products:
            product_ids = self.products.ids
            where_qry += ' AND pp1.id in (' + ','.join(map(str, product_ids)) + ') '
        if self.categories:
            category_ids = self.categories.ids
            where_qry += ' AND pro_cat.id in (' + ','.join(map(str, category_ids)) + ') '
        if self.warehouse_ids:
            warehouse_ids = self.warehouse_ids.ids
            where_qry += ' AND sto.supply_warehouse_id in (' + ','.join(map(str, warehouse_ids)) + ') '

        group = ''
        if self.report_type == 'detail':
            select = '''
                sm.date,
                sp.name,
                sto.name as origin,
                rp.name as partner,
                CONCAT(sw1.name, ' --> ', sw2.name) as move,
                sm.product_uom_qty as qty,
                COALESCE(sm.product_uom_qty * sm.price_unit, 0) as total,
                0 as tax,
                COALESCE(sm.product_uom_qty * sm.price_unit, 0) as taxed,
                COALESCE(sm.product_uom_qty * sm.price_unit, 0) as cost'''
        else:
            select = '''
                sp.date_done,
                sp.name,
                sto.name as origin,
                rp.name as partner,
                CONCAT(sw1.name, ' --> ', sw2.name) as move,
                sum(sm.product_uom_qty) as qty,
                sum(COALESCE(sm.product_uom_qty * sm.price_unit, 0)) as total,
                0 as tax,
                sum(COALESCE(sm.product_uom_qty * sm.price_unit, 0)) as taxed,
                sum(COALESCE(sm.product_uom_qty * sm.price_unit, 0)) as cost'''
            group = ' GROUP BY sp.id, sto.id, rp.id, sw1.id, sw2.id '
        query = '''
            SELECT
                %s
            FROM
                stock_transit_order sto
                RIGHT JOIN stock_picking sp on sp.transit_order_id = sto.id
                RIGHT JOIN stock_move sm on sm.picking_id = sp.id
                LEFT JOIN product_product pp1 ON sm.product_id = pp1.id
                LEFT JOIN product_template pt ON pt.id = pp1.product_tmpl_id
                LEFT JOIN product_category pro_cat ON pt.categ_id = pro_cat.id
                LEFT JOIN res_partner rp on rp.id = sp.partner_id
                LEFT JOIN stock_location sl1 on sl1.id = sm.location_id
                LEFT JOIN stock_location sl2 on sl2.id = sm.location_dest_id
                LEFT JOIN stock_warehouse sw1 on sw1.id = sto.supply_warehouse_id
	            LEFT JOIN stock_warehouse sw2 on sw2.id = sto.warehouse_id
            WHERE
                sto.state in ('confirmed', 'done') AND
                sm.state in ('done') AND
                sl1.usage = 'internal' AND
                sl2.usage = 'transit' AND
                sm.date BETWEEN '%s' AND '%s'
                %s %s order by sp.date_done
        ''' % (select, self.start_date, self.end_date, where_qry, group)
        self._cr.execute(query)
        return self._cr.dictfetchall()

    @api.model
    def _get_sale_lines(self):
        where_qry = ''
        if self.partners:
            partner_ids = self.partners.ids
            where_qry += ' AND rp.id in (' + ','.join(map(str, partner_ids)) + ') '
        if self.products:
            product_ids = self.products.ids
            where_qry += ' AND pp.id in (' + ','.join(map(str, product_ids)) + ') '
        if self.categories:
            category_ids = self.categories.ids
            where_qry += ' AND pro_cat.id in (' + ','.join(map(str, category_ids)) + ') '
        if self.warehouse_ids:
            warehouse_ids = self.warehouse_ids.ids
            where_qry += ' AND so.warehouse_id in (' + ','.join(map(str, warehouse_ids)) + ') '

        group = ''
        if self.report_type == 'detail':
            select = '''
                sm.date,
                sp.name,
                so.name as origin,
                rp.name as partner,
                sum(sm.product_uom_qty) as qty,
                sum(COALESCE(sol.price_unit * sm.product_uom_qty, 0)) as total,
                sum(COALESCE(sol.price_unit * sm.product_uom_qty * sol.discount / 100, 0)) as discount,
                CASE 
                    WHEN sum(COALESCE(sol.price_unit * sm.product_uom_qty, 0) * (100 - sol.discount) / 100) is NULL THEN 0 
                    ELSE sum(COALESCE(sol.price_unit * sm.product_uom_qty, 0) * (100 - sol.discount) / 100) END as pure,
                CASE WHEN sum(sol.price_tax) > 0 AND sum(sm.product_uom_qty) > 0 AND sum(sol.product_uom_qty) > 0 THEN
                    sum(CASE WHEN sol.product_uom_qty >0 THEN sol.price_tax/sol.product_uom_qty*sm.product_uom_qty ELSE 0 END) ELSE 0 END as tax,
                CASE WHEN sum(sol.price_subtotal) > 0 AND sum(sm.product_uom_qty) > 0 AND sum(sol.product_uom_qty) > 0 THEN
                    sum(CASE WHEN sol.product_uom_qty > 0 THEN sol.price_subtotal/sol.product_uom_qty*sm.product_uom_qty ELSE 0 END) ELSE 0 END as taxed,
                CASE WHEN sm.price_unit * sm.product_uom_qty is NULL THEN 0 
                ELSE sm.price_unit * sm.product_uom_qty END as cost'''
            group = ' GROUP BY sp.id, so.id, rp.id, sm.id'
        else:
            select = '''
                sp.date_done,
                sp.name,
                so.name as origin,
                rp.name as partner,
                sum(sm.product_uom_qty) as qty,
                sum(COALESCE(sol.price_unit * sm.product_uom_qty, 0)) as total,
                sum(COALESCE(sol.price_unit * sm.product_uom_qty * sol.discount / 100, 0)) as discount,
                CASE 
                    WHEN sum(COALESCE(sol.price_unit * sm.product_uom_qty, 0) * (100 - sol.discount) / 100) is NULL THEN 0 
                    ELSE sum(COALESCE(sol.price_unit * sm.product_uom_qty, 0) * (100 - sol.discount) / 100) END as pure,
                CASE WHEN sum(sol.price_tax) > 0 AND sum(sm.product_uom_qty) > 0 AND sum(sol.product_uom_qty) > 0 THEN
                    sum(CASE WHEN sol.product_uom_qty >0 THEN sol.price_tax/sol.product_uom_qty*sm.product_uom_qty ELSE 0 END)  ELSE 0 END as tax,
                CASE WHEN sum(sol.price_subtotal) > 0 AND sum(sm.product_uom_qty) > 0 AND sum(sol.product_uom_qty) > 0 THEN
                    sum(CASE WHEN sol.product_uom_qty >0 THEN sol.price_subtotal/sol.product_uom_qty*sm.product_uom_qty ELSE 0 END) ELSE 0 END as taxed,
                CASE WHEN sum(sm.price_unit * sm.product_uom_qty) is NULL THEN 0 
                ELSE sum(sm.price_unit * sm.product_uom_qty) END as cost'''
            group = ' GROUP BY sp.id, so.id, rp.id'

        query = '''
            SELECT
                %s
            FROM
                sale_order so
                right join stock_picking sp on sp.group_id = so.procurement_group_id
                right join stock_move sm on sp.id = sm.picking_id
                left join res_partner rp on rp.id = so.partner_id
                LEFT JOIN product_product pp ON sm.product_id = pp.id
                LEFT JOIN product_template pt ON pt.id = pp.product_tmpl_id
                LEFT JOIN product_category pro_cat ON pt.categ_id = pro_cat.id
                left join sale_order_line sol on sol.order_id = so.id and sm.product_id = sol.product_id
                LEFT JOIN account_tax_sale_order_line_rel rel on rel.sale_order_line_id = sol.id
                LEFT JOIN account_tax at on rel.account_tax_id = at.id
                left join stock_location sl1 on sl1.id = sm.location_id
                left join stock_location sl2 on sl2.id = sm.location_dest_id
            WHERE
                so.procurement_group_id is not null AND
                sm.state in ('done') AND
                sl1.usage = 'internal' AND
                sl2.usage = 'customer' AND
                sm.date BETWEEN '%s' AND '%s'
                %s %s ORDER BY sp.date_done
        ''' % (select, self.start_date, self.end_date, where_qry, group)
        self._cr.execute(query)
        return self._cr.dictfetchall()

    @api.model
    def _get_return_lines(self):
        where_qry = ''
        if self.partners:
            partner_ids = self.partners.ids
            where_qry += ' AND rp.id in (' + ','.join(map(str, partner_ids)) + ') '
        if self.products:
            product_ids = self.products.ids
            where_qry += ' AND pp.id in (' + ','.join(map(str, product_ids)) + ') '
        if self.warehouse_ids:
            location_ids = self.warehouse_ids.mapped('lot_stock_id').ids
            where_qry += ' AND sm1.location_id in (' + ','.join(map(str, location_ids)) + ') '

        group = ''
        if self.report_type == 'detail':
            select = '''
                sm.date,
                pp.id AS product_id,
                sp.name,
                sp.origin as origin,
                rp.name as partner,
                sm.product_uom_qty as qty,
                COALESCE(pol.price_unit * sm.product_uom_qty, 0) as total,
                COALESCE(pol.price_unit * sm.product_uom_qty * pol.discount / 100, 0) as discount,
                COALESCE(pol.price_unit * sm.product_uom_qty - pol.discount * pol.price_unit * sm.product_uom_qty / 100, 0) as pure,
                CASE
                    WHEN at.price_include = 'f' and at.amount_type = 'percent' then (pol.price_unit * sm.product_uom_qty - pol.discount * pol.price_unit * sm.product_uom_qty /100) * at.amount/100
                    WHEN at.price_include = 't' and at.amount_type = 'percent' then pol.price_unit * sm.product_uom_qty - pol.discount * pol.price_unit * sm.product_uom_qty / 100 - pol.price_unit * sm.product_uom_qty - pol.discount * pol.price_unit * sm.product_uom_qty*100/(100 + at.amount)
                    ELSE 0
                END as tax,
                COALESCE(pol.price_unit * sm.product_uom_qty - pol.discount * pol.price_unit * sm.product_uom_qty / 100, 0) - 
                CASE
                    WHEN at.price_include = 'f' and at.amount_type = 'percent' then (pol.price_unit * sm.product_uom_qty - pol.discount * pol.price_unit * sm.product_uom_qty /100) * at.amount/100
                    WHEN at.price_include = 't' and at.amount_type = 'percent' then pol.price_unit * sm.product_uom_qty - pol.discount * pol.price_unit * sm.product_uom_qty / 100 - pol.price_unit * sm.product_uom_qty - pol.discount * pol.price_unit * sm.product_uom_qty*100/(100 + at.amount)
                    ELSE 0
                END as taxed,
                sm.price_unit * sm.product_uom_qty as cost'''
        else:
            select = '''
                sp.date_done,
                sp.name,
                sp.origin as origin,
                rp.name as partner,
                sum(sm.product_uom_qty) as qty,
                sum(COALESCE(pol.price_unit * sm.product_uom_qty, 0)) as total,
                sum(COALESCE(pol.price_unit * sm.product_uom_qty * pol.discount / 100, 0)) as discount,
                sum(COALESCE(pol.price_unit * sm.product_uom_qty - pol.discount * pol.price_unit * sm.product_uom_qty / 100, 0)) as pure,
                CASE
                    WHEN at.price_include = 'f' and at.amount_type = 'percent' then sum((pol.price_unit * sm.product_uom_qty - pol.discount * pol.price_unit * sm.product_uom_qty /100) * at.amount/100)
                    WHEN at.price_include = 't' and at.amount_type = 'percent' then sum(pol.price_unit * sm.product_uom_qty - pol.discount * pol.price_unit * sm.product_uom_qty / 100 - pol.price_unit * sm.product_uom_qty - pol.discount * pol.price_unit * sm.product_uom_qty*100/(100 + at.amount))
                    ELSE 0
                END as tax,
                sum(COALESCE(pol.price_unit * sm.product_uom_qty - pol.discount * pol.price_unit * sm.product_uom_qty / 100, 0)) - 
                CASE
                    WHEN at.price_include = 'f' and at.amount_type = 'percent' then sum((pol.price_unit * sm.product_uom_qty - pol.discount * pol.price_unit * sm.product_uom_qty /100) * at.amount/100)
                    WHEN at.price_include = 't' and at.amount_type = 'percent' then sum(pol.price_unit * sm.product_uom_qty - pol.discount * pol.price_unit * sm.product_uom_qty / 100 - pol.price_unit * sm.product_uom_qty - pol.discount * pol.price_unit * sm.product_uom_qty*100/(100 + at.amount))
                    ELSE 0
                END as taxed,
               sum(sm.price_unit * sm.product_uom_qty) as cost'''
            group = ' GROUP BY sp.id, rp.id, at.id'
        query = '''
            SELECT
                %s
            FROM
                stock_move sm
                LEFT JOIN product_product pp ON sm.product_id = pp.id
                LEFT JOIN stock_picking sp on sp.id = sm.picking_id
                LEFT JOIN res_partner rp on rp.id = sp.partner_id
                LEFT JOIN stock_move sm1 on sm1.id = sm.origin_returned_move_id
                LEFT JOIN purchase_order_line pol on pol.id = sm1.purchase_line_id
                LEFT JOIN account_tax_purchase_order_line_rel rel on rel.purchase_order_line_id = pol.id
                LEFT JOIN account_tax at on rel.account_tax_id = at.id
            WHERE 
                sm.origin_returned_move_id in
                (SELECT
                    sm.id
                FROM
                purchase_order po
                RIGHT JOIN purchase_order_line pol on pol.order_id = po.id
                LEFT JOIN stock_move sm on sm.purchase_line_id = pol.id) AND
                sm.origin_returned_move_id is NOT NULL AND
                sm.date BETWEEN '%s' AND '%s'
                %s %s ORDER BY sp.date_done
        ''' % (select, self.start_date, self.end_date, where_qry, group)
        self._cr.execute(query)
        return self._cr.dictfetchall()

    @api.model
    def _get_expense_lines(self, date_from, date_to):
        where_qry = ''
        if self.partners:
            partner_ids = self.partners.ids
            where_qry += ' AND rp.id in (' + ','.join(map(str, partner_ids)) + ') '
        if self.products:
            product_ids = self.products.ids
            where_qry += ' AND pp1.id in (' + ','.join(map(str, product_ids)) + ') '
        if self.categories:
            category_ids = self.categories.ids
            where_qry += ' AND pro_cat.id in (' + ','.join(map(str, category_ids)) + ') '
        if self.warehouse_ids:
            warehouse_ids = self.warehouse_ids.ids
            where_qry += ' AND pe.warehouse in (' + ','.join(map(str, warehouse_ids)) + ') '
        group = ''
        if self.report_type == 'detail':
            select = '''
                sm.date,
                sp.name,
                pe.code as origin,
                sm.product_uom_qty as qty,
                COALESCE(sm.product_uom_qty * sm.price_unit, 0) as total,
                0 as tax,
                COALESCE(sm.product_uom_qty * sm.price_unit, 0) as taxed,
                    COALESCE(sm.product_uom_qty * sm.price_unit, 0) as cost'''
        else:
            select = '''
                sp.date_done,
                sp.name,
                pe.code as origin,
                sum(sm.product_uom_qty) as qty,
                sum(COALESCE(sm.product_uom_qty * sm.price_unit, 0)) as total,
                0 as tax,
                sum(COALESCE(sm.product_uom_qty * sm.price_unit, 0)) as taxed,
                sum(COALESCE(sm.product_uom_qty * sm.price_unit, 0)) as cost'''
            group = 'GROUP BY sp.id, pe.id '
        query = '''
            SELECT
                %s
            FROM
                stock_picking sp
                LEFT JOIN stock_move sm on sm.picking_id = sp.id
                LEFT JOIN product_expense pe on pe.id = sp.expense
                LEFT JOIN product_product pp1 ON sm.product_id = pp1.id
                LEFT JOIN product_template pt ON pt.id = pp1.product_tmpl_id
                LEFT JOIN product_category pro_cat ON pt.categ_id = pro_cat.id
            WHERE
                sp.expense is not null AND
                sm.state in ('done') AND
                sm.date BETWEEN '%s' AND '%s'
                %s %s ORDER BY sp.date_done
        ''' % (select, date_from, date_to, where_qry, group)
        self._cr.execute(query)
        return self._cr.dictfetchall()

    @api.model
    def _get_pos_lines(self):
        where_qry = ''
        if self.partners:
            partner_ids = self.partners.ids
            where_qry += ' AND rp.id in (' + ','.join(map(str, partner_ids)) + ') '
        if self.products:
            product_ids = self.products.ids
            where_qry += ' AND pp1.id in (' + ','.join(map(str, product_ids)) + ') '
        if self.categories:
            category_ids = self.categories.ids
            where_qry += ' AND pro_cat.id in (' + ','.join(map(str, category_ids)) + ') '
        if self.warehouse_ids:
            location_ids = self.warehouse_ids.mapped('lot_stock_id').ids
            where_qry += ' AND ps.stock_location_id in (' + ','.join(map(str, location_ids)) + ') '
        group = ''
        if self.report_type == 'detail':
            select = '''
                sm.date,
                sp.name,
                po.name as origin,
                sm.product_uom_qty as qty,
                COALESCE(pol.qty * pol.price_unit, 0) as total,
                COALESCE(pol.price_unit * pol.qty * pol.discount / 100, 0) as discount,
                COALESCE(pol.price_unit * pol.qty - pol.discount * pol.price_unit * pol.qty / 100, 0) as pure,
                CASE
                        WHEN at.price_include = 'f' and at.amount_type = 'percent' then (pol.price_unit * pol.qty - pol.discount * pol.price_unit * pol.qty / 100) * at.amount / 100
                        WHEN at.price_include = 't' and at.amount_type = 'percent' then (pol.price_unit * pol.qty - pol.discount * pol.price_unit * pol.qty / 100) * (1 - 100 / (100 + at.amount))
                        ELSE 0
                    END as tax,
                CASE
                        WHEN at.price_include = 'f' and at.amount_type = 'percent' then (pol.price_unit * pol.qty - pol.discount * pol.price_unit * pol.qty / 100) * at.amount / 100
                        WHEN at.price_include = 't' and at.amount_type = 'percent' then (pol.price_unit * pol.qty - pol.discount * pol.price_unit * pol.qty / 100) * 100 / (100 + at.amount)
                        ELSE 0
                    END as taxed,
                    COALESCE(sm.product_uom_qty * sm.price_unit, 0) as cost'''
        else:
            select = '''
                sp.date_done,
                sp.name,
                po.name as origin,
                sum(sm.product_uom_qty) as qty,
                sum(COALESCE(pol.qty * pol.price_unit, 0)) as total,
                sum(COALESCE(pol.price_unit * pol.qty * pol.discount / 100, 0)) as discount,
                sum(COALESCE(pol.price_unit * pol.qty - pol.discount * pol.price_unit * pol.qty / 100, 0)) as pure,
                CASE
                        WHEN at.price_include = 'f' and at.amount_type = 'percent' then sum((pol.price_unit * pol.qty - pol.discount * pol.price_unit * pol.qty / 100) * at.amount / 100)
                        WHEN at.price_include = 't' and at.amount_type = 'percent' then sum((pol.price_unit * pol.qty - pol.discount * pol.price_unit * pol.qty / 100) * (1 - 100 / (100 + at.amount)))
                        ELSE 0
                    END as tax,
                CASE
                        WHEN at.price_include = 'f' and at.amount_type = 'percent' then sum((pol.price_unit * pol.qty - pol.discount * pol.price_unit * pol.qty / 100) * at.amount / 100)
                        WHEN at.price_include = 't' and at.amount_type = 'percent' then sum((pol.price_unit * pol.qty - pol.discount * pol.price_unit * pol.qty / 100) * 100 / (100 + at.amount))
                        ELSE 0
                    END as taxed,
                    sum(COALESCE(sm.product_uom_qty * sm.price_unit, 0)) as cost'''
            group = ' GROUP BY sp.id, po.id, at.id '

        query = '''
            SELECT
                %s
            FROM
                pos_order po
                LEFT JOIN stock_picking sp on po.picking_id = sp.id
                LEFT JOIN stock_move sm on sm.picking_id = sp.id
                LEFT JOIN product_product pp1 ON sm.product_id = pp1.id
                LEFT JOIN product_template pt ON pt.id = pp1.product_tmpl_id
                LEFT JOIN product_category pro_cat ON pt.categ_id = pro_cat.id
                LEFT JOIN res_partner rp on rp.id = po.partner_id
                LEFT JOIN pos_order_line pol on pol.order_id = po.id and sm.order_line_id = pol.id
                    LEFT JOIN account_tax_pos_order_line_rel rel on rel.pos_order_line_id = pol.id
                    LEFT JOIN account_tax at on rel.account_tax_id = at.id
                LEFT JOIN pos_session ps on ps.id = po.session_id
            WHERE
                sm.state in ('done') AND
                sm.date BETWEEN '%s' AND '%s'
                %s %s ORDER BY sp.date_done
        ''' % (select, self.start_date, self.end_date, where_qry, group)
        self._cr.execute(query)
        return self._cr.dictfetchall()

    @api.model
    def _get_mrp_lines(self):
        where_qry = ''
        if self.partners:
            partner_ids = self.partners.ids
            where_qry += ' AND rp.id in (' + ','.join(map(str, partner_ids)) + ') '
        if self.products:
            product_ids = self.products.ids
            where_qry += ' AND pp1.id in (' + ','.join(map(str, product_ids)) + ') '
        if self.categories:
            category_ids = self.categories.ids
            where_qry += ' AND pro_cat.id in (' + ','.join(map(str, category_ids)) + ') '
        if self.warehouse_ids:
            location_ids = self.warehouse_ids.mapped('lot_stock_id').ids
            where_qry += ' AND mp.location_src_id in (' + ','.join(map(str, location_ids)) + ') '
        group = ''
        if self.report_type == 'detail':
            select = '''                
                sm.date,
                mp.name,
                mp.origin,
                sm.product_uom_qty as qty,
                COALESCE(sm.product_uom_qty * sm.price_unit, 0) as total,
                    0 as tax,
                    COALESCE(sm.product_uom_qty * sm.price_unit, 0) as taxed,
                    COALESCE(sm.product_uom_qty * sm.price_unit, 0) as cost'''
        else:
            select = '''                
                mp.date_planned_start,
                mp.name,
                mp.origin,
                sum(sm.product_uom_qty) as qty,
                sum(COALESCE(sm.product_uom_qty * sm.price_unit, 0)) as total,
                0 as tax,
                sum(COALESCE(sm.product_uom_qty * sm.price_unit, 0)) as taxed,
                sum(COALESCE(sm.product_uom_qty * sm.price_unit, 0)) as cost'''
            group = ' GROUP BY mp.id'
        query = '''
            SELECT
                %s
            FROM
                mrp_production mp
                LEFT JOIN stock_move sm on mp.id = sm.raw_material_production_id
                LEFT JOIN product_product pp1 ON sm.product_id = pp1.id
                LEFT JOIN product_template pt ON pt.id = pp1.product_tmpl_id
                LEFT JOIN product_category pro_cat ON pt.categ_id = pro_cat.id
                LEFT JOIN res_partner rp on rp.id = sm.partner_id
            WHERE
                sm.state = 'done' AND
                sm.date BETWEEN '%s' AND '%s'
                %s %s order by mp.date_planned_start
        ''' % (select, self.start_date, self.end_date, where_qry, group)
        self._cr.execute(query)
        return self._cr.dictfetchall()

    def get_sheet_format(self, sheet):
        # Тайлангийн хуудас
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.39, 0.39, 0.39, 0.39)  # 1cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Arial"&9&P', {'margin': 0.1})
        sheet.hide_gridlines(2)
        sheet.set_landscape()
        return sheet

    @api.multi
    def get_header(self, sheet, header_formats, rowx, report_name):
        StockWarehouse = self.env['stock.warehouse']
        warehouse_ids = StockWarehouse.search([]).ids if not self.warehouse_ids else self.warehouse_ids.ids
        warehouse_names = ''
        for warehouse_id in warehouse_ids:
            warehouse_names += (str(StockWarehouse.browse(warehouse_id).name) + ', ')
        sheet.write(rowx, 0, _('Company name: %s') % self.company_id.name, header_formats['format_filter'])
        rowx += 1
        sheet.write(rowx, 0, _('Duration: %s - %s') % (self.start_date, self.end_date), header_formats['format_filter'])
        rowx += 1
        sheet.write(rowx, 0, _('Warehouse: %s') % warehouse_names[:-2], header_formats['format_filter'])
        rowx += 1
        sheet.write(rowx, 0, _('Date: %s') % time.strftime('%Y-%m-%d'), header_formats['format_filter'])
        rowx += 1
        sheet.merge_range(rowx, 2, rowx, 9, report_name.upper(), header_formats['format_name'])
        rowx += 3
        return sheet, rowx

    @api.multi
    def get_value(self, sheet, content_line_formats, rowx, format_content_header):
        sheet.merge_range(rowx, 0, rowx + 1, 0, '№', format_content_header)
        sheet.merge_range(rowx, 1, rowx + 1, 1, _('Date'), format_content_header)
        sheet.merge_range(rowx, 2, rowx + 1, 2, _('Receipt number'), format_content_header)
        sheet.merge_range(rowx, 3, rowx + 1, 3, _('Origin'), format_content_header)
        sheet.merge_range(rowx, 4, rowx + 1, 4, _('Partner name'), format_content_header)
        sheet.merge_range(rowx, 5, rowx + 1, 5, _('Stock move'), format_content_header)
        sheet.merge_range(rowx, 6, rowx + 1, 6, _('Quantity'), format_content_header)
        sheet.merge_range(rowx, 7, rowx + 1, 7, _('Total'), format_content_header)
        sheet.merge_range(rowx, 8, rowx + 1, 8, _('Discount'), format_content_header)
        sheet.merge_range(rowx, 9, rowx + 1, 9, _('Pure'), format_content_header)
        sheet.merge_range(rowx, 10, rowx + 1, 10, _('NUAT'), format_content_header)
        sheet.merge_range(rowx, 11, rowx + 1, 11, _('Amount without NUAT'), format_content_header)
        sheet.merge_range(rowx, 12, rowx + 1, 12, _('Cost'), format_content_header)
        rowx += 1
        date_from = str(get_day_by_user_timezone(self.start_date + " 00:00:00", self.env.user))
        date_to = str(get_day_by_user_timezone(self.end_date + " 23:59:59", self.env.user))

        inventory_lines = self._get_inventory_lines() if self.inventory else False
        transit_lines = self._get_transit_lines() if self.transit else False
        sale_lines = self._get_sale_lines() if self.sales else False
        return_lines = self._get_return_lines() if self.refund else False
        expense_lines = self._get_expense_lines(date_from,date_to) if self.expense else False
        pos_lines = self._get_pos_lines() if self.pos else False
        mrp_lines = self._get_mrp_lines() if self.mrp else False
        rowx += 1
        sum_qty = 0 or 0
        sum_total = 0 or 0
        sum_discount = 0 or 0
        sum_pure = 0 or 0
        sum_tax = 0 or 0
        sum_taxed = 0 or 0
        sum_cost = 0 or 0
        total_qty = 0 or 0
        sub_total = 0 or 0
        total_discount = 0 or 0
        total_pure = 0 or 0
        total_tax = 0 or 0
        total_taxed = 0 or 0
        total_cost = 0 or 0
        total_qty += sum_qty or 0
        sub_total += sum_total or 0
        total_discount += sum_discount or 0
        total_pure += sum_pure or 0
        total_tax += sum_tax or 0
        total_taxed += sum_taxed or 0
        total_cost += sum_cost or 0
        total_rowx = rowx
        sheet.write(rowx, 7, '', content_line_formats['format_content_float_border'])
        sheet.write(rowx, 8, '', content_line_formats['format_content_float_border'])
        sheet.write(rowx, 9, '', content_line_formats['format_content_float_border'])
        sheet.write(rowx, 10, '', content_line_formats['format_content_float_border'])
        sheet.write(rowx, 11, '', content_line_formats['format_content_float_border'])
        sheet.write(rowx, 12, '', content_line_formats['format_content_float_border'])
        rowx += 1
        if inventory_lines:
            sum_qty = 0 or 0
            sum_cost = 0 or 0
            sequence = 0
            for line in inventory_lines:
                sum_qty += line.get('qty', 0)
                sum_cost += line.get('cost', 0)
            rowx += 1
            sheet.merge_range(rowx, 0, rowx, 4, 'Inventory', content_line_formats['format_content_left_color'])
            sheet.write(rowx, 5, '', content_line_formats['format_content_left_color'])
            sheet.write(rowx, 6, sum_qty, content_line_formats['format_content_float_border'])
            total_qty += sum_qty or 0
            sheet.write(rowx, 7, 0, content_line_formats['format_content_left_color'])
            sheet.write(rowx, 8, 0, content_line_formats['format_content_left_color'])
            sheet.write(rowx, 9, 0, content_line_formats['format_content_left_color'])
            sheet.write(rowx, 10, 0, content_line_formats['format_content_left_color'])
            sheet.write(rowx, 11, 0, content_line_formats['format_content_left_color'])
            sheet.write(rowx, 12, sum_cost, content_line_formats['format_content_float_border'])
            total_cost += sum_cost or 0
            rowx += 1
            sequence += 1
            for line in inventory_lines:
                sheet.write(rowx, 0, sequence, content_line_formats['format_content_left_color'])
                sheet.write(rowx, 1, line.get('date'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 2, line.get('name'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 3, line.get('origin'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 4, '', content_line_formats['format_content_left_color'])
                sheet.write(rowx, 5, '', content_line_formats['format_content_left_color'])
                sheet.write(rowx, 6, line.get('qty'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 7, '', content_line_formats['format_content_left_color'])
                sheet.write(rowx, 8, '', content_line_formats['format_content_left_color'])
                sheet.write(rowx, 9, '', content_line_formats['format_content_left_color'])
                sheet.write(rowx, 10, '', content_line_formats['format_content_left_color'])
                sheet.write(rowx, 11, '', content_line_formats['format_content_left_color'])
                sheet.write(rowx, 12, line.get('cost'), content_line_formats['format_content_float_border'])
                rowx += 1
                sequence += 1
        if transit_lines:
            sum_qty = 0
            sum_cost = 0 or 0
            sequence = 0
            for line in transit_lines:
                sum_qty += line.get('qty', 0)
                sum_cost += line.get('cost', 0)
            sheet.merge_range(rowx, 0, rowx, 4, 'Stock Transit Order', content_line_formats['format_content_left_color'])
            sheet.write(rowx, 5, '', content_line_formats['format_content_left_color'])
            sheet.write(rowx, 6, sum_qty, content_line_formats['format_content_float_border'])
            total_qty += sum_qty or 0
            sheet.write(rowx, 7, 0, content_line_formats['format_content_left_color'])
            sheet.write(rowx, 8, 0, content_line_formats['format_content_left_color'])
            sheet.write(rowx, 9, 0, content_line_formats['format_content_left_color'])
            sheet.write(rowx, 10, 0, content_line_formats['format_content_left_color'])
            sheet.write(rowx, 11, 0, content_line_formats['format_content_left_color'])
            sheet.write(rowx, 12, sum_cost, content_line_formats['format_content_float_border'])
            total_cost += sum_cost or 0
            rowx += 1
            sequence += 1
            for line in transit_lines:
                sheet.write(rowx, 0, sequence, content_line_formats['format_content_left_color'])
                sheet.write(rowx, 1, line.get('date'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 2, line.get('name'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 3, line.get('origin'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 4, line.get('partner'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 5, line.get('move'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 6, line.get('qty'), content_line_formats['format_content_float_border'])
                sheet.write(rowx, 7, '', content_line_formats['format_content_float_border'])
                sheet.write(rowx, 8, '', content_line_formats['format_content_float_border'])
                sheet.write(rowx, 9, '', content_line_formats['format_content_float_border'])
                sheet.write(rowx, 10, '', content_line_formats['format_content_float_border'])
                sheet.write(rowx, 11, '', content_line_formats['format_content_float_border'])
                sheet.write(rowx, 12, line.get('cost'), content_line_formats['format_content_float_border'])
                rowx += 1
                sequence += 1
        if sale_lines:
            sum_qty = 0 or 0
            sum_total = 0 or 0
            sum_discount = 0 or 0
            sum_pure = 0 or 0
            sum_tax = 0 or 0
            sum_taxed = 0 or 0
            sum_cost = 0 or 0
            sequence = 0
            for line in sale_lines:
                sum_qty += (line.get('qty', 0) or 0)
                sum_total += (line.get('total', 0) or 0)
                sum_discount += (line.get('discount', 0) or 0)
                sum_pure += (line.get('pure', 0) or 0)
                sum_tax += (line.get('tax', 0) or 0)
                sum_taxed += (line.get('taxed', 0) or 0)
                sum_cost += (line.get('cost', 0) or 0)
            sheet.merge_range(rowx, 0, rowx, 4, 'Sales', content_line_formats['format_content_left_color'])
            sheet.write(rowx, 5, '', content_line_formats['format_content_left_color'])
            sheet.write(rowx, 6, sum_qty, content_line_formats['format_content_float_border'])
            total_qty += sum_qty or 0
            sheet.write(rowx, 7, sum_total, content_line_formats['format_content_float_border'])
            sub_total += sum_total or 0
            sheet.write(rowx, 8, sum_discount, content_line_formats['format_content_float_border'])
            total_discount += sum_discount or 0
            sheet.write(rowx, 9, sum_pure, content_line_formats['format_content_float_border'])
            total_pure += sum_pure or 0
            sheet.write(rowx, 10, sum_tax, content_line_formats['format_content_float_border'])
            total_tax += sum_tax or 0
            sheet.write(rowx, 11, sum_taxed, content_line_formats['format_content_float_border'])
            total_taxed += sum_taxed or 0
            sheet.write(rowx, 12, sum_cost, content_line_formats['format_content_float_border'])
            total_cost += sum_cost or 0
            rowx += 1
            sequence += 1
            for line in sale_lines:
                sheet.write(rowx, 0, sequence, content_line_formats['format_content_left_color'])
                sheet.write(rowx, 1, line.get('date'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 2, line.get('name'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 3, line.get('origin'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 4, line.get('partner'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 5, '', content_line_formats['format_content_left_color'])
                sheet.write(rowx, 6, line.get('qty'), content_line_formats['format_content_float_border'])
                sheet.write(rowx, 7, line.get('total'), content_line_formats['format_content_float_border'])
                sheet.write(rowx, 8, line.get('discount'), content_line_formats['format_content_float_border'])
                sheet.write(rowx, 9, line.get('pure'), content_line_formats['format_content_float_border'])
                sheet.write(rowx, 10, line.get('tax'), content_line_formats['format_content_float_border'])
                sheet.write(rowx, 11, line.get('taxed'), content_line_formats['format_content_float_border'])
                sheet.write(rowx, 12, line.get('cost'), content_line_formats['format_content_float_border'])
                rowx += 1
                sequence += 1
        if return_lines:
            sum_qty = 0 or 0
            sum_total = 0 or 0
            sum_discount = 0 or 0
            sum_pure = 0 or 0
            sum_tax = 0 or 0
            sum_taxed = 0 or 0
            sum_cost = 0 or 0
            sequence = 0
            for line in return_lines:
                sum_qty += line.get('qty', 0)
                sum_total += line.get('total', 0)
                sum_discount += line.get('discount', 0)
                sum_pure += line.get('pure', 0)
                sum_tax += line.get('tax', 0)
                sum_taxed += line.get('taxed', 0)
                sum_cost += line.get('cost', 0)
            sheet.merge_range(rowx, 0, rowx, 4, 'Return', content_line_formats['format_content_left_color'])
            sheet.write(rowx, 5, '', content_line_formats['format_content_left_color'])
            sheet.write(rowx, 6, sum_qty, content_line_formats['format_content_float_border'])
            total_qty += sum_qty or 0
            sheet.write(rowx, 7, sum_total, content_line_formats['format_content_float_border'])
            sub_total += sum_total or 0
            sheet.write(rowx, 8, sum_discount, content_line_formats['format_content_float_border'])
            total_discount += sum_discount or 0
            sheet.write(rowx, 9, sum_pure, content_line_formats['format_content_float_border'])
            total_pure += sum_pure or 0
            sheet.write(rowx, 10, sum_tax, content_line_formats['format_content_float_border'])
            total_tax += sum_tax or 0
            sheet.write(rowx, 11, sum_taxed, content_line_formats['format_content_float_border'])
            total_taxed += sum_taxed or 0
            sheet.write(rowx, 12, sum_cost, content_line_formats['format_content_float_border'])
            total_cost += sum_cost or 0
            rowx += 1
            sequence += 1
            for line in return_lines:
                sheet.write(rowx, 0, sequence, content_line_formats['format_content_left_color'])
                sheet.write(rowx, 1, line.get('date'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 2, line.get('name'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 3, line.get('origin'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 4, line.get('partner'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 5, '', content_line_formats['format_content_left_color'])
                sheet.write(rowx, 6, line.get('qty'), content_line_formats['format_content_float_border'])
                sheet.write(rowx, 7, line.get('total'), content_line_formats['format_content_float_border'])
                sheet.write(rowx, 8, line.get('discount'), content_line_formats['format_content_float_border'])
                sheet.write(rowx, 9, line.get('pure'), content_line_formats['format_content_float_border'])
                sheet.write(rowx, 10, line.get('tax'), content_line_formats['format_content_float_border'])
                sheet.write(rowx, 11, line.get('taxed'), content_line_formats['format_content_float_border'])
                sheet.write(rowx, 12, line.get('cost'), content_line_formats['format_content_float_border'])
                rowx += 1
                sequence += 1
        if expense_lines:
            sum_qty = 0 or 0
            sum_cost = 0 or 0
            sequence = 0
            for line in expense_lines:
                sum_qty += line.get('qty', 0)
                sum_cost += line.get('cost', )
            sheet.merge_range(rowx, 0, rowx, 4, 'Expense', content_line_formats['format_content_left_color'])
            sheet.write(rowx, 5, '', content_line_formats['format_content_left_color'])
            sheet.write(rowx, 6, sum_qty, content_line_formats['format_content_float_border'])
            total_qty += sum_qty or 0
            sheet.write(rowx, 7, 0, content_line_formats['format_content_float_border'])
            sheet.write(rowx, 8, 0, content_line_formats['format_content_float_border'])
            sheet.write(rowx, 9, 0, content_line_formats['format_content_float_border'])
            sheet.write(rowx, 10, 0, content_line_formats['format_content_float_border'])
            sheet.write(rowx, 11, 0, content_line_formats['format_content_float_border'])
            sheet.write(rowx, 12, sum_cost, content_line_formats['format_content_float_border'])
            total_cost += sum_cost or 0
            rowx += 1
            sequence += 1
            for line in expense_lines:
                sheet.write(rowx, 0, sequence, content_line_formats['format_content_left_color'])
                sheet.write(rowx, 1, line.get('date'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 2, line.get('name'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 3, line.get('origin'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 4, '', content_line_formats['format_content_left_color'])
                sheet.write(rowx, 5, '', content_line_formats['format_content_left_color'])
                sheet.write(rowx, 6, line.get('qty'), content_line_formats['format_content_float_border'])
                sheet.write(rowx, 7, '', content_line_formats['format_content_float_border'])
                sheet.write(rowx, 8, '', content_line_formats['format_content_float_border'])
                sheet.write(rowx, 9, '', content_line_formats['format_content_float_border'])
                sheet.write(rowx, 10, '', content_line_formats['format_content_float_border'])
                sheet.write(rowx, 11, '', content_line_formats['format_content_float_border'])
                sheet.write(rowx, 12, line.get('cost'), content_line_formats['format_content_float_border'])
                rowx += 1
                sequence += 1
        if pos_lines:
            sum_qty = 0 or 0
            sum_total = 0 or 0
            sum_discount = 0 or 0
            sum_pure = 0 or 0
            sum_tax = 0 or 0
            sum_taxed = 0 or 0
            sum_cost = 0 or 0
            sequence = 0
            for line in pos_lines:
                sum_qty += line.get('qty', 0)
                sum_total += line.get('total', 0)
                sum_discount += line.get('discount', 0)
                sum_pure += line.get('pure', 0)
                sum_tax += line.get('tax', 0)
                sum_taxed += line.get('taxed', 0)
                sum_cost += line.get('cost', 0)
            sheet.merge_range(rowx, 0, rowx, 4, 'Pos Sale', content_line_formats['format_content_left_color'])
            sheet.write(rowx, 5, '', content_line_formats['format_content_left_color'])
            sheet.write(rowx, 6, sum_qty, content_line_formats['format_content_float_border'])
            total_qty += sum_qty or 0
            sheet.write(rowx, 7, sum_total, content_line_formats['format_content_float_border'])
            sub_total += sum_total or 0
            sheet.write(rowx, 8, sum_discount, content_line_formats['format_content_float_border'])
            total_discount += sum_discount or 0
            sheet.write(rowx, 9, sum_pure, content_line_formats['format_content_float_border'])
            total_pure += sum_pure or 0
            sheet.write(rowx, 10, sum_tax, content_line_formats['format_content_float_border'])
            total_tax += sum_tax or 0
            sheet.write(rowx, 11, sum_taxed, content_line_formats['format_content_float_border'])
            total_taxed += sum_taxed or 0
            sheet.write(rowx, 12, sum_cost, content_line_formats['format_content_float_border'])
            total_cost += sum_cost or 0
            rowx += 1
            sequence += 1
            for line in pos_lines:
                sheet.write(rowx, 0, sequence, content_line_formats['format_content_left_color'])
                sheet.write(rowx, 1, line.get('date'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 2, line.get('name'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 3, line.get('origin'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 4, '', content_line_formats['format_content_left_color'])
                sheet.write(rowx, 5, '', content_line_formats['format_content_left_color'])
                sheet.write(rowx, 6, line.get('qty'), content_line_formats['format_content_float_border'])
                sheet.write(rowx, 7, line.get('total'), content_line_formats['format_content_float_border'])
                sheet.write(rowx, 8, line.get('discount'), content_line_formats['format_content_float_border'])
                sheet.write(rowx, 9, line.get('pure'), content_line_formats['format_content_float_border'])
                sheet.write(rowx, 10, line.get('tax'), content_line_formats['format_content_float_border'])
                sheet.write(rowx, 11, line.get('taxed'), content_line_formats['format_content_float_border'])
                sheet.write(rowx, 12, line.get('cost'), content_line_formats['format_content_float_border'])
                rowx += 1
                sequence += 1
        if mrp_lines:
            sum_qty = 0 or 0
            sum_cost = 0 or 0
            sequence = 0
            for line in mrp_lines:
                sum_qty += line.get('qty', 0)
                sum_cost += line.get('cost', 0)
            sheet.merge_range(rowx, 0, rowx, 4, 'Mrp Production', content_line_formats['format_content_left_color'])
            sheet.write(rowx, 5, '', content_line_formats['format_content_float_border'])
            sheet.write(rowx, 6, sum_qty, content_line_formats['format_content_float_border'])
            total_qty += sum_qty or 0
            sheet.write(rowx, 7, 0, content_line_formats['format_content_float_border'])
            sheet.write(rowx, 8, 0, content_line_formats['format_content_float_border'])
            sheet.write(rowx, 9, 0, content_line_formats['format_content_float_border'])
            sheet.write(rowx, 10, 0, content_line_formats['format_content_float_border'])
            sheet.write(rowx, 11, 0, content_line_formats['format_content_float_border'])
            sheet.write(rowx, 12, sum_cost, content_line_formats['format_content_float_border'])
            total_cost += sum_cost or 0
            rowx += 1
            sequence += 1
            for line in mrp_lines:
                sheet.write(rowx, 0, sequence, content_line_formats['format_content_left_color'])
                sheet.write(rowx, 1, line.get('date'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 2, line.get('name'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 3, line.get('origin'), content_line_formats['format_content_left_color'])
                sheet.write(rowx, 4, '', content_line_formats['format_content_left_color'])
                sheet.write(rowx, 5, '', content_line_formats['format_content_left_color'])
                sheet.write(rowx, 6, line.get('qty'), content_line_formats['format_content_float_border'])
                sheet.write(rowx, 7, '', content_line_formats['format_content_float_border'])
                sheet.write(rowx, 8, '', content_line_formats['format_content_float_border'])
                sheet.write(rowx, 9, '', content_line_formats['format_content_float_border'])
                sheet.write(rowx, 10, '', content_line_formats['format_content_float_border'])
                sheet.write(rowx, 11, '', content_line_formats['format_content_float_border'])
                sheet.write(rowx, 12, line.get('cost'), content_line_formats['format_content_float_border'])
                rowx += 1
                sequence += 1
        sheet.merge_range(total_rowx, 0, total_rowx, 5, 'TOTAL', content_line_formats['format_title_float_border'])
        sheet.write(total_rowx, 6, total_qty, content_line_formats['format_title_float_border'])
        sheet.write(total_rowx, 7, sub_total, content_line_formats['format_title_float_border'])
        sheet.write(total_rowx, 8, total_discount, content_line_formats['format_title_float_border'])
        sheet.write(total_rowx, 9, total_pure, content_line_formats['format_title_float_border'])
        sheet.write(total_rowx, 10, total_tax, content_line_formats['format_title_float_border'])
        sheet.write(total_rowx, 11, total_taxed, content_line_formats['format_title_float_border'])
        sheet.write(total_rowx, 12, total_cost, content_line_formats['format_title_float_border'])
        rowx += 2
        return sheet, rowx

    def get_footer(self, sheet, header_formats, rowx):

        sheet.write(rowx, 1, _('Executive Director .................................. (                                   )'), header_formats['format_filter'])
        sheet.write(rowx + 2, 1, _('General Accountant .................................. (                                   )'), header_formats['format_filter'])
        return sheet

    @api.multi
    def export_report(self):
        ''' Тайлангийн загварыг боловсруулж өгөгдлүүдийг
                    тооцоолж байрлуулна.
        '''
        # Тайлан хэвлэх процесс
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # Нэр үүсгэх
        report_name = _('Expense ledger report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'))
        # Тайлангийн формат хэсэг
        format_content_header = book.add_format(ReportExcelCellStyles.format_content_header)
        format_content_float_border = book.add_format(ReportExcelCellStyles.format_content_float_border)
        format_content_left_color = book.add_format(ReportExcelCellStyles.format_content_left_color)
        format_title_float_border = book.add_format(ReportExcelCellStyles.format_title_float_border)
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title_color = book.add_format(ReportExcelCellStyles.format_title_color)

        header_formats = {
            'format_filter': format_filter,
            'format_name': format_name,
        }

        content_line_formats = {
            'format_content_float_border': format_content_float_border,
            'format_content_left_color': format_content_left_color,
            'format_title_float_border': format_title_float_border,
            'format_title_color': format_title_color,
        }

        # тайлангийн обьект
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=report_name,
                                                                                     form_title=file_name,
                                                                                     end_date=self.end_date,
                                                                                     start_date=self.start_date).create(
            {})

        # Тайлангийн хуудас
        sheet = book.add_worksheet(report_name)
        sheet = self.get_sheet_format(sheet)

        # Тооцоолох багана
        sheet.set_column('A:A', 4)
        sheet.set_column('B:B', 30)
        sheet.set_column('B:B', 10)
        sheet.set_column('C:C', 10)
        sheet.set_column('D:D', 27)
        sheet.set_column('E:E', 15)
        sheet.set_column('F:F', 30)
        sheet.set_column('G:G', 10)
        sheet.set_column('H:H', 15)
        sheet.set_column('I:I', 15)
        sheet.set_column('J:J', 15)
        sheet.set_column('K:K', 15)
        sheet.set_column('L:L', 15)
        sheet.set_column('M:M', 12)
        sheet.set_column('N:N', 8)

        # тайлангийн(хүснэгт) толгой хэсэг
        rowx = 0
        sheet, rowx = self.get_header(sheet, header_formats, rowx, report_name)
        sheet, rowx = self.get_value(sheet, content_line_formats, rowx, format_content_header)
        sheet = self.get_footer(sheet, header_formats, rowx)
        book.close()
        # Файлын өгөгдлийг тохируулах
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # Экспортын функц дуудаж байна
        return report_excel_output_obj.export_report()
