# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Stock Cost Calculation",
    'version': '1.0',
    'depends': ['l10n_mn_stock_account_wrong_transactions'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Нийт орлого, зарлагын хувьд өртөг тооцолох багаж
       - Нийт орлогын нийлбэрээр дундаж өртгийг олон нийт зарлагын өртгөө шинэчлэх зарчмаар ажиллана
       - Мөн зарим орлогыг тооцсон өртгөөр шинэчлэнэ
       - Нийт Орлого = Эхний үлдэгдлийн тооллогын орлого + Худалдан авалтын орлого - Худалдан авалтын буцаалт + Үйлдвэрлэлийн орлого
       - Нийт Зарлага = Борлуулалтын зарлага + Үйлдвэрлэлийн зарлага + Тооллогын зарлага + Нөхөн дүүргэлтийн зарлага
       - Тооцсон Өртгөөрх Орлого = Нөхөн дүүргэлтийн орлого + Тооллогын орлого
       - Энэ аргаар өртөг угаахад хугацааны дунд тухайн бараа хасах руу орсон нь чухал биш болох бөгөөд хугацааны сүүлд л хасах руу орсон эсэхийг шалгана
       - Барааны хөдөлгөөнтэй холбоотой журналын мөрийн дүн мөн шинэчлэгдэнэ
       - Худалдан авалтын буцаалтыг эх баримтын дүнгээр өөрчилнө
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/stock_cost_calculation_view.xml',
        #'views/stock_move_account_move_delete_view.xml',
        #'views/stock_move_edit_account_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
