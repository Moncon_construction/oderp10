# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError
import time
from datetime import date, datetime
import logging
_logger = logging.getLogger(__name__)


class StockCostCalculation(models.Model):
    _name = 'stock.cost.calculation'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = 'Stock Cost Calculation'
     
    name = fields.Char('Name', required=True)
    calculation_period = fields.Selection([('all_time', 'Calculate All Time'),
                                           ('date', 'Calculate Date'),
                                           ], string='Cost Calculation Period', default='all_time', required=True, track_visibility='onchange')
    product_filter = fields.Selection([('with_stock_move', 'Select Products With Stock Move'),
                                       ('manual', 'Select Products Manually')], string='Calculate cost of',default='with_stock_move', required=True, track_visibility='onchange')
    date_from = fields.Date('Date From', track_visibility='onchange')
    date_to = fields.Date('Date To', track_visibility='onchange')
    description = fields.Text('Description')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    state = fields.Selection([('draft', 'Draft'),
                              ('started', 'Started'),
                              ('done', 'Done')], string='State', default='draft', required=True, track_visibility='onchange')
    calculate_by = fields.Selection([('1', 'Calculate Products by 1'),
                                     ('5', 'Calculate Products by 5'),
                                     ('10', 'Calculate Products by 10'),
                                     ('50', 'Calculate Products by 50'),
                                     ('100', 'Calculate Products by 100'),
                                     ('500', 'Calculate Products by 500'),
                                     ('1000', 'Calculate Products by 1000'),
                                     ('5000', 'Calculate Products by 5000')], string='Calculate Products By', default='100', required=True, track_visibility='onchange')
    minus_line_ids = fields.One2many('stock.cost.calculation.minus.line', "calculation_id", string='Minus Products', readonly=True, copy=False)
    calculate_product_ids = fields.Many2many('product.product', 'cost_calculate_product_rel', 'calculcation_id', 'product_id',
                                             'Products to Calculate', copy=False)
    income_product_ids = fields.Many2many('product.product', 'cost_calculate_imcome_product_rel', 'calculcation_id', 'product_id',
                                          'Imcome Products to Calculate', readonly=True, copy=False)
    ready_product_ids = fields.Many2many('product.product', 'cost_calculate_ready_product_rel', 'calculcation_id', 'product_id',
                                         'Ready Products for Calculation', readonly=True, copy=False)
    calculated_done_product_ids = fields.One2many('stock.cost.calculation.done', "calculation_id", string='Calculated Products', readonly=True,
                                                  copy=False, domain=[('state', '=', 'stock_move_corrected')])
    account_corrected_product_ids = fields.One2many('stock.cost.calculation.done', "calculation_id",  string='Calculated Products',
                                                    readonly=True, copy=False, domain=[('state', '=', 'account_move_corrected')])

    @api.multi
    def make_draft(self):
        self.write({'state': 'draft'})

    @api.multi
    def make_done(self):
        self.write({'state': 'done'})

    @api.multi
    def check_minus_move(self, product_ids):
        self.minus_line_ids = None
        minus_line_obj = self.env['stock.cost.calculation.minus.line']
        location_obj = self.env['stock.location']
        minus_product_ids = []
        where = ' AND company_id = ' + str(self.company_id.id) + ' '
        locations = []
        if product_ids:
            where += ' AND m.product_id in (' + ','.join(map(str, product_ids)) + ') '
        loc_id = location_obj.search([('usage', '=', 'internal'), ('company_id', '=', self.company_id.id)]).ids
        if len(loc_id) > 0:
            locations = tuple(loc_id)
        if self.calculation_period == 'from_date' and self.date_to:
            date_to = self.date_to
        else:
            date_to = datetime.today().strftime('%Y-%m-%d')
        self.env.cr.execute("SELECT l.prod_id AS pid, l.lid as lid, SUM(l.qty) AS qty "
                            "FROM ( SELECT m.product_id AS prod_id, "
                                            "CASE WHEN m.location_id NOT IN %s AND m.location_dest_id IN %s "
                                            "THEN m.product_qty "
                                            "WHEN m.location_id IN %s AND m.location_dest_id NOT IN %s "
                                            "THEN -m.product_qty ELSE 0 END AS qty, "
                                            "CASE WHEN m.location_id NOT IN %s AND m.location_dest_id IN %s "
                                            "THEN m.location_dest_id "
                                            "WHEN m.location_id IN %s AND m.location_dest_id NOT IN %s "
                                            "THEN location_id ELSE 0 END AS lid "
                                    "FROM stock_move m "
                                    "WHERE m.date <= %s AND m.state = 'done' " + where + " ) AS l "
                            "GROUP BY l.prod_id, l.lid "
                            "HAVING SUM(l.qty) < 0",
                            (locations, locations, locations, locations, locations, locations, locations, locations, date_to))
        minus_products = self.env.cr.dictfetchall()
        for minus_product in minus_products:
            qty = round(minus_product['qty'], self.env['decimal.precision'].precision_get('Account'))
            if qty < 0:
                if minus_product['pid'] not in minus_product_ids:
                    minus_product_ids.append(minus_product['pid'])
                minus_line_obj.create({'product_id': minus_product['pid'],
                                       'minus_date': self.date_to or datetime.today().strftime('%Y-%m-%d'),
                                       'location_id': minus_product['lid'],
                                       'minus_qty': qty,
                                       'calculation_id': self.id,
                                       })
        if minus_product_ids:
            return minus_product_ids

    @api.multi
    def check_income_move(self, product_ids):
        income_product_ids = []
        where = " AND m.company_id = " + str(self.company_id.id) + " "
        if self.calculation_period == 'date':
            if not self.date_to:
                where += " AND m.date >= '" + self.date_from + "' "
            else:
                where += " AND m.date BETWEEN '" + self.date_from + "' AND '" + self.date_to + "' "
        self.env.cr.execute("SELECT distinct(m.product_id)"
                            "FROM stock_move m "
                            "JOIN stock_location l ON (m.location_id = l.id) "
                            "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                            "WHERE l.usage = 'production' AND l1.usage = 'internal' AND m.state = 'done' " + where + " ")
        products = self.env.cr.fetchall()
        for product in products:
            income_product_ids.append(product[0])
        if income_product_ids:
            self.income_product_ids = income_product_ids
            return income_product_ids

    @api.multi
    def start(self):
        self.minus_line_ids = None
        self.calculate_product_ids = None
        self.income_product_ids = None
        self.ready_product_ids = None
        self.calculated_done_product_ids = None
        self.account_corrected_product_ids = None
        product_ids = []
        where = " WHERE company_id = " + str(self.company_id.id) + " AND state != 'cancel' "
        if self.calculation_period == 'date':
            if not self.date_from:
                raise UserError(_("Please select date from field"))
            if not self.date_to:
                where += " AND date >= '" + self.date_from + "' "
            else:
                where += " AND date BETWEEN '" + self.date_from + "' AND '" + self.date_to + "' "

        if self.product_filter == 'with_stock_move':                # Агуулахад хөдөлгөөн хийгдсэн бараануудыг сонгох
            self.env.cr.execute("SELECT distinct(product_id) FROM stock_move" + where + " ")
        result = self.env.cr.fetchall()
        for res in result:
            product_ids.append(res[0])
        if product_ids:
            self.calculate_product_ids = product_ids
            delete_product_ids = self.check_minus_move(product_ids)
            if delete_product_ids:
                self.calculate_product_ids = [(2, product_id, False) for product_id in delete_product_ids]
            income_product_ids = self.check_income_move(self.calculate_product_ids)
            if income_product_ids:
                self.calculate_product_ids = [(2, product_id, False) for product_id in income_product_ids]
        self.write({'state': 'started'})

    @api.multi
    def check_products(self):
        '''Энэ товчийг нэг удаа дараад 'Тооцоолох бараанууд' табнаас барааг 'Нэг удаагийн уншуулах хэмжээ' талбарт заасан тоогоор уншиж,
            хасах руу орсон эсэхийг шалгаад хасах руу орсон барааг 'Хасах үлдэгдэлтэй бараанууд' таб руу, хасах руу ороогүй барааг
            'Өртөг тооцоход бэлэн' таб руу тус тус ялгаж ангилна. Уншсан бараагаа Тооцоолох бараанууд табнаас устгана.'''
        add_to_ready_product_ids = []
        range = int(self.calculate_by)
        count = 0
        for product in self.calculate_product_ids:
            if count == range:
                break
            add_to_ready_product_ids.append(product.id)
            count += 1
        self.calculate_product_ids = [(2, product_id, False) for product_id in add_to_ready_product_ids]
        self.ready_product_ids = [(4, product_id, False) for product_id in add_to_ready_product_ids]

    @api.multi
    def income_check_products(self):
        '''Энэ товчийг нэг удаа дараад 'Тооцоолох бараанууд' табнаас барааг 'Нэг удаагийн уншуулах хэмжээ' талбарт заасан тоогоор уншиж,
            хасах руу орсон эсэхийг шалгаад хасах руу орсон барааг 'Хасах үлдэгдэлтэй бараанууд' таб руу, хасах руу ороогүй барааг
            'Өртөг тооцоход бэлэн' таб руу тус тус ялгаж ангилна. Уншсан бараагаа Тооцоолох бараанууд табнаас устгана.'''
        add_to_ready_product_ids = []
        range = int(self.calculate_by)
        count = 0
        for product in self.income_product_ids:
            if count == range:
                break
            add_to_ready_product_ids.append(product.id)
            count += 1
        self.income_product_ids = [(2, product_id, False) for product_id in add_to_ready_product_ids]
        self.ready_product_ids = [(4, product_id, False) for product_id in add_to_ready_product_ids]

    @api.multi
    def refresh_minus_products(self):
        '''Хасах үлдэгдэлтэй бараагаа засчихаад энэ товчийг дарж засагдсан эсэхийг дахин шалгуулна. 'Хасах үлдэгдэлтэй бараанууд' табан дахь
            бараануудыг дахин шалгаад нэмэх рүү орсон барааг нь энэ жагсаалтнаас хасч, 'Өртөг тооцоход бэлэн' таб руу шилжүүлнэ.
            Хасах үлдэгдэлтэй хэвээр байгаа барааны мөрийн утгыг шинэчилнэ.
        '''
        minus_product_ids = []
        ready_product_ids = []
        for minus in self.minus_line_ids:
            if minus.product_id and minus.product_id.id not in minus_product_ids:
                minus_product_ids.append(minus.product_id.id)
        if minus_product_ids:
            product_ids = self.check_minus_move(minus_product_ids)
        if product_ids:
            for product in minus_product_ids:
                if product not in product_ids:
                    ready_product_ids.append(product)
            self.calculate_product_ids = [(2, product_id, False) for product_id in ready_product_ids]
            self.ready_product_ids = [(4, product_id, False) for product_id in ready_product_ids]

    @api.model
    def is_module_installed(self, module_name):
        self._cr.execute(
            "SELECT id FROM ir_module_module WHERE name = '%s' AND state IN ('installed', 'to upgrade')" % module_name)
        results = self._cr.dictfetchall()
        if results and len(results) > 0:
            return True
        else:
            return False

    @api.multi
    def purchase_refund_cost_edit(self, where):
        # Худалдан авалтын буцаалтын өртөгийг Худалдан авалтын өртөгөөр засах
        if self.ready_product_ids:
            where += ' AND m.product_id in (' + ','.join(map(str, self.ready_product_ids.ids)) + ') '
        self.env.cr.execute("UPDATE stock_move m "
                            "SET price_unit = p.price_unit "
                            "FROM (SELECT m.id AS id, COALESCE(m1.price_unit, 0) AS price_unit "
                                    "FROM stock_move m "
                                    "JOIN stock_move m1 ON (m1.id = m.origin_returned_move_id) "
                                    "JOIN stock_location l ON (m.location_id = l.id) "
                                    "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                                    "WHERE l.usage = 'internal' AND l1.usage = 'supplier' AND m.state = 'done' "
                                            + where + " AND  (m1.price_unit::numeric, 4) != (m.price_unit::numeric, 4) ) AS p "
                            "WHERE m.id = p.id ")

    @api.multi
    def mrp_income_cost_edit(self, where, product):
        # Үйлдвэрийн орлогын өртгийг орцын өртөгөөр засах
        note = ''
        self.env.cr.execute("SELECT m.production_id AS pid, m.location_dest_id AS lid, sum(product_qty) AS qty "
                            "FROM stock_move m "
                            "JOIN stock_location l ON (m.location_id = l.id) "
                            "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                            "WHERE l.usage = 'production' AND l1.usage = 'internal' AND m.state = 'done' "
                                    "AND m.production_id IS NOT NULL AND m.product_id = " + str(product) + where + " "
                             "GROUP BY m.production_id, m.location_dest_id ")
        productions = self.env.cr.dictfetchall()
        for production in productions:
            if production['qty'] > 0:
                self.env.cr.execute("SELECT sum(p.cost) AS cost "
                                    "FROM (SELECT m.raw_material_production_id AS pid, m.id, m.price_unit * m.product_qty AS cost "
                                            "FROM stock_move m "
                                            "JOIN stock_location l ON (m.location_id = l.id) "
                                            "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                                            "WHERE l.usage = 'internal' AND l1.usage = 'production' AND m.state = 'done' "
                                            "AND m.raw_material_production_id = " + str(production['pid']) + where + " ) AS p ")
                incomes = self.env.cr.fetchall()
                total_cost = 0
                for income in incomes:
                    total_cost += income[0]
                cost = round(total_cost / production['qty'], self.env['decimal.precision'].precision_get('Account'))
                self.env.cr.execute("UPDATE stock_move SET price_unit = " + str(cost) + " WHERE state = 'done' AND production_id = " + str(production['pid']) + " ")
                note += _('\n        Production: %s, cost: %s') % (production['pid'], cost)
            else:
                note += _('\n        Production: %s not calculated cost.') % (production['pid'])
        return note

    @api.multi
    def income_cost_calculate(self, where, product):
        # Худалдан авалтын орлого, Үйлдвэрийн орлого, Эхний тооллогын орлогыг нэмэн Худалдан авалтын буцаалтыг хасан өртгийг тооцоолох
        self.env.cr.execute("SELECT sum(p.cost) AS cost, sum(p.qty) AS qty "
                            "FROM (SELECT sum(COALESCE(m.product_qty, 0) * COALESCE(m.price_unit, 0)) AS cost, sum(COALESCE(m.product_qty, 0)) AS qty, m.product_id AS pid "
                                   "FROM stock_move m "
                                   "JOIN stock_location l ON (m.location_id = l.id) "
                                   "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                                   "WHERE l.usage = 'supplier' AND l1.usage = 'internal' AND m.state = 'done' AND m.product_id = " + str(product) + where + " "
                                   "GROUP BY m.product_id "
                                   "UNION ALL "
                                   "SELECT sum(COALESCE(m.product_qty, 0) * COALESCE(m.price_unit, 0)) AS cost, sum(COALESCE(m.product_qty, 0)) AS qty, m.product_id AS pid "
                                   "FROM stock_move m "
                                   "JOIN stock_location l ON (m.location_id = l.id) "
                                   "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                                   "WHERE l.usage = 'production' AND l1.usage = 'internal' AND m.state = 'done' AND m.product_id = " + str(product) + where + " "
                                   "GROUP BY m.product_id "
                                   "UNION ALL "
                                   "SELECT sum(COALESCE(m.product_qty, 0) * COALESCE(m.price_unit, 0)) AS cost, sum(COALESCE(m.product_qty, 0)) AS qty, m.product_id AS pid "
                                   "FROM stock_move m "
                                   "JOIN stock_location l ON (m.location_id = l.id) "
                                   "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                                   "JOIN stock_inventory i ON (m.inventory_id = i.id) "
                                   "WHERE l.usage = 'inventory' AND l1.usage = 'internal' AND m.state = 'done' AND i.is_initial = 't' AND m.product_id = "
                                            + str(product) + where + " "
                                   "GROUP BY m.product_id "
                                   "UNION ALL "
                                   "SELECT sum(-COALESCE(m.product_qty, 0) * COALESCE(m.price_unit, 0)) AS cost, sum(-COALESCE(m.product_qty, 0)) AS qty, m.product_id AS pid "
                                   "FROM stock_move m "
                                   "JOIN stock_move m1 ON (m1.id = m.origin_returned_move_id) "
                                   "JOIN stock_location l ON (m.location_id = l.id) "
                                   "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                                   "WHERE l.usage = 'internal' AND l1.usage = 'supplier' AND m.state = 'done' AND m.product_id = " + str(product) + where + " " 
                                   "GROUP BY m.product_id ) AS p "
                            "GROUP BY p.pid ")
        return self.env.cr.dictfetchall()

    @api.multi
    def inventory_income_cost_calculate(self, where, product):
        # Тооллогын орлогоор өртгийг тооцоолох
        self.env.cr.execute("SELECT SUM(COALESCE(p.cost, 0)) AS cost, SUM(COALESCE(p.qty, 0)) AS qty "
                            "FROM (SELECT w.id AS wid, sum(m.product_qty * m.price_unit) AS cost, sum(m.product_qty) AS qty "
                                    "FROM stock_move m "
                                    "JOIN stock_location l ON (m.location_id = l.id) "
                                    "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                                    "JOIN stock_inventory i ON (m.inventory_id = i.id) "
                                    "JOIN stock_warehouse w ON (w.lot_stock_id = l1.id) "
                                    "WHERE l.usage = 'inventory' AND l1.usage = 'internal' AND m.state = 'done' "
                                    "AND i.is_initial = 'f' AND m.product_id = " + str(product) + where + " "
                                    "GROUP BY w.id ) AS p "
                            "GROUP BY p.wid")
        return self.env.cr.dictfetchall()

    @api.multi
    def calculate_cost(self):
        '''Энэ товчийг нэг удаа дараад 'Өртөг тооцоход бэлэн' табнаас барааг 'Нэг удаагийн уншуулах хэмжээ' талбарт заасан тоогоор уншиж,
            уншсан бараануудын өртгийг тооцоолж бүх stock.move-үүд дэх өртгийг зөв болгож, барааны өнөөдрийн өртгийг засна. Амжилттай өртөг
            тооцогдсон бараануудыг 'Өртөг тооцогдсон бараанууд' таб руу шилжүүлнэ.
        '''
        done_products_obj = self.env['stock.cost.calculation.done']
        price_history = self.env['product.price.history']
        count = 1
        description = ''
        delete_product_ids = []
        _logger.info(_('Start: Product cost calculate'))
        # Эхлээд бүх орлогын нийт өртгийг нийт тоо хэмжээнд хувааж дундаж өртгийг олооод олсон өртгөө бүх зарлага дээр тавиах аргаар өртөг тооцно
        where = " AND m.company_id = " + str(self.company_id.id) + " "
        if self.calculation_period == 'date':
            locations = []
            loc_id = self.env['stock.location'].search([('usage', '=', 'internal'), ('company_id', '=', self.company_id.id)]).ids
            if len(loc_id) > 0:
                locations = ','.join(map(str, loc_id))
            if not self.date_to:
                where += " AND m.date >= '" + self.date_from + "' "
            else:
                where += " AND m.date BETWEEN '" + self.date_from + "' AND '" + self.date_to + "' "
        # Худалдан авалтын буцаалтын дүнг засна
        self.purchase_refund_cost_edit(where)
        for product in self.ready_product_ids:
            initial_fault = True
            done_product = True
            is_calculate = False
            price_unit = False
            old_cost = qty = cost = 0
            note = _('\n [%s] %s product cost calculate start. ') % (product.default_code, product.name)
            if self.calculation_period == 'date':
                # Хугацааны хооронд тооцоолж байгаа тохиолдолд эхний үлдэгдлийг олно
                self.env.cr.execute("SELECT SUM(l.cost) AS cost , SUM(l.qty) AS qty "
                                    "FROM ( SELECT m.product_id AS prod_id, "
                                                    "CASE WHEN m.location_id NOT IN (" + locations + ") AND m.location_dest_id IN (" + locations + ") "
                                                    "THEN COALESCE(m.product_qty, 0) "
                                                    "WHEN m.location_id IN (" + locations + ") AND m.location_dest_id NOT IN (" + locations + ") "
                                                    "THEN -COALESCE(m.product_qty, 0) ELSE 0 END AS qty, "
                                                    "CASE WHEN m.location_id NOT IN (" + locations + ") AND m.location_dest_id IN (" + locations + ") "
                                                    "THEN COALESCE(m.product_qty, 0) * COALESCE(m.price_unit, 0) "
                                                    "WHEN m.location_id IN (" + locations + ") AND m.location_dest_id NOT IN (" + locations + ") "
                                                    "THEN -COALESCE(m.product_qty, 0) * COALESCE(m.price_unit, 0) ELSE 0 END AS cost "
                                                    "FROM stock_move m "
                                                    "WHERE m.date < %s AND m.state = 'done' AND m.company_id = " + str(self.company_id.id) + " "
                                                           "AND m.product_id = " + str(product.id) + ") AS l "
                                    "GROUP BY l.prod_id ",(self.date_from,))
                initials = self.env.cr.dictfetchall()
                for initial in initials:
                    qty += round(initial['qty'], self.env['decimal.precision'].precision_get('Account'))
                    cost += round(initial['cost'], self.env['decimal.precision'].precision_get('Account'))
                    if qty > 0 and cost >= 0:
                        old_cost = round(cost / qty, self.env['decimal.precision'].precision_get('Account'))
                        note += _('\n    %s before') % self.date_from
                        note += _('\n    Average cost: %s, Qty: %s') % (old_cost, qty)
                    elif qty == 0 and cost == 0:
                        note += _('\n    %s before') % self.date_from
                        note += _('\n    Average cost: 0, Qty: 0')
                    else:
                        initial_fault = False
                        note += _('\n    %s before') % self.date_from
                        note += _('\n    Initial cost invalid')
                        note += _('\n    Total cost: %s, Qty: %s') % (cost, qty)
            print (initial_fault, old_cost)
            if initial_fault:
                if self.is_module_installed("mrp"):
                    note += self.mrp_income_cost_edit(where, product.id)
                incomes = self.income_cost_calculate(where, product.id)
                if len(incomes) == 0:
                    incomes = self.inventory_income_cost_calculate(where, product.id)
                for income in incomes:
                    qty += income['qty']
                    cost += income['cost']
                if qty > 0 and cost > 0:
                    is_calculate = True
                    price_unit = round(cost / qty, self.env['decimal.precision'].precision_get('Account'))
                    note += _('\n        Renewed cost: %s') % price_unit
                elif qty == 0 and cost == 0:
                    self.env.cr.execute("SELECT SUM(p.qty) "
                                        "FROM ( SELECT m.product_id AS pid, SUM(m.product_qty) AS qty "
                                                "FROM stock_move m "
                                                "JOIN stock_location l ON (m.location_id = l.id) "
                                                "WHERE m.state = 'done' AND l.usage != 'supplier' " + where +
                                                        "AND m.product_id = " + str(product.id) + " AND m.location_dest_id in (" + locations + ") "
                                                "GROUP BY m.product_id "
                                                "UNION ALL "
                                                "SELECT m.product_id AS pid, SUM(-m.product_qty) AS qty "
                                                "FROM stock_move m "
                                                "JOIN stock_location l ON (m.location_dest_id = l.id) "
                                                "WHERE m.state = 'done' AND l.usage != 'supplier' " + where +
                                                        "AND m.product_id = " + str(product.id) + " AND m.location_id in (" + locations + ") " ""
                                                "GROUP BY m.product_id ) AS p "
                                        "GROUP BY p.pid ")
                    expense = self.env.cr.fetchone()
                    if not expense or len(expense) == 0 or (len(expense) > 0 and expense[0] == None or expense[0] == 0):
                        is_calculate = True
                        note += _('\n        ')
                    else:
                        note += _('\n        This product not calculate cost. Cost = 0, Income Qty = 0. But Expense Qty = %s' % (expense[0]))
                else:
                    note += _('\n        This product not calculate cost. Total cost: %s, Qty: %s') %(cost, qty)
                    done_product = False
                if is_calculate and price_unit > 0:
                    # Компани дундаа өртөг хөтөлж байгаа үед тооцоолсон шинэ өртгийг үнийн түүхэнд нэмнэ
                    price_history.create({'product_id': product.id,
                                          'price_type': 'standard_price',
                                          'cost': price_unit,
                                          # 'old_cost': product.standard_price,
                                          'company_id': self.company_id.id,
                                          'product_template_id': product.product_tmpl_id and product.product_tmpl_id.id,
                                          'datetime': datetime.today()
                                          })
                    # Борлуулалт, Нөхөн дүүргэлт болон Үйлдвэрлэлийн зарлагын өртгийг тооцоолсон өртгөөр засах
                    self.env.cr.execute("UPDATE stock_move m "
                                        "SET price_unit = " + str(price_unit) + " "
                                        "FROM (SELECT m.id AS id FROM stock_move m "
                                                "JOIN stock_location l ON (m.location_dest_id = l.id) "
                                                "JOIN stock_location l1 ON (m.location_id = l1.id) "
                                                "WHERE l.usage in ('customer', 'transit', 'production') AND l1.usage = 'internal' "
                                                        + where + "AND m.product_id = " + str(product.id) + " AND m.price_unit != " + str(price_unit) + ") AS p "
                                        "WHERE m.id = p.id ")

                    # Борлуулалтын буцаалт болон нөхөн дүүргэлтийн орлогын өртгийг тооцоолсон өртгөөр засах
                    self.env.cr.execute("UPDATE stock_move m "
                                        "SET price_unit = " + str(price_unit) + " "
                                        "FROM (SELECT m.id AS id FROM stock_move m "
                                                "JOIN stock_location l ON (m.location_id = l.id) "
                                                "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                                                "WHERE l.usage in ('customer', 'transit') AND l1.usage = 'internal' "
                                                        + where + "AND m.product_id = " + str(product.id) + " AND m.price_unit != " + str(price_unit) + ") AS p "
                                        "WHERE m.id = p.id ")

                    # Тооллогын зарлагын өртгийг тооцоолсон өртгөөр засах
                    self.env.cr.execute("UPDATE stock_move m "
                                        "SET price_unit = " + str(price_unit) + " "
                                        "FROM (SELECT m.id AS id FROM stock_move m "
                                                "JOIN stock_location l ON (m.location_dest_id = l.id) "
                                                "JOIN stock_location l1 ON (m.location_id = l1.id) "
                                                "JOIN stock_inventory i ON (m.inventory_id = i.id) "
                                                "WHERE l.usage = 'inventory' AND l1.usage = 'internal' AND i.is_initial = 'f' "
                                                        + where + "AND m.product_id = " + str(product.id) + " AND m.price_unit != " + str(price_unit) + ") AS p "
                                        "WHERE m.id = p.id ")

                    # Тооллогын орлогын өртгийг тооцоолсон өртгөөр засах
                    self.env.cr.execute("UPDATE stock_move m "
                                        "SET price_unit =  " + str(price_unit) + " "
                                        "FROM (SELECT m.id AS id FROM stock_move m "
                                                "JOIN stock_location l ON (m.location_id = l.id) "
                                                "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                                                "JOIN stock_inventory i ON (m.inventory_id = i.id) "
                                                "WHERE l.usage = 'inventory' AND l1.usage = 'internal' AND i.is_initial = 'f' "
                                                        + where + "AND m.product_id = " + str(product.id) + " AND m.price_unit != " + str(price_unit) + ") AS p "
                                        "WHERE m.id = p.id ")

                    # Шаардахын зарлагын өртгийг тооцоолсон өртгөөр засах
                    self.env.cr.execute("UPDATE stock_move m "
                                        "SET price_unit = " + str(price_unit) + " "
                                        "FROM (SELECT m.id AS id FROM stock_move m "
                                                "JOIN stock_location l ON (m.location_dest_id = l.id) "
                                                "JOIN stock_location l1 ON (m.location_id = l1.id) "
                                                "JOIN stock_picking p ON (m.picking_id = p.id) "
                                                "WHERE l.usage = 'inventory' AND l1.usage = 'internal' "
                                                        + where + "AND m.product_id = " + str(product.id) + " AND m.price_unit != " + str(price_unit) + ") AS p "
                                        "WHERE m.id = p.id ")

                    # Шаардахын буцаалтын өртгийг тооцоолсон өртгөөр засах
                    self.env.cr.execute("UPDATE stock_move m "
                                        "SET price_unit =  " + str(price_unit) + " "
                                        "FROM (SELECT m.id AS id FROM stock_move m "
                                                "JOIN stock_location l ON (m.location_id = l.id) "
                                                "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                                                "JOIN stock_picking p ON (m.picking_id = p.id) "
                                                "WHERE l.usage = 'inventory' AND l1.usage = 'internal'"
                                                        + where + "AND m.product_id = " + str(product.id) + " AND m.price_unit != " + str(price_unit) + ") AS p "
                                        "WHERE m.id = p.id ")
                elif not is_calculate:
                    done_product = False
            else:
                done_product = False
            if done_product:
                # Тооцоолсон барааны мэдээллийг үүсгэх
                done_products_obj.create({'product_id': product.id,
                                          'old_standard_price': old_cost,
                                          'new_standard_price': price_unit,
                                          'calculation_note': note,
                                          'calculation_id': self.id,
                                          'state': 'stock_move_corrected',
                                          })
                delete_product_ids.append(product.id)
                _logger.info(_('Calculated count: %s') % (count))
                count += 1
            else:
                description += note
        if delete_product_ids:
            # Тооцоолох бараануудаас өртгийг шинэчилсэн бараануудыг хасах
            self.ready_product_ids = [(2, product_id, False) for product_id in delete_product_ids]
        self.description = description
        _logger.info(_('Done: Product cost calculated'))

    @api.multi
    def fix_acount_moves(self):
        '''Энэ товч нь 'Өртөг тооцогдсон таб' дахь бараануудын барааны хөдөлгөөнүүдийг 'Нэг удаагийн уншуулах хэмжээ' талбарт заасан тоогоор уншиж,
            холбоотой ажил гүйлгээнүүдийг нь зөв дүнгээр засна. Засагдсан мөрүүдийг 'Дансны дүн засагдсан' таб руу шилжүүлнэ.
        '''
        product_ids = []
        _logger.info(_('Start: Account move lines calculate'))
        where = " AND m.company_id = " + str(self.company_id.id) + " "
        count = 1
        if self.calculation_period == 'date':
            if not self.date_to:
                where += " AND m.date >= '" + self.date_from + "' "
            else:
                where += " AND m.date BETWEEN '" + self.date_from + "' AND '" + self.date_to + "' "
        range = int(self.calculate_by)
        for done_product in self.calculated_done_product_ids:
            product_ids.append(done_product.product_id.id)
            done_product.account_fix_note = _('Account move lines calculated [%s] %s. ') % (done_product.product_id.default_code, done_product.product_id.name)
            done_product.state = 'account_move_corrected'
            if count == range:
                self.env.cr.execute("UPDATE account_move_line l "
                                    "SET debit = m.price_unit * m.product_qty "
                                    "FROM stock_move m "
                                    "WHERE m.id = l.stock_move_id AND l.debit > 0 AND (l.debit::numeric, 4) != ((m.price_unit * m.product_qty)::numeric, 4) "
                                    + where + "AND m.state = 'done' AND m.product_id in (" + ','.join(map(str, product_ids)) + ") ")
                _logger.info(_('Done: Debit account move lines calculate'))
                self.env.cr.execute("UPDATE account_move_line l "
                                    "SET credit = m.price_unit * m.product_qty "
                                    "FROM stock_move m "
                                    "WHERE m.id = l.stock_move_id AND l.credit > 0 AND (l.credit::numeric, 4) != ((m.price_unit * m.product_qty)::numeric, 4) "
                                    + where + "AND m.state = 'done' AND m.product_id in (" + ','.join(map(str, product_ids)) + ") ")
                _logger.info(_('Done: Credit account move lines calculate'))
                break
            count += 1
        pass


class StockCostCalculationMinusLine(models.Model):
    _name = 'stock.cost.calculation.minus.line'
    _description = 'Stock With Minus Balance'
    
    product_id = fields.Many2one('product.product', required=True, string='Product')
    location_id = fields.Many2one('stock.location', required=True, string='Stock Location')
    minus_qty = fields.Float('Minus Quantity', required=True, readonly=True)
    minus_date = fields.Date('Minus Date', required=True, readonly=True)
    calculation_id = fields.Many2one('stock.cost.calculation', string='Stock Cost Calculation')
    user_id = fields.Many2one('res.users', required=True, string='Checked User', default=lambda self: self.env.user)
    checked_date = fields.Date('Checked Date', required=True, readonly=True, default=fields.Date.context_today)
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)

class StockCostCalculatedDone(models.Model):
    _name = 'stock.cost.calculation.done'
    _description = 'Calculated Products'

    product_id = fields.Many2one('product.product', required=True, string='Product')
    old_standard_price = fields.Float('Old Standard Price')
    new_standard_price = fields.Float('New Standard Price')
    calculation_note = fields.Text('Cost Calculation Note')
    account_fix_note = fields.Text('Account Move Fix Note')
    user_id = fields.Many2one('res.users', required=True, string='Checked User', default=lambda self: self.env.user)
    calculated_date = fields.Datetime('Calculated Date', required=True, readonly=True, default=fields.Date.context_today)
    calculation_id = fields.Many2one('stock.cost.calculation', string='Product Cost Calculation')
    state = fields.Selection([('stock_move_corrected', 'Stock Move Corrected'),
                              ('account_move_corrected', 'Account Move Correct')], string='State', default='stock_move_corrected', required=True)