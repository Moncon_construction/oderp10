# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError
import time
from datetime import date, datetime
import logging

_logger = logging.getLogger(__name__)


class StockMoveAccountMoveDelete(models.Model):
    _name = 'stock.move.account.move.delete'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = 'Stock Move Account Move Delete'

    name = fields.Char('Name', required=True)
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    date_from = fields.Date('Date From', default=lambda *a: time.strftime('%Y-%m-01'), required=True, track_visibility='onchange', readonly=True, states={'draft': [('readonly', False)]})
    date_to = fields.Date('Date To', default=lambda *a: time.strftime('%Y-%m-01'), required=True, track_visibility='onchange', readonly=True, states={'draft': [('readonly', False)]})
    account_move_line_ids = fields.Many2many('account.move.line', 'stock_move_account_move_line_delete_rel', 'model_id', 'line_id', string='Account Move Lines', readonly=True, copy=False)
    description = fields.Text('Description')
    state = fields.Selection([('draft', 'Draft'),
                              ('started', 'Started'),
                              ('done', 'Done')], string='State', default='draft', required=True, track_visibility='onchange')

    # Компани, эхлэх болон дуусах огноогоор нэр талбар бөглөгдөх
    @api.onchange('date_from', 'date_to')
    def onchange_name(self):
        for report in self:
            name = ''
            if report.company_id:
                name += _('%s is ') % self.company_id.name
            if report.date_from:
                name += _('[%s - ') % self.date_from
            if report.date_to:
                name += _('%s] duration stock move delete account move') % self.date_to
            report.name = name

    @api.multi
    def make_draft(self):
        self.write({'state': 'draft'})

    @api.multi
    def start(self):
        ''' Барааны хөдөлгөөний журналын мөрүүд нь олон үүссэнг Дт, Кт 1 1 үлдээн бусдыг устгах
        '''
        self.account_move_line_ids = None
        self.stock_move_ids = None
        move_line_ids = []
        note = 'Stock moves: '
        where = " AND m.company_id = " + str(self.company_id.id) + " AND m.state = 'done' "
        where += " AND m.date BETWEEN '" + self.date_from + "' AND '" + self.date_to + "' "
        # Барааны орлогын данс зөв байгаа эсэхийг шалгана
        self.env.cr.execute("SELECT stock_move_id AS stock_move_id, id AS id, debit AS debit, credit AS credit FROM account_move_line "
                            "WHERE stock_move_id in (SELECT distinct(l.stock_move_id) "
                                                    "FROM account_move_line l "
                                                    "JOIN stock_move m ON (l.stock_move_id = m.id)"
                                                    "WHERE m.state = 'done' " + where + " "
                                                    "GROUP BY l.stock_move_id "
                                                    "HAVING count(l.stock_move_id) > 2) "
                            "ORDER BY stock_move_id, debit ")
        lines = self.env.cr.dictfetchall()
        move_id = False
        for line in lines:
            if move_id != line['stock_move_id']:
                note += ' %s, ' % move_id
                count_dt = count_kt = 0
            if line['debit'] > 0:
                count_dt += 1
                if count_dt > 1:
                    move_line_ids.append(line['id'])
            elif line['credit'] > 0:
                count_kt += 1
                if count_kt > 1:
                    move_line_ids.append(line['id'])
            move_id = line['stock_move_id']
        note += ' %s' % move_id
        # Устгах журналын мөрүүдийг буцаана
        if move_line_ids:
            self.account_move_line_ids = move_line_ids
        self.write({'state': 'started', 'description': note})

    @api.multi
    def delete_moves(self):
        ''' Давхардаж үүссэн журналын мөрүүдийг устгах
        '''
        _logger.info(_('Start: Account move line delete'))
        ids = []
        for line in self.account_move_line_ids:
            ids.append(line.id)
        self.env.cr.execute('UPDATE account_move_line SET debit = 0, credit = 0, stock_move_id = NULL WHERE id in (' + ','.join(map(str, ids)) + ') ')
        _logger.info(_('Done: Account move line delete'))
        self.write({'state': 'done'})