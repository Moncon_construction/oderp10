# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError
import time
from datetime import date, datetime
import logging
_logger = logging.getLogger(__name__)


class StockMoveEditAccount(models.Model):
    _name = 'stock.move.edit.account'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = 'Stock Move Edit Account'
     
    name = fields.Char('Name', required=True)
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    date_from = fields.Date('Date From', default=lambda *a: time.strftime('%Y-%m-01'), required=True, track_visibility='onchange', readonly=True, states={'draft': [('readonly', False)]})
    date_to = fields.Date('Date To', default=lambda *a: time.strftime('%Y-%m-01'), required=True, track_visibility='onchange', readonly=True, states={'draft': [('readonly', False)]})
    warehouse_id = fields.Many2one('stock.warehouse', string="Warehouse", required=True, track_visibility='onchange', readonly=True, states={'draft': [('readonly', False)]})
    location_ids = fields.Many2many('stock.location', string="Locations", readonly=True, states={'draft': [('readonly', False)]})
    against_check = fields.Boolean('Against Check', default=False, track_visibility='onchange', readonly=True, states={'draft': [('readonly', False)]})
    check_account_id = fields.Many2one('account.account', string='Check Account', domain=[('deprecated', '=', False)], track_visibility='onchange',
                                 readonly=True, states={'draft': [('readonly', False)]})
    account_id = fields.Many2one('account.account', string='Account', required=True, domain=[('deprecated', '=', False)], track_visibility='onchange',
                                 readonly=True, states={'draft': [('readonly', False)]})
    #other_account = fields.Boolean('Edit Other Account', default=False, track_visibility='onchange', readonly=True, states={'draft': [('readonly', False)]})
    income_move_line_ids = fields.Many2many('account.move.line', 'stock_move_income_account_move_line_rel', 'edit_id', 'line_id', string='Income Account Move Lines', readonly=True, copy=False)
    expense_move_line_ids = fields.Many2many('account.move.line', 'stock_move_expense_account_move_line_rel', 'edit_id', 'line_id', string='Expense Account Move Lines', readonly=True, copy=False)
    line_ids = fields.One2many('stock.move.edit.account.line', "edit_id", string='Lines', readonly=True, copy=False)
    description = fields.Text('Description')
    state = fields.Selection([('draft', 'Draft'),
                              ('started', 'Started'),
                              ('done', 'Done')], string='State', default='draft', required=True, track_visibility='onchange')

    @api.onchange("company_id")
    def _get_domain(self):
        domain = {}
        if self.company_id:
            warehouse_ids = self.env['stock.warehouse'].search([('company_id', '=', self.company_id.id)]).ids
            domain['warehouse_id'] = [('id', 'in', warehouse_ids)]
            account_ids = self.env['account.account'].search([('company_id', '=', self.company_id.id)]).ids
            domain['account_id'] = [('id', 'in', account_ids)]
        return {'domain': domain}

    # Компани, эхлэх болон дуусах огноогоор нэр талбар бөглөгдөх
    @api.onchange('date_from', 'date_to')
    def onchange_name(self):
        for report in self:
            name = ''
            if report.company_id:
                name += _('%s is ') % self.company_id.name
            if report.date_from:
                name += _('[%s - ') % self.date_from
            if report.date_to:
                name += _('%s] duration stock move edit account') % self.date_to
            report.name = name

    @api.onchange("warehouse_id")
    def _get_location_domain(self):
        domain = {}
        if self.warehouse_id:
            loc_ids = self.env['stock.location'].search([('usage', '=', 'internal'), ('location_id', 'child_of', [self.warehouse_id.view_location_id.id])]).ids
            domain['location_ids'] = [('id', 'in', loc_ids)]
        return {'domain': domain}

    @api.onchange("against_check")
    def _get_account(self):
        if self.against_check == True:
            self.account_id = None

    @api.multi
    def make_draft(self):
        self.write({'state': 'draft'})

    @api.multi
    def start(self):
        ''' Барааны хөдөлгөөний журналын мөрийн сонгосон данстай таарч буй эсэхийг шалгана. Сонгосон данстай таарахгүй байгаа журналын мөрүүдийг буцаана.
        '''
        self.income_move_line_ids = None
        self.expense_move_line_ids = None
        self.line_ids = None
        _logger.info(_('Start: Check account move line'))
        income_line_ids = []
        expense_line_ids = []
        income_where = ''
        expense_where = ''
        where = " AND m.company_id = " + str(self.company_id.id) + " "
        where += " AND m.date BETWEEN '" + self.date_from + "' AND '" + self.date_to + "' "
        if self.location_ids:
            if self.against_check:
                expense_where += ' AND m.location_dest_id in (' + ','.join(map(str, self.location_ids)) + ') '
                income_where += ' AND m.location_id in (' + ','.join(map(str, self.location_ids)) + ') '
            else:
                income_where += ' AND m.location_dest_id in (' + ','.join(map(str, self.location_ids)) + ') '
                expense_where += ' AND m.location_id in (' + ','.join(map(str, self.location_ids)) + ') '
        else:
            loc_id = self.env['stock.location'].search([('usage', '=', 'internal'), ('id', 'child_of', self.warehouse_id.view_location_id.ids), ('company_id', '=', self.company_id.id)]).ids
            if len(loc_id) > 0:
                if self.against_check:
                    expense_where += ' AND m.location_dest_id in (' + ','.join(map(str, loc_id)) + ') '
                    income_where += ' AND m.location_id in (' + ','.join(map(str, loc_id)) + ') '
                else:
                    income_where += ' AND m.location_dest_id in (' + ','.join(map(str, loc_id)) + ') '
                    expense_where += ' AND m.location_id in (' + ','.join(map(str, loc_id)) + ') '
        if self.against_check:
            where += " AND l.account_id = " + str(self.check_account_id.id) + " "
        else:
            where += " AND l.account_id != " + str(self.account_id.id) + " "
        # Барааны орлогын данс зөв байгаа эсэхийг шалгана
        self.env.cr.execute("SELECT l.id AS id "
                            "FROM stock_move m "
                            "JOIN account_move_line l ON (m.id = l.stock_move_id) "
                            "WHERE l.debit > 0 AND m.state = 'done' " + where + income_where + " ")
        income_lines = self.env.cr.fetchall()
        for income_line in income_lines:
            income_line_ids.append(income_line[0])
        # Сонгосон данстай зөрүүтэй барааны орлогын журналын мөрүүдийг буцаана
        if income_line_ids:
            self.income_move_line_ids = income_line_ids
        # Барааны зарлагын данс зөв байгаа эсэхийг шалгана
        self.env.cr.execute("SELECT l.id AS id "
                            "FROM stock_move m "
                            "JOIN account_move_line l ON (m.id = l.stock_move_id) "
                            "WHERE  l.credit > 0 AND m.state = 'done' " + where + expense_where + " ")
        expense_lines = self.env.cr.fetchall()
        for expense_line in expense_lines:
            expense_line_ids.append(expense_line[0])
        # Сонгосон данстай зөрүүтэй барааны зарлагын журналын мөрүүдийг буцаана
        if expense_line_ids:
            self.expense_move_line_ids = expense_line_ids
        _logger.info(_('Done: Check account move line'))
        self.write({'state': 'started'})

    @api.multi
    def edit_account(self):
        ''' Сонгосон данстай таарахгүй байгаа журналын мөрүүдийн дансыг сонгосон дансаар өөрчилнө
        '''
        line_obj = self.env['stock.move.edit.account.line']
        delete_income_line_ids = []
        delete_expense_line_ids = []
        _logger.info(_('Start: Stock move account edit'))
        for line in self.income_move_line_ids:
            # Сонгосон данстай зөрүүтэй барааны орлогын журналын мөрүүдийн дансыг сонгосон дансаар өөрчилнө
            old_account_id = line.account_id.id
            self.env.cr.execute("UPDATE account_move_line SET account_id = " + str(self.account_id.id) + " WHERE id = " + str(line.id) + " ")
            # Барааны данс зассан түүх
            line_obj.create({'edit_id': self.id,
                             'account_move_line_id': line.id,
                             'stock_move_id': line.stock_move_id.id,
                             'old_account_id': old_account_id,
                             'new_account_id': self.account_id.id,
                             'debit': line.debit,
                             'credit': line.credit})
            delete_income_line_ids.append(line.id)
        if delete_income_line_ids:
            # Дансыг нь зассан журналын мөрүүдийг хасах
            self.income_move_line_ids = [(2, line_id, False) for line_id in delete_income_line_ids]
        for line in self.expense_move_line_ids:
            # Сонгосон данстай зөрүүтэй барааны зарлагын журналын мөрүүдийн дансыг сонгосон дансаар өөрчилнө
            self.env.cr.execute("UPDATE account_move_line SET account_id = " + str(self.account_id.id) + " WHERE id = " + str(line.id) + " ")
            # Барааны данс зассан түүх
            line_obj.create({'edit_id': self.id,
                             'account_move_line_id': line.id,
                             'stock_move_id': line.stock_move_id.id,
                             'old_account_id': old_account_id,
                             'new_account_id': self.account_id.id,
                             'debit': line.debit,
                             'credit': line.credit})
            delete_expense_line_ids.append(line.id)
        if delete_expense_line_ids:
            # Дансыг нь зассан журналын мөрүүдийг хасах
            self.expense_move_line_ids = [(2, line_id, False) for line_id in delete_expense_line_ids]
        _logger.info(_('Done: Stock move account edit'))
        self.write({'state': 'done'})

class StockMoveEditAccountLine(models.Model):
    _name = 'stock.move.edit.account.line'
    _description = 'Stock Move Edit Account Line'
    
    edit_id = fields.Many2one('stock.move.edit.account', required=True, string='Stock Move Edit Account')
    account_move_line_id = fields.Many2one('account.move.line', string='Account Move Line')
    stock_move_id = fields.Many2one('stock.move', string='Stock Move')
    old_account_id = fields.Many2one('account.account', required=True, string='Old Account')
    new_account_id = fields.Many2one('account.account', required=True, string='New Account')
    debit = fields.Float('Debit')
    credit = fields.Float('Credit')
    user_id = fields.Many2one('res.users', required=True, string='Checked User', default=lambda self: self.env.user)
    checked_date = fields.Date('Checked Date', required=True, readonly=True, default=fields.Date.context_today)
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)