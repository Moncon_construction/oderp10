# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Stock Cost Calculation for Each Warehouse ",
    'version': '1.0',
    'depends': ['l10n_mn_stock_account_cost_for_each_wh', 'l10n_mn_stock_cost_calculation'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Агуулах бүрээр нийт орлого, зарлагын хувьд өртөг тооцолох багаж
    """,
    'data': [],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
