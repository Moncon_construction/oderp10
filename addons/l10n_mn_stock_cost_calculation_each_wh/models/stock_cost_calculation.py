# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError
import time
from datetime import date, datetime
import logging

_logger = logging.getLogger(__name__)


class StockCostCalculation(models.Model):
    _inherit = 'stock.cost.calculation'

    @api.multi
    def make_draft(self):
        self.write({'state': 'draft'})

    @api.multi
    def check_income_move(self, product_ids):
        income_product_ids = []
        where = " AND m.company_id = " + str(self.company_id.id) + " "
        if self.calculation_period == 'date':
            if not self.date_to:
                where += " AND m.date >= ' " + self.date_from + " ' "
            else:
                where += " AND m.date BETWEEN ' " + self.date_from + " ' AND ' " + self.date_to + " ' "
        self.env.cr.execute("SELECT distinct(m.product_id) "
                            "FROM stock_move m "
                            "JOIN stock_location l ON (m.location_id = l.id) "
                            "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                            "WHERE l.usage = 'production' AND l1.usage = 'internal' AND m.state = 'done' " + where + " "
                             "UNION "
                             "SELECT distinct(m.product_id) "
                             "FROM stock_move m "
                             "JOIN stock_location l ON (m.location_id = l.id) "
                             "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                             "WHERE l.usage = 'transit' AND l1.usage = 'internal' AND m.state = 'done' " + where + " "
                             "UNION "
                             "SELECT distinct(m.product_id) "
                             "FROM stock_move m "
                             "JOIN stock_location l ON (m.location_id = l.id) "
                             "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                             "WHERE l.usage = 'internal' AND l1.usage = 'transit' AND m.state = 'done' " + where + " ")
        products = self.env.cr.fetchall()
        for product in products:
            income_product_ids.append(product[0])
        if income_product_ids:
            self.income_product_ids = income_product_ids
            return income_product_ids

    @api.multi
    def purchase_refund_cost_edit(self, where):
        # Худалдан авалтын буцаалтын өртөгийг Худалдан авалтын өртөгөөр засах
        if self.ready_product_ids:
            where += ' AND m.product_id in (' + ','.join(map(str, self.ready_product_ids.ids)) + ') '
        self.env.cr.execute("UPDATE stock_move m "
                            "SET price_unit = p.price_unit "
                            "FROM (SELECT m.id AS id, COALESCE(m1.price_unit, 0) AS price_unit "
                                   "FROM stock_move m "
                                   "JOIN stock_move m1 ON (m1.id = m.origin_returned_move_id) "
                                   "JOIN stock_location l ON (m.location_id = l.id) "
                                   "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                                   "WHERE l.usage = 'internal' AND l1.usage = 'supplier' AND m.state = 'done' "
                                            + where + " AND  (m1.price_unit::numeric, 4) != (m.price_unit::numeric, 4) ) AS p "
                            "WHERE m.id = p.id ")

    @api.multi
    def check_transit(self, where, product, warehouse_ids):
        warehouse_obj = self.env['stock.warehouse']
        is_transit = False
        done_product = True
        note = ''
        same_warehouse = []
        other_warehouse = []
        # Нөхөн дүүргэлттэй эсэхийг шалгах
        self.env.cr.execute("SELECT distinct(o.supply_warehouse_id) "
                            "FROM stock_move m "
                            "JOIN stock_picking p ON (m.picking_id = p.id) "
                            "JOIN stock_transit_order o ON (p.transit_order_id = o.id) "
                            "WHERE m.state != 'cancel' AND o.supply_warehouse_id in (" + ','.join(map(str, warehouse_ids)) + ") "
                                    + where + " AND m.product_id = " + str(product) + " " )
        supply_warehouse = self.env.cr.fetchall()
        supply_warehouse_id = [x[0] for x in supply_warehouse]
        self.env.cr.execute("SELECT distinct(o.warehouse_id) "
                            "FROM stock_move m "
                            "JOIN stock_picking p ON (m.picking_id = p.id) "
                            "JOIN stock_transit_order o ON (p.transit_order_id = o.id) "
                            "WHERE m.state != 'cancel' AND o.warehouse_id in (" + ','.join(map(str, warehouse_ids)) + ") "
                                    + where + " AND m.product_id = " + str(product) + " ")
        receive_warehouse = self.env.cr.fetchall()
        receive_warehouse_id = [x[0] for x in receive_warehouse]
        if supply_warehouse_id and receive_warehouse_id:
            for warehouse in warehouse_ids:
                if warehouse not in receive_warehouse_id and warehouse not in supply_warehouse_id:
                    other_warehouse.append(warehouse)
            is_transit = True
            for x in supply_warehouse_id:
                if x in receive_warehouse_id:
                    same_warehouse.append(x)
                    supply_warehouse_id.remove(x)

            for warehouse in warehouse_obj.browse(supply_warehouse_id):
                note += _('\n        Warehouse: %s') % warehouse.name
                calculate_cost = self.cost(where, product, warehouse.id)
                if done_product:
                    done_product = calculate_cost[0]
                note += calculate_cost[1]
            if same_warehouse:
                warehouse_id = False
                self.env.cr.execute("SELECT m.date, o.supply_warehouse_id "
                                    "FROM stock_move m "
                                    "JOIN stock_picking p ON (m.picking_id = p.id) "
                                    "JOIN stock_transit_order o ON (p.transit_order_id = o.id) "
                                    "WHERE m.state = 'done' AND o.supply_warehouse_id in (" + ','.join(map(str, same_warehouse)) +  ") "
                                            + where + " AND m.product_id = " + str(product) + " "
                                    "ORDER BY m.date ")
                transit_lines = self.env.cr.fetchall()
                for transit_line in transit_lines:
                    if warehouse_id != transit_line[1]:
                        note += _('\n        Warehouse: %s') % warehouse_obj.browse(transit_line[1]).name
                        calculate_cost = self.cost(where, product, transit_line[1])
                        if done_product:
                            done_product = calculate_cost[0]
                        note += calculate_cost[1]
                    warehouse_id = transit_line[1]
                for x in receive_warehouse_id:
                    if x in same_warehouse:
                        receive_warehouse_id.remove(x)
            for warehouse in warehouse_obj.browse(receive_warehouse_id):
                note += _('\n        Warehouse: %s') % warehouse.name
                calculate_cost = self.cost(where, product, warehouse.id)
                if done_product:
                    done_product = calculate_cost[0]
                note += calculate_cost[1]
            for warehouse in warehouse_obj.browse(other_warehouse):
                note += _('\n        Warehouse: %s') % warehouse.name
                calculate_cost = self.cost(where, product, warehouse.id)
                if done_product:
                    done_product = calculate_cost[0]
                note += calculate_cost[1]
        return is_transit, done_product, note

    @api.multi
    def income_cost_calculate(self, where, where_warehouse, product):
        select = ''
        # Үйлдвэрийн орлогыг өртөг тооцоолоход нэмэх
        if self.is_module_installed("mrp"):
            select = "UNION ALL " \
                     "SELECT t.warehouse_id AS wid, sum(m.product_qty * m.price_unit) AS cost, sum(m.product_qty) AS qty " \
                     "FROM stock_move m " \
                     "JOIN stock_location l ON (m.location_id = l.id) " \
                     "JOIN stock_location l1 ON (m.location_dest_id = l1.id) " \
                     "JOIN mrp_production p ON (m.production_id = p.id) " \
                     "JOIN stock_picking_type t ON (p.picking_type_id = t.id) " \
                     "WHERE l.usage = 'production' AND l1.usage = 'internal' AND m.state = 'done' AND m.product_id = " + str(product) + where + " " \
                     "GROUP BY t.warehouse_id "

        # Худалдан авалтын орлого, Эхний тооллогын орлого, Нөхөн дүүргэлтийн орлого болон Үйлдвэрийн орлогыг нэмэн Худалдан авалтын буцаалтыг хасан өртгийг тооцоолох
        self.env.cr.execute("SELECT SUM(COALESCE(p.cost, 0)) AS cost, SUM(COALESCE(p.qty, 0)) AS qty "
                            "FROM (SELECT t.warehouse_id AS wid, sum(m.product_qty * m.price_unit) AS cost, sum(m.product_qty) AS qty "
                                   "FROM stock_move m "
                                   "JOIN stock_location l ON (m.location_id = l.id) "
                                   "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                                   "JOIN stock_picking_type t ON (m.picking_type_id = t.id) "
                                   "WHERE l.usage = 'supplier' AND l1.usage = 'internal' AND m.state = 'done' AND m.product_id = " + str(product) + where + " "
                                   "GROUP BY t.warehouse_id "
                                   "UNION ALL "
                                   "SELECT w.id AS wid, sum(m.product_qty * m.price_unit) AS cost, sum(m.product_qty) AS qty "
                                   "FROM stock_move m "
                                   "JOIN stock_location l ON (m.location_id = l.id) "
                                   "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                                   "JOIN stock_inventory i ON (m.inventory_id = i.id) "
                                   "JOIN stock_warehouse w ON (w.lot_stock_id = l1.id) "
                                   "WHERE l.usage = 'inventory' AND l1.usage = 'internal' AND m.state = 'done' "
                                            "AND i.is_initial = 't' AND m.product_id = " + str(product) + where + " "
                                   "GROUP BY w.id "
                                   "UNION ALL "
                                   "SELECT t.warehouse_id AS wid, sum(-m.product_qty * m.price_unit) AS cost, sum(-m.product_qty) AS qty "
                                   "FROM stock_move m "
                                   "JOIN stock_move m1 ON (m1.id = m.origin_returned_move_id) "
                                   "JOIN stock_location l ON (m.location_id = l.id) "
                                   "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                                   "JOIN stock_picking_type t ON (m.picking_type_id = t.id) "
                                   "WHERE l.usage = 'internal' AND l1.usage = 'supplier' AND m.state = 'done' AND m.product_id = " + str(product) + where + " "
                                   "GROUP BY t.warehouse_id "
                                   "UNION ALL "
                                   "SELECT t.warehouse_id AS wid, sum(m.product_qty * m.price_unit) AS cost, sum(m.product_qty) AS qty "
                                   "FROM stock_move m "
                                   "JOIN stock_location l ON (m.location_id = l.id) "
                                   "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                                   "JOIN stock_picking_type t ON (m.picking_type_id = t.id) "
                                   "WHERE l.usage = 'transit' AND l1.usage = 'internal' AND m.state = 'done' "
                                   "AND m.origin_returned_move_id IS NULL AND m.product_id = " + str(product) + where + " "
                                   "GROUP BY t.warehouse_id " + select + ") AS p "
                            "JOIN stock_warehouse w ON (w.id = p.wid) " + where_warehouse + " "
                            "GROUP BY p.wid")
        return self.env.cr.dictfetchall()

    @api.multi
    def inventory_income_cost_calculate(self, where, where_warehouse, product):
        # Тооллогын орлогоор өртгийг тооцоолох
        self.env.cr.execute("SELECT SUM(COALESCE(p.cost, 0)) AS cost, SUM(COALESCE(p.qty, 0)) AS qty "
                            "FROM (SELECT w.id AS wid, sum(m.product_qty * m.price_unit) AS cost, sum(m.product_qty) AS qty "
                                    "FROM stock_move m "
                                    "JOIN stock_location l ON (m.location_id = l.id) "
                                    "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                                    "JOIN stock_inventory i ON (m.inventory_id = i.id) "
                                    "JOIN stock_warehouse w ON (w.lot_stock_id = l1.id) "
                                    "WHERE l.usage = 'inventory' AND l1.usage = 'internal' AND m.state = 'done' "
                                            "AND i.is_initial = 'f' AND m.product_id = " + str(product) + where + " "
                                    "GROUP BY w.id ) AS p "
                            "JOIN stock_warehouse w ON (w.id = p.wid) " + where_warehouse + " "
                            "GROUP BY p.wid")
        return self.env.cr.dictfetchall()

    @api.multi
    def cost(self, where, product, warehouse):
        warehouse_price_obj = self.env['product.warehouse.standard.price']
        note = ''
        initial_fault = True
        done_product = True
        is_calculate = False
        price_unit = False
        old_cost = 0
        where_warehouse = " WHERE p.wid = " + str(warehouse) + " "
        _logger.info(_('warehouse id: %s') % (warehouse))
        locations = []
        wh = self.env['stock.warehouse'].browse(warehouse)
        loc_id = self.env['stock.location'].search([('usage', '=', 'internal'), ('id', 'child_of', wh.view_location_id.ids),
                                                    ('company_id', '=', self.company_id.id)]).ids
        if len(loc_id) > 0:
            locations = ','.join(map(str, loc_id))
        incomes = self.income_cost_calculate(where, where_warehouse, product)
        if len(incomes) == 0:
            incomes = self.inventory_income_cost_calculate(where, where_warehouse, product)
        _logger.info(_('incomes: %s') % (incomes))
        if len(incomes) > 0:
            qty = cost = 0
            if self.calculation_period == 'date':
                # Хугацааны хооронд тооцоолж байгаа тохиолдолд эхний үлдэгдлийг олно
                self.env.cr.execute("SELECT SUM(l.cost) AS cost , SUM(l.qty) AS qty "
                                    "FROM ( SELECT m.product_id AS prod_id, "
                                                "CASE WHEN m.location_id NOT IN (" + locations + ") AND m.location_dest_id IN (" + locations + ") "
                                                "THEN m.product_qty "
                                                "WHEN m.location_id IN (" + locations + ") AND m.location_dest_id NOT IN (" + locations + ") "
                                                "THEN -m.product_qty ELSE 0 END AS qty, "
                                                "CASE WHEN m.location_id NOT IN (" + locations + ") AND m.location_dest_id IN (" + locations + ") "
                                                "THEN m.product_qty * m.price_unit "
                                                "WHEN m.location_id IN (" + locations + ") AND m.location_dest_id NOT IN (" + locations + ") "
                                                "THEN -m.product_qty * m.price_unit ELSE 0 END AS cost "
                                            "FROM stock_move m "
                                            "WHERE m.date < %s AND m.state = 'done' AND m.company_id = " + str(self.company_id.id) + " "
                                                    "AND m.product_id = " + str(product) + ") AS l "
                                    "GROUP BY l.prod_id ", (self.date_from, ))
                initials = self.env.cr.dictfetchall()
                for initial in initials:
                    qty += initial['qty']
                    cost += initial['cost']
                if qty > 0 and cost >= 0:
                    old_cost = round(cost / qty, self.env['decimal.precision'].precision_get('Account'))
                    note += _('\n    %s before') % self.date_from
                    note += _('\n    Warehouse: %s') % income['wname']
                    note += _('\n    Average cost: %s, Qty: %s') % (old_cost, qty)
                elif qty == 0 and cost == 0:
                    note += _('\n    %s before') % self.date_from
                    note += _('\n    Average cost: 0, Qty: 0')
                else:
                    initial_fault = False
                    note += _('\n    %s before') % self.date_from
                    note += _('\n    Initial cost invalid')
                    note += _('\n    Total cost: %s, Qty: %s') % (cost, qty)
            if initial_fault:
                for income in incomes:
                    qty += income['qty']
                    cost += income['cost']
                if qty > 0 and cost > 0:
                    price_unit = round(cost / qty, self.env['decimal.precision'].precision_get('Account'))
                    is_calculate = True
                    note += _('\n        Renewed cost: %s') % price_unit
                elif qty == 0 and cost == 0:
                    self.env.cr.execute("SELECT SUM(p.qty) "
                                        "FROM (SELECT m.product_id AS pid, SUM(m.product_qty) AS qty "
                                                "FROM stock_move m "
                                                "JOIN stock_location l ON (m.location_id = l.id) "
                                                "WHERE m.state = 'done' AND l.usage != 'supplier' " + where +
                                                        "AND m.product_id = " + str(product) + " AND m.location_dest_id in (" + locations + ") "
                                                "GROUP BY m.product_id "
                                                "UNION ALL "
                                                "SELECT m.product_id AS pid, SUM(-m.product_qty) AS qty "
                                                "FROM stock_move m "
                                                "JOIN stock_location l ON (m.location_dest_id = l.id) "
                                                "WHERE m.state = 'done' AND l.usage != 'supplier' " + where +
                                                        "AND m.product_id = " + str(product) + " AND m.location_id in (" + locations + ") " ""
                                                "GROUP BY m.product_id ) AS p "
                                        "GROUP BY p.pid ")
                    expense = self.env.cr.fetchone()
                    if not expense or len(expense) == 0 or (len(expense) > 0 and expense[0] == None or expense[0] == 0):
                        is_calculate = True
                        note += _('\n        ')
                    else:
                        note += _('\n        This product not calculate cost. Cost = 0, Income Qty = 0. But Expense Qty = %s' % (expense[0]))
                else:
                    note += _('\n        This product not calculate cost. Total cost: %s, Qty: %s') %(cost, qty)
                    done_product = False
                if is_calculate and price_unit > 0:
                    # Агуулах бүрээр өртөг хөтөлж байгаа үед тооцоолсон шинэ өртгийг үнийн түүхэнд нэмнэ
                    self._cr.execute("DELETE FROM product_warehouse_standard_price WHERE product_id = %s AND warehouse_id = %s", (product, warehouse))
                    warehouse_price_obj.create({'product_id': product,
                                                'warehouse_id': warehouse,
                                                'standard_price': price_unit,
                                                'initial_standard_price': old_cost,
                                                'company_id': self.company_id.id
                                                })
                    # Борлуулалтын зарлагын өртгийг тооцоолсон өртгөөр засах
                    # self.env.cr.execute("SELECT m.id AS id FROM stock_move m "
                    #                     "JOIN stock_location l ON (m.location_dest_id = l.id) "
                    #                     "JOIN stock_location l1 ON (m.location_id = l1.id) "
                    #                     "JOIN stock_picking_type t ON (m.picking_type_id = t.id) "
                    #                     "WHERE l.usage = 'customer' AND l1.usage = 'internal' AND t.warehouse_id = " + str(warehouse) + " "
                    #                     + where + "AND m.product_id = " + str(product) + "AND m.price_unit != " + str(price_unit) + " ")
                    # ids = self.env.cr.dictfetchall()
                    self.env.cr.execute("UPDATE stock_move m "
                                        "SET price_unit = " + str(price_unit) + " "
                                        "FROM (SELECT m.id AS id FROM stock_move m "
                                                "JOIN stock_location l ON (m.location_dest_id = l.id) "
                                                "JOIN stock_location l1 ON (m.location_id = l1.id) "
                                                "JOIN stock_picking_type t ON (m.picking_type_id = t.id) "
                                                "WHERE l.usage = 'customer' AND l1.usage = 'internal' AND t.warehouse_id = " + str(warehouse) + " "
                                                        + where + "AND m.product_id = " + str(product) + "AND m.price_unit != " + str(price_unit) + ") AS p "
                                        "WHERE m.id = p.id ")

                    # Борлуулалтын буцаалтыг өртгийг тооцоолсон өртгөөр засах
                    self.env.cr.execute("UPDATE stock_move m "
                                        "SET price_unit = " + str(price_unit) + " "
                                        "FROM (SELECT m.id AS id FROM stock_move m "
                                                "JOIN stock_location l ON (m.location_id = l.id) "
                                                "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                                                "JOIN stock_picking_type t ON (m.picking_type_id = t.id) "
                                                "WHERE l.usage = 'customer' AND l1.usage = 'internal' AND t.warehouse_id = " + str(warehouse) + " "
                                                        + where + "AND m.product_id = " + str(product) + " AND m.price_unit != " + str(price_unit) + ") AS p "
                                        "WHERE m.id = p.id ")

                    # Тооллогын зарлагын өртгийг тооцоолсон өртгөөр засах
                    self.env.cr.execute("UPDATE stock_move m "
                                        "SET price_unit = " + str(price_unit) + " "
                                        "FROM (SELECT m.id AS id FROM stock_move m "
                                                "JOIN stock_location l ON (m.location_dest_id = l.id) "
                                                "JOIN stock_location l1 ON (m.location_id = l1.id) "
                                                "JOIN stock_inventory i ON (m.inventory_id = i.id) "
                                                "JOIN stock_warehouse w ON (w.lot_stock_id = l1.id) "
                                                "WHERE l.usage = 'inventory' AND l1.usage = 'internal' AND w.id= " + str(warehouse) + " "
                                                        + where + "AND i.is_initial = 'f' AND m.product_id = " + str(product) + " AND m.price_unit != " + str(price_unit) + ") AS p "
                                        "WHERE m.id = p.id ")

                    # Тооллогын орлогын өртгийг тооцоолсон өртгөөр засах
                    self.env.cr.execute("UPDATE stock_move m "
                                        "SET price_unit = " + str(price_unit) + " "
                                        "FROM (SELECT m.id AS id FROM stock_move m "
                                                "JOIN stock_location l ON (m.location_id = l.id) "
                                                "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                                                "JOIN stock_inventory i ON (m.inventory_id = i.id) "
                                                "JOIN stock_warehouse w ON (w.lot_stock_id = l1.id) "
                                                "WHERE l.usage = 'inventory' AND l1.usage = 'internal' AND w.id = " + str(warehouse) + " "
                                                        + where + "AND i.is_initial = 'f' AND m.product_id = " + str(product) + " AND m.price_unit != " + str(price_unit) + ") AS p "
                                        "WHERE m.id = p.id ")

                    # Шаардахын зарлагын өртгийг тооцоолсон өртгөөр засах
                    self.env.cr.execute("UPDATE stock_move m "
                                        "SET price_unit = " + str(price_unit) + " "
                                        "FROM (SELECT m.id AS id FROM stock_move m "
                                                "JOIN stock_location l ON (m.location_dest_id = l.id) "
                                                "JOIN stock_location l1 ON (m.location_id = l1.id) "
                                                "JOIN stock_picking p ON (m.picking_id = p.id) "
                                                "JOIN stock_picking_type t ON (m.picking_type_id = t.id) "
                                                "WHERE l.usage = 'inventory' AND l1.usage = 'internal' AND t.warehouse_id = " + str(warehouse) + " "
                                                        + where + "AND m.product_id = " + str(product) + " AND m.price_unit != " + str(price_unit) + ") AS p "
                                        "WHERE m.id = p.id ")

                    # Шаардахын буцаалтын өртгийг тооцоолсон өртгөөр засах
                    self.env.cr.execute("UPDATE stock_move m "
                                        "SET price_unit =  " + str(price_unit) + " "
                                         "FROM (SELECT m.id AS id FROM stock_move m "
                                                 "JOIN stock_location l ON (m.location_id = l.id) "
                                                 "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                                                 "JOIN stock_picking p ON (m.picking_id = p.id) "
                                                 "JOIN stock_picking_type t ON (m.picking_type_id = t.id) "
                                                 "WHERE l.usage = 'inventory' AND l1.usage = 'internal' AND t.warehouse_id = " + str(warehouse) + " "
                                                        + where + "AND m.product_id = " + str(product) + " AND m.price_unit != " + str(price_unit) + ") AS p "
                                         "WHERE m.id = p.id ")

                    # Үйлдвэрлэлийн зарлагын өртгийг тооцоолсон өртгөөр засах
                    if self.is_module_installed("mrp"):
                        self.env.cr.execute("UPDATE stock_move m "
                                            "SET price_unit = " + str(price_unit) + " "
                                            "FROM (SELECT m.id AS id FROM stock_move m "
                                                    "JOIN stock_location l ON (m.location_dest_id = l.id) "
                                                    "JOIN mrp_production p ON (m.raw_material_production_id = p.id) "
                                                    "JOIN stock_picking_type t ON (p.picking_type_id = t.id) "
                                                    "WHERE l.usage = 'production' AND t.warehouse_id = " + str(warehouse) + " "
                                                            + where + "AND m.product_id = " + str(product) + " AND m.price_unit != " + str(price_unit) + ") AS p "
                                            "WHERE m.id = p.id ")

                    # Нөхөн дүүргэлтийн зарлагын өртгийг тооцоолсон өртгөөр засах
                    self.env.cr.execute("UPDATE stock_move m "
                                        "SET price_unit = " + str(price_unit) + " "
                                        "FROM (SELECT m.id AS id FROM stock_move m "
                                                "JOIN stock_location l ON (m.location_dest_id = l.id) "
                                                "JOIN stock_location l1 ON (m.location_id = l1.id) "
                                                "JOIN stock_picking_type t ON (m.picking_type_id = t.id) "
                                                "WHERE l.usage = 'transit' AND l1.usage = 'internal' AND t.warehouse_id = " + str(warehouse) + " "
                                                        + where + "AND m.product_id = " + str(product) + " AND m.price_unit != " + str(price_unit) + ") AS p "
                                        "WHERE m.id = p.id ")

                    # Нөхөн дүүргэлтийн зарлагын буцаалтыг өртгийг тооцоолсон өртгөөр засах
                    self.env.cr.execute("UPDATE stock_move m "
                                        "SET price_unit = " + str(price_unit) + " "
                                        "FROM (SELECT m.id AS id FROM stock_move m "
                                                "JOIN stock_location l ON (m.location_id = l.id) "
                                                "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                                                "JOIN stock_picking_type t ON (m.picking_type_id = t.id) "
                                                "WHERE l.usage = 'transit' AND l1.usage = 'internal' AND t.warehouse_id = " + str(warehouse) + " "
                                                        + where + "AND m.product_id = " + str(product) +
                                                        " AND m.price_unit != " + str(price_unit) + " AND m.origin_returned_move_id IS NOT NULL) AS p "
                                        "WHERE m.id = p.id ")

                    # Тухайн агуулахаас өөр агуулах руу нөхөн дүүргэсэн орлогын өртгийг тооцоолсон өртгөөр засах
                    self.env.cr.execute("UPDATE stock_move m "
                                        "SET price_unit = " + str(price_unit) + " "
                                        "FROM (SELECT m.id AS id FROM stock_move m "
                                                "JOIN stock_location l ON (m.location_id = l.id) "
                                                "JOIN stock_location l1 ON (m.location_dest_id = l1.id) "
                                                "JOIN stock_picking p ON (m.picking_id = p.id) "
                                                "JOIN stock_transit_order o ON (p.transit_order_id = o.id) "
                                                "WHERE l.usage = 'transit' AND l1.usage = 'internal' AND o.supply_warehouse_id = " + str(warehouse) + " "
                                                        + where + "AND m.product_id = " + str(product) + " AND m.price_unit != " + str(price_unit) + ") AS p "
                                        "WHERE m.id = p.id ")

                    # Тухайн агуулахаас өөр агуулах руу нөхөн дүүргэсэн орлогын буцаалтын өртгийг тооцоолсон өртгөөр засах
                    self.env.cr.execute("UPDATE stock_move m "
                                        "SET price_unit = " + str(price_unit) + " "
                                        "FROM (SELECT m.id AS id FROM stock_move m "
                                                "JOIN stock_location l ON (m.location_dest_id = l.id) "
                                                "JOIN stock_location l1 ON (m.location_id = l1.id) "
                                                "JOIN stock_picking p ON (m.picking_id = p.id) "
                                                "JOIN stock_transit_order o ON (p.transit_order_id = o.id) "
                                                "WHERE l.usage = 'transit' AND l1.usage = 'internal' AND o.supply_warehouse_id = " + str(warehouse) + " "
                                                        + where + "AND m.product_id = " + str(product) +
                                                        "AND m.price_unit != " + str(price_unit) +" AND m.origin_returned_move_id IS NOT NULL) AS p "
                                        "WHERE m.id = p.id ")
                elif not is_calculate:
                    done_product = False
            else:
                done_product = False
        else:
            self.env.cr.execute("SELECT SUM(p.qty) "
                                "FROM (SELECT m.product_id AS pid, SUM(m.product_qty) AS qty "
                                        "FROM stock_move m "
                                        "JOIN stock_location l ON (m.location_id = l.id) "
                                        "WHERE m.state = 'done' AND l.usage != 'supplier' " + where +
                                                "AND m.product_id = " + str(product) + " AND m.location_dest_id in (" + locations + ") "
                                        "GROUP BY m.product_id "
                                        "UNION ALL "
                                        "SELECT m.product_id AS pid, SUM(-m.product_qty) AS qty "
                                        "FROM stock_move m "
                                        "JOIN stock_location l ON (m.location_dest_id = l.id) "
                                        "WHERE m.state = 'done' AND l.usage != 'supplier' " + where +
                                                "AND m.product_id = " + str(product) + " AND m.location_dest_id in (" + locations + ") " ""
                                         "GROUP BY m.product_id ) AS p "
                                "GROUP BY p.pid ")
            expense = self.env.cr.fetchone()
            if len(expense) == 0 or (len(expense) > 0 and expense[0] == None or expense[0] == 0):
                note += _('\n        ')
            else:
                done_product = False
                note += _('\n        This warehouse not income.')
                note += _('\n        This product not calculate cost. Cost = 0, Income Qty = 0. But Expense Qty = %s' % (expense[0]))

        return done_product, note

    @api.multi
    def calculate_cost(self):
        '''Энэ товчийг нэг удаа дараад 'Өртөг тооцоход бэлэн' табнаас барааг 'Нэг удаагийн уншуулах хэмжээ' талбарт заасан тоогоор уншиж,
            уншсан бараануудын өртгийг тооцоолж бүх stock.move-үүд дэх өртгийг зөв болгож, барааны өнөөдрийн өртгийг засна. Амжилттай өртөг
            тооцогдсон бараануудыг 'Өртөг тооцогдсон бараанууд' таб руу шилжүүлнэ.
        '''
        done_products_obj = self.env['stock.cost.calculation.done']
        warehouse_obj = self.env['stock.warehouse']
        location_obj = self.env['stock.location']
        count = 1
        description = ''
        locations = []
        delete_product_ids = []
        _logger.info(_('Start: Product cost calculate'))
        # Эхлээд бүх орлогын нийт өртгийг нийт тоо хэмжээнд хувааж дундаж өртгийг олооод олсон өртгөө бүх зарлага дээр тавиах аргаар өртөг тооцно
        where = " AND m.company_id = " + str(self.company_id.id) + " "
        if self.calculation_period == 'date':
            if not self.date_to:
                where += " AND m.date >= '" + self.date_from + "' "
            else:
                where += " AND m.date BETWEEN '" + self.date_from + "' AND '" + self.date_to + "' "
        loc_id = location_obj.search([('usage', '=', 'internal'), ('company_id', '=', self.company_id.id)]).ids
        if len(loc_id) > 0:
            locations = tuple(loc_id)
        # Худалдан авалтын буцаалтын дүнг засна
        self.purchase_refund_cost_edit(where)
        for product in self.ready_product_ids:
            done_product = True
            _logger.info(_('product id: %s') % (product.id))
            note = _('\n [%s] %s product cost calculate start. ') % (product.default_code, product.name)
            if self.is_module_installed("mrp"):
                note += self.mrp_income_cost_edit(where, product.id)
            # Тухайн барааны тоо хэмжээ нь 0-с их агуулахуудыг гаргах
            self.env.cr.execute("SELECT l.lid as lid, SUM(l.qty) AS qty "
                                "FROM ( SELECT m.product_id AS prod_id, "
                                        "CASE WHEN m.location_id NOT IN %s AND m.location_dest_id IN %s "
                                            "THEN TRUNC(m.product_qty, 4) "
                                            "WHEN m.location_id IN %s AND m.location_dest_id NOT IN %s "
                                            "THEN TRUNC(-m.product_qty, 4) ELSE 0 END AS qty, "
                                        "CASE WHEN m.location_id NOT IN %s AND m.location_dest_id IN %s "
                                            "THEN m.location_dest_id "
                                            "WHEN m.location_id IN %s AND m.location_dest_id NOT IN %s "
                                            "THEN location_id ELSE 0 END AS lid "
                                        "FROM stock_move m "
                                        "WHERE m.state = 'done' AND m.product_id = " + str(product.id) + where + " ) AS l "
                                "GROUP BY l.lid "
                                "HAVING SUM(l.qty) >= 0 ", (locations, locations, locations, locations,
                                                            locations, locations, locations, locations))
            location_ids = self.env.cr.fetchall()
            warehouse_ids = []
            for location_id in location_ids:
                wh = warehouse_obj.search([('view_location_id', 'parent_of', location_id[0])])
                if wh and wh.ids[0] not in warehouse_ids:
                    warehouse_ids.append(wh.id)
            _logger.info(_('warehouse ids: %s') % (warehouse_ids))
            if warehouse_ids:
                is_transit = self.check_transit(where, product.id, warehouse_ids)[0]
                if is_transit:
                    calculate_cost = self.check_transit(where, product.id, warehouse_ids)
                    if done_product:
                        done_product = calculate_cost[1]
                    note += calculate_cost[2]
                else:
                    for warehouse in warehouse_obj.browse(warehouse_ids):
                        note += _('\n        Warehouse: %s') % warehouse.name
                        calculate_cost = self.cost(where, product.id, warehouse.id)
                        if done_product:
                            done_product = calculate_cost[0]
                        note += calculate_cost[1]
            else:
                done_product = False
            _logger.info(done_product)
            if done_product:
                _logger.info(_('Calculated count: %s') % (count))
                count += 1
                done_products_obj.create({'product_id': product.id,
                                          'calculation_note': note,
                                          'calculation_id': self.id,
                                          'state': 'stock_move_corrected',
                                          })
                delete_product_ids.append(product.id)
            else:
                description += note
        if delete_product_ids:
            self.ready_product_ids = [(2, product_id, False) for product_id in delete_product_ids]
        self.description = description
        _logger.info(_('Done: Product cost calculated'))