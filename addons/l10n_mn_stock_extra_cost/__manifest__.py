# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Stock Extra Cost",
    'version': '1.0',
    'depends': ['l10n_mn_stock',
                'l10n_mn_extra_cost_item'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
        Stock Additional Features
    """,
    'data': [
            'security/ir.model.access.csv',
            'wizard/stock_extra_cost_print.xml',
            'wizard/stock_ec_backorder_confirmation_views.xml',
            'wizard/stock_complete_approval_views.xml',
            'wizard/stock_nocost_approval_views.xml',
            'wizard/stock_cost_approval_views.xml',
            'views/stock_extra_cost_view.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
